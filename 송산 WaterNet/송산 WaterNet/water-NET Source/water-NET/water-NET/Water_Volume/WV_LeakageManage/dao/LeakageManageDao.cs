﻿using System.Text;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.dao;
using WaterNet.WV_LeakageManage.vo;
using Oracle.DataAccess.Client;
using System;

namespace WaterNet.WV_LeakageManage.dao
{
    public class LeakageManageDao : BaseDao
    {
        private static LeakageManageDao dao = null;

        private LeakageManageDao() { }

        public static LeakageManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new LeakageManageDao();
            }
            return dao;
        }

        public void SelectLeakageCompare(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                                                           ");
            query.AppendLine("(                                                                                                                                                                     ");
            query.AppendLine("select c1.loc_code                                                                                                                                                    ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                        ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                              ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                        ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                              ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                               ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                                                    ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                           ");
            query.AppendLine("      ,decode(c1.ftr_code, 'BZ002', (select loc_gbn from cm_location where loc_name = c1.rel_loc_name)) rel_loc_gbn                                                   ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                                                                ");
            query.AppendLine("      ,c1.ord                                                                                                                                                         ");
            query.AppendLine("  from                                                                                                                                                                ");
            query.AppendLine("      (                                                                                                                                                               ");
            query.AppendLine("       select sgccd                                                                                                                                                   ");
            query.AppendLine("             ,loc_code                                                                                                                                                ");
            query.AppendLine("             ,ploc_code                                                                                                                                               ");
            query.AppendLine("             ,loc_name                                                                                                                                                ");
            query.AppendLine("             ,ftr_idn                                                                                                                                                 ");
            query.AppendLine("             ,ftr_code                                                                                                                                                ");
            query.AppendLine("             ,rel_loc_name                                                                                                                                            ");
            query.AppendLine("             ,kt_gbn                                                                                                                                                  ");
            query.AppendLine("             ,rownum ord                                                                                                                                              ");
            query.AppendLine("         from cm_location                                                                                                                                             ");
            query.AppendLine("        where 1 = 1                                                                                                                                                   ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                    ");
            query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                                 ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                         ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                          ");
            query.AppendLine("      ) c1                                                                                                                                                            ");
            query.AppendLine("       ,cm_location c2                                                                                                                                                ");
            query.AppendLine("       ,cm_location c3                                                                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                          ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                  ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                  ");
            query.AppendLine(" order by c1.ord                                                                                                                                                      ");
            query.AppendLine(")                                                                                                                                                                     ");
            query.AppendLine("select loc.loc_code                                                                                                                                                   ");
            query.AppendLine("      ,loc.lblock                                                                                                                                                     ");
            query.AppendLine("      ,loc.mblock                                                                                                                                                     ");
            query.AppendLine("      ,loc.sblock                                                                                                                                                     ");
            query.AppendLine("      ,to_date(wrr.year_mon, 'yyyymm') year_mon                                                                                                                       ");
            query.AppendLine("      ,to_number(wrr.water_supplied) water_supplied                                                                                                                   ");
            query.AppendLine("      ,to_number(wrr.revenue) revenue                                                                                                                                 ");
            query.AppendLine("      ,to_number(wrr.water_supplied-wrr.revenue) non_revenue                                                                                                          ");
            query.AppendLine("      ,to_number(wto.leaks) leaks                                                                                                                                     ");
            query.AppendLine("      ,0 leaks_davg                                                                                                                                                   ");
            query.AppendLine("  from loc                                                                                                                                                            ");
            query.AppendLine("      ,wv_revenue_ratio wrr                                                                                                                                           ");
            query.AppendLine("      ,wv_total wto                                                                                                                                                   ");
            query.AppendLine(" where 1 = 1                                                                                                                                                          ");
            query.AppendLine("   and wrr.loc_code = loc.loc_code                                                                                                                                    ");
            query.AppendLine("   and to_date(wrr.year_mon, 'yyyymm') between to_date(substr(:STARTDATE,1,6), 'yyyymm') and to_date(substr(:ENDDATE,1,6), 'yyyymm')                                  ");
            query.AppendLine("   and wto.loc_code(+) = wrr.loc_code                                                                                                                                 ");
            query.AppendLine("   and to_date(wto.year_mon(+), 'yyyymm') = to_date(wrr.year_mon, 'yyyymm')                                                                                           ");
            query.AppendLine(" order by to_date(wrr.year_mon, 'yyyymm')                                                                                                                             ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            
            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //일자별 블록옵션 저장
        public void UpdateLeakageManageOption(OracleDBManager manager, LeakageCalculationVO leakageVO)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_block_day wbd                                                                                    ");
            query.AppendLine("   set					                                                  								 ");
            query.AppendLine("	(													                                                     ");
            query.AppendLine("		 jumal_used											                                                 ");
            query.AppendLine("		,gajung_used										                                                 ");
            query.AppendLine("		,bgajung_used										                                                 ");
            query.AppendLine("		,special_used_percent								                                                 ");
            query.AppendLine("		,special_used										                                                 ");
            query.AppendLine("		,n1													                                                 ");
            query.AppendLine("	)													                                                     ");
            query.AppendLine("	=													                                                     ");
            query.AppendLine("	(select												                                                     ");
            query.AppendLine("		 :JUMAL_USED                                                                                         ");
            query.AppendLine("		,:GAJUNG_USED                                                                                        ");
            query.AppendLine("		,:BGAJUNG_USED                                                                                       ");
            query.AppendLine("		,:SPECIAL_USED_PERCENT                                                                               ");
            query.AppendLine("		,:SPECIAL_USED                                                                                       ");
            query.AppendLine("		,:N1                                                                                                 ");
            query.AppendLine("	  from dual											                                                     ");
            query.AppendLine("	)													                                                     ");
            query.AppendLine("where wbd.datee = :DATEE                                                                                   ");
            query.AppendLine("  and wbd.loc_code = :LOC_CODE                                                                             ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("JUMAL_USED", OracleDbType.Varchar2)
                     ,new OracleParameter("GAJUNG_USED", OracleDbType.Varchar2)
                     ,new OracleParameter("BGAJUNG_USED", OracleDbType.Varchar2)
                     ,new OracleParameter("SPECIAL_USED_PERCENT", OracleDbType.Varchar2)
                     ,new OracleParameter("SPECIAL_USED", OracleDbType.Varchar2)
                     ,new OracleParameter("N1", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = leakageVO.LeakageOptionVO[0].JUMAL_USED.ToString();
            parameters[1].Value = leakageVO.LeakageOptionVO[0].GAJUNG_USED.ToString();
            parameters[2].Value = leakageVO.LeakageOptionVO[0].BGAJUNG_USED.ToString();
            parameters[3].Value = leakageVO.LeakageOptionVO[0].SPECIAL_USED_PERCENT.ToString();
            parameters[4].Value = leakageVO.LeakageOptionVO[0].SPECIAL_USED.ToString();
            parameters[5].Value = leakageVO.LeakageOptionVO[0].N1.ToString();
            parameters[6].Value = leakageVO.DATEE.ToString("yyyyMMdd");
            parameters[7].Value = leakageVO.LOC_CODE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //블록옵션 등록
        public void InsertLeakageDefaultOption(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into wv_block_day                                                                                       ");
            query.AppendLine("select wbo.loc_code																							 ");
            query.AppendLine("       ,tmp.datee																								 ");
            query.AppendLine("       ,wbo.jumal_used																					     ");
            query.AppendLine("       ,wbo.gajung_used																						 ");
            query.AppendLine("       ,wbo.bgajung_used																						 ");
            query.AppendLine("       ,wbo.special_used																						 ");
            query.AppendLine("       ,wbo.n1																								 ");
            query.AppendLine("       ,wbo.special_used_percent																				 ");
            query.AppendLine("  from wv_block_default_option wbo																			 ");
            query.AppendLine("      ,(																										 ");
            query.AppendLine("       select to_char(to_date(:STARTDATE, 'yyyymmdd') -1 + rownum, 'yyyymmdd') datee                           ");
            query.AppendLine("         from dual connect by to_date(:STARTDATE, 'yyyymmdd') -1 + rownum <= to_date(:ENDDATE, 'yyyymmdd')     ");
            query.AppendLine("       ) tmp																									 ");
            query.AppendLine("  where 1 = 1																									 ");
            query.AppendLine("    and wbo.loc_code= :LOC_CODE                                                                                ");
            query.AppendLine("    and not exists (select * from wv_block_day where datee = tmp.datee and loc_code = :LOC_CODE)               ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["LOC_CODE"];
            parameters[4].Value = parameter["LOC_CODE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //누수한계치 수정
        public object UpdateLeakageMaxValue(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_block_default_option                                                       ");
            query.Append("   set max_leakage = :MAX_LEAKAGE                                                        ");
            query.Append(" where loc_code = :LOC_CODE                                                              ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("MAX_LEAKAGE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["MAX_LEAKAGE"];
            parameters[1].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //누수한계치 조회
        public object SelectLeakageMaxValue(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select max_leakage                                                                   ");
            query.AppendLine("  from wv_block_default_option                                                       ");
            query.AppendLine(" where loc_code = :LOC_CODE                                                          ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //누수량추정
        public void SelectLeakageCalculation(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                                                          ");
            query.AppendLine("(                                                                                                                                                                    ");
            query.AppendLine("select c1.loc_code                                                                                                                                                   ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                      ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                              ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                                                   ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                          ");
            query.AppendLine("      ,c1.ord                                                                                                                                                        ");
            query.AppendLine("  from                                                                                                                                                               ");
            query.AppendLine("      (                                                                                                                                                              ");
            query.AppendLine("       select sgccd                                                                                                                                                  ");
            query.AppendLine("             ,loc_code                                                                                                                                               ");
            query.AppendLine("             ,ploc_code                                                                                                                                              ");
            query.AppendLine("             ,loc_name                                                                                                                                               ");
            query.AppendLine("             ,ftr_idn                                                                                                                                                ");
            query.AppendLine("             ,ftr_code                                                                                                                                               ");
            query.AppendLine("             ,kt_gbn                                                                                                                                                 ");
            query.AppendLine("             ,rownum ord                                                                                                                                             ");
            query.AppendLine("         from cm_location                                                                                                                                            ");
            query.AppendLine("        where 1 = 1                                                                                                                                                  ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                   ");
            query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                                ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                        ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                    ");
            query.AppendLine("      ) c1                                                                                                                                                           ");
            query.AppendLine("       ,cm_location c2                                                                                                                                               ");
            query.AppendLine("       ,cm_location c3                                                                                                                                               ");
            query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                 ");
            query.AppendLine(" order by c1.ord                                                                                                                                                     ");
            query.AppendLine(")                                                                                                                                                                    ");
            query.AppendLine(",sub_loc as                                                                                                                                                         ");
            query.AppendLine("(                                                                                                                                                                   ");
            query.AppendLine("select sgccd, loc_code, ftr_idn                                                                                                                                     ");
            query.AppendLine("  from cm_location                                                                                                                                                  ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and ftr_code = 'BZ003'                                                                                                                                           ");
            query.AppendLine(" start with loc_code = :LOC_CODE                                                                                                                                    ");
            query.AppendLine("connect by prior loc_code = ploc_code                                                                                                                               ");
            query.AppendLine(")                                                                                                                                                                   ");
            query.AppendLine("select wbd.loc_code                                                                                                                                                 ");
            query.AppendLine("      ,wbd.sgccd                                                                                                                                                    ");
            query.AppendLine("      ,wbd.lblock                                                                                                                                                   ");
            query.AppendLine("      ,wbd.mblock                                                                                                                                                   ");
            query.AppendLine("      ,wbd.sblock                                                                                                                                                   ");
            query.AppendLine("      ,to_date(wbd.datee,'yyyymmdd') datee                                                                                                                          ");
            query.AppendLine("      ,wbd.jumal_used                                                                                                                                               ");
            query.AppendLine("      ,wbd.gajung_used                                                                                                                                              ");
            query.AppendLine("      ,wbd.bgajung_used                                                                                                                                             ");
            query.AppendLine("      ,wbd.special_used_percent                                                                                                                                     ");
            query.AppendLine("      ,wbd.special_used                                                                                                                                             ");
            query.AppendLine("      ,wbd.n1                                                                                                                                                       ");
            query.AppendLine("      ,wbd.lc                                                                                                                                                       ");
            query.AppendLine("      ,wbd.icf                                                                                                                                                      ");
            query.AppendLine("      ,wcd.gajung_gagusu                                                                                                                                            ");
            query.AppendLine("      ,wcd.bgajung_gagusu                                                                                                                                           ");
            query.AppendLine("      ,wcd.gupsujunsu                                                                                                                                               ");
            query.AppendLine("      ,wpl.pip_len                                                                                                                                                  ");
            query.AppendLine("      ,wlm.special_amtuse                                                                                                                                           ");
            query.AppendLine("      ,iwn.mvavg_mintime                                                                                                                                            ");
            query.AppendLine("      ,iwn.m_average                                                                                                                                                ");
            query.AppendLine("      ,iwn.filtering                                                                                                                                                ");
            query.AppendLine("      ,iay.day_supplied                                                                                                                                             ");
            query.AppendLine("      ,iip.hpres_in infpnt_hpres_davg                                                                                                                               ");
            query.AppendLine("      ,iip.hh00_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh01_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh02_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh03_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh04_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh05_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh06_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh07_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh08_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh09_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh10_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh11_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh12_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh13_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh14_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh15_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh16_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh17_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh18_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh19_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh20_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh21_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh22_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh23_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,imp.avg_hpres_pavg                                                                                                                                           ");
            query.AppendLine("      ,imp.hh00_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh01_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh02_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh03_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh04_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh05_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh06_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh07_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh08_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh09_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh10_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh11_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh12_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh13_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh14_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh15_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh16_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh17_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh18_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh19_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh20_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh21_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh22_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh23_hpres_mid                                                                                                                                           ");
            query.AppendLine("  from                                                                                                                                                              ");
            
            //옵션
            query.AppendLine("(                                                                                                                                                                   ");
            query.AppendLine(" select wbd.loc_code                                                                                                                                                ");
            query.AppendLine("       ,loc.sgccd                                                                                                                                                   ");
            query.AppendLine("       ,loc.lblock                                                                                                                                                  ");
            query.AppendLine("       ,loc.mblock                                                                                                                                                  ");
            query.AppendLine("       ,loc.sblock                                                                                                                                                  ");
            query.AppendLine("       ,wbd.datee                                                                                                                                                   ");
            query.AppendLine("       ,wbd.jumal_used                                                                                                                                              ");
            query.AppendLine("       ,wbd.gajung_used                                                                                                                                             ");
            query.AppendLine("       ,wbd.bgajung_used                                                                                                                                            ");
            query.AppendLine("       ,wbd.special_used_percent                                                                                                                                    ");
            query.AppendLine("       ,wbd.special_used                                                                                                                                            ");
            query.AppendLine("       ,wbd.n1                                                                                                                                                      ");
            query.AppendLine("       ,wbdo.lc                                                                                                                                                     ");
            query.AppendLine("       ,wbdo.icf                                                                                                                                                    ");
            query.AppendLine("   from loc                                                                                                                                                         ");
            query.AppendLine("       ,wv_block_default_option wbdo                                                                                                                                ");
            query.AppendLine("       ,wv_block_day wbd                                                                                                                                            ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and wbdo.loc_code = loc.loc_code                                                                                                                                ");
            query.AppendLine("    and wbd.loc_code = wbdo.loc_code                                                                                                                                ");
            query.AppendLine("    and wbd.datee between :STARTDATE and :ENDDATE                                                                                                                   ");
            query.AppendLine(") wbd                                                                                                                                                               ");
            
            //일별수용가정보(가정가구수, 비가정가구수, 급수전수)
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select wcd.datee                                                                                                                                                   ");
            query.AppendLine("       ,sum(wcd.gajung_gagusu) gajung_gagusu                                                                                                                        ");
            query.AppendLine("       ,sum(wcd.bgajung_gagusu) bgajung_gagusu                                                                                                                      ");
            query.AppendLine("       ,sum(wcd.gupsujunsu) gupsujunsu                                                                                                                              ");
            query.AppendLine("   from sub_loc loc                                                                                                                                                 ");
            query.AppendLine("       ,wv_consumer_day wcd                                                                                                                                         ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and wcd.loc_code = loc.loc_code		                                                                                                                          ");
            query.AppendLine("    and wcd.datee between :STARTDATE and :ENDDATE                                                                                                                   ");
            query.AppendLine("  group by wcd.datee                                                                                                                                                ");
            query.AppendLine(") wcd                                                                                                                                                               ");
            
            //일별관로정보(배수관연장)
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select wpl.datee                                                                                                                                                   ");
            query.AppendLine("       ,sum(wpl.pip_len) pip_len                                                                                                                                    ");
            query.AppendLine("   from sub_loc loc                                                                                                                                                 ");
            query.AppendLine("       ,wv_pipe_lm_day wpl                                                                                                                                          ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and wpl.loc_code = loc.loc_code                                                                                                                                 ");
            query.AppendLine("    and wpl.datee between :STARTDATE and :ENDDATE                                                                                                                   ");
            query.AppendLine("    and wpl.saa_cde = 'SAA004'                                                                                                                                      ");
            query.AppendLine("  group by wpl.datee                                                                                                                                                ");
            query.AppendLine(") wpl                                                                                                                                                               ");
            
            //월대수용가사용량 -> 일별 대수용가사용량
            //query.AppendLine(",(                                                                                                                                                                  ");
            //query.AppendLine(" select srw.stym                                                                                                                                                    ");
            //query.AppendLine("       ,round((sum(to_number(wsstvol))*1000)/to_number(to_char((last_day(to_date(srw.stym||'01','yyyymmdd'))),'dd'))/24,2) special_amtuse                           ");
            //query.AppendLine("   from sub_loc loc                                                                                                                                                 ");
            //query.AppendLine("       ,dminfo dmi                                                                                                                                                  ");
            //query.AppendLine("       ,wv_lconsumer_link wll                                                                                                                                       ");
            //query.AppendLine("       ,stwchrg srw                                                                                                                                                 ");
            //query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            //query.AppendLine("    and dmi.sgccd = loc.sgccd                                                                                                                                       ");
            //query.AppendLine("    and dmi.sftridn = loc.ftr_idn                                                                                                                                   ");
            //query.AppendLine("    and dmi.dmno = wll.dmno                                                                                                                                         ");
            //query.AppendLine("    and srw.dmno = dmi.dmno                                                                                                                                         ");
            //query.AppendLine("    and srw.stym between substr(:STARTDATE, 1, 6) and substr(:ENDDATE, 1, 6)                                                                                        ");
            //query.AppendLine("  group by srw.stym                                                                                                                                                 ");
            //query.AppendLine(") wlm                                                                                                                                                               ");

            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select b.year_mon                                                                                                                ");
            query.AppendLine("       ,b.datee																													");
            query.AppendLine("       ,round(a.special_amtuse																									");
            query.AppendLine("        /to_number((add_Months(to_date(b.year_mon||b.revenue_day,'yyyymmdd'),1)) - to_date(b.year_mon||b.revenue_day,'yyyymmdd'))	");
            query.AppendLine("        /24,2) special_amtuse																										");
            query.AppendLine("   from (																															");
            query.AppendLine("        select to_char(add_months(to_date(srw.stym,'yyyymm'),-1),'yyyymm') year_mon												");
            query.AppendLine("              ,sum(to_number(wsstvol))*1000 special_amtuse																		");
            query.AppendLine("          from sub_loc loc																										");
            query.AppendLine("              ,wi_dminfo dmi																											");
            query.AppendLine("              ,wv_lconsumer_link wll																								");
            query.AppendLine("              ,wi_stwchrg srw																										");
            query.AppendLine("         where 1 = 1																												");
            query.AppendLine("           and dmi.sgccd = loc.sgccd																								");
            query.AppendLine("           and dmi.sftridn = loc.ftr_idn																							");
            query.AppendLine("           and dmi.dmno = wll.dmno																								");
            query.AppendLine("           and srw.dmno = dmi.dmno																								");
            query.AppendLine("         group by srw.stym																										");
            query.AppendLine("       ) a																														");
            query.AppendLine("       ,(																															");
            query.AppendLine("        select to_char(add_months(b.timestamp, 																					");
            query.AppendLine("                                  decode(sign(to_char(b.timestamp,'dd')-a.revenue_day), 											");
            query.AppendLine("                       0, 1, 1, 1, -1, 0) ),																						");
            query.AppendLine("               'yyyymm') year_mon																									");
            query.AppendLine("              ,b.timestamp datee																									");
            query.AppendLine("              ,a.revenue_day																										");
            query.AppendLine("          from wv_block_default_option a																							");
            query.AppendLine("               ,(																													");
            query.AppendLine("               select to_date(:STARTDATE,'yyyymmdd') + (rownum-1) timestamp														");
            query.AppendLine("                 from dual																										");
            query.AppendLine("              connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2								");
            query.AppendLine("              ) b																													");
            query.AppendLine("         where a.loc_code = :LOC_CODE																								");
            query.AppendLine("       ) b																														");
            query.AppendLine("  where a.year_mon(+) = b.year_mon																								");
            query.AppendLine("  order by b.datee																												");
            query.AppendLine(") wlm                                                                                                                             ");


            //이동평균야간최소유량, 필터링야간최소유량
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select to_char(iwn.timestamp,'yyyymmdd') datee                                                                                                                     ");
            query.AppendLine("       ,to_char(to_date(min(iwn.mvavg_mintime), 'yyyymmddhh24miss'), 'hh24') mvavg_mintime                                                                          ");
            query.AppendLine("       ,round(to_number(sum(iwn.mvavg_min)),2) m_average                                                                                                            ");
            query.AppendLine("       ,round(to_number(sum(iwn.filtering)),2) filtering                                                                                                            ");
            query.AppendLine("   from loc                                                                                                                                                         ");
            query.AppendLine("       ,if_ihtags iih                                                                                                                                               ");
            query.AppendLine("       ,if_wv_mnf iwn                                                                                                                                               ");
            query.AppendLine("       ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                               ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and iih.loc_code = loc.tag_loc_code                                                                                                                             ");
            query.AppendLine("    and iih.tagname = itg.tagname                                                                                                                                   ");
            query.AppendLine("    and itg.tag_gbn = 'MNF'                                                                                                                                         ");
            query.AppendLine("    and iwn.tagname = itg.tagname                                                                                                                                   ");
            query.AppendLine("    and iwn.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                           ");
            query.AppendLine("  group by to_char(iwn.timestamp,'yyyymmdd')                                                                                                                        ");
            query.AppendLine(") iwn                                                                                                                                                               ");
            
            //시간대별 유입지점 수압
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine("select to_char(iwp.timestamp, 'yyyymmdd') datee                                                                                                                     ");
            query.AppendLine("      ,round(to_number(avg(iwp.tavg)),2) hpres_in                                                                                                                   ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '00', iwp.tavg, null))),2) hh00_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '01', iwp.tavg, null))),2) hh01_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '02', iwp.tavg, null))),2) hh02_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '03', iwp.tavg, null))),2) hh03_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '04', iwp.tavg, null))),2) hh04_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '05', iwp.tavg, null))),2) hh05_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '06', iwp.tavg, null))),2) hh06_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '07', iwp.tavg, null))),2) hh07_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '08', iwp.tavg, null))),2) hh08_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '09', iwp.tavg, null))),2) hh09_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '10', iwp.tavg, null))),2) hh10_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '11', iwp.tavg, null))),2) hh11_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '12', iwp.tavg, null))),2) hh12_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '13', iwp.tavg, null))),2) hh13_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '14', iwp.tavg, null))),2) hh14_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '15', iwp.tavg, null))),2) hh15_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '16', iwp.tavg, null))),2) hh16_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '17', iwp.tavg, null))),2) hh17_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '18', iwp.tavg, null))),2) hh18_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '19', iwp.tavg, null))),2) hh19_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '20', iwp.tavg, null))),2) hh20_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '21', iwp.tavg, null))),2) hh21_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '22', iwp.tavg, null))),2) hh22_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '23', iwp.tavg, null))),2) hh23_hpres_in                                                          ");
            query.AppendLine("  from sub_loc loc                                                                                                                                                  ");
            query.AppendLine("      ,if_ihtags iih                                                                                                                                                ");
            query.AppendLine("      ,if_wv_pravg iwp                                                                                                                                              ");
            query.AppendLine("      ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and iih.loc_code = loc.loc_code                                                                                                                                  ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and itg.tag_gbn = 'PRI'                                                                                                                                          ");
            query.AppendLine("   and iwp.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and iwp.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                            ");
            query.AppendLine(" group by to_char(iwp.timestamp, 'yyyymmdd')                                                                                                                        ");
            query.AppendLine(") iip                                                                                                                                                               ");
            
            //시간대별 평균지점 수압
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine("select substr(wha.dates, 1, 8) datee                                                                                                                                ");
            query.AppendLine("      ,round(to_number(avg(wha.value)),2) avg_hpres_pavg                                                                                                            ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '00', wha.value, null))),2) hh00_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '01', wha.value, null))),2) hh01_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '02', wha.value, null))),2) hh02_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '03', wha.value, null))),2) hh03_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '04', wha.value, null))),2) hh04_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '05', wha.value, null))),2) hh05_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '06', wha.value, null))),2) hh06_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '07', wha.value, null))),2) hh07_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '08', wha.value, null))),2) hh08_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '09', wha.value, null))),2) hh09_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '10', wha.value, null))),2) hh10_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '11', wha.value, null))),2) hh11_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '12', wha.value, null))),2) hh12_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '13', wha.value, null))),2) hh13_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '14', wha.value, null))),2) hh14_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '15', wha.value, null))),2) hh15_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '16', wha.value, null))),2) hh16_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '17', wha.value, null))),2) hh17_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '18', wha.value, null))),2) hh18_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '19', wha.value, null))),2) hh19_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '20', wha.value, null))),2) hh20_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '21', wha.value, null))),2) hh21_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '22', wha.value, null))),2) hh22_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '23', wha.value, null))),2) hh23_hpres_mid                                                               ");
            query.AppendLine("  from sub_loc loc                                                                                                                                                  ");
            query.AppendLine("      ,wv_hpres_avg wha                                                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and wha.loc_code = loc.loc_code                                                                                                                                  ");
            query.AppendLine("   and wha.dates between :STARTDATE||'0000' and :ENDDATE||'2359'                                                                                                    ");
            query.AppendLine(" group by substr(wha.dates, 1, 8)                                                                                                                                   ");
            query.AppendLine(") imp                                                                                                                                                               ");
            
            //공급량
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine("select to_char(iay.timestamp,'yyyymmdd') datee                                                                                                                      ");
            query.AppendLine("      ,sum(value) day_supplied                                                                                                                                      ");
            query.AppendLine("  from loc                                                                                                                                                          ");
            query.AppendLine("      ,if_ihtags iih                                                                                                                                                ");
            query.AppendLine("      ,if_accumulation_yesterday iay                                                                                                                                ");
            query.AppendLine("      ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and iih.loc_code = loc.tag_loc_code                                                                                                                              ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and itg.tag_gbn = 'YD'                                                                                                                                           ");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                                                                   ");
            query.AppendLine("   and iay.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                            ");
            query.AppendLine(" group by to_char(iay.timestamp,'yyyymmdd')                                                                                                                         ");
            query.AppendLine(") iay                                                                                                                                                               ");            
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and wcd.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and wpl.datee(+) = wbd.datee                                                                                                                                     ");
            
            //query.AppendLine("   and wlm.stym(+) = substr(wbd.datee,1,6)                                                                                                                          ");
            
            query.AppendLine("   and wlm.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and iwn.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and iip.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and imp.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and iay.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("order by datee                                                                                                                                                      ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //        ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //};

            //parameters[0].Value = parameter["LOC_CODE"];
            //parameters[1].Value = parameter["LOC_CODE"];
            //parameters[2].Value = parameter["STARTDATE"];
            //parameters[3].Value = parameter["ENDDATE"];
            //parameters[4].Value = parameter["STARTDATE"];
            //parameters[5].Value = parameter["ENDDATE"];
            //parameters[6].Value = parameter["STARTDATE"];
            //parameters[7].Value = parameter["ENDDATE"];
            //parameters[8].Value = parameter["STARTDATE"];
            //parameters[9].Value = parameter["ENDDATE"];
            //parameters[10].Value = parameter["STARTDATE"];
            //parameters[11].Value = parameter["ENDDATE"];
            //parameters[12].Value = parameter["STARTDATE"];
            //parameters[13].Value = parameter["ENDDATE"];
            //parameters[14].Value = parameter["STARTDATE"];
            //parameters[15].Value = parameter["ENDDATE"];
            //parameters[16].Value = parameter["STARTDATE"];
            //parameters[17].Value = parameter["ENDDATE"];

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];
            parameters[5].Value = parameter["ENDDATE"];
            parameters[6].Value = parameter["STARTDATE"];
            parameters[7].Value = parameter["ENDDATE"];
            
            parameters[8].Value = parameter["STARTDATE"];
            parameters[9].Value = parameter["ENDDATE"];
            parameters[10].Value = parameter["STARTDATE"];
            parameters[11].Value = parameter["LOC_CODE"];

            parameters[12].Value = parameter["STARTDATE"];
            parameters[13].Value = parameter["ENDDATE"];
            parameters[14].Value = parameter["STARTDATE"];
            parameters[15].Value = parameter["ENDDATE"];
            parameters[16].Value = parameter["STARTDATE"];
            parameters[17].Value = parameter["ENDDATE"];
            parameters[18].Value = parameter["STARTDATE"];
            parameters[19].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록기본옵션조회
        public void SelectBlockDefaultOption(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select jumal_used													 ");
            query.AppendLine("      ,gajung_used												 ");
            query.AppendLine("      ,bgajung_used												 ");
            query.AppendLine("      ,special_used												 ");
            query.AppendLine("      ,n1															 ");
            query.AppendLine("      ,lc															 ");
            query.AppendLine("      ,icf														 ");
            query.AppendLine("      ,special_used_percent										 ");
            query.AppendLine("  from wv_block_default_option									 ");
            query.AppendLine(" where 1 = 1														 ");
            query.AppendLine("   and loc_code = :LOC_CODE                                        ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void SelectLeakageResult(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                      ");
            //query.AppendLine("(                                                                                                                              ");
            //query.AppendLine("select loc.loc_code                                                                                                            ");
            //query.AppendLine("      ,loc.sgccd                                                                                                               ");
            //query.AppendLine("      ,loc.lblock                                                                                                              ");
            //query.AppendLine("      ,loc.mblock                                                                                                              ");
            //query.AppendLine("      ,loc.sblock                                                                                                              ");
            //query.AppendLine("      ,loc.loc_name                                                                                                            ");
            //query.AppendLine("      ,loc.ftr_idn                                                                                                             ");
            //query.AppendLine("      ,loc.ftr_code                                                                                                            ");
            //query.AppendLine("      ,loc.ord                                                                                                                 ");
            //query.AppendLine("  from                                                                                                                         ");
            //query.AppendLine("(                                                                                                                              ");
            //query.AppendLine("select c1.loc_code                                                                                                             ");
            //query.AppendLine("      ,c1.sgccd                                                                                                                ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                        ");
            //query.AppendLine("      ,c1.loc_name                                                                                                             ");
            //query.AppendLine("      ,c1.ftr_idn                                                                                                              ");
            //query.AppendLine("      ,c1.ftr_code                                                                                                             ");
            //query.AppendLine("      ,c1.ord                                                                                                                  ");
            //query.AppendLine("  from                                                                                                                         ");
            //query.AppendLine("      (                                                                                                                        ");
            //query.AppendLine("       select sgccd                                                                                                            ");
            //query.AppendLine("             ,loc_code                                                                                                         ");
            //query.AppendLine("             ,ploc_code                                                                                                        ");
            //query.AppendLine("             ,loc_name                                                                                                         ");
            //query.AppendLine("             ,ftr_idn                                                                                                          ");
            //query.AppendLine("             ,ftr_code                                                                                                         ");
            //query.AppendLine("             ,res_code                                                                                                         ");
            //query.AppendLine("             ,rownum ord                                                                                                       ");
            //query.AppendLine("         from cm_location                                                                                                      ");
            //query.AppendLine("        where 1 = 1                                                                                                            ");
            //query.AppendLine("          and ftr_code = 'BZ003'                                                                                               ");
            //query.AppendLine("        start with ftr_code = 'BZ001'                                                                                          ");
            //query.AppendLine("        connect by prior loc_code = ploc_code                                                                                  ");
            //query.AppendLine("        order SIBLINGS by ftr_idn                                                                                              ");
            //query.AppendLine("      ) c1                                                                                                                     ");
            //query.AppendLine("       ,cm_location c2                                                                                                         ");
            //query.AppendLine("       ,cm_location c3                                                                                                         ");
            //query.AppendLine(" where 1 = 1                                                                                                                   ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                           ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                           ");
            //query.AppendLine(" order by c1.ord                                                                                                               ");
            //query.AppendLine(") loc                                                                                                                          ");
            //query.AppendLine("      ,if_ihtags iih                                                                                                           ");
            //query.AppendLine("      ,if_tag_gbn itg                                                                                                          ");
            //query.AppendLine(" where iih.loc_code = loc.loc_code                                                                                             ");
            //query.AppendLine("   and itg.tagname = iih.tagname                                                                                               ");
            //query.AppendLine(" group by                                                                                                                      ");
            //query.AppendLine("       loc.loc_code                                                                                                            ");
            //query.AppendLine("      ,loc.sgccd                                                                                                               ");
            //query.AppendLine("      ,loc.lblock                                                                                                              ");
            //query.AppendLine("      ,loc.mblock                                                                                                              ");
            //query.AppendLine("      ,loc.sblock                                                                                                              ");
            //query.AppendLine("      ,loc.loc_name                                                                                                            ");
            //query.AppendLine("      ,loc.ftr_idn                                                                                                             ");
            //query.AppendLine("      ,loc.ftr_code                                                                                                            ");
            //query.AppendLine("      ,loc.ord                                                                                                                 ");
            //query.AppendLine(" order by                                                                                                                      ");
            //query.AppendLine("       loc.ord                                                                                                                 ");
            //query.AppendLine(")                                                                                                                              ");
            //query.AppendLine("select loc.sgccd                                                                                                                 ");
            //query.AppendLine("      ,loc.loc_code                                                                                                              ");
            //query.AppendLine("      ,loc.lblock                                                                                                                ");
            //query.AppendLine("      ,loc.mblock                                                                                                                ");
            //query.AppendLine("      ,loc.sblock                                                                                                                ");
            //query.AppendLine("      ,loc.loc_name                                                                                                              ");
            //query.AppendLine("      ,loc.ftr_code                                                                                                              ");
            //query.AppendLine("      ,loc.ftr_idn                                                                                                               ");
            //query.AppendLine("      ,to_date(:STARTDATE,'yyyymmdd') datee                                                                                      ");
            //query.AppendLine("      ,decode(data.monitor_result, 'T', 'N', nvl(monitor.monitor_result, 'N')) monitor_result                                    ");
            //query.AppendLine("  from loc                                                                                                                       ");
            //query.AppendLine("      ,(                                                                                                                         ");
            //query.AppendLine("      select wm.loc_code                                                                                                         ");
            //query.AppendLine("            ,wm.datee                                                                                                            ");
            //query.AppendLine("            ,nvl(max(decode(wm.monitor_result, 'T', 'T', null)),'F') monitor_result                                              ");
            //query.AppendLine("        from wv_monitering wm                                                                                                    ");
            //query.AppendLine("       where 1 = 1                                                                                                               ");
            //query.AppendLine("         and wm.monitor_type in ('1090', '1180')                                                                                 ");
            //query.AppendLine("         and wm.datee = to_date(:STARTDATE,'yyyymmdd')                                                                           ");
            //query.AppendLine("       group by wm.loc_code, wm.datee                                                                                            ");
            //query.AppendLine("      ) data                                                                                                                     ");
            //query.AppendLine("      ,(                                                                                                                         ");
            //query.AppendLine("      select wm.loc_code                                                                                                         ");
            //query.AppendLine("            ,wm.datee                                                                                                            ");
            //query.AppendLine("            ,nvl(max(decode(wm.monitor_result, 'T', 'T', null)),'F') monitor_result                                              ");
            //query.AppendLine("        from wv_monitering wm                                                                                                    ");
            //query.AppendLine("            ,cm_code cc                                                                                                          ");
            //query.AppendLine("       where 1 = 1                                                                                                               ");
            //query.AppendLine("         and cc.pcode = '2013'                                                                                                   ");
            //query.AppendLine("         and cc.use_yn = 'Y'                                                                                                     ");
            //query.AppendLine("         and wm.monitor_type = cc.code                                                                                           ");
            //query.AppendLine("         and wm.monitor_type != '1090'                                                                                           ");
            //query.AppendLine("         and wm.monitor_type != '1180'                                                                                           ");
            //query.AppendLine("         and wm.datee = to_date(:STARTDATE,'yyyymmdd')                                                                           ");
            //query.AppendLine("       group by wm.loc_code, wm.datee                                                                                            ");
            //query.AppendLine("      ) monitor                                                                                                                  ");
            //query.AppendLine("where 1 = 1                                                                                                                      ");
            //query.AppendLine("  and data.loc_code(+) = loc.loc_code                                                                                            ");
            //query.AppendLine("  and monitor.loc_code(+) = loc.loc_code                                                                                         ");
            //query.AppendLine("order by loc.ord                                                                                                                 ");

            //query.AppendLine("with loc as                                                                                                                         ");
            //query.AppendLine("(                                                                                                                              	  ");
            //query.AppendLine("select loc.loc_code                                                                                                            	  ");
            //query.AppendLine("	  ,loc.sgccd                                                                                                               		  ");
            //query.AppendLine("	  ,loc.lblock                                                                                                              		  ");
            //query.AppendLine("	  ,loc.mblock                                                                                                              		  ");
            //query.AppendLine("	  ,loc.sblock                                                                                                              		  ");
            //query.AppendLine("	  ,loc.loc_name                                                                                                            		  ");
            //query.AppendLine("	  ,loc.ftr_idn                                                                                                             		  ");
            //query.AppendLine("	  ,loc.ftr_code                                                                                                            		  ");
            //query.AppendLine("	  ,loc.ord                                                                                                                 		  ");
            //query.AppendLine("  from                                                                                                                         	  ");
            //query.AppendLine("	  (                                                                                                                               ");
            //query.AppendLine("	  select c1.loc_code                                                                                                              ");
            //query.AppendLine("  		    ,c1.sgccd                                                                                                             ");
            //query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code))) ");
            //query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                  ");
            //query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            //query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                  ");
            //query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            //query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                         ");
            //query.AppendLine("		    ,c1.loc_name                                                                                                              ");
            //query.AppendLine("		    ,c1.ftr_idn                                                                                                               ");
            //query.AppendLine("		    ,c1.ftr_code                                                                                                              ");
            //query.AppendLine("		    ,c1.ord                                                                                                                   ");
            //query.AppendLine("	    from                                                                                                                          ");
            //query.AppendLine("		    (                                                                                                                         ");
            //query.AppendLine("		     select sgccd                                                                                                             ");
            //query.AppendLine("			  	   ,loc_code                                                                                                          ");
            //query.AppendLine("				   ,ploc_code                                                                                                         ");
            //query.AppendLine("				   ,loc_name                                                                                                          ");
            //query.AppendLine("				   ,ftr_idn                                                                                                           ");
            //query.AppendLine("				   ,ftr_code                                                                                                          ");
            //query.AppendLine("				   ,res_code                                                                                                          ");
            //query.AppendLine("				   ,orderby ord                                                                                                        ");
            //query.AppendLine("			   from cm_location                                                                                                       ");
            //query.AppendLine("			  where 1 = 1                                                                                                             ");
            //query.AppendLine("			    and ftr_code = 'BZ003'                                                                                                ");
            //query.AppendLine("			  start with ftr_code = 'BZ001'                                                                                           ");
            //query.AppendLine("			  connect by prior loc_code = ploc_code                                                                                   ");
            //query.AppendLine("			  order SIBLINGS by ftr_idn                                                                                               ");
            //query.AppendLine("		    ) c1                                                                                                                      ");
            //query.AppendLine("		     ,cm_location c2                                                                                                          ");
            //query.AppendLine("		     ,cm_location c3                                                                                                          ");
            //query.AppendLine("	   where 1 = 1                                                                                                                    ");
            //query.AppendLine("	     and c1.ploc_code = c2.loc_code(+)                                                                                            ");
            //query.AppendLine("	     and c2.ploc_code = c3.loc_code(+)                                                                                            ");
            //query.AppendLine("	   order by c1.ord                                                                                                                ");
            //query.AppendLine("	  ) loc                                                                                                                           ");
            //query.AppendLine("	  ,if_ihtags iih                                                                                                           		  ");
            //query.AppendLine("	  ,if_tag_gbn itg                                                                                                          		  ");
            //query.AppendLine(" where iih.loc_code = loc.loc_code                                                                                             	  ");
            //query.AppendLine("   and itg.tagname = iih.tagname                                                                                               	  ");
            //query.AppendLine(" group by                                                                                                                      	  ");
            //query.AppendLine("	   loc.loc_code                                                                                                            		  ");
            //query.AppendLine("	  ,loc.sgccd                                                                                                               		  ");
            //query.AppendLine("	  ,loc.lblock                                                                                                              		  ");
            //query.AppendLine("	  ,loc.mblock                                                                                                              		  ");
            //query.AppendLine("	  ,loc.sblock                                                                                                              		  ");
            //query.AppendLine("	  ,loc.loc_name                                                                                                            		  ");
            //query.AppendLine("	  ,loc.ftr_idn                                                                                                             		  ");
            //query.AppendLine("	  ,loc.ftr_code                                                                                                            		  ");
            //query.AppendLine("	  ,loc.ord                                                                                                                 		  ");
            //query.AppendLine(" order by                                                                                                                      	  ");
            //query.AppendLine("	   loc.ord                                                                                                                 		  ");
            //query.AppendLine(")                                                                                                                              	  ");
            //query.AppendLine("select loc.sgccd                                                                                                                 	  ");
            //query.AppendLine("	  ,loc.loc_code                                                                                                              	  ");
            //query.AppendLine("	  ,loc.lblock                                                                                                                	  ");
            //query.AppendLine("	  ,loc.mblock                                                                                                                	  ");
            //query.AppendLine("	  ,loc.sblock                                                                                                                	  ");
            //query.AppendLine("	  ,loc.loc_name                                                                                                              	  ");
            //query.AppendLine("	  ,loc.ftr_code                                                                                                              	  ");
            //query.AppendLine("	  ,loc.ftr_idn                                                                                                               	  ");
            //query.AppendLine("	  ,to_date(:STARTDATE,'yyyymmdd') datee                                                                                      	  ");
            //query.AppendLine("	  ,nvl(monitor.monitor_result, 'N') monitor_result																				  ");
            //query.AppendLine("    ,decode(monitor.monitor_alarm, 'T', 'True', 'False') monitor_alarm														      ");
            //query.AppendLine("  from loc                                                                                                                          ");
            //query.AppendLine("	  ,(                                                                                                                         	  ");
            //query.AppendLine("	  select wm.loc_code                                                                                                         	  ");
            //query.AppendLine("			,wm.datee                                                                                                            	  ");
            //query.AppendLine("			,nvl(max(decode(wm.monitor_result, 'T', 'T', null)),'F') monitor_result													  ");
            //query.AppendLine("            ,nvl(max(decode(wm.monitor_alarm, 'T', 'T', null)),'F') monitor_alarm													  ");
            //query.AppendLine("		from wv_monitering wm                                                                                                    	  ");
            //query.AppendLine("			,cm_code cc                                                                                                          	  ");
            //query.AppendLine("	   where 1 = 1                                                                                                               	  ");
            //query.AppendLine("		 and cc.pcode = '2013'                                                                                                   	  ");
            //query.AppendLine("		 and cc.use_yn = 'Y'                                                                                                     	  ");
            //query.AppendLine("		 and wm.monitor_type = cc.code                                                                                                ");
            //query.AppendLine("		 and wm.datee = to_date(:STARTDATE,'yyyymmdd')                                                                           	  ");
            //query.AppendLine("	   group by wm.loc_code, wm.datee                                                                                            	  ");
            //query.AppendLine("	  ) monitor                                                                                                                  	  ");
            //query.AppendLine("where 1 = 1                                                                                                                      	  ");
            //query.AppendLine("  and monitor.loc_code(+) = loc.loc_code                                                                                         	  ");
            //query.AppendLine("order by loc.ord        																											  ");

            query.AppendLine("with loc as                                                                                                                         ");
            query.AppendLine("(                                                                                                                              	  ");
            query.AppendLine("select loc.loc_code                                                                                                            	  ");
            query.AppendLine("	  ,loc.sgccd                                                                                                               		  ");
            query.AppendLine("	  ,loc.lblock                                                                                                              		  ");
            query.AppendLine("	  ,loc.mblock                                                                                                              		  ");
            query.AppendLine("	  ,loc.sblock                                                                                                              		  ");
            query.AppendLine("	  ,loc.loc_name                                                                                                            		  ");
            query.AppendLine("	  ,loc.ftr_idn                                                                                                             		  ");
            query.AppendLine("	  ,loc.ftr_code                                                                                                            		  ");
            query.AppendLine("	  ,loc.ord                                                                                                                 		  ");
            query.AppendLine("  from                                                                                                                         	  ");
            query.AppendLine("	  (                                                                                                                               ");
            query.AppendLine("	  select c1.loc_code                                                                                                              ");
            query.AppendLine("  		    ,c1.sgccd                                                                                                             ");
            query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code))) ");
            query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                  ");
            query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                  ");
            query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                         ");
            query.AppendLine("		    ,c1.loc_name                                                                                                              ");
            query.AppendLine("		    ,c1.ftr_idn                                                                                                               ");
            query.AppendLine("		    ,c1.ftr_code                                                                                                              ");
            query.AppendLine("		    ,c1.ord                                                                                                                   ");
            query.AppendLine("	    from                                                                                                                          ");
            query.AppendLine("		    (                                                                                                                         ");
            query.AppendLine("		     select sgccd                                                                                                             ");
            query.AppendLine("			  	   ,loc_code                                                                                                          ");
            query.AppendLine("				   ,ploc_code                                                                                                         ");
            query.AppendLine("				   ,loc_name                                                                                                          ");
            query.AppendLine("				   ,ftr_idn                                                                                                           ");
            query.AppendLine("				   ,ftr_code                                                                                                          ");
            query.AppendLine("				   ,res_code                                                                                                          ");
            query.AppendLine("				   ,orderby ord                                                                                                        ");
            query.AppendLine("			   from cm_location                                                                                                       ");
            query.AppendLine("			  where 1 = 1                                                                                                             ");
            query.AppendLine("			    and ftr_code = 'BZ003'                                                                                                ");
            query.AppendLine("			  start with ftr_code = 'BZ001'                                                                                           ");
            query.AppendLine("			  connect by prior loc_code = ploc_code                                                                                   ");
            query.AppendLine("			  order SIBLINGS by ftr_idn                                                                                               ");
            query.AppendLine("		    ) c1                                                                                                                      ");
            query.AppendLine("		     ,cm_location c2                                                                                                          ");
            query.AppendLine("		     ,cm_location c3                                                                                                          ");
            query.AppendLine("	   where 1 = 1                                                                                                                    ");
            query.AppendLine("	     and c1.ploc_code = c2.loc_code(+)                                                                                            ");
            query.AppendLine("	     and c2.ploc_code = c3.loc_code(+)                                                                                            ");
            query.AppendLine("	   order by c1.ord                                                                                                                ");
            query.AppendLine("	  ) loc                                                                                                                           ");
            query.AppendLine("	  ,if_ihtags iih                                                                                                           		  ");
            query.AppendLine("	  ,if_tag_gbn itg                                                                                                          		  ");
            query.AppendLine(" where iih.loc_code = loc.loc_code                                                                                             	  ");
            query.AppendLine("   and itg.tagname = iih.tagname                                                                                               	  ");
            query.AppendLine(" group by                                                                                                                      	  ");
            query.AppendLine("	   loc.loc_code                                                                                                            		  ");
            query.AppendLine("	  ,loc.sgccd                                                                                                               		  ");
            query.AppendLine("	  ,loc.lblock                                                                                                              		  ");
            query.AppendLine("	  ,loc.mblock                                                                                                              		  ");
            query.AppendLine("	  ,loc.sblock                                                                                                              		  ");
            query.AppendLine("	  ,loc.loc_name                                                                                                            		  ");
            query.AppendLine("	  ,loc.ftr_idn                                                                                                             		  ");
            query.AppendLine("	  ,loc.ftr_code                                                                                                            		  ");
            query.AppendLine("	  ,loc.ord                                                                                                                 		  ");
            query.AppendLine(" order by                                                                                                                      	  ");
            query.AppendLine("	   loc.ord                                                                                                                 		  ");
            query.AppendLine(")                                                                                                                              	  ");
            query.AppendLine("select loc.sgccd                                                                                                                 	  ");
            query.AppendLine("	  ,loc.loc_code                                                                                                              	  ");
            query.AppendLine("	  ,loc.lblock                                                                                                                	  ");
            query.AppendLine("	  ,loc.mblock                                                                                                                	  ");
            query.AppendLine("	  ,loc.sblock                                                                                                                	  ");
            query.AppendLine("	  ,loc.loc_name                                                                                                              	  ");
            query.AppendLine("	  ,loc.ftr_code                                                                                                              	  ");
            query.AppendLine("	  ,loc.ftr_idn                                                                                                               	  ");
            query.AppendLine("	  ,to_date(:STARTDATE,'yyyymmdd') datee                                                                                      	  ");
            query.AppendLine("	  ,case when mon.mvg = 'N' or mon.fvg = 'N' or mon.mvt = 'N' or mon.fvt = 'N' then 'N'                                        	  ");
            query.AppendLine("	        when mon.mvg = 'T' or mon.fvg = 'T' or mon.mvt = 'T' or mon.fvt = 'T' then 'T'                                        	  ");
            query.AppendLine("	   else                                                                                                                     	  ");
            query.AppendLine("	  nvl(decode(mon.mvg,'T', decode(mon.fvg,'T','T', decode(mon.mmc,'T','A','F')                                                     ");
            query.AppendLine("	  , decode(mon.mvt,'T', decode(mon.fvt,'T','T', decode(mon.mmc,'T','A','F')),'F')), decode(mon.mmc,'T','A','F')),'F')             ");
            query.AppendLine("     end monitor_result                                               														      ");
            query.AppendLine("    ,decode(monitor.monitor_alarm, 'T', 'True', 'False') monitor_alarm														      ");
            query.AppendLine("  from loc                                                                                                                          ");
            query.AppendLine("	  ,(                                                                                                                         	  ");
            query.AppendLine("	  select wm.loc_code                                                                                                         	  ");
            query.AppendLine("			,wm.datee                                                                                                            	  ");            
            query.AppendLine("          ,nvl(max(decode(wm.monitor_alarm, 'T', 'T', null)),'F') monitor_alarm													  ");
            query.AppendLine("		from wv_monitering wm                                                                                                    	  ");
            query.AppendLine("			,cm_code cc                                                                                                          	  ");
            query.AppendLine("	   where 1 = 1                                                                                                               	  ");
            query.AppendLine("		 and cc.pcode = '2013'                                                                                                   	  ");
            query.AppendLine("		 and cc.use_yn = 'Y'                                                                                                     	  ");
            query.AppendLine("		 and wm.monitor_type = cc.code                                                                                                ");
            query.AppendLine("		 and wm.datee = to_date(:STARTDATE,'yyyymmdd')                                                                           	  ");
            query.AppendLine("	   group by wm.loc_code, wm.datee                                                                                            	  ");
            query.AppendLine("	   ) monitor,                                                                                                                   	  ");
            query.AppendLine("	    (  select loc_code,                                  	                                                                  ");
            query.AppendLine("	       max(mon_rst.mvg)mvg                                 	                                                                      ");
            query.AppendLine("	       , max(mon_rst.mvt)mvt                                 	                                                                  ");
            query.AppendLine("	       , max(mon_rst.fvg)fvg                                 	                                                                  ");
            query.AppendLine("	       , max(mon_rst.fvt)fvt                                 	                                                                  ");
            query.AppendLine("	       , max(mon_rst.mmc)mmc from                                  	                                                              ");
            query.AppendLine("	       (select loc_code,                                  	                                                                      ");
            query.AppendLine("	             decode(monitor_type, '1200', decode(monitor_result, 'T','T',decode(monitor_result, 'N','N','F'))) mvg                                 	  ");
            query.AppendLine("	             , decode(monitor_type, '1210', decode(monitor_result, 'T', 'T',decode(monitor_result, 'N','N','F'))) mvt                                 	  ");
            query.AppendLine("	             , decode(monitor_type, '1220', decode(monitor_result, 'T', 'T',decode(monitor_result, 'N','N','F'))) fvg                                 	  ");
            query.AppendLine("	             , decode(monitor_type, '1230', decode(monitor_result, 'T', 'T',decode(monitor_result, 'N','N','F'))) fvt                                 	  ");
            query.AppendLine("	             , decode(monitor_type, '1240', decode(monitor_result, 'T', 'T',decode(monitor_result, 'N','N','F'))) mmc                                 	  ");
            query.AppendLine("	       from wv_monitering                                 	                                                                ");
            query.AppendLine("	       where  datee = to_date(:STARTDATE,'yyyymmdd')) mon_rst                                  	  ");
            query.AppendLine("	 group by loc_code                                 	                                                                              ");
            query.AppendLine("	 )mon                                 	                                                                                          ");
            query.AppendLine("where 1 = 1                                                                                                                      	  ");
            query.AppendLine("  and monitor.loc_code(+) = loc.loc_code                                                                                         	  ");
            query.AppendLine("  and mon.loc_code = monitor.loc_code                                                                                         	  ");
            query.AppendLine("order by loc.ord        																											  ");

            IDataParameter[] parameters =  {
                    new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void SelectLeakageResultDetail(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select :LOC_CODE loc_code                                                                 ");
            query.AppendLine("      ,to_date(:STARTDATE,'yyyymmdd') datee                                               ");
            query.AppendLine("      ,cc.code                                                                            ");
            //query.AppendLine("      ,decode(wmo.monitor_alarm, 'T', 'True', decode(wmo.monitor_alarm, 'F', 'False')) monitor_alarm          ");
            query.AppendLine("      ,cc.code_name                                                                       ");
            query.AppendLine("      ,wmo.monitor_count_max                                                              ");
            query.AppendLine("      ,wmo.monitor_count                                                                  ");
            query.AppendLine("      ,decode(cc.code, '1090', decode(wmo.monitor_result, 'T', 'N', 'F'),                 ");
            query.AppendLine("       decode(cc.code, '1180', decode(wmo.monitor_result, 'T', 'N', 'F'),                 ");
            query.AppendLine("       wmo.monitor_result)) monitor_result                                                ");
            query.AppendLine("  from wv_monitering wmo                                                                  ");
            query.AppendLine("      ,cm_code cc                                                                         ");
            query.AppendLine(" where 1 = 1                                                                              ");
            query.AppendLine("   and wmo.loc_code = :LOC_CODE                                                           ");
            query.AppendLine("   and cc.pcode = '2013'                                                                  ");
            query.AppendLine("   and cc.use_yn = 'Y'                                                                    ");
            query.AppendLine("   and wmo.monitor_type = cc.code                                                         ");
            query.AppendLine("   and datee = to_date(:STARTDATE,'yyyymmdd')                                             ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //누수알람여부 수정
        public object UpdateLeakageResultAlarm(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_monitering                                                                                            ");
            query.AppendLine("   set monitor_alarm = decode(:MONITOR_ALARM, 'True', 'T', decode(:MONITOR_ALARM, 'False', 'F', :MONITOR_ALARM))");
            query.AppendLine(" where loc_code = :LOC_CODE                                                                                     ");
            query.AppendLine(" and datee = to_date(:DATEE,'yyyymmdd')                                                                         ");
            query.AppendLine(" and monitor_count_max <= monitor_count                                                                         ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("MONITOR_ALARM", OracleDbType.Varchar2)
                     ,new OracleParameter("MONITOR_ALARM", OracleDbType.Varchar2)
                     ,new OracleParameter("MONITOR_ALARM", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["MONITOR_ALARM"];
            parameters[1].Value = parameter["MONITOR_ALARM"];
            parameters[2].Value = parameter["MONITOR_ALARM"];
            parameters[3].Value = parameter["LOC_CODE"];
            parameters[4].Value = parameter["DATEE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //한달간 야간최소유량 공급량검색
        public void SelectLeakageResultData(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                            ");
            query.AppendLine("(                                                                                                                                 	 ");
            query.AppendLine("select c1.loc_code                                                                                                                	 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                   	 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))   	 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                    	 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          	 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                    	 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          	 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                           	 ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                	 ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code            ");
            query.AppendLine("      ,c1.ord                                                                                                                     	 ");
            query.AppendLine("  from                                                                                                                            	 ");
            query.AppendLine("      (                                                                                                                           	 ");
            query.AppendLine("       select sgccd                                                                                                               	 ");
            query.AppendLine("             ,loc_code                                                                                                            	 ");
            query.AppendLine("             ,ploc_code                                                                                                           	 ");
            query.AppendLine("             ,loc_name                                                                                                            	 ");
            query.AppendLine("             ,ftr_idn                                                                                                             	 ");
            query.AppendLine("             ,ftr_code                                                                                                            	 ");
            query.AppendLine("             ,kt_gbn                                                                                                            	     ");
            query.AppendLine("             ,rownum ord                                                                                                          	 ");
            query.AppendLine("         from cm_location                                                                                                         	 ");
            query.AppendLine("        where 1 = 1                                                                                                               	 ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                     			 ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                     	 ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                 	 ");
            query.AppendLine("      ) c1                                                                                                                        	 ");
            query.AppendLine("       ,cm_location c2                                                                                                            	 ");
            query.AppendLine("       ,cm_location c3                                                                                                            	 ");
            query.AppendLine(" where 1 = 1                                                                                                                      	 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                              	 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                              	 ");
            query.AppendLine(" order by c1.ord                                                                                                                  	 ");
            query.AppendLine(")                                                                                                                                 	 ");
            query.AppendLine("select to_date(iwm.datee,'yyyymmdd') datee                                                                                        	 ");
            query.AppendLine("      ,to_char(sum(iay.flow),'999,999,999,990.99') flow                                                                                                         	 ");
            query.AppendLine("      ,to_char(decode(round(sum(iwm.mnf),2), 0, null, round(sum(iwm.mnf),2)),'999,999,999,990.99') mnf                                                               ");
            query.AppendLine("  from                                                                                                                            	 ");
            query.AppendLine("      (                                                                                                                                    ");
            query.AppendLine("      select datee, sum(mnf) mnf                                                                                                           ");
            query.AppendLine("        from                                                                                                                               ");
            query.AppendLine("            (                                                                                                                           	 ");
            query.AppendLine("            select to_char(iwm.timestamp,'yyyymmdd') datee                                                                              	 ");
            query.AppendLine("                  ,to_number(iwm.filtering) mnf                                                                                       	 ");
            query.AppendLine("              from loc                                                                                                                  	 ");
            query.AppendLine("                  ,if_ihtags iih                                                                                                        	 ");
            query.AppendLine("                  ,if_tag_gbn itg                                                                                                       	 ");
            query.AppendLine("                  ,if_wv_mnf iwm                                                                                                        	 ");
            query.AppendLine("             where 1 = 1                                                                                                                	 ");
            query.AppendLine("               and iih.loc_code = loc.tag_loc_code                                                                                      	 ");
            query.AppendLine("               and itg.tagname = iih.tagname                                                                                            	 ");
            query.AppendLine("               and itg.tag_gbn = 'MNF'                                                                                                  	 ");
            query.AppendLine("               and iwm.tagname = itg.tagname                                                                                            	 ");
            query.AppendLine("               and iwm.timestamp between add_Months(to_date(:DATEE||'0000','yyyymmddhh24mi'),-1) and to_date(:DATEE||'2359','yyyymmddhh24mi')+1");
            query.AppendLine("             union all                                                                                                                  	 ");
            query.AppendLine("            select to_char(add_Months(to_date(:DATEE, 'yyyymmdd'),-1) + rownum -1,'yyyymmdd') datee , 0 from dual                       	 ");
            query.AppendLine("            connect by rownum < to_date(:DATEE,'yyyymmdd') - add_Months(to_date(:DATEE,'yyyymmdd'),-1) + 3                              	 ");
            query.AppendLine("            )                                                                                                                        	 ");
            query.AppendLine("   group by datee                                                                                                                     ");
            query.AppendLine("      ) iwm                                                                                                                            ");
            query.AppendLine("      ,(                                                                                                                          	 ");
            query.AppendLine("      select to_char(iay.timestamp,'yyyymmdd') datee                                                                              	 ");
            query.AppendLine("            ,iay.value flow                                                                                                       	 ");
            query.AppendLine("        from loc                                                                                                                  	 ");
            query.AppendLine("            ,if_ihtags iih                                                                                                        	 ");
            query.AppendLine("            ,if_tag_gbn itg                                                                                                       	 ");
            query.AppendLine("            ,if_accumulation_yesterday iay                                                                                        	 ");
            query.AppendLine("       where 1 = 1                                                                                                                	 ");
            query.AppendLine("         and iih.loc_code = loc.tag_loc_code                                                                                      	 ");
            query.AppendLine("         and itg.tagname = iih.tagname                                                                                            	 ");
            query.AppendLine("         and itg.tag_gbn = 'YD'                                                                                                   	 ");
            query.AppendLine("         and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                                ");
            query.AppendLine("         and iay.tagname = itg.tagname                                                                                            	 ");
            query.AppendLine("         and iay.timestamp between add_Months(to_date(:DATEE||'0000','yyyymmddhh24mi'),-1) and to_date(:DATEE||'2359','yyyymmddhh24mi')+1");
            query.AppendLine("      ) iay                                                                                                                       	 ");
            query.AppendLine(" where 1 = 1                                                                                                                      	 ");
            query.AppendLine("   and iay.datee(+) = iwm.datee                                                                                                        ");
            query.AppendLine(" group by to_date(iwm.datee,'yyyymmdd')                                                                                           	 ");
            query.AppendLine(" order by datee desc                                                                                                                   ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["DATEE"];
            parameters[3].Value = parameter["DATEE"];
            parameters[4].Value = parameter["DATEE"];
            parameters[5].Value = parameter["DATEE"];
            parameters[6].Value = parameter["DATEE"];
            parameters[7].Value = parameter["DATEE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }
    }
}
