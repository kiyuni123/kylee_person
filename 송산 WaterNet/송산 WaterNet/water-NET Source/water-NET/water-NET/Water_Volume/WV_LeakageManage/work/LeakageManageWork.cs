﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_LeakageManage.dao;
using WaterNet.WV_Common.work;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_LeakageManage.vo;
using WaterNet.WV_Common.util;
using WaterNet.WaterNetCore;

namespace WaterNet.WV_LeakageManage.work
{
    public class LeakageManageWork: BaseWork
    {
        private static LeakageManageWork work = null;
        private LeakageManageDao dao = null;

        public static LeakageManageWork GetInstance()
        {
            if (work == null)
            {
                work = new LeakageManageWork();
            }
            return work;
        }

        private LeakageManageWork()
        {
            this.dao = LeakageManageDao.GetInstance();
        }

        public void UpdateLeakageManageOption(object leakageList)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                IList<LeakageCalculationVO> leakage = (IList<LeakageCalculationVO>)leakageList;
                foreach (LeakageCalculationVO leakageVO in leakage)
                {
                    //Hashtable parameter = new Hashtable();
                    //parameter["LOC_CODE"] = leakageVO.LOC_CODE;
                    //parameter["DATEE"] = leakageVO.DATEE.ToString("yyyyMMdd");
                    //this.dao.UpdateLeakageManageOption(DataBaseManager, leakageVO.LeakageOptionVO[0], parameter);
                    this.dao.UpdateLeakageManageOption(DataBaseManager, leakageVO);
                }
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            MessageBox.Show("정상적으로 처리되었습니다.");
        }

        public int UpdateLeakageMaxValue(Hashtable parameter)
        {
            int result = 0;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.UpdateLeakageMaxValue(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public double SelectLeakageMaxValue(Hashtable parameter)
        {
            double result = 0;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = Utils.ToDouble(dao.SelectLeakageMaxValue(base.DataBaseManager, parameter));

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectLeakageCompare(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                IList<LeakageCalculationVO> leakageCalculation = this.SelectLeakageManage(parameter);

                base.ConnectionOpen();
                base.BeginTransaction();

                dao.SelectLeakageCompare(base.DataBaseManager, result, "RESULT", parameter);

                DateTime Now = DateTime.Now.AddYears(-2).AddMonths(-11);

                foreach (DataRow row in result.Tables[0].Rows)
                {
                    DateTime sumMonth = Convert.ToDateTime(row["YEAR_MON"]);
                    double dayLeakage = 0;
                    foreach (LeakageCalculationVO leakage in leakageCalculation)
                    {
                        if (sumMonth.Year == leakage.DATEE.Year && sumMonth.Month == leakage.DATEE.Month)
                        {
                            dayLeakage += leakage.LEAKS_DAVG;
                        }
                    }
                    row["LEAKS_DAVG"] = dayLeakage;
                }
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public IList<LeakageCalculationVO> SelectLeakageManage(Hashtable parameter)
        {
            IList<LeakageCalculationVO> LeakageCalculationList = new List<LeakageCalculationVO>();
            DataSet result = new DataSet();
            try
            {
                base.ConnectionOpen();
                base.BeginTransaction();

                //옵션값 등록여부를 체크해서 해당일에 등록이 안되어 있으면 일자별 기본옵션값을 등록해준다.
                this.dao.InsertLeakageDefaultOption(DataBaseManager, parameter);

                this.dao.SelectLeakageCalculation(DataBaseManager, result, "RESULT", parameter);

                foreach (DataRow row in result.Tables["RESULT"].Rows)
                {
                    LeakageCalculationVO leakageObj = new LeakageCalculationVO();

                    foreach (DataColumn column in result.Tables["RESULT"].Columns)
                    {
                        if (row[column.ColumnName] == DBNull.Value && column.ColumnName != "SBLOCK")
                        {
                            row[column.ColumnName] = 0;
                        }
                    }
                    leakageObj.LOC_CODE = Convert.ToString(row["LOC_CODE"]);
                    leakageObj.LBLOCK = Convert.ToString(row["LBLOCK"]);
                    leakageObj.MBLOCK = Convert.ToString(row["MBLOCK"]);
                    leakageObj.SBLOCK = Convert.ToString(row["SBLOCK"]);
                    leakageObj.DATEE = Convert.ToDateTime(row["DATEE"]);
                    leakageObj.M_AVERAGE = Utils.ToDouble(row["M_AVERAGE"]);
                    leakageObj.FILTERING = Utils.ToDouble(row["FILTERING"]);
                    leakageObj.PIP_LEN = Utils.ToDouble(row["PIP_LEN"]);
                    leakageObj.GUPSUJUNSU = Convert.ToInt32(row["GUPSUJUNSU"]);
                    leakageObj.GAJUNG_GAGUSU = Convert.ToInt32(row["GAJUNG_GAGUSU"]);
                    leakageObj.BGAJUNG_GAGUSU = Convert.ToInt32(row["BGAJUNG_GAGUSU"]);
                    leakageObj.SPECIAL_AMTUSE = Utils.ToDouble(row["SPECIAL_AMTUSE"]);
                    leakageObj.INFPNT_HPRES_DAVG = Utils.ToDouble(row["INFPNT_HPRES_DAVG"]);
                    leakageObj.DAY_SUPPLIED = Utils.ToDouble(row["DAY_SUPPLIED"]);
                    leakageObj.MVAVG_MINTIME  = Convert.ToString(row["MVAVG_MINTIME"]);
                    leakageObj.HH00_HPRES_IN = Utils.ToDouble(row["HH00_HPRES_IN"]);
                    leakageObj.HH01_HPRES_IN = Utils.ToDouble(row["HH01_HPRES_IN"]);
                    leakageObj.HH02_HPRES_IN = Utils.ToDouble(row["HH02_HPRES_IN"]);
                    leakageObj.HH03_HPRES_IN = Utils.ToDouble(row["HH03_HPRES_IN"]);
                    leakageObj.HH04_HPRES_IN = Utils.ToDouble(row["HH04_HPRES_IN"]);
                    leakageObj.HH05_HPRES_IN = Utils.ToDouble(row["HH05_HPRES_IN"]);
                    leakageObj.HH06_HPRES_IN = Utils.ToDouble(row["HH06_HPRES_IN"]);
                    leakageObj.HH07_HPRES_IN = Utils.ToDouble(row["HH07_HPRES_IN"]);
                    leakageObj.HH08_HPRES_IN = Utils.ToDouble(row["HH08_HPRES_IN"]);
                    leakageObj.HH09_HPRES_IN = Utils.ToDouble(row["HH09_HPRES_IN"]);
                    leakageObj.HH10_HPRES_IN = Utils.ToDouble(row["HH10_HPRES_IN"]);
                    leakageObj.HH11_HPRES_IN = Utils.ToDouble(row["HH11_HPRES_IN"]);
                    leakageObj.HH12_HPRES_IN = Utils.ToDouble(row["HH12_HPRES_IN"]);
                    leakageObj.HH13_HPRES_IN = Utils.ToDouble(row["HH13_HPRES_IN"]);
                    leakageObj.HH14_HPRES_IN = Utils.ToDouble(row["HH14_HPRES_IN"]);
                    leakageObj.HH15_HPRES_IN = Utils.ToDouble(row["HH15_HPRES_IN"]);
                    leakageObj.HH16_HPRES_IN = Utils.ToDouble(row["HH16_HPRES_IN"]);
                    leakageObj.HH17_HPRES_IN = Utils.ToDouble(row["HH17_HPRES_IN"]);
                    leakageObj.HH18_HPRES_IN = Utils.ToDouble(row["HH18_HPRES_IN"]);
                    leakageObj.HH19_HPRES_IN = Utils.ToDouble(row["HH19_HPRES_IN"]);
                    leakageObj.HH20_HPRES_IN = Utils.ToDouble(row["HH20_HPRES_IN"]);
                    leakageObj.HH21_HPRES_IN = Utils.ToDouble(row["HH21_HPRES_IN"]);
                    leakageObj.HH22_HPRES_IN = Utils.ToDouble(row["HH22_HPRES_IN"]);
                    leakageObj.HH23_HPRES_IN = Utils.ToDouble(row["HH23_HPRES_IN"]);
                    leakageObj.AVG_HPRES_PAVG = Utils.ToDouble(row["AVG_HPRES_PAVG"]);
                    leakageObj.HH00_HPRES_MID = Utils.ToDouble(row["HH00_HPRES_MID"]);
                    leakageObj.HH01_HPRES_MID = Utils.ToDouble(row["HH01_HPRES_MID"]);
                    leakageObj.HH02_HPRES_MID = Utils.ToDouble(row["HH02_HPRES_MID"]);
                    leakageObj.HH03_HPRES_MID = Utils.ToDouble(row["HH03_HPRES_MID"]);
                    leakageObj.HH04_HPRES_MID = Utils.ToDouble(row["HH04_HPRES_MID"]);
                    leakageObj.HH05_HPRES_MID = Utils.ToDouble(row["HH05_HPRES_MID"]);
                    leakageObj.HH06_HPRES_MID = Utils.ToDouble(row["HH06_HPRES_MID"]);
                    leakageObj.HH07_HPRES_MID = Utils.ToDouble(row["HH07_HPRES_MID"]);
                    leakageObj.HH08_HPRES_MID = Utils.ToDouble(row["HH08_HPRES_MID"]);
                    leakageObj.HH09_HPRES_MID = Utils.ToDouble(row["HH09_HPRES_MID"]);
                    leakageObj.HH10_HPRES_MID = Utils.ToDouble(row["HH10_HPRES_MID"]);
                    leakageObj.HH11_HPRES_MID = Utils.ToDouble(row["HH11_HPRES_MID"]);
                    leakageObj.HH12_HPRES_MID = Utils.ToDouble(row["HH12_HPRES_MID"]);
                    leakageObj.HH13_HPRES_MID = Utils.ToDouble(row["HH13_HPRES_MID"]);
                    leakageObj.HH14_HPRES_MID = Utils.ToDouble(row["HH14_HPRES_MID"]);
                    leakageObj.HH15_HPRES_MID = Utils.ToDouble(row["HH15_HPRES_MID"]);
                    leakageObj.HH16_HPRES_MID = Utils.ToDouble(row["HH16_HPRES_MID"]);
                    leakageObj.HH17_HPRES_MID = Utils.ToDouble(row["HH17_HPRES_MID"]);
                    leakageObj.HH18_HPRES_MID = Utils.ToDouble(row["HH18_HPRES_MID"]);
                    leakageObj.HH19_HPRES_MID = Utils.ToDouble(row["HH19_HPRES_MID"]);
                    leakageObj.HH20_HPRES_MID = Utils.ToDouble(row["HH20_HPRES_MID"]);
                    leakageObj.HH21_HPRES_MID = Utils.ToDouble(row["HH21_HPRES_MID"]);
                    leakageObj.HH22_HPRES_MID = Utils.ToDouble(row["HH22_HPRES_MID"]);
                    leakageObj.HH23_HPRES_MID = Utils.ToDouble(row["HH23_HPRES_MID"]);

                    leakageObj.AddLeakageOptionVO(
                        Convert.ToDouble(row["JUMAL_USED"]), 
                        Convert.ToDouble(row["GAJUNG_USED"]), 
                        Convert.ToDouble(row["BGAJUNG_USED"]), 
                        Convert.ToInt32(row["SPECIAL_USED_PERCENT"]),
                        Convert.ToDouble(row["SPECIAL_USED"]), 
                        Convert.ToDouble(row["N1"]), 
                        Convert.ToDouble(row["LC"]), 
                        Convert.ToDouble(row["ICF"])
                        );

                    LeakageCalculationList.Add(leakageObj);
                }

                base.CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                base.CloseTransaction();
                base.ConnectionClose();
            }
            return LeakageCalculationList;
        }

        public LeakageOptionVO SelectBlockDefaultOption(Hashtable parameter)
        {
            LeakageOptionVO LeakageOption = null;
            DataSet result = new DataSet();
            try
            {
                base.ConnectionOpen();
                base.BeginTransaction();
                this.dao.SelectBlockDefaultOption(DataBaseManager, result, "RESULT", parameter);

                foreach (DataRow row in result.Tables["RESULT"].Rows)
                {
                    LeakageOption = new LeakageOptionVO(
                        Convert.ToDouble(DataUtils.NullToZero(row["JUMAL_USED"])),
                        Convert.ToDouble(DataUtils.NullToZero(row["GAJUNG_USED"])),
                        Convert.ToDouble(DataUtils.NullToZero(row["BGAJUNG_USED"])),
                        Convert.ToInt32(DataUtils.NullToZero(row["SPECIAL_USED_PERCENT"])),
                        Convert.ToDouble(DataUtils.NullToZero(row["SPECIAL_USED"])),
                        Convert.ToDouble(DataUtils.NullToZero(row["N1"])),
                        Convert.ToDouble(DataUtils.NullToZero(row["LC"])),
                        Convert.ToDouble(DataUtils.NullToZero(row["ICF"]))
                        );
                }

                base.CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                base.CloseTransaction();
                base.ConnectionClose();
            }
            return LeakageOption;
        }


        public DataTable SelectLeakageResult(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageResult(DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result.Tables["RESULT"];
        }

        public DataTable SelectLeakageResultDetail(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageResultDetail(DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result.Tables["RESULT"];
        }

        public void UpdateLeakageResultAlarm(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.UpdateLeakageResultAlarm(DataBaseManager, parameter);

                if (parameter["MONITOR_ALARM"].ToString() == "True")
                {
                    MessageBox.Show("누수경고 알람을 시작 합니다.");
                }
                else if (parameter["MONITOR_ALARM"].ToString() == "False")
                {
                    MessageBox.Show("누수경고 알람을 일시 정지 합니다.");
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        //한달간 야간최소유량, 공급량 조회
        public DataTable SelectLeakageResultData(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageResultData(DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result.Tables["RESULT"];
        }
    }
}
