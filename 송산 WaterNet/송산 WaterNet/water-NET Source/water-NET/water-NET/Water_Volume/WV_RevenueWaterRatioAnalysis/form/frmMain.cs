﻿using System;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.form;
using System.Collections;
using ChartFX.WinForms;
using WaterNet.WV_RevenueWaterRatioAnalysis.work;
using ChartFX.WinForms.DataProviders;
using WaterNet.WV_Common.interface1;
using EMFrame.log;

namespace WaterNet.WV_RevenueWaterRatioAnalysis.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private TableLayout tableLayout = null;
        private UltraGridManager gridManager = null;
        private ChartManager chartManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);

            this.searchBox1.SmallBlockContainer.Visible = false;
            this.searchBox1.SmallBlockObject.SelectedIndex = 0;
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(this.chart1, "yyyy-MM");
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);
            this.searchBtn.Click += new EventHandler(SelectRevenueWaterRatioStatus_EventHandler);
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.chart1, "유수율상호분석", 0, 0, 9, 20);
                this.excelManager.AddWorksheet(this.ultraGrid1, "유수율상호분석", 10, 0);
                this.excelManager.Save("유수율상호분석", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectRevenueWaterRatioStatus_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectRevenueWaterRatioStatus(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 유수율상호분석 검색
        /// </summary>
        private void SelectRevenueWaterRatioStatus(Hashtable parameter)
        {
            this.mainMap.MoveToBlock(parameter);
            DataSet result = RevenueWaterRatioAnalysisWork.GetInstance().SelectBlockRevenueWaterRatioStatus(parameter);
            this.gridManager.SetDataSource(this.ultraGrid1, result.Tables["BOUND"], result.Tables["UNBOUND"], 1, "###,##0.00");

            //소수점 자리 표현(차트포함)
            //foreach (UltraGridColumn gridColumn in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            //{
            //    if (!gridColumn.IsBound)
            //    {
            //        gridColumn.Format = "###,###.00";
            //    }
            //}

            this.InitializeChartSetting(result.Tables["CHART"]);
        }

        /// <summary>
        /// 검색 데이터 변경시 차트 세팅/ 데이터 초기화 
        /// </summary>
        private void InitializeChartSetting(object resultList)
        {
            DataTable ratioList = (DataTable)resultList;

            Chart chart = this.chartManager.Items[0];
            this.chartManager.AllClear(chart);

            DataTableProvider dt = new DataTableProvider(ratioList);
            CrosstabDataProvider cdt = new CrosstabDataProvider();
            cdt.DataSource = dt;

            chart.DataSourceSettings.Fields.Add(new FieldMap("BLK_NAM", FieldUsage.ColumnHeading));
            chart.DataSourceSettings.Fields.Add(new FieldMap("YEAR_MON", FieldUsage.RowHeading));
            chart.DataSourceSettings.Fields.Add(new FieldMap("REVENUE_RATIO", FieldUsage.Value));

            chart.DataSource = cdt;

            foreach (SeriesAttributes series in chart.Series)
            {
                if (chart.Series.IndexOf(series) != 0)
                {
                    series.Gallery = Gallery.Lines;
                    series.Line.Width = 3;
                    series.MarkerSize = 3;
                }
            }

            if (chart.Series.Count == 0)
            {
                return;
            }

            chart.Series[0].Gallery = Gallery.Bar;
            chart.Series[0].SendToBack();
            chart.AxesY[0].Title.Text = "유수율(%)";

            chart.Refresh();
        }
    }
}
