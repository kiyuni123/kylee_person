﻿using System;
using WaterNet.WV_RevenueWaterRatioAnalysis.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;

namespace WaterNet.WV_RevenueWaterRatioAnalysis.work
{
    public class RevenueWaterRatioAnalysisWork : BaseWork
    {
        private static RevenueWaterRatioAnalysisWork work = null;
        private RevenueWaterRatioAnalysisDao dao = null;

        public static RevenueWaterRatioAnalysisWork GetInstance()
        {
            if (work == null)
            {
                work = new RevenueWaterRatioAnalysisWork();
            }
            return work;
        }


        public DataSet SelectBlockRevenueWaterRatioStatus(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectPeriodRevenueWaterRatioStatus(base.DataBaseManager, result, "BOUND", parameter); //bound column
                dao.SelectBlockRevenueWaterRatioStatus(base.DataBaseManager, result, "UNBOUND", parameter); //unbound column
                dao.SelectChartRevenueWaterRatioStatus(base.DataBaseManager, result, "CHART", parameter); //chart data
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        private RevenueWaterRatioAnalysisWork()
        {
            dao = RevenueWaterRatioAnalysisDao.GetInstance();
        }
    }
}
