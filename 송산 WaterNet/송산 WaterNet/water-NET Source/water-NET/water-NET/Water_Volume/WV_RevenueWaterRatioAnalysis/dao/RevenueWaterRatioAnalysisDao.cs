﻿using System.Text;
using WaterNet.WV_Common.dao;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_RevenueWaterRatioAnalysis.dao
{
    public class RevenueWaterRatioAnalysisDao : BaseDao
    {
        private static RevenueWaterRatioAnalysisDao dao = null;

        private RevenueWaterRatioAnalysisDao() { }

        public static RevenueWaterRatioAnalysisDao GetInstance()
        {
            if (dao == null)
            {
                dao = new RevenueWaterRatioAnalysisDao();
            }
            return dao;
        }

        public void SelectPeriodRevenueWaterRatioStatus(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyy-mm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");
            
            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void SelectBlockRevenueWaterRatioStatus(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc.loc_name blk_nam                                                                     ");
            query.AppendLine("      ,round(((to_number(wrr.revenue) / decode(to_number(wrr.water_supplied),0,null,to_number(wrr.water_supplied))) * 100),2) revenue_ratio  ");
            query.AppendLine("  from (																						  ");
            query.AppendLine("       select loc.sgccd																		  ");
            query.AppendLine("             ,loc.loc_code																	  ");
            query.AppendLine("             ,loc.ploc_code																	  ");
            query.AppendLine("             ,loc.loc_name																	  ");
            query.AppendLine("             ,loc.ord																			  ");
            query.AppendLine("             ,tmp.year_mon																	  ");
            query.AppendLine("        from (																				  ");
            query.AppendLine("              select a.sgccd																	  ");
            query.AppendLine("                    ,a.loc_code																	  ");
            query.AppendLine("                    ,a.ploc_code																  ");
            query.AppendLine("                    ,a.loc_name																	  ");
            query.AppendLine("                    ,rownum ord																  ");
            query.AppendLine("                from cm_location a															  ");
            query.AppendLine("               where 1 = 1																	  ");

            query.AppendLine("                 and (select count(*) from if_ihtags where loc_code = a.loc_code) != 0          ");

            if (parameter["FTR_CODE"].ToString() == "BZ001")
            {
                query.AppendLine("                 and a.ftr_code != 'BZ003'														  ");
            }

            query.AppendLine("               start with a.loc_code = :LOC_CODE												  ");
            query.AppendLine("             connect by prior a.loc_code = a.ploc_code											  ");
            query.AppendLine("               order SIBLINGS by a.ftr_idn														  ");
            query.AppendLine("              ) loc																			  ");
            query.AppendLine("             ,(																				  ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");
            
            query.AppendLine("              ) tmp																			  ");
            query.AppendLine("      ) loc																					  ");
            query.AppendLine("      ,wv_revenue_ratio wrr																	  ");
            query.AppendLine(" where 1 = 1																					  ");
            query.AppendLine("   and wrr.loc_code(+) = loc.loc_code															  ");
            query.AppendLine("   and wrr.year_mon(+) = loc.year_mon															  ");
            query.AppendLine(" order by																						  ");
            query.AppendLine("       to_date(loc.year_mon, 'yyyymm')														  ");
            query.AppendLine("      ,loc.ord																				  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
	                 ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void SelectChartRevenueWaterRatioStatus(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm') year_mon                                             ");
            query.AppendLine("      ,loc.loc_name blk_nam                                                                     ");
            query.AppendLine("      ,round(((to_number(wrr.revenue) / decode(to_number(wrr.water_supplied),0,null,to_number(wrr.water_supplied))) * 100),2) revenue_ratio  ");
            query.AppendLine("  from (																						  ");
            query.AppendLine("       select loc.sgccd																		  ");
            query.AppendLine("             ,loc.loc_code																	  ");
            query.AppendLine("             ,loc.ploc_code																	  ");
            query.AppendLine("             ,loc.loc_name																	  ");
            query.AppendLine("             ,loc.ord																			  ");
            query.AppendLine("             ,tmp.year_mon																	  ");
            query.AppendLine("        from (																				  ");
            query.AppendLine("              select a.sgccd																	  ");
            query.AppendLine("                    ,a.loc_code																	  ");
            query.AppendLine("                    ,a.ploc_code																  ");
            query.AppendLine("                    ,a.loc_name																	  ");
            query.AppendLine("                    ,rownum ord																  ");
            query.AppendLine("                from cm_location a															  ");
            query.AppendLine("               where 1 = 1																	  ");

            query.AppendLine("                 and (select count(*) from if_ihtags where loc_code = a.loc_code) != 0          ");

            if (parameter["FTR_CODE"].ToString() == "BZ001")
            {
                query.AppendLine("                 and a.ftr_code != 'BZ003'														  ");
            }

            query.AppendLine("               start with a.loc_code = :LOC_CODE												  ");
            query.AppendLine("             connect by prior a.loc_code = a.ploc_code											  ");
            query.AppendLine("               order SIBLINGS by a.ftr_idn														  ");
            query.AppendLine("              ) loc																			  ");
            query.AppendLine("             ,(																				  ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");
            
            query.AppendLine("              ) tmp																			  ");
            query.AppendLine("      ) loc																					  ");
            query.AppendLine("      ,wv_revenue_ratio wrr																	  ");
            query.AppendLine(" where 1 = 1																					  ");
            query.AppendLine("   and wrr.loc_code(+) = loc.loc_code															  ");
            query.AppendLine("   and wrr.year_mon(+) = loc.year_mon															  ");
            query.AppendLine(" order by																						  ");
            query.AppendLine("       to_date(loc.year_mon, 'yyyymm')														  ");
            query.AppendLine("      ,loc.ord																				  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
	                 ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }
    }
}
