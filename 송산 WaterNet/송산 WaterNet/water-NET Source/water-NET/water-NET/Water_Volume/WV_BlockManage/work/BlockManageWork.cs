﻿using System;
using WaterNet.WV_BlockManage.dao;
using WaterNet.WV_Common.work;
using System.Data;
using WaterNet.WV_Common.dao;
using System.Collections;
using System.Windows.Forms;

namespace WaterNet.WV_BlockManage.work
{
    public class BlockManageWork : BaseWork
    {
        private static BlockManageWork work = null;
        private BlockManageDao dao = null;

        public static BlockManageWork GetInstance()
        {
            if (work == null)
            {
                work = new BlockManageWork();
            }
            return work;
        }

        private BlockManageWork()
        {
            dao = BlockManageDao.GetInstance();
        }

        public DataSet SelectBlock()
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                BlockDao.GetInstance().SelectBlockListByLevel(DataBaseManager, "BZ001", result, "LARGE");
                BlockDao.GetInstance().SelectBlockListByLevel(DataBaseManager, "BZ002", result, "MIDDLE");
                BlockDao.GetInstance().SelectBlockListByLevel(DataBaseManager, "BZ003", result, "SMALL");

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="LOC_CODE">
        /// LOC_CODE : 지역관리코드
        /// </param>
        /// <returns></returns>
        public DataTable SelectBlockOption(string LOC_CODE)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectBlockDefaultOption(DataBaseManager, result, "RESULT", LOC_CODE);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result.Tables["RESULT"];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter">
        ///LOC_CODE                   : 지역관리코드
        ///SGCCD                      : 센터
        ///LBLOCK                     : 대블록
        ///MBLOCK                     : 중블록
        ///SBLOCK                     : 소블록
        ///MAX_LEAKAGE                : 누수한계치
        ///TM_INS_YN                  : 평균수압지점수압계사용여부
        ///HPRES_DIFF                 : 수압차
        ///HPRES_KIND                 : 수압제어유형
        ///THOUR_START                : 주간시작시간
        ///THOUR_END                  : 주간종료시간
        ///NHOUR_START                : 야간시작시간
        ///NHOUR_END                  : 야간종료시간
        ///GAJUNG_USED                : 가정야간사용
        ///BGAJUNG_USED               : 비가정야간사용
        ///JUMAL_USED                 : 주말야간사용
        ///SPECIAL_USED_PERCENT       : 특별야간사용퍼센트
        ///SPECIAL_USED               : 특별야간사용
        ///N1                         : N1
        ///LC                         : 옥내관연장
        ///ICF                        : 시설상태지수
        ///LOC_TYPE                   : 블록유형
        /// </param>
        public void UpdateBlockOption(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                string loc_code = parameter["LOC_CODE"].ToString();

                if (Convert.ToBoolean(parameter["All"]))
                {
                    DataTable dataTable = dao.SelectSubBlock(DataBaseManager, parameter["LOC_CODE"].ToString()).Tables[0];

                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        parameter["LOC_CODE"] = dataRow["LOC_CODE"];

                        if (loc_code == dataRow["LOC_CODE"].ToString())
                        {
                            dao.UpdateBlockOption(DataBaseManager, parameter);
                        }
                        else
                        {
                            dao.UpdateBlockOption2(DataBaseManager, parameter);
                        }
                    }
                }
                else if (!Convert.ToBoolean(parameter["All"]))
                {
                    dao.UpdateBlockOption(DataBaseManager, parameter);
                }
                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }
    }
}
