﻿using System;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_BlockManage.work;
using Infragistics.Win.UltraWinTree;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using EMFrame.log;

namespace WaterNet.WV_BlockManage.form
{
    public partial class frmMain : Form, WaterNet.WaterNetCore.IForminterface
    {
        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion


        private UltraGridManager gridManager = null;

        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================야간최소유량 산정 방식별 조회

            object o = EMFrame.statics.AppStatic.USER_MENU["블록별수량관리기본설정ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeEvent();
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
        }

        //폼초기화
        private void InitializeForm()
        {
            this.TreeDataSetting();
        }

        //트리설정
        private void TreeDataSetting()
        {
            DataSet dataSet = BlockManageWork.GetInstance().SelectBlock();

            UltraTreeNode largeBlock = null;
            UltraTreeNode middelBlock = null;

            try
            {
                foreach (DataRow largerow in dataSet.Tables["LARGE"].Rows)
                {
                    UltraTree1.Nodes.Add(largerow["LOC_CODE"].ToString(), largerow["LOC_NAME"].ToString());
                }

                foreach (DataRow largerow in dataSet.Tables["LARGE"].Rows)
                {
                    largeBlock = UltraTree1.Nodes[largerow["LOC_CODE"].ToString()];

                    foreach (DataRow middlerow in dataSet.Tables["MIDDLE"].Rows)
                    {
                        if (largerow["LOC_CODE"].ToString() == middlerow["PLOC_CODE"].ToString())
                        {
                            middelBlock = largeBlock.Nodes.Add(middlerow["LOC_CODE"].ToString(), middlerow["LOC_NAME"].ToString());

                            foreach (DataRow smallrow in dataSet.Tables["SMALL"].Rows)
                            {
                                if (middlerow["LOC_CODE"].ToString() == smallrow["PLOC_CODE"].ToString())
                                {
                                    middelBlock.Nodes.Add(smallrow["LOC_CODE"].ToString(), smallrow["LOC_NAME"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            //트리펼치기
            this.UltraTree1.ExpandAll();

            //첫노트선택
            this.UltraTree1.Nodes[0].Selected = true;
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            //this.gridManager.Add(this.ultraGrid1);

            //그리드 세팅
            this.ultraGrid1.DisplayLayout.Bands[0].CardView = true;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

            this.ultraGrid1.DisplayLayout.Bands[0].CardSettings.AutoFit = true;
            this.ultraGrid1.DisplayLayout.Bands[0].CardSettings.Style = CardStyle.StandardLabels;
        }

        private void InitializeEvent()
        {
            this.UltraTree1.AfterSelect += new AfterNodeSelectEventHandler(UltraTree1_AfterSelect);
            this.ultraGrid1.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(ultraGrid1_CellChange);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);

            this.cancelBtn.Click += new EventHandler(cancelBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.closeBtn.Click += new EventHandler(closeBtn_Click);
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SGCCD"].CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SGCCD"].CellDisplayStyle = CellDisplayStyle.FormattedText;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].CellDisplayStyle = CellDisplayStyle.FormattedText;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].CellDisplayStyle = CellDisplayStyle.FormattedText;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].CellDisplayStyle = CellDisplayStyle.FormattedText;

            this.ChangeColumsEditState();
        }

        //셀변경
        private void ultraGrid1_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            this.ultraGrid1.UpdateData();
            this.ChangeColumsEditState();
        }

        private void ChangeColumsEditState()
        {
            if (this.ultraGrid1.Rows.Count == 0)
            {
                return;
            }

            CellsCollection cells = this.ultraGrid1.Rows[0].Cells;

            //if (cells["TM_INS_YN"].Value.ToString() == "Y")
            //{
            //    cells["HPRES_DIFF"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            //}
            //else
            //{
            //    cells["HPRES_DIFF"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            //}

            ////고정식
            //if (cells["HPRES_KIND"].Value.ToString() == "000001")
            //{
            //    cells["THOUR_START"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            //    cells["THOUR_END"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            //    cells["NHOUR_START"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            //    cells["NHOUR_END"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

            //    cells["THOUR_START"].Column.CellDisplayStyle = CellDisplayStyle.FormattedText;
            //    cells["THOUR_END"].Column.CellDisplayStyle = CellDisplayStyle.FormattedText;
            //    cells["NHOUR_START"].Column.CellDisplayStyle = CellDisplayStyle.FormattedText;
            //    cells["NHOUR_END"].Column.CellDisplayStyle = CellDisplayStyle.FormattedText;
            //}
            //else
            //{
            //    cells["THOUR_START"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            //    cells["THOUR_END"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            //    cells["NHOUR_START"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            //    cells["NHOUR_END"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;

            //    cells["THOUR_START"].Column.CellDisplayStyle = CellDisplayStyle.FullEditorDisplay;
            //    cells["THOUR_END"].Column.CellDisplayStyle = CellDisplayStyle.FullEditorDisplay;
            //    cells["NHOUR_START"].Column.CellDisplayStyle = CellDisplayStyle.FullEditorDisplay;
            //    cells["NHOUR_END"].Column.CellDisplayStyle = CellDisplayStyle.FullEditorDisplay;
            //}

            if (cells["SPECIAL_USED_PERCENT"].Value.ToString() != "99")
            {
                cells["SPECIAL_USED"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            }
            else
            {
                cells["SPECIAL_USED"].Column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            }
        }

        private void UltraTree1_AfterSelect(object sender, SelectEventArgs e)
        {
            this.SelectBlockOption(e.NewSelections[0].Key);
        }

        private void SelectBlockOption(string LOC_CODE)
        {
            this.ultraGrid1.DataSource = BlockManageWork.GetInstance().SelectBlockOption(LOC_CODE);

            if (this.ultraGrid1.Rows.Count == 0)
            {
                return;
            }

            if (this.ultraGrid1.Rows[0].Cells["FTR_CODE"].Value.ToString() == "BZ001")
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SUPPLIED_DAY"].Hidden = false;
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["REVENUE_DAY"].Hidden = false;
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["REVENUE_FIX"].Hidden = false;
            }
            else if (this.ultraGrid1.Rows[0].Cells["FTR_CODE"].Value.ToString() != "BZ001")
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SUPPLIED_DAY"].Hidden = true;
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["REVENUE_DAY"].Hidden = true;
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["REVENUE_FIX"].Hidden = true;
            }
        }

        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }
            //if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("TM_INS_YN"))
            //{
            //    valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("TM_INS_YN");
            //    Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2006");
            //}
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SUPPLIED_DAY"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SUPPLIED_DAY");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SINGLE_DAY, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("REVENUE_DAY"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("REVENUE_DAY");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SINGLE_DAY, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("REVENUE_FIX"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("REVENUE_FIX");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SINGLE_DAY, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("HPRES_KIND"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("HPRES_KIND");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2005");
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("THOUR_START"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("THOUR_START");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SINGLE_HOUR, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("THOUR_END"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("THOUR_END");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SINGLE_HOUR, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("NHOUR_START"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("NHOUR_START");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SINGLE_HOUR, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("NHOUR_END"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("NHOUR_END");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SINGLE_HOUR, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SPECIAL_USED_PERCENT"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SPECIAL_USED_PERCENT");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2004");
            }
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SUPPLIED_DAY"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SUPPLIED_DAY"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["REVENUE_DAY"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["REVENUE_DAY"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["REVENUE_FIX"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["REVENUE_FIX"];
            //this.ultraGrid1.DisplayLayout.Bands[0].Columns["TM_INS_YN"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["TM_INS_YN"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["HPRES_KIND"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["HPRES_KIND"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["THOUR_START"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["THOUR_START"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["THOUR_END"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["THOUR_END"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["NHOUR_START"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["NHOUR_START"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["NHOUR_END"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["NHOUR_END"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SPECIAL_USED_PERCENT"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SPECIAL_USED_PERCENT"];
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //주간, 야간시간 선택 값 체크,
                //시작 시간과 종료시간을 비교해서 시작시간이 더 클수 없게 한다.
                double thour_start = Utils.ToDouble(this.ultraGrid1.Rows[0].Cells["THOUR_START"].Value);
                double thour_end = Utils.ToDouble(this.ultraGrid1.Rows[0].Cells["THOUR_END"].Value);
                double nhour_start = Utils.ToDouble(this.ultraGrid1.Rows[0].Cells["NHOUR_START"].Value);
                double nhour_end = Utils.ToDouble(this.ultraGrid1.Rows[0].Cells["NHOUR_END"].Value);

                if (thour_start > thour_end)
                {
                    MessageBox.Show("주간시작 시간이 종료시간보다 크게 설정되었습니다.\n다시 설정해주세요");
                    return;
                }

                if (nhour_start > nhour_end)
                {
                    MessageBox.Show("야간시작 시간이 종료시간보다 크게 설정되었습니다.\n다시 설정해주세요");
                    return;
                }

                Hashtable parameter = Utils.ConverToHashtable(((DataTable)this.ultraGrid1.DataSource).Rows[0]);
                parameter["All"] = true;

                if (parameter["FTR_CODE"].ToString() != "BZ003")
                {
                    DialogResult qe = MessageBox.Show("하위 블록이 존재합니다.\n일괄 적용 합니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (qe == DialogResult.Yes)
                    {
                        parameter["All"] = true;
                    }
                    if (qe == DialogResult.No)
                    {
                        parameter["All"] = false;
                    }
                }

                BlockManageWork.GetInstance().UpdateBlockOption(parameter);
                this.SelectBlockOption(this.UltraTree1.SelectedNodes[0].Key);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.SelectBlockOption(this.UltraTree1.SelectedNodes[0].Key);
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
