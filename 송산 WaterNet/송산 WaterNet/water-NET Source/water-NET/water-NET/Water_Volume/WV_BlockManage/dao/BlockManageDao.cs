﻿using System.Text;
using WaterNet.WV_Common.dao;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_BlockManage.dao
{
    public class BlockManageDao : BaseDao
    {
        private static BlockManageDao dao = null;

        private BlockManageDao() { }

        public static BlockManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new BlockManageDao();
            }
            return dao;
        }

        //본인포함 하위블록 모두 조회
        public DataSet SelectSubBlock(OracleDBManager manager, string LOC_CODE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" select loc_code                                                                                                                  ");
            query.AppendLine("   from cm_location                                                                                                               ");
            query.AppendLine("  start with loc_code = :LOC_CODE                                                                                                 ");
            query.AppendLine("connect by prior loc_code = ploc_code                                                                                             ");

            IDataParameter[] parameters =  {
                        new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = LOC_CODE;

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //해당계층조회
        public void SelectBlockListByLevel(OracleDBManager manager, string FTR_CODE, DataSet dataSet, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT loc_code                                                                             ");
            query.AppendLine("      ,ploc_code                                                                            ");
            query.AppendLine("      ,loc_name                                                                             ");
            query.AppendLine("      ,ftr_idn                                                                              ");
            query.AppendLine("      ,ftr_code                                                                             ");
            query.AppendLine("      ,rel_loc_name                                                                         ");
            query.AppendLine("      ,kt_gbn                                                                               ");
            query.AppendLine("  FROM cm_location                                                                          ");
            query.AppendLine(" WHERE 1 = 1                                                                                ");
            query.AppendLine("   AND ftr_code = :FTR_CODE                                                                 ");
            query.AppendLine(" START WITH ftr_code = :FTR_CODE                                                            ");
            query.AppendLine(" CONNECT BY PRIOR loc_code = ploc_code                                                      ");
            query.AppendLine(" ORDER SIBLINGS BY ftr_idn                                                                  ");

            IDataParameter[] parameters =  {
                        new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                        ,new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = FTR_CODE;
            parameters[1].Value = FTR_CODE;

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록계층조회
        public object SelectFtrCode(OracleDBManager manager, string LOC_CODE)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("select ftr_code from cm_location where loc_code = :LOC_CODE                                                                       ");

            IDataParameter[] parameters =  {
                        new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = LOC_CODE;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        /// <summary>
        ///  블록의 기본옵션을 조회한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataSet"></param>
        /// <param name="dataMember"></param>
        /// <param name="LOC_CODE"></param>
        public void SelectBlockDefaultOption(OracleDBManager manager, DataSet dataSet, string dataMember, string LOC_CODE)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                       ");
            query.AppendLine("(                                                                                                                                 ");
            query.AppendLine("select c1.sgccd                                                                                                                   ");
            //query.AppendLine("      ,c1.loc_code                                                                                                                ");

            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ftr_code = 'BZ003' and ploc_code = c1.loc_code), c1.loc_code) loc_code ");
            
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                           ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                ");
            query.AppendLine("  from                                                                                                                            ");
            query.AppendLine("      (                                                                                                                           ");
            query.AppendLine("       select sgccd                                                                                                               ");
            query.AppendLine("             ,loc_code                                                                                                            ");
            query.AppendLine("             ,ploc_code                                                                                                           ");
            query.AppendLine("             ,loc_name                                                                                                            ");
            query.AppendLine("             ,ftr_idn                                                                                                             ");
            query.AppendLine("             ,ftr_code                                                                                                            ");
            query.AppendLine("             ,rel_loc_name                                                                                                        ");
            query.AppendLine("             ,kt_gbn                                                                                                              ");
            query.AppendLine("             ,rownum ord                                                                                                          ");
            query.AppendLine("         from cm_location                                                                                                         ");
            query.AppendLine("        where 1 = 1                                                                                                               ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                           ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                     ");
            query.AppendLine("        order siblings by ftr_idn                                                                                                 ");
            query.AppendLine("      ) c1                                                                                                                        ");
            query.AppendLine("       ,cm_location c2                                                                                                            ");
            query.AppendLine("       ,cm_location c3                                                                                                            ");
            query.AppendLine(" where 1 = 1                                                                                                                      ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                              ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                              ");
            query.AppendLine(" order by c1.ord                                                                                                                  ");
            query.AppendLine(")                                                                                                                                 ");
            query.AppendLine("select loc.sgccd                                                                                                                  ");
            query.AppendLine("      ,loc.ftr_code                                                                                                               ");
            query.AppendLine("      ,loc.loc_code                                                                                                               ");
            query.AppendLine("      ,loc.lblock                                                                                                                 ");
            query.AppendLine("      ,loc.mblock                                                                                                                 ");
            query.AppendLine("      ,loc.sblock                                                                                                                 ");
            query.AppendLine("      ,supplied_day                                                                                                               ");
            query.AppendLine("      ,revenue_day                                                                                                                ");
            query.AppendLine("      ,revenue_fix                                                                                                                ");
            query.AppendLine("      ,max_leakage                                                                                                                ");
            query.AppendLine("      ,hpres_kind                                                                                                                 ");
            query.AppendLine("      ,thour_start                                                                                                                ");
            query.AppendLine("      ,thour_end                                                                                                                  ");
            query.AppendLine("      ,nhour_start                                                                                                                ");
            query.AppendLine("      ,nhour_end                                                                                                                  ");
            query.AppendLine("      ,jumal_used                                                                                                                 ");
            query.AppendLine("      ,gajung_used                                                                                                                ");
            query.AppendLine("      ,bgajung_used                                                                                                               ");
            query.AppendLine("      ,special_used                                                                                                               ");
            query.AppendLine("      ,n1                                                                                                                         ");
            query.AppendLine("      ,lc                                                                                                                         ");
            query.AppendLine("      ,icf                                                                                                                        ");
            query.AppendLine("      ,special_used_percent                                                                                                       ");
            query.AppendLine("  from loc loc                                                                                                                    ");
            query.AppendLine("      ,wv_block_default_option wbd                                                                                                ");
            query.AppendLine(" where loc.loc_code = wbd.loc_code                                                                                                ");

            IDataParameter[] parameters =  {
                        new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                        ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = LOC_CODE;
            parameters[1].Value = LOC_CODE;

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        public void UpdateBlockOption(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_block_default_option                                                   ");
            query.Append("   set max_leakage = :MAX_LEAKAGE                                                    ");
            query.Append("      ,supplied_day = :SUPPLIED_DAY                                                  ");
            query.Append("      ,revenue_day = :REVENUE_DAY                                                    ");
            query.Append("      ,revenue_fix = :REVENUE_FIX                                                    ");
            query.Append("      ,hpres_kind = :HPRES_KIND                                                      ");
            query.Append("      ,thour_start = :THOUR_START                                                    ");
            query.Append("      ,thour_end = :THOUR_END                                                        ");
            query.Append("      ,nhour_start = :NHOUR_START                                                    ");
            query.Append("      ,nhour_end = :NHOUR_END                                                        ");
            query.Append("      ,jumal_used = :JUMAL_USED                                                      ");
            query.Append("      ,gajung_used = :GAJUNG_USED                                                    ");
            query.Append("      ,bgajung_used = :BGAJUNG_USED                                                  ");
            query.Append("      ,special_used = :SPECIAL_USED                                                  ");
            query.Append("      ,n1 = :N1                                                                      ");
            query.Append("      ,lc = :LC                                                                      ");
            query.Append("      ,icf = :ICF                                                                    ");
            query.Append("      ,special_used_percent = :SPECIAL_USED_PERCENT                                  ");
            query.Append(" where loc_code = :LOC_CODE                                                          ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("MAX_LEAKAGE", OracleDbType.Varchar2)
	                    ,new OracleParameter("SUPPLIED_DAY", OracleDbType.Varchar2)
	                    ,new OracleParameter("REVENUE_DAY", OracleDbType.Varchar2)
                        ,new OracleParameter("REVENUE_FIX", OracleDbType.Varchar2)
	                    ,new OracleParameter("HPRES_KIND", OracleDbType.Varchar2)
	                    ,new OracleParameter("THOUR_START", OracleDbType.Varchar2)
                        ,new OracleParameter("THOUR_END", OracleDbType.Varchar2)
                        ,new OracleParameter("NHOUR_START", OracleDbType.Varchar2)
                        ,new OracleParameter("NHOUR_END", OracleDbType.Varchar2)
                        ,new OracleParameter("JUMAL_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("GAJUNG_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("BGAJUNG_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("SPECIAL_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("N1", OracleDbType.Varchar2)
                        ,new OracleParameter("LC", OracleDbType.Varchar2)
                        ,new OracleParameter("ICF", OracleDbType.Varchar2)
                        ,new OracleParameter("SPECIAL_USED_PERCENT", OracleDbType.Varchar2)
                        ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["MAX_LEAKAGE"];
            parameters[1].Value = parameter["SUPPLIED_DAY"];
            parameters[2].Value = parameter["REVENUE_DAY"];
            parameters[3].Value = parameter["REVENUE_FIX"];
            parameters[4].Value = parameter["HPRES_KIND"];
            parameters[5].Value = parameter["THOUR_START"];
            parameters[6].Value = parameter["THOUR_END"];
            parameters[7].Value = parameter["NHOUR_START"];
            parameters[8].Value = parameter["NHOUR_END"];
            parameters[9].Value = parameter["JUMAL_USED"];
            parameters[10].Value = parameter["GAJUNG_USED"];
            parameters[11].Value = parameter["BGAJUNG_USED"];
            parameters[12].Value = parameter["SPECIAL_USED"];
            parameters[13].Value = parameter["N1"];
            parameters[14].Value = parameter["LC"];
            parameters[15].Value = parameter["ICF"];
            parameters[16].Value = parameter["SPECIAL_USED_PERCENT"];
            parameters[17].Value = parameter["LOC_CODE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void UpdateBlockOption2(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_block_default_option                                                   ");
            query.Append("   set supplied_day = :SUPPLIED_DAY                                                  ");
            query.Append("      ,revenue_day = :REVENUE_DAY                                                    ");
            query.Append("      ,revenue_fix = :REVENUE_FIX                                                    ");
            query.Append("      ,hpres_kind = :HPRES_KIND                                                      ");
            query.Append("      ,thour_start = :THOUR_START                                                    ");
            query.Append("      ,thour_end = :THOUR_END                                                        ");
            query.Append("      ,nhour_start = :NHOUR_START                                                    ");
            query.Append("      ,nhour_end = :NHOUR_END                                                        ");
            query.Append("      ,jumal_used = :JUMAL_USED                                                      ");
            query.Append("      ,gajung_used = :GAJUNG_USED                                                    ");
            query.Append("      ,bgajung_used = :BGAJUNG_USED                                                  ");
            query.Append("      ,special_used = :SPECIAL_USED                                                  ");
            query.Append("      ,n1 = :N1                                                                      ");
            query.Append("      ,lc = :LC                                                                      ");
            query.Append("      ,icf = :ICF                                                                    ");
            query.Append("      ,special_used_percent = :SPECIAL_USED_PERCENT                                  ");
            query.Append(" where loc_code = :LOC_CODE                                                          ");

            IDataParameter[] parameters =  {
	                    new OracleParameter("SUPPLIED_DAY", OracleDbType.Varchar2)
	                    ,new OracleParameter("REVENUE_DAY", OracleDbType.Varchar2)
                        ,new OracleParameter("REVENUE_FIX", OracleDbType.Varchar2)
	                    ,new OracleParameter("HPRES_KIND", OracleDbType.Varchar2)
	                    ,new OracleParameter("THOUR_START", OracleDbType.Varchar2)
                        ,new OracleParameter("THOUR_END", OracleDbType.Varchar2)
                        ,new OracleParameter("NHOUR_START", OracleDbType.Varchar2)
                        ,new OracleParameter("NHOUR_END", OracleDbType.Varchar2)
                        ,new OracleParameter("JUMAL_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("GAJUNG_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("BGAJUNG_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("SPECIAL_USED", OracleDbType.Varchar2)
                        ,new OracleParameter("N1", OracleDbType.Varchar2)
                        ,new OracleParameter("LC", OracleDbType.Varchar2)
                        ,new OracleParameter("ICF", OracleDbType.Varchar2)
                        ,new OracleParameter("SPECIAL_USED_PERCENT", OracleDbType.Varchar2)
                        ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["SUPPLIED_DAY"];
            parameters[1].Value = parameter["REVENUE_DAY"];
            parameters[2].Value = parameter["REVENUE_FIX"];
            parameters[3].Value = parameter["HPRES_KIND"];
            parameters[4].Value = parameter["THOUR_START"];
            parameters[5].Value = parameter["THOUR_END"];
            parameters[6].Value = parameter["NHOUR_START"];
            parameters[7].Value = parameter["NHOUR_END"];
            parameters[8].Value = parameter["JUMAL_USED"];
            parameters[9].Value = parameter["GAJUNG_USED"];
            parameters[10].Value = parameter["BGAJUNG_USED"];
            parameters[11].Value = parameter["SPECIAL_USED"];
            parameters[12].Value = parameter["N1"];
            parameters[13].Value = parameter["LC"];
            parameters[14].Value = parameter["ICF"];
            parameters[15].Value = parameter["SPECIAL_USED_PERCENT"];
            parameters[16].Value = parameter["LOC_CODE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
