﻿using System.Text;
using WaterNet.WaterNetCore;
using System.Data;

namespace WaterNet.WV_Common.dao
{
    public class CodeDao: BaseDao
    {
        private static CodeDao dao = null;

        private CodeDao() {} 
        public static CodeDao GetInstance()
        {
            if (dao == null)
            {
                dao = new CodeDao();
            }
            return dao;
        } 

        //해당계층조회
        public DataSet SelectCodeList(OracleDBManager manager, string parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select code                                                                     ");
            query.AppendLine("      ,code_name                                                                ");
            query.AppendLine("  from cm_code                                                                  ");
            query.AppendLine(" where 1 = 1                                                                    ");
            query.Append("   and pcode = '").Append(parameter).AppendLine("'");
            query.AppendLine("   and use_yn = 'Y'                                                             ");
            //query.AppendLine("   and display_yn = 'Y'                                                         ");
            query.AppendLine(" order by to_number(orderby)                                                    ");

            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        //태그정보조회
        public DataSet SelectTagList(OracleDBManager manager, string dataMember)
        {
            dataMember = "TAG";
            StringBuilder query = new StringBuilder();

            query.AppendLine("select distinct b.loc_code loc_code        ");
            query.AppendLine("     , b.loc_name loc_name                 ");
            query.AppendLine("     , b.tagname tag1                      ");
            query.AppendLine("     , b.description desc1                 ");
            query.AppendLine("     , a.ctagname tag2                     ");
            query.AppendLine("     , c.description desc2                 ");
            query.AppendLine("     , decode(a.cal_gbn, 'P', '+', 'M', '-') cal_gbn                   ");
            query.AppendLine("     , b.tag_gbn tag_gbn                   ");
            query.AppendLine("  from if_tag_cal a                        ");
            query.AppendLine("     , (                                   ");
            query.AppendLine("        select a.loc_code                  ");
            query.AppendLine("             , a.loc_name                  ");
            query.AppendLine("             , b.TAGNAME                   ");
            query.AppendLine("             , c.tag_gbn                   ");
            query.AppendLine("             , b.description               ");
            query.AppendLine("          from cm_location2 a               ");
            query.AppendLine("             , if_ihtags b                 ");
            query.AppendLine("             , if_tag_gbn c                ");
            query.AppendLine("         where a.loc_code = b.loc_code2     ");
            query.AppendLine("           and b.tagname = c.TAGNAME       ");
            query.AppendLine("       ) b                                 ");
            query.AppendLine("     , if_ihtags c                         ");
            query.AppendLine(" where a.TAGNAME(+) = b.TAGNAME            ");
            query.AppendLine("   and a.CTAGNAME = c.tagname(+)           ");
            query.AppendLine(" order by cal_gbn, tag2 desc                   ");

            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }
    }
}
