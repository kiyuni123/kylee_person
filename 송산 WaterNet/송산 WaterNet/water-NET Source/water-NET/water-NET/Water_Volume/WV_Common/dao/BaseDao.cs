﻿using System;
using System.Text;
using System.Diagnostics;

namespace WaterNet.WV_Common.dao
{
    public class BaseDao
    {
        #region Public Instance Constructors
        public BaseDao()
        {
        } 
        #endregion


        [Conditional("DEBUG")]
        protected void println(StringBuilder str)
        {
            Console.WriteLine(str.ToString());
        }

        [Conditional("DEBUG")]
        protected void println(string str)
        {
            Console.WriteLine(str);
        }
    }
}
