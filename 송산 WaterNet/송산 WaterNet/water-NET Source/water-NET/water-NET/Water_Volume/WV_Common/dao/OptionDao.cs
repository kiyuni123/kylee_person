﻿using System.Text;
using WaterNet.WaterNetCore;
using System.Data;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_Common.dao
{
    public class OptionDao: BaseDao
    {
        private static OptionDao dao = null;
        private OptionDao() { }
        public static OptionDao GetInstance()
        {
            if (dao == null)
            {
                dao = new OptionDao();
            }
            return dao;
        }

        public object GetSuppliedDay(OracleDBManager manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select supplied_day                                                              ");
            query.AppendLine("  from																		   ");
            query.AppendLine("      (																		   ");
            query.AppendLine("      select loc_code															   ");
            query.AppendLine("        from cm_location2														   ");
            query.AppendLine("       where ftr_code = 'BZ001'												   ");
            query.AppendLine("       start with loc_code = :LOC_CODE										   ");
            query.AppendLine("       connect by loc_code = prior ploc_code									   ");
            query.AppendLine("      ) a																		   ");
            query.AppendLine("     ,wv_block_default_option b												   ");
            query.AppendLine(" where b.loc_code = a.loc_code												   ");

            IDataParameter[] parameters =  {
                          new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object GetRevenueDay(OracleDBManager manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select revenue_day                                                              ");
            query.AppendLine("  from																		   ");
            query.AppendLine("      (																		   ");
            query.AppendLine("      select loc_code															   ");
            query.AppendLine("        from cm_location2														   ");
            query.AppendLine("       where ftr_code = 'BZ001'												   ");
            query.AppendLine("       start with loc_code = :LOC_CODE										   ");
            query.AppendLine("       connect by loc_code = prior ploc_code									   ");
            query.AppendLine("      ) a																		   ");
            query.AppendLine("     ,wv_block_default_option b												   ");
            query.AppendLine(" where b.loc_code = a.loc_code												   ");

            IDataParameter[] parameters =  {
                          new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object GetRevenueFix(OracleDBManager manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select revenue_fix                                                               ");
            query.AppendLine("  from																		   ");
            query.AppendLine("      (																		   ");
            query.AppendLine("      select loc_code															   ");
            query.AppendLine("        from cm_location2														   ");
            query.AppendLine("       where ftr_code = 'BZ001'												   ");
            query.AppendLine("       start with loc_code = :LOC_CODE										   ");
            query.AppendLine("       connect by loc_code = prior ploc_code									   ");
            query.AppendLine("      ) a																		   ");
            query.AppendLine("     ,wv_block_default_option b												   ");
            query.AppendLine(" where b.loc_code = a.loc_code												   ");

            IDataParameter[] parameters =  {
                          new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }
    }
}
