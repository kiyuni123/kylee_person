﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace WaterNet.WV_Common.excel
{
    public static class Clipboard
    {
        private readonly static string CF_LINKSOURCE_ID = "Link Source";

        public static string[,] GetStringRange()
        {
            Range range = GetRange();
            if (range == null)
            {
                return null;
            }

            int row_range_count = 0;
            int cell_range_count = 0;

            foreach (Range row_range in range.Rows)
            {
                cell_range_count = 0;
                foreach (Range cell_range in row_range.Cells)
                {
                    cell_range_count++;
                }
                row_range_count++;
            }

            string[,] result = new string[row_range_count, cell_range_count];

            int row_idx = 0;
            int cell_idx = 0;

            foreach (Range row in range.Rows)
            {
                cell_idx = 0;
                foreach (Range cell in row.Cells)
                {
                    if (cell.Value2 != null)
                    {
                        result[row_idx, cell_idx] = cell.Value2.ToString();
                    }
                    else
                    {
                        result[row_idx, cell_idx] = null;
                    }
                    cell_idx++;
                }

                row_idx++;
            }

            return result;
        }

        public static Range GetRange()
        {
            return GetRange(GetIDataObjectFromClipboard());
        }

        private static IDataObject GetIDataObjectFromClipboard()
        {
            if (System.Windows.Forms.Clipboard.ContainsData(CF_LINKSOURCE_ID))
                return System.Windows.Forms.Clipboard.GetDataObject() as IDataObject;
            else
            {
                return null;
            }
        }

        public static Range GetRange(IDataObject dataObject)
        {
            if (dataObject == null)
            {
                return null;
            }

            IStream iStream = IStreamFromDataObject(dataObject);
            IMoniker compositeMoniker = IMonikerFromIStream(iStream);
            return RangeFromCompositeMoniker(compositeMoniker);
        }

        private static IStream IStreamFromDataObject(IDataObject dataObject)
        {
            if (dataObject == null)
                throw new ArgumentNullException("dataObject", "dataObject is null.");

            STGMEDIUM medium;
            FORMATETC formatEtc = new FORMATETC();
            formatEtc.cfFormat = (short)System.Windows.Forms.DataFormats.GetFormat(CF_LINKSOURCE_ID).Id;
            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
            formatEtc.lindex = -1;
            formatEtc.ptd = new IntPtr(0);
            formatEtc.tymed = TYMED.TYMED_ISTREAM;

            dataObject.GetData(ref formatEtc, out medium);
            return Marshal.GetObjectForIUnknown(medium.unionmember) as IStream;
        }

        private static IMoniker IMonikerFromIStream(IStream iStream)
        {
            if (iStream == null)
                throw new ArgumentNullException("iStream", "iStream is null.");

            iStream.Seek(0, 0, IntPtr.Zero);
            Guid guid = Marshal.GenerateGuidForType(typeof(stdole.IUnknown));
            object obj;
            if (ole32.OleLoadFromStream(iStream, ref guid, out obj))
                return obj as IMoniker;
            else
                return null;
        }

        private static Range RangeFromCompositeMoniker(IMoniker compositeMoniker)
        {
            List<IMoniker> monikers = SplitCompositeMoniker(compositeMoniker);
            if (monikers.Count != 2)
                throw new ApplicationException("Invalid moniker");

            IBindCtx bindctx;
            if (!ole32.CreateBindCtx(0, out bindctx) || bindctx == null)
                throw new ApplicationException("Can't create bindctx");

            object obj;
            Guid workbookGuid = Marshal.GenerateGuidForType(typeof(Workbook));
            monikers[0].BindToObject(bindctx, null, ref workbookGuid, out obj);
            Workbook workbook = obj as Workbook;

            ExcelItemMonikerHelper helper = new ExcelItemMonikerHelper(monikers[1], bindctx);
            return helper.GetRange(workbook);
        }

        private static List<IMoniker> SplitCompositeMoniker(IMoniker compositeMoniker)
        {
            if (compositeMoniker == null)
                throw new ArgumentNullException("compositeMoniker", "compositeMoniker is null.");

            List<IMoniker> monikerList = new List<IMoniker>();
            IEnumMoniker enumMoniker;
            compositeMoniker.Enum(true, out enumMoniker);
            if (enumMoniker != null)
            {
                IMoniker[] monikerArray = new IMoniker[1];
                IntPtr fetched = new IntPtr();
                HRESULT res;
                while (res = enumMoniker.Next(1, monikerArray, fetched))
                {
                    monikerList.Add(monikerArray[0]);
                }
                return monikerList;
            }
            else
                throw new ApplicationException("IMoniker is not composite");
        }

        private class ExcelItemMonikerHelper
        {
            private enum RangeType
            {
                Cells,
                Rows,
                Columns
            }

            private XlReferenceStyle _referenceStyle;
            private RangeType _rangeType;
            private readonly string _displayName;

            private string _sheetName;
            private string _bound1Name;
            private string _bound2Name;

            public ExcelItemMonikerHelper(IMoniker excelItemMoniker, IBindCtx bindCtx)
            {
                if (excelItemMoniker == null)
                    throw new ArgumentNullException("excelItemMoniker", "excelItemMoniker is null.");

                excelItemMoniker.GetDisplayName(bindCtx, null, out _displayName);
            }

            public Range GetRange(Workbook workbook)
            {
                Parse();
                Worksheet sheet = GetSheet(workbook);
                _referenceStyle = sheet.Application.ReferenceStyle;
                Range range = null;
                switch (_rangeType)
                {
                    case RangeType.Cells:
                        range = sheet.get_Range(GetBound1Name(), GetBound2Name());
                        break;
                    case RangeType.Rows:
                        range = sheet.get_Range(sheet.Rows[GetBound1Name(), Type.Missing], sheet.Rows[GetBound2Name(), Type.Missing]);
                        break;
                    case RangeType.Columns:
                        range = sheet.get_Range(sheet.Columns[GetBound1Name(), Type.Missing], sheet.Columns[GetBound2Name(), Type.Missing]);
                        break;
                    default:
                        throw new ApplicationException("Illegal RangeType");
                }
                return range;
            }

            private void Parse()
            {
                string[] names = _displayName.Split('!');
                Debug.Assert(names.Length >= 2);
                _sheetName = names[1];

                string[] R1C1Bounds = names[2].Split(':');
                _bound1Name = R1C1Bounds[0];
                _bound2Name = R1C1Bounds.Length == 2 ? R1C1Bounds[1] : _bound1Name;

                if (_bound1Name.Contains("R") && _bound1Name.Contains("C"))
                    _rangeType = RangeType.Cells;
                else if (_bound1Name.Contains("R"))
                    _rangeType = RangeType.Rows;
                else if (_bound1Name.Contains("C"))
                    _rangeType = RangeType.Columns;
            }

            private Worksheet GetSheet(Workbook workbook)
            {
                return (Worksheet)workbook.Sheets[GetSheetName()];
            }

            private string GetSheetName()
            {
                return _sheetName;
            }

            private string GetBound1Name()
            {
                switch (_referenceStyle)
                {
                    case XlReferenceStyle.xlA1:
                        return RangeName.R1C1ToA1(_bound1Name);
                    case XlReferenceStyle.xlR1C1:
                        return _bound1Name;
                    default:
                        throw new ApplicationException("Illegal XlReferenceStyle");
                }
            }

            private object GetBound2Name()
            {
                switch (_referenceStyle)
                {
                    case XlReferenceStyle.xlA1:
                        return RangeName.R1C1ToA1(_bound2Name);
                    case XlReferenceStyle.xlR1C1:
                        return _bound2Name;
                    default:
                        throw new ApplicationException("Illegal XlReferenceStyle");
                }
            }
        }
    }


    public class RangeName
    {
        public static string GetColumnName(int columnNumber)
        {
            if (columnNumber <= 0)
                throw new ArgumentOutOfRangeException("columnNumber");

            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        public static int GetColumnNumber(string columnName)
        {
            int columnNumber = 0;
            int pow = 1;
            for (int i = columnName.Length - 1; i >= 0; i--)
            {
                columnNumber += (columnName[i] - 'A' + 1) * pow;
                pow *= 26;
            }

            return columnNumber;
        }

        public static string R1C1ToA1(string R1C1Name)
        {
            string[] indices = R1C1Name.Split('R', 'C');
            if (indices.Length == 3)
            {
                int columnNumber = int.Parse(indices[2]);
                return GetColumnName(columnNumber) + indices[1];
            }
            else if (indices.Length == 2 && R1C1Name.Contains("C"))
            {
                int columnNumber = int.Parse(indices[1]);
                return GetColumnName(columnNumber);
            }
            else if (indices.Length == 2 && R1C1Name.Contains("R"))
            {
                return indices[1];
            }
            else
                throw new ApplicationException("Can't parse R1C1Name");
        }

        public static string A1ToR1C1(string A1Name)
        {
            string[] indices = Regex.Split(A1Name, @"(\D+)");
            if (indices.Length == 2)
            {
                return "R" + indices[1] + "C" + GetColumnNumber(indices[0]).ToString();
            }
            else if (indices.Length == 1 && char.IsDigit(indices[0][0]))
            {
                return "R" + indices[0];
            }
            else if (indices.Length == 1 && char.IsLetter(indices[0][0]))
            {
                return "C" + GetColumnNumber(indices[0]).ToString();
            }
            else
                throw new ApplicationException("Can't parse A1Name");
        }
    }

    public class ole32
    {
        [DllImport("ole32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
        public static extern HRESULT OleLoadFromStream(
            IStream pStm,
            [In] ref Guid riid,
            [MarshalAs(UnmanagedType.IUnknown)] out object ppvObj);

        [DllImport("ole32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
        public static extern HRESULT CreateBindCtx(
            uint reserved,
            [Out, MarshalAs(UnmanagedType.Interface)] out IBindCtx pctx);
    }

}
