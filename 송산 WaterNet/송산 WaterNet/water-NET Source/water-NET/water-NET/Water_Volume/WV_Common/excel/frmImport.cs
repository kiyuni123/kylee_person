﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.utils;
using System.Collections;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;

namespace WaterNet.WV_Common.excel
{
	public partial class frmImport: Form
	{
        private UltraGridManager gridManager = new UltraGridManager();
        
        private ExcelImport import = null;
        public ExcelImport UpdateExcel
        {
            set
            {
                this.import = value;
            }
        }

        private List<ImportItem> items = new List<ImportItem>();
        public List<ImportItem> Items
        {
            get
            {
                return this.items;
            }
        }

        private Dictionary<string, ValueList> valueList = new Dictionary<string, ValueList>();

		public frmImport()
		{
			InitializeComponent();
            this.Load += new EventHandler(frmImport_Load);
		}

        private void frmImport_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeValueList();
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.columnBtn.Click += new EventHandler(columnBtn_Click);
            this.closeBtn.Click += new EventHandler(closeBtn_Click); 

            //파일선택
            this.fileBtn.Click += new EventHandler(fileBtn_Click);
            this.openFileDialog1.FileOk += new CancelEventHandler(openFileDialog1_FileOk);
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            DataTable data = new DataTable();
            foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            {
                if (this.ultraGrid1.Rows.Count != 0)
                {
                    foreach (ImportItem item in this.items)
                    {
                        if (item.getColumn() == column.Key)
                        {
                            data.Columns.Add(item.getColumn());
                            break;
                        }
                    }
                }
            }

            if (this.ultraGrid1.Rows.Count != 0)
            {
                foreach (UltraGridRow row in this.ultraGrid2.Rows)
                {
                    DataRow new_row = data.NewRow();
                    foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
                    {
                        if (this.ultraGrid2.DisplayLayout.Bands[0].Columns.IndexOf(this.ultraGrid1.Rows[0].Cells[column.Key].Value.ToString()) != -1)
                        {
                            new_row[column.Key] = row.Cells[this.ultraGrid1.Rows[0].Cells[column.Key].Value.ToString()].Value;
                        }
                    }

                    data.Rows.Add(new_row);
                }
            }

            foreach (UltraGridRow row in this.ultraGrid2.Rows)
            {
                foreach (UltraGridColumn column in this.ultraGrid2.DisplayLayout.Bands[0].Columns)
                {
                    if (row.Cells[column.Key].Appearance.BackColor == Color.Chocolate)
                    {
                        MessageBox.Show("저장할 수 없습니다.\n데이터를 다시 확인해주세요.");
                        return;
                    }
                }
            }

            if (this.import != null)
            {
                try
                {
                    this.import.update(data);
                    MessageBox.Show("정상적으로 처리되었습니다.");
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return;
                }

            }
        }

        private void columnBtn_Click(object sender, EventArgs e)
        {
            //컬럼에 데이터 타입이 아닌 경우는 오류처리
            foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            {
                if (this.ultraGrid1.Rows.Count != 0)
                {
                    ImportItem importItem = null;
                    foreach (ImportItem item in this.items)
                    {
                        if (item.getColumn() == column.Key)
                        {
                            importItem = item;
                            break;
                        }
                    }

                    if (importItem != null)
                    {
                        string columnKey = this.ultraGrid1.Rows[0].Cells[column.Key].Value.ToString();
                        foreach (UltraGridRow row in this.ultraGrid2.Rows)
                        {
                            if (columnKey == "")
                            {
                                break;
                            }

                            object value = row.Cells[columnKey].Value;

                            if (importItem.getCheckNull())
                            {
                                if (value == null || value.ToString() == "")
                                {
                                    row.Cells[columnKey].Appearance.BackColor = Color.Chocolate;
                                    row.Cells[columnKey].ToolTipText = "내용을 입력해주세요.";
                                }
                                else
                                {
                                    row.Cells[columnKey].Appearance.BackColor = Color.Empty;
                                }
                            }

                            if (importItem.getType() == typeof(double))
                            {
                                double checkValue = 0;
                                if (!Double.TryParse(value.ToString(), out checkValue))
                                {
                                    row.Cells[columnKey].Appearance.BackColor = Color.Chocolate;
                                    row.Cells[columnKey].ToolTipText = "숫자를 입력해주세요.";
                                }
                                else
                                {
                                    row.Cells[columnKey].Appearance.BackColor = Color.Empty;
                                }
                            }

                            //if (importItem.getType() == typeof(DateTime))
                            //{
                            //    DateTime checkValue;
                            //    if (!DateTime.TryParse(value.ToString(), out checkValue))
                            //    {
                            //        row.Cells[columnKey].Appearance.BackColor = Color.Chocolate;
                            //        row.Cells[columnKey].ToolTipText = "년월일을 입력해주세요.\n예) 19990101";
                            //    }
                            //    else
                            //    {
                            //        row.Cells[columnKey].Appearance.BackColor = Color.Empty;
                            //    }
                            //}
                        }

                        if (this.valueList.ContainsKey(importItem.getColumn()))
                        {
                            if (this.ultraGrid2.DisplayLayout.Bands[0].Columns.IndexOf(columnKey) != -1)
                            {
                                this.ultraGrid2.DisplayLayout.Bands[0].Columns[columnKey].ValueList = this.valueList[importItem.getColumn()];

                                foreach (UltraGridRow row in this.ultraGrid2.Rows)
                                {
                                    bool hasValueItem = false;
                                    foreach (ValueListItem item in this.valueList[importItem.getColumn()].ValueListItems)
                                    {
                                        if (row.Cells[columnKey].Value != null && item.DisplayText == row.Cells[columnKey].Value.ToString())
                                        {
                                            row.Cells[columnKey].Value = item.DataValue;
                                        }
                                        if (row.Cells[columnKey].Value != null && item.DataValue.ToString() == row.Cells[columnKey].Value.ToString())
                                        {
                                            row.Cells[columnKey].Value = item.DataValue;
                                            hasValueItem = true;
                                            break;
                                        }

                                    }
                                    if (!hasValueItem)
                                    {
                                        row.Cells[columnKey].Appearance.BackColor = Color.Chocolate;
                                        row.Cells[columnKey].ToolTipText = "내용을 선택해 주세요.";
                                    }
                                    else
                                    {
                                        row.Cells[columnKey].Appearance.BackColor = Color.Empty;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void InitializeGrid()
        {
            //그리드 세팅
            this.ultraGrid1.DisplayLayout.Bands[0].CardView = true;
            this.ultraGrid1.DisplayLayout.Bands[0].CardSettings.ShowCaption = false;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

            this.ultraGrid1.DisplayLayout.Bands[0].CardSettings.AutoFit = true;
            this.ultraGrid1.DisplayLayout.Bands[0].CardSettings.Style = CardStyle.StandardLabels;

            this.gridManager.Add(this.ultraGrid2);
            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid2.DisplayLayout.Override.CellClickAction = CellClickAction.Edit;
        }

        private void InitializeValueList()
        {
            ValueList valueList = null;

            valueList = new ValueList();
            Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            this.valueList["LOC_CODE"] = valueList;

            valueList = new ValueList();
            Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2001");
            this.valueList["LEK_CDE"] = valueList;

            valueList = new ValueList();
            Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2014");
            this.valueList["PIP_MOP"] = valueList;

            valueList = new ValueList();
            Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2015");
            this.valueList["LRS_CDE"] = valueList;

            valueList = new ValueList();
            Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2016");
            this.valueList["LEP_CDE"] = valueList;
        }

        /// <summary>
        /// 파일선택 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileBtn_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
            this.afterExcelFileOpen();
        }

        /// <summary>
        /// 파일을 선택한후에 이벤트 처리
        /// 1. 엑셀파일 여부 체크
        /// 2. 엑셀데이터를 데이터 테이블로 변환
        /// 3. 그리드 데이터 소스 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            int type = -1;

            try
            {
                type = LiAsExcelDB.ExcelFileType(this.openFileDialog1.FileName);
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
                return;
            }

            DataSet DS = null;

            // 0 : 97~2003,  1: 2007이상.
            if (type == 0 || type == 1)
            {
                DS = LiAsExcelDB.OpenExcel(this.openFileDialog1.FileName, false);
            }

            this.ultraGrid2.DataSource = DS.Tables[0];
        }

        private void afterExcelFileOpen()
        {
            this.setColumnMappingData();
            this.setColumnMappingDataValueList();
        }

        private void setColumnMappingData()
        {
            DataTable data = new DataTable();
            data.Rows.Add(data.NewRow());
            foreach (ImportItem item in this.items)
            {
                DataColumn column = data.Columns.Add(item.getColumn());
                column.Caption = item.getCaption();
            }

            this.ultraGrid1.DataSource = data;
        }

        private void setColumnMappingDataValueList()
        {
            ValueList valueList = new ValueList();
            foreach (UltraGridColumn column in this.ultraGrid2.DisplayLayout.Bands[0].Columns)
            {
                valueList.ValueListItems.Add(column.Key, column.Header.Caption);
            }

            foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            {
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.ValueList = valueList;

                if (column.ValueList.ItemCount != 0)
                {
                    if (column.ValueList.ItemCount > column.Index)
                    {
                        this.ultraGrid1.Rows[0].Cells[column.Key].Value = column.ValueList.GetValue(column.Index);
                    }
                }
            }
        }
	}
}
