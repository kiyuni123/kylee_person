﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WV_Common.excel
{
    public class ImportItem
    {
        private string column = string.Empty;
        private string caption = string.Empty;
        private Type type = null;
        private bool checkNull = false;

        public ImportItem(string column, string caption, Type type, bool checkNull)
        {
            this.column = column;
            this.caption = caption;
            this.type = type;
            this.checkNull = checkNull;
        }

        public string getColumn()
        {
            return this.column;
        }

        public string getCaption()
        {
            return this.caption;
        }

        public Type getType()
        {
            return this.type;
        }

        public bool getCheckNull()
        {
            return this.checkNull;
        }
    }
}
