﻿using System;
using System.Linq;
using System.IO;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;
using Infragistics.Excel;
using Infragistics.Win.UltraWinGrid.ExcelExport;
using System.Drawing;
using ChartFX.WinForms;

namespace WaterNet.WV_Common.util
{
    public class ExcelUtil
    {
        public static Workbook ExcelLoad(Stream stream)
        {
            return Workbook.Load(stream);
        }

        public static Workbook ExcelLoad()
        {
            return new Workbook();
        }

        public static Workbook Create(UltraGrid ultraGrid)
        {
            UltraGridExcelExporter exporter = new UltraGridExcelExporter();
            return exporter.Export(ultraGrid);
        }

        public static void Create(Chart chart, Workbook workbook, string worksheetName, int worksheetIndex)
        {
            Worksheet worksheet = null;

            //workbook 내에 worksheet의 인덱스 존재 유무 검사
            foreach (Worksheet sheet in workbook.Worksheets)
            {
                if (workbook.Worksheets.IndexOf(sheet) == worksheetIndex)
                {
                    worksheet = sheet;
                }
            }

            //worksheet 없으면 새로 만들어준다
            if (worksheet == null)
            {
                worksheet = workbook.Worksheets.Add(worksheetName);
            }

            if (File.Exists("tempChart.bmp"))
            {
                File.Delete("tempChart.bmp");
            }

            chart.Export(FileFormat.Bitmap, "tempChart.bmp");

            Stream stream = File.OpenRead("tempChart.bmp");

            Image image = new Bitmap(stream);

            WorksheetShape shape = new WorksheetImage(image);
            shape.TopLeftCornerCell = worksheet.Rows[0].Cells[0];
            shape.BottomRightCornerCell = worksheet.Rows[10].Cells[20];
            worksheet.Shapes.Add(shape);
        }

        public static void Create(UltraGrid ultraGrid, Workbook workbook, string worksheetName, int worksheetIndex)
        {
            Worksheet worksheet = null;

            //workbook 내에 worksheet의 인덱스 존재 유무 검사
            foreach (Worksheet sheet in workbook.Worksheets)
            {
                if (workbook.Worksheets.IndexOf(sheet) == worksheetIndex)
                {
                    worksheet = sheet;
                }
            }

            //worksheet 없으면 새로 만들어준다
            if (worksheet == null)
            {
                worksheet = workbook.Worksheets.Add(worksheetName);
            }

            //worksheet name
            worksheet.Name = worksheetName;

            UltraGridExcelExporter exporter = new UltraGridExcelExporter();
            exporter.Export(ultraGrid, worksheet);
        }

        public static void CreateAfterSave(UltraGrid ultraGrid, string strTitle, bool isAfterSaveOpen)
        {
            UltraGridExcelExporter exporter = new UltraGridExcelExporter();
            Workbook workbook = exporter.Export(ultraGrid);
            ExcelUtil.Save(workbook, strTitle, isAfterSaveOpen);
        }

        public static void CopyContent(Workbook templete, Workbook content, int startRow, int startColumn)
        {
            int row = 0;
            int column = 0;

            for (int i = 0; i < content.Worksheets[0].Rows.Count(); i++)
            {
                for (int j = 0; j < content.Worksheets[0].Rows[i].Cells.Count(); j++)
                {
                    row = i + startRow;
                    column = j + startColumn;

                    IWorksheetCellFormat cellFormat = content.Worksheets[0].Rows[i].Cells[j].CellFormat;

                    if (cellFormat.Indent == -1)
                    {
                        cellFormat.Indent = 0;
                    }

                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.Alignment = content.Worksheets[0].Rows[i].Cells[j].CellFormat.Alignment;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.BottomBorderColor = content.Worksheets[0].Rows[i].Cells[j].CellFormat.BottomBorderColor;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.BottomBorderStyle = content.Worksheets[0].Rows[i].Cells[j].CellFormat.BottomBorderStyle;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FillPattern = content.Worksheets[0].Rows[i].Cells[j].CellFormat.FillPattern;

                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FillPatternBackgroundColor = content.Worksheets[0].Rows[i].Cells[j].CellFormat.FillPatternBackgroundColor;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FillPatternForegroundColor = content.Worksheets[0].Rows[i].Cells[j].CellFormat.FillPatternForegroundColor;

                    //templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FormatString = content.Worksheets[0].Rows[i].Cells[j].CellFormat.FormatString;

                    if (i == 6)
                    {
                        templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FormatString = "###,###,###.0";
                    }
                    else
                    {
                        templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FormatString = "###,###,###";
                    }

                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.LeftBorderColor = content.Worksheets[0].Rows[i].Cells[j].CellFormat.LeftBorderColor;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.LeftBorderStyle = content.Worksheets[0].Rows[i].Cells[j].CellFormat.LeftBorderStyle;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.Locked = content.Worksheets[0].Rows[i].Cells[j].CellFormat.Locked;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.RightBorderColor = content.Worksheets[0].Rows[i].Cells[j].CellFormat.RightBorderColor;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.RightBorderStyle = content.Worksheets[0].Rows[i].Cells[j].CellFormat.RightBorderStyle;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.Rotation = content.Worksheets[0].Rows[i].Cells[j].CellFormat.Rotation;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.ShrinkToFit = content.Worksheets[0].Rows[i].Cells[j].CellFormat.ShrinkToFit;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.TopBorderColor = content.Worksheets[0].Rows[i].Cells[j].CellFormat.TopBorderColor;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.TopBorderStyle = content.Worksheets[0].Rows[i].Cells[j].CellFormat.TopBorderStyle;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.VerticalAlignment = content.Worksheets[0].Rows[i].Cells[j].CellFormat.VerticalAlignment;
                    templete.Worksheets[0].Rows[row].Cells[column].CellFormat.WrapText = content.Worksheets[0].Rows[i].Cells[j].CellFormat.WrapText;
                    templete.Worksheets[0].Rows[row].Cells[column].Value = content.Worksheets[0].Rows[i].Cells[j].Value;


                    if (templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FillPatternForegroundColor != Color.Empty)
                    {
                        templete.Worksheets[0].Rows[row].Cells[column].CellFormat.FillPatternForegroundColor = SystemColors.Control;
                    }
                }
            }

            for (int i = 0; i < content.Worksheets[0].MergedCellsRegions.Count; i++)
            {
                templete.Worksheets[0].MergedCellsRegions.Add(content.Worksheets[0].MergedCellsRegions[i].FirstRow + startRow,
                    content.Worksheets[0].MergedCellsRegions[i].FirstColumn + startColumn,
                    content.Worksheets[0].MergedCellsRegions[i].LastRow + startRow,
                    content.Worksheets[0].MergedCellsRegions[i].LastColumn + startColumn);
            }

        }

        public static void Add(UltraGrid ultraGrid, Workbook workbook, int startRow, int startColumn)
        {
            UltraGridExcelExporter exporter = new UltraGridExcelExporter();
            exporter.Export(ultraGrid, workbook, startRow, startColumn);
        }

        public static void Save(Workbook workbook, string strTitle, bool isAfterSaveOpen)
        {
            string fileName = string.Empty;

            try
            {
                fileName = ExcelUtil.Save(workbook, strTitle);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }

            if (isAfterSaveOpen)
            {
                ExcelUtil.Open(fileName);
            }
        }

        public static string Save(Workbook workbook, string strTitle)
        {
            string today = string.Empty;
            string strFullName = string.Empty;

            SaveFileDialog oSFD = new SaveFileDialog();
            today = DateTime.Now.ToString("yyyyMMdd");

            oSFD.Filter = "Excel files (*.xls)|*.xls";
            oSFD.FilterIndex = 1;
            oSFD.RestoreDirectory = true;
            oSFD.FileName = strTitle + "-" + today + "";

            if (oSFD.ShowDialog() == DialogResult.OK)
            {
                workbook.Save(oSFD.FileName);
            }

            return oSFD.FileName;
        }

        public static void Open(string fileName)
        {
            if (DialogResult.Yes == MessageBox.Show("엑셀파일을 열어보시겠습니까?", "내용보기", MessageBoxButtons.YesNo))
            {
                System.Diagnostics.Process oProc = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo oProcStartInfor = new System.Diagnostics.ProcessStartInfo("Excel.exe");

                oProcStartInfor.FileName = fileName;
                oProcStartInfor.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                oProc.StartInfo = oProcStartInfor;
                oProc.Start();
            }
        }
    }
}
