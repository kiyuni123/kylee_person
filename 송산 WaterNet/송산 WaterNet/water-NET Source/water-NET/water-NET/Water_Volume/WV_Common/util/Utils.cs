﻿using System;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.IO;
using Infragistics.Win.UltraWinEditors;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.work;

namespace WaterNet.WV_Common.util
{
    /// <summary>
    /// 공통으로 사용되는 유틸모음 클래스이다.
    /// </summary>
    public class Utils
    {
        public static DataTable ColumnMatch(UltraGrid ultraGrid, DataTable dataTable)
        {
            if (dataTable != null)
            {
                for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                {
                    if (ultraGrid.DisplayLayout.Bands[0].Columns.IndexOf(dataTable.Columns[i].ColumnName) == -1)
                    {
                        dataTable.Columns.Remove(dataTable.Columns[i].ColumnName);
                    }
                }
            }
            return dataTable;
        }

        public static bool HashOwnedForm(Form form, Type type)
        {
            bool result = false;
            foreach (Form oForm in form.OwnedForms)
            {
                if (oForm.GetType().Equals(type))
                {
                    oForm.WindowState = FormWindowState.Normal;
                    result = true;
                }
            }
            return result;
        }

        public static Hashtable ConverToHashtable(object target)
        {
            Hashtable parameter = null;

            if (target is UltraGridRow)
            {
                UltraGridRow row = target as UltraGridRow;
                parameter = new Hashtable();

                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.Value is DateTime)
                    {
                        parameter.Add(cell.Column.Key, Convert.ToDateTime(cell.Value).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        parameter.Add(cell.Column.Key, cell.Value);
                    }
                }
            }
            else if (target is Control)
            {
                Control control = target as Control;
                parameter = new Hashtable();
                AddHashtableToControls(parameter, control);
            }
            else if (target is DataRow)
            {
                DataRow row = target as DataRow;
                parameter = new Hashtable();

                foreach (DataColumn column in row.Table.Columns)
                {
                    if (row[column] is DateTime)
                    {
                        parameter.Add(column.ColumnName, Convert.ToDateTime(row[column]).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        parameter.Add(column.ColumnName, row[column]);
                    }
                }
            }

            return parameter;
        }

        public static void AddHashtableToControls(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    AddHashtableToControls(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();
                    object parameterValue = null;

                    if (control is TextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        parameterValue = combobox.SelectedValue;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        parameterValue = Convert.ToDateTime(date.Value).ToString("yyyyMMdd");

                        string strDate = parameterValue as string;

                        if (strDate == "00010101")
                        {
                            parameterValue = string.Empty;
                        }

                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is RichTextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is CheckBox)
                    {
                        CheckBox cb = control as CheckBox;
                        parameterValue = cb.Checked.ToString();
                        if (parameterValue == null)
                        {
                            parameterValue = "false";
                        }
                        parameter.Add(parameterName, parameterValue);
                    }
                }
            }
        }

        public static void ClearControls(Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    ClearControls(control);
                }
                else
                {
                    if (control is TextBox)
                    {
                        control.Text = string.Empty;
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (combobox.Items.Count > 0)
                        {
                            combobox.SelectedIndex = 0;
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        date.Value = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }

                    if (control is RichTextBox)
                    {
                        control.Text = string.Empty;
                    }
                }
            }
        }

        public static void SetControlDataSetting(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    SetControlDataSetting(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();

                    if (control is TextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (parameter.ContainsKey(parameterName))
                        {
                            combobox.SelectedValue = parameter[parameterName];
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        if (parameter.ContainsKey(parameterName))
                        {
                            date.Value = parameter[parameterName].ToString();
                        }
                    }

                    if (control is RichTextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is CheckBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            CheckBox cb = control as CheckBox;
                            cb.Checked = Convert.ToBoolean(parameter[parameterName]);
                        }
                    }
                }
            }
        }


        public static void SetValueList(object target, VALUELIST_TYPE type, string pcode)
        {
            //대상에 따라서 객체는 두가지로 나뉘어질수있음
            //1. 일반 콤보박스
            //2. 그리드의 ValueLit

            ValueList valueList = target as ValueList;
            ComboBox comboBox = null;

            if (valueList == null)
            {
                comboBox = target as ComboBox;

                if (comboBox == null)
                {
                    return;
                }

                comboBox.ValueMember = "CODE";
                comboBox.DisplayMember = "CODE_NAME";

                if (type == VALUELIST_TYPE.SINGLE_YEAR)
                {
                    comboBox.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
                }
                else if (type == VALUELIST_TYPE.SINGLE_MONTH)
                {
                    comboBox.DataSource = DateUtils.GetMonth("CODE", "CODE_NAME");
                }
                else if (type == VALUELIST_TYPE.CODE)
                {
                    if (pcode == null)
                    {
                        return;
                    }
                    comboBox.DataSource = CodeWork.GetInstance().SelectCodeList(pcode).Tables[0];
                }
            }

            else if (valueList != null)
            {
                DataTable table = null;

                if (type == VALUELIST_TYPE.SINGLE_YEAR)
                {
                    table = DateUtils.GetYear(10, "CODE", "CODE_NAME");

                    foreach (DataRow row in table.Rows)
                    {
                        valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                    }
                }
                else if (type == VALUELIST_TYPE.SINGLE_MONTH)
                {
                    table = DateUtils.GetMonth("CODE", "CODE_NAME");

                    foreach (DataRow row in table.Rows)
                    {
                        valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                    }
                }
                else if (type == VALUELIST_TYPE.SINGLE_DAY)
                {
                    table = DateUtils.GetDay("CODE", "CODE_NAME");

                    foreach (DataRow row in table.Rows)
                    {
                        valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                    }
                }
                else if (type == VALUELIST_TYPE.SINGLE_HOUR)
                {
                    table = DateUtils.GetHours("CODE", "CODE_NAME");

                    foreach (DataRow row in table.Rows)
                    {
                        valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                    }
                }
                else if (type == VALUELIST_TYPE.CODE)
                {
                    if (pcode == null)
                    {
                        return;
                    }
                    table = CodeWork.GetInstance().SelectCodeList(pcode).Tables[0];

                    foreach (DataRow row in table.Rows)
                    {
                        valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                    }
                }
                else if (type == VALUELIST_TYPE.CENTER)
                {

                }
                else if (type == VALUELIST_TYPE.PURIFICATION_PLANT)
                {

                }
                else if (type == VALUELIST_TYPE.LARGE_BLOCK)
                {
                    Hashtable parameter = new Hashtable();
                    parameter["FTR_CODE"] = "BZ001";

                    table = BlockWork.GetInstance().SelectBlockListByLevel(parameter, "RESULT").Tables[0];

                    foreach (DataRow row in table.Rows)
                    {
                        ValueListItem item = new ValueListItem();
                        item.DataValue = row["LOC_CODE"];
                        item.DisplayText = row["LOC_NAME"].ToString();
                        item.Tag = row["PLOC_CODE"];
                        valueList.ValueListItems.Add(item);
                    }
                }
                else if (type == VALUELIST_TYPE.MIDDLE_BLOCK)
                {
                    Hashtable parameter = new Hashtable();
                    parameter["FTR_CODE"] = "BZ002";

                    table = BlockWork.GetInstance().SelectBlockListByLevel(parameter, "RESULT").Tables[0];

                    foreach (DataRow row in table.Rows)
                    {
                        ValueListItem item = new ValueListItem();
                        item.DataValue = row["LOC_CODE"];
                        item.DisplayText = row["LOC_NAME"].ToString();
                        item.Tag = row["PLOC_CODE"];
                        valueList.ValueListItems.Add(item);
                    }
                }
                else if (type == VALUELIST_TYPE.SMALL_BLOCK)
                {
                    Hashtable parameter = new Hashtable();
                    parameter["FTR_CODE"] = "BZ003";

                    table = BlockWork.GetInstance().SelectBlockListByLevel(parameter, "RESULT").Tables[0];

                    foreach (DataRow row in table.Rows)
                    {
                        ValueListItem item = new ValueListItem();
                        item.DataValue = row["LOC_CODE"];
                        item.DisplayText = row["LOC_NAME"].ToString();
                        item.Tag = row["PLOC_CODE"];
                        valueList.ValueListItems.Add(item);
                    }
                }
            }
        }

        public static double ToDouble(object value)
        {
            double result = 0;

            try
            {
                if (value != DBNull.Value && !Double.IsInfinity(Convert.ToDouble(value)) && !Double.IsNaN(Convert.ToDouble(value)))
                {
                    result = Convert.ToDouble(value);
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }


        public static bool CanSupplieUpdate(string datee, string format, string loc_code)
        {
            bool result = false;

            DateTime dt = DateTime.ParseExact(GetSuppliedEnddate(datee, format, loc_code), "yyyyMMdd", null);

            if (DateTime.Now > dt)
            {
                result = true;
            }
            return result;
        }

        public static bool CanRevenueUpdate(string year_mon, string format, string loc_code)
        {
            bool result = false;
            int st = Convert.ToInt32(OptionWork.GetInstance().GetRevenueFix(loc_code));
            int st1 = 0;
            
            int lastDay = DateTime.ParseExact(year_mon + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;
            DateTime Months = DateTime.ParseExact(year_mon, "yyyyMM", null);

            if (lastDay <= st)
            {
                st1 = lastDay;
            }
            else
            {
                st1 = st;
            }

            if (DateTime.Now > Months.AddMonths(1).AddDays(st1 - 1))
            {
                result = true;
            }
            return result;
        }

        public static bool CanRevenueUpdate2(string datee, string format, string loc_code)
        {
            bool result = false;
            int st = Convert.ToInt32(OptionWork.GetInstance().GetRevenueFix(loc_code));

            DateTime dt = DateTime.ParseExact(datee, format, null);
            int lastDay = DateTime.ParseExact(datee.Substring(0, 6) + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;

            if (lastDay < st)
            {
                st = lastDay;
            }

            DateTime rt = DateTime.ParseExact(dt.ToString("yyyyMM") + st.ToString("00"), "yyyyMMdd", null);

            if (DateTime.Now > rt)
            {
                result = true;
            }
            return result;
        }

        public static string GetSuppliedMonth(string datee, string format, string loc_code)
        {
            int st = Convert.ToInt32(OptionWork.GetInstance().GetSuppliedDay(loc_code)) - 1;

            if (st == 0)
            {
                st = 31;
            }

            DateTime dt = DateTime.ParseExact(datee, format, null);
            int lastDay = DateTime.ParseExact(datee.Substring(0, 6) + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;

            if (lastDay <= st)
            {
                st = lastDay;
            }

            string year_mon = string.Empty;

            if (dt.Day < st - 1)
            {
                year_mon = dt.AddMonths(-1).ToString("yyyyMM");
            }

            if (dt.Day < st + 1)
            {
                year_mon = dt.ToString("yyyyMM");
            }

            if (dt.Day > st)
            {
                year_mon = dt.AddMonths(1).ToString("yyyyMM");
            }

            if (dt.Day == st)
            {
                if (st <= lastDay)
                {
                    year_mon = dt.ToString("yyyyMM");
                }
                else
                {
                    year_mon = dt.AddMonths(1).ToString("yyyyMM");
                }
            }

            return year_mon;
        }

        public static string GetSuppliedStartdate(string datee, string format, string loc_code)
        {
            int st = Convert.ToInt32(OptionWork.GetInstance().GetSuppliedDay(loc_code));
            int st1 = 0;

            string year_mon = GetSuppliedMonth(datee, format, loc_code);

            int lastDay = DateTime.ParseExact(year_mon + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;

            if (lastDay <= st)
            {
                st1 = lastDay;
            }
            else
            {
                st1 = st;
            }

            DateTime dt = DateTime.ParseExact(datee, format, null);

            DateTime rt;

            if (DateTime.ParseExact(year_mon + st1.ToString("00"), "yyyyMMdd", null) <= dt)
            {
                rt = DateTime.ParseExact(year_mon + st1.ToString("00"), "yyyyMMdd", null);
            }
            else
            {
                rt = DateTime.ParseExact(year_mon + st1.ToString("00"), "yyyyMMdd", null).AddMonths(-1);

                if (rt.ToString("yyyyMM") != year_mon && (st >= DateTime.ParseExact(rt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day))
                {
                    rt = DateTime.ParseExact(rt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1);
                }

                if (rt.ToString("yyyyMM") != year_mon && (st1 < DateTime.ParseExact(rt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day))
                {
                    rt = DateTime.ParseExact(rt.ToString("yyyyMM") + st.ToString("00"), "yyyyMMdd", null);
                }

            }
            return rt.ToString("yyyyMMdd");
        }

        public static string GetSuppliedEnddate(string datee, string format, string loc_code)
        {
            int st = Convert.ToInt32(OptionWork.GetInstance().GetSuppliedDay(loc_code)) - 1;

            if (st == 0)
            {
                st = 31;
            }

            DateTime dt = DateTime.ParseExact(GetSuppliedStartdate(datee, format, loc_code), "yyyyMMdd", null).AddMonths(1).AddDays(-1);
            int lastDay = DateTime.ParseExact(dt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;

            if (st == 31)
            {
                dt = DateTime.ParseExact(dt.ToString("yyyyMM") + lastDay.ToString("00"), "yyyyMMdd", null);
            }

            if (st <= lastDay)
            {
                dt = DateTime.ParseExact(dt.ToString("yyyyMM") + st.ToString("00"), "yyyyMMdd", null);
            }

            return dt.ToString("yyyyMMdd");
        }

        public static string GetTempFileName(string strTitle, FILE_TYPE type)
        {
            string filename = Path.GetTempFileName();
            string exp = string.Empty;

            if (type == FILE_TYPE.EXCEL)
            {
                exp = "xls";
            }

            if (type == FILE_TYPE.BITMAP)
            {
                exp = "bmp";
            }

            filename = filename.Substring(filename.LastIndexOf('\\')+1, filename.LastIndexOf('.') - filename.LastIndexOf('\\'));

            return Path.GetTempPath() + strTitle + "_" + filename + exp;
        }

        //=========================================2015-03-26 강현복 추가=========================================

        static Random pRandom = new Random();

        //combo box의 데이터에 전체 항목을 추가
        public static void AddAllRow(DataTable table)
        {
            DataRow row = table.NewRow();
            row["CODE"] = "";
            row["TEXT"] = "전체";

            table.Rows.InsertAt(row, 0);
        }

        //숫자인지 확인
        public static bool IsNumber(string value)
        {
            value = value.Replace("-", "");
            string check = value.Trim();

            bool returnVal = false;
            int dotCount = 0;
            for (int i = 0; i < check.Length; i++)
            {
                if (System.Char.IsNumber(check, i))
                    returnVal = true;
                else
                {
                    if (check.Substring(i, 1) == ".")
                    {
                        returnVal = true;
                        dotCount++;
                    }
                    else
                    {
                        returnVal = false;
                        break;
                    }
                }
            }

            if (dotCount > 1)
            {
                returnVal = false;
            }

            return returnVal;
        }

        //Hashtable의 내용을 Console에 출력
        public static void WriteHashtableToConsole(Hashtable data)
        {
            Console.WriteLine("===========write start===========");

            foreach (string key in data.Keys)
            {
                Console.WriteLine(key + " : " + data[key]);
            }

            Console.WriteLine("===========write end===========");
        }

        //grid에 행추가
        public static void AddGridRow(UltraGrid grid)
        {
            DataTable dataTable = grid.DataSource as DataTable;
            dataTable.Rows.Add(dataTable.NewRow());

            //grid.Rows[dataTable.Rows.Count - 1].Selected = true;
            //grid.Rows[dataTable.Rows.Count - 1].Activate();

            grid.Rows[grid.Rows.Count - 1].Selected = true;
            grid.Rows[grid.Rows.Count - 1].Activate();
        }

        //grid에 행추가 (초기값 존재)
        public static void AddGridRow(UltraGrid grid, Hashtable initValueSet)
        {
            DataTable dataTable = grid.DataSource as DataTable;
            DataRow newRow = dataTable.NewRow();

            foreach (string key in initValueSet.Keys)
            {
                newRow[key] = initValueSet[key].ToString();
            }

            dataTable.Rows.Add(newRow);

            grid.Rows[grid.Rows.Count - 1].Selected = true;
            grid.Rows[grid.Rows.Count - 1].Activate();
        }

        //grid에 행삭제
        public static Hashtable DeleteGridRow(UltraGrid grid, string message)
        {
            DataTable dt = grid.DataSource as DataTable;
            Hashtable deleteData = null;

            if (grid.Selected.Rows.Count != 0)
            {

                if (!"".Equals(message))
                {
                    if (MessageBox.Show(message, "데이터삭제", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    {
                        return null;
                    }
                }

                int idx = grid.Selected.Rows[0].Index;
                DataRow realDataRow = ((DataRowView)grid.Rows[idx].ListObject).Row;

                if (realDataRow.RowState != DataRowState.Added)
                {

                    deleteData = new Hashtable();

                    foreach (DataColumn column in dt.Columns)
                    {
                        deleteData.Add(column.ToString(), ((DataRowView)grid.Rows[idx].ListObject).Row[column.ToString()].ToString());
                    }
                }

                dt.Rows.RemoveAt(dt.Rows.IndexOf(realDataRow));

                if (dt.Rows.Count > 0)
                {
                    int newIdx = 0;

                    if (idx - 1 < 0)
                    {
                        newIdx = 0;
                    }
                    else
                    {
                        newIdx = idx - 1;
                    }

                    grid.Rows[newIdx].Selected = true;
                    grid.Rows[newIdx].Activate();
                }
            }

            return deleteData;
        }

        //grid data 저장 전 확인
        public static bool SaveGridData(UltraGrid grid, Hashtable validationSet)
        {
            if (grid.Rows.Count == 0)
            {
                return false;
            }

            bool result = false;

            if (!HaveChangedData(grid.DataSource as DataTable))
            {
                MessageBox.Show("변경된 데이터가 없습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //validation
                if (validationSet != null)
                {
                    if (!ValidationGridData(grid, validationSet))
                    {
                        return false;
                    }
                }

                if (MessageBox.Show("저장하시겠습니까?", "데이터저장", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    result = true;
                }
            }

            return result;
        }

        //grid data validation
        public static bool ValidationGridData(UltraGrid grid, Hashtable validationSet)
        {
            DataTable tmpDt = grid.DataSource as DataTable;

            foreach (DataRow row in tmpDt.Rows)
            {
                if (row.RowState == DataRowState.Unchanged)
                {
                    continue;
                }

                foreach (DataColumn column in tmpDt.Columns)
                {
                    if (validationSet[column.ToString()] != null)
                    {
                        string headerCaption = grid.DisplayLayout.Bands[0].Columns[column.ColumnName].Header.Caption;
                        string[] validationData = validationSet[column.ToString()].ToString().Split('|');

                        //사이즈 check
                        if (int.Parse(validationData[0]) < System.Text.Encoding.GetEncoding(949).GetByteCount((row[column.ToString()].ToString())))
                        {
                            MessageBox.Show("입력값이 너무 큽니다. \n" + headerCaption + " : " + row[column], "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }

                        //Null 허용 Check
                        if ("N".Equals(validationData[1].ToUpper()) && "".Equals(row[column].ToString()))
                        {
                            MessageBox.Show("[" + headerCaption + "] 항목에 빈값이 올 수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }

                        //숫자여부 확인
                        if ("Y".Equals(validationData[2].ToUpper()) && !IsNumber(row[column].ToString()))
                        {
                            MessageBox.Show("[" + headerCaption + "] 항목은 숫자만 입력가능합니다. \n" + headerCaption + " : " + row[column], "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        //저장 전 변경데이터 확인
        public static bool HaveChangedData(DataTable data)
        {
            bool result = false;

            foreach (DataRow row in data.Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                {
                    result = true;
                }
            }

            return result;
        }

        //그리드와 컨트롤의 값을 비교하여 값이 다를경우 컨트롤 텍스트 체인지 이벤트를 허용한다. : 2014-12-15_shpark
        public static void ControlTextChanged(UltraGrid grid, string key, string value)
        {
            if (grid.Selected.Rows.Count > 0)
            {
                if (!grid.Selected.Rows[0].Cells[key].Value.ToString().Equals(value))
                {
                    SetGridCellValue(grid, key, value);
                }
            }
        }

        //grid에서 선택된 행/컬럼의 값을 발췌
        public static string GetGridCellValue(UltraGrid grid, string columnName)
        {
            string result = "";

            if (grid.Selected.Rows.Count > 0)
            {
                result = grid.Selected.Rows[0].Cells[columnName].Value.ToString();
            }

            return result;
        }

        //해당 값을 grid의 값으로 할당
        public static void SetGridCellValue(UltraGrid grid, string columnName, string value)
        {
            if (grid.Selected.Rows.Count > 0)
            {
                grid.Selected.Rows[0].Cells[columnName].Value = value;
                grid.UpdateData();
            }
        }

        //grid의 선택 행 상태 반환
        public static DataRowState GetRowState(UltraGrid grid)
        {
            DataTable table = grid.DataSource as DataTable;
            return table.Rows[grid.Selected.Rows[0].Index].RowState;
        }

        //grid의 선택된 로우의 위치를 위 또는 아래로 한칸씩 이동    : 2015-02-04_shpark
        public static void MoveRowUpDown(UltraGrid grid, string upDown)
        {
            if (grid.Selected.Rows.Count == 0) return;

            UltraGridRow row = grid.ActiveRow;
            int rowIndex = grid.Selected.Rows[0].Index;

            if (upDown.Equals("UP"))
            {
                rowIndex = rowIndex == 0 ? 0 : rowIndex - 1;
            }
            else if (upDown.Equals("DOWN"))
            {
                rowIndex = rowIndex == grid.Rows.Count - 1 ? rowIndex : rowIndex + 1;
            }

            grid.Rows.Move(row, rowIndex);

            grid.Rows[rowIndex].Activate();
            grid.Rows[rowIndex].Selected = true;
        }

        //날짜 반환
        public static string StringToDateTime(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;
        }

        //Exception 처리
        public static void ShowExceptionMessage(string message)
        {
            MessageBox.Show(message, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Exception 처리 (Console 출력 포함)
        public static void ShowExceptionMessageWithConsole(Exception e)
        {
            Console.WriteLine(e);
            MessageBox.Show(e.Message, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //메세지 처리
        public static void ShowInformationMessage(string message)
        {
            MessageBox.Show(message, "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Yes/No Dialog Box 처리
        public static DialogResult ShowYesNoDialog(string message)
        {
            return MessageBox.Show(message, "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        //조회 후 grid 첫째행 선택
        public static void SelectFirstGridRow(UltraGrid grid)
        {
            if (grid.Rows.Count > 0)
            {
                grid.Rows[0].Selected = true;
                grid.Rows[0].Activate();
            }
        }

        //grid 특정행을 선택
        public static void SelectGridRow(UltraGrid grid, int idx)
        {
            if (grid.Rows.Count > 0)
            {
                grid.Rows[idx].Selected = true;
                grid.Rows[idx].Activate();
            }
        }

        //null을 공백문자로
        public static object NullToString(object obj)
        {
            if (obj == null)
            {
                return "";
            }
            else
            {
                return obj;
            }
        }

        //일련번호 생성
        public static String GetSerialNumber(string workFlag)
        {
            //static Random pRandom = new Random();

            string strRtn = string.Empty;
            string strWQSN = string.Empty;

            strWQSN = StringToDateTime(DateTime.Now)
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0');
            strWQSN += (pRandom.Next(1, 9).ToString()).PadLeft(1, '0');

            strRtn = workFlag + strWQSN;

            return strRtn;
        }

    }

    public class ComboBoxUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMemeber"></param>
        /// <param name="text"></param>
        /// <param name="interval"></param>
        public static void AddData(ComboBox target, string valueMember, string displayMemeber, string text, int interval)
        {
            SetMember(target, valueMember, displayMemeber);
            target.DataSource = DataUtils.GetRequstData(valueMember, displayMemeber, text, interval);
        }

        public static void AddDefaultOption(ComboBox target, string display, string value, bool isSelected)
        {
            if (target == null)
            {
                return;
            }

            DataTable table = target.DataSource as DataTable;

            if (table == null)
            {
                table = ((DataView)target.DataSource).Table;
            }

            DataRow row = table.NewRow();
            row[target.DisplayMember] = display;
            row[target.ValueMember] = value;
            table.Rows.InsertAt(row, 0);
            if (isSelected)
            {
                target.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 콤보박스 기본옵션 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="isSelected">옵션값 선택 / 전체 여부</param>
        public static void AddDefaultOption(ComboBox target, bool isSelected)
        {
            if (target == null)
            {
                return;
            }

            string text = isSelected ? "선택" : "전체";
            string value = isSelected ? "X" : "O";

            string displayMember = target.DisplayMember != null ? target.DisplayMember : "CODE_NAME";
            string valueMember = target.ValueMember != null ? target.ValueMember : "CODE";

            SetMember(target, valueMember, displayMember);

            DataTable table = target.DataSource as DataTable;

            if (table == null)
            {
                table = new DataTable();
                table.Columns.Add(displayMember);
                table.Columns.Add(valueMember);
                DataRow row = table.NewRow();
                row[displayMember] = text;
                row[valueMember] = value;
                table.Rows.InsertAt(row, 0);
                target.DataSource = table;
                target.SelectedIndex = 0;
            }
            else
            {
                if (table.Rows.Count != 0)
                {
                    if (table.Rows[0][valueMember].ToString() == "X" || table.Rows[0][valueMember].ToString() == "O")
                    {
                        return;
                    }
                }

                DataRow row = table.NewRow();
                row[displayMember] = text;
                row[valueMember] = value;
                table.Rows.InsertAt(row, 0);
                target.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 콤보박스 기본 멤버 설정 및 기본옵션 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="isSelected">옵션값 선택 / 전체 여부</param>
        /// <param name="valueMember">값 필드멤버명</param>
        /// <param name="displayMember">텍스트 필드멤버명</param>
        public static void AddDefaultOption(ComboBox target, bool isSelected, string valueMember, string displayMember)
        {
            SetMember(target, valueMember, displayMember);
            AddDefaultOption(target, isSelected);
        }

        /// <summary>
        /// 콤보박스 기본 멤버 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="valueMember">값 필드멤버명</param>
        /// <param name="displayMemeber">텍스트 필드멤버명</param>
        public static void SetMember(ComboBox target, string valueMember, string displayMemeber)
        {
            target.ValueMember = valueMember;
            target.DisplayMember = displayMemeber;
        }


        /// <summary>
        /// 콤보박스의 displayValue를 기준으로 같은 아이템을 선택해준다.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="displayValue"></param>
        public static void SelectItemByDisplayMember(ComboBox target, string displayMember)
        {
            DataTable Items = target.DataSource as DataTable;

            if (Items == null)
            {
                Items = ((DataView)target.DataSource).Table;
            }

            int index = 0;

            foreach (DataRow row in Items.Rows)
            {
                if (row[target.DisplayMember].ToString() == displayMember)
                {
                    break;
                }
                index++;
            }
            target.SelectedIndex = index;
        }


        public static void SelectItemByValueMember(ComboBox target, string valueMember)
        {
            DataTable Items = target.DataSource as DataTable;

            if (Items == null)
            {
                Items = ((DataView)target.DataSource).Table;
            }

            int index = 0;

            foreach (DataRow row in Items.Rows)
            {
                if (row[target.ValueMember].ToString() == valueMember)
                {
                    break;
                }
                index++;
            }
            target.SelectedIndex = index;
        }
    }

    public class DateUtils
    {
        /// <summary>
        /// 금년기준 요청 년도 기간으로 연산하여 반환
        /// </summary>
        /// <param name="interval">년도 간격</param>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetYear(int interval, string valueColumn, string textColumn)
        {
            DateTime today = DateTime.Now;

            DataTable years = new DataTable();
            years.Columns.Add(valueColumn);
            years.Columns.Add(textColumn);

            for (int i = today.Year; i >= today.AddYears(interval).Year; i--)
            {
                DataRow year = years.NewRow();
                year[valueColumn] = i.ToString();
                year[textColumn] = i.ToString()+"년";
                years.Rows.Add(year);
            }
            return years;
        }

        /// <summary>
        /// 1월 - 12월을 DataTable로 반환
        /// </summary>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetMonth(string valueColumn, string textColumn)
        {
            DataTable months = new DataTable();
            months.Columns.Add(valueColumn);
            months.Columns.Add(textColumn);
            for (int i = 1; i <= 12; i++)
            {
                DataRow month = months.NewRow();
                month[valueColumn] = i.ToString("00");
                month[textColumn] = i.ToString("00") + "월";
                months.Rows.Add(month);
            }
            return months;
        }
        /// <summary>
        /// 1일 31일 DataTable로 반환
        /// </summary>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetDay(string valueColumn, string textColumn)
        {
            DataTable day = new DataTable();
            day.Columns.Add(valueColumn);
            day.Columns.Add(textColumn);
            for (int i = 1; i <= 31; i++)
            {
                DataRow dataRow = day.NewRow();
                dataRow[valueColumn] = i.ToString();
                dataRow[textColumn] = "매월 "+ i.ToString() + " 일";
                day.Rows.Add(dataRow);
            }
            return day;
        }
        /// <summary>
        /// 1월 - 12월을 DataTable로 반환
        /// </summary>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetHours(string valueColumn, string textColumn)
        {
            DataTable hours = new DataTable();
            hours.Columns.Add(valueColumn);
            hours.Columns.Add(textColumn);
            for (int i = 0; i <= 23; i++)
            {
                DataRow hour = hours.NewRow();
                hour[valueColumn] = i.ToString("00");
                hour[textColumn] = i.ToString("00") + "시";
                hours.Rows.Add(hour);
            }
            return hours;
        }

        /// <summary>
        /// [year] + [division] + [month] + [dibision] + [day] 형식으로 문자열 반환
        /// </summary>
        /// <param name="division">구분값</param>
        /// <param name="add_year">연산 년</param>
        /// <param name="add_month">연산 월</param>
        /// <param name="add_day">연산 일</param>
        /// <returns></returns>
        public static string GetDate(char division, int add_year, int add_month, int add_day)
        {
            DateTime today = DateTime.Now;
            return today.AddYears(add_year).Year.ToString() + division + today.AddMonths(add_month).Month.ToString() + division + today.AddDays(add_day).Day.ToString();
        }

        /// <summary>
        /// [year] + [division] + [month] + [dibision] + [day] + [space] + [hour] + [:] + [minute] 형식으로 문자열 반환
        /// </summary>
        /// <param name="division">구분값</param>
        /// <param name="year">연산 년</param>
        /// <param name="month">연산 월</param>
        /// <param name="day">연산 일</param>
        /// <param name="time">세팅 시간</param>
        /// <returns></returns>
        public static string GetDateTime(char division, int add_year, int add_month, int add_day, int origin_time, int origin_minute)
        {
            DateTime today = DateTime.Now;
            return today.AddYears(add_year).Year.ToString() + division + today.AddMonths(add_month).Month.ToString() + division + today.AddDays(add_day).Day.ToString() + " " + origin_time.ToString("00") + ":" + origin_minute.ToString("00");
        }

        /// <summary>
        /// 포멧형식으로 날자를 반환
        /// </summary>
        /// <param name="target">날자 객체</param>
        /// <param name="format">포멧 (yyyymmdd, yyyymmddhhmi)</param>
        /// <returns></returns>
        public static string DateTimeToString(DateTime target, string format)
        {
            return target.ToString(format);
        }
    }

    public class DataUtils
    {
        public static DataTable DataTableWapperClone(DataTable target)
        {
            DataTable result = new DataTable();
            foreach (DataColumn column in target.Columns)
            {
                result.Columns.Add(column.ColumnName, column.DataType);
            }
            return result;
        }

        public static LOCATION_TYPE GetLocationType(string value)
        {
            object type = null;
            if (value == "BZ001")
            {
                type = LOCATION_TYPE.LARGE_BLOCK;
            }
            if (value == "BZ002")
            {
                type = LOCATION_TYPE.MIDDLE_BLOCK;
            }
            if (value == "BZ003")
            {
                type = LOCATION_TYPE.SMALL_BLOCK;
            }
            return (LOCATION_TYPE)type;
        }

        public static DataTable GetRequstData(string valueColumn, string textColumn, string text, int interval)
        {
            DataTable result = new DataTable();
            result.Columns.Add(valueColumn);
            result.Columns.Add(textColumn);
            for (int i = 1; i <= interval; i++)
            {
                DataRow line = result.NewRow();
                line[valueColumn] = i.ToString("00");
                line[textColumn] = i.ToString() + text;
                result.Rows.Add(line);
            }
            return result;
        }

        public static Hashtable ConvertDataRowViewToHashtable(DataRowView target)
        {
            DataColumnCollection columns = target.Row.Table.Columns;

            Hashtable result = new Hashtable();

            for (int i = 0; i < columns.Count; i++)
            {
                result[columns[i].ColumnName] = target[columns[i].ColumnName];
            }

            return result;
        }


        public static double NanToZero(double value)
        {
            if (Double.IsNaN(value))
            {
                return 0;
            }

            if (value.Equals(DBNull.Value))
            {
                value = 0;
            }
            return value;
        }


        public static object NullToZero(object value)
        {
            if (value == DBNull.Value)
            {
                return 0;
            }

            return value;
        }

        public static string StringToFormatString(string value, string format)
        {
            string result = string.Empty;

            if (value.Length == 8)
            {
                if (format == "yyyy-MM-dd")
                {
                    result = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);

                }

            }
            return result;
        }
    }

    public class ControlUtils
    {
        public static void SetEnabled(Control control, bool isEnabled)
        {
            if (isEnabled)
            {
                control.Enabled = true;
            }
            else
            {
                control.Enabled = false;
            }
        }

        /// <summary>
        /// UltraDateTimeEditor 포멧을 변경한다.
        /// </summary>
        /// <param name="timeEdit"></param>
        /// <param name="format"></param>
        //public static void SetTimeEditFormat(UltraDateTimeEditor timeEdit, string format)
        //{
        //    timeEdit.MaskInput = format;
        //    timeEdit.PromptChar = ' ';
        //}

        /// <summary>
        /// TextBox의 값이 Double인지 체크하고 값을 반환한다.
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static double GetDoubleValue(TextBox textBox)
        {
            double result = 0;
            try
            {
                if (textBox.Enabled)
                {
                    result = Convert.ToDouble(textBox.Text);
                }
            }
            catch (Exception e)
            {
                textBox.SelectAll();
                MessageBox.Show(textBox.Name + "] 형식은 숫자형식이 되어야 합니다");
            }
            return result;
        }

        /// <summary>
        /// TextBox의 값이 Double인지 체크하고 값을 반환한다.
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static double GetDoubleValue(ComboBox controls)
        {
            double result = 0;
            try
            {
                result = Convert.ToDouble(controls.SelectedValue);
            }
            catch (Exception e)
            {
                MessageBox.Show(controls.Name + "] 형식은 숫자형식이 되어야 합니다");
            }
            return result;
        }
    }
}
