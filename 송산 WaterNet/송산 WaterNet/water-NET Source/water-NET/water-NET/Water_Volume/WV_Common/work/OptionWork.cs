﻿using System;
using WaterNet.WV_Common.dao;

namespace WaterNet.WV_Common.work
{
    public class OptionWork : BaseWork
    {
        private static OptionWork work = null;
        private OptionDao dao = null;

        public static OptionWork GetInstance()
        {
            if (work == null)
            {
                work = new OptionWork();
            }
            return work;
        }

        private OptionWork()
        {
            dao = OptionDao.GetInstance();
        }

        public string GetSuppliedDay(string loc_code)
        {
            string result = string.Empty;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.GetSuppliedDay(DataBaseManager, loc_code).ToString();
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public string GetRevenueDay(string loc_code)
        {
            string result = string.Empty;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.GetRevenueDay(DataBaseManager, loc_code).ToString();
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public string GetRevenueFix(string loc_code)
        {
            string result = string.Empty;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.GetRevenueFix(DataBaseManager, loc_code).ToString();
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
