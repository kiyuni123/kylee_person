﻿using System;
using WaterNet.WV_Common.dao;
using System.Data;
using System.Collections;

namespace WaterNet.WV_Common.work
{
    public class BlockWork : BaseWork
    {
        private static BlockWork work = null;
        private BlockDao dao = null;

        public static BlockWork GetInstance()
        {
            if (work == null)
            {
                work = new BlockWork();
            }
            return work;
        }

        private BlockWork()
        {
            dao = BlockDao.GetInstance();
        }

        //
        //특정 지역의 태그종류별 태그를 조회한다.
        //면단위 지역의 소블록의 경우 순시유량, 적산, 압력등의 태그는 해당 블록 분기태그를 사용해야한다.
        //
        //
        //
        public string SelectBlockTag(string loc_code, string ftr_cde, string tag_gbn)
        {
            string result = string.Empty;
            string relLocGbn = string.Empty;
            //LOCATION_TYPE type = DataUtils.GetLocationType(ftr_cde);

            try
            {
                ConnectionOpen();
                BeginTransaction();

                //중블록일 경우 배수지 유무에 따라서 조회 태그가 달라진다..
                if (ftr_cde == "BZ002")
                {
                    result = Convert.ToString(dao.SelectMiddleBlockTag(DataBaseManager, loc_code, tag_gbn));
                }
                //소블록일 경우 해당 중블록의 배수지 유무에 따라서 조회 태그가 달라진다..
                if (ftr_cde == "BZ003")
                {
                    result = Convert.ToString(dao.SelectSmallBlockTag(DataBaseManager, loc_code, tag_gbn));
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public bool HasReservoir(string loc_code)
        {
            bool result = false;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = Convert.ToBoolean(dao.HasReservoir(DataBaseManager, loc_code));
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectCenter(Hashtable parameter, string dataMember)
        {
            DataSet result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectCenter(DataBaseManager, parameter, dataMember);
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectJungSuJang(Hashtable parameter, string dataMember)
        {
            DataSet result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectJungSuJang(DataBaseManager, parameter, dataMember);
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockListByLevel(Hashtable parameter, string dataMember)
        {
            DataSet result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectBlockListByLevel(DataBaseManager, parameter, dataMember);
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectSmallBlock(Hashtable parameter, string dataMember)
        {
            DataSet result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectSmallBlock(DataBaseManager, parameter, dataMember);
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockInfoByBlockCode(Hashtable parameter, string dataMember)
        {
            DataSet result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectBlockInfoByBlockCode(DataBaseManager, parameter, dataMember);
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
