﻿using System;
using WaterNet.WV_Common.dao;
using System.Data;

namespace WaterNet.WV_Common.work
{
    public class CodeWork: BaseWork
    {
        private static CodeWork work = null;
        private CodeDao dao = null;

        public static CodeWork GetInstance()
        {
            if (work == null)
            {
                work = new CodeWork();
            }
            return work;
        }

        private CodeWork()
        {
            dao = CodeDao.GetInstance();
        }

        public DataSet SelectCodeList(string parameter)
        {
            DataSet result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectCodeList(DataBaseManager, parameter, "RESULT");
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectTagList()
        {
            DataSet result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectTagList(DataBaseManager, "RESULT");
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
