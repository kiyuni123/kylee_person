﻿using System;
using System.Text;
using WaterNet.WaterNetCore;

using System.Diagnostics;

namespace WaterNet.WV_Common.work
{
    public class BaseWork
    {
        /// <summary>
        /// 비지니스로직(Work)클래스의 부모클래스이다.
        /// </summary>
        private OracleDBManager databaseManager = null;

        protected BaseWork()
        {
            databaseManager = new OracleDBManager();
            //databaseManager.ConnectionString = Utils.GetConnectString();
            databaseManager.ConnectionString = FunctionManager.GetConnectionString();
        }

        /// <summary>
        /// 
        /// </summary>
        protected OracleDBManager DataBaseManager
        {
            get
            {
                return databaseManager;
            }
        }

        protected void ConnectionOpen()
        {
            databaseManager.Open();
        }

        protected void ConnectionClose()
        {
            databaseManager.Close();
        }

        protected void BeginTransaction()
        {
            databaseManager.BeginTransaction();
        }

        protected void CommitTransaction()
        {
            databaseManager.CommitTransaction();
        }

        protected void RollbackTransaction()
        {
            databaseManager.RollbackTransaction();
        }

        protected void CloseTransaction()
        {
            if (databaseManager.Transaction != null)
            {
                databaseManager.Transaction.Dispose();
                databaseManager.Transaction = null;
            }
        }

        [Conditional("DEBUG")]
        protected void println(StringBuilder str)
        {
            Console.WriteLine(str.ToString());
        }

        [Conditional("DEBUG")]
        protected void println(string str)
        {
            Console.WriteLine(str);
        }
    }
}
