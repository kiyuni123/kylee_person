﻿namespace WaterNet.WV_Common.form
{
    partial class SearchBox
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Year = new System.Windows.Forms.Panel();
            this.oYear = new System.Windows.Forms.ComboBox();
            this.YearIntervalText = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.Panel();
            this.oDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DateIntervalText = new System.Windows.Forms.Label();
            this.MonthToMonth = new System.Windows.Forms.Panel();
            this.oEndMonth = new System.Windows.Forms.ComboBox();
            this.oEndYear = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.oStartMonth = new System.Windows.Forms.ComboBox();
            this.oStartYear = new System.Windows.Forms.ComboBox();
            this.MonthToMonthIntervalText = new System.Windows.Forms.Label();
            this.DateToDate = new System.Windows.Forms.Panel();
            this.oEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label6 = new System.Windows.Forms.Label();
            this.oStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DateToDateIntervalText = new System.Windows.Forms.Label();
            this.TimeToTime = new System.Windows.Forms.Panel();
            this.oEndTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.oStartTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.TimeToTimeIntervalText = new System.Windows.Forms.Label();
            this.Center = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.oCenter = new System.Windows.Forms.ComboBox();
            this.JungSu = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.oJungSu = new System.Windows.Forms.ComboBox();
            this.LargeBlock = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.oLargeBlock = new System.Windows.Forms.ComboBox();
            this.MiddleBlock = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.oMiddleBlock = new System.Windows.Forms.ComboBox();
            this.SmallBlock = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.oSmallBlock = new System.Windows.Forms.ComboBox();
            this.Month = new System.Windows.Forms.Panel();
            this.oMonth = new System.Windows.Forms.ComboBox();
            this.oYearMonth = new System.Windows.Forms.ComboBox();
            this.MonthIntervalText = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.Year.SuspendLayout();
            this.Date.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDate)).BeginInit();
            this.MonthToMonth.SuspendLayout();
            this.DateToDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oStartDate)).BeginInit();
            this.TimeToTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oStartTime)).BeginInit();
            this.Center.SuspendLayout();
            this.JungSu.SuspendLayout();
            this.LargeBlock.SuspendLayout();
            this.MiddleBlock.SuspendLayout();
            this.SmallBlock.SuspendLayout();
            this.Month.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(764, 111);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.Year);
            this.flowLayoutPanel1.Controls.Add(this.Date);
            this.flowLayoutPanel1.Controls.Add(this.MonthToMonth);
            this.flowLayoutPanel1.Controls.Add(this.DateToDate);
            this.flowLayoutPanel1.Controls.Add(this.TimeToTime);
            this.flowLayoutPanel1.Controls.Add(this.Month);
            this.flowLayoutPanel1.Controls.Add(this.Center);
            this.flowLayoutPanel1.Controls.Add(this.JungSu);
            this.flowLayoutPanel1.Controls.Add(this.LargeBlock);
            this.flowLayoutPanel1.Controls.Add(this.MiddleBlock);
            this.flowLayoutPanel1.Controls.Add(this.SmallBlock);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(5, 19);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(754, 112);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // Year
            // 
            this.Year.Controls.Add(this.oYear);
            this.Year.Controls.Add(this.YearIntervalText);
            this.Year.Location = new System.Drawing.Point(3, 3);
            this.Year.Name = "Year";
            this.Year.Size = new System.Drawing.Size(130, 22);
            this.Year.TabIndex = 0;
            // 
            // oYear
            // 
            this.oYear.FormattingEnabled = true;
            this.oYear.Location = new System.Drawing.Point(58, 0);
            this.oYear.Name = "oYear";
            this.oYear.Size = new System.Drawing.Size(70, 20);
            this.oYear.TabIndex = 2;
            // 
            // YearIntervalText
            // 
            this.YearIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.YearIntervalText.AutoSize = true;
            this.YearIntervalText.Location = new System.Drawing.Point(3, 4);
            this.YearIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.YearIntervalText.Name = "YearIntervalText";
            this.YearIntervalText.Size = new System.Drawing.Size(53, 12);
            this.YearIntervalText.TabIndex = 1;
            this.YearIntervalText.Text = "검색기간";
            // 
            // Date
            // 
            this.Date.Controls.Add(this.oDate);
            this.Date.Controls.Add(this.DateIntervalText);
            this.Date.Location = new System.Drawing.Point(139, 3);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(160, 22);
            this.Date.TabIndex = 16;
            this.Date.Visible = false;
            // 
            // oDate
            // 
            this.oDate.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.oDate.Location = new System.Drawing.Point(58, 0);
            this.oDate.Name = "oDate";
            this.oDate.Size = new System.Drawing.Size(100, 21);
            this.oDate.TabIndex = 8;
            this.oDate.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // DateIntervalText
            // 
            this.DateIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DateIntervalText.AutoSize = true;
            this.DateIntervalText.Location = new System.Drawing.Point(3, 4);
            this.DateIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.DateIntervalText.Name = "DateIntervalText";
            this.DateIntervalText.Size = new System.Drawing.Size(53, 12);
            this.DateIntervalText.TabIndex = 1;
            this.DateIntervalText.Text = "검색기간";
            // 
            // MonthToMonth
            // 
            this.MonthToMonth.Controls.Add(this.oEndMonth);
            this.MonthToMonth.Controls.Add(this.oEndYear);
            this.MonthToMonth.Controls.Add(this.label3);
            this.MonthToMonth.Controls.Add(this.oStartMonth);
            this.MonthToMonth.Controls.Add(this.oStartYear);
            this.MonthToMonth.Controls.Add(this.MonthToMonthIntervalText);
            this.MonthToMonth.Location = new System.Drawing.Point(305, 3);
            this.MonthToMonth.Name = "MonthToMonth";
            this.MonthToMonth.Size = new System.Drawing.Size(321, 22);
            this.MonthToMonth.TabIndex = 1;
            // 
            // oEndMonth
            // 
            this.oEndMonth.FormattingEnabled = true;
            this.oEndMonth.Location = new System.Drawing.Point(269, 0);
            this.oEndMonth.Name = "oEndMonth";
            this.oEndMonth.Size = new System.Drawing.Size(50, 20);
            this.oEndMonth.TabIndex = 6;
            // 
            // oEndYear
            // 
            this.oEndYear.FormattingEnabled = true;
            this.oEndYear.Location = new System.Drawing.Point(196, 0);
            this.oEndYear.Name = "oEndYear";
            this.oEndYear.Size = new System.Drawing.Size(70, 20);
            this.oEndYear.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "-";
            // 
            // oStartMonth
            // 
            this.oStartMonth.FormattingEnabled = true;
            this.oStartMonth.Location = new System.Drawing.Point(131, 0);
            this.oStartMonth.Name = "oStartMonth";
            this.oStartMonth.Size = new System.Drawing.Size(50, 20);
            this.oStartMonth.TabIndex = 3;
            // 
            // oStartYear
            // 
            this.oStartYear.FormattingEnabled = true;
            this.oStartYear.Location = new System.Drawing.Point(58, 0);
            this.oStartYear.Name = "oStartYear";
            this.oStartYear.Size = new System.Drawing.Size(70, 20);
            this.oStartYear.TabIndex = 2;
            // 
            // MonthToMonthIntervalText
            // 
            this.MonthToMonthIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MonthToMonthIntervalText.AutoSize = true;
            this.MonthToMonthIntervalText.Location = new System.Drawing.Point(3, 4);
            this.MonthToMonthIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.MonthToMonthIntervalText.Name = "MonthToMonthIntervalText";
            this.MonthToMonthIntervalText.Size = new System.Drawing.Size(53, 12);
            this.MonthToMonthIntervalText.TabIndex = 1;
            this.MonthToMonthIntervalText.Text = "검색기간";
            // 
            // DateToDate
            // 
            this.DateToDate.Controls.Add(this.oEndDate);
            this.DateToDate.Controls.Add(this.label6);
            this.DateToDate.Controls.Add(this.oStartDate);
            this.DateToDate.Controls.Add(this.DateToDateIntervalText);
            this.DateToDate.Location = new System.Drawing.Point(3, 31);
            this.DateToDate.Name = "DateToDate";
            this.DateToDate.Size = new System.Drawing.Size(275, 22);
            this.DateToDate.TabIndex = 10;
            // 
            // oEndDate
            // 
            this.oEndDate.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.oEndDate.Location = new System.Drawing.Point(173, 0);
            this.oEndDate.Name = "oEndDate";
            this.oEndDate.Size = new System.Drawing.Size(100, 21);
            this.oEndDate.TabIndex = 9;
            this.oEndDate.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(161, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 12);
            this.label6.TabIndex = 4;
            this.label6.Text = "-";
            // 
            // oStartDate
            // 
            this.oStartDate.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.oStartDate.Location = new System.Drawing.Point(58, 0);
            this.oStartDate.Name = "oStartDate";
            this.oStartDate.Size = new System.Drawing.Size(100, 21);
            this.oStartDate.TabIndex = 8;
            this.oStartDate.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // DateToDateIntervalText
            // 
            this.DateToDateIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DateToDateIntervalText.AutoSize = true;
            this.DateToDateIntervalText.Location = new System.Drawing.Point(3, 4);
            this.DateToDateIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.DateToDateIntervalText.Name = "DateToDateIntervalText";
            this.DateToDateIntervalText.Size = new System.Drawing.Size(53, 12);
            this.DateToDateIntervalText.TabIndex = 1;
            this.DateToDateIntervalText.Text = "검색기간";
            // 
            // TimeToTime
            // 
            this.TimeToTime.Controls.Add(this.oEndTime);
            this.TimeToTime.Controls.Add(this.label4);
            this.TimeToTime.Controls.Add(this.oStartTime);
            this.TimeToTime.Controls.Add(this.TimeToTimeIntervalText);
            this.TimeToTime.Location = new System.Drawing.Point(284, 31);
            this.TimeToTime.Name = "TimeToTime";
            this.TimeToTime.Size = new System.Drawing.Size(351, 22);
            this.TimeToTime.TabIndex = 7;
            // 
            // oEndTime
            // 
            this.oEndTime.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.oEndTime.Location = new System.Drawing.Point(213, 0);
            this.oEndTime.MaskInput = "{date} hh:mm";
            this.oEndTime.Name = "oEndTime";
            this.oEndTime.Size = new System.Drawing.Size(137, 21);
            this.oEndTime.TabIndex = 9;
            this.oEndTime.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(199, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "-";
            // 
            // oStartTime
            // 
            this.oStartTime.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.oStartTime.Location = new System.Drawing.Point(58, 0);
            this.oStartTime.MaskInput = "{date} hh:mm";
            this.oStartTime.Name = "oStartTime";
            this.oStartTime.Size = new System.Drawing.Size(137, 21);
            this.oStartTime.TabIndex = 8;
            this.oStartTime.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // TimeToTimeIntervalText
            // 
            this.TimeToTimeIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeToTimeIntervalText.AutoSize = true;
            this.TimeToTimeIntervalText.Location = new System.Drawing.Point(3, 4);
            this.TimeToTimeIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.TimeToTimeIntervalText.Name = "TimeToTimeIntervalText";
            this.TimeToTimeIntervalText.Size = new System.Drawing.Size(53, 12);
            this.TimeToTimeIntervalText.TabIndex = 1;
            this.TimeToTimeIntervalText.Text = "검색기간";
            // 
            // Center
            // 
            this.Center.Controls.Add(this.label8);
            this.Center.Controls.Add(this.oCenter);
            this.Center.Location = new System.Drawing.Point(192, 59);
            this.Center.Name = "Center";
            this.Center.Size = new System.Drawing.Size(125, 22);
            this.Center.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "센터";
            // 
            // oCenter
            // 
            this.oCenter.FormattingEnabled = true;
            this.oCenter.Location = new System.Drawing.Point(33, 0);
            this.oCenter.Name = "oCenter";
            this.oCenter.Size = new System.Drawing.Size(90, 20);
            this.oCenter.TabIndex = 3;
            // 
            // JungSu
            // 
            this.JungSu.Controls.Add(this.label9);
            this.JungSu.Controls.Add(this.oJungSu);
            this.JungSu.Location = new System.Drawing.Point(323, 59);
            this.JungSu.Name = "JungSu";
            this.JungSu.Size = new System.Drawing.Size(140, 22);
            this.JungSu.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "정수장";
            // 
            // oJungSu
            // 
            this.oJungSu.FormattingEnabled = true;
            this.oJungSu.Location = new System.Drawing.Point(48, 0);
            this.oJungSu.Name = "oJungSu";
            this.oJungSu.Size = new System.Drawing.Size(90, 20);
            this.oJungSu.TabIndex = 4;
            // 
            // LargeBlock
            // 
            this.LargeBlock.Controls.Add(this.label10);
            this.LargeBlock.Controls.Add(this.oLargeBlock);
            this.LargeBlock.Location = new System.Drawing.Point(469, 59);
            this.LargeBlock.Name = "LargeBlock";
            this.LargeBlock.Size = new System.Drawing.Size(140, 22);
            this.LargeBlock.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "대블록";
            // 
            // oLargeBlock
            // 
            this.oLargeBlock.FormattingEnabled = true;
            this.oLargeBlock.Location = new System.Drawing.Point(48, 0);
            this.oLargeBlock.Name = "oLargeBlock";
            this.oLargeBlock.Size = new System.Drawing.Size(90, 20);
            this.oLargeBlock.TabIndex = 5;
            // 
            // MiddleBlock
            // 
            this.MiddleBlock.Controls.Add(this.label11);
            this.MiddleBlock.Controls.Add(this.oMiddleBlock);
            this.MiddleBlock.Location = new System.Drawing.Point(3, 87);
            this.MiddleBlock.Name = "MiddleBlock";
            this.MiddleBlock.Size = new System.Drawing.Size(140, 22);
            this.MiddleBlock.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "중블록";
            // 
            // oMiddleBlock
            // 
            this.oMiddleBlock.FormattingEnabled = true;
            this.oMiddleBlock.Location = new System.Drawing.Point(48, 0);
            this.oMiddleBlock.Name = "oMiddleBlock";
            this.oMiddleBlock.Size = new System.Drawing.Size(90, 20);
            this.oMiddleBlock.TabIndex = 6;
            // 
            // SmallBlock
            // 
            this.SmallBlock.Controls.Add(this.label12);
            this.SmallBlock.Controls.Add(this.oSmallBlock);
            this.SmallBlock.Location = new System.Drawing.Point(149, 87);
            this.SmallBlock.Name = "SmallBlock";
            this.SmallBlock.Size = new System.Drawing.Size(140, 22);
            this.SmallBlock.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "소블록";
            // 
            // oSmallBlock
            // 
            this.oSmallBlock.FormattingEnabled = true;
            this.oSmallBlock.Location = new System.Drawing.Point(48, 0);
            this.oSmallBlock.Name = "oSmallBlock";
            this.oSmallBlock.Size = new System.Drawing.Size(90, 20);
            this.oSmallBlock.TabIndex = 7;
            // 
            // Month
            // 
            this.Month.Controls.Add(this.oMonth);
            this.Month.Controls.Add(this.oYearMonth);
            this.Month.Controls.Add(this.MonthIntervalText);
            this.Month.Location = new System.Drawing.Point(3, 59);
            this.Month.Name = "Month";
            this.Month.Size = new System.Drawing.Size(183, 22);
            this.Month.TabIndex = 17;
            // 
            // oMonth
            // 
            this.oMonth.FormattingEnabled = true;
            this.oMonth.Location = new System.Drawing.Point(131, 0);
            this.oMonth.Name = "oMonth";
            this.oMonth.Size = new System.Drawing.Size(50, 20);
            this.oMonth.TabIndex = 3;
            // 
            // oYearMonth
            // 
            this.oYearMonth.FormattingEnabled = true;
            this.oYearMonth.Location = new System.Drawing.Point(58, 0);
            this.oYearMonth.Name = "oYearMonth";
            this.oYearMonth.Size = new System.Drawing.Size(70, 20);
            this.oYearMonth.TabIndex = 2;
            // 
            // MonthIntervalText
            // 
            this.MonthIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MonthIntervalText.AutoSize = true;
            this.MonthIntervalText.Location = new System.Drawing.Point(3, 4);
            this.MonthIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.MonthIntervalText.Name = "MonthIntervalText";
            this.MonthIntervalText.Size = new System.Drawing.Size(53, 12);
            this.MonthIntervalText.TabIndex = 1;
            this.MonthIntervalText.Text = "검색기간";
            // 
            // SearchBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.groupBox1);
            this.Name = "SearchBox";
            this.Size = new System.Drawing.Size(764, 113);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.Year.ResumeLayout(false);
            this.Year.PerformLayout();
            this.Date.ResumeLayout(false);
            this.Date.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDate)).EndInit();
            this.MonthToMonth.ResumeLayout(false);
            this.MonthToMonth.PerformLayout();
            this.DateToDate.ResumeLayout(false);
            this.DateToDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oStartDate)).EndInit();
            this.TimeToTime.ResumeLayout(false);
            this.TimeToTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oStartTime)).EndInit();
            this.Center.ResumeLayout(false);
            this.Center.PerformLayout();
            this.JungSu.ResumeLayout(false);
            this.JungSu.PerformLayout();
            this.LargeBlock.ResumeLayout(false);
            this.LargeBlock.PerformLayout();
            this.MiddleBlock.ResumeLayout(false);
            this.MiddleBlock.PerformLayout();
            this.SmallBlock.ResumeLayout(false);
            this.SmallBlock.PerformLayout();
            this.Month.ResumeLayout(false);
            this.Month.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel Year;
        private System.Windows.Forms.Label YearIntervalText;
        private System.Windows.Forms.ComboBox oYear;
        private System.Windows.Forms.Panel MonthToMonth;
        private System.Windows.Forms.ComboBox oEndMonth;
        private System.Windows.Forms.ComboBox oEndYear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox oStartMonth;
        private System.Windows.Forms.ComboBox oStartYear;
        private System.Windows.Forms.Label MonthToMonthIntervalText;
        private System.Windows.Forms.Panel TimeToTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TimeToTimeIntervalText;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor oEndTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor oStartTime;
        private System.Windows.Forms.Panel DateToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor oEndDate;
        private System.Windows.Forms.Label label6;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor oStartDate;
        private System.Windows.Forms.Label DateToDateIntervalText;
        private System.Windows.Forms.Panel Center;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox oCenter;
        private System.Windows.Forms.Panel JungSu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox oJungSu;
        private System.Windows.Forms.Panel LargeBlock;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox oLargeBlock;
        private System.Windows.Forms.Panel MiddleBlock;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox oMiddleBlock;
        private System.Windows.Forms.Panel SmallBlock;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox oSmallBlock;
        private System.Windows.Forms.Panel Date;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor oDate;
        private System.Windows.Forms.Label DateIntervalText;
        private System.Windows.Forms.Panel Month;
        private System.Windows.Forms.ComboBox oMonth;
        private System.Windows.Forms.ComboBox oYearMonth;
        private System.Windows.Forms.Label MonthIntervalText;
    }
}
