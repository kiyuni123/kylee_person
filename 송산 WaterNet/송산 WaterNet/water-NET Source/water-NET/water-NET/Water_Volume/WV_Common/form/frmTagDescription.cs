﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WV_Common.form
{
    public partial class frmTagDescription : Form
    {
        private WV_Common.manager.UltraGridManager gridManager = null;
        private string LOC_CODE = string.Empty;
        private string TAG_GBN = string.Empty;
        private static System.Data.DataSet tagTable;

        public frmTagDescription(string locCode, string tagGbn)
        {
            InitializeComponent();

            if (tagTable == null || tagTable.Tables.Count == 0 || tagTable.Tables[0].Rows.Count == 0)
            {
                 tagTable = work.CodeWork.GetInstance().SelectTagList();
            }
            this.LOC_CODE = locCode;
            this.TAG_GBN = tagGbn;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new WV_Common.manager.UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            Infragistics.Win.UltraWinGrid.UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TAG2";
            column.Header.Caption = "태그명";
            column.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Left;
            column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            column.Width = 60;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DESC2";
            column.Header.Caption = "태그설명";
            column.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Left;
            column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            column.Width = 240;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CAL_GBN";
            column.Header.Caption = "계산식";
            column.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            column.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
            column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            column.Width = 40;
            column.Hidden = false;

            #endregion
        }

        private void frmTagDescription_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Cursor.Position.X + 5, Cursor.Position.Y - 5);

            this.InitializeGrid();
            this.InitializeGridColumn();

            if (string.IsNullOrEmpty(this.LOC_CODE) || string.IsNullOrEmpty(this.TAG_GBN)) 
            {
                this.Close();
                return;
            }

            System.Data.DataRow[] rows = tagTable.Tables[0].Select(string.Format("LOC_CODE = '{0}' AND TAG_GBN = '{1}'", this.LOC_CODE, this.TAG_GBN),"CAL_GBN desc, tag2 asc");
            if (rows.Length == 0) 
            {
                this.Close();
                return;
            }
            
            this.lblBlock.Text = rows[0]["LOC_NAME"].ToString();
            this.lblTag.Text = rows[0]["TAG1"].ToString();
            this.lblDesc.Text = rows[0]["DESC1"].ToString();

            DataTable table = rows.CopyToDataTable();
            table.Columns.Remove("LOC_CODE");
            table.Columns.Remove("LOC_NAME");
            table.Columns.Remove("TAG1");
            table.Columns.Remove("DESC1");
            table.Columns.Remove("TAG_GBN");
            this.ultraGrid1.DataSource = table;

            this.timer1.Interval = 1000 * 60;
            this.timer1.Start();
            this.BringToFront();
        }

        private void frmTagDescription_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.timer1.Stop();
        }
    }
}
