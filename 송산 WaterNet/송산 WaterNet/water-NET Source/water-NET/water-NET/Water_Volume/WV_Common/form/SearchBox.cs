﻿using System;
using System.Data;
using System.Windows.Forms;
using Infragistics.Win.UltraWinEditors;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.control;
using WaterNet.WV_Common.interface1;
using System.Collections;
using WaterNet.WV_Common.enum1;

namespace WaterNet.WV_Common.form
{
    public partial class SearchBox : UserControl, IParameter
    {
        public SearchBox()
        {
            InitializeComponent();
            this.Load += new EventHandler(SearchBoxLoad_EventHandler);
        }

        public void InitializeSearchBox()
        {
            this.InitializeForm();
        }

        private void SearchBoxLoad_EventHandler(object sender, EventArgs e)
        {
            InitializeEvent();
            //InitializeForm();
        }

        /// <summary>
        /// 컨트롤을 추가할 경우 기본영역에 추가
        /// </summary>
        /// <param name="child">컨트롤 객체</param>
        public void Add(Control child)
        {
            flowLayoutPanel1.Controls.Add(child);
        }

        /// <summary>
        /// 초기 검색 기간 설정
        /// </summary>
        public INTERVAL_TYPE IntervalType
        {
            set
            {
                this.intervalType = value;
                SetInterval();
            }
        }

        /// <summary>
        /// 검색지역을 활성/비활성
        /// </summary>
        public bool LocationVisible
        {
            set
            {
                this.Center.Visible = value;
                this.JungSu.Visible = value;
                this.LargeBlock.Visible = value;
                this.MiddleBlock.Visible = value;
                this.SmallBlock.Visible = value;
            }
        }

        /// <summary>
        /// 그룹박스 제목 수정
        /// </summary>
        public override string Text
        {
            set
            {
                this.groupBox1.Text = value;
            }
        }

        /// <summary>
        /// 초기 검색 기간 기본값 (년월일 - 년월일)
        /// </summary>
        private INTERVAL_TYPE intervalType = INTERVAL_TYPE.YEAR_MONTH;

        /// <summary>
        /// 초기 검색 기간의 이름
        /// </summary>
        private string intervalText = "검색기간";

        /// <summary>
        /// 블럭 제어 클래스
        /// </summary>
        private BlockComboboxControl blockControl = null;

        /// <summary>
        /// 초기 검색 기간이름 변경
        /// </summary>
        public string IntervalText
        {
            set
            {
                this.intervalText = value;
                SetInterval();
            }
        }

        /// <summary>
        /// 블록 제어의 이벤트 제어 여부
        /// </summary>
        public bool StopEvent
        {
            set
            {
                this.blockControl.StopEvent = value;
            }
        }

        public BlockComboboxControl BlockControl
        {
            get
            {
                return this.blockControl;
            }
        }

        /// <summary>
        /// 초기 폼 설정
        /// </summary>
        private void InitializeForm()
        {
            //블럭제어 설정
            this.blockControl = new BlockComboboxControl(this.oCenter, this.oJungSu, this.oLargeBlock, this.oMiddleBlock, this.oSmallBlock);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            this.BlockDefaultSetting();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //초기 검색 기간 설정
            this.SetInterval();

            //사이즈 재조정
            //this.sizeChanged_EventHandler(null, null);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        ///개발중 임의 세팅
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        private void BlockDefaultSetting()
        {
            blockControl.AddDefaultOption(LOCATION_TYPE.CENTER, true);
            blockControl.AddDefaultOption(LOCATION_TYPE.PURIFICATION_PLANT, true);
            blockControl.AddDefaultOption(LOCATION_TYPE.MIDDLE_BLOCK, false);
            blockControl.AddDefaultOption(LOCATION_TYPE.SMALL_BLOCK, false);

            this.oCenter.Enabled = false;
            this.oJungSu.Enabled = false;

            //this.MiddleBlockObject.SelectedIndex = 2;
            //this.SmallBlockObject.SelectedIndex = 1;
        }

        /// <summary>
        /// 초기 이벤트 설정
        /// </summary>
        private void InitializeEvent()
        {
            this.flowLayoutPanel1.SizeChanged += new EventHandler(sizeChanged_EventHandler);
        }

        /// <summary>
        /// 높이 변환 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sizeChanged_EventHandler(object sender, EventArgs e)
        {
            int oHeight = this.flowLayoutPanel1.Height + this.groupBox1.Margin.Top + this.groupBox1.Margin.Bottom + 30;
      
            this.groupBox1.Height = oHeight;
            this.Height = oHeight;
        }

        /// <summary>
        /// 초기 검색 기간 설정값에 따라 컨트롤을 Visible 한다.
        /// </summary>
        private void SetInterval()
        {
            this.Year.Visible = false;
            this.Month.Visible = false;
            this.MonthToMonth.Visible = false;
            this.DateToDate.Visible = false;
            this.TimeToTime.Visible = false;

            switch (intervalType)
            {
                case INTERVAL_TYPE.SINGLE_YEAR:
                    {
                        this.Year.Visible = true;
                        this.YearIntervalText.Text = this.intervalText;
                        this.InitializeElements();
                        break;
                    }
                case INTERVAL_TYPE.SINGLE_MONTH:
                    {
                        this.Month.Visible = true;
                        this.MonthIntervalText.Text = this.intervalText;
                        this.InitializeElements();
                        break;
                    }
                case INTERVAL_TYPE.SINGLE_DATE:
                    {
                        this.Date.Visible = true;
                        this.DateIntervalText.Text = this.intervalText;
                        this.InitializeElements();
                        break;
                    }
                case INTERVAL_TYPE.YEAR_MONTH:
                    {
                        this.MonthToMonth.Visible = true;
                        this.MonthToMonthIntervalText.Text = this.intervalText;
                        this.InitializeElements();
                        break;
                    }
                case INTERVAL_TYPE.DATE:
                    {
                        this.DateToDate.Visible = true;
                        this.DateToDateIntervalText.Text = this.intervalText;
                        this.InitializeElements();
                        break;
                    }
                case INTERVAL_TYPE.DATE_MINUTE:
                    {
                        this.TimeToTime.Visible = true;
                        this.TimeToTimeIntervalText.Text = this.intervalText;
                        this.InitializeElements();
                        break;
                    }
                case INTERVAL_TYPE.NONE:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// 단일 년도 작업 수행 객체 반환
        /// </summary>
        public ComboBox YearObject
        {
            get
            {
                return this.oYear;
            }
        }

        /// <summary>
        /// 단일 년월 년도 작업 수행 객체 반환
        /// </summary>
        public ComboBox YearMonthObject
        {
            get
            {
                return this.oYearMonth;
            }
        }

        /// <summary>
        /// 단일 년월 월 작업 수행 객체 반환
        /// </summary>
        public ComboBox MonthObject
        {
            get
            {
                return this.oMonth;
            }
        }

        /// <summary>
        /// 복합 년월 작업 수행의 시작년 객체 반환 
        /// </summary>
        public ComboBox StartYearObject
        {
            get
            {
                return this.oStartYear;
            }
        }

        /// <summary>
        /// 복합 년월 작업 수행의 종료년 객체 반환
        /// </summary>
        public ComboBox EndYearObject
        {
            get
            {
                return this.oEndYear;
            }
        }

        /// <summary>
        /// 복합 년월 작업 수행의 시작월 객체 반환
        /// </summary>
        public ComboBox StartMonthObject
        {
            get
            {
                return this.oStartMonth;
            }
        }

        /// <summary>
        /// 복합 년월 작업 수행의 종료월 객체 반환
        /// </summary>
        public ComboBox EndMonthObject
        {
            get
            {
                return this.oEndMonth;
            }
        }

        /// <summary>
        /// 단일 일자 작업 수행 객체 반환
        /// </summary>
        public UltraDateTimeEditor DateObject
        {
            get
            {
                return this.oDate;
            }
        }

        /// <summary>
        /// 복합 일자 작업 수행의 시작일 객체 반환
        /// </summary>
        public UltraDateTimeEditor StartDateObject
        {
            get
            {
                return this.oStartDate;
            }
        }

        /// <summary>
        /// 복합 일자 작업 수행의 종료일 객체 반환
        /// </summary>
        public UltraDateTimeEditor EndDateObject
        {
            get
            {
                return this.oEndDate;
            }
        }

        /// <summary>
        /// 복합 일시 작업 수행의 시작일시 객체 반환
        /// </summary>
        public UltraDateTimeEditor StartDateTimeObject
        {
            get
            {
                return this.oStartTime;
            }
        }

        /// <summary>
        /// 복합 일시 작업 수행의 종료일시 객체 반환
        /// </summary>
        public UltraDateTimeEditor EndDateTimeObject
        {
            get
            {
                return this.oEndTime;
            }
        }

        /// <summary>
        /// 센터 영역 반환(Visible 제어)
        /// </summary>
        public Panel CenterContainer
        {
            get
            {
                return this.Center;
            }
        }

        /// <summary>
        /// 정수장 영역 반환(Visible 제어)
        /// </summary>
        public Panel JungSuContainer
        {
            get
            {
                return this.JungSu;
            }
        }

        /// <summary>
        /// 대블록 영역 반환(Visible 제어)
        /// </summary>
        public Panel LargeBlockContainer
        {
            get
            {
                return this.LargeBlock;
            }
        }

        /// <summary>
        /// 중블록 영역 반환(Visible 제어)
        /// </summary>
        public Panel MiddleBlockContainer
        {
            get
            {
                return this.MiddleBlock;
            }
        }

        /// <summary>
        /// 소블록 영역 반환(Visible 제어)
        /// </summary>
        public Panel SmallBlockContainer
        {
            get
            {
                return this.SmallBlock;
            }
        }

        /// <summary>
        /// 센터 객체 반환
        /// </summary>
        public ComboBox CenterObject
        {
            get
            {
                return this.oCenter;
            }
        }

        /// <summary>
        /// 정수장 객체 반환
        /// </summary>
        public ComboBox JungSuObject
        {
            get
            {
                return this.oJungSu;
            }
        }

        /// <summary>
        /// 대블록 객체 반환
        /// </summary>
        public ComboBox LargeBlockObject
        {
            get
            {
                return this.oLargeBlock;
            }
        }

        /// <summary>
        /// 중블록 객체 반환
        /// </summary>
        public ComboBox MiddleBlockObject
        {
            get
            {
                return this.oMiddleBlock;
            }
        }

        /// <summary>
        /// 소블록 객체 반환
        /// </summary>
        public ComboBox SmallBlockObject
        {
            get
            {
                return this.oSmallBlock;
            }
        }

        /// <summary>
        /// 지역타입의 콤보박스를 선택한다.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="displayMember"></param>
        public void SelectBlockItem(LOCATION_TYPE type, string displayMember)
        {
            ComboBoxUtils.SelectItemByDisplayMember(this.GetBlock(type), displayMember);
        }

        private ComboBox GetBlock(LOCATION_TYPE type)
        {
            ComboBox block = null;
            if (type == LOCATION_TYPE.LARGE_BLOCK)
            {
                block = this.LargeBlockObject;
            }
            if (type == LOCATION_TYPE.MIDDLE_BLOCK)
            {
                block = this.MiddleBlockObject;
            }
            if (type == LOCATION_TYPE.SMALL_BLOCK)
            {
                block = this.SmallBlockObject;
            }
            return block;
        }

        private void InitializeElements()
        {
            DateTime Now = DateTime.Now;

            switch (intervalType)
            {
                case INTERVAL_TYPE.SINGLE_YEAR:
                    {
                        ComboBoxUtils.SetMember(this.oYear, "CODE", "CODE_NAME");
                        this.oYear.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
                        this.oYear.SelectedValue = Now.Year - 1;
                        break;
                    }
                case INTERVAL_TYPE.SINGLE_MONTH:
                    {
                        ComboBoxUtils.SetMember(this.oYearMonth, "CODE", "CODE_NAME");
                        ComboBoxUtils.SetMember(this.oMonth, "CODE", "CODE_NAME");
                        this.oYearMonth.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
                        this.oMonth.DataSource = DateUtils.GetMonth("CODE", "CODE_NAME");
                        this.oYearMonth.SelectedValue = Now.AddMonths(-1).ToString("yyyy");
                        this.oMonth.SelectedValue = Now.AddMonths(-1).ToString("MM");
                        break;
                    }
                case INTERVAL_TYPE.SINGLE_DATE:
                    {
                        this.oDate.Value = DateUtils.GetDate('-', 0, 0, -1);
                        break;
                    }
                case INTERVAL_TYPE.YEAR_MONTH:
                    {
                        ComboBoxUtils.SetMember(this.oStartYear, "CODE", "CODE_NAME");
                        ComboBoxUtils.SetMember(this.oEndYear, "CODE", "CODE_NAME");
                        ComboBoxUtils.SetMember(this.oStartMonth, "CODE", "CODE_NAME");
                        ComboBoxUtils.SetMember(this.oEndMonth, "CODE", "CODE_NAME");

                        this.oStartYear.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
                        this.oEndYear.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
                        this.oStartMonth.DataSource = DateUtils.GetMonth("CODE", "CODE_NAME");
                        this.oEndMonth.DataSource = DateUtils.GetMonth("CODE", "CODE_NAME");

                        this.oStartYear.SelectedValue = Now.AddYears(-1).AddMonths(-1).ToString("yyyy");
                        this.oEndYear.SelectedValue = Now.AddMonths(-1).ToString("yyyy");
                        this.oStartMonth.SelectedValue = Now.AddYears(-1).AddMonths(-1).ToString("MM");
                        this.oEndMonth.SelectedValue = Now.AddMonths(-1).ToString("MM");
                        break;
                    }
                case INTERVAL_TYPE.DATE:
                    {
                        this.oStartDate.Value = DateTime.Now.AddYears(-1).AddDays(-1).ToString("yyyy-MM-dd");
                        this.oEndDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                        break;
                    }
                case INTERVAL_TYPE.DATE_MINUTE:
                    {
                        //this.oStartTime.Value = DateUtils.GetDateTime('-', 0, 0, -2, 00, 00);
                        //this.oEndTime.Value = DateUtils.GetDateTime('-', 0, 0, -1, 23, 59);
                        this.oStartTime.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00");
                        this.oEndTime.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 23:59");
                        break;
                    }
            }
        }

        private void ClearElements()
        {
            this.oYear.Items.Clear();
            this.oStartYear.Items.Clear();
            this.oStartMonth.Items.Clear();
            this.oEndYear.Items.Clear();
            this.oEndMonth.Items.Clear();
        }

        #region IParameter 멤버


        private Hashtable parameters = null;

        public IParameter InitializeParameter()
        {
            if (parameters == null)
            {
                parameters = new Hashtable();
            }
            else
            {
                parameters.Clear();
            }

            switch (intervalType)
            {
                case INTERVAL_TYPE.SINGLE_YEAR:
                    {
                        parameters["STARTDATE"] = this.oYear.SelectedValue.ToString();
                        break;
                    }
                case INTERVAL_TYPE.SINGLE_MONTH:
                    {
                        if (this.oYearMonth.Items.Count > 0 && this.oMonth.Items.Count > 0)
                        {
                            parameters["STARTDATE"] = this.oYearMonth.SelectedValue.ToString() + this.oMonth.SelectedValue.ToString();
                            parameters["ENDDATE"] = this.oYearMonth.SelectedValue.ToString() + this.oMonth.SelectedValue.ToString();
                        }
                        break;
                    }
                case INTERVAL_TYPE.SINGLE_DATE:
                    {
                        parameters["STARTDATE"] = ((DateTime)this.oDate.Value).ToString("yyyyMMdd");
                        break;
                    }
                case INTERVAL_TYPE.YEAR_MONTH:
                    {
                        if (this.oStartYear.Items.Count > 0 && this.oStartMonth.Items.Count > 0 && this.oEndYear.Items.Count > 0 && this.oEndMonth.Items.Count > 0)
                        {
                            parameters["STARTDATE"] = this.oStartYear.SelectedValue.ToString() + this.oStartMonth.SelectedValue.ToString();
                            parameters["ENDDATE"] = this.oEndYear.SelectedValue.ToString() + this.oEndMonth.SelectedValue.ToString();
                        }
                        break;
                    }
                case INTERVAL_TYPE.DATE:
                    {
                        parameters["STARTDATE"] = ((DateTime)this.oStartDate.Value).ToString("yyyyMMdd");
                        parameters["ENDDATE"] = ((DateTime)this.oEndDate.Value).ToString("yyyyMMdd");
                        break;
                    }
                case INTERVAL_TYPE.DATE_MINUTE:
                    {
                        parameters["STARTDATE"] = ((DateTime)this.oStartTime.Value).ToString("yyyyMMddHHmm");
                        parameters["ENDDATE"] = ((DateTime)this.oEndTime.Value).ToString("yyyyMMddHHmm");
                        break;
                    }
            }

            DataRowView selectedItem = null;

            if (this.SmallBlockObject != null && this.SmallBlockObject.SelectedIndex > 0)
            {
                selectedItem = this.SmallBlockObject.SelectedItem as DataRowView;
            }
            else if (this.MiddleBlockObject != null && this.MiddleBlockObject.SelectedIndex > 0)
            {
                selectedItem = this.MiddleBlockObject.SelectedItem as DataRowView;
            }
            else
            {
                selectedItem = this.LargeBlockObject.SelectedItem as DataRowView;
            }

            if (selectedItem != null)
            {
                parameters["LOC_CODE"] = selectedItem["LOC_CODE"];
                parameters["FTR_IDN"] = selectedItem["FTR_IDN"];
                parameters["FTR_CODE"] = selectedItem["FTR_CODE"];
                parameters["LOC_NAME"] = selectedItem["LOC_NAME"];
            }
 
            return this;
        }

        public Hashtable Parameters
        {
            get
            {
                if (this.parameters == null)
                {
                    this.InitializeParameter();
                }

                return this.parameters;
            }
            set
            {
                this.parameters = value;
            }
        }

        public bool IsEmptyParameter
        {
            get
            {
                bool isEmptyParameter = false;
                if (this.parameters == null)
                {
                    isEmptyParameter = true;
                }
                return isEmptyParameter;
            }
        }

        public LOCATION_TYPE GetLocationType()
        {
            if (parameters == null || !parameters.ContainsKey("FTR_CODE"))
            {
                throw new Exception("선택한 블록이 유효하지 않습니다.");
            }

            object type = null ;

            if (parameters["FTR_CODE"].ToString() == "BZ001")
            {
                type = LOCATION_TYPE.LARGE_BLOCK;
            }
            if (parameters["FTR_CODE"].ToString() == "BZ002")
            {
                type = LOCATION_TYPE.MIDDLE_BLOCK;
            }
            if (parameters["FTR_CODE"].ToString() == "BZ003")
            {
                type = LOCATION_TYPE.SMALL_BLOCK;
            }
            return (LOCATION_TYPE)type;
        }

        #endregion
    }

    /// <summary>
    /// 기간 타입
    /// {
    ///     년도
    ///     년월 - 년월
    ///     년월일 - 년월일
    ///     년월일 시분 - 년월일 시분
    /// }
    /// </summary>
    //public enum IntervalType
    //{
    //    Year, Date, MonthToMonth, DateToDate, TimeToTime
    //}

    /// <summary>
    /// 검색 지역 타입
    /// {
    ///     센터
    ///     정수장
    ///     대블록
    ///     중블록
    ///     소블록
    ///     전체
    /// }
    /// </summary>
    //public enum LocationType
    //{
    //    Center=0, JungSuJang=1, LargeBlock=2, MiddleBlock=3, SmallBlock=4, All=9
    //}
}
