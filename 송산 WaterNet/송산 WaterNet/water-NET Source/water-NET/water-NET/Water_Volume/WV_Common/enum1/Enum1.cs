﻿
namespace WaterNet.WV_Common.enum1
{
    public class Enum1
    {
    }


    public enum INTERVAL_TYPE
    {
        SINGLE_YEAR, SINGLE_MONTH, SINGLE_DATE, YEAR_MONTH, DATE, DATE_MINUTE, NONE
    }

    public enum LOCATION_TYPE
    {
        CENTER = 0, PURIFICATION_PLANT = 1, LARGE_BLOCK = 2, MIDDLE_BLOCK = 3, SMALL_BLOCK = 4, All = 9
    }
    
    public enum VALUELIST_TYPE
    {
        SINGLE_YEAR, SINGLE_MONTH, SINGLE_DAY, SINGLE_HOUR, CODE, CENTER, PURIFICATION_PLANT, LARGE_BLOCK, MIDDLE_BLOCK, SMALL_BLOCK
    }

    public enum FILE_TYPE
    {
        EXCEL, BITMAP
    }

    public enum WORKBOOK_TYPE
    {
        CONTENT, TEMPLATE
    }
}
