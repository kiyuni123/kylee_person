﻿using System.Text;
using System.Collections;
using WaterNet.WV_Common.work;

namespace WaterNet.WV_Common.helper
{
    public class SqlHelper : BaseWork
    {
        public static string GetTagNameByLocationCode(string loc_code, string br_code, string io_gbn)
        {
            return "";
        }

        /// <summary>
        /// 지역코드를 기준으로 매핑된 태크명을 반환
        /// 소블록인 경우 유입태그를 찾는다, 
        /// 중블록, 대블록인 경우 유출태그를 찾는다.
        /// </summary>
        /// <param name="loc_code">지역코드</param>
        /// <param name="br_code">태그구분코드</param>
        /// <param name="isSingle">하위블록을 조회할지 여부</param>
        /// <param name="isMiddleIo">계측기가 블록내부인경우 true 설정</param>
        /// <returns></returns>
        public static string GetTagNameByLocationCode(string loc_code, string br_code, bool isSingle, bool isMiddleIo)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc.loc_code                                                                                                                       ");
            query.AppendLine("      ,loc.lblock                                                                                                                         ");
            query.AppendLine("      ,loc.mblock                                                                                                                         ");
            query.AppendLine("      ,loc.sblock                                                                                                                         ");
            query.AppendLine("      ,iih.tagname                                                                                                                        ");
            query.AppendLine("      ,iih.description                                                                                                                    ");
            query.AppendLine("  from                                                                                                                                    ");
            query.AppendLine("      (                                                                                                                                   ");

            query.Append(GetBlockInfoByLocationCode(loc_code, isSingle));
            
            query.AppendLine("     ) loc                                                                                                                                ");
            query.AppendLine("     ,if_ihtags iih                                                                                                                       ");
            query.AppendLine("where 1 = 1                                                                                                                               ");
            query.AppendLine("  and iih.loc_code = loc.loc_code                                                                                                         ");
            query.Append("  and iih.br_code = ").AppendLine(br_code);
            query.AppendLine("  and iih.use_yn = 'Y'                                                                                                                    ");
            
            if (isMiddleIo)
            {
                query.AppendLine("  and iih.io_gbn = 'MID'                                                                                  ");
            }
            else if(!isMiddleIo)
            {
                query.AppendLine("  and iih.io_gbn = decode(loc.sblock, null, 'OUT', 'IN')                                                                                  ");
            }

            return query.ToString();
        }

        public static string GetBlockByLocationCode(Hashtable parameter, bool isSingle)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c1.loc_code                                                                                                              ");
            query.AppendLine("      ,c1.sgccd                                                                                                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                         ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))  ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                         ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                          ");
            query.AppendLine("      ,c1.ord                                                                                                                   ");
            query.AppendLine("  from                                                                                                                          ");
            query.AppendLine("      (                                                                                                                         ");
            query.AppendLine("       select sgccd                                                                                                             ");
            query.AppendLine("             ,loc_code                                                                                                          ");
            query.AppendLine("             ,ploc_code                                                                                                         ");
            query.AppendLine("             ,loc_name                                                                                                          ");
            query.AppendLine("             ,ftr_idn                                                                                                           ");
            query.AppendLine("             ,ftr_code                                                                                                          ");
            query.AppendLine("             ,rel_loc_name                                                                                                      ");
            query.AppendLine("             ,rownum ord                                                                                                        ");
            query.AppendLine("         from cm_location                                                                                                       ");
            query.AppendLine("        where 1 = 1                                                                                                             ");
            if (isSingle)
            {
                query.Append("          and loc_code = ").Append("'").Append(parameter["LOC_CODE"].ToString()).AppendLine("'");
            }
            query.Append("          and ftr_code = decode('").Append(parameter["FTR_CODE"].ToString()).Append("', 'BZ001', 'BZ002', decode('").Append(parameter["FTR_CODE"].ToString()).AppendLine("', 'BZ002', 'BZ003', 'BZ003'))");
            query.Append("        start with loc_code = '").Append(parameter["LOC_CODE"].ToString()).AppendLine("'                                        ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                   ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                   ");
            query.AppendLine("      ) c1                                                                                                                      ");
            query.AppendLine("       ,cm_location c2                                                                                                          ");
            query.AppendLine("       ,cm_location c3                                                                                                          ");
            query.AppendLine(" where 1 = 1                                                                                                                    ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                            ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                            ");
            query.AppendLine(" order by c1.ord                                                                                                                ");

            return query.ToString();
        }


        public static string GetBlockInfoByLocationCode(string loc_code, bool isSingle)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c1.loc_code                                                                                                                              ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))                 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                                         ");

            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))                  ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                                         ");

            query.AppendLine("      ,c1.rel_loc_name                                                                                                                          ");
            query.AppendLine("      ,c1.ord                                                                                                                                   ");
            query.AppendLine("  from                                                                                                                                          ");
            query.AppendLine("      (                                                                                                                                         ");
            query.AppendLine("       select sgccd                                                                                                                             ");
            query.AppendLine("             ,loc_code                                                                                                                          ");
            query.AppendLine("             ,ploc_code                                                                                                                         ");
            query.AppendLine("             ,loc_name                                                                                                                          ");
            query.AppendLine("             ,ftr_idn                                                                                                                           ");
            query.AppendLine("             ,ftr_code                                                                                                                          ");
            query.AppendLine("             ,rel_loc_name                                                                                                                      ");
            query.AppendLine("             ,rownum ord                                                                                                                        ");
            query.AppendLine("         from cm_location                                                                                                                       ");
            query.AppendLine("        where 1 = 1                                                                                                                             ");

            if (isSingle)
            {
                query.Append("          and loc_code = ").Append("'").Append(loc_code).AppendLine("'");
            }

            query.Append("        start with loc_code = ").Append("'").Append(loc_code).AppendLine("'");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                   ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                    ");
            query.AppendLine("      ) c1                                                                                                                                      ");
          query.AppendLine("       ,cm_location c2                                                                                                                          ");
            query.AppendLine("       ,cm_location c3                                                                                                                          ");
            query.AppendLine(" where 1 = 1                                                                                                                                    ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                            ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                            ");
            query.AppendLine(" order by c1.ord                                                                                                                                ");

            return query.ToString();
        }

        public static string GetBlockInfoByBlockCode(string block_kind, string block_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c1.loc_code                                                                                                                  ");
            query.AppendLine("      ,c1.sgccd                                                                                                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))     ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))      ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                             ");
            query.AppendLine("                                                                                                                                    ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                              ");
            query.AppendLine("      ,c1.ord                                                                                                                       ");
            query.AppendLine("  from                                                                                                                              ");
            query.AppendLine("      (                                                                                                                             ");
            query.AppendLine("       select sgccd                                                                                                                 ");
            query.AppendLine("             ,loc_code                                                                                                              ");
            query.AppendLine("             ,ploc_code                                                                                                             ");
            query.AppendLine("             ,loc_name                                                                                                              ");
            query.AppendLine("             ,ftr_idn                                                                                                               ");
            query.AppendLine("             ,ftr_code                                                                                                              ");
            query.AppendLine("             ,rel_loc_name                                                                                                          ");
            query.AppendLine("             ,rownum ord                                                                                                            ");
            query.AppendLine("         from cm_location                                                                                                           ");
            query.AppendLine("        where 1 = 1                                                                                                                 ");

            query.Append("          and ftr_idn = ").Append("'").Append(block_code).AppendLine("'");
            query.Append("        start with ftr_code = ").Append("'").Append(block_kind).AppendLine("'");
            
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                       ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                        ");
            query.AppendLine("      ) c1                                                                                                                          ");
            query.AppendLine("       ,cm_location c2                                                                                                              ");
            query.AppendLine("       ,cm_location c3                                                                                                              ");
            query.AppendLine(" where 1 = 1                                                                                                                        ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                ");
            query.AppendLine(" order by c1.ord                                                                                                                    ");

            return query.ToString();
        }
    }
}
