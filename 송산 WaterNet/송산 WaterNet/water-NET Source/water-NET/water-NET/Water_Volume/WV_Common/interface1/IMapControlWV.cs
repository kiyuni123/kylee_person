﻿using System.Collections;
using ESRI.ArcGIS.Controls;
using System.Windows.Forms;

namespace WaterNet.WV_Common.interface1
{
    public interface IMapControlWV
    {
        void MoveToBlock(Hashtable parameter);
        void MoveFocusAt(string layerName, string whereCase, int scale);
        void SelectedFeatureByBlock(Hashtable parameter);
        IMapControl3 IMapControl3
        {
            get;
        }
        ToolStripButton ToolActionCommand
        {
            get;
        }
        AxMapControl AxMap
        {
            get;
        }
        void SetLeakageMoniteringRenderer(Hashtable parameter);
    }
}
