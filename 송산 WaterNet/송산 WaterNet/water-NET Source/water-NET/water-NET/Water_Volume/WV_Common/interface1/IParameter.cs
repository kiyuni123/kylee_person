﻿using System.Collections;

namespace WaterNet.WV_Common.interface1
{
    public interface IParameter
    {
        IParameter InitializeParameter();
        Hashtable Parameters { get; set; }
    }
}
