﻿using System;
using System.Windows.Forms;

namespace WaterNet.WV_Common.manager
{
    public class LayoutManager
    {
    }

    public class TableLayout
    {
        public TableLayout(TableLayoutPanel container, Panel chartPanel, Panel tablePanel, Button chartBtn, Button tableBtn)
        {
            this.container = container;
            this.chartPanel = chartPanel;
            this.tablePanel = tablePanel;
            this.chartBtn = chartBtn;
            this.tableBtn = tableBtn;

            this.chartBtn.Click += new EventHandler(AutoLayout);
            this.tableBtn.Click += new EventHandler(AutoLayout);
        }

        public int DefaultChartHeight
        {
            set
            {
                this.defaultChartHeight = value;
                AutoLayout();
            }
        }

        public int DefaultTableHeight
        {
            set
            {
                this.defaultTableHeight = value;
                AutoLayout();
            }
        }

        public void AutoLayout(Object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (btn != null)
            {
                if (btn.Equals(chartBtn))
                {
                    //if (cVisible)
                    //{
                    //    cVisible = false;
                    //}
                    //else
                    //{
                    //    cVisible = true;
                    //}
                    if (tVisible)
                    {
                        tVisible = false;
                        cVisible = true;
                    }
                    else
                    {
                        tVisible = true;
                    }
                }
                else if (btn.Equals(tableBtn))
                {
                    //if (tVisible)
                    //{
                    //    tVisible = false;
                    //}
                    //else
                    //{
                    //    tVisible = true;
                    //}
                    if (cVisible)
                    {
                        cVisible = false;
                        tVisible = true;
                    }
                    else
                    {
                        cVisible = true;
                    }
                }
            }
            AutoLayout();
        }

        private void AutoLayout()
        {
            if (tVisible && cVisible)
            {
                chartPanel.Visible = true;
                tablePanel.Visible = true;

                container.RowStyles[0].SizeType = SizeType.Percent;
                container.RowStyles[1].SizeType = SizeType.Percent;

                container.RowStyles[0].Height = this.defaultChartHeight;
                container.RowStyles[1].Height = this.defaultTableHeight;
            }

            if (!tVisible && cVisible)
            {
                chartPanel.Visible = true;
                tablePanel.Visible = false;

                container.RowStyles[0].SizeType = SizeType.Percent;
                container.RowStyles[1].SizeType = SizeType.AutoSize;

                container.RowStyles[0].Height = 100;
            }

            if (tVisible && !cVisible)
            {
                chartPanel.Visible = false;
                tablePanel.Visible = true;

                container.RowStyles[0].SizeType = SizeType.AutoSize;
                container.RowStyles[1].SizeType = SizeType.Percent;

                container.RowStyles[1].Height = 100;
            }
        }

        private bool cVisible = true;
        private bool tVisible = true;

        private Button chartBtn;
        private Button tableBtn;

        private Panel chartPanel;
        private Panel tablePanel;

        private TableLayoutPanel container;

        private int defaultChartHeight = 40;
        private int defaultTableHeight = 60;
    }

    public class SearchboxLayout
    {
        public SearchboxLayout(Form form, GroupBox groupbox)
        {
            form.SizeChanged += new EventHandler(form_SizeChanged);
            this.groupbox = groupbox;
            form_SizeChanged(form, null);
        }

        public int DefaultTopWidth
        {
            set
            {
                this.defaultFormWidth = value;
            }
        }

        public int DefaultGroupboxMinHeight
        {
            set
            {
                this.DefaultGroupboxMinHeight = value;
            }
        }

        public int DefaultGroupboxMaxHeight
        {
            set
            {
                this.defaultGroupboxMaxHeight = value;
            }
        }

        private void form_SizeChanged(object sender, EventArgs e)
        {
            Form form = sender as Form;

            if (form != null)
            {
                if (form.Width >= defaultFormWidth && groupbox.Height != defaultGroupboxMinHeight)
                {
                    groupbox.Height = defaultGroupboxMinHeight;
                }

                if (form.Width <= (defaultFormWidth - 1) && groupbox.Height != defaultGroupboxMaxHeight)
                {
                    groupbox.Height = defaultGroupboxMaxHeight;
                }
            }
        }

        private int defaultFormWidth = 985;
        private int defaultGroupboxMinHeight = 86;
        private int defaultGroupboxMaxHeight = 116;
        private GroupBox groupbox;
    }
}
