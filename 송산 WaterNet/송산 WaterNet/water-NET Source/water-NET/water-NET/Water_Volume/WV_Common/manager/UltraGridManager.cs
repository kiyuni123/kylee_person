﻿using System;
using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WaterNetCore;
using System.Drawing;
using System.Data;
using Infragistics.Win;
using System.Windows.Forms;
using System.Collections;

namespace WaterNet.WV_Common.manager
{
    public class UltraGridManager
    {
        public UltraGridManager() { }

        private IList<UltraGrid> _items = new List<UltraGrid>();

        public IList<UltraGrid> Items
        {
            get
            {
                return this._items;
            }
        }

        public void Add(UltraGrid item)
        {
            this._items.Add(item);
            this.InitializeGrid(item);
            this.InitializeEvent(item);
        }

        private void InitializeGrid(UltraGrid item)
        {
            FormManager.SetGridStyle(item);
            item.DisplayLayout.Appearance.BorderColor = SystemColors.InactiveCaption;
            item.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.ColumnChooserButton;
            item.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            item.Font = new Font("굴림", 9f);


            foreach (UltraGridColumn gridColumn in item.DisplayLayout.Bands[0].Columns)
            {
                gridColumn.Header.Appearance.TextVAlign = VAlign.Middle;
                gridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            }

            item.Select();
        }

        public void SetRowClick(UltraGrid item, bool isClick)
        {
            if (isClick)
            {
                item.DisplayLayout.Override.ActiveRowAppearance.BackColor = SystemColors.Highlight;
                item.DisplayLayout.Override.ActiveRowAppearance.ForeColor = SystemColors.HighlightText;
            }

            if (!isClick)
            {
                item.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.Empty;
                item.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Empty;
            }
        }

        public void SetCellClick(UltraGrid item)
        {
            item.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;
            item.DisplayLayout.Override.SelectTypeCell = SelectType.Single;
            item.DisplayLayout.SelectionOverlayColor = Color.Empty;
            item.DisplayLayout.SelectionOverlayBorderColor = Color.Blue;
            item.DisplayLayout.Override.ActiveCellBorderThickness = 3;
            item.DisplayLayout.Override.ActiveCellAppearance.BorderColor = Color.Blue;
            item.DisplayLayout.Override.ActiveCellAppearance.BorderColor2 = Color.FromArgb(128, 128, 255);
        }


        private void InitializeEvent(UltraGrid item)
        {
            item.KeyDown += new KeyEventHandler(item_KeyDown);
        }

        private void item_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //((UltraGrid)sender).UpdateData();
                ((UltraGrid)sender).PerformAction(UltraGridAction.ExitEditMode);
            }
        }

        public void ChangeRowColor(UltraGridRow row, DataRowState state)
        {
            if (state == DataRowState.Added)
            {
                row.Appearance.BackColor = Color.Aqua;
            }

            if (state == DataRowState.Deleted)
            {
                row.Appearance.BackColor = Color.Orange;
            }

            if (state == DataRowState.Modified)
            {
                row.Appearance.BackColor = Color.Yellow;
            }
        }

        public DataRowState GetDataRowState(UltraGridRow row)
        {
            DataRowState state = DataRowState.Unchanged;

            if (row.Appearance.BackColor == Color.Aqua)
            {
                state = DataRowState.Added;
            }
            if (row.Appearance.BackColor == Color.Orange)
            {
                state = DataRowState.Deleted;
            }
            if (row.Appearance.BackColor == Color.Yellow)
            {
                state = DataRowState.Modified;
            }
            return state;
        }
        

        /// <summary>
        /// 그리드매니저 클래스에 등록된 그리드에 데이터소스를 설정한다.
        /// 데이터소스의 타입은 데이터셋이며 정적컬럼의 테이블이름과 동적컬럼의 테이블이름을 인자로 갖는다.
        /// 동적컬럼인경우는 검색조건에 따라 변할수 있는 컬럼이며 통계성 데이터는 세로 데이터를 날짜를 기준
        /// 가로데이터로 변환해서 그리드 셀에 값을 설정한다.'
        /// 정적컬럼과 동적컬럼의 값을 데이터베이스에서 검색할때는 정렬에 주의해야 한다.
        /// 하나의 동적컬럼이 세로로 여러값을 가질때 추가인자로서 값의 갯수를 준다.
        /// ex)      1월    2월
        ///  계      100     80      count 1
        ///  마곡1    80     60      count 2
        ///  마곡2    20     20      count 3
        ///  valueCount = 3
        /// </summary>
        /// <param name="dataSource">System.Data.DataSet</param>
        /// <param name="dataMember">DataTable Name</param>
        public void SetDataSource(UltraGrid item, object bindData, object unBindData, int unBindColumnCount)
        {
            try
            {
                item.DataSource = bindData;
                this.SetUnBoundDataSource(item, unBindData, unBindColumnCount);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SetDataSource(UltraGrid item, object bindData, object unBindData, int unBindColumnCount, string format)
        {
            try
            {
                item.DataSource = bindData;
                this.SetUnBoundDataSource(item, unBindData, unBindColumnCount, format);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 그래드매니저 클래스에 등록된 그리드의 동적컬럼의 데이터를 기준으로 컬럼과 셀 값을 설정한다.
        /// 인자로 데이터셋과 테이블이름과 세로값의 갯수를 갖는다.
        /// </summary>
        /// <param name="dataSource"></param>
        private void SetUnBoundDataSource(UltraGrid item, object unBindData, int unBindColumnCount)
        {
            this.RemoveDynamicComumns(item);

            RowsCollection rows = item.DisplayLayout.Rows;
            ColumnsCollection columns = item.DisplayLayout.Bands[0].Columns;

            DataTable table = null;

            if (unBindData is DataTable)
            {
                table = unBindData as DataTable;
            }

            if (table == null || table.Rows.Count == 0)
            {
                return;
            }

            int createColumnCount = 0;
            int rowCursorIndex = 0;
            bool isCreateColumnComplete = false;

            item.BeginUpdate();

            for (int i = 0; i < rows.Count; i++)
            {
                DataRowCollection dataRows = table.Rows;

                if (!isCreateColumnComplete)
                {
                    foreach (DataRow dataRow in dataRows)
                    {
                        string columnKey = dataRow[0].ToString();
                        object columnValue = dataRow[0];

                        if (!columns.Exists(columnKey))
                        {
                            this.AddColumn(item, columnKey);
                            createColumnCount++;
                        }
                        else if (columns.Exists(columnKey))
                        {
                            break;
                        }
                    }
                }

                isCreateColumnComplete = true;

                if (isCreateColumnComplete)
                {
                    int columnWriteCount = 0;

                    for (int k = rowCursorIndex; k < dataRows.Count; k++)
                    {
                        for (int j = 0; j < unBindColumnCount; j++)
                        {
                            object columnValue = dataRows[rowCursorIndex][j + 1];

                            rows.GetRowWithListIndex(i + j, false).Cells[columns.BoundColumnsCount + columnWriteCount].Value = columnValue;

                            if (j == (unBindColumnCount - 1))
                            {
                                columnWriteCount++;
                                break;
                            }
                        }
                        rowCursorIndex++;
                        if (createColumnCount == columnWriteCount)
                        {
                            i += unBindColumnCount - 1;
                            break;
                        }
                    }
                }
            }
            item.EndUpdate();

            foreach (UltraGridRow row in item.Rows)
            {
                row.CancelUpdate();
            }
        }

        private void SetUnBoundDataSource(UltraGrid item, object unBindData, int unBindColumnCount, string format)
        {
            this.RemoveDynamicComumns(item);

            RowsCollection rows = item.DisplayLayout.Rows;
            ColumnsCollection columns = item.DisplayLayout.Bands[0].Columns;

            DataTable table = null;

            if (unBindData is DataTable)
            {
                table = unBindData as DataTable;
            }

            if (table == null || table.Rows.Count == 0)
            {
                return;
            }

            int createColumnCount = 0;
            int rowCursorIndex = 0;
            bool isCreateColumnComplete = false;

            item.BeginUpdate();

            for (int i = 0; i < rows.Count; i++)
            {
                DataRowCollection dataRows = table.Rows;

                if (!isCreateColumnComplete)
                {
                    foreach (DataRow dataRow in dataRows)
                    {
                        string columnKey = dataRow[0].ToString();
                        object columnValue = dataRow[0];

                        if (!columns.Exists(columnKey))
                        {
                            this.AddColumn(item, columnKey, format);
                            createColumnCount++;
                        }
                        else if (columns.Exists(columnKey))
                        {
                            break;
                        }
                    }
                }

                isCreateColumnComplete = true;

                if (isCreateColumnComplete)
                {
                    int columnWriteCount = 0;

                    for (int k = rowCursorIndex; k < dataRows.Count; k++)
                    {
                        for (int j = 0; j < unBindColumnCount; j++)
                        {
                            object columnValue = dataRows[rowCursorIndex][j + 1];

                            rows.GetRowWithListIndex(i + j, false).Cells[columns.BoundColumnsCount + columnWriteCount].Value = columnValue;

                            if (j == (unBindColumnCount - 1))
                            {
                                columnWriteCount++;
                                break;
                            }
                        }
                        rowCursorIndex++;
                        if (createColumnCount == columnWriteCount)
                        {
                            i += unBindColumnCount - 1;
                            break;
                        }
                    }
                }
            }
            item.EndUpdate();

            foreach (UltraGridRow row in item.Rows)
            {
                row.CancelUpdate();
            }
        }

        /// <summary>
        /// 그래드매니저 클래스에 등록된 그리드의 컬럼을 추가하고 스타일을 설정한다.
        /// 인자로 컬럼의 키값을 갖으며 기본적인 컬럼의 스타일은 수치형데이터에 스타일이다.
        /// </summary>
        /// <param name="key">Column Key Property</param>
        private void AddColumn(UltraGrid item, string key)
        {
            int columnsCount = item.DisplayLayout.Bands[0].Columns.Count;
            UltraGridColumn column = item.DisplayLayout.Bands[0].Columns.Insert(columnsCount, key);
            column.DataType = typeof(int);
            column.Format = "###,###,###";

            column.Width = 60;
            column.Key = key;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Caption = key;
        }

        /// <summary>
        /// 그래드매니저 클래스에 등록된 그리드의 컬럼을 추가하고 스타일을 설정한다.
        /// 인자로 컬럼의 키값을 갖으며 기본적인 컬럼의 스타일은 수치형데이터에 스타일이다.
        /// </summary>
        /// <param name="key">Column Key Property</param>
        private void AddColumn(UltraGrid item, string key, string format)
        {
            int columnsCount = item.DisplayLayout.Bands[0].Columns.Count;
            UltraGridColumn column = item.DisplayLayout.Bands[0].Columns.Insert(columnsCount, key);
            column.DataType = typeof(double);
            column.Format = format;

            column.Width = 60;
            column.Key = key;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Caption = key;
        }


        /// <summary>
        /// 그래드매니저 클래스에 등록된 그리드의 동적으로 추가된 컬럼을 모두 삭제한다.
        /// 삭제를 하지 않으면 컬럼추가시에 기존에 생성한 컬럼과 남아있는 컬럼이 뒤섞이게 된다.
        /// </summary>
        public void RemoveDynamicComumns(UltraGrid item)
        {
            ColumnsCollection columns = item.DisplayLayout.Bands[0].Columns;
            for (int i = columns.Count - 1; i >= 0; i--)
            {
                if (!columns[i].IsBound)
                {
                    columns.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// 데이터 소스 삭제
        /// </summary>
        public void ClearDataSource(UltraGrid item)
        {
            if (item.DataSource != null)
            {
                if (item.DataSource is BindingSource)
                {
                    BindingSource source = item.DataSource as BindingSource;
                    source.Clear();
                }

                if (item.DataSource is IList)
                {
                    IList source = item.DataSource as IList;
                    source.Clear();
                }

                if (item.DataSource is DataTable)
                {
                    DataTable source = item.DataSource as DataTable;
                    source.Clear();
                }
            }
        }

        /// <summary>
        /// 기본설정 : 그리드의 빈값을 설정한다.
        /// 빈 로우 생성/
        /// </summary>
        public void DefaultColumnsMapping(UltraGrid ultraGrid)
        {
            DataTable dataTable = new DataTable();

            foreach (UltraGridColumn gridColumn in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                dataTable.Columns.Add(gridColumn.Key, typeof(double));
            }

            DataRow dataRow = dataTable.NewRow();

            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                dataRow[dataColumn.ColumnName] = DBNull.Value;
            }

            dataTable.Rows.Add(dataRow);

            ultraGrid.DataSource = dataTable;
        }

        /// <summary>
        /// 기본설정 : 그리드의 빈값을 설정한다.
        /// 빈 로우 생성/
        /// </summary>
        public void DefaultColumnsMapping(UltraGrid ultraGrid, Type t)
        {
            DataTable dataTable = new DataTable();

            foreach (UltraGridColumn gridColumn in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                dataTable.Columns.Add(gridColumn.Key, t);
            }

            DataRow dataRow = dataTable.NewRow();

            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                dataRow[dataColumn.ColumnName] = DBNull.Value;
            }

            dataTable.Rows.Add(dataRow);

            ultraGrid.DataSource = dataTable;
        }

        /// <summary>
        /// 기본설정 : 그리드의 빈값을 설정한다.
        /// 로우 생성 안함
        /// </summary>
        public void DefaultColumnsMapping2(UltraGrid ultraGrid)
        {
            DataTable dataTable = new DataTable();

            foreach (UltraGridColumn gridColumn in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                dataTable.Columns.Add(gridColumn.Key);
            }

            ultraGrid.DataSource = dataTable;
        }

        /// <summary>
        /// 그룹명을 설정한다.
        /// </summary>
        /// <param name="year"></param>
        public void SetGridGroupCaption(UltraGrid ultraGrid, string groupname, string caption)
        {
            ultraGrid.DisplayLayout.Bands[0].Groups[groupname].Header.Caption = caption;
        }
    }
}
