﻿using System;
using System.Linq;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Excel;
using System.IO;
using Infragistics.Win.UltraWinGrid.ExcelExport;
using ChartFX.WinForms;
using System.Windows.Forms;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using System.Drawing;

namespace WaterNet.WV_Common.manager
{
    /// <summary>
    /// 수량관리에서 엑셀에 관련된 기능을 관리한다.
    /// </summary>
    public class ExcelManager
    {
        /// <summary>
        /// 그리드의 엑셀로 내보내는 기능을 담당한다.
        /// </summary>
        private UltraGridExcelExporter exporter = null;
        
        private Workbook content = null;
        private Workbook template = null;

        public Workbook Workbook
        {
            get
            {
                return this.content;
            }
        }

        public UltraGridExcelExporter Exporter
        {
            get
            {
                return this.exporter;
            }
        }

        public ExcelManager()
        {
            this.exporter = new UltraGridExcelExporter();
            this.content = new Workbook();
        }

        public UltraGridExcelExporter UltraGridExcelExporter
        {
            get
            {
                return this.exporter;
            }
        }

        public void Clear()
        {
            this.content.Worksheets.Clear();
            if (this.template != null)
            {
                this.template.Worksheets.Clear();
                this.template = null;
            }
        }

        public void Load(Stream stream, WORKBOOK_TYPE type)
        {
            if (type == WORKBOOK_TYPE.CONTENT)
            {
                this.content = Workbook.Load(stream);
            }
            if (type == WORKBOOK_TYPE.TEMPLATE)
            {
                this.template = Workbook.Load(stream);
            }
        }

        public void Load(Stream stream)
        {
            this.content = Workbook.Load(stream);
        }

        public void AddWorksheet(UltraGrid ultraGrid, string worksheetName, WORKBOOK_TYPE type)
        {
            if (type == WORKBOOK_TYPE.CONTENT)
            {
                this.AddWorksheet(ultraGrid, this.content, worksheetName, 0, 0);
            }
            if (type == WORKBOOK_TYPE.TEMPLATE)
            {
                this.AddWorksheet(ultraGrid, this.template, worksheetName, 0, 0);
            }
        }

        public void AddWorksheet(UltraGrid ultraGrid, string worksheetName, int startRow, int startColumn)
        {
            this.AddWorksheet(ultraGrid, this.content, worksheetName, startRow, startColumn);
        }

        public void AddWorksheet(UltraGrid ultraGrid, string worksheetName, int startRow, int startColumn, WORKBOOK_TYPE type)
        {
            if (type == WORKBOOK_TYPE.CONTENT)
            {
                this.AddWorksheet(ultraGrid, this.content, worksheetName, startRow, startColumn);
            }
            if (type == WORKBOOK_TYPE.TEMPLATE)
            {
                this.AddWorksheet(ultraGrid, this.template, worksheetName, startRow, startColumn);
            }
        }

        public void AddWorksheet(UltraGrid ultraGrid, string worksheetName)
        {
            this.AddWorksheet(ultraGrid, this.content, worksheetName, 0, 0);
        }

        public void AddWorksheet(UltraGrid ultraGrid, Workbook workbook, string worksheetName, int startRow, int startColumn)
        {
            exporter.Export(ultraGrid, this.GetWorksheet(workbook, worksheetName), startRow, startColumn);
        }


        public void AddWorksheet(Chart chart, string worksheetName, int startRow, int startCell, int endRow, int endCell)
        {
            this.AddWorksheet(chart, worksheetName, startRow, startCell, endRow, endCell, WORKBOOK_TYPE.CONTENT);
        }

        public void AddWorksheet(Chart chart, string worksheetName, int startRow, int startCell, int endRow, int endCell, WORKBOOK_TYPE type)
        {
            Worksheet worksheet = null;

            if (type == WORKBOOK_TYPE.CONTENT)
            {
                worksheet = this.GetWorksheet(this.content, worksheetName);
            }
            if (type == WORKBOOK_TYPE.TEMPLATE)
            {
                worksheet = this.GetWorksheet(this.template, worksheetName);
            }

            if (worksheet == null)
            {
                return;
            }

            string tempFile = Utils.GetTempFileName("waternet-chart", FILE_TYPE.BITMAP);

            chart.Export(FileFormat.Bitmap, tempFile);

            if (!File.Exists(tempFile))
            {
                return;
            }

            Stream stream = File.OpenRead(tempFile);
            Image image = new Bitmap(stream);
            WorksheetShape shape = new WorksheetImage(image);
            shape.TopLeftCornerCell = worksheet.Rows[startRow].Cells[startCell];
            shape.BottomRightCornerCell = worksheet.Rows[endRow].Cells[endCell];
            worksheet.Shapes.Add(shape);
        }

        public void WriteContentToTemplete(int contentSheetIndex, int templeteSheerIndex, int templeteStartRow, int templeteColumn)
        {
            int row = 0;
            int column = 0;

            for (int i = 0; i < content.Worksheets[contentSheetIndex].Rows.Count(); i++)
            {
                for (int j = 0; j < content.Worksheets[contentSheetIndex].Rows[i].Cells.Count(); j++)
                {
                    row = i + templeteStartRow;
                    column = j + templeteColumn;

                    double value = 0;
                    bool result = false;

                    if (content.Worksheets[contentSheetIndex].Rows[i].Cells[j].Value != null)
                    {
                        result = double.TryParse(content.Worksheets[contentSheetIndex].Rows[i].Cells[j].Value.ToString(), out value);
                    }

                    if (result)
                    {
                        this.template.Worksheets[templeteSheerIndex].Rows[row].Cells[column].Value = value;
                    }
                    else
                    {
                        this.template.Worksheets[templeteSheerIndex].Rows[row].Cells[column].Value = content.Worksheets[contentSheetIndex].Rows[i].Cells[j].Value;

                    }
                }
            }
        }

        public void WriteLine(int sheetIndex, int startRow, int startColumn, string value, WORKBOOK_TYPE type)
        {
            if (type == WORKBOOK_TYPE.CONTENT)
            {
                this.content.Worksheets[sheetIndex].Rows[startRow].Cells[startColumn].Value = value;
            }
            if (type == WORKBOOK_TYPE.TEMPLATE)
            {
                this.template.Worksheets[sheetIndex].Rows[startRow].Cells[startColumn].Value = value;
            }
        }

        public void WriteLine(int sheetIndex, int startRow, int startColumn, object value, WORKBOOK_TYPE type)
        {
            if (type == WORKBOOK_TYPE.CONTENT)
            {
                this.content.Worksheets[sheetIndex].Rows[startRow].Cells[startColumn].Value = value;
            }
            if (type == WORKBOOK_TYPE.TEMPLATE)
            {
                this.template.Worksheets[sheetIndex].Rows[startRow].Cells[startColumn].Value = value;
            }
        }

        public void WriteLine(int sheetIndex, int startRow, int startColumn, string value)
        {
            this.content.Worksheets[sheetIndex].Rows[startRow].Cells[startColumn].Value = value;
        }

        public void WriteLine(int sheetIndex, int startRow, int startColumn, object value)
        {
            this.content.Worksheets[sheetIndex].Rows[startRow].Cells[startColumn].Value = value;
        }

        public void Save(string strTitle, bool isOpen)
        {
            this.Save(strTitle, isOpen, WORKBOOK_TYPE.CONTENT);
        }

        public void Save(string strTitle, bool isOpen, WORKBOOK_TYPE type)
        {
            string fileName = string.Empty;

            try
            {
                if (type == WORKBOOK_TYPE.CONTENT)
                {
                    fileName = this.Save(this.content, strTitle);
                }
                if (type == WORKBOOK_TYPE.TEMPLATE)
                {
                    fileName = this.Save(this.template, strTitle);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }

            if (isOpen)
            {
                this.Open(fileName);
            }
        }

        public void Open(string strTitle, WORKBOOK_TYPE type)
        {
            strTitle = strTitle + "-" + DateTime.Now.ToString("yyyyMMdd");

            string tempFile = Utils.GetTempFileName(strTitle, FILE_TYPE.EXCEL);

            if (type == WORKBOOK_TYPE.CONTENT)
            {
                if (this.content == null)
                {
                    return;
                }
                this.content.Save(tempFile);
            }

            if (type == WORKBOOK_TYPE.TEMPLATE)
            {
                if (this.template == null)
                {
                    return;
                }
                this.template.Save(tempFile);
            }

            System.Diagnostics.Process oProc = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo oProcStartInfor = new System.Diagnostics.ProcessStartInfo("Excel.exe");
            oProcStartInfor.FileName = tempFile;
            oProcStartInfor.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            oProc.StartInfo = oProcStartInfor;
            oProc.Start();
        }

        public string Save(Workbook workbook, string strTitle)
        {
            string today = string.Empty;
            string strFullName = string.Empty;

            SaveFileDialog oSFD = new SaveFileDialog();
            today = DateTime.Now.ToString("yyyyMMdd");

            oSFD.Filter = "Excel files (*.xls)|*.xls";
            oSFD.FilterIndex = 1;
            oSFD.RestoreDirectory = true;
            oSFD.FileName = strTitle + "-" + today + "";

            if (oSFD.ShowDialog() == DialogResult.OK)
            {
                workbook.Save(oSFD.FileName);
            }
            else
            {
                oSFD.FileName = null;
            }
            return oSFD.FileName;
        }

        public void Open(string fileName)
        {
            if (fileName == null || fileName == "")
            {
                return;
            }

            if (DialogResult.Yes == MessageBox.Show("엑셀파일을 열어보시겠습니까?", "내용보기", MessageBoxButtons.YesNo))
            {
                System.Diagnostics.Process oProc = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo oProcStartInfor = new System.Diagnostics.ProcessStartInfo("Excel.exe");

                oProcStartInfor.FileName = fileName;
                oProcStartInfor.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                oProc.StartInfo = oProcStartInfor;
                oProc.Start();
            }
        }

        private Worksheet GetWorksheet(Workbook workbook, string worksheetName)
        {
            Worksheet worksheet = null;

            foreach (Worksheet ws in workbook.Worksheets)
            {
                if (ws.Name == worksheetName)
                {
                    worksheet = ws;
                }
            }

            if (worksheet == null)
            {
                worksheet = workbook.Worksheets.Add(worksheetName);
            }

            return worksheet;
        }
    }
}
