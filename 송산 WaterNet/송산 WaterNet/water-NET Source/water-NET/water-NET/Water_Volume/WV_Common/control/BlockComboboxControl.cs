﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.work;
using System.Data;
using WaterNet.WV_Common.util;
using System.Collections;
using WaterNet.WV_Common.enum1;

namespace WaterNet.WV_Common.control
{
    /// <summary>
    /// 동적으로 변하는 블록 콤보박스를 컨트롤해준다.
    /// 상위블록 콤보박스를 선택하면 포함관계에 따라 하위블록 콤보박스의 목록을 변경시켜주고
    /// 하위블록 콤보박스를 선택하면 포함관계에 따라 상위블록 콤보박스의 블록을 선택해준다.
    /// 이벤트이상으로 대블록 변경시 소블록이 과다 변경된다.(원인파악중)
    /// </summary>
    public class BlockComboboxControl
    {
        #region Public Instance Constructors

        /// <summary>
        /// 초기화
        /// </summary>
        /// <param name="center">센터 객체(콤보박스)</param>
        /// <param name="jungsu">정수장 객체(콤보박스)</param>
        /// <param name="largeBlock">대블록 객체(콤보박스)</param>
        /// <param name="middleBlock">중블록 객체(콤보박스)</param>
        /// <param name="smallBlock">소블록 객체(콤보박스)</param>
        public BlockComboboxControl(ComboBox center, ComboBox jungsu, ComboBox largeBlock, ComboBox middleBlock, ComboBox smallBlock)
        {
            this.center = center;
            this.jungsu = jungsu;
            this.largeBlock = largeBlock;
            this.middleBlock = middleBlock;
            this.smallBlock = smallBlock;
            InitializeComponent();
        }

        /// <summary>
        /// 초기화
        /// </summary>
        /// <param name="largeBlock">대블록 객체(콤보박스)</param>
        /// <param name="middleBlock">중블록 객체(콤보박스)</param>
        /// <param name="smallBlock">소블록 객체(콤보박스)</param>
        public BlockComboboxControl(ComboBox largeBlock, ComboBox middleBlock, ComboBox smallBlock)
        {
            this.largeBlock = largeBlock;
            this.middleBlock = middleBlock;
            this.smallBlock = smallBlock;
            InitializeComponent();
        }
        #endregion

        
        /// <summary>
        /// 마지막으로 선택한 블록의 정보를 반환함
        /// </summary>
        public Hashtable CurrentBlockInfo
        {
            get
            {
                Hashtable selectedItem = null;

                if (smallBlock != null && smallBlock.SelectedIndex > 0)
                {
                    selectedItem = ConvertDataRowView(smallBlock.SelectedItem as DataRowView);
                }
                else if (middleBlock != null && middleBlock.SelectedIndex > 0)
                {
                    selectedItem = ConvertDataRowView(middleBlock.SelectedItem as DataRowView);
                }
                else if (largeBlock != null && largeBlock.SelectedIndex > 0)
                {
                    selectedItem = ConvertDataRowView(largeBlock.SelectedItem as DataRowView);
                }
                return selectedItem;
            }
        }

        
        /// <summary>
        /// 블록의 기본 설정
        /// DisplayMember,ValueMember 등의 설정을 하고, 데이터 소스를 세팅한다.
        /// </summary>
        private void InitializeComponent()
        {
            Hashtable parameter = new Hashtable();

            if (this.center != null)
            {
                ComboBoxUtils.SetMember(this.center, "LOC_CODE", "LOC_NAME");
            }

            if (this.jungsu != null)
            {
                ComboBoxUtils.SetMember(this.jungsu, "LOC_CODE", "LOC_NAME");
            }

            if (this.largeBlock != null)
            {
                this.largeBlock.SelectedIndexChanged += new EventHandler(SelectedIndexChanged);
                parameter["FTR_CODE"] = "BZ001";
                this.largeBlock.DataSource = blockWork.SelectBlockListByLevel(parameter, "RESULT").Tables[0];
                ComboBoxUtils.SetMember(this.largeBlock, "LOC_CODE", "LOC_NAME");
            }

            if (this.middleBlock != null)
            {
                this.middleBlock.SelectedIndexChanged += new EventHandler(SelectedIndexChanged);
                ComboBoxUtils.SetMember(this.middleBlock, "LOC_CODE", "LOC_NAME");
                if (this.largeBlock == null)
                {
                    parameter["FTR_CODE"] = "BZ002";
                    this.middleBlock.DataSource = blockWork.SelectBlockListByLevel(parameter, "RESULT").Tables[0];
                    ComboBoxUtils.SetMember(this.middleBlock, "LOC_CODE", "LOC_NAME");
                }
            }

            if (this.smallBlock != null)
            {
                ComboBoxUtils.SetMember(this.smallBlock, "LOC_CODE", "LOC_NAME");
                if (this.largeBlock == null && this.middleBlock == null)
                {
                    parameter["FTR_CODE"] = "BZ003";
                    this.smallBlock.DataSource = blockWork.SelectBlockListByLevel(parameter, "RESULT").Tables[0];
                    ComboBoxUtils.SetMember(this.smallBlock, "LOC_CODE", "LOC_NAME");
                }
            }
        }

        /// <summary>
        /// 이벤트 중지 여부
        /// </summary>
        private bool stopEvent = false;

        /// <summary>
        /// 이벤트 중지 여부
        /// </summary>
        public bool StopEvent
        {
            set
            {
                this.stopEvent = value;
            }
        }

        /// <summary>
        /// 선택 인덱스가 변경되었을때 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedIndexChanged(Object sender, EventArgs e)
        {
            //임의 이벤트 제어
            if (stopEvent)
            {
                return;
            }

            DataRowView selectedItem = ((ComboBox)sender).SelectedItem as DataRowView;

            if (selectedItem == null)
            {
                return;
            }

            Hashtable parameter = ConvertDataRowView(selectedItem);

            if (sender.Equals(this.largeBlock))
            {
                //대블록을 전체나 선택을 선택한 경우,,
                //전체 중블록의 목록을 가져오기 위해 중블록의 계층 코드를 부여한다.
                if (parameter["LOC_CODE"].ToString() == "X" || parameter["LOC_CODE"].ToString() == "O")
                {
                    parameter.Remove("LOC_CODE");
                    parameter["FTR_CODE"] = "BZ002";
                }

                this.middleBlock.DataSource = blockWork.SelectBlockListByLevel(parameter, "RESULT").Tables[0];

                if (this.isSelectedUpdate)
                {
                    ComboBoxUtils.AddDefaultOption(this.middleBlock, this.isMiddleSelected);
                }
            }
            else if (sender.Equals(this.middleBlock))
            {
                //중블록을 전체나 선택을 선택한 경우,,
                //전체 소블록의 목록을 가져오기 위해 소블록의 계층 코드를 부여한다.
                if (parameter["LOC_CODE"].ToString() == "X" || parameter["LOC_CODE"].ToString() == "O")
                {
                    parameter.Remove("LOC_CODE");
                    parameter["FTR_CODE"] = "BZ003";
                }
                
                this.smallBlock.DataSource = blockWork.SelectBlockListByLevel(parameter, "RESULT").Tables[0];

                if (this.isSelectedUpdate)
                {
                    ComboBoxUtils.AddDefaultOption(this.smallBlock, this.isSmallSelected);
                }
            }
        }

        private BlockWork blockWork = BlockWork.GetInstance();
        private ComboBox center;
        private ComboBox jungsu;
        private ComboBox largeBlock;
        private ComboBox middleBlock;
        private ComboBox smallBlock;

        private bool isSelectedUpdate = false;
        private bool isSelected = false;

        private bool isCenterSelected = false;
        private bool isJungSuSelected = false;
        private bool isLargeSelected = false;
        private bool isMiddleSelected = false;
        private bool isSmallSelected = false;

        public void AddDefaultOption(LOCATION_TYPE locationType, bool isSelected)
        {
            ComboBox box = null;

            switch (locationType)
            {
                case LOCATION_TYPE.CENTER:
                    this.isCenterSelected = this.isSelected;
                    box = this.center;
                    break;
                case LOCATION_TYPE.PURIFICATION_PLANT:
                    this.isJungSuSelected = this.isSelected;
                    box = this.jungsu;
                    break;
                case LOCATION_TYPE.LARGE_BLOCK:
                    this.isLargeSelected = this.isSelected;
                    box = this.largeBlock;
                    break;
                case LOCATION_TYPE.MIDDLE_BLOCK:
                    this.isMiddleSelected = this.isSelected;
                    box = this.middleBlock;
                    break;
                case LOCATION_TYPE.SMALL_BLOCK:
                    this.isSmallSelected = this.isSelected;
                    box = this.smallBlock;
                    break;
                case LOCATION_TYPE.All:
                    this.isCenterSelected = this.isSelected;
                    this.isJungSuSelected = this.isSelected;
                    this.isLargeSelected = this.isSelected;
                    this.isMiddleSelected = this.isSelected;
                    this.isSmallSelected = this.isSelected;
                    ComboBoxUtils.AddDefaultOption(this.center, this.isSelected);
                    ComboBoxUtils.AddDefaultOption(this.jungsu, this.isSelected);
                    ComboBoxUtils.AddDefaultOption(this.largeBlock, this.isSelected);
                    ComboBoxUtils.AddDefaultOption(this.middleBlock, this.isSelected);
                    ComboBoxUtils.AddDefaultOption(this.smallBlock, this.isSelected);
                    break;
            }

            this.isSelectedUpdate = true;

            if (box == null)
            {
                return;
            }
            ComboBoxUtils.AddDefaultOption(box, isSelected);
        }

        private Hashtable ConvertDataRowView(DataRowView view)
        {
            DataColumnCollection columns = view.Row.Table.Columns;

            Hashtable result = new Hashtable();

            for (int i = 0; i < columns.Count; i++)
            {
                result[columns[i].ColumnName] = view[columns[i].ColumnName];
            }

            return result;
        }
    }
}
