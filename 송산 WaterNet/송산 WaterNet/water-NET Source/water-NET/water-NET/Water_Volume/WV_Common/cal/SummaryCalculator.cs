﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.util;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.WV_Common.cal
{
    public class SummaryCalculator
    {
    }

    public class DF : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            double dl = 0;
            double rl = 0;

            foreach (UltraGridRow gridRow in rows)
            {
                dl += Utils.ToDouble(gridRow.Cells["DF_THFQ"].Value);
                rl += Utils.ToDouble(gridRow.Cells["RL_THFQ"].Value);
            }

            result = dl - rl;

            if (result < 0)
            {
                result = 0;
            }

            //Console.WriteLine("====  "+result.ToString() + "===  " + dl.ToString() + "===  " + rl.ToString());

            return result;
        }
        #endregion
    }

    public class RMS : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            foreach (UltraGridRow gridRow in rows)
            {
                count++;
                this.result += Math.Pow(Utils.ToDouble(gridRow.Cells[summarySettings.SourceColumn.Key].Value), 2);
            }

            return Math.Sqrt(this.result / count);
        }
        #endregion
    }

    public class Percent : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            double t_count = 0;
            double q_count = 0;

            foreach (UltraGridRow gridRow in rows)
            {
                if (gridRow.Cells[summarySettings.SourceColumn.Key].Value != DBNull.Value)
                {
                    q_count++;
                }

                foreach (UltraGridColumn gridColumn in gridRow.Band.Columns)
                {
                    if (gridColumn.DataType != typeof(DateTime) && gridColumn.DataType != typeof(string))
                    {
                        if (gridRow.Cells[gridColumn.Key].Value != DBNull.Value)
                        {
                            t_count++;
                        }
                    }
                }
            }

            if (t_count == 0)
            {
                return this.result;
            }

            this.result = q_count / t_count * 100;

            return this.result;
        }
        #endregion
    }

    public class Count : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            foreach (UltraGridRow gridRow in rows)
            {
                if (gridRow.Cells[summarySettings.SourceColumn.Key].Value != DBNull.Value)
                {
                    this.result++;
                }
            }

            return this.result;
        }
        #endregion
    }

    public class MonthAverage : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = 0; i < rows.Count; i++)
            {
                double value = Utils.ToDouble(rows[i].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }
        #endregion
    }

    public class Week4Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 28; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week3Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 21; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week2Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 14; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week1Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 7; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

}
