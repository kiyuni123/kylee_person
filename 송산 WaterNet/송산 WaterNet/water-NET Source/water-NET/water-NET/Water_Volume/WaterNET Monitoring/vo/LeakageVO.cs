﻿using System;
using System.Collections.Generic;
using WaterNet.WaterNETMonitoring.interface1;
using System.Data;
using WaterNet.WaterNETMonitoring.cal_leakage;
using System.Collections;
using WaterNet.WaterNETMonitoring.work;
using WaterNet.WaterNETMonitoring.cal_leakage2;

namespace WaterNet.WaterNETMonitoring.vo
{
    public class LeakageVO
    {
        //
        private string loc_code = null;
        private DateTime dateTime;

        //
        private DataTable thisYearMNF = null;
        //private DataTable lastYearMNF = null;
        private DataTable thisYearFlow = null;
        //private DataTable lastYearFlow = null;
        //private double thisYearFlowRatio = 0;
        //private double lastYearFlowRatio = 0;

        //산정 집합,
        private List<ICal> calList = new List<ICal>();

        public LeakageVO(string loc_code, DateTime dateTime)
        {
            this.loc_code = loc_code;
            this.dateTime = dateTime;
            this.InitializeDataSetting();
            this.DoMonitering();
            this.WriteResult();
        }

        /// <summary>
        /// 기본세팅
        /// </summary>
        private void InitializeDataSetting()
        {
            Hashtable result = MoniteringWork.GetInstance().SelectMonitering(this.GetParameter());
            this.thisYearMNF = result["thisYearMNF"] as DataTable;
            //this.lastYearMNF = result["lastYearMNF"] as DataTable;
            this.thisYearFlow = result["thisYearFlow"] as DataTable;
            //this.lastYearFlow = result["lastYearFlow"] as DataTable;
            //this.thisYearFlowRatio = Utils.ToDouble(result["thisYearFlowRatio"]);
            //this.lastYearFlowRatio = Utils.ToDouble(result["lastYearFlowRatio"]);
        }

        private Hashtable GetParameter()
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.loc_code;
            parameter["DATETIME"] = this.dateTime;
            return parameter;
        }

        /// <summary>
        /// 감시
        /// </summary>
        private void DoMonitering()
        {
            if (this.calList.Count > 0)
            {
                this.calList.Clear();
            }

            //전일 야간최소유량, 유입유량 데이터 차이값으로 누수판단 (AND 조건 적용)
            calList.Add(new MNFValueGap(this.loc_code, this.dateTime, this.thisYearMNF));
            calList.Add(new FlowValueGap(this.loc_code, this.dateTime, this.thisYearFlow));
            calList.Add(new MNFValueTrend(this.loc_code, this.dateTime, this.thisYearMNF));
            calList.Add(new FlowValueTrend(this.loc_code, this.dateTime, this.thisYearFlow));
            calList.Add(new MaxMNFCompare(this.loc_code, this.dateTime, this.thisYearMNF));

            //2012.01.03 AND조건 주석처리함
            //2012.03.02 AND조건 적용
            //if (calList[0].MonitorResult == "T" && calList[1].MonitorResult == "T")
            //{
            //    calList[0].MonitorResult = "T";
            //    calList[1].MonitorResult = "T";
            //}
            //else if (calList[0].MonitorResult == "F" || calList[1].MonitorResult == "F")
            //{
            //    calList[0].MonitorResult = "F";
            //    calList[1].MonitorResult = "F";
            //}

            //if (calList[2].MonitorResult == "T" && calList[3].MonitorResult == "T")
            //{
            //    calList[2].MonitorResult = "T";
            //    calList[3].MonitorResult = "T";
            //}
            //else if (calList[2].MonitorResult == "F" || calList[3].MonitorResult == "F")
            //{
            //    calList[2].MonitorResult = "F";
            //    calList[3].MonitorResult = "F";
            //}





            ////전일 야간최소유량, 유입유량 데이터 차이값으로 누수판단 (AND 조건 적용)
            //calList.Add(new MNFValueGap(this.loc_code, this.dateTime, this.thisYearMNF));
            //calList.Add(new FlowValueGap(this.loc_code, this.dateTime, this.thisYearFlow));

            //if (calList[0].MonitorResult == "T" && calList[1].MonitorResult == "T")
            //{
            //    calList[0].MonitorResult = "T";
            //    calList[1].MonitorResult = "T";
            //}
            //else if (calList[0].MonitorResult == "F" || calList[1].MonitorResult == "F")
            //{
            //    calList[0].MonitorResult = "F";
            //    calList[1].MonitorResult = "F";
            //}

            ////야간최소유량, 유입유량 데이터 추세 판단 (AND 조건 적용)
            //calList.Add(new MNFValueTrend(this.loc_code, this.dateTime, this.thisYearMNF));
            //calList.Add(new FlowValueTrend(this.loc_code, this.dateTime, this.thisYearFlow));

            //if (calList[2].MonitorResult == "T" && calList[3].MonitorResult == "T")
            //{
            //    calList[2].MonitorResult = "T";
            //    calList[3].MonitorResult = "T";
            //}
            //else if (calList[2].MonitorResult == "F" || calList[3].MonitorResult == "F")
            //{
            //    calList[2].MonitorResult = "F";
            //    calList[3].MonitorResult = "F";
            //}

            ////야간최소유량 한계치 판단
            //calList.Add(new MaxMNFCompare(this.loc_code, this.dateTime, this.thisYearMNF));
        }


        ///// <summary>
        ///// 감시
        ///// </summary>
        //private void DoMonitering()
        //{
        //    if (this.calList.Count > 0)
        //    {
        //        this.calList.Clear();
        //    }

        //    //---------------------------------------------------------------------------------------------------------

        //    //야간최소유량 데이터 오류 판단
        //    calList.Add(new MNFCheck(this.dateTime, this.thisYearMNF));

        //    //유량 데이터 오류 판단
        //    calList.Add(new FlowCheck(this.dateTime, this.thisYearFlow));

        //    if (calList[0].MonitorResult == "T" && calList[1].MonitorResult == "T")
        //    {
        //        return;
        //    }

        //    //3일전 야간최소유량 데이터비교
        //    calList.Add(new MNF3DailyVariation(this.dateTime, this.thisYearMNF));

        //    //3일전 유량 데이터비교
        //    calList.Add(new Flow3DailyVariation(this.dateTime, this.thisYearFlow));

        //    if (calList[2].MonitorResult == "T" && calList[3].MonitorResult == "T")
        //    {
        //        calList[2].MonitorResult = "T";
        //        calList[3].MonitorResult = "T";
        //    }
        //    else if (calList[2].MonitorResult == "F" || calList[3].MonitorResult == "F")
        //    {
        //        calList[2].MonitorResult = "F";
        //        calList[3].MonitorResult = "F";
        //    }


        //    //30일전 야간최소유량 데이터비교
        //    calList.Add(new MNF30DailyVariation(this.dateTime, this.thisYearMNF));

        //    //30일전 유량 데이터비교
        //    calList.Add(new Flow30DailyVariation(this.dateTime, this.thisYearFlow));

        //    if (calList[4].MonitorResult == "T" && calList[5].MonitorResult == "T")
        //    {
        //        calList[4].MonitorResult = "T";
        //        calList[5].MonitorResult = "T";
        //    }
        //    else if (calList[4].MonitorResult == "F" || calList[5].MonitorResult == "F")
        //    {
        //        calList[4].MonitorResult = "F";
        //        calList[5].MonitorResult = "F";
        //    }

        //    //30일전 최고,최저 야간최소유량 데이터비교
        //    calList.Add(new MNF30DailyMax(this.dateTime, this.thisYearMNF));

        //    //30일전 최고,최저 유량 데이터비교
        //    calList.Add(new Flow30DailyMax(this.dateTime, this.thisYearFlow));

        //    if (calList[6].MonitorResult == "T" && calList[7].MonitorResult == "T")
        //    {
        //        calList[6].MonitorResult = "T";
        //        calList[7].MonitorResult = "T";
        //    }
        //    else if (calList[6].MonitorResult == "F" || calList[7].MonitorResult == "F")
        //    {
        //        calList[6].MonitorResult = "F";
        //        calList[7].MonitorResult = "F";
        //    }

        //    //7일평균 야간최소유량 데이터비교
        //    calList.Add(new MNF7DailyAverage(this.dateTime, this.thisYearMNF));

        //    //7일평균 유량 데이터비교
        //    calList.Add(new Flow7DailyAverage(this.dateTime, this.thisYearFlow));

        //    if (calList[8].MonitorResult == "T" && calList[9].MonitorResult == "T")
        //    {
        //        calList[8].MonitorResult = "T";
        //        calList[9].MonitorResult = "T";
        //    }
        //    else if (calList[8].MonitorResult == "F" || calList[9].MonitorResult == "F")
        //    {
        //        calList[8].MonitorResult = "F";
        //        calList[9].MonitorResult = "F";
        //    }

        //    //전일 야간최소유량 데이터 차이값으로 누수판단
        //    calList.Add(new MNFVariationCompare(this.loc_code, this.dateTime, this.thisYearMNF));

        //    //전일 유량 데이터 차이값으로 누수판단
        //    calList.Add(new FlowVariationCompare(this.loc_code, this.dateTime, this.thisYearFlow));

        //    if (calList[10].MonitorResult == "T" && calList[11].MonitorResult == "T")
        //    {
        //        calList[10].MonitorResult = "T";
        //        calList[11].MonitorResult = "T";
        //    }
        //    else if (calList[10].MonitorResult == "F" || calList[11].MonitorResult == "F")
        //    {
        //        calList[10].MonitorResult = "F";
        //        calList[11].MonitorResult = "F";
        //    }

        //    //야간최소유량 데이터 추세 판단
        //    calList.Add(new MNFTrendCompare(this.loc_code, this.dateTime, this.thisYearMNF));

        //    //야간최소유량 데이터 추세 판단
        //    calList.Add(new FlowTrendCompare(this.loc_code, this.dateTime, this.thisYearFlow));

        //    if (calList[12].MonitorResult == "T" && calList[13].MonitorResult == "T")
        //    {
        //        calList[12].MonitorResult = "T";
        //        calList[13].MonitorResult = "T";
        //    }
        //    else if (calList[12].MonitorResult == "F" || calList[13].MonitorResult == "F")
        //    {
        //        calList[12].MonitorResult = "F";
        //        calList[13].MonitorResult = "F";
        //    }




        //전년 야간최소유량 데이터비교
        //( 2011-1-5일 : 회의 결과 로직 제거)
        //calList.Add(new MNFLastYearCompare(this.dateTime, this.thisYearMNF, this.lastYearMNF, this.thisYearFlow, this.lastYearFlow, this.thisYearFlowRatio, this.lastYearFlowRatio));

        //7일 최고 야간최소유량 데이터비교
        //( 2011-1-5일 : 회의 결과 로직 제거)
        //calList.Add(new MNF7DailyMax(this.dateTime, this.thisYearMNF));

        //3일 최고 야간최소유량 데이터비교
        ////( 2011-1-5일 : 회의 결과 로직 제거)
        //calList.Add(new MNF3DailyMax(this.dateTime, this.thisYearMNF));

        //---------------------------------------------------------------------------------------------------------
        //3일전 유량 데이터비교
        //calList.Add(new Flow3DailyVariation(this.dateTime, this.thisYearFlow));

        //30일전 유량 데이터비교
        //calList.Add(new Flow30DailyVariation(this.dateTime, this.thisYearFlow));

        //30일전 최고,최저 유량 데이터비교
        //calList.Add(new Flow30DailyMax(this.dateTime, this.thisYearFlow));

        //7일평균 유량 데이터비교
        //calList.Add(new Flow7DailyAverage(this.dateTime, this.thisYearFlow));

        //전일 유량 데이터 차이값으로 누수판단
        //calList.Add(new FlowVariationCompare(this.loc_code, this.dateTime, this.thisYearFlow));

        //야간최소유량 데이터 추세 판단
        //calList.Add(new FlowTrendCompare(this.loc_code, this.dateTime, this.thisYearFlow));

        //7일 최고 유량 데이터비교
        //( 2011-1-5일 : 회의 결과 로직 제거)
        //calList.Add(new Flow7DailyMax(this.dateTime, this.thisYearFlow));

        //3일 최고 유량 데이터비교
        //( 2011-1-5일 : 회의 결과 로직 제거)
        //calList.Add(new Flow3DailyMax(this.dateTime, this.thisYearFlow));
        //}

        /// <summary>
        /// 감시결과DB저장
        /// </summary>
        private void WriteResult()
        {
            MoniteringWork.GetInstance().UpdatetLakageMonitoring(this.GetParameter(), this.calList);
        }

        //테스트
        private void InitializeSetting_TEST()
        {
            //DateTime dateTime = new DateTime(2009, 12, 31);

            //double thisYearFlowRatio = 117.5;
            //double lastYearFlowRatio = 85.1;

            //DataTable thisYearMNF = new DataTable();
            //DataTable lastYearMNF = new DataTable();
            //DataTable thisYearFlow = new DataTable();
            //DataTable lastYearFlow = new DataTable();

            //thisYearMNF.Columns.Add("TIMESTAMP");
            //thisYearMNF.Columns.Add("VALUE");

            //lastYearMNF.Columns.Add("TIMESTAMP");
            //lastYearMNF.Columns.Add("VALUE");

            //thisYearFlow.Columns.Add("TIMESTAMP");
            //thisYearFlow.Columns.Add("VALUE");

            //lastYearFlow.Columns.Add("TIMESTAMP");
            //lastYearFlow.Columns.Add("VALUE");

            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 02), 18.98333333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 03), 22.285);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 04), 21.14833333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 05), 20.435);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 06), 18.48833333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 07), 17.69833333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 08), 23.625);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 09), 23.57666667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 10), 23.21333333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 11), 24.715);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 12), 23.05666667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 13), 21.195);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 14), 19.71333333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 15), 25.70166667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 16), 23.72333333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 17), 24.42666667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 18), 24.44166667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 19), 24.05333333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 20), 21.42833333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 21), 23.53166667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 22), 27.665);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 23), 29.98333333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 24), 23.32666667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 25), 24.77833333);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 26), 21.46666667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 27), 17.20166667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 28), 14.95666667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 29), 21.97166667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 30), 30.34666667);
            //thisYearMNF.Rows.Add(new DateTime(2009, 12, 31), 23.13833333);

            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 17), 22.835);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 18), 32.33166667);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 19), 23.035);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 20), 18.66);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 21), 21.26);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 22), 16.59833333);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 23), 24.83333333);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 24), 24.135);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 25), 27.29333333);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 26), 18.37833333);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 27), 19.64833333);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 28), 19.67);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 29), 15.86);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 30), 24.16666667);
            //lastYearMNF.Rows.Add(new DateTime(2008, 12, 31), 20.325);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 01), 21.40833333);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 02), 16.00833333);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 03), 23.73);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 04), 21.27666667);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 05), 19.885);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 06), 26.44666667);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 07), 23.65);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 08), 25.615);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 09), 22.25333333);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 10), 21.41666667);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 11), 22.585);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 12), 19.71333333);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 13), 21.15833333);
            //lastYearMNF.Rows.Add(new DateTime(2009, 01, 14), 26.01);

            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 02), 787.24);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 03), 800.04);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 04), 816.0166667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 05), 635.97);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 06), 566.23);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 07), 806.6716667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 08), 821.7033333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 09), 833.5183333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 10), 843.5716667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 11), 839.5433333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 12), 721.4466667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 13), 605.4716667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 14), 840.4116667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 15), 861.4366667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 16), 842.5433333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 17), 854.8266667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 18), 836.89);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 19), 734.3383333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 20), 669.6816667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 21), 895.0183333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 22), 945.78);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 23), 936.0483333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 24), 886.4866667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 25), 668.3266667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 26), 664.1383333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 27), 479.625);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 28), 779.0566667);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 29), 864.4333333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 30), 885.2483333);
            //thisYearFlow.Rows.Add(new DateTime(2009, 12, 31), 690.18);

            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 17), 983.4433333);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 18), 1011.396667);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 19), 917.33);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 20), 696.16);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 21), 563.9733333);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 22), 907.6316667);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 23), 889.0883333);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 24), 957.8366667);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 25), 630.5616667);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 26), 768.585);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 27), 607.9266667);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 28), 533.9083333);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 29), 824.9916667);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 30), 835.9);
            //lastYearFlow.Rows.Add(new DateTime(2008, 12, 31), 844.51);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 01), 521.3766667);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 02), 725.9216667);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 03), 726.1983333);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 04), 638.48);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 05), 959.18);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 06), 1069.945);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 07), 1005.923333);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 08), 1027.755);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 09), 926.785);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 10), 685.8083333);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 11), 565.2033333);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 12), 788.6016667);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 13), 942.7116667);
            //lastYearFlow.Rows.Add(new DateTime(2009, 01, 14), 1020.361667);

            //this.thisYearFlowRatio = thisYearFlowRatio;
            //this.lastYearFlowRatio = lastYearFlowRatio;
            //this.thisYearMNF = thisYearMNF;
            //this.lastYearMNF = lastYearMNF;
            //this.thisYearFlow = thisYearFlow;
            //this.lastYearFlow = lastYearFlow;

            //this.dateTime = dateTime;
        }

        //테스트
        private void WriteResult_TEST()
        {
            foreach (ICal cal in this.calList)
            {
                Console.WriteLine("loc_code : " + this.loc_code + "\t type : " + cal.MonitorType + "\t result : " + cal.MonitorResult + "\t count :" + cal.MonitorCount.ToString());
            }
        }

        //테스트
        private void Data_TEST()
        {
            Console.WriteLine("======================== thisYearMNF ==============================");
            foreach (DataRow row in this.thisYearMNF.Rows)
            {
                Console.WriteLine("date : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") + "\t value : " + row["VALUE"].ToString());
            }

            Console.WriteLine("======================== lastYearMNF ==============================");
            //foreach (DataRow row in this.lastYearMNF.Rows)
            //{
            //    Console.WriteLine("date : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") + "\t value : " + row["VALUE"].ToString());
            //}

            Console.WriteLine("======================== thisYearFlow =============================");
            foreach (DataRow row in this.thisYearFlow.Rows)
            {
                Console.WriteLine("date : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") + "\t value : " + row["VALUE"].ToString());
            }

            Console.WriteLine("======================== lastYearFlow =============================");
            //foreach (DataRow row in this.lastYearFlow.Rows)
            //{
            //    Console.WriteLine("date : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") + "\t value : " + row["VALUE"].ToString());
            //}
        }
    }
}
