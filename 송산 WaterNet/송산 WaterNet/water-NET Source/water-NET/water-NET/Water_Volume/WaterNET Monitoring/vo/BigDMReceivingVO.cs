﻿using System;

namespace WaterNet.WaterNETMonitoring.vo
{
    public class BigDMReceivingVO
    {
        private string loc_code = null;
        private DateTime dateTime;

        public string LOC_CODE
        {
            set
            {
                this.loc_code = value;
            }
        }

        public DateTime DATETIME
        {
            set
            {
                this.dateTime = value;
            }
        }

        public void DoMonitering()
        {
            this.GetData();
        }

        private void GetData()
        {

        }
    }
}
