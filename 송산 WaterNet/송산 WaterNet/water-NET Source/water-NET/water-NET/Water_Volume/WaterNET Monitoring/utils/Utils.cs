﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WaterNETMonitoring.utils
{
    public class Utils
    {
        public static double ToDouble(object value)
        {
            double result = 0;

            if (value != DBNull.Value && !Double.IsInfinity(Convert.ToDouble(value)) && !Double.IsNaN(Convert.ToDouble(value)))
            {
                result = Convert.ToDouble(value);
            }

            return result;
        }
    }
}
