﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using WaterNet.WaterNETMonitoring.enum1;
using WaterNet.WaterNETMonitoring.vo;
using WaterNet.WaterNETMonitoring.work;

namespace WaterNet.WaterNETMonitoring.Control
{
    public class Monitering
    {
        List<string> locationList = new List<string>();

        public Monitering() { }

        //하나의 지역을 감시대상에 포함한다.
        public Monitering Add(string loc_code)
        {
            this.locationList.Add(loc_code);

            //중복제거필요
            this.locationList = this.locationList.Distinct().ToList();
            return this;
        }

        //모든 지역(중,소블록)을 조회후 감시대상에 포함한다.
        public Monitering AddAll()
        {
            DataSet locList = MoniteringWork.GetInstance().SelectLocation();
            foreach (DataRow row in locList.Tables[0].Rows)
            {
                this.Add(row["LOC_CODE"].ToString());
            }

            //중복제거필요
            this.locationList = this.locationList.Distinct().ToList();
            return this;
        }

        //등록된 지역의 감시구분별 감시를 시작한다.
        public void Start(DateTime dateTime, MONITOR_TYPE type)
        {
            if (type == MONITOR_TYPE.LEAKAGE)
            {
                this.DoLeakageMonitering(dateTime);
            }
            else if (type == MONITOR_TYPE.BIG_DM_RECEIVING)
            {
                this.DoBigDMReceivingMonitering(dateTime);
            }
        }

        //등록된 지역별 누수 감시를 시작한다.
        private void DoLeakageMonitering(DateTime dateTime)
        {
            for (int i = 0; i < this.locationList.Count; i++)
            {
                new LeakageVO(this.locationList[i], dateTime);
            }
        }

        //등록된 지역별 대수용가수수 감시를 시작한다.
        private void DoBigDMReceivingMonitering(DateTime dateTime)
        {
            for (int i = 0; i < this.locationList.Count; i++)
            {

            }
        }

        //테스트
        private void AddAll_TEST()
        {
            DataSet locList = MoniteringWork.GetInstance().SelectLocation();

            foreach (DataRow row in locList.Tables[0].Rows)
            {
                Console.WriteLine(row["LOC_CODE"].ToString());
            }
        }
    }
}
