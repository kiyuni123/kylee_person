﻿using System;
using WaterNet.WaterNETMonitoring.interface1;
using System.Data;
using WaterNet.WaterNETMonitoring.work;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    public class FlowVariationCompare : ICal
    {
        private string type = "1170";
        private int count = 0;
        private string result = "F";
        private int count_max = 0;

        private int minusday01Count = 0;

        public FlowVariationCompare(string loc_code, DateTime dateTime, DataTable thisYearFlow)
        {
            this.GetFlowCount(loc_code, dateTime);
            this.Cal(dateTime, thisYearFlow);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearFlow)
        {
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }

            double todayFlow = 0;
            double minusday01Flow = 0;
            double minusday02Flow = 0;
            double minusday03Flow = 0;
            double minusday04Flow = 0;

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayFlow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday02Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday03Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday04Flow = Utils.ToDouble(row["VALUE"]);
                }
            }

            int count = 0;

            if ((todayFlow - minusday01Flow) > (minusday01Flow - minusday02Flow))
            {
                count++;
            }
            if ((todayFlow - minusday01Flow) > (minusday02Flow - minusday03Flow))
            {
                count++;
            }
            if ((todayFlow - minusday01Flow) > (minusday03Flow - minusday04Flow))
            {
                count++;
            }

            if (count == 3 && this.minusday01Count == 2)
            {
                this.result = "T";
                this.count = 0;
            }

            if (count == 3 && this.minusday01Count == 1)
            {
                this.result = "F";
                this.count = 2;
            }

            if (count == 3 && this.minusday01Count == 0)
            {
                this.result = "F";
                this.count = 1;
            }
        }

        //1일전 Flow차이값으로 누수 카운트를 가져온다.
        private void GetFlowCount(string loc_code, DateTime dateTime)
        {
            object count = MoniteringWork.GetInstance().SelectLeakageCount(loc_code, dateTime.AddDays(-1).ToString("yyyyMMdd"), this.type);
            this.minusday01Count = Convert.ToInt32(count);
        }


        private void GetFlowCount_TEST(string loc_code, DateTime dateTime)
        {
            this.minusday01Count = 2;
        }
    }
}

