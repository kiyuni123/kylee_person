﻿using System;
using System.Data;
using WaterNet.WaterNETMonitoring.interface1;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    //30일전 Data와의 비교
    public class MNF30DailyVariation : ICal
    {
        private string type = "1020";
        private int count = 0;
        private string result = "N";
        private int count_max = 25;

        public MNF30DailyVariation(DateTime dateTime, DataTable thisYearMNF)
        {
            this.Cal(dateTime, thisYearMNF);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearMNF)
        {
            if (thisYearMNF == null || thisYearMNF.Rows.Count == 0)
            {
                return;
            }

            double todayMNF = 0;
            double minusday01MNF = 0;
            double minusday02MNF = 0;
            double minusday03MNF = 0;
            double minusday04MNF = 0;
            double minusday05MNF = 0;
            double minusday06MNF = 0;
            double minusday07MNF = 0;
            double minusday08MNF = 0;
            double minusday09MNF = 0;
            double minusday10MNF = 0;
            double minusday11MNF = 0;
            double minusday12MNF = 0;
            double minusday13MNF = 0;
            double minusday14MNF = 0;
            double minusday15MNF = 0;
            double minusday16MNF = 0;
            double minusday17MNF = 0;
            double minusday18MNF = 0;
            double minusday19MNF = 0;
            double minusday20MNF = 0;
            double minusday21MNF = 0;
            double minusday22MNF = 0;
            double minusday23MNF = 0;
            double minusday24MNF = 0;
            double minusday25MNF = 0;
            double minusday26MNF = 0;
            double minusday27MNF = 0;
            double minusday28MNF = 0;
            double minusday29MNF = 0;
            double minusday30MNF = 0;

            foreach (DataRow row in thisYearMNF.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayMNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday02MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday03MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday04MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday05MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday06MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday07MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday08MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday09MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday10MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday11MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday12MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday13MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday14MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-15).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday15MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-16).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday16MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-17).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday17MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-18).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday18MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-19).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday19MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-20).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday20MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-21).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday21MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-22).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday22MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-23).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday23MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-24).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday24MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-25).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday25MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-26).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday26MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-27).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday27MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-28).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday28MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-29).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday29MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-30).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday30MNF = Utils.ToDouble(row["VALUE"]);
                }
            }

            int count = 0;

            if (minusday01MNF < todayMNF)
            {
                count++;
            }
            if (minusday02MNF < todayMNF)
            {
                count++;
            }
            if (minusday03MNF < todayMNF)
            {
                count++;
            }
            if (minusday04MNF < todayMNF)
            {
                count++;
            }
            if (minusday05MNF < todayMNF)
            {
                count++;
            }
            if (minusday06MNF < todayMNF)
            {
                count++;
            }
            if (minusday07MNF < todayMNF)
            {
                count++;
            }
            if (minusday08MNF < todayMNF)
            {
                count++;
            }
            if (minusday09MNF < todayMNF)
            {
                count++;
            }
            if (minusday10MNF < todayMNF)
            {
                count++;
            }
            if (minusday11MNF < todayMNF)
            {
                count++;
            }
            if (minusday12MNF < todayMNF)
            {
                count++;
            }
            if (minusday13MNF < todayMNF)
            {
                count++;
            }
            if (minusday14MNF < todayMNF)
            {
                count++;
            }
            if (minusday15MNF < todayMNF)
            {
                count++;
            }
            if (minusday16MNF < todayMNF)
            {
                count++;
            }
            if (minusday17MNF < todayMNF)
            {
                count++;
            }
            if (minusday18MNF < todayMNF)
            {
                count++;
            }
            if (minusday19MNF < todayMNF)
            {
                count++;
            }
            if (minusday20MNF < todayMNF)
            {
                count++;
            }
            if (minusday21MNF < todayMNF)
            {
                count++;
            }
            if (minusday22MNF < todayMNF)
            {
                count++;
            }
            if (minusday23MNF < todayMNF)
            {
                count++;
            }
            if (minusday24MNF < todayMNF)
            {
                count++;
            }
            if (minusday25MNF < todayMNF)
            {
                count++;
            }
            if (minusday26MNF < todayMNF)
            {
                count++;
            }
            if (minusday27MNF < todayMNF)
            {
                count++;
            }
            if (minusday28MNF < todayMNF)
            {
                count++;
            }
            if (minusday29MNF < todayMNF)
            {
                count++;
            }
            if (minusday30MNF < todayMNF)
            {
                count++;
            }
            if (count >= 25)
            {
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }

            this.count = count;
        }
    }
}
