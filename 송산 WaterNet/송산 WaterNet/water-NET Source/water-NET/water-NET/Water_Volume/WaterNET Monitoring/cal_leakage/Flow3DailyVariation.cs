﻿using System;
using System.Data;
using WaterNet.WaterNETMonitoring.interface1;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    public class Flow3DailyVariation : ICal
    {
        private string type = "1110";
        private int count = 0;
        private string result = "N";
        private int count_max = 4;

        public Flow3DailyVariation(DateTime dateTime, DataTable thisYearFlow)
        {
            this.Cal(dateTime, thisYearFlow);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearFlow)
        {
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }

            double todayFlow = 0;
            double minusday01Flow = 0;
            double minusday02Flow = 0;
            double minusday03Flow = 0;

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayFlow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday02Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday03Flow = Utils.ToDouble(row["VALUE"]);
                }
            }

            int count = 0;

            if (minusday01Flow < todayFlow)
            {
                count++;
            }
            if (minusday02Flow < todayFlow)
            {
                count++;
            }
            if (minusday03Flow < todayFlow)
            {
                count++;
            }
            if (minusday02Flow < minusday01Flow)
            {
                count++;
            }
            if (minusday02Flow < minusday01Flow)
            {
                count++;
            }
            if (minusday03Flow < minusday02Flow)
            {
                count++;
            }

            if (count >= 4)
            {
                result = "T";
            }
            else
            {
                result = "F";
            }

            this.count = count;
        }
    }
}
