﻿using System;
using System.Data;
using WaterNet.WaterNETMonitoring.interface1;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    //작년 같은날의 Data와의 비교
    public class MNFLastYearCompare: ICal
    {
        private string type = "1040";
        private int count = 0;
        private string result = "N";
        private int count_max = 3;

        public MNFLastYearCompare(DateTime dateTime, DataTable thisYearMNF, DataTable lastYearMNF, DataTable thisYearFlow, DataTable lastYearFlow, double thisYearFlowRatio, double lastYearFlowRatio)
        {
            this.Cal(dateTime, thisYearMNF, lastYearMNF, thisYearFlow, lastYearFlow, thisYearFlowRatio, lastYearFlowRatio);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearMNF, DataTable lastYearMNF, DataTable thisYearFlow, DataTable lastYearFlow, double thisYearFlowRatio, double lastYearFlowRatio)
        {
            if (thisYearMNF == null || thisYearMNF.Rows.Count == 0)
            {
                return;
            }
            if (lastYearMNF == null || lastYearMNF.Rows.Count == 0)
            {
                return;
            }
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }
            if (lastYearFlow == null || lastYearFlow.Rows.Count == 0)
            {
                return;
            }

            double todayMNF = 0;
            double todayFlow = 0;

            double minusday14AverageValueMNF = 0;
            double add14AverageValueMNF = 0;
            double minusday14AverageValueFlow = 0;
            double add14AverageValueFlow = 0;

            foreach (DataRow row in thisYearMNF.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayMNF = Utils.ToDouble(row["VALUE"]);
                    break;
                }
            }

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayFlow = Utils.ToDouble(row["VALUE"]);
                    break;
                }
            }

            int count = 0;

            foreach (DataRow row in lastYearMNF.Rows)
            {
                if (dateTime.AddYears(-1).AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueMNF += mnf;
                }
            }

            if (count == 0)
            {
                
            }

            minusday14AverageValueMNF = minusday14AverageValueMNF / count;

            count = 0;

            foreach (DataRow row in lastYearMNF.Rows)
            {
                if (dateTime.AddYears(-1).AddDays(1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
                if (dateTime.AddYears(-1).AddDays(14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueMNF += mnf;
                }
            }

            if (count == 0)
            {
                
            }

            add14AverageValueMNF = add14AverageValueMNF / count;

            count = 0;

            foreach (DataRow row in lastYearFlow.Rows)
            {
                if (dateTime.AddYears(-1).AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    minusday14AverageValueFlow += flow;
                }
            }

            if (count == 0)
            {
                
            }

            minusday14AverageValueFlow = minusday14AverageValueFlow / count;

            count = 0;

            foreach (DataRow row in lastYearFlow.Rows)
            {
                if (dateTime.AddYears(-1).AddDays(1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
                if (dateTime.AddYears(-1).AddDays(14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    count++;
                    double flow = Utils.ToDouble(row["VALUE"]);
                    add14AverageValueFlow += flow;
                }
            }

            if (count == 0)
            {
                
            }

            add14AverageValueFlow = add14AverageValueFlow / count;

            count = 0;

            if ((minusday14AverageValueMNF * lastYearFlowRatio) < (todayMNF * thisYearFlowRatio))
            {
                count++;
            }
            if ((add14AverageValueMNF * lastYearFlowRatio) < (todayMNF * thisYearFlowRatio))
            {
                count++;
            }
            if ((minusday14AverageValueFlow * lastYearFlowRatio) < (todayFlow * thisYearFlowRatio))
            {
                count++;
            }
            if ((add14AverageValueFlow * lastYearFlowRatio) < (todayFlow * thisYearFlowRatio))
            {
                count++;
            }

            if (count >= 3)
            {
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }

            this.count = count;
        }
    }
}
