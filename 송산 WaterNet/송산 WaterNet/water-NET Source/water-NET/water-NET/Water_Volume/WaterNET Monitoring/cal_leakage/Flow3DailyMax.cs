﻿using System;
using WaterNet.WaterNETMonitoring.interface1;
using System.Data;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    public class Flow3DailyMax : ICal
    {
        private string type = "1160";
        private int count = 0;
        private string result = "N";
        private int count_max = 4;

        public Flow3DailyMax(DateTime dateTime, DataTable thisYearFlow)
        {
            this.Cal(dateTime, thisYearFlow);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearFlow)
        {
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }

            double minusday03MaxValue = 0;
            double minusday06MaxValue = 0;
            double minusday09MaxValue = 0;
            double minusday12MaxValue = 0;

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday03MaxValue < Flow)
                    {
                        minusday03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday03MaxValue < Flow)
                    {
                        minusday03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday03MaxValue < Flow)
                    {
                        minusday03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday06MaxValue < Flow)
                    {
                        minusday06MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday06MaxValue < Flow)
                    {
                        minusday06MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday06MaxValue < Flow)
                    {
                        minusday06MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday09MaxValue < Flow)
                    {
                        minusday09MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday09MaxValue < Flow)
                    {
                        minusday09MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday09MaxValue < Flow)
                    {
                        minusday09MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday12MaxValue < Flow)
                    {
                        minusday12MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday12MaxValue < Flow)
                    {
                        minusday12MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusday12MaxValue < Flow)
                    {
                        minusday12MaxValue = Flow;
                    }
                }
            }

            int count = 0;

            if (minusday06MaxValue < minusday03MaxValue)
            {
                count++;
            }
            if (minusday09MaxValue < minusday03MaxValue)
            {
                count++;
            }
            if (minusday12MaxValue < minusday03MaxValue)
            {
                count++;
            }
            if (minusday09MaxValue < minusday06MaxValue)
            {
                count++;
            }
            if (minusday12MaxValue < minusday06MaxValue)
            {
                count++;
            }
            if (minusday12MaxValue < minusday09MaxValue)
            {
                count++;
            }

            if (count >= 4)
            {
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }

            this.count = count;
        }
    }
}

