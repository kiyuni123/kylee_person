﻿using System;
using WaterNet.WaterNETMonitoring.interface1;
using System.Data;
using WaterNet.WaterNETMonitoring.work;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    //MNF유량 차이값으로 누수여부 판단
    public class MNFVariationCompare : ICal
    {
        private string type = "1080";
        private int count = 0;
        private string result = "F";
        private int count_max = 0;

        private int minusday01Count = 0;

        public MNFVariationCompare(string loc_code, DateTime dateTime, DataTable thisYearMNF)
        {
            this.GetMNFCount(loc_code, dateTime);
            this.Cal(dateTime, thisYearMNF);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearMNF)
        {
            if (thisYearMNF == null || thisYearMNF.Rows.Count == 0)
            {
                return;
            }

            double todayMNF = 0;
            double minusday01MNF = 0;
            double minusday02MNF = 0;
            double minusday03MNF = 0;
            double minusday04MNF = 0;

            foreach (DataRow row in thisYearMNF.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayMNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday02MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday03MNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday04MNF = Utils.ToDouble(row["VALUE"]);
                }
            }

            int count = 0;

            if ((todayMNF - minusday01MNF) > (minusday01MNF - minusday02MNF))
            {
                count++;
            }
            if ((todayMNF - minusday01MNF) > (minusday02MNF - minusday03MNF))
            {
                count++;
            }
            if ((todayMNF - minusday01MNF) > (minusday03MNF - minusday04MNF))
            {
                count++;
            }

            if (count == 3 && this.minusday01Count == 2)
            {
                this.result = "T";
                this.count = 0;
            }

            if (count == 3 && this.minusday01Count == 1)
            {
                this.result = "F";
                this.count = 2;
            }

            if (count == 3 && this.minusday01Count == 0)
            {
                this.result = "F";
                this.count = 1;
            }
        }

        //1일전 2일전 MNF차이값으로 누수 카운트를 가져온다.
        private void GetMNFCount(string loc_code, DateTime dateTime)
        {
            object count = MoniteringWork.GetInstance().SelectLeakageCount(loc_code, dateTime.AddDays(-1).ToString("yyyyMMdd"), this.type);
            this.minusday01Count = Convert.ToInt32(count);
        }


        private void GetMNFCount_TEST(string loc_code, DateTime dateTime)
        {
            this.minusday01Count = 2;
        }
    }
}

