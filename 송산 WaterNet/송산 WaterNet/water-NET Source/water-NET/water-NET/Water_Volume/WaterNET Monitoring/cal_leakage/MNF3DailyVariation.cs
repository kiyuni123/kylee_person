﻿using System;
using WaterNet.WaterNETMonitoring.interface1;
using System.Data;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    //3일전 Data와의 비교
    public class MNF3DailyVariation : ICal
    {
        private string type = "1010";
        private int count = 0;
        private string result = "N";
        private int count_max = 4;

        public MNF3DailyVariation(DateTime dateTime, DataTable thisYearMNF)
        {
            this.Cal(dateTime, thisYearMNF);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearMNF)
        {
            if (thisYearMNF == null || thisYearMNF.Rows.Count == 0)
            {
                return;
            }

            double todayMNF = 0;
            double minusday01MNF = 0;
            double minusday02MNF = 0;
            double minusday03MNF = 0;

            foreach (DataRow row in thisYearMNF.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayMNF = Utils.ToDouble(row["VALUE"]);
                    //Console.WriteLine("MNF : " + todayMNF.ToString() + "  TIME : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"));
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01MNF = Utils.ToDouble(row["VALUE"]);
                    //Console.WriteLine("MNF : " + minusday01MNF.ToString() + "  TIME : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"));
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday02MNF = Utils.ToDouble(row["VALUE"]);
                    //Console.WriteLine("MNF : " + minusday02MNF.ToString() + "  TIME : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"));
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday03MNF = Utils.ToDouble(row["VALUE"]);
                    //Console.WriteLine("MNF : " + minusday03MNF.ToString() + "  TIME : " + Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"));
                }
            }

            int count = 0;

            if (minusday01MNF < todayMNF)
            {
                count++;
            }
            if (minusday02MNF < todayMNF)
            {
                count++;
            }
            if (minusday03MNF < todayMNF)
            {
                count++;
            }
            if (minusday02MNF < minusday01MNF)
            {
                count++;
            }
            if (minusday02MNF < minusday01MNF)
            {
                count++;
            }
            if (minusday03MNF < minusday02MNF)
            {
                count++;
            }

            if (count >= 4)
            {
                result = "T";
            }
            else
            {
                result = "F";
            }

            this.count = count;
        }
    }
}
