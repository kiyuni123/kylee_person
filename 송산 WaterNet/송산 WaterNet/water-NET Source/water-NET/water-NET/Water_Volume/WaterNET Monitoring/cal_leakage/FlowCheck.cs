﻿using System;
using System.Data;
using WaterNet.WaterNETMonitoring.interface1;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    public class FlowCheck: ICal
    {
        private string type = "1180";
        private int count = 0;
        private string result = "T";
        private int count_max = 0;

        public FlowCheck(DateTime dateTime, DataTable thisYearFlow)
        {
            this.Cal(dateTime, thisYearFlow);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearFlow)
        {
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }

            double todayFlow = 0;
            double minusday01Flow = 0;

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayFlow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01Flow = Utils.ToDouble(row["VALUE"]);
                }
            }

            if (((minusday01Flow * 2) < todayFlow) || (todayFlow * 2) < minusday01Flow)
            {
                Console.WriteLine("today : " + todayFlow.ToString() + "  -1 : " + minusday01Flow.ToString());


                this.result = "T";
            }
            else
            {
                this.result = "F";
            }
        }
    }
}

