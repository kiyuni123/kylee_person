﻿using System;
using System.Data;
using WaterNet.WaterNETMonitoring.interface1;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    //일주일(7일) 평균 Data와의 비교
    public class MNF7DailyAverage : ICal
    {
        private string type = "1050";
        private int count = 0;
        private string result = "N";
        private int count_max = 4;

        public MNF7DailyAverage(DateTime dateTime, DataTable thisYearMNF)
        {
            this.Cal(dateTime, thisYearMNF);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearMNF)
        {
            if (thisYearMNF == null || thisYearMNF.Rows.Count == 0)
            {
                return;
            }

            double minusweek01AverageValue = 0;
            double minusweek02AverageValue = 0;
            double minusweek03AverageValue = 0;
            double minusweek04AverageValue = 0;

            foreach (DataRow row in thisYearMNF.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek01AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek01AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek01AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek01AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek01AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek01AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek01AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek02AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek02AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek02AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek02AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek02AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek02AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek02AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek03AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-15).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek03AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-16).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek03AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-17).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek03AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-18).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek03AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-19).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek03AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-20).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek03AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-21).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek04AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-22).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek04AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-23).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek04AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-24).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek04AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-25).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek04AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-26)

                    .ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek04AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-27).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusweek04AverageValue += Utils.ToDouble(row["VALUE"]);
                }
            }
            minusweek01AverageValue = minusweek01AverageValue / 7;
            minusweek02AverageValue = minusweek02AverageValue / 7;
            minusweek03AverageValue = minusweek03AverageValue / 7;
            minusweek04AverageValue = minusweek04AverageValue / 7;

            int count = 0;

            if (minusweek02AverageValue < minusweek01AverageValue)
            {
                count++;
            }
            if (minusweek03AverageValue < minusweek01AverageValue)
            {
                count++;
            }
            if (minusweek04AverageValue < minusweek01AverageValue)
            {
                count++;
            }
            if (minusweek03AverageValue < minusweek02AverageValue)
            {
                count++;
            }
            if (minusweek04AverageValue < minusweek02AverageValue)
            {
                count++;
            }
            if (minusweek04AverageValue < minusweek03AverageValue)
            {
                count++;
            }

            if (count >= 4)
            {
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }

            this.count = count;
        }
    }
}
