﻿using System;
using WaterNet.WaterNETMonitoring.interface1;
using System.Data;
using WaterNet.WaterNETMonitoring.work;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    public class FlowTrendCompare : ICal
    {
        private string type = "1190";
        private int count = 0;
        private string result = "F";
        private int count_max = 3;

        //private int minusday01Count = 0;

        public FlowTrendCompare(string loc_code, DateTime dateTime, DataTable thisYearFlow)
        {
            this.GetFlowCount(loc_code, dateTime);
            this.Cal(dateTime, thisYearFlow);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearFlow)
        {
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }

            double todayFlow = 0;
            double week3AverageValue = 0;

            //전일 카운트에 따라서 비교 대상일자를 다르게 줘야함.
            //int startDay = this.minusday01Count - 1;

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayFlow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-15).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-16).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-17).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-18).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-19).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-20).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-21).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
            }
            week3AverageValue = week3AverageValue / 21;

            if (week3AverageValue < todayFlow)
            {
                this.count++;
                //this.result = "T";
            }
            else
            {
                this.count = 0;
            }

            if (this.count >= 3)
            {
                this.result = "T";
            }

            //if (this.minusday01Count == 2)
            //{
            //    this.count = 3;
            //}
            //if (this.minusday01Count == 1)
            //{
            //    this.count = 2;
            //}
            //if (this.minusday01Count == 0)
            //{
            //    this.count = 1;
            //}
        }

        private void GetFlowCount(string loc_code, DateTime dateTime)
        {
            object count = MoniteringWork.GetInstance().SelectLeakageCount(loc_code, dateTime.AddDays(-1).ToString("yyyyMMdd"), this.type);
            this.count = Convert.ToInt32(count);
        }

        //전날의 추세분석 COUNT를 가져온다....
        private void GetFlowCount_TEST(string loc_code, DateTime dateTime)
        {
            //this.minusday01Count = 1;
        }
    }
}
