﻿using System;
using System.Data;
using WaterNet.WaterNETMonitoring.interface1;
using WaterNet.WaterNETMonitoring.utils;

namespace WaterNet.WaterNETMonitoring.cal_leakage
{
    public class Flow7DailyMax: ICal
    {
        private string type = "1150";
        private int count = 0;
        private string result = "N";
        private int count_max = 4;

        public Flow7DailyMax(DateTime dateTime, DataTable thisYearFlow)
        {
            this.Cal(dateTime, thisYearFlow);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearFlow)
        {
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }

            double minusweek01MaxValue = 0;
            double minusweek02MaxValue = 0;
            double minusweek03MaxValue = 0;
            double minusweek04MaxValue = 0;

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek01MaxValue < Flow)
                    {
                        minusweek01MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek01MaxValue < Flow)
                    {
                        minusweek01MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek01MaxValue < Flow)
                    {
                        minusweek01MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek01MaxValue < Flow)
                    {
                        minusweek01MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek01MaxValue < Flow)
                    {
                        minusweek01MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek01MaxValue < Flow)
                    {
                        minusweek01MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek01MaxValue < Flow)
                    {
                        minusweek01MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek02MaxValue < Flow)
                    {
                        minusweek02MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek02MaxValue < Flow)
                    {
                        minusweek02MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek02MaxValue < Flow)
                    {
                        minusweek02MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek02MaxValue < Flow)
                    {
                        minusweek02MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek02MaxValue < Flow)
                    {
                        minusweek02MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek02MaxValue < Flow)
                    {
                        minusweek02MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek02MaxValue < Flow)
                    {
                        minusweek02MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek03MaxValue < Flow)
                    {
                        minusweek03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-15).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek03MaxValue < Flow)
                    {
                        minusweek03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-16).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek03MaxValue < Flow)
                    {
                        minusweek03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-17).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek03MaxValue < Flow)
                    {
                        minusweek03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-18).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek03MaxValue < Flow)
                    {
                        minusweek03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-19).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek03MaxValue < Flow)
                    {
                        minusweek03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-20).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek03MaxValue < Flow)
                    {
                        minusweek03MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-21).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek04MaxValue < Flow)
                    {
                        minusweek04MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-22).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek04MaxValue < Flow)
                    {
                        minusweek04MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-23).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek04MaxValue < Flow)
                    {
                        minusweek04MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-24).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek04MaxValue < Flow)
                    {
                        minusweek04MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-25).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek04MaxValue < Flow)
                    {
                        minusweek04MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-26).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek04MaxValue < Flow)
                    {
                        minusweek04MaxValue = Flow;
                    }
                }
                else if (dateTime.AddDays(-27).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double Flow = Utils.ToDouble(row["VALUE"]);
                    if (minusweek04MaxValue < Flow)
                    {
                        minusweek04MaxValue = Flow;
                    }
                }
            }

            int count = 0;

            if (minusweek02MaxValue < minusweek01MaxValue)
            {
                count++;
            }
            if (minusweek03MaxValue < minusweek01MaxValue)
            {
                count++;
            }
            if (minusweek04MaxValue < minusweek01MaxValue)
            {
                count++;
            }
            if (minusweek03MaxValue < minusweek02MaxValue)
            {
                count++;
            }
            if (minusweek04MaxValue < minusweek02MaxValue)
            {
                count++;
            }
            if (minusweek04MaxValue < minusweek03MaxValue)
            {
                count++;
            }

            if (count >= 4)
            {
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }

            this.count = count;
        }
    }
}
