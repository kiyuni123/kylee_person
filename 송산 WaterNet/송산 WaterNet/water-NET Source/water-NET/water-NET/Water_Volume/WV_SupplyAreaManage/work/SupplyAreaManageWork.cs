﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.work;
using WaterNet.WV_SupplyAreaManage.dao;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.util;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.interface1;
using WaterNet.WaterAOCore;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WV_SupplyAreaManage.work
{
    public class SupplyAreaManageWork : BaseWork
    {
        private static SupplyAreaManageWork work = null;
        private SupplyAreaManageDao dao = null;

        private SupplyAreaManageWork()
        {
            dao = SupplyAreaManageDao.GetInstance();
        }

        public static SupplyAreaManageWork GetInstance()
        {
            if (work == null)
            {
                work = new SupplyAreaManageWork();
            }
            return work;
        }

        //유량계표준규격관리 조회
        public DataTable SelectFlowMetaStandard(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectFlowMetaStandard(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectTag(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectTag(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //유량계관리 조회
        public DataTable SelectFlowMetaMasterManage(WaterNet.DF_Common.interface1.IMapControl mainMap, Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                //result = mainMap.GetResultTable(parameter);

                result.Columns.Add("SELECTED", typeof(bool));
                result.Columns.Add("DEMAND", typeof(double));

                if (result != null)
                {
                    foreach (DataRow dataRow in result.Rows)
                    {
                        DataSet dataSet =
                            dao.SelectFlowMetaMasterManageLink(base.DataBaseManager, dataRow["FTR_IDN"].ToString());

                        Hashtable linkInfo = null;

                        if (dataSet.Tables.Count > 0)
                        {
                            if (dataSet.Tables[0].Rows.Count > 0)
                            {
                                linkInfo = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);
                            }
                        }

                        if (linkInfo != null)
                        {
                            dataRow["DEMAND"] = linkInfo["DEMAND"];
                        }

                        dataRow["SELECTED"] = false;
                    }
                }
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //지자체조회
        public DataTable SelectSgc()
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectSgc(base.DataBaseManager);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //배수지관리 조회
        public DataTable SelectSupplyAreaManage(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectSupplyAreaManage(base.DataBaseManager, parameter);
                if (dataSet.Tables.Count > 0)
                {
                    result = dataSet.Tables[0];
                }
                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //배수지관리 삭제
        public void DeleteSupplyAreaManage(UltraGrid ultraGrid)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                for (int i = ultraGrid.Rows.Count - 1; i >= 0; i--)
                {
                    if (Convert.ToBoolean(ultraGrid.Rows[i].Cells["SELECTED"].Value))
                    {
                        if (ultraGrid.Rows[i].Cells["SUPY_FTR_IDN"].Value.ToString() != "")
                        {
                            Hashtable parameter = Utils.ConverToHashtable(ultraGrid.Rows[i]);
                            dao.DeleteSupplyAreaManage(base.DataBaseManager, parameter);
                        }
                    }
                }

                CommitTransaction();

                for (int i = ultraGrid.Rows.Count - 1; i >= 0; i--)
                {
                    if (Convert.ToBoolean(ultraGrid.Rows[i].Cells["SELECTED"].Value))
                    {
                        ultraGrid.Rows[i].Delete(false);
                    }
                }

                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        //배수지관리 수정
        public void UpdateSupplyAreaManage(UltraGrid ultraGrid)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow row in ultraGrid.Rows)
                {
                    if (row.Cells["SUPY_FTR_IDN"].Value.ToString() != "")
                    {
                        Hashtable parameter = Utils.ConverToHashtable(row);
                        dao.UpdateSupplyAreaManage(base.DataBaseManager, parameter);
                    }                    
                }

                CommitTransaction();

                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        //배수지관리 등록
        public void InsertSupplyAreaManage(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                parameter["SUPY_FTR_IDN"] = dao.SelectSupplyAreaManageSEQ(base.DataBaseManager);
                dao.InsertSupplyAreaManage(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        //배수지관리설정 조회
        public Hashtable SelectSupplyAreaManageSetting(Hashtable parameter)
        {
            Hashtable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectSupplyAreaManageSetting(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //배수지관리설정 저장
        public void UpdateSupplyAreaManageSetting(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.UpdateSupplyAreaManageSetting(base.DataBaseManager, parameter);

                CommitTransaction();

                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        //FCV 밸브 조회
        public DataTable getValvesFormHydrodynamicMasterModel()
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = dao.getValvesFormHydrodynamicMasterModel(base.DataBaseManager);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
