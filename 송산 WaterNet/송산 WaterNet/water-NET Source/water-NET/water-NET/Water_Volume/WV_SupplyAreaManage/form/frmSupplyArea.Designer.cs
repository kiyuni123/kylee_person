﻿namespace WaterNet.WV_SupplyAreaManage.form
{
    partial class frmSupplyArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.insertBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLWL = new System.Windows.Forms.TextBox();
            this.txtHWL = new System.Windows.Forms.TextBox();
            this.txtFac_volumn = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtServNm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSupy_ftrIdn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wr2ndBtn = new System.Windows.Forms.Button();
            this.wr2nd_tag = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.wr1stBtn = new System.Windows.Forms.Button();
            this.wr1st_tag = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.outflowBtn = new System.Windows.Forms.Button();
            this.out_flow_tag = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.inflowBtn = new System.Windows.Forms.Button();
            this.in_flow_tag = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panelCommand.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Controls.Add(this.deleteBtn);
            this.panelCommand.Controls.Add(this.insertBtn);
            this.panelCommand.Controls.Add(this.searchBtn);
            this.panelCommand.Controls.Add(this.updateBtn);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(888, 31);
            this.panelCommand.TabIndex = 20;
            // 
            // deleteBtn
            // 
            this.deleteBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deleteBtn.AutoSize = true;
            this.deleteBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.deleteBtn.Location = new System.Drawing.Point(558, 3);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(75, 25);
            this.deleteBtn.TabIndex = 60;
            this.deleteBtn.TabStop = false;
            this.deleteBtn.Text = "삭제";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Visible = false;
            // 
            // insertBtn
            // 
            this.insertBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.insertBtn.AutoSize = true;
            this.insertBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.insertBtn.Location = new System.Drawing.Point(477, 3);
            this.insertBtn.Name = "insertBtn";
            this.insertBtn.Size = new System.Drawing.Size(75, 25);
            this.insertBtn.TabIndex = 59;
            this.insertBtn.TabStop = false;
            this.insertBtn.Text = "추가";
            this.insertBtn.UseVisualStyleBackColor = true;
            this.insertBtn.Visible = false;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(720, 3);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 25);
            this.searchBtn.TabIndex = 57;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // updateBtn
            // 
            this.updateBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.updateBtn.AutoSize = true;
            this.updateBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.updateBtn.Location = new System.Drawing.Point(639, 3);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(75, 25);
            this.updateBtn.TabIndex = 58;
            this.updateBtn.TabStop = false;
            this.updateBtn.Text = "저장";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 31);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.ultraGrid1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.label10);
            this.splitContainer.Panel2.Controls.Add(this.label8);
            this.splitContainer.Panel2.Controls.Add(this.txtLWL);
            this.splitContainer.Panel2.Controls.Add(this.txtHWL);
            this.splitContainer.Panel2.Controls.Add(this.txtFac_volumn);
            this.splitContainer.Panel2.Controls.Add(this.label7);
            this.splitContainer.Panel2.Controls.Add(this.label6);
            this.splitContainer.Panel2.Controls.Add(this.label5);
            this.splitContainer.Panel2.Controls.Add(this.btnSave);
            this.splitContainer.Panel2.Controls.Add(this.txtServNm);
            this.splitContainer.Panel2.Controls.Add(this.label4);
            this.splitContainer.Panel2.Controls.Add(this.txtSupy_ftrIdn);
            this.splitContainer.Panel2.Controls.Add(this.label3);
            this.splitContainer.Panel2.Controls.Add(this.wr2ndBtn);
            this.splitContainer.Panel2.Controls.Add(this.wr2nd_tag);
            this.splitContainer.Panel2.Controls.Add(this.label11);
            this.splitContainer.Panel2.Controls.Add(this.wr1stBtn);
            this.splitContainer.Panel2.Controls.Add(this.wr1st_tag);
            this.splitContainer.Panel2.Controls.Add(this.label9);
            this.splitContainer.Panel2.Controls.Add(this.outflowBtn);
            this.splitContainer.Panel2.Controls.Add(this.out_flow_tag);
            this.splitContainer.Panel2.Controls.Add(this.label2);
            this.splitContainer.Panel2.Controls.Add(this.inflowBtn);
            this.splitContainer.Panel2.Controls.Add(this.in_flow_tag);
            this.splitContainer.Panel2.Controls.Add(this.label1);
            this.splitContainer.Panel2Collapsed = true;
            this.splitContainer.Size = new System.Drawing.Size(888, 417);
            this.splitContainer.SplitterDistance = 289;
            this.splitContainer.TabIndex = 21;
            // 
            // ultraGrid1
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance37;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance44;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance47;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(888, 417);
            this.ultraGrid1.TabIndex = 3;
            this.ultraGrid1.Text = "상수관로";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(584, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 12);
            this.label10.TabIndex = 64;
            this.label10.Text = "m";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(252, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 12);
            this.label8.TabIndex = 63;
            this.label8.Text = "m";
            // 
            // txtLWL
            // 
            this.txtLWL.Location = new System.Drawing.Point(453, 80);
            this.txtLWL.Name = "txtLWL";
            this.txtLWL.Size = new System.Drawing.Size(128, 21);
            this.txtLWL.TabIndex = 62;
            this.txtLWL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtHWL
            // 
            this.txtHWL.Location = new System.Drawing.Point(120, 79);
            this.txtHWL.Name = "txtHWL";
            this.txtHWL.Size = new System.Drawing.Size(128, 21);
            this.txtHWL.TabIndex = 61;
            this.txtHWL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtFac_volumn
            // 
            this.txtFac_volumn.Location = new System.Drawing.Point(120, 49);
            this.txtFac_volumn.Name = "txtFac_volumn";
            this.txtFac_volumn.Size = new System.Drawing.Size(128, 21);
            this.txtFac_volumn.TabIndex = 60;
            this.txtFac_volumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(341, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 12);
            this.label7.TabIndex = 59;
            this.label7.Text = "최소수위(LWL)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(13, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 12);
            this.label6.TabIndex = 58;
            this.label6.Text = "최대수위(HWL)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(13, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 12);
            this.label5.TabIndex = 57;
            this.label5.Text = "배수지용량";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(891, 146);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 25);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // txtServNm
            // 
            this.txtServNm.Location = new System.Drawing.Point(453, 18);
            this.txtServNm.Name = "txtServNm";
            this.txtServNm.ReadOnly = true;
            this.txtServNm.Size = new System.Drawing.Size(128, 21);
            this.txtServNm.TabIndex = 55;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(341, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 12);
            this.label4.TabIndex = 54;
            this.label4.Text = "배수지명";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSupy_ftrIdn
            // 
            this.txtSupy_ftrIdn.Location = new System.Drawing.Point(121, 18);
            this.txtSupy_ftrIdn.Name = "txtSupy_ftrIdn";
            this.txtSupy_ftrIdn.ReadOnly = true;
            this.txtSupy_ftrIdn.Size = new System.Drawing.Size(128, 21);
            this.txtSupy_ftrIdn.TabIndex = 52;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(13, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 12);
            this.label3.TabIndex = 51;
            this.label3.Text = "배수지코드";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // wr2ndBtn
            // 
            this.wr2ndBtn.Location = new System.Drawing.Point(592, 143);
            this.wr2ndBtn.Name = "wr2ndBtn";
            this.wr2ndBtn.Size = new System.Drawing.Size(63, 23);
            this.wr2ndBtn.TabIndex = 49;
            this.wr2ndBtn.Text = "태그선택";
            this.wr2ndBtn.UseVisualStyleBackColor = true;
            // 
            // wr2nd_tag
            // 
            this.wr2nd_tag.Location = new System.Drawing.Point(453, 145);
            this.wr2nd_tag.Name = "wr2nd_tag";
            this.wr2nd_tag.ReadOnly = true;
            this.wr2nd_tag.Size = new System.Drawing.Size(128, 21);
            this.wr2nd_tag.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(341, 150);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 12);
            this.label11.TabIndex = 47;
            this.label11.Text = "2지수위태그";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // wr1stBtn
            // 
            this.wr1stBtn.Location = new System.Drawing.Point(254, 145);
            this.wr1stBtn.Name = "wr1stBtn";
            this.wr1stBtn.Size = new System.Drawing.Size(63, 23);
            this.wr1stBtn.TabIndex = 46;
            this.wr1stBtn.Text = "태그선택";
            this.wr1stBtn.UseVisualStyleBackColor = true;
            // 
            // wr1st_tag
            // 
            this.wr1st_tag.Location = new System.Drawing.Point(120, 145);
            this.wr1st_tag.Name = "wr1st_tag";
            this.wr1st_tag.ReadOnly = true;
            this.wr1st_tag.Size = new System.Drawing.Size(128, 21);
            this.wr1st_tag.TabIndex = 45;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(13, 150);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 12);
            this.label9.TabIndex = 44;
            this.label9.Text = "1지수위태그";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // outflowBtn
            // 
            this.outflowBtn.Location = new System.Drawing.Point(592, 112);
            this.outflowBtn.Name = "outflowBtn";
            this.outflowBtn.Size = new System.Drawing.Size(63, 23);
            this.outflowBtn.TabIndex = 43;
            this.outflowBtn.Text = "태그선택";
            this.outflowBtn.UseVisualStyleBackColor = true;
            // 
            // out_flow_tag
            // 
            this.out_flow_tag.Location = new System.Drawing.Point(453, 112);
            this.out_flow_tag.Name = "out_flow_tag";
            this.out_flow_tag.ReadOnly = true;
            this.out_flow_tag.Size = new System.Drawing.Size(128, 21);
            this.out_flow_tag.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(341, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 12);
            this.label2.TabIndex = 41;
            this.label2.Text = "유출유량 태그";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // inflowBtn
            // 
            this.inflowBtn.Location = new System.Drawing.Point(254, 112);
            this.inflowBtn.Name = "inflowBtn";
            this.inflowBtn.Size = new System.Drawing.Size(63, 23);
            this.inflowBtn.TabIndex = 40;
            this.inflowBtn.Text = "태그선택";
            this.inflowBtn.UseVisualStyleBackColor = true;
            // 
            // in_flow_tag
            // 
            this.in_flow_tag.Location = new System.Drawing.Point(120, 112);
            this.in_flow_tag.Name = "in_flow_tag";
            this.in_flow_tag.ReadOnly = true;
            this.in_flow_tag.Size = new System.Drawing.Size(128, 21);
            this.in_flow_tag.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(13, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 12);
            this.label1.TabIndex = 38;
            this.label1.Text = "유입유량 태그";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.AutoSize = true;
            this.btnClose.Font = new System.Drawing.Font("굴림", 8F);
            this.btnClose.Location = new System.Drawing.Point(801, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 25);
            this.btnClose.TabIndex = 61;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // frmSupplyArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 448);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmSupplyArea";
            this.Text = "배수지정보";
            this.panelCommand.ResumeLayout(false);
            this.panelCommand.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Button insertBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.SplitContainer splitContainer;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLWL;
        private System.Windows.Forms.TextBox txtHWL;
        private System.Windows.Forms.TextBox txtFac_volumn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtServNm;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSupy_ftrIdn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button wr2ndBtn;
        private System.Windows.Forms.TextBox wr2nd_tag;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button wr1stBtn;
        private System.Windows.Forms.TextBox wr1st_tag;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button outflowBtn;
        private System.Windows.Forms.TextBox out_flow_tag;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button inflowBtn;
        private System.Windows.Forms.TextBox in_flow_tag;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
    }
}