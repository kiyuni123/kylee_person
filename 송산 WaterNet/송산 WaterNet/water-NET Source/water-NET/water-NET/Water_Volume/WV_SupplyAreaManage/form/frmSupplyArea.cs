﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.DF_Common.interface1;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WV_SupplyAreaManage.work;
using WaterNet.WV_Common.util;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WV_SupplyAreaManage.form
{
    public partial class frmSupplyArea : Form
    {
        public frmSupplyArea()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmSupplyArea_Load);
        }

        //MainMap 을 제어하기 위한 인스턴스 변수
        private IMapControl mainMap = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControl MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSupplyArea_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();

            this.searchBtn.PerformClick();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {

        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            //this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            this.ultraGrid1.DisplayLayout.Bands[0].ColHeaderLines = 2;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SGCCD";
            column.Header.Caption = "지자체";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SUPY_FTR_IDN";
            column.Header.Caption = "배수지\n관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SERVNM";
            column.Header.Caption = "배수지명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FAC_VOLUMN";
            column.Header.Caption = "시설용량";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "HWL";
            column.Header.Caption = "운영\n최고수위";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.Format = "n";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "LWL";
            column.Header.Caption = "운영\n최저수위";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.Format = "n";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "IN_FLOW_TAG";
            column.Header.Caption = "유입유량\n(TAG)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "OUT_FLOW_TAG";
            column.Header.Caption = "유출유량\n(TAG)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "WR1ST_TAG";
            column.Header.Caption = "1지수위\n(TAG)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "WR2ND_TAG";
            column.Header.Caption = "2지수위\n(TAG)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            

            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            DataTable sgc = SupplyAreaManageWork.GetInstance().SelectSgc();

            //지자체
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SGCCD"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SGCCD");

                foreach (DataRow row in sgc.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SGCCD"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["SGCCD"];
            }

            //ComboBoxUtils.SetMember(this.sgccd, "CODE", "CODE_NAME");
            //this.sgccd.DataSource = sgc;
            //ComboBoxUtils.AddDefaultOption(this.sgccd, false);
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.btnSave.Click += new EventHandler(btnSave_Click);
            //this.specBtn.Click += new EventHandler(specBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);
            this.btnClose.Click += new EventHandler(btnClose_Click);

            this.ultraGrid1.ClickCell += new ClickCellEventHandler(ultraGrid1_ClickCell);
            this.ultraGrid1.ClickCellButton += new CellEventHandler(ultraGrid1_ClickCellButton);
            //this.excelBtn.Click += new EventHandler(excelBtn_Click);

            //this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            //this.settingUpdateBtn.Click += new EventHandler(settingUpdateBtn_Click);

            //this.flowmeterBtn.Click += new EventHandler(flowmeterBtn_Click);
        

            //this.ultraGrid1.AfterHeaderCheckStateChanged += new AfterHeaderCheckStateChangedEventHandler(ultraGrid1_AfterHeaderCheckStateChanged);
            //this.ultraGrid1.ClickCellButton += new CellEventHandler(ultraGrid1_ClickCellButton);
        }

        void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void ultraGrid1_ClickCellButton(object sender, CellEventArgs e)
        {
            if ((new string[] {"IN_FLOW_TAG","OUT_FLOW_TAG","WR1ST_TAG", "WR2ND_TAG"}).Contains(e.Cell.Column.Key))
            {
                frmTag form = new frmTag(e.Cell);
                form.ShowDialog();
            }
        }

        void ultraGrid1_ClickCell(object sender, ClickCellEventArgs e)
        {
            

            //this.txtSupy_ftrIdn.Text = e.Cell.Row.Cells["SUPY_FTR_IDN"].Value == DBNull.Value ? string.Empty :  (string)e.Cell.Row.Cells["SUPY_FTR_IDN"].Value;
            //this.txtServNm.Text = e.Cell.Row.Cells["SERVNM"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["SERVNM"].Value;
            //this.txtFac_volumn.Text = e.Cell.Row.Cells["FAC_VOLUMN"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["FAC_VOLUMN"].Value;
            //this.txtHWL.Text = e.Cell.Row.Cells["HWL"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["HWL"].Value;
            //this.txtLWL.Text = e.Cell.Row.Cells["LWL"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["LWL"].Value;
            //this.in_flow_tag.Text = e.Cell.Row.Cells["IN_FLOW_TAG"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["IN_FLOW_TAG"].Value;
            //this.out_flow_tag.Text = e.Cell.Row.Cells["OUT_FLOW_TAG"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["OUT_FLOW_TAG"].Value;
            //this.wr1st_tag.Text = e.Cell.Row.Cells["WR1ST_TAG"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["WR1ST_TAG"].Value;
            //this.wr2nd_tag.Text = e.Cell.Row.Cells["WR2ND_TAG"].Value == DBNull.Value ? string.Empty : (string)e.Cell.Row.Cells["WR2ND_TAG"].Value;
        }

        void searchBtn_Click(object sender, EventArgs e)
        {
            this.SelectSupplyAreaManage();
        }

        void insertBtn_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void updateBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("선택항목을 저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                Cursor currentCursor = this.Cursor;

                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    SupplyAreaManageWork.GetInstance().UpdateSupplyAreaManage(this.ultraGrid1);

                    //this.UpdateSupplyAreaManage();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.ToString());
                }
                finally
                {
                    this.Cursor = currentCursor;
                }
            }

            this.SelectSupplyAreaManage();
        }

        void deleteBtn_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
           
        }

        private void SelectSupplyAreaManage()
        {
            this.ultraGrid1.DataSource = SupplyAreaManageWork.GetInstance().SelectSupplyAreaManage(new System.Collections.Hashtable());
        }
    }
}
