﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.interface1;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using Infragistics.Win;
using WaterNet.WV_SupplyAreaManage.work;
using EMFrame.log;

namespace WaterNet.WV_SupplyAreaManage.form
{
    public partial class frmTag : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        //MainMap 을 제어하기 위한 인스턴스 변수
        private WaterNet.DF_Common.interface1.IMapControl mainMap = null;

        private UltraGridCell cell = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public WaterNet.DF_Common.interface1.IMapControl MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        public frmTag()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        public frmTag(UltraGridCell cell)
        {
            this.cell = cell;
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
            this.searchBtn.PerformClick();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            if (this.cell.Column.Key == "IN_FLOW_TAG" || this.cell.Column.Key == "OUT_FLOW_TAG")
            {
                this.checkBox1.Checked = true;
            }
            if (this.cell.Column.Key == "WR1ST_TAG" || this.cell.Column.Key == "WR2ND_TAG")
            {
                this.checkBox5.Checked = true;
            }
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }


        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TAG_GBN";
            column.Header.Caption = "태그구분";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TAG_NAME";
            column.Header.Caption = "태그명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TAG_DESCRIPTION";
            column.Header.Caption = "태그설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 450;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "USE_YN";
            column.Header.Caption = "사용여부";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //태그구분
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("TAG_GBN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("TAG_GBN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "7010");
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["TAG_GBN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["TAG_GBN"];
            }

            //사용여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");
                valueList.ValueListItems.Add("Y", "사용");
                valueList.ValueListItems.Add("N", "미사용");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.unselectedBtn.Click += new EventHandler(unselectedBtn_Click);
            this.selectedBtn.Click += new EventHandler(selectedBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);

            this.checkBox1.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox2.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox3.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox4.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox5.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox6.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox7.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
        }

        private void unselectedBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("연결항목을 해제 하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                this.cell.Value = string.Empty;;
                this.Close();
            }
        }

        private void selectedBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                MessageBox.Show("태그를 선택해주세요.");
                return;
            }
            this.cell.Value = this.ultraGrid1.ActiveRow.Cells["TAG_NAME"].Value;
            this.Close();
        }

        private Hashtable parameter = null;

        //검색버튼 이벤트 핸들러(태그목록 조회)
        private void searchBtn_Click(object sender, EventArgs e)
        {
            Cursor currentCursor = this.Cursor;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = new Hashtable();
                this.parameter["TAG_NAME"] = this.tag_name.Text;
                this.parameter["TAG_DESCRIPTION"] = this.tag_description.Text;
                this.SelectTag();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = currentCursor;
            }
        }

        //태그목록조회
        private void SelectTag()
        {
            this.ultraGrid1.DataSource = SupplyAreaManageWork.GetInstance().SelectTag(this.parameter);
            this.checkBox_CheckedChanged(null, null);
        }

        //엑셀버튼 이벤트 핸들러
        private void excelBtn_Click(object sender, EventArgs e)
        {

        }

        //태그 목록 숨기기, 보이기 이벤트 핸들러
        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                row.Hidden = false;

                //유량
                if (!checkBox1.Checked && (new string[] {"FRQ_I","FRQ_O"}).Contains(row.Cells["TAG_GBN"].Value.ToString()))
                {
                    row.Hidden = true;
                }

                //적산
                if (!checkBox2.Checked && row.Cells["TAG_GBN"].Value.ToString() == "000003")
                {
                    row.Hidden = true;
                }

                //적산차
                if (!checkBox3.Checked && row.Cells["TAG_GBN"].Value.ToString() == "000004")
                {
                    row.Hidden = true;
                }

                //압력
                if (!checkBox4.Checked && row.Cells["TAG_GBN"].Value.ToString() == "000002")
                {
                    row.Hidden = true;
                }

                //수위
                if (!checkBox5.Checked && (new string[] { "LEI"}).Contains(row.Cells["TAG_GBN"].Value.ToString()))
                {
                    row.Hidden = true;
                }

                //개도
                if (!checkBox6.Checked && row.Cells["TAG_GBN"].Value.ToString() == "999999")
                {
                    row.Hidden = true;
                }

                //가동여부
                if (!checkBox7.Checked && row.Cells["TAG_GBN"].Value.ToString() == "000006")
                {
                    row.Hidden = true;
                }
            }
        }
    }
}
