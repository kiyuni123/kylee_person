﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_SupplyAreaManage.dao
{
    public class SupplyAreaManageDao
    {
        private static SupplyAreaManageDao dao = null;
        private SupplyAreaManageDao() { }

        public static SupplyAreaManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new SupplyAreaManageDao();
            }
            return dao;
        }

        //유량계표준규격관리 조회
        public DataSet SelectFlowMetaStandard(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select to_number(seq) seq                           ");
            query.AppendLine("      ,flmfr_dvcd                                   ");
            query.AppendLine("      ,flmfr_dvnm                                   ");
            query.AppendLine("      ,cb                                           ");
            query.AppendLine("      ,to_number(q1) q1                             ");
            query.AppendLine("      ,to_number(q2) q2                             ");
            query.AppendLine("      ,to_number(q3) q3                             ");
            query.AppendLine("      ,to_number(q2q1) q2q1                         ");
            query.AppendLine("      ,to_number(q3q1) q3q1                         ");
            query.AppendLine("  from wv_flowmeter_standard                        ");
            query.AppendLine(" where flmfr_dvcd = nvl(:FLMFR_DVCD, flmfr_dvcd)    ");
            query.AppendLine("   and cb = nvl(:CB, cb)                            ");
            query.AppendLine("  order by to_number(seq)                           ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("FLMFR_DVCD", OracleDbType.Varchar2),
                     new OracleParameter("CB", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FLMFR_DVCD"];
            parameters[1].Value = parameter["CB"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //태그조회
        public DataSet SelectTag(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select b.tagname tag_name           ");
            query.AppendLine("      ,b.tag_gbn tag_gbn            ");
            query.AppendLine("      ,b.tag_desc tag_description   ");
            query.AppendLine("      ,a.use_yn use_yn              ");
            query.AppendLine("  from if_ihtags a                  ");
            query.AppendLine("      ,if_tag_gbn b                 ");
            query.AppendLine(" where 1 = 1                ");
            query.AppendLine("   and a.tagname = b.tagname        ");
            query.AppendLine("   and b.tag_gbn in ('FRQ_I','FRQ_O','LEI')        ");

            if (parameter["TAG_NAME"] != null && parameter["TAG_NAME"].ToString() != "")
            {
                query.Append("   and b.tagname like '%").Append(parameter["TAG_NAME"].ToString().Trim()).AppendLine("%'");
            }

            if (parameter["TAG_DESCRIPTION"] != null && parameter["TAG_DESCRIPTION"].ToString() != "")
            {
                query.Append("   and b.tag_desc like '%").Append(parameter["TAG_DESCRIPTION"].ToString().Trim()).AppendLine("%'");
            }

            return manager.ExecuteScriptDataSet(query.ToString(), null);
        }


        //유량계관리 조회
        public DataSet SelectFlowMetaMasterManageLink(OracleDBManager manager, string FTR_IDN)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select b.tag1                                                     ");
            query.AppendLine("      ,b.tag2                                                     ");
            query.AppendLine("      ,b.tag3                                                     ");
            query.AppendLine("      ,c.seq spec                                                 ");
            query.AppendLine("      ,c.q1                                                       ");
            query.AppendLine("      ,c.q2                                                       ");
            query.AppendLine("      ,c.q3                                                       ");
            query.AppendLine("      ,a.dm_count                                                 ");
            query.AppendLine("      ,a.io_gbn                                                   ");
            query.AppendLine("      ,to_number(a.demand) demand                                 ");
            query.AppendLine("      ,a.tm_yn                                                    ");
            query.AppendLine("  from wv_flowmeter a                                             ");
            query.AppendLine("      ,(                                                          ");
            query.AppendLine("      select max(decode(b.metdvicd, '000001', a.tagname)) tag1    ");
            query.AppendLine("            ,max(decode(b.metdvicd, '000004', a.tagname)) tag2    ");
            query.AppendLine("            ,max(decode(b.metdvicd, '000003', a.tagname)) tag3    ");
            query.AppendLine("            ,max(a.ftr_idn) flowmeter_ftr_idn                     ");
            query.AppendLine("        from if_tag_mapping a                                     ");
            query.AppendLine("            ,if_wide_ihtags b                                     ");
            query.AppendLine("       where a.ftr_gbn = '000001'                                 ");
            query.AppendLine("         and b.tagname = a.tagname                                ");
            query.AppendLine("      ) b                                                         ");
            query.AppendLine("      ,wv_flowmeter_standard c                                    ");
            query.AppendLine(" where a.flowmeter_ftr_idn = :FTR_IDN                             ");
            query.AppendLine("   and b.flowmeter_ftr_idn(+) = a.flowmeter_ftr_idn               ");
            query.AppendLine("   and c.seq(+) = a.spec                                          ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = FTR_IDN;

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //지자체 조회
        public DataSet SelectSgc(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" select si_code code                                ");
            query.AppendLine("       ,si_name code_name                           ");
            query.AppendLine("  from cm_dongcode                                  ");
            query.AppendLine(" group by si_name, si_code                          ");
            query.AppendLine(" order by si_name                                   ");

            return manager.ExecuteScriptDataSet(query.ToString(), null);
        }

        public object SelectSupplyAreaManageSEQ(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select nvl(max(to_number(supy_ftr_idn)),0) + 1 from we_supply_area ");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        //배수지관리 조회
        public DataSet SelectSupplyAreaManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select b.sgccd                             ");
            query.AppendLine("      ,b.loc_code as supy_ftr_idn          ");
            query.AppendLine("      ,b.loc_name as servnm                ");
            query.AppendLine("      ,a.fac_volumn                        ");
            query.AppendLine("      ,a.hwl                               ");
            query.AppendLine("      ,a.lwl                               ");
            query.AppendLine("      ,a.in_flow_tag                       ");
            query.AppendLine("      ,a.out_flow_tag                      ");
            query.AppendLine("      ,a.wr1st_tag                         ");
            query.AppendLine("      ,a.wr2nd_tag                         ");
            query.AppendLine("  from WE_SUPPLY_AREA a                    ");
            query.AppendLine("      ,cm_location b                       ");
            query.AppendLine(" where a.supy_ftr_idn(+) = b.loc_code      ");
            query.AppendLine("   and a.sgccd(+) = b.sgccd                ");
            query.AppendLine("   and b.loc_gbn = '배수지'                ");

            return manager.ExecuteScriptDataSet(query.ToString(), null);
        }


        //배수지관리 삭제
        public void DeleteSupplyAreaManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from we_supply_area where supy_ftr_idn = :SUPY_FTR_IDN ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("SUPY_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["SUPY_FTR_IDN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void InsertSupplyAreaManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into we_supply_area  (supy_ftr_idn)  ");
            query.AppendLine("                    values (:SUPY_FTR_IDN)  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("SUPY_FTR_IDN", OracleDbType.Varchar2)
                };
             
            parameters[0].Value = parameter["SUPY_FTR_IDN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //배수지관리 수정
        public void UpdateSupplyAreaManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into we_supply_area a                                         ");
            query.AppendLine("using (                                                             ");
            query.AppendLine("       select :SGCCD sgccd                                          ");
            query.AppendLine("             ,:SUPY_FTR_IDN supy_ftr_idn                            ");
            query.AppendLine("             ,:SERVNM servnm                                        ");
            query.AppendLine("             ,:FAC_VOLUMN fac_volumn                                ");
            query.AppendLine("             ,:HWL hwl                                              ");
            query.AppendLine("             ,:LWL lwl                                              ");
            query.AppendLine("             ,:IN_FLOW_TAG in_flow_tag                              ");
            query.AppendLine("             ,:OUT_FLOW_TAG out_flow_tag                            ");
            query.AppendLine("             ,:WR1ST_TAG wr1st_tag                                  ");
            query.AppendLine("             ,:WR2ND_TAG wr2nd_tag                                  ");
            query.AppendLine("         from dual                                                  ");
            query.AppendLine("      ) b                                                           ");
            query.AppendLine("   on (                                                             ");
            query.AppendLine("       a.supy_ftr_idn = b.supy_ftr_idn                              ");
            query.AppendLine("      )                                                             ");
            query.AppendLine(" when matched then                                                  ");
            query.AppendLine("      update set a.sgccd = b.sgccd                                  ");
            query.AppendLine("                ,a.servnm = b.servnm                                ");
            query.AppendLine("                ,a.fac_volumn = b.fac_volumn                        ");
            query.AppendLine("                ,a.hwl = b.hwl                                      ");
            query.AppendLine("                ,a.lwl = b.lwl                                      ");
            query.AppendLine("                ,a.in_flow_tag = b.in_flow_tag                      ");
            query.AppendLine("                ,a.out_flow_tag = b.out_flow_tag                    ");
            query.AppendLine("                ,a.wr1st_tag = b.wr1st_tag                          ");
            query.AppendLine("                ,a.wr2nd_tag = b.wr2nd_tag                          ");
            query.AppendLine(" when not matched then                                              ");
            query.AppendLine("      insert (                                                      ");
            query.AppendLine("              a.supy_ftr_idn                                        ");
            query.AppendLine("             ,a.sgccd                                               ");
            query.AppendLine("             ,a.servnm                                              ");
            query.AppendLine("             ,a.fac_volumn                                          ");
            query.AppendLine("             ,a.hwl                                                 ");
            query.AppendLine("             ,a.lwl                                                 ");
            query.AppendLine("             ,a.in_flow_tag                                         ");
            query.AppendLine("             ,a.out_flow_tag                                        ");
            query.AppendLine("             ,a.wr1st_tag                                           ");
            query.AppendLine("             ,a.wr2nd_tag                                           ");
            query.AppendLine("             )                                                      ");
            query.AppendLine("      values (                                                      ");
            query.AppendLine("              b.supy_ftr_idn                                        ");
            query.AppendLine("             ,b.sgccd                                               ");
            query.AppendLine("             ,b.servnm                                              ");
            query.AppendLine("             ,b.fac_volumn                                          ");
            query.AppendLine("             ,b.hwl                                                 ");
            query.AppendLine("             ,b.lwl                                                 ");
            query.AppendLine("             ,b.in_flow_tag                                         ");
            query.AppendLine("             ,b.out_flow_tag                                        ");
            query.AppendLine("             ,b.wr1st_tag                                           ");
            query.AppendLine("             ,b.wr2nd_tag                                           ");
            query.AppendLine("             )                                                      ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("SGCCD", OracleDbType.Varchar2),
                     new OracleParameter("SUPY_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("SERVNM", OracleDbType.Varchar2),
                     new OracleParameter("FAC_VOLUMN", OracleDbType.Varchar2),
                     new OracleParameter("HWL", OracleDbType.Varchar2),
                     new OracleParameter("LWL", OracleDbType.Varchar2),
                     new OracleParameter("IN_FLOW_TAG", OracleDbType.Varchar2),
                     new OracleParameter("OUT_FLOW_TAG", OracleDbType.Varchar2),
                     new OracleParameter("WR1ST_TAG", OracleDbType.Varchar2),
                     new OracleParameter("WR2ND_TAG", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["SGCCD"];
            parameters[1].Value = parameter["SUPY_FTR_IDN"];
            parameters[2].Value = parameter["SERVNM"];
            parameters[3].Value = parameter["FAC_VOLUMN"];
            parameters[4].Value = parameter["HWL"];
            parameters[5].Value = parameter["LWL"];
            parameters[6].Value = parameter["IN_FLOW_TAG"];
            parameters[7].Value = parameter["OUT_FLOW_TAG"];
            parameters[8].Value = parameter["WR1ST_TAG"];
            parameters[9].Value = parameter["WR2ND_TAG"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //배수지관리 조회
        public DataSet SelectSupplyAreaManageSetting(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select a.in_flow_tag                                     ");
            query.AppendLine("      ,a.out_flow_tag                                    ");
            query.AppendLine("      ,a.wr1st_tag                                       ");
            query.AppendLine("      ,a.wr2nd_tag                                       ");
            query.AppendLine("      ,a.flowmeter_ftr_idn                               ");
            query.AppendLine("      ,decode(a.df_yn, 'Y', 'True', 'False') df_yn       ");
            query.AppendLine("      ,spec                                              ");
            query.AppendLine("  from we_supply_area a                                  ");
            query.AppendLine(" where a.supy_ftr_idn = :SUPY_FTR_IDN                    ");

            IDataParameter[] parameters =  {
                     new OracleParameter("SUPY_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["SUPY_FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //배수지설정 수정
        public void UpdateSupplyAreaManageSetting(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update we_supply_area                                    ");
            query.AppendLine("   set in_flow_tag = :IN_FLOW_TAG                        ");
            query.AppendLine("      ,out_flow_tag = :OUT_FLOW_TAG                      ");
            query.AppendLine("      ,wr1st_tag = :WR1ST_TAG                            ");
            query.AppendLine("      ,wr2nd_tag = :WR2ND_TAG                            ");
            query.AppendLine("      ,flowmeter_ftr_idn = :FLOWMETER_FTR_IDN            ");
            query.AppendLine("      ,spec = :SPEC                                      ");
            query.AppendLine(" where supy_ftr_idn = :SUPY_FTR_IDN                      ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("IN_FLOW_TAG", OracleDbType.Varchar2),
                     new OracleParameter("OUT_FLOW_TAG", OracleDbType.Varchar2),
                     new OracleParameter("WR1ST_TAG", OracleDbType.Varchar2),
                     new OracleParameter("WR2ND_TAG", OracleDbType.Varchar2),
                     new OracleParameter("FLOWMETER_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("SPEC", OracleDbType.Varchar2),
                     new OracleParameter("SUPY_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["IN_FLOW_TAG"];
            parameters[1].Value = parameter["OUT_FLOW_TAG"];
            parameters[2].Value = parameter["WR1ST_TAG"];
            parameters[3].Value = parameter["WR2ND_TAG"];
            parameters[4].Value = parameter["FLOWMETER_FTR_IDN"];
            parameters[5].Value = parameter["SPEC"];
            parameters[6].Value = parameter["SUPY_FTR_IDN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //FCV 밸브 조회
        public DataTable getValvesFormHydrodynamicMasterModel(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select id link_id ");
            query.AppendLine("      ,setting ");
            query.AppendLine("      ,remark ");
            query.AppendLine("  from wh_valves ");
            query.AppendLine(" where inp_number = (select inp_number from wh_title where use_gbn = 'WH') ");

            return manager.ExecuteScriptDataTable(query.ToString(), null);
        }
    }
}
