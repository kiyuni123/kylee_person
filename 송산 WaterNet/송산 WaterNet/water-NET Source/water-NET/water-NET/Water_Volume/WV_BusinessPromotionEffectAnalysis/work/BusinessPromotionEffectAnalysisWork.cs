﻿using System;
using WaterNet.WV_BusinessPromotionEffectAnalysis.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;

namespace WaterNet.WV_BusinessPromotionEffectAnalysis.work
{
    public class BusinessPromotionEffectAnalysisWork : BaseWork
    {

        public static BusinessPromotionEffectAnalysisWork GetInstance()
        {
            if (work == null)
            {
                work = new BusinessPromotionEffectAnalysisWork();
            }
            return work;
        }

        public DataSet SelectBusinessPromotionEffectAnalysis(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBusinessPromotionEffectAnalysis(base.DataBaseManager, result, "result", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }


        private static BusinessPromotionEffectAnalysisWork work = null;
        private BusinessPromotionEffectAnalysisDao dao = null;


        private BusinessPromotionEffectAnalysisWork()
        {
            dao = BusinessPromotionEffectAnalysisDao.GetInstance();
        }
    }
}
