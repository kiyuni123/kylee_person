﻿using System;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.util;
using ChartFX.WinForms;
using System.Collections;
using WaterNet.WV_BusinessPromotionEffectAnalysis.work;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.enum1;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

namespace WaterNet.WV_BusinessPromotionEffectAnalysis.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private TableLayout tableLayout = null;
        private ChartManager chartManager = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 변수 (WV_Common.form.frmMainMap)
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체 설정
        /// </summary>
        private void InitializeForm()
        {
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            this.searchBox1.InitializeSearchBox();
            this.searchBox1.Text = "검색조건";
            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE;
            this.searchBox1.IntervalText = "검색기간";
            this.searchBox1.StartDateObject.Value = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 차트를 기본 세팅 한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-MM-dd");

            //값이 0 인경우 차트에 표시안함(차트 조건절 설정)
            ConditionalAttributes conditionalAttributes = new ConditionalAttributes();
            conditionalAttributes.Condition.From = 0;
            conditionalAttributes.Condition.To = 0;
            conditionalAttributes.MarkerShape = MarkerShape.None;
            chart1.ConditionalAttributes.Add(conditionalAttributes);
        }

        /// <summary>
        /// 그리드의 설정을 세팅 한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            //동진 수정_2012.6.27
            this.ultraGrid1.BeforeColumnChooserDisplayed += new BeforeColumnChooserDisplayedEventHandler(ultraGrid1_BeforeColumnChooserDisplayed);  //동진_6.12
            //=======================
        }

        //=========================================================
        //
        //                    동진 수정_2012.6.27
        //                     ColumnChooser 셀 높이
        //=========================================================
        void ultraGrid1_BeforeColumnChooserDisplayed(object sender, BeforeColumnChooserDisplayedEventArgs e)
        {
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.Reset();
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;  //원클릭( 원래는 더블클릭이였음)
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

        }
        //=========================================================================

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];

            Utils.SetValueList(this.seriesChange1, VALUELIST_TYPE.CODE, "2009");
        }

        /// <summary>
        /// 이벤트가 필요한 객체에 이벤트 핸들러를 등록 한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);
            this.searchBtn.Click += new EventHandler(SelectBusinessPromotionEffectAnalysis_EventHandler);

            //차트series_변경 이벤트핸들러
            seriesChange1.SelectedIndexChanged += new EventHandler(ChartSeriesChange_EventHandler);
            seriesChange2.CheckedChanged += new EventHandler(ChartSeriesChange_EventHandler);
            seriesChange3.CheckedChanged += new EventHandler(ChartSeriesChange_EventHandler);
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.chart1, "사업효과분석정보", 0, 0, 9, 20);
                this.excelManager.AddWorksheet(this.ultraGrid1, "사업효과분석정보", 10, 0);
                this.excelManager.Save("사업효과분석정보", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender">버튼</param>
        /// <param name="e">버튼 클릭</param>
        private void SelectBusinessPromotionEffectAnalysis_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.SelectBusinessPromotionEffectAnalysis(this.searchBox1.InitializeParameter().Parameters);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 사업추진효과분석 검색
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectBusinessPromotionEffectAnalysis(Hashtable parameter)
        {
            if (parameter["FTR_CODE"].ToString() == "BZ001")
            {
                MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
                return;
            }
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = BusinessPromotionEffectAnalysisWork.GetInstance().SelectBusinessPromotionEffectAnalysis(parameter).Tables[0];
            this.InitializeChartSetting();
        }

        /// <summary>
        /// 검색 데이터 변경시 차트 세팅/ 데이터 초기화 
        /// </summary>
        private void InitializeChartSetting()
        {
            DataTable effectList = (DataTable)this.ultraGrid1.DataSource;

            Chart chart = this.chartManager.Items[0];

            chart.Data.Series = 15;
            chart.Data.Points = effectList.Rows.Count;

            foreach (DataRow row in effectList.Rows)
            {
                int pointIndex = effectList.Rows.IndexOf(row);
                chart.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["DATEE"]).ToString("yyyy-MM-dd");
                chart.Data[0, pointIndex] = Utils.ToDouble(row["INSTANT"]);
                chart.Data[1, pointIndex] = Utils.ToDouble(row["M_AVERAGE"]);
                chart.Data[2, pointIndex] = Utils.ToDouble(row["INFPNT_HPRES_DAVG"]);
                chart.Data[3, pointIndex] = Utils.ToDouble(row["INFPNT_HPRES_TAVG"]);
                chart.Data[4, pointIndex] = Utils.ToDouble(row["INFPNT_HPRES_NAVG"]);
                chart.Data[5, pointIndex] = Utils.ToDouble(row["AVG_HPRES_NAVG"]);
                chart.Data[6, pointIndex] = Utils.ToDouble(row["MINLEAK_COUNT"]);
                chart.Data[7, pointIndex] = Utils.ToDouble(row["TAMLEAK_COUNT"]);
                chart.Data[8, pointIndex] = Utils.ToDouble(row["MTRCHGSU"]);
                chart.Data[9, pointIndex] = Utils.ToDouble(row["GUPSUJUNSU"]);
                chart.Data[10, pointIndex] = Utils.ToDouble(row["GAJUNG_GAGUSU"]);
                chart.Data[11, pointIndex] = Utils.ToDouble(row["BGAJUNG_GAGUSU"]);
                chart.Data[12, pointIndex] = Utils.ToDouble(row["PIP_LEN"]);
                chart.Data[13, pointIndex] = Utils.ToDouble(row["WORK_CNT"]);
                chart.Data[14, pointIndex] = Utils.ToDouble(row["YONGSU_AMT"]);
            }

            chart.Series[0].Gallery = Gallery.Curve;
            chart.Series[0].Line.Width = 1;
            chart.Series[0].MarkerShape = MarkerShape.None;

            chart.Series[1].Gallery = Gallery.Curve;
            chart.Series[1].Line.Width = 1;
            chart.Series[1].MarkerShape = MarkerShape.None;

            chart.Series[2].AxisY = chart1.AxisY2;
            chart.Series[2].Gallery = Gallery.Step;
            chart.Series[2].Line.Width = 3;
            chart.Series[2].MarkerShape = MarkerShape.None;

            chart.Series[3].AxisY = chart1.AxisY2;
            chart.Series[3].Gallery = Gallery.Step;
            chart.Series[3].Line.Width = 3;
            chart.Series[3].MarkerShape = MarkerShape.None;

            chart.Series[4].AxisY = chart1.AxisY2;
            chart.Series[4].Gallery = Gallery.Step;
            chart.Series[4].Line.Width = 3;
            chart.Series[4].MarkerShape = MarkerShape.None;

            chart.Series[5].AxisY = chart1.AxisY2;
            chart.Series[5].Gallery = Gallery.Step;
            chart.Series[5].Line.Width = 3;
            chart.Series[5].MarkerShape = MarkerShape.None;

            chart.Series[6].AxisY = chart1.AxisY2;
            chart.Series[6].Gallery = Gallery.Scatter;
            chart.Series[6].MarkerShape = MarkerShape.Triangle;
            chart.Series[6].MarkerSize = 5;

            chart.Series[7].AxisY = chart1.AxisY2;
            chart.Series[7].Gallery = Gallery.Scatter;
            chart.Series[7].MarkerShape = MarkerShape.Triangle;
            chart.Series[7].MarkerSize = 5;

            chart.Series[8].AxisY = chart1.AxisY2;
            chart.Series[8].Gallery = Gallery.Scatter;
            chart.Series[8].MarkerShape = MarkerShape.Triangle;
            chart.Series[8].MarkerSize = 5;

            chart.Series[9].AxisY = chart1.AxisY2;
            chart.Series[9].Gallery = Gallery.Step;
            chart.Series[9].Line.Width = 3;
            chart.Series[9].MarkerShape = MarkerShape.None;

            chart.Series[10].AxisY = chart1.AxisY2;
            chart.Series[10].Gallery = Gallery.Step;
            chart.Series[10].Line.Width = 3;
            chart.Series[10].MarkerShape = MarkerShape.None;

            chart.Series[11].AxisY = chart1.AxisY2;
            chart.Series[11].Gallery = Gallery.Step;
            chart.Series[11].Line.Width = 3;
            chart.Series[11].MarkerShape = MarkerShape.None;

            chart.Series[12].AxisY = chart1.AxisY2;
            chart.Series[12].Gallery = Gallery.Bar;
            chart.Series[12].Line.Width = 1;
            chart.Series[12].MarkerShape = MarkerShape.None;

            chart.Series[13].AxisY = chart1.AxisY2;
            chart.Series[13].Gallery = Gallery.Scatter;
            chart.Series[13].MarkerShape = MarkerShape.Triangle;
            chart.Series[13].MarkerSize = 5;

            chart.Series[14].AxisY = chart1.AxisY2;
            chart.Series[14].Gallery = Gallery.Bar;
            chart.Series[14].Line.Width = 1;
            chart.Series[14].MarkerShape = MarkerShape.None;

            chart.Series[0].Text = "순시 야간최소유량(㎥/h)";
            chart.Series[1].Text = "보정 야간최소유량(㎥/h)";
            chart.Series[2].Text = "유입지점 일평균수압(kgf/㎠)";
            chart.Series[3].Text = "유입지점 주간평균수압(kgf/㎠)";
            chart.Series[4].Text = "유입지점 야간평균수압(kgf/㎠)";
            chart.Series[5].Text = "평균수압지점 야간평균수압(kgf/㎠)";
            chart.Series[6].Text = "민원누수복구건수";
            chart.Series[7].Text = "탐사누수복구건수";
            chart.Series[8].Text = "계량기교체건수";
            chart.Series[9].Text = "급수전수";
            chart.Series[10].Text = "가정가구수";
            chart.Series[11].Text = "비가정가구수";
            chart.Series[12].Text = "관개대체(m)";
            chart.Series[13].Text = "기타단수작업";
            chart.Series[14].Text = "수도사업용수량(㎥/일)";

            chart.Refresh();
            ChangeChartSeries();
        }

        /// <summary>
        /// 차트 시리즈 변경에 따른 이벤트 핸들러로서 차트 변경 시리즈 함수를 호출한다.
        /// </summary>
        private void ChartSeriesChange_EventHandler(Object sender, EventArgs e)
        {
            this.ChangeChartSeries();
        }

        /// <summary>
        /// 차트시리즈를 선택하는 콘트롤을 참조해서 해당 차트시리즈만 숨기기 기능을 해제 합니다.
        /// </summary>
        private void ChangeChartSeries()
        {
            Chart chart = this.chartManager.Items[0];

            if (chart.Series.Count < 14)
            {
                return;
            }

            //초기 설정은 모든 시리즈 숨기기
            foreach (SeriesAttributes serie in chart1.Series)
            {
                serie.Visible = false;
            }

            //순시야간최소유량 시리즈
            if (seriesChange2.Checked)
            {
                chart.Series[0].Visible = true;
            }

            //이동평균야간최소유량 시리즈
            if (seriesChange3.Checked)
            {
                chart.Series[1].Visible = true;
            }

            chart.AxesY[0].Title.Text = "유량(㎥/h)";

            switch (seriesChange1.SelectedIndex)
            {
                    //계량기교체
                case 0 :
                    chart.AxisY2.Title.Text = "계량기교체건수";
                    chart.Series[8].Visible = true;
                    break;

                    //관개대체연장
                case 1:
                    chart.AxisY2.Title.Text = "관개대체(m)";
                    chart.Series[12].Visible = true;
                    break;

                    //기타단수작업
                case 2:
                    chart.AxisY2.Title.Text = "기타단수작업건수";
                    chart.Series[13].Visible = true;
                    break;

                    //누수복구
                case 3:
                    chart.AxisY2.Title.Text = "누수복구건수";
                    chart.Series[6].Visible = true;
                    chart.Series[7].Visible = true;
                    break;

                    //수도사업용수량
                case 4:
                    chart.AxisY2.Title.Text = "수도사업용수량(㎥/일)";
                    chart.Series[14].Visible = true;
                    break;
                    
                    //수압계측정보
                case 5:
                    chart.AxisY2.Title.Text = "수압(kgf/㎠)";
                    chart.Series[2].Visible = true;
                    chart.Series[3].Visible = true;
                    chart.Series[4].Visible = true;
                    chart.Series[5].Visible = true;
                    break;

                    //수용가정보
                case 6:
                    chart.AxisY2.Title.Text = "수용가 급수전/ 가구수";
                    chart.Series[9].Visible = true;
                    chart.Series[10].Visible = true;
                    chart.Series[11].Visible = true;
                    break;
            }
        }
    }
}
