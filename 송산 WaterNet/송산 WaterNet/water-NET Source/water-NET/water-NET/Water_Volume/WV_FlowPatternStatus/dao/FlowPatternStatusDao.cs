﻿using System.Text;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.dao;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_FlowPatternStatus.dao
{
    public class FlowPatternStatusDao : BaseDao
    {
        private static FlowPatternStatusDao dao = null;
        private FlowPatternStatusDao() { }

        public static FlowPatternStatusDao GetInstance()
        {
            if (dao == null)
            {
                dao = new FlowPatternStatusDao();
            }
            return dao;
        }

        //지역코드의 배수지 계통여부 조회
        public object IsReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.Append("select decode(kt_gbn, '001', 'True', 'False') value from cm_location where 1 = 1 and loc_code = :LOC_CODE");
            query.Append("select decode(res_code, null, 'False', 'True') value from cm_location where 1 = 1 and loc_code = :LOC_CODE");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }


        //배수지인경우 검색
        public void SelectFlowPatternStatus_Reservoir(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                                                          ");
            //query.AppendLine("(                                                                                                                                                                    ");
            //query.AppendLine("select c1.loc_code                                                                                                                                                   ");
            //query.AppendLine("      ,c1.sgccd                                                                                                                                                      ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                      ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                       ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                       ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                              ");
            //query.AppendLine("      ,c1.ftr_code                                                                                                                                                   ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                           ");
            //query.AppendLine("      ,c1.rel_loc_name                                                                                                                                               ");
            //query.AppendLine("      ,c1.ord                                                                                                                                                        ");
            //query.AppendLine("  from                                                                                                                                                               ");
            //query.AppendLine("      (                                                                                                                                                              ");
            //query.AppendLine("       select sgccd                                                                                                                                                  ");
            //query.AppendLine("             ,loc_code                                                                                                                                               ");
            //query.AppendLine("             ,ploc_code                                                                                                                                              ");
            //query.AppendLine("             ,loc_name                                                                                                                                               ");
            //query.AppendLine("             ,ftr_idn                                                                                                                                                ");
            //query.AppendLine("             ,ftr_code                                                                                                                                               ");
            //query.AppendLine("             ,res_code                                                                                                                                               ");
            //query.AppendLine("             ,kt_gbn                                                                                                                                                 ");
            //query.AppendLine("             ,rel_loc_name                                                                                                                                           ");
            //query.AppendLine("             ,rownum ord                                                                                                                                             ");
            //query.AppendLine("         from cm_location                                                                                                                                            ");
            //query.AppendLine("        where 1 = 1                                                                                                                                                  ");
            //query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                   ");
            //query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                                ");
            //query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                        ");
            //query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                    ");
            //query.AppendLine("      ) c1                                                                                                                                                           ");
            //query.AppendLine("       ,cm_location c2                                                                                                                                               ");
            //query.AppendLine("       ,cm_location c3                                                                                                                                               ");
            //query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                 ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                 ");
            //query.AppendLine(" order by c1.ord                                                                                                                                                     ");
            //query.AppendLine(")                                                                                                                                                                    ");
            //query.AppendLine("select loc_code                                                                                                                                                      ");
            //query.AppendLine("      ,sgccd                                                                                                                                                         ");
            //query.AppendLine("      ,lblock                                                                                                                                                        ");
            //query.AppendLine("      ,mblock                                                                                                                                                        ");
            //query.AppendLine("      ,sblock                                                                                                                                                        ");
            //query.AppendLine("      ,rel_loc_name                                                                                                                                                  ");
            //query.AppendLine("      ,timestamp                                                                                                                                                     ");
            //query.AppendLine("      ,round(to_number(sum(mnf)),2) mnf                                                                                                                              ");
            //query.AppendLine("      ,round(to_number(max(lei)),2) lei                                                                                                                              ");
            //query.AppendLine("  from                                                                                                                                                               ");
            //query.AppendLine("    (                                                                                                                                                                ");
            //query.AppendLine("    select iih.loc_code                                                                                                                                              ");
            //query.AppendLine("          ,iih.sgccd                                                                                                                                                 ");
            //query.AppendLine("          ,iih.lblock                                                                                                                                                ");
            //query.AppendLine("          ,iih.mblock                                                                                                                                                ");
            //query.AppendLine("          ,iih.sblock                                                                                                                                                ");
            //query.AppendLine("          ,iih.rel_loc_name                                                                                                                                          ");
            //query.AppendLine("          ,igr.tagname                                                                                                                                               ");
            //query.AppendLine("          ,igr.timestamp                                                                                                                                             ");
            //query.AppendLine("          ,iih.tag_gbn                                                                                                                                               ");
            //query.AppendLine("          ,decode(iih.tag_gbn, 'MNF', igr.value) mnf                                                                                                                 ");
            //query.AppendLine("          ,decode(iih.tag_gbn, 'LEI', igr.value) lei                                                                                                                 ");
            //query.AppendLine("      from                                                                                                                                                           ");
            //query.AppendLine("        (                                                                                                                                                            ");
            //query.AppendLine("        select loc.loc_code                                                                                                                                          ");
            //query.AppendLine("              ,loc.sgccd                                                                                                                                             ");
            //query.AppendLine("              ,loc.lblock                                                                                                                                            ");
            //query.AppendLine("              ,loc.mblock                                                                                                                                            ");
            //query.AppendLine("              ,loc.sblock                                                                                                                                            ");
            //query.AppendLine("              ,loc.rel_loc_name                                                                                                                                      ");
            //query.AppendLine("              ,iih.tagname                                                                                                                                           ");
            //query.AppendLine("              ,itg.tag_gbn                                                                                                                                           ");
            //query.AppendLine("          from loc                                                                                                                                                   ");
            //query.AppendLine("              ,if_ihtags iih                                                                                                                                         ");
            //query.AppendLine("              ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                         ");
            //query.AppendLine("         where 1 = 1                                                                                                                                                 ");
            //query.AppendLine("           and iih.loc_code = loc.loc_code                                                                                                                       ");
            //query.AppendLine("           and itg.tag_gbn in ('MNF', 'LEI')                                                                                                                         ");
            //query.AppendLine("           and iih.tagname = itg.tagname                                                                       ");
            //query.AppendLine("        ) iih                                                                                                                                                        ");
            //query.AppendLine("          ,if_gather_realtime igr                                                                                                                                    ");
            //query.AppendLine("     where igr.tagname = iih.tagname                                                                                                                                 ");
            //query.AppendLine("       and igr.timestamp between to_date(:STARTDATE,'yyyymmddhh24mi') and to_date(:ENDDATE,'yyyymmddhh24mi')                                                         ");
            //query.AppendLine("    )                                                                                                                                                                ");
            //query.AppendLine(" group by                                                                                                                                                            ");
            //query.AppendLine("       loc_code                                                                                                                                                      ");
            //query.AppendLine("      ,sgccd                                                                                                                                                         ");
            //query.AppendLine("      ,lblock                                                                                                                                                        ");
            //query.AppendLine("      ,mblock                                                                                                                                                        ");
            //query.AppendLine("      ,sblock                                                                                                                                                        ");
            //query.AppendLine("      ,rel_loc_name                                                                                                                                                  ");
            //query.AppendLine("      ,timestamp                                                                                                                                                     ");
            //query.AppendLine(" order by                                                                                                                                                            ");
            //query.AppendLine("       timestamp                                                                                                                                                     ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //         ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["LOC_CODE"];
            //parameters[1].Value = parameter["STARTDATE"];
            //parameters[2].Value = parameter["ENDDATE"];

            query.AppendLine("with loc as                                                                                                                   ");
            query.AppendLine("(																																");
            query.AppendLine("select c1.loc_code																											");
            query.AppendLine("	  ,c1.sgccd																													");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))	");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock											");
            query.AppendLine("	  ,c1.ftr_code																												");
            query.AppendLine("	  ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code		");
            query.AppendLine("	  ,c1.rel_loc_name																											");
            query.AppendLine("	  ,c1.ord																													");
            query.AppendLine("      ,tmp.timestamp																											");
            query.AppendLine("  from																														");
            query.AppendLine("	  (																															");
            query.AppendLine("	   select sgccd																												");
            query.AppendLine("			 ,loc_code																											");
            query.AppendLine("			 ,ploc_code																											");
            query.AppendLine("			 ,loc_name																											");
            query.AppendLine("			 ,ftr_idn																											");
            query.AppendLine("			 ,ftr_code																											");
            query.AppendLine("			 ,res_code																											");
            query.AppendLine("			 ,kt_gbn																											");
            query.AppendLine("			 ,rel_loc_name																										");
            query.AppendLine("			 ,rownum ord																										");
            query.AppendLine("		 from cm_location																										");
            query.AppendLine("		where 1 = 1																												");
            query.AppendLine("		  and loc_code = :LOC_CODE																								");
            query.AppendLine("		start with ftr_code = 'BZ001'																							");
            query.AppendLine("		connect by prior loc_code = ploc_code																					");
            query.AppendLine("		order SIBLINGS by ftr_idn																								");
            query.AppendLine("	  ) c1																														");
            query.AppendLine("	   ,cm_location c2																											");
            query.AppendLine("	   ,cm_location c3																											");
            query.AppendLine("       ,(																														");
            query.AppendLine("       select to_date(:STARTDATE,'yyyymmddhh24mi') + ((1/24/ 60)*(rownum-1)) timestamp										");
            query.AppendLine("         from dual																											");
            query.AppendLine("       connect by rownum < ((to_date(:ENDDATE,'yyyymmddhh24mi')																");
            query.AppendLine("                            -to_date(:STARTDATE,'yyyymmddhh24mi')) * 24 * 60)+2												");
            query.AppendLine("       ) tmp																													");
            query.AppendLine(" where 1 = 1																													");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							");
            query.AppendLine(" order by c1.ord																												");
            query.AppendLine(")																																");
            query.AppendLine("select iih.loc_code																											");
            query.AppendLine("	    ,iih.sgccd																												");
            query.AppendLine("	    ,iih.lblock																												");
            query.AppendLine("	    ,iih.mblock																												");
            query.AppendLine("	    ,iih.sblock																												");
            query.AppendLine("	    ,iih.rel_loc_name																										");
            query.AppendLine("      ,iih.timestamp																											");
            query.AppendLine("      ,sum(decode(iih.tag_gbn, 'MNF', igr.value)) mnf																			");
            query.AppendLine("      ,max(decode(iih.tag_gbn, 'LEI', igr.value)) lei																			");
            query.AppendLine("  from (																														");
            query.AppendLine("       select loc.loc_code																									");
            query.AppendLine("             ,loc.sgccd																										");
            query.AppendLine("             ,loc.lblock																										");
            query.AppendLine("             ,loc.mblock																										");
            query.AppendLine("             ,loc.sblock																										");
            query.AppendLine("             ,loc.rel_loc_name																								");
            query.AppendLine("             ,loc.timestamp																									");
            query.AppendLine("             ,itg.tag_gbn																										");
            query.AppendLine("             ,itg.tagname																										");
            query.AppendLine("         from loc																												");
            query.AppendLine("             ,if_ihtags iih																									");
            query.AppendLine("             ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg													");
            query.AppendLine("        where 1 = 1																											");
            query.AppendLine("          and iih.loc_code = loc.tag_loc_code																					");
            query.AppendLine("          and itg.tagname = iih.tagname																						");
            query.AppendLine("          and itg.tag_gbn in ('MNF', 'LEI')																					");
            query.AppendLine("      ) iih																													");
            query.AppendLine("      ,if_gather_realtime igr																									");
            query.AppendLine(" where igr.tagname(+) = iih.tagname																							");
            query.AppendLine("   and igr.timestamp(+) = iih.timestamp																						");
            query.AppendLine(" group by																														");
            query.AppendLine("       iih.loc_code																											");
            query.AppendLine("	    ,iih.sgccd																												");
            query.AppendLine("	    ,iih.lblock																												");
            query.AppendLine("	    ,iih.mblock																												");
            query.AppendLine("	    ,iih.sblock																												");
            query.AppendLine("	    ,iih.rel_loc_name																										");
            query.AppendLine("      ,iih.timestamp																											");
            query.AppendLine(" order by																														");
            query.AppendLine("       iih.timestamp																											");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //배수지가 아닌 경우 검색
        public void SelectFlowPatternStatus(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                                                          ");
            //query.AppendLine("(                                                                                                                                                                    ");
            //query.AppendLine("select c1.loc_code                                                                                                                                                   ");
            //query.AppendLine("      ,c1.sgccd                                                                                                                                                      ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                      ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                       ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                       ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                              ");
            //query.AppendLine("      ,c1.ftr_code                                                                                                                                                   ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                          ");
            //query.AppendLine("      ,c1.rel_loc_name                                                                                                                                               ");
            //query.AppendLine("      ,c1.ord                                                                                                                                                        ");
            //query.AppendLine("  from                                                                                                                                                               ");
            //query.AppendLine("      (                                                                                                                                                              ");
            //query.AppendLine("       select sgccd                                                                                                                                                  ");
            //query.AppendLine("             ,loc_code                                                                                                                                               ");
            //query.AppendLine("             ,ploc_code                                                                                                                                              ");
            //query.AppendLine("             ,loc_name                                                                                                                                               ");
            //query.AppendLine("             ,ftr_idn                                                                                                                                                ");
            //query.AppendLine("             ,ftr_code                                                                                                                                               ");
            //query.AppendLine("             ,res_code                                                                                                                                               ");
            //query.AppendLine("             ,kt_gbn                                                                                                                                                 ");
            //query.AppendLine("             ,rel_loc_name                                                                                                                                           ");
            //query.AppendLine("             ,rownum ord                                                                                                                                             ");
            //query.AppendLine("         from cm_location                                                                                                                                            ");
            //query.AppendLine("        where 1 = 1                                                                                                                                                  ");
            //query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                   ");
            //query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                                ");
            //query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                        ");
            //query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                    ");
            //query.AppendLine("      ) c1                                                                                                                                                           ");
            //query.AppendLine("       ,cm_location c2                                                                                                                                               ");
            //query.AppendLine("       ,cm_location c3                                                                                                                                               ");
            //query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                 ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                 ");
            //query.AppendLine(" order by c1.ord                                                                                                                                                     ");
            //query.AppendLine(")                                                                                                                                                                    ");
            //query.AppendLine("select loc_code                                                                                                                                                      ");
            //query.AppendLine("      ,sgccd                                                                                                                                                         ");
            //query.AppendLine("      ,lblock                                                                                                                                                        ");
            //query.AppendLine("      ,mblock                                                                                                                                                        ");
            //query.AppendLine("      ,sblock                                                                                                                                                        ");
            //query.AppendLine("      ,rel_loc_name                                                                                                                                                  ");
            //query.AppendLine("      ,timestamp                                                                                                                                                     ");
            //query.AppendLine("      ,sum(fri) fri                                                                                                                                                  ");
            //query.AppendLine("      ,sum(pri) pri                                                                                                                                                  ");
            //query.AppendLine("  from                                                                                                                                                               ");
            //query.AppendLine("    (                                                                                                                                                                ");
            //query.AppendLine("    select iih.loc_code                                                                                                                                              ");
            //query.AppendLine("          ,iih.sgccd                                                                                                                                                 ");
            //query.AppendLine("          ,iih.lblock                                                                                                                                                ");
            //query.AppendLine("          ,iih.mblock                                                                                                                                                ");
            //query.AppendLine("          ,iih.sblock                                                                                                                                                ");
            //query.AppendLine("          ,iih.rel_loc_name                                                                                                                                          ");
            //query.AppendLine("          ,igr.tagname                                                                                                                                               ");
            //query.AppendLine("          ,igr.timestamp                                                                                                                                             ");
            //query.AppendLine("          ,iih.tag_gbn                                                                                                                                               ");
            //query.AppendLine("          ,round(to_number(decode(iih.tag_gbn, 'FRI', igr.value)),2) fri                                                                                             ");
            //query.AppendLine("          ,round(to_number(decode(iih.tag_gbn, 'PRI', igr.value)),2) pri                                                                                             ");
            //query.AppendLine("      from                                                                                                                                                           ");
            //query.AppendLine("        (                                                                                                                                                            ");
            //query.AppendLine("        select loc.loc_code                                                                                                                                          ");
            //query.AppendLine("              ,loc.sgccd                                                                                                                                             ");
            //query.AppendLine("              ,loc.lblock                                                                                                                                            ");
            //query.AppendLine("              ,loc.mblock                                                                                                                                            ");
            //query.AppendLine("              ,loc.sblock                                                                                                                                            ");
            //query.AppendLine("              ,loc.rel_loc_name                                                                                                                                      ");
            //query.AppendLine("              ,iih.tagname                                                                                                                                           ");
            //query.AppendLine("              ,itg.tag_gbn                                                                                                                                           ");
            //query.AppendLine("          from loc                                                                                                                                                   ");
            //query.AppendLine("              ,if_ihtags iih                                                                                                                                         ");
            //query.AppendLine("              ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                         ");
            //query.AppendLine("         where 1 = 1                                                                                                                                                 ");
            //query.AppendLine("           and iih.loc_code = loc.tag_loc_code                                                                                                                       ");
            //query.AppendLine("           and itg.tag_gbn in ('FRI', 'PRI')                                                                                                                         ");
            //query.AppendLine("           and iih.tagname = itg.tagname                                                                       ");
            //query.AppendLine("        ) iih                                                                                                                                                        ");
            //query.AppendLine("          ,if_gather_realtime igr                                                                                                                                    ");
            //query.AppendLine("     where igr.tagname = iih.tagname                                                                                                                                 ");
            //query.AppendLine("       and igr.timestamp between to_date(:STARTDATE,'yyyymmddhh24mi') and to_date(:ENDDATE,'yyyymmddhh24mi')                                         ");
            //query.AppendLine("    )                                                                                                                                                                ");
            //query.AppendLine(" group by                                                                                                                                                            ");
            //query.AppendLine("       loc_code                                                                                                                                                      ");
            //query.AppendLine("      ,sgccd                                                                                                                                                         ");
            //query.AppendLine("      ,lblock                                                                                                                                                        ");
            //query.AppendLine("      ,mblock                                                                                                                                                        ");
            //query.AppendLine("      ,sblock                                                                                                                                                        ");
            //query.AppendLine("      ,rel_loc_name                                                                                                                                                  ");
            //query.AppendLine("      ,timestamp                                                                                                                                                     ");
            //query.AppendLine(" order by                                                                                                                                                            ");
            //query.AppendLine("       timestamp                                                                                                                                                     ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //         ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["LOC_CODE"];
            //parameters[1].Value = parameter["STARTDATE"];
            //parameters[2].Value = parameter["ENDDATE"];

            query.AppendLine("with loc as                                                                                                                   ");
            query.AppendLine("(																																");
            query.AppendLine("select c1.loc_code																											");
            query.AppendLine("	  ,c1.sgccd																													");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))	");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock											");
            query.AppendLine("	  ,c1.ftr_code																												");
            query.AppendLine("	  ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code		");
            query.AppendLine("	  ,c1.rel_loc_name																											");
            query.AppendLine("	  ,c1.ord																													");
            query.AppendLine("      ,tmp.timestamp																											");
            query.AppendLine("  from																														");
            query.AppendLine("	  (																															");
            query.AppendLine("	   select sgccd																												");
            query.AppendLine("			 ,loc_code																											");
            query.AppendLine("			 ,ploc_code																											");
            query.AppendLine("			 ,loc_name																											");
            query.AppendLine("			 ,ftr_idn																											");
            query.AppendLine("			 ,ftr_code																											");
            query.AppendLine("			 ,res_code																											");
            query.AppendLine("			 ,kt_gbn																											");
            query.AppendLine("			 ,rel_loc_name																										");
            query.AppendLine("			 ,rownum ord																										");
            query.AppendLine("		 from cm_location																										");
            query.AppendLine("		where 1 = 1																												");
            query.AppendLine("		  and loc_code = :LOC_CODE																								");
            query.AppendLine("		start with ftr_code = 'BZ001'																							");
            query.AppendLine("		connect by prior loc_code = ploc_code																					");
            query.AppendLine("		order SIBLINGS by ftr_idn																								");
            query.AppendLine("	  ) c1																														");
            query.AppendLine("	   ,cm_location c2																											");
            query.AppendLine("	   ,cm_location c3																											");
            query.AppendLine("       ,(																														");
            query.AppendLine("       select to_date(:STARTDATE,'yyyymmddhh24mi') + ((1/24/ 60)*(rownum-1)) timestamp										");
            query.AppendLine("         from dual																											");
            query.AppendLine("       connect by rownum < ((to_date(:ENDDATE,'yyyymmddhh24mi')																");
            query.AppendLine("                            -to_date(:STARTDATE,'yyyymmddhh24mi')) * 24 * 60)+2												");
            query.AppendLine("       ) tmp																													");
            query.AppendLine(" where 1 = 1																													");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							");
            query.AppendLine(" order by c1.ord																												");
            query.AppendLine(")																																");
            query.AppendLine("select iih.loc_code																											");
            query.AppendLine("	  ,iih.sgccd																												");
            query.AppendLine("	  ,iih.lblock																												");
            query.AppendLine("	  ,iih.mblock																												");
            query.AppendLine("	  ,iih.sblock																												");
            query.AppendLine("	  ,iih.rel_loc_name																											");
            query.AppendLine("      ,iih.timestamp																											");
            query.AppendLine("      ,round(sum(decode(iih.tag_gbn, 'FRI', igr.value)),2) fri																			");
            query.AppendLine("      ,round(sum(decode(iih.tag_gbn, 'PRI', igr.value)),2) pri																			");
            query.AppendLine("  from (																														");
            query.AppendLine("       select loc.loc_code																									");
            query.AppendLine("             ,loc.sgccd																										");
            query.AppendLine("             ,loc.lblock																										");
            query.AppendLine("             ,loc.mblock																										");
            query.AppendLine("             ,loc.sblock																										");
            query.AppendLine("             ,loc.rel_loc_name																								");
            query.AppendLine("             ,loc.timestamp																									");
            query.AppendLine("             ,itg.tag_gbn																										");
            query.AppendLine("             ,itg.tagname																										");
            query.AppendLine("         from loc																												");
            query.AppendLine("             ,if_ihtags iih																									");
            query.AppendLine("             ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg													");
            query.AppendLine("        where 1 = 1																											");
            query.AppendLine("          and iih.loc_code = loc.tag_loc_code																					");
            query.AppendLine("          and itg.tagname = iih.tagname																						");
            query.AppendLine("          and itg.tag_gbn in ('FRI', 'PRI')																					");
            query.AppendLine("      ) iih																													");
            query.AppendLine("      ,if_gather_realtime igr																									");
            query.AppendLine(" where igr.tagname(+) = iih.tagname																							");
            query.AppendLine("   and igr.timestamp(+) = iih.timestamp																						");
            query.AppendLine(" group by																														");
            query.AppendLine("       iih.loc_code																											");
            query.AppendLine("	  ,iih.sgccd																												");
            query.AppendLine("	  ,iih.lblock																												");
            query.AppendLine("	  ,iih.mblock																												");
            query.AppendLine("	  ,iih.sblock																												");
            query.AppendLine("	  ,iih.rel_loc_name																											");
            query.AppendLine("      ,iih.timestamp																											");
            query.AppendLine(" order by																														");
            query.AppendLine("       iih.timestamp																											");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }
    }
}