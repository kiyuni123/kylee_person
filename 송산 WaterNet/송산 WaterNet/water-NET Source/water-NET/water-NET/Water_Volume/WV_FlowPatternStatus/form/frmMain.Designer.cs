﻿namespace WaterNet.WV_FlowPatternStatus.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn23 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn24 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SGCCD");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn27 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("REL_LOC_NAME");
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TIMESTAMP");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRI");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn31 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MNF");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn32 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PRI");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEI");
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground1 = new ChartFX.WinForms.Adornments.SolidBackground();
            ChartFX.WinForms.SeriesAttributes seriesAttributes1 = new ChartFX.WinForms.SeriesAttributes();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dataChange = new System.Windows.Forms.ComboBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.chartVisibleBtn = new System.Windows.Forms.Button();
            this.tableVisibleBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            this.tablePanel1 = new System.Windows.Forms.Panel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.chartPanel1 = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel8.SuspendLayout();
            this.tablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.chartPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(1132, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 598);
            this.pictureBox4.TabIndex = 16;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 598);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 608);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1142, 10);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1142, 10);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.dataChange);
            this.panel8.Controls.Add(this.searchBtn);
            this.panel8.Controls.Add(this.chartVisibleBtn);
            this.panel8.Controls.Add(this.tableVisibleBtn);
            this.panel8.Controls.Add(this.excelBtn);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(10, 121);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1122, 30);
            this.panel8.TabIndex = 20;
            // 
            // dataChange
            // 
            this.dataChange.FormattingEnabled = true;
            this.dataChange.Location = new System.Drawing.Point(0, 10);
            this.dataChange.Name = "dataChange";
            this.dataChange.Size = new System.Drawing.Size(89, 20);
            this.dataChange.TabIndex = 29;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1082, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 22;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // chartVisibleBtn
            // 
            this.chartVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chartVisibleBtn.Location = new System.Drawing.Point(924, 2);
            this.chartVisibleBtn.Name = "chartVisibleBtn";
            this.chartVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.chartVisibleBtn.TabIndex = 25;
            this.chartVisibleBtn.TabStop = false;
            this.chartVisibleBtn.Text = "그래프";
            this.chartVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // tableVisibleBtn
            // 
            this.tableVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tableVisibleBtn.Location = new System.Drawing.Point(980, 2);
            this.tableVisibleBtn.Name = "tableVisibleBtn";
            this.tableVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.tableVisibleBtn.TabIndex = 24;
            this.tableVisibleBtn.TabStop = false;
            this.tableVisibleBtn.Text = "테이블";
            this.tableVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(1036, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 23;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1122, 111);
            this.searchBox1.TabIndex = 19;
            // 
            // tablePanel1
            // 
            this.tablePanel1.BackColor = System.Drawing.Color.White;
            this.tablePanel1.Controls.Add(this.ultraGrid1);
            this.tablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel1.Location = new System.Drawing.Point(0, 187);
            this.tablePanel1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Size = new System.Drawing.Size(1122, 270);
            this.tablePanel1.TabIndex = 1;
            // 
            // ultraGrid1
            // 
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance28;
            appearance29.TextHAlignAsString = "Center";
            ultraGridColumn23.Header.Appearance = appearance29;
            ultraGridColumn23.Header.Caption = "지역관리번호";
            ultraGridColumn23.Header.VisiblePosition = 0;
            ultraGridColumn23.Hidden = true;
            ultraGridColumn24.Header.Caption = "센터";
            ultraGridColumn24.Header.VisiblePosition = 1;
            ultraGridColumn24.Hidden = true;
            appearance30.TextHAlignAsString = "Center";
            ultraGridColumn25.Header.Appearance = appearance30;
            ultraGridColumn25.Header.Caption = "대블록";
            ultraGridColumn25.Header.VisiblePosition = 2;
            ultraGridColumn25.Width = 80;
            appearance31.TextHAlignAsString = "Center";
            ultraGridColumn26.Header.Appearance = appearance31;
            ultraGridColumn26.Header.Caption = "중블록";
            ultraGridColumn26.Header.VisiblePosition = 3;
            ultraGridColumn26.Width = 80;
            appearance32.TextHAlignAsString = "Center";
            ultraGridColumn27.Header.Appearance = appearance32;
            ultraGridColumn27.Header.Caption = "소블록";
            ultraGridColumn27.Header.VisiblePosition = 4;
            ultraGridColumn27.Width = 80;
            appearance33.TextHAlignAsString = "Center";
            ultraGridColumn28.Header.Appearance = appearance33;
            ultraGridColumn28.Header.Caption = "관련배수지";
            ultraGridColumn28.Header.VisiblePosition = 5;
            ultraGridColumn28.Width = 100;
            ultraGridColumn29.Format = "yyyy-MM-dd HH:mm";
            ultraGridColumn29.Header.Caption = "계측시간";
            ultraGridColumn29.Header.VisiblePosition = 6;
            ultraGridColumn29.Width = 154;
            ultraGridColumn30.Format = "###,###";
            ultraGridColumn30.Header.Caption = "유입유량(㎥/h)";
            ultraGridColumn30.Header.VisiblePosition = 7;
            ultraGridColumn30.Width = 120;
            ultraGridColumn31.Format = "###,###";
            ultraGridColumn31.Header.Caption = "유출유량(㎥/h)";
            ultraGridColumn31.Header.VisiblePosition = 8;
            ultraGridColumn31.Hidden = true;
            ultraGridColumn31.Width = 120;
            ultraGridColumn32.Format = "###,###.00";
            ultraGridColumn32.Header.Caption = "수압(kgf/㎠)";
            ultraGridColumn32.Header.VisiblePosition = 9;
            ultraGridColumn32.Width = 120;
            ultraGridColumn33.Format = "###,###.00";
            ultraGridColumn33.Header.Caption = "수위";
            ultraGridColumn33.Header.VisiblePosition = 10;
            ultraGridColumn33.Hidden = true;
            ultraGridColumn33.Width = 120;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn23,
            ultraGridColumn24,
            ultraGridColumn25,
            ultraGridColumn26,
            ultraGridColumn27,
            ultraGridColumn28,
            ultraGridColumn29,
            ultraGridColumn30,
            ultraGridColumn31,
            ultraGridColumn32,
            ultraGridColumn33});
            ultraGridBand1.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance34;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance35;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance36.BackColor2 = System.Drawing.SystemColors.Control;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance36;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance37;
            appearance38.BackColor = System.Drawing.SystemColors.Highlight;
            appearance38.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance38;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance39;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            appearance40.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance40;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance41.BackColor = System.Drawing.SystemColors.Control;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance41;
            appearance42.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance42;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance43;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance44;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1122, 270);
            this.ultraGrid1.TabIndex = 2;
            this.ultraGrid1.TabStop = false;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // chartPanel1
            // 
            this.chartPanel1.BackColor = System.Drawing.Color.White;
            this.chartPanel1.Controls.Add(this.chart1);
            this.chartPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel1.Location = new System.Drawing.Point(0, 10);
            this.chartPanel1.Margin = new System.Windows.Forms.Padding(0, 10, 0, 5);
            this.chartPanel1.Name = "chartPanel1";
            this.chartPanel1.Size = new System.Drawing.Size(1122, 167);
            this.chartPanel1.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart1.AllSeries.Line.Width = ((short)(1));
            this.chart1.AllSeries.MarkerSize = ((short)(1));
            this.chart1.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart1.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart1.AxisY.Title.Text = "";
            solidBackground1.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart1.Background = solidBackground1;
            this.chart1.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart1.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart1.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart1.LegendBox.PlotAreaOnly = false;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.MainPane.Title.Text = "";
            this.chart1.Name = "chart1";
            this.chart1.RandomData.Series = 1;
            seriesAttributes1.Visible = false;
            this.chart1.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes1});
            this.chart1.Size = new System.Drawing.Size(1122, 167);
            this.chart1.TabIndex = 7;
            this.chart1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.chartPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tablePanel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 151);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1122, 457);
            this.tableLayoutPanel1.TabIndex = 21;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 618);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmMain";
            this.Text = "블록별 공급량/ 수압트랜드 조회";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.chartPanel1.ResumeLayout(false);
            this.chartPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button chartVisibleBtn;
        private System.Windows.Forms.Button tableVisibleBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.ComboBox dataChange;
        private System.Windows.Forms.Panel tablePanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Panel chartPanel1;
        private ChartFX.WinForms.Chart chart1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}