﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.manager;
using ChartFX.WinForms;
using WaterNet.WV_Common.util;
using System.Collections;
using WaterNet.WV_FlowPatternStatus.work;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.enum1;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using EMFrame.log;

namespace WaterNet.WV_FlowPatternStatus.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private TableLayout tableLayout = null;
        private ChartManager chartManager = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE_MINUTE;
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-mm-dd hh:MM");
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            //this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;

        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];

            Utils.SetValueList(this.dataChange, VALUELIST_TYPE.CODE, "2007");
        }


        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);
            this.searchBtn.Click += new EventHandler(SelectFlowPatternStatus_EventHandler);

            this.dataChange.SelectedIndexChanged += new EventHandler(flowMinute_CheckedChanged);

            this.ultraGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid1.DoubleClickHeader += new DoubleClickHeaderEventHandler(ultraGrid1_DoubleClickHeader);
        }

        void ultraGrid1_DoubleClickHeader(object sender, DoubleClickHeaderEventArgs e)
        {
            try
            {
                string loc_code = string.Empty;
                string loc_gbn = string.Empty;

                if (e.Header.Column == null || !(new string[] { "MNF", "LEI", "FRI", "PRI" }).Contains(e.Header.Column.Key)) return;
                if (ultraGrid1.Rows.Count == 0) return;
                loc_code = (string)ultraGrid1.Rows[0].Cells["LOC_CODE"].Value;
                loc_gbn = e.Header.Column.Key;
                if (string.IsNullOrEmpty(loc_code) || loc_code.IndexOf("SUM") > -1) return;

                WV_Common.form.frmTagDescription form = new WaterNet.WV_Common.form.frmTagDescription(loc_code, loc_gbn);
                form.Show();
            }
            catch { }
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.chart1, "공급량수압트랜드현황", 0, 0, 9, 20);
                this.excelManager.AddWorksheet(this.ultraGrid1, "공급량수압트랜드현황", 10, 0);
                this.excelManager.Save("공급량수압트랜드현황", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// 유량패턴 현황 조회 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectFlowPatternStatus_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectFlowPatternStatus(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색 완료된 1분 데이터,
        /// </summary>
        private DataTable flow1Minute = null;

        /// <summary>
        /// 유량패턴 현황 조회
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectFlowPatternStatus(Hashtable parameter)
        {
            //LOCATION_TYPE type = DataUtils.GetLocationType(parameter["FTR_CODE"].ToString());

            if (parameter["FTR_CODE"].ToString() == "BZ001")
            {
                MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
                return;
            }

            //GIS에서 블럭 포커스 이동
            this.mainMap.MoveToBlock(parameter);

            this.ultraGrid1.DataSource = FlowPatternStatusWork.GetInstance().SelectFlowPatternStatus(parameter).Tables["RESULT"];
            this.InitializeChartSetting();

            //검색 완료후 1분데이터를 백업한다,
            if (this.flow1Minute != null)
            {
                this.flow1Minute.Clear();
            }

            this.flow1Minute = FlowPatternStatusWork.GetInstance().SelectFlowPatternStatus(parameter).Tables["RESULT"];

            //라디오버튼의 선택에 따른 적산값을 보여주기 위한 함수호출
            this.ChangeFlowMinute();
        }

        /// <summary>
        /// 라디오 버튼 체크확인후 그리드& 차트 데이터세팅
        /// </summary>
        private void ChangeFlowMinute()
        {
            if (this.flow1Minute == null)
            {
                return;
            }

            int minute = 0;

            try
            {
                minute = Convert.ToInt32(this.dataChange.SelectedValue);
            }
            catch (Exception e)
            {
                e = new Exception("적산 데이터 분설정의 값이 숫자 형식이 아니거나 잘못되었습니다.");
                throw e;
            }

            if (minute == 1)
            {
                this.ultraGrid1.DataSource = this.flow1Minute;
            }
            else
            {
                this.ultraGrid1.DataSource = FlowPatternStatusWork.GetInstance().ChangeFlowMinute(this.flow1Minute, minute);
            }
            this.InitializeChartSetting();
        }

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.chartManager.AllClear(0);

            DataTable table = this.ultraGrid1.DataSource as DataTable;

            if (table == null)
            {
                return;
            }

            this.chart1.Data.Series = 2;
            this.chart1.Data.Points = table.Rows.Count;

            string value1 = string.Empty;
            string value2 = string.Empty;

            this.chart1.Series[0].Gallery = Gallery.Area;
            this.chart1.Series[1].AxisY = chart1.AxisY2;

            if (table.Columns.Contains("FRI") && table.Columns.Contains("PRI"))
            {
                value1 = "FRI";
                value2 = "PRI";

                this.chart1.AxesY[0].Title.Text = "유량(㎥/h)";
                this.chart1.AxesY[1].Title.Text = "수압(kgf/㎠)";
                this.chart1.Series[0].Text = "유입유량(㎥/h)";
                this.chart1.Series[1].Text = "수압(kgf/㎠)";
            }
            else if (table.Columns.Contains("MNF") && table.Columns.Contains("LEI"))
            {
                value1 = "MNF";
                value2 = "LEI";

                this.chart1.AxesY[0].Title.Text = "유량(㎥/h)";
                this.chart1.AxesY[1].Title.Text = "수위";
                this.chart1.Series[0].Text = "유출유량(㎥/h)";
                this.chart1.Series[1].Text = "수위";
            }

            foreach (DataRow row in table.Rows)
            {
                int pointIndex = table.Rows.IndexOf(row);
                this.chart1.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyy-MM-dd HH:mm");
                this.chart1.Data[0, pointIndex] = Utils.ToDouble(row[value1]);
                this.chart1.Data[1, pointIndex] = Utils.ToDouble(row[value2]);
            }

            chart1.Series[0].Visible = true;
            chart1.Series[1].Visible = true;
            chart1.Series[1].BringToFront();
        }

        /// <summary>
        /// 유량적산 기준 선택 이벤트핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flowMinute_CheckedChanged(object sender, EventArgs e)
        {
            ComboBox target = sender as ComboBox;

            if (target != null)
            {
                this.ChangeFlowMinute();
            }
        }

        //검색시에,
        private void ultraGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            foreach (UltraGridColumn column in e.Layout.Bands[0].Columns)
            {
                if (!column.Key.Equals("TIMESTAMP"))
                {
                    column.SortIndicator = SortIndicator.Disabled;
                }

                if (column.Key == "FRI")
                {
                    e.Layout.Bands[0].Columns["FRI"].Width = 120;
                    e.Layout.Bands[0].Columns["FRI"].Header.Caption = "유입유량(㎥/h)";
                    e.Layout.Bands[0].Columns["FRI"].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    e.Layout.Bands[0].Columns["FRI"].CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    e.Layout.Bands[0].Columns["FRI"].Format = "###,###";
                    e.Layout.Bands[0].Columns["FRI"].Hidden = false;
                }
                if (column.Key == "MNF")
                {
                    e.Layout.Bands[0].Columns["MNF"].Width = 120;
                    e.Layout.Bands[0].Columns["MNF"].Header.Caption = "유출유량(㎥/h)";
                    e.Layout.Bands[0].Columns["MNF"].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    e.Layout.Bands[0].Columns["MNF"].CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    e.Layout.Bands[0].Columns["MNF"].Format = "###,###";
                    e.Layout.Bands[0].Columns["MNF"].Hidden = false;
                }
                if (column.Key == "PRI")
                {
                    e.Layout.Bands[0].Columns["PRI"].Width = 120;
                    e.Layout.Bands[0].Columns["PRI"].Header.Caption = "수압(kgf/㎠)";
                    e.Layout.Bands[0].Columns["PRI"].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    e.Layout.Bands[0].Columns["PRI"].CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    e.Layout.Bands[0].Columns["PRI"].Format = "###,###.00";
                    e.Layout.Bands[0].Columns["PRI"].Hidden = false;
                }
                if (column.Key == "LEI")
                {
                    e.Layout.Bands[0].Columns["LEI"].Width = 120;
                    e.Layout.Bands[0].Columns["LEI"].Header.Caption = "수위";
                    e.Layout.Bands[0].Columns["LEI"].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    e.Layout.Bands[0].Columns["LEI"].CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    e.Layout.Bands[0].Columns["LEI"].Format = "###,###.00";
                    e.Layout.Bands[0].Columns["LEI"].Hidden = false;
                }
            }
        }
    }
}
