﻿using System;
using WaterNet.WV_FlowPatternStatus.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_FlowPatternStatus.work
{
    public class FlowPatternStatusWork : BaseWork
    {
        private static FlowPatternStatusWork work = null;
        private FlowPatternStatusDao dao = null;

        private FlowPatternStatusWork()
        {
            dao = FlowPatternStatusDao.GetInstance();
        }

        public static FlowPatternStatusWork GetInstance()
        {
            if (work == null)
            {
                work = new FlowPatternStatusWork();
            }
            return work;
        }

        public DataSet SelectFlowPatternStatus(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                bool isReservoir = Convert.ToBoolean(dao.IsReservoir(base.DataBaseManager, parameter));

                //블록구분이 계통인경우
                if (isReservoir)
                {
                    dao.SelectFlowPatternStatus_Reservoir(base.DataBaseManager, result, "RESULT", parameter);
                }
                //블록구분이 면지역인경우, 소블록, 정수장
                else if (!isReservoir)
                {
                    dao.SelectFlowPatternStatus(base.DataBaseManager, result, "RESULT", parameter);
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        /// <summary>
        /// 1분데이터로 선택분으로 변환해서 리턴함
        /// </summary>
        /// <param name="flow1Minute">1분데이터결과</param>
        /// <param name="minute">적산할 분</param>
        /// <returns></returns>
        public DataTable ChangeFlowMinute(DataTable flow1Minute, int minute)
        {
            DataTable result = DataUtils.DataTableWapperClone(flow1Minute);

            double value = 0;
            int count = 0;

            string value1 = string.Empty;
            string value2 = string.Empty;

            if (flow1Minute.Columns.Contains("FRI") && flow1Minute.Columns.Contains("PRI"))
            {
                value1 = "FRI";
                value2 = "PRI";
            }
            else if (flow1Minute.Columns.Contains("MNF") && flow1Minute.Columns.Contains("LEI"))
            {
                value1 = "MNF";
                value2 = "LEI";
            }

            for (int i = 0; i < flow1Minute.Rows.Count; i++)
            {
                value += Utils.ToDouble(flow1Minute.Rows[i][value1]);
                count++;

                if (i!=0 && i%minute == 0)
                {
                    DataRow newRow = result.NewRow();
                    newRow["LOC_CODE"] = flow1Minute.Rows[i]["LOC_CODE"];
                    newRow["LBLOCK"] = flow1Minute.Rows[i]["LBLOCK"];
                    newRow["MBLOCK"] = flow1Minute.Rows[i]["MBLOCK"];
                    newRow["SBLOCK"] = flow1Minute.Rows[i]["SBLOCK"];
                    newRow["REL_LOC_NAME"] = flow1Minute.Rows[i]["REL_LOC_NAME"];
                    newRow["TIMESTAMP"] = flow1Minute.Rows[i]["TIMESTAMP"];
                    newRow[value1] = value/count;
                    newRow[value2] = flow1Minute.Rows[i][value2];
                    
                    result.Rows.Add(newRow);

                    value = 0;
                    count = 0;
                }
                else if(i == flow1Minute.Rows.Count-1)
                {
                    DataRow newRow = result.NewRow();
                    newRow["LOC_CODE"] = flow1Minute.Rows[i]["LOC_CODE"];
                    newRow["LBLOCK"] = flow1Minute.Rows[i]["LBLOCK"];
                    newRow["MBLOCK"] = flow1Minute.Rows[i]["MBLOCK"];
                    newRow["SBLOCK"] = flow1Minute.Rows[i]["SBLOCK"];
                    newRow["REL_LOC_NAME"] = flow1Minute.Rows[i]["REL_LOC_NAME"];
                    newRow["TIMESTAMP"] = flow1Minute.Rows[i]["TIMESTAMP"];
                    newRow[value1] = value/count;
                    newRow[value2] = flow1Minute.Rows[i][value2];
                }
            }
            return result;
        }
    }
}
