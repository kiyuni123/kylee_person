﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.form;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_RevenueWaterRatioReport.work;
using ChartFX.WinForms;
using WaterNet.WV_Common.util;
using System.IO;
using WaterNet.WV_Common.work;
using EMFrame.log;

namespace WaterNet.WV_RevenueWaterRatioReport.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private ChartManager chartManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.SINGLE_MONTH;

            //확정일자를 다음달 10일이라고 가정할 경우,
            //현재 4.5일 이라면.
            //3월을 선택하는게 아니라, 4월 10일 이전이므로 2월을 선택한다.

            int day = Convert.ToInt32(OptionWork.GetInstance().GetRevenueFix(this.searchBox1.LargeBlockObject.SelectedValue.ToString()));

            if (DateTime.Now.Day < day)
            {
                this.searchBox1.YearMonthObject.SelectedValue = DateTime.Now.AddMonths(-2).ToString("yyyy");
                this.searchBox1.MonthObject.SelectedValue = DateTime.Now.AddMonths(-2).ToString("MM");
            }
            else
            {
                this.searchBox1.YearMonthObject.SelectedValue = DateTime.Now.AddMonths(-1).ToString("yyyy");
                this.searchBox1.MonthObject.SelectedValue = DateTime.Now.AddMonths(-1).ToString("MM");
            }
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.Add(this.chart2);

            //값이 0인경우 마커를 숨긴다,
            this.chartManager.ZeroHidden(this.chart1);

            //X축의 라벨값을 일정하게 설정한다.
            this.chart1.AxisX.Staggered = false;

            //범례를 차트의 오른쪽으로 설정하고 내용은 가운데 정렬한다.
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Right;
            this.chart1.LegendBox.ContentLayout = ContentLayout.Center;
            this.chart2.LegendBox.Dock = ChartFX.WinForms.DockArea.Right;
            this.chart2.LegendBox.ContentLayout = ContentLayout.Center;
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            //컬럼너비 자동 설정
            this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            this.ultraGrid2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            //로우셀렉터 사용 안함
            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.Empty;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Black;
            this.ultraGrid2.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.Empty;
            this.ultraGrid2.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Black;

            //셀클릭가능하게..
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["PIPE_CHANGE"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["LEAKAGE_TAMSA"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["LEAKAGE_REPAIR"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["LEAKAGE_MINWON"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["METER_CHANGE"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["GUPSUJUN"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["SHUTOFF"].CellClickAction = CellClickAction.CellSelect;

            this.gridManager.SetGridGroupCaption(this.ultraGrid1, "NewGroup3", DateTime.Now.Year.ToString() + "년");
            this.gridManager.DefaultColumnsMapping(this.ultraGrid1);
            this.gridManager.DefaultColumnsMapping(this.ultraGrid2);
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            this.ultraGrid2.DoubleClickCell += new DoubleClickCellEventHandler(ultraGrid2_DoubleClickCell);
        }

        /// <summary>
        /// 엑셀버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.Load(new MemoryStream(Properties.Resources.RevenueWaterRatioReport));

                this.ExcelExport1();
                this.ExcelExport2();

                this.excelManager.AddWorksheet(this.chart1, "월유수율분석", 8, 1, 13, 29);
                this.excelManager.AddWorksheet(this.chart2, "월유수율분석", 21, 1, 28, 29);
                this.excelManager.Save("월유수율분석", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void ExcelExport1()
        {
            GroupsCollection groups = this.ultraGrid1.DisplayLayout.Bands[0].Groups;
            UltraGridRow gridRow = this.ultraGrid1.Rows[0];

            //this.excelManager.WriteLine(sheetIndex, startRow, startColumn, value, Type);
            this.excelManager.WriteLine(0, 1, 1, groups["NewGroup0"].Header.Caption);
            this.excelManager.WriteLine(0, 1, 3, groups["NewGroup1"].Header.Caption);
            this.excelManager.WriteLine(0, 1, 4, groups["NewGroup2"].Header.Caption);
            this.excelManager.WriteLine(0, 1, 5, groups["NewGroup3"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 5, groups["NewGroup4"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 7, groups["NewGroup5"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 9, groups["NewGroup6"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 11, groups["NewGroup7"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 13, groups["NewGroup8"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 15, groups["NewGroup9"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 17, groups["NewGroup10"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 19, groups["NewGroup11"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 21, groups["NewGroup12"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 23, groups["NewGroup13"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 25, groups["NewGroup14"].Header.Caption);
            this.excelManager.WriteLine(0, 2, 27, groups["NewGroup15"].Header.Caption);

            this.excelManager.WriteLine(0, 3, 1, groups["NewGroup17"].Header.Caption);
            this.excelManager.WriteLine(0, 3, 2, groups["NewGroup18"].Header.Caption);
            this.excelManager.WriteLine(0, 4, 2, groups["NewGroup19"].Header.Caption);
            this.excelManager.WriteLine(0, 5, 2, groups["NewGroup20"].Header.Caption);
            this.excelManager.WriteLine(0, 6, 2, groups["NewGroup16"].Header.Caption);

            this.excelManager.WriteLine(0, 3, 3, gridRow.Cells["LASTYEAR_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 3, gridRow.Cells["LASTYEAR_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 3, gridRow.Cells["LASTYEAR_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 3, gridRow.Cells["LASTYEAR_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 4, gridRow.Cells["THISYEAR_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 4, gridRow.Cells["THISYEAR_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 4, gridRow.Cells["THISYEAR_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 4, gridRow.Cells["THISYEAR_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 5, gridRow.Cells["1M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 5, gridRow.Cells["1M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 5, gridRow.Cells["1M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 5, gridRow.Cells["1M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 7, gridRow.Cells["2M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 7, gridRow.Cells["2M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 7, gridRow.Cells["2M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 7, gridRow.Cells["2M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 9, gridRow.Cells["3M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 9, gridRow.Cells["3M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 9, gridRow.Cells["3M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 9, gridRow.Cells["3M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 11, gridRow.Cells["4M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 11, gridRow.Cells["4M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 11, gridRow.Cells["4M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 11, gridRow.Cells["4M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 13, gridRow.Cells["5M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 13, gridRow.Cells["5M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 13, gridRow.Cells["5M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 13, gridRow.Cells["5M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 15, gridRow.Cells["6M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 15, gridRow.Cells["6M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 15, gridRow.Cells["6M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 15, gridRow.Cells["6M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 17, gridRow.Cells["7M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 17, gridRow.Cells["7M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 17, gridRow.Cells["7M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 17, gridRow.Cells["7M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 17, gridRow.Cells["8M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 17, gridRow.Cells["8M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 17, gridRow.Cells["8M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 17, gridRow.Cells["8M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 19, gridRow.Cells["9M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 19, gridRow.Cells["9M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 19, gridRow.Cells["9M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 19, gridRow.Cells["9M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 21, gridRow.Cells["10M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 21, gridRow.Cells["10M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 21, gridRow.Cells["10M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 21, gridRow.Cells["10M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 23, gridRow.Cells["11M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 23, gridRow.Cells["11M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 23, gridRow.Cells["11M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 23, gridRow.Cells["11M_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 3, 25, gridRow.Cells["12M_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 4, 25, gridRow.Cells["12M_REVENUE"].Value);
            this.excelManager.WriteLine(0, 5, 25, gridRow.Cells["12M_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 6, 25, gridRow.Cells["12M_REVENUE_RATIO"].Value);
        }

        private void ExcelExport2()
        {
            GroupsCollection groups = this.ultraGrid2.DisplayLayout.Bands[0].Groups;
            UltraGridRow gridRow = this.ultraGrid2.Rows[0];

            //this.excelManager.WriteLine(sheetIndex, startRow, startColumn, value, Type);
            this.excelManager.WriteLine(0, 13, 1, groups["NewGroup0"].Header.Caption);
            this.excelManager.WriteLine(0, 13, 3, groups["NewGroup2"].Header.Caption);
            this.excelManager.WriteLine(0, 13, 4, groups["NewGroup3"].Header.Caption);
            this.excelManager.WriteLine(0, 13, 5, groups["NewGroup18"].Header.Caption);
            this.excelManager.WriteLine(0, 13, 7, groups["NewGroup4"].Header.Caption);
            this.excelManager.WriteLine(0, 14, 7, groups["NewGroup19"].Header.Caption);
            this.excelManager.WriteLine(0, 14, 9, groups["NewGroup5"].Header.Caption);
            this.excelManager.WriteLine(0, 14, 12, groups["NewGroup6"].Header.Caption);
            this.excelManager.WriteLine(0, 15, 12, groups["NewGroup7"].Header.Caption);
            this.excelManager.WriteLine(0, 15, 14, groups["NewGroup8"].Header.Caption);
            this.excelManager.WriteLine(0, 14, 16, groups["NewGroup9"].Header.Caption);
            this.excelManager.WriteLine(0, 14, 20, groups["NewGroup10"].Header.Caption);
            this.excelManager.WriteLine(0, 14, 23, groups["NewGroup11"].Header.Caption);
            this.excelManager.WriteLine(0, 14, 26, groups["NewGroup12"].Header.Caption);
            this.excelManager.WriteLine(0, 16, 1, groups["NewGroup13"].Header.Caption);
            this.excelManager.WriteLine(0, 16, 2, groups["NewGroup14"].Header.Caption);
            this.excelManager.WriteLine(0, 17, 2, groups["NewGroup15"].Header.Caption);
            this.excelManager.WriteLine(0, 18, 2, groups["NewGroup16"].Header.Caption);
            this.excelManager.WriteLine(0, 19, 2, groups["NewGroup17"].Header.Caption);
            this.excelManager.WriteLine(0, 16, 7, groups["NewGroup1"].Header.Caption);

            this.excelManager.WriteLine(0, 16, 3, gridRow.Cells["LASTMONTH_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 17, 3, gridRow.Cells["LASTMONTH_REVENUE"].Value);
            this.excelManager.WriteLine(0, 18, 3, gridRow.Cells["LASTMONTH_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 19, 3, gridRow.Cells["LASTMONTH_REVENUE_RATIO"].Value);
            this.excelManager.WriteLine(0, 16, 4, gridRow.Cells["THISMONTH_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 17, 4, gridRow.Cells["THISMONTH_REVENUE"].Value);
            this.excelManager.WriteLine(0, 18, 4, gridRow.Cells["THISMONTH_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 19, 4, gridRow.Cells["THISMONTH_REVENUE_RATIO"].Value);
            this.excelManager.WriteLine(0, 16, 5, gridRow.Cells["DIFF_SUPPLIED"].Value);
            this.excelManager.WriteLine(0, 17, 5, gridRow.Cells["DIFF_REVENUE"].Value);
            this.excelManager.WriteLine(0, 18, 5, gridRow.Cells["DIFF_NON_REVENUE"].Value);
            this.excelManager.WriteLine(0, 19, 5, gridRow.Cells["DIFF_REVENUE_RATIO"].Value);

            this.excelManager.WriteLine(0, 16, 9, gridRow.Cells["PIPE_CHANGE"].Value);
            this.excelManager.WriteLine(0, 16, 12, gridRow.Cells["LEAKAGE_TAMSA"].Value);
            this.excelManager.WriteLine(0, 16, 14, gridRow.Cells["LEAKAGE_REPAIR"].Value);
            this.excelManager.WriteLine(0, 16, 16, gridRow.Cells["LEAKAGE_MINWON"].Value);
            this.excelManager.WriteLine(0, 16, 20, gridRow.Cells["METER_CHANGE"].Value);
            this.excelManager.WriteLine(0, 16, 23, gridRow.Cells["GUPSUJUN"].Value);
            this.excelManager.WriteLine(0, 16, 26, gridRow.Cells["SHUTOFF"].Value);
        }

        /// <summary>
        /// 검색버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;

                int year = Convert.ToInt32(parameter["STARTDATE"].ToString().Substring(0, 4));
                int month = Convert.ToInt32(parameter["STARTDATE"].ToString().Substring(4, 2));

                parameter.Remove("STARTDATE");
                parameter.Remove("ENDDATE");

                string year_mon = year.ToString("00") + month.ToString("00");

                parameter["STARTDATE"] = Utils.GetSuppliedStartdate(year_mon, "yyyyMM", parameter["LOC_CODE"].ToString());
                parameter["ENDDATE"] = Utils.GetSuppliedEnddate(year_mon, "yyyyMM", parameter["LOC_CODE"].ToString());

                parameter["LBLOCK"] = this.searchBox1.LargeBlockObject.SelectedValue;
                parameter["MBLOCK"] = this.searchBox1.MiddleBlockObject.SelectedValue;
                parameter["SBLOCK"] = this.searchBox1.SmallBlockObject.SelectedValue;

                this.SelectRevenueWaterRatioReport(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //사업추진, 클릭일경우 관련 업무를 활성화시킨다.
        private void ultraGrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            string column = e.Cell.Column.Key;
            
            if (this.SearchBox.Parameters.Count == 0)
            {
                return;
            }

            object lblock = this.SearchBox.Parameters["LBLOCK"];
            object mblock = this.SearchBox.Parameters["MBLOCK"];
            object sblock = this.SearchBox.Parameters["SBLOCK"];

            string startdate = DateTime.ParseExact(this.searchBox1.Parameters["STARTDATE"].ToString(), "yyyyMMdd", null).ToString("yyyy-MM-dd");
            string enddate = DateTime.ParseExact(this.searchBox1.Parameters["ENDDATE"].ToString(), "yyyyMMdd", null).ToString("yyyy-MM-dd");

            if (column == "PIPE_CHANGE")
            {
                WV_PipeChangeManage.form.frmMain form = new WV_PipeChangeManage.form.frmMain();
                form.MainMap = this.mainMap;
                form.Show();

                form.SearchBox.LargeBlockObject.SelectedValue = this.SearchBox.Parameters["LBLOCK"];
                form.SearchBox.MiddleBlockObject.SelectedValue = this.SearchBox.Parameters["MBLOCK"];
                form.SearchBox.SmallBlockObject.SelectedValue = this.SearchBox.Parameters["SBLOCK"];
                form.SearchBox.StartDateObject.Value = startdate;
                form.SearchBox.EndDateObject.Value = enddate;
                form.UsePlan = true;
            }
            else if (column == "LEAKAGE_TAMSA" || column == "LEAKAGE_REPAIR" || column == "LEAKAGE_MINWON")
            {
                WV_LeakageRepairManage.form.frmMain form = new WaterNet.WV_LeakageRepairManage.form.frmMain();
                form.MainMap = this.mainMap;
                form.Show();

                form.SearchBox.LargeBlockObject.SelectedValue = this.SearchBox.Parameters["LBLOCK"];
                form.SearchBox.MiddleBlockObject.SelectedValue = this.SearchBox.Parameters["MBLOCK"];
                form.SearchBox.SmallBlockObject.SelectedValue = this.SearchBox.Parameters["SBLOCK"];
                form.SearchBox.StartDateObject.Value = startdate;
                form.SearchBox.EndDateObject.Value = enddate;

                Hashtable parameter = form.SearchBox.InitializeParameter().Parameters;
                parameter["GBN"] = column;

                form.UsePlan = true;
            }
            else if (column == "METER_CHANGE")
            {
                WV_MeterChangeInformation.form.frmMain form = new WaterNet.WV_MeterChangeInformation.form.frmMain();
                form.MainMap = this.mainMap;
                form.Show();
                form.SearchBox.LargeBlockObject.SelectedValue = this.SearchBox.Parameters["LBLOCK"];
                form.SearchBox.MiddleBlockObject.SelectedValue = this.SearchBox.Parameters["MBLOCK"];
                form.SearchBox.SmallBlockObject.SelectedValue = this.SearchBox.Parameters["SBLOCK"];
                form.SearchBox.StartDateObject.Value = startdate;
                form.SearchBox.EndDateObject.Value = enddate;
                form.UsePlan = true;
            }
            else if (column == "GUPSUJUN")
            {
                WV_MeterChangeInformation.form.frmDMSearch form = new WaterNet.WV_MeterChangeInformation.form.frmDMSearch();
                form.Show();
                form.SearchBox.LargeBlockObject.SelectedValue = this.SearchBox.Parameters["LBLOCK"];
                form.SearchBox.MiddleBlockObject.SelectedValue = this.SearchBox.Parameters["MBLOCK"];
                form.SearchBox.SmallBlockObject.SelectedValue = this.SearchBox.Parameters["SBLOCK"];
                form.SearchBox.StartDateObject.Value = startdate;
                form.SearchBox.EndDateObject.Value = enddate;
                form.UsePlan = true;
            }
            else if (column == "SHUTOFF")
            {
                WV_ShutOffManage.form.frmMain form = new WaterNet.WV_ShutOffManage.form.frmMain();
                form.MainMap = this.mainMap;
                form.Show();

                form.SearchBox.LargeBlockObject.SelectedValue = this.SearchBox.Parameters["LBLOCK"];
                form.SearchBox.MiddleBlockObject.SelectedValue = this.SearchBox.Parameters["MBLOCK"];
                form.SearchBox.SmallBlockObject.SelectedValue = this.SearchBox.Parameters["SBLOCK"];
                form.SearchBox.StartDateObject.Value = startdate;
                form.SearchBox.EndDateObject.Value = enddate;
                form.UsePlan = true;
            }
        }

        /// <summary>
        /// 유수율분석조회
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectRevenueWaterRatioReport(Hashtable parameter)
        {
            //if (parameter["FTR_CODE"].ToString() == "BZ001")
            //{
            //    MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
            //    return;
            //}
            this.mainMap.MoveToBlock(parameter);
            this.mainMap.SelectedFeatureByBlock(parameter);

            //그리드의 내용 초기화
            this.gridManager.DefaultColumnsMapping(this.ultraGrid1);
            this.gridManager.DefaultColumnsMapping(this.ultraGrid2);

            DataSet dataSet = RevenueWaterRatioReportWork.GetInstance().SelectRevenueWaterRatioReport(parameter);

            //지역코드로 지역명을 검색한다.
            string location = RevenueWaterRatioReportWork.GetInstance().SelectLocationName(parameter).ToString();

            //전년도.
            string lastYear = "전년도\n(㎥/년)";
            if (dataSet.Tables["LASTYEAR"].Rows.Count != 0)
            {
                lastYear = dataSet.Tables["LASTYEAR"].Rows[0]["LASTYEAR"].ToString() + "년\n(㎥/년)";
            }

            //금년도.
            string thisYear = "금년도 누계\n(㎥/년)";
            string year = parameter["STARTDATE"].ToString().Substring(0, 4);

            if (dataSet.Tables["THISYEAR"].Rows.Count != 0)
            {
                thisYear = dataSet.Tables["THISYEAR"].Rows[0]["THISYEAR"].ToString() + "년 누계\n(㎥/년)";
                year = dataSet.Tables["THISYEAR"].Rows[0]["THISYEAR"].ToString() + "년";
            }

            //그리드내에 그룹Cation으로 블럭명을 설정한다.

            this.gridManager.SetGridGroupCaption(this.ultraGrid1, "NewGroup17", location);
            this.gridManager.SetGridGroupCaption(this.ultraGrid1, "NewGroup1", lastYear);
            this.gridManager.SetGridGroupCaption(this.ultraGrid1, "NewGroup2", thisYear);
            this.gridManager.SetGridGroupCaption(this.ultraGrid1, "NewGroup3", year);

            //전월
            string lastMonth = "전월\n(㎥/월)";
            if (dataSet.Tables["LASTMONTH"].Rows.Count != 0)
            {
                lastMonth = Convert.ToDateTime(dataSet.Tables["LASTMONTH"].Rows[0]["YEAR_MON"]).ToString("yyyy-MM") + "\n(㎥/월)";
            }

            //금월
            string thisMonth = "금월\n(㎥/월)";
            if (dataSet.Tables["THISMONTH"].Rows.Count != 0)
            {
                thisMonth = Convert.ToDateTime(dataSet.Tables["THISMONTH"].Rows[0]["YEAR_MON"]).ToString("yyyy-MM") + "\n(㎥/월)";
            }

            //그리드내에 그룹Cation으로 블럭명을 설정한다.
            this.gridManager.SetGridGroupCaption(this.ultraGrid2, "NewGroup13", location);
            this.gridManager.SetGridGroupCaption(this.ultraGrid2, "NewGroup2", lastMonth);
            this.gridManager.SetGridGroupCaption(this.ultraGrid2, "NewGroup3", thisMonth);

            this.UltraGrid1DataSetting(dataSet);
            this.UltraGrid2DataSetting(dataSet);
            this.Chart1DataSetting();
            this.Chart2DataSetting(dataSet);
        }

        //그리드1 데이터 세팅
        private void UltraGrid1DataSetting(DataSet dataSet)
        {
            if (dataSet.Tables["LASTYEAR"].Rows.Count != 0)
            {
                DataRow lastYear = dataSet.Tables["LASTYEAR"].Rows[0];
                this.ultraGrid1.Rows[0].Cells["LASTYEAR_SUPPLIED"].Value = lastYear["LASTYEAR_SUPPLIED"];
                this.ultraGrid1.Rows[0].Cells["LASTYEAR_REVENUE"].Value = lastYear["LASTYEAR_REVENUE"];
                this.ultraGrid1.Rows[0].Cells["LASTYEAR_NON_REVENUE"].Value = lastYear["LASTYEAR_NON_REVENUE"];
                this.ultraGrid1.Rows[0].Cells["LASTYEAR_REVENUE_RATIO"].Value = lastYear["LASTYEAR_REVENUE_RATIO"];
            }

            if (dataSet.Tables["THISYEAR"].Rows.Count != 0)
            {
                DataRow thisYear = dataSet.Tables["THISYEAR"].Rows[0];
                this.ultraGrid1.Rows[0].Cells["THISYEAR_SUPPLIED"].Value = thisYear["THISYEAR_SUPPLIED"];
                this.ultraGrid1.Rows[0].Cells["THISYEAR_REVENUE"].Value = thisYear["THISYEAR_REVENUE"];
                this.ultraGrid1.Rows[0].Cells["THISYEAR_NON_REVENUE"].Value = thisYear["THISYEAR_NON_REVENUE"];
                this.ultraGrid1.Rows[0].Cells["THISYEAR_REVENUE_RATIO"].Value = thisYear["THISYEAR_REVENUE_RATIO"];
            }

            if (dataSet.Tables["MONTHS"].Rows.Count != 0)
            {
                foreach (DataRow dataRow in dataSet.Tables["MONTHS"].Rows)
                {
                    string month = dataRow["MON"].ToString() + "M_";
                    this.ultraGrid1.Rows[0].Cells[month + "SUPPLIED"].Value = dataRow["SUPPLIED"];
                    this.ultraGrid1.Rows[0].Cells[month + "REVENUE"].Value = dataRow["REVENUE"];
                    this.ultraGrid1.Rows[0].Cells[month + "NON_REVENUE"].Value = dataRow["NON_REVENUE"];
                    this.ultraGrid1.Rows[0].Cells[month + "REVENUE_RATIO"].Value = dataRow["REVENUE_RATIO"];
                }
            }
        }

        //그리드2 데이터 세팅
        private void UltraGrid2DataSetting(DataSet dataSet)
        {
            double lastmonth_supplied = 0;
            double lastmonth_revenue = 0;
            double lastmonth_non_revenue = 0;
            double lastmonth_revenue_ratio = 0;

            double thismonth_supplied = 0;
            double thismonth_revenue = 0;
            double thismonth_non_revenue = 0;
            double thismonth_revenue_ratio = 0;

            //전월
            if (dataSet.Tables["LASTMONTH"].Rows.Count != 0)
            {
                DataRow lastMonth = dataSet.Tables["LASTMONTH"].Rows[0];
                this.ultraGrid2.Rows[0].Cells["LASTMONTH_SUPPLIED"].Value = lastMonth["SUPPLIED"];
                this.ultraGrid2.Rows[0].Cells["LASTMONTH_REVENUE"].Value = lastMonth["REVENUE"];
                this.ultraGrid2.Rows[0].Cells["LASTMONTH_NON_REVENUE"].Value = lastMonth["NON_REVENUE"];
                this.ultraGrid2.Rows[0].Cells["LASTMONTH_REVENUE_RATIO"].Value = lastMonth["REVENUE_RATIO"];

                lastmonth_supplied = Utils.ToDouble( lastMonth["SUPPLIED"]);
                lastmonth_revenue = Utils.ToDouble(lastMonth["REVENUE"]);
                lastmonth_non_revenue = Utils.ToDouble(lastMonth["NON_REVENUE"]);
                lastmonth_revenue_ratio = Utils.ToDouble(lastMonth["REVENUE_RATIO"]);
            }

            //금월
            if (dataSet.Tables["THISMONTH"].Rows.Count != 0)
            {
                DataRow thisMonth = dataSet.Tables["THISMONTH"].Rows[0];
                this.ultraGrid2.Rows[0].Cells["THISMONTH_SUPPLIED"].Value = thisMonth["SUPPLIED"];
                this.ultraGrid2.Rows[0].Cells["THISMONTH_REVENUE"].Value = thisMonth["REVENUE"];
                this.ultraGrid2.Rows[0].Cells["THISMONTH_NON_REVENUE"].Value = thisMonth["NON_REVENUE"];
                this.ultraGrid2.Rows[0].Cells["THISMONTH_REVENUE_RATIO"].Value = thisMonth["REVENUE_RATIO"];

                thismonth_supplied = Utils.ToDouble(thisMonth["SUPPLIED"]);
                thismonth_revenue = Utils.ToDouble(thisMonth["REVENUE"]);
                thismonth_non_revenue = Utils.ToDouble(thisMonth["NON_REVENUE"]);
                thismonth_revenue_ratio = Utils.ToDouble(thisMonth["REVENUE_RATIO"]);
            }

            //차이
            this.ultraGrid2.Rows[0].Cells["DIFF_SUPPLIED"].Value = thismonth_supplied - lastmonth_supplied;
            this.ultraGrid2.Rows[0].Cells["DIFF_REVENUE"].Value = thismonth_revenue - lastmonth_revenue;
            this.ultraGrid2.Rows[0].Cells["DIFF_NON_REVENUE"].Value = thismonth_non_revenue - lastmonth_non_revenue;
            this.ultraGrid2.Rows[0].Cells["DIFF_REVENUE_RATIO"].Value = thismonth_revenue_ratio - lastmonth_revenue_ratio;

            //금월
            if (dataSet.Tables["TOTAL_BUSINESS"].Rows.Count != 0)
            {
                DataRow total_business = dataSet.Tables["TOTAL_BUSINESS"].Rows[0];
                this.ultraGrid2.Rows[0].Cells["PIPE_CHANGE"].Value = total_business["PIPE_CHANGE"];
                this.ultraGrid2.Rows[0].Cells["LEAKAGE_TAMSA"].Value = total_business["LEAKAGE_TAMSA"];
                this.ultraGrid2.Rows[0].Cells["LEAKAGE_REPAIR"].Value = total_business["LEAKAGE_REPAIR"];
                this.ultraGrid2.Rows[0].Cells["LEAKAGE_MINWON"].Value = total_business["LEAKAGE_MINWON"];
                this.ultraGrid2.Rows[0].Cells["METER_CHANGE"].Value = total_business["METER_CHANGE"];
                this.ultraGrid2.Rows[0].Cells["GUPSUJUN"].Value = total_business["GUPSUJUN"];
                this.ultraGrid2.Rows[0].Cells["SHUTOFF"].Value = total_business["SHUTOFF"];
            }
        }

        //차트1 데이터 세팅
        private void Chart1DataSetting()
        {
            UltraGridRow gridRow = this.ultraGrid1.Rows[0];
            Chart chart = this.chartManager.Items[0];
            chart.Data.Clear();

            chart.Data.Series = 4;
            chart.Data.Points = 12;

            for (int i = 1; i < 13; i++)
            {
                string month = i.ToString() + "M_";
                chart.AxisX.Labels[i - 1] = i.ToString() + "월";
                double supplied = Utils.ToDouble(gridRow.Cells[month + "SUPPLIED"].Value);
                double revenue = Utils.ToDouble(gridRow.Cells[month + "REVENUE"].Value);
                double non_revenue = Utils.ToDouble(gridRow.Cells[month + "NON_REVENUE"].Value);
                double revenue_ratio = Utils.ToDouble(gridRow.Cells[month + "REVENUE_RATIO"].Value);

                if (supplied != 0)
                {
                    chart.Data[0, i - 1] = supplied;
                }

                if (revenue != 0)
                {
                    chart.Data[1, i - 1] = revenue;
                }

                if (non_revenue != 0)
                {
                    chart.Data[2, i - 1] = non_revenue;
                }

                if (revenue_ratio != 0)
                {
                    chart.Data[3, i - 1] = revenue_ratio;
                }
            }

            chart.Series[0].AxisY = chart1.AxisY2;
            chart.Series[0].Gallery = Gallery.Lines;
            chart.Series[0].Line.Width = 2;
            chart.Series[0].MarkerSize = 3;
            chart.Series[0].MarkerShape = MarkerShape.Diamond;

            chart.Series[1].AxisY = chart1.AxisY2;
            chart.Series[1].Gallery = Gallery.Lines;
            chart.Series[1].Line.Width = 2;
            chart.Series[1].MarkerSize = 3;
            chart.Series[0].MarkerShape = MarkerShape.Rect;

            chart.Series[2].AxisY = chart1.AxisY2;
            chart.Series[2].Gallery = Gallery.Lines;
            chart.Series[2].Line.Width = 2;
            chart.Series[2].MarkerSize = 3;

            chart.Series[3].Gallery = Gallery.Bar;
            chart.Series[3].Line.Width = 1;
            chart.Series[3].MarkerSize = 0;

            chart.Series[0].Text = "급수량(㎥/월)";
            chart.Series[1].Text = "유수수량(㎥/월)";
            chart.Series[2].Text = "무수수량(㎥/월)";
            chart.Series[3].Text = "유수율(%)";

            chart.AxesY[0].Title.Text = "유수율(%)";
            chart.AxesY[1].Title.Text = "유량(㎥/월)";

            chart.AxesY[1].LabelsFormat.Format = AxisFormat.Number;

            this.chartManager.AllShowSeries(chart);

            chart.Refresh();
        }

        //차트2 데이터 세팅
        private void Chart2DataSetting(DataSet dataSet)
        {
            DataTable mnf = dataSet.Tables["MNF"];

            if (mnf == null)
            {
                return;
            }

            Chart chart = this.chartManager.Items[1];
            chart.Data.Clear();

            chart.Data.Series = 1;
            chart.Data.Points = mnf.Rows.Count;
            foreach (DataRow dataRow in mnf.Rows)
            {
                int pointIndex = mnf.Rows.IndexOf(dataRow);
                chart.AxisX.Labels[pointIndex] = Convert.ToDateTime(dataRow["TIMESTAMP"]).ToString("MM월dd일");
                chart.Data[0, pointIndex] = Utils.ToDouble(dataRow["MNF"]);
            }

            chart.Series[0].Gallery = Gallery.Lines;
            chart.Series[0].Line.Width = 2;
            chart.Series[0].MarkerSize = 3;
            chart.Series[0].MarkerShape = MarkerShape.Diamond;

            chart.Series[0].Text = "야간최소유량(㎥/일)";

            chart.AxesY[0].Title.Text = "유량(㎥/일)";
            chart.AxesY[0].LabelsFormat.Format = AxisFormat.Number;

            this.chartManager.AllShowSeries(chart);

            chart.Refresh();
        }
    }
}
