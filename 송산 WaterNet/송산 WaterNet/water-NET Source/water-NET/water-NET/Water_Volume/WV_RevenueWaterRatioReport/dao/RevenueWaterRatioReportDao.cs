﻿using System.Text;
using WaterNet.WV_Common.dao;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;
using WaterNet.WaterNetCore;

namespace WaterNet.WV_RevenueWaterRatioReport.dao
{
    public class RevenueWaterRatioReportDao: BaseDao
    {
        private static RevenueWaterRatioReportDao dao = null;

        private RevenueWaterRatioReportDao() { }

        public static RevenueWaterRatioReportDao GetInstance()
        {
            if (dao == null)
            {
                dao = new RevenueWaterRatioReportDao();
            }
            return dao;
        }

        //전년누적
        public void SelectLastYeayWaterRatioReport(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc_code                                                                                                                ");
            query.AppendLine("      ,to_char(to_date(year_mon,'yyyymm'),'yyyy') as lastyear                                                                  ");
            query.AppendLine("      ,sum(water_supplied+nvl(add_water_supplied,0)) as lastyear_supplied                                                      ");
            query.AppendLine("      ,sum(revenue+nvl(add_revenue,0)) as lastyear_revenue                                                                     ");
            query.AppendLine("      ,(sum(water_supplied+nvl(add_water_supplied,0)) - sum(revenue+nvl(add_revenue,0))) as lastyear_non_revenue               ");
            query.AppendLine("      ,round((sum(revenue+nvl(add_revenue,0))/decode(sum(water_supplied+nvl(add_water_supplied,0)),0,null,sum(water_supplied+nvl(add_water_supplied,0))))*100,1) as lastyear_revenue_ratio  ");
            query.AppendLine("  from wv_revenue_ratio                                                                                                        ");
            query.AppendLine(" where loc_code = :LOC_CODE                                                                                                    ");
            query.AppendLine("   and year_mon between to_char(add_months(to_date(:ENDDATE,'yyyymmdd'),-12),'yyyy')||'01'                                     ");
            query.AppendLine("                    and to_char(add_months(to_date(:ENDDATE,'yyyymmdd'),-12),'yyyy')||'12'                                     ");
            query.AppendLine(" group by loc_code,to_char(to_date(year_mon,'yyyymm'),'yyyy')                                                                  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //금년누적
        public void SelectThisYeayWaterRatioReport(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc_code                                                                                                                ");
            query.AppendLine("      ,to_char(to_date(year_mon,'yyyymm'),'yyyy') as thisyear                                                                  ");
            query.AppendLine("      ,sum(water_supplied+nvl(add_water_supplied,0)) as thisyear_supplied                                                      ");
            query.AppendLine("      ,sum(revenue+nvl(add_revenue,0)) as thisyear_revenue                                                                     ");
            query.AppendLine("      ,(sum(water_supplied+nvl(add_water_supplied,0)) - sum(revenue+nvl(add_revenue,0))) as thisyear_non_revenue               ");
            query.AppendLine("      ,round((sum(revenue+nvl(add_revenue,0))/decode(sum(water_supplied+nvl(add_water_supplied,0)),0,null,sum(water_supplied+nvl(add_water_supplied,0))))*100,1) as thisyear_revenue_ratio  ");
            query.AppendLine("  from wv_revenue_ratio                                                                                                        ");
            query.AppendLine(" where loc_code = :LOC_CODE                                                                                                    ");
            query.AppendLine("   and year_mon between to_char(to_date(:ENDDATE,'yyyymmdd'),'yyyy')||'01'                                                     ");
            query.AppendLine("                    and to_char(to_date(:ENDDATE,'yyyymmdd'),'yyyy')||to_char(to_date(:ENDDATE,'yyyymmdd'),'mm')               ");
            query.AppendLine(" group by loc_code,to_char(to_date(year_mon,'yyyymm'),'yyyy')                                                                  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //월별현황
        public void SelectWaterRatioReport(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc_code                                                                                                                ");
            query.AppendLine("      ,to_number(to_char(to_date(year_mon,'yyyymm'),'mm')) mon                                                                 ");
            query.AppendLine("      ,water_supplied+nvl(add_water_supplied,0) as supplied                                                                    ");
            query.AppendLine("      ,revenue+nvl(add_revenue,0) as revenue                                                                                   ");
            query.AppendLine("      ,((water_supplied+nvl(add_water_supplied,0)) - (revenue+nvl(add_revenue,0))) as non_revenue                              ");
            query.AppendLine("      ,round(((revenue+nvl(add_revenue,0))/decode((water_supplied+nvl(add_water_supplied,0)),0,null,(water_supplied+nvl(add_water_supplied,0))))*100,1) as revenue_ratio                 ");
            query.AppendLine("  from wv_revenue_ratio                                                                                                        ");
            query.AppendLine(" where loc_code = :LOC_CODE                                                                                                    ");
            query.AppendLine("   and year_mon between to_char(to_date(:ENDDATE,'yyyymmdd'),'yyyy')||'01'                                                     ");
            query.AppendLine("                    and to_char(to_date(:ENDDATE,'yyyymmdd'),'yyyy')||to_char(to_date(:ENDDATE,'yyyymmdd'),'mm')               ");
            query.AppendLine(" order by year_mon                                                                                                             ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //월현황
        public void SelectWaterRatioReportMonth(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc_code                                                                                                                ");
            query.AppendLine("      ,to_date(year_mon,'yyyymm') as year_mon                                                                                  ");
            query.AppendLine("      ,water_supplied+nvl(add_water_supplied,0) as supplied                                                                    ");
            query.AppendLine("      ,revenue+nvl(add_revenue,0) as revenue                                                                                   ");
            query.AppendLine("      ,((water_supplied+nvl(add_water_supplied,0)) - (revenue+nvl(add_revenue,0))) as non_revenue                              ");
            query.AppendLine("      ,round(((revenue+nvl(add_revenue,0))/decode((water_supplied+nvl(add_water_supplied,0)),0,null,(water_supplied+nvl(add_water_supplied,0))))*100,1) as revenue_ratio                 ");
            //query.AppendLine("      ,water_supplied as supplied                                                                                            ");
            //query.AppendLine("      ,revenue as revenue                                                                                                    ");
            //query.AppendLine("      ,(water_supplied - revenue) as non_revenue                                                                             ");
            //query.AppendLine("      ,round((revenue/water_supplied)*100,1) as revenue_ratio                                                                ");
            query.AppendLine("  from wv_revenue_ratio                                                                                                        ");
            query.AppendLine(" where loc_code = :LOC_CODE                                                                                                    ");
            query.AppendLine("   and year_mon = to_char(to_date(:ENDDATE,'yyyymmdd'),'yyyymm')                                                               ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //지역명검색
        public object SelectLocationName(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            
            query.AppendLine("select loc_name                                                                                                              ");
            query.AppendLine("  from cm_location                                                                                                           ");
            query.AppendLine(" where loc_code = :LOC_CODE                                                                                                  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //사업추진현황
        public void SelectBusinessPromotion(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                             ");
            query.AppendLine("(                                                                                                                                       ");
            query.AppendLine("select c1.loc_code                                                                                                                      ");
            query.AppendLine("      ,c1.sgccd                                                                                                                         ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                          ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                          ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                           ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                           ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                                 ");
            query.AppendLine("      ,c1.ord                                                                                                                           ");
            query.AppendLine("  from                                                                                                                                  ");
            query.AppendLine("      (                                                                                                                                 ");
            query.AppendLine("       select sgccd                                                                                                                     ");
            query.AppendLine("             ,loc_code                                                                                                                  ");
            query.AppendLine("             ,ploc_code                                                                                                                 ");
            query.AppendLine("             ,loc_name                                                                                                                  ");
            query.AppendLine("             ,ftr_idn                                                                                                                   ");
            query.AppendLine("             ,ftr_code                                                                                                                  ");
            query.AppendLine("             ,rel_loc_name                                                                                                              ");
            query.AppendLine("             ,rownum ord                                                                                                                ");
            query.AppendLine("         from cm_location                                                                                                               ");
            query.AppendLine("        where 1 = 1                                                                                                                     ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                                 ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                           ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                       ");
            query.AppendLine("      ) c1                                                                                                                              ");
            query.AppendLine("       ,cm_location c2                                                                                                                  ");
            query.AppendLine("       ,cm_location c3                                                                                                                  ");
            query.AppendLine(" where 1 = 1                                                                                                                            ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                    ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                    ");
            query.AppendLine(" order by c1.ord                                                                                                                        ");
            query.AppendLine(")                                                                                                                                       ");
            query.AppendLine("select wde.pip_change as pipe_change                                                                                                    ");
            query.AppendLine("      ,wlr1.leakage_tamsa as leakage_tamsa                                                                                              ");
            query.AppendLine("      ,wlr2.leakage_repair as leakage_repair                                                                                            ");
            query.AppendLine("      ,wlr3.leakage_minwon as leakage_minwon                                                                                            ");
            query.AppendLine("      ,mtr.meter_change as meter_change                                                                                                 ");
            query.AppendLine("      ,wcd.gupsujun as gupsujun                                                                                                         ");
            query.AppendLine("      ,wec.shutoff as shutoff                                                                                                           ");
            query.AppendLine("  from                                                                                                                                  ");
            query.AppendLine("        (                                                                                                                               ");
            query.AppendLine("        select sum(wde.pip_len) pip_change                                                                                              ");
            query.AppendLine("          from wv_degeche wde                                                                                                           ");
            query.AppendLine("              ,loc loc                                                                                                                  ");
            query.AppendLine("         where 1 = 1                                                                                                                    ");
            query.AppendLine("           and wde.loc_code = loc.loc_code                                                                                              ");
            query.AppendLine("           and wde.datee between :STARTDATE and :ENDDATE                                                                                ");
            query.AppendLine("        ) wde                                                                                                                           ");
            
            //누수탐사확인
            query.AppendLine("        ,(                                                                                                                              ");
            query.AppendLine("        select count(*) leakage_tamsa                                                                                                   ");
            query.AppendLine("          from wv_leak_restore wlr                                                                                                     ");
            query.AppendLine("              ,loc loc                                                                                                                  ");
            query.AppendLine("         where 1 = 1                                                                                                                    ");
            query.AppendLine("           and wlr.loc_code = loc.loc_code                                                                                              ");
            query.AppendLine("           and wlr.lek_ymd between :STARTDATE and :ENDDATE                                                                              ");
            query.AppendLine("           and wlr.lek_cde != 'BEG001'                                                                                                  ");
            query.AppendLine("        ) wlr1                                                                                                                          ");
            
            //누수탐사복구
            query.AppendLine("        ,(                                                                                                                              ");
            query.AppendLine("        select count(*) leakage_repair                                                                                                  ");
            query.AppendLine("          from wv_leak_restore wlr                                                                                                     ");
            query.AppendLine("              ,loc loc                                                                                                                  ");
            query.AppendLine("         where 1 = 1                                                                                                                    ");
            query.AppendLine("           and wlr.loc_code = loc.loc_code                                                                                              ");
            query.AppendLine("           and wlr.ree_ymd between :STARTDATE and :ENDDATE                                                                              ");
            query.AppendLine("           and wlr.lek_cde != 'BEG001'                                                                                                  ");
            query.AppendLine("        ) wlr2                                                                                                                          ");
            
            //누수민원복구
            query.AppendLine("        ,(                                                                                                                              ");
            query.AppendLine("        select count(*) leakage_minwon                                                                                                  ");
            query.AppendLine("          from wv_leak_restore wlr                                                                                                     ");
            query.AppendLine("              ,loc loc                                                                                                                  ");
            query.AppendLine("         where 1 = 1                                                                                                                    ");
            query.AppendLine("           and wlr.loc_code = loc.loc_code                                                                                              ");
            query.AppendLine("           and wlr.ree_ymd between :STARTDATE and :ENDDATE                                                                              ");
            query.AppendLine("           and wlr.lek_cde = 'BEG001'                                                                                                   ");
            query.AppendLine("        ) wlr3                                                                                                                          ");
            query.AppendLine("        ,(                                                                                                                              ");
            query.AppendLine("        select count(*) meter_change                                                                                                    ");
            query.AppendLine("          from                                                                                                                          ");
            query.AppendLine("              (                                                                                                                         ");
            query.AppendLine("              select dmno from wv_mtrchg where mddt between :STARTDATE and :ENDDATE                                                     ");
            query.AppendLine("              union all                                                                                                                 ");
            query.AppendLine("              select dmno from wi_mtrchginfo where mddt between :STARTDATE and :ENDDATE                                                    ");
            query.AppendLine("              ) mtr                                                                                                                     ");
            query.AppendLine("              ,wi_dminfo dmi                                                                                                               ");
            query.AppendLine("              ,loc loc                                                                                                                  ");
            query.AppendLine("         where 1= 1                                                                                                                     ");
            query.AppendLine("            and dmi.dmno = mtr.dmno                                                                                                     ");
            query.AppendLine("            and dmi.lftridn = loc.lftridn                                                                                               ");
            query.AppendLine("            and dmi.mftridn = loc.mftridn                                                                                               ");
            query.AppendLine("            and dmi.sftridn = loc.sftridn                                                                                               ");
            query.AppendLine("        ) mtr                                                                                                                           ");
            query.AppendLine("        ,(                                                                                                                              ");
            query.AppendLine("        select sum(gupsujunsu) gupsujun                                                                                                 ");
            query.AppendLine("          from wv_consumer_day wcd                                                                                                      ");
            query.AppendLine("              ,loc                                                                                                                      ");
            query.AppendLine("         where 1 = 1                                                                                                                    ");
            query.AppendLine("           and wcd.loc_code = loc.loc_code                                                                                              ");
            query.AppendLine("           and wcd.datee = :ENDDATE                                                                                                     ");
            query.AppendLine("        ) wcd                                                                                                                           ");
            query.AppendLine("        ,(                                                                                                                              ");
            query.AppendLine("        select count(*) shutoff                                                                                                         ");
            query.AppendLine("          from wv_etc_cutoff wec                                                                                                        ");
            query.AppendLine("              ,loc                                                                                                                      ");
            query.AppendLine("         where 1 = 1                                                                                                                    ");
            query.AppendLine("           and wec.loc_code = loc.loc_code                                                                                              ");
            query.AppendLine("           and wec.datee between :STARTDATE and :ENDDATE                                                                                ");
            query.AppendLine("        ) wec                                                                                                                           ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["STARTDATE"];
            parameters[6].Value = parameter["ENDDATE"];
            parameters[7].Value = parameter["STARTDATE"];
            parameters[8].Value = parameter["ENDDATE"];
            parameters[9].Value = parameter["STARTDATE"];
            parameters[10].Value = parameter["ENDDATE"];
            parameters[11].Value = parameter["STARTDATE"];
            parameters[12].Value = parameter["ENDDATE"];
            parameters[13].Value = parameter["ENDDATE"];
            parameters[14].Value = parameter["STARTDATE"];
            parameters[15].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //야간최소유량조회
        //필터링 존재하는경우 필터링 데이터 아닌경우 이동평균야간최소유량 데이터
        public void SelectMNF(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                 ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,kt_gbn                                                                                                               ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                 ");
            query.AppendLine("        start with ftr_code = 'BZ001'                                                                                              ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("      ) c1                                                                                                                         ");
            query.AppendLine("       ,cm_location c2                                                                                                             ");
            query.AppendLine("       ,cm_location c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select loc.loc_code                                                                                                                ");
            query.AppendLine("      ,to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd') timestamp                                                             ");
            query.AppendLine("      ,round(sum(to_number(iwm.filtering)),1) mnf                                                                                  ");
            query.AppendLine("  from loc                                                                                                                         ");
            query.AppendLine("      ,if_ihtags iih                                                                                                               ");
            query.AppendLine("      ,if_wv_mnf iwm                                                                                                               ");
            query.AppendLine("      ,(                                                                                                                           ");
            query.AppendLine("        select tagname, tag_gbn, dummy_relation from if_tag_gbn                                                                    ");
            query.AppendLine("      ) itg                                                                                                                        ");
            query.AppendLine("  where 1 = 1                                                                                                                      ");
            query.AppendLine("    and iih.loc_code = loc.tag_loc_code                                                                                            ");
            query.AppendLine("    and iih.tagname = itg.tagname                                                                                                  ");
            query.AppendLine("    and itg.tag_gbn = 'MNF'                                                                                                        ");
            query.AppendLine("    and iwm.tagname = itg.tagname                                                                                                  ");
            query.AppendLine("    and iwm.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')          ");
            query.AppendLine(" group by loc.loc_code, to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd')                                                      ");
            query.AppendLine(" order by timestamp                                                                                                                ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }
    }
}
