﻿using System;
using WaterNet.WV_RevenueWaterRatioReport.dao;
using System.Collections;
using WaterNet.WV_Common.work;
using System.Data;

namespace WaterNet.WV_RevenueWaterRatioReport.work
{
    public class RevenueWaterRatioReportWork: BaseWork
    {
        private static RevenueWaterRatioReportWork work = null;
        private RevenueWaterRatioReportDao dao = null;

        public static RevenueWaterRatioReportWork GetInstance()
        {
            if (work == null)
            {
                work = new RevenueWaterRatioReportWork();
            }
            return work;
        }

        private RevenueWaterRatioReportWork()
        {
            this.dao = RevenueWaterRatioReportDao.GetInstance();
        }

        public DataSet SelectRevenueWaterRatioReport(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                //전년
                this.dao.SelectLastYeayWaterRatioReport(DataBaseManager, result, "LASTYEAR", parameter);
                
                //금년
                this.dao.SelectThisYeayWaterRatioReport(DataBaseManager, result, "THISYEAR", parameter);
                
                //월별현황
                this.dao.SelectWaterRatioReport(DataBaseManager, result, "MONTHS", parameter);

                
                //검색일자를 한달전으로 세팅해야함(전월을 구하기위함)
                Hashtable parameter2 = new Hashtable();
                parameter2["LOC_CODE"] = parameter["LOC_CODE"];
                parameter2["ENDDATE"] = parameter["STARTDATE"];

                //전월
                this.dao.SelectWaterRatioReportMonth(DataBaseManager, result, "LASTMONTH", parameter2);

                //금월
                this.dao.SelectWaterRatioReportMonth(DataBaseManager, result, "THISMONTH", parameter);

                //금월사업추진내역(월계)
                this.dao.SelectBusinessPromotion(DataBaseManager, result, "TOTAL_BUSINESS", parameter);

                //금월사업추진내역(일자별)

                //야간최소유량
                this.dao.SelectMNF(DataBaseManager, result, "MNF", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public object SelectLocationName(Hashtable parameter)
        {
            object result = string.Empty;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = this.dao.SelectLocationName(DataBaseManager, parameter);
                
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

    }
}
