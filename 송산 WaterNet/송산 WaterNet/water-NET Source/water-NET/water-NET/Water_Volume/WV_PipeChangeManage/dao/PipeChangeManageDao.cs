﻿using System.Text;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using Oracle.DataAccess.Client;
using WaterNet.WV_Common.dao;

namespace WaterNet.WV_PipeChangeManage.dao
{
    public class PipeChangeManageDao : BaseDao
    {
        private static PipeChangeManageDao dao = null;

        private PipeChangeManageDao() { }

        public static PipeChangeManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new PipeChangeManageDao();
            }
            return dao;
        }

        //관교체통수 목록조회
        public object HasDegeche(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select decode(count(*), 0, 'False', 'True')                                                                                                                                ");
            query.AppendLine("  from wv_degeche                                                                                                                                                          ");
            query.AppendLine(" where loc_code = :LOC_CODE                                                                                                                                                ");
            query.AppendLine("   and datee = :DATEE                                                                                                                                                      ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //관교체통수 목록조회
        public void SelectDegeche(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                                                                ");
            query.AppendLine("(                                                                                                                                                                          ");
            query.AppendLine("select c1.loc_code                                                                                                                                                         ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                                            ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                                   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                                   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                                    ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                                                         ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                                ");
            query.AppendLine("      ,decode(c1.ftr_code, 'BZ002', (select loc_gbn from cm_location where loc_name = c1.rel_loc_name)) rel_loc_gbn                                                        ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                                                                     ");
            query.AppendLine("      ,c1.ord                                                                                                                                                              ");
            query.AppendLine("  from                                                                                                                                                                     ");
            query.AppendLine("      (                                                                                                                                                                    ");
            query.AppendLine("       select sgccd                                                                                                                                                        ");
            query.AppendLine("             ,loc_code                                                                                                                                                     ");
            query.AppendLine("             ,ploc_code                                                                                                                                                    ");
            query.AppendLine("             ,loc_name                                                                                                                                                     ");
            query.AppendLine("             ,ftr_idn                                                                                                                                                      ");
            query.AppendLine("             ,ftr_code                                                                                                                                                     ");
            query.AppendLine("             ,rel_loc_name                                                                                                                                                 ");
            query.AppendLine("             ,kt_gbn                                                                                                                                                       ");
            query.AppendLine("             ,rownum ord                                                                                                                                                   ");
            query.AppendLine("         from cm_location                                                                                                                                                  ");
            query.AppendLine("        where 1 = 1                                                                                                                                                        ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                                                                    ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                              ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                          ");
            query.AppendLine("      ) c1                                                                                                                                                                 ");
            query.AppendLine("       ,cm_location c2                                                                                                                                                     ");
            query.AppendLine("       ,cm_location c3                                                                                                                                                     ");
            query.AppendLine(" where 1 = 1                                                                                                                                                               ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                       ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                       ");
            query.AppendLine(" order by c1.ord                                                                                                                                                           ");
            query.AppendLine(")                                                                                                                                                                          ");
            query.AppendLine("select loc.loc_code                                                                                                                                                        ");
            query.AppendLine("      ,loc.sgccd                                                                                                                                                           ");
            query.AppendLine("      ,loc.lblock                                                                                                                                                          ");
            query.AppendLine("      ,loc.mblock                                                                                                                                                          ");
            query.AppendLine("      ,loc.sblock                                                                                                                                                          ");
            query.AppendLine("      ,to_date(wde.datee,'yyyymmdd') datee                                                                                                                                 ");
            query.AppendLine("      ,wde.mdfy_yn                                                                                                                                                         ");
            query.AppendLine("      ,sum(wde.pip_len) pip_len                                                                                                                                            ");
            query.AppendLine("  from loc                                                                                                                                                                 ");
            query.AppendLine("      ,wv_degeche wde                                                                                                                                                      ");
            query.AppendLine(" where 1 = 1                                                                                                                                                               ");
            query.AppendLine("   and wde.loc_code = loc.loc_code                                                                                                                                         ");
            query.AppendLine("   and wde.datee between :STARTDATE and :ENDDATE                                                                                                                           ");
            query.AppendLine(" group by                                                                                                                                                                  ");
            query.AppendLine("       loc.loc_code                                                                                                                                                        ");
            query.AppendLine("      ,loc.sgccd                                                                                                                                                           ");
            query.AppendLine("      ,loc.lblock                                                                                                                                                          ");
            query.AppendLine("      ,loc.mblock                                                                                                                                                          ");
            query.AppendLine("      ,loc.sblock                                                                                                                                                          ");
            query.AppendLine("      ,to_date(wde.datee,'yyyymmdd')                                                                                                                                       ");
            query.AppendLine("      ,wde.mdfy_yn                                                                                                                                                         ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //관교체통수 목록조회
        public void SelectDegecheSection(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                                                          ");
            query.AppendLine("(                                                                                                                                                                    ");
            query.AppendLine("select c1.loc_code                                                                                                                                                   ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                      ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                              ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                                                   ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                          ");
            query.AppendLine("      ,decode(c1.ftr_code, 'BZ002', (select loc_gbn from cm_location where loc_name = c1.rel_loc_name)) rel_loc_gbn                                                  ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                                                               ");
            query.AppendLine("      ,c1.ord                                                                                                                                                        ");
            query.AppendLine("  from                                                                                                                                                               ");
            query.AppendLine("      (                                                                                                                                                              ");
            query.AppendLine("       select sgccd                                                                                                                                                  ");
            query.AppendLine("             ,loc_code                                                                                                                                               ");
            query.AppendLine("             ,ploc_code                                                                                                                                              ");
            query.AppendLine("             ,loc_name                                                                                                                                               ");
            query.AppendLine("             ,ftr_idn                                                                                                                                                ");
            query.AppendLine("             ,ftr_code                                                                                                                                               ");
            query.AppendLine("             ,rel_loc_name                                                                                                                                           ");
            query.AppendLine("             ,kt_gbn                                                                                                                                                 ");
            query.AppendLine("             ,rownum ord                                                                                                                                             ");
            query.AppendLine("         from cm_location                                                                                                                                            ");
            query.AppendLine("        where 1 = 1                                                                                                                                                  ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                   ");
            query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                                ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                        ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                    ");
            query.AppendLine("      ) c1                                                                                                                                                           ");
            query.AppendLine("       ,cm_location c2                                                                                                                                               ");
            query.AppendLine("       ,cm_location c3                                                                                                                                               ");
            query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                 ");
            query.AppendLine(" order by c1.ord                                                                                                                                                     ");
            query.AppendLine(")                                                                                                                                                                    ");
            query.AppendLine("select wde.loc_code                                                                                                                                                  ");
            query.AppendLine("      ,to_date(wde.datee,'yyyymmdd') datee                                                                                                                           ");
            query.AppendLine("      ,wde.mdfy_yn                                                                                                                                                   ");
            query.AppendLine("      ,wde.section                                                                                                                                                   ");
            query.AppendLine("      ,sum(wde.pip_len) pip_len                                                                                                                                      ");
            query.AppendLine("  from loc                                                                                                                                                           ");
            query.AppendLine("      ,wv_degeche wde                                                                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            query.AppendLine("   and wde.loc_code = loc.loc_code                                                                                                                                   ");
            query.AppendLine("   and wde.datee = :DATEE                                                                                                                                            ");
            query.AppendLine("   and wde.mdfy_yn = :MDFY_YN                                                                                                                                        ");
            query.AppendLine(" group by                                                                                                                                                            ");
            query.AppendLine("       wde.loc_code                                                                                                                                                  ");
            query.AppendLine("      ,wde.datee                                                                                                                                                     ");
            query.AppendLine("      ,wde.mdfy_yn                                                                                                                                                   ");
            query.AppendLine("      ,wde.section                                                                                                                                                   ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
	                ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["MDFY_YN"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //관교체통수 목록조회
        public void SelectDegecheSectionDetail(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc_code                                                                                                                                                      ");
            query.AppendLine("      ,to_date(datee,'yyyymmdd') datee                                                                                                                               ");
            query.AppendLine("      ,mdfy_yn                                                                                                                                                       ");
            query.AppendLine("      ,section                                                                                                                                                       ");
            query.AppendLine("      ,ftr_idn                                                                                                                                                       ");
            query.AppendLine("      ,pip_dip                                                                                                                                                       ");
            query.AppendLine("      ,pip_len pip_len                                                                                                                                               ");
            query.AppendLine("      ,ftr_cde                                                                                                                                                       ");
            query.AppendLine("  from wv_degeche wde                                                                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            query.AppendLine("   and loc_code = :LOC_CODE                                                                                                                                          ");
            query.AppendLine("   and datee = :DATEE                                                                                                                                                ");
            query.AppendLine("   and mdfy_yn = :MDFY_YN                                                                                                                                            ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
	                ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["MDFY_YN"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }



        //관교체통수이력 삭제
        public void DeleteDegecheSectionDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete wv_degeche                                                                      ");
            query.AppendLine(" where 1 = 1                                                                           ");
            query.AppendLine("   and loc_code = :LOC_CODE                                                            ");
            query.AppendLine("   and datee = :DATEE                                                                  ");
            query.AppendLine("   and mdfy_yn = 'Y'                                                                   ");
            

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }


        //관교체통수이력 등록
        public void InsertDegecheSectionDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
             
            query.AppendLine("insert into wv_degeche                                                                ");
            query.AppendLine("select :LOC_CODE                                                                      ");
            query.AppendLine("      ,:DATEE                                                                         ");
            query.AppendLine("      ,'Y'                                                                            ");
            query.AppendLine("      ,:SECTION                                                                       ");
            query.AppendLine("      ,:FTR_IDN                                                                       ");
            query.AppendLine("      ,:PIP_DIP                                                                       ");
            query.AppendLine("      ,:PIP_LEN                                                                       ");
            query.AppendLine("      ,:FTR_CDE                                                                       ");
            query.AppendLine("  from dual                                                                           ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
	                ,new OracleParameter("SECTION", OracleDbType.Varchar2)
                    ,new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                    ,new OracleParameter("PIP_DIP", OracleDbType.Varchar2)
                    ,new OracleParameter("PIP_LEN", OracleDbType.Varchar2)
                    ,new OracleParameter("FTR_CDE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["SECTION"];
            parameters[3].Value = parameter["FTR_IDN"];
            parameters[4].Value = parameter["PIP_DIP"];
            parameters[5].Value = parameter["PIP_LEN"];
            parameters[6].Value = parameter["FTR_CDE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //관교체통수이력 등록
        public void UpdateExcelImport(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WV_DEGECHE A ");
            query.AppendLine("USING ");
            query.AppendLine("( ");
            query.AppendLine("      SELECT :LOC_CODE LOC_CODE ");
            query.AppendLine("            ,:DATEE DATEE ");
            query.AppendLine("            ,:MDFY_YN MDFY_YN ");
            query.AppendLine("            ,:SECTION SECTION ");
            query.AppendLine("            ,:FTR_IDN FTR_IDN ");
            query.AppendLine("            ,:PIP_DIP PIP_DIP ");
            query.AppendLine("            ,:PIP_LEN PIP_LEN ");
            query.AppendLine("            ,:FTR_CDE FTR_CDE ");
            query.AppendLine("        FROM DUAL ");
            query.AppendLine(") B ");
            query.AppendLine("   ON ");
            query.AppendLine("( ");
            query.AppendLine("             A.LOC_CODE = B.LOC_CODE ");
            query.AppendLine("         AND A.DATEE = B.DATEE ");
            query.AppendLine("         AND A.MDFY_YN = B.MDFY_YN ");
            query.AppendLine("         AND A.SECTION = B.SECTION ");
            query.AppendLine(")  ");
            query.AppendLine("WHEN MATCHED THEN ");
            query.AppendLine("     UPDATE SET A.FTR_IDN = B.FTR_IDN ");
            query.AppendLine("               ,A.PIP_DIP = B.PIP_DIP ");
            query.AppendLine("               ,A.PIP_LEN = B.PIP_LEN ");
            query.AppendLine("               ,A.FTR_CDE = B.FTR_CDE ");
            query.AppendLine("WHEN NOT MATCHED THEN ");
            query.AppendLine("     INSERT (A.LOC_CODE, A.DATEE, A.MDFY_YN, A.SECTION, A.FTR_IDN, A.PIP_DIP, A.PIP_LEN, A.FTR_CDE) ");
            query.AppendLine("     VALUES (B.LOC_CODE, B.DATEE, B.MDFY_YN, B.SECTION, B.FTR_IDN, B.PIP_DIP, B.PIP_LEN, B.FTR_CDE) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
	                ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                    ,new OracleParameter("SECTION", OracleDbType.Varchar2)
                    ,new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                    ,new OracleParameter("PIP_DIP", OracleDbType.Varchar2)
                    ,new OracleParameter("PIP_LEN", OracleDbType.Varchar2)
                    ,new OracleParameter("FTR_CDE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["MDFY_YN"];
            parameters[3].Value = parameter["SECTION"];
            parameters[4].Value = parameter["FTR_IDN"];
            parameters[5].Value = parameter["PIP_DIP"];
            parameters[6].Value = parameter["PIP_LEN"];
            parameters[7].Value = parameter["FTR_CDE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
