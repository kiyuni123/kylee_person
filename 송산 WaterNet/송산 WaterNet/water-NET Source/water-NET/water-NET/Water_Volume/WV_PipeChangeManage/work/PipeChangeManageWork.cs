﻿using System;
using WaterNet.WV_PipeChangeManage.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.util;
using System.Windows.Forms;

namespace WaterNet.WV_PipeChangeManage.work
{
    public class PipeChangeManageWork : BaseWork
    {
        private static PipeChangeManageWork work = null;
        private PipeChangeManageDao dao = null;

        public static PipeChangeManageWork GetInstance()
        {
            if (work == null)
            {
                work = new PipeChangeManageWork();
            }
            return work;
        }

        private PipeChangeManageWork()
        {
            dao = PipeChangeManageDao.GetInstance();
        }

        public object HasDegeche(Hashtable parameter)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = this.dao.HasDegeche(DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectDegeche(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.SelectDegeche(DataBaseManager, result, "RESULT", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectDegecheSection(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.SelectDegecheSection(DataBaseManager, result, "RESULT", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectDegecheSectionDetail(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.SelectDegecheSectionDetail(DataBaseManager, result, "RESULT", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }


        public DataSet UpdateDegecheSectionDetail(Hashtable parameter, DataTable degecheList)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.DeleteDegecheSectionDetail(DataBaseManager, parameter);

                foreach (DataRow row in degecheList.Rows)
                {
                    this.dao.InsertDegecheSectionDetail(DataBaseManager, Utils.ConverToHashtable(row));
                }

                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }


        public DataSet DeleteDegecheSectionDetail(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.DeleteDegecheSectionDetail(DataBaseManager, parameter);

                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet UpdateExcelImport(DataTable data)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (DataRow row in data.Rows)
                {
                    this.dao.UpdateExcelImport(DataBaseManager, Utils.ConverToHashtable(row));
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
