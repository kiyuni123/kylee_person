﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.form;
using ESRI.ArcGIS.Carto;
using WaterNet.WaterAOCore;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.control;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WV_PipeChangeManage.work;
using Infragistics.Win;
using System.Data;
using EMFrame.log;
using WaterNet.WV_Common.excel;

namespace WaterNet.WV_PipeChangeManage.form
{
    public partial class frmMain : Form, ExcelImport
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private BlockComboboxControl detailblock = null;
        private ExcelManager excelManager = new ExcelManager();

        //상수관로 레이어
        private ILayer pLayer = null;

        //급수관로 레이어
        private ILayer lLayer = null;

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// 데이터 확인용폼으로 사용될 경우
        /// </summary>
        public bool UsePlan
        {
            set
            {
                if (value == true)
                {
                    this.searchBox1.Visible = false;
                    this.panel1.Visible = false;
                    this.sectionBtn.Visible = false;
                    this.pipCheck.Visible = false;
                    this.searchBox1.InitializeParameter();
                    this.SelectDegeche(this.searchBox1.Parameters);
                }
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        private bool DetailEnabled
        {
            get
            {
                bool enabled = false;

                UltraGridRow row = null;

                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    row = this.ultraGrid1.Selected.Rows[0];
                }

                if (row == null)
                {
                    enabled = true;
                }

                if (row != null)
                {
                    if (row.Cells["MDFY_YN"].Value.ToString() == "Y")
                    {
                        enabled = true;
                    }
                    else if (row.Cells["MDFY_YN"].Value.ToString() == "N")
                    {
                        enabled = false;
                    }
                }
                return enabled;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["관개대체관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.insertBtn.Enabled = false;
                this.updateBtn.Enabled = false;
                this.deleteBtn.Enabled = false;

                this.sectionBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 폼 초기화 설정
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.Text = "검색조건";
            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE;
            this.searchBox1.IntervalText = "검색기간";
            this.searchBox1.StartDateObject.Value = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");

            this.detailblock = new BlockComboboxControl(this.lblock, this.mblock, this.sblock);
            this.detailblock.AddDefaultOption(LOCATION_TYPE.All, false);

            this.detailblock.StopEvent = true;

            //IFeatureSelection selection = (IFeatureSelection)this.getPLayerInstance();
            //selection.SelectionColor = ArcManager.GetColor(Color.Red);

            //selection = (IFeatureSelection)this.getLLayerInstance();
            //selection.SelectionColor = ArcManager.GetColor(Color.Red);
        }

        /// <summary>
        /// 그리드 초기화 설정
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);
            this.gridManager.Add(this.ultraGrid3);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.Default;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.Default;
            this.ultraGrid3.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.Default;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            //수정가능여부코드,
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MDFY_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MDFY_YN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2011");
            }
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MDFY_YN"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MDFY_YN"];
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.importBtn.Click += new EventHandler(importBtn_Click);

            this.Disposed += new EventHandler(frmMain_Disposed);

            this.mainMap.AxMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(AxMap_OnMouseDown);
            this.pipCheck.CheckedChanged += new EventHandler(PipCheck_CheckedChanged);

            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.sectionBtn.Click += new EventHandler(sectionBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);

            this.ultraGrid2.AfterRowActivate += new EventHandler(ultraGrid2_AfterRowActivate);
            this.ultraGrid2.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid2_AfterSelectChange);

            this.ultraGrid3.AfterRowActivate += new EventHandler(ultraGrid3_AfterRowActivate);
            this.ultraGrid3.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid3_AfterSelectChange);
        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            frmImport import = new frmImport();
            import.Items.Add(new ImportItem("LOC_CODE", "지역관리코드", typeof(string), true));
            import.Items.Add(new ImportItem("DATEE", "통수일자", typeof(string), true));
            import.Items.Add(new ImportItem("MDFY_YN", "수정가능여부", typeof(string), true));
            import.Items.Add(new ImportItem("SECTION", "구간번호", typeof(string), true));
            import.Items.Add(new ImportItem("FTR_IDN", "관로 관리번호", typeof(string), true));
            import.Items.Add(new ImportItem("PIP_DIP", "구경", typeof(string), false));
            import.Items.Add(new ImportItem("PIP_LEN", "관연장", typeof(string), false));
            import.Items.Add(new ImportItem("FTR_CDE", "관구분", typeof(string), false));
            import.UpdateExcel = this;
            import.ShowDialog();
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "관개대체정보");
                this.excelManager.Save("관개대체정보", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void frmMain_Disposed(object sender, EventArgs e)
        {
            this.mainMap.AxMap.OnMouseDown -= AxMap_OnMouseDown;

            //IFeatureSelection selection = (IFeatureSelection)this.getPLayerInstance();
            //selection.SelectionColor = ArcManager.GetColor(Color.Empty);

            //selection = (IFeatureSelection)this.getLLayerInstance();
            //selection.SelectionColor = ArcManager.GetColor(Color.Empty);
        }

        //맵을 클릭했을때 상수관로 선택관련.
        private void AxMap_OnMouseDown(object sender, ESRI.ArcGIS.Controls.IMapControlEvents2_OnMouseDownEvent e)
        {
            if (e.button == 1 && this.mainMap.ToolActionCommand.Checked && this.mainMap.IMapControl3.CurrentTool == null && this.pipCheck.Checked)
            {
                ILayer pLayer = this.getPLayerInstance();

                if (pLayer == null)
                {
                    return;
                }

                IFeatureSelection selection = (IFeatureSelection)pLayer;
                
                ESRI.ArcGIS.Geometry.IPoint pPoint = new ESRI.ArcGIS.Geometry.PointClass();
                pPoint.X = e.mapX;
                pPoint.Y = e.mapY;
                pPoint.Z = 0;

                IFeatureCursor cursor =
                    ArcManager.GetSpatialCursor(pLayer, pPoint, 5, esriSpatialRelEnum.esriSpatialRelIntersects, null);

                IFeature feature = cursor.NextFeature();

                while (feature != null)
                {
                    IEnumIDs enumIDs = selection.SelectionSet.IDs;
                    enumIDs.Reset();

                    int iDs = 0;
                    bool selected = false;

                    while ((iDs = enumIDs.Next()) != -1)
                    {
                        if (iDs == feature.OID)
                        {
                            selected = true;
                            break;
                        }
                    }

                    if (!selected)
                    {
                        selection.SelectionSet.Add(feature.OID);

                        //그리드내 관로정보 추가
                        if (this.ultraGrid2.DataSource !=null && this.ultraGrid2.Selected.Rows.Count > 0)
                        {
                            UltraGridRow parentRow = this.ultraGrid2.Selected.Rows[0];

                            if (this.ultraGrid3.DataSource == null)
                            {
                                DataTable tempTable = new DataTable();
                                tempTable.Columns.Add("LOC_CODE", typeof(string));
                                tempTable.Columns.Add("DATEE", typeof(DateTime));
                                tempTable.Columns.Add("MDFY_YN", typeof(string));
                                tempTable.Columns.Add("SECTION", typeof(string));
                                tempTable.Columns.Add("FTR_IDN", typeof(string));
                                tempTable.Columns.Add("PIP_DIP", typeof(string));
                                tempTable.Columns.Add("PIP_LEN", typeof(string));
                                tempTable.Columns.Add("FTR_CDE", typeof(string));

                                this.ultraGrid3.DataSource = tempTable;
                            }

                            DataRow newRow = ((DataTable)this.ultraGrid3.DataSource).NewRow();
                            newRow["LOC_CODE"] = parentRow.Cells["LOC_CODE"].Value;
                            newRow["DATEE"] = parentRow.Cells["DATEE"].Value;
                            newRow["MDFY_YN"] = parentRow.Cells["MDFY_YN"].Value;
                            newRow["SECTION"] = parentRow.Cells["SECTION"].Value;
                            newRow["FTR_IDN"] = feature.get_Value(feature.Fields.FindField("FTR_IDN"));
                            newRow["PIP_DIP"] = feature.get_Value(feature.Fields.FindField("PIP_DIP"));
                            newRow["PIP_LEN"] = feature.get_Value(feature.Fields.FindField("PIP_LEN"));
                            newRow["FTR_CDE"] = feature.get_Value(feature.Fields.FindField("FTR_CDE"));

                            ((DataTable)this.ultraGrid3.DataSource).Rows.Add(newRow);
                        }
                    }
                    else if (selected)
                    {
                        List<int> constructOIDDeleteList = new List<int>();        
                        constructOIDDeleteList.Add(feature.OID);
                        int[] oidDeleteList = constructOIDDeleteList.ToArray();
                        selection.SelectionSet.RemoveList(oidDeleteList.Length, ref oidDeleteList[0]);

                        //그리드내 관로정보 삭제
                        if (this.ultraGrid2.DataSource != null && this.ultraGrid2.Selected.Rows.Count > 0)
                        {
                            for (int i = 0; i < ((DataTable)this.ultraGrid3.DataSource).Rows.Count; i++)
                            {
                                if (feature.get_Value(feature.Fields.FindField("FTR_IDN")).ToString() == ((DataTable)this.ultraGrid3.DataSource).Rows[i]["FTR_IDN"].ToString())
                                {
                                    ((DataTable)this.ultraGrid3.DataSource).Rows.Remove(((DataTable)this.ultraGrid3.DataSource).Rows[i]);
                                }
                            }
                        }
                    }
                    feature = cursor.NextFeature();
                }
                this.mainMap.IMapControl3.ActiveView.Refresh();


                //그리드 2의 관로 길이를 재 설정한다.

                foreach (UltraGridRow gridRow in this.ultraGrid2.Rows)
                {
                    double len = 0;

                    foreach (UltraGridRow subRow in this.ultraGrid3.Rows)
                    {
                        if (gridRow.Cells["SECTION"].Value.ToString() == subRow.Cells["SECTION"].Value.ToString())
                        {
                            len += Utils.ToDouble(subRow.Cells["PIP_LEN"].Value);
                        }
                    }

                    gridRow.Cells["PIP_LEN"].Value = len;
                }
            }
        }

        //통수일자 구간내 상수관로를 추가하기전에 툴박스선택을 사용자정의로 변경함.
        private void PipCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (!pipCheck.Checked)
            {
                return;
            }

            if (this.ultraGrid2.Selected.Rows.Count == 0 && this.pipCheck.Checked)
            {
                MessageBox.Show("구간을 추가해야 합니다.");
                this.pipCheck.Checked = false;
                return;
            }

            this.mainMap.ToolActionCommand.Checked = true;
            this.mainMap.IMapControl3.CurrentTool = null;
        }

        //추가버튼클릭
        private void insertBtn_Click(object sender, EventArgs e)
        {
            this.InsertDegecheDetail();
            this.ChangeSection();
        }

        private void InsertDegecheDetail()
        {
            this.ultraGrid1.Selected.Rows.Clear();
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Activated = false;
            }

            if (this.ultraGrid1.Rows.Count > 0)
            {
                this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(this.ultraGrid1.Rows[0]);
            }

            if (this.ultraGrid2.DataSource != null)
            {
                ((DataTable)this.ultraGrid2.DataSource).Rows.Clear();
                ((DataTable)this.ultraGrid2.DataSource).AcceptChanges();
            }
            if (this.ultraGrid3.DataSource != null)
            {
                ((DataTable)this.ultraGrid3.DataSource).Rows.Clear();
                ((DataTable)this.ultraGrid3.DataSource).AcceptChanges();
            }

            this.lblock.SelectedIndex = 0;
            this.mblock.SelectedIndex = 0;
            this.sblock.SelectedIndex = 0;
            this.datee.Value = DateTime.Now;

            this.SetEnabledDetailForm(this.DetailEnabled);
        }

        //검색버튼 클릭 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectDegeche(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //관교체후통수 조회.
        private void SelectDegeche(Hashtable parameter)
        {
            //GIS에서 블럭 포커스 이동
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = PipeChangeManageWork.GetInstance().SelectDegeche(parameter).Tables[0];

            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                if (this.ultraGrid2.DataSource != null)
                {
                    ((DataTable)this.ultraGrid2.DataSource).Rows.Clear();
                    ((DataTable)this.ultraGrid2.DataSource).AcceptChanges();
                }
                if (this.ultraGrid3.DataSource != null)
                {
                    ((DataTable)this.ultraGrid3.DataSource).Rows.Clear();
                    ((DataTable)this.ultraGrid3.DataSource).AcceptChanges();
                }
                ILayer pLayer = this.getPLayerInstance();

                if (pLayer == null)
                {
                    return;
                }

                IFeatureSelection selection = (IFeatureSelection)pLayer;
                selection.Clear();

                this.mainMap.IMapControl3.ActiveView.Refresh();
            }
        }

        private void sectionBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                Hashtable parameter = new Hashtable();
                parameter["LOC_CODE"] = this.sblock.SelectedValue;
                parameter["DATEE"] = Convert.ToDateTime(this.datee.Value).ToString("yyyyMMdd");
                if (Convert.ToBoolean(PipeChangeManageWork.GetInstance().HasDegeche(parameter)))
                {
                    MessageBox.Show("블록내 관개대체 이력이 존재합니다.\n해당 데이터를 선택하고 구간추가를 진행하셔야 합니다.");
                    return;
                }
            }

            DataRow row = null;

            if (this.ultraGrid2.DataSource == null)
            {
                DataTable tempTable = new DataTable();
                tempTable.Columns.Add("LOC_CODE", typeof(string));
                tempTable.Columns.Add("DATEE", typeof(DateTime));
                tempTable.Columns.Add("MDFY_YN", typeof(string));
                tempTable.Columns.Add("SECTION", typeof(string));
                tempTable.Columns.Add("PIP_LEN", typeof(string));
                tempTable.Columns.Add("FTR_CDE", typeof(string));

                this.ultraGrid2.DataSource = tempTable;
            }

            row = ((DataTable)this.ultraGrid2.DataSource).NewRow();

            if (this.sblock.SelectedIndex == 0)
            {
                MessageBox.Show("소블록을 선택해야 합니다.");
                return;
            }

            row["LOC_CODE"] = this.sblock.SelectedValue;
            row["DATEE"] = this.datee.Value;
            row["MDFY_YN"] = "Y";

            int tmpSection = 0;

            foreach (DataRow tmpRow in ((DataTable)this.ultraGrid2.DataSource).Rows)
            {
                if (tmpSection < Convert.ToInt32(tmpRow["SECTION"]))
                {
                    tmpSection = Convert.ToInt32(tmpRow["SECTION"]);
                }
            }

            row["SECTION"] = (tmpSection + 1).ToString();

            ((DataTable)this.ultraGrid2.DataSource).Rows.Add(row);
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid2.Selected.Rows.Count == 0)
            {
                return;
            }

            Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid2.Selected.Rows[0]);
            PipeChangeManageWork.GetInstance().UpdateDegecheSectionDetail(parameter, (DataTable)this.ultraGrid3.DataSource);
            this.SelectDegeche(this.searchBox1.Parameters);
        }

        /// <summary>
        /// 활성화된 로우에 클릭 이벤트를 준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Selected = true;
            }
        }

        /// <summary>
        /// 그리드 로우 선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid1.Selected.Rows[0]);

            this.lblock.SelectedValue = parameter["LBLOCK"];
            this.mblock.SelectedValue = parameter["MBLOCK"];
            this.sblock.SelectedValue = parameter["SBLOCK"];
            this.datee.Value = this.ultraGrid1.Selected.Rows[0].Cells["DATEE"].Value;
            
            //수정가능이면.....

            this.SetEnabledDetailForm(this.DetailEnabled);

            if (parameter["MDFY_YN"].ToString() == "Y")
            {
                this.detailblock.StopEvent = false;
            }
            else if (parameter["MDFY_YN"].ToString() == "N")
            {
                this.detailblock.StopEvent = true;
            }

            this.lblock.Enabled = false;
            this.mblock.Enabled = false;
            this.sblock.Enabled = false;
            this.datee.Enabled = false;

            this.SelectDegecheSection(parameter);
        }

        //관교체후통수 상세 조회.
        private void SelectDegecheSection(Hashtable parameter)
        {
            this.ultraGrid2.DataSource = PipeChangeManageWork.GetInstance().SelectDegecheSection(parameter).Tables[0];
            this.ultraGrid3.DataSource = PipeChangeManageWork.GetInstance().SelectDegecheSectionDetail(parameter).Tables[0];
        }

        /// <summary>
        /// 활성화된 로우에 클릭 이벤트를 준다. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid2_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid2.ActiveRow != null)
            {
                this.ultraGrid2.ActiveRow.Selected = true;
            }
        }

        /// <summary>
        /// 그리드 로우 선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid2_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (this.ultraGrid2.Selected.Rows.Count == 0)
            {
                return;
            }

            //선택된 구간의 상로를 선택상태로 만들고,,, 관로의 중간으로 이동함.
            Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid2.Selected.Rows[0]);
            this.SelectDegecheSectionDetail(parameter);
        }

        //관교체후통수 상세 조회.
        private void SelectDegecheSectionDetail(Hashtable parameter)
        {
            foreach (UltraGridRow gridRow in this.ultraGrid3.Rows)
            {
                gridRow.Hidden = true;

                if (parameter["SECTION"].ToString() == gridRow.Cells["SECTION"].Value.ToString())
                {
                    gridRow.Hidden = false;
                }
            }

            foreach (UltraGridRow gridRow in this.ultraGrid3.Rows)
            {
                if (!gridRow.Hidden)
                {
                    gridRow.Activate();
                    break;
                }
            }
            this.ChangeSection();
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            DialogResult qe = MessageBox.Show("삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid1.Selected.Rows[0]);
                PipeChangeManageWork.GetInstance().DeleteDegecheSectionDetail(parameter);
                this.SelectDegeche(this.searchBox1.Parameters);
            }
        }

        /// <summary>
        /// 활성화된 로우에 클릭 이벤트를 준다. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid3_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid3.ActiveRow != null)
            {
                this.ultraGrid3.ActiveRow.Selected = true;
            }
        }

        /// <summary>
        /// 그리드 로우 선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid3_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (this.ultraGrid3.Selected.Rows.Count == 0)
            {
                return;
            }

            UltraGridRow row = this.ultraGrid3.Selected.Rows[0];


            ILayer pLayer = this.getPLayerInstance();

            if (pLayer == null)
            {
                return;
            }

            string strWhere = "FTR_IDN = '"+row.Cells["FTR_IDN"].Value.ToString() + "'";

            IFeature feature = ArcManager.GetFeature(pLayer, strWhere);

            if (feature != null)
            {
                this.mainMap.IMapControl3.MapScale = 2000;
               ArcManager.MoveCenterAt(this.mainMap.IMapControl3, feature);
            }

            //선택된 구간의 상로를 선택상태로 만들고,,, 관로의 중간으로 이동함.
            this.mainMap.IMapControl3.ActiveView.Refresh();
        }

        private void ChangeSection()
        {
            ILayer pLayer = this.getPLayerInstance();
            ILayer lLayer = this.getLLayerInstance();

            if (pLayer == null || lLayer == null)
            {
                return;
            }

            IFeatureSelection pselection = (IFeatureSelection)pLayer;
            pselection.Clear();

            IFeatureSelection lselection = (IFeatureSelection)lLayer;
            lselection.Clear();

            foreach (UltraGridRow row in this.ultraGrid3.Rows)
            {
                if (!row.Hidden)
                {
                    string strWhere = "FTR_IDN = '" + row.Cells["FTR_IDN"].Value.ToString() + "'";
                    IFeature feature = ArcManager.GetFeature(pLayer, strWhere);

                    if (feature != null)
                    {
                        pselection.Add(feature);
                    }
                }
            }

            this.mainMap.IMapControl3.ActiveView.Refresh();
        }

        /// <summary>
        /// 상수관로 레이어를 반환
        /// </summary>
        /// <returns></returns>
        private ILayer getPLayerInstance()
        {
            if (this.pLayer == null)
            {
                this.pLayer = ArcManager.GetMapLayer(this.mainMap.IMapControl3, "상수관로");
            }
            return this.pLayer;
        }

        /// <summary>
        /// 급수관로 레이어를 반환
        /// </summary>
        /// <returns></returns>
        private ILayer getLLayerInstance()
        {
            if (this.lLayer == null)
            {
                this.lLayer = ArcManager.GetMapLayer(this.mainMap.IMapControl3, "급수관로");
            }
            return this.lLayer;
        }

        ///<summary>
        ///상세폼의 객체 활성/ 비활성 제어
        ///</summary>
        ///<param name="isEnabled"></param>
        private void SetEnabledDetailForm(bool isEnabled)
        {
            this.lblock.Enabled = isEnabled;
            this.mblock.Enabled = isEnabled;
            this.sblock.Enabled = isEnabled;
            this.datee.Enabled = isEnabled;

            this.deleteBtn.Enabled = isEnabled;
            this.updateBtn.Enabled = isEnabled;

            this.sectionBtn.Enabled = isEnabled;
            this.pipCheck.Enabled = isEnabled;

            //=========================================================
            //
            //                    동진 수정_2012.6.12
            //			권한박탈(조회만 가능) 조회권한만 가진 사용자는 접근못함.
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["단수작업관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("0") ? true : false))
            {
                this.deleteBtn.Enabled = isEnabled;
                this.updateBtn.Enabled = isEnabled;
            }

            //====================================================================

        }

        #region ExcelImport 멤버

        public void update(DataTable data)
        {
            PipeChangeManageWork.GetInstance().UpdateExcelImport(data);
        }

        #endregion
    }
}
