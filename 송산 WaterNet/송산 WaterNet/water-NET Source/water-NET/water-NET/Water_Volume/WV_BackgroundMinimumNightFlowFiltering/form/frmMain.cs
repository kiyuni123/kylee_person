﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.enum1;
using WaterNet.WaterNetCore;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WV_BackgroundMinimumNightFlowFiltering.work;
using Infragistics.Win;
using ChartFX.WinForms;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

namespace WaterNet.WV_BackgroundMinimumNightFlowFiltering.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private TableLayout tableLayout = null;
        private ChartManager chartManager = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================야간최소유량 산정 방식별 조회

            object o = EMFrame.statics.AppStatic.USER_MENU["야간최소유량산정방식별조회ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE;

            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-mm-dd");
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);
            this.gridManager.SetCellClick(this.ultraGrid1);

            //그리드 수정가능하게,
            FormManager.SetGridStyle_ColumeAllowEdit(this.ultraGrid1, 10);
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["TIMESTAMP"].SortIndicator = SortIndicator.Descending;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);

            //this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["DATEE"].SortIndicator = SortIndicator.Descending;
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.chart1, "야간최소유량", 0, 0, 9, 20);
                this.excelManager.AddWorksheet(this.ultraGrid1, "야간최소유량", 10, 0);
                this.excelManager.Save("야간최소유량", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 야간최소유량 필터링 조회 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectMNF(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 야간최소유량 필터링 조회
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectMNF(Hashtable parameter)
        {
            if (parameter["FTR_CODE"].ToString() == "BZ001")
            {
                MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
                return;
            }

            //GIS에서 블럭 포커스 이동
            this.mainMap.MoveToBlock(parameter);
            DataSet result = BackgroundMinimumNightFlowFilteringWork.GetInstance().SelectMNF(parameter);
            this.ultraGrid1.DataSource = result.Tables[0];
            this.InitializeChartSetting();
        }

        /// <summary>
        /// 검색 데이터 변경시 차트 세팅/ 데이터 초기화 
        /// </summary>
        private void InitializeChartSetting()
        {
            Chart chart = this.chartManager.Items[0];
            this.chart1.Data.Clear();

            DataTable mnfList = (DataTable)this.ultraGrid1.DataSource;
            chart.Data.Series = 3;
            chart.Data.Points = mnfList.Rows.Count;

            foreach (DataRow row in mnfList.Rows)
            {
                int pointIndex = mnfList.Rows.IndexOf(row);
                chart.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyy-MM-dd");

                chart.Data[0, pointIndex] = Utils.ToDouble(row["MIN"]);
                chart.Data[1, pointIndex] = Utils.ToDouble(row["MVAVG_MIN"]);
                chart.Data[2, pointIndex] = Utils.ToDouble(row["FILTERING"]);
            }

            chart.AxisX.LabelsFormat.Format = AxisFormat.Date;
            chart.AxisX.LabelsFormat.CustomFormat = "yyyy-MM-dd";

            chart.AxesY[0].LabelsFormat.Format = AxisFormat.Number;
            chart.AxesY[0].LabelsFormat.CustomFormat = "####,####.00";
            chart.Series[0].Gallery = Gallery.Curve;

            chart.Series[0].Line.Width = 1;
            //chart.Series[0].Color = Color.Yellow;

            chart.Series[1].Gallery = Gallery.Curve;
            chart.Series[1].Line.Width = 1;
            chart.Series[1].Color = Color.Orange;

            chart.Series[2].Gallery = Gallery.Curve;
            chart.Series[2].Line.Width = 2;
            chart.Series[2].Color = Color.Red;

            chart.Series[0].Text = "순시 야간최소유량(㎥/h)";
            chart.Series[1].Text = "이동평균 야간최소유량(㎥/h)";
            chart.Series[2].Text = "보정 야간최소유량(㎥/h)";
            chart.AxisY.Title.Text = "유량(㎥/h)";
            chart.Refresh();

            chartManager.AllShowSeries(chart);
        }

        /// <summary>
        /// 야간최소유량 필터링 수정 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DataTable mnfList = (DataTable)this.ultraGrid1.DataSource;
                BackgroundMinimumNightFlowFilteringWork.GetInstance().UpdateMNF(mnfList);
                this.SelectMNF(this.searchBox1.Parameters);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }
    }
}
