﻿using System;
using WaterNet.WV_BackgroundMinimumNightFlowFiltering.dao;
using WaterNet.WV_Common.work;
using System.Collections;
using System.Data;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_BackgroundMinimumNightFlowFiltering.work
{
    public class BackgroundMinimumNightFlowFilteringWork : BaseWork
    {
        private static BackgroundMinimumNightFlowFilteringWork work = null;
        private BackgroundMinimumNightFlowFilteringDao dao = null;

        public static BackgroundMinimumNightFlowFilteringWork GetInstance()
        {
            if (work == null)
            {
                work = new BackgroundMinimumNightFlowFilteringWork();
            }
            return work;
        }

        private BackgroundMinimumNightFlowFilteringWork()
        {
            dao = BackgroundMinimumNightFlowFilteringDao.GetInstance();
        }

        public DataSet SelectMNF(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectMNF(base.DataBaseManager, result, "RESULT", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet UpdateMNF(DataTable table)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (DataRow row in table.Rows)
                {
                    dao.UpdateMNF(base.DataBaseManager, Utils.ConverToHashtable(row));
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
