﻿using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;
using WaterNet.WaterNetCore;

namespace WaterNet.WV_BackgroundMinimumNightFlowFiltering.dao
{
    public class BackgroundMinimumNightFlowFilteringDao
    {
        private static BackgroundMinimumNightFlowFilteringDao dao = null;

        private BackgroundMinimumNightFlowFilteringDao() { }

        public static BackgroundMinimumNightFlowFilteringDao GetInstance()
        {
            if (dao == null)
            {
                dao = new BackgroundMinimumNightFlowFilteringDao();
            }
            return dao;
        }

        //야간최소유량 필터링 조회
        public void SelectMNF(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                                                          ");
            //query.AppendLine("(                                                                                                                                                                    ");
            //query.AppendLine("select c1.loc_code                                                                                                                                                   ");
            //query.AppendLine("      ,c1.sgccd                                                                                                                                                      ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                      ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                       ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                       ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                              ");
            //query.AppendLine("      ,c1.ftr_code                                                                                                                                                   ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                          ");
            //query.AppendLine("      ,c1.ord                                                                                                                                                        ");
            //query.AppendLine("  from                                                                                                                                                               ");
            //query.AppendLine("      (                                                                                                                                                              ");
            //query.AppendLine("       select sgccd                                                                                                                                                  ");
            //query.AppendLine("             ,loc_code                                                                                                                                               ");
            //query.AppendLine("             ,ploc_code                                                                                                                                              ");
            //query.AppendLine("             ,loc_name                                                                                                                                               ");
            //query.AppendLine("             ,ftr_idn                                                                                                                                                ");
            //query.AppendLine("             ,ftr_code                                                                                                                                               ");
            //query.AppendLine("             ,kt_gbn                                                                                                                                                 ");
            //query.AppendLine("             ,rownum ord                                                                                                                                             ");
            //query.AppendLine("         from cm_location                                                                                                                                            ");
            //query.AppendLine("        where 1 = 1                                                                                                                                                  ");
            //query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                   ");
            //query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                                ");
            //query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                        ");
            //query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                    ");
            //query.AppendLine("      ) c1                                                                                                                                                           ");
            //query.AppendLine("       ,cm_location c2                                                                                                                                               ");
            //query.AppendLine("       ,cm_location c3                                                                                                                                               ");
            //query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                 ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                 ");
            //query.AppendLine(" order by c1.ord                                                                                                                                                     ");
            //query.AppendLine(")                                                                                                                                                                    ");
            //query.AppendLine("select loc.loc_code                                                                                                                                                       ");
            //query.AppendLine("      ,loc.sgccd                                                                                                                                                          ");
            //query.AppendLine("      ,loc.lblock                                                                                                                                                         ");
            //query.AppendLine("      ,loc.mblock                                                                                                                                                         ");
            //query.AppendLine("      ,loc.sblock                                                                                                                                                         ");
            //query.AppendLine("      ,iwm.tagname                                                                                                                                                        ");
            //query.AppendLine("      ,to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd') timestamp                                                                                                    ");
            //query.AppendLine("      ,to_char(to_date(iwm.mvavg_mintime,'yyyymmddhh24miss'),'hh24') mvavg_mintime                                                                                        ");
            //query.AppendLine("      ,round(to_number(iwm.min),1) min                                                                                                                                    ");
            //query.AppendLine("      ,round(to_number(iwm.mvavg_min),2) mvavg_min                                                                                                                        ");
            //query.AppendLine("      ,round(to_number(iwm.filtering),2) filtering                                                                                                                        ");
            //query.AppendLine("      ,nvl(data.leakage, 'N') leakage                                                                                                                                     ");
            //query.AppendLine("      ,nvl(data.mnf,'T') mnf                                                                                                                                              ");
            //query.AppendLine("  from loc                                                                                                                                                                ");
            //query.AppendLine("      ,if_ihtags iih                                                                                                                                                      ");
            //query.AppendLine("      ,if_wv_mnf iwm                                                                                                                                                      ");
            //query.AppendLine("      ,(                                                                                                                                                                  ");
            //query.AppendLine("        select tagname, tag_gbn, dummy_relation from if_tag_gbn                                                                                                           ");
            //query.AppendLine("      ) itg                                                                                                                                                               ");
            //query.AppendLine("      ,(                                                                                                                                                                  ");
            //query.AppendLine("        select to_date(to_char(datee,'yyyymmdd'),'yyyymmdd') datee                                                                                                        ");
            //query.AppendLine("              ,max(decode(monitor_type, '1090', monitor_result)) leakage                                                                                                  ");
            //query.AppendLine("              ,max(decode(monitor_type, '9999', monitor_result)) mnf                                                                                                      ");
            //query.AppendLine("          from wv_monitering                                                                                                                                              ");
            //query.AppendLine("         where loc_code = :LOC_CODE                                                                                                                                       ");
            //query.AppendLine("         group by loc_code, to_date(to_char(datee,'yyyymmdd'),'yyyymmdd')                                                                                                 ");
            //query.AppendLine("       ) data                                                                                                                                                             ");
            //query.AppendLine("  where 1 = 1                                                                                                                                                             ");
            //query.AppendLine("    and iih.loc_code = loc.tag_loc_code                                                                                                                                   ");
            //query.AppendLine("    and iih.tagname = itg.tagname                                                                                   ");
            //query.AppendLine("    and itg.tag_gbn = 'MNF'                                                                                                                                               ");
            //query.AppendLine("    and iwm.tagname = itg.tagname                                                                                                                                         ");
            //query.AppendLine("    and iwm.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                                 ");
            //query.AppendLine("    and data.datee(+) = to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd')                                                                                             ");
            //query.AppendLine(" order by timestamp                                                                                                                                                       ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //        ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //        ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //        ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["LOC_CODE"];
            //parameters[1].Value = parameter["LOC_CODE"];
            //parameters[2].Value = parameter["STARTDATE"];
            //parameters[3].Value = parameter["ENDDATE"];

            query.AppendLine("with loc as                                                                                                                  ");
            query.AppendLine("(																															   ");
            query.AppendLine("select c1.loc_code																										   ");
            query.AppendLine("	  ,c1.sgccd																												   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										   ");
            query.AppendLine("	  ,c1.ftr_code																											   ");
            query.AppendLine("	  ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code	   ");
            query.AppendLine("	  ,c1.ord																												   ");
            query.AppendLine("      ,tmp.timestamp																										   ");
            query.AppendLine("  from																													   ");
            query.AppendLine("	  (																														   ");
            query.AppendLine("	   select sgccd																											   ");
            query.AppendLine("			 ,loc_code																										   ");
            query.AppendLine("			 ,ploc_code																										   ");
            query.AppendLine("			 ,loc_name																										   ");
            query.AppendLine("			 ,ftr_idn																										   ");
            query.AppendLine("			 ,ftr_code																										   ");
            query.AppendLine("			 ,kt_gbn																										   ");
            query.AppendLine("			 ,rownum ord																									   ");
            query.AppendLine("		 from cm_location																									   ");
            query.AppendLine("		where 1 = 1																											   ");
            query.AppendLine("		  and loc_code = :LOC_CODE																							   ");
            query.AppendLine("		start with ftr_code = 'BZ001'																						   ");
            query.AppendLine("		connect by prior loc_code = ploc_code																				   ");
            query.AppendLine("		order SIBLINGS by ftr_idn																							   ");
            query.AppendLine("	  ) c1																													   ");
            query.AppendLine("	   ,cm_location c2																										   ");
            query.AppendLine("	   ,cm_location c3																										   ");
            query.AppendLine("      ,(																													   ");
            query.AppendLine("        select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 timestamp from dual 											   ");
            query.AppendLine("        connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2 							   ");
            query.AppendLine("      ) tmp																												   ");
            query.AppendLine(" where 1 = 1																												   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																						   ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																						   ");
            query.AppendLine(" order by c1.ord																											   ");
            query.AppendLine(")																															   ");
            query.AppendLine("select loc.loc_code																										   ");
            query.AppendLine("	  ,loc.sgccd																											   ");
            query.AppendLine("	  ,loc.lblock																											   ");
            query.AppendLine("	  ,loc.mblock																											   ");
            query.AppendLine("	  ,loc.sblock																											   ");
            query.AppendLine("	  ,loc.tagname																											   ");
            query.AppendLine("      ,loc.timestamp																										   ");
            query.AppendLine("	  ,to_char(to_date(iwm.mvavg_mintime,'yyyymmddhh24miss'),'hh24') mvavg_mintime											   ");
            query.AppendLine("	  ,round(to_number(iwm.min),1) min																						   ");
            query.AppendLine("	  ,round(to_number(iwm.mvavg_min),2) mvavg_min																			   ");
            query.AppendLine("	  ,round(to_number(iwm.filtering),2) filtering																			   ");
            query.AppendLine("  from																													   ");
            query.AppendLine("      (																													   ");
            query.AppendLine("       select loc.loc_code																								   ");
            query.AppendLine("	         ,loc.sgccd																										   ");
            query.AppendLine("	         ,loc.lblock																									   ");
            query.AppendLine("	         ,loc.mblock																									   ");
            query.AppendLine("       	     ,loc.sblock																								   ");
            query.AppendLine("	         ,itg.tagname																									   ");
            query.AppendLine("             ,loc.timestamp																								   ");
            query.AppendLine("         from loc																											   ");
            query.AppendLine("             ,if_ihtags iih																								   ");
            query.AppendLine("	         ,(																												   ");
            query.AppendLine("		       select tagname, tag_gbn, dummy_relation from if_tag_gbn														   ");
            query.AppendLine("	         ) itg																											   ");
            query.AppendLine("        where 1 = 1																										   ");
            query.AppendLine("          and iih.loc_code = loc.tag_loc_code																				   ");
            query.AppendLine("          and iih.tagname = itg.tagname																					   ");
            query.AppendLine("          and itg.tag_gbn = 'MNF'																							   ");
            query.AppendLine("       ) loc																												   ");
            query.AppendLine("       ,if_wv_mnf iwm																										   ");
            query.AppendLine("  where iwm.tagname(+) = loc.tagname																						   ");
            query.AppendLine("    and iwm.timestamp(+) = loc.timestamp																					   ");
            query.AppendLine("  order by timestamp																										   ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //야간최소유량 필터링 수정
        public void UpdateMNF(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("update if_wv_mnf                                                                                                          ");
            //query.AppendLine("   set filtering = :FILTERING                                                                                             ");
            //query.AppendLine(" where 1 = 1                                                                                                              ");
            //query.AppendLine("   and tagname = :TAGNAME                                                                                                 ");
            //query.AppendLine("   and timestamp between to_date(:TIMESTAMP||'0000', 'yyyymmddhh24mi') and to_date(:TIMESTAMP||'2359', 'yyyymmddhh24mi')  ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("FILTERING", OracleDbType.Varchar2)
            //        ,new OracleParameter("TAGNAME", OracleDbType.Varchar2)
            //        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
            //        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["FILTERING"];
            //parameters[1].Value = parameter["TAGNAME"];
            //parameters[2].Value = parameter["TIMESTAMP"];
            //parameters[3].Value = parameter["TIMESTAMP"];

            query.AppendLine("merge into if_wv_mnf a                                                                                     ");
            query.AppendLine("using (select :TAGNAME tagname, to_date(:TIMESTAMP,'yyyymmdd') timestamp, :FILTERING filtering from dual) b");
            query.AppendLine("   on (a.tagname = b.tagname and a.timestamp = b.timestamp)												 ");
            query.AppendLine(" when matched then 																						 ");
            query.AppendLine("      update set a.filtering = b.filtering																 ");
            query.AppendLine(" when not matched then																					 ");
            query.AppendLine("      insert (a.tagname, a.timestamp, a.filtering)														 ");
            query.AppendLine("	  values (b.tagname, b.timestamp, b.filtering)															 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                    ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    ,new OracleParameter("FILTERING", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["FILTERING"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
