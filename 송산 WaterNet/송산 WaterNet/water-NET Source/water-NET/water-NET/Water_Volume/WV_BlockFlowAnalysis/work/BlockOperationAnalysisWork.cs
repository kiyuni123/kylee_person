﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using WaterNet.WV_Common.work;
using WaterNet.WV_BlockFlowAnalysis.dao;
using WaterNet.WV_LeakageManage.work;
using WaterNet.WV_LeakageManage.vo;

namespace WaterNet.WV_BlockFlowAnalysis.work
{
    public class BlockOperationAnalysisWork : BaseWork
    {
        private static BlockOperationAnalysisWork work = null;
        private BlockOperationAnalysisDao dao = null;

        public static BlockOperationAnalysisWork GetInstance()
        {
            if (work == null)
            {
                work = new BlockOperationAnalysisWork();
            }
            return work;
        }

        private BlockOperationAnalysisWork()
        {
            dao = BlockOperationAnalysisDao.GetInstance();
        }

        //블럭별운영분석을 조회한다.
        public DataSet SelectBlockOperationAnalysis(Hashtable parameter)
        {
            //비교조건의 존재 체크
            if (!parameter.ContainsKey("INTERVAL") || parameter["INTERVAL"] == null)
            {
                MessageBox.Show("비교조건이 선택되지 않았습니다.\n다시 검색해주세요.");
            }

            DataSet result = new DataSet();

            string interval = parameter["INTERVAL"].ToString();

            try
            {
                ConnectionOpen();
                BeginTransaction();

                Console.WriteLine(interval.Substring(0, 4));

                if (interval.Substring(0, 4) != "YEAR")
                {
                    IList<LeakageCalculationVO> this_year = LeakageManageWork.GetInstance().SelectLeakageManage(parameter);
                    result.Tables.Add(this.ConvertResult(this_year, interval));
                }
                else if (interval.Substring(0, 4) == "YEAR")
                {
                    this.ClearSearchInterval(parameter);
                    parameter["STARTDATE"] = parameter["STARTDATE1"];
                    parameter["ENDDATE"] = parameter["ENDDATE1"];
                    IList<LeakageCalculationVO> this_year = LeakageManageWork.GetInstance().SelectLeakageManage(parameter);

                    this.ClearSearchInterval(parameter);
                    parameter["STARTDATE"] = parameter["STARTDATE2"];
                    parameter["ENDDATE"] = parameter["ENDDATE2"];
                    IList<LeakageCalculationVO> last_year = LeakageManageWork.GetInstance().SelectLeakageManage(parameter);

                    result.Tables.Add(this.ConvertResult(this_year, interval));
                    result.Tables.Add(this.ConvertResult(last_year, interval));
                }
                /*
                //비교조건에 따라서 분류한다.
                switch (interval)
                {
                    //비교조건이 어제인경우
                    case "YESTERDAY":
                        result.Tables.Add(this.ConvertResult(leakageWork.SelectLeakageManage(parameter), interval));
                        break;

                    //비교조건이 7일인경우
                    case "WEEK":
                        result.Tables.Add(this.ConvertResult(leakageWork.SelectLeakageManage(parameter), interval));
                        break;

                    //비교조건이 한달인경우
                    case "MONTH":
                        result.Tables.Add(this.ConvertResult(leakageWork.SelectLeakageManage(parameter), interval));
                        break;

                    //비교조건이 주평균인 경우
                    case "WEEK_AVERAGE":
                        result.Tables.Add(this.ConvertResult(leakageWork.SelectLeakageManage(parameter), interval));
                        break;

                    //비교조건이 월평균인경우
                    case "MONTH_AVERAGE":
                        result.Tables.Add(this.ConvertResult(leakageWork.SelectLeakageManage(parameter), interval));
                        break;

                    //비교조건이 전년인 경우
                    case "YEAR_YESTERDAY":
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE1"];
                        parameter["ENDDATE"] = parameter["ENDDATE1"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "THIS_YEAR", parameter);
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE2"];
                        parameter["ENDDATE"] = parameter["ENDDATE2"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "LAST_YEAR", parameter);
                        break;

                    //비교조건이 전년 7일인경우
                    case "YEAR_WEEK":
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE1"];
                        parameter["ENDDATE"] = parameter["ENDDATE1"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "THIS_YEAR", parameter);
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE2"];
                        parameter["ENDDATE"] = parameter["ENDDATE2"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "LAST_YEAR", parameter);
                        break;

                    //비교조건이 전년 전월인 경우
                    case "YEAR_MONTH":
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE1"];
                        parameter["ENDDATE"] = parameter["ENDDATE1"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "THIS_YEAR", parameter);
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE2"];
                        parameter["ENDDATE"] = parameter["ENDDATE2"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "LAST_YEAR", parameter);
                        break;

                    //비교조건이 전년 주평균인 경우
                    case "YEAR_WEEK_AVERAGE":
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE1"];
                        parameter["ENDDATE"] = parameter["ENDDATE1"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "THIS_YEAR", parameter);
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE2"];
                        parameter["ENDDATE"] = parameter["ENDDATE2"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "LAST_YEAR", parameter);
                        break;

                    //비교조건이 전년 월평균인 경우
                    case "YEAR_MONTH_AVERAGE":
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE1"];
                        parameter["ENDDATE"] = parameter["ENDDATE1"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "THIS_YEAR", parameter);
                        this.ClearSearchInterval(parameter);
                        parameter["STARTDATE"] = parameter["STARTDATE2"];
                        parameter["ENDDATE"] = parameter["ENDDATE2"];
                        this.dao.SelectBlockOperationAnalysisByDayInterval(DataBaseManager, result, "LAST_YEAR", parameter);
                        break;
                }
                */

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        private void ClearSearchInterval(Hashtable parameter)
        {
            if (parameter.ContainsKey("STARTDATE"))
            {
                parameter.Remove("STARTDATE");
            }
            if (parameter.ContainsKey("ENDDATE"))
            {
                parameter.Remove("ENDDATE");
            }
        }

        private DataTable ConvertResult(IList<LeakageCalculationVO> resultList, string interval)
        {
            DataTable result = new DataTable();
            result.Columns.Add("LOC_CODE", typeof(string));
            result.Columns.Add("SGCCD", typeof(string));
            result.Columns.Add("LBLOCK", typeof(string));
            result.Columns.Add("MBLOCK", typeof(string));
            result.Columns.Add("SBLOCK", typeof(string));
            result.Columns.Add("DATEE", typeof(DateTime));
            result.Columns.Add("DAY_TOTAL", typeof(double));
            result.Columns.Add("DAY_MNF", typeof(double));
            result.Columns.Add("DAY_LEAKAGE", typeof(double));
            DataRow row = null;

            //주 평균
            if (interval == "WEEK_AVERAGE" || interval == "YEAR_WEEK_AVERAGE")
            {
                //첫번째 로우에서 한주씩

                DateTime minusDate = new DateTime();
                DateTime firstDate = new DateTime();

                int row_count = 0;
                int week_count = 1;
                double day_total = 0;
                double day_mnf = 0;
                double day_leakage = 0;

                foreach (LeakageCalculationVO leakageCalculation in resultList)
                {
                    if (row_count == 0 && week_count == 1)
                    {
                        firstDate = leakageCalculation.DATEE;
                    }

                    if (row_count == 0)
                    {
                        minusDate = firstDate.AddDays(7 * week_count);
                    }

                    day_total += leakageCalculation.DAY_SUPPLIED;
                    
                    //day_mnf += leakageCalculation.M_AVERAGE;
                    day_mnf += leakageCalculation.FILTERING;
                    
                    day_leakage += leakageCalculation.LEAKS_DAVG;

                    row_count++;

                    if (minusDate.ToString("yyyyMMdd") == leakageCalculation.DATEE.ToString("yyyyMMdd"))
                    {
                        row = result.NewRow();
                        row["LOC_CODE"] = leakageCalculation.LOC_CODE;
                        row["SGCCD"] = null;
                        row["LBLOCK"] = leakageCalculation.LBLOCK;
                        row["MBLOCK"] = leakageCalculation.MBLOCK;
                        row["SBLOCK"] = leakageCalculation.SBLOCK;
                        row["DATEE"] = minusDate;
                        row["DAY_TOTAL"] = day_total / row_count;
                        row["DAY_MNF"] = day_mnf / row_count;
                        row["DAY_LEAKAGE"] = day_leakage / row_count;
                        result.Rows.Add(row);

                        day_total = 0;
                        day_mnf = 0;
                        day_leakage = 0;
                        row_count = 0;
                        week_count++;
                    }
                }
            }

            //월 평균
            else if (interval == "MONTH_AVERAGE" || interval == "YEAR_MONTH_AVERAGE")
            {
                //첫번째 로우에서 한달씩

                DateTime minusDate = new DateTime();
                DateTime firstDate = new DateTime();

                int row_count = 0;
                int month_count = 1;
                double day_total = 0;
                double day_mnf = 0;
                double day_leakage = 0;

                foreach (LeakageCalculationVO leakageCalculation in resultList)
                {
                    if (row_count == 0 && month_count == 1)
                    {
                        firstDate = leakageCalculation.DATEE;
                    }

                    if (row_count == 0)
                    {
                        minusDate = firstDate.AddMonths(month_count);
                    }

                    day_total += leakageCalculation.DAY_SUPPLIED;

                    //day_mnf += leakageCalculation.M_AVERAGE;
                    day_mnf += leakageCalculation.FILTERING;

                    day_leakage += leakageCalculation.LEAKS_DAVG;

                    row_count++;

                    if (minusDate.ToString("yyyyMMdd") == leakageCalculation.DATEE.ToString("yyyyMMdd"))
                    {
                        row = result.NewRow();
                        row["LOC_CODE"] = leakageCalculation.LOC_CODE;
                        row["SGCCD"] = null;
                        row["LBLOCK"] = leakageCalculation.LBLOCK;
                        row["MBLOCK"] = leakageCalculation.MBLOCK;
                        row["SBLOCK"] = leakageCalculation.SBLOCK;
                        row["DATEE"] = minusDate;
                        row["DAY_TOTAL"] = day_total / row_count;
                        row["DAY_MNF"] = day_mnf / row_count;
                        row["DAY_LEAKAGE"] = day_leakage / row_count;
                        result.Rows.Add(row);

                        day_total = 0;
                        day_mnf = 0;
                        day_leakage = 0;
                        row_count = 0;
                        month_count++;
                    }
                }
            }

            //디폴트 (일일기준)
            else
            {
                foreach (LeakageCalculationVO leakageCalculation in resultList)
                {
                    row = result.NewRow();
                    row["LOC_CODE"] = leakageCalculation.LOC_CODE;
                    row["SGCCD"] = null;
                    row["LBLOCK"] = leakageCalculation.LBLOCK;
                    row["MBLOCK"] = leakageCalculation.MBLOCK;
                    row["SBLOCK"] = leakageCalculation.SBLOCK;
                    row["DATEE"] = leakageCalculation.DATEE;
                    row["DAY_TOTAL"] = leakageCalculation.DAY_SUPPLIED;
                    
                    //row["DAY_MNF"] = leakageCalculation.M_AVERAGE;
                    row["DAY_MNF"] = leakageCalculation.FILTERING;
                    
                    row["DAY_LEAKAGE"] = leakageCalculation.LEAKS_DAVG;
                    result.Rows.Add(row);
                }
            }
            return result;
        }
    }
}
