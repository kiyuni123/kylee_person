﻿using WaterNet.WV_Common.form;
namespace WaterNet.WV_BlockFlowAnalysis.form
{
    partial class BlockOperationAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ChartFX.WinForms.Adornments.SolidBackground solidBackground1 = new ChartFX.WinForms.Adornments.SolidBackground();
            ChartFX.WinForms.SeriesAttributes seriesAttributes1 = new ChartFX.WinForms.SeriesAttributes();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SGCCD");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DATEE");
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAY_TOTAL");
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAY_MNF");
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAY_LEAKAGE");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SGCCD");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DATEE");
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAY_TOTAL");
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAY_MNF");
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAY_LEAKAGE");
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlockOperationAnalysis));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.YEAR_MONTH_AVERAGE_INTERVAL = new System.Windows.Forms.ComboBox();
            this.MONTH_AVERAGE_INTERVAL = new System.Windows.Forms.ComboBox();
            this.YEAR_WEEK_AVERAGE_INTERVAL = new System.Windows.Forms.ComboBox();
            this.WEEK_AVERAGE_INTERVAL = new System.Windows.Forms.ComboBox();
            this.YEAR_MONTH_AVERAGE = new System.Windows.Forms.CheckBox();
            this.YEAR_WEEK_AVERAGE = new System.Windows.Forms.CheckBox();
            this.MONTH_AVERAGE = new System.Windows.Forms.CheckBox();
            this.WEEK_AVERAGE = new System.Windows.Forms.CheckBox();
            this.YEAR_MONTH = new System.Windows.Forms.CheckBox();
            this.YEAR_WEEK = new System.Windows.Forms.CheckBox();
            this.YEAR_YESTERDAY = new System.Windows.Forms.CheckBox();
            this.MONTH = new System.Windows.Forms.CheckBox();
            this.WEEK = new System.Windows.Forms.CheckBox();
            this.YESTERDAY = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chartVisibleBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.tableVisibleBtn = new System.Windows.Forms.Button();
            this.DAY_TOTAL = new System.Windows.Forms.CheckBox();
            this.DAY_LEAKAGE = new System.Windows.Forms.CheckBox();
            this.DAY_MNF = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chartPanel1 = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.tablePanel1 = new System.Windows.Forms.Panel();
            this.ultraGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.chartPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(1062, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 642);
            this.pictureBox4.TabIndex = 25;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 642);
            this.pictureBox3.TabIndex = 24;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 652);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1072, 10);
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1072, 10);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(10, 121);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(1052, 10);
            this.pictureBox5.TabIndex = 32;
            this.pictureBox5.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.YEAR_MONTH_AVERAGE_INTERVAL);
            this.groupBox1.Controls.Add(this.MONTH_AVERAGE_INTERVAL);
            this.groupBox1.Controls.Add(this.YEAR_WEEK_AVERAGE_INTERVAL);
            this.groupBox1.Controls.Add(this.WEEK_AVERAGE_INTERVAL);
            this.groupBox1.Controls.Add(this.YEAR_MONTH_AVERAGE);
            this.groupBox1.Controls.Add(this.YEAR_WEEK_AVERAGE);
            this.groupBox1.Controls.Add(this.MONTH_AVERAGE);
            this.groupBox1.Controls.Add(this.WEEK_AVERAGE);
            this.groupBox1.Controls.Add(this.YEAR_MONTH);
            this.groupBox1.Controls.Add(this.YEAR_WEEK);
            this.groupBox1.Controls.Add(this.YEAR_YESTERDAY);
            this.groupBox1.Controls.Add(this.MONTH);
            this.groupBox1.Controls.Add(this.WEEK);
            this.groupBox1.Controls.Add(this.YESTERDAY);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 131);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1052, 80);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "비교조건";
            // 
            // YEAR_MONTH_AVERAGE_INTERVAL
            // 
            this.YEAR_MONTH_AVERAGE_INTERVAL.FormattingEnabled = true;
            this.YEAR_MONTH_AVERAGE_INTERVAL.Location = new System.Drawing.Point(828, 46);
            this.YEAR_MONTH_AVERAGE_INTERVAL.Name = "YEAR_MONTH_AVERAGE_INTERVAL";
            this.YEAR_MONTH_AVERAGE_INTERVAL.Size = new System.Drawing.Size(62, 20);
            this.YEAR_MONTH_AVERAGE_INTERVAL.TabIndex = 13;
            // 
            // MONTH_AVERAGE_INTERVAL
            // 
            this.MONTH_AVERAGE_INTERVAL.FormattingEnabled = true;
            this.MONTH_AVERAGE_INTERVAL.Location = new System.Drawing.Point(828, 20);
            this.MONTH_AVERAGE_INTERVAL.Name = "MONTH_AVERAGE_INTERVAL";
            this.MONTH_AVERAGE_INTERVAL.Size = new System.Drawing.Size(62, 20);
            this.MONTH_AVERAGE_INTERVAL.TabIndex = 12;
            // 
            // YEAR_WEEK_AVERAGE_INTERVAL
            // 
            this.YEAR_WEEK_AVERAGE_INTERVAL.FormattingEnabled = true;
            this.YEAR_WEEK_AVERAGE_INTERVAL.Location = new System.Drawing.Point(585, 48);
            this.YEAR_WEEK_AVERAGE_INTERVAL.Name = "YEAR_WEEK_AVERAGE_INTERVAL";
            this.YEAR_WEEK_AVERAGE_INTERVAL.Size = new System.Drawing.Size(62, 20);
            this.YEAR_WEEK_AVERAGE_INTERVAL.TabIndex = 11;
            // 
            // WEEK_AVERAGE_INTERVAL
            // 
            this.WEEK_AVERAGE_INTERVAL.FormattingEnabled = true;
            this.WEEK_AVERAGE_INTERVAL.Location = new System.Drawing.Point(585, 19);
            this.WEEK_AVERAGE_INTERVAL.Name = "WEEK_AVERAGE_INTERVAL";
            this.WEEK_AVERAGE_INTERVAL.Size = new System.Drawing.Size(62, 20);
            this.WEEK_AVERAGE_INTERVAL.TabIndex = 10;
            // 
            // YEAR_MONTH_AVERAGE
            // 
            this.YEAR_MONTH_AVERAGE.AutoSize = true;
            this.YEAR_MONTH_AVERAGE.Location = new System.Drawing.Point(684, 50);
            this.YEAR_MONTH_AVERAGE.Name = "YEAR_MONTH_AVERAGE";
            this.YEAR_MONTH_AVERAGE.Size = new System.Drawing.Size(144, 16);
            this.YEAR_MONTH_AVERAGE.TabIndex = 9;
            this.YEAR_MONTH_AVERAGE.Text = "전년 월간별 평균 비교";
            this.YEAR_MONTH_AVERAGE.UseVisualStyleBackColor = true;
            // 
            // YEAR_WEEK_AVERAGE
            // 
            this.YEAR_WEEK_AVERAGE.AutoSize = true;
            this.YEAR_WEEK_AVERAGE.Location = new System.Drawing.Point(436, 50);
            this.YEAR_WEEK_AVERAGE.Name = "YEAR_WEEK_AVERAGE";
            this.YEAR_WEEK_AVERAGE.Size = new System.Drawing.Size(144, 16);
            this.YEAR_WEEK_AVERAGE.TabIndex = 8;
            this.YEAR_WEEK_AVERAGE.Text = "전년 주간별 평균 비교";
            this.YEAR_WEEK_AVERAGE.UseVisualStyleBackColor = true;
            // 
            // MONTH_AVERAGE
            // 
            this.MONTH_AVERAGE.AutoSize = true;
            this.MONTH_AVERAGE.Location = new System.Drawing.Point(684, 24);
            this.MONTH_AVERAGE.Name = "MONTH_AVERAGE";
            this.MONTH_AVERAGE.Size = new System.Drawing.Size(116, 16);
            this.MONTH_AVERAGE.TabIndex = 7;
            this.MONTH_AVERAGE.Text = "월간별 평균 비교";
            this.MONTH_AVERAGE.UseVisualStyleBackColor = true;
            // 
            // WEEK_AVERAGE
            // 
            this.WEEK_AVERAGE.AutoSize = true;
            this.WEEK_AVERAGE.Location = new System.Drawing.Point(436, 24);
            this.WEEK_AVERAGE.Name = "WEEK_AVERAGE";
            this.WEEK_AVERAGE.Size = new System.Drawing.Size(116, 16);
            this.WEEK_AVERAGE.TabIndex = 6;
            this.WEEK_AVERAGE.Text = "주간별 평균 비교";
            this.WEEK_AVERAGE.UseVisualStyleBackColor = true;
            // 
            // YEAR_MONTH
            // 
            this.YEAR_MONTH.AutoSize = true;
            this.YEAR_MONTH.Location = new System.Drawing.Point(246, 50);
            this.YEAR_MONTH.Name = "YEAR_MONTH";
            this.YEAR_MONTH.Size = new System.Drawing.Size(96, 16);
            this.YEAR_MONTH.TabIndex = 5;
            this.YEAR_MONTH.Text = "전년월간비교";
            this.YEAR_MONTH.UseVisualStyleBackColor = true;
            // 
            // YEAR_WEEK
            // 
            this.YEAR_WEEK.AutoSize = true;
            this.YEAR_WEEK.Location = new System.Drawing.Point(124, 50);
            this.YEAR_WEEK.Name = "YEAR_WEEK";
            this.YEAR_WEEK.Size = new System.Drawing.Size(96, 16);
            this.YEAR_WEEK.TabIndex = 4;
            this.YEAR_WEEK.Text = "전년주간비교";
            this.YEAR_WEEK.UseVisualStyleBackColor = true;
            // 
            // YEAR_YESTERDAY
            // 
            this.YEAR_YESTERDAY.AutoSize = true;
            this.YEAR_YESTERDAY.Location = new System.Drawing.Point(7, 50);
            this.YEAR_YESTERDAY.Name = "YEAR_YESTERDAY";
            this.YEAR_YESTERDAY.Size = new System.Drawing.Size(96, 16);
            this.YEAR_YESTERDAY.TabIndex = 3;
            this.YEAR_YESTERDAY.Text = "전년전일비교";
            this.YEAR_YESTERDAY.UseVisualStyleBackColor = true;
            // 
            // MONTH
            // 
            this.MONTH.AutoSize = true;
            this.MONTH.Location = new System.Drawing.Point(246, 24);
            this.MONTH.Name = "MONTH";
            this.MONTH.Size = new System.Drawing.Size(72, 16);
            this.MONTH.TabIndex = 2;
            this.MONTH.Text = "월간비교";
            this.MONTH.UseVisualStyleBackColor = true;
            // 
            // WEEK
            // 
            this.WEEK.AutoSize = true;
            this.WEEK.Location = new System.Drawing.Point(124, 24);
            this.WEEK.Name = "WEEK";
            this.WEEK.Size = new System.Drawing.Size(72, 16);
            this.WEEK.TabIndex = 1;
            this.WEEK.Text = "주간비교";
            this.WEEK.UseVisualStyleBackColor = true;
            // 
            // YESTERDAY
            // 
            this.YESTERDAY.AutoSize = true;
            this.YESTERDAY.Checked = true;
            this.YESTERDAY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.YESTERDAY.Location = new System.Drawing.Point(7, 25);
            this.YESTERDAY.Name = "YESTERDAY";
            this.YESTERDAY.Size = new System.Drawing.Size(72, 16);
            this.YESTERDAY.TabIndex = 0;
            this.YESTERDAY.Text = "전일비교";
            this.YESTERDAY.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chartVisibleBtn);
            this.panel1.Controls.Add(this.searchBtn);
            this.panel1.Controls.Add(this.excelBtn);
            this.panel1.Controls.Add(this.tableVisibleBtn);
            this.panel1.Controls.Add(this.DAY_TOTAL);
            this.panel1.Controls.Add(this.DAY_LEAKAGE);
            this.panel1.Controls.Add(this.DAY_MNF);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 211);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1052, 30);
            this.panel1.TabIndex = 35;
            // 
            // chartVisibleBtn
            // 
            this.chartVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chartVisibleBtn.Location = new System.Drawing.Point(850, 3);
            this.chartVisibleBtn.Name = "chartVisibleBtn";
            this.chartVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.chartVisibleBtn.TabIndex = 33;
            this.chartVisibleBtn.TabStop = false;
            this.chartVisibleBtn.Text = "그래프";
            this.chartVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1012, 3);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 30;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(964, 3);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 31;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // tableVisibleBtn
            // 
            this.tableVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableVisibleBtn.Location = new System.Drawing.Point(906, 3);
            this.tableVisibleBtn.Name = "tableVisibleBtn";
            this.tableVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.tableVisibleBtn.TabIndex = 32;
            this.tableVisibleBtn.TabStop = false;
            this.tableVisibleBtn.Text = "테이블";
            this.tableVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // DAY_TOTAL
            // 
            this.DAY_TOTAL.AutoSize = true;
            this.DAY_TOTAL.Checked = true;
            this.DAY_TOTAL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DAY_TOTAL.Location = new System.Drawing.Point(7, 8);
            this.DAY_TOTAL.Name = "DAY_TOTAL";
            this.DAY_TOTAL.Size = new System.Drawing.Size(60, 16);
            this.DAY_TOTAL.TabIndex = 27;
            this.DAY_TOTAL.Text = "공급량";
            this.DAY_TOTAL.UseVisualStyleBackColor = true;
            // 
            // DAY_LEAKAGE
            // 
            this.DAY_LEAKAGE.AutoSize = true;
            this.DAY_LEAKAGE.Location = new System.Drawing.Point(124, 8);
            this.DAY_LEAKAGE.Name = "DAY_LEAKAGE";
            this.DAY_LEAKAGE.Size = new System.Drawing.Size(94, 16);
            this.DAY_LEAKAGE.TabIndex = 29;
            this.DAY_LEAKAGE.Text = "누수량(추정)";
            this.DAY_LEAKAGE.UseVisualStyleBackColor = true;
            // 
            // DAY_MNF
            // 
            this.DAY_MNF.AutoSize = true;
            this.DAY_MNF.Location = new System.Drawing.Point(246, 8);
            this.DAY_MNF.Name = "DAY_MNF";
            this.DAY_MNF.Size = new System.Drawing.Size(124, 16);
            this.DAY_MNF.TabIndex = 28;
            this.DAY_MNF.Text = "보정 야간최소유량";
            this.DAY_MNF.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.chartPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tablePanel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 241);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1052, 411);
            this.tableLayoutPanel1.TabIndex = 37;
            // 
            // chartPanel1
            // 
            this.chartPanel1.BackColor = System.Drawing.Color.White;
            this.chartPanel1.Controls.Add(this.chart1);
            this.chartPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel1.Location = new System.Drawing.Point(0, 10);
            this.chartPanel1.Margin = new System.Windows.Forms.Padding(0, 10, 0, 5);
            this.chartPanel1.Name = "chartPanel1";
            this.chartPanel1.Size = new System.Drawing.Size(1052, 231);
            this.chartPanel1.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart1.AllSeries.Line.Width = ((short)(1));
            this.chart1.AllSeries.MarkerSize = ((short)(1));
            this.chart1.AxisX.Font = new System.Drawing.Font("굴림", 8F);
            this.chart1.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart1.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart1.AxisY.Title.Text = "";
            solidBackground1.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart1.Background = solidBackground1;
            this.chart1.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart1.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart1.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart1.LegendBox.PlotAreaOnly = false;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.MainPane.Title.Text = "";
            this.chart1.Name = "chart1";
            this.chart1.RandomData.Series = 1;
            seriesAttributes1.Visible = false;
            seriesAttributes1.Volume = ((short)(0));
            this.chart1.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes1});
            this.chart1.Size = new System.Drawing.Size(1052, 231);
            this.chart1.TabIndex = 7;
            this.chart1.TabStop = false;
            // 
            // tablePanel1
            // 
            this.tablePanel1.BackColor = System.Drawing.Color.White;
            this.tablePanel1.Controls.Add(this.ultraGrid2);
            this.tablePanel1.Controls.Add(this.ultraGrid1);
            this.tablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel1.Location = new System.Drawing.Point(0, 251);
            this.tablePanel1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Size = new System.Drawing.Size(1052, 160);
            this.tablePanel1.TabIndex = 1;
            // 
            // ultraGrid2
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid2.DisplayLayout.Appearance = appearance25;
            appearance26.TextHAlignAsString = "Center";
            ultraGridColumn1.Header.Appearance = appearance26;
            ultraGridColumn1.Header.Caption = "지역코드";
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.Hidden = true;
            ultraGridColumn17.Header.Caption = "센터";
            ultraGridColumn17.Header.VisiblePosition = 1;
            ultraGridColumn17.Hidden = true;
            appearance27.TextHAlignAsString = "Center";
            ultraGridColumn2.Header.Appearance = appearance27;
            ultraGridColumn2.Header.Caption = "대블록";
            ultraGridColumn2.Header.VisiblePosition = 2;
            ultraGridColumn2.Hidden = true;
            ultraGridColumn2.Width = 60;
            appearance28.TextHAlignAsString = "Center";
            ultraGridColumn3.Header.Appearance = appearance28;
            ultraGridColumn3.Header.Caption = "중블록";
            ultraGridColumn3.Header.VisiblePosition = 3;
            ultraGridColumn3.Hidden = true;
            ultraGridColumn3.Width = 60;
            appearance29.TextHAlignAsString = "Center";
            ultraGridColumn4.Header.Appearance = appearance29;
            ultraGridColumn4.Header.Caption = "소블록";
            ultraGridColumn4.Header.VisiblePosition = 4;
            ultraGridColumn4.Hidden = true;
            ultraGridColumn4.Width = 60;
            appearance30.TextHAlignAsString = "Center";
            ultraGridColumn5.CellAppearance = appearance30;
            ultraGridColumn5.Format = "yyyy-MM-dd";
            appearance31.TextHAlignAsString = "Center";
            ultraGridColumn5.Header.Appearance = appearance31;
            ultraGridColumn5.Header.Caption = "일자";
            ultraGridColumn5.Header.VisiblePosition = 5;
            ultraGridColumn5.Width = 87;
            appearance32.TextHAlignAsString = "Right";
            ultraGridColumn6.CellAppearance = appearance32;
            ultraGridColumn6.Format = "###,###,###";
            appearance33.TextHAlignAsString = "Center";
            ultraGridColumn6.Header.Appearance = appearance33;
            ultraGridColumn6.Header.Caption = "공급유량(㎥/일)";
            ultraGridColumn6.Header.VisiblePosition = 6;
            ultraGridColumn6.Width = 107;
            appearance34.TextHAlignAsString = "Right";
            ultraGridColumn7.CellAppearance = appearance34;
            ultraGridColumn7.Format = "###,##0.00";
            appearance35.TextHAlignAsString = "Center";
            ultraGridColumn7.Header.Appearance = appearance35;
            ultraGridColumn7.Header.Caption = "보정야간최소유량(㎥/h)";
            ultraGridColumn7.Header.VisiblePosition = 8;
            ultraGridColumn7.Width = 196;
            appearance36.TextHAlignAsString = "Right";
            ultraGridColumn8.CellAppearance = appearance36;
            ultraGridColumn8.Format = "###,##0.0";
            appearance37.TextHAlignAsString = "Center";
            ultraGridColumn8.Header.Appearance = appearance37;
            ultraGridColumn8.Header.Caption = "일평균누수량(㎥/일)";
            ultraGridColumn8.Header.VisiblePosition = 7;
            ultraGridColumn8.Width = 129;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn17,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8});
            ultraGridBand1.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid2.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid2.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.ultraGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.ultraGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.ultraGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid2.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid2.DisplayLayout.Override.CellAppearance = appearance44;
            this.ultraGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.ultraGrid2.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.ultraGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid2.DisplayLayout.Override.RowAppearance = appearance47;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.ultraGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid2.Location = new System.Drawing.Point(728, 0);
            this.ultraGrid2.Name = "ultraGrid2";
            this.ultraGrid2.Size = new System.Drawing.Size(324, 160);
            this.ultraGrid2.TabIndex = 9;
            this.ultraGrid2.TabStop = false;
            this.ultraGrid2.Text = "ultraGrid2";
            this.ultraGrid2.Visible = false;
            // 
            // ultraGrid1
            // 
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance49;
            appearance50.TextHAlignAsString = "Center";
            ultraGridColumn9.Header.Appearance = appearance50;
            ultraGridColumn9.Header.Caption = "지역코드";
            ultraGridColumn9.Header.VisiblePosition = 0;
            ultraGridColumn9.Hidden = true;
            ultraGridColumn18.Header.Caption = "센터";
            ultraGridColumn18.Header.VisiblePosition = 1;
            ultraGridColumn18.Hidden = true;
            appearance51.TextHAlignAsString = "Center";
            ultraGridColumn10.Header.Appearance = appearance51;
            ultraGridColumn10.Header.Caption = "대블록";
            ultraGridColumn10.Header.VisiblePosition = 2;
            ultraGridColumn10.Width = 60;
            appearance52.TextHAlignAsString = "Center";
            ultraGridColumn11.Header.Appearance = appearance52;
            ultraGridColumn11.Header.Caption = "중블록";
            ultraGridColumn11.Header.VisiblePosition = 3;
            ultraGridColumn11.Width = 60;
            appearance53.TextHAlignAsString = "Center";
            ultraGridColumn12.Header.Appearance = appearance53;
            ultraGridColumn12.Header.Caption = "소블록";
            ultraGridColumn12.Header.VisiblePosition = 4;
            ultraGridColumn12.Width = 60;
            appearance54.TextHAlignAsString = "Center";
            ultraGridColumn13.CellAppearance = appearance54;
            ultraGridColumn13.Format = "yyyy-MM-dd";
            appearance55.TextHAlignAsString = "Center";
            ultraGridColumn13.Header.Appearance = appearance55;
            ultraGridColumn13.Header.Caption = "일자";
            ultraGridColumn13.Header.VisiblePosition = 5;
            ultraGridColumn13.Width = 87;
            appearance56.TextHAlignAsString = "Right";
            ultraGridColumn14.CellAppearance = appearance56;
            ultraGridColumn14.Format = "###,###,###";
            appearance57.TextHAlignAsString = "Center";
            ultraGridColumn14.Header.Appearance = appearance57;
            ultraGridColumn14.Header.Caption = "공급유량(㎥/일)";
            ultraGridColumn14.Header.VisiblePosition = 6;
            ultraGridColumn14.Width = 108;
            appearance58.TextHAlignAsString = "Right";
            ultraGridColumn15.CellAppearance = appearance58;
            ultraGridColumn15.Format = "###,##0.00";
            appearance59.TextHAlignAsString = "Center";
            ultraGridColumn15.Header.Appearance = appearance59;
            ultraGridColumn15.Header.Caption = "보정야간최소유량(㎥/h)";
            ultraGridColumn15.Header.VisiblePosition = 8;
            ultraGridColumn15.Width = 201;
            appearance60.TextHAlignAsString = "Right";
            ultraGridColumn16.CellAppearance = appearance60;
            ultraGridColumn16.Format = "###,##0.0";
            appearance61.TextHAlignAsString = "Center";
            ultraGridColumn16.Header.Appearance = appearance61;
            ultraGridColumn16.Header.Caption = "일평균누수량(㎥/일)";
            ultraGridColumn16.Header.VisiblePosition = 7;
            ultraGridColumn16.Width = 130;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn9,
            ultraGridColumn18,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16});
            ultraGridBand2.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.ultraGrid1.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance68;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            this.ultraGrid1.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.CellsOnly;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance71;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(728, 160);
            this.ultraGrid1.TabIndex = 2;
            this.ultraGrid1.TabStop = false;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1052, 111);
            this.searchBox1.TabIndex = 26;
            // 
            // BlockOperationAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 662);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "BlockOperationAnalysis";
            this.Text = "공급량/ 야간최소유량 분석 조회";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.chartPanel1.ResumeLayout(false);
            this.chartPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private SearchBox searchBox1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox WEEK;
        private System.Windows.Forms.CheckBox YESTERDAY;
        private System.Windows.Forms.CheckBox MONTH_AVERAGE;
        private System.Windows.Forms.CheckBox WEEK_AVERAGE;
        private System.Windows.Forms.CheckBox YEAR_MONTH;
        private System.Windows.Forms.CheckBox YEAR_WEEK;
        private System.Windows.Forms.CheckBox YEAR_YESTERDAY;
        private System.Windows.Forms.CheckBox MONTH;
        private System.Windows.Forms.ComboBox YEAR_MONTH_AVERAGE_INTERVAL;
        private System.Windows.Forms.ComboBox MONTH_AVERAGE_INTERVAL;
        private System.Windows.Forms.ComboBox YEAR_WEEK_AVERAGE_INTERVAL;
        private System.Windows.Forms.ComboBox WEEK_AVERAGE_INTERVAL;
        private System.Windows.Forms.CheckBox YEAR_MONTH_AVERAGE;
        private System.Windows.Forms.CheckBox YEAR_WEEK_AVERAGE;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button chartVisibleBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.Button tableVisibleBtn;
        private System.Windows.Forms.CheckBox DAY_TOTAL;
        private System.Windows.Forms.CheckBox DAY_LEAKAGE;
        private System.Windows.Forms.CheckBox DAY_MNF;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel chartPanel1;
        private ChartFX.WinForms.Chart chart1;
        private System.Windows.Forms.Panel tablePanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid2;
    }
}