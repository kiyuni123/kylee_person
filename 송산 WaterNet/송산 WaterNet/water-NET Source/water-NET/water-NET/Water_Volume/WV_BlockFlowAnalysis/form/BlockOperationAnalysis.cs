﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.manager;
using System.Collections;
using WaterNet.WV_Common.work;
using Infragistics.Win.UltraWinGrid;
using ChartFX.WinForms;
using WaterNet.WV_BlockFlowAnalysis.work;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.enum1;
using Infragistics.Win;
using EMFrame.log;

namespace WaterNet.WV_BlockFlowAnalysis.form
{
    public partial class BlockOperationAnalysis : Form
    {
        
        private IMapControlWV mainMap = null;
        private TableLayout tableLayout = null;
        private UltraGridManager gridManager = null;
        private ChartManager chartManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 영역 반환
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 생성자
        /// </summary>
        public BlockOperationAnalysis()
        {
            InitializeComponent();
            this.Load += new EventHandler(BlockOperationAnalysisLoad_EventHandler);
        }

        /// <summary>
        /// 폼로드 완료후 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BlockOperationAnalysisLoad_EventHandler(object sender, EventArgs e)
        {
            this.searchBox1.InitializeSearchBox();
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 폼 초기화 설정
        /// </summary>
        private void InitializeForm()
        {
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 60;
            this.tableLayout.DefaultTableHeight = 40;

            this.basicCheck = this.DAY_TOTAL;
            this.analysisCheck = this.YESTERDAY;

            this.searchBox1.Text = "검색조건";
            this.searchBox1.IntervalType = INTERVAL_TYPE.SINGLE_DATE;
            this.searchBox1.IntervalText = "기준일자";
            this.SetSearchInterval();
        }

        /// <summary>
        /// 이벤트 초기화 설정
        /// </summary>
        private void InitializeEvent()
        {
            this.DAY_TOTAL.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.DAY_MNF.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.DAY_LEAKAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
           
            this.YESTERDAY.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.WEEK.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.MONTH.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.YEAR_YESTERDAY.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.YEAR_WEEK.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.YEAR_MONTH.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.WEEK_AVERAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.MONTH_AVERAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.YEAR_WEEK_AVERAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.YEAR_MONTH_AVERAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);

            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);
            this.searchBtn.Click += new EventHandler(SelectBlockOperationAnalysis_EventHandler);

            this.ultraGrid2.AfterRowRegionScroll += new RowScrollRegionEventHandler(ultraGrid2_AfterRowRegionScroll);

            this.ultraGrid1.MouseDown += new MouseEventHandler(ultraGrid1_MouseDown);
            this.ultraGrid2.MouseDown += new MouseEventHandler(ultraGrid2_MouseDown);
        }

        /// <summary>
        /// 그리드1 로우클릭이벤트 핸들러
        /// 그리드2가 활성상태라면 그리드1의 로우를 클릭했을때 그리드2의 로우도 같이 클릭 (예외처리필요)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_MouseDown(object sender, MouseEventArgs e)
        {
            if (!ultraGrid2.Visible)
            {
                return;
            }

            if (e.Button == MouseButtons.Left)
            {
                Infragistics.Win.UIElement element = this.ultraGrid1.DisplayLayout.UIElement.ElementFromPoint(new System.Drawing.Point(e.X, e.Y));
                UltraGridCell cell = element.GetContext(typeof(UltraGridCell)) as UltraGridCell;

                if (cell != null)
                {
                    if (this.ultraGrid1.ActiveRow != null)
                    {
                        this.ultraGrid1.ActiveRow.Activated = false;
                    }
                    if (this.ultraGrid2.ActiveRow != null)
                    {
                        this.ultraGrid2.ActiveRow.Activated = false;
                    }

                    this.ultraGrid1.Selected.Rows.Clear();
                    this.ultraGrid1.ActiveRow = cell.Row;
                    cell.Row.Selected = true;

                    this.ultraGrid2.Selected.Rows.Clear();
                    this.ultraGrid2.Rows[cell.Row.Index].Activated = true;
                    this.ultraGrid2.Rows[cell.Row.Index].Selected = true;
                }
            }
            this.ultraGrid2.Select();
        }

        /// <summary>
        /// 그리드2 로우클릭이벤트 핸들러
        /// 그리드2의 로우를 클릭했을때 그리드1의 로우도 같이 클릭 (예외처리필요)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Infragistics.Win.UIElement element = this.ultraGrid2.DisplayLayout.UIElement.ElementFromPoint(new System.Drawing.Point(e.X, e.Y));
                UltraGridCell cell = element.GetContext(typeof(UltraGridCell)) as UltraGridCell;

                if (cell != null)
                {
                    if (this.ultraGrid1.ActiveRow != null)
                    {
                        this.ultraGrid1.ActiveRow.Activated = false;
                    }
                    if (this.ultraGrid2.ActiveRow != null)
                    {
                        this.ultraGrid2.ActiveRow.Activated = false;
                    }

                    this.ultraGrid2.Selected.Rows.Clear();
                    this.ultraGrid2.ActiveRow = cell.Row;
                    cell.Row.Selected = true;

                    this.ultraGrid1.Selected.Rows.Clear();
                    this.ultraGrid1.Rows[cell.Row.Index].Activated = true;
                    this.ultraGrid1.Rows[cell.Row.Index].Selected = true;
                }
            }
        }

        /// <summary>
        /// 그리드2 스크롤바 위치변경 이벤트 핸들러
        /// 그리드1의 위치도 동일하게 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid2_AfterRowRegionScroll(object sender, RowScrollRegionEventArgs e)
        {
            this.ultraGrid1.DisplayLayout.RowScrollRegions[0].ScrollPosition = e.RowScrollRegion.ScrollPosition;
        }

        /// <summary>
        /// 그리드 초기화 설정
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);
            
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.Default;
            this.ultraGrid1.DisplayLayout.Override.ColumnSizingArea = ColumnSizingArea.HeadersOnly;
            this.ultraGrid1.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.ultraGrid1.DisplayLayout.Override.ColumnAutoSizeMode = ColumnAutoSizeMode.None;
            this.ultraGrid1.DisplayLayout.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.None;
            this.ultraGrid1.Dock = DockStyle.Fill;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.Default;
            this.ultraGrid2.DisplayLayout.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.Automatic;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
        }

        private CheckBox analysisCheck = null;
        private CheckBox basicCheck = null;

        /// <summary>
        /// 체크박스 체크 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxCheckedChanged_EventHandler(object sender, EventArgs e)
        {
            if (sender.Equals(this.DAY_TOTAL) || sender.Equals(this.DAY_MNF) || sender.Equals(this.DAY_LEAKAGE))
            {
                this.ChangeChartSetting();
            }
            else
            {
                if (!sender.Equals(this.analysisCheck))
                {
                    this.analysisCheck.Checked = false;
                    this.analysisCheck = (CheckBox)sender;
                }
            }
        }

        /// <summary>
        /// 검색조건의 체크 여부 판단
        /// </summary>
        /// <returns></returns>
        private bool IsSearchCheckBoxChecked()
        {
            bool result = true;

            if (!this.YESTERDAY.Checked && !this.WEEK.Checked && !this.MONTH.Checked && 
                !this.WEEK_AVERAGE.Checked && !this.MONTH_AVERAGE.Checked &&
                !this.YEAR_YESTERDAY.Checked && !this.YEAR_WEEK.Checked && !this.YEAR_MONTH.Checked && 
                !this.YEAR_WEEK_AVERAGE.Checked && !this.YEAR_MONTH_AVERAGE.Checked)
            {
                result = false;
            }
            return result;
        }

        private bool CASE = false;

        /// <summary>
        /// 검색조건중 사용자 선택 기간에 따라서 검색기간을 설정한다.
        /// </summary>
        /// <returns></returns>
        private void SetSearchDateParameter(Hashtable parameter)
        {
            //기존에 존재하는 키를 삭제한다.
            if (parameter.ContainsKey("STARTDATE"))
            {
                parameter.Remove("STARTDATE");
            }

            if (parameter.ContainsKey("ENDDATE"))
            {
                parameter.Remove("ENDDATE");
            }

            //조회기준일자(조회기준은 어제임)
            DateTime standDate = (DateTime)searchBox1.DateObject.Value;

            //비교대상이 어제인 경우(어제~어제)
            if (this.YESTERDAY.Checked)
            {
                parameter["STARTDATE"] = standDate.AddDays(-1).ToString("yyyyMMdd");
                parameter["ENDDATE"] = standDate.ToString("yyyyMMdd");
                parameter["INTERVAL"] = this.YESTERDAY.Name;
            }
            //비교대상이 일주일인 경우(어제~7일전)
            else if (this.WEEK.Checked)
            {
                parameter["STARTDATE"] = standDate.AddDays(-7).ToString("yyyyMMdd");
                parameter["ENDDATE"] = standDate.ToString("yyyyMMdd");
                parameter["INTERVAL"] = this.WEEK.Name;
            }
            //비교대상이 한달인 경우
            else if (this.MONTH.Checked)
            {
                parameter["STARTDATE"] = standDate.AddMonths(-1).ToString("yyyyMMdd");
                parameter["ENDDATE"] = standDate.ToString("yyyyMMdd");
                parameter["INTERVAL"] = this.MONTH.Name;
            }
            //비교대상이 주간별평균인 경우
            else if (this.WEEK_AVERAGE.Checked)
            {
                int startDays = (Convert.ToInt32(this.WEEK_AVERAGE_INTERVAL.SelectedValue)) * 7;

                parameter["STARTDATE"] = standDate.AddDays(-startDays).ToString("yyyyMMdd");
                parameter["ENDDATE"] = standDate.ToString("yyyyMMdd");
                parameter["INTERVAL"] = this.WEEK_AVERAGE.Name;
            }
            //비교대상이 월간별평균인 경우
            else if (this.MONTH_AVERAGE.Checked)
            {
                int startMonths = Convert.ToInt32(this.MONTH_AVERAGE_INTERVAL.SelectedValue);

                parameter["STARTDATE"] = standDate.AddMonths(-startMonths).ToString("yyyyMMdd");
                parameter["ENDDATE"] = standDate.ToString("yyyyMMdd");
                parameter["INTERVAL"] = this.MONTH_AVERAGE.Name;
            }
            //비교대상이 전년전일인 경우
            else if (this.YEAR_YESTERDAY.Checked)
            {
                //전년전일
                if (CASE)
                {
                    parameter["STARTDATE"] = standDate.AddYears(-1).AddDays(-1).ToString("yyyyMMdd");
                    parameter["ENDDATE"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_YESTERDAY.Name;
                }
                else if (!CASE)
                {
                    parameter["STARTDATE1"] = standDate.AddDays(-1).ToString("yyyyMMdd");
                    parameter["ENDDATE1"] = standDate.ToString("yyyyMMdd");
                    parameter["STARTDATE2"] = standDate.AddYears(-1).AddDays(-1).ToString("yyyyMMdd");
                    parameter["ENDDATE2"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_YESTERDAY.Name;
                }
            }
            //비교대상이 전년전주인 경우
            else if (this.YEAR_WEEK.Checked)
            {
                //전년전주
                if (CASE)
                {
                    parameter["STARTDATE1"] = standDate.AddYears(-1).AddDays(-7).ToString("yyyyMMdd");
                    parameter["ENDDATE1"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_WEEK.Name;
                }
                else if (!CASE)
                {
                    parameter["STARTDATE1"] = standDate.AddDays(-7).ToString("yyyyMMdd");
                    parameter["ENDDATE1"] = standDate.ToString("yyyyMMdd");
                    parameter["STARTDATE2"] = standDate.AddYears(-1).AddDays(-7).ToString("yyyyMMdd");
                    parameter["ENDDATE2"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_WEEK.Name;
                }
            }
            //비교대상이 전년월간인 경우
            else if (this.YEAR_MONTH.Checked)
            {
                //전년전월
                if (CASE)
                {
                    parameter["STARTDATE"] = standDate.AddYears(-1).AddMonths(-1).ToString("yyyyMMdd");
                    parameter["ENDDATE"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_MONTH.Name;
                }
                else if (!CASE)
                {
                    parameter["STARTDATE1"] = standDate.AddMonths(-1).ToString("yyyyMMdd");
                    parameter["ENDDATE1"] = standDate.ToString("yyyyMMdd");
                    parameter["STARTDATE2"] = standDate.AddYears(-1).AddMonths(-1).ToString("yyyyMMdd");
                    parameter["ENDDATE2"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_MONTH.Name;
                }
            }
            //비교대상이 전년주간별평균인 경우
            else if (this.YEAR_WEEK_AVERAGE.Checked)
            {
                int startDays = (Convert.ToInt32(this.YEAR_WEEK_AVERAGE_INTERVAL.SelectedValue)) * 7;

                //전년전월
                if (CASE)
                {
                    parameter["STARTDATE"] = standDate.AddYears(-1).AddDays(-startDays).ToString("yyyyMMdd");
                    parameter["ENDDATE"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_WEEK_AVERAGE.Name;
                }
                else if (!CASE)
                {
                    parameter["STARTDATE1"] = standDate.AddDays(-startDays).ToString("yyyyMMdd");
                    parameter["ENDDATE1"] = standDate.ToString("yyyyMMdd");
                    parameter["STARTDATE2"] = standDate.AddYears(-1).AddDays(-startDays).ToString("yyyyMMdd");
                    parameter["ENDDATE2"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_WEEK_AVERAGE.Name;
                }
            }
            //비교대상이 전년월간별평균인 경우
            else if (this.YEAR_MONTH_AVERAGE.Checked)
            {
                int startMonths = Convert.ToInt32(this.YEAR_MONTH_AVERAGE_INTERVAL.SelectedValue);

                if (CASE)
                {
                    parameter["STARTDATE"] = standDate.AddYears(-1).AddMonths(-startMonths).ToString("yyyyMMdd");
                    parameter["ENDDATE"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_MONTH_AVERAGE.Name;
                }
                else if (!CASE)
                {
                    parameter["STARTDATE1"] = standDate.AddMonths(-startMonths).ToString("yyyyMMdd");
                    parameter["ENDDATE1"] = standDate.ToString("yyyyMMdd");
                    parameter["STARTDATE2"] = standDate.AddYears(-1).AddMonths(-startMonths).ToString("yyyyMMdd");
                    parameter["ENDDATE2"] = standDate.AddYears(-1).ToString("yyyyMMdd");
                    parameter["INTERVAL"] = this.YEAR_MONTH_AVERAGE.Name;
                }
            }
        }

        /// <summary>
        /// 검색기간 데이터 설정 
        /// </summary>
        private void SetSearchInterval()
        {
            ComboBoxUtils.AddData(WEEK_AVERAGE_INTERVAL, "CODE", "CODE_NAME", "주", 12);
            ComboBoxUtils.AddData(YEAR_WEEK_AVERAGE_INTERVAL, "CODE", "CODE_NAME", "주", 12);
            ComboBoxUtils.AddData(MONTH_AVERAGE_INTERVAL, "CODE", "CODE_NAME", "개월", 12);
            ComboBoxUtils.AddData(YEAR_MONTH_AVERAGE_INTERVAL, "CODE", "CODE_NAME", "개월", 12);

            WEEK_AVERAGE_INTERVAL.SelectedIndex = 3;
            YEAR_WEEK_AVERAGE_INTERVAL.SelectedIndex = 3;
            MONTH_AVERAGE_INTERVAL.SelectedIndex = 2;
            YEAR_MONTH_AVERAGE_INTERVAL.SelectedIndex = 2;
        }

        /// <summary>
        /// 블럭별운영현황 조회 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectBlockOperationAnalysis_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    Hashtable parameter = searchBox1.InitializeParameter().Parameters;
                    this.SetSearchDateParameter(parameter);
                    this.SelectBlockOperationAnalysis(parameter);
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            

        }

        private string interval_type;

        /// <summary>
        /// 블럭별운영현황 조회
        /// </summary>
        /// <param name="parameter">
        /// LOC_CODE : 지역코드
        /// STARTDATE : 검색시작일자
        /// ENDDATE : 검색종료일자
        /// INTERVAL : 비교조건
        /// </param>
        public void SelectBlockOperationAnalysis(Hashtable parameter)
        {
            //비교조건이 선택안된경우
            if (!this.IsSearchCheckBoxChecked())
            {
                MessageBox.Show("비교조건을 선택해야 합니다.");
                return;
            }
            if (parameter["FTR_CODE"].ToString() == "BZ001")
            {
                MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
                return;
            }
            this.mainMap.MoveToBlock(parameter);
            this.mainMap.SelectedFeatureByBlock(parameter);

            DataSet result = BlockOperationAnalysisWork.GetInstance().SelectBlockOperationAnalysis(parameter);

            this.gridManager.ClearDataSource(this.ultraGrid1);
            this.gridManager.ClearDataSource(this.ultraGrid2);

            this.interval_type = parameter["INTERVAL"].ToString();

            if (interval_type.Substring(0, 4) != "YEAR")
            {
                this.ChangeVisibleSubGrid(false);
                ultraGrid1.DataSource = result.Tables[0];
            }
            else if (interval_type.Substring(0, 4) == "YEAR")
            {
                this.ChangeVisibleSubGrid(true);
                ultraGrid1.DataSource = result.Tables[0];
                ultraGrid2.DataSource = result.Tables[1];
            }

            //차트변경
            this.ChangeChartSetting();
        }

        /// <summary>
        /// 엑셀파일 다운로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                excelManager.AddWorksheet(this.chart1, "공급량야간최소유량분석", 0, 0, 9, 20);

                excelManager.AddWorksheet(this.ultraGrid1, "공급량야간최소유량분석", 10, 0);

                if (this.ultraGrid2.Visible)
                {
                    excelManager.AddWorksheet(this.ultraGrid2, "공급량야간최소유량분석", 10, 7);
                }

                excelManager.Save("공급량야간최소유량분석", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 블럭의 종류(대,중,소)와 코드를 받아 검색조건 설정
        /// </summary>
        /// <param name="block_kind"></param>
        /// <param name="block_code"></param>
        public void SetSearchBlock(string blockKind, string blockCode)
        {
            //코드가 안넘어오는경우 그냥넝김다. (안넘기면 오류)
            if (blockKind == null || blockCode == null)
            {
                return;
            }
            Hashtable parameter = new Hashtable();
            parameter["FTR_CODE"] = blockKind;
            parameter["FTR_IDN"] = blockCode;

            //블럭정보,
            DataSet blockInfo = BlockWork.GetInstance().SelectBlockInfoByBlockCode(parameter, "RESULT");

            //값이없는경우 그냥 넘긴다.
            if (blockInfo.Tables.Count == 0)
            {
                return;
            }

            foreach (DataRow row in blockInfo.Tables["RESULT"].Rows)
            {
                if (row["LBLOCK"] != DBNull.Value)
                {
                    ComboBoxUtils.SelectItemByDisplayMember(this.searchBox1.LargeBlockObject, row["LBLOCK"].ToString());
                }

                if (row["MBLOCK"] != DBNull.Value)
                {
                    ComboBoxUtils.SelectItemByDisplayMember(this.searchBox1.MiddleBlockObject, row["MBLOCK"].ToString());
                }

                if (row["SBLOCK"] != DBNull.Value)
                {
                    ComboBoxUtils.SelectItemByDisplayMember(this.searchBox1.SmallBlockObject, row["SBLOCK"].ToString());
                }
            }
        }

        /// <summary>
        /// 그리드2의 활성여부에 의한 객체 핸들링
        /// </summary>
        /// <param name="isVisible"></param>
        private void ChangeVisibleSubGrid(bool isVisible)
        {
            this.ultraGrid2.Visible = isVisible;

            //오른쪽 테이블 활성화
            if (isVisible)
            {
                this.ultraGrid1.DisplayLayout.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.None;
                this.ultraGrid1.Dock = DockStyle.Left;
            }
            else if (!isVisible)
            {
                this.ultraGrid1.DisplayLayout.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.Automatic;
                this.ultraGrid1.Dock = DockStyle.Fill;
            }
        }

        /// <summary>
        /// 차트 기본값 설정
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-mm-dd");
        }

        /// <summary>
        /// 차트 가변값 설정
        /// </summary>
        private void ChangeChartSetting()
        {
            this.chart1.Series.Clear();
            this.chart1.DataSourceSettings.Fields.Clear();
            this.chart1.Panes.Clear();
            this.chart1.AxesX.Clear();
            this.chart1.AxesY.Clear();
            this.chart1.Data.Clear();

            if (this.interval_type == "YESTERDAY" || this.interval_type == "WEEK" || this.interval_type == "MONTH" ||
                this.interval_type == "WEEK_AVERAGE" || this.interval_type == "MONTH_AVERAGE")
            {
                this.chart1.Data.Series = 3;

                this.chart1.Series[0].Visible = false;
                this.chart1.Series[1].Visible = false;
                this.chart1.Series[2].Visible = false;

                this.chart1.Data.Points = ((DataTable)this.ultraGrid1.DataSource).Rows.Count;

                foreach (DataRow row in ((DataTable)this.ultraGrid1.DataSource).Rows)
                {
                    int pointIndex = ((DataTable)this.ultraGrid1.DataSource).Rows.IndexOf(row);
                    this.chart1.AxisX.Labels[pointIndex] = ((DateTime)row["DATEE"]).ToString("yyyy-MM-dd");
                    this.chart1.Data[0, pointIndex] = Convert.ToInt32(row["DAY_TOTAL"]);
                    this.chart1.Data[1, pointIndex] = Convert.ToDouble(row["DAY_LEAKAGE"]);
                    this.chart1.Data[2, pointIndex] = Convert.ToDouble(row["DAY_MNF"]);
                }

                this.chart1.Series[0].Gallery = Gallery.Bar;
                this.chart1.Series[1].Gallery = Gallery.Bar;
                this.chart1.Series[2].Gallery = Gallery.Bar;

                this.chart1.Series[0].Text = "공급량(㎥/일)";
                this.chart1.Series[1].Text = "일평균누수량(㎥/일)";
                this.chart1.Series[2].Text = "보정 야간최소유량(㎥/h)";

                this.chart1.Series[2].AxisY = this.chart1.AxisY2;

                this.chart1.AxisY.Title.Text = "공급량/일평균누수량(㎥/일)";
                this.chart1.AxisY2.Title.Text = "보정 야간최소유량(㎥/h)";

                this.chart1.AllSeries.PointLabels.Visible = true;
                this.chart1.AllSeries.PointLabels.Font = new Font("굴림", 8);
                this.chart1.AllSeries.PointLabels.TextColor = Color.Black;

                if (this.DAY_TOTAL.Checked)
                {
                    chart1.Series[0].Visible = true;
                }
                if (this.DAY_MNF.Checked)
                {
                    chart1.Series[2].Visible = true;
                }
                if (this.DAY_LEAKAGE.Checked)
                {
                    chart1.Series[1].Visible = true;
                }
            }
            else if (this.interval_type == "YEAR_YESTERDAY" || this.interval_type == "YEAR_WEEK" || this.interval_type == "YEAR_MONTH" ||
                this.interval_type == "YEAR_WEEK_AVERAGE" || this.interval_type == "YEAR_MONTH_AVERAGE")
            {
                this.chart1.Data.Series = 6;

                this.chart1.Series[0].Visible = false;
                this.chart1.Series[1].Visible = false;
                this.chart1.Series[2].Visible = false;
                this.chart1.Series[3].Visible = false;
                this.chart1.Series[4].Visible = false;
                this.chart1.Series[5].Visible = false;

                this.chart1.Data.Points = ((DataTable)this.ultraGrid1.DataSource).Rows.Count + ((DataTable)this.ultraGrid2.DataSource).Rows.Count;

                foreach (DataRow row in ((DataTable)this.ultraGrid1.DataSource).Rows)
                {
                    int pointIndex = ((DataTable)this.ultraGrid1.DataSource).Rows.IndexOf(row);
                    this.chart1.AxisX.Labels[pointIndex] = ((DateTime)row["DATEE"]).ToString("yyyy-MM-dd");
                    this.chart1.Data[0, pointIndex] = Convert.ToInt32(row["DAY_TOTAL"]);
                    this.chart1.Data[1, pointIndex] = Convert.ToDouble(row["DAY_LEAKAGE"]);
                    this.chart1.Data[2, pointIndex] = Convert.ToDouble(row["DAY_MNF"]);
                }

                foreach (DataRow row in ((DataTable)this.ultraGrid2.DataSource).Rows)
                {
                    int pointIndex = ((DataTable)this.ultraGrid1.DataSource).Rows.Count + ((DataTable)this.ultraGrid2.DataSource).Rows.IndexOf(row);
                    this.chart1.AxisX.Labels[pointIndex] = ((DateTime)row["DATEE"]).ToString("yyyy-MM-dd");
                    this.chart1.Data[3, pointIndex] = Convert.ToInt32(row["DAY_TOTAL"]);
                    this.chart1.Data[4, pointIndex] = Convert.ToDouble(row["DAY_LEAKAGE"]);
                    this.chart1.Data[5, pointIndex] = Convert.ToDouble(row["DAY_MNF"]);
                }

                this.chart1.Series[0].Gallery = Gallery.Bar;
                this.chart1.Series[1].Gallery = Gallery.Bar;
                this.chart1.Series[2].Gallery = Gallery.Bar;
                this.chart1.Series[3].Gallery = Gallery.Bar;
                this.chart1.Series[4].Gallery = Gallery.Bar;
                this.chart1.Series[5].Gallery = Gallery.Bar;

                this.chart1.Series[0].Text = "공급량(㎥/일)";
                this.chart1.Series[1].Text = "일평균누수량(㎥/일)";
                this.chart1.Series[3].Text = "전년 공급량(㎥/일)";
                this.chart1.Series[4].Text = "전년 일평균누수량(㎥/일)";

                this.chart1.Series[2].Text = "보정 야간최소유량(㎥/h)";
                this.chart1.Series[5].Text = "전년 보정 야간최소유량(㎥/h)";

                this.chart1.Series[2].AxisY = this.chart1.AxisY2;
                this.chart1.Series[5].AxisY = this.chart1.AxisY2;

                this.chart1.AxisY.Title.Text = "공급량/일평균누수량(㎥/일)";
                this.chart1.AxisY2.Title.Text = "보정 야간최소유량(㎥/h)";

                this.chart1.AllSeries.PointLabels.Visible = true;
                this.chart1.AllSeries.PointLabels.Font = new Font("굴림", 8);
                this.chart1.AllSeries.PointLabels.TextColor = Color.Black;

                if (this.DAY_TOTAL.Checked)
                {
                    chart1.Series[0].Visible = true;
                    chart1.Series[3].Visible = true;
                }
                if (this.DAY_MNF.Checked)
                {
                    chart1.Series[2].Visible = true;
                    chart1.Series[5].Visible = true;
                }
                if (this.DAY_LEAKAGE.Checked)
                {
                    chart1.Series[1].Visible = true;
                    chart1.Series[4].Visible = true;
                }
            }
        }
    }
}
