﻿using System;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using WaterNet.WV_Common.dao;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_BlockFlowAnalysis.dao
{
    public class BlockOperationAnalysisDao : BaseDao
    {
        private static BlockOperationAnalysisDao dao = null;

        private BlockOperationAnalysisDao() {}

        public static BlockOperationAnalysisDao GetInstance()
        {
            if (dao == null)
            {
                dao = new BlockOperationAnalysisDao();
            }
            return dao;
        }

        public void SelectBlockOperationAnalysis(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                                                         ");
            query.AppendLine("(                                                                                                                                                                   ");
            query.AppendLine("select c1.loc_code                                                                                                                                                  ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                     ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                             ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                                                  ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                         ");
            query.AppendLine("      ,decode(c1.ftr_code, 'BZ002', (select loc_gbn from cm_location where loc_name = c1.rel_loc_name)) rel_loc_gbn                                                 ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                                                              ");
            query.AppendLine("      ,c1.ord                                                                                                                                                       ");
            query.AppendLine("  from                                                                                                                                                              ");
            query.AppendLine("      (                                                                                                                                                             ");
            query.AppendLine("       select sgccd                                                                                                                                                 ");
            query.AppendLine("             ,loc_code                                                                                                                                              ");
            query.AppendLine("             ,ploc_code                                                                                                                                             ");
            query.AppendLine("             ,loc_name                                                                                                                                              ");
            query.AppendLine("             ,ftr_idn                                                                                                                                               ");
            query.AppendLine("             ,ftr_code                                                                                                                                              ");
            query.AppendLine("             ,rel_loc_name                                                                                                                                          ");
            query.AppendLine("             ,kt_gbn                                                                                                                                                ");
            query.AppendLine("             ,rownum ord                                                                                                                                            ");
            query.AppendLine("         from cm_location                                                                                                                                           ");
            query.AppendLine("        where 1 = 1                                                                                                                                                 ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                  ");
            query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                               ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                       ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                   ");
            query.AppendLine("      ) c1                                                                                                                                                          ");
            query.AppendLine("       ,cm_location c2                                                                                                                                              ");
            query.AppendLine("       ,cm_location c3                                                                                                                                              ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                ");
            query.AppendLine(" order by c1.ord                                                                                                                                                    ");
            query.AppendLine(")                                                                                                                                                                   ");
            query.AppendLine(",sub_loc as                                                                                                                                                         ");
            query.AppendLine("(                                                                                                                                                                   ");
            query.AppendLine("select sgccd, loc_code, ftr_idn                                                                                                                                     ");
            query.AppendLine("  from cm_location                                                                                                                                                  ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and ftr_code = 'BZ003'                                                                                                                                           ");
            query.AppendLine(" start with loc_code = :LOC_CODE                                                                                                                                    ");
            query.AppendLine("connect by prior loc_code = ploc_code                                                                                                                               ");
            query.AppendLine(")                                                                                                                                                                   ");
            query.AppendLine("select wbd.loc_code                                                                                                                                                 ");
            query.AppendLine("      ,wbd.sgccd                                                                                                                                                    ");
            query.AppendLine("      ,wbd.lblock                                                                                                                                                   ");
            query.AppendLine("      ,wbd.mblock                                                                                                                                                   ");
            query.AppendLine("      ,wbd.sblock                                                                                                                                                   ");
            query.AppendLine("      ,to_date(wbd.datee,'yyyymmdd') datee                                                                                                                          ");
            query.AppendLine("      ,wbd.jumal_used                                                                                                                                               ");
            query.AppendLine("      ,wbd.gajung_used                                                                                                                                              ");
            query.AppendLine("      ,wbd.bgajung_used                                                                                                                                             ");
            query.AppendLine("      ,wbd.special_used_percent                                                                                                                                     ");
            query.AppendLine("      ,wbd.special_used                                                                                                                                             ");
            query.AppendLine("      ,wbd.n1                                                                                                                                                       ");
            query.AppendLine("      ,wbd.lc                                                                                                                                                       ");
            query.AppendLine("      ,wbd.icf                                                                                                                                                      ");
            query.AppendLine("      ,wcd.gajung_gagusu                                                                                                                                            ");
            query.AppendLine("      ,wcd.bgajung_gagusu                                                                                                                                           ");
            query.AppendLine("      ,wcd.gupsujunsu                                                                                                                                               ");
            query.AppendLine("      ,wpl.pip_len                                                                                                                                                  ");
            query.AppendLine("      ,wlm.special_amtuse                                                                                                                                           ");
            query.AppendLine("      ,iwn.mvavg_mintime                                                                                                                                            ");
            query.AppendLine("      ,iwn.m_average                                                                                                                                                ");
            query.AppendLine("      ,iwn.filtering                                                                                                                                                ");
            query.AppendLine("      ,iay.supply                                                                                                                                                   ");
            query.AppendLine("      ,iip.hpres_in infpnt_hpres_davg                                                                                                                               ");
            query.AppendLine("      ,iip.hh00_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh01_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh02_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh03_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh04_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh05_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh06_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh07_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh08_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh09_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh10_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh11_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh12_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh13_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh14_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh15_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh16_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh17_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh18_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh19_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh20_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh21_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh22_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,iip.hh23_hpres_in                                                                                                                                            ");
            query.AppendLine("      ,imp.avg_hpres_pavg                                                                                                                                           ");
            query.AppendLine("      ,imp.hh00_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh01_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh02_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh03_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh04_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh05_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh06_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh07_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh08_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh09_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh10_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh11_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh12_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh13_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh14_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh15_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh16_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh17_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh18_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh19_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh20_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh21_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh22_hpres_mid                                                                                                                                           ");
            query.AppendLine("      ,imp.hh23_hpres_mid                                                                                                                                           ");
            query.AppendLine("  from                                                                                                                                                              ");
            query.AppendLine("(                                                                                                                                                                   ");
            query.AppendLine(" select wbd.loc_code                                                                                                                                                ");
            query.AppendLine("       ,loc.sgccd                                                                                                                                                   ");
            query.AppendLine("       ,loc.lblock                                                                                                                                                  ");
            query.AppendLine("       ,loc.mblock                                                                                                                                                  ");
            query.AppendLine("       ,loc.sblock                                                                                                                                                  ");
            query.AppendLine("       ,wbd.datee                                                                                                                                                   ");
            query.AppendLine("       ,wbd.jumal_used                                                                                                                                              ");
            query.AppendLine("       ,wbd.gajung_used                                                                                                                                             ");
            query.AppendLine("       ,wbd.bgajung_used                                                                                                                                            ");
            query.AppendLine("       ,wbd.special_used_percent                                                                                                                                    ");
            query.AppendLine("       ,wbd.special_used                                                                                                                                            ");
            query.AppendLine("       ,wbd.n1                                                                                                                                                      ");
            query.AppendLine("       ,wbdo.lc                                                                                                                                                     ");
            query.AppendLine("       ,wbdo.icf                                                                                                                                                    ");
            query.AppendLine("   from loc                                                                                                                                                         ");
            query.AppendLine("       ,wv_block_default_option wbdo                                                                                                                                ");
            query.AppendLine("       ,wv_block_day wbd                                                                                                                                            ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and wbdo.loc_code = loc.loc_code                                                                                                                                ");
            query.AppendLine("    and wbd.loc_code = wbdo.loc_code                                                                                                                                ");
            query.AppendLine("    and wbd.datee between :STARTDATE and :ENDDATE                                                                                                                   ");
            query.AppendLine(") wbd                                                                                                                                                               ");
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select wcd.datee                                                                                                                                                   ");
            query.AppendLine("       ,sum(wcd.gajung_gagusu) gajung_gagusu                                                                                                                        ");
            query.AppendLine("       ,sum(wcd.bgajung_gagusu) bgajung_gagusu                                                                                                                      ");
            query.AppendLine("       ,sum(wcd.gupsujunsu) gupsujunsu                                                                                                                              ");
            query.AppendLine("   from sub_loc loc                                                                                                                                                 ");
            query.AppendLine("       ,wv_consumer_day wcd                                                                                                                                         ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and wcd.loc_code = loc.loc_code		                                                                                                                          ");
            query.AppendLine("    and wcd.datee between :STARTDATE and :ENDDATE                                                                                                                   ");
            query.AppendLine("  group by wcd.datee                                                                                                                                                ");
            query.AppendLine(") wcd                                                                                                                                                               ");
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select wpl.datee                                                                                                                                                   ");
            query.AppendLine("       ,sum(wpl.pip_len) pip_len                                                                                                                                    ");
            query.AppendLine("   from sub_loc loc                                                                                                                                                 ");
            query.AppendLine("       ,wv_pipe_lm_day wpl                                                                                                                                          ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and wpl.loc_code = loc.loc_code                                                                                                                                 ");
            query.AppendLine("    and wpl.datee between :STARTDATE and :ENDDATE                                                                                                                   ");
            query.AppendLine("    and wpl.saa_cde = 'SAA004'                                                                                                                                      ");
            query.AppendLine("  group by wpl.datee                                                                                                                                                ");
            query.AppendLine(") wpl                                                                                                                                                               ");
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select srw.stym                                                                                                                                                    ");
            query.AppendLine("       ,round((sum(to_number(wsstvol))*1000)/to_number(to_char((last_day(to_date(srw.stym||'01','yyyymmdd'))),'dd'))/24,2) special_amtuse                           ");
            query.AppendLine("   from sub_loc loc                                                                                                                                                 ");
            query.AppendLine("       ,wi_dminfo dmi                                                                                                                                                  ");
            query.AppendLine("       ,wv_lconsumer_link wll                                                                                                                                       ");
            query.AppendLine("       ,wi_stwchrg srw                                                                                                                                                 ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and dmi.sgccd = loc.sgccd                                                                                                                                       ");
            query.AppendLine("    and dmi.sftridn = loc.ftr_idn                                                                                                                                   ");
            query.AppendLine("    and dmi.dmno = wll.dmno                                                                                                                                         ");
            query.AppendLine("    and srw.dmno = dmi.dmno                                                                                                                                         ");
            query.AppendLine("    and srw.stym between substr(:STARTDATE, 1, 6) and substr(:ENDDATE, 1, 6)                                                                                        ");
            query.AppendLine("  group by srw.stym                                                                                                                                                 ");
            query.AppendLine(") wlm                                                                                                                                                               ");
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine(" select to_char(iwn.timestamp,'yyyymmdd') datee                                                                                                                     ");
            query.AppendLine("       ,to_char(to_date(min(iwn.mvavg_mintime), 'yyyymmddhh24miss'), 'hh24') mvavg_mintime                                                                          ");
            query.AppendLine("       ,round(to_number(sum(iwn.mvavg_min)),2) m_average                                                                                                            ");
            query.AppendLine("       ,round(to_number(sum(iwn.filtering)),2) filtering                                                                                                            ");
            query.AppendLine("   from loc                                                                                                                                                         ");
            query.AppendLine("       ,if_ihtags iih                                                                                                                                               ");
            query.AppendLine("       ,if_wv_mnf iwn                                                                                                                                               ");
            query.AppendLine("       ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                               ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and iih.loc_code = loc.tag_loc_code                                                                                                                             ");
            query.AppendLine("    and iih.tagname = itg.tagname                                                                             ");
            query.AppendLine("    and itg.tag_gbn = 'MNF'                                                                                                                                         ");
            query.AppendLine("    and iwn.tagname = itg.tagname                                                                                                                                   ");
            query.AppendLine("    and iwn.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                           ");
            query.AppendLine("  group by to_char(iwn.timestamp,'yyyymmdd')                                                                                                                        ");
            query.AppendLine(") iwn                                                                                                                                                               ");
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine("select to_char(iwp.timestamp, 'yyyymmdd') datee                                                                                                                     ");
            query.AppendLine("      ,round(to_number(avg(iwp.tavg)),2) hpres_in                                                                                                                   ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '00', iwp.tavg, null))),2) hh00_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '01', iwp.tavg, null))),2) hh01_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '02', iwp.tavg, null))),2) hh02_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '03', iwp.tavg, null))),2) hh03_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '04', iwp.tavg, null))),2) hh04_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '05', iwp.tavg, null))),2) hh05_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '06', iwp.tavg, null))),2) hh06_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '07', iwp.tavg, null))),2) hh07_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '08', iwp.tavg, null))),2) hh08_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '09', iwp.tavg, null))),2) hh09_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '10', iwp.tavg, null))),2) hh10_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '11', iwp.tavg, null))),2) hh11_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '12', iwp.tavg, null))),2) hh12_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '13', iwp.tavg, null))),2) hh13_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '14', iwp.tavg, null))),2) hh14_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '15', iwp.tavg, null))),2) hh15_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '16', iwp.tavg, null))),2) hh16_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '17', iwp.tavg, null))),2) hh17_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '18', iwp.tavg, null))),2) hh18_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '19', iwp.tavg, null))),2) hh19_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '20', iwp.tavg, null))),2) hh20_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '21', iwp.tavg, null))),2) hh21_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '22', iwp.tavg, null))),2) hh22_hpres_in                                                          ");
            query.AppendLine("      ,round(to_number(avg(decode(to_char(iwp.timestamp, 'hh24'), '23', iwp.tavg, null))),2) hh23_hpres_in                                                          ");
            query.AppendLine("  from sub_loc loc                                                                                                                                                  ");
            query.AppendLine("      ,if_ihtags iih                                                                                                                                                ");
            query.AppendLine("      ,if_wv_pravg iwp                                                                                                                                              ");
            query.AppendLine("      ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and iih.loc_code = loc.loc_code                                                                                                                                  ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                              ");
            query.AppendLine("   and itg.tag_gbn = 'PRI'                                                                                                                                          ");
            query.AppendLine("   and iwp.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and iwp.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                            ");
            query.AppendLine(" group by to_char(iwp.timestamp, 'yyyymmdd')                                                                                                                        ");
            query.AppendLine(") iip                                                                                                                                                               ");
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine("select substr(wha.dates, 1, 8) datee                                                                                                                                ");
            query.AppendLine("      ,round(to_number(avg(wha.value)),2) avg_hpres_pavg                                                                                                            ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '00', wha.value, null))),2) hh00_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '01', wha.value, null))),2) hh01_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '02', wha.value, null))),2) hh02_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '03', wha.value, null))),2) hh03_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '04', wha.value, null))),2) hh04_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '05', wha.value, null))),2) hh05_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '06', wha.value, null))),2) hh06_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '07', wha.value, null))),2) hh07_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '08', wha.value, null))),2) hh08_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '09', wha.value, null))),2) hh09_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '10', wha.value, null))),2) hh10_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '11', wha.value, null))),2) hh11_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '12', wha.value, null))),2) hh12_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '13', wha.value, null))),2) hh13_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '14', wha.value, null))),2) hh14_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '15', wha.value, null))),2) hh15_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '16', wha.value, null))),2) hh16_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '17', wha.value, null))),2) hh17_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '18', wha.value, null))),2) hh18_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '19', wha.value, null))),2) hh19_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '20', wha.value, null))),2) hh20_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '21', wha.value, null))),2) hh21_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '22', wha.value, null))),2) hh22_hpres_mid                                                               ");
            query.AppendLine("      ,round(to_number(avg(decode(substr(wha.dates, 9, 2), '23', wha.value, null))),2) hh23_hpres_mid                                                               ");
            query.AppendLine("  from sub_loc loc                                                                                                                                                  ");
            query.AppendLine("      ,wv_hpres_avg wha                                                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and wha.loc_code = loc.loc_code                                                                                                                                  ");
            query.AppendLine("   and wha.dates between :STARTDATE||'0000' and :ENDDATE||'2359'                                                                                                    ");
            query.AppendLine(" group by substr(wha.dates, 1, 8)                                                                                                                                   ");
            query.AppendLine(") imp                                                                                                                                                               ");
            query.AppendLine(",(                                                                                                                                                                  ");
            query.AppendLine("select to_char(iay.timestamp,'yyyymmdd') datee                                                                                                                      ");
            query.AppendLine("      ,sum(value) supply                                                                                                                                            ");
            query.AppendLine("  from loc                                                                                                                                                          ");
            query.AppendLine("      ,if_ihtags iih                                                                                                                                                ");
            query.AppendLine("      ,if_accumulation_yesterday iay                                                                                                                                ");
            query.AppendLine("      ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and iih.loc_code = loc.tag_loc_code                                                                                                                              ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and itg.tag_gbn = 'YD'                                                                                                                                           ");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                                                                   ");
            query.AppendLine("   and iay.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                            ");
            query.AppendLine(" group by to_char(iay.timestamp,'yyyymmdd')                                                                                                                         ");
            query.AppendLine(") iay                                                                                                                                                               ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and wcd.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and wpl.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and wlm.stym(+) = substr(wbd.datee,1,6)                                                                                                                          ");
            query.AppendLine("   and iwn.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and iip.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and imp.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("   and iay.datee(+) = wbd.datee                                                                                                                                     ");
            query.AppendLine("order by datee                                                                                                                                                      ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];
            parameters[5].Value = parameter["ENDDATE"];
            parameters[6].Value = parameter["STARTDATE"];
            parameters[7].Value = parameter["ENDDATE"];
            parameters[8].Value = parameter["STARTDATE"];
            parameters[9].Value = parameter["ENDDATE"];
            parameters[10].Value = parameter["STARTDATE"];
            parameters[11].Value = parameter["ENDDATE"];
            parameters[12].Value = parameter["STARTDATE"];
            parameters[13].Value = parameter["ENDDATE"];
            parameters[14].Value = parameter["STARTDATE"];
            parameters[15].Value = parameter["ENDDATE"];
            parameters[16].Value = parameter["STARTDATE"];
            parameters[17].Value = parameter["ENDDATE"];

            //println(query);

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        Random ran = new Random();

        /// <summary>
        /// 테스트용 - 데이터를 일자간격기준으로 조회, 
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataSet"></param>
        /// <param name="dataMember"></param>
        /// <param name="parameter"></param>
        public void SelectBlockOperationAnalysisByDayInterval(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            DateTime startDate = new DateTime(
                Convert.ToInt32(parameter["STARTDATE"].ToString().Substring(0, 4)),
                Convert.ToInt32(parameter["STARTDATE"].ToString().Substring(4, 2)),
                Convert.ToInt32(parameter["STARTDATE"].ToString().Substring(6, 2))
                );

            DateTime endDate = new DateTime(
                Convert.ToInt32(parameter["ENDDATE"].ToString().Substring(0, 4)),
                Convert.ToInt32(parameter["ENDDATE"].ToString().Substring(4, 2)),
                Convert.ToInt32(parameter["ENDDATE"].ToString().Substring(6, 2))
                );

            DataTable result = new DataTable();
            result.TableName = dataMember;

            result.Columns.Add("LOC_CODE", typeof(string));
            result.Columns.Add("LBLOCK", typeof(string));
            result.Columns.Add("MBLOCK", typeof(string));
            result.Columns.Add("SBLOCK", typeof(string));
            result.Columns.Add("DATEE", typeof(DateTime));
            result.Columns.Add("DAY_TOTAL", typeof(int));
            result.Columns.Add("DAY_MNF", typeof(double));
            result.Columns.Add("DAY_LEAKAGE", typeof(double));


            if (parameter["INTERVAL"].ToString() == "WEEK_AVERAGE" || parameter["INTERVAL"].ToString() == "YEAR_WEEK_AVERAGE")
            {
                for (DateTime i = startDate; i <= endDate; i = i.AddDays(7))
                {
                    DataRow row = result.NewRow();
                    row["LOC_CODE"] = parameter["LOC_CODE"];
                    row["LBLOCK"] = parameter["LBLOCK"];
                    row["MBLOCK"] = parameter["MBLOCK"];
                    row["SBLOCK"] = parameter["SBLOCK"];
                    row["DATEE"] = i;
                    row["DAY_TOTAL"] = ran.Next(1500, 1700);
                    row["DAY_MNF"] = Convert.ToDouble(ran.Next(80, 110)) / 10;
                    row["DAY_LEAKAGE"] = Convert.ToDouble(ran.Next(2000, 3000)) / 10;
                    result.Rows.Add(row);
                }
            }
            else if (parameter["INTERVAL"].ToString() == "MONTH_AVERAGE" || parameter["INTERVAL"].ToString() == "YEAR_MONTH_AVERAGE")
            {
                for (DateTime i = startDate; i <= endDate; i = i.AddMonths(1))
                {
                    DataRow row = result.NewRow();
                    row["LOC_CODE"] = parameter["LOC_CODE"];
                    row["LBLOCK"] = parameter["LBLOCK"];
                    row["MBLOCK"] = parameter["MBLOCK"];
                    row["SBLOCK"] = parameter["SBLOCK"];
                    row["DATEE"] = i;
                    row["DAY_TOTAL"] = ran.Next(1500, 1700);
                    row["DAY_MNF"] = Convert.ToDouble(ran.Next(80, 110)) / 10;
                    row["DAY_LEAKAGE"] = Convert.ToDouble(ran.Next(2000, 3000)) / 10;
                    result.Rows.Add(row);
                }
            }
            else
            {
                for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
                {
                    DataRow row = result.NewRow();
                    row["LOC_CODE"] = parameter["LOC_CODE"];
                    row["LBLOCK"] = parameter["LBLOCK"];
                    row["MBLOCK"] = parameter["MBLOCK"];
                    row["SBLOCK"] = parameter["SBLOCK"];
                    row["DATEE"] = i;
                    row["DAY_TOTAL"] = ran.Next(1500, 1700);
                    row["DAY_MNF"] = Convert.ToDouble(ran.Next(80, 110)) / 10;
                    row["DAY_LEAKAGE"] = Convert.ToDouble(ran.Next(2000, 3000)) / 10;
                    result.Rows.Add(row);
                }
            }
            dataSet.Tables.Add(result);
        }
    }
}
