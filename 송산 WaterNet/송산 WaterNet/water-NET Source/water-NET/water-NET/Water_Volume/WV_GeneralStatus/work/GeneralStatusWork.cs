﻿using System;
using WaterNet.WV_GeneralStatus.dao;
using WaterNet.WV_Common.work;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using ESRI.ArcGIS.Controls;

namespace WaterNet.WV_GeneralStatus.work
{
    /// <summary>
    /// 일반현황 Work Class
    /// </summary>
    public class GeneralStatusWork : BaseWork
    {
        private static GeneralStatusWork work = null;
        private GeneralStatusDao dao = null;

        public static GeneralStatusWork GetInstance()
        {
            if (work == null)
            {
                work = new GeneralStatusWork();
            }
            return work;
        }

        private GeneralStatusWork()
        {
            dao = GeneralStatusDao.GetInstance();
        }

        public DataSet SelectBlockUsed(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockUsed(DataBaseManager, result, "BlockUsed", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectUsed(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                if (parameter.ContainsKey("IS_BIG_DM"))
                {
                    parameter.Remove("IS_BIG_DM");
                }

                parameter["IS_BIG_DM"] = "Y";

                Console.WriteLine("1");
                dao.SelectBlockInfo(DataBaseManager, result, "BlockInfo", parameter);
                Console.WriteLine("2");
                dao.SelectBlockUsed(DataBaseManager, result, "BlockUsed", parameter);
                Console.WriteLine("3");
                dao.SelectDMInfo(DataBaseManager, result, "DMInfo", parameter);
                Console.WriteLine("4");
                dao.SelectDMUsed(DataBaseManager, result, "DMUsed", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockUsedByIndustry(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockUsedByIndustry(DataBaseManager, result, "BlockUsedByIndustry", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectUsedByIndustry(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockInfoByIndustry(DataBaseManager, result, "BlockInfoByIndustry", parameter);
                dao.SelectBlockUsedByIndustry(DataBaseManager, result, "BlockUsedByIndustry", parameter);
                dao.SelectDMInfo(DataBaseManager, result, "DMInfo", parameter);
                dao.SelectDMUsed(DataBaseManager, result, "DMUsed", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockHouseHoldsByIndustry(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockHouseHoldsByIndustry(DataBaseManager, result, "BlockHouseHoldsByIndustry", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectHouseHoldsByIndustry(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockInfoByIndustry(DataBaseManager, result, "BlockInfoByIndustry", parameter);
                dao.SelectBlockHouseHoldsByIndustry(DataBaseManager, result, "BlockHouseHoldsByIndustry", parameter);
                dao.SelectDMInfo(DataBaseManager, result, "DMInfo", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockWaterTap(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockWaterTap(DataBaseManager, result, "BlockWaterTap", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectWaterTap(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockInfo(DataBaseManager, result, "BlockInfo", parameter);
                dao.SelectBlockWaterTap(DataBaseManager, result, "BlockWaterTap", parameter);
                dao.SelectDMInfo(DataBaseManager, result, "DMInfo", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockUsedBigDM(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockUsedBigDM(DataBaseManager, result, "BlockUsedBigDM", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBigDMUsed(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                if (parameter.ContainsKey("IS_BIG_DM"))
                {
                    parameter.Remove("IS_BIG_DM");
                }

                parameter["IS_BIG_DM"] = "Y";

                dao.SelectBlockInfoBigDM(DataBaseManager, result, "BlockInfoBigDM", parameter);
                dao.SelectBlockUsedBigDM(DataBaseManager, result, "BlockUsedBigDM", parameter);
                dao.SelectDMInfoBigDM(DataBaseManager, result, "DMInfoBigDM", parameter);
                dao.SelectDMUsedBigDM(DataBaseManager, result, "DMUsedBigDM", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockMeterChange(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockCountMeterChange(DataBaseManager, result, "BlockMeterChange", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectMeterChange(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockInfo(DataBaseManager, result, "BlockInfo", parameter);
                dao.SelectBlockCountMeterChange(DataBaseManager, result, "BlockMeterChange", parameter);
                dao.SelectDMCountMeterChange(DataBaseManager, result, "DMMeterChange", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectBlockPipeStatus(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockPipeLen(DataBaseManager, result, "BlockPipeLen", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectPipeStatus(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockPipeInfo(DataBaseManager, result, "BlockPipeInfo", parameter);
                dao.SelectBlockPipeLen(DataBaseManager, result, "BlockPipeLen", parameter);
                dao.SelectPipeInfo(DataBaseManager, result, "PipeInfo", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectPipeInfo(Hashtable parameter, IMapControl3 imapControl3)
        {
            DataSet dataSet = new DataSet();

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("LBLOCK");
            dataTable.Columns.Add("MBLOCK");
            dataTable.Columns.Add("SBLOCK");
            dataTable.Columns.Add("FTR_KND"); //상수관로, 급수관로
            dataTable.Columns.Add("FTR_IDN", typeof(int));
            dataTable.Columns.Add("SAA_CDE");
            dataTable.Columns.Add("PIP_DIP", typeof(int));
            dataTable.Columns.Add("PIP_LEN", typeof(decimal));
            dataTable.Columns.Add("PIP_LBL");

            //if (parameter != null)
            //{
            //    IFeatureLayer smallBlockFeatureLayer = default(IFeatureLayer);

            //    DataTable blocks = BlockWork.GetInstance().SelectSmallBlock(parameter, "RESULT").Tables[0];

            //    smallBlockFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(imapControl3, "소블록");

            //    ILayer blockLayer = ArcManager.GetMapLayer(imapControl3, "소블록");

            //    if (blockLayer != null)
            //    {
            //        ILayer pipeSupplyLayer = ArcManager.GetMapLayer(imapControl3, "급수관로");
            //        ILayer pipeLayer = ArcManager.GetMapLayer(imapControl3, "상수관로");


            //        foreach (DataRow block in blocks.Rows)
            //        {
            //            string strWhere = "FTR_IDN = '" + block["SFTRIDN"].ToString() + "'";

            //            ESRI.ArcGIS.Geodatabase.IFeature blockFeature = ArcManager.GetFeature(blockLayer, strWhere);

            //            ESRI.ArcGIS.Geodatabase.IFeatureCursor pipeSupplyCursor = ArcManager.GetSpatialCursor(pipeSupplyLayer, blockFeature.Shape, null);
            //            ESRI.ArcGIS.Geodatabase.IFeatureCursor pipeCursor = ArcManager.GetSpatialCursor(pipeLayer, blockFeature.Shape, null);

            //            ESRI.ArcGIS.Geodatabase.IFeature pipeSupplyFeature = pipeSupplyCursor.NextFeature();
            //            while (pipeSupplyFeature != null)
            //            {
            //                DataRow row = dataTable.NewRow();
            //                row["LBLOCK"] = block["LBLOCK"];
            //                row["MBLOCK"] = block["MBLOCK"];
            //                row["SBLOCK"] = block["SBLOCK"];
            //                row["FTR_KND"] = "급수관로";
            //                row["FTR_IDN"] = pipeSupplyFeature.get_Value(pipeSupplyFeature.Fields.FindField("FTR_IDN"));
            //                row["SAA_CDE"] = pipeSupplyFeature.get_Value(pipeSupplyFeature.Fields.FindField("SAA_CDE")).ToString();
            //                row["PIP_DIP"] = pipeSupplyFeature.get_Value(pipeSupplyFeature.Fields.FindField("PIP_DIP"));
            //                row["PIP_LEN"] = pipeSupplyFeature.get_Value(pipeSupplyFeature.Fields.FindField("PIP_LEN"));
            //                row["PIP_LBL"] = pipeSupplyFeature.get_Value(pipeSupplyFeature.Fields.FindField("PIP_LBL")).ToString();

            //                dataTable.Rows.Add(row);

            //                pipeSupplyFeature = pipeSupplyCursor.NextFeature();
            //            }

            //            ESRI.ArcGIS.Geodatabase.IFeature pipeFeature = pipeCursor.NextFeature();
            //            while (pipeFeature != null)
            //            {
            //                DataRow row = dataTable.NewRow();
            //                row["LBLOCK"] = block["LBLOCK"];
            //                row["MBLOCK"] = block["MBLOCK"];
            //                row["SBLOCK"] = block["SBLOCK"];
            //                row["FTR_KND"] = "상수관로";
            //                row["FTR_IDN"] = pipeFeature.get_Value(pipeFeature.Fields.FindField("FTR_IDN"));
            //                row["SAA_CDE"] = pipeFeature.get_Value(pipeFeature.Fields.FindField("SAA_CDE")).ToString();
            //                row["PIP_DIP"] = pipeFeature.get_Value(pipeFeature.Fields.FindField("PIP_DIP"));
            //                row["PIP_LEN"] = pipeFeature.get_Value(pipeFeature.Fields.FindField("PIP_LEN"));
            //                row["PIP_LBL"] = pipeFeature.get_Value(pipeFeature.Fields.FindField("PIP_LBL")).ToString();

            //                dataTable.Rows.Add(row);

            //                pipeFeature = pipeCursor.NextFeature();
            //            }
            //        }
            //    }
            //    dataTable.TableName = "PipeInfo";
            //    dataSet.Tables.Add(dataTable);
            //}
            return dataSet;
        }

        public void InsertBigUsedDM(Hashtable parameter)
        {
            if (parameter == null && !parameter.ContainsKey("DMNO"))
            {
                MessageBox.Show("올바른 선택 경로가 아닙니다.");
                return;
            }

            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = new DataSet();
                dao.SelectBigUsedDM(DataBaseManager, dataSet, "RESULT", parameter["DMNO"].ToString());

                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("기존에 선정된 수용가 입니다.");
                    return;
                }

                dao.InsertBigUsedDM(DataBaseManager, parameter["DMNO"].ToString());
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            MessageBox.Show("정상적으로 처리 되었습니다.");
        }

        public void DeleteBigUsedDM(Hashtable parameter)
        {
            if (parameter == null && !parameter.ContainsKey("DMNO"))
            {
                MessageBox.Show("올바른 선택 경로가 아닙니다.");
            }
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.DeleteBigUsedDM(DataBaseManager, parameter["DMNO"].ToString());
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            MessageBox.Show("정상적으로 처리 되었습니다.");
        }


        public void UpdateBigUsedDM(DataTable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (DataRow row in parameter.Rows)
                {
                    dao.DeleteBigUsedDM(DataBaseManager, row["DMNO"].ToString());

                    if (row["IS_BIG_DM"].ToString() == "True")
                    {
                        dao.InsertBigUsedDM(DataBaseManager, row["DMNO"].ToString());
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            MessageBox.Show("정상적으로 처리 되었습니다.");
        }

        public void DeleteBigUsedDM(DataTable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (DataRow row in parameter.Rows)
                {
                    if (row["IS_BIG_DM"].ToString() == "True")
                    {
                        dao.DeleteBigUsedDM(DataBaseManager, row["DMNO"].ToString());
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            MessageBox.Show("정상적으로 처리 되었습니다.");
        }
    }
}
