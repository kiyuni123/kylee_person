﻿using System;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_GeneralStatus.work;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using ChartFX.WinForms;
using WaterNet.WV_Common.util;
using System.Collections.Generic;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.interface1;
using Infragistics.Win;
using System.Drawing;
using WaterNet.WV_Common.enum1;
using EMFrame.log;
namespace WaterNet.WV_GeneralStatus.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private List<TableLayout> tableLayouts = null;
        private ChartManager chartManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //동진_2012.08.23
            object o = EMFrame.statics.AppStatic.USER_MENU["블록별일반현황정보조회ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.BigUsedUpdate.Enabled = false;
            }

            this.InitializeValueList();
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeContextMenu();
            this.InitializeEvent();
        }

        /// <summary>
        /// 코드데이터 초기화 설정
        /// </summary>
        private void InitializeValueList()
        {
            Utils.SetValueList(this.UsedDMCombo, VALUELIST_TYPE.CODE, "2008");
            ComboBoxUtils.AddDefaultOption(this.UsedDMCombo, "입력", "X", false);
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.tableLayouts = new List<TableLayout>();
            this.tableLayouts.Add(new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn));
            this.tableLayouts.Add(new TableLayout(tableLayoutPanel2, chartPanel2, tablePanel2, chartVisibleBtn, tableVisibleBtn));
            this.tableLayouts.Add(new TableLayout(tableLayoutPanel3, chartPanel3, tablePanel3, chartVisibleBtn, tableVisibleBtn));
            this.tableLayouts.Add(new TableLayout(tableLayoutPanel4, chartPanel4, tablePanel4, chartVisibleBtn, tableVisibleBtn));
            this.tableLayouts.Add(new TableLayout(tableLayoutPanel5, chartPanel5, tablePanel5, chartVisibleBtn, tableVisibleBtn));
            this.tableLayouts.Add(new TableLayout(tableLayoutPanel6, chartPanel6, tablePanel6, chartVisibleBtn, tableVisibleBtn));
            this.tableLayouts.Add(new TableLayout(tableLayoutPanel7, chartPanel7, tablePanel7, chartVisibleBtn, tableVisibleBtn));

            //사용량 필터링 기본 설정
            this.textBox1.Enabled = false;
            this.UsedDMCombo.SelectedIndex = 10;
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            
            //index  0 : 블록 사용량현황 차트
            //index  1 : 블록 업종별 사용량현황 차트
            //index  2 : 블록 업종별 가구수현황 차트
            //index  3 : 블록 급수전현황 챁,
            //index  4 : 블록 대수용가 사용량현황 차트
            //index  5 : 블록 계량기교체현황 차트
            //index  6 : 블록 관로구분별 연장현황 차트
            this.chartManager.Add(this.chart1);
            this.chartManager.Add(this.chart2);
            this.chartManager.Add(this.chart3);
            this.chartManager.Add(this.chart4);
            this.chartManager.Add(this.chart5);
            this.chartManager.Add(this.chart6);
            this.chartManager.Add(this.chart7);

            foreach (Chart chart in this.chartManager.Items)
            {
                this.chartManager.SetAxisXFormat(chart, "yyyy-MM");
            }
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();

            //index  0 : 블록별 사용량현황 그리드 매니저
            //index  1 : 수용가 사용량현황 그리드 매니저
            //index  2 : 블록별 업종별 사용량현황 그리드 매니저
            //index  3 : 수용가 업종별 사용량현황 그리드 매니저
            //index  4 : 블록별 업종별 가구수현황 그리드 매니저
            //index  5 : 수용가 업종별 가구수현황 그리드 매니저
            //index  6 : 블록별 급수전현황 그리드 매니저
            //index  7 : 수용가 급수전현황 그리드 매니저
            //index  8 : 블록별 대수용가 사용량현황 그리드 매니저
            //index  9 : 대수용가 사용량현황 그리드 매니저
            //index 10 : 블록별 계량기교체현황 그리드 매니저
            //index 11 : 수용가 계량기교체현황 그리드 매니저
            //index 12 : 블록별 관로구분별현황 그리드 매니저
            //index 13 : 관로현황 그리드 매니저

            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);
            this.gridManager.Add(this.ultraGrid3);
            this.gridManager.Add(this.ultraGrid4);
            this.gridManager.Add(this.ultraGrid5);
            this.gridManager.Add(this.ultraGrid6);
            this.gridManager.Add(this.ultraGrid7);
            this.gridManager.Add(this.ultraGrid8);
            this.gridManager.Add(this.ultraGrid9);
            this.gridManager.Add(this.ultraGrid10);
            this.gridManager.Add(this.ultraGrid11);
            this.gridManager.Add(this.ultraGrid12);
            this.gridManager.Add(this.ultraGrid13);
            this.gridManager.Add(this.ultraGrid14);

            this.ultraGrid2.DisplayLayout.Bands[0].Columns["IS_BIG_DM"].CellActivation = Activation.AllowEdit;
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["IS_BIG_DM"].CellClickAction = CellClickAction.Edit;

            this.ultraGrid10.DisplayLayout.Bands[0].Columns["IS_BIG_DM"].CellActivation = Activation.AllowEdit;
            this.ultraGrid10.DisplayLayout.Bands[0].Columns["IS_BIG_DM"].CellClickAction = CellClickAction.Edit;
        }

        /// <summary>
        /// 마우스 우클릭 메뉴를 설정한다.
        /// </summary>
        private void InitializeContextMenu()
        {
            
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            //블록별 현황에서 로우 선택시 차트값을 변동하는 이벤트 핸들러 추가
            this.ultraGrid1.AfterRowActivate += new EventHandler(Grid_AfterRowActivate);
            this.ultraGrid3.AfterRowActivate += new EventHandler(Grid_AfterRowActivate);
            this.ultraGrid5.AfterRowActivate += new EventHandler(Grid_AfterRowActivate);
            this.ultraGrid7.AfterRowActivate += new EventHandler(Grid_AfterRowActivate);
            this.ultraGrid9.AfterRowActivate += new EventHandler(Grid_AfterRowActivate);
            this.ultraGrid11.AfterRowActivate += new EventHandler(Grid_AfterRowActivate);
            this.ultraGrid13.AfterRowActivate += new EventHandler(Grid_AfterRowActivate);

            //더블클릭시에 해당 객체로 이동
            this.ultraGrid2.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid_DoubleClickRow);
            this.ultraGrid4.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid_DoubleClickRow);
            this.ultraGrid6.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid_DoubleClickRow);
            this.ultraGrid8.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid_DoubleClickRow);
            this.ultraGrid10.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid_DoubleClickRow);
            this.ultraGrid12.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid_DoubleClickRow);
            this.ultraGrid14.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid_DoubleClickRow);

            this.UsedDMCombo.SelectedIndexChanged += new EventHandler(UsedDMCombo_SelectedIndexChanged);
            this.UsedFilteringBtn.Click += new EventHandler(UsedFilteringBtn_Click);

            this.ultraGrid2.MouseDown += new MouseEventHandler(ultraGrid_MouseDown);
            this.ultraGrid10.MouseDown += new MouseEventHandler(ultraGrid_MouseDown);

            this.BigUsedUpdate.Click += new EventHandler(BigUsedUpdate_Click);
            this.ultraGrid2.AfterHeaderCheckStateChanged += new AfterHeaderCheckStateChangedEventHandler(ultraGrid2_AfterHeaderCheckStateChanged);
            this.ultraGrid10.AfterHeaderCheckStateChanged += new AfterHeaderCheckStateChangedEventHandler(ultraGrid2_AfterHeaderCheckStateChanged);

            this.BigUsedDelete.Click += new EventHandler(BigUsedDelete_Click);
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            Cursor currentCursor = this.Cursor;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                int index = this.tabControl1.SelectedTab.Index * 2;
                int tabIndex = this.tabControl1.SelectedTab.Index;

                switch (this.tabControl1.SelectedTab.Index)
                {
                    case 0:
                        {
                            this.excelManager.Clear();
                            excelManager.AddWorksheet(this.chartManager.Items[tabIndex], "블록별_사용량_현황", 0, 0, 9, 20);
                            excelManager.AddWorksheet(this.gridManager.Items[index], "블록별_사용량_현황", 10, 0);
                            excelManager.AddWorksheet(this.gridManager.Items[index + 1], "수용가별_사용량_현황");
                            excelManager.Save("일반현황_사용량", true);
                            break;
                        }
                    case 1:
                        {
                            this.excelManager.Clear();
                            excelManager.AddWorksheet(this.chartManager.Items[tabIndex], "블록별_업종별사용량_현황", 0, 0, 9, 20);
                            excelManager.AddWorksheet(this.gridManager.Items[index], "블록별_업종별사용량_현황", 10, 0);
                            excelManager.AddWorksheet(this.gridManager.Items[index + 1], "수용가별_업종별사용량_현황");
                            excelManager.Save("일반현황_업종별사용량", true);
                            break;
                        }
                    case 2:
                        {
                            this.excelManager.Clear();
                            excelManager.AddWorksheet(this.chartManager.Items[tabIndex], "블록별_업종별가구수_현황", 0, 0, 9, 20);
                            excelManager.AddWorksheet(this.gridManager.Items[index], "블록별_업종별가구수_현황", 10, 0);
                            excelManager.AddWorksheet(this.gridManager.Items[index + 1], "수용가별_업종별가구수_현황");
                            excelManager.Save("일반현황_업종별가구수", true);
                            break;
                        }
                    case 3:
                        {
                            this.excelManager.Clear();
                            excelManager.AddWorksheet(this.chartManager.Items[tabIndex], "블록별_급수전_현황", 0, 0, 9, 20);
                            excelManager.AddWorksheet(this.gridManager.Items[index], "블록별_급수전_현황", 10, 0);
                            excelManager.AddWorksheet(this.gridManager.Items[index + 1], "수용가별_급수전_현황");
                            excelManager.Save("일반현황_급수전", true);
                            break;
                        }
                    case 4:
                        {
                            this.excelManager.Clear();
                            excelManager.AddWorksheet(this.chartManager.Items[tabIndex], "블록별_대수용가사용량_현황", 0, 0, 9, 20);
                            excelManager.AddWorksheet(this.gridManager.Items[index], "블록별_대수용가사용량_현황", 10, 0);
                            excelManager.AddWorksheet(this.gridManager.Items[index + 1], "수용가별_대수용가사용량_현황");
                            excelManager.Save("일반현황_대수용가사용량", true);
                            break;
                        }
                    case 5:
                        {
                            this.excelManager.Clear();
                            excelManager.AddWorksheet(this.chartManager.Items[tabIndex], "블록별_계량기교체_현황", 0, 0, 9, 20);
                            excelManager.AddWorksheet(this.gridManager.Items[index], "블록별_계량기교체_현황", 10, 0);
                            excelManager.AddWorksheet(this.gridManager.Items[index + 1], "수용가별_계량기교체_현황");
                            excelManager.Save("일반현황_계량기교체", true);
                            break;
                        }
                    case 6:
                        {
                            this.excelManager.Clear();
                            excelManager.AddWorksheet(this.chartManager.Items[tabIndex], "블록별_관로_현황", 0, 0, 9, 20);
                            excelManager.AddWorksheet(this.gridManager.Items[index], "블록별_관로_현황", 10, 0);
                            excelManager.AddWorksheet(this.gridManager.Items[index + 1], "관로_현황");
                            excelManager.Save("일반현황_관로", true);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = currentCursor;
            }
        }

        /// <summary>
        /// 일반현황 조회 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            Cursor currentCursor = this.Cursor;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectGeneralStatus(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = currentCursor;
            }
        }

        /// <summary>
        /// 일반현황 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectGeneralStatus(Hashtable parameter)
        {
            this.mainMap.MoveToBlock(parameter);

            int index = this.tabControl1.SelectedTab.Index * 2;

            if (this.tabControl1.SelectedTab.Index == 0)
            {
                DataSet result = GeneralStatusWork.GetInstance().SelectUsed(parameter);

                this.gridManager.SetDataSource(this.gridManager.Items[index], result.Tables["BlockInfo"], result.Tables["BlockUsed"], 1);
                this.gridManager.SetDataSource(this.gridManager.Items[index + 1], result.Tables["DMInfo"], result.Tables["DMUsed"], 1);
            }
            else if (this.tabControl1.SelectedTab.Index == 1)
            {
                DataSet result = GeneralStatusWork.GetInstance().SelectUsedByIndustry(parameter);

                this.gridManager.SetDataSource(this.gridManager.Items[index], result.Tables["BlockInfoByIndustry"], result.Tables["BlockUsedByIndustry"], 2);
                this.gridManager.SetDataSource(this.gridManager.Items[index + 1], result.Tables["DMInfo"], result.Tables["DMUsed"], 1);
            }
            else if (this.tabControl1.SelectedTab.Index == 2)
            {
                DataSet result = GeneralStatusWork.GetInstance().SelectHouseHoldsByIndustry(parameter);

                this.gridManager.SetDataSource(this.gridManager.Items[index], result.Tables["BlockInfoByIndustry"], result.Tables["BlockHouseHoldsByIndustry"], 2);
                this.gridManager.Items[index + 1].DataSource = result.Tables["DMInfo"];
            }
            else if (this.tabControl1.SelectedTab.Index == 3)
            {
                DataSet result = GeneralStatusWork.GetInstance().SelectWaterTap(parameter);

                this.gridManager.SetDataSource(this.gridManager.Items[index], result.Tables["BlockInfo"], result.Tables["BlockWaterTap"], 1);
                this.gridManager.Items[index + 1].DataSource = result.Tables["DMInfo"];
            }
            else if (this.tabControl1.SelectedTab.Index == 4)
            {
                DataSet result = GeneralStatusWork.GetInstance().SelectBigDMUsed(parameter);

                this.gridManager.SetDataSource(this.gridManager.Items[index], result.Tables["BlockInfoBigDM"], result.Tables["BlockUsedBigDM"], 1);
                this.gridManager.SetDataSource(this.gridManager.Items[index + 1], result.Tables["DMInfoBigDM"], result.Tables["DMUsedBigDM"], 1);
            }
            else if (this.tabControl1.SelectedTab.Index == 5)
            {
                DataSet result = GeneralStatusWork.GetInstance().SelectMeterChange(parameter);

                this.gridManager.SetDataSource(this.gridManager.Items[index], result.Tables["BlockInfo"], result.Tables["BlockMeterChange"], 1);
                this.gridManager.Items[index + 1].DataSource = result.Tables["DMMeterChange"];
            }
            else if (this.tabControl1.SelectedTab.Index == 6)
            {
                DataSet result = GeneralStatusWork.GetInstance().SelectPipeStatus(parameter);

                this.gridManager.SetDataSource(this.gridManager.Items[index], result.Tables["BlockPipeInfo"], result.Tables["BlockPipeLen"], 2);
                this.gridManager.Items[index + 1].DataSource = result.Tables["PipeInfo"];
            }
        }

        /// <summary>
        /// 블럭현황 행선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_AfterRowActivate(object sender, EventArgs e)
        {
            Cursor currentCursor = this.Cursor;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                UltraGridRow activeRow = ((UltraGrid)sender).ActiveRow;

                Hashtable parameter = this.searchBox1.Parameters;
                parameter["LOC_CODE"] = activeRow.Cells["LOC_CODE"].Text;
                parameter["CHART"] = "Y";

                this.InitializeChartSetting(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = currentCursor;
            }
        }

        /// <summary>
        /// GIS객체이동 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            Cursor currentCursor = this.Cursor;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                //this.ultraGrid14인경우만, 관로로 이동한다 그외에는 수용가.
                UltraGrid ultraGrid = (UltraGrid)sender;

                string layer_name = string.Empty;
                string strWhere = string.Empty;

                if (ultraGrid.Equals(this.ultraGrid14))
                {
                    if (e.Row.Cells.IndexOf("FTR_KND") == -1 || e.Row.Cells.IndexOf("FTR_IDN") == -1)
                    {
                        return;
                    }

                    layer_name = e.Row.Cells["FTR_KND"].Value.ToString();
                    strWhere = "FTR_IDN = '" + e.Row.Cells["FTR_IDN"].Value.ToString() + "'";
                }
                else
                {
                    layer_name = "수도계량기";
                    if (e.Row.Cells.IndexOf("META_IDN") == -1)
                    {
                        return;
                    }
                    strWhere = "FTR_IDN = '" + e.Row.Cells["META_IDN"].Value.ToString() + "'";
                }

                mainMap.MoveFocusAt(layer_name, strWhere, 1000);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = currentCursor;
            }
        }

        /// <summary>
        /// 차트 세팅 초기화
        /// </summary>
        /// <param name="parameter"></param>
        private void InitializeChartSetting(Hashtable parameter)
        {
            Chart chart = this.chartManager.Items[this.tabControl1.SelectedTab.Index];
            this.chartManager.AllClear(chart);

            DataTable table = null;

            if (this.tabControl1.SelectedTab.Index == 0)
            {
                table = GeneralStatusWork.GetInstance().SelectBlockUsed(parameter).Tables[0];

                if (table == null)
                {
                    return;
                }

                chart.Data.Series = 1;
                chart.Data.Points = table.Rows.Count;

                foreach (DataRow row in table.Rows)
                {
                    int pointIndex = table.Rows.IndexOf(row);
                    chart.AxisX.Labels[pointIndex] = row["YEAR_MON"].ToString();
                    chart.Data[0, pointIndex] = Utils.ToDouble(row["AMTUSE"]);
                }
                chart.Series[0].Text = "사용량(㎥/월)";
                chart.Series[0].MarkerSize = 2;
                chart.Series[0].Line.Width = 2;
                chart.AxesY[0].Title.Text = "사용량(㎥/월)";
            }
            else if (this.tabControl1.SelectedTab.Index == 1)
            {
                table = GeneralStatusWork.GetInstance().SelectBlockUsedByIndustry(parameter).Tables[0];

                if (table == null)
                {
                    return;
                }

                chart.Data.Series = 2;
                chart.Data.Points = table.Rows.Count;

                foreach (DataRow row in table.Rows)
                {
                    int pointIndex = table.Rows.IndexOf(row);
                    chart.AxisX.Labels[pointIndex] = row["YEAR_MON"].ToString();
                    chart.Data[0, pointIndex] = Utils.ToDouble(row["GAJUNG_AMTUSE"]);
                    chart.Data[1, pointIndex] = Utils.ToDouble(row["BGAJUNG_AMTUSE"]);
                }
                chart.Series[0].Text = "가정 사용량(㎥/월)";
                chart.Series[1].Text = "비가정 사용량(㎥/월)";
                chart.Series[0].MarkerSize = 2;
                chart.Series[0].Line.Width = 2;
                chart.Series[1].MarkerSize = 2;
                chart.Series[1].Line.Width = 2;
                chart.AxesY[0].Title.Text = "사용량(㎥/월)";
            }
            else if (this.tabControl1.SelectedTab.Index == 2)
            {
                table = GeneralStatusWork.GetInstance().SelectBlockHouseHoldsByIndustry(parameter).Tables[0];

                if (table == null)
                {
                    return;
                }

                chart.Data.Series = 2;
                chart.Data.Points = table.Rows.Count;

                foreach (DataRow row in table.Rows)
                {
                    int pointIndex = table.Rows.IndexOf(row);
                    chart.AxisX.Labels[pointIndex] = row["DATEE"].ToString();
                    chart.Data[0, pointIndex] = Utils.ToDouble(row["GAJUNG_GAGUSU"]);
                    chart.Data[1, pointIndex] = Utils.ToDouble(row["BGAJUNG_GAGUSU"]);
                }
                chart.Series[0].Text = "가정 가구수(가구수)";
                chart.Series[1].Text = "비가정 가구수(가구수)";
                chart.Series[0].MarkerSize = 2;
                chart.Series[0].Line.Width = 2;
                chart.Series[1].MarkerSize = 2;
                chart.Series[1].Line.Width = 2;
                chart.AxesY[0].Title.Text = "가구수";
            }
            else if (this.tabControl1.SelectedTab.Index == 3)
            {
                table = GeneralStatusWork.GetInstance().SelectBlockWaterTap(parameter).Tables[0];

                if (table == null)
                {
                    return;
                }

                chart.Data.Series = 1;
                chart.Data.Points = table.Rows.Count;

                foreach (DataRow row in table.Rows)
                {
                    int pointIndex = table.Rows.IndexOf(row);
                    chart.AxisX.Labels[pointIndex] = row["DATEE"].ToString();
                    chart.Data[0, pointIndex] = Utils.ToDouble(row["GUPSUJUNSU"]);
                }
                chart.Series[0].Text = "급수전수(전수)";
                chart.Series[0].MarkerSize = 2;
                chart.Series[0].Line.Width = 2;
                chart.AxesY[0].Title.Text = "급수전수(전수)";
            }
            else if (this.tabControl1.SelectedTab.Index == 4)
            {
                table = GeneralStatusWork.GetInstance().SelectBlockUsedBigDM(parameter).Tables[0];

                if (table == null)
                {
                    return;
                }

                chart.Data.Series = 1;
                chart.Data.Points = table.Rows.Count;

                foreach (DataRow row in table.Rows)
                {
                    int pointIndex = table.Rows.IndexOf(row);
                    chart.AxisX.Labels[pointIndex] = row["YEAR_MON"].ToString();
                    chart.Data[0, pointIndex] = Utils.ToDouble(row["AMTUSE"]);
                }
                chart.Series[0].Text = "대수용가 사용량(㎥/월)";
                chart.Series[0].MarkerSize = 2;
                chart.Series[0].Line.Width = 2;
                chart.AxesY[0].Title.Text = "사용량(㎥/월)";
            }
            else if (this.tabControl1.SelectedTab.Index == 5)
            {
                table = GeneralStatusWork.GetInstance().SelectBlockMeterChange(parameter).Tables[0];

                if (table == null)
                {
                    return;
                }

                chart.Data.Series = 1;
                chart.Data.Points = table.Rows.Count;

                foreach (DataRow row in table.Rows)
                {
                    int pointIndex = table.Rows.IndexOf(row);
                    chart.AxisX.Labels[pointIndex] = row["DATEE"].ToString();
                    chart.Data[0, pointIndex] = Utils.ToDouble(row["MTRCHGSU"]);
                }
                chart.Series[0].Text = "계량기교체건수";
                chart.Series[0].MarkerSize = 2;
                chart.Series[0].Line.Width = 2;
                chart.AxesY[0].Title.Text = "교체건수";
            }
            else if (this.tabControl1.SelectedTab.Index == 6)
            {
                table = GeneralStatusWork.GetInstance().SelectBlockPipeStatus(parameter).Tables[0];

                if (table == null)
                {
                    return;
                }

                chart.Data.Series = 2;
                chart.Data.Points = table.Rows.Count;

                foreach (DataRow row in table.Rows)
                {
                    int pointIndex = table.Rows.IndexOf(row);
                    chart.AxisX.Labels[pointIndex] = row["YEAR_MON"].ToString();
                    chart.Data[0, pointIndex] = Utils.ToDouble(row["SPLY_LEN"]);
                    chart.Data[1, pointIndex] = Utils.ToDouble(row["PIPE_LEN"]);
                }
                chart.Series[0].Text = "급수관로";
                chart.Series[1].Text = "상수관로";
                chart.Series[0].MarkerSize = 2;
                chart.Series[1].MarkerSize = 2;
                chart.Series[0].Line.Width = 2;
                chart.Series[1].Line.Width = 2;
                chart.AxesY[0].Title.Text = "관로길이(m)";
            }
            this.chartManager.AllShowSeries(chart);
        }

        /// <summary>
        /// 사용량 필터링 기준 콤보박스 값변경 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsedDMCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combobox = sender as ComboBox;

            if (combobox != null)
            {
                if (combobox.SelectedValue.ToString() == "X")
                {
                    this.textBox1.Enabled = true;
                }
                else
                {
                    this.textBox1.Enabled = false;
                }
            }
        }

        /// <summary>
        /// 사용량 필터링 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsedFilteringBtn_Click(object sender, EventArgs e)
        {
            Cursor currentCursor = this.Cursor;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                double value = 0;

                if (this.textBox1.Enabled)
                {
                    value = ControlUtils.GetDoubleValue(this.textBox1);
                }
                else
                {
                    value = ControlUtils.GetDoubleValue(this.UsedDMCombo);
                }

                UltraGrid grid = this.gridManager.Items[1];

                //칼럼을 돌면서 사용량을 체크해야한다.

                List<int> columnIndex = new List<int>();
                ColumnsCollection columns = grid.DisplayLayout.Bands[0].Columns;
                for (int i = 0; i < columns.Count; i++)
                {
                    if (!columns[i].IsBound)
                    {
                        columnIndex.Add(i);
                    }
                }

                foreach (UltraGridRow row in grid.Rows)
                {
                    bool isOverUsed = false;
                    foreach (int i in columnIndex)
                    {
                        if (Convert.ToDouble(row.Cells[i].Value) >= value)
                        {
                            isOverUsed = true;
                            break;
                        }
                    }

                    if (isOverUsed)
                    {
                        row.Hidden = false;
                    }
                    else
                    {
                        row.Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = currentCursor;
            }
        }

        /// <summary>
        /// 마우스 우클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            UltraGrid ultraGrid = (UltraGrid)sender;
            UIElement element = ultraGrid.DisplayLayout.UIElement.ElementFromPoint(new System.Drawing.Point(e.X, e.Y));
            UltraGridCell cell = element.GetContext(typeof(UltraGridCell)) as UltraGridCell;

            if (cell != null)
            {
                ContextMenu menu = new ContextMenu();
                if (ultraGrid.Equals(this.ultraGrid2))
                {
                    menu.MenuItems.Add(new MenuItem("대수용가 선정", InsertBigUsedDM));
                }
                else if (ultraGrid.Equals(this.ultraGrid10))
                {
                    menu.MenuItems.Add(new MenuItem("대수용가 제외", DeleteBigUsedDM));
                }

                if (ultraGrid.ActiveRow != null)
                {
                    ultraGrid.ActiveRow.Activated = false;
                }

                ultraGrid.Selected.Rows.Clear();
                ultraGrid.ActiveRow = cell.Row;
                cell.Row.Selected = true;
                
                Point mousePoint = new Point(e.X, e.Y);
                menu.Show(ultraGrid, mousePoint);
            }
        }

        /// <summary>
        /// 대수용가 선정 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertBigUsedDM(object sender, EventArgs e)
        {
            UltraGridRow row = this.ultraGrid2.ActiveRow;

            Hashtable parameter = new Hashtable();
            parameter["DMNO"] = row.Cells["DMNO"].Value.ToString();

            GeneralStatusWork.GetInstance().InsertBigUsedDM(parameter);
        }

        /// <summary>
        /// 대수용가 제외 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteBigUsedDM(object sender, EventArgs e)
        {
            UltraGridRow row = this.ultraGrid10.ActiveRow;

            Hashtable parameter = new Hashtable();
            parameter["DMNO"] = row.Cells["DMNO"].Value.ToString();
            GeneralStatusWork.GetInstance().DeleteBigUsedDM(parameter);
            SelectGeneralStatus(this.searchBox1.Parameters);
        }

        private void BigUsedUpdate_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid2.Rows.Count == 0)
            {
                return;
            }

            GeneralStatusWork.GetInstance().UpdateBigUsedDM((DataTable)this.ultraGrid2.DataSource);
        }


        private void ultraGrid2_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;

            bool result = false;

            if (e.Column.GetHeaderCheckedState(e.Rows) == CheckState.Checked)
            {
                result = true;
            }

            foreach (UltraGridRow row in ultraGrid.Rows)
            {
                if (!row.Hidden)
                {
                    row.Cells["IS_BIG_DM"].Value = result;
                }
            }

            ultraGrid.UpdateData();
        }


        private void BigUsedDelete_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid10.Rows.Count == 0)
            {
                return;
            }

            GeneralStatusWork.GetInstance().DeleteBigUsedDM((DataTable)this.ultraGrid10.DataSource);

            this.SelectGeneralStatus(this.searchBox1.Parameters);
        }

    }
}
