﻿namespace WaterNet.WV_GeneralStatus.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ChartFX.WinForms.Adornments.SolidBackground solidBackground1 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn160 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn161 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn162 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn163 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNSU");
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn164 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUNSU");
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn165 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUNSU");
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn166 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUNSU");
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn167 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CHANGEJUNSU");
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn209 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAGUSU");
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IS_BIG_DM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn211 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn212 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn213 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn214 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMCLASS");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn215 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn216 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn217 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HYDRNTNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("META_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn218 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UMDCD");
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn219 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NRDADDR");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn220 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSREPBIZKNDCD");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn221 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSPIPESZCD");
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn222 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUN");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn223 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUN");
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn224 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUN");
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn225 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDDT");
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn226 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSNOHSHD");
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand3 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn302 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CENTER");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn303 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn304 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn305 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn306 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNSU");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn307 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUNSU");
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn308 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUNSU");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn309 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUNSU");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn310 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CHANGEJUNSU");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn311 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAGUSU");
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn312 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UPJONG");
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand4 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn228 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn229 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn230 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn231 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMCLASS");
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn232 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn233 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn234 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HYDRNTNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("META_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn235 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UMDCD");
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn236 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NRDADDR");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn237 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSREPBIZKNDCD");
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn238 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSPIPESZCD");
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn239 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUN");
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn240 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUN");
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn241 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUN");
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn242 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDDT");
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn243 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSNOHSHD");
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground2 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance220 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand5 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn147 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CENTER");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn149 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn150 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn151 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn152 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNSU");
            Infragistics.Win.Appearance appearance221 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn153 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUNSU");
            Infragistics.Win.Appearance appearance222 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn154 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUNSU");
            Infragistics.Win.Appearance appearance223 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn155 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUNSU");
            Infragistics.Win.Appearance appearance224 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn156 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CHANGEJUNSU");
            Infragistics.Win.Appearance appearance225 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn157 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAGUSU");
            Infragistics.Win.Appearance appearance226 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn300 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UPJONG");
            Infragistics.Win.Appearance appearance227 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance228 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance229 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance230 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance231 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance232 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance233 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance234 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance235 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance236 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance237 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance238 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance239 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand6 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn256 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn257 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn258 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn259 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMCLASS");
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn260 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn261 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn262 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HYDRNTNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("META_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn263 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UMDCD");
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn264 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NRDADDR");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn265 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSREPBIZKNDCD");
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn266 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSPIPESZCD");
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn267 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUN");
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn268 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUN");
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn269 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUN");
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn270 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDDT");
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn271 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSNOHSHD");
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance198 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance199 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground3 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance240 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand7 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn274 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn275 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn276 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn277 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNSU");
            Infragistics.Win.Appearance appearance241 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn278 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUNSU");
            Infragistics.Win.Appearance appearance242 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn279 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUNSU");
            Infragistics.Win.Appearance appearance243 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn280 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUNSU");
            Infragistics.Win.Appearance appearance244 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn281 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CHANGEJUNSU");
            Infragistics.Win.Appearance appearance245 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn282 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAGUSU");
            Infragistics.Win.Appearance appearance246 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance247 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance248 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance249 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance250 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance251 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance252 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance253 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance254 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance255 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance265 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance266 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance267 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance200 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand8 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn284 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn285 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn286 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn287 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMCLASS");
            Infragistics.Win.Appearance appearance201 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn288 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn289 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn290 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HYDRNTNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("META_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn291 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UMDCD");
            Infragistics.Win.Appearance appearance202 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn292 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NRDADDR");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn293 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSREPBIZKNDCD");
            Infragistics.Win.Appearance appearance203 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn294 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSPIPESZCD");
            Infragistics.Win.Appearance appearance204 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn295 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUN");
            Infragistics.Win.Appearance appearance205 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn296 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUN");
            Infragistics.Win.Appearance appearance206 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn297 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUN");
            Infragistics.Win.Appearance appearance207 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn298 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDDT");
            Infragistics.Win.Appearance appearance208 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn299 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSNOHSHD");
            Infragistics.Win.Appearance appearance209 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance210 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance211 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance212 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance213 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance214 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance215 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance216 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance217 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance256 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance257 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance258 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance259 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground4 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance433 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand9 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNSU");
            Infragistics.Win.Appearance appearance434 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn102 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAGUSU");
            Infragistics.Win.Appearance appearance435 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance436 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance437 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance438 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance439 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance440 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance441 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance442 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance443 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance444 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance445 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance446 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance447 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand10 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IS_BIG_DM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CENTER");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn110 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn111 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn112 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn113 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMCLASS");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn114 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn115 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn116 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HYDRNTNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("META_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn117 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UMDCD");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn118 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NRDADDR");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn119 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSREPBIZKNDCD");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn120 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSPIPESZCD");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn130 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUN");
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn131 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUN");
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn132 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUN");
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn133 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDDT");
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn134 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSNOHSHD");
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground5 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance448 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand11 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn201 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn202 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn203 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn204 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNSU");
            Infragistics.Win.Appearance appearance449 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn205 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GEJUNSU");
            Infragistics.Win.Appearance appearance450 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance451 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn206 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PEJUNSU");
            Infragistics.Win.Appearance appearance452 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance453 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn207 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("STOPJUNSU");
            Infragistics.Win.Appearance appearance454 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance455 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn208 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CHANGEJUNSU");
            Infragistics.Win.Appearance appearance456 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance457 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn85 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAGUSU");
            Infragistics.Win.Appearance appearance458 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance459 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance460 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance461 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance462 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance463 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance464 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance465 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance466 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance467 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance468 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance469 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance470 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance471 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand12 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn27 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMCLASS");
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn31 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn32 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HYDRNTNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn37 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("META_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UMDCD");
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NRDADDR");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn77 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSREPBIZKNDCD");
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn78 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TRANSCLASS");
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn79 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDDT");
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn80 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OMFRNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn81 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OWSPIPESZCD");
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn82 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSRMVNDL");
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn83 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NMFRNO");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn84 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NWSPIPESZCD");
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn127 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSATTNDL");
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn148 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MRYM");
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn128 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TRANSRS");
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance191 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance192 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance193 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance194 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance195 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance196 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance197 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance218 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance219 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance260 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance261 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground6 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand13 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn22 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn169 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn170 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn171 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIP_KND");
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand14 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn23 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn175 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn176 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn185 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FTR_KND");
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FTR_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SAA_CDE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn24 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MOP_CDE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIP_DIP");
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIP_LEN");
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground7 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab11 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab12 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab14 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabContent1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chartPanel1 = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.tablePanel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.ultraGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel20 = new System.Windows.Forms.Panel();
            this.BigUsedUpdate = new System.Windows.Forms.Button();
            this.UsedFilteringBtn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UsedDMCombo = new System.Windows.Forms.ComboBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabContent2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.ultraGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.ultraGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.chartPanel2 = new System.Windows.Forms.Panel();
            this.chart2 = new ChartFX.WinForms.Chart();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabContent3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.ultraGrid5 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.ultraGrid6 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.chartPanel3 = new System.Windows.Forms.Panel();
            this.chart3 = new ChartFX.WinForms.Chart();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabContent4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.ultraGrid7 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.ultraGrid8 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel29 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.chartPanel4 = new System.Windows.Forms.Panel();
            this.chart4 = new ChartFX.WinForms.Chart();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabContent5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel55 = new System.Windows.Forms.TableLayoutPanel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.ultraGrid9 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel32 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.ultraGrid10 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel34 = new System.Windows.Forms.Panel();
            this.BigUsedDelete = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.chartPanel5 = new System.Windows.Forms.Panel();
            this.chart5 = new ChartFX.WinForms.Chart();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabContent6 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel6 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel66 = new System.Windows.Forms.TableLayoutPanel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.ultraGrid11 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel37 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.ultraGrid12 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel39 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.chartPanel6 = new System.Windows.Forms.Panel();
            this.chart6 = new ChartFX.WinForms.Chart();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabContent7 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel7 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel77 = new System.Windows.Forms.TableLayoutPanel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.ultraGrid13 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.ultraGrid14 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel44 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.chartPanel7 = new System.Windows.Forms.Panel();
            this.chart7 = new ChartFX.WinForms.Chart();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chartVisibleBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.tableVisibleBtn = new System.Windows.Forms.Button();
            this.tabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            this.ultraTabPageControl1.SuspendLayout();
            this.tabContent1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.chartPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tablePanel1.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.panel19.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.tabContent2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tablePanel2.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid3)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid4)).BeginInit();
            this.panel16.SuspendLayout();
            this.chartPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            this.tabContent3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tablePanel3.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid5)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid6)).BeginInit();
            this.panel24.SuspendLayout();
            this.chartPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            this.tabContent4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tablePanel4.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid7)).BeginInit();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid8)).BeginInit();
            this.panel29.SuspendLayout();
            this.chartPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            this.ultraTabPageControl5.SuspendLayout();
            this.tabContent5.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tablePanel5.SuspendLayout();
            this.tableLayoutPanel55.SuspendLayout();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid9)).BeginInit();
            this.panel32.SuspendLayout();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid10)).BeginInit();
            this.panel34.SuspendLayout();
            this.chartPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            this.ultraTabPageControl6.SuspendLayout();
            this.tabContent6.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tablePanel6.SuspendLayout();
            this.tableLayoutPanel66.SuspendLayout();
            this.panel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid11)).BeginInit();
            this.panel37.SuspendLayout();
            this.panel38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid12)).BeginInit();
            this.panel39.SuspendLayout();
            this.chartPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            this.ultraTabPageControl7.SuspendLayout();
            this.tabContent7.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tablePanel7.SuspendLayout();
            this.tableLayoutPanel77.SuspendLayout();
            this.panel41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid13)).BeginInit();
            this.panel42.SuspendLayout();
            this.panel43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid14)).BeginInit();
            this.panel44.SuspendLayout();
            this.chartPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.tabContent1);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox9);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox8);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox7);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(2, 24);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1050, 436);
            // 
            // tabContent1
            // 
            this.tabContent1.Controls.Add(this.tableLayoutPanel1);
            this.tabContent1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContent1.Location = new System.Drawing.Point(10, 10);
            this.tabContent1.Name = "tabContent1";
            this.tabContent1.Size = new System.Drawing.Size(1030, 416);
            this.tabContent1.TabIndex = 10;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.chartPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tablePanel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1030, 416);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // chartPanel1
            // 
            this.chartPanel1.BackColor = System.Drawing.Color.White;
            this.chartPanel1.Controls.Add(this.chart1);
            this.chartPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel1.Location = new System.Drawing.Point(0, 0);
            this.chartPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.chartPanel1.Name = "chartPanel1";
            this.chartPanel1.Size = new System.Drawing.Size(1030, 166);
            this.chartPanel1.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart1.AllSeries.Line.Width = ((short)(1));
            this.chart1.AllSeries.MarkerSize = ((short)(1));
            this.chart1.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart1.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart1.AxisY.Title.Text = "";
            solidBackground1.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart1.Background = solidBackground1;
            this.chart1.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart1.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart1.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart1.LegendBox.PlotAreaOnly = false;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.MainPane.Title.Text = "";
            this.chart1.Name = "chart1";
            this.chart1.RandomData.Series = 1;
            this.chart1.Size = new System.Drawing.Size(1030, 166);
            this.chart1.TabIndex = 6;
            this.chart1.TabStop = false;
            // 
            // tablePanel1
            // 
            this.tablePanel1.BackColor = System.Drawing.Color.White;
            this.tablePanel1.Controls.Add(this.tableLayoutPanel11);
            this.tablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel1.Location = new System.Drawing.Point(0, 166);
            this.tablePanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Size = new System.Drawing.Size(1030, 250);
            this.tablePanel1.TabIndex = 1;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.panel17, 0, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1030, 250);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.ultraGrid1);
            this.panel7.Controls.Add(this.panel19);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1030, 100);
            this.panel7.TabIndex = 0;
            // 
            // ultraGrid1
            // 
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance82;
            ultraGridColumn15.Header.Caption = "지역관리번호";
            ultraGridColumn15.Header.VisiblePosition = 0;
            ultraGridColumn15.Hidden = true;
            ultraGridColumn160.Header.Caption = "대블록";
            ultraGridColumn160.Header.VisiblePosition = 1;
            ultraGridColumn160.Width = 60;
            ultraGridColumn161.Header.Caption = "중블록";
            ultraGridColumn161.Header.VisiblePosition = 2;
            ultraGridColumn161.Width = 60;
            ultraGridColumn162.Header.Caption = "소블록";
            ultraGridColumn162.Header.VisiblePosition = 3;
            ultraGridColumn162.Width = 60;
            appearance83.TextHAlignAsString = "Right";
            ultraGridColumn163.CellAppearance = appearance83;
            ultraGridColumn163.DefaultCellValue = "0";
            ultraGridColumn163.Format = "###,###,###";
            ultraGridColumn163.Header.Caption = "전수";
            ultraGridColumn163.Header.VisiblePosition = 4;
            ultraGridColumn163.Width = 60;
            appearance106.TextHAlignAsString = "Right";
            ultraGridColumn164.CellAppearance = appearance106;
            ultraGridColumn164.DefaultCellValue = "0";
            ultraGridColumn164.Format = "###,###,###";
            ultraGridColumn164.Header.Caption = "개전수";
            ultraGridColumn164.Header.VisiblePosition = 5;
            ultraGridColumn164.Width = 60;
            appearance107.TextHAlignAsString = "Right";
            ultraGridColumn165.CellAppearance = appearance107;
            ultraGridColumn165.DefaultCellValue = "0";
            ultraGridColumn165.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn165.Format = "###,###,###";
            ultraGridColumn165.Header.Caption = "폐전수";
            ultraGridColumn165.Header.VisiblePosition = 6;
            ultraGridColumn165.Width = 60;
            appearance129.TextHAlignAsString = "Right";
            ultraGridColumn166.CellAppearance = appearance129;
            ultraGridColumn166.DefaultCellValue = "0";
            ultraGridColumn166.Format = "###,###,###";
            ultraGridColumn166.Header.Caption = "중지전수";
            ultraGridColumn166.Header.VisiblePosition = 7;
            ultraGridColumn166.Width = 60;
            appearance130.TextHAlignAsString = "Right";
            ultraGridColumn167.CellAppearance = appearance130;
            ultraGridColumn167.DefaultCellValue = "0";
            ultraGridColumn167.Format = "###,###,###";
            ultraGridColumn167.Header.Caption = "교체전수";
            ultraGridColumn167.Header.VisiblePosition = 8;
            ultraGridColumn167.Width = 60;
            appearance131.TextHAlignAsString = "Right";
            ultraGridColumn209.CellAppearance = appearance131;
            ultraGridColumn209.DefaultCellValue = "0";
            ultraGridColumn209.Format = "###,###,###";
            ultraGridColumn209.Header.Caption = "가구수";
            ultraGridColumn209.Header.VisiblePosition = 9;
            ultraGridColumn209.Width = 60;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn15,
            ultraGridColumn160,
            ultraGridColumn161,
            ultraGridColumn162,
            ultraGridColumn163,
            ultraGridColumn164,
            ultraGridColumn165,
            ultraGridColumn166,
            ultraGridColumn167,
            ultraGridColumn209});
            appearance132.TextHAlignAsString = "Center";
            ultraGridBand1.Header.Appearance = appearance132;
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance133.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance133.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance133.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance133.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance133;
            appearance134.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance134;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance149.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance149.BackColor2 = System.Drawing.SystemColors.Control;
            appearance149.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance149.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance149;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance150.BackColor = System.Drawing.SystemColors.Window;
            appearance150.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance150;
            appearance151.BackColor = System.Drawing.SystemColors.Highlight;
            appearance151.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance151;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance152.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance152;
            appearance153.BorderColor = System.Drawing.Color.Silver;
            appearance153.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance153;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance159.BackColor = System.Drawing.SystemColors.Control;
            appearance159.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance159.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance159.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance159.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance159;
            appearance160.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance160;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance161.BackColor = System.Drawing.SystemColors.Window;
            appearance161.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance161;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance162.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance162;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1030, 74);
            this.ultraGrid1.TabIndex = 5;
            this.ultraGrid1.TabStop = false;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.White;
            this.panel19.Controls.Add(this.label8);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1030, 26);
            this.panel19.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Tan;
            this.label8.Location = new System.Drawing.Point(0, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "블록별 현황       ";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.ultraGrid2);
            this.panel17.Controls.Add(this.panel20);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 100);
            this.panel17.Margin = new System.Windows.Forms.Padding(0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(1030, 150);
            this.panel17.TabIndex = 1;
            // 
            // ultraGrid2
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid2.DisplayLayout.Appearance = appearance1;
            ultraGridColumn1.Header.Caption = "";
            ultraGridColumn1.Header.CheckBoxAlignment = Infragistics.Win.UltraWinGrid.HeaderCheckBoxAlignment.Center;
            ultraGridColumn1.Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
            ultraGridColumn1.Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Always;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn1.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn1.Width = 42;
            ultraGridColumn211.Header.Caption = "대블록";
            ultraGridColumn211.Header.VisiblePosition = 4;
            ultraGridColumn211.Width = 60;
            ultraGridColumn212.Header.Caption = "중블록";
            ultraGridColumn212.Header.VisiblePosition = 5;
            ultraGridColumn212.Width = 60;
            ultraGridColumn213.Header.Caption = "소블록";
            ultraGridColumn213.Header.VisiblePosition = 6;
            ultraGridColumn213.Width = 60;
            ultraGridColumn214.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance3.TextHAlignAsString = "Center";
            ultraGridColumn214.CellAppearance = appearance3;
            ultraGridColumn214.Header.Caption = "주/부";
            ultraGridColumn214.Header.VisiblePosition = 1;
            ultraGridColumn214.Width = 54;
            ultraGridColumn215.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn215.Header.Caption = "수용가번호";
            ultraGridColumn215.Header.VisiblePosition = 2;
            ultraGridColumn215.Width = 100;
            ultraGridColumn216.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn216.Header.Caption = "수용가명";
            ultraGridColumn216.Header.VisiblePosition = 3;
            ultraGridColumn216.Width = 150;
            ultraGridColumn217.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn217.Header.Caption = "수전번호";
            ultraGridColumn217.Header.VisiblePosition = 7;
            ultraGridColumn217.Width = 80;
            ultraGridColumn11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn11.Header.Caption = "계량기관리번호";
            ultraGridColumn11.Header.VisiblePosition = 8;
            ultraGridColumn11.Width = 115;
            appearance35.TextHAlignAsString = "Center";
            ultraGridColumn218.CellAppearance = appearance35;
            ultraGridColumn218.Header.Caption = "읍면동";
            ultraGridColumn218.Header.VisiblePosition = 9;
            ultraGridColumn218.Width = 60;
            ultraGridColumn219.Header.Caption = "주소";
            ultraGridColumn219.Header.VisiblePosition = 10;
            ultraGridColumn219.Width = 150;
            appearance36.TextHAlignAsString = "Center";
            ultraGridColumn220.CellAppearance = appearance36;
            ultraGridColumn220.Header.Caption = "업종";
            ultraGridColumn220.Header.VisiblePosition = 11;
            ultraGridColumn220.Width = 60;
            appearance37.TextHAlignAsString = "Right";
            ultraGridColumn221.CellAppearance = appearance37;
            ultraGridColumn221.Format = "###,###,###";
            ultraGridColumn221.Header.Caption = "구경";
            ultraGridColumn221.Header.VisiblePosition = 12;
            ultraGridColumn221.Width = 60;
            appearance38.TextHAlignAsString = "Center";
            ultraGridColumn222.CellAppearance = appearance38;
            ultraGridColumn222.Header.Caption = "개전일자";
            ultraGridColumn222.Header.VisiblePosition = 13;
            ultraGridColumn222.Width = 80;
            appearance39.TextHAlignAsString = "Center";
            ultraGridColumn223.CellAppearance = appearance39;
            ultraGridColumn223.Header.Caption = "폐전일자";
            ultraGridColumn223.Header.VisiblePosition = 15;
            ultraGridColumn223.Width = 80;
            appearance40.TextHAlignAsString = "Center";
            ultraGridColumn224.CellAppearance = appearance40;
            ultraGridColumn224.Header.Caption = "중지일자";
            ultraGridColumn224.Header.VisiblePosition = 16;
            ultraGridColumn224.Width = 80;
            appearance41.TextHAlignAsString = "Center";
            ultraGridColumn225.CellAppearance = appearance41;
            ultraGridColumn225.Header.Caption = "교체일자";
            ultraGridColumn225.Header.VisiblePosition = 17;
            ultraGridColumn225.Width = 80;
            appearance43.TextHAlignAsString = "Right";
            ultraGridColumn226.CellAppearance = appearance43;
            ultraGridColumn226.DefaultCellValue = "0";
            ultraGridColumn226.Format = "###,###,###";
            ultraGridColumn226.Header.Caption = "가구수";
            ultraGridColumn226.Header.VisiblePosition = 14;
            ultraGridColumn226.Width = 60;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn211,
            ultraGridColumn212,
            ultraGridColumn213,
            ultraGridColumn214,
            ultraGridColumn215,
            ultraGridColumn216,
            ultraGridColumn217,
            ultraGridColumn11,
            ultraGridColumn218,
            ultraGridColumn219,
            ultraGridColumn220,
            ultraGridColumn221,
            ultraGridColumn222,
            ultraGridColumn223,
            ultraGridColumn224,
            ultraGridColumn225,
            ultraGridColumn226});
            appearance44.TextHAlignAsString = "Center";
            ultraGridBand2.Header.Appearance = appearance44;
            this.ultraGrid2.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.ultraGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid2.DisplayLayout.GroupByBox.Appearance = appearance45;
            appearance46.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance46;
            this.ultraGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance47.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance47.BackColor2 = System.Drawing.SystemColors.Control;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance47.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance47;
            this.ultraGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            appearance48.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance48;
            appearance49.BackColor = System.Drawing.SystemColors.Highlight;
            appearance49.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance49;
            this.ultraGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid2.DisplayLayout.Override.CardAreaAppearance = appearance91;
            appearance92.BorderColor = System.Drawing.Color.Silver;
            appearance92.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid2.DisplayLayout.Override.CellAppearance = appearance92;
            this.ultraGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance93.BackColor = System.Drawing.SystemColors.Control;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance93;
            appearance94.TextHAlignAsString = "Left";
            this.ultraGrid2.DisplayLayout.Override.HeaderAppearance = appearance94;
            this.ultraGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid2.DisplayLayout.Override.RowAppearance = appearance95;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance96;
            this.ultraGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid2.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid2.Name = "ultraGrid2";
            this.ultraGrid2.Size = new System.Drawing.Size(1030, 124);
            this.ultraGrid2.TabIndex = 7;
            this.ultraGrid2.TabStop = false;
            this.ultraGrid2.Text = "ultraGrid3";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.White;
            this.panel20.Controls.Add(this.BigUsedUpdate);
            this.panel20.Controls.Add(this.UsedFilteringBtn);
            this.panel20.Controls.Add(this.textBox1);
            this.panel20.Controls.Add(this.label9);
            this.panel20.Controls.Add(this.label2);
            this.panel20.Controls.Add(this.label1);
            this.panel20.Controls.Add(this.UsedDMCombo);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(1030, 26);
            this.panel20.TabIndex = 5;
            // 
            // BigUsedUpdate
            // 
            this.BigUsedUpdate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BigUsedUpdate.Location = new System.Drawing.Point(940, 3);
            this.BigUsedUpdate.Name = "BigUsedUpdate";
            this.BigUsedUpdate.Size = new System.Drawing.Size(90, 20);
            this.BigUsedUpdate.TabIndex = 5;
            this.BigUsedUpdate.Text = "대수용가추가";
            this.BigUsedUpdate.UseVisualStyleBackColor = true;
            // 
            // UsedFilteringBtn
            // 
            this.UsedFilteringBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.UsedFilteringBtn.Location = new System.Drawing.Point(880, 3);
            this.UsedFilteringBtn.Name = "UsedFilteringBtn";
            this.UsedFilteringBtn.Size = new System.Drawing.Size(55, 20);
            this.UsedFilteringBtn.TabIndex = 4;
            this.UsedFilteringBtn.Text = "필터링";
            this.UsedFilteringBtn.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.textBox1.Location = new System.Drawing.Point(750, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(51, 21);
            this.textBox1.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Tan;
            this.label9.Location = new System.Drawing.Point(0, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 12);
            this.label9.TabIndex = 1;
            this.label9.Text = "수용가 현황       ";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(806, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "㎥/월 이상";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(642, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "사용량";
            // 
            // UsedDMCombo
            // 
            this.UsedDMCombo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.UsedDMCombo.FormattingEnabled = true;
            this.UsedDMCombo.Location = new System.Drawing.Point(689, 2);
            this.UsedDMCombo.Name = "UsedDMCombo";
            this.UsedDMCombo.Size = new System.Drawing.Size(55, 20);
            this.UsedDMCombo.TabIndex = 1;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox9.Location = new System.Drawing.Point(1040, 10);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 416);
            this.pictureBox9.TabIndex = 9;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox8.Location = new System.Drawing.Point(0, 10);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 416);
            this.pictureBox8.TabIndex = 8;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox7.Location = new System.Drawing.Point(0, 426);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox7.TabIndex = 7;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(0, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox6.TabIndex = 6;
            this.pictureBox6.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.tabContent2);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox14);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox15);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox16);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1050, 436);
            // 
            // tabContent2
            // 
            this.tabContent2.Controls.Add(this.tableLayoutPanel2);
            this.tabContent2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContent2.Location = new System.Drawing.Point(10, 10);
            this.tabContent2.Name = "tabContent2";
            this.tabContent2.Size = new System.Drawing.Size(1030, 416);
            this.tabContent2.TabIndex = 14;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.tablePanel2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.chartPanel2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1030, 416);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // tablePanel2
            // 
            this.tablePanel2.BackColor = System.Drawing.Color.White;
            this.tablePanel2.Controls.Add(this.tableLayoutPanel22);
            this.tablePanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel2.Location = new System.Drawing.Point(0, 166);
            this.tablePanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel2.Name = "tablePanel2";
            this.tablePanel2.Size = new System.Drawing.Size(1030, 250);
            this.tablePanel2.TabIndex = 3;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.panel13, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.panel15, 0, 1);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel22.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1030, 250);
            this.tableLayoutPanel22.TabIndex = 0;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.ultraGrid3);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1030, 100);
            this.panel13.TabIndex = 0;
            // 
            // ultraGrid3
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid3.DisplayLayout.Appearance = appearance2;
            ultraGridColumn16.Header.Caption = "지역관리번호";
            ultraGridColumn16.Header.VisiblePosition = 0;
            ultraGridColumn16.Hidden = true;
            ultraGridColumn302.Header.Caption = "센터";
            ultraGridColumn302.Header.VisiblePosition = 1;
            ultraGridColumn302.Hidden = true;
            ultraGridColumn302.Width = 80;
            ultraGridColumn303.GroupByMode = Infragistics.Win.UltraWinGrid.GroupByMode.Text;
            ultraGridColumn303.Header.Caption = "대블록";
            ultraGridColumn303.Header.VisiblePosition = 2;
            ultraGridColumn303.Width = 60;
            ultraGridColumn304.Header.Caption = "중블록";
            ultraGridColumn304.Header.VisiblePosition = 3;
            ultraGridColumn304.Width = 60;
            ultraGridColumn305.Header.Caption = "소블록";
            ultraGridColumn305.Header.VisiblePosition = 4;
            ultraGridColumn305.Width = 60;
            appearance4.TextHAlignAsString = "Right";
            ultraGridColumn306.CellAppearance = appearance4;
            ultraGridColumn306.DefaultCellValue = "0";
            ultraGridColumn306.Format = "###,###,###";
            ultraGridColumn306.Header.Caption = "전수";
            ultraGridColumn306.Header.VisiblePosition = 5;
            ultraGridColumn306.Width = 60;
            appearance5.TextHAlignAsString = "Right";
            ultraGridColumn307.CellAppearance = appearance5;
            ultraGridColumn307.DefaultCellValue = "0";
            ultraGridColumn307.Format = "###,###,###";
            ultraGridColumn307.Header.Caption = "개전수";
            ultraGridColumn307.Header.VisiblePosition = 6;
            ultraGridColumn307.Width = 60;
            appearance6.TextHAlignAsString = "Right";
            ultraGridColumn308.CellAppearance = appearance6;
            ultraGridColumn308.DefaultCellValue = "0";
            ultraGridColumn308.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn308.Format = "###,###,###";
            ultraGridColumn308.Header.Caption = "폐전수";
            ultraGridColumn308.Header.VisiblePosition = 7;
            ultraGridColumn308.Width = 60;
            appearance7.TextHAlignAsString = "Right";
            ultraGridColumn309.CellAppearance = appearance7;
            ultraGridColumn309.DefaultCellValue = "0";
            ultraGridColumn309.Format = "###,###,###";
            ultraGridColumn309.Header.Caption = "중지전수";
            ultraGridColumn309.Header.VisiblePosition = 8;
            ultraGridColumn309.Width = 60;
            appearance8.TextHAlignAsString = "Right";
            ultraGridColumn310.CellAppearance = appearance8;
            ultraGridColumn310.DefaultCellValue = "0";
            ultraGridColumn310.Format = "###,###,###";
            ultraGridColumn310.Header.Caption = "교체전수";
            ultraGridColumn310.Header.VisiblePosition = 9;
            ultraGridColumn310.Width = 60;
            appearance9.TextHAlignAsString = "Right";
            ultraGridColumn311.CellAppearance = appearance9;
            ultraGridColumn311.DefaultCellValue = "0";
            ultraGridColumn311.Format = "###,###,###";
            ultraGridColumn311.Header.Caption = "가구수";
            ultraGridColumn311.Header.VisiblePosition = 10;
            ultraGridColumn311.Width = 60;
            appearance10.TextHAlignAsString = "Center";
            ultraGridColumn312.CellAppearance = appearance10;
            ultraGridColumn312.Header.Caption = "업종";
            ultraGridColumn312.Header.VisiblePosition = 11;
            ultraGridColumn312.Width = 60;
            ultraGridBand3.Columns.AddRange(new object[] {
            ultraGridColumn16,
            ultraGridColumn302,
            ultraGridColumn303,
            ultraGridColumn304,
            ultraGridColumn305,
            ultraGridColumn306,
            ultraGridColumn307,
            ultraGridColumn308,
            ultraGridColumn309,
            ultraGridColumn310,
            ultraGridColumn311,
            ultraGridColumn312});
            appearance11.TextHAlignAsString = "Center";
            ultraGridBand3.Header.Appearance = appearance11;
            this.ultraGrid3.DisplayLayout.BandsSerializer.Add(ultraGridBand3);
            this.ultraGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid3.DisplayLayout.GroupByBox.Appearance = appearance12;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance25;
            this.ultraGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance26.BackColor2 = System.Drawing.SystemColors.Control;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance26;
            this.ultraGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance27;
            appearance28.BackColor = System.Drawing.SystemColors.Highlight;
            appearance28.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance28;
            this.ultraGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid3.DisplayLayout.Override.CardAreaAppearance = appearance29;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            appearance30.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid3.DisplayLayout.Override.CellAppearance = appearance30;
            this.ultraGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance31.BackColor = System.Drawing.SystemColors.Control;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance31;
            appearance32.TextHAlignAsString = "Left";
            this.ultraGrid3.DisplayLayout.Override.HeaderAppearance = appearance32;
            this.ultraGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid3.DisplayLayout.Override.RowAppearance = appearance33;
            this.ultraGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance34;
            this.ultraGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid3.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid3.Name = "ultraGrid3";
            this.ultraGrid3.Size = new System.Drawing.Size(1030, 74);
            this.ultraGrid3.TabIndex = 6;
            this.ultraGrid3.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.White;
            this.panel14.Controls.Add(this.label10);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1030, 26);
            this.panel14.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Tan;
            this.label10.Location = new System.Drawing.Point(0, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 12);
            this.label10.TabIndex = 1;
            this.label10.Text = "블록별 현황       ";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.ultraGrid4);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 100);
            this.panel15.Margin = new System.Windows.Forms.Padding(0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1030, 150);
            this.panel15.TabIndex = 1;
            // 
            // ultraGrid4
            // 
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid4.DisplayLayout.Appearance = appearance97;
            ultraGridColumn228.Header.Caption = "대블록";
            ultraGridColumn228.Header.VisiblePosition = 0;
            ultraGridColumn228.Hidden = true;
            ultraGridColumn228.Width = 60;
            ultraGridColumn229.Header.Caption = "중블록";
            ultraGridColumn229.Header.VisiblePosition = 1;
            ultraGridColumn229.Hidden = true;
            ultraGridColumn229.Width = 60;
            ultraGridColumn230.Header.Caption = "소블록";
            ultraGridColumn230.Header.VisiblePosition = 2;
            ultraGridColumn230.Hidden = true;
            ultraGridColumn230.Width = 60;
            ultraGridColumn231.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance108.TextHAlignAsString = "Center";
            ultraGridColumn231.CellAppearance = appearance108;
            ultraGridColumn231.Header.Caption = "주/부";
            ultraGridColumn231.Header.VisiblePosition = 3;
            ultraGridColumn231.Width = 55;
            ultraGridColumn232.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn232.Header.Caption = "수용가번호";
            ultraGridColumn232.Header.VisiblePosition = 4;
            ultraGridColumn232.Width = 100;
            ultraGridColumn233.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn233.Header.Caption = "수용가명";
            ultraGridColumn233.Header.VisiblePosition = 5;
            ultraGridColumn233.Width = 150;
            ultraGridColumn234.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn234.Header.Caption = "수전번호";
            ultraGridColumn234.Header.VisiblePosition = 6;
            ultraGridColumn234.Width = 80;
            ultraGridColumn13.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn13.Header.Caption = "계량기관리번호";
            ultraGridColumn13.Header.VisiblePosition = 7;
            ultraGridColumn13.Width = 115;
            appearance109.TextHAlignAsString = "Center";
            ultraGridColumn235.CellAppearance = appearance109;
            ultraGridColumn235.Header.Caption = "읍면동";
            ultraGridColumn235.Header.VisiblePosition = 8;
            ultraGridColumn235.Width = 60;
            ultraGridColumn236.Header.Caption = "주소";
            ultraGridColumn236.Header.VisiblePosition = 9;
            ultraGridColumn236.Width = 150;
            appearance110.TextHAlignAsString = "Center";
            ultraGridColumn237.CellAppearance = appearance110;
            ultraGridColumn237.Header.Caption = "업종";
            ultraGridColumn237.Header.VisiblePosition = 10;
            ultraGridColumn237.Width = 60;
            appearance111.TextHAlignAsString = "Right";
            ultraGridColumn238.CellAppearance = appearance111;
            ultraGridColumn238.Format = "###,###,###";
            ultraGridColumn238.Header.Caption = "구경";
            ultraGridColumn238.Header.VisiblePosition = 11;
            ultraGridColumn238.Width = 60;
            appearance112.TextHAlignAsString = "Center";
            ultraGridColumn239.CellAppearance = appearance112;
            ultraGridColumn239.Header.Caption = "개전일자";
            ultraGridColumn239.Header.VisiblePosition = 12;
            ultraGridColumn239.Width = 80;
            appearance113.TextHAlignAsString = "Center";
            ultraGridColumn240.CellAppearance = appearance113;
            ultraGridColumn240.Header.Caption = "폐전일자";
            ultraGridColumn240.Header.VisiblePosition = 14;
            ultraGridColumn240.Width = 80;
            appearance124.TextHAlignAsString = "Center";
            ultraGridColumn241.CellAppearance = appearance124;
            ultraGridColumn241.Header.Caption = "중지일자";
            ultraGridColumn241.Header.VisiblePosition = 15;
            ultraGridColumn241.Width = 80;
            appearance125.TextHAlignAsString = "Center";
            ultraGridColumn242.CellAppearance = appearance125;
            ultraGridColumn242.Header.Caption = "교체일자";
            ultraGridColumn242.Header.VisiblePosition = 16;
            ultraGridColumn242.Width = 80;
            appearance126.TextHAlignAsString = "Right";
            ultraGridColumn243.CellAppearance = appearance126;
            ultraGridColumn243.DefaultCellValue = "0";
            ultraGridColumn243.Format = "###,###,###";
            ultraGridColumn243.Header.Caption = "가구수";
            ultraGridColumn243.Header.VisiblePosition = 13;
            ultraGridColumn243.Width = 60;
            ultraGridBand4.Columns.AddRange(new object[] {
            ultraGridColumn228,
            ultraGridColumn229,
            ultraGridColumn230,
            ultraGridColumn231,
            ultraGridColumn232,
            ultraGridColumn233,
            ultraGridColumn234,
            ultraGridColumn13,
            ultraGridColumn235,
            ultraGridColumn236,
            ultraGridColumn237,
            ultraGridColumn238,
            ultraGridColumn239,
            ultraGridColumn240,
            ultraGridColumn241,
            ultraGridColumn242,
            ultraGridColumn243});
            appearance127.TextHAlignAsString = "Center";
            ultraGridBand4.Header.Appearance = appearance127;
            this.ultraGrid4.DisplayLayout.BandsSerializer.Add(ultraGridBand4);
            this.ultraGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance128.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance128.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance128.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance128.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid4.DisplayLayout.GroupByBox.Appearance = appearance128;
            appearance139.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance139;
            this.ultraGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance140.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance140.BackColor2 = System.Drawing.SystemColors.Control;
            appearance140.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance140.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance140;
            this.ultraGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance141.BackColor = System.Drawing.SystemColors.Window;
            appearance141.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance141;
            appearance142.BackColor = System.Drawing.SystemColors.Highlight;
            appearance142.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance142;
            this.ultraGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance143.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid4.DisplayLayout.Override.CardAreaAppearance = appearance143;
            appearance144.BorderColor = System.Drawing.Color.Silver;
            appearance144.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid4.DisplayLayout.Override.CellAppearance = appearance144;
            this.ultraGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance145.BackColor = System.Drawing.SystemColors.Control;
            appearance145.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance145.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance145.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance145.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance145;
            appearance146.TextHAlignAsString = "Left";
            this.ultraGrid4.DisplayLayout.Override.HeaderAppearance = appearance146;
            this.ultraGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance147.BackColor = System.Drawing.SystemColors.Window;
            appearance147.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid4.DisplayLayout.Override.RowAppearance = appearance147;
            this.ultraGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance148.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance148;
            this.ultraGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid4.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid4.Name = "ultraGrid4";
            this.ultraGrid4.Size = new System.Drawing.Size(1030, 124);
            this.ultraGrid4.TabIndex = 8;
            this.ultraGrid4.TabStop = false;
            this.ultraGrid4.Text = "ultraGrid3";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.White;
            this.panel16.Controls.Add(this.label11);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1030, 26);
            this.panel16.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Tan;
            this.label11.Location = new System.Drawing.Point(0, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 12);
            this.label11.TabIndex = 2;
            this.label11.Text = "수용가 현황       ";
            // 
            // chartPanel2
            // 
            this.chartPanel2.BackColor = System.Drawing.Color.White;
            this.chartPanel2.Controls.Add(this.chart2);
            this.chartPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel2.Location = new System.Drawing.Point(0, 0);
            this.chartPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.chartPanel2.Name = "chartPanel2";
            this.chartPanel2.Size = new System.Drawing.Size(1030, 166);
            this.chartPanel2.TabIndex = 2;
            // 
            // chart2
            // 
            this.chart2.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart2.AllSeries.Line.Width = ((short)(1));
            this.chart2.AllSeries.MarkerSize = ((short)(1));
            this.chart2.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart2.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart2.AxisY.Title.Text = "";
            solidBackground2.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart2.Background = solidBackground2;
            this.chart2.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart2.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart2.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart2.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart2.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart2.LegendBox.PlotAreaOnly = false;
            this.chart2.Location = new System.Drawing.Point(0, 0);
            this.chart2.MainPane.Title.Text = "";
            this.chart2.Name = "chart2";
            this.chart2.Size = new System.Drawing.Size(1030, 166);
            this.chart2.TabIndex = 5;
            this.chart2.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox14.Location = new System.Drawing.Point(1040, 10);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(10, 416);
            this.pictureBox14.TabIndex = 13;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox15.Location = new System.Drawing.Point(0, 10);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(10, 416);
            this.pictureBox15.TabIndex = 12;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox16.Location = new System.Drawing.Point(0, 426);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox16.TabIndex = 11;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox17.Location = new System.Drawing.Point(0, 0);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox17.TabIndex = 10;
            this.pictureBox17.TabStop = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.tabContent3);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox18);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox19);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox20);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox21);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1050, 436);
            // 
            // tabContent3
            // 
            this.tabContent3.Controls.Add(this.tableLayoutPanel3);
            this.tabContent3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContent3.Location = new System.Drawing.Point(10, 10);
            this.tabContent3.Name = "tabContent3";
            this.tabContent3.Size = new System.Drawing.Size(1030, 416);
            this.tabContent3.TabIndex = 14;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.tablePanel3, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.chartPanel3, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1030, 416);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // tablePanel3
            // 
            this.tablePanel3.BackColor = System.Drawing.Color.White;
            this.tablePanel3.Controls.Add(this.tableLayoutPanel33);
            this.tablePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel3.Location = new System.Drawing.Point(0, 166);
            this.tablePanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel3.Name = "tablePanel3";
            this.tablePanel3.Size = new System.Drawing.Size(1030, 250);
            this.tablePanel3.TabIndex = 3;
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 1;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel33.Controls.Add(this.panel21, 0, 0);
            this.tableLayoutPanel33.Controls.Add(this.panel23, 0, 1);
            this.tableLayoutPanel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel33.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel33.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 2;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(1030, 250);
            this.tableLayoutPanel33.TabIndex = 0;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.ultraGrid5);
            this.panel21.Controls.Add(this.panel22);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Margin = new System.Windows.Forms.Padding(0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(1030, 100);
            this.panel21.TabIndex = 0;
            // 
            // ultraGrid5
            // 
            appearance220.BackColor = System.Drawing.SystemColors.Window;
            appearance220.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid5.DisplayLayout.Appearance = appearance220;
            ultraGridColumn17.Header.Caption = "지역관리번호";
            ultraGridColumn17.Header.VisiblePosition = 0;
            ultraGridColumn17.Hidden = true;
            ultraGridColumn147.Header.Caption = "센터";
            ultraGridColumn147.Header.VisiblePosition = 1;
            ultraGridColumn147.Hidden = true;
            ultraGridColumn147.Width = 80;
            ultraGridColumn149.Header.Caption = "대블록";
            ultraGridColumn149.Header.VisiblePosition = 2;
            ultraGridColumn149.Width = 60;
            ultraGridColumn150.Header.Caption = "중블록";
            ultraGridColumn150.Header.VisiblePosition = 3;
            ultraGridColumn150.Width = 60;
            ultraGridColumn151.Header.Caption = "소블록";
            ultraGridColumn151.Header.VisiblePosition = 4;
            ultraGridColumn151.Width = 60;
            appearance221.TextHAlignAsString = "Right";
            ultraGridColumn152.CellAppearance = appearance221;
            ultraGridColumn152.DefaultCellValue = "0";
            ultraGridColumn152.Format = "###,###,###";
            ultraGridColumn152.Header.Caption = "전수";
            ultraGridColumn152.Header.VisiblePosition = 5;
            ultraGridColumn152.Width = 60;
            appearance222.TextHAlignAsString = "Right";
            ultraGridColumn153.CellAppearance = appearance222;
            ultraGridColumn153.DefaultCellValue = "0";
            ultraGridColumn153.Format = "###,###,###";
            ultraGridColumn153.Header.Caption = "개전수";
            ultraGridColumn153.Header.VisiblePosition = 6;
            ultraGridColumn153.Width = 60;
            appearance223.TextHAlignAsString = "Right";
            ultraGridColumn154.CellAppearance = appearance223;
            ultraGridColumn154.DefaultCellValue = "0";
            ultraGridColumn154.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn154.Format = "###,###,###";
            ultraGridColumn154.Header.Caption = "폐전수";
            ultraGridColumn154.Header.VisiblePosition = 7;
            ultraGridColumn154.Width = 60;
            appearance224.TextHAlignAsString = "Right";
            ultraGridColumn155.CellAppearance = appearance224;
            ultraGridColumn155.DefaultCellValue = "0";
            ultraGridColumn155.Format = "###,###,###";
            ultraGridColumn155.Header.Caption = "중지전수";
            ultraGridColumn155.Header.VisiblePosition = 8;
            ultraGridColumn155.Width = 60;
            appearance225.TextHAlignAsString = "Right";
            ultraGridColumn156.CellAppearance = appearance225;
            ultraGridColumn156.DefaultCellValue = "0";
            ultraGridColumn156.Format = "###,###,###";
            ultraGridColumn156.Header.Caption = "교체전수";
            ultraGridColumn156.Header.VisiblePosition = 9;
            ultraGridColumn156.Width = 60;
            appearance226.TextHAlignAsString = "Right";
            ultraGridColumn157.CellAppearance = appearance226;
            ultraGridColumn157.DefaultCellValue = "0";
            ultraGridColumn157.Format = "###,###,###";
            ultraGridColumn157.Header.Caption = "가구수";
            ultraGridColumn157.Header.VisiblePosition = 10;
            ultraGridColumn157.Width = 60;
            appearance227.TextHAlignAsString = "Center";
            ultraGridColumn300.CellAppearance = appearance227;
            ultraGridColumn300.Header.Caption = "업종";
            ultraGridColumn300.Header.VisiblePosition = 11;
            ultraGridColumn300.Width = 60;
            ultraGridBand5.Columns.AddRange(new object[] {
            ultraGridColumn17,
            ultraGridColumn147,
            ultraGridColumn149,
            ultraGridColumn150,
            ultraGridColumn151,
            ultraGridColumn152,
            ultraGridColumn153,
            ultraGridColumn154,
            ultraGridColumn155,
            ultraGridColumn156,
            ultraGridColumn157,
            ultraGridColumn300});
            appearance228.TextHAlignAsString = "Center";
            ultraGridBand5.Header.Appearance = appearance228;
            this.ultraGrid5.DisplayLayout.BandsSerializer.Add(ultraGridBand5);
            this.ultraGrid5.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid5.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance229.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance229.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance229.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance229.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.GroupByBox.Appearance = appearance229;
            appearance230.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid5.DisplayLayout.GroupByBox.BandLabelAppearance = appearance230;
            this.ultraGrid5.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance231.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance231.BackColor2 = System.Drawing.SystemColors.Control;
            appearance231.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance231.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid5.DisplayLayout.GroupByBox.PromptAppearance = appearance231;
            this.ultraGrid5.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid5.DisplayLayout.MaxRowScrollRegions = 1;
            appearance232.BackColor = System.Drawing.SystemColors.Window;
            appearance232.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid5.DisplayLayout.Override.ActiveCellAppearance = appearance232;
            appearance233.BackColor = System.Drawing.SystemColors.Highlight;
            appearance233.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid5.DisplayLayout.Override.ActiveRowAppearance = appearance233;
            this.ultraGrid5.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid5.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance234.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.Override.CardAreaAppearance = appearance234;
            appearance235.BorderColor = System.Drawing.Color.Silver;
            appearance235.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid5.DisplayLayout.Override.CellAppearance = appearance235;
            this.ultraGrid5.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid5.DisplayLayout.Override.CellPadding = 0;
            appearance236.BackColor = System.Drawing.SystemColors.Control;
            appearance236.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance236.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance236.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance236.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.Override.GroupByRowAppearance = appearance236;
            appearance237.TextHAlignAsString = "Left";
            this.ultraGrid5.DisplayLayout.Override.HeaderAppearance = appearance237;
            this.ultraGrid5.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid5.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance238.BackColor = System.Drawing.SystemColors.Window;
            appearance238.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid5.DisplayLayout.Override.RowAppearance = appearance238;
            this.ultraGrid5.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance239.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid5.DisplayLayout.Override.TemplateAddRowAppearance = appearance239;
            this.ultraGrid5.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid5.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid5.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid5.Name = "ultraGrid5";
            this.ultraGrid5.Size = new System.Drawing.Size(1030, 74);
            this.ultraGrid5.TabIndex = 7;
            this.ultraGrid5.TabStop = false;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.White;
            this.panel22.Controls.Add(this.label12);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1030, 26);
            this.panel22.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Tan;
            this.label12.Location = new System.Drawing.Point(0, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 12);
            this.label12.TabIndex = 2;
            this.label12.Text = "블록별 현황       ";
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.ultraGrid6);
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(0, 100);
            this.panel23.Margin = new System.Windows.Forms.Padding(0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(1030, 150);
            this.panel23.TabIndex = 1;
            // 
            // ultraGrid6
            // 
            appearance154.BackColor = System.Drawing.SystemColors.Window;
            appearance154.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid6.DisplayLayout.Appearance = appearance154;
            ultraGridColumn256.Header.Caption = "대블록";
            ultraGridColumn256.Header.VisiblePosition = 0;
            ultraGridColumn256.Hidden = true;
            ultraGridColumn256.Width = 60;
            ultraGridColumn257.Header.Caption = "중블록";
            ultraGridColumn257.Header.VisiblePosition = 1;
            ultraGridColumn257.Hidden = true;
            ultraGridColumn257.Width = 60;
            ultraGridColumn258.Header.Caption = "소블록";
            ultraGridColumn258.Header.VisiblePosition = 2;
            ultraGridColumn258.Hidden = true;
            ultraGridColumn258.Width = 60;
            ultraGridColumn259.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance155.TextHAlignAsString = "Center";
            ultraGridColumn259.CellAppearance = appearance155;
            ultraGridColumn259.Header.Caption = "주/부";
            ultraGridColumn259.Header.VisiblePosition = 3;
            ultraGridColumn259.Width = 55;
            ultraGridColumn260.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn260.Header.Caption = "수용가번호";
            ultraGridColumn260.Header.VisiblePosition = 4;
            ultraGridColumn260.Width = 100;
            ultraGridColumn261.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn261.Header.Caption = "수용가명";
            ultraGridColumn261.Header.VisiblePosition = 5;
            ultraGridColumn261.Width = 150;
            ultraGridColumn262.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn262.Header.Caption = "수전번호";
            ultraGridColumn262.Header.VisiblePosition = 6;
            ultraGridColumn262.Width = 80;
            ultraGridColumn14.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn14.Header.Caption = "계량기관리번호";
            ultraGridColumn14.Header.VisiblePosition = 7;
            ultraGridColumn14.Width = 115;
            appearance156.TextHAlignAsString = "Center";
            ultraGridColumn263.CellAppearance = appearance156;
            ultraGridColumn263.Header.Caption = "읍면동";
            ultraGridColumn263.Header.VisiblePosition = 8;
            ultraGridColumn263.Width = 60;
            ultraGridColumn264.Header.Caption = "주소";
            ultraGridColumn264.Header.VisiblePosition = 9;
            ultraGridColumn264.Width = 150;
            appearance157.TextHAlignAsString = "Center";
            ultraGridColumn265.CellAppearance = appearance157;
            ultraGridColumn265.Header.Caption = "업종";
            ultraGridColumn265.Header.VisiblePosition = 10;
            ultraGridColumn265.Width = 60;
            appearance158.TextHAlignAsString = "Right";
            ultraGridColumn266.CellAppearance = appearance158;
            ultraGridColumn266.Format = "###,###,###";
            ultraGridColumn266.Header.Caption = "구경";
            ultraGridColumn266.Header.VisiblePosition = 11;
            ultraGridColumn266.Width = 60;
            appearance163.TextHAlignAsString = "Center";
            ultraGridColumn267.CellAppearance = appearance163;
            ultraGridColumn267.Header.Caption = "개전일자";
            ultraGridColumn267.Header.VisiblePosition = 12;
            ultraGridColumn267.Width = 80;
            appearance164.TextHAlignAsString = "Center";
            ultraGridColumn268.CellAppearance = appearance164;
            ultraGridColumn268.Header.Caption = "폐전일자";
            ultraGridColumn268.Header.VisiblePosition = 14;
            ultraGridColumn268.Width = 80;
            appearance165.TextHAlignAsString = "Center";
            ultraGridColumn269.CellAppearance = appearance165;
            ultraGridColumn269.Header.Caption = "중지일자";
            ultraGridColumn269.Header.VisiblePosition = 15;
            ultraGridColumn269.Width = 80;
            appearance166.TextHAlignAsString = "Center";
            ultraGridColumn270.CellAppearance = appearance166;
            ultraGridColumn270.Header.Caption = "교체일자";
            ultraGridColumn270.Header.VisiblePosition = 16;
            ultraGridColumn270.Width = 80;
            appearance167.TextHAlignAsString = "Right";
            ultraGridColumn271.CellAppearance = appearance167;
            ultraGridColumn271.DefaultCellValue = "0";
            ultraGridColumn271.Format = "###,###,###";
            ultraGridColumn271.Header.Caption = "가구수";
            ultraGridColumn271.Header.VisiblePosition = 13;
            ultraGridColumn271.Width = 60;
            ultraGridBand6.Columns.AddRange(new object[] {
            ultraGridColumn256,
            ultraGridColumn257,
            ultraGridColumn258,
            ultraGridColumn259,
            ultraGridColumn260,
            ultraGridColumn261,
            ultraGridColumn262,
            ultraGridColumn14,
            ultraGridColumn263,
            ultraGridColumn264,
            ultraGridColumn265,
            ultraGridColumn266,
            ultraGridColumn267,
            ultraGridColumn268,
            ultraGridColumn269,
            ultraGridColumn270,
            ultraGridColumn271});
            appearance168.TextHAlignAsString = "Center";
            ultraGridBand6.Header.Appearance = appearance168;
            this.ultraGrid6.DisplayLayout.BandsSerializer.Add(ultraGridBand6);
            this.ultraGrid6.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid6.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance169.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance169.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance169.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance169.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid6.DisplayLayout.GroupByBox.Appearance = appearance169;
            appearance170.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid6.DisplayLayout.GroupByBox.BandLabelAppearance = appearance170;
            this.ultraGrid6.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance171.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance171.BackColor2 = System.Drawing.SystemColors.Control;
            appearance171.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance171.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid6.DisplayLayout.GroupByBox.PromptAppearance = appearance171;
            this.ultraGrid6.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid6.DisplayLayout.MaxRowScrollRegions = 1;
            appearance172.BackColor = System.Drawing.SystemColors.Window;
            appearance172.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid6.DisplayLayout.Override.ActiveCellAppearance = appearance172;
            appearance173.BackColor = System.Drawing.SystemColors.Highlight;
            appearance173.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid6.DisplayLayout.Override.ActiveRowAppearance = appearance173;
            this.ultraGrid6.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid6.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance174.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid6.DisplayLayout.Override.CardAreaAppearance = appearance174;
            appearance175.BorderColor = System.Drawing.Color.Silver;
            appearance175.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid6.DisplayLayout.Override.CellAppearance = appearance175;
            this.ultraGrid6.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid6.DisplayLayout.Override.CellPadding = 0;
            appearance176.BackColor = System.Drawing.SystemColors.Control;
            appearance176.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance176.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance176.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance176.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid6.DisplayLayout.Override.GroupByRowAppearance = appearance176;
            appearance177.TextHAlignAsString = "Left";
            this.ultraGrid6.DisplayLayout.Override.HeaderAppearance = appearance177;
            this.ultraGrid6.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid6.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance198.BackColor = System.Drawing.SystemColors.Window;
            appearance198.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid6.DisplayLayout.Override.RowAppearance = appearance198;
            this.ultraGrid6.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance199.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid6.DisplayLayout.Override.TemplateAddRowAppearance = appearance199;
            this.ultraGrid6.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid6.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid6.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid6.Name = "ultraGrid6";
            this.ultraGrid6.Size = new System.Drawing.Size(1030, 124);
            this.ultraGrid6.TabIndex = 8;
            this.ultraGrid6.TabStop = false;
            this.ultraGrid6.Text = "ultraGrid3";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.White;
            this.panel24.Controls.Add(this.label13);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1030, 26);
            this.panel24.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Tan;
            this.label13.Location = new System.Drawing.Point(0, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 12);
            this.label13.TabIndex = 3;
            this.label13.Text = "수용가 현황       ";
            // 
            // chartPanel3
            // 
            this.chartPanel3.BackColor = System.Drawing.Color.White;
            this.chartPanel3.Controls.Add(this.chart3);
            this.chartPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel3.Location = new System.Drawing.Point(0, 0);
            this.chartPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.chartPanel3.Name = "chartPanel3";
            this.chartPanel3.Size = new System.Drawing.Size(1030, 166);
            this.chartPanel3.TabIndex = 2;
            // 
            // chart3
            // 
            this.chart3.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart3.AllSeries.Line.Width = ((short)(1));
            this.chart3.AllSeries.MarkerSize = ((short)(1));
            this.chart3.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart3.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart3.AxisY.Title.Text = "";
            solidBackground3.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart3.Background = solidBackground3;
            this.chart3.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart3.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart3.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart3.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart3.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart3.LegendBox.PlotAreaOnly = false;
            this.chart3.Location = new System.Drawing.Point(0, 0);
            this.chart3.MainPane.Title.Text = "";
            this.chart3.Name = "chart3";
            this.chart3.Size = new System.Drawing.Size(1030, 166);
            this.chart3.TabIndex = 5;
            this.chart3.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox18.Location = new System.Drawing.Point(1040, 10);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(10, 416);
            this.pictureBox18.TabIndex = 13;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox19.Location = new System.Drawing.Point(0, 10);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(10, 416);
            this.pictureBox19.TabIndex = 12;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox20.Location = new System.Drawing.Point(0, 426);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox20.TabIndex = 11;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox21.Location = new System.Drawing.Point(0, 0);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox21.TabIndex = 10;
            this.pictureBox21.TabStop = false;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.tabContent4);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox22);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox23);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox24);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox25);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1050, 436);
            // 
            // tabContent4
            // 
            this.tabContent4.Controls.Add(this.tableLayoutPanel4);
            this.tabContent4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContent4.Location = new System.Drawing.Point(10, 10);
            this.tabContent4.Name = "tabContent4";
            this.tabContent4.Size = new System.Drawing.Size(1030, 416);
            this.tabContent4.TabIndex = 14;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.tablePanel4, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.chartPanel4, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1030, 416);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // tablePanel4
            // 
            this.tablePanel4.BackColor = System.Drawing.Color.White;
            this.tablePanel4.Controls.Add(this.tableLayoutPanel44);
            this.tablePanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel4.Location = new System.Drawing.Point(0, 166);
            this.tablePanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel4.Name = "tablePanel4";
            this.tablePanel4.Size = new System.Drawing.Size(1030, 250);
            this.tablePanel4.TabIndex = 3;
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 1;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel44.Controls.Add(this.panel26, 0, 0);
            this.tableLayoutPanel44.Controls.Add(this.panel28, 0, 1);
            this.tableLayoutPanel44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel44.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel44.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 2;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(1030, 250);
            this.tableLayoutPanel44.TabIndex = 0;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.ultraGrid7);
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Margin = new System.Windows.Forms.Padding(0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1030, 100);
            this.panel26.TabIndex = 0;
            // 
            // ultraGrid7
            // 
            appearance240.BackColor = System.Drawing.SystemColors.Window;
            appearance240.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid7.DisplayLayout.Appearance = appearance240;
            ultraGridColumn18.Header.Caption = "지역관리번호";
            ultraGridColumn18.Header.VisiblePosition = 0;
            ultraGridColumn18.Hidden = true;
            ultraGridColumn274.Header.Caption = "대블록";
            ultraGridColumn274.Header.VisiblePosition = 1;
            ultraGridColumn274.Width = 60;
            ultraGridColumn275.Header.Caption = "중블록";
            ultraGridColumn275.Header.VisiblePosition = 2;
            ultraGridColumn275.Width = 60;
            ultraGridColumn276.Header.Caption = "소블록";
            ultraGridColumn276.Header.VisiblePosition = 3;
            ultraGridColumn276.Width = 60;
            appearance241.TextHAlignAsString = "Right";
            ultraGridColumn277.CellAppearance = appearance241;
            ultraGridColumn277.DefaultCellValue = "0";
            ultraGridColumn277.Format = "###,###,###";
            ultraGridColumn277.Header.Caption = "전수";
            ultraGridColumn277.Header.VisiblePosition = 4;
            ultraGridColumn277.Width = 60;
            appearance242.TextHAlignAsString = "Right";
            ultraGridColumn278.CellAppearance = appearance242;
            ultraGridColumn278.DefaultCellValue = "0";
            ultraGridColumn278.Format = "###,###,###";
            ultraGridColumn278.Header.Caption = "개전수";
            ultraGridColumn278.Header.VisiblePosition = 5;
            ultraGridColumn278.Width = 60;
            appearance243.TextHAlignAsString = "Right";
            ultraGridColumn279.CellAppearance = appearance243;
            ultraGridColumn279.DefaultCellValue = "0";
            ultraGridColumn279.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn279.Format = "###,###,###";
            ultraGridColumn279.Header.Caption = "폐전수";
            ultraGridColumn279.Header.VisiblePosition = 6;
            ultraGridColumn279.Width = 60;
            appearance244.TextHAlignAsString = "Right";
            ultraGridColumn280.CellAppearance = appearance244;
            ultraGridColumn280.DefaultCellValue = "0";
            ultraGridColumn280.Format = "###,###,###";
            ultraGridColumn280.Header.Caption = "중지전수";
            ultraGridColumn280.Header.VisiblePosition = 7;
            ultraGridColumn280.Width = 60;
            appearance245.TextHAlignAsString = "Right";
            ultraGridColumn281.CellAppearance = appearance245;
            ultraGridColumn281.DefaultCellValue = "0";
            ultraGridColumn281.Format = "###,###,###";
            ultraGridColumn281.Header.Caption = "교체전수";
            ultraGridColumn281.Header.VisiblePosition = 8;
            ultraGridColumn281.Width = 60;
            appearance246.TextHAlignAsString = "Right";
            ultraGridColumn282.CellAppearance = appearance246;
            ultraGridColumn282.DefaultCellValue = "0";
            ultraGridColumn282.Format = "###,###,###";
            ultraGridColumn282.Header.Caption = "가구수";
            ultraGridColumn282.Header.VisiblePosition = 9;
            ultraGridColumn282.Width = 60;
            ultraGridBand7.Columns.AddRange(new object[] {
            ultraGridColumn18,
            ultraGridColumn274,
            ultraGridColumn275,
            ultraGridColumn276,
            ultraGridColumn277,
            ultraGridColumn278,
            ultraGridColumn279,
            ultraGridColumn280,
            ultraGridColumn281,
            ultraGridColumn282});
            appearance247.TextHAlignAsString = "Center";
            ultraGridBand7.Header.Appearance = appearance247;
            this.ultraGrid7.DisplayLayout.BandsSerializer.Add(ultraGridBand7);
            this.ultraGrid7.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid7.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance248.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance248.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance248.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance248.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid7.DisplayLayout.GroupByBox.Appearance = appearance248;
            appearance249.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid7.DisplayLayout.GroupByBox.BandLabelAppearance = appearance249;
            this.ultraGrid7.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance250.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance250.BackColor2 = System.Drawing.SystemColors.Control;
            appearance250.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance250.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid7.DisplayLayout.GroupByBox.PromptAppearance = appearance250;
            this.ultraGrid7.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid7.DisplayLayout.MaxRowScrollRegions = 1;
            appearance251.BackColor = System.Drawing.SystemColors.Window;
            appearance251.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid7.DisplayLayout.Override.ActiveCellAppearance = appearance251;
            appearance252.BackColor = System.Drawing.SystemColors.Highlight;
            appearance252.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid7.DisplayLayout.Override.ActiveRowAppearance = appearance252;
            this.ultraGrid7.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid7.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance253.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid7.DisplayLayout.Override.CardAreaAppearance = appearance253;
            appearance254.BorderColor = System.Drawing.Color.Silver;
            appearance254.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid7.DisplayLayout.Override.CellAppearance = appearance254;
            this.ultraGrid7.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid7.DisplayLayout.Override.CellPadding = 0;
            appearance255.BackColor = System.Drawing.SystemColors.Control;
            appearance255.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance255.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance255.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance255.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid7.DisplayLayout.Override.GroupByRowAppearance = appearance255;
            appearance265.TextHAlignAsString = "Left";
            this.ultraGrid7.DisplayLayout.Override.HeaderAppearance = appearance265;
            this.ultraGrid7.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid7.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance266.BackColor = System.Drawing.SystemColors.Window;
            appearance266.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid7.DisplayLayout.Override.RowAppearance = appearance266;
            this.ultraGrid7.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance267.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid7.DisplayLayout.Override.TemplateAddRowAppearance = appearance267;
            this.ultraGrid7.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid7.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid7.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid7.Name = "ultraGrid7";
            this.ultraGrid7.Size = new System.Drawing.Size(1030, 74);
            this.ultraGrid7.TabIndex = 6;
            this.ultraGrid7.TabStop = false;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.White;
            this.panel27.Controls.Add(this.label14);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1030, 26);
            this.panel27.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Tan;
            this.label14.Location = new System.Drawing.Point(0, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 12);
            this.label14.TabIndex = 3;
            this.label14.Text = "블록별 현황       ";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.ultraGrid8);
            this.panel28.Controls.Add(this.panel29);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel28.Location = new System.Drawing.Point(0, 100);
            this.panel28.Margin = new System.Windows.Forms.Padding(0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(1030, 150);
            this.panel28.TabIndex = 1;
            // 
            // ultraGrid8
            // 
            appearance200.BackColor = System.Drawing.SystemColors.Window;
            appearance200.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid8.DisplayLayout.Appearance = appearance200;
            ultraGridColumn284.Header.Caption = "대블록";
            ultraGridColumn284.Header.VisiblePosition = 0;
            ultraGridColumn284.Hidden = true;
            ultraGridColumn284.Width = 60;
            ultraGridColumn285.Header.Caption = "중블록";
            ultraGridColumn285.Header.VisiblePosition = 1;
            ultraGridColumn285.Hidden = true;
            ultraGridColumn285.Width = 60;
            ultraGridColumn286.Header.Caption = "소블록";
            ultraGridColumn286.Header.VisiblePosition = 2;
            ultraGridColumn286.Hidden = true;
            ultraGridColumn286.Width = 60;
            ultraGridColumn287.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance201.TextHAlignAsString = "Center";
            ultraGridColumn287.CellAppearance = appearance201;
            ultraGridColumn287.Header.Caption = "주/부";
            ultraGridColumn287.Header.VisiblePosition = 3;
            ultraGridColumn287.Width = 56;
            ultraGridColumn288.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn288.Header.Caption = "수용가번호";
            ultraGridColumn288.Header.VisiblePosition = 4;
            ultraGridColumn288.Width = 100;
            ultraGridColumn289.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn289.Header.Caption = "수용가명";
            ultraGridColumn289.Header.VisiblePosition = 5;
            ultraGridColumn289.Width = 150;
            ultraGridColumn290.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn290.Header.Caption = "수전번호";
            ultraGridColumn290.Header.VisiblePosition = 6;
            ultraGridColumn290.Width = 80;
            ultraGridColumn25.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn25.Header.Caption = "계량기관리번호";
            ultraGridColumn25.Header.VisiblePosition = 7;
            ultraGridColumn25.Width = 115;
            appearance202.TextHAlignAsString = "Center";
            ultraGridColumn291.CellAppearance = appearance202;
            ultraGridColumn291.Header.Caption = "읍면동";
            ultraGridColumn291.Header.VisiblePosition = 8;
            ultraGridColumn291.Width = 60;
            ultraGridColumn292.Header.Caption = "주소";
            ultraGridColumn292.Header.VisiblePosition = 9;
            ultraGridColumn292.Width = 150;
            appearance203.TextHAlignAsString = "Center";
            ultraGridColumn293.CellAppearance = appearance203;
            ultraGridColumn293.Header.Caption = "업종";
            ultraGridColumn293.Header.VisiblePosition = 10;
            ultraGridColumn293.Width = 60;
            appearance204.TextHAlignAsString = "Right";
            ultraGridColumn294.CellAppearance = appearance204;
            ultraGridColumn294.Format = "###,###,###";
            ultraGridColumn294.Header.Caption = "구경";
            ultraGridColumn294.Header.VisiblePosition = 11;
            ultraGridColumn294.Width = 60;
            appearance205.TextHAlignAsString = "Center";
            ultraGridColumn295.CellAppearance = appearance205;
            ultraGridColumn295.Header.Caption = "개전일자";
            ultraGridColumn295.Header.VisiblePosition = 12;
            ultraGridColumn295.Width = 80;
            appearance206.TextHAlignAsString = "Center";
            ultraGridColumn296.CellAppearance = appearance206;
            ultraGridColumn296.Header.Caption = "폐전일자";
            ultraGridColumn296.Header.VisiblePosition = 14;
            ultraGridColumn296.Width = 80;
            appearance207.TextHAlignAsString = "Center";
            ultraGridColumn297.CellAppearance = appearance207;
            ultraGridColumn297.Header.Caption = "중지일자";
            ultraGridColumn297.Header.VisiblePosition = 15;
            ultraGridColumn297.Width = 80;
            appearance208.TextHAlignAsString = "Center";
            ultraGridColumn298.CellAppearance = appearance208;
            ultraGridColumn298.Header.Caption = "교체일자";
            ultraGridColumn298.Header.VisiblePosition = 16;
            ultraGridColumn298.Width = 80;
            appearance209.TextHAlignAsString = "Right";
            ultraGridColumn299.CellAppearance = appearance209;
            ultraGridColumn299.DefaultCellValue = "0";
            ultraGridColumn299.Format = "###,###,###";
            ultraGridColumn299.Header.Caption = "가구수";
            ultraGridColumn299.Header.VisiblePosition = 13;
            ultraGridColumn299.Width = 60;
            ultraGridBand8.Columns.AddRange(new object[] {
            ultraGridColumn284,
            ultraGridColumn285,
            ultraGridColumn286,
            ultraGridColumn287,
            ultraGridColumn288,
            ultraGridColumn289,
            ultraGridColumn290,
            ultraGridColumn25,
            ultraGridColumn291,
            ultraGridColumn292,
            ultraGridColumn293,
            ultraGridColumn294,
            ultraGridColumn295,
            ultraGridColumn296,
            ultraGridColumn297,
            ultraGridColumn298,
            ultraGridColumn299});
            appearance210.TextHAlignAsString = "Center";
            ultraGridBand8.Header.Appearance = appearance210;
            this.ultraGrid8.DisplayLayout.BandsSerializer.Add(ultraGridBand8);
            this.ultraGrid8.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid8.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance211.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance211.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance211.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance211.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid8.DisplayLayout.GroupByBox.Appearance = appearance211;
            appearance212.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid8.DisplayLayout.GroupByBox.BandLabelAppearance = appearance212;
            this.ultraGrid8.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance213.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance213.BackColor2 = System.Drawing.SystemColors.Control;
            appearance213.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance213.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid8.DisplayLayout.GroupByBox.PromptAppearance = appearance213;
            this.ultraGrid8.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid8.DisplayLayout.MaxRowScrollRegions = 1;
            appearance214.BackColor = System.Drawing.SystemColors.Window;
            appearance214.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid8.DisplayLayout.Override.ActiveCellAppearance = appearance214;
            appearance215.BackColor = System.Drawing.SystemColors.Highlight;
            appearance215.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid8.DisplayLayout.Override.ActiveRowAppearance = appearance215;
            this.ultraGrid8.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid8.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance216.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid8.DisplayLayout.Override.CardAreaAppearance = appearance216;
            appearance217.BorderColor = System.Drawing.Color.Silver;
            appearance217.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid8.DisplayLayout.Override.CellAppearance = appearance217;
            this.ultraGrid8.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid8.DisplayLayout.Override.CellPadding = 0;
            appearance256.BackColor = System.Drawing.SystemColors.Control;
            appearance256.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance256.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance256.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance256.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid8.DisplayLayout.Override.GroupByRowAppearance = appearance256;
            appearance257.TextHAlignAsString = "Left";
            this.ultraGrid8.DisplayLayout.Override.HeaderAppearance = appearance257;
            this.ultraGrid8.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid8.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance258.BackColor = System.Drawing.SystemColors.Window;
            appearance258.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid8.DisplayLayout.Override.RowAppearance = appearance258;
            this.ultraGrid8.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance259.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid8.DisplayLayout.Override.TemplateAddRowAppearance = appearance259;
            this.ultraGrid8.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid8.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid8.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid8.Name = "ultraGrid8";
            this.ultraGrid8.Size = new System.Drawing.Size(1030, 124);
            this.ultraGrid8.TabIndex = 8;
            this.ultraGrid8.TabStop = false;
            this.ultraGrid8.Text = "ultraGrid3";
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.White;
            this.panel29.Controls.Add(this.label15);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1030, 26);
            this.panel29.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Tan;
            this.label15.Location = new System.Drawing.Point(0, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 12);
            this.label15.TabIndex = 4;
            this.label15.Text = "수용가 현황       ";
            // 
            // chartPanel4
            // 
            this.chartPanel4.BackColor = System.Drawing.Color.White;
            this.chartPanel4.Controls.Add(this.chart4);
            this.chartPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel4.Location = new System.Drawing.Point(0, 0);
            this.chartPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.chartPanel4.Name = "chartPanel4";
            this.chartPanel4.Size = new System.Drawing.Size(1030, 166);
            this.chartPanel4.TabIndex = 2;
            // 
            // chart4
            // 
            this.chart4.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart4.AllSeries.Line.Width = ((short)(1));
            this.chart4.AllSeries.MarkerSize = ((short)(1));
            this.chart4.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart4.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart4.AxisY.Title.Text = "";
            solidBackground4.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart4.Background = solidBackground4;
            this.chart4.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart4.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart4.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart4.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart4.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart4.LegendBox.PlotAreaOnly = false;
            this.chart4.Location = new System.Drawing.Point(0, 0);
            this.chart4.MainPane.Title.Text = "";
            this.chart4.Name = "chart4";
            this.chart4.RandomData.Series = 1;
            this.chart4.Size = new System.Drawing.Size(1030, 166);
            this.chart4.TabIndex = 5;
            this.chart4.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox22.Location = new System.Drawing.Point(1040, 10);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(10, 416);
            this.pictureBox22.TabIndex = 13;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox23.Location = new System.Drawing.Point(0, 10);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(10, 416);
            this.pictureBox23.TabIndex = 12;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox24.Location = new System.Drawing.Point(0, 426);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox24.TabIndex = 11;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox25.Location = new System.Drawing.Point(0, 0);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox25.TabIndex = 10;
            this.pictureBox25.TabStop = false;
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.tabContent5);
            this.ultraTabPageControl5.Controls.Add(this.pictureBox26);
            this.ultraTabPageControl5.Controls.Add(this.pictureBox27);
            this.ultraTabPageControl5.Controls.Add(this.pictureBox28);
            this.ultraTabPageControl5.Controls.Add(this.pictureBox29);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1050, 436);
            // 
            // tabContent5
            // 
            this.tabContent5.Controls.Add(this.tableLayoutPanel5);
            this.tabContent5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContent5.Location = new System.Drawing.Point(10, 10);
            this.tabContent5.Name = "tabContent5";
            this.tabContent5.Size = new System.Drawing.Size(1030, 416);
            this.tabContent5.TabIndex = 14;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.tablePanel5, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.chartPanel5, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1030, 416);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // tablePanel5
            // 
            this.tablePanel5.BackColor = System.Drawing.Color.White;
            this.tablePanel5.Controls.Add(this.tableLayoutPanel55);
            this.tablePanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel5.Location = new System.Drawing.Point(0, 166);
            this.tablePanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel5.Name = "tablePanel5";
            this.tablePanel5.Size = new System.Drawing.Size(1030, 250);
            this.tablePanel5.TabIndex = 5;
            // 
            // tableLayoutPanel55
            // 
            this.tableLayoutPanel55.ColumnCount = 1;
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel55.Controls.Add(this.panel31, 0, 0);
            this.tableLayoutPanel55.Controls.Add(this.panel33, 0, 1);
            this.tableLayoutPanel55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel55.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel55.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel55.Name = "tableLayoutPanel55";
            this.tableLayoutPanel55.RowCount = 2;
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel55.Size = new System.Drawing.Size(1030, 250);
            this.tableLayoutPanel55.TabIndex = 0;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.ultraGrid9);
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Margin = new System.Windows.Forms.Padding(0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1030, 100);
            this.panel31.TabIndex = 0;
            // 
            // ultraGrid9
            // 
            appearance433.BackColor = System.Drawing.SystemColors.Window;
            appearance433.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid9.DisplayLayout.Appearance = appearance433;
            ultraGridColumn19.Header.Caption = "지역관리번호";
            ultraGridColumn19.Header.VisiblePosition = 0;
            ultraGridColumn19.Hidden = true;
            ultraGridColumn2.Header.Caption = "대블록";
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.Width = 60;
            ultraGridColumn3.Header.Caption = "중블록";
            ultraGridColumn3.Header.VisiblePosition = 2;
            ultraGridColumn3.Width = 60;
            ultraGridColumn4.Header.Caption = "소블록";
            ultraGridColumn4.Header.VisiblePosition = 3;
            ultraGridColumn4.Width = 60;
            appearance434.TextHAlignAsString = "Right";
            ultraGridColumn5.CellAppearance = appearance434;
            ultraGridColumn5.DefaultCellValue = "0";
            ultraGridColumn5.Format = "###,###,###";
            ultraGridColumn5.Header.Caption = "전수";
            ultraGridColumn5.Header.VisiblePosition = 4;
            ultraGridColumn5.Width = 60;
            appearance435.TextHAlignAsString = "Right";
            ultraGridColumn102.CellAppearance = appearance435;
            ultraGridColumn102.DefaultCellValue = "0";
            ultraGridColumn102.Format = "###,###,###";
            ultraGridColumn102.Header.Caption = "가구수";
            ultraGridColumn102.Header.VisiblePosition = 5;
            ultraGridColumn102.Width = 60;
            ultraGridBand9.Columns.AddRange(new object[] {
            ultraGridColumn19,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn102});
            appearance436.TextHAlignAsString = "Center";
            ultraGridBand9.Header.Appearance = appearance436;
            this.ultraGrid9.DisplayLayout.BandsSerializer.Add(ultraGridBand9);
            this.ultraGrid9.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid9.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance437.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance437.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance437.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance437.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid9.DisplayLayout.GroupByBox.Appearance = appearance437;
            appearance438.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid9.DisplayLayout.GroupByBox.BandLabelAppearance = appearance438;
            this.ultraGrid9.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance439.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance439.BackColor2 = System.Drawing.SystemColors.Control;
            appearance439.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance439.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid9.DisplayLayout.GroupByBox.PromptAppearance = appearance439;
            this.ultraGrid9.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid9.DisplayLayout.MaxRowScrollRegions = 1;
            appearance440.BackColor = System.Drawing.SystemColors.Window;
            appearance440.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid9.DisplayLayout.Override.ActiveCellAppearance = appearance440;
            appearance441.BackColor = System.Drawing.SystemColors.Highlight;
            appearance441.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid9.DisplayLayout.Override.ActiveRowAppearance = appearance441;
            this.ultraGrid9.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid9.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance442.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid9.DisplayLayout.Override.CardAreaAppearance = appearance442;
            appearance443.BorderColor = System.Drawing.Color.Silver;
            appearance443.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid9.DisplayLayout.Override.CellAppearance = appearance443;
            this.ultraGrid9.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid9.DisplayLayout.Override.CellPadding = 0;
            appearance444.BackColor = System.Drawing.SystemColors.Control;
            appearance444.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance444.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance444.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance444.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid9.DisplayLayout.Override.GroupByRowAppearance = appearance444;
            appearance445.TextHAlignAsString = "Left";
            this.ultraGrid9.DisplayLayout.Override.HeaderAppearance = appearance445;
            this.ultraGrid9.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid9.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance446.BackColor = System.Drawing.SystemColors.Window;
            appearance446.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid9.DisplayLayout.Override.RowAppearance = appearance446;
            this.ultraGrid9.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance447.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid9.DisplayLayout.Override.TemplateAddRowAppearance = appearance447;
            this.ultraGrid9.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid9.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid9.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid9.Name = "ultraGrid9";
            this.ultraGrid9.Size = new System.Drawing.Size(1030, 74);
            this.ultraGrid9.TabIndex = 7;
            this.ultraGrid9.TabStop = false;
            this.ultraGrid9.Text = "ultraGrid2";
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.White;
            this.panel32.Controls.Add(this.label16);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(1030, 26);
            this.panel32.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Tan;
            this.label16.Location = new System.Drawing.Point(0, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 12);
            this.label16.TabIndex = 4;
            this.label16.Text = "블록별 현황       ";
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.ultraGrid10);
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel33.Location = new System.Drawing.Point(0, 100);
            this.panel33.Margin = new System.Windows.Forms.Padding(0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1030, 150);
            this.panel33.TabIndex = 1;
            // 
            // ultraGrid10
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid10.DisplayLayout.Appearance = appearance13;
            ultraGridColumn21.Header.Caption = "";
            ultraGridColumn21.Header.CheckBoxAlignment = Infragistics.Win.UltraWinGrid.HeaderCheckBoxAlignment.Center;
            ultraGridColumn21.Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
            ultraGridColumn21.Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Always;
            ultraGridColumn21.Header.VisiblePosition = 0;
            ultraGridColumn21.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn21.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn21.Width = 42;
            ultraGridColumn36.Header.Caption = "센터";
            ultraGridColumn36.Header.VisiblePosition = 1;
            ultraGridColumn36.Hidden = true;
            ultraGridColumn36.Width = 80;
            ultraGridColumn110.Header.Caption = "대블록";
            ultraGridColumn110.Header.VisiblePosition = 2;
            ultraGridColumn110.Hidden = true;
            ultraGridColumn110.Width = 60;
            ultraGridColumn111.Header.Caption = "중블록";
            ultraGridColumn111.Header.VisiblePosition = 3;
            ultraGridColumn111.Hidden = true;
            ultraGridColumn111.Width = 60;
            ultraGridColumn112.Header.Caption = "소블록";
            ultraGridColumn112.Header.VisiblePosition = 4;
            ultraGridColumn112.Hidden = true;
            ultraGridColumn112.Width = 60;
            ultraGridColumn113.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance14.TextHAlignAsString = "Center";
            ultraGridColumn113.CellAppearance = appearance14;
            ultraGridColumn113.Header.Caption = "주/부";
            ultraGridColumn113.Header.VisiblePosition = 5;
            ultraGridColumn113.Width = 56;
            ultraGridColumn114.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn114.Header.Caption = "수용가번호";
            ultraGridColumn114.Header.VisiblePosition = 6;
            ultraGridColumn114.Width = 100;
            ultraGridColumn115.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn115.Header.Caption = "수용가명";
            ultraGridColumn115.Header.VisiblePosition = 7;
            ultraGridColumn115.Width = 150;
            ultraGridColumn116.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn116.Header.Caption = "수전번호";
            ultraGridColumn116.Header.VisiblePosition = 8;
            ultraGridColumn116.Width = 80;
            ultraGridColumn35.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn35.Header.Caption = "계량기관리번호";
            ultraGridColumn35.Header.VisiblePosition = 9;
            ultraGridColumn35.Width = 115;
            appearance15.TextHAlignAsString = "Center";
            ultraGridColumn117.CellAppearance = appearance15;
            ultraGridColumn117.Header.Caption = "읍면동";
            ultraGridColumn117.Header.VisiblePosition = 10;
            ultraGridColumn117.Width = 60;
            ultraGridColumn118.Header.Caption = "주소";
            ultraGridColumn118.Header.VisiblePosition = 11;
            ultraGridColumn118.Width = 150;
            appearance16.TextHAlignAsString = "Center";
            ultraGridColumn119.CellAppearance = appearance16;
            ultraGridColumn119.Header.Caption = "업종";
            ultraGridColumn119.Header.VisiblePosition = 12;
            ultraGridColumn119.Width = 60;
            appearance17.TextHAlignAsString = "Right";
            ultraGridColumn120.CellAppearance = appearance17;
            ultraGridColumn120.Format = "###,###,###";
            ultraGridColumn120.Header.Caption = "구경";
            ultraGridColumn120.Header.VisiblePosition = 13;
            ultraGridColumn120.Width = 60;
            appearance18.TextHAlignAsString = "Center";
            ultraGridColumn130.CellAppearance = appearance18;
            ultraGridColumn130.Header.Caption = "개전일자";
            ultraGridColumn130.Header.VisiblePosition = 14;
            ultraGridColumn130.Width = 80;
            appearance19.TextHAlignAsString = "Center";
            ultraGridColumn131.CellAppearance = appearance19;
            ultraGridColumn131.Header.Caption = "폐전일자";
            ultraGridColumn131.Header.VisiblePosition = 16;
            ultraGridColumn131.Width = 80;
            appearance20.TextHAlignAsString = "Center";
            ultraGridColumn132.CellAppearance = appearance20;
            ultraGridColumn132.Header.Caption = "중지일자";
            ultraGridColumn132.Header.VisiblePosition = 17;
            ultraGridColumn132.Width = 80;
            appearance21.TextHAlignAsString = "Center";
            ultraGridColumn133.CellAppearance = appearance21;
            ultraGridColumn133.Header.Caption = "교체일자";
            ultraGridColumn133.Header.VisiblePosition = 18;
            ultraGridColumn133.Width = 80;
            appearance22.TextHAlignAsString = "Right";
            ultraGridColumn134.CellAppearance = appearance22;
            ultraGridColumn134.DefaultCellValue = "0";
            ultraGridColumn134.Format = "###,###,###";
            ultraGridColumn134.Header.Caption = "가구수";
            ultraGridColumn134.Header.VisiblePosition = 15;
            ultraGridColumn134.Width = 60;
            ultraGridBand10.Columns.AddRange(new object[] {
            ultraGridColumn21,
            ultraGridColumn36,
            ultraGridColumn110,
            ultraGridColumn111,
            ultraGridColumn112,
            ultraGridColumn113,
            ultraGridColumn114,
            ultraGridColumn115,
            ultraGridColumn116,
            ultraGridColumn35,
            ultraGridColumn117,
            ultraGridColumn118,
            ultraGridColumn119,
            ultraGridColumn120,
            ultraGridColumn130,
            ultraGridColumn131,
            ultraGridColumn132,
            ultraGridColumn133,
            ultraGridColumn134});
            appearance23.TextHAlignAsString = "Center";
            ultraGridBand10.Header.Appearance = appearance23;
            this.ultraGrid10.DisplayLayout.BandsSerializer.Add(ultraGridBand10);
            this.ultraGrid10.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid10.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid10.DisplayLayout.GroupByBox.Appearance = appearance24;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid10.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.ultraGrid10.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance58.BackColor2 = System.Drawing.SystemColors.Control;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid10.DisplayLayout.GroupByBox.PromptAppearance = appearance58;
            this.ultraGrid10.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid10.DisplayLayout.MaxRowScrollRegions = 1;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid10.DisplayLayout.Override.ActiveCellAppearance = appearance59;
            appearance60.BackColor = System.Drawing.SystemColors.Highlight;
            appearance60.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid10.DisplayLayout.Override.ActiveRowAppearance = appearance60;
            this.ultraGrid10.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid10.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid10.DisplayLayout.Override.CardAreaAppearance = appearance61;
            appearance62.BorderColor = System.Drawing.Color.Silver;
            appearance62.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid10.DisplayLayout.Override.CellAppearance = appearance62;
            this.ultraGrid10.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid10.DisplayLayout.Override.CellPadding = 0;
            appearance63.BackColor = System.Drawing.SystemColors.Control;
            appearance63.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance63.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance63.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid10.DisplayLayout.Override.GroupByRowAppearance = appearance63;
            appearance64.TextHAlignAsString = "Left";
            this.ultraGrid10.DisplayLayout.Override.HeaderAppearance = appearance64;
            this.ultraGrid10.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid10.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid10.DisplayLayout.Override.RowAppearance = appearance65;
            this.ultraGrid10.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance66.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid10.DisplayLayout.Override.TemplateAddRowAppearance = appearance66;
            this.ultraGrid10.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid10.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid10.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid10.Name = "ultraGrid10";
            this.ultraGrid10.Size = new System.Drawing.Size(1030, 124);
            this.ultraGrid10.TabIndex = 8;
            this.ultraGrid10.TabStop = false;
            this.ultraGrid10.Text = "ultraGrid3";
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.White;
            this.panel34.Controls.Add(this.BigUsedDelete);
            this.panel34.Controls.Add(this.label17);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel34.Location = new System.Drawing.Point(0, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(1030, 26);
            this.panel34.TabIndex = 5;
            // 
            // BigUsedDelete
            // 
            this.BigUsedDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BigUsedDelete.Location = new System.Drawing.Point(940, 3);
            this.BigUsedDelete.Name = "BigUsedDelete";
            this.BigUsedDelete.Size = new System.Drawing.Size(90, 20);
            this.BigUsedDelete.TabIndex = 6;
            this.BigUsedDelete.Text = "대수용가제외";
            this.BigUsedDelete.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Tan;
            this.label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.label17.Location = new System.Drawing.Point(0, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 12);
            this.label17.TabIndex = 5;
            this.label17.Text = "대수용가 현황    ";
            // 
            // chartPanel5
            // 
            this.chartPanel5.BackColor = System.Drawing.Color.White;
            this.chartPanel5.Controls.Add(this.chart5);
            this.chartPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel5.Location = new System.Drawing.Point(0, 0);
            this.chartPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.chartPanel5.Name = "chartPanel5";
            this.chartPanel5.Size = new System.Drawing.Size(1030, 166);
            this.chartPanel5.TabIndex = 4;
            // 
            // chart5
            // 
            this.chart5.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart5.AllSeries.Line.Width = ((short)(1));
            this.chart5.AllSeries.MarkerSize = ((short)(1));
            this.chart5.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart5.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart5.AxisY.Title.Text = "";
            solidBackground5.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart5.Background = solidBackground5;
            this.chart5.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart5.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart5.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart5.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart5.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart5.LegendBox.PlotAreaOnly = false;
            this.chart5.Location = new System.Drawing.Point(0, 0);
            this.chart5.MainPane.Title.Text = "";
            this.chart5.Name = "chart5";
            this.chart5.RandomData.Series = 1;
            this.chart5.Size = new System.Drawing.Size(1030, 166);
            this.chart5.TabIndex = 5;
            this.chart5.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox26.Location = new System.Drawing.Point(1040, 10);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(10, 416);
            this.pictureBox26.TabIndex = 13;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox27.Location = new System.Drawing.Point(0, 10);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(10, 416);
            this.pictureBox27.TabIndex = 12;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox28.Location = new System.Drawing.Point(0, 426);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox28.TabIndex = 11;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox29.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox29.Location = new System.Drawing.Point(0, 0);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox29.TabIndex = 10;
            this.pictureBox29.TabStop = false;
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.tabContent6);
            this.ultraTabPageControl6.Controls.Add(this.pictureBox30);
            this.ultraTabPageControl6.Controls.Add(this.pictureBox31);
            this.ultraTabPageControl6.Controls.Add(this.pictureBox32);
            this.ultraTabPageControl6.Controls.Add(this.pictureBox33);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1050, 436);
            // 
            // tabContent6
            // 
            this.tabContent6.Controls.Add(this.tableLayoutPanel6);
            this.tabContent6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContent6.Location = new System.Drawing.Point(10, 10);
            this.tabContent6.Name = "tabContent6";
            this.tabContent6.Size = new System.Drawing.Size(1030, 416);
            this.tabContent6.TabIndex = 15;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.tablePanel6, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.chartPanel6, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1030, 416);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // tablePanel6
            // 
            this.tablePanel6.BackColor = System.Drawing.Color.White;
            this.tablePanel6.Controls.Add(this.tableLayoutPanel66);
            this.tablePanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel6.Location = new System.Drawing.Point(0, 166);
            this.tablePanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel6.Name = "tablePanel6";
            this.tablePanel6.Size = new System.Drawing.Size(1030, 250);
            this.tablePanel6.TabIndex = 3;
            // 
            // tableLayoutPanel66
            // 
            this.tableLayoutPanel66.ColumnCount = 1;
            this.tableLayoutPanel66.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel66.Controls.Add(this.panel36, 0, 0);
            this.tableLayoutPanel66.Controls.Add(this.panel38, 0, 1);
            this.tableLayoutPanel66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel66.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel66.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel66.Name = "tableLayoutPanel66";
            this.tableLayoutPanel66.RowCount = 2;
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel66.Size = new System.Drawing.Size(1030, 250);
            this.tableLayoutPanel66.TabIndex = 0;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.ultraGrid11);
            this.panel36.Controls.Add(this.panel37);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Margin = new System.Windows.Forms.Padding(0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(1030, 100);
            this.panel36.TabIndex = 0;
            // 
            // ultraGrid11
            // 
            appearance448.BackColor = System.Drawing.SystemColors.Window;
            appearance448.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid11.DisplayLayout.Appearance = appearance448;
            ultraGridColumn20.Header.Caption = "지역관리번호";
            ultraGridColumn20.Header.VisiblePosition = 0;
            ultraGridColumn20.Hidden = true;
            ultraGridColumn201.Header.Caption = "대블록";
            ultraGridColumn201.Header.VisiblePosition = 1;
            ultraGridColumn201.Width = 60;
            ultraGridColumn202.Header.Caption = "중블록";
            ultraGridColumn202.Header.VisiblePosition = 2;
            ultraGridColumn202.Width = 60;
            ultraGridColumn203.Header.Caption = "소블록";
            ultraGridColumn203.Header.VisiblePosition = 3;
            ultraGridColumn203.Width = 60;
            appearance449.TextHAlignAsString = "Right";
            ultraGridColumn204.CellAppearance = appearance449;
            ultraGridColumn204.DefaultCellValue = "0";
            ultraGridColumn204.Format = "###,###,###";
            ultraGridColumn204.Header.Caption = "전수";
            ultraGridColumn204.Header.VisiblePosition = 4;
            ultraGridColumn204.Width = 60;
            appearance450.TextHAlignAsString = "Right";
            ultraGridColumn205.CellAppearance = appearance450;
            appearance451.TextHAlignAsString = "Right";
            ultraGridColumn205.CellButtonAppearance = appearance451;
            ultraGridColumn205.DefaultCellValue = "0";
            ultraGridColumn205.Format = "###,###,###";
            ultraGridColumn205.Header.Caption = "개전수";
            ultraGridColumn205.Header.VisiblePosition = 5;
            ultraGridColumn205.Width = 60;
            appearance452.TextHAlignAsString = "Right";
            ultraGridColumn206.CellAppearance = appearance452;
            appearance453.TextHAlignAsString = "Right";
            ultraGridColumn206.CellButtonAppearance = appearance453;
            ultraGridColumn206.DefaultCellValue = "0";
            ultraGridColumn206.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn206.Format = "###,###,###";
            ultraGridColumn206.Header.Caption = "폐전수";
            ultraGridColumn206.Header.VisiblePosition = 6;
            ultraGridColumn206.Width = 60;
            appearance454.TextHAlignAsString = "Right";
            ultraGridColumn207.CellAppearance = appearance454;
            appearance455.TextHAlignAsString = "Right";
            ultraGridColumn207.CellButtonAppearance = appearance455;
            ultraGridColumn207.DefaultCellValue = "0";
            ultraGridColumn207.Format = "###,###,###";
            ultraGridColumn207.Header.Caption = "중지전수";
            ultraGridColumn207.Header.VisiblePosition = 7;
            ultraGridColumn207.Width = 60;
            appearance456.TextHAlignAsString = "Right";
            ultraGridColumn208.CellAppearance = appearance456;
            appearance457.TextHAlignAsString = "Right";
            ultraGridColumn208.CellButtonAppearance = appearance457;
            ultraGridColumn208.DefaultCellValue = "0";
            ultraGridColumn208.Format = "###,###,###";
            ultraGridColumn208.Header.Caption = "교체전수";
            ultraGridColumn208.Header.VisiblePosition = 8;
            ultraGridColumn208.Width = 60;
            appearance458.TextHAlignAsString = "Right";
            ultraGridColumn85.CellAppearance = appearance458;
            appearance459.TextHAlignAsString = "Right";
            ultraGridColumn85.CellButtonAppearance = appearance459;
            ultraGridColumn85.DefaultCellValue = "0";
            ultraGridColumn85.Format = "###,###,###";
            ultraGridColumn85.Header.Caption = "가구수";
            ultraGridColumn85.Header.VisiblePosition = 9;
            ultraGridColumn85.Width = 60;
            ultraGridBand11.Columns.AddRange(new object[] {
            ultraGridColumn20,
            ultraGridColumn201,
            ultraGridColumn202,
            ultraGridColumn203,
            ultraGridColumn204,
            ultraGridColumn205,
            ultraGridColumn206,
            ultraGridColumn207,
            ultraGridColumn208,
            ultraGridColumn85});
            appearance460.TextHAlignAsString = "Center";
            ultraGridBand11.Header.Appearance = appearance460;
            this.ultraGrid11.DisplayLayout.BandsSerializer.Add(ultraGridBand11);
            this.ultraGrid11.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid11.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance461.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance461.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance461.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance461.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid11.DisplayLayout.GroupByBox.Appearance = appearance461;
            appearance462.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid11.DisplayLayout.GroupByBox.BandLabelAppearance = appearance462;
            this.ultraGrid11.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance463.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance463.BackColor2 = System.Drawing.SystemColors.Control;
            appearance463.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance463.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid11.DisplayLayout.GroupByBox.PromptAppearance = appearance463;
            this.ultraGrid11.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid11.DisplayLayout.MaxRowScrollRegions = 1;
            appearance464.BackColor = System.Drawing.SystemColors.Window;
            appearance464.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid11.DisplayLayout.Override.ActiveCellAppearance = appearance464;
            appearance465.BackColor = System.Drawing.SystemColors.Highlight;
            appearance465.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid11.DisplayLayout.Override.ActiveRowAppearance = appearance465;
            this.ultraGrid11.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid11.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance466.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid11.DisplayLayout.Override.CardAreaAppearance = appearance466;
            appearance467.BorderColor = System.Drawing.Color.Silver;
            appearance467.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid11.DisplayLayout.Override.CellAppearance = appearance467;
            this.ultraGrid11.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid11.DisplayLayout.Override.CellPadding = 0;
            appearance468.BackColor = System.Drawing.SystemColors.Control;
            appearance468.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance468.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance468.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance468.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid11.DisplayLayout.Override.GroupByRowAppearance = appearance468;
            appearance469.TextHAlignAsString = "Left";
            this.ultraGrid11.DisplayLayout.Override.HeaderAppearance = appearance469;
            this.ultraGrid11.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid11.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance470.BackColor = System.Drawing.SystemColors.Window;
            appearance470.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid11.DisplayLayout.Override.RowAppearance = appearance470;
            this.ultraGrid11.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance471.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid11.DisplayLayout.Override.TemplateAddRowAppearance = appearance471;
            this.ultraGrid11.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid11.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid11.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid11.Name = "ultraGrid11";
            this.ultraGrid11.Size = new System.Drawing.Size(1030, 74);
            this.ultraGrid11.TabIndex = 6;
            this.ultraGrid11.TabStop = false;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.White;
            this.panel37.Controls.Add(this.label18);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(1030, 26);
            this.panel37.TabIndex = 4;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Tan;
            this.label18.Location = new System.Drawing.Point(0, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 12);
            this.label18.TabIndex = 5;
            this.label18.Text = "블록별 현황       ";
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.ultraGrid12);
            this.panel38.Controls.Add(this.panel39);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel38.Location = new System.Drawing.Point(0, 100);
            this.panel38.Margin = new System.Windows.Forms.Padding(0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(1030, 150);
            this.panel38.TabIndex = 1;
            // 
            // ultraGrid12
            // 
            appearance178.BackColor = System.Drawing.SystemColors.Window;
            appearance178.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid12.DisplayLayout.Appearance = appearance178;
            ultraGridColumn26.Header.Caption = "대블록";
            ultraGridColumn26.Header.VisiblePosition = 0;
            ultraGridColumn26.Hidden = true;
            ultraGridColumn26.Width = 60;
            ultraGridColumn27.Header.Caption = "중블록";
            ultraGridColumn27.Header.VisiblePosition = 1;
            ultraGridColumn27.Hidden = true;
            ultraGridColumn27.Width = 60;
            ultraGridColumn28.Header.Caption = "소블록";
            ultraGridColumn28.Header.VisiblePosition = 2;
            ultraGridColumn28.Hidden = true;
            ultraGridColumn28.Width = 60;
            ultraGridColumn29.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance179.TextHAlignAsString = "Center";
            ultraGridColumn29.CellAppearance = appearance179;
            ultraGridColumn29.Header.Caption = "주/부";
            ultraGridColumn29.Header.VisiblePosition = 3;
            ultraGridColumn29.Width = 54;
            ultraGridColumn30.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn30.Header.Caption = "수용가번호";
            ultraGridColumn30.Header.VisiblePosition = 4;
            ultraGridColumn30.Width = 100;
            ultraGridColumn31.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn31.Header.Caption = "수용가명";
            ultraGridColumn31.Header.VisiblePosition = 5;
            ultraGridColumn31.Width = 150;
            ultraGridColumn32.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn32.Header.Caption = "수전번호";
            ultraGridColumn32.Header.VisiblePosition = 6;
            ultraGridColumn32.Width = 80;
            ultraGridColumn37.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn37.Header.Caption = "계량기관리번호";
            ultraGridColumn37.Header.VisiblePosition = 7;
            ultraGridColumn37.Width = 115;
            appearance180.TextHAlignAsString = "Center";
            ultraGridColumn33.CellAppearance = appearance180;
            ultraGridColumn33.Header.Caption = "읍면동";
            ultraGridColumn33.Header.VisiblePosition = 8;
            ultraGridColumn33.Width = 60;
            ultraGridColumn34.Header.Caption = "주소";
            ultraGridColumn34.Header.VisiblePosition = 9;
            ultraGridColumn34.Width = 150;
            appearance181.TextHAlignAsString = "Center";
            ultraGridColumn77.CellAppearance = appearance181;
            ultraGridColumn77.Header.Caption = "업종";
            ultraGridColumn77.Header.VisiblePosition = 10;
            ultraGridColumn77.Width = 60;
            appearance182.TextHAlignAsString = "Center";
            ultraGridColumn78.CellAppearance = appearance182;
            ultraGridColumn78.Header.Caption = "교체구분";
            ultraGridColumn78.Header.VisiblePosition = 11;
            ultraGridColumn78.Width = 80;
            appearance183.TextHAlignAsString = "Center";
            ultraGridColumn79.CellAppearance = appearance183;
            ultraGridColumn79.Header.Caption = "교체일자";
            ultraGridColumn79.Header.VisiblePosition = 12;
            ultraGridColumn79.Width = 80;
            ultraGridColumn80.Header.Caption = "교체전제작번호";
            ultraGridColumn80.Header.VisiblePosition = 13;
            ultraGridColumn80.Width = 100;
            appearance184.TextHAlignAsString = "Right";
            ultraGridColumn81.CellAppearance = appearance184;
            ultraGridColumn81.Header.Caption = "교체전구경";
            ultraGridColumn81.Header.VisiblePosition = 14;
            ultraGridColumn81.Width = 80;
            appearance185.TextHAlignAsString = "Right";
            ultraGridColumn82.CellAppearance = appearance185;
            ultraGridColumn82.Header.Caption = "교체전지침";
            ultraGridColumn82.Header.VisiblePosition = 15;
            ultraGridColumn82.Width = 80;
            ultraGridColumn83.Header.Caption = "교체후제작번호";
            ultraGridColumn83.Header.VisiblePosition = 16;
            ultraGridColumn83.Width = 100;
            appearance186.TextHAlignAsString = "Right";
            ultraGridColumn84.CellAppearance = appearance186;
            ultraGridColumn84.Header.Caption = "교체후구경";
            ultraGridColumn84.Header.VisiblePosition = 17;
            ultraGridColumn84.Width = 80;
            appearance187.TextHAlignAsString = "Right";
            ultraGridColumn127.CellAppearance = appearance187;
            ultraGridColumn127.Header.Caption = "교체후지침";
            ultraGridColumn127.Header.VisiblePosition = 18;
            ultraGridColumn127.Width = 80;
            appearance188.TextHAlignAsString = "Center";
            ultraGridColumn148.CellAppearance = appearance188;
            ultraGridColumn148.Header.Caption = "검침적용년월";
            ultraGridColumn148.Header.VisiblePosition = 19;
            ultraGridColumn148.Width = 100;
            ultraGridColumn128.Header.Caption = "변경사유";
            ultraGridColumn128.Header.VisiblePosition = 20;
            ultraGridColumn128.Width = 150;
            ultraGridBand12.Columns.AddRange(new object[] {
            ultraGridColumn26,
            ultraGridColumn27,
            ultraGridColumn28,
            ultraGridColumn29,
            ultraGridColumn30,
            ultraGridColumn31,
            ultraGridColumn32,
            ultraGridColumn37,
            ultraGridColumn33,
            ultraGridColumn34,
            ultraGridColumn77,
            ultraGridColumn78,
            ultraGridColumn79,
            ultraGridColumn80,
            ultraGridColumn81,
            ultraGridColumn82,
            ultraGridColumn83,
            ultraGridColumn84,
            ultraGridColumn127,
            ultraGridColumn148,
            ultraGridColumn128});
            appearance189.TextHAlignAsString = "Center";
            ultraGridBand12.Header.Appearance = appearance189;
            appearance190.BackColor = System.Drawing.Color.LightYellow;
            ultraGridBand12.Override.MergedCellAppearance = appearance190;
            this.ultraGrid12.DisplayLayout.BandsSerializer.Add(ultraGridBand12);
            this.ultraGrid12.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid12.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance191.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance191.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance191.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance191.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid12.DisplayLayout.GroupByBox.Appearance = appearance191;
            appearance192.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid12.DisplayLayout.GroupByBox.BandLabelAppearance = appearance192;
            this.ultraGrid12.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance193.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance193.BackColor2 = System.Drawing.SystemColors.Control;
            appearance193.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance193.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid12.DisplayLayout.GroupByBox.PromptAppearance = appearance193;
            this.ultraGrid12.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid12.DisplayLayout.MaxRowScrollRegions = 1;
            appearance194.BackColor = System.Drawing.SystemColors.Window;
            appearance194.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid12.DisplayLayout.Override.ActiveCellAppearance = appearance194;
            appearance195.BackColor = System.Drawing.SystemColors.Highlight;
            appearance195.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid12.DisplayLayout.Override.ActiveRowAppearance = appearance195;
            this.ultraGrid12.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid12.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance196.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid12.DisplayLayout.Override.CardAreaAppearance = appearance196;
            appearance197.BorderColor = System.Drawing.Color.Silver;
            appearance197.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid12.DisplayLayout.Override.CellAppearance = appearance197;
            this.ultraGrid12.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid12.DisplayLayout.Override.CellPadding = 0;
            appearance218.BackColor = System.Drawing.SystemColors.Control;
            appearance218.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance218.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance218.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance218.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid12.DisplayLayout.Override.GroupByRowAppearance = appearance218;
            appearance219.TextHAlignAsString = "Left";
            this.ultraGrid12.DisplayLayout.Override.HeaderAppearance = appearance219;
            this.ultraGrid12.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid12.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance260.BackColor = System.Drawing.SystemColors.Window;
            appearance260.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid12.DisplayLayout.Override.RowAppearance = appearance260;
            this.ultraGrid12.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance261.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid12.DisplayLayout.Override.TemplateAddRowAppearance = appearance261;
            this.ultraGrid12.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid12.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid12.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid12.Name = "ultraGrid12";
            this.ultraGrid12.Size = new System.Drawing.Size(1030, 124);
            this.ultraGrid12.TabIndex = 9;
            this.ultraGrid12.TabStop = false;
            this.ultraGrid12.Text = "ultraGrid3";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.White;
            this.panel39.Controls.Add(this.label19);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel39.Location = new System.Drawing.Point(0, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(1030, 26);
            this.panel39.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Tan;
            this.label19.Location = new System.Drawing.Point(0, 11);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(97, 12);
            this.label19.TabIndex = 6;
            this.label19.Text = "수용가 현황       ";
            // 
            // chartPanel6
            // 
            this.chartPanel6.BackColor = System.Drawing.Color.White;
            this.chartPanel6.Controls.Add(this.chart6);
            this.chartPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel6.Location = new System.Drawing.Point(0, 0);
            this.chartPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.chartPanel6.Name = "chartPanel6";
            this.chartPanel6.Size = new System.Drawing.Size(1030, 166);
            this.chartPanel6.TabIndex = 2;
            // 
            // chart6
            // 
            this.chart6.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart6.AllSeries.Line.Width = ((short)(1));
            this.chart6.AllSeries.MarkerSize = ((short)(1));
            this.chart6.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart6.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart6.AxisY.Title.Text = "";
            solidBackground6.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart6.Background = solidBackground6;
            this.chart6.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart6.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart6.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart6.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart6.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart6.LegendBox.PlotAreaOnly = false;
            this.chart6.Location = new System.Drawing.Point(0, 0);
            this.chart6.MainPane.Title.Text = "";
            this.chart6.Name = "chart6";
            this.chart6.RandomData.Series = 1;
            this.chart6.Size = new System.Drawing.Size(1030, 166);
            this.chart6.TabIndex = 5;
            this.chart6.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox30.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox30.Location = new System.Drawing.Point(1040, 10);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(10, 416);
            this.pictureBox30.TabIndex = 13;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox31.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox31.Location = new System.Drawing.Point(0, 10);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(10, 416);
            this.pictureBox31.TabIndex = 12;
            this.pictureBox31.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox32.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox32.Location = new System.Drawing.Point(0, 426);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox32.TabIndex = 11;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox33.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox33.Location = new System.Drawing.Point(0, 0);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox33.TabIndex = 10;
            this.pictureBox33.TabStop = false;
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.tabContent7);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox34);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox35);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox36);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox37);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(1050, 436);
            // 
            // tabContent7
            // 
            this.tabContent7.Controls.Add(this.tableLayoutPanel7);
            this.tabContent7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContent7.Location = new System.Drawing.Point(10, 10);
            this.tabContent7.Name = "tabContent7";
            this.tabContent7.Size = new System.Drawing.Size(1030, 416);
            this.tabContent7.TabIndex = 16;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.Controls.Add(this.tablePanel7, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.chartPanel7, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1030, 416);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // tablePanel7
            // 
            this.tablePanel7.BackColor = System.Drawing.Color.White;
            this.tablePanel7.Controls.Add(this.tableLayoutPanel77);
            this.tablePanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel7.Location = new System.Drawing.Point(0, 166);
            this.tablePanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel7.Name = "tablePanel7";
            this.tablePanel7.Size = new System.Drawing.Size(1030, 250);
            this.tablePanel7.TabIndex = 3;
            // 
            // tableLayoutPanel77
            // 
            this.tableLayoutPanel77.ColumnCount = 1;
            this.tableLayoutPanel77.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel77.Controls.Add(this.panel41, 0, 0);
            this.tableLayoutPanel77.Controls.Add(this.panel43, 0, 1);
            this.tableLayoutPanel77.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel77.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel77.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel77.Name = "tableLayoutPanel77";
            this.tableLayoutPanel77.RowCount = 2;
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel77.Size = new System.Drawing.Size(1030, 250);
            this.tableLayoutPanel77.TabIndex = 0;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.ultraGrid13);
            this.panel41.Controls.Add(this.panel42);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel41.Location = new System.Drawing.Point(0, 0);
            this.panel41.Margin = new System.Windows.Forms.Padding(0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(1030, 100);
            this.panel41.TabIndex = 0;
            // 
            // ultraGrid13
            // 
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid13.DisplayLayout.Appearance = appearance52;
            ultraGridColumn22.Header.Caption = "지역관리번호";
            ultraGridColumn22.Header.VisiblePosition = 0;
            ultraGridColumn22.Hidden = true;
            ultraGridColumn169.Header.Caption = "대블록";
            ultraGridColumn169.Header.VisiblePosition = 1;
            ultraGridColumn169.Width = 60;
            ultraGridColumn170.Header.Caption = "중블록";
            ultraGridColumn170.Header.VisiblePosition = 2;
            ultraGridColumn170.Width = 60;
            ultraGridColumn171.Header.Caption = "소블록";
            ultraGridColumn171.Header.VisiblePosition = 3;
            ultraGridColumn171.Width = 60;
            appearance53.TextHAlignAsString = "Center";
            ultraGridColumn7.CellAppearance = appearance53;
            ultraGridColumn7.Header.Caption = "관로구분";
            ultraGridColumn7.Header.VisiblePosition = 4;
            ultraGridColumn7.Width = 70;
            ultraGridBand13.Columns.AddRange(new object[] {
            ultraGridColumn22,
            ultraGridColumn169,
            ultraGridColumn170,
            ultraGridColumn171,
            ultraGridColumn7});
            appearance77.TextHAlignAsString = "Center";
            ultraGridBand13.Header.Appearance = appearance77;
            this.ultraGrid13.DisplayLayout.BandsSerializer.Add(ultraGridBand13);
            this.ultraGrid13.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid13.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance78.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance78.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance78.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid13.DisplayLayout.GroupByBox.Appearance = appearance78;
            appearance79.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid13.DisplayLayout.GroupByBox.BandLabelAppearance = appearance79;
            this.ultraGrid13.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance80.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance80.BackColor2 = System.Drawing.SystemColors.Control;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance80.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid13.DisplayLayout.GroupByBox.PromptAppearance = appearance80;
            this.ultraGrid13.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid13.DisplayLayout.MaxRowScrollRegions = 1;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid13.DisplayLayout.Override.ActiveCellAppearance = appearance81;
            appearance84.BackColor = System.Drawing.SystemColors.Highlight;
            appearance84.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid13.DisplayLayout.Override.ActiveRowAppearance = appearance84;
            this.ultraGrid13.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid13.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid13.DisplayLayout.Override.CardAreaAppearance = appearance85;
            appearance86.BorderColor = System.Drawing.Color.Silver;
            appearance86.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid13.DisplayLayout.Override.CellAppearance = appearance86;
            this.ultraGrid13.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid13.DisplayLayout.Override.CellPadding = 0;
            appearance87.BackColor = System.Drawing.SystemColors.Control;
            appearance87.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance87.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance87.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance87.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid13.DisplayLayout.Override.GroupByRowAppearance = appearance87;
            appearance88.TextHAlignAsString = "Left";
            this.ultraGrid13.DisplayLayout.Override.HeaderAppearance = appearance88;
            this.ultraGrid13.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid13.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid13.DisplayLayout.Override.RowAppearance = appearance89;
            this.ultraGrid13.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid13.DisplayLayout.Override.TemplateAddRowAppearance = appearance90;
            this.ultraGrid13.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid13.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid13.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid13.Name = "ultraGrid13";
            this.ultraGrid13.Size = new System.Drawing.Size(1030, 74);
            this.ultraGrid13.TabIndex = 5;
            this.ultraGrid13.TabStop = false;
            this.ultraGrid13.Text = "ultraGrid13";
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.White;
            this.panel42.Controls.Add(this.label20);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel42.Location = new System.Drawing.Point(0, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(1030, 26);
            this.panel42.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Tan;
            this.label20.Location = new System.Drawing.Point(0, 11);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 12);
            this.label20.TabIndex = 6;
            this.label20.Text = "블록별 현황       ";
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.ultraGrid14);
            this.panel43.Controls.Add(this.panel44);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel43.Location = new System.Drawing.Point(0, 100);
            this.panel43.Margin = new System.Windows.Forms.Padding(0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(1030, 150);
            this.panel43.TabIndex = 1;
            // 
            // ultraGrid14
            // 
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid14.DisplayLayout.Appearance = appearance50;
            ultraGridColumn23.Header.Caption = "지역관리번호";
            ultraGridColumn23.Header.VisiblePosition = 0;
            ultraGridColumn23.Hidden = true;
            ultraGridColumn175.Header.Caption = "대블록";
            ultraGridColumn175.Header.VisiblePosition = 1;
            ultraGridColumn175.Width = 60;
            ultraGridColumn176.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn176.Header.Caption = "중블록";
            ultraGridColumn176.Header.VisiblePosition = 2;
            ultraGridColumn176.Width = 68;
            ultraGridColumn185.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn185.Header.Caption = "소블록";
            ultraGridColumn185.Header.VisiblePosition = 3;
            ultraGridColumn185.Width = 70;
            ultraGridColumn6.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance51.TextHAlignAsString = "Center";
            ultraGridColumn6.CellAppearance = appearance51;
            ultraGridColumn6.Header.Caption = "관로구분";
            ultraGridColumn6.Header.VisiblePosition = 4;
            ultraGridColumn6.Width = 87;
            ultraGridColumn8.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn8.Header.Caption = "관리번호";
            ultraGridColumn8.Header.VisiblePosition = 5;
            ultraGridColumn8.Width = 94;
            ultraGridColumn9.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn9.Header.Caption = "관로용도";
            ultraGridColumn9.Header.VisiblePosition = 6;
            ultraGridColumn9.Width = 114;
            ultraGridColumn24.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn24.Header.Caption = "관로재질";
            ultraGridColumn24.Header.VisiblePosition = 7;
            ultraGridColumn24.Width = 118;
            ultraGridColumn10.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance54.TextHAlignAsString = "Right";
            ultraGridColumn10.CellAppearance = appearance54;
            ultraGridColumn10.Header.Caption = "관로구경";
            ultraGridColumn10.Header.VisiblePosition = 8;
            ultraGridColumn10.Width = 87;
            ultraGridColumn12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance55.TextHAlignAsString = "Right";
            ultraGridColumn12.CellAppearance = appearance55;
            ultraGridColumn12.Format = "###,###,###";
            ultraGridColumn12.Header.Caption = "관로길이(m)";
            ultraGridColumn12.Header.VisiblePosition = 9;
            ultraGridColumn12.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            ultraGridColumn12.Width = 102;
            ultraGridBand14.Columns.AddRange(new object[] {
            ultraGridColumn23,
            ultraGridColumn175,
            ultraGridColumn176,
            ultraGridColumn185,
            ultraGridColumn6,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn24,
            ultraGridColumn10,
            ultraGridColumn12});
            appearance56.TextHAlignAsString = "Center";
            ultraGridBand14.Header.Appearance = appearance56;
            this.ultraGrid14.DisplayLayout.BandsSerializer.Add(ultraGridBand14);
            this.ultraGrid14.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid14.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance57.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid14.DisplayLayout.GroupByBox.Appearance = appearance57;
            appearance67.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid14.DisplayLayout.GroupByBox.BandLabelAppearance = appearance67;
            this.ultraGrid14.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance68.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance68.BackColor2 = System.Drawing.SystemColors.Control;
            appearance68.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance68.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid14.DisplayLayout.GroupByBox.PromptAppearance = appearance68;
            this.ultraGrid14.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid14.DisplayLayout.MaxRowScrollRegions = 1;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid14.DisplayLayout.Override.ActiveCellAppearance = appearance69;
            appearance70.BackColor = System.Drawing.SystemColors.Highlight;
            appearance70.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid14.DisplayLayout.Override.ActiveRowAppearance = appearance70;
            this.ultraGrid14.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid14.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid14.DisplayLayout.Override.CardAreaAppearance = appearance71;
            appearance72.BorderColor = System.Drawing.Color.Silver;
            appearance72.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid14.DisplayLayout.Override.CellAppearance = appearance72;
            this.ultraGrid14.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid14.DisplayLayout.Override.CellPadding = 0;
            appearance73.BackColor = System.Drawing.SystemColors.Control;
            appearance73.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance73.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance73.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid14.DisplayLayout.Override.GroupByRowAppearance = appearance73;
            appearance74.TextHAlignAsString = "Left";
            this.ultraGrid14.DisplayLayout.Override.HeaderAppearance = appearance74;
            this.ultraGrid14.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid14.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid14.DisplayLayout.Override.RowAppearance = appearance75;
            this.ultraGrid14.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid14.DisplayLayout.Override.TemplateAddRowAppearance = appearance76;
            this.ultraGrid14.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid14.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid14.Location = new System.Drawing.Point(0, 26);
            this.ultraGrid14.Name = "ultraGrid14";
            this.ultraGrid14.Size = new System.Drawing.Size(1030, 124);
            this.ultraGrid14.TabIndex = 6;
            this.ultraGrid14.TabStop = false;
            this.ultraGrid14.Text = "ultraGrid14";
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.White;
            this.panel44.Controls.Add(this.label21);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel44.Location = new System.Drawing.Point(0, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(1030, 26);
            this.panel44.TabIndex = 5;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Tan;
            this.label21.Location = new System.Drawing.Point(0, 11);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 12);
            this.label21.TabIndex = 7;
            this.label21.Text = "관로 현황          ";
            // 
            // chartPanel7
            // 
            this.chartPanel7.BackColor = System.Drawing.Color.White;
            this.chartPanel7.Controls.Add(this.chart7);
            this.chartPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel7.Location = new System.Drawing.Point(0, 0);
            this.chartPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.chartPanel7.Name = "chartPanel7";
            this.chartPanel7.Size = new System.Drawing.Size(1030, 166);
            this.chartPanel7.TabIndex = 2;
            // 
            // chart7
            // 
            this.chart7.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart7.AllSeries.Line.Width = ((short)(1));
            this.chart7.AllSeries.MarkerSize = ((short)(1));
            this.chart7.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart7.AxisY.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Number;
            this.chart7.AxisY.Title.Text = "";
            solidBackground7.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart7.Background = solidBackground7;
            this.chart7.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart7.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart7.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart7.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart7.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart7.LegendBox.PlotAreaOnly = false;
            this.chart7.Location = new System.Drawing.Point(0, 0);
            this.chart7.MainPane.Title.Text = "";
            this.chart7.Name = "chart7";
            this.chart7.RandomData.Series = 2;
            this.chart7.Size = new System.Drawing.Size(1030, 166);
            this.chart7.TabIndex = 5;
            this.chart7.TabStop = false;
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox34.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox34.Location = new System.Drawing.Point(1040, 10);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(10, 416);
            this.pictureBox34.TabIndex = 13;
            this.pictureBox34.TabStop = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox35.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox35.Location = new System.Drawing.Point(0, 10);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(10, 416);
            this.pictureBox35.TabIndex = 12;
            this.pictureBox35.TabStop = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox36.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox36.Location = new System.Drawing.Point(0, 426);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox36.TabIndex = 11;
            this.pictureBox36.TabStop = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox37.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox37.Location = new System.Drawing.Point(0, 0);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(1050, 10);
            this.pictureBox37.TabIndex = 10;
            this.pictureBox37.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(1064, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 603);
            this.pictureBox4.TabIndex = 16;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 603);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 613);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1074, 10);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1074, 10);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // chartVisibleBtn
            // 
            this.chartVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chartVisibleBtn.Location = new System.Drawing.Point(856, 2);
            this.chartVisibleBtn.Name = "chartVisibleBtn";
            this.chartVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.chartVisibleBtn.TabIndex = 17;
            this.chartVisibleBtn.TabStop = false;
            this.chartVisibleBtn.Text = "그래프";
            this.chartVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1014, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 14;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(968, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 15;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // tableVisibleBtn
            // 
            this.tableVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableVisibleBtn.Location = new System.Drawing.Point(912, 2);
            this.tableVisibleBtn.Name = "tableVisibleBtn";
            this.tableVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.tableVisibleBtn.TabIndex = 16;
            this.tableVisibleBtn.TabStop = false;
            this.tableVisibleBtn.Text = "테이블";
            this.tableVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabControl1.Controls.Add(this.ultraTabPageControl1);
            this.tabControl1.Controls.Add(this.ultraTabPageControl2);
            this.tabControl1.Controls.Add(this.ultraTabPageControl3);
            this.tabControl1.Controls.Add(this.ultraTabPageControl4);
            this.tabControl1.Controls.Add(this.ultraTabPageControl5);
            this.tabControl1.Controls.Add(this.ultraTabPageControl6);
            this.tabControl1.Controls.Add(this.ultraTabPageControl7);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(10, 151);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabControl1.Size = new System.Drawing.Size(1054, 462);
            this.tabControl1.TabIndex = 21;
            ultraTab8.TabPage = this.ultraTabPageControl1;
            ultraTab8.Text = "사용량";
            ultraTab9.TabPage = this.ultraTabPageControl2;
            ultraTab9.Text = "업종별사용량";
            ultraTab10.TabPage = this.ultraTabPageControl3;
            ultraTab10.Text = "업종별가구수";
            ultraTab11.TabPage = this.ultraTabPageControl4;
            ultraTab11.Text = "급수전";
            ultraTab12.TabPage = this.ultraTabPageControl5;
            ultraTab12.Text = "대수용가사용량";
            ultraTab13.TabPage = this.ultraTabPageControl6;
            ultraTab13.Text = "계량기교체";
            ultraTab14.TabPage = this.ultraTabPageControl7;
            ultraTab14.Text = "관로";
            this.tabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab8,
            ultraTab9,
            ultraTab10,
            ultraTab11,
            ultraTab12,
            ultraTab13,
            ultraTab14});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1050, 436);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.searchBtn);
            this.panel1.Controls.Add(this.excelBtn);
            this.panel1.Controls.Add(this.tableVisibleBtn);
            this.panel1.Controls.Add(this.chartVisibleBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 121);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1054, 30);
            this.panel1.TabIndex = 22;
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1054, 111);
            this.searchBox1.TabIndex = 19;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 623);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.MinimumSize = new System.Drawing.Size(650, 650);
            this.Name = "frmMain";
            this.Text = "블록별 일반현황 정보 조회";
            this.ultraTabPageControl1.ResumeLayout(false);
            this.tabContent1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.chartPanel1.ResumeLayout(false);
            this.chartPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tablePanel1.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.tabContent2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tablePanel2.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid3)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid4)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.chartPanel2.ResumeLayout(false);
            this.chartPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.tabContent3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tablePanel3.ResumeLayout(false);
            this.tableLayoutPanel33.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid5)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid6)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.chartPanel3.ResumeLayout(false);
            this.chartPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.tabContent4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tablePanel4.ResumeLayout(false);
            this.tableLayoutPanel44.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid7)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid8)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.chartPanel4.ResumeLayout(false);
            this.chartPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            this.ultraTabPageControl5.ResumeLayout(false);
            this.tabContent5.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tablePanel5.ResumeLayout(false);
            this.tableLayoutPanel55.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid9)).EndInit();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid10)).EndInit();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.chartPanel5.ResumeLayout(false);
            this.chartPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            this.ultraTabPageControl6.ResumeLayout(false);
            this.tabContent6.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tablePanel6.ResumeLayout(false);
            this.tableLayoutPanel66.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid11)).EndInit();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid12)).EndInit();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.chartPanel6.ResumeLayout(false);
            this.chartPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            this.ultraTabPageControl7.ResumeLayout(false);
            this.tabContent7.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tablePanel7.ResumeLayout(false);
            this.tableLayoutPanel77.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid13)).EndInit();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid14)).EndInit();
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.chartPanel7.ResumeLayout(false);
            this.chartPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Button chartVisibleBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.Button tableVisibleBtn;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.Panel tabContent1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel chartPanel1;
        private ChartFX.WinForms.Chart chart1;
        private System.Windows.Forms.Panel tablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Panel panel7;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel17;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid2;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.Panel tabContent2;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private System.Windows.Forms.Panel tabContent3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private System.Windows.Forms.Panel tabContent4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private System.Windows.Forms.Panel tabContent5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private System.Windows.Forms.Panel tabContent6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox33;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        private System.Windows.Forms.Panel tabContent7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.Panel chartPanel3;
        private ChartFX.WinForms.Chart chart3;
        private System.Windows.Forms.Panel tablePanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.Panel panel21;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid5;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel23;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid6;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel chartPanel4;
        private ChartFX.WinForms.Chart chart4;
        private System.Windows.Forms.Panel tablePanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.Panel panel26;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid7;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel28;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid8;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel chartPanel5;
        private ChartFX.WinForms.Chart chart5;
        private System.Windows.Forms.Panel tablePanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel55;
        private System.Windows.Forms.Panel panel31;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid9;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel33;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid10;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel chartPanel6;
        private ChartFX.WinForms.Chart chart6;
        private System.Windows.Forms.Panel tablePanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel66;
        private System.Windows.Forms.Panel panel36;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid11;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel38;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid12;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel chartPanel7;
        private ChartFX.WinForms.Chart chart7;
        private System.Windows.Forms.Panel tablePanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel77;
        private System.Windows.Forms.Panel panel41;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid13;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel43;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid14;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox UsedDMCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button UsedFilteringBtn;
        private System.Windows.Forms.Button BigUsedUpdate;
        private System.Windows.Forms.Button BigUsedDelete;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel tablePanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Panel panel13;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid3;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel15;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid4;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel chartPanel2;
        private ChartFX.WinForms.Chart chart2;
    }
}