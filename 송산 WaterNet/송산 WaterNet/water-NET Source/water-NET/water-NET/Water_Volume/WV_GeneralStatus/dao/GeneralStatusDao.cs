﻿using System.Collections;
using System.Text;
using WaterNet.WaterNetCore;
using System.Data;
using Oracle.DataAccess.Client;
using WaterNet.WV_Common.dao;
using System;

namespace WaterNet.WV_GeneralStatus.dao
{
    /// <summary>
    /// 일반현황 DataAcessObject Class
    /// </summary>
    public class GeneralStatusDao : BaseDao
    {
        private static GeneralStatusDao dao = null;

        private GeneralStatusDao() { }

        public static GeneralStatusDao GetInstance()
        {
            if (dao == null)
            {
                dao = new GeneralStatusDao();
            }
            return dao;
        }

        //블록별 기본정보
        public void SelectBlockInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            //대블록선택 (중블록 전체)
            //중블록선택 (소블록 전체)
            //소블록선택 (소블록)
            query.AppendLine("with loc as                                                                                                                   ");
            query.AppendLine("(																																");
            query.AppendLine("select c1.loc_code																											");
            query.AppendLine("	  ,c1.sgccd																													");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))	");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock											");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))	");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn											");
            query.AppendLine("	  ,c1.ord																													");
            query.AppendLine("  from																														");
            query.AppendLine("	  (																															");
            query.AppendLine("	   select sgccd																												");
            query.AppendLine("			 ,loc_code																											");
            query.AppendLine("			 ,ploc_code																											");
            query.AppendLine("			 ,loc_name																											");
            query.AppendLine("			 ,ftr_idn																											");
            query.AppendLine("			 ,ftr_code																											");
            query.AppendLine("			 ,res_code																											");
            query.AppendLine("			 ,kt_gbn																											");
            query.AppendLine("			 ,rel_loc_name																										");
            query.AppendLine("			 ,rownum ord																										");
            query.AppendLine("		 from cm_location																										");
            query.AppendLine("		where 1 = 1																												");
            query.AppendLine("        and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')                                                            ");
            query.AppendLine("		 start with loc_code = :LOC_CODE																						");
            query.AppendLine("		 connect by prior loc_code = ploc_code																					");
            query.AppendLine("	 	 order SIBLINGS by ftr_idn																								");
            query.AppendLine("	   ) c1																														");
            query.AppendLine("	    ,cm_location c2																											");
            query.AppendLine("	    ,cm_location c3																											");
            query.AppendLine(" where 1 = 1																													");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							");
            query.AppendLine(" order by c1.ord																												");
            query.AppendLine(")																																");
            query.AppendLine("select loc.loc_code																											");
            query.AppendLine("      ,loc.lblock																												");
            query.AppendLine("      ,loc.mblock																												");
            query.AppendLine("	    ,loc.sblock																												");
            query.AppendLine("	    ,to_number(wco.junsu) junsu																								");
            query.AppendLine("	    ,to_number(wco.gejunsu) gejunsu																							");
            query.AppendLine("	    ,to_number(wco.pejunsu) pejunsu																							");
            query.AppendLine("	    ,to_number(wco.stopjunsu) stopjunsu																						");
            query.AppendLine("	    ,(																														");
            query.AppendLine("	     select to_number(sum(mtrchgsu))																						");
            query.AppendLine("		   from wv_consumer_day																									");
            query.AppendLine("        where loc_code = loc.loc_code																							");
            query.AppendLine("	  	  group by loc_code																										");
            query.AppendLine("	     ) changejunsu																											");
            query.AppendLine("      ,(																														");
            query.AppendLine("       select (gajung_gagusu+bgajung_gagusu)																					");
            query.AppendLine("         from wv_consumer_day																									");
            query.AppendLine("        where loc_code = loc.loc_code																							");
            query.AppendLine("          and datee = 																										");
            query.AppendLine("              (select max(datee) from wv_consumer_day where loc_code = loc.loc_code)											");
            query.AppendLine("      ) gagusu																												");
            query.AppendLine("  from loc																													");
            query.AppendLine("      ,wv_consumer wco																										");
            query.AppendLine(" where wco.loc_code = loc.loc_code																							");
            query.AppendLine(" order by loc.ord																							                    ");

            IDataParameter[] parameters =  {
                      new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
	                 ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록별 사용량정보
        public void SelectBlockUsed(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                     ");
            query.AppendLine("(																				  ");
            query.AppendLine("select c1.loc_code															  ");
            query.AppendLine("      ,tmp.year_mon															  ");
            query.AppendLine("	  ,c1.ord																	  ");
            query.AppendLine("  from																		  ");
            query.AppendLine("	  (																			  ");
            query.AppendLine("	   select sgccd																  ");
            query.AppendLine("			 ,loc_code															  ");
            query.AppendLine("			 ,ploc_code															  ");
            query.AppendLine("			 ,loc_name															  ");
            query.AppendLine("			 ,ftr_idn															  ");
            query.AppendLine("			 ,ftr_code															  ");
            query.AppendLine("			 ,res_code															  ");
            query.AppendLine("			 ,kt_gbn															  ");
            query.AppendLine("			 ,rel_loc_name														  ");
            query.AppendLine("			 ,rownum ord														  ");
            query.AppendLine("		 from cm_location														  ");
            query.AppendLine("		where 1 = 1																  ");
            query.AppendLine("        and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')             ");
            query.AppendLine("		start with loc_code = :LOC_CODE											  ");
            query.AppendLine("		connect by prior loc_code = ploc_code									  ");
            query.AppendLine("		order SIBLINGS by ftr_idn												  ");
            query.AppendLine("	  ) c1																		  ");
            query.AppendLine("	   ,cm_location c2															  ");
            query.AppendLine("	   ,cm_location c3															  ");
            query.AppendLine("	 ,(																			  ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");

            //query.AppendLine("	  select to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm') year_mon  ");
            //query.AppendLine("		from if_timestamp_yyyymmddhh24 ity										  ");
            //query.AppendLine("	   where ity.yyyymmddhh24 between :STARTDATE||'0100' and :ENDDATE||'0100'	  ");
            //query.AppendLine("	   group by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')		  ");
            //query.AppendLine("	   order by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')		  ");
            
            query.AppendLine("	  ) tmp																		  ");
            query.AppendLine(" where 1 = 1																	  ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)											  ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)											  ");
            query.AppendLine(" order by																		  ");
            query.AppendLine("       c1.ord																	  ");
            query.AppendLine("      ,tmp.year_mon															  ");
            query.AppendLine(")																				  ");
            query.AppendLine("select to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm') year_mon               ");
            query.AppendLine("	    ,to_number(wcm.amtuse) amtuse											  ");
            query.AppendLine("  from wv_consumer_mon wcm													  ");
            query.AppendLine("	    ,loc																	  ");
            query.AppendLine(" where wcm.loc_code(+) = loc.loc_code											  ");
            query.AppendLine("   and wcm.year_mon(+) = loc.year_mon											  ");
            query.AppendLine(" order by																		  ");
            query.AppendLine("       loc.ord																  ");
            query.AppendLine("	    ,loc.year_mon															  ");

            IDataParameter[] parameters =  {
                      new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //수용가별 기본정보
        public void SelectDMInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(																																 ");
            query.AppendLine("select c1.loc_code																											 ");
            query.AppendLine("      ,c1.sgccd																												 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock										 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn										 ");
            query.AppendLine("      ,c1.ord																													 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("      (																														 ");
            query.AppendLine("       select sgccd																											 ");
            query.AppendLine("             ,loc_code																										 ");
            query.AppendLine("             ,ploc_code																										 ");
            query.AppendLine("             ,loc_name																										 ");
            query.AppendLine("             ,ftr_idn																											 ");
            query.AppendLine("             ,ftr_code																										 ");
            query.AppendLine("             ,res_code																										 ");
            query.AppendLine("             ,kt_gbn																											 ");
            query.AppendLine("             ,rel_loc_name																									 ");
            query.AppendLine("             ,rownum ord																										 ");
            query.AppendLine("         from cm_location																										 ");
            query.AppendLine("        where 1 = 1																											 ");
            query.AppendLine("          and ftr_code = 'BZ003'																								 ");
            query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            query.AppendLine("      ) c1																													 ");
            query.AppendLine("       ,cm_location c2																										 ");
            query.AppendLine("       ,cm_location c3																										 ");
            query.AppendLine(" where 1 = 1																													 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            query.AppendLine(" order by c1.ord																												 ");
            query.AppendLine(")																																 ");
            query.AppendLine("select loc.lblock																												 ");
            query.AppendLine("      ,loc.mblock																												 ");
            query.AppendLine("      ,loc.sblock																												 ");

            if (parameter.ContainsKey("IS_BIG_DM") && parameter["IS_BIG_DM"].ToString() == "Y")
            {
                query.AppendLine("      ,nvl((select 'True' from wv_lconsumer_link where dmno = dmi.dmno),'False') is_big_dm  ");
            }

            query.AppendLine("      ,decode(dmi.dmclass, '부', '부', '주') dmclass																			 ");
            query.AppendLine("      ,dmi.dmno																												 ");
            query.AppendLine("      ,dmi.dmnm																												 ");
            query.AppendLine("      ,dmi.hydrntno																											 ");
            query.AppendLine("      ,me.meta_idn                                                                                                             ");
            query.AppendLine("      ,dmi.umdcd																												 ");
            //query.AppendLine("      ,dmi.nrdaddr																											 ");
            query.AppendLine("      ,decode(dmw.wsrepbizkndcd,'1000','가정용','영업용') wsrepbizkndcd														 ");
            query.AppendLine("      ,dmw.wspipeszcd																											 ");
            query.AppendLine("      ,decode(substr(dmw.hydrntstat,0,2), '개전', dmw.statappdt) gejun				                                         ");
            query.AppendLine("      ,decode(substr(dmw.hydrntstat,0,2), '폐전', dmw.statappdt) pejun				                                         ");
            query.AppendLine("      ,decode(substr(dmw.hydrntstat,0,2), '중지', dmw.statappdt, decode(substr(dmw.hydrntstat,0,2), '정지', dmw.statappdt)) stopjun ");
            query.AppendLine("      ,to_char(to_date(mtr.mddt,'yyyymmdd'),'yyyy-mm-dd') mddt																 ");
            query.AppendLine("      ,to_number(dmw.wsnohshd) wsnohshd																						 ");
            query.AppendLine("  from loc																													 ");
            query.AppendLine("      ,wi_dminfo dmi																												 ");
            query.AppendLine("      ,wi_dmwsrsrc dmw																											 ");
            query.AppendLine("      ,(																														 ");
            query.AppendLine("       select dmno																											 ");
            query.AppendLine("             ,max(mddt) mddt																									 ");
            query.AppendLine("         from																													 ");
            query.AppendLine("             (																												 ");
            query.AppendLine("             select dmno, max(mddt) mddt from wi_mtrchginfo group by dmno														 ");
            query.AppendLine("             union all																										 ");
            query.AppendLine("             select dmno, max(mddt) from wv_mtrchg group by dmno																 ");
            query.AppendLine("             )																												 ");
            query.AppendLine("        group by dmno																											 ");
            query.AppendLine("       ) mtr																													 ");
            query.AppendLine("       ,(                                                                                                                      ");
            query.AppendLine("        select dmno                                                                                                            ");
            query.AppendLine("              ,max(s_ftr_idn) meta_idn                                                                                         ");
            query.AppendLine("          from si_pipe_block                                                                                                   ");
            query.AppendLine("         where s_name = 'WTL_META_PS'                                                                                          ");
            query.AppendLine("         group by dmno                                                                                                         ");
            query.AppendLine("       ) me                                                                                                                    ");
            query.AppendLine(" where dmi.lftridn = loc.lftridn																								 ");
            query.AppendLine("   and dmi.mftridn = loc.mftridn																								 ");
            query.AppendLine("   and dmi.sftridn = loc.sftridn																								 ");
            query.AppendLine("   and dmw.dmno = dmi.dmno																									 ");
            query.AppendLine("   and mtr.dmno(+) = dmw.dmno																									 ");
            query.AppendLine("   and me.dmno(+) = dmw.dmno                                                                                                   ");
            query.AppendLine(" order by																														 ");
            query.AppendLine("       loc.ord																												 ");
            query.AppendLine("      ,dmi.dmno																												 ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //수용가별 사용량정보
        public void SelectDMUsed(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(																																 ");
            query.AppendLine("select c1.loc_code																											 ");
            query.AppendLine("      ,c1.sgccd																												 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock										 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn										 ");
            query.AppendLine("      ,c1.ord																													 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("      (																														 ");
            query.AppendLine("       select sgccd																											 ");
            query.AppendLine("             ,loc_code																										 ");
            query.AppendLine("             ,ploc_code																										 ");
            query.AppendLine("             ,loc_name																										 ");
            query.AppendLine("             ,ftr_idn																											 ");
            query.AppendLine("             ,ftr_code																										 ");
            query.AppendLine("             ,res_code																										 ");
            query.AppendLine("             ,kt_gbn																											 ");
            query.AppendLine("             ,rel_loc_name																									 ");
            query.AppendLine("             ,rownum ord																										 ");
            query.AppendLine("         from cm_location																										 ");
            query.AppendLine("        where 1 = 1																											 ");
            query.AppendLine("          and ftr_code = 'BZ003'																								 ");
            query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            query.AppendLine("      ) c1																													 ");
            query.AppendLine("       ,cm_location c2																										 ");
            query.AppendLine("       ,cm_location c3																										 ");
            query.AppendLine(" where 1 = 1																													 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            query.AppendLine(" order by c1.ord																												 ");
            query.AppendLine(")																																 ");
            query.AppendLine("select to_char(to_date(dmi.stym,'yyyymm'),'yyyy-mm') stym																		 ");
            query.AppendLine("      ,to_number(nvl(stw.wsstvol, 0)) wsstvol																					 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("      (																														 ");
            query.AppendLine("      select loc.ord																											 ");
            query.AppendLine("            ,dmi.dmno																											 ");
            query.AppendLine("            ,dmp.stym stym																                                     ");
            query.AppendLine("        from loc																												 ");
            query.AppendLine("            ,wi_dminfo dmi																										 ");
            query.AppendLine("            ,wi_dmwsrsrc dmw																										 ");
            query.AppendLine("            ,(																												 ");
            query.AppendLine("             select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') stym 								     ");
            query.AppendLine("               from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1	 ");
            query.AppendLine("            ) dmp																												 ");
            query.AppendLine("       where dmi.lftridn = loc.lftridn																						 ");
            query.AppendLine("         and dmi.mftridn = loc.mftridn																						 ");
            query.AppendLine("         and dmi.sftridn = loc.sftridn																						 ");
            query.AppendLine("         and dmw.dmno = dmi.dmno																								 ");
            query.AppendLine("      ) dmi																													 ");
            query.AppendLine("      ,wi_stwchrg stw																											 ");
            query.AppendLine(" where stw.dmno(+) = dmi.dmno																									 ");
            query.AppendLine("   and stw.stym(+) = dmi.stym																									 ");
            query.AppendLine(" order by																														 ");
            query.AppendLine("       dmi.ord																												 ");
            query.AppendLine("      ,dmi.dmno																												 ");
            query.AppendLine("      ,dmi.stym																												 ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블럭별 업종별 기본정보
        public void SelectBlockInfoByIndustry(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                     ");
            query.AppendLine("(																																  ");
            query.AppendLine("select c1.loc_code																											  ");
            query.AppendLine("	  ,c1.sgccd																													  ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))	  ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock									  ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		  ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock									  ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		  ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock											  ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))	  ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									  ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		  ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									  ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		  ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn											  ");
            query.AppendLine("	  ,c1.ord																													  ");
            query.AppendLine("  from																														  ");
            query.AppendLine("	  (																															  ");
            query.AppendLine("	   select sgccd																												  ");
            query.AppendLine("			 ,loc_code																											  ");
            query.AppendLine("			 ,ploc_code																											  ");
            query.AppendLine("			 ,loc_name																											  ");
            query.AppendLine("			 ,ftr_idn																											  ");
            query.AppendLine("			 ,ftr_code																											  ");
            query.AppendLine("			 ,res_code																											  ");
            query.AppendLine("			 ,kt_gbn																											  ");
            query.AppendLine("			 ,rel_loc_name																										  ");
            query.AppendLine("			 ,rownum ord																										  ");
            query.AppendLine("		 from cm_location																										  ");
            query.AppendLine("		where 1 = 1																												  ");
            query.AppendLine("        and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')             ");
            query.AppendLine("		 start with loc_code = :LOC_CODE																						  ");
            query.AppendLine("		 connect by prior loc_code = ploc_code																					  ");
            query.AppendLine("		 order SIBLINGS by ftr_idn																								  ");
            query.AppendLine("	   ) c1																														  ");
            query.AppendLine("		,cm_location c2																											  ");
            query.AppendLine("		,cm_location c3																											  ");
            query.AppendLine(" where 1 = 1																													  ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							  ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							  ");
            query.AppendLine(" order by c1.ord																												  ");
            query.AppendLine(")																																  ");
            query.AppendLine("select loc.loc_code																											  ");
            query.AppendLine("	  ,loc.lblock																												  ");
            query.AppendLine("	  ,loc.mblock																												  ");
            query.AppendLine("	  ,loc.sblock																												  ");
            query.AppendLine("	  ,to_number(wco.junsu) junsu																								  ");
            query.AppendLine("	  ,to_number(wco.gejunsu) gejunsu																							  ");
            query.AppendLine("	  ,to_number(wco.pejunsu) pejunsu																							  ");
            query.AppendLine("	  ,to_number(wco.stopjunsu) stopjunsu																						  ");
            query.AppendLine("	  ,(																														  ");
            query.AppendLine("	   select to_number(sum(mtrchgsu))																							  ");
            query.AppendLine("		 from wv_consumer_day																									  ");
            query.AppendLine("		where loc_code = loc.loc_code																							  ");
            query.AppendLine("		  group by loc_code																										  ");
            query.AppendLine("	   ) changejunsu																											  ");
            query.AppendLine("	  ,(																														  ");
            query.AppendLine("	   select (gajung_gagusu+bgajung_gagusu)																					  ");
            query.AppendLine("		 from wv_consumer_day																									  ");
            query.AppendLine("		where loc_code = loc.loc_code																							  ");
            query.AppendLine("		  and datee =																											  ");
            query.AppendLine("			  (select max(datee) from wv_consumer_day where loc_code = loc.loc_code)											  ");
            query.AppendLine("	  ) gagusu																													  ");
            query.AppendLine("      ,a.upjong																												  ");
            query.AppendLine("  from loc																													  ");
            query.AppendLine("	  ,wv_consumer wco																											  ");
            query.AppendLine("      ,(select decode(rownum, 1, '가정용', '영업용') upjong from dual connect by rownum <= 2) a								  ");
            query.AppendLine(" where wco.loc_code = loc.loc_code																							  ");
            query.AppendLine(" order by loc.ord, a.upjong																									  ");

            IDataParameter[] parameters =  {
                      new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블럭별 업종별 사용량정보
        public void SelectBlockUsedByIndustry(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                       ");
            query.AppendLine("(																					");
            query.AppendLine("select c1.loc_code																");
            query.AppendLine("	  ,tmp.year_mon																	");
            query.AppendLine("	  ,c1.ord																		");
            query.AppendLine("  from																			");
            query.AppendLine("	  (																				");
            query.AppendLine("	   select sgccd																	");
            query.AppendLine("			 ,loc_code																");
            query.AppendLine("			 ,ploc_code																");
            query.AppendLine("			 ,loc_name																");
            query.AppendLine("			 ,ftr_idn																");
            query.AppendLine("			 ,ftr_code																");
            query.AppendLine("			 ,res_code																");
            query.AppendLine("			 ,kt_gbn																");
            query.AppendLine("			 ,rel_loc_name															");
            query.AppendLine("			 ,rownum ord															");
            query.AppendLine("		 from cm_location															");
            query.AppendLine("		where 1 = 1																	");
            query.AppendLine("        and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')             ");
            query.AppendLine("		start with loc_code = :LOC_CODE												");
            query.AppendLine("		connect by prior loc_code = ploc_code										");
            query.AppendLine("		order SIBLINGS by ftr_idn													");
            query.AppendLine("	  ) c1																			");
            query.AppendLine("	   ,cm_location c2																");
            query.AppendLine("	   ,cm_location c3																");
            query.AppendLine("	 ,(																				");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");

            //query.AppendLine("	  select to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm') year_mon	");
            //query.AppendLine("		from if_timestamp_yyyymmddhh24 ity											");
            //query.AppendLine("	   where ity.yyyymmddhh24 between :STARTDATE||'0100' and :ENDDATE||'0100'		");
            //query.AppendLine("	   group by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')			");
            //query.AppendLine("	   order by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')			");
            
            query.AppendLine("	  ) tmp																			");
            query.AppendLine(" where 1 = 1																		");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)												");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)												");
            query.AppendLine(" order by																			");
            query.AppendLine("	   c1.ord																		");
            query.AppendLine("	  ,tmp.year_mon																	");
            query.AppendLine(")																					");
            query.AppendLine("select to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm') year_mon					");
            query.AppendLine("	  ,to_number(wcm.gajung_amtuse) gajung_amtuse									");
            query.AppendLine("	  ,to_number(wcm.bgajung_amtuse) bgajung_amtuse									");
            query.AppendLine("  from loc																		");
            query.AppendLine("	  ,wv_consumer_mon wcm															");
            query.AppendLine(" where wcm.loc_code(+) = loc.loc_code												");
            query.AppendLine("   and wcm.year_mon(+) = loc.year_mon												");
            query.AppendLine(" order by																			");
            query.AppendLine("	   loc.ord																		");
            query.AppendLine("	  ,loc.year_mon																	");

            IDataParameter[] parameters =  {
                      new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블럭별 업종별 가구수정보
        public void SelectBlockHouseHoldsByIndustry(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                     ");
            query.AppendLine("(																								  ");
            query.AppendLine("select c1.loc_code																			  ");
            query.AppendLine("	  ,tmp.year_mon																				  ");
            query.AppendLine("	  ,c1.ord																					  ");
            query.AppendLine("  from																						  ");
            query.AppendLine("	  (																							  ");
            query.AppendLine("	   select sgccd																				  ");
            query.AppendLine("			 ,loc_code																			  ");
            query.AppendLine("			 ,ploc_code																			  ");
            query.AppendLine("			 ,loc_name																			  ");
            query.AppendLine("			 ,ftr_idn																			  ");
            query.AppendLine("			 ,ftr_code																			  ");
            query.AppendLine("			 ,res_code																			  ");
            query.AppendLine("			 ,kt_gbn																			  ");
            query.AppendLine("			 ,rel_loc_name																		  ");
            query.AppendLine("			 ,rownum ord																		  ");
            query.AppendLine("		 from cm_location																		  ");
            query.AppendLine("		where 1 = 1																				  ");
            query.AppendLine("        and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')             ");
            query.AppendLine("		start with loc_code = :LOC_CODE															  ");
            query.AppendLine("		connect by prior loc_code = ploc_code													  ");
            query.AppendLine("		order SIBLINGS by ftr_idn																  ");
            query.AppendLine("	  ) c1																						  ");
            query.AppendLine("	   ,cm_location c2																			  ");
            query.AppendLine("	   ,cm_location c3																			  ");
            query.AppendLine("	 ,(																							  ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");
            
            //query.AppendLine("	  select to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm') year_mon				  ");
            //query.AppendLine("		from if_timestamp_yyyymmddhh24 ity														  ");
            //query.AppendLine("	   where ity.yyyymmddhh24 between :STARTDATE||'0100' and :ENDDATE||'0100'					  ");
            //query.AppendLine("	   group by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')						  ");
            //query.AppendLine("	   order by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')						  ");
            
            query.AppendLine("	  ) tmp																						  ");
            query.AppendLine(" where 1 = 1																					  ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)															  ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)															  ");
            query.AppendLine(" order by																						  ");
            query.AppendLine("	   c1.ord																					  ");
            query.AppendLine("	  ,tmp.year_mon																				  ");
            query.AppendLine(")																								  ");
            query.AppendLine("select to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm') datee								  ");
            query.AppendLine("	  ,floor(avg(wcd.gajung_gagusu)) gajung_gagusu												  ");
            query.AppendLine("	  ,floor(avg(wcd.bgajung_gagusu)) bgajung_gagusu											  ");
            query.AppendLine("  from loc																					  ");
            query.AppendLine("	  ,wv_consumer_day wcd																		  ");
            query.AppendLine(" where wcd.loc_code(+) = loc.loc_code															  ");
            query.AppendLine("   and wcd.datee(+) between loc.year_mon||'01' and loc.year_mon||'31'							  ");
            query.AppendLine(" group by																						  ");
            query.AppendLine("	   loc.ord																					  ");
            query.AppendLine("	  ,loc.year_mon																				  ");
            query.AppendLine(" order by																						  ");
            query.AppendLine("	   loc.ord																					  ");
            query.AppendLine("	  ,loc.year_mon																				  ");

            IDataParameter[] parameters =  {
                      new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블럭별 급수전 정보
        public void SelectBlockWaterTap(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                    ");
            query.AppendLine("(																				 ");
            query.AppendLine("select c1.loc_code															 ");
            query.AppendLine("	  ,tmp.year_mon																 ");
            query.AppendLine("	  ,c1.ord																	 ");
            query.AppendLine("  from																		 ");
            query.AppendLine("	  (																			 ");
            query.AppendLine("	   select sgccd																 ");
            query.AppendLine("			 ,loc_code															 ");
            query.AppendLine("			 ,ploc_code															 ");
            query.AppendLine("			 ,loc_name															 ");
            query.AppendLine("			 ,ftr_idn															 ");
            query.AppendLine("			 ,ftr_code															 ");
            query.AppendLine("			 ,res_code															 ");
            query.AppendLine("			 ,kt_gbn															 ");
            query.AppendLine("			 ,rel_loc_name														 ");
            query.AppendLine("			 ,rownum ord														 ");
            query.AppendLine("		 from cm_location														 ");
            query.AppendLine("		where 1 = 1																 ");
            query.AppendLine("        and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')             ");
            query.AppendLine("		start with loc_code = :LOC_CODE											 ");
            query.AppendLine("		connect by prior loc_code = ploc_code									 ");
            query.AppendLine("		order SIBLINGS by ftr_idn												 ");
            query.AppendLine("	  ) c1																		 ");
            query.AppendLine("	   ,cm_location c2															 ");
            query.AppendLine("	   ,cm_location c3															 ");
            query.AppendLine("	 ,(																			 ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");

            //query.AppendLine("	  select to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm') year_mon ");
            //query.AppendLine("		from if_timestamp_yyyymmddhh24 ity										 ");
            //query.AppendLine("	   where ity.yyyymmddhh24 between :STARTDATE||'0100' and :ENDDATE||'0100'	 ");
            //query.AppendLine("	   group by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')		 ");
            //query.AppendLine("	   order by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')		 ");
            
            query.AppendLine("	  ) tmp																		 ");
            query.AppendLine(" where 1 = 1																	 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)											 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)											 ");
            query.AppendLine(" order by																		 ");
            query.AppendLine("	   c1.ord																	 ");
            query.AppendLine("	  ,tmp.year_mon																 ");
            query.AppendLine(")																				 ");
            query.AppendLine("select to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm') datee				 ");
            query.AppendLine("	  ,floor(avg(wcd.gupsujunsu)) gupsujunsu									 ");
            query.AppendLine("  from loc																	 ");
            query.AppendLine("	  ,wv_consumer_day wcd														 ");
            query.AppendLine(" where wcd.loc_code(+) = loc.loc_code											 ");
            query.AppendLine("   and wcd.datee(+) between loc.year_mon||'01' and loc.year_mon||'31'			 ");
            query.AppendLine(" group by																		 ");
            query.AppendLine("	   loc.ord																	 ");
            query.AppendLine("	  ,loc.year_mon																 ");
            query.AppendLine(" order by																		 ");
            query.AppendLine("	   loc.ord																	 ");
            query.AppendLine("	  ,loc.year_mon																 ");

            IDataParameter[] parameters =  {
                      new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록별 대수용가 기본정보
        public void SelectBlockInfoBigDM(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(                                                                                                                              ");
            query.AppendLine("select c1.loc_code                                                                                                             ");
            query.AppendLine("      ,c1.sgccd                                                                                                                ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                        ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                        ");
            query.AppendLine("      ,c1.ord                                                                                                                  ");
            query.AppendLine("  from                                                                                                                         ");
            query.AppendLine("      (                                                                                                                        ");
            query.AppendLine("       select sgccd                                                                                                            ");
            query.AppendLine("             ,loc_code                                                                                                         ");
            query.AppendLine("             ,ploc_code                                                                                                        ");
            query.AppendLine("             ,loc_name                                                                                                         ");
            query.AppendLine("             ,ftr_idn                                                                                                          ");
            query.AppendLine("             ,ftr_code                                                                                                         ");
            query.AppendLine("             ,res_code                                                                                                         ");
            query.AppendLine("             ,kt_gbn                                                                                                           ");
            query.AppendLine("             ,rel_loc_name                                                                                                     ");
            query.AppendLine("             ,rownum ord                                                                                                       ");
            query.AppendLine("         from cm_location                                                                                                      ");
            query.AppendLine("        where 1 = 1                                                                                                            ");
            query.AppendLine("          and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')                                                           ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                        ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                  ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                              ");
            query.AppendLine("      ) c1                                                                                                                     ");
            query.AppendLine("       ,cm_location c2                                                                                                         ");
            query.AppendLine("       ,cm_location c3                                                                                                         ");
            query.AppendLine(" where 1 = 1                                                                                                                   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                           ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                           ");
            query.AppendLine(" order by c1.ord                                                                                                               ");
            query.AppendLine(")                                                                                                                              ");
            query.AppendLine("select loc.loc_code                                                                                                            ");
            query.AppendLine("      ,loc.lblock                                                                                                              ");
            query.AppendLine("      ,loc.mblock                                                                                                              ");
            query.AppendLine("      ,loc.sblock                                                                                                              ");
            query.AppendLine("      ,sum(dmi.junsu) junsu                                                                                                    ");
            query.AppendLine("      ,sum(dmi.gagusu) gagusu                                                                                                  ");
            query.AppendLine("  from (                                                                                                                       ");
            query.AppendLine("       select loc.loc_code                                                                                                     ");
            query.AppendLine("             ,loc.lblock                                                                                                       ");
            query.AppendLine("             ,loc.mblock                                                                                                       ");
            query.AppendLine("             ,loc.sblock                                                                                                       ");
            query.AppendLine("             ,loc.lftridn                                                                                                      ");
            query.AppendLine("             ,loc.mftridn                                                                                                      ");
            query.AppendLine("             ,sb.ftr_idn sftridn                                                                                               ");
            query.AppendLine("             ,loc.ord                                                                                                          ");
            query.AppendLine("         from loc                                                                                                              ");
            query.AppendLine("             ,(                                                                                                                ");
            query.AppendLine("              select loc_code                                                                                                  ");
            query.AppendLine("                    ,ftr_idn                                                                                                   ");
            query.AppendLine("                    ,pftr_idn                                                                                                  ");
            query.AppendLine("                from cm_location                                                                                               ");
            query.AppendLine("               where 1 = 1                                                                                                     ");
            query.AppendLine("                 and ftr_code = 'BZ003'                                                                                        ");
            query.AppendLine("               start with loc_code = :LOC_CODE                                                                                 ");
            query.AppendLine("               connect by prior loc_code = ploc_code                                                                           ");
            query.AppendLine("               order SIBLINGS by ftr_idn                                                                                       ");
            query.AppendLine("              ) sb                                                                                                             ");
            query.AppendLine("        where sb.pftr_idn(+) = loc.mftridn                                                                                     ");
            query.AppendLine("          and sb.ftr_idn(+) = decode(:FTR_CDE,'BZ001', sb.ftr_idn(+), loc.sftridn)                                             ");
            query.AppendLine("        group by                                                                                                               ");
            query.AppendLine("              loc.loc_code                                                                                                     ");
            query.AppendLine("             ,loc.lblock                                                                                                       ");
            query.AppendLine("             ,loc.mblock                                                                                                       ");
            query.AppendLine("             ,loc.sblock                                                                                                       ");
            query.AppendLine("             ,loc.lftridn                                                                                                      ");
            query.AppendLine("             ,loc.mftridn                                                                                                      ");
            query.AppendLine("             ,sb.ftr_idn                                                                                                       ");
            query.AppendLine("             ,loc.ord                                                                                                          ");
            query.AppendLine("       ) loc                                                                                                                   ");
            query.AppendLine("      ,(                                                                                                                       ");
            query.AppendLine("       select dmi.lftridn                                                                                                      ");
            query.AppendLine("             ,dmi.mftridn                                                                                                      ");
            query.AppendLine("             ,dmi.sftridn                                                                                                      ");
            query.AppendLine("             ,count(*) junsu                                                                                                   ");
            query.AppendLine("             ,sum(dmw.wsnohshd) gagusu                                                                                         ");
            query.AppendLine("         from wi_dminfo dmi                                                                                                       ");
            query.AppendLine("             ,wi_dmwsrsrc dmw                                                                                                     ");
            query.AppendLine("             ,wv_lconsumer_link wll                                                                                            ");
            query.AppendLine("        where dmw.dmno = dmi.dmno                                                                                              ");
            query.AppendLine("          and wll.dmno = dmi.dmno                                                                                              ");
            query.AppendLine("        group by                                                                                                               ");
            query.AppendLine("              dmi.lftridn                                                                                                      ");
            query.AppendLine("             ,dmi.mftridn                                                                                                      ");
            query.AppendLine("             ,dmi.sftridn                                                                                                      ");
            query.AppendLine("      ) dmi                                                                                                                    ");
            query.AppendLine(" where 1 = 1                                                                                                                   ");
            query.AppendLine("   and dmi.lftridn(+) = loc.lftridn                                                                                            ");
            query.AppendLine("   and dmi.mftridn(+) = loc.mftridn                                                                                            ");
            query.AppendLine("   and dmi.sftridn(+) = loc.sftridn                                                                                            ");
            query.AppendLine(" group by                                                                                                                      ");
            query.AppendLine("       loc.ord                                                                                                                 ");
            query.AppendLine("      ,loc.loc_code                                                                                                            ");
            query.AppendLine("      ,loc.lblock                                                                                                              ");
            query.AppendLine("      ,loc.mblock                                                                                                              ");
            query.AppendLine("      ,loc.sblock                                                                                                              ");
            query.AppendLine(" order by                                                                                                                      ");
            query.AppendLine("       loc.ord                                                                                                                 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["FTR_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록별 대수용가 사용량
        public void SelectBlockUsedBigDM(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(                                                                                                                              ");
            query.AppendLine("select c1.loc_code                                                                                                             ");
            query.AppendLine("      ,c1.sgccd                                                                                                                ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                        ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                        ");
            query.AppendLine("      ,c1.ord                                                                                                                  ");
            query.AppendLine("      ,tmp.year_mon                                                                                                            ");
            query.AppendLine("  from                                                                                                                         ");
            query.AppendLine("      (                                                                                                                        ");
            query.AppendLine("       select sgccd                                                                                                            ");
            query.AppendLine("             ,loc_code                                                                                                         ");
            query.AppendLine("             ,ploc_code                                                                                                        ");
            query.AppendLine("             ,loc_name                                                                                                         ");
            query.AppendLine("             ,ftr_idn                                                                                                          ");
            query.AppendLine("             ,ftr_code                                                                                                         ");
            query.AppendLine("             ,res_code                                                                                                         ");
            query.AppendLine("             ,kt_gbn                                                                                                           ");
            query.AppendLine("             ,rel_loc_name                                                                                                     ");
            query.AppendLine("             ,rownum ord                                                                                                       ");
            query.AppendLine("         from cm_location                                                                                                      ");
            query.AppendLine("        where 1 = 1                                                                                                            ");
            query.AppendLine("          and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')                                                           ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                        ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                  ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                              ");
            query.AppendLine("      ) c1                                                                                                                     ");
            query.AppendLine("       ,cm_location c2                                                                                                         ");
            query.AppendLine("       ,cm_location c3                                                                                                         ");
            query.AppendLine("       ,(                                                                                                                      ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");

            //query.AppendLine("        select to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm') year_mon                                             ");
            //query.AppendLine("          from if_timestamp_yyyymmddhh24 ity                                                                                   ");
            //query.AppendLine("         where ity.yyyymmddhh24 between :STARTDATE||'0100' and :ENDDATE||'0100'                                                ");
            //query.AppendLine("         group by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')                                                   ");
            //query.AppendLine("         order by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')                                                   ");
            
            query.AppendLine("        ) tmp                                                                                                                  ");
            query.AppendLine(" where 1 = 1                                                                                                                   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                           ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                           ");
            query.AppendLine(" order by c1.ord, tmp.year_mon                                                                                                 ");
            query.AppendLine(")                                                                                                                              ");
            query.AppendLine("select to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm') year_mon                                                              ");
            query.AppendLine("      ,sum(dmi.amtuse) amtuse                                                                                                  ");
            query.AppendLine("  from (                                                                                                                       ");
            query.AppendLine("       select loc.loc_code                                                                                                     ");
            query.AppendLine("             ,loc.lblock                                                                                                       ");
            query.AppendLine("             ,loc.mblock                                                                                                       ");
            query.AppendLine("             ,loc.sblock                                                                                                       ");
            query.AppendLine("             ,loc.lftridn                                                                                                      ");
            query.AppendLine("             ,loc.mftridn                                                                                                      ");
            query.AppendLine("             ,sb.ftr_idn sftridn                                                                                               ");
            query.AppendLine("             ,loc.ord                                                                                                          ");
            query.AppendLine("             ,loc.year_mon                                                                                                     ");
            query.AppendLine("         from loc                                                                                                              ");
            query.AppendLine("             ,(                                                                                                                ");
            query.AppendLine("              select loc_code                                                                                                  ");
            query.AppendLine("                    ,ftr_idn                                                                                                   ");
            query.AppendLine("                    ,pftr_idn                                                                                                  ");
            query.AppendLine("                from cm_location                                                                                               ");
            query.AppendLine("               where 1 = 1                                                                                                     ");
            query.AppendLine("                 and ftr_code = 'BZ003'                                                                                        ");
            query.AppendLine("               start with loc_code = :LOC_CODE                                                                                  ");
            query.AppendLine("               connect by prior loc_code = ploc_code                                                                           ");
            query.AppendLine("               order SIBLINGS by ftr_idn                                                                                       ");
            query.AppendLine("              ) sb                                                                                                             ");
            query.AppendLine("        where sb.pftr_idn(+) = loc.mftridn                                                                                     ");
            query.AppendLine("          and sb.ftr_idn(+) = decode(:FTR_CODE,'BZ001', sb.ftr_idn(+), loc.sftridn)                                            ");
            query.AppendLine("        group by                                                                                                               ");
            query.AppendLine("              loc.loc_code                                                                                                     ");
            query.AppendLine("             ,loc.lblock                                                                                                       ");
            query.AppendLine("             ,loc.mblock                                                                                                       ");
            query.AppendLine("             ,loc.sblock                                                                                                       ");
            query.AppendLine("             ,loc.lftridn                                                                                                      ");
            query.AppendLine("             ,loc.mftridn                                                                                                      ");
            query.AppendLine("             ,sb.ftr_idn                                                                                                       ");
            query.AppendLine("             ,loc.ord                                                                                                          ");
            query.AppendLine("             ,loc.year_mon                                                                                                     ");
            query.AppendLine("       ) loc                                                                                                                   ");
            query.AppendLine("      ,(                                                                                                                       ");
            query.AppendLine("       select dmi.lftridn                                                                                                      ");
            query.AppendLine("             ,dmi.mftridn                                                                                                      ");
            query.AppendLine("             ,dmi.sftridn                                                                                                      ");
            query.AppendLine("             ,stw.stym year_mon                                                                                                ");
            query.AppendLine("             ,sum(stw.wsstvol) amtuse                                                                                          ");
            query.AppendLine("         from wi_dminfo dmi                                                                                                       ");
            query.AppendLine("             ,wi_dmwsrsrc dmw                                                                                                     ");
            query.AppendLine("             ,wi_stwchrg stw                                                                                                      ");
            query.AppendLine("             ,wv_lconsumer_link wll                                                                                            ");
            query.AppendLine("        where dmw.dmno = dmi.dmno                                                                                              ");
            query.AppendLine("          and stw.dmno = dmi.dmno                                                                                              ");
            query.AppendLine("          and wll.dmno = stw.dmno                                                                                              ");
            query.AppendLine("          and stw.stym between :STARTDATE and :ENDDATE                                                                         ");
            query.AppendLine("        group by                                                                                                               ");
            query.AppendLine("              dmi.lftridn                                                                                                      ");
            query.AppendLine("             ,dmi.mftridn                                                                                                      ");
            query.AppendLine("             ,dmi.sftridn                                                                                                      ");
            query.AppendLine("             ,stw.stym                                                                                                         ");
            query.AppendLine("      ) dmi                                                                                                                    ");
            query.AppendLine(" where 1 = 1                                                                                                                   ");
            query.AppendLine("   and dmi.lftridn(+) = loc.lftridn                                                                                            ");
            query.AppendLine("   and dmi.mftridn(+) = loc.mftridn                                                                                            ");
            query.AppendLine("   and dmi.sftridn(+) = loc.sftridn                                                                                            ");
            query.AppendLine("   and dmi.year_mon(+) = loc.year_mon                                                                                          ");
            query.AppendLine(" group by                                                                                                                      ");
            query.AppendLine("       loc.ord                                                                                                                 ");
            query.AppendLine("      ,loc.loc_code                                                                                                            ");
            query.AppendLine("      ,loc.lblock                                                                                                              ");
            query.AppendLine("      ,loc.mblock                                                                                                              ");
            query.AppendLine("      ,loc.sblock                                                                                                              ");
            query.AppendLine("      ,to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm')                                                                       ");
            query.AppendLine(" order by                                                                                                                      ");
            query.AppendLine("       loc.ord                                                                                                                 ");
            query.AppendLine("      ,to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm')                                                                       ");

            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];
            parameters[5].Value = parameter["LOC_CODE"];
            parameters[6].Value = parameter["FTR_CODE"];
            parameters[7].Value = parameter["STARTDATE"];
            parameters[8].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //대수용가 정보
        public void SelectDMInfoBigDM(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(																																 ");
            query.AppendLine("select c1.loc_code																											 ");
            query.AppendLine("      ,c1.sgccd																												 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock										 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn										 ");
            query.AppendLine("      ,c1.ord																													 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("      (																														 ");
            query.AppendLine("       select sgccd																											 ");
            query.AppendLine("             ,loc_code																										 ");
            query.AppendLine("             ,ploc_code																										 ");
            query.AppendLine("             ,loc_name																										 ");
            query.AppendLine("             ,ftr_idn																											 ");
            query.AppendLine("             ,ftr_code																										 ");
            query.AppendLine("             ,res_code																										 ");
            query.AppendLine("             ,kt_gbn																											 ");
            query.AppendLine("             ,rel_loc_name																									 ");
            query.AppendLine("             ,rownum ord																										 ");
            query.AppendLine("         from cm_location																										 ");
            query.AppendLine("        where 1 = 1																											 ");
            query.AppendLine("          and ftr_code = 'BZ003'																								 ");
            query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            query.AppendLine("      ) c1																													 ");
            query.AppendLine("       ,cm_location c2																										 ");
            query.AppendLine("       ,cm_location c3																										 ");
            query.AppendLine(" where 1 = 1																													 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            query.AppendLine(" order by c1.ord																												 ");
            query.AppendLine(")																																 ");
            query.AppendLine("select loc.lblock																												 ");
            query.AppendLine("      ,loc.mblock																												 ");
            query.AppendLine("      ,loc.sblock																												 ");

            if (parameter.ContainsKey("IS_BIG_DM") && parameter["IS_BIG_DM"].ToString() == "Y")
            {
                query.AppendLine("      ,'False' is_big_dm  ");
            }

            query.AppendLine("      ,decode(dmi.dmclass, '부', '부', '주') dmclass																			 ");
            query.AppendLine("      ,dmi.dmno																												 ");
            query.AppendLine("      ,dmi.dmnm																												 ");
            query.AppendLine("      ,dmi.hydrntno																											 ");
            query.AppendLine("      ,me.meta_idn                                                                                                             ");
            query.AppendLine("      ,dmi.umdcd																												 ");
            //query.AppendLine("      ,dmi.nrdaddr																											 ");
            query.AppendLine("      ,decode(dmw.wsrepbizkndcd,'1000','가정용','영업용') wsrepbizkndcd														 ");
            query.AppendLine("      ,dmw.wspipeszcd																											 ");
            //query.AppendLine("      ,decode(substr(dmw.hydrntstat,1,2), '개전', to_char(to_date(dmw.statappdt,'yyyymmdd'),'yyyy-mm-dd')) gejun				 ");
            //query.AppendLine("      ,decode(substr(dmw.hydrntstat,1,2), '폐전', to_char(to_date(dmw.statappdt,'yyyymmdd'),'yyyy-mm-dd')) pejun				 ");
            //query.AppendLine("      ,decode(substr(dmw.hydrntstat,1,2), '중지', to_char(to_date(dmw.statappdt,'yyyymmdd'),'yyyy-mm-dd')) stopjun			 ");

            query.AppendLine("      ,decode(substr(dmw.hydrntstat,0,2), '개전', dmw.statappdt) gejun				                                         ");
            query.AppendLine("      ,decode(substr(dmw.hydrntstat,0,2), '폐전', dmw.statappdt) pejun				                                         ");
            query.AppendLine("      ,decode(substr(dmw.hydrntstat,0,2), '중지', dmw.statappdt, decode(substr(dmw.hydrntstat,0,2), '정지', dmw.statappdt)) stopjun ");
            
            query.AppendLine("      ,to_char(to_date(mtr.mddt,'yyyymmdd'),'yyyy-mm-dd') mddt																 ");
            query.AppendLine("      ,to_number(dmw.wsnohshd) wsnohshd																						 ");
            query.AppendLine("  from loc																													 ");
            query.AppendLine("      ,wi_dminfo dmi																												 ");
            query.AppendLine("      ,wi_dmwsrsrc dmw																											 ");
            query.AppendLine("      ,(																														 ");
            query.AppendLine("       select dmno																											 ");
            query.AppendLine("             ,max(mddt) mddt																									 ");
            query.AppendLine("         from																													 ");
            query.AppendLine("             (																												 ");
            query.AppendLine("             select dmno, max(mddt) mddt from wi_mtrchginfo group by dmno														 ");
            query.AppendLine("             union all																										 ");
            query.AppendLine("             select dmno, max(mddt) from wv_mtrchg group by dmno																 ");
            query.AppendLine("             )																												 ");
            query.AppendLine("        group by dmno																											 ");
            query.AppendLine("       ) mtr																													 ");
            query.AppendLine("       ,(                                                                                                                      ");
            query.AppendLine("        select dmno                                                                                                            ");
            query.AppendLine("              ,max(s_ftr_idn) meta_idn                                                                                         ");
            query.AppendLine("          from si_pipe_block                                                                                                   ");
            query.AppendLine("         where s_name = 'WTL_META_PS'                                                                                          ");
            query.AppendLine("         group by dmno                                                                                                         ");
            query.AppendLine("       ) me                                                                                                                    ");
            query.AppendLine("      ,wv_lconsumer_link wll																									 ");
            query.AppendLine(" where dmi.lftridn = loc.lftridn																								 ");
            query.AppendLine("   and dmi.mftridn = loc.mftridn																								 ");
            query.AppendLine("   and dmi.sftridn = loc.sftridn																								 ");
            query.AppendLine("   and dmw.dmno = dmi.dmno																									 ");
            query.AppendLine("   and mtr.dmno(+) = dmi.dmno																									 ");
            query.AppendLine("   and me.dmno(+) = dmi.dmno                                                                                                   ");
            query.AppendLine("   and wll.dmno = dmi.dmno																									 ");
            query.AppendLine(" order by																														 ");
            query.AppendLine("       loc.ord																												 ");
            query.AppendLine("      ,dmi.dmno																												 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //대수용가 사용량
        public void SelectDMUsedBigDM(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(																																 ");
            query.AppendLine("select c1.loc_code																											 ");
            query.AppendLine("      ,c1.sgccd																												 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock										 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn										 ");
            query.AppendLine("      ,c1.ord																													 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("      (																														 ");
            query.AppendLine("       select sgccd																											 ");
            query.AppendLine("             ,loc_code																										 ");
            query.AppendLine("             ,ploc_code																										 ");
            query.AppendLine("             ,loc_name																										 ");
            query.AppendLine("             ,ftr_idn																											 ");
            query.AppendLine("             ,ftr_code																										 ");
            query.AppendLine("             ,res_code																										 ");
            query.AppendLine("             ,kt_gbn																											 ");
            query.AppendLine("             ,rel_loc_name																									 ");
            query.AppendLine("             ,rownum ord																										 ");
            query.AppendLine("         from cm_location																										 ");
            query.AppendLine("        where 1 = 1																											 ");
            query.AppendLine("          and ftr_code = 'BZ003'																								 ");
            query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            query.AppendLine("      ) c1																													 ");
            query.AppendLine("       ,cm_location c2																										 ");
            query.AppendLine("       ,cm_location c3																										 ");
            query.AppendLine(" where 1 = 1																													 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            query.AppendLine(" order by c1.ord																												 ");
            query.AppendLine(")																																 ");
            query.AppendLine("select to_char(to_date(dmi.stym,'yyyymm'),'yyyy-mm') year_mon																     ");
            query.AppendLine("      ,to_number(nvl(stw.wsstvol, 0)) amtuse																					 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("      (																														 ");
            query.AppendLine("      select loc.ord																											 ");
            query.AppendLine("            ,dmi.dmno																											 ");
            query.AppendLine("            ,dmp.stym stym																									 ");
            query.AppendLine("        from loc																												 ");
            query.AppendLine("            ,wi_dminfo dmi																										 ");
            query.AppendLine("            ,wi_dmwsrsrc dmw																										 ");
            query.AppendLine("            ,(																												 ");
            query.AppendLine("             select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') stym									 ");
            query.AppendLine("               from dual connect by rownum <= months_between(:ENDDATE||'01', :STARTDATE||'01') + 1							 ");
            query.AppendLine("            ) dmp																												 ");
            query.AppendLine("            ,wv_lconsumer_link wll																							 ");
            query.AppendLine("       where dmi.lftridn = loc.lftridn																						 ");
            query.AppendLine("         and dmi.mftridn = loc.mftridn																						 ");
            query.AppendLine("         and dmi.sftridn = loc.sftridn																						 ");
            query.AppendLine("         and dmw.dmno = dmi.dmno																								 ");
            query.AppendLine("         and wll.dmno = dmi.dmno																								 ");
            query.AppendLine("      ) dmi																													 ");
            query.AppendLine("      ,wi_stwchrg stw																											 ");
            query.AppendLine(" where stw.dmno(+) = dmi.dmno																									 ");
            query.AppendLine("   and stw.stym(+) = dmi.stym																									 ");
            query.AppendLine(" order by																														 ");
            query.AppendLine("       dmi.ord																												 ");
            query.AppendLine("      ,dmi.dmno																												 ");
            query.AppendLine("      ,dmi.stym																												 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록별 계량기교체 건수정보
        public void SelectBlockCountMeterChange(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(																																 ");
            query.AppendLine("select c1.loc_code																											 ");
            query.AppendLine("	  ,c1.sgccd																													 ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))	 ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock									 ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock									 ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock											 ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))	 ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									 ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									 ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn											 ");
            query.AppendLine("	  ,c1.ord																													 ");
            query.AppendLine("      ,tmp.year_mon																											 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("	  (																															 ");
            query.AppendLine("	   select sgccd																												 ");
            query.AppendLine("			 ,loc_code																											 ");
            query.AppendLine("			 ,ploc_code																											 ");
            query.AppendLine("			 ,loc_name																											 ");
            query.AppendLine("			 ,ftr_idn																											 ");
            query.AppendLine("			 ,ftr_code																											 ");
            query.AppendLine("			 ,res_code																											 ");
            query.AppendLine("			 ,kt_gbn																											 ");
            query.AppendLine("			 ,rel_loc_name																										 ");
            query.AppendLine("			 ,rownum ord																										 ");
            query.AppendLine("		 from cm_location																										 ");
            query.AppendLine("		where 1 = 1																												 ");
            query.AppendLine("		  and ftr_code = decode(:FTR_CODE, 'BZ001', 'BZ002', 'BZ003')															 ");
            query.AppendLine("		start with loc_code = :LOC_CODE																							 ");
            query.AppendLine("		connect by prior loc_code = ploc_code																					 ");
            query.AppendLine("		order SIBLINGS by ftr_idn																								 ");
            query.AppendLine("	 ) c1																														 ");
            query.AppendLine("	  ,cm_location c2																											 ");
            query.AppendLine("	  ,cm_location c3																											 ");
            query.AppendLine("      ,(																														 ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");

            //query.AppendLine("       select to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm') year_mon												 ");
            //query.AppendLine("         from if_timestamp_yyyymmddhh24 ity																					 ");
            //query.AppendLine("        where ity.yyyymmddhh24 between :STARTDATE||'0100' and :ENDDATE||'0100'												 ");
            //query.AppendLine("        group by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')													 ");
            //query.AppendLine("        order by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')													 ");
            
            query.AppendLine("      ) tmp																													 ");
            query.AppendLine(" where 1 = 1																													 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            query.AppendLine(" order by c1.ord																												 ");
            query.AppendLine(")																																 ");
            query.AppendLine("select to_char(to_date(wcd.datee,'yyyymmdd'),'yyyy-mm') datee																	 ");
            query.AppendLine("      ,sum(wcd.mtrchgsu) mtrchgsu																								 ");
            query.AppendLine("  from loc																													 ");
            query.AppendLine("      ,wv_consumer_day wcd																									 ");
            query.AppendLine(" where wcd.loc_code(+) = loc.loc_code																							 ");
            query.AppendLine("   and to_char(to_date(wcd.datee(+),'yyyymmdd'),'yyyymm') = loc.year_mon														 ");
            query.AppendLine("   and wcd.datee between :STARTDATE||'01' and :ENDDATE||'31'																	 ");
            query.AppendLine(" group by																														 ");
            query.AppendLine("       loc.ord																												 ");
            query.AppendLine("      ,to_char(to_date(wcd.datee,'yyyymmdd'),'yyyy-mm')																		 ");
            query.AppendLine(" order by																														 ");
            query.AppendLine("       loc.ord																												 ");
            query.AppendLine("      ,to_char(to_date(wcd.datee,'yyyymmdd'),'yyyy-mm')																		 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];
            parameters[5].Value = parameter["STARTDATE"];
            parameters[6].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //계량기교체정보
        public void SelectDMCountMeterChange(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(																																 ");
            query.AppendLine("select c1.loc_code																											 ");
            query.AppendLine("      ,c1.sgccd																												 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock								 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock										 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn										 ");
            query.AppendLine("      ,c1.ord																													 ");
            query.AppendLine("  from																														 ");
            query.AppendLine("      (																														 ");
            query.AppendLine("       select sgccd																											 ");
            query.AppendLine("             ,loc_code																										 ");
            query.AppendLine("             ,ploc_code																										 ");
            query.AppendLine("             ,loc_name																										 ");
            query.AppendLine("             ,ftr_idn																											 ");
            query.AppendLine("             ,ftr_code																										 ");
            query.AppendLine("             ,res_code																										 ");
            query.AppendLine("             ,kt_gbn																											 ");
            query.AppendLine("             ,rel_loc_name																									 ");
            query.AppendLine("             ,rownum ord																										 ");
            query.AppendLine("         from cm_location																										 ");
            query.AppendLine("        where 1 = 1																											 ");
            query.AppendLine("          and ftr_code = 'BZ003'																								 ");
            query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            query.AppendLine("      ) c1																													 ");
            query.AppendLine("       ,cm_location c2																										 ");
            query.AppendLine("       ,cm_location c3																										 ");
            query.AppendLine(" where 1 = 1																													 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            query.AppendLine(" order by c1.ord																												 ");
            query.AppendLine(")																																 ");
            query.AppendLine("select loc.lblock																												 ");
            query.AppendLine("      ,loc.mblock																												 ");
            query.AppendLine("      ,loc.sblock																												 ");
            query.AppendLine("      ,decode(dmi.dmclass, '부', '부', '주') dmclass																			 ");
            query.AppendLine("      ,dmi.dmno																												 ");
            query.AppendLine("      ,dmi.dmnm																												 ");
            query.AppendLine("      ,dmi.hydrntno																											 ");
            query.AppendLine("      ,me.meta_idn                                                                                                             ");
            query.AppendLine("      ,dmi.umdcd																												 ");
            //query.AppendLine("      ,dmi.nrdaddr																											 ");
            query.AppendLine("      ,decode(dmw.wsrepbizkndcd,'1000','가정용','영업용') wsrepbizkndcd														 ");
            query.AppendLine("      ,mtr.transclass																											 ");
            query.AppendLine("      ,to_char(to_date(mtr.mddt,'yyyymmdd'),'yyyy-mm-dd') mddt																 ");
            query.AppendLine("      ,mtr.omfrno																												 ");
            query.AppendLine("      ,to_number(mtr.owspipeszcd) owspipeszcd																					 ");
            query.AppendLine("      ,to_number(mtr.wsrmvndl) wsrmvndl																						 ");
            query.AppendLine("      ,mtr.nmfrno																												 ");
            query.AppendLine("      ,to_number(mtr.nwspipeszcd) nwspipeszcd																					 ");
            query.AppendLine("      ,to_number(mtr.wsattndl) wsattndl																						 ");
            query.AppendLine("      ,to_char(to_date(mtr.mrym,'yyyymm'),'yyyy-mm') mrym																		 ");
            query.AppendLine("      ,mtr.transrs																											 ");
            query.AppendLine("  from loc																													 ");
            query.AppendLine("      ,wi_dminfo dmi																												 ");
            query.AppendLine("      ,wi_dmwsrsrc dmw																											 ");
            query.AppendLine("      ,(																														 ");
            query.AppendLine("       select dmno																											 ");
            query.AppendLine("             ,mtrchgserno																										 ");
            query.AppendLine("             ,transclass																										 ");
            query.AppendLine("             ,mrym																											 ");
            query.AppendLine("             ,mddt																											 ");
            query.AppendLine("             ,omfrno																											 ");
            query.AppendLine("             ,owspipeszcd																										 ");
            query.AppendLine("             ,wsrmvndl																										 ");
            query.AppendLine("             ,nmfrno																											 ");
            query.AppendLine("             ,nwspipeszcd																										 ");
            query.AppendLine("             ,wsattndl																										 ");
            query.AppendLine("             ,transrs																											 ");
            //query.AppendLine("             ,rflym																											 ");
            query.AppendLine("        from wi_mtrchginfo																										 ");
            query.AppendLine("       where mddt between :STARTDATE||'01' and :ENDDATE||'31'																	 ");
            query.AppendLine("      union all																												 ");
            query.AppendLine("       select dmno																											 ");
            query.AppendLine("             ,mtrchgserno																										 ");
            query.AppendLine("             ,transclass																										 ");
            query.AppendLine("             ,mrym																											 ");
            query.AppendLine("             ,mddt																											 ");
            query.AppendLine("             ,omfrno																											 ");
            query.AppendLine("             ,owspipeszcd																										 ");
            query.AppendLine("             ,wsrmvndl																										 ");
            query.AppendLine("             ,nmfrno																											 ");
            query.AppendLine("             ,nwspipeszcd																										 ");
            query.AppendLine("             ,wsattndl																										 ");
            query.AppendLine("             ,transrs																											 ");
            //query.AppendLine("             ,rflym																											 ");
            query.AppendLine("        from wv_mtrchg																										 ");
            query.AppendLine("       where mddt between :STARTDATE||'01' and :ENDDATE||'31'																	 ");
            query.AppendLine("      ) mtr																													 ");
            query.AppendLine("       ,(                                                                                                                      ");
            query.AppendLine("        select dmno                                                                                                            ");
            query.AppendLine("              ,max(s_ftr_idn) meta_idn                                                                                         ");
            query.AppendLine("          from si_pipe_block                                                                                                   ");
            query.AppendLine("         where s_name = 'WTL_META_PS'                                                                                          ");
            query.AppendLine("         group by dmno                                                                                                         ");
            query.AppendLine("       ) me                                                                                                                    ");
            query.AppendLine(" where dmi.lftridn = loc.lftridn																								 ");
            query.AppendLine("   and dmi.mftridn = loc.mftridn																								 ");
            query.AppendLine("   and dmi.sftridn = loc.sftridn																								 ");
            query.AppendLine("   and dmw.dmno = dmi.dmno																									 ");
            query.AppendLine("   and mtr.dmno = dmw.dmno																									 ");
            query.AppendLine("   and me.dmno(+) = mtr.dmno                                                                                                   ");
            query.AppendLine(" order by																														 ");
            query.AppendLine("       loc.ord																												 ");
            query.AppendLine("      ,dmi.dmno																												 ");
            query.AppendLine("      ,mtr.mddt																												 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록별 관로 정보
        public void SelectBlockPipeInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(                                                                                                                              ");
            query.AppendLine("select c1.loc_code                                                                                                             ");
            query.AppendLine("      ,c1.sgccd                                                                                                                ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                        ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))       ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                        ");
            query.AppendLine("      ,c1.ord                                                                                                                  ");
            query.AppendLine("  from                                                                                                                         ");
            query.AppendLine("      (                                                                                                                        ");
            query.AppendLine("       select sgccd                                                                                                            ");
            query.AppendLine("             ,loc_code                                                                                                         ");
            query.AppendLine("             ,ploc_code                                                                                                        ");
            query.AppendLine("             ,loc_name                                                                                                         ");
            query.AppendLine("             ,ftr_idn                                                                                                          ");
            query.AppendLine("             ,ftr_code                                                                                                         ");
            query.AppendLine("             ,res_code                                                                                                         ");
            query.AppendLine("             ,kt_gbn                                                                                                           ");
            query.AppendLine("             ,rel_loc_name                                                                                                     ");
            query.AppendLine("             ,rownum ord                                                                                                       ");
            query.AppendLine("         from cm_location                                                                                                      ");
            query.AppendLine("        where 1 = 1                                                                                                            ");
            query.AppendLine("          and ftr_code = decode(:FTR_CDE, 'BZ001', 'BZ002', 'BZ003')                                                           ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                        ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                  ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                              ");
            query.AppendLine("      ) c1                                                                                                                     ");
            query.AppendLine("       ,cm_location c2                                                                                                         ");
            query.AppendLine("       ,cm_location c3                                                                                                         ");
            query.AppendLine(" where 1 = 1                                                                                                                   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                           ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                           ");
            query.AppendLine(" order by c1.ord                                                                                                               ");
            query.AppendLine(")                                                                                                                              ");
            query.AppendLine("select loc.loc_code                                                                                                            ");
            query.AppendLine("      ,loc.lblock                                                                                                              ");
            query.AppendLine("      ,loc.mblock                                                                                                              ");
            query.AppendLine("      ,loc.sblock                                                                                                              ");
            query.AppendLine("      ,a.pip_knd                                                                                                               ");
            query.AppendLine("  from loc                                                                                                                     ");
            query.AppendLine("      ,(select decode(rownum, 1, '급수관로', '상수관로') pip_knd from dual connect by rownum <= 2) a							 ");
            query.AppendLine(" order by																														 ");
            query.AppendLine("       loc.ord																												 ");
            query.AppendLine("      ,a.pip_knd																												 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //블록별 관로 길이
        public void SelectBlockPipeLen(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                ");
            query.AppendLine("(																							 ");
            query.AppendLine("select c1.loc_code																		 ");
            query.AppendLine("	  ,tmp.year_mon																			 ");
            query.AppendLine("	  ,c1.ord																				 ");
            query.AppendLine("  from																					 ");
            query.AppendLine("	  (																						 ");
            query.AppendLine("	   select sgccd																			 ");
            query.AppendLine("			 ,loc_code																		 ");
            query.AppendLine("			 ,ploc_code																		 ");
            query.AppendLine("			 ,loc_name																		 ");
            query.AppendLine("			 ,ftr_idn																		 ");
            query.AppendLine("			 ,ftr_code																		 ");
            query.AppendLine("			 ,res_code																		 ");
            query.AppendLine("			 ,kt_gbn																		 ");
            query.AppendLine("			 ,rel_loc_name																	 ");
            query.AppendLine("			 ,rownum ord																	 ");
            query.AppendLine("		 from cm_location																	 ");
            query.AppendLine("		where 1 = 1																			 ");
            query.AppendLine("		and ftr_code = decode(:FTR_CODE, 'BZ001', 'BZ002', 'BZ003')							 ");
            query.AppendLine("		start with loc_code = :LOC_CODE														 ");
            query.AppendLine("		connect by prior loc_code = ploc_code												 ");
            query.AppendLine("		order SIBLINGS by ftr_idn															 ");
            query.AppendLine("	  ) c1																					 ");
            query.AppendLine("	   ,cm_location c2																		 ");
            query.AppendLine("	   ,cm_location c3																		 ");
            query.AppendLine("	 ,(																						 ");

            query.AppendLine("select to_char(add_months(to_date(:STARTDATE,'yyyymm'),rownum-1),'yyyymm') year_mon");
            query.AppendLine("  from dual connect by rownum <= months_between(to_date(:ENDDATE,'yyyymm'), to_date(:STARTDATE,'yyyymm')) + 1");

            //query.AppendLine("	  select to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm') year_mon			 ");
            //query.AppendLine("		from if_timestamp_yyyymmddhh24 ity													 ");
            //query.AppendLine("	   where ity.yyyymmddhh24 between :STARTDATE||'0100' and :ENDDATE||'0100'				 ");
            //query.AppendLine("	   group by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')					 ");
            //query.AppendLine("	   order by to_char(to_date(ity.yyyymmddhh24,'yyyymmddhh24'),'yyyymm')					 ");
            
            query.AppendLine("	  ) tmp																					 ");
            query.AppendLine(" where 1 = 1																				 ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)														 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)														 ");
            query.AppendLine(" order by																					 ");
            query.AppendLine("	   c1.ord																				 ");
            query.AppendLine("	  ,tmp.year_mon																			 ");
            query.AppendLine(")																							 ");
            query.AppendLine("select to_char(to_date(loc.year_mon,'yyyymm'),'yyyy-mm') year_mon							 ");
            query.AppendLine("      ,sum(sply_len) sply_len																 ");
            query.AppendLine("      ,sum(pipe_len) pipe_len																 ");
            query.AppendLine("  from loc																				 ");
            query.AppendLine("      ,(																					 ");
            query.AppendLine("       select loc_code																	 ");
            query.AppendLine("             ,year_mon																	 ");
            query.AppendLine("             ,pip_knd																		 ");
            query.AppendLine("             ,saa_cde																		 ");
            query.AppendLine("             ,decode(pip_knd, '급수관로', max(to_number(pip_len))) sply_len				 ");
            query.AppendLine("             ,decode(pip_knd, '상수관로', max(to_number(pip_len))) pipe_len				 ");
            query.AppendLine("         from																				 ");
            query.AppendLine("             (																			 ");
            query.AppendLine("              select loc_code																 ");
            query.AppendLine("                    ,substr(datee,1,6) year_mon											 ");
            query.AppendLine("                    ,'상수관로' pip_knd													 ");
            query.AppendLine("                    ,saa_cde																 ");
            query.AppendLine("                    ,pip_len																 ");
            query.AppendLine("                from wv_pipe_lm_day														 ");
            query.AppendLine("               union all																	 ");
            query.AppendLine("              select loc_code																 ");
            query.AppendLine("                    ,substr(datee,1,6) year_mon											 ");
            query.AppendLine("                    ,'급수관로' pip_knd													 ");
            query.AppendLine("                    ,saa_cde																 ");
            query.AppendLine("                    ,pip_len																 ");
            query.AppendLine("                from wv_sply_ls_day														 ");
            query.AppendLine("              )																			 ");
            query.AppendLine("        where year_mon between :STARTDATE and :ENDDATE									 ");
            query.AppendLine("        group by																			 ");
            query.AppendLine("              loc_code																	 ");
            query.AppendLine("             ,year_mon																	 ");
            query.AppendLine("             ,pip_knd																		 ");
            query.AppendLine("             ,saa_cde																		 ");
            query.AppendLine("      ) a																					 ");
            query.AppendLine(" where a.loc_code(+) = loc.loc_code														 ");
            query.AppendLine("   and a.year_mon(+) = loc.year_mon														 ");
            query.AppendLine(" group by																					 ");
            query.AppendLine("       loc.ord																			 ");
            query.AppendLine("      ,loc.loc_code																		 ");
            query.AppendLine("      ,loc.year_mon																		 ");
            query.AppendLine(" order by																					 ");
            query.AppendLine("       loc.ord																			 ");
            query.AppendLine("      ,loc.year_mon																		 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["STARTDATE"];
            parameters[5].Value = parameter["STARTDATE"];
            parameters[6].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //관로 정보
        public void SelectPipeInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                   ");
            query.AppendLine("(																																");
            query.AppendLine("select c1.loc_code																											");
            query.AppendLine("	  ,c1.sgccd																													");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))	");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock											");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))	");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn											");
            query.AppendLine("	  ,c1.ord																													");
            query.AppendLine("  from																														");
            query.AppendLine("	  (																															");
            query.AppendLine("	   select sgccd																												");
            query.AppendLine("			 ,loc_code																											");
            query.AppendLine("			 ,ploc_code																											");
            query.AppendLine("			 ,loc_name																											");
            query.AppendLine("			 ,ftr_idn																											");
            query.AppendLine("			 ,ftr_code																											");
            query.AppendLine("			 ,res_code																											");
            query.AppendLine("			 ,kt_gbn																											");
            query.AppendLine("			 ,rel_loc_name																										");
            query.AppendLine("			 ,rownum ord																										");
            query.AppendLine("		 from cm_location																										");
            query.AppendLine("		where 1 = 1																												");
            query.AppendLine("		  and ftr_code = 'BZ003'																								");
            query.AppendLine("		start with loc_code = :LOC_CODE																							");
            query.AppendLine("		connect by prior loc_code = ploc_code																					");
            query.AppendLine("		order SIBLINGS by ftr_idn																								");
            query.AppendLine("	 ) c1																														");
            query.AppendLine("	  ,cm_location c2																											");
            query.AppendLine("	  ,cm_location c3																											");
            query.AppendLine(" where 1 = 1																													");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							");
            query.AppendLine(" order by c1.ord																												");
            query.AppendLine(")																																");
            query.AppendLine("select loc.loc_code																											");
            query.AppendLine("      ,loc.lblock																												");
            query.AppendLine("      ,loc.mblock																												");
            query.AppendLine("      ,loc.sblock																												");
            query.AppendLine("      ,decode(spb.s_name,'WTL_SPLY_LS','급수관로','상수관로') ftr_knd															");
            query.AppendLine("      ,spb.saa_cde																											");
            query.AppendLine("      ,spb.s_ftr_idn ftr_idn																									");
            query.AppendLine("      ,spb.mop_cde																											");
            query.AppendLine("      ,spb.pip_dip																											");
            query.AppendLine("      ,spb.pip_len																											");
            query.AppendLine("  from loc																													");
            query.AppendLine("      ,si_pipe_block spb																										");
            query.AppendLine(" where spb.b_ftr_idn = loc.sftridn																							");
            query.AppendLine(" order by																														");
            query.AppendLine("       loc.ord																												");
            query.AppendLine("      ,spb.s_name																												");
            query.AppendLine("      ,spb.saa_cde																											");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //대수용가조회
        public void SelectBigUsedDM(OracleDBManager manager, DataSet dataSet, string dataMember, string DMNO)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("select dmno from wv_lconsumer_link ");
            query.AppendLine("where dmno = :DMNO                 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("DMNO", OracleDbType.Varchar2)
                };

            parameters[0].Value = DMNO;

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }
        //대수용가선정
        public void InsertBigUsedDM(OracleDBManager manager, string DMNO)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("insert into wv_lconsumer_link ");
            query.AppendLine("select :DMNO from dual        ");

            IDataParameter[] parameters =  {
                     new OracleParameter("DMNO", OracleDbType.Varchar2)
                };

            parameters[0].Value = DMNO;

            manager.ExecuteScript(query.ToString(), parameters);
        }
        //대수용가제외
        public void DeleteBigUsedDM(OracleDBManager manager, string DMNO)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("delete wv_lconsumer_link ");
            query.AppendLine(" where dmno = :DMNO      ");

            IDataParameter[] parameters =  {
                     new OracleParameter("DMNO", OracleDbType.Varchar2)
                };

            parameters[0].Value = DMNO;

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
