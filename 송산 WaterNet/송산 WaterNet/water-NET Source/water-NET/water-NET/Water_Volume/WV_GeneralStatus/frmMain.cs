﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace WV_GeneralStatus
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            InitializeStyle(this);
        }

        private void InitializeStyle(Control control)
        {
            control.Padding = new Padding(10, 10, 10, 10);

            IEnumerator enumerator = control.Controls.GetEnumerator();

            enumerator.Reset();
            while (enumerator.MoveNext())
            {
                Control item = enumerator.Current as Control;

                if (item.Controls.Count > 0)
                {
                    InitializeStyle(item);
                }

                //item.Padding = new Padding(10, 10, 10, 10);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }
    }
}
