﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WV_WaterPressureControlInformation.form
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
            SizeChanged += new EventHandler(frmMain_SizeChanged);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            ultraGrid1.Select();
        }

        //form 넓이가 변하면 검색박스 영역 높이조정
        private void frmMain_SizeChanged(object sender, EventArgs e)
        {
            if (this.Width >= 985 && groupBox1.Height != 86)
            {
                groupBox1.Height = 86;
            }

            if (this.Width <= 984 && groupBox1.Height != 116)
            {
                groupBox1.Height = 116;
            }
        }
    }
}
