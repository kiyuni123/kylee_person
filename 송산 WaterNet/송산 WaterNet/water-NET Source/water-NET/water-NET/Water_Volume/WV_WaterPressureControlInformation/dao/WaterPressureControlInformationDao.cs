﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WV_WaterPressureControlInformation.dao
{
    public class WaterPressureControlInformationDao
    {
        private static WaterPressureControlInformationDao dao = null;

        private WaterPressureControlInformationDao() { }

        public static WaterPressureControlInformationDao GetInstance()
        {
            if (dao == null)
            {
                dao = new WaterPressureControlInformationDao();
            }
            return dao;
        }
    }
}
