﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_WaterPressureControlInformation.dao;
using WaterNet.WV_Common.work;

namespace WaterNet.WV_WaterPressureControlInformation.work
{
    public class WaterPressureControlInformationWork : BaseWork
    {
        private static WaterPressureControlInformationWork work = null;
        private WaterPressureControlInformationDao dao = null;

        public static WaterPressureControlInformationWork GetInstance()
        {
            if (work == null)
            {
                work = new WaterPressureControlInformationWork();
            }
            return work;
        }

        private WaterPressureControlInformationWork()
        {
            dao = WaterPressureControlInformationDao.GetInstance();
        }
    }
}
