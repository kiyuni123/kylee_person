﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp
{
    public partial class FrmFindResCode : Form
    {
        private List<BlockItem> _blockList = null;
        public string Selected { get; set; }

        public FrmFindResCode()
        {
            InitializeComponent();
        }

        public FrmFindResCode(List<BlockItem> blockList) : this()
        {
            this._blockList = blockList;
        }

        private void FrmFindResCode_Load(object sender, EventArgs e)
        {
            try
            {
                // 목록 생성

                ListViewItem nullItem = new ListViewItem("없음");
                nullItem.SubItems.Add("");
                listView1.Items.Add(nullItem);

                var objects = from BlockItem item in _blockList
                              where item.LOC_GBN.IndexOf("블록") < 0
                              select item;
                if (objects != null && objects.Count() > 0)
                {
                    for (int i = 0; i < objects.Count(); i++)
                    {
                        BlockItem block = objects.ElementAt(i);
                        if (block.LOC_NAME == "" || block.LOC_NAME.Length <= 0) continue;

                        ListViewItem item = new ListViewItem(block.LOC_NAME);
                        item.SubItems.Add(block.LOC_CODE);
                        listView1.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private TimeSpan ts = new TimeSpan();

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (ts == null) ts = new TimeSpan(DateTime.Now.Ticks);

            TimeSpan tsNow = new TimeSpan(DateTime.Now.Ticks);
            TimeSpan subs = tsNow.Subtract(ts);
            ts = tsNow;

            if (subs.TotalMilliseconds <= 2000) btnSelect_Click(this, null);            
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems == null || listView1.SelectedItems.Count <= 0) return;
            Selected = listView1.SelectedItems[0].SubItems[1].Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}
