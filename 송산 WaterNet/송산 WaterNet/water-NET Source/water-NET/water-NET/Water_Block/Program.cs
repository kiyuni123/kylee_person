﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WaterNet.BlockApp.TagManage;

using ESRI.ArcGIS.esriSystem;

namespace WaterNet.BlockApp
{
    static class Program
    {
        private static LicenseInitializer m_AOLicenseInitializer = new BlockApp.LicenseInitializer();
        public static string CONNECTION_KEY = "121";
        public static string CONNECTION_KEY_MAIN = "MAIN";
        public static string SID = "WN_MANAGER";

        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            m_AOLicenseInitializer.InitializeApplication(new esriLicenseProductCode[] { 
                esriLicenseProductCode.esriLicenseProductCodeEngine },
                new esriLicenseExtensionCode[] { }
            );

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string svcName = "waternet";
            string ip = "emdj.iptime.org";
            string id = "WNIMS";
            //string id = "WN_MANAGER";
            string passwd = "wnims123";
            //string passwd = "1234";
            string port = "1521";

            //string conStr = "Data Source=(DESCRIPTION="
            //    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
            //    + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
            //    + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";
            string conStr = "Data Source=(DESCRIPTION="
                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1})))"
                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={2})));"
                + "User Id={3};Password={4};Enlist=false";

            //EMFrame.dm.EMapper.ConnectionString.Add(CONNECTION_KEY_MAIN, conStr);
            //EMFrame.dm.EMapper.ConnectionString.Add(CONNECTION_KEY, conStr);
            EMFrame.dm.EMapper.ConnectionString.Add(CONNECTION_KEY_MAIN, string.Format(conStr, ip, port, svcName, id, passwd));
            
            Application.Run(new FrmShapeBlockManager());
            // 처음화면이 TAGMANAGER 화면이 생성되게 한다.
            //Application.Run(new FrmTagManage());
        }
    }
}
