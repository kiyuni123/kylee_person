﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp
{
    public class BlockItem
    {
        /// <summary>
        /// 계층 단계
        /// </summary>
        public int LEVEL { get; set; }

        /// <summary>
        /// FTR_CODE
        /// </summary>
        public string FTR_CODE { get; set; }
        /// <summary>
        /// FTR_IDN
        /// </summary>
        public string FTR_IDN { get; set; }
        /// <summary>
        /// 상위 FTR_CODE
        /// </summary>
        public string PFTR_CODE { get; set; }
        /// <summary>
        /// 상위 FTR_IDN
        /// </summary>
        public string PFTR_IDN { get; set; }

        /// <summary>
        /// 지역관리코드
        /// </summary>
        public string LOC_CODE { get; set; }
        /// <summary>
        /// 상위 지역관리코드
        /// </summary>
        public string PLOC_CODE { get; set; }

        /// <summary>
        /// 블록명
        /// </summary>
        public string LOC_NAME { get; set; }
        /// <summary>
        /// 관련지역명
        /// </summary>
        public string REL_LOC_NAME { get; set; }

        /// <summary>
        /// 관련배수지코드
        /// </summary>
        public string RES_CODE { get; set; }
        /// <summary>
        /// 계통구분
        /// </summary>
        public string KT_GBN { get; set; }
        /// <summary>
        /// Order By
        /// </summary>
        public int ORDERBY { get; set; }
        
        /// <summary>
        /// 지자체코드
        /// </summary>
        public string SGCCD { get; set; }
        /// <summary>
        /// 블록 구분 (대블록/중블록/소블록/...)
        /// </summary>
        public string LOC_GBN { get; set; }

        /// <summary>
        /// Table 구분 (1:CM_LOCATION+CM_LOCATION2 / 2:CM_LOCATION2 ONLY) (TB_GBN -> CM_LOC_GBN 변경)
        /// </summary>
        public string CM_LOC_GBN { get; set; }

        /// <summary>
        /// 자식 BlockItem
        /// </summary>
        public List<BlockItem> Child { get; set; }

        /// <summary>
        /// Shape 정보와의 매칭 여부
        /// </summary>
        public bool IsMatch { get; set; }

        /// <summary>
        /// Shape에는 있지만 Block에 없어서 임시로 생성된 데이터
        /// </summary>
        public bool IsTemp { get; set; }

        /// <summary>
        /// 태그구분 변경 처리시 사용
        /// </summary>
        //public bool IsUpdate { get; set; }

        /// <summary>
        /// Database Columns
        /// </summary>
        //public struct FIELDS
        //{
        //    public const string LEVEL = "LV";
        //    public const string FTR_CODE = "FTR_CODE";
        //    public const string FTR_IDN = "FTR_IDN";
        //    public const string PFTR_CODE = "PFTR_CODE";
        //    public const string PFTR_IDN = "PFTR_IDN";
        //    public const string LOC_CODE = "LOC_CODE";
        //    public const string PLOC_CODE = "PLOC_CODE";
        //    public const string LOC_NAME = "LOC_NAME";
        //    public const string REL_LOC_NAME = "REL_LOC_NAME";
        //    public const string RES_CODE = "RES_CODE";
        //    public const string KT_GBN = "KT_GBN";
        //    public const string ORDERBY = "ORDERBY";
        //    public const string SGCCD = "SGCCD";
        //    public const string LOC_GBN = "LOC_GBN";
        //    public const string CM_LOC_GBN = "CM_LOC_GBN";
        //}
    }

    /// <summary>
    /// Custom comparer for the BlockItem class
    /// </summary>
    class BlockItemComparer : IEqualityComparer<BlockItem>
    {
        /// <summary>
        /// BlockItem are equal if their LOC_CODE and TB_GBN are equal.
        /// </summary>
        /// <param name="a">BlockItem</param>
        /// <param name="b">BlockItem</param>
        /// <returns>bool</returns>
        public bool Equals(BlockItem a, BlockItem b)
        {
            // Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(a, b)) return true;
            // Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(a, null) || Object.ReferenceEquals(a, null)) return true;

            // Check whether the BlockItems' properties are equal.
            return a.LOC_CODE == b.LOC_CODE;
        }

        /// <summary>
        /// If equals() returns true for a pair of objects
        /// then GetHashCode() must return the same value for these objects.
        /// </summary>
        /// <param name="blockItem">BlockItem</param>
        /// <returns>int</returns>
        public int GetHashCode(BlockItem blockItem)
        {
            // Check whether the object is null.
            if (Object.ReferenceEquals(blockItem, null)) return 0;

            // Get hash code for the LOC_CODE field if it is not null.
            int hashLocCode = blockItem.LOC_CODE == null ? 0 : blockItem.LOC_CODE.GetHashCode();

            // Get hash code for the TB_GBN field.
            int hashTbGbn = blockItem.CM_LOC_GBN.GetHashCode();

            // Calculate the hash code for the BlockItem.
            return hashLocCode ^ hashTbGbn;
        }

        #region Simple Code
        //public bool Equals(BlockItem a, BlockItem b)
        //{
        //    return a.LOC_CODE == b.LOC_CODE;
        //}

        //public int GetHashCode(BlockItem blockItem)
        //{
        //    return blockItem.GetHashCode();
        //}
        #endregion
    }
}
