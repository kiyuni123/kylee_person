﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp.SystemManage
{
    /// <summary>
    /// 계통정보 아이템
    /// </summary>
    public class SystemItem
    {
        /// <summary>
        /// 상태 구분
        /// </summary>
        public enum StateType
        {
            None = 0,   // None - 아무런 동작 없음
            New = 1,    // Insert - 신규
            Mod = 2,    // Update - 변경
            Del = 3,    // Delete - 삭제
        }

        public StateType State { get; set; }

        /// <summary>
        /// true:CM_LOCATION / false:WV_SYSTEM_MASTER
        /// </summary>
        public bool IsTemp { get; set; }
        /// <summary>
        /// 1:중블록 / 2:소블록
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// [PK, FK] System Code
        /// </summary>
        public string SYSTEM_CODE { get; set; }
        /// <summary>
        /// System Name (LOC_NAME)
        /// </summary>
        public string SYSTEM_NAME { get; set; }
        /// <summary>
        /// [PK] 지역관리코드
        /// </summary>
        public string LOC_CODE { get; set; }
        /// <summary>
        /// 정렬순번
        /// </summary>
        public int ORDERBY { get; set; }

        public List<SystemItem> List { get; set; }

        public SystemItem()
        {
            this.List = new List<SystemItem>();
            this.IsTemp = true;
            this.State = StateType.None;
        }
    }
}
