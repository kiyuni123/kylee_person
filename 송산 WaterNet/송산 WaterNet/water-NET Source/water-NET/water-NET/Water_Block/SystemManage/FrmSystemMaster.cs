﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.BlockApp.SystemManage
{
    public partial class FrmSystemMaster : Form
    {
        private List<SystemItem> _systemList = null;
        private List<SystemItem> _blockList = null;
        //private int _orderBy1 = 0;
        //private int _orderBy2 = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        #region public FrmSystemMaster()
        public FrmSystemMaster()
        {
            InitializeComponent();
            _systemList = new List<SystemItem>();
        } 
        #endregion

        #region Helper Methods
        /// <summary>
        /// 오류처리
        /// </summary>
        /// <param name="method">오류 메서드명</param>
        /// <param name="ex">오류내용</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion

        /// <summary>
        /// where절에서의 Like 조건절 함수
        /// </summary>
        /// <param name="src">Source text</param>
        /// <param name="find">find text</param>
        /// <returns>boolean</returns>
        #region private bool Like(string src, string find)
        private bool Like(string src, string find)
        {
            if (src.IndexOf(find) >= 0) return true;
            else return false;
        }
        #endregion

        /// <summary>
        /// UltraGrid 전체 Row 삭제
        /// </summary>
        /// <param name="grid">UltraGrid</param>
        #region private void RemoveRowsAll(UltraGrid grid)
        private void RemoveRowsAll(UltraGrid grid)
        {
            grid.Selected.Rows.AddRange((UltraGridRow[])grid.Rows.All);
            grid.DeleteSelectedRows(false);
        }
        #endregion


        #endregion

        #region private void FrmSystemMaster_Load(object sender, EventArgs e)
        private void FrmSystemMaster_Load(object sender, EventArgs e)
        {
            try
            {
                // 계통정보 목록
                DataTable dtSystem = SelectSystemList();
                if (dtSystem == null || dtSystem.Rows.Count <= 0)
                {
                    MessageBox.Show("계통 정보가 없습니다.", "데이터", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                CreateSystemList(dtSystem, _systemList, true);
                dgvGyetong.DataSource = dtSystem;

                // 블록정보 목록
                DataTable dtBlock = SelectBlockList();
                CreateSystemList(dtBlock, _blockList, false);
                dgvBlock.DataSource = dtBlock;
            }
            catch (Exception ex)
            {
                FireException("FrmSystemMaster_Load", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// 초기 System 목록 조회
        /// </summary>
        /// <returns>목록</returns>
        #region private DataTable SelectSystemList()
        private DataTable SelectSystemList()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                dt = mapper.ExecuteScriptDataTable(Database.Queries.SYSTEM_SYSTEM_MASTER_SELECT, null);

                if (dt == null || dt.Rows.Count <= 0)
                {
                    // WV_SYSTEM_MASTER에 데이터가 없을 경우 CM_LOCATION 정보를 사용하여 초기 목록을 생성
                    dt = mapper.ExecuteScriptDataTable(Database.Queries.SYSTEM_CM_LOCATION_SELECT, null);

                    #region 20141211 - CM_LOCATION의 데이터를 WV_SYSTEM_MAPPING으로 복사하여 생성
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        int systemCode = 0;
                        string newSystemCode = "";

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dr = dt.Rows[i];
                            string lv = string.Format("{0}", dr["LV"]);

                            if (lv == "1")
                            {   // 중블록 등록
                                systemCode += 1;
                                newSystemCode = systemCode.ToString().PadLeft(6, '0');

                                #region WV_SYSTEM_MASTER
                                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),  // [0] SYSTEM_CODE  (Where)
                                                  new OracleParameter("2", OracleDbType.Varchar2),  // [1] LOC_CODE     (Where)
                                                  new OracleParameter("3", OracleDbType.Varchar2),  // [2] SYSTEM_NAME
                                                  new OracleParameter("4", OracleDbType.Varchar2),  // [3] LOC_CODE
                                                  new OracleParameter("5", OracleDbType.Varchar2),  // [4] ORDERBY      
                                                  new OracleParameter("6", OracleDbType.Varchar2),  // [5] SYSTEM_CODE  (Where)
                                                  new OracleParameter("7", OracleDbType.Varchar2),  // [6] LOC_CODE     (Where)
                                                  new OracleParameter("8", OracleDbType.Varchar2),  // [7] SYSTEM_CODE
                                                  new OracleParameter("9", OracleDbType.Varchar2),  // [8] SYSTEM_NAME
                                                  new OracleParameter("10", OracleDbType.Varchar2), // [9] LOC_CODE
                                                  new OracleParameter("11", OracleDbType.Varchar2)  // [10] ORDERBY
                                              };
                                parameters[0].Value = newSystemCode;
                                parameters[1].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[2].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME]);
                                parameters[3].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[4].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.ORDERBY]);
                                parameters[5].Value = newSystemCode;
                                parameters[6].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[7].Value = newSystemCode;
                                parameters[8].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME]);
                                parameters[9].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[10].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.ORDERBY]);

                                mapper.ExecuteScript(Database.Queries.SYSTEM_MASTER_INS_UPD, parameters);
                                #endregion
                            }
                            else
                            {   // 소블록 등록
                                #region WV_SYSTEM_MAPPING
                                IDataParameter[] parameters = {
                                                      new OracleParameter("1", OracleDbType.Varchar2),  // [0] SYSTEM_CODE  (Where)
                                                      new OracleParameter("2", OracleDbType.Varchar2),  // [1] LOC_CODE     (Where)
                                                      new OracleParameter("3", OracleDbType.Varchar2),  // [2] SYSTEM_CODE
                                                      new OracleParameter("4", OracleDbType.Varchar2),  // [3] ORDERBY
                                                      new OracleParameter("5", OracleDbType.Varchar2),  // [4] SYSTEM_CODE  (Where)
                                                      new OracleParameter("6", OracleDbType.Varchar2),  // [5] LOC_CODE     (Where)
                                                      new OracleParameter("7", OracleDbType.Varchar2),  // [6] SYSTEM_CODE
                                                      new OracleParameter("8", OracleDbType.Varchar2),  // [7] LOC_CODE
                                                      new OracleParameter("9", OracleDbType.Varchar2)   // [8] ORDERBY
                                                  };

                                parameters[0].Value = newSystemCode;
                                parameters[1].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[2].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[3].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.ORDERBY]);
                                parameters[4].Value = newSystemCode;
                                parameters[5].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[6].Value = newSystemCode;
                                parameters[7].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                                parameters[8].Value = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.ORDERBY]);

                                mapper.ExecuteScript(Database.Queries.SYSTEM_MAPPING_INS_UPD, parameters);
                                #endregion
                            }                            
                        }
                    }
                    #endregion

                    // 다시 조회
                    dt = mapper.ExecuteScriptDataTable(Database.Queries.SYSTEM_SYSTEM_MASTER_SELECT, null);
                }
            }
            catch (Exception ex)
            {
                FireException("SelectSystemList", ex);
            }
            finally
            {
                if (mapper != null) mapper.Dispose();
            }
            return dt;
        } 
        #endregion

        /// <summary>
        /// 블록 목록 조회
        /// </summary>
        /// <returns>목록</returns>
        #region private DataTable SelectBlockList()
        private DataTable SelectBlockList()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                dt = mapper.ExecuteScriptDataTable(Database.Queries.SYSTEM_CM_LOCATION_SELECT, null);
            }
            catch (Exception ex)
            {
                FireException("SelectBlockList", ex);
            }
            finally
            {
                if (mapper != null) mapper.Dispose();
            }
            return dt;
        }
        #endregion

        /// <summary>
        /// 내부 관리용 데이터 생성
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="list">List</param>
        /// <param name="setOrderBy">자동 정렬순번 채번 설정</param>
        #region private void CreateSystemList(DataTable dt, List<SystemItem> list, bool setOrderBy)
        private void CreateSystemList(DataTable dt, List<SystemItem> list, bool setOrderBy)
        {
            try
            {
                if (list == null) list = new List<SystemItem>();
                list.Clear();

                SystemItem pItem = null;
                foreach (DataRow dr in dt.Rows)
                {
                    // 데이터 추출
                    string isTemp = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.IS_TEMP]);
                    string level = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LEVEL]);
                    string systemCode = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE]);
                    string systemName = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME]);
                    string locCode = string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.LOC_CODE]);
                    int orderBy = 0;
                    if (level == "1")
                    {
                        if (!int.TryParse(string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.ORDERBY1]), out orderBy)) orderBy = 0;
                        //if (setOrderBy == true && orderBy > _orderBy1) _orderBy1 = orderBy;
                    }
                    else
                    {
                        if (!int.TryParse(string.Format("{0}", dr[Database.Fields.SYSTEM_MASTER.ORDERBY2]), out orderBy)) orderBy = 0;
                        //if (setOrderBy == true && orderBy > _orderBy2) _orderBy2 = orderBy;
                    }
                    // SystemItem 생성
                    SystemItem item = new SystemItem();
                    if (isTemp == "0") item.IsTemp = true;
                    else item.IsTemp = false;
                    item.Level = Convert.ToInt32(level);
                    item.SYSTEM_CODE = systemCode;
                    item.SYSTEM_NAME = systemName;
                    item.LOC_CODE = locCode;
                    item.ORDERBY = orderBy;
                    if (level == "1")
                    {
                        // 중블록 아이템 임시 저장
                        pItem = item;
                    }
                    else
                    {
                        // 임시 저장된 중블록의 하위로 소블록 등록
                        if (pItem != null) pItem.List.Add(item);
                    }
                    item.State = SystemItem.StateType.None;
                    list.Add(item);
                }
            }
            catch (Exception ex)
            {
                FireException("CreateSystemList", ex);
                throw ex;
            }
        } 
        #endregion





        /// <summary>
        /// 계통정보 그리드 선택 이벤트 (상세정보 출력)
        /// </summary>
        #region private void dgvGyetong_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        private void dgvGyetong_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                lblNew.Visible = false;
                if (dgvGyetong.Selected == null || dgvGyetong.Selected.Rows.Count <= 0) return;

                // 상세정보 추출
                string level = string.Format("{0}", dgvGyetong.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value);
                string systemCode = string.Format("{0}", dgvGyetong.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE].Value);
                string systemName = string.Format("{0}", dgvGyetong.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME].Value);
                string locCode = string.Format("{0}", dgvGyetong.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE].Value);
                string orderBy = string.Format("{0}", dgvGyetong.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.ORDERBY].Value);

                if (level == "1")
                {
                    txtSystemCode.ReadOnly = true;
                    txtSystemName.ReadOnly = false;
                    txtLocCode.ReadOnly = false;                    
                }
                else
                {
                    txtSystemCode.ReadOnly = false;
                    txtSystemName.ReadOnly = true;
                    txtLocCode.ReadOnly = true;
                }
                // 상세정보 출력
                txtSystemCode.Text = systemCode;
                txtSystemName.Text = systemName;
                txtLocCode.Text = locCode;
                txtOrderBy.Text = orderBy;
            }
            catch (Exception ex)
            {
                FireException("dgvGyetong_AfterSelectChange", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion





        #region Drag & Drop
        /// <summary>
        /// 블록 그리드에서 Drag 시작 이벤트
        /// </summary>
        #region private void dgvBlock_SelectionDrag(object sender, CancelEventArgs e)
        private void dgvBlock_SelectionDrag(object sender, CancelEventArgs e)
        {
            if (lblNew.Visible == true) return;
            // 선택 없을시 종료
            if (dgvBlock.Selected == null || dgvBlock.Selected.Rows.Count <= 0) return;
            // Drag Drop 시작
            dgvBlock.DoDragDrop(dgvBlock.Selected.Rows, DragDropEffects.Copy);
        } 
        #endregion

        /// <summary>
        /// 계통 그리드에 Drop 시작 이벤트
        /// </summary>
        #region private void dgvGyetong_DragOver(object sender, DragEventArgs e)
        private void dgvGyetong_DragOver(object sender, DragEventArgs e)
        {
            // 블록 선택이 없을시 종료
            if (dgvBlock.Selected == null || dgvBlock.Selected.Rows.Count <= 0) return;
            e.Effect = DragDropEffects.Copy;
        } 
        #endregion

        /// <summary>
        /// 계통 그리드에 Drop 처리 이벤트
        /// </summary>
        #region private void dgvGyetong_DragDrop(object sender, DragEventArgs e)
        private void dgvGyetong_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (dgvBlock.Selected == null || dgvBlock.Selected.Rows.Count <= 0) return;

                if (e.Effect == DragDropEffects.Copy)
                {
                    // 끌어 놓은 위치의 계통 Row 찾기
                    Point clientPoint = dgvGyetong.PointToClient(new Point(e.X, e.Y));
                    UIElement uieOver = dgvGyetong.DisplayLayout.UIElement.ElementFromPoint(clientPoint);
                    UltraGridRow gyetongRow = uieOver.GetContext(typeof(UltraGridRow), true) as UltraGridRow;
                    if (gyetongRow == null) return;

                    // 계통 정보 추출
                    string gyetongLevel = string.Format("{0}", gyetongRow.Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value);
                    string gyetongSystemCode = string.Format("{0}", gyetongRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE].Value);
                    string gyetongSystemName = string.Format("{0}", gyetongRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME].Value);
                    string gyetongLocCode = string.Format("{0}", gyetongRow.Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE].Value);
                    string gyetongOrderBy = string.Format("{0}", gyetongRow.Cells[Database.Fields.SYSTEM_MASTER.ORDERBY].Value);

                    // 중블록의 정보 찾기
                    SystemItem gyetongItem = _systemList.Find(item => item.SYSTEM_CODE == gyetongSystemCode);
                    if (gyetongItem == null) return;

                    UltraGridRow lastRow = null;
                    int idx = gyetongRow.Index + 1; // 끌어 놓은 Row의 다음 Row에 삽입
                    
                    // 중블록 추가시에는 1Row만 Drop
                    if (dgvBlock.Selected.Rows.Count == 1)
                    {
                        // 계통 정보 추출
                        string level = string.Format("{0}", dgvBlock.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value);
                        string systemName = string.Format("{0}", dgvBlock.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME].Value);
                        string locCode = string.Format("{0}", dgvBlock.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE].Value);

                        // 중블록만 처리
                        if (level == "1")
                        {
                            #region 중블록 처리
                            // LOC_CODE로 중복 체크
                            if (_systemList.FindIndex(item => item.LOC_CODE == locCode) > 0)
                            {
                                MessageBox.Show("이미 등록된 중블록입니다. (LOC_CODE:" + locCode + ")", "중복", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            // 다음번 System Code 찾기
                            string systemCode = _systemList.Max(item => item.SYSTEM_CODE);
                            int maxSystemCode = 0;
                            if (int.TryParse(systemCode, out maxSystemCode)) maxSystemCode += 1;
                            else maxSystemCode = 1;

                            systemCode = maxSystemCode.ToString().PadLeft(6, '0');

                            // 다음번 정렬순번 찾기
                            //int orderBy = _systemList.Max(item => item.ORDERBY);
                            int orderBy = _systemList.FindAll(item => item.Level == 1).Max(item => item.ORDERBY);
                            orderBy += 1;

                            // 내부 아이템 생성
                            SystemItem newItem = new SystemItem();
                            //_orderBy1 += 1;
                            newItem.State = SystemItem.StateType.New;
                            newItem.IsTemp = true;
                            newItem.Level = 1;
                            newItem.SYSTEM_CODE = systemCode;
                            newItem.SYSTEM_NAME = systemName;
                            newItem.LOC_CODE = locCode;
                            newItem.ORDERBY = orderBy;

                            // 내부 관리 목록에 추가
                            _systemList.Add(newItem);

                            // 계통정보 그리드 처리
                            UltraGridRow newRow = dgvGyetong.DisplayLayout.Bands[0].AddNew();
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.IS_TEMP].Value = "0";
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE1].Value = systemCode;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME1].Value = systemName;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE1].Value = locCode;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.ORDERBY1].Value = orderBy;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.ORD1].Value = orderBy;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE2].Value = "";
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME2].Value = "";
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE2].Value = "";
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value = "1";
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE].Value = systemCode;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME].Value = systemName;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE].Value = locCode;
                            newRow.Cells[Database.Fields.SYSTEM_MASTER.ORDERBY].Value = orderBy;
                            // 위치 이동
                            //newRow.ParentCollection.Move(newRow, idx);
                            #endregion
                        }
                    }

                    for (int i = 0; i < dgvBlock.Selected.Rows.Count; i++)
                    {
                        // 계통 정보 추출
                        string level = string.Format("{0}", dgvBlock.Selected.Rows[i].Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value);
                        //string systemCode = string.Format("{0}", dgvBlock.Selected.Rows[i].Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE].Value);
                        string systemName = string.Format("{0}", dgvBlock.Selected.Rows[i].Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME].Value);
                        string locCode = string.Format("{0}", dgvBlock.Selected.Rows[i].Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE].Value);
                        
                        // LV:2가 아닐 경우 소블록이 아니므로 패스
                        if (level != "2") continue;

                        // 중복 체크
                        var dupObjs = from SystemItem item in _systemList
                                      where item.SYSTEM_CODE == gyetongSystemCode && item.LOC_CODE == locCode
                                      select item;
                        if (dupObjs != null && dupObjs.Count() > 0) continue;

                        //int orderBy = _systemList.FindAll(item => item.Level == 2).Max(item => item.ORDERBY);
                        int orderBy = 0;
                        foreach (SystemItem sysItem in _systemList)
                        {
                            if (sysItem.Level != 2) continue;
                            if (sysItem.ORDERBY != null && sysItem.ORDERBY > orderBy) orderBy = sysItem.ORDERBY;
                        }
                        orderBy += 1;

                        // SystemItem 생성
                        SystemItem newItem = new SystemItem();
                        newItem.State = SystemItem.StateType.New;
                        newItem.IsTemp = true;
                        newItem.Level = 2;
                        newItem.SYSTEM_CODE = gyetongSystemCode;
                        newItem.SYSTEM_NAME = systemName;
                        newItem.LOC_CODE = locCode;
                        newItem.ORDERBY = orderBy;
                        
                        // 내부 관리 목록에 추가
                        _systemList.Add(newItem);
                        // 중블록에 추가
                        gyetongItem.List.Add(newItem);

                        // 계통정보 그리드 처리
                        UltraGridRow newRow = dgvGyetong.DisplayLayout.Bands[0].AddNew();
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.IS_TEMP].Value = "0";
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE1].Value = "";
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME1].Value = "";
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE1].Value = "";
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.ORDERBY1].Value = "";
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.ORD1].Value = gyetongOrderBy;
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE2].Value = gyetongSystemCode;
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME2].Value = systemName;
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE2].Value = locCode;
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value = "2";
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE].Value = gyetongSystemCode;
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_NAME].Value = systemName;
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE].Value = locCode;
                        newRow.Cells[Database.Fields.SYSTEM_MASTER.ORDERBY].Value = orderBy;
                        // 위치 이동
                        newRow.ParentCollection.Move(newRow, idx);
                        idx += 1;

                        lastRow = newRow;
                    }

                    // 모든 처리를 끝낸 이후 스크롤 이동
                    if (lastRow != null) dgvGyetong.ActiveRowScrollRegion.ScrollRowIntoView(lastRow);
                }
            }
            catch (Exception ex)
            {
                FireException("dgvGyetong_DragDrop", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion
        #endregion





        /// <summary>
        /// 신규 버튼
        /// </summary>
        #region private void btnNew_Click(object sender, EventArgs e)
        private void btnNew_Click(object sender, EventArgs e)
        {
            string systemCode = _systemList.Max(item => item.SYSTEM_CODE);
            int maxSystemCode = 0;
            if (int.TryParse(systemCode, out maxSystemCode)) maxSystemCode += 1;
            else maxSystemCode = 1;
            
            systemCode = maxSystemCode.ToString().PadLeft(6, '0');

            int orderBy = _systemList.FindAll(item => item.Level == 1).Max(item => item.ORDERBY);
            orderBy += 1;

            lblNew.Visible = true;
            txtSystemCode.ReadOnly = false;
            txtSystemName.ReadOnly = false;
            txtLocCode.ReadOnly = false;

            txtSystemCode.Text = systemCode;
            txtSystemName.Text = "";
            txtLocCode.Text = "";
            txtOrderBy.Text = orderBy.ToString();

            txtSystemName.Focus();
        } 
        #endregion

        #region 저장 처리 Methods
        /// <summary>
        /// WV_SYSTEM_MASTER 저장
        /// </summary>
        /// <param name="mapper">EMFrame.dm.EMapper</param>
        #region private void SaveSystem(EMFrame.dm.EMapper mapper)
        private void SaveSystem(EMFrame.dm.EMapper mapper)
        {
            try
            {
                if (lblNew.Visible == false && (dgvGyetong.Selected == null || dgvGyetong.Selected.Rows.Count <= 0)) return;

                string level = "";
                if (lblNew.Visible == true) level = "1";
                else level = string.Format("{0}", dgvGyetong.Selected.Rows[0].Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value);

                SystemItem systemItem = null;

                if (level == "1")
                {
                    systemItem = _systemList.Find(item => item.SYSTEM_CODE == txtSystemCode.Text);
                    
                    string systemCodeOrg = "";
                    string locCodeOrg = "";

                    if (systemItem == null)
                    {
                        systemCodeOrg = txtSystemCode.Text;
                        locCodeOrg = txtLocCode.Text;
                    }
                    else
                    {
                        systemCodeOrg = systemItem.SYSTEM_CODE;
                        locCodeOrg = systemItem.LOC_CODE;
                    }

                    // WV_SYSTEM_MASTER
                    IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),  // [0] SYSTEM_CODE  (Where)
                                                  new OracleParameter("2", OracleDbType.Varchar2),  // [1] LOC_CODE     (Where)
                                                  new OracleParameter("3", OracleDbType.Varchar2),  // [2] SYSTEM_NAME
                                                  new OracleParameter("4", OracleDbType.Varchar2),  // [3] LOC_CODE
                                                  new OracleParameter("5", OracleDbType.Varchar2),  // [4] ORDERBY      
                                                  new OracleParameter("6", OracleDbType.Varchar2),  // [5] SYSTEM_CODE  (Where)
                                                  new OracleParameter("7", OracleDbType.Varchar2),  // [6] LOC_CODE     (Where)
                                                  new OracleParameter("8", OracleDbType.Varchar2),  // [7] SYSTEM_CODE
                                                  new OracleParameter("9", OracleDbType.Varchar2),  // [8] SYSTEM_NAME
                                                  new OracleParameter("10", OracleDbType.Varchar2), // [9] LOC_CODE
                                                  new OracleParameter("11", OracleDbType.Varchar2)  // [10] ORDERBY
                                              };
                    parameters[0].Value = systemCodeOrg;
                    parameters[1].Value = locCodeOrg;
                    parameters[2].Value = txtSystemName.Text;
                    parameters[3].Value = txtLocCode.Text;
                    parameters[4].Value = txtOrderBy.Text;
                    parameters[5].Value = systemCodeOrg;
                    parameters[6].Value = locCodeOrg;
                    parameters[7].Value = txtSystemCode.Text;
                    parameters[8].Value = txtSystemName.Text;
                    parameters[9].Value = txtLocCode.Text;
                    parameters[10].Value = txtOrderBy.Text;

                    mapper.ExecuteScript(Database.Queries.SYSTEM_MASTER_INS_UPD, parameters);
                }
                else
                {
                    systemItem = _systemList.Find(item => item.LOC_CODE == txtLocCode.Text);

                    string systemCodeOrg = "";
                    string locCodeOrg = "";

                    if (systemItem == null)
                    {
                        systemCodeOrg = txtSystemCode.Text;
                        locCodeOrg = txtLocCode.Text;
                    }
                    else
                    {
                        systemCodeOrg = systemItem.SYSTEM_CODE;
                        locCodeOrg = systemItem.LOC_CODE;
                    }

                    // WV_SYSTEM_MAPPING
                    IDataParameter[] parameters = {
                                                      new OracleParameter("1", OracleDbType.Varchar2),  // [0] SYSTEM_CODE  (Where)
                                                      new OracleParameter("2", OracleDbType.Varchar2),  // [1] LOC_CODE     (Where)
                                                      new OracleParameter("3", OracleDbType.Varchar2),  // [2] SYSTEM_CODE
                                                      new OracleParameter("4", OracleDbType.Varchar2),  // [3] ORDERBY
                                                      new OracleParameter("5", OracleDbType.Varchar2),  // [4] SYSTEM_CODE  (Where)
                                                      new OracleParameter("6", OracleDbType.Varchar2),  // [5] LOC_CODE     (Where)
                                                      new OracleParameter("7", OracleDbType.Varchar2),  // [6] SYSTEM_CODE
                                                      new OracleParameter("8", OracleDbType.Varchar2),  // [7] LOC_CODE
                                                      new OracleParameter("9", OracleDbType.Varchar2)   // [8] ORDERBY
                                                  };
                    parameters[0].Value = systemCodeOrg;
                    parameters[1].Value = locCodeOrg;
                    parameters[2].Value = txtSystemCode.Text;
                    parameters[3].Value = txtOrderBy.Text;
                    parameters[4].Value = systemCodeOrg;
                    parameters[5].Value = locCodeOrg;
                    parameters[6].Value = txtSystemCode.Text;
                    parameters[7].Value = txtLocCode.Text;
                    parameters[8].Value = txtOrderBy.Text;

                    mapper.ExecuteScript(Database.Queries.SYSTEM_MAPPING_INS_UPD, parameters);
                }
            }
            catch (Exception ex)
            {
                FireException("SaveSystem", ex);
                throw ex;
            }
        } 
        #endregion

        /// <summary>
        /// WV_SYSTEM_MAPPING 저장
        /// </summary>
        /// <param name="mapper">EMFrame.dm.Emapper</param>
        #region private void SaveMapping(EMFrame.dm.EMapper mapper)
        private void SaveMapping(EMFrame.dm.EMapper mapper)
        {
            try
            {
                for (int i = 0; i < _systemList.Count; i++)
                {
                    SystemItem item = _systemList[i];
                    // 소블록이 아닐 경우 패스
                    if (item.Level != 2) continue;
                    // 상태 변경 없다면 패스
                    if (item.State == SystemItem.StateType.None) continue;

                    if (item.State == SystemItem.StateType.Del)
                    {
                        IDataParameter[] parameters = {
                                                          new OracleParameter("1", OracleDbType.Varchar2),  // [0] SYSTEM_CODE
                                                          new OracleParameter("2", OracleDbType.Varchar2)   // [1] LOC_CODE
                                                      };
                        parameters[0].Value = item.SYSTEM_CODE;
                        parameters[1].Value = item.LOC_CODE;

                        mapper.ExecuteScript(Database.Queries.SYSTEM_MAPPING_DELETE, parameters);
                    }
                    else
                    {
                        IDataParameter[] parameters = {
                                                          new OracleParameter("1", OracleDbType.Varchar2),  // [0] SYSTEM_CODE
                                                          new OracleParameter("2", OracleDbType.Varchar2),  // [1] LOC_CODE
                                                          new OracleParameter("3", OracleDbType.Varchar2),  // [2] LOC_CODE
                                                          new OracleParameter("4", OracleDbType.Varchar2),  // [3] ORDERBY
                                                          new OracleParameter("5", OracleDbType.Varchar2),  // [4] SYSTEM_CODE
                                                          new OracleParameter("6", OracleDbType.Varchar2),  // [5] LOC_CODE
                                                          new OracleParameter("7", OracleDbType.Varchar2),  // [6] SYSTEM_CODE
                                                          new OracleParameter("8", OracleDbType.Varchar2),  // [7] LOC_CODE
                                                          new OracleParameter("9", OracleDbType.Varchar2)   // [8] ORDERBY
                                                      };
                        parameters[0].Value = item.SYSTEM_CODE;
                        parameters[1].Value = item.LOC_CODE;
                        parameters[2].Value = item.LOC_CODE;
                        parameters[3].Value = item.ORDERBY;
                        parameters[4].Value = item.SYSTEM_CODE;
                        parameters[5].Value = item.LOC_CODE;
                        parameters[6].Value = item.SYSTEM_CODE;
                        parameters[7].Value = item.LOC_CODE;
                        parameters[8].Value = item.ORDERBY;

                        mapper.ExecuteScript(Database.Queries.SYSTEM_MAPPING_INS_UPD, parameters);
                    }
                }
            }
            catch (Exception ex)
            {
                FireException("SaveMapping", ex);
                throw ex;
            }
        } 
        #endregion
        #endregion

        #region private void btnSave_Click(object sender, EventArgs e)
        private void btnSave_Click(object sender, EventArgs e)
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                if (lblNew.Visible || (dgvGyetong.Selected != null && dgvGyetong.Selected.Rows.Count > 0))
                {
                    if (txtSystemCode.Text == "" || txtSystemCode.Text.Length <= 0)
                    {
                        MessageBox.Show("System Code는 필수 입력입니다.", "필수 입력", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSystemCode.Focus();
                        return;
                    }
                    if (txtSystemName.Text == "" || txtSystemName.Text.Length <= 0)
                    {
                        MessageBox.Show("System Name은 필수 입력입니다.", "필수 입력", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSystemName.Focus();
                        return;
                    }
                    if (txtLocCode.Text == "" || txtLocCode.Text.Length <= 0)
                    {
                        MessageBox.Show("Loc Code는 필수 입력입니다.", "필수 입력", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtLocCode.Focus();
                        return;
                    }
                    int orderBy = _systemList.FindAll(item => item.Level == 1).Max(item => item.ORDERBY);
                    orderBy += 1;
                    if (txtOrderBy.Text == "" || txtOrderBy.Text.Length <= 0)
                    {
                        //_orderBy1 += 1;
                        txtOrderBy.Text = orderBy.ToString();
                    }
                    else
                    {
                        int orderByT = 0;
                        if (!int.TryParse(txtOrderBy.Text, out orderByT))
                        {
                            txtOrderBy.Text = orderBy.ToString();
                        }
                        else
                        {
                            txtOrderBy.Text = orderByT.ToString();
                        }
                    }
                }

                if (MessageBox.Show("저장하시겠습니까", "저장", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper.BeginTransaction();

                // System Master 처리
                SaveSystem(mapper);
                // System Mapping 처리
                SaveMapping(mapper);

                mapper.CommitTransaction();

                // 계통정보 목록 다시 읽기
                //_orderBy1 = 0;
                RemoveRowsAll(dgvGyetong);
                DataTable dtSystem = SelectSystemList();
                CreateSystemList(dtSystem, _systemList, true);
                dgvGyetong.DataSource = dtSystem;

                MessageBox.Show("저장되었습니다.", "처리완료", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                mapper.RollbackTransaction();
                FireException("btnSave_Click", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                lblNew.Visible = false;
                if (mapper != null) mapper.Close();
            }
        } 
        #endregion

        #region private void btnDel_Click(object sender, EventArgs e)
        private void btnDel_Click(object sender, EventArgs e)
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                if (dgvGyetong.Selected == null || dgvGyetong.Selected.Rows.Count <= 0)
                {
                    MessageBox.Show("선택된 중블록이 없습니다.", "삭제", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtLocCode.Focus();
                    return;
                }
                if (MessageBox.Show("삭제하시겠습니까", "삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper.BeginTransaction();

                for (int i = 0; i < dgvGyetong.Selected.Rows.Count; i++)
                {
                    string level = string.Format("{0}", dgvGyetong.Selected.Rows[i].Cells[Database.Fields.SYSTEM_MASTER.LEVEL].Value);
                    string systemCode = string.Format("{0}", dgvGyetong.Selected.Rows[i].Cells[Database.Fields.SYSTEM_MASTER.SYSTEM_CODE].Value);
                    string locCode = string.Format("{0}", dgvGyetong.Selected.Rows[i].Cells[Database.Fields.SYSTEM_MASTER.LOC_CODE].Value);

                    // 아이템 찾기
                    SystemItem systemItem = null;
                    if (level == "1") systemItem = _systemList.FindAll(item => item.Level == Convert.ToInt32(level)).Find(item => item.SYSTEM_CODE == systemCode);
                    else systemItem = _systemList.Find(item => item.LOC_CODE == locCode);

                    if (systemItem == null)
                    {
                        MessageBox.Show("데이터에 오류가 발생했습니다.", "삭제", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return;
                    }

                    // 상태가 New일 경우 DB에는 데이터가 없으므로 처리하지 않는다.
                    if (systemItem.State != SystemItem.StateType.New)
                    {
                        IDataParameter[] parameters = {
                                                      new OracleParameter("1", OracleDbType.Varchar2),  // [0] SYSTEM_CODE
                                                      new OracleParameter("2", OracleDbType.Varchar2)   // [1] LOC_CODE
                                                  };
                        parameters[0].Value = systemCode;
                        parameters[1].Value = locCode;

                        if (level == "1")
                        {
                            mapper.ExecuteScript(Database.Queries.SYSTEM_MASTER_DELETE, parameters);

                            // 소블록 삭제 (SYSTEM_MAPPING)
                            IDataParameter[] parameters1 = {
                                                           new OracleParameter("1", OracleDbType.Varchar2)
                                                       };
                            parameters1[0].Value = systemCode;
                            mapper.ExecuteScript(Database.Queries.SYSTEM_MAPPING_DELETE_SYSTEM, parameters1);
                        }
                        else
                        {
                            mapper.ExecuteScript(Database.Queries.SYSTEM_MAPPING_DELETE, parameters);
                        }
                    }
                }
                mapper.CommitTransaction();

                // 계통정보 목록 다시 읽기
                //_orderBy1 = 0;
                RemoveRowsAll(dgvGyetong);
                DataTable dtSystem = SelectSystemList();
                CreateSystemList(dtSystem, _systemList, true);
                dgvGyetong.DataSource = dtSystem;

                MessageBox.Show("삭제되었습니다.", "삭제완료", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                mapper.RollbackTransaction();
                FireException("btnDel_Click", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }
        }
        #endregion


        

        

        #region private void btnClose_Click(object sender, EventArgs e)
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        } 
        #endregion

        

        
    }
}
