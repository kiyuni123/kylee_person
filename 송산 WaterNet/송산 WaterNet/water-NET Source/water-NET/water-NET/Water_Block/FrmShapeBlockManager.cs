﻿//#define _DEBUG

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Infragistics.Win.UltraWinTree;
using Oracle.DataAccess.Client;


namespace WaterNet.BlockApp
{
    public partial class FrmShapeBlockManager : Form
    {
        /// <summary>
        /// Shape TreeNode의 Tag 분리
        /// </summary>
        #region private struct ShapeTag
        private struct ShapeTag
        {
            // // FTR_CODE | FTR_IDN | PFTR_CODE | PFTR_IDN
            public string FTR_CODE;
            public string PFTR_CODE;
            public string FTR_IDN;
            public string PFTR_IDN;
        } 
        #endregion

        /// <summary>
        /// Block TreeNode의 Tag 분리
        /// </summary>
        #region private struct BlockTag
        private struct BlockTag
        {
            // LOC_CODE | PLOC_CODE | FTR_CODE | PFTR_CODE | FTR_IDN | PFTR_IDN
            public string LOC_CODE;
            public string PLOC_CODE;
            public string FTR_CODE;
            public string PFTR_CODE;
            public string FTR_IDN;
            public string PFTR_IDN;
        } 
        #endregion

        /// <summary>
        /// Shape List
        /// </summary>
        private List<ShapeItem> _shapeList = null;
        /// <summary>
        /// Block List
        /// </summary>
        private List<BlockItem> _blockList = null;



        /// <summary>
        /// 최종 LOC_CODE
        /// </summary>
        private int _lastLocCode = 0;
        /// <summary>
        /// 최종 ORDERBY
        /// </summary>
        private int _lastOrderBy = 0;



        /// <summary>
        /// 생성자
        /// </summary>
        #region public FrmShapeBlockManager()
        public FrmShapeBlockManager()
        {
            InitializeComponent();
        } 
        #endregion






        #region private void FrmBlockManager_Load(object sender, EventArgs e)
        private void FrmBlockManager_Load(object sender, EventArgs e)
        {
            try
            {
                //InitializeShape();
                //InitializeList();

                OpenZone();
            }
            catch (Exception ex)
            {
                FireException("FrmBlockManager_Load", ex);
            }
        } 
        #endregion








        
        /// <summary>
        /// 오류 로그 (디버깅용)
        /// </summary>
        /// <param name="method">발생 메서드</param>
        /// <param name="ex">발생 오류</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion






        /// <summary>
        /// 지역 열기
        /// </summary>
        #region private void OpenZone()
        private void OpenZone()
        {
#if _DEBUG
            FrmOpen frmOpen = new FrmOpen();
            // 팝업에서 지역 선택시
            if (frmOpen.ShowDialog(this) == DialogResult.OK)
            {
                EMFrame.statics.AppStatic.DATA_DIR = frmOpen.PATH;
                EMFrame.statics.AppStatic.USER_ID = "에너지";

                EMFrame.statics.AppStatic.ZONE_GBN = frmOpen.ZONE;
                EMFrame.statics.AppStatic.USER_SGCCD = frmOpen.SGCCD;

                string datadir = EMFrame.statics.AppStatic.DATA_DIR + System.IO.Path.DirectorySeparatorChar + EMFrame.statics.AppStatic.USER_SGCCD;
                WaterNet.WaterAOCore.VariableManager.m_Topographic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Topographic");
                WaterNet.WaterAOCore.VariableManager.m_Pipegraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Pipegraphic");
                WaterNet.WaterAOCore.VariableManager.m_INPgraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic");
                WaterNet.WaterAOCore.VariableManager.m_INPgraphicRootDirectory = datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic";

                // Shape 정보 초기화
                InitializeShape();
                // 목록 생성 (ListView)
                InitializeList();
            }
#else
            FrmSelectSgccd frmSgccd = new FrmSelectSgccd();
            if (frmSgccd.ShowDialog(this) == DialogResult.OK)
            {
                EMFrame.statics.AppStatic.DATA_DIR = frmSgccd.PATH;
                EMFrame.statics.AppStatic.USER_ID = "에너지";

                EMFrame.statics.AppStatic.ZONE_GBN = frmSgccd.ZONE;
                EMFrame.statics.AppStatic.USER_SGCCD = frmSgccd.SGCCD;

                string datadir = EMFrame.statics.AppStatic.DATA_DIR + System.IO.Path.DirectorySeparatorChar + EMFrame.statics.AppStatic.USER_SGCCD;
                WaterNet.WaterAOCore.VariableManager.m_Topographic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Topographic");
                WaterNet.WaterAOCore.VariableManager.m_Pipegraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Pipegraphic");
                WaterNet.WaterAOCore.VariableManager.m_INPgraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic");
                WaterNet.WaterAOCore.VariableManager.m_INPgraphicRootDirectory = datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic";

                // Shape 정보 초기화
                InitializeShape();
                // 목록 생성 (ListView)
                InitializeList();
            }
#endif

            isTreeEvent = false;
        } 
        #endregion

        /// <summary>
        /// Shape Data 초기화
        /// </summary>
        #region private void InitializeShape()
        private void InitializeShape()
        {
            try
            {
                // FrmOpen.cs로 이동

                //EMFrame.statics.AppStatic.DATA_DIR = Application.StartupPath + @"\WaterNET-Data";
                //EMFrame.statics.AppStatic.USER_ID = "에너지";

                //EMFrame.statics.AppStatic.ZONE_GBN = "지방";
                //EMFrame.statics.AppStatic.USER_SGCCD = "47900";// 함평:"46860";

                //string datadir = EMFrame.statics.AppStatic.DATA_DIR + System.IO.Path.DirectorySeparatorChar + EMFrame.statics.AppStatic.USER_SGCCD;
                //WaterNet.WaterAOCore.VariableManager.m_Topographic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Topographic");
                //WaterNet.WaterAOCore.VariableManager.m_Pipegraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Pipegraphic");
                //WaterNet.WaterAOCore.VariableManager.m_INPgraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic");
                //WaterNet.WaterAOCore.VariableManager.m_INPgraphicRootDirectory = datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic";

                // Shape 정보 읽기
                ShapeDataTable.OpenTable();
            }
            catch (Exception ex)
            {
                FireException("InitializeShape", ex);
                throw ex;
            }
        } 
        #endregion

        /// <summary>
        /// TreeView 초기화 (Shape, Block)
        /// </summary>
        #region private void InitializeList()
        private void InitializeList()
        {
            try
            {
                // Shape 목록
                if (_shapeList == null) _shapeList = new List<ShapeItem>();
                _shapeList.Clear();
                _shapeList = ShapeDataTable.GetSortList(ShapeDataTable.DataTable);
                // Block 목록
                DataTable dtBlock = SelectBlockData();
                if (_blockList == null) _blockList = new List<BlockItem>();
                _blockList.Clear();
                _blockList = GetSortList(dtBlock);

                // 트리 초기화
                tvShape.Nodes.Clear();
                tvBlock.Nodes.Clear();

                // 'Shape vs Block 매칭', 'Block vs Shape 매칭' 두 구역은 데이터를 검사하며,
                // 동일한 작업을 두 번 수행하는 이유는 Shape와 Block의 개수가 반드시 같다는
                // 보장이 없기에 양쪽 모두 맞추기 위하여 두번을 수행한다.

                #region Shape vs Block 매칭
                // Shape와 Block간의 데이터 매칭을 검사한다. (검사하여 결과를 해당 Shape, Block 아이템에 마킹까지만 수행)

                // 대블록
                for (int i = 0; i < _shapeList.Count; i++)
                {
                    ShapeItem itemI = _shapeList[i];
                    // 현재 Shape의 블록 정보가 있는지 확인. 있다면 해당 블록 반환
                    BlockItem blockI = CheckBlock(itemI, _blockList);
                    BlockItem blockJ = null;
                    BlockItem blockK = null;
                    BlockItem blockM = null;

                    if (blockI == null)
                    {
                        // Shape의 블록 정보가 없다면 연결 끊김 아이템으로 생성

                        itemI.IsMatch = false;

                        #region 임시 블럭 생성 (가상 블록)
                        blockI = new BlockItem();
                        blockI.IsMatch = false;
                        blockI.IsTemp = true;
                        blockI.FTR_CODE = itemI.FTR_CODE;
                        blockI.FTR_IDN = itemI.FTR_IDN;
                        blockI.PFTR_CODE = itemI.PFTR_CODE;
                        blockI.PFTR_IDN = itemI.PFTR_IDN;
                        blockI.LOC_NAME = itemI.LOC_NAME;
                        blockI.REL_LOC_NAME = itemI.REL_LOC_NAME;
                        blockI.SGCCD = itemI.SGCCD;
                        blockI.LOC_GBN = itemI.BLK_GBN;
                        blockI.CM_LOC_GBN = "0";
                        blockI.Child = new List<BlockItem>();
                        _blockList.Add(blockI);
                        #endregion
                    }
                    else
                    {
                        itemI.IsMatch = true;
                        blockI.IsMatch = true;
                        blockI.IsTemp = false;
                    }

                    // 중블록
                    for (int j = 0; j < itemI.Child.Count; j++)
                    {
                        ShapeItem itemJ = itemI.Child[j];
                        // 현재 Shape의 블록 정보가 있는지 확인. 있다면 해당 블록 반환
                        blockJ = CheckBlock(itemJ, blockI.Child);

                        if (blockJ == null)
                        {
                            // Shape의 블록 정보가 없다면 연결 끊김 아이템으로 생성

                            itemJ.IsMatch = false;

                            #region 임시 블럭 생성 (가상 블록)
                            blockJ = new BlockItem();
                            blockJ.IsMatch = false;
                            blockJ.IsTemp = true;
                            blockJ.FTR_CODE = itemJ.FTR_CODE;
                            blockJ.FTR_IDN = itemJ.FTR_IDN;
                            blockJ.PFTR_CODE = itemJ.PFTR_CODE;
                            blockJ.PFTR_IDN = itemJ.PFTR_IDN;
                            blockJ.LOC_NAME = itemJ.LOC_NAME;
                            blockJ.REL_LOC_NAME = itemJ.REL_LOC_NAME;
                            blockJ.SGCCD = itemJ.SGCCD;
                            blockJ.LOC_GBN = itemJ.BLK_GBN;
                            blockJ.CM_LOC_GBN = "0";
                            blockJ.Child = new List<BlockItem>();
                            blockI.Child.Add(blockJ);
                            #endregion
                        }
                        else
                        {
                            itemJ.IsMatch = true;
                            blockJ.IsMatch = true;
                            blockJ.IsTemp = false;
                        }

                        // 소블록
                        for (int k = 0; k < itemJ.Child.Count; k++)
                        {
                            ShapeItem itemK = itemJ.Child[k];
                            // 현재 Shape의 블록 정보가 있는지 확인. 있다면 해당 블록 반환
                            blockK = CheckBlock(itemK, blockJ.Child);
                            if (blockK == null)
                            {
                                // Shape의 블록 정보가 없다면 연결 끊김 아이템으로 생성

                                itemK.IsMatch = false;

                                #region 임시 블럭 생성 (가상 블록)
                                blockK = new BlockItem();
                                blockK.IsMatch = false;
                                blockK.IsTemp = true;
                                blockK.FTR_CODE = itemK.FTR_CODE;
                                blockK.FTR_IDN = itemK.FTR_IDN;
                                blockK.PFTR_CODE = itemK.PFTR_CODE;
                                blockK.PFTR_IDN = itemK.PFTR_IDN;
                                blockK.LOC_NAME = itemK.LOC_NAME;
                                blockK.REL_LOC_NAME = itemK.REL_LOC_NAME;
                                blockK.SGCCD = itemK.SGCCD;
                                blockK.LOC_GBN = itemK.BLK_GBN;
                                blockK.CM_LOC_GBN = "0";
                                blockK.Child = new List<BlockItem>();
                                blockJ.Child.Add(blockK);
                                #endregion
                            }
                            else
                            {
                                itemK.IsMatch = true;
                                blockK.IsMatch = true;
                                blockK.IsTemp = false;
                            }

                            // 소소블록
                            for (int m = 0; m < itemK.Child.Count; m++)
                            {
                                ShapeItem itemM = itemK.Child[m];
                                // 현재 Shape의 블록 정보가 있는지 확인. 있다면 해당 블록 반환
                                blockM = CheckBlock(itemM, blockK.Child);
                                if (blockM == null)
                                {
                                    // Shape의 블록 정보가 없다면 연결 끊김 아이템으로 생성

                                    itemM.IsMatch = false;

                                    #region 임시 블럭 생성 (가상 블록)
                                    blockM = new BlockItem();
                                    blockM.IsMatch = false;
                                    blockM.IsTemp = true;
                                    blockM.FTR_CODE = itemM.FTR_CODE;
                                    blockM.FTR_IDN = itemM.FTR_IDN;
                                    blockM.PFTR_CODE = itemM.PFTR_CODE;
                                    blockM.PFTR_IDN = itemM.PFTR_IDN;
                                    blockM.LOC_NAME = itemM.LOC_NAME;
                                    blockM.REL_LOC_NAME = itemM.REL_LOC_NAME;
                                    blockM.SGCCD = itemM.SGCCD;
                                    blockM.LOC_GBN = itemM.BLK_GBN;
                                    blockM.CM_LOC_GBN = "0";
                                    blockM.Child = new List<BlockItem>();
                                    blockK.Child.Add(blockM);
                                    #endregion
                                }
                                else
                                {
                                    itemM.IsMatch = true;
                                    blockM.IsMatch = true;
                                    blockM.IsTemp = false;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Block vs Shape 매칭
                // Shape와 Block간의 데이터 매칭을 검사한다. (검사하여 결과를 해당 Shape, Block 아이템에 마킹까지만 수행)

                // 대블록
                for (int i = 0; i < _blockList.Count; i++)
                {
                    BlockItem itemI = _blockList[i];
                    // 현재 Block의 Shape 정보가 있는지 확인. 있다면 해당 Shape 반환
                    ShapeItem shapeI = CheckShape(itemI, _shapeList);
                    ShapeItem shapeJ = null;
                    ShapeItem shapeK = null;
                    ShapeItem shapeM = null;

                    if (shapeI == null)
                    {
                        // Block의 Shape 정보가 없다면 연결 끊김 아이템으로 생성

                        itemI.IsMatch = false;
                    }
                    else
                    {
                        itemI.IsMatch = true;
                    }
                    // 중블록
                    for (int j = 0; j < itemI.Child.Count; j++)
                    {
                        BlockItem itemJ = itemI.Child[j];
                        if (shapeI == null)
                        {
                            itemJ.IsMatch = false;
                        }
                        else
                        {
                            // 현재 Block의 Shape 정보가 있는지 확인. 있다면 해당 Shape 반환
                            shapeJ = CheckShape(itemJ, shapeI.Child);
                            if (shapeJ == null) itemJ.IsMatch = false;
                            else itemJ.IsMatch = true;
                        }
                        // 소블록
                        for (int k = 0; k < itemJ.Child.Count; k++)
                        {
                            BlockItem itemK = itemJ.Child[k];
                            if (shapeI == null || shapeJ == null)
                            {
                                itemK.IsMatch = false;
                            }
                            else
                            {
                                // 현재 Block의 Shape 정보가 있는지 확인. 있다면 해당 Shape 반환
                                shapeK = CheckShape(itemK, shapeJ.Child);
                                if (shapeK == null) itemK.IsMatch = false;
                                else itemK.IsMatch = true;
                            }
                            // 소소블록
                            for (int m = 0; m < itemK.Child.Count; m++)
                            {
                                BlockItem itemM = itemK.Child[m];
                                if (shapeI == null || shapeJ == null || shapeK == null)
                                {
                                    itemM.IsMatch = false;
                                }
                                else
                                {
                                    // 현재 Block의 Shape 정보가 있는지 확인. 있다면 해당 Shape 반환
                                    shapeM = CheckShape(itemM, shapeK.Child);
                                    if (shapeM == null) itemM.IsMatch = false;
                                    else itemM.IsMatch = true;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Create Shape Tree
                // Root Node 생성
                Infragistics.Win.UltraWinTree.UltraTreeNode rootShape = tvShape.Nodes.Add("S", "Shape");

                #region 대블록
                for (int i = 0; i < _shapeList.Count; i++)
                {
                    ShapeItem itemI = _shapeList[i];
                    // 블록이 아닐 경우 패스 (배수지, 정수장등은 표현하지 않는다.)
                    // 2014-11-14 다시 해제
                    //if (itemI.BLK_GBN.IndexOf("블록") < 0) continue;

                    // 대블록 노드 생성
                    Infragistics.Win.UltraWinTree.UltraTreeNode node1 = rootShape.Nodes.Add(string.Format("S{0}", i), itemI.LOC_NAME + "[" + itemI.FTR_IDN + "]");
                    // 노드의 태그에 FTR_CODE, 상위 FTR_CODE, FTR_IDN, 상위 FTR_IDN을 담아서 차후 상위 노드 찾기에서 사용한다.
                    node1.Tag = string.Format("{0}|{1}|{2}|{3}", itemI.FTR_CODE, itemI.PFTR_CODE, itemI.FTR_IDN, itemI.PFTR_IDN); ;
                    
                    if (itemI.IsMatch == false)
                    {
                        // 연결 끊긴 아이템 이미지 표현
                        node1.Override.NodeAppearance.Image = imageList1.Images[2];
                        node1.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                    }

                    #region 중블록
                    if (itemI.Child != null && itemI.Child.Count > 0)
                    {
                        for (int j = 0; j < itemI.Child.Count; j++)
                        {
                            ShapeItem itemJ = itemI.Child[j];
                            // 중블록 노드 생성
                            Infragistics.Win.UltraWinTree.UltraTreeNode node2 = node1.Nodes.Add(string.Format("S{0}_{1}", i, j), itemJ.LOC_NAME + "[" + itemJ.FTR_IDN + "]");
                            node2.Tag = string.Format("{0}|{1}|{2}|{3}", itemJ.FTR_CODE, itemJ.PFTR_CODE, itemJ.FTR_IDN, itemJ.PFTR_IDN); ;

                            if (itemJ.IsMatch == false)
                            {
                                node2.Override.NodeAppearance.Image = imageList1.Images[2];
                                node2.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                            }

                            #region 소블록
                            if (itemJ.Child != null && itemJ.Child.Count > 0)
                            {
                                for (int k = 0; k < itemJ.Child.Count; k++)
                                {
                                    ShapeItem itemK = itemJ.Child[k];
                                    // 소블록 노드 생성
                                    Infragistics.Win.UltraWinTree.UltraTreeNode node3 = node2.Nodes.Add(string.Format("S{0}_{1}_{2}", i, j, k), itemK.LOC_NAME + "[" + itemK.FTR_IDN + "]");
                                    node3.Tag = string.Format("{0}|{1}|{2}|{3}", itemK.FTR_CODE, itemK.PFTR_CODE, itemK.FTR_IDN, itemK.PFTR_IDN); ;

                                    if (itemK.IsMatch == false)
                                    {
                                        node3.Override.NodeAppearance.Image = imageList1.Images[2];
                                        node3.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                                    }

                                    #region 소소블록
                                    if (itemK.Child != null && itemK.Child.Count > 0)
                                    {
                                        for (int m = 0; m < itemK.Child.Count; m++)
                                        {
                                            ShapeItem itemM = itemK.Child[m];
                                            // 소소블록 노드 생성
                                            Infragistics.Win.UltraWinTree.UltraTreeNode node4 = node3.Nodes.Add(string.Format("S{0}_{1}_{2}_{3}", i, j, k, m), itemM.LOC_NAME + "[" + itemM.FTR_IDN + "]");
                                            node4.Tag = string.Format("{0}|{1}|{2}|{3}", itemM.FTR_CODE, itemM.PFTR_CODE, itemM.FTR_IDN, itemM.PFTR_IDN); ;

                                            if (itemM.IsMatch == false)
                                            {
                                                node4.Override.NodeAppearance.Image = imageList1.Images[2];
                                                node4.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion

                // 모든 노드 확장
                tvShape.ExpandAll();
                #endregion

                #region Create Block Tree
                // Root 노드 생성
                Infragistics.Win.UltraWinTree.UltraTreeNode rootBlock = tvBlock.Nodes.Add("B", "Block");

                #region 대블록
                for (int i = 0; i < _blockList.Count; i++)
                {
                    BlockItem itemI = _blockList[i];
                    // 블록이 아닐 경우 패스 (배수지, 정수장등의 경우 표현 안함)
                    // 2014-11-14 다시 해제
                    //if (itemI.LOC_GBN.IndexOf("블록") < 0) continue;
                    
                    // 대블록 노드 생성
                    Infragistics.Win.UltraWinTree.UltraTreeNode node1 = rootBlock.Nodes.Add(string.Format("B{0}", i), itemI.LOC_NAME + "[" + itemI.FTR_IDN + "]");
                    // 노드 선택시 상위 노드 정보를 찾기 위한 정보를 TAG로 설정
                    node1.Tag = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", itemI.LOC_CODE, itemI.PLOC_CODE, itemI.FTR_CODE, itemI.PFTR_CODE, itemI.FTR_IDN, itemI.PFTR_IDN);

                    if (itemI.IsMatch == false)
                    {
                        // 연결 끊긴 아이템 이미지 표시
                        node1.Override.NodeAppearance.Image = imageList1.Images[2];
                        node1.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                    }
                    if (itemI.IsTemp)
                    {
                        // 가상으로 생성된 아이템 이미지 표시 (Shape정보는 있지만 Block에 없을 경우)
                        node1.Override.NodeAppearance.Image = imageList1.Images[3];
                        node1.Override.SelectedNodeAppearance.Image = imageList1.Images[4];
                    }

                    #region 중블록
                    if (itemI.Child != null && itemI.Child.Count > 0)
                    {
                        for (int j = 0; j < itemI.Child.Count; j++)
                        {
                            BlockItem itemJ = itemI.Child[j];
                            // 중블록 노드 생성
                            Infragistics.Win.UltraWinTree.UltraTreeNode node2 = node1.Nodes.Add(string.Format("B{0}_{1}", i, j), itemJ.LOC_NAME + "[" + itemJ.FTR_IDN + "]");
                            node2.Tag = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", itemJ.LOC_CODE, itemJ.PLOC_CODE, itemJ.FTR_CODE, itemJ.PFTR_CODE, itemJ.FTR_IDN, itemJ.PFTR_IDN);

                            if (itemJ.IsMatch == false)
                            {
                                // 연결 끊긴 아이템 이미지 표시
                                node2.Override.NodeAppearance.Image = imageList1.Images[2];
                                node2.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                            }
                            if (itemJ.IsTemp)
                            {
                                // 가상으로 생성된 아이템 이미지 표시 (Shape정보는 있지만 Block에 없을 경우)
                                node2.Override.NodeAppearance.Image = imageList1.Images[3];
                                node2.Override.SelectedNodeAppearance.Image = imageList1.Images[4];
                            }

                            #region 소블록
                            if (itemJ.Child != null && itemJ.Child.Count > 0)
                            {
                                for (int k = 0; k < itemJ.Child.Count; k++)
                                {
                                    BlockItem itemK = itemJ.Child[k];
                                    // 소블록 노드 생성
                                    Infragistics.Win.UltraWinTree.UltraTreeNode node3 = node2.Nodes.Add(string.Format("B{0}_{1}_{2}", i, j, k), itemK.LOC_NAME + "[" + itemK.FTR_IDN + "]");
                                    node3.Tag = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", itemK.LOC_CODE, itemK.PLOC_CODE, itemK.FTR_CODE, itemK.PFTR_CODE, itemK.FTR_IDN, itemK.PFTR_IDN);

                                    if (itemK.IsMatch == false)
                                    {
                                        // 연결 끊긴 아이템 이미지 표시
                                        node3.Override.NodeAppearance.Image = imageList1.Images[2];
                                        node3.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                                    }
                                    if (itemK.IsTemp)
                                    {
                                        // 가상으로 생성된 아이템 이미지 표시 (Shape정보는 있지만 Block에 없을 경우)
                                        node3.Override.NodeAppearance.Image = imageList1.Images[3];
                                        node3.Override.SelectedNodeAppearance.Image = imageList1.Images[4];
                                    }

                                    #region 소소블록
                                    if (itemK.Child != null && itemK.Child.Count > 0)
                                    {
                                        for (int m = 0; m < itemK.Child.Count; m++)
                                        {
                                            BlockItem itemM = itemK.Child[m];
                                            // 소소블록 노드 생성
                                            Infragistics.Win.UltraWinTree.UltraTreeNode node4 = node3.Nodes.Add(string.Format("B{0}_{1}_{2}_{3}", i, j, k, m), itemM.LOC_NAME + "[" + itemM.FTR_IDN + "]");
                                            node4.Tag = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", itemM.LOC_CODE, itemM.PLOC_CODE, itemM.FTR_CODE, itemM.PFTR_CODE, itemM.FTR_IDN, itemM.PFTR_IDN);

                                            if (itemM.IsMatch == false)
                                            {
                                                // 연결 끊긴 아이템 이미지 표시
                                                node3.Override.NodeAppearance.Image = imageList1.Images[2];
                                                node3.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                                            }
                                            if (itemM.IsTemp)
                                            {
                                                // 가상으로 생성된 아이템 이미지 표시 (Shape정보는 있지만 Block에 없을 경우)
                                                node3.Override.NodeAppearance.Image = imageList1.Images[2];
                                                node3.Override.SelectedNodeAppearance.Image = imageList1.Images[5];
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion

                // 모든 노드 확장
                tvBlock.ExpandAll();
                #endregion
            }
            catch (Exception ex)
            {
                FireException("InitializeList", ex);
                throw ex;
            }
        } 
        #endregion

        private int SetKtGbnCombo(string ktGbn)
        {
            int retVal = 0;

            if (ktGbn == "001") retVal = 1;
            else if (ktGbn == "002") retVal = 2;
            else if (ktGbn == "003") retVal = 3;
            return retVal;
        }

        private string GetKtGbnCombo()
        {
            string retVal = "";

            if (cbBlockKtGbn.SelectedIndex == 1) retVal = "001";
            else if (cbBlockKtGbn.SelectedIndex == 2) retVal = "002";
            else if (cbBlockKtGbn.SelectedIndex == 3) retVal = "003";

            return retVal;
        }







        #region TreeView를 구성하기 위한 Helper Methods
        /// <summary>
        /// DataBase에서 블록 데이터 조회
        /// </summary>
        /// <returns>Block DataTable</returns>
        #region public static DataTable SelectBlockData()
        public static DataTable SelectBlockData()
        {
            DataTable dt = new DataTable();

            try
            {
                EMFrame.dm.EMapper mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);

                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.BLOCKMAPPING_BLOCK_SELECT, EMFrame.statics.AppStatic.USER_SGCCD), null);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Exception] SelectBlockData -> " + ex.Message);
            }
            return dt;
        } 
        #endregion

        /// <summary>
        /// 블록 DataTable을 List로 변환
        /// (DB에서 조회한 데이터를 내부 관리용 정보로 변환하여 List로 관리한다)
        /// </summary>
        /// <param name="dt">블록 DataTable</param>
        /// <returns>Block List</returns>
        #region public List<BlockItem> GetSortList(DataTable dt)
        public List<BlockItem> GetSortList(DataTable dt)
        {
            List<BlockItem> retVal = new List<BlockItem>();
            
            try
            {
                if (dt == null || dt.Rows.Count <= 0) return retVal;
                
                #region Database 데이터를 내부 데이터로 변환
                List<BlockItem> itemList = new List<BlockItem>();
                foreach (DataRow dr in dt.Rows)
                {
                    // 블록 아이템 생성
                    BlockItem newItem    = new BlockItem();
                    newItem.LEVEL        = toInt32(string.Format("{0}", dr[Database.Fields.BLOCK.LEVEL  ]));// 계층 단계
                    newItem.FTR_CODE     = string.Format("{0}", dr[Database.Fields.BLOCK.FTR_CODE    ]);
                    newItem.FTR_IDN      = string.Format("{0}", dr[Database.Fields.BLOCK.FTR_IDN     ]);
                    newItem.PFTR_CODE    = string.Format("{0}", dr[Database.Fields.BLOCK.PFTR_CODE   ]);
                    newItem.PFTR_IDN     = string.Format("{0}", dr[Database.Fields.BLOCK.PFTR_IDN    ]);
                    newItem.LOC_CODE     = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_CODE    ]);
                    newItem.PLOC_CODE    = string.Format("{0}", dr[Database.Fields.BLOCK.PLOC_CODE   ]);
                    newItem.LOC_NAME     = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_NAME    ]);
                    newItem.REL_LOC_NAME = string.Format("{0}", dr[Database.Fields.BLOCK.REL_LOC_NAME]);
                    newItem.RES_CODE     = string.Format("{0}", dr[Database.Fields.BLOCK.RES_CODE    ]);
                    newItem.KT_GBN       = string.Format("{0}", dr[Database.Fields.BLOCK.KT_GBN      ]);
                    newItem.ORDERBY      = toInt32(string.Format("{0}", dr[Database.Fields.BLOCK.ORDERBY]));
                    newItem.SGCCD        = string.Format("{0}", dr[Database.Fields.BLOCK.SGCCD       ]);
                    // 블록 구분 (대블록/중블록/소블록/...)
                    newItem.LOC_GBN = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_GBN]);
                    // Table 구분 (1:CM_LOCATION+CM_LOCATION2 / 2:CM_LOCATION2 ONLY) (TB_GBN -> CM_LOC_GBN 변경)
                    newItem.CM_LOC_GBN   = string.Format("{0}", dr[Database.Fields.BLOCK.CM_LOC_GBN  ]);
                    itemList.Add(newItem);
                    
                    // 신규 데이터 혹은 매핑 처리시 자동 LOC_CODE 처리시 사용하도록 최종 LOC_CODE를 기억함.
                    int locCode = 0;
                    if (int.TryParse(newItem.LOC_CODE, out locCode))
                    {
                        if (locCode > _lastLocCode) _lastLocCode = locCode;
                    }
                    if (newItem.ORDERBY > _lastOrderBy) _lastOrderBy = newItem.ORDERBY;
                }
                #endregion


                // 아래 대블록, 정수장, 가압장등의 코드는 수평된 데이터를 계층을 구성하도록 데이터를 조절한다.

                #region 대블록 (BLBG)
                // 블록 리스트중 블록 구분이 '대블록'인 아이템을 처리
                var blbgObjs = from BlockItem item in itemList
                               where item.LOC_GBN == ShapeDataTable.BLOCK_NAME.BLBG
                               orderby item.ORDERBY ascending
                               select item;
                if (blbgObjs != null && blbgObjs.Count() > 0)
                {
                    for (int i = 0; i < blbgObjs.Count(); i++)
                    {
                        BlockItem itemBlbg = blbgObjs.ElementAt(i);
                        itemBlbg.Child = new List<BlockItem>();
                        retVal.Add(itemBlbg);

                        #region 중블록 (BLBM)
                        // 블록 리스트중 블록 구분이 '중블록'인 아이템을 처리
                        var blbmObjs = from BlockItem item in itemList
                                       where item.PLOC_CODE == itemBlbg.LOC_CODE
                                       orderby item.ORDERBY ascending
                                       select item;
                        if (blbmObjs != null && blbmObjs.Count() > 0)
                        {
                            for (int j = 0; j < blbmObjs.Count(); j++)
                            {
                                BlockItem itemBlbm = blbmObjs.ElementAt(j);
                                itemBlbm.Child = new List<BlockItem>();
                                // 중블록 아이템을 대블록 아이템의 자식으로 등록한다.
                                itemBlbg.Child.Add(itemBlbm);

                                #region 소블록 (BLSM)
                                var blsmObjs = from BlockItem item in itemList
                                               where item.PLOC_CODE == itemBlbm.LOC_CODE
                                               orderby item.ORDERBY ascending
                                               select item;
                                if (blsmObjs != null && blsmObjs.Count() > 0)
                                {
                                    for (int k = 0; k < blsmObjs.Count(); k++)
                                    {
                                        BlockItem itemBlsm = blsmObjs.ElementAt(k);
                                        itemBlsm.Child = new List<BlockItem>();
                                        // 소블록 아이템을 중블록 아이템의 자식으로 등록한다.
                                        itemBlbm.Child.Add(itemBlsm);

                                        #region 소소블록 (BLSS)
                                        var blssObjs = from BlockItem item in itemList
                                                       where item.PLOC_CODE == itemBlsm.LOC_CODE
                                                       orderby item.ORDERBY ascending
                                                       select item;
                                        if (blssObjs != null && blssObjs.Count() > 0)
                                        {
                                            for (int m = 0; m < blssObjs.Count(); m++)
                                            {
                                                BlockItem itemBlss = blssObjs.ElementAt(m);
                                                itemBlss.Child = new List<BlockItem>();
                                                // 소소블록 아이템을 소블록 아이템의 자식으로 등록한다.
                                                itemBlsm.Child.Add(itemBlss);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                #region 정수장
                var puriObjs = from BlockItem item in itemList
                               where item.LOC_GBN == ShapeDataTable.BLOCK_NAME.PURI
                               orderby item.LOC_CODE ascending
                               select item;
                if (puriObjs != null && puriObjs.Count() > 0)
                {
                    for (int i = 0; i < puriObjs.Count(); i++)
                    {
                        BlockItem item = puriObjs.ElementAt(i);
                        item.Child = new List<BlockItem>();
                        retVal.Add(item);
                    }
                }
                #endregion

                #region 가압장
                var presObjs = from BlockItem item in itemList
                               where item.LOC_GBN == ShapeDataTable.BLOCK_NAME.PRES
                               orderby item.LOC_CODE ascending
                               select item;
                if (presObjs != null && presObjs.Count() > 0)
                {
                    for (int i = 0; i < presObjs.Count(); i++)
                    {
                        BlockItem item = presObjs.ElementAt(i);
                        item.Child = new List<BlockItem>();
                        retVal.Add(item);
                    }
                }
                #endregion

                #region 취수원
                var headObjs = from BlockItem item in itemList
                               where item.LOC_GBN == ShapeDataTable.BLOCK_NAME.HEAD
                               orderby item.LOC_CODE ascending
                               select item;
                if (headObjs != null && headObjs.Count() > 0)
                {
                    for (int i = 0; i < headObjs.Count(); i++)
                    {
                        BlockItem item = headObjs.ElementAt(i);
                        item.Child = new List<BlockItem>();
                        retVal.Add(item);
                    }
                }
                #endregion

                #region 배수지
                var servObjs = from BlockItem item in itemList
                               where item.LOC_GBN == ShapeDataTable.BLOCK_NAME.SERV
                               orderby item.LOC_CODE ascending
                               select item;
                if (servObjs != null && servObjs.Count() > 0)
                {
                    for (int i = 0; i < servObjs.Count(); i++)
                    {
                        BlockItem item = servObjs.ElementAt(i);
                        item.Child = new List<BlockItem>();
                        retVal.Add(item);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("GetSortList", ex);
                throw ex;
            }
            return retVal;
        } 
        #endregion

        /// <summary>
        /// String to int32
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        #region public static int toInt32(string val)
        public static int toInt32(string val)
        {
            int retVal = 0;

            if (!int.TryParse(val, out retVal)) retVal = 0;

            return retVal;
        } 
        #endregion

        /// <summary>
        /// Shape정보가 Block정보에 있는지 확인
        /// </summary>
        /// <param name="shape">Shape 정보</param>
        /// <param name="list">Block 리스트</param>
        /// <returns>Shape정보가 Block정보에 있을 경우 BlockItem</returns>
        #region private bool CheckBlock(ShapeItem shape, List<BlockItem> list)
        private BlockItem CheckBlock(ShapeItem shape, List<BlockItem> list)
        {
            BlockItem retVal = null;

            try
            {
                var objects = from BlockItem item in list
                              where item.FTR_IDN == shape.FTR_IDN
                              select item;

                // 조회 데이터가 null이거나 개수가 0 이하일 경우 null 반환
                if (objects == null && objects.Count() <= 0) return null;
                // 조회 데이터가 존재할 경우 가장 첫번째 데이터를 반환
                retVal = (objects.Count() > 0 ? objects.First() : null);
            }
            catch (Exception ex)
            {
                FireException("CheckBlock('" + shape.LOC_NAME + "')", ex);
                throw ex;
            }
            return retVal;
        } 
        #endregion

        /// <summary>
        /// Block정보가 Shape정보에 있는지 확인
        /// </summary>
        /// <param name="block">Block 정보</param>
        /// <param name="list">Shape 리스트</param>
        /// <returns>Block정보가 Shape정보에 있을 경우 ShapeItem</returns>
        #region private bool CheckShape(BlockItem block, List<ShapeItem> list)
        private ShapeItem CheckShape(BlockItem block, List<ShapeItem> list)
        {
            var objects = from ShapeItem item in list
                          where item.FTR_IDN == block.FTR_IDN
                          select item;

            // 조회 데이터가 null이거나 개수가 0 이하일 경우 null 반환
            if (objects == null && objects.Count() <= 0) return null;
            // 조회 데이터가 존재할 경우 가장 첫번째 데이터를 반환
            return (objects.Count() > 0 ? objects.First() : null);
        } 
        #endregion

        
        #endregion









        #region TreeView Select Event
        private bool isTreeEvent = false;

        /// <summary>
        /// Shape Tree 선택 이벤트
        /// </summary>
        #region private void tvShape_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        private void tvShape_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        {
            try
            {
                // 화면에서 '신규' 표시 제거
                lblNew.Visible = false;

                // Tree 관련 처리중 발생한 이벤트라면 패스
                if (isTreeEvent) return;
                // Tree 관련 처리중임을 설정
                isTreeEvent = true;

                // 화면 초기화
                ClearShapeForm();
                ClearBlockForm();

                // Shape 트리 선택이 없다면 패스
                if (tvShape.SelectedNodes == null || tvShape.SelectedNodes.Count <= 0) return;
                // 선택된 노드의 Shape 아이템 찾기
                ShapeItem shape = FindTreeShape(tvShape.SelectedNodes[0], false);
                
                // 선택된 Shape정보를 Form에 출력
                SetTreeShapeInfo(shape);

                // 선택된 Shape가 Root일 경우
                if (shape.BLK_GBN == "Shape") tvBlock.Nodes[0].Selected = true;
                if (tvBlock.SelectedNodes == null || tvBlock.SelectedNodes.Count <= 0)
                {
                    // 블록 선택이 없다면 종료
                    isTreeEvent = false;
                    return;
                }
                // 선택된 블록 아이템 찾기 (사용자가 선택한 Shape 정보에 해당하는 블록 아이템 찾기)
                BlockItem block = FindTreeBlock(tvBlock.SelectedNodes[0], true);                
                // 선택된 블록 정보를 Form에 출력
                SetTreeBlockInfo(block);

                // 테이블 선택 (범용, 보고서용)
                if (block.IsTemp)
                {
                    chkbxTB1.Enabled = true;
                    chkbxTB2.Enabled = true;
                }
                else
                {
                    chkbxTB1.Enabled = false;
                    chkbxTB2.Enabled = false;
                }
                // 이벤트 발생 해제
                isTreeEvent = false;
            }
            catch (Exception ex)
            {
                isTreeEvent = false;
                FireException("tvShape_AfterSelect", ex);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// Shape 폼 채우기
        /// </summary>
        /// <param name="shape">ShapeItem</param>
        #region private void SetTreeShapeInfo(ShapeItem shape)
        private void SetTreeShapeInfo(ShapeItem shape)
        {
            if (shape.BLK_GBN == "Shape") return;

            txtShapeFtrCode.Text    = shape.FTR_CODE;
            txtShapeFtrIdn.Text     = shape.FTR_IDN;
            txtShapePFtrCode.Text   = shape.PFTR_CODE;
            txtShapePFtrIdn.Text    = shape.PFTR_IDN;
            txtShapeLocName.Text    = shape.LOC_NAME;
            txtShapeRelLocName.Text = shape.REL_LOC_NAME;
            txtShapeSgccd.Text      = shape.SGCCD;
        }
        #endregion

        /// <summary>
        /// 선택된 ShapeNode의 ShapeItem 찾기
        /// </summary>
        /// <param name="shapeNode">UltraTreeNode</param>
        /// <param name="isNotSelect">BlockTree선택</param>
        /// <returns>ShapeItem</returns>
        #region private ShapeItem FindTreeShape(Infragistics.Win.UltraWinTree.UltraTreeNode shapeNode, bool isNotSelect)
        private ShapeItem FindTreeShape(Infragistics.Win.UltraWinTree.UltraTreeNode shapeNode, bool isNotSelect)
        {
            ShapeItem retVal = null;

            try
            {
                ShapeTag shapeTag0 = new ShapeTag();
                ShapeTag shapeTag1 = new ShapeTag();
                ShapeTag shapeTag2 = new ShapeTag();
                ShapeTag shapeTag3 = new ShapeTag();

                // Node.Tag : Shape\함평대블럭\함평중블럭\함평02블럭
                string[] paths = shapeNode.FullPath.Split('\\');
                int depth = paths.Length - 1;

                if (depth <= 0)
                {
                    // 태그가 0단계일 경우 Root
                    // 비어있는 Shape 아이템 반환
                    retVal = new ShapeItem();
                    retVal.SGCCD = _shapeList[0].SGCCD;
                    retVal.BLK_GBN = "Shape";
                    return retVal;
                }

                if (depth >= 4)
                {
                    // 태그가 4단계일 경우 소소블록

                    // 대,중,소,소소블록 Shape 태그 정보 생성
                    shapeTag0 = SplitShapeTag(string.Format("{0}", shapeNode.Parent.Parent.Parent.Tag));
                    shapeTag1 = SplitShapeTag(string.Format("{0}", shapeNode.Parent.Parent.Tag));
                    shapeTag2 = SplitShapeTag(string.Format("{0}", shapeNode.Parent.Tag));
                    shapeTag3 = SplitShapeTag(string.Format("{0}", shapeNode.Tag));

                    #region ShapeItem 찾기
                    // 대블록 찾기
                    var objects = from ShapeItem item in _shapeList
                                  where item.FTR_IDN == shapeTag0.FTR_IDN
                                  select item;
                    if (objects != null && objects.Count() > 0)
                    {
                        ShapeItem shape0 = objects.First();
                        // 중블록 찾기
                        var objects1 = from ShapeItem item in shape0.Child
                                       where item.FTR_IDN == shapeTag1.FTR_IDN
                                       select item;
                        if (objects1 != null && objects1.Count() > 0)
                        {
                            ShapeItem shape1 = objects1.First();
                            // 소블록 찾기
                            var objects2 = from ShapeItem item in shape1.Child
                                           where item.FTR_IDN == shapeTag2.FTR_IDN
                                           select item;
                            if (objects2 != null && objects2.Count() > 0)
                            {
                                ShapeItem shape2 = objects2.First();
                                // 소소블록 찾기
                                var objects3 = from ShapeItem item in shape2.Child
                                               where item.FTR_IDN == shapeTag3.FTR_IDN
                                               select item;
                                if (objects3 != null && objects3.Count() > 0)
                                {
                                    // 찾아낸 Shape 아이템 (소소블록)
                                    retVal = objects3.First();
                                }
                            }
                        }
                    }
                    #endregion
                }
                else if (depth >= 3)
                {
                    // 태그가 3단계일 경우 소블록

                    // 대,중,소블록 Shape 태그 정보 생성
                    shapeTag0 = SplitShapeTag(string.Format("{0}", shapeNode.Parent.Parent.Tag));
                    shapeTag1 = SplitShapeTag(string.Format("{0}", shapeNode.Parent.Tag));
                    shapeTag2 = SplitShapeTag(string.Format("{0}", shapeNode.Tag));

                    #region ShapeItem 찾기
                    // 대블록 찾기
                    var objects = from ShapeItem item in _shapeList
                                  where item.FTR_IDN == shapeTag0.FTR_IDN
                                  select item;
                    if (objects != null && objects.Count() > 0)
                    {
                        ShapeItem shape0 = objects.First();
                        // 중블록 찾기
                        var objects1 = from ShapeItem item in shape0.Child
                                       where item.FTR_IDN == shapeTag1.FTR_IDN
                                       select item;
                        if (objects1 != null && objects1.Count() > 0)
                        {
                            ShapeItem shape1 = objects1.First();
                            // 소블록 찾기
                            var objects2 = from ShapeItem item in shape1.Child
                                           where item.FTR_IDN == shapeTag2.FTR_IDN
                                           select item;
                            if (objects2 != null && objects2.Count() > 0)
                            {
                                // 찾아낸 Shape 아이템 (소블록)
                                retVal = objects2.First();
                            }
                        }
                    }
                    #endregion
                }
                else if (depth >= 2)
                {
                    // 태그가 2단계일 경우 중블록

                    // 대,중블록 shape 태그 정보 생성
                    shapeTag0 = SplitShapeTag(string.Format("{0}", shapeNode.Parent.Tag));
                    shapeTag1 = SplitShapeTag(string.Format("{0}", shapeNode.Tag));

                    #region ShapeItem 찾기
                    // 대블록 찾기
                    var objects = from ShapeItem item in _shapeList
                                  where item.FTR_IDN == shapeTag0.FTR_IDN
                                  select item;
                    if (objects != null && objects.Count() > 0)
                    {
                        ShapeItem shape0 = objects.First();
                        // 중블록 찾기
                        var objects1 = from ShapeItem item in shape0.Child
                                       where item.FTR_IDN == shapeTag1.FTR_IDN
                                       select item;
                        if (objects1 != null && objects1.Count() > 0)
                        {
                            // 찾아낸 Shape 아이템 (중블록)
                            retVal = objects1.First();
                        }
                    }
                    #endregion
                }
                else if (depth >= 1)
                {
                    // 태그가 1단계일 경우 대블록

                    // 대블록 Shape 태그 정보 생성
                    shapeTag0 = SplitShapeTag(string.Format("{0}", shapeNode.Tag));

                    #region ShapeItem 찾기
                    // 대블록 찾기
                    var objects = from ShapeItem item in _shapeList
                                  where item.FTR_IDN == shapeTag0.FTR_IDN
                                  select item;
                    if (objects != null && objects.Count() > 0)
                    {
                        // 찾아낸 Shape 아이템 (대블록)
                        retVal = objects.First();
                    }
                    #endregion
                }

                // 미선택일 경우 Tree에서 태그를 선택하지 않고 종료
                if (isNotSelect) return retVal;

                bool isFind = false;
                // 태그의 Root 노드
                Infragistics.Win.UltraWinTree.UltraTreeNode root = tvBlock.Nodes[0];
                
                // 대블록
                for (int i = 0; i < root.Nodes.Count; i++)
                {
                    // 대블록 아이템 찾기
                    BlockTag blockTag = SplitBlockTag(string.Format("{0}", root.Nodes[i].Tag));
                    if (blockTag.FTR_IDN == shapeTag0.FTR_IDN)
                    {
                        if (shapeTag1.FTR_IDN == null || shapeTag1.FTR_IDN == "")
                        {
                            // 중블록 Shape 태그가 없다면 대블록 Shape 선택이므로 더이상 찾지 않고 종료
                            root.Nodes[i].Selected = true;
                            tvBlock.ActiveNode = root.Nodes[i];
                            isFind = true;
                            break;
                        }

                        // 중블록 Shape 태그가 있을 경우
                        for (int j = 0; j < root.Nodes[i].Nodes.Count; j++)
                        {
                            // 중블록 아이템 찾기
                            blockTag = SplitBlockTag(string.Format("{0}", root.Nodes[i].Nodes[j].Tag));
                            if (blockTag.FTR_IDN == shapeTag1.FTR_IDN)
                            {
                                if (shapeTag2.FTR_IDN == null || shapeTag2.FTR_IDN == "")
                                {
                                    // 소블록 Shape 태그가 없다면 중블록 Shape 선택이므로 더이상 찾지 않고 종료
                                    root.Nodes[i].Nodes[j].Selected = true;
                                    tvBlock.ActiveNode = root.Nodes[i].Nodes[j];
                                    isFind = true;
                                    break;
                                }
                                
                                // 소블록 Shape 태그가 있을 경우
                                for (int k = 0; k < root.Nodes[i].Nodes[j].Nodes.Count; k++)
                                {
                                    // 소블록 아이템 찾기
                                    blockTag = SplitBlockTag(string.Format("{0}", root.Nodes[i].Nodes[j].Nodes[k].Tag));
                                    if (blockTag.FTR_IDN == shapeTag2.FTR_IDN)
                                    {
                                        //if (shapeTag3.FTR_IDN == null || shapeTag3.FTR_IDN == "")
                                        //{
                                        //    // 소소블록
                                        //    for (int m = 0; m < root.Nodes[i].Nodes[j].Nodes[k].Nodes.Count; m++)
                                        //    {
                                        //        blockTag = SplitBlockTag(string.Format("{0}", root.Nodes[i].Nodes[j].Nodes[k].Nodes[m].Tag));
                                        //        if (blockTag.FTR_IDN == shapeTag3.FTR_IDN)
                                        //        {
                                        //            root.Nodes[i].Nodes[j].Nodes[k].Nodes[m].Selected = true;
                                        //            tvBlock.ActiveNode = root.Nodes[i].Nodes[j].Nodes[k].Nodes[m];
                                        //            isFind = true;
                                        //            break;
                                        //        }
                                        //    }
                                        //}

                                        root.Nodes[i].Nodes[j].Nodes[k].Selected = true;
                                        tvBlock.ActiveNode = root.Nodes[i].Nodes[j].Nodes[k];
                                        isFind = true;
                                        break;
                                    }

                                    // 소소블록 (차후 소소블록 사용시 아래 주석, 위 주석해제)
                                    for (int m = 0; m < root.Nodes[i].Nodes[j].Nodes[k].Nodes.Count; m++)
                                    {
                                        blockTag = SplitBlockTag(string.Format("{0}", root.Nodes[i].Nodes[j].Nodes[k].Nodes[m].Tag));
                                        if (blockTag.FTR_IDN == shapeTag3.FTR_IDN)
                                        {
                                            root.Nodes[i].Nodes[j].Nodes[k].Nodes[m].Selected = true;
                                            tvBlock.ActiveNode = root.Nodes[i].Nodes[j].Nodes[k].Nodes[m];
                                            isFind = true;
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                // 만약 블록 정보를 찾지 못 했다면 블록 트리에서 선택을 초기화한다.
                if (!isFind) tvBlock.SelectedNodes.Clear();
            }
            catch (Exception ex)
            {
                FireException("FindTreeShape", ex);
                throw ex;
            }
            return retVal;
        } 
        #endregion

        /// <summary>
        /// Shape TreeNode의 Tag를 ShapeTag 정보로 분리
        /// </summary>
        /// <param name="tag">TreeNode.Tag</param>
        /// <returns>ShapeTag</returns>
        #region private ShapeTag SplitShapeTag(string tag)
        private ShapeTag SplitShapeTag(string tag)
        {
            // FTR_CODE | PFTR_CODE | FTR_IDN | PFTR_IDN
            ShapeTag retVal = new ShapeTag();
            string[] split = tag.Split('|');
            if (split != null && split.Length == 4)
            {
                retVal.FTR_CODE  = split[0];
                retVal.PFTR_CODE = split[1];
                retVal.FTR_IDN   = split[2];
                retVal.PFTR_IDN  = split[3];
            }
            return retVal;
        } 
        #endregion



        /// <summary>
        /// Block Tree 선택 이벤트
        /// </summary>
        #region private void tvBlock_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        private void tvBlock_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        {
            try
            {
                // 화면에서 '신규' 표시 제거
                lblNew.Visible = false;

                // Tree 관련 처리중 발생한 이벤트라면 패스
                if (isTreeEvent) return;
                // Tree 관련 처리중임을 설정
                isTreeEvent = true;

                // 화면 초기화
                ClearBlockForm();
                ClearShapeForm();

                // 블록 트리 선택이 없다면 패스
                if (tvBlock.SelectedNodes == null || tvBlock.SelectedNodes.Count <= 0) return;

                // 선택 블록 노드의 정보 찾기
                BlockItem block = FindTreeBlock(tvBlock.SelectedNodes[0], false);
                // 블록 정보 화면 출력
                SetTreeBlockInfo(block);

                // Root 노드일 경우
                if (block.LOC_GBN == "Block") tvShape.Nodes[0].Selected = true;
                if (tvShape.SelectedNodes == null || tvShape.SelectedNodes.Count <= 0)
                {
                    // Shape 노드 선택이 없을 경우 종료
                    isTreeEvent = false;
                    return;
                }
                // Shape 정보 찾기
                ShapeItem shape = FindTreeShape(tvShape.SelectedNodes[0], true);
                // Shape 정보 화면 출력
                SetTreeShapeInfo(shape);
                isTreeEvent = false;
            }
            catch (Exception ex)
            {
                isTreeEvent = false;
                FireException("tvBlock_AfterSelect", ex);
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// Block 폼 채우기
        /// </summary>
        /// <param name="block">BlockItem</param>
        #region private void SetTreeBlockInfo(BlockItem block)
        private void SetTreeBlockInfo(BlockItem block)
        {
            if (block.LOC_GBN == "Block") return;

            chkbxTB1.Enabled = false;
            chkbxTB1.ForeColor = Color.Black;
            chkbxTB2.Enabled = false;
            chkbxTB2.ForeColor = Color.Black;

            txtBlockFtrCode.ReadOnly = true;
            txtBlockPFtrCode.ReadOnly = true;
            txtBlockFtrIdn.ReadOnly = true;
            txtBlockPFtrIdn.ReadOnly = true;
            txtBlockLocName.ReadOnly = true;
            txtBlockRelLocName.ReadOnly = true;
            txtBlockSgccd.ReadOnly = true;

            txtBlockLocCode.Text = block.LOC_CODE;
            lblLocCodeOrg.Text = block.LOC_CODE;
            txtBlockPLocCode.Text = block.PLOC_CODE;
            txtBlockLocGbn.Text = block.LOC_GBN;
            lblBlockResCode.Text = block.RES_CODE;
            //txtBlockKtGbn.Text = block.KT_GBN;
            cbBlockKtGbn.SelectedIndex = SetKtGbnCombo(block.KT_GBN);
            txtBlockOrderBy.Text = string.Format("{0}", block.ORDERBY);
            txtBlockFtrCode.Text = block.FTR_CODE;
            txtBlockFtrIdn.Text = block.FTR_IDN;
            txtBlockPFtrCode.Text = block.PFTR_CODE;
            txtBlockPFtrIdn.Text = block.PFTR_IDN;
            txtBlockLocName.Text = block.LOC_NAME;
            txtBlockRelLocName.Text = block.REL_LOC_NAME;
            txtBlockSgccd.Text = block.SGCCD;

            if (txtBlockOrderBy.Text == "0") txtBlockOrderBy.Text = "";
            if (block.CM_LOC_GBN == "1")
            {   // CM_LOCATION + CM_LOCATION2 (범용+보고서용)
                chkbxTB1.Checked = true;    // 범용
                chkbxTB2.Checked = false;   // 보고서용
            }
            else if (block.CM_LOC_GBN == "2")
            {
                // CM_LOCATION2 (보고서용)
                chkbxTB1.Checked = true;
                chkbxTB2.Checked = true;
            }
            else
            {
                chkbxTB1.Checked = false;
                chkbxTB2.Checked = false;
            }

            // Shape에 있고 Block에 없는 데이터일 경우
            if (block.IsTemp)
            {
                BlockItem parentItem = FindTreeBlock(tvBlock.SelectedNodes[0].Parent, true);
                if (parentItem != null)
                {
                    txtBlockPLocCode.Text = parentItem.LOC_CODE;
                }
                string tempLocCode = string.Format("000000{0}", _lastLocCode + 1);
                txtBlockLocCode.Text = tempLocCode.Substring(tempLocCode.Length - 6, 6);
                txtBlockOrderBy.Text = string.Format("{0}", _lastOrderBy + 1);
                chkbxTB1.Enabled = true;
                chkbxTB2.Enabled = true;
                lblNew.Visible = true;
            }
        } 
        #endregion

        /// <summary>
        /// 선택된 BlockNode의 BlockItem 찾기
        /// </summary>
        /// <param name="blockNode">UltraTreeNode</param>
        /// <param name="isNotSelect">ShapeTree 선택</param>
        /// <returns>BlockItem</returns>
        #region private BlockItem FindTreeBlock(Infragistics.Win.UltraWinTree.UltraTreeNode blockNode, bool isNotSelect)
        private BlockItem FindTreeBlock(Infragistics.Win.UltraWinTree.UltraTreeNode blockNode, bool isNotSelect)
        {
            BlockItem retVal = null;

            try
            {
                BlockTag blockTag0 = new BlockTag();
                BlockTag blockTag1 = new BlockTag();
                BlockTag blockTag2 = new BlockTag();
                BlockTag blockTag3 = new BlockTag();

                // Shape\함평대블럭\함평중블럭\함평02블럭
                string[] paths = blockNode.FullPath.Split('\\');
                int depth = paths.Length - 1;

                if (depth <= 0)
                {
                    // 태그가 0단계일 경우 Root
                    // 비어있는 Block 아이템 반환
                    retVal = new BlockItem();
                    retVal.SGCCD = _blockList[0].SGCCD;
                    retVal.LOC_GBN = "Block";
                    retVal.LOC_NAME = "Block";
                    return retVal;
                }

                if (depth >= 4)
                {
                    // 태그가 4단계일 경우 소소블록

                    // 대,중,소,소소블록 태그 정보 생성
                    blockTag0 = SplitBlockTag(string.Format("{0}", blockNode.Parent.Parent.Parent.Tag));
                    blockTag1 = SplitBlockTag(string.Format("{0}", blockNode.Parent.Parent.Tag));
                    blockTag2 = SplitBlockTag(string.Format("{0}", blockNode.Parent.Tag));
                    blockTag3 = SplitBlockTag(string.Format("{0}", blockNode.Tag));

                    #region BlockItem 찾기
                    // 대블록 찾기
                    var objects = _blockList.AsEnumerable();
                    // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                    if (blockTag0.LOC_CODE == null || blockTag0.LOC_CODE == "")
                    {
                        objects = from BlockItem item in _blockList
                                  where item.FTR_IDN == blockTag0.FTR_IDN
                                  select item;
                    }
                    else
                    {
                        objects = from BlockItem item in _blockList
                                  where item.LOC_CODE == blockTag0.LOC_CODE
                                  select item;
                    }
                    // 찾아낸 대블록이 있을 경우
                    if (objects != null && objects.Count() > 0)
                    {
                        BlockItem block0 = objects.First();
                        // 중블록 찾기
                        var objects1 = _blockList.AsEnumerable();
                        // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                        if (blockTag1.LOC_CODE == null || blockTag1.LOC_CODE == "")
                        {
                            objects1 = from BlockItem item in block0.Child
                                       where item.FTR_IDN == blockTag1.FTR_IDN
                                       select item;
                        }
                        else
                        {
                            objects1 = from BlockItem item in block0.Child
                                       where item.LOC_CODE == blockTag1.LOC_CODE
                                       select item;
                        }
                        if (objects1 != null && objects1.Count() > 0)
                        {
                            BlockItem block1 = objects1.First();
                            // 소블록 찾기
                            var objects2 = _blockList.AsEnumerable();
                            // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                            if (blockTag2.LOC_CODE == null || blockTag2.LOC_CODE == "")
                            {
                                objects2 = from BlockItem item in block1.Child
                                           where item.FTR_IDN == blockTag2.FTR_IDN
                                           select item;
                            }
                            else
                            {
                                objects2 = from BlockItem item in block1.Child
                                           where item.LOC_CODE == blockTag2.LOC_CODE
                                           select item;
                            }
                            if (objects2 != null && objects2.Count() > 0)
                            {
                                BlockItem block2 = objects2.First();
                                // 소소블록 찾기
                                var objects3 = _blockList.AsEnumerable();
                                // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                                if (blockTag3.LOC_CODE == null || blockTag3.LOC_CODE == "")
                                {
                                    objects3 = from BlockItem item in block2.Child
                                               where item.FTR_IDN == blockTag3.FTR_IDN
                                               select item;
                                }
                                else
                                {
                                    objects3 = from BlockItem item in block2.Child
                                               where item.LOC_CODE == blockTag3.LOC_CODE
                                               select item;
                                }
                                if (objects3 != null && objects3.Count() > 0)
                                {
                                    retVal = objects3.First();
                                }
                            }
                        }
                    }
                    #endregion
                }
                else if (depth >= 3)
                {
                    // 태그가 3단계일 경우 소블록

                    // 대,중,소블록 태그 정보 생성
                    blockTag0 = SplitBlockTag(string.Format("{0}", blockNode.Parent.Parent.Tag));
                    blockTag1 = SplitBlockTag(string.Format("{0}", blockNode.Parent.Tag));
                    blockTag2 = SplitBlockTag(string.Format("{0}", blockNode.Tag));

                    #region BlockItem 찾기
                    var objects = _blockList.AsEnumerable();
                    // 대블록 찾기
                    // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                    if (blockTag0.LOC_CODE == null || blockTag0.LOC_CODE == "")
                    {
                        objects = from BlockItem item in _blockList
                                  where item.FTR_IDN == blockTag0.FTR_IDN
                                  select item;
                    }
                    else
                    {
                        objects = from BlockItem item in _blockList
                                  where item.LOC_CODE == blockTag0.LOC_CODE
                                  select item;
                    }
                    if (objects != null && objects.Count() > 0)
                    {
                        BlockItem block0 = objects.First();
                        // 중블록 찾기
                        var objects1 = _blockList.AsEnumerable();
                        // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                        if (blockTag1.LOC_CODE == null || blockTag1.LOC_CODE == "")
                        {
                            objects1 = from BlockItem item in block0.Child
                                       where item.FTR_IDN == blockTag1.FTR_IDN
                                       select item;
                        }
                        else
                        {
                            objects1 = from BlockItem item in block0.Child
                                       where item.LOC_CODE == blockTag1.LOC_CODE
                                       select item;
                        }
                        if (objects1 != null && objects1.Count() > 0)
                        {
                            BlockItem block1 = objects1.First();
                            // 소블록 찾기
                            var objects2 = _blockList.AsEnumerable();
                            // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                            if (blockTag2.LOC_CODE == null || blockTag2.LOC_CODE == "")
                            {
                                objects2 = from BlockItem item in block1.Child
                                           where item.FTR_IDN == blockTag2.FTR_IDN
                                           select item;
                            }
                            else
                            {
                                objects2 = from BlockItem item in block1.Child
                                           where item.LOC_CODE == blockTag2.LOC_CODE
                                           select item;
                            }
                            if (objects2 != null && objects2.Count() > 0)
                            {
                                retVal = objects2.First();
                            }
                        }
                    }
                    #endregion
                }
                else if (depth >= 2)
                {
                    // 태그가 2단계일 경우 중블록

                    // 대,중블록 태그 정보 생성
                    blockTag0 = SplitBlockTag(string.Format("{0}", blockNode.Parent.Tag));
                    blockTag1 = SplitBlockTag(string.Format("{0}", blockNode.Tag));

                    #region BlockItem 찾기
                    // 대블록 찾기
                    var objects = _blockList.AsEnumerable();
                    // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                    if (blockTag0.LOC_CODE == null || blockTag0.LOC_CODE == "")
                    {
                        objects = from BlockItem item in _blockList
                                  where item.FTR_IDN == blockTag0.FTR_IDN
                                  select item;
                    }
                    else
                    {
                        objects = from BlockItem item in _blockList
                                  where item.LOC_CODE == blockTag0.LOC_CODE
                                  select item;
                    }
                    if (objects != null && objects.Count() > 0)
                    {
                        BlockItem block0 = objects.First();
                        // 중블록 찾기
                        var objects1 = _blockList.AsEnumerable();
                        // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                        if (blockTag1.LOC_CODE == null || blockTag1.LOC_CODE == "")
                        {
                            objects1 = from BlockItem item in block0.Child
                                       where item.FTR_IDN == blockTag1.FTR_IDN
                                       select item;
                        }
                        else
                        {
                            objects1 = from BlockItem item in block0.Child
                                       where item.LOC_CODE == blockTag1.LOC_CODE
                                       select item;
                        }
                        if (objects1 != null && objects1.Count() > 0)
                        {
                            retVal = objects1.First();
                        }
                    }
                    #endregion
                }
                else if (depth >= 1)
                {
                    // 태그가 1단계일 경우 소블록

                    // 소블록 태그 정보 생성
                    blockTag0 = SplitBlockTag(string.Format("{0}", blockNode.Tag));

                    #region BlockItem 찾기
                    // 소블록 찾기
                    var objects = _blockList.AsEnumerable();
                    // LOC_CODE가 없을 경우 FTR_IDN을 이용하여 찾아낸다.
                    if (blockTag0.LOC_CODE == null || blockTag0.LOC_CODE == "")
                    {
                        objects = from BlockItem item in _blockList
                                  where item.FTR_IDN == blockTag0.FTR_IDN
                                  select item;
                    }
                    else
                    {
                        objects = from BlockItem item in _blockList
                                  where item.LOC_CODE == blockTag0.LOC_CODE
                                  select item;
                    }
                    if (objects != null && objects.Count() > 0)
                    {
                        retVal = objects.First();
                    }
                    #endregion
                }

                // TreeView 강제 선택 여부 (ShapeTree 이벤트 연계)
                if (isNotSelect) return retVal;

                // 테이블 선택
                if (retVal.IsTemp)
                {
                    chkbxTB1.Enabled = true;
                    chkbxTB2.Enabled = true;
                }
                else
                {
                    chkbxTB1.Enabled = false;
                    chkbxTB2.Enabled = false;
                }

                bool isFind = false;
                // Shape의 Root 노드
                Infragistics.Win.UltraWinTree.UltraTreeNode root = tvShape.Nodes[0];
                // 대블록
                for (int i = 0; i < root.Nodes.Count; i++)
                {
                    // 대블록 아이템 찾기
                    ShapeTag shapeTag = SplitShapeTag(string.Format("{0}", root.Nodes[i].Tag));
                    if (shapeTag.FTR_IDN == blockTag0.FTR_IDN)
                    {
                        if (blockTag1.FTR_IDN == null || blockTag1.FTR_IDN == "")
                        {
                            // 중블록 정보가 없다면 대블록 선택이므로 더이상 찾지 않고 종료
                            root.Nodes[i].Selected = true;
                            isFind = true;
                            break;
                        }
                        // 중블록 태그가 있을 경우
                        for (int j = 0; j < root.Nodes[i].Nodes.Count; j++)
                        {
                            // 중블록 아이템 찾기
                            shapeTag = SplitShapeTag(string.Format("{0}", root.Nodes[i].Nodes[j].Tag));
                            if (shapeTag.FTR_IDN == blockTag1.FTR_IDN)
                            {
                                if (blockTag2.FTR_IDN == null || blockTag2.FTR_IDN == "")
                                {
                                    // 소블록 태그가 없다면 중블록 선택이므로 더이상 찾지 않고 종료
                                    root.Nodes[i].Nodes[j].Selected = true;
                                    isFind = true;
                                    break;
                                }
                                // 소블록 태그가 있을 경우
                                for (int k = 0; k < root.Nodes[i].Nodes[j].Nodes.Count; k++)
                                {
                                    // 소블록 찾기
                                    shapeTag = SplitShapeTag(string.Format("{0}", root.Nodes[i].Nodes[j].Nodes[k].Tag));
                                    if (shapeTag.FTR_IDN == blockTag2.FTR_IDN)
                                    {
                                        if (blockTag3.FTR_IDN == null || blockTag3.FTR_IDN == "")
                                        {
                                            root.Nodes[i].Nodes[j].Nodes[k].Selected = true;
                                            isFind = true;
                                            break;
                                        }
                                        // 소소블록
                                        for (int m = 0; m < root.Nodes[i].Nodes[j].Nodes[k].Nodes.Count; m++)
                                        {
                                            shapeTag = SplitShapeTag(string.Format("{0}", root.Nodes[i].Nodes[j].Nodes[k].Nodes[m].Tag));
                                            if (shapeTag.FTR_IDN == blockTag3.FTR_IDN)
                                            {
                                                root.Nodes[k].Nodes[j].Nodes[k].Nodes[m].Selected = true;
                                                isFind = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                // 만약 Shape 정보를 찾지 못 했다면 Shape 트리에서 선택을 초기화한다.
                if (!isFind) tvShape.SelectedNodes.Clear();
            }
            catch (Exception ex)
            {
                FireException("FindTreeBlock", ex);
                throw ex;
            }
            return retVal;
        }
        #endregion

        /// <summary>
        /// Block TreeNode의 Tag를 BlockTag 정보로 분리
        /// </summary>
        /// <param name="tag">TreeNode.Tag</param>
        /// <returns>BlockTag</returns>
        #region private BlockTag SplitBlockTag(string tag)
        private BlockTag SplitBlockTag(string tag)
        {
            // LOC_CODE | PLOC_CODE | FTR_CODE | PFTR_CODE | FTR_IDN | PFTR_IDN
            BlockTag retVal = new BlockTag();
            string[] split = tag.Split('|');
            if (split != null && split.Length == 6)
            {
                retVal.LOC_CODE = split[0];
                retVal.PLOC_CODE = split[1];
                retVal.FTR_CODE = split[2];
                retVal.PFTR_CODE = split[3];
                retVal.FTR_IDN = split[4];
                retVal.PFTR_IDN = split[5];
            }
            return retVal;
        } 
        #endregion
        #endregion






        #region 기타 화면 함수 및 이벤트

        private void chkbxTB1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbxTB1.Checked) chkbxTB2.Checked = true;
        }

        /// <summary>
        /// Block 정보폼 초기화
        /// </summary>
        #region private void ClearBlockForm()
        private void ClearBlockForm()
        {
            txtBlockLocCode.Text = "";
            txtBlockPLocCode.Text = "";
            txtBlockLocGbn.Text = "";
            lblBlockResCode.Text = "";
            //txtBlockKtGbn.Text = "";
            cbBlockKtGbn.SelectedIndex = SetKtGbnCombo("");
            txtBlockOrderBy.Text = "";
            txtBlockFtrCode.Text = "";
            txtBlockFtrIdn.Text = "";
            txtBlockPFtrCode.Text = "";
            txtBlockPFtrIdn.Text = "";
            txtBlockLocName.Text = "";
            txtBlockRelLocName.Text = "";
            txtBlockSgccd.Text = "";
        } 
        #endregion

        /// <summary>
        /// Shape 정보폼 초기화
        /// </summary>
        #region private void ClearShapeForm()
        private void ClearShapeForm()
        {
            txtShapeFtrCode.Text = "";
            txtShapeFtrIdn.Text = "";
            txtShapePFtrCode.Text = "";
            txtShapePFtrIdn.Text = "";
            txtShapeLocName.Text = "";
            txtShapeRelLocName.Text = "";
            txtShapeSgccd.Text = "";
        } 
        #endregion
        #endregion







        #region Databases 처리
        /// <summary>
        /// Insert/Update
        /// </summary>
        /// <param name="isNew">신규 여부</param>
        /// <returns>처리결과</returns>
        #region private bool Save(bool isNew)
        private bool Save(bool isNew)
        {
            bool retVal = false;
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);

                mapper.BeginTransaction();
                
                if (isNew)
                {
                    //IDataParameter[] parameters0 = CreateCmLocationInsertParam();
                    //mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION_INSERT, parameters0);
                    //if (chkbxTB1.Checked)
                    //{
                    //    IDataParameter[] parameters1 = CreateCmLocationInsertParam();
                    //    mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION2_INSERT, parameters1);
                    //}


                    // CM_LOCATION2 (보고서용 체크시 2에만 저장)
                    IDataParameter[] parameters1 = CreateCmLocationInsertParam();
                    mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION2_INSERT, parameters1);

                    if (chkbxTB2.Checked == false)
                    {
                        IDataParameter[] parameters0 = CreateCmLocationInsertParam();
                        // CM_LOCATION
                        mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION_INSERT, parameters0);
                    }

                    retVal = true;
                }
                else
                {
                    //IDataParameter[] paramCmLocation0 = CreateCmLocationUpdateParam();
                    //mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION_UPDATE, paramCmLocation0);
                    //if (chkbxTB1.Checked)
                    //{
                    //    IDataParameter[] paramCmLocation1 = CreateCmLocationUpdateParam();
                    //    mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION2_UPDATE, paramCmLocation1);
                    //}

                    IDataParameter[] paramCmLocation1 = CreateCmLocationUpdateParam();
                    mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION2_UPDATE, paramCmLocation1);

                    if (chkbxTB2.Checked == false)
                    {
                        IDataParameter[] paramCmLocation0 = CreateCmLocationUpdateParam();
                        mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION_UPDATE, paramCmLocation0);
                    }
                    retVal = true;
                }
                
                /*
                // CM_LOCATION
                IDataParameter[] parameter1 = CreateCmLocationMergeParam();
                mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION_INS_UPD, parameter1);
                if (chkbxTB1.Checked)
                {
                    // CM_LOCATION2
                    IDataParameter[] parameter2 = CreateCmLocation2MergeParam();
                    mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION2_INS_UPD, parameter2);
                }
                 */

                mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                mapper.RollbackTransaction();
                FireException("Save", ex);
                retVal = false;
                throw ex;
            }
            return retVal;
        } 
        #endregion

        /// <summary>
        /// Delete
        /// </summary>
        /// <returns>처리결과</returns>
        #region private bool Delete()
        private bool Delete()
        {
            bool retVal = false;
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);

                mapper.BeginTransaction();

                IDataParameter[] checkParam = CreateChildCountParam();
                object count = mapper.ExecuteScriptScalar(Database.Queries.BLOCKMAPPING_CM_LOCATION2_CHILD_COUNT, checkParam);
                if (count != null)
                {
                    int cnt = 0;
                    if (int.TryParse(string.Format("{0}", count), out cnt))
                    {
                        // 자식으로 등록된 정보가 있다면 확인
                        if (cnt > 0)
                        {
                            string msg = "하위 등록된 정보가 있습니다.\r\n진행시 하위 정보도 함께 삭제됩니다.\r\n삭제하시겠습니까?";
                            if (MessageBox.Show(msg, "삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return false;

                            // 하위 정보 삭제
                            IDataParameter[] childDelParam = CreateCmLocationDeleteParam();
                            mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION_CHILD_DELETE, childDelParam);
                            IDataParameter[] childDelParam2 = CreateCmLocationDeleteParam();
                            mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION2_CHILD_DELETE, childDelParam2);
                        }
                    }
                }

                IDataParameter[] parameters2 = CreateCmLocationDeleteParam();
                mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION2_DELETE, parameters2);

                if (chkbxTB1.Checked)
                {
                    IDataParameter[] parameters = CreateCmLocationDeleteParam();
                    mapper.ExecuteScript(Database.Queries.BLOCKMAPPING_CM_LOCATION_DELETE, parameters);
                }
                mapper.CommitTransaction();
                retVal = true;
            }
            catch (Exception ex)
            {
                mapper.RollbackTransaction();
                FireException("Delete", ex);
                retVal = false;
                throw ex;
            }
            return retVal;
        } 
        #endregion

        #region private IDataParameter[] CreateCmLocationMergeParam()
        //private IDataParameter[] CreateCmLocationMergeParam()
        //{
        //    IDataParameter[] parameters = {
        //                                      new OracleParameter("1", OracleDbType.Varchar2),  // [0] LOC_CODE
        //                                      new OracleParameter("2", OracleDbType.Varchar2),  // [1] PLOC_CODE
        //                                      new OracleParameter("3", OracleDbType.Varchar2),  // [2] LOC_GBN
        //                                      new OracleParameter("4", OracleDbType.Varchar2),  // [3] RES_CODE
        //                                      new OracleParameter("5", OracleDbType.Varchar2),  // [4] KT_GBN
        //                                      new OracleParameter("6", OracleDbType.Int32),     // [5] ORDERBY
        //                                      new OracleParameter("7", OracleDbType.Varchar2),  // [6] LOC_CODE
        //                                      new OracleParameter("8", OracleDbType.Varchar2),  // [7] PLOC_CODE
        //                                      new OracleParameter("9", OracleDbType.Varchar2),  // [8] LOC_GBN
        //                                      new OracleParameter("10", OracleDbType.Varchar2), // [9] FTR_CODE
        //                                      new OracleParameter("11", OracleDbType.Varchar2), // [10] FTR_IDN
        //                                      new OracleParameter("12", OracleDbType.Varchar2), // [11] LOC_NAME
        //                                      new OracleParameter("13", OracleDbType.Varchar2), // [12] REL_LOC_NAME
        //                                      new OracleParameter("14", OracleDbType.Varchar2), // [13] PFTR_CODE
        //                                      new OracleParameter("15", OracleDbType.Varchar2), // [14] PFTR_IDN
        //                                      new OracleParameter("16", OracleDbType.Varchar2), // [15] SGCCD
        //                                      new OracleParameter("17", OracleDbType.Varchar2), // [16] RES_CODE
        //                                      new OracleParameter("18", OracleDbType.Varchar2), // [17] KT_GBN
        //                                      new OracleParameter("19", OracleDbType.Int32)     // [18] ORDERBY
        //                                  };

        //    object orderBy = DBNull.Value;
        //    if (txtBlockOrderBy.Text.Length > 0)
        //        orderBy = Convert.ToInt32(txtBlockOrderBy.Text);

        //    parameters[0].Value = txtBlockLocCode.Text;     // [0] LOC_CODE
        //    parameters[1].Value = CheckParamVal(txtBlockPLocCode.Text);    // [1] PLOC_CODE
        //    parameters[2].Value = CheckParamVal(txtBlockLocGbn.Text);      // [2] LOC_GBN
        //    parameters[3].Value = CheckParamVal(lblBlockResCode.Text);     // [3] RES_CODE
        //    parameters[4].Value = CheckParamVal(txtBlockKtGbn.Text);       // [4] KT_GBN
        //    parameters[5].Value = orderBy;                  // [5] ORDERBY
        //    parameters[6].Value = CheckParamVal(txtBlockLocCode.Text);     // [6] LOC_CODE
        //    parameters[7].Value = CheckParamVal(txtBlockPLocCode.Text);    // [7] PLOC_CODE
        //    parameters[8].Value = CheckParamVal(txtBlockLocGbn.Text);      // [8] LOC_GBN
        //    parameters[9].Value = CheckParamVal(txtBlockFtrCode.Text);     // [9] FTR_CODE
        //    parameters[10].Value = CheckParamVal(txtBlockFtrIdn.Text);     // [10] FTR_IDN
        //    parameters[11].Value = CheckParamVal(txtBlockLocName.Text);    // [11] LOC_NAME
        //    parameters[12].Value = CheckParamVal(txtBlockRelLocName.Text); // [12] REL_LOC_NAME
        //    parameters[13].Value = CheckParamVal(txtBlockPFtrCode.Text);   // [13] PFTR_CODE
        //    parameters[14].Value = CheckParamVal(txtBlockPFtrIdn.Text);    // [14] PFTR_IDN
        //    parameters[15].Value = CheckParamVal(txtBlockSgccd.Text);      // [15] SGCCD
        //    parameters[16].Value = CheckParamVal(lblBlockResCode.Text);    // [16] RES_CODE
        //    parameters[17].Value = CheckParamVal(txtBlockKtGbn.Text);      // [17] KT_GBN
        //    parameters[18].Value = orderBy;                 // [18] ORDERBY

        //    return parameters;
        //}
        #endregion

        #region private IDataParameter[] CreateCmLocation2MergeParam()
        //private IDataParameter[] CreateCmLocation2MergeParam()
        //{
        //    IDataParameter[] parameters = {
        //                                      new OracleParameter("1", OracleDbType.Varchar2),
        //                                      new OracleParameter("2", OracleDbType.Varchar2),
        //                                      new OracleParameter("3", OracleDbType.Varchar2),
        //                                      new OracleParameter("4", OracleDbType.Varchar2),
        //                                      new OracleParameter("5", OracleDbType.Varchar2),
        //                                      new OracleParameter("6", OracleDbType.Int32),
        //                                      new OracleParameter("7", OracleDbType.Varchar2),
        //                                      new OracleParameter("8", OracleDbType.Varchar2),
        //                                      new OracleParameter("9", OracleDbType.Varchar2),
        //                                      new OracleParameter("10", OracleDbType.Varchar2),
        //                                      new OracleParameter("11", OracleDbType.Varchar2),
        //                                      new OracleParameter("12", OracleDbType.Varchar2),
        //                                      new OracleParameter("13", OracleDbType.Varchar2),
        //                                      new OracleParameter("14", OracleDbType.Varchar2),
        //                                      new OracleParameter("15", OracleDbType.Varchar2),
        //                                      new OracleParameter("16", OracleDbType.Varchar2),
        //                                      new OracleParameter("17", OracleDbType.Varchar2),
        //                                      new OracleParameter("18", OracleDbType.Varchar2),
        //                                      new OracleParameter("19", OracleDbType.Int32)
        //                                  };

        //    object orderBy = DBNull.Value;
        //    if (txtBlockOrderBy.Text.Length > 0)
        //        orderBy = Convert.ToInt32(txtBlockOrderBy.Text);

        //    parameters[0].Value = txtBlockLocCode.Text;
        //    parameters[1].Value = txtBlockPLocCode.Text;
        //    parameters[2].Value = txtBlockLocGbn.Text;
        //    parameters[3].Value = lblBlockResCode.Text;
        //    parameters[4].Value = txtBlockKtGbn.Text;
        //    parameters[5].Value = orderBy;
        //    parameters[6].Value = txtBlockLocCode.Text;
        //    parameters[7].Value = txtBlockPLocCode.Text;
        //    parameters[8].Value = txtBlockLocGbn.Text;
        //    parameters[9].Value = txtBlockFtrCode.Text;
        //    parameters[10].Value = txtBlockFtrIdn.Text;
        //    parameters[11].Value = txtBlockLocName.Text;
        //    parameters[12].Value = txtBlockRelLocName.Text;
        //    parameters[13].Value = txtBlockPFtrCode.Text;
        //    parameters[14].Value = txtBlockPFtrIdn.Text;
        //    parameters[15].Value = txtBlockSgccd.Text;
        //    parameters[16].Value = lblBlockResCode.Text;
        //    parameters[17].Value = txtBlockKtGbn.Text;
        //    parameters[18].Value = orderBy;

        //    return parameters;
        //}
        #endregion

        #region private IDataParameter[] CreateCmLocationInsertParam()
        private IDataParameter[] CreateCmLocationInsertParam()
        {
            IDataParameter[] parameters = {
                                              new OracleParameter("1", OracleDbType.Varchar2),
                                              new OracleParameter("2", OracleDbType.Varchar2),
                                              new OracleParameter("3", OracleDbType.Varchar2),
                                              new OracleParameter("4", OracleDbType.Varchar2),
                                              new OracleParameter("5", OracleDbType.Varchar2),
                                              new OracleParameter("6", OracleDbType.Varchar2),
                                              new OracleParameter("7", OracleDbType.Varchar2),
                                              new OracleParameter("8", OracleDbType.Varchar2),
                                              new OracleParameter("9", OracleDbType.Varchar2),
                                              new OracleParameter("10", OracleDbType.Varchar2),
                                              new OracleParameter("11", OracleDbType.Varchar2),
                                              new OracleParameter("12", OracleDbType.Varchar2),
                                              new OracleParameter("13", OracleDbType.Int32)
            };

            parameters[0].Value = txtBlockLocCode.Text;
            parameters[1].Value = txtBlockPLocCode.Text;
            parameters[2].Value = txtBlockLocGbn.Text;
            parameters[3].Value = txtBlockFtrCode.Text;
            parameters[4].Value = txtBlockFtrIdn.Text;
            parameters[5].Value = txtBlockLocName.Text;
            parameters[6].Value = txtBlockRelLocName.Text;
            parameters[7].Value = txtBlockPFtrCode.Text;
            parameters[8].Value = txtBlockPFtrIdn.Text;
            parameters[9].Value = txtBlockSgccd.Text;
            parameters[10].Value = lblBlockResCode.Text;
            //parameters[11].Value = txtBlockKtGbn.Text;
            parameters[11].Value = GetKtGbnCombo();
            if (txtBlockOrderBy.Text.Length <= 0)
                parameters[12].Value = DBNull.Value;
            else
                parameters[12].Value = Convert.ToInt32(txtBlockOrderBy.Text);
            return parameters;
        } 
        #endregion

        #region private IDataParameter[] CreateCmLocationUpdateParam()
        private IDataParameter[] CreateCmLocationUpdateParam()
        {
            IDataParameter[] parameters = {
                                              new OracleParameter("1", OracleDbType.Varchar2),
                                              new OracleParameter("2", OracleDbType.Varchar2),
                                              new OracleParameter("3", OracleDbType.Varchar2),
                                              new OracleParameter("4", OracleDbType.Varchar2),
                                              new OracleParameter("5", OracleDbType.Varchar2),
                                              new OracleParameter("6", OracleDbType.Int32),
                                              new OracleParameter("7", OracleDbType.Varchar2),
                                              new OracleParameter("8", OracleDbType.Varchar2)
            };

            parameters[0].Value = txtBlockLocCode.Text;
            parameters[1].Value = txtBlockPLocCode.Text;
            parameters[2].Value = txtBlockLocGbn.Text;
            parameters[3].Value = lblBlockResCode.Text;
            //parameters[4].Value = txtBlockKtGbn.Text;
            parameters[4].Value = GetKtGbnCombo();
            if (txtBlockOrderBy.Text.Length <= 0)
                parameters[5].Value = DBNull.Value;
            else
                parameters[5].Value = Convert.ToInt32(txtBlockOrderBy.Text);
            //parameters[6].Value = txtBlockLocCode.Text;
            parameters[6].Value = lblLocCodeOrg.Text;
            parameters[7].Value = EMFrame.statics.AppStatic.USER_SGCCD;

            return parameters;
        }
        #endregion

        #region private IDataParameter[] CreateCmLocationDeleteParam()
        private IDataParameter[] CreateCmLocationDeleteParam()
        {
            IDataParameter[] parameters = {
                                              new OracleParameter("1", OracleDbType.Varchar2),
                                              new OracleParameter("2", OracleDbType.Varchar2)
                                          };

            parameters[0].Value = txtBlockLocCode.Text;
            parameters[1].Value = EMFrame.statics.AppStatic.USER_SGCCD;
            return parameters;
        } 
        #endregion

        #region private IDataParameter[] CreateChildCountParam()
        private IDataParameter[] CreateChildCountParam()
        {
            IDataParameter[] parameters = {
                                              new OracleParameter("1", OracleDbType.Varchar2),
                                              new OracleParameter("2", OracleDbType.Varchar2)
                                          };
            parameters[0].Value = txtBlockLocCode.Text;
            parameters[1].Value = EMFrame.statics.AppStatic.USER_SGCCD;
            return parameters;
        } 
        #endregion

        /// <summary>
        /// 저장 데이터 검증
        /// </summary>
        /// <param name="block">선택 블록</param>
        /// <returns>검증 결과</returns>
        #region private bool ValidateForm()//BlockItem block)
        private bool ValidateForm()//BlockItem block)
        {
            if (txtBlockLocCode.Text.Length <= 0)
            {
                MessageBox.Show("'지역관리코드'를 입력하십시오.", "입력오류", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBlockLocCode.Focus();
                return false;
            }
            if (txtBlockLocGbn.Text.Length <= 0)
            {
                MessageBox.Show("'지역관리구분'을 입력하십시오.", "입력오류", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBlockLocGbn.Focus();
                return false;
            }
            if (chkbxTB1.Checked == false && chkbxTB2.Checked == false)
            {
                MessageBox.Show("'범용', '보고서용'을 선택하십시오.", "입력오류", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                chkbxTB1.Focus();
                return false;
            }
            if (chkbxTB1.Checked == false)
            {
                MessageBox.Show("'범용', '보고서용'은 기본 설정값입니다.", "보고서용", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkbxTB2.Checked = true;
            }
            if (chkbxTB1.Checked == true && chkbxTB2.Checked == false)
            {
                MessageBox.Show("CM_LOCATION2에만 데이터가 저장됩니다.", "보고서용", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            // 신규 체크
            if (txtBlockLocName.Text.Length <= 0)
            {
                MessageBox.Show("'지역관리명'을 입력하십시오.", "입력오류", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBlockLocName.Focus();
                return false;
            }
            if (txtBlockLocGbn.Text == ShapeDataTable.BLOCK_NAME.BLBG ||
                txtBlockLocGbn.Text == ShapeDataTable.BLOCK_NAME.BLBM ||
                txtBlockLocGbn.Text == ShapeDataTable.BLOCK_NAME.BLSM ||
                txtBlockLocGbn.Text == ShapeDataTable.BLOCK_NAME.BLSS)
            {
                if (txtBlockFtrCode.Text.Length <= 0)
                {
                    MessageBox.Show("'계층코드'를 입력하십시오.", "입력오류", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtBlockFtrCode.Focus();
                    return false;
                }
            }
            if (txtBlockSgccd.Text.Length <= 0)
            {
                MessageBox.Show("'지자체코드'를 입력하십시오.", "입력오류", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBlockSgccd.Focus();
                return false;
            }

            return true;
        } 
        #endregion
        #endregion






        /// <summary>
        /// 관련 배수지 선택
        /// </summary>
        #region private void btnResCode_Click(object sender, EventArgs e)
        private void btnResCode_Click(object sender, EventArgs e)
        {
            try
            {
                FrmFindResCode findResCode = new FrmFindResCode(_blockList);
                if (findResCode.ShowDialog(this) == DialogResult.OK)
                {
                    lblBlockResCode.Text = findResCode.Selected;
                }
            }
            catch (Exception ex)
            {
                FireException("btnResCode_Click", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion






        /// <summary>
        /// 지역 열기 버튼
        /// </summary>
        #region private void mnuOpen_Click(object sender, EventArgs e)
        private void mnuOpen_Click(object sender, EventArgs e)
        {
            OpenZone();
        } 
        #endregion
        

        #region 목록 다시 읽기 버튼
        private void btnReload_Click(object sender, EventArgs e)
        {
            lblNew.Visible = false;

            ClearShapeForm();
            ClearBlockForm();

            tvShape.Nodes.Clear();
            tvBlock.Nodes.Clear();

            InitializeList();

            isTreeEvent = false;
        } 
        #endregion





        /// <summary>
        /// 태그 매핑 버튼
        /// </summary>
        #region private void btnTagManage_Click(object sender, EventArgs e)
        private void btnTagManage_Click(object sender, EventArgs e)
        {
            TagManage.FrmTagManage tagManage = new BlockApp.TagManage.FrmTagManage();
            tagManage.Show(this);
        } 
        #endregion

        /// <summary>
        /// 태그 계산식 관리 버튼
        /// </summary>
        #region private void btnTagCalc_Click(object sender, EventArgs e)
        private void btnTagCalc_Click(object sender, EventArgs e)
        {
            TagManage.FrmTagCalcManage tagCalc = new BlockApp.TagManage.FrmTagCalcManage();
            tagCalc.Show(this);
        } 
        #endregion

        /// <summary>
        /// 태그 계산식 관리 버튼 (기타 태그구분)
        /// </summary>
        #region private void btnTagCalcEtc_Click(object sender, EventArgs e)
        private void btnTagCalcEtc_Click(object sender, EventArgs e)
        {
            TagManage.FrmTagCalcManageEtc tagCalc = new BlockApp.TagManage.FrmTagCalcManageEtc();
            tagCalc.Show(this);
        } 
        #endregion









        /// <summary>
        /// 태그 매핑
        /// </summary>
        private void mnuTagMapping_Click(object sender, EventArgs e)
        {
            TagManage.FrmTagManage tagManage = new BlockApp.TagManage.FrmTagManage();
            tagManage.Show(this);
        }

        /// <summary>
        /// 태그 계산식
        /// </summary>
        private void mnuTagCalc_Click(object sender, EventArgs e)
        {
            TagManage.FrmTagCalcManage tagCalc = new BlockApp.TagManage.FrmTagCalcManage();
            tagCalc.Show(this);
        }

        /// <summary>
        /// 태그 계산식
        /// </summary>
        private void mnuTagCalcEtc_Click(object sender, EventArgs e)
        {
            TagManage.FrmTagCalcManageEtc tagCalc = new BlockApp.TagManage.FrmTagCalcManageEtc();
            tagCalc.Show(this);
        }



        /// <summary>
        /// Water-Infos 코드 매핑
        /// </summary>
        private void mnuWaterInfosCode_Click(object sender, EventArgs e)
        {
            CodeManage.FrmWaterInfosCode waterInfosCode = new BlockApp.CodeManage.FrmWaterInfosCode();
            waterInfosCode.Show(this);
        }

        /// <summary>
        /// 관망운영일보 설정
        /// </summary>
        private void mnuSystemMaster_Click(object sender, EventArgs e)
        {
            SystemManage.FrmSystemMaster systemMaster = new BlockApp.SystemManage.FrmSystemMaster();
            systemMaster.Show(this);
        }

        private void mnuExcelUpload_Click(object sender, EventArgs e)
        {
            ExcelUpload.FrmRead frmRead = new BlockApp.ExcelUpload.FrmRead();
            frmRead.Show(this);
        }





























        #region 신규 버튼
        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvBlock.SelectedNodes == null || tvBlock.SelectedNodes.Count <= 0) return;
                // 현재 선택된 트리노드의 자식으로 등록
                BlockItem pItem = FindTreeBlock(tvBlock.SelectedNodes[0], true);

                if (pItem.LOC_GBN == "소소블록")
                {
                    MessageBox.Show("소소블록 이하는 등록할 수 없습니다.", "등록불가", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                MessageBox.Show(pItem.LOC_NAME + "의 하위로 등록합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

                lblNew.Visible = true;
                ClearBlockForm();

                string tempLocCode = string.Format("000000{0}", _lastLocCode + 1);
                txtBlockLocCode.Text = tempLocCode.Substring(tempLocCode.Length - 6, 6);
                txtBlockOrderBy.Text = string.Format("{0}", _lastOrderBy + 1);
                txtBlockPLocCode.Text = pItem.LOC_CODE;
                txtBlockLocGbn.Text = GetNewBlockGubun(pItem.LOC_GBN);

                chkbxTB1.Enabled = true;
                chkbxTB2.Enabled = true;

                txtBlockFtrCode.ReadOnly = false;
                txtBlockPFtrCode.ReadOnly = false;
                txtBlockFtrIdn.ReadOnly = false;
                txtBlockPFtrIdn.ReadOnly = false;
                txtBlockLocName.ReadOnly = false;
                txtBlockRelLocName.ReadOnly = false;
                txtBlockSgccd.ReadOnly = false;

                chkbxTB1.Checked = true;
                chkbxTB2.Checked = false;
                txtBlockLocName.Text = "신규_" + tempLocCode;
                txtBlockSgccd.Text = pItem.SGCCD;
            }
            catch (Exception ex)
            {
                FireException("btnNew_Click", ex);
            }
        }

        /// <summary>
        /// 선택된 블록의 하위 블록 구분 반환
        /// </summary>
        /// <param name="pBlkGbn">상위 블록 구분</param>
        /// <returns>하위 블록 구분</returns>
        private string GetNewBlockGubun(string pBlkGbn)
        {
            if (pBlkGbn == "Block") return ShapeDataTable.BLOCK_NAME.BLBG;
            else if (pBlkGbn == ShapeDataTable.BLOCK_NAME.BLBG) return ShapeDataTable.BLOCK_NAME.BLBM;
            else if (pBlkGbn == ShapeDataTable.BLOCK_NAME.BLBM) return ShapeDataTable.BLOCK_NAME.BLSM;
            else if (pBlkGbn == ShapeDataTable.BLOCK_NAME.BLSM) return ShapeDataTable.BLOCK_NAME.BLSS;
            else return "";
        }
        #endregion

        #region 저장 버튼
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateForm() == false) return;

                bool isNew = lblNew.Visible;

                if (!Save(isNew))
                {
                    // 저장 처리 실패시
                    MessageBox.Show("처리 실패", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    // 저장 처리 성공시
                    if (isNew)
                    {
                        // 신규 처리일 경우
                        lblNew.Visible = false;

                        tvShape.Nodes.Clear();
                        tvBlock.Nodes.Clear();

                        InitializeList();

                        MessageBox.Show("GIS 추가 작업 메시지", "저장", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        if (tvBlock.SelectedNodes == null || tvBlock.SelectedNodes.Count <= 0) return;
                        // 입력한 정보로 내부 블록 정보 갱신
                        BlockItem block = FindTreeBlock(tvBlock.SelectedNodes[0], true);
                        if (block.IsTemp == true)
                        {
                            // Shape에는 있지만 블록 정보에는 없는 경우의 저장 처리
                            // 처리 성공
                            block.IsMatch = true;
                            block.IsTemp = false;
                            block.LOC_CODE = txtBlockLocCode.Text;
                            block.PLOC_CODE = txtBlockPLocCode.Text;
                            block.LOC_GBN = txtBlockLocGbn.Text;
                            //block.KT_GBN = txtBlockKtGbn.Text;
                            block.KT_GBN = GetKtGbnCombo();
                            block.RES_CODE = lblBlockResCode.Text;
                            int orderBy = 0;
                            if (int.TryParse(txtBlockOrderBy.Text, out orderBy))
                                block.ORDERBY = orderBy;

                            if (chkbxTB1.Checked && chkbxTB2.Checked) block.CM_LOC_GBN = "1";
                            else if (!chkbxTB1.Checked && chkbxTB2.Checked) block.CM_LOC_GBN = "2";

                            // 입력한 정보로 TreeView의 정보 갱신
                            Infragistics.Win.UltraWinTree.UltraTreeNode nodeBlock = tvBlock.SelectedNodes[0];
                            nodeBlock.Override.NodeAppearance.Image = imageList1.Images[0];
                            nodeBlock.Override.SelectedNodeAppearance.Image = imageList1.Images[1];

                            Infragistics.Win.UltraWinTree.UltraTreeNode nodeShape = tvShape.SelectedNodes[0];
                            ShapeItem shape = FindTreeShape(nodeShape, true);
                            nodeShape.Override.NodeAppearance.Image = imageList1.Images[0];
                            nodeShape.Override.SelectedNodeAppearance.Image = imageList1.Images[1];
                            shape.IsMatch = true;

                            _lastLocCode += 1;
                            _lastOrderBy += 1;
                        }
                        else
                        {
                            // Shape와 블록 정보가 모두 있을 경우의 저장 처리

                            block.LOC_CODE = txtBlockLocCode.Text;
                            block.PLOC_CODE = txtBlockPLocCode.Text;
                            block.LOC_GBN = txtBlockLocGbn.Text;
                            //block.KT_GBN = txtBlockKtGbn.Text;
                            block.KT_GBN = GetKtGbnCombo();
                            block.RES_CODE = lblBlockResCode.Text;
                            int orderBy = 0;
                            if (int.TryParse(txtBlockOrderBy.Text, out orderBy))
                                block.ORDERBY = orderBy;

                            Infragistics.Win.UltraWinTree.UltraTreeNode nodeBlock = tvBlock.SelectedNodes[0];
                            string[] tags = string.Format("{0}", nodeBlock.Tag).Split('|');
                            string tag = txtBlockLocCode.Text;
                            for (int i = 1; i < tags.Length; i++)
                            {
                                tag += "|" + tags[i];
                            }
                            nodeBlock.Tag = tag;
                        }
                    }
                    MessageBox.Show("저장되었습니다.", "저장성공", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                FireException("btnSave_Click", ex);
                MessageBox.Show("오류가 발생했습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                isTreeEvent = false;
            }
        }
        #endregion

        #region 삭제 버튼
        private void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvBlock.SelectedNodes == null || tvBlock.SelectedNodes.Count <= 0)
                {
                    MessageBox.Show("선택된 블록이 없습니다.", "블록선택", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                BlockItem block = FindTreeBlock(tvBlock.SelectedNodes[0], true);
                if (block.IsTemp)
                {
                    MessageBox.Show("미생성 데이터입니다.", "삭제불가", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (MessageBox.Show(block.LOC_NAME + "을(를) 삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                if (Delete())
                {
                    MessageBox.Show("삭제되었습니다.", "삭제", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tvShape.Nodes.Clear();
                    tvBlock.Nodes.Clear();

                    InitializeList();
                }
                //else
                //{
                //    MessageBox.Show("삭제 작업이 실패하였습니다.", "삭제", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
            catch (Exception ex)
            {
                FireException("btnDel_Click", ex);
                MessageBox.Show("삭제 작업중 오류가 발생하였습니다.", "삭제", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                isTreeEvent = false;
            }
        }
        #endregion


        #region 닫기 버튼
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        /// <summary>
        /// 종료(닫기)
        /// </summary>
        #region 닫기 메뉴
        private void mnuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        } 
        #endregion

        

        
        
    }

    
}
