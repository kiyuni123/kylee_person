﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp
{
    public partial class FrmSelectSgccd : Form
    {
        /// <summary>
        /// DB Item List
        /// </summary>
        private List<DBItem> _itemList = new List<DBItem>();

        public string SGCCD { get; set; }
        public string ZONE { get; set; }
        public string PATH { get; set; }

        /// <summary>
        /// 생성자
        /// </summary>
        public FrmSelectSgccd()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Form Load Event
        /// </summary>
        private void FrmSelectSgccd_Load(object sender, EventArgs e)
        {
            LoadData("지방");

            cbZone.SelectedIndex = 0;
            cbZone.SelectedIndexChanged += new EventHandler(cbZone_SelectedIndexChanged);
        }

        /// <summary>
        /// 목록 조회
        /// </summary>
        /// <param name="zone"></param>
        private void LoadData(string zone)
        {
            try
            {
                EMFrame.dm.EMapper mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY_MAIN);
                DataTable dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.MAIN_SELECT, zone), null);

                if (dt == null || dt.Rows.Count <= 0)
                {
                    MessageBox.Show("등록된 지역이 없습니다.", "지역 없음", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                listBox1.Items.Clear();
                _itemList.Clear();

                // DB 접속 연결 문자열
                string conStr = "Data Source=(DESCRIPTION="
                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1})))"
                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={2})));"
                + "User Id={3};Password={4};Enlist=false";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    string mgdpcd = string.Format("{0}", dr[Database.Fields.DEPARTMENT.MGDPCD]);
                    string mgdpnm = string.Format("{0}", dr[Database.Fields.DEPARTMENT.MGDPNM]);
                    string title = string.Format("{0}", dr[Database.Fields.DEPARTMENT.TITLE]);
                    string sid = string.Format("{0}", dr[Database.Fields.DEPARTMENT.DB_SID]);
                    string address = string.Format("{0}", dr[Database.Fields.DEPARTMENT.DB_ADDRESS]);
                    string port = string.Format("{0}", dr[Database.Fields.DEPARTMENT.DB_PORT]);
                    string id = string.Format("{0}", dr[Database.Fields.DEPARTMENT.DB_ID]);
                    string pass = string.Format("{0}", dr[Database.Fields.DEPARTMENT.DB_PASS]);
                    string dataPath = string.Format("{0}", dr[Database.Fields.DEPARTMENT.DATA_PATH]);

                    listBox1.Items.Add(title);

                    // 연결 문자열 등록
                    if (EMFrame.dm.EMapper.ConnectionString.ContainsKey(mgdpcd))
                        EMFrame.dm.EMapper.ConnectionString[mgdpcd] = string.Format(conStr, address, port, sid, id, pass);
                    else
                        EMFrame.dm.EMapper.ConnectionString.Add(mgdpcd, string.Format(conStr, address, port, sid, id, pass));

                    // DB 정보를 목록으로 기억
                    DBItem item = new DBItem();
                    item.MGDPCD = mgdpcd;
                    item.MGDPNM = mgdpnm;
                    item.TITLE = title;
                    item.SID = sid;
                    item.ADDRESS = address;
                    item.PORT = port;
                    item.ID = id;
                    item.PASSWD = pass;
                    item.DATA_PATH = dataPath;
                    _itemList.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Zone 변경 이벤트
        /// </summary>
        private void cbZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            // DB 접속 정보 조회 및 목록 생성
            LoadData(cbZone.Text);
        }

        /// <summary>
        /// DB 아이템 선택 이벤트
        /// </summary>
        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SelectZone();
        }

        /// <summary>
        /// 선택 버튼 이벤트
        /// </summary>
        private void btnSelect_Click(object sender, EventArgs e)
        {
            SelectZone();
        }

        private void SelectZone()
        {
            int selected = listBox1.SelectedIndex;
            Program.CONNECTION_KEY = _itemList[selected].MGDPCD;
            Program.SID = _itemList[selected].SID.ToUpper();
            this.ZONE = listBox1.Text;
            this.SGCCD = _itemList[selected].MGDPCD;
            this.PATH = _itemList[selected].DATA_PATH;// +"\\" + _itemList[selected].MGDPCD;

            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// 닫기 버튼 이벤트
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }

    /// <summary>
    /// DB 아이템 정보
    /// </summary>
    #region public class DBItem
    public class DBItem
    {
        public string MGDPCD { get; set; }
        public string MGDPNM { get; set; }
        public string TITLE { get; set; }
        public string SID { get; set; }
        public string ADDRESS { get; set; }
        public string PORT { get; set; }
        public string ID { get; set; }
        public string PASSWD { get; set; }
        public string DATA_PATH { get; set; }
    } 
    #endregion

}
