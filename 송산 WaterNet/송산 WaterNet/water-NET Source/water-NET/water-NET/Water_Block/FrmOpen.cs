﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp
{
    public partial class FrmOpen : Form
    {
        private string _path = "";
        public string PATH { get { return _path; } }
        public string SGCCD { get; set; }
        public string ZONE { get; set; }
        private FolderBrowserDialog fd = null;
       

        public FrmOpen()
        {
            InitializeComponent();
        }

        private void FrmOpen_Load(object sender, EventArgs e)
        {
            fd = new FolderBrowserDialog();
            
            if (EMFrame.statics.AppStatic.DATA_DIR != null)
            {
                // 이미 선택되어 있는 폴더가 있을 경우 설정
                if (System.IO.Directory.Exists(EMFrame.statics.AppStatic.DATA_DIR))
                {
                    _path = EMFrame.statics.AppStatic.DATA_DIR;                    
                }
            }
            else
            {
                // 선택되어 있는 폴더 없을 경우 기본 폴더 설정
                if (System.IO.Directory.Exists(Application.StartupPath + @"\WaterNET-Data"))
                {
                    _path = Application.StartupPath + @"\WaterNET-Data";
                }
            }
            fd.SelectedPath = _path;
            string[] paths = _path.Split('\\');
            lblPath.Text = paths[paths.Length - 1];

            string[] dirs = System.IO.Directory.GetDirectories(_path);
            if (dirs != null && dirs.Length > 0)
            {
                for (int i = 0; i < dirs.Length; i++)
                {
                    string[] sgccd = dirs[i].Split('\\');
                    lvList.Items.Add(sgccd[sgccd.Length - 1]);
                }
            }
        }

        private void btnFolder_Click(object sender, EventArgs e)
        {
            try
            {
                if (fd.ShowDialog(this) != DialogResult.OK) return;

                _path = fd.SelectedPath;
                
                string[] paths = _path.Split('\\');
                lblPath.Text = paths[paths.Length - 1];
                lvList.Items.Clear();

                string[] dirs = System.IO.Directory.GetDirectories(_path);
                if (dirs != null && dirs.Length > 0)
                {
                    for (int i = 0; i < dirs.Length; i++)
                    {
                        string[] sgccd = dirs[i].Split('\\');
                        lvList.Items.Add(sgccd[sgccd.Length - 1]);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (lvList.SelectedItems == null && lvList.SelectedItems.Count <= 0) return;

                Program.CONNECTION_KEY = lvList.SelectedItems[0].Text;
                this.SGCCD = lvList.SelectedItems[0].Text;
                this.ZONE = "지방";
                
                string svcName = "waternet";
                string ip = "192.168.40.121";
                string id = "WN_MANAGER";
                string passwd = "1234";
                string port = "1521";

                string conStr = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1})))"
                    + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={2})));"
                    + "User Id={3};Password={4};Enlist=false";
                
                if (EMFrame.dm.EMapper.ConnectionString.ContainsKey(Program.CONNECTION_KEY))
                {
                    EMFrame.dm.EMapper.ConnectionString[Program.CONNECTION_KEY] = string.Format(conStr, ip, port, svcName, id, passwd);
                }
                else
                {
                    EMFrame.dm.EMapper.ConnectionString.Add(Program.CONNECTION_KEY, string.Format(conStr, ip, port, svcName, id, passwd));
                }

                //EMFrame.statics.AppStatic.DATA_DIR = _path;
                //EMFrame.statics.AppStatic.USER_ID = "에너지";

                //EMFrame.statics.AppStatic.ZONE_GBN = "지방";
                //EMFrame.statics.AppStatic.USER_SGCCD = lvList.SelectedItems[0].Text;
                
                //string datadir = EMFrame.statics.AppStatic.DATA_DIR + System.IO.Path.DirectorySeparatorChar + EMFrame.statics.AppStatic.USER_SGCCD;
                //WaterNet.WaterAOCore.VariableManager.m_Topographic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Topographic");
                //WaterNet.WaterAOCore.VariableManager.m_Pipegraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Pipegraphic");
                //WaterNet.WaterAOCore.VariableManager.m_INPgraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic");
                //WaterNet.WaterAOCore.VariableManager.m_INPgraphicRootDirectory = datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic";
                
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        private TimeSpan ts = new TimeSpan();
        
        private void lvList_MouseClick(object sender, MouseEventArgs e)
        {
            if (ts == null) ts = new TimeSpan(DateTime.Now.Ticks);

            TimeSpan tsNow = new TimeSpan(DateTime.Now.Ticks);
            TimeSpan subs = tsNow.Subtract(ts);
            ts = tsNow;

            if (subs.TotalMilliseconds <= 2000) btnSelect_Click(this, null);
        }

        
    }
}
