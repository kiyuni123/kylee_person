﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp
{
    public class ShapeItem
    {
        /// <summary>
        /// FTR_CODE
        /// </summary>
        public string FTR_CODE { get; set; }
        /// <summary>
        /// FTR_IDN
        /// </summary>
        public string FTR_IDN { get; set; }
        /// <summary>
        /// 상위 FTR_CODE
        /// </summary>
        public string PFTR_CODE { get; set; }
        /// <summary>
        /// 상위 FTR_IDN
        /// </summary>
        public string PFTR_IDN { get; set; }

        /// <summary>
        /// 블록명
        /// </summary>
        public string LOC_NAME { get; set; }
        /// <summary>
        /// 관련지역명
        /// </summary>
        public string REL_LOC_NAME { get; set; }
        
        /// <summary>
        /// 지자체코드
        /// </summary>
        public string SGCCD { get; set; }
        /// <summary>
        /// 블록 구분
        /// </summary>
        public string BLK_GBN { get; set; }

        public List<ShapeItem> Child { get; set; }

        public bool IsMatch { get; set; }

        /// <summary>
        /// Block Item
        /// </summary>
        public BlockItem Block { get; set; }
    }
}
