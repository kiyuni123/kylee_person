﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace WaterNet.BlockApp.ExcelUpload
{
    public partial class FrmRead : Form
    {
        /// <summary>
        /// 읽어들인 엑셀을 표현할 사용자 정의 컨트롤
        /// </summary>
        private Controls.ucExcel ucExcel = null;
        /// <summary>
        /// 테이블
        /// </summary>
        private DataTable _dtTables = null;

        #region public FrmRead()
        public FrmRead()
        {
            InitializeComponent();
        } 
        #endregion

        /// <summary>
        /// 오류처리
        /// </summary>
        /// <param name="method">오류 메서드명</param>
        /// <param name="ex">오류내용</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion

        /// <summary>
        /// Form Load Event
        /// </summary>
        #region private void FrmRead_Load(object sender, EventArgs e)
        private void FrmRead_Load(object sender, EventArgs e)
        {
            try
            {
                // 테이블 목록 조회
                SelectTables();

                // 엑셀 컨트롤 생성
                this.ucExcel = new Controls.ucExcel(_dtTables);
                ucExcel.Dock = DockStyle.Fill;
                this.pnlExcel.Controls.Add(ucExcel);

                btnOpen_Click(this, null);
            }
            catch (Exception ex)
            {
                FireException("FrmRead.FrmRead_Load", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// 모든 테이블 조회
        /// </summary>
        #region private void SelectTables()
        private void SelectTables()
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                _dtTables = mapper.ExecuteScriptDataTable(Database.Queries.EXCEL_ALL_TABLES, null);
            }
            catch (Exception ex)
            {
                FireException("FrmRead.SelectTables", ex);
                throw ex;
            }
            finally
            {
                if (mapper != null) mapper.Dispose();
            }
        } 
        #endregion

        /// <summary>
        /// 엑셀 열기 버튼 
        /// </summary>
        #region private void btnOpen_Click(object sender, EventArgs e)
        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                // Excel Open
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.InitialDirectory = Application.StartupPath;
                ofd.Filter = "All files (*.*)|*.*|Excel (*.xls)|*.xls|Excel (*.xlsx)|*.xlsx";
                if (ofd.ShowDialog(this) != DialogResult.OK) return;

                // Excel을 담을 DataSet
                DataSet ds = new DataSet();
                ucExcel.Clear();

                // 선택된 Excep Open & Create object
                using (SpreadsheetDocument sl = SpreadsheetDocument.Open(ofd.FileName, false))
                {
                    // 엑셀의 모든 시트 가져오기
                    var sheets = sl.WorkbookPart.Workbook.Descendants<Sheet>();
                    // 엑셀의 시트별로 데이터 처리
                    foreach (Sheet sheet in sheets)
                    {
                        // 엑셀 시트의 데이터를 담을 DataTable 생성
                        DataTable dt = ds.Tables.Add();

                        // Table명 설정 (Sheet Name)
                        foreach (OpenXmlAttribute attr in sheet.GetAttributes())
                        {
                            if (attr.LocalName.ToUpper() == "NAME")
                            {
                                dt.TableName = attr.Value;
                                break;
                            }
                        }

                        // Cell Value 설정 시작
                        WorksheetPart worksheetPart = (WorksheetPart)sl.WorkbookPart.GetPartById(sheet.Id);
                        // 시트 내의 모든 데이터 가져오기
                        SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                        // 셀에 데이터가 있는게 아니라 별도의 위치에 데이터가 있으므로,
                        // 셀과 매핑하여 실제 데이터를 가져오기 위한 객체를 가져온다.
                        var sharedStringPart = sl.WorkbookPart.SharedStringTablePart;
                        var values = sharedStringPart.SharedStringTable.Elements<SharedStringItem>().ToArray();

                        // 엑셀 정보를 DataTable에 설정
                        int rowCnt = 0;
                        int colCnt = 0;
                        foreach (Row row in sheetData.Elements<Row>())
                        {
                            if (rowCnt == 0)
                            {
                                // 첫번째 Row는 Header로 간주
                                colCnt = 0;
                                foreach (Cell cell in row.Elements<Cell>())
                                {
                                    try
                                    {
                                        string header = "";
                                        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                                            header = values[int.Parse(cell.CellValue.Text)].InnerText;
                                        else if (cell.CellValue != null)
                                            header = cell.CellValue.Text;
                                        else
                                            header = colCnt.ToString();

                                        // 셀에 데이터 없을 경우 임의 컬럼명 생성
                                        if (dt.Columns.Contains(header)) header = "Col_" + colCnt.ToString();
                                        dt.Columns.Add(new DataColumn(header, typeof(string)));
                                    }
                                    catch (Exception ex1)
                                    {
                                        // Debug
                                        Console.WriteLine(string.Format("[{0}]ex1 : ({1}){2}", dt.TableName, colCnt, ex1.Message));
                                    }
                                    ++colCnt;
                                }
                                ++rowCnt;
                                continue;
                            }
                            // 읽어들인 셀값을 Table의 Row에 설정
                            DataRow dr = dt.NewRow();
                            colCnt = 0;
                            // 데이터가 0일 경우 값이 없는 Row이므로 추가하지 않는다.
                            int checkData = 0;
                            foreach (Cell cell in row.Elements<Cell>())
                            {
                                try
                                {
                                    colCnt = GetColumn(cell.CellReference.Value);
                                    if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                                    {
                                        dr[colCnt] = values[int.Parse(cell.CellValue.Text)].InnerText;
                                        ++checkData;
                                    }
                                    else if (cell.CellValue != null)
                                    {
                                        dr[colCnt] = cell.CellValue.Text;
                                        ++checkData;
                                    }
                                    else
                                    {
                                        dr[colCnt] = "";
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    Console.WriteLine(string.Format("[{0}]ex2 : (r{1}/c{2}){3}", dt.TableName, rowCnt, colCnt, ex2.Message));
                                }
                                ++colCnt;
                            }
                            if (checkData > 0) dt.Rows.Add(dr);
                            ++rowCnt;
                        }
                    }
                }

                if (ds != null && ds.Tables.Count > 0) ucExcel.SetData(ds);
            }
            catch (Exception ex)
            {
                FireException("FrmRead.btnOpen_Click", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// [미사용] Sheet명 가져오기
        /// </summary>
        /// <param name="sl">엑셀 객체</param>
        /// <returns>Sheet명</returns>
        #region private string[] GetSheetNames(SpreadsheetDocument sl)
        private string[] GetSheetNames(SpreadsheetDocument sl)
        {
            string[] names = null;

            Sheets sheets = sl.WorkbookPart.Workbook.Sheets;
            names = new string[sheets.Count()];

            int cnt = 0;
            foreach (OpenXmlElement sheet in sheets)
            {
                foreach (OpenXmlAttribute attr in sheet.GetAttributes())
                {
                    if (attr.LocalName == "name")
                    {
                        names[cnt] = attr.Value;
                        ++cnt;
                    }
                }
            }

            return names;
        } 
        #endregion

        /// <summary>
        /// Cell명칭으로 Column Index 찾기
        /// </summary>
        /// <param name="colName">CellName</param>
        /// <returns>Column Index</returns>
        #region private int GetColumn(string colName)
        private int GetColumn(string colName)
        {
            int retVal = 0;

            try
            {
                string colNo = "";
                System.Text.RegularExpressions.Regex engRegex = new System.Text.RegularExpressions.Regex(@"[a-zA-Z]");
                Boolean isMatch = true;

                for (int i = 0; i < colName.Length; i++)
                {
                    string ch = colName.Substring(i, 1);
                    isMatch = engRegex.IsMatch(ch);

                    if (!isMatch)
                    {
                        char[] chars = colName.Substring(0, i).ToCharArray();
                        for (int j = 0; j < chars.Length; j++)
                        {
                            // A:65 / Z:90
                            // a:97 / z:122
                            int tmp = (int)chars[j];
                            if (tmp >= 65 && tmp <= 90)
                                colNo += string.Format("{0}", tmp - 65);
                            else if (tmp >= 97 && tmp <= 122)
                                colNo += string.Format("{0}", tmp - 97);
                        }
                        break;
                    }
                }
                if (colName.Length == 1 && isMatch)
                {
                    char[] chars = colName.ToCharArray();
                    int tmp = (int)chars[0];
                    if (tmp >= 65 && tmp <= 90)
                        colNo += string.Format("{0}", tmp - 65);
                    else if (tmp >= 97 && tmp <= 122)
                        colNo += string.Format("{0}", tmp - 97);
                }
                retVal = Convert.ToInt32(colNo);
            }
            catch (Exception ex)
            {
                FireException("FrmRead.GetColumn", ex);
                throw ex;
            }
            return retVal;
        } 
        #endregion



        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
