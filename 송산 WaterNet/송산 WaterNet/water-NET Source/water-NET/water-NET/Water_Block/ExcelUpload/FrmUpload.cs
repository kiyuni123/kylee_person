﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp.ExcelUpload
{
    public partial class FrmUpload : Form
    {
        /// <summary>
        /// 처리할 쿼리문
        /// </summary>
        private List<string> _queries = null;
        private List<bool> _result = null;
        public List<bool> Result { get { return _result; } }

        // 처리중 발생되는 이벤트 처리용 대리자
        private delegate void ProgressInvokeDelegate();
        /// <summary>
        /// 처리중 발생되는 오류 표현용 대리자
        /// </summary>
        /// <param name="ex"></param>
        private delegate void ExceptionDelegate(Exception ex);

        #region public FrmUpload()
        public FrmUpload()
        {
            InitializeComponent();
        } 
        #endregion

        #region public FrmUpload(List<string> queries)
        public FrmUpload(List<string> queries)
            : this()
        {
            this._queries = queries;
        } 
        #endregion

        #region private void FrmUpload_Load(object sender, EventArgs e)
        private void FrmUpload_Load(object sender, EventArgs e)
        {
            lblTotal.Text = _queries.Count.ToString();
            lblSuccess.Text = "0";
            lblFail.Text = "0";

            progressBar1.Maximum = _queries.Count;

            // 처리용 스레드 생성 및 시작
            System.Threading.Thread thProc = new System.Threading.Thread(
                new System.Threading.ThreadStart(this.Execute));
            thProc.Start();
        } 
        #endregion

        /// <summary>
        /// 닫기 버튼
        /// </summary>
        #region private void btnClose_Click(object sender, EventArgs e)
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        } 
        #endregion

        /// <summary>
        /// 스레드 메서드
        /// </summary>
        #region private void Execute()
        private void Execute()
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);

                _result = new List<bool>();
                //mapper.BeginTransaction();

                for (int i = 0; i < _queries.Count; i++)
                {
                    try
                    {
                        Console.WriteLine(_queries[i]);
                        mapper.ExecuteScript(_queries[i], null);
                        progressBar1.Invoke(new ProgressInvokeDelegate(Success));
                        _result.Add(true);
                        System.Threading.Thread.Sleep(1);
                    }
                    catch (Exception ex1)
                    {
                        _result.Add(false);
                        Console.WriteLine(ex1.Message);
                        progressBar1.Invoke(new ProgressInvokeDelegate(Fail));
                    }
                    System.Threading.Thread.Sleep(1);
                }
                btnClose.Invoke(new ProgressInvokeDelegate(Complete));
            }
            catch (Exception ex)
            {
                this.Invoke(new ExceptionDelegate(FireException), new object[] { ex });
            }
            finally
            {
                //mapper.RollbackTransaction();
                if (mapper != null) mapper.Dispose();
            }
        } 
        #endregion

        /// <summary>
        /// 성공 표시
        /// </summary>
        private void Success()
        {
            if (progressBar1.Value < progressBar1.Maximum) progressBar1.Value += 1;
            int success = Convert.ToInt32(lblSuccess.Text);
            success += 1;
            lblSuccess.Text = success.ToString();
        }

        /// <summary>
        /// 실패 표시
        /// </summary>
        private void Fail()
        {
            if (progressBar1.Value < progressBar1.Maximum) progressBar1.Value += 1;
            int fail = Convert.ToInt32(lblFail.Text);
            fail += 1;
            lblFail.Text = fail.ToString();
        }

        /// <summary>
        /// 완료 표시
        /// </summary>
        private void Complete()
        {
            btnClose.Enabled = true;
            MessageBox.Show("완료되었습니다.", "완료", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        /// <summary>
        /// 오류 표시
        /// </summary>
        /// <param name="ex">Exception</param>
        private void FireException(Exception ex)
        {
            MessageBox.Show(ex.Message);
            btnClose.Enabled = true;
        }
    }
}
