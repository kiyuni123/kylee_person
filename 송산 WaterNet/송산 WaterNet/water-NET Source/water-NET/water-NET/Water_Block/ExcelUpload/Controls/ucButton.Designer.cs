﻿namespace WaterNet.BlockApp.ExcelUpload.Controls
{
    partial class ucButton
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMargin = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pnlMargin
            // 
            this.pnlMargin.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMargin.Location = new System.Drawing.Point(0, 0);
            this.pnlMargin.Name = "pnlMargin";
            this.pnlMargin.Size = new System.Drawing.Size(10, 31);
            this.pnlMargin.TabIndex = 0;
            this.pnlMargin.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.pnlMargin.Click += new System.EventHandler(this.Button_Click);
            this.pnlMargin.MouseHover += new System.EventHandler(this.Button_MouseHover);
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.SystemColors.Control;
            this.lblName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblName.Location = new System.Drawing.Point(10, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(93, 31);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Sheet1";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblName.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.lblName.Click += new System.EventHandler(this.Button_Click);
            this.lblName.MouseHover += new System.EventHandler(this.Button_MouseHover);
            // 
            // ucButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.pnlMargin);
            this.Name = "ucButton";
            this.Size = new System.Drawing.Size(103, 31);
            this.Load += new System.EventHandler(this.ucTabButton_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMargin;
        private System.Windows.Forms.Label lblName;
    }
}
