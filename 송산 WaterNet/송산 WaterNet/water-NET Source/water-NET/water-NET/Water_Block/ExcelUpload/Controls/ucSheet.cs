﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp.ExcelUpload.Controls
{
    public partial class ucSheet : UserControl
    {
        /// <summary>
        /// Excel Sheet Data
        /// </summary>
        private DataTable _dtData = null;
        /// <summary>
        /// DB Table List
        /// </summary>
        private DataTable _dtTable = null;
        /// <summary>
        /// Table Column Info List
        /// </summary>
        private List<ColumnItem> _columnList = null;
        
        #region public ucSheet()
        public ucSheet()
        {
            InitializeComponent();
        } 
        #endregion

        #region public ucSheet(DataTable dtData, DataTable dtTable)
        public ucSheet(DataTable dtData, DataTable dtTable)
            : this()
        {
            this._dtData = dtData;
            this._dtTable = dtTable;
        } 
        #endregion

        /// <summary>
        /// 오류처리
        /// </summary>
        /// <param name="method">오류 메서드명</param>
        /// <param name="ex">오류내용</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion

        /// <summary>
        /// Form Load Event
        /// </summary>
        #region private void ucSheet_Load(object sender, EventArgs e)
        private void ucSheet_Load(object sender, EventArgs e)
        {
            try
            {   
                if (_dtData == null) return;

                // 엑셀 시트의 데이터를 엑셀 그리드에 바인딩
                this.dgvExcel.DataSource = _dtData;
                this.dgvExcel.DisplayLayout.Override.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Left;
                this.dgvExcel.DisplayLayout.Override.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                DataTable dt = new DataTable();
                // 컬럼 생성 & 속성 설정
                for (int i = 0; i < this.dgvExcel.DisplayLayout.Bands[0].Columns.Count; i++)
                {
                    // Column Header
                    dt.Columns.Add(new DataColumn(this.dgvExcel.DisplayLayout.Bands[0].Columns[i].Header.Caption, typeof(string)));
                    // Properties
                    this.dgvExcel.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.dgvExcel.DisplayLayout.Bands[0].Columns[i].CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
                    this.dgvExcel.DisplayLayout.Bands[0].Columns[i].CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                }

                // 데이터 생성
                DataRow dr = dt.NewRow();
                for (int i = 0; i < this.dgvExcel.DisplayLayout.Bands[0].Columns.Count; i++)
                {
                    dr[i] = "";
                }
                dt.Rows.Add(dr);
                this.dgvMapping.DataSource = dt;

                // 테이블 설정
                cbTables.DisplayMember = Database.Fields.EXCEL.TABLE_NAME;
                cbTables.ValueMember = Database.Fields.EXCEL.TABLE_NAME;
                cbTables.DataSource = _dtTable;
            }
            catch (Exception ex)
            {
                FireException("ucSheet.ucSheet_Load", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// [버림 - 권한 문제]
        /// 테이블 변경 처리 (컬럼명 설정)
        /// </summary>
        /// <param name="tableName">테이블명</param>
        #region [버림 - 권한 문제] private void ChangeTable1(string tableName)
        //private void ChangeTable1(string tableName)
        //{
        //    EMFrame.dm.EMapper mapper = null;

        //    try
        //    {
        //        this.dgvMapping.DisplayLayout.ValueLists.Clear();
        //        mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
        //        DataTable dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.EXCEL_ALL_COLUMNS, Program.SID, tableName), null);

        //        if (dt == null || dt.Rows.Count <= 0) return;
        //        if (_columnList == null) _columnList = new List<ColumnItem>();
        //        _columnList.Clear();
                
        //        Infragistics.Win.ValueList valueList = new Infragistics.Win.ValueList();
        //        Infragistics.Win.ValueListItem valueListItem0 = new Infragistics.Win.ValueListItem();
        //        valueListItem0.DataValue = "";
        //        valueListItem0.DisplayText = "";
        //        valueList.ValueListItems.Add(valueListItem0);

        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            DataRow dr = dt.Rows[i];
        //            string columnName = string.Format("{0}", dr[Database.Fields.EXCEL.COLUMN_NAME]);
        //            string dataType = string.Format("{0}", dr[Database.Fields.EXCEL.DATA_TYPE]);
        //            string dataLength = string.Format("{0}", dr[Database.Fields.EXCEL.DATA_LENGTH]);
        //            string nullable = string.Format("{0}", dr[Database.Fields.EXCEL.NULLABLE]);

        //            ColumnItem item = new ColumnItem(columnName, dataType, dataLength, nullable);
        //            _columnList.Add(item);

        //            Infragistics.Win.ValueListItem valueListItem = new Infragistics.Win.ValueListItem();
        //            valueListItem.DataValue = columnName;
        //            valueListItem.DisplayText = columnName;
        //            valueList.ValueListItems.Add(valueListItem);
        //        }

        //        // ValueList 설정
        //        this.dgvMapping.DisplayLayout.ValueLists.Add(valueList);

        //        // 컬럼 속성 변경 (DropDownList)
        //        for (int i = 0; i < this.dgvMapping.DisplayLayout.Bands[0].Columns.Count; i++)
        //        {
        //            this.dgvMapping.DisplayLayout.Bands[0].Columns[i].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
        //            this.dgvMapping.DisplayLayout.Bands[0].Columns[i].ValueList = valueList;
        //            this.dgvMapping.DisplayLayout.Bands[0].Columns[i].CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

        //            this.dgvMapping.Rows[0].Cells[i].Value = "";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        FireException("ucSheet.ChangeTable", ex);
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        if (mapper != null) mapper.Dispose();
        //    }
        //} 
        #endregion

        /// <summary>
        /// 테이블 변경 처리 (컬럼명 설정)
        /// </summary>
        /// <param name="tableName">테이블명</param>
        #region private void ChangeTable(string tableName)
        private void ChangeTable(string tableName)
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                this.dgvMapping.DisplayLayout.ValueLists.Clear();
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                DataTable dt = mapper.ExecuteScriptDataTable(string.Format("SELECT * FROM {0} WHERE ROWNUM = 1", tableName), null);

                if (dt == null) return;
                if (_columnList == null) _columnList = new List<ColumnItem>();
                _columnList.Clear();

                Infragistics.Win.ValueList valueList = new Infragistics.Win.ValueList();
                Infragistics.Win.ValueListItem valueListItem0 = new Infragistics.Win.ValueListItem();
                valueListItem0.DataValue = "";
                valueListItem0.DisplayText = "";
                valueList.ValueListItems.Add(valueListItem0);

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    string columnName = dt.Columns[i].ColumnName;
                    string dataType = string.Format("{0}", dt.Columns[i].DataType);
                    string dataLength = string.Format("{0}", dt.Columns[i].MaxLength);
                    string nullable = "N";
                    if (dt.Columns[i].AllowDBNull == true) nullable = "Y";

                    ColumnItem item = new ColumnItem(columnName, dataType, dataLength, nullable);
                    _columnList.Add(item);

                    Infragistics.Win.ValueListItem valueListItem = new Infragistics.Win.ValueListItem();
                    valueListItem.DataValue = columnName;
                    valueListItem.DisplayText = columnName;
                    valueList.ValueListItems.Add(valueListItem);
                }

                // ValueList 설정
                this.dgvMapping.DisplayLayout.ValueLists.Add(valueList);

                // 컬럼 속성 변경 (DropDownList)
                for (int i = 0; i < this.dgvMapping.DisplayLayout.Bands[0].Columns.Count; i++)
                {
                    this.dgvMapping.DisplayLayout.Bands[0].Columns[i].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                    this.dgvMapping.DisplayLayout.Bands[0].Columns[i].ValueList = valueList;
                    this.dgvMapping.DisplayLayout.Bands[0].Columns[i].CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    this.dgvMapping.Rows[0].Cells[i].Value = "";
                }
            }
            catch (Exception ex)
            {
                FireException("ucSheet.ChangeTable", ex);
                MessageBox.Show("ucSheet.ChangeTable -> \r\n" + ex.Message);
            }
            finally
            {
                if (mapper != null) mapper.Dispose();
            }
        } 
        #endregion

        /// <summary>
        /// 테이블 선택 변경 이벤트
        /// </summary>
        #region private void cbTables_SelectedIndexChanged(object sender, EventArgs e)
        private void cbTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeTable(cbTables.Text);
        } 
        #endregion

        /// <summary>
        /// 컬럼명 선택 이벤트 (중복 체크)
        /// </summary>
        #region private void dgvMapping_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        private void dgvMapping_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                string selected = string.Format("{0}", e.Cell.Text);
                for (int i = 0; i < dgvMapping.DisplayLayout.Bands[0].Columns.Count; i++)
                {
                    if (i == e.Cell.Column.Index) continue;
                    string val = string.Format("{0}", dgvMapping.Rows[0].Cells[i].Text);
                    if (selected == val)
                    {
                        MessageBox.Show("'" + selected + "' 컬럼은 이미 선택되었습니다.", "선택", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        e.Cell.Value = "";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                FireException("ucSheet.dgvMapping_CellChange", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// 처리 버튼
        /// 처리할 대상의 쿼리문을 미리 만들어서 FrmUpload에서 처리토록 한다
        /// </summary>
        #region private void btnExec_Click(object sender, EventArgs e)
        private void btnExec_Click(object sender, EventArgs e)
        {
            try
            {
                // 쿼리 작성용
                string insertQuery = "";
                string valuesQuery = "";
                
                // 처리 대상 컬럼 정보
                List<ColumnItem> colInfos = new List<ColumnItem>();

                // 처리 대상 INSERT 구문 생성
                for (int i = 0; i < dgvMapping.DisplayLayout.Bands[0].Columns.Count; i++)
                {
                    string col = string.Format("{0}", dgvMapping.Rows[0].Cells[i].Value);
                    if (col == "" || col.Length <= 0) continue;

                    insertQuery += col + ",";
                    ColumnItem colInfo = _columnList.Find(item => item.COLUMN_NAME == col);
                    if (colInfo != null) colInfo.INDEX = i;
                    colInfos.Add(colInfo);
                }
                if (insertQuery.Length <= 0)
                {
                    MessageBox.Show("설정후 처리가 가능합니다.", "저장", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                insertQuery = "INSERT INTO " + cbTables.Text + " (" + insertQuery.Substring(0, insertQuery.Length - 1) + ") ";

                // 처리 대상 VALUES 구문 생성
                List<string> queries = new List<string>();
                for (int i = 0; i < dgvExcel.Rows.Count; i++)
                {
                    valuesQuery = "";
                    for (int j = 0; j < colInfos.Count; j++)
                    {
                        string val = string.Format("{0}", dgvExcel.Rows[i].Cells[colInfos[j].INDEX].Value);
                        if (val == "" || val.Length <= 0)
                        {
                            valuesQuery += "NULL,";
                        }
                        else
                        {
                            if (colInfos[j].DATA_TYPE == "NUMBER")
                                valuesQuery += val + ",";
                            else
                                valuesQuery += "'" + val + "',";
                        }
                    }
                    if (valuesQuery.Length <= 0) continue;
                    valuesQuery = "VALUES (" + valuesQuery.Substring(0, valuesQuery.Length - 1) + ") ";
                    queries.Add(insertQuery + valuesQuery);
                }
                
                FrmUpload frmUpload = new FrmUpload(queries);
                frmUpload.ShowDialog(this);

                // 처리후 표현
                List<bool> result = frmUpload.Result;
                if (result == null || result.Count <= 0) return;

                // 실패건수 없을 경우 종료
                if (result.FindIndex(item => item == false) < 0) return;

                // 실패 건수 출력
                FrmResult frmResult = new FrmResult(result, this.dgvExcel.DataSource as DataTable);
                frmResult.Show(this);
            }
            catch (Exception ex)
            {
                FireException("ucSheet.btnExec_Click", ex);
                MessageBox.Show(ex.Message);
            }
            
        } 
        #endregion




        /// <summary>
        /// DataTAble Column Item
        /// </summary>
        #region private class ColumnItem
        private class ColumnItem
        {
            public string COLUMN_NAME { get; set; }
            public string DATA_TYPE { get; set; }
            public float DATA_LENGTH { get; set; }
            public string NULLABLE { get; set; }

            /// <summary>
            /// 저장 처리시 사용될 그리드 컬럼 인덱스
            /// </summary>
            public int INDEX { get; set; }

            public ColumnItem()
            {
            }

            public ColumnItem(string columnName, string dataType, string dataLength, string nullable)
            {
                this.COLUMN_NAME = columnName;
                this.DATA_TYPE = dataType;
                float len = 0F;
                if (float.TryParse(dataLength, out len)) this.DATA_LENGTH = len;
                this.NULLABLE = nullable;
            }
        } 
        #endregion


        


    }
}
