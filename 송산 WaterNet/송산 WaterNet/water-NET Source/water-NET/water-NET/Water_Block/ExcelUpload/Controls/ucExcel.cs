﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp.ExcelUpload.Controls
{
    public partial class ucExcel : UserControl
    {
        private DataSet _ds = null;
        private DataTable _dtTable = null;
        private ucSheet[] _sheets = null;
        private ucButton[] _buttons = null;

        #region public ucExcel()
        public ucExcel()
        {
            InitializeComponent();
        } 
        #endregion
        
        #region public ucExcel(DataTable dtTable)
        public ucExcel(DataTable dtTable) 
            : this()
        {
            this._dtTable = dtTable;
        } 
        #endregion

        /// <summary>
        /// 오류처리
        /// </summary>
        /// <param name="method">오류 메서드명</param>
        /// <param name="ex">오류내용</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion

        /// <summary>
        /// 읽어들인 엑셀 데이터 반영
        /// </summary>
        /// <param name="ds">엑셀 데이터</param>
        #region public void SetData(DataSet ds)
        public void SetData(DataSet ds)
        {
            try
            {
                if (ds == null || ds.Tables.Count <= 0) return;
                this._ds = ds;

                _sheets = new ucSheet[ds.Tables.Count];
                _buttons = new ucButton[ds.Tables.Count];

                for (int i = _ds.Tables.Count - 1; i >= 0; i--)
                //for (int i=0; i<_ds.Tables.Count; i++)
                {
                    // WorkSheet DataTable
                    DataTable dt = _ds.Tables[i];
                    // WorkSheet Control Create
                    ucSheet sheet = new ucSheet(dt, _dtTable);
                    // Tab Button Control Create
                    //ucButton button = new ucButton(string.Format("Sheet{0}", i + 1));
                    ucButton button = new ucButton(dt.TableName);

                    // Sheet 컨트롤 설정
                    sheet.Dock = DockStyle.Fill;
                    _sheets[i] = sheet;
                    // Button 컨트롤 설정
                    button.OnSelected += new ButtonSelected(this.OnSheetSelected);
                    _buttons[i] = button;

                    // 버튼 컨트롤은 화면에 미리 설정
                    button.Dock = DockStyle.Left;
                    button.Size = new Size(button.ButtonWidth, 10);
                    pnlButton.Controls.Add(button);
                    // 버튼 간격 조정용 패널 삽입
                    Panel pnl = new Panel();
                    pnl.Dock = DockStyle.Left;
                    pnl.Size = new Size(5, 5);
                    pnlButton.Controls.Add(pnl);
                }
                // Sheet1 선택으로 표시
                int first = 0;// _buttons.Length - 1;
                OnSheetSelected(_buttons[0], new ButtonSelectedEventArgs(_buttons[first], _buttons[first].ButtonName, true));
            }
            catch (Exception ex)
            {
                FireException("ucExcel.SetData", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// 버튼 선택 발생시 처리
        /// </summary>
        #region private void OnSheetSelected(object sender, ButtonSelectedEventArgs e)
        private void OnSheetSelected(object sender, ButtonSelectedEventArgs e)
        {
            try
            {
                string name = e.Name;
                for (int i = 0; i < _buttons.Length; i++)
                {
                    if (_buttons[i].ButtonName == name)
                    {
                        // 이미 선택된 Sheet일 경우 종료
                        if (_buttons[i].Selected) return;
                        // 이전 Sheet 정보 제거
                        pnlSheet.Controls.Clear();
                        pnlSheet.Controls.Add(_sheets[i]);
                        _buttons[i].SetSelected(true);
                    }
                    else
                    {
                        _buttons[i].SetSelected(false);
                    }
                }
            }
            catch (Exception ex)
            {
                FireException("ucExcel.OnSheetSelected", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// 초기화
        /// </summary>
        #region public void Clear()
        public void Clear()
        {
            _ds = null;
            _sheets = null;
            _buttons = null;
            if (pnlButton != null) pnlButton.Controls.Clear();
            if (pnlSheet != null) pnlSheet.Controls.Clear();
        } 
        #endregion
    }
}
