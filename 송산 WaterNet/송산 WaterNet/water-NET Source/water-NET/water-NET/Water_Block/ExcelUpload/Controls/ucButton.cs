﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp.ExcelUpload.Controls
{
    public delegate void ButtonSelected(object sender, ButtonSelectedEventArgs e);

    public partial class ucButton : UserControl
    {
        public string ButtonName { get; set; }
        public int ButtonWidth
        {
            get
            {
                int width = this.lblName.Width;
                Graphics g = null;
                try
                {
                    g = this.CreateGraphics();
                    SizeF size = g.MeasureString(this.lblName.Text, lblName.Font);
                    width = Convert.ToInt32(size.Width) + 35;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ucTabButton_Load -> " + ex.Message);
                }
                finally
                {
                    g.Dispose();
                }
                return width;
            }
        }
        public bool Selected { get; set; }

        public ButtonSelected OnSelected;

        public ucButton()
        {
            InitializeComponent();

            this.Selected = false;
        }

        public ucButton(string name)
            : this()
        {
            this.ButtonName = name;
        }

        private void ucTabButton_Load(object sender, EventArgs e)
        {
            // 버튼 글자 설정
            this.lblName.Text = this.ButtonName;
            // 버튼의 너비를 글자크기 + 20으로 설정 (실제 크기는 부모에서 처리)
            //this.ButtonWidth = this.lblName.Size.Width + 20;
        }

        /// <summary>
        /// 버튼 클릭 이벤트
        /// </summary>
        private void Button_Click(object sender, EventArgs e)
        {
            if (OnSelected != null) OnSelected(this, new ButtonSelectedEventArgs(this, this.ButtonName, true));
        }

        /// <summary>
        /// 버튼 선택 변경
        /// </summary>
        /// <param name="selected">선택 여부</param>
        public void SetSelected(bool selected)
        {
            this.Selected = selected;
            if (selected)
            {
                this.lblName.BackColor = Color.SkyBlue;
                this.pnlMargin.BackColor = Color.SkyBlue;
            }
            else
            {
                this.lblName.BackColor = Color.FromArgb(240, 240, 240);
                this.pnlMargin.BackColor = Color.FromArgb(240, 240, 240);
            }
        }

        private void Button_MouseHover(object sender, EventArgs e)
        {
            lblName.Font = new Font("굴림", 9F, FontStyle.Bold);
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            lblName.Font = new Font("굴림", 9F);
        }
    }

    /// <summary>
    /// Button Event Argument
    /// </summary>
    #region public class ButtonSelectedEventArgs : EventArgs
    public class ButtonSelectedEventArgs : EventArgs
    {
        /// <summary>
        /// Event object
        /// </summary>
        public object Sender { get; set; }
        /// <summary>
        /// Button name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Button selected
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sender">Button Object</param>
        /// <param name="name">Button Name</param>
        /// <param name="selected">Button Selected</param>
        public ButtonSelectedEventArgs(object sender, string name, bool selected)
        {
            this.Sender = sender;
            this.Name = name;
            this.Selected = selected;
        }
    } 
    #endregion

}
