﻿namespace WaterNet.BlockApp.ExcelUpload.Controls
{
    partial class ucExcel
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButton = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlSheet = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlButton
            // 
            this.pnlButton.AutoScroll = true;
            this.pnlButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButton.Location = new System.Drawing.Point(0, 641);
            this.pnlButton.Name = "pnlButton";
            this.pnlButton.Size = new System.Drawing.Size(954, 30);
            this.pnlButton.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 636);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(954, 5);
            this.panel2.TabIndex = 1;
            // 
            // pnlSheet
            // 
            this.pnlSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSheet.Location = new System.Drawing.Point(0, 0);
            this.pnlSheet.Name = "pnlSheet";
            this.pnlSheet.Size = new System.Drawing.Size(954, 636);
            this.pnlSheet.TabIndex = 2;
            // 
            // ucExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlSheet);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlButton);
            this.Name = "ucExcel";
            this.Size = new System.Drawing.Size(954, 671);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlSheet;
    }
}
