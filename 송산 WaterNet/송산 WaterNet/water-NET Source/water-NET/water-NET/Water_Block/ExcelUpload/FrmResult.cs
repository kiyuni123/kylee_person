﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.BlockApp.ExcelUpload
{
    public partial class FrmResult : Form
    {
        /// <summary>
        /// 처리 결과
        /// </summary>
        private List<bool> _result = null;
        /// <summary>
        /// 엑셀데이터
        /// </summary>
        private DataTable _dt = null;

        public FrmResult()
        {
            InitializeComponent();
        }

        public FrmResult(List<bool> result, DataTable dt)
            : this()
        {
            this._result = result;
            this._dt = dt;
        }

        private void FrmResult_Load(object sender, EventArgs e)
        {
            try
            {
                if (this._dt == null || this._dt.Rows.Count <= 0) return;

                // 오류만 출력하도록 성공처리 데이터는 제거
                DataTable dt = _dt.Clone();
                for (int i = 0; i < _result.Count; i++)
                {
                    if (!_result[i])
                    {
                        DataRow dr = dt.NewRow();
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            dr[j] = _dt.Rows[i][j];
                        }
                        dt.Rows.Add(dr);
                    }
                }
                dgvExcel.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 그리드를 Excel로 출력
        /// </summary>
        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel (*.xls)|*.xls";
                sfd.InitialDirectory = Application.StartupPath;

                if (sfd.ShowDialog(this) != DialogResult.OK) return;

                DataTable dt = dgvExcel.DataSource as DataTable;
                if (dt == null || dt.Rows.Count <= 0) return;
                dgvExport.Export(dgvExcel, sfd.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
