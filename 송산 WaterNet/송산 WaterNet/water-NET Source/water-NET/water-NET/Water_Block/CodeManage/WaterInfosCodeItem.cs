﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp.CodeManage
{
    /// <summary>
    /// IF_CODE_MAPPING 처리용 메모리 아이템
    /// </summary>
    public class WaterInfosCodeItem
    {
        /// <summary>
        /// 매핑구분
        /// </summary>
        public string MAPPING_GBN { get; set; }
        /// <summary>
        /// 원래코드 (Water-Infos Code)
        /// </summary>
        public string SOURCE_CODE { get; set; }
        /// <summary>
        /// 지역관리명
        /// </summary>
        public string LOC_NAME { get; set; }
        /// <summary>
        /// 변경할 코드 (LOC_CODE)
        /// </summary>
        public string DEST_CODE { get; set; }
        /// <summary>
        /// 상위 LOC_CODE
        /// </summary>
        public string PDEST_CODE { get; set; }
        /// <summary>
        /// 설명 (water-INFOS + LOC_NAME)
        /// </summary>
        public string REMARK { get; set; }

        /// <summary>
        /// 변경 여부
        /// </summary>
        public bool IsChange { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public WaterInfosCodeItem()
        {
            IsChange = false;
        }
    }
}
