﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.BlockApp.CodeManage
{
    /// <summary>
    /// IF_CODE_MAPPING 처리 화면
    /// </summary>
    public partial class FrmWaterInfosCode : Form
    {
        /// <summary>
        /// IF_CODE_MAPPING 내부 데이터 아이템
        /// </summary>
        private List<WaterInfosCodeItem> _codeList = null;

        #region public FrmWaterInfosCode()
        public FrmWaterInfosCode()
        {
            InitializeComponent();
        } 
        #endregion

        #region Helper Methods
        /// <summary>
        /// 오류처리
        /// </summary>
        /// <param name="method">오류 메서드명</param>
        /// <param name="ex">오류내용</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion
        #endregion

        #region private void FrmWaterInfosCode_Load(object sender, EventArgs e)
        private void FrmWaterInfosCode_Load(object sender, EventArgs e)
        {
            try
            {
                // 내부 데이터용 리스트 
                _codeList = new List<WaterInfosCodeItem>();
                // 데이터 조회
                DataTable dt = SelectList();
                // DataSource Set
                if (dt != null && dt.Rows.Count > 0) dgvList.DataSource = dt;
            }
            catch (Exception ex)
            {
                FireException("FrmWaterInfosCode_Load", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// IF_CODE_MAPPING 데이터 조회 및 내부 데이터 생성
        /// </summary>
        /// <returns>DataTable</returns>
        #region private DataTable SelectList()
        private DataTable SelectList()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.CODE_WIS_SELECT, EMFrame.statics.AppStatic.USER_SGCCD), null);

                #region Code List 생성
                if (dt != null && dt.Rows.Count > 0)
                {
                    _codeList.Clear();

                    string befLocCode1 = "";
                    string befLocCode2 = "";
                    string befLocCode3 = "";

                    foreach (DataRow dr in dt.Rows)
                    {
                        // DB의 데이터 추출
                        string locCode1 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.LOC_CODE1]);
                        string locCode2 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.LOC_CODE2]);
                        string locCode3 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.LOC_CODE3]);

                        string sourceCode1 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.SOURCE_CODE1]);
                        string sourceCode2 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.SOURCE_CODE2]);
                        string sourceCode3 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.SOURCE_CODE3]);

                        string locName1 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.LOC_NAME1]);
                        string locName2 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.LOC_NAME2]);
                        string locName3 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.LOC_NAME3]);

                        string destCode1 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.FTR_IDN1]);
                        string destCode2 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.FTR_IDN2]);
                        string destCode3 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.FTR_IDN3]);

                        string remark1 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.REMARK1]);
                        string remark2 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.REMARK2]);
                        string remark3 = string.Format("{0}", dr[Database.Fields.WATERINFOS_CODE.REMARK3]);

                        if (locCode1 != befLocCode1)
                        {
                            // 새로운 대블록일 경우 리스트에 추가
                            WaterInfosCodeItem item = new WaterInfosCodeItem();
                            item.MAPPING_GBN = "001";
                            item.SOURCE_CODE = sourceCode1;
                            item.LOC_NAME = locName1;
                            item.DEST_CODE = destCode1;
                            item.PDEST_CODE = "";
                            if (remark1 == "" || remark1.Length <= 0)
                            {
                                item.REMARK = "water-INFOS " + locName1;
                            }
                            item.IsChange = false;
                            _codeList.Add(item);
                        }
                        if (locCode2 != befLocCode2)
                        {
                            // 새로운 중블록일 경우 리스트에 추가
                            WaterInfosCodeItem item = new WaterInfosCodeItem();
                            item.MAPPING_GBN = "002";
                            item.SOURCE_CODE = sourceCode2;
                            item.LOC_NAME = locName2;
                            item.DEST_CODE = destCode2;
                            item.PDEST_CODE = destCode1;
                            if (remark2 == "" || remark2.Length <= 0)
                            {
                                item.REMARK = "water-INFOS " + locName2;
                            }
                            item.IsChange = false;
                            _codeList.Add(item);
                        }
                        if (locCode3 != befLocCode3)
                        {
                            // 새로운 소블록일 경우 리스트에 추가
                            WaterInfosCodeItem item = new WaterInfosCodeItem();
                            item.MAPPING_GBN = "003";
                            item.SOURCE_CODE = sourceCode3;
                            item.LOC_NAME = locName3;
                            item.DEST_CODE = destCode3;
                            item.PDEST_CODE = destCode2;
                            if (remark3 == "" || remark3.Length <= 0)
                            {
                                item.REMARK = "water-INFOS " + locName3;
                            }
                            item.IsChange = false;
                            _codeList.Add(item);
                        }
                        befLocCode1 = locCode1;
                        befLocCode2 = locCode2;
                        befLocCode3 = locCode3;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("SelectList", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (mapper != null) mapper.Dispose();
            }
            return dt;
        } 
        #endregion




        #region private void dgvList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        private void dgvList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                int colIdx = e.Cell.Column.Index;
                int rowIdx = e.Cell.Row.Index;

                // 14:대블록/15:중블록/16:소블록
                if (colIdx < 14 || colIdx > 16) return;

                string locCode = "";
                string destCode = ""; // FTR_IDN VS DEST_CODE

                string mappingGbn = ""; // 001:대블록 / 002:중블록 / 003:소블록
                string sourceCode = ""; // WaterInfos Code -> 사용자 입력 값
                string remark = "";     // water-INFOS + ' ' + LOC_NAME

                #region 그리드 값 추출
                switch (colIdx)
                {
                    case 14: // 대블록
                        locCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.LOC_CODE1].Value);
                        destCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.FTR_IDN1].Value);
                        mappingGbn = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.MAPPING_GBN1].Value);
                        sourceCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.SOURCE_CODE1].Text);
                        remark = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.REMARK1].Value);
                        break;
                    case 15: // 중블록
                        locCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.LOC_CODE2].Value);
                        destCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.FTR_IDN2].Value);
                        mappingGbn = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.MAPPING_GBN2].Value);
                        sourceCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.SOURCE_CODE2].Text);
                        remark = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.REMARK2].Value);
                        break;
                    case 16: // 소블록
                        locCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.LOC_CODE3].Value);
                        destCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.FTR_IDN3].Value);
                        mappingGbn = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.MAPPING_GBN3].Value);
                        sourceCode = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.SOURCE_CODE3].Text);
                        remark = string.Format("{0}", dgvList.Rows[rowIdx].Cells[Database.Fields.WATERINFOS_CODE.REMARK3].Value);

                        // 중복 코드 체크
                        for (int i = 0; i < dgvList.Rows.Count; i++)
                        {
                            if (i == rowIdx) continue;
                            string sc = string.Format("{0}", dgvList.Rows[i].Cells[Database.Fields.WATERINFOS_CODE.SOURCE_CODE3].Text);
                            if (sc == sourceCode)
                            {
                                MessageBox.Show("'" + sourceCode + "'는 이미 있는 코드입니다.", "코드중복", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cell.Value = "";
                                return;                      
                            }
                        }
                        break;
                }
                #endregion

                #region 내부 데이터 변경
                var objects = from WaterInfosCodeItem item in _codeList
                              where item.DEST_CODE == destCode
                              select item;
                if (objects != null && objects.Count() > 0)
                {
                    for (int i = 0; i < objects.Count(); i++)
                    {
                        WaterInfosCodeItem codeItem = objects.ElementAt(i);
                        codeItem.SOURCE_CODE = sourceCode;
                        codeItem.IsChange = true;
                    }
                }
                #endregion

                #region 그리드 데이터 변경
                bool isStart = false;
                for (int i = 0; i < dgvList.Rows.Count; i++)
                {
                    string gridDestCode = "";
                    if (colIdx == 14) gridDestCode = string.Format("{0}", dgvList.Rows[i].Cells[Database.Fields.WATERINFOS_CODE.FTR_IDN1].Value);
                    else if (colIdx == 15) gridDestCode = string.Format("{0}", dgvList.Rows[i].Cells[Database.Fields.WATERINFOS_CODE.FTR_IDN2].Value);
                    else if (colIdx == 16) gridDestCode = string.Format("{0}", dgvList.Rows[i].Cells[Database.Fields.WATERINFOS_CODE.FTR_IDN3].Value);

                    // LOC_CODE가 다른 값이 나오면 완료
                    if (isStart == true && gridDestCode != destCode) break;

                    // FTR_IDN이 같다면 SOURCE_CODE 설정
                    if (gridDestCode == destCode)
                    {
                        if (colIdx == 14) dgvList.Rows[i].Cells[Database.Fields.WATERINFOS_CODE.SOURCE_CODE1].Value = sourceCode;
                        if (colIdx == 15) dgvList.Rows[i].Cells[Database.Fields.WATERINFOS_CODE.SOURCE_CODE2].Value = sourceCode;
                        if (colIdx == 16) dgvList.Rows[i].Cells[Database.Fields.WATERINFOS_CODE.SOURCE_CODE3].Value = sourceCode;
                        isStart = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("dgvList_CellChange", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion



        /// <summary>
        /// 입력 확인
        /// </summary>
        /// <returns>boolean</returns>
        #region private bool Validate()
        private bool Validate()
        {
            bool retVal = true;

            try
            {
                bool isNull = false;

                for (int i = 0; i < _codeList.Count; i++)
                {
                    WaterInfosCodeItem item = _codeList[i];
                    if (item.MAPPING_GBN == "003" && item.SOURCE_CODE != "" && item.SOURCE_CODE.Length > 0)
                    {
                        WaterInfosCodeItem item02 = FindParent(item);
                        if (item02.SOURCE_CODE == "" || item02.SOURCE_CODE.Length <= 0)
                        {
                            isNull = true;
                            break;
                        }
                        WaterInfosCodeItem item01 = FindParent(item02);
                        if (item01.SOURCE_CODE == "" || item01.SOURCE_CODE.Length <= 0)
                        {
                            isNull = true;
                            break;
                        }
                    }
                }
                if (isNull)
                {
                    string message = "코드를 입력하지 않은 블록이 있습니다.\r\n계속하시겠습니까?";
                    if (MessageBox.Show(message, "저장", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        retVal = false;
                    }
                }
            }
            catch (Exception ex)
            {
                FireException("Validate", ex);
                MessageBox.Show(ex.Message);
            }
            return retVal;
        } 
        #endregion

        /// <summary>
        /// 상위 아이템 찾기
        /// </summary>
        /// <param name="src">부모를 찾을 아이템</param>
        /// <returns>부모 아이템</returns>
        #region private WaterInfosCodeItem FindParent(WaterInfosCodeItem src)
        private WaterInfosCodeItem FindParent(WaterInfosCodeItem src)
        {
            WaterInfosCodeItem retVal = _codeList.Find(item => item.DEST_CODE == src.PDEST_CODE);
            return retVal;
        } 
        #endregion



        /// <summary>
        /// 저장 버튼
        /// </summary>
        #region private void btnSave_Click(object sender, EventArgs e)
        private void btnSave_Click(object sender, EventArgs e)
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                if (Validate() == false) return;
                if (MessageBox.Show("저장하시겠습니까?", "저장", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper.BeginTransaction();

                for (int i = 0; i < _codeList.Count; i++)
                {
                    WaterInfosCodeItem item = _codeList[i];
                    if (item.IsChange)
                    {
                        //Console.WriteLine(item.REMARK);
                        Save(mapper, item);
                        item.IsChange = false;
                    }
                }
                mapper.CommitTransaction();
                MessageBox.Show("처리되었습니다.", "저장", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                if (mapper != null) mapper.RollbackTransaction();
                FireException("btnSave_Click", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }
        } 
        #endregion

        /// <summary>
        /// 저장 처리
        /// </summary>
        /// <param name="mapper">EMapper</param>
        /// <param name="item">WaterInfosCodeItem</param>
        #region private void Save(EMFrame.dm.EMapper mapper, WaterInfosCodeItem item)
        private void Save(EMFrame.dm.EMapper mapper, WaterInfosCodeItem item)
        {
            try
            {
                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),  // [0] MAPPING_GBN
                                                  new OracleParameter("2", OracleDbType.Varchar2),  // [1] DEST_CODE
                                                  new OracleParameter("3", OracleDbType.Varchar2),  // [2] SOURCE_CODE
                                                  new OracleParameter("4", OracleDbType.Varchar2),  // [3] REMARK
                                                  new OracleParameter("5", OracleDbType.Varchar2),  // [4] MAPPING_GBN
                                                  new OracleParameter("6", OracleDbType.Varchar2),  // [5] DEST_CODE
                                                  new OracleParameter("7", OracleDbType.Varchar2),  // [6] MAPPING_GBN
                                                  new OracleParameter("8", OracleDbType.Varchar2),  // [7] SOURCE_CODE
                                                  new OracleParameter("9", OracleDbType.Varchar2),  // [8] DEST_CODE
                                                  new OracleParameter("10", OracleDbType.Varchar2)  // [9] REMARK
                                              };
                parameters[0].Value = item.MAPPING_GBN;
                parameters[1].Value = item.DEST_CODE;
                parameters[2].Value = item.SOURCE_CODE;
                parameters[3].Value = item.REMARK;
                parameters[4].Value = item.MAPPING_GBN;
                parameters[5].Value = item.DEST_CODE;
                parameters[6].Value = item.MAPPING_GBN;
                parameters[7].Value = item.SOURCE_CODE;
                parameters[8].Value = item.DEST_CODE;
                parameters[9].Value = item.REMARK;

                mapper.ExecuteScript(Database.Queries.CODE_WIS_INS_UPD, parameters);
            }
            catch (Exception ex)
            {
                FireException("Save", ex);
                throw ex;
            }
        } 
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
