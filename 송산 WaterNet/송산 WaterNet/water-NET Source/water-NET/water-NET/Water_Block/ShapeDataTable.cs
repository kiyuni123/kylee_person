﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

namespace WaterNet.BlockApp
{
    public class ShapeDataTable
    {
        /// <summary>
        /// [SHAPE] 테이블명
        /// </summary>
        #region private struct TABLE_NAME
        private struct TABLE_NAME
        {
            public const string BLBG = "WTL_BLBG_AS"; // 대블록
            public const string BLBM = "WTL_BLBM_AS"; // 중블록
            public const string BLSM = "WTL_BLSM_AS"; // 소블록

            public const string PURI = "WTL_PURI_AS"; // 정수장
            public const string PRES = "WTL_PRES_AS"; // 가압장
            public const string HEAD = "WTL_HEAD_AS"; // 취수원
            public const string SERV = "WTL_SERV_AS"; // 배수지
        }
        #endregion

        /// <summary>
        /// 블록명
        /// </summary>
        #region public struct BLOCK_NAME
        public struct BLOCK_NAME
        {
            /// <summary>
            /// 대블록
            /// </summary>
            public const string BLBG = "대블록";
            /// <summary>
            /// 중블록
            /// </summary>
            public const string BLBM = "중블록";
            /// <summary>
            /// 소블록
            /// </summary>
            public const string BLSM = "소블록";
            /// <summary>
            /// 소소블록
            /// </summary>
            public const string BLSS = "소소블록";

            /// <summary>
            /// 정수장
            /// </summary>
            public const string PURI = "정수장";
            /// <summary>
            /// 가압장
            /// </summary>
            public const string PRES = "가압장";
            /// <summary>
            /// 취수원
            /// </summary>
            public const string HEAD = "취수원";
            /// <summary>
            /// 배수지
            /// </summary>
            public const string SERV = "배수지";

            /// <summary>
            /// ALL
            /// </summary>
            public const string ALL = "ALL";
            /// <summary>
            /// 블록
            /// </summary>
            public const string BLOCK = "블록";
            /// <summary>
            /// 블록외
            /// </summary>
            public const string ETC = "블록외";
        }
        #endregion

        /// <summary>
        /// [컬럼] 대분류
        /// </summary>
        #region private struct FIELDS_BLBG
        private struct FIELDS_BLBG
        {
            public const string FTR_CODE = "FTR_CDE";
            public const string FTR_IDN = "FTR_IDN";
            public const string LOC_NAME = "BLK_NAM";
            public const string REL_LOC_NAME = "WSP_NAM";
            public const string SGCCD = "SGCCD";
            public const string BLK_GBN = "BLK_GBN";
        }
        #endregion

        /// <summary>
        /// [컬럼] 중분류
        /// </summary>
        #region private struct FIELDS_BLBM
        private struct FIELDS_BLBM
        {
            public const string FTR_CODE = "FTR_CDE";
            public const string FTR_IDN = "FTR_IDN";
            public const string LOC_NAME = "BLK_NAM";
            public const string REL_LOC_NAME = "WSP_NAM";
            public const string PFTR_CODE = "UBL_CDE";
            public const string PFTR_IDN = "UBL_IDN";
            public const string SGCCD = "SGCCD";
            public const string BLK_GBN = "BLK_GBN";
        }
        #endregion

        /// <summary>
        /// [컬럼] 소분류
        /// </summary>
        #region private struct FIELDS_BLSM
        private struct FIELDS_BLSM
        {
            /// <summary>
            /// FTR_CDE
            /// </summary>
            public const string FTR_CODE = "FTR_CDE";
            /// <summary>
            /// FTR_IDN
            /// </summary>
            public const string FTR_IDN = "FTR_IDN";
            /// <summary>
            /// BLK_NAM
            /// </summary>
            public const string LOC_NAME = "BLK_NAM";
            /// <summary>
            /// WSP_NAM
            /// </summary>
            public const string REL_LOC_NAME = "WSP_NAM";
            /// <summary>
            /// UBL_CDE
            /// </summary>
            public const string PFTR_CODE = "UBL_CDE";
            /// <summary>
            /// UBL_IDN
            /// </summary>
            public const string PFTR_IDN = "UBL_IDN";
            /// <summary>
            /// SGCCD
            /// </summary>
            public const string SGCCD = "SGCCD";
            /// <summary>
            /// BLK_GBN
            /// </summary>
            public const string BLK_GBN = "BLK_GBN";
        }
        #endregion

        /// <summary>
        /// [컬럼] 취수원
        /// </summary>
        #region private struct FIELDS_HEAD
        private struct FIELDS_HEAD
        {
            /// <summary>
            /// FTR_CDE
            /// </summary>
            public const string FTR_CODE = "FTR_CDE";
            /// <summary>
            /// FTR_IDN
            /// </summary>
            public const string FTR_IDN = "FTR_IDN";
            /// <summary>
            /// HEA_NAM
            /// </summary>
            public const string LOC_NAME = "HEA_NAM";
            /// <summary>
            /// IRV_NAM
            /// </summary>
            public const string REL_LOC_NAME = "IRV_NAM";
            /// <summary>
            /// SGCCD
            /// </summary>
            public const string SGCCD = "SGCCD";
            /// <summary>
            /// BLK_GBN
            /// </summary>
            public const string BLK_GBN = "BLK_GBN";
        }
        #endregion

        /// <summary>
        /// [컬럼] 배수지
        /// </summary>
        #region private struct FIELDS_SERV
        private struct FIELDS_SERV
        {
            /// <summary>
            /// FTR_CDE
            /// </summary>
            public const string FTR_CODE = "FTR_CDE";
            /// <summary>
            /// FTR_IDN
            /// </summary>
            public const string FTR_IDN = "FTR_IDN";
            /// <summary>
            /// SRV_NAM
            /// </summary>
            public const string LOC_NAME = "SRV_NAM";
            //public const string REL_LOC_NAME = "PUR_NAM";
            /// <summary>
            /// SGCCD
            /// </summary>
            public const string SGCCD = "SGCCD";
            /// <summary>
            /// BLK_GBN
            /// </summary>
            public const string BLK_GBN = "BLK_GBN";
        }
        #endregion

        /// <summary>
        /// [컬럼] 정수장
        /// </summary>
        #region private struct FIELDS_PURI
        private struct FIELDS_PURI
        {
            /// <summary>
            /// FTR_CDE
            /// </summary>
            public const string FTR_CODE = "FTR_CDE";
            /// <summary>
            /// FTR_IDN
            /// </summary>
            public const string FTR_IDN = "FTR_IDN";
            /// <summary>
            /// PUR_NAM
            /// </summary>
            public const string LOC_NAME = "PUR_NAM";
            /// <summary>
            /// GAI_NAM
            /// </summary>
            public const string REL_LOC_NAME = "GAI_NAM"; // SRV_NAM
            /// <summary>
            /// SGCCD
            /// </summary>
            public const string SGCCD = "SGCCD";
            /// <summary>
            /// BLK_GBN
            /// </summary>
            public const string BLK_GBN = "BLK_GBN";
        }
        #endregion

        /// <summary>
        /// [컬럼] 가압장
        /// </summary>
        #region private struct FIELDS_PRES
        private struct FIELDS_PRES
        {
            /// <summary>
            /// FTR_CDE
            /// </summary>
            public const string FTR_CODE = "FTR_CDE";
            /// <summary>
            /// FTR_IDN
            /// </summary>
            public const string FTR_IDN = "FTR_IDN";
            /// <summary>
            /// PRS_NAM
            /// </summary>
            public const string LOC_NAME = "PRS_NAM";
            //public const string REL_LOC_NAME = "";
            /// <summary>
            /// SGCCD
            /// </summary>
            public const string SGCCD = "SGCCD";
            /// <summary>
            /// BLK_GBN
            /// </summary>
            public const string BLK_GBN = "BLK_GBN";
        }
        #endregion

        #region 컬럼목록 (colXXXX)
        /// <summary>
        /// 컬럼목록_대분류
        /// </summary>
        private static string[] colBlbg = new string[] {
            FIELDS_BLBG.FTR_CODE,
            FIELDS_BLBG.FTR_IDN,
            FIELDS_BLBG.LOC_NAME,
            FIELDS_BLBG.REL_LOC_NAME,
            FIELDS_BLBG.SGCCD,
            FIELDS_BLBG.BLK_GBN
        };
        /// <summary>
        /// 컬럼목록_중분류
        /// </summary>
        private static string[] colBlbm = new string[] {
            FIELDS_BLBM.FTR_CODE,
            FIELDS_BLBM.FTR_IDN,
            FIELDS_BLBM.LOC_NAME,
            FIELDS_BLBM.REL_LOC_NAME,
            FIELDS_BLBM.PFTR_CODE,
            FIELDS_BLBM.PFTR_IDN,
            FIELDS_BLBM.SGCCD,
            FIELDS_BLBM.BLK_GBN
        };
        /// <summary>
        /// 컬럼목록_소분류
        /// </summary>
        private static string[] colBlsm = new string[] {
            FIELDS_BLSM.FTR_CODE,
            FIELDS_BLSM.FTR_IDN,
            FIELDS_BLSM.LOC_NAME,
            FIELDS_BLSM.REL_LOC_NAME,
            FIELDS_BLSM.PFTR_CODE,
            FIELDS_BLSM.PFTR_IDN,
            FIELDS_BLSM.SGCCD,
            FIELDS_BLSM.BLK_GBN
        };
        /// <summary>
        /// 컬럼목록_정수장
        /// </summary>
        private static string[] colPuri = new string[] {
            FIELDS_PURI.FTR_CODE,
            FIELDS_PURI.FTR_IDN,
            FIELDS_PURI.LOC_NAME,
            FIELDS_PURI.REL_LOC_NAME,
            FIELDS_PURI.SGCCD,
            FIELDS_PURI.BLK_GBN
        };
        /// <summary>
        /// 컬럼목록_가압장
        /// </summary>
        private static string[] colPres = new string[] {
            FIELDS_PRES.FTR_CODE,
            FIELDS_PRES.FTR_IDN,
            FIELDS_PRES.LOC_NAME,
            FIELDS_PRES.SGCCD,
            FIELDS_PRES.BLK_GBN
        };
        /// <summary>
        /// 컬럼목록_취수원
        /// </summary>
        private static string[] colHead = new string[] {
            FIELDS_HEAD.FTR_CODE,
            FIELDS_HEAD.FTR_IDN,
            FIELDS_HEAD.LOC_NAME,
            FIELDS_HEAD.REL_LOC_NAME,
            FIELDS_HEAD.SGCCD,
            FIELDS_HEAD.BLK_GBN
        };
        /// <summary>
        /// 컬럼목록_배수지
        /// </summary>
        private static string[] colServ = new string[] {
            FIELDS_SERV.FTR_CODE,
            FIELDS_SERV.FTR_IDN,
            FIELDS_SERV.LOC_NAME,
            //FIELDS_SERV.REL_LOC_NAME,
            FIELDS_SERV.SGCCD,
            FIELDS_SERV.BLK_GBN
        };
        #endregion


        #region DataTable (전체, 타입별)
        private static DataTable _dt = null;
        /// <summary>
        /// Shape의 DataTable (전체 데이터)
        /// </summary>
        #region public static DataTable DataTable
        public static DataTable DataTable
        {
            get
            {
                if (_dt == null) return null;

                return _dt.Copy();
            }
        } 
        #endregion

        //private static DataTable _dtSort = null;
        /// <summary>
        /// 정렬된 Shape DataTable
        /// </summary>
        #region public static DataTable DataSort
        //public static DataTable DataSort
        //{
        //    get
        //    {
        //        if (_dtSort == null) return null;
        //        return _dtSort.Copy();
        //    }
        //} 
        #endregion

        private static DataTable _dtBLBG = null;
        /// <summary>
        /// 대블록 DataTable
        /// </summary>
        #region public static DataTable DataBlbg
        public static DataTable DataBlbg
        {
            get
            {
                if (_dtBLBG == null) return null;

                return _dtBLBG.Copy();
            }
        }
        #endregion

        private static DataTable _dtBLBM = null;
        /// <summary>
        /// 중블록 DataTable
        /// </summary>
        #region public static DataTable DataBlbm
        public static DataTable DataBlbm
        {
            get
            {
                if (_dtBLBM == null) return null;
                return _dtBLBM.Copy();
            }
        } 
        #endregion

        private static DataTable _dtBLSM = null;
        /// <summary>
        /// 소블록 DataTable
        /// </summary>
        #region public static DataTable DataBlsm
        public static DataTable DataBlsm
        {
            get
            {
                if (_dtBLSM == null) return null;
                return _dtBLSM.Copy();
            }
        }
        #endregion

        private static DataTable _dtPURI = null;
        /// <summary>
        /// 정수장 DataTable
        /// </summary>
        #region public static DataTable DataPuri
        public static DataTable DataPuri
        {
            get
            {
                if (_dtPURI == null) return null;
                return _dtPURI.Copy();
            }
        }
        #endregion

        private static DataTable _dtPRES = null;
        /// <summary>
        /// 가압장 DataTable
        /// </summary>
        #region public static DataTable DataPres
        public static DataTable DataPres
        {
            get
            {
                if (_dtPRES == null) return null;
                return _dtPRES.Copy();
            }
        }
        #endregion

        private static DataTable _dtHEAD = null;
        /// <summary>
        /// 취수원 DataTable
        /// </summary>
        #region public static DataTable DataHead
        public static DataTable DataHead
        {
            get
            {
                if (_dtHEAD == null) return null;
                return _dtHEAD.Copy();
            }
        }
        #endregion

        private static DataTable _dtSERV = null;
        /// <summary>
        /// 배수지 DataTable
        /// </summary>
        #region public static DataTable DataServ
        public static DataTable DataServ
        {
            get
            {
                if (_dtSERV == null) return null;
                return _dtSERV.Copy();
            }
        }
        #endregion
        #endregion



        /// <summary>
        /// Shape 테이블 열기
        /// </summary>
        #region public static void OpenTable()
        public static void OpenTable()
        {
            try
            {
                _dt = null;
                // DataTable 생성
                if (_dt == null)
                {
                    _dt = new DataTable();
                    // DataColumn 생성 (소블럭 기준)
                    for (int i = 0; i < colBlsm.Length; i++)
                    {
                        string colName = colBlsm[i];
                        colName = ConvField(colBlsm[i]);

                        _dt.Columns.Add(new DataColumn(colName, typeof(string)));
                    }
                }

                //_dtSort = _dt.Clone();
                _dtBLBG = _dt.Clone();
                _dtBLBM = _dt.Clone();
                _dtBLSM = _dt.Clone();
                _dtPURI = _dt.Clone();
                _dtPRES = _dt.Clone();
                _dtHEAD = _dt.Clone();
                _dtSERV = _dt.Clone();

                // 대블럭 정보 열기
                OpenTable(TABLE_NAME.BLBG, colBlbg, _dtBLBG);
                // 중블럭 정보 열기
                OpenTable(TABLE_NAME.BLBM, colBlbm, _dtBLBM);
                // 소블럭 정보 열기
                OpenTable(TABLE_NAME.BLSM, colBlsm, _dtBLSM);

                // 정수장
                OpenTable(TABLE_NAME.PURI, colPuri, _dtPURI);
                // 가압장
                OpenTable(TABLE_NAME.PRES, colPres, _dtPRES);
                // 취수원
                OpenTable(TABLE_NAME.HEAD, colHead, _dtHEAD);
                // 배수지
                OpenTable(TABLE_NAME.SERV, colServ, _dtSERV);

                // Shape 데이터 정렬
                //Sort();
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Exception] OpenTable -> " + ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// [Test] Table Column View
        /// </summary>
        /// <param name="tbName">Table Name</param>
        #region private static void ViewColumns(string tbName)
        private static void ViewColumns(string tbName)
        {
            try
            {
                ESRI.ArcGIS.Carto.ILayer pLayer = WaterNet.WaterAOCore.ArcManager.GetShapeLayer(
                    WaterNet.WaterAOCore.VariableManager.m_Pipegraphic,
                    tbName,
                    ""
                );
                ESRI.ArcGIS.Carto.IFeatureLayer fLayer = pLayer as ESRI.ArcGIS.Carto.IFeatureLayer;
                ESRI.ArcGIS.Geodatabase.ITable table = fLayer as ESRI.ArcGIS.Geodatabase.ITable;

                Console.WriteLine("");
                Console.WriteLine("[" + tbName + "]");
                for (int i = 0; i < table.Fields.FieldCount; i++)
                {
                    ESRI.ArcGIS.Geodatabase.IField field = table.Fields.get_Field(i);
                    string colInfo = string.Format("[{0}] Name : {1}", i, field.Name);
                    Console.WriteLine(colInfo);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Exception] ViewColumns(" + tbName + " -> " + ex.Message);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// 블록별 테이블 Shape 테이블 열기
        /// </summary>
        /// <param name="tbName"><c ref="TABLE_NAME">테이블명</c></param>
        /// <param name="cols">컬럼 목록</param>
        /// <param name="dt">타입별 데이터테이블</param>
        #region private static void OpenTable(string tbName, string[] cols, DataTable dt)
        private static void OpenTable(string tbName, string[] cols, DataTable dt)
        {
            try
            {
                ESRI.ArcGIS.Carto.ILayer pLayer = WaterNet.WaterAOCore.ArcManager.GetShapeLayer(
                    WaterNet.WaterAOCore.VariableManager.m_Pipegraphic,
                    tbName,
                    ""
                );
                ESRI.ArcGIS.Carto.IFeatureLayer fLayer = pLayer as ESRI.ArcGIS.Carto.IFeatureLayer;
                ESRI.ArcGIS.Geodatabase.ITable table = fLayer as ESRI.ArcGIS.Geodatabase.ITable;

                ESRI.ArcGIS.Geodatabase.ICursor cursor = WaterNet.WaterAOCore.ArcManager.GetCursor(fLayer, "");
                ESRI.ArcGIS.Geodatabase.IRow row = null;

                DataRow dr = null;
                DataRow drType = null;

                row = cursor.NextRow();
                while (row != null)
                {
                    dr = _dt.NewRow();
                    drType = dt.NewRow();

                    for (int i = 0; i < cols.Length; i++)
                    {
                        // 블럭 구분 설정
                        if (cols[i] == FIELDS_BLSM.BLK_GBN)
                        {
                            dr[cols[i]] = GetBlockName(tbName);
                            drType[cols[i]] = GetBlockName(tbName);
                            continue;
                        }
                        object val = WaterNet.WaterAOCore.ArcManager.GetValue(row, cols[i]);

                        string colName = cols[i];
                        //if (cols[i] == FIELDS_PURI.LOC_NAME) colName = FIELDS_BLSM.LOC_NAME;
                        //else if (cols[i] == FIELDS_PURI.REL_LOC_NAME) colName = FIELDS_BLSM.REL_LOC_NAME;
                        //else if (cols[i] == FIELDS_PRES.LOC_NAME) colName = FIELDS_BLSM.LOC_NAME;
                        //else if (cols[i] == FIELDS_HEAD.LOC_NAME) colName = FIELDS_BLSM.LOC_NAME;
                        //else if (cols[i] == FIELDS_HEAD.REL_LOC_NAME) colName = FIELDS_BLSM.REL_LOC_NAME;
                        //else if (cols[i] == FIELDS_SERV.LOC_NAME) colName = FIELDS_BLSM.LOC_NAME;
                        //// FTR_CODE / PFTR_CODE / PFTR_IDN / REL_LOC_NAME
                        //if (colName == FIELDS_BLSM.FTR_CODE) colName = "FTR_CODE";
                        //else if (colName == FIELDS_BLSM.PFTR_CODE) colName = "PFTR_CODE";
                        //else if (colName == FIELDS_BLSM.PFTR_IDN) colName = "PFTR_IDN";
                        //else if (colName == FIELDS_BLSM.REL_LOC_NAME) colName = "REL_LOC_NAME";
                        colName = ConvField(cols[i]);

                        dr[colName] = string.Format("{0}", val);
                        drType[colName] = string.Format("{0}", val);
                    }
                    _dt.Rows.Add(dr);
                    dt.Rows.Add(drType);
                    row = cursor.NextRow();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Exception] OpenTable(" + tbName + ") -> " + ex.Message);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// 테이블명으로 블록명 찾기
        /// </summary>
        /// <param name="tbName">테이블명</param>
        /// <returns>블록명</returns>
        #region private static string GetBlockName(string tbName)
        private static string GetBlockName(string tbName)
        {
            string retVal = "";

            switch (tbName)
            {
                case TABLE_NAME.BLBG:
                    retVal = BLOCK_NAME.BLBG;
                    break;
                case TABLE_NAME.BLBM:
                    retVal = BLOCK_NAME.BLBM;
                    break;
                case TABLE_NAME.BLSM:
                    retVal = BLOCK_NAME.BLSM;
                    break;
                case TABLE_NAME.PURI:
                    retVal = BLOCK_NAME.PURI;
                    break;
                case TABLE_NAME.PRES:
                    retVal = BLOCK_NAME.PRES;
                    break;
                case TABLE_NAME.HEAD:
                    retVal = BLOCK_NAME.HEAD;
                    break;
                case TABLE_NAME.SERV:
                    retVal = BLOCK_NAME.SERV;
                    break;
            }

            return retVal;
        }
        #endregion

        /// <summary>
        /// 분류에 따른 데이터 조회
        /// </summary>
        /// <param name="dataType">BLOCK_NAME</param>
        /// <returns>DataTable</returns>
        #region public static DataTable GetDataTable(string dataType)
        public static DataTable GetDataTable(string dataType)
        {
            DataTable retVal = new DataTable();

            try
            {
                if (_dt == null || _dt.Rows.Count <= 0) return retVal;

                retVal = _dt.Clone();
                DataRow[] drs = null;

                if (dataType == BLOCK_NAME.ALL)
                {
                    drs = _dt.Select(FIELDS_BLSM.BLK_GBN + " <> '" + dataType + "'");
                }
                else if (dataType == BLOCK_NAME.BLOCK)
                {
                    drs = _dt.Select(
                        FIELDS_BLSM.BLK_GBN + " = '" + BLOCK_NAME.BLBG + "' OR " +
                        FIELDS_BLSM.BLK_GBN + " = '" + BLOCK_NAME.BLBM + "' OR " +
                        FIELDS_BLSM.BLK_GBN + " = '" + BLOCK_NAME.BLSM + "' OR " +
                        FIELDS_BLSM.BLK_GBN + " = '" + BLOCK_NAME.BLSS + "'"
                    );
                }
                else if (dataType == BLOCK_NAME.ETC)
                {
                    drs = _dt.Select(
                        FIELDS_BLSM.BLK_GBN + " <> '" + BLOCK_NAME.BLBG + "' AND " +
                        FIELDS_BLSM.BLK_GBN + " <> '" + BLOCK_NAME.BLBM + "' AND " +
                        FIELDS_BLSM.BLK_GBN + " <> '" + BLOCK_NAME.BLSM + "' AND " +
                        FIELDS_BLSM.BLK_GBN + " <> '" + BLOCK_NAME.BLSS + "'"
                    );
                }
                else
                {
                    drs = _dt.Select(FIELDS_BLSM.BLK_GBN + " = '" + dataType + "'");
                }

                if (drs != null && drs.Length > 0)
                {
                    for (int i = 0; i < drs.Length; i++)
                    {
                        DataRow dr = drs[i];
                        DataRow newDr = retVal.NewRow();

                        for (int j = 0; j < colBlsm.Length; j++)
                        {
                            string colName = colBlsm[j];
                            // FTR_CODE / PFTR_CODE / PFTR_IDN / REL_LOC_NAME
                            //if (colName == FIELDS_BLSM.FTR_CODE) colName = "FTR_CODE";
                            //else if (colName == FIELDS_BLSM.PFTR_CODE) colName = "PFTR_CODE";
                            //else if (colName == FIELDS_BLSM.PFTR_IDN) colName = "PFTR_IDN";
                            //else if (colName == FIELDS_BLSM.REL_LOC_NAME) colName = "REL_LOC_NAME";
                            colName = ConvField(colBlsm[j]);

                            newDr[colName] = dr[colName];
                        }

                        retVal.Rows.Add(newDr);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Exception] GetDataTable(" + dataType + ") -> " + ex.Message);
            }

            return retVal;
        }
        #endregion

        #region public static string ConvField(string name)
        public static string ConvField(string name)
        {
            string colName = name;
            switch (colName)
            {
                case FIELDS_PURI.LOC_NAME:
                    colName = FIELDS_BLSM.LOC_NAME;
                    break;
                case FIELDS_PURI.REL_LOC_NAME:
                    colName = FIELDS_BLSM.REL_LOC_NAME;
                    break;
                case FIELDS_PRES.LOC_NAME:
                    colName = FIELDS_BLSM.LOC_NAME;
                    break;
                case FIELDS_HEAD.LOC_NAME:
                    colName = FIELDS_BLSM.LOC_NAME;
                    break;
                case FIELDS_HEAD.REL_LOC_NAME:
                    colName = FIELDS_BLSM.REL_LOC_NAME;
                    break;
                case FIELDS_SERV.LOC_NAME:
                    colName = FIELDS_BLSM.LOC_NAME;
                    break;
            }

            if (colName == FIELDS_BLSM.FTR_CODE) colName = "FTR_CODE";
            else if (colName == FIELDS_BLSM.PFTR_CODE) colName = "PFTR_CODE";
            else if (colName == FIELDS_BLSM.PFTR_IDN) colName = "PFTR_IDN";
            else if (colName == FIELDS_BLSM.REL_LOC_NAME) colName = "REL_LOC_NAME";

            return colName;
        } 
        #endregion

        #region public static DataTable GetSortDataTable(DataTable dt)
        public static DataTable GetSortDataTable(DataTable dt)
        {
            DataTable retVal = null;

            try
            {
                retVal = dt.Clone();
                if (dt == null || dt.Rows.Count <= 0) return retVal;

                List<ShapeItem> itemList = new List<ShapeItem>();
                foreach (DataRow dr in dt.Rows)
                {
                    ShapeItem newItem = new ShapeItem();
                    newItem.FTR_CODE = string.Format("{0}", dr[ConvField(FIELDS_BLSM.FTR_CODE)]);
                    newItem.FTR_IDN = string.Format("{0}", dr[ConvField(FIELDS_BLSM.FTR_IDN)]);
                    newItem.LOC_NAME = string.Format("{0}", dr[ConvField(FIELDS_BLSM.LOC_NAME)]);
                    newItem.REL_LOC_NAME = string.Format("{0}", dr[ConvField(FIELDS_BLSM.REL_LOC_NAME)]);
                    newItem.PFTR_CODE = string.Format("{0}", dr[ConvField(FIELDS_BLSM.PFTR_CODE)]);
                    newItem.PFTR_IDN = string.Format("{0}", dr[ConvField(FIELDS_BLSM.PFTR_IDN)]);
                    newItem.SGCCD = string.Format("{0}", dr[ConvField(FIELDS_BLSM.SGCCD)]);
                    newItem.BLK_GBN = string.Format("{0}", dr[ConvField(FIELDS_BLSM.BLK_GBN)]);

                    itemList.Add(newItem);
                }

                #region 대블록 (중/소 블록 포함)
                var blbgObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.BLBG
                               orderby item.FTR_IDN ascending
                               select item;
                if (blbgObjs != null && blbgObjs.Count() > 0)
                {
                    for (int i = 0; i < blbgObjs.Count(); i++)
                    {
                        ShapeItem itemBlbg = blbgObjs.ElementAt(i);
                        DataRow drBlbg = retVal.NewRow();
                        drBlbg[ConvField(FIELDS_BLSM.FTR_CODE)] = itemBlbg.FTR_CODE;
                        drBlbg[ConvField(FIELDS_BLSM.FTR_IDN)] = itemBlbg.FTR_IDN;
                        drBlbg[ConvField(FIELDS_BLSM.LOC_NAME)] = itemBlbg.LOC_NAME;
                        drBlbg[ConvField(FIELDS_BLSM.REL_LOC_NAME)] = itemBlbg.REL_LOC_NAME;
                        drBlbg[ConvField(FIELDS_BLSM.PFTR_CODE)] = itemBlbg.PFTR_CODE;
                        drBlbg[ConvField(FIELDS_BLSM.PFTR_IDN)] = itemBlbg.PFTR_IDN;
                        drBlbg[ConvField(FIELDS_BLSM.SGCCD)] = itemBlbg.SGCCD;
                        drBlbg[ConvField(FIELDS_BLSM.BLK_GBN)] = itemBlbg.BLK_GBN;
                        retVal.Rows.Add(drBlbg);

                        #region 중블록
                        var blbmObjs = from ShapeItem item in itemList
                                       where item.PFTR_IDN == itemBlbg.FTR_IDN
                                       orderby item.FTR_IDN ascending
                                       select item;
                        if (blbmObjs != null && blbmObjs.Count() > 0)
                        {
                            for (int j = 0; j < blbmObjs.Count(); j++)
                            {
                                ShapeItem itemBlbm = blbmObjs.ElementAt(j);
                                DataRow drBlbm = retVal.NewRow();
                                drBlbm[ConvField(FIELDS_BLSM.FTR_CODE)] = itemBlbm.FTR_CODE;
                                drBlbm[ConvField(FIELDS_BLSM.FTR_IDN)] = itemBlbm.FTR_IDN;
                                drBlbm[ConvField(FIELDS_BLSM.LOC_NAME)] = itemBlbm.LOC_NAME;
                                drBlbm[ConvField(FIELDS_BLSM.REL_LOC_NAME)] = itemBlbm.REL_LOC_NAME;
                                drBlbm[ConvField(FIELDS_BLSM.PFTR_CODE)] = itemBlbm.PFTR_CODE;
                                drBlbm[ConvField(FIELDS_BLSM.PFTR_IDN)] = itemBlbm.PFTR_IDN;
                                drBlbm[ConvField(FIELDS_BLSM.SGCCD)] = itemBlbm.SGCCD;
                                drBlbm[ConvField(FIELDS_BLSM.BLK_GBN)] = itemBlbm.BLK_GBN;
                                retVal.Rows.Add(drBlbm);

                                #region 소블록
                                var blsmObjs = from ShapeItem item in itemList
                                               where item.PFTR_IDN == itemBlbm.FTR_IDN
                                               orderby item.FTR_IDN ascending
                                               select item;
                                if (blsmObjs != null && blsmObjs.Count() > 0)
                                {
                                    for (int k = 0; k < blsmObjs.Count(); k++)
                                    {
                                        ShapeItem itemBlsm = blsmObjs.ElementAt(k);
                                        DataRow drBlsm = retVal.NewRow();
                                        drBlsm[ConvField(FIELDS_BLSM.FTR_CODE)] = itemBlsm.FTR_CODE;
                                        drBlsm[ConvField(FIELDS_BLSM.FTR_IDN)] = itemBlsm.FTR_IDN;
                                        drBlsm[ConvField(FIELDS_BLSM.LOC_NAME)] = itemBlsm.LOC_NAME;
                                        drBlsm[ConvField(FIELDS_BLSM.REL_LOC_NAME)] = itemBlsm.REL_LOC_NAME;
                                        drBlsm[ConvField(FIELDS_BLSM.PFTR_CODE)] = itemBlsm.PFTR_CODE;
                                        drBlsm[ConvField(FIELDS_BLSM.PFTR_IDN)] = itemBlsm.PFTR_IDN;
                                        drBlsm[ConvField(FIELDS_BLSM.SGCCD)] = itemBlsm.SGCCD;
                                        drBlsm[ConvField(FIELDS_BLSM.BLK_GBN)] = itemBlsm.BLK_GBN;
                                        retVal.Rows.Add(drBlsm);
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                #region 정수장
                var puriObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.PURI
                               orderby item.FTR_IDN ascending
                               select item;
                if (puriObjs != null && puriObjs.Count() > 0)
                {
                    for (int i = 0; i < puriObjs.Count(); i++)
                    {
                        ShapeItem item = puriObjs.ElementAt(i);
                        DataRow dr = retVal.NewRow();
                        dr[ConvField(FIELDS_BLSM.FTR_CODE)] = item.FTR_CODE;
                        dr[ConvField(FIELDS_BLSM.FTR_IDN)] = item.FTR_IDN;
                        dr[ConvField(FIELDS_BLSM.LOC_NAME)] = item.LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.REL_LOC_NAME)] = item.REL_LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.PFTR_CODE)] = item.PFTR_CODE;
                        dr[ConvField(FIELDS_BLSM.PFTR_IDN)] = item.PFTR_IDN;
                        dr[ConvField(FIELDS_BLSM.SGCCD)] = item.SGCCD;
                        dr[ConvField(FIELDS_BLSM.BLK_GBN)] = item.BLK_GBN;
                        retVal.Rows.Add(dr);
                    }
                }
                #endregion

                #region 가압장
                var presObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.PRES
                               orderby item.FTR_IDN ascending
                               select item;
                if (presObjs != null && presObjs.Count() > 0)
                {
                    for (int i = 0; i < presObjs.Count(); i++)
                    {
                        ShapeItem item = presObjs.ElementAt(i);
                        DataRow dr = retVal.NewRow();
                        dr[ConvField(FIELDS_BLSM.FTR_CODE)] = item.FTR_CODE;
                        dr[ConvField(FIELDS_BLSM.FTR_IDN)] = item.FTR_IDN;
                        dr[ConvField(FIELDS_BLSM.LOC_NAME)] = item.LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.REL_LOC_NAME)] = item.REL_LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.PFTR_CODE)] = item.PFTR_CODE;
                        dr[ConvField(FIELDS_BLSM.PFTR_IDN)] = item.PFTR_IDN;
                        dr[ConvField(FIELDS_BLSM.SGCCD)] = item.SGCCD;
                        dr[ConvField(FIELDS_BLSM.BLK_GBN)] = item.BLK_GBN;
                        retVal.Rows.Add(dr);
                    }
                }
                #endregion

                #region 취수원
                var headObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.HEAD
                               orderby item.FTR_IDN ascending
                               select item;
                if (headObjs != null && headObjs.Count() > 0)
                {
                    for (int i = 0; i < headObjs.Count(); i++)
                    {
                        ShapeItem item = headObjs.ElementAt(i);
                        DataRow dr = retVal.NewRow();
                        dr[ConvField(FIELDS_BLSM.FTR_CODE)] = item.FTR_CODE;
                        dr[ConvField(FIELDS_BLSM.FTR_IDN)] = item.FTR_IDN;
                        dr[ConvField(FIELDS_BLSM.LOC_NAME)] = item.LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.REL_LOC_NAME)] = item.REL_LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.PFTR_CODE)] = item.PFTR_CODE;
                        dr[ConvField(FIELDS_BLSM.PFTR_IDN)] = item.PFTR_IDN;
                        dr[ConvField(FIELDS_BLSM.SGCCD)] = item.SGCCD;
                        dr[ConvField(FIELDS_BLSM.BLK_GBN)] = item.BLK_GBN;
                        retVal.Rows.Add(dr);
                    }
                }
                #endregion

                #region 배수지
                var servObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.SERV
                               orderby item.FTR_IDN ascending
                               select item;
                if (servObjs != null && servObjs.Count() > 0)
                {
                    for (int i = 0; i < servObjs.Count(); i++)
                    {
                        ShapeItem item = servObjs.ElementAt(i);
                        DataRow dr = retVal.NewRow();
                        dr[ConvField(FIELDS_BLSM.FTR_CODE)] = item.FTR_CODE;
                        dr[ConvField(FIELDS_BLSM.FTR_IDN)] = item.FTR_IDN;
                        dr[ConvField(FIELDS_BLSM.LOC_NAME)] = item.LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.REL_LOC_NAME)] = item.REL_LOC_NAME;
                        dr[ConvField(FIELDS_BLSM.PFTR_CODE)] = item.PFTR_CODE;
                        dr[ConvField(FIELDS_BLSM.PFTR_IDN)] = item.PFTR_IDN;
                        dr[ConvField(FIELDS_BLSM.SGCCD)] = item.SGCCD;
                        dr[ConvField(FIELDS_BLSM.BLK_GBN)] = item.BLK_GBN;
                        retVal.Rows.Add(dr);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Exception] SortDataTable() -> " + ex.Message);
            }
            return retVal;
        }
        #endregion

        #region public static List<ShapeItem> GetSortList(DataTable dt)
        public static List<ShapeItem> GetSortList(DataTable dt)
        {
            List<ShapeItem> retVal = new List<ShapeItem>();

            try
            {
                if (dt == null || dt.Rows.Count <= 0) return retVal;

                #region 정렬용 리스트 생성
                List<ShapeItem> itemList = new List<ShapeItem>();
                foreach (DataRow dr in dt.Rows)
                {
                    ShapeItem newItem = new ShapeItem();
                    newItem.FTR_CODE = string.Format("{0}", dr[ConvField(FIELDS_BLSM.FTR_CODE)]);
                    newItem.FTR_IDN = string.Format("{0}", dr[ConvField(FIELDS_BLSM.FTR_IDN)]);
                    newItem.LOC_NAME = string.Format("{0}", dr[ConvField(FIELDS_BLSM.LOC_NAME)]);
                    newItem.REL_LOC_NAME = string.Format("{0}", dr[ConvField(FIELDS_BLSM.REL_LOC_NAME)]);
                    newItem.PFTR_CODE = string.Format("{0}", dr[ConvField(FIELDS_BLSM.PFTR_CODE)]);
                    newItem.PFTR_IDN = string.Format("{0}", dr[ConvField(FIELDS_BLSM.PFTR_IDN)]);
                    newItem.SGCCD = string.Format("{0}", dr[ConvField(FIELDS_BLSM.SGCCD)]);
                    newItem.BLK_GBN = string.Format("{0}", dr[ConvField(FIELDS_BLSM.BLK_GBN)]);

                    itemList.Add(newItem);
                }
                #endregion

                #region 대블록 (중/소 블록 포함)
                var blbgObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.BLBG
                               orderby item.FTR_IDN ascending
                               select item;
                if (blbgObjs != null && blbgObjs.Count() > 0)
                {
                    for (int i = 0; i < blbgObjs.Count(); i++)
                    {
                        ShapeItem itemBlbg = blbgObjs.ElementAt(i);
                        itemBlbg.Child = new List<ShapeItem>();
                        retVal.Add(itemBlbg);

                        #region 중블록
                        var blbmObjs = from ShapeItem item in itemList
                                       where item.PFTR_IDN == itemBlbg.FTR_IDN
                                       orderby item.FTR_IDN ascending
                                       select item;
                        if (blbmObjs != null && blbmObjs.Count() > 0)
                        {
                            for (int j = 0; j < blbmObjs.Count(); j++)
                            {
                                ShapeItem itemBlbm = blbmObjs.ElementAt(j);
                                // 상위 FTR_CODE (대블록)
                                itemBlbm.PFTR_CODE = itemBlbg.FTR_CODE;
                                itemBlbm.Child = new List<ShapeItem>();
                                itemBlbg.Child.Add(itemBlbm);

                                #region 소블록
                                var blsmObjs = from ShapeItem item in itemList
                                               where item.PFTR_IDN == itemBlbm.FTR_IDN
                                               orderby item.FTR_IDN ascending
                                               select item;
                                if (blsmObjs != null && blsmObjs.Count() > 0)
                                {
                                    for (int k = 0; k < blsmObjs.Count(); k++)
                                    {
                                        ShapeItem itemBlsm = blsmObjs.ElementAt(k);
                                        // 상위 FTR_CODE (중블록)
                                        itemBlsm.PFTR_CODE = itemBlbm.FTR_CODE;
                                        itemBlsm.Child = new List<ShapeItem>();
                                        itemBlbm.Child.Add(itemBlsm);

                                        #region 소소블록

                                        #endregion
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                #region 정수장
                var puriObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.PURI
                               orderby item.FTR_IDN ascending
                               select item;
                if (puriObjs != null && puriObjs.Count() > 0)
                {
                    for (int i = 0; i < puriObjs.Count(); i++)
                    {
                        ShapeItem item = puriObjs.ElementAt(i);
                        item.Child = new List<ShapeItem>();
                        retVal.Add(item);
                    }
                }
                #endregion

                #region 가압장
                var presObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.PRES
                               orderby item.FTR_IDN ascending
                               select item;
                if (presObjs != null && presObjs.Count() > 0)
                {
                    for (int i = 0; i < presObjs.Count(); i++)
                    {
                        ShapeItem item = presObjs.ElementAt(i);
                        item.Child = new List<ShapeItem>();
                        retVal.Add(item);
                    }
                }
                #endregion

                #region 취수원
                var headObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.HEAD
                               orderby item.FTR_IDN ascending
                               select item;
                if (headObjs != null && headObjs.Count() > 0)
                {
                    for (int i = 0; i < headObjs.Count(); i++)
                    {
                        ShapeItem item = headObjs.ElementAt(i);
                        item.Child = new List<ShapeItem>();
                        retVal.Add(item);
                    }
                }
                #endregion

                #region 배수지
                var servObjs = from ShapeItem item in itemList
                               where item.BLK_GBN == BLOCK_NAME.SERV
                               orderby item.FTR_IDN ascending
                               select item;
                if (servObjs != null && servObjs.Count() > 0)
                {
                    for (int i = 0; i < servObjs.Count(); i++)
                    {
                        ShapeItem item = servObjs.ElementAt(i);
                        item.Child = new List<ShapeItem>();
                        retVal.Add(item);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Exception] SortList() -> " + ex.Message);
            }
            return retVal;
        }
        #endregion
    }
}
