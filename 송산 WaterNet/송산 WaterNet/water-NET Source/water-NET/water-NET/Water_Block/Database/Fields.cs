﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp.Database
{
    /// <summary>
    /// Table Column 정보
    /// </summary>
    public class Fields
    {
        #region public struct DEPARTMENT
        public struct DEPARTMENT
        {
            /// <summary>
            /// SGCCD
            /// </summary>
            public const string MGDPCD = "MGDPCD";
            /// <summary>
            /// 지역명
            /// </summary>
            public const string MGDPNM = "MGDPNM";
            /// <summary>
            /// 지역명(SGCCD)
            /// </summary>
            public const string TITLE = "TITLE";
            /// <summary>
            /// DB_SID
            /// </summary>
            public const string DB_SID = "DB_SID";
            /// <summary>
            /// DB_ADDRESS
            /// </summary>
            public const string DB_ADDRESS = "DB_ADDRESS";
            /// <summary>
            /// DB_ID
            /// </summary>
            public const string DB_ID = "DB_ID";
            /// <summary>
            /// DB_ADDRESS
            /// </summary>
            public const string DB_PORT = "DB_PORT";
            /// <summary>
            /// DB_PORT
            /// </summary>
            public const string DB_PASS = "DB_PASS";
            /// <summary>
            /// DATA_PATH
            /// </summary>
            public const string DATA_PATH = "DATA_PATH";
        }
        #endregion

        /// <summary>
        /// CM_LOCATION
        /// </summary>
        #region public struct BLOCK
        public struct BLOCK
        {
            /// <summary>
            /// 계층 단계
            /// </summary>
            public const string LEVEL = "LV";
            /// <summary>
            /// FTR_CODE
            /// </summary>
            public const string FTR_CODE = "FTR_CODE";
            /// <summary>
            /// FTR_IDN
            /// </summary>
            public const string FTR_IDN = "FTR_IDN";
            /// <summary>
            /// 상위 FTR_CODE
            /// </summary>
            public const string PFTR_CODE = "PFTR_CODE";
            /// <summary>
            /// 상위 FTR_IDN
            /// </summary>
            public const string PFTR_IDN = "PFTR_IDN";
            /// <summary>
            /// 지역관리코드
            /// </summary>
            public const string LOC_CODE = "LOC_CODE";
            /// <summary>
            /// 상위 지역관리코드
            /// </summary>
            public const string PLOC_CODE = "PLOC_CODE";
            /// <summary>
            /// 블록명
            /// </summary>
            public const string LOC_NAME = "LOC_NAME";
            /// <summary>
            /// 관련지역명
            /// </summary>
            public const string REL_LOC_NAME = "REL_LOC_NAME";
            /// <summary>
            /// 관련배수지코드
            /// </summary>
            public const string RES_CODE = "RES_CODE";
            /// <summary>
            /// 계통구분 (ex:001)
            /// </summary>
            public const string KT_GBN = "KT_GBN";
            /// <summary>
            /// Order By
            /// </summary>
            public const string ORDERBY = "ORDERBY";
            /// <summary>
            /// 지자체코드
            /// </summary>
            public const string SGCCD = "SGCCD";

            /// <summary>
            /// (사용자) 블록 구분 (대블록/중블록/소블록/...)
            /// </summary>
            public const string LOC_GBN = "LOC_GBN";
            /// <summary>
            /// (사용자) Table 구분 (1:CM_LOCATION+CM_LOCATION2 / 2:CM_LOCATION2 ONLY) (TB_GBN -> CM_LOC_GBN 변경)
            /// </summary>
            public const string CM_LOC_GBN = "CM_LOC_GBN";

            /// <summary>
            /// (그리드) 블록명
            /// </summary>
            public const string LOC_NAME_ORG = "LOC_NAME_ORG";
            /// <summary>
            /// (그리드) B:블록 / T:태그
            /// </summary>
            public const string TYPE = "TYPE";            
            /// <summary>
            /// (그리드) 대블록
            /// </summary>
            public const string LOC_NAME_1 = "LOC_NAME_1";
            /// <summary>
            /// (그리드) 중블록
            /// </summary>
            public const string LOC_NAME_2 = "LOC_NAME_2";
            /// <summary>
            /// (그리드) 소블록
            /// </summary>
            public const string LOC_NAME_3 = "LOC_NAME_3";
            public const string LOC_NAME_0 = "LOC_NAME_0";
            
        } 
        #endregion

        /// <summary>
        /// IF_IHTAGS
        /// </summary>
        #region public struct TAG
        public struct TAG
        {
            /// <summary>
            /// 태그일련번호
            /// </summary>
            public const string TAGNAME = "TAGNAME";
            /// <summary>
            /// 태그설명_내부태그
            /// </summary>
            public const string DESC = "DESCRIPTION";
            /// <summary>
            /// 사용여부
            /// </summary>
            public const string USE_YN = "USE_YN";
            /// <summary>
            /// FTR_IDN
            /// </summary>
            public const string FTR_IDN = "FTR_IDN";
            /// <summary>
            /// 계수 (유량에 곱해지는 인자)
            /// </summary>
            public const string COEFFICIENT = "COEFFICIENT";
            /// <summary>
            /// 지역관리코드
            /// </summary>
            public const string LOC_CODE = "LOC_CODE";
            /// <summary>
            /// 지역관리코드2
            /// </summary>
            public const string LOC_CODE2 = "LOC_CODE2";
            /// 자동태그
            /// </summary>
            public const string AUTO_TAG_GBN = "AUTO_TAG_GBN";
            // (그리드)
            public const string TAG_TAGNAME = "TAG_TAGNAME";
            // (그리드)
            public const string TAG_DESCRIPTION = "TAG_DESCRIPTION";
            // (그리드)
            public const string TAG_USE_YN = "TAG_USE_YN";
            // (그리드)
            public const string TAG_COEFFICIENT = "TAG_COEFFICIENT";
            // (그리드)
            public const string TAG_LOC_CODE = "TAG_LOC_CODE";
            // (그리드)
            public const string TAG_LOC_CODE2 = "TAG_LOC_CODE2";
            // (그리드)
            public const string TAG_HTAGNAME = "TAG_HTAGNAME";
            // (그리드)
            public const string TAG_AUTO = "TAG_AUTO";
        }
        #endregion

        /// <summary>
        /// IF_TAG_GBN
        /// </summary>
        #region public struct TAG_GUBUN
        public struct TAG_GUBUN
        {
            /// <summary>
            /// 태그일련번호
            /// </summary>
            public const string TAGNAME = "TAGNAME";
            /// <summary>
            /// 업무구분
            /// </summary>
            public const string TAG_GBN = "TAG_GBN";
            /// <summary>
            /// 태그업무설명
            /// </summary>
            public const string TAG_DESC = "TAG_DESC";
            /// <summary>
            /// 가상태그구분 (CAL)
            /// </summary>
            public const string DUMMY_GBN = "DUMMY_GBN";
            /// <summary>
            /// 가상태그구분 (CAL)
            /// </summary>
            public const string HTAGANME = "HTAGNAME";
            /// <summary>
            /// 자동태크
            /// </summary>
            public const string AUTO_TAG_GBN = "AUTO_TAG_GBN";

            public const string RT = "RT";
            public const string FRI = "FRI";
            public const string TT = "TT";
            public const string YD = "YD";
            public const string YD_R = "YD_R";
            public const string TD = "TD";
            public const string MNF = "MNF";
            public const string FRQ = "FRQ";
            public const string FRQ_I = "FRQ_I";
            public const string FRQ_O = "FRQ_O";
            public const string SPL_D = "SPL_D";
            public const string SPL_I = "SPL_I";
            public const string SPL_O = "SPL_O";
            public const string LEI = "LEI";
            public const string PHI = "PHI";
            public const string CLI = "CLI";
            public const string TBI = "TBI";
            public const string PRI = "PRI";
            public const string PMB = "PMB";
        }
        #endregion

        /// <summary>
        /// IF_TAG_CAL
        /// </summary>
        #region public struct TAG_CAL
        public struct TAG_CAL
        {
            /// <summary>
            /// 태그일련번호 (DUMMY TAG)
            /// </summary>
            public const string TAGNAME = "TAGNAME";
            /// <summary>
            /// 계산 대상 태그일련번호
            /// </summary>
            public const string CTAGNAME = "CTAGNAME";
            /// <summary>
            /// 계산기호 (+, -)
            /// </summary>
            public const string CAL_GBN = "CAL_GBN";
        } 
        #endregion

        /// <summary>
        /// IF_CODE_MAPPING
        /// </summary>
        #region public struct WATERINFOS_CODE
        public struct WATERINFOS_CODE
        {
            /// <summary>
            /// MAPPING_GBN (매핑구분 001:대 / 002:중 / 003:소)
            /// </summary>
            public const string MAPPING_GBN = "MAPPING_GBN";
            /// <summary>
            /// SOURCE_CODE (WaterInfos Code)
            /// </summary>
            public const string SOURCE_CODE = "SOURCE_CODE";
            /// <summary>
            /// DEST_CODE (FTR_IDN)
            /// </summary>
            public const string DEST_CODE = "DEST_CODE";
            /// <summary>
            /// REMARK (water-INFOS + ' ' + LOC_NAME)
            /// </summary>
            public const string REMARK = "REMARK";

            #region Grid Columns
            public const string LOC_CODE1 = "LOC_CODE1";
            public const string LOC_CODE2 = "LOC_CODE2";
            public const string LOC_CODE3 = "LOC_CODE3";
            
            public const string FTR_IDN1 = "FTR_IDN1";
            public const string FTR_IDN2 = "FTR_IDN2";
            public const string FTR_IDN3 = "FTR_IDN3";

            public const string LOC_NAME1 = "LOC_NAME1";
            public const string LOC_NAME2 = "LOC_NAME2";
            public const string LOC_NAME3 = "LOC_NAME3";

            public const string MAPPING_GBN1 = "MAPPING_GBN1";
            public const string MAPPING_GBN2 = "MAPPING_GBN2";
            public const string MAPPING_GBN3 = "MAPPING_GBN3";

            public const string SOURCE_CODE1 = "SOURCE_CODE1";
            public const string SOURCE_CODE2 = "SOURCE_CODE2";
            public const string SOURCE_CODE3 = "SOURCE_CODE3";

            public const string REMARK1 = "REMARK1";
            public const string REMARK2 = "REMARK2";
            public const string REMARK3 = "REMARK3";
            #endregion
        } 
        #endregion

        /// <summary>
        /// SYSTEM_MASTER
        /// </summary>
        #region public struct SYSTEM_MASTER
        public struct SYSTEM_MASTER
        {
            /// <summary>
            /// SYSTEM_CODE
            /// </summary>
            public const string SYSTEM_CODE = "SYSTEM_CODE";
            public const string SYSTEM_CODE1 = "SYSTEM_CODE1";
            public const string SYSTEM_CODE2 = "SYSTEM_CODE2";
            /// <summary>
            /// SYSTEM_NAME
            /// </summary>
            public const string SYSTEM_NAME = "SYSTEM_NAME";
            public const string SYSTEM_NAME1 = "SYSTEM_NAME1";
            public const string SYSTEM_NAME2 = "SYSTEM_NAME2";
            /// <summary>
            /// LOC_CODE
            /// </summary>
            public const string LOC_CODE = "LOC_CODE";
            public const string LOC_CODE1 = "LOC_CODE1";
            public const string LOC_CODE2 = "LOC_CODE2";
            /// <summary>
            /// ORDERBY
            /// </summary>
            public const string ORDERBY = "ORDERBY";
            public const string ORDERBY1 = "ORDERBY1";
            public const string ORDERBY2 = "ORDERBY2";
            public const string ORD1 = "ORD1";
            /// <summary>
            /// IS_TEMP (0:CM_LOCATION / 1:WV_SYSTEM_MASTER)
            /// </summary>
            public const string IS_TEMP = "IS_TEMP";
            /// <summary>
            /// LV (1:중블록 / 2:소블록)
            /// </summary>
            public const string LEVEL = "LV";
        } 
        #endregion

        /// <summary>
        /// Excel Upload
        /// </summary>
        #region public struct EXCEL
        public struct EXCEL
        {
            /// <summary>
            /// TABLE_NAME (테이블명)
            /// </summary>
            public const string TABLE_NAME = "TABLE_NAME";
            /// <summary>
            /// COLUMN_NAME
            /// </summary>
            public const string COLUMN_NAME = "COLUMN_NAME";
            /// <summary>
            /// DATA_TYPE
            /// </summary>
            public const string DATA_TYPE = "DATA_TYPE";
            /// <summary>
            /// DATA_LENGTH
            /// </summary>
            public const string DATA_LENGTH = "DATA_LENGTH";
            /// <summary>
            /// NULLABLE
            /// </summary>
            public const string NULLABLE = "NULLABLE";
        }
        #endregion
    }
}
