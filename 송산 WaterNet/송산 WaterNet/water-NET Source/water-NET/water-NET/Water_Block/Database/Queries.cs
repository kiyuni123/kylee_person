﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp.Database
{
    /// <summary>
    /// Database 쿼리
    /// </summary>
    public class Queries
    {

        // LOC_CODE2
        #region DB (LOC_CODE) 선택
        /*public static string BLOCK_SELECT = "" +
            "SELECT  LOC_CODE AS CODE                   " + Environment.NewLine +
            ",       LOC_NAME AS CODE_NAME              " + Environment.NewLine +
            "FROM    CM_LOCATION                        " + Environment.NewLine +
            "WHERE   FTR_CODE in ('BZ001','BZ002','BZ003')                 ";*/


        public static string BLOCK_SELECT = "" +
            "SELECT  '0' AS CODE                        " + Environment.NewLine +
            "       ,'전체' AS CODE_NAME                " + Environment.NewLine +
            "FROM   DUAL                                " + Environment.NewLine +
            "UNION                                      " + Environment.NewLine +
            "SELECT  LOC_CODE AS CODE                   " + Environment.NewLine +
            ",       LOC_NAME AS CODE_NAME              " + Environment.NewLine +
            "        FROM    CM_LOCATION                " + Environment.NewLine +
            "WHERE   FTR_CODE in ('BZ001','BZ002','BZ003')  ";
        #endregion

        // SGCCD : EMFrame.statics.AppStatic.USER_SGCCD
        #region DB (지역) 선택
        public static string MAIN_SELECT = "" +
            "SELECT  MGDPCD                             " + Environment.NewLine +
            ",       MGDPNM                             " + Environment.NewLine +
            ",       MGDPNM||' ('||MGDPCD||')' TITLE    " + Environment.NewLine +
            ",       DB_SID                             " + Environment.NewLine +
            ",       DB_ADDRESS                         " + Environment.NewLine +
            ",       DB_PORT                            " + Environment.NewLine +
            ",       DB_ID                              " + Environment.NewLine +
            ",       DB_PASS                            " + Environment.NewLine +
            ",       DATA_PATH                          " + Environment.NewLine +
            "FROM    MANAGEMENT_DEPARTMENT              " + Environment.NewLine +
            "WHERE   ZONE_GBN    = '{0}'                ";
        #endregion

        #region Shape vs 블록 매핑 화면 (FrmShapeBlockManager.cs)
        /// <summary>
        /// Shape vs 블록 매핑 화면 - 블록 계층구조 트리 조회
        /// </summary>
        #region public static string BLOCKMAPPING_BLOCK_SELECT
        public static string BLOCKMAPPING_BLOCK_SELECT = "" +
            "SELECT                                                         " + Environment.NewLine +
            "        LOC_NAME                                               " + Environment.NewLine +
            ",       LOC_GBN                                                " + Environment.NewLine +
            ",       LEVEL LV                                               " + Environment.NewLine +
            ",       LOC_CODE                                               " + Environment.NewLine +
            ",       PLOC_CODE                                              " + Environment.NewLine +
            ",       FTR_CODE                                               " + Environment.NewLine +
            ",       PFTR_CODE                                              " + Environment.NewLine +
            ",       FTR_IDN                                                " + Environment.NewLine +
            ",       PFTR_IDN                                               " + Environment.NewLine +
            ",       REL_LOC_NAME                                           " + Environment.NewLine +
            ",       SGCCD                                                  " + Environment.NewLine +
            ",       RES_CODE                                               " + Environment.NewLine +
            ",       KT_GBN                                                 " + Environment.NewLine +
            ",       ORDERBY                                                " + Environment.NewLine +
            ",       CM_LOC_GBN                                             " + Environment.NewLine +
            "FROM    (                                                      " + Environment.NewLine +
            "        SELECT  A.LOC_NAME                                     " + Environment.NewLine +
            "        ,       A.LOC_GBN                                      " + Environment.NewLine +
            "        ,       A.LOC_CODE                                     " + Environment.NewLine +
            "        ,       A.PLOC_CODE                                    " + Environment.NewLine +
            "        ,       A.FTR_CODE                                     " + Environment.NewLine +
            "        ,       A.PFTR_CODE                                    " + Environment.NewLine +
            "        ,       A.FTR_IDN                                      " + Environment.NewLine +
            "        ,       A.PFTR_IDN                                     " + Environment.NewLine +
            "        ,       A.REL_LOC_NAME                                 " + Environment.NewLine +
            "        ,       A.SGCCD                                        " + Environment.NewLine +
            "        ,       A.RES_CODE                                     " + Environment.NewLine +
            "        ,       A.KT_GBN                                       " + Environment.NewLine +
            "        ,       A.ORDERBY                                      " + Environment.NewLine +
            "        ,       DECODE(B.LOC_CODE, NULL, '2', '1') CM_LOC_GBN  " + Environment.NewLine +
            "        FROM    CM_LOCATION2 A                                 " + Environment.NewLine +
            "        ,       CM_LOCATION  B                                 " + Environment.NewLine +
            "        WHERE   A.LOC_CODE = B.LOC_CODE(+)                     " + Environment.NewLine +
            "        AND     A.SGCCD    = B.SGCCD(+)                        " + Environment.NewLine +
            "        AND     A.SGCCD    = '{0}'                             " + Environment.NewLine +
            "        ) A                                                    " + Environment.NewLine +
            "WHERE SUBSTR(LOC_GBN, LENGTH(LOC_GBN)-1,2) = '블록'            " + Environment.NewLine +
            "START WITH PLOC_CODE IS NULL                                   " + Environment.NewLine +
            "CONNECT BY PRIOR LOC_CODE = PLOC_CODE                          " + Environment.NewLine +
            "                                                               " + Environment.NewLine +
            "UNION ALL                                                      " + Environment.NewLine +
            "                                                               " + Environment.NewLine +
            "SELECT  A.LOC_NAME                                             " + Environment.NewLine +
            ",       A.LOC_GBN                                              " + Environment.NewLine +
            ",       9 LV                                                   " + Environment.NewLine +
            ",       A.LOC_CODE                                             " + Environment.NewLine +
            ",       A.PLOC_CODE                                            " + Environment.NewLine +
            ",       A.FTR_CODE                                             " + Environment.NewLine +
            ",       A.PFTR_CODE                                            " + Environment.NewLine +
            ",       A.FTR_IDN                                              " + Environment.NewLine +
            ",       A.PFTR_IDN                                             " + Environment.NewLine +
            ",       A.REL_LOC_NAME                                         " + Environment.NewLine +
            ",       A.SGCCD                                                " + Environment.NewLine +
            ",       A.RES_CODE                                             " + Environment.NewLine +
            ",       A.KT_GBN                                               " + Environment.NewLine +
            ",       A.ORDERBY                                              " + Environment.NewLine +
            ",       DECODE(B.LOC_CODE, NULL, '2', '1') CM_LOC_GBN          " + Environment.NewLine +
            "FROM    CM_LOCATION2 A                                         " + Environment.NewLine +
            ",       CM_LOCATION  B                                         " + Environment.NewLine +
            "WHERE   A.LOC_CODE = B.LOC_CODE(+)                             " + Environment.NewLine +
            "AND     A.SGCCD    = B.SGCCD(+)                                " + Environment.NewLine +
            "AND     A.SGCCD    = '{0}'                                     " + Environment.NewLine +
            "AND     SUBSTR(A.LOC_GBN, LENGTH(A.LOC_GBN)-1,2) <> '블록'     "; 
        #endregion



        /// <summary>
        /// MERGE INTO CM_LOCATION
        /// </summary>
        #region BLOCKMAPPING_CM_LOCATION_INS_UPD
        public static string BLOCKMAPPING_CM_LOCATION_INS_UPD = "" +
            "MERGE INTO CM_LOCATION T       " + Environment.NewLine +
            "USING                          " + Environment.NewLine +
            "(                              " + Environment.NewLine +
            "    SELECT  COUNT(*) CNT       " + Environment.NewLine +
            "    FROM    CM_LOCATION        " + Environment.NewLine +
            "    WHERE   LOC_CODE    = :1   " + Environment.NewLine +   // [0] LOC_CODE
            ") S ON (S.CNT > 0)             " + Environment.NewLine +
            "WHEN MATCHED THEN              " + Environment.NewLine +
            "    UPDATE                     " + Environment.NewLine +
            "    SET                        " + Environment.NewLine +
            "            PLOC_CODE   = :2   " + Environment.NewLine +   // [1] PLOC_CODE
            "    ,       LOC_GBN     = :3   " + Environment.NewLine +   // [2] LOC_GBN
            "    ,       RES_CODE    = :4   " + Environment.NewLine +   // [3] RES_CODE
            "    ,       KT_GBN      = :5   " + Environment.NewLine +   // [4] KT_GBN
            "    ,       ORDERBY     = :6   " + Environment.NewLine +   // [5] ORDERBY - 6
            "    WHERE   LOC_CODE    = :7   " + Environment.NewLine +
            "WHEN NOT MATCHED THEN          " + Environment.NewLine +
            "    INSERT                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        LOC_CODE               " + Environment.NewLine +
            "    ,   PLOC_CODE              " + Environment.NewLine +
            "    ,   LOC_GBN                " + Environment.NewLine +
            "    ,   FTR_CODE               " + Environment.NewLine +
            "    ,   FTR_IDN                " + Environment.NewLine +
            "    ,   LOC_NAME               " + Environment.NewLine +
            "    ,   REL_LOC_NAME           " + Environment.NewLine +
            "    ,   PFTR_CODE              " + Environment.NewLine +
            "    ,   PFTR_IDN               " + Environment.NewLine +
            "    ,   SGCCD                  " + Environment.NewLine +
            "    ,   RES_CODE               " + Environment.NewLine +
            "    ,   KT_GBN                 " + Environment.NewLine +
            "    ,   ORDERBY                " + Environment.NewLine +
            "    )                          " + Environment.NewLine +
            "    VALUES                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        :8                     " + Environment.NewLine +   // [6] LOC_CODE
            "    ,   :9                     " + Environment.NewLine +   // [7] PLOC_CODE
            "    ,   :10                    " + Environment.NewLine +   // [8] LOC_GBN
            "    ,   :11                    " + Environment.NewLine +   // [9] FTR_CODE
            "    ,   :12                    " + Environment.NewLine +   // [10] FTR_IDN
            "    ,   :13                    " + Environment.NewLine +   // [11] LOC_NAME
            "    ,   :14                    " + Environment.NewLine +   // [12] REL_LOC_NAME
            "    ,   :15                    " + Environment.NewLine +   // [13] PFTR_CODE
            "    ,   :16                    " + Environment.NewLine +   // [14] PFTR_IDN
            "    ,   :17                    " + Environment.NewLine +   // [15] SGCCD
            "    ,   :18                    " + Environment.NewLine +   // [16] RES_CODE
            "    ,   :19                    " + Environment.NewLine +   // [17] KT_GBN
            "    ,   :20                    " + Environment.NewLine +   // [18] ORDERBY - 19
            "    )                          ";
        #endregion
        
        /// <summary>
        /// MERGE INTO CM_LOCATION2
        /// </summary>
        #region BLOCKMAPPING_CM_LOCATION2_INS_UPD
        public static string BLOCKMAPPING_CM_LOCATION2_INS_UPD = "" +
            "MERGE INTO CM_LOCATION2 T      " + Environment.NewLine +
            "USING                          " + Environment.NewLine +
            "(                              " + Environment.NewLine +
            "    SELECT  COUNT(*) CNT       " + Environment.NewLine +
            "    FROM    CM_LOCATION2       " + Environment.NewLine +
            "    WHERE   LOC_CODE    = :1   " + Environment.NewLine +   // [0] LOC_CODE
            ") S ON (S.CNT > 0)             " + Environment.NewLine +
            "WHEN MATCHED THEN              " + Environment.NewLine +
            "    UPDATE                     " + Environment.NewLine +
            "    SET                        " + Environment.NewLine +
            "            PLOC_CODE   = :2   " + Environment.NewLine +   // [1] PLOC_CODE
            "    ,       LOC_GBN     = :3   " + Environment.NewLine +   // [2] LOC_GBN
            "    ,       RES_CODE    = :4   " + Environment.NewLine +   // [3] RES_CODE
            "    ,       KT_GBN      = :5   " + Environment.NewLine +   // [4] KT_GBN
            "    ,       ORDERBY     = :6   " + Environment.NewLine +   // [5] ORDERBY - 6
            "    WHERE   LOC_CODE    = :7   " + Environment.NewLine +
            "WHEN NOT MATCHED THEN          " + Environment.NewLine +
            "    INSERT                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        LOC_CODE               " + Environment.NewLine +
            "    ,   PLOC_CODE              " + Environment.NewLine +
            "    ,   LOC_GBN                " + Environment.NewLine +
            "    ,   FTR_CODE               " + Environment.NewLine +
            "    ,   FTR_IDN                " + Environment.NewLine +
            "    ,   LOC_NAME               " + Environment.NewLine +
            "    ,   REL_LOC_NAME           " + Environment.NewLine +
            "    ,   PFTR_CODE              " + Environment.NewLine +
            "    ,   PFTR_IDN               " + Environment.NewLine +
            "    ,   SGCCD                  " + Environment.NewLine +
            "    ,   RES_CODE               " + Environment.NewLine +
            "    ,   KT_GBN                 " + Environment.NewLine +
            "    ,   ORDERBY                " + Environment.NewLine +
            "    )                          " + Environment.NewLine +
            "    VALUES                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        :8                     " + Environment.NewLine +   // [6] LOC_CODE
            "    ,   :9                     " + Environment.NewLine +   // [7] PLOC_CODE
            "    ,   :10                    " + Environment.NewLine +   // [8] LOC_GBN
            "    ,   :11                    " + Environment.NewLine +   // [9] FTR_CODE
            "    ,   :12                    " + Environment.NewLine +   // [10] FTR_IDN
            "    ,   :13                    " + Environment.NewLine +   // [11] LOC_NAME
            "    ,   :14                    " + Environment.NewLine +   // [12] REL_LOC_NAME
            "    ,   :15                    " + Environment.NewLine +   // [13] PFTR_CODE
            "    ,   :16                    " + Environment.NewLine +   // [14] PFTR_IDN
            "    ,   :17                    " + Environment.NewLine +   // [15] SGCCD
            "    ,   :18                    " + Environment.NewLine +   // [16] RES_CODE
            "    ,   :19                    " + Environment.NewLine +   // [17] KT_GBN
            "    ,   :20                    " + Environment.NewLine +   // [18] ORDERBY - 19
            "    )                          ";
        #endregion

        /// <summary>
        /// INSERT CM_LOCATION
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION_INSERT
        public const string BLOCKMAPPING_CM_LOCATION_INSERT = "" +
            "INSERT INTO CM_LOCATION    \r\n" +
            "(                          \r\n" +
            "   LOC_CODE                \r\n" + // 1
            ",  PLOC_CODE               \r\n" + // 2
            ",  LOC_GBN                 \r\n" + // 3
            ",  FTR_CODE                \r\n" + // 4
            ",  FTR_IDN                 \r\n" + // 5
            ",  LOC_NAME                \r\n" + // 6
            ",  REL_LOC_NAME            \r\n" + // 7
            ",  PFTR_CODE               \r\n" + // 8
            ",  PFTR_IDN                \r\n" + // 9
            ",  SGCCD                   \r\n" + // 10
            ",  RES_CODE                \r\n" + // 11
            ",  KT_GBN                  \r\n" + // 12
            ",  ORDERBY                 \r\n" + // 13
            ")                          \r\n" +
            "VALUES                     \r\n" +
            "(                          \r\n" +
            "   :1                      \r\n" +
            ",  :2                      \r\n" +
            ",  :3                      \r\n" +
            ",  :4                      \r\n" +
            ",  :5                      \r\n" +
            ",  :6                      \r\n" +
            ",  :7                      \r\n" +
            ",  :8                      \r\n" +
            ",  :9                      \r\n" +
            ",  :10                     \r\n" +
            ",  :11                     \r\n" +
            ",  :12                     \r\n" +
            ",  :13                     \r\n" +
            ")                          ";
        #endregion

        /// <summary>
        /// INSERT CM_LOCATION2
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION2_INSERT
        public const string BLOCKMAPPING_CM_LOCATION2_INSERT = "" +
            "INSERT INTO CM_LOCATION2   " +
            "(                          " +
            "   LOC_CODE                " + // 1
            ",  PLOC_CODE               " + // 2
            ",  LOC_GBN                 " + // 3
            ",  FTR_CODE                " + // 4
            ",  FTR_IDN                 " + // 5
            ",  LOC_NAME                " + // 6
            ",  REL_LOC_NAME            " + // 7
            ",  PFTR_CODE               " + // 8
            ",  PFTR_IDN                " + // 9
            ",  SGCCD                   " + // 10
            ",  RES_CODE                " + // 11
            ",  KT_GBN                  " + // 12
            ",  ORDERBY                 " + // 13
            ")                          " +
            "VALUES                     " +
            "(                          " +
            "   :1                      " +
            ",  :2                      " +
            ",  :3                      " +
            ",  :4                      " +
            ",  :5                      " +
            ",  :6                      " +
            ",  :7                      " +
            ",  :8                      " +
            ",  :9                      " +
            ",  :10                     " +
            ",  :11                     " +
            ",  :12                     " +
            ",  :13                     " +
            ")                          ";
        #endregion

        /// <summary>
        /// UPDATE CM_LOCATION
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION_UPDATE
        public const string BLOCKMAPPING_CM_LOCATION_UPDATE = "" +
            "UPDATE CM_LOCATION         " +
            "SET    LOC_CODE    = :1    " +
            ",      PLOC_CODE   = :2    " +
            ",      LOC_GBN     = :3    " +
            ",      RES_CODE    = :4    " +
            ",      KT_GBN      = :5    " +
            ",      ORDERBY     = :6    " +
            "WHERE  LOC_CODE    = :7    " +
            "AND    SGCCD       = :8    ";
        #endregion

        /// <summary>
        /// UPDATE CM_LOCATION2 Query
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION2_UPDATE
        public const string BLOCKMAPPING_CM_LOCATION2_UPDATE = "" +
            "UPDATE CM_LOCATION2        " +
            "SET    LOC_CODE    = :1    " +
            ",      PLOC_CODE   = :2    " +
            ",      LOC_GBN     = :3    " +
            ",      RES_CODE    = :4    " +
            ",      KT_GBN      = :5    " +
            ",      ORDERBY     = :6    " +
            "WHERE  LOC_CODE    = :7    " +
            "AND    SGCCD       = :8    ";
        #endregion

        /// <summary>
        /// DELETE CM_LOCATION
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION_DELETE
        public const string BLOCKMAPPING_CM_LOCATION_DELETE = "" +
            "DELETE                     " +
            "FROM   CM_LOCATION         " +
            "WHERE  LOC_CODE    = :1    " +
            "AND    SGCCD       = :2    ";
        #endregion

        /// <summary>
        /// DELETE CM_LOCATION2
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION2_DELETE
        public const string BLOCKMAPPING_CM_LOCATION2_DELETE = "" +
            "DELETE                     " +
            "FROM   CM_LOCATION2        " +
            "WHERE  LOC_CODE    = :1    " +
            "AND    SGCCD       = :2    ";
        #endregion

        /// <summary>
        /// CHILD COUNT
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION2_CHILD_COUNT
        public const string BLOCKMAPPING_CM_LOCATION2_CHILD_COUNT = "" +
            "SELECT                 " +
            "       COUNT(*) CNT    " +
            "FROM   CM_LOCATION     " +
            "WHERE  PLOC_CODE = :1  " +
            "AND    SGCCD     = :2  ";
        #endregion

        /// <summary>
        /// DELETE CM_LOCATION CHILD
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION_CHILD_DELETE
        public const string BLOCKMAPPING_CM_LOCATION_CHILD_DELETE = "" +
            "DELETE                     " +
            "FROM   CM_LOCATION         " +
            "WHERE  PLOC_CODE   = :1    " +
            "AND    SGCCD       = :2    ";
        #endregion

        /// <summary>
        /// DELETE CM_LOCATION2 CHILD
        /// </summary>
        #region public const string BLOCKMAPPING_CM_LOCATION2_CHILD_DELETE
        public const string BLOCKMAPPING_CM_LOCATION2_CHILD_DELETE = "" +
            "DELETE                     " +
            "FROM   CM_LOCATION2        " +
            "WHERE  PLOC_CODE   = :1    " +
            "AND    SGCCD       = :2    ";
        #endregion
            
        #endregion

        #region 블록 vs 태그 매핑 화면 (FrmTagManager.cs)
        /// <summary>
        /// 블록 vs 태그 매핑 화면 - 블록 태그 정보 그리드 조회
        /// </summary>
        #region public static string TAGMAPPING_BlOCK_TAG_SELECT
        public static string TAGMAPPING_BlOCK_TAG_SELECT = "" +
            "SELECT  LOC_NAME                                                           " + Environment.NewLine +
            ",       LOC_GBN                                                            " + Environment.NewLine +
            ",       DECODE(LOC_GBN,'대블록',1,'중블록',2,'소블록',3,'소소블록',4) LV   " + Environment.NewLine +
            ",       LOC_CODE                                                           " + Environment.NewLine +
            ",       PLOC_CODE                                                          " + Environment.NewLine +
            ",       FTR_CODE                                                           " + Environment.NewLine +
            ",       PFTR_CODE                                                          " + Environment.NewLine +
            ",       FTR_IDN                                                            " + Environment.NewLine +
            ",       PFTR_IDN                                                           " + Environment.NewLine +
            ",       REL_LOC_NAME                                                       " + Environment.NewLine +
            ",       SGCCD                                                              " + Environment.NewLine +
            ",       RES_CODE                                                           " + Environment.NewLine +
            ",       KT_GBN                                                             " + Environment.NewLine +
            ",       ORDERBY                                                            " + Environment.NewLine +
            ",       CM_LOC_GBN                                                         " + Environment.NewLine +
            ",       TAGNAME                                                            " + Environment.NewLine +
            ",       DESCRIPTION                                                        " + Environment.NewLine +
            ",       USE_YN                                                             " + Environment.NewLine +
            ",       COEFFICIENT                                                        " + Environment.NewLine +
            ",       TAG_GBN                                                            " + Environment.NewLine +
            ",       TAG_LOC_CODE                                                       " + Environment.NewLine +
            ",       TAG_LOC_CODE2                                                      " + Environment.NewLine +
            ",       DUMMY_GBN                                                          " + Environment.NewLine +
            ",       AUTO_TAG_GBN                                                       " + Environment.NewLine +
            "FROM    (                                                                  " + Environment.NewLine +
            "        SELECT  A.LOC_NAME                                                 " + Environment.NewLine +
            "        ,       A.LOC_GBN                                                  " + Environment.NewLine +
            "        ,       A.LOC_CODE                                                 " + Environment.NewLine +
            "        ,       A.PLOC_CODE                                                " + Environment.NewLine +
            "        ,       A.FTR_CODE                                                 " + Environment.NewLine +
            "        ,       A.PFTR_CODE                                                " + Environment.NewLine +
            "        ,       A.FTR_IDN                                                  " + Environment.NewLine +
            "        ,       A.PFTR_IDN                                                 " + Environment.NewLine +
            "        ,       A.REL_LOC_NAME                                             " + Environment.NewLine +
            "        ,       A.SGCCD                                                    " + Environment.NewLine +
            "        ,       A.RES_CODE                                                 " + Environment.NewLine +
            "        ,       A.KT_GBN                                                   " + Environment.NewLine +
            "        ,       A.ORDERBY                                                  " + Environment.NewLine +
            "        ,       DECODE(B.LOC_CODE, NULL, '2', '1') CM_LOC_GBN              " + Environment.NewLine +
            "        ,       C.TAGNAME                                                  " + Environment.NewLine +
            "        ,       C.DESCRIPTION                                              " + Environment.NewLine +
            "        ,       C.USE_YN                                                   " + Environment.NewLine +
            "        ,       C.COEFFICIENT                                              " + Environment.NewLine +
            "        ,       WM_CONCAT(D.TAG_GBN) TAG_GBN                               " + Environment.NewLine +
            "        ,       C.LOC_CODE  TAG_LOC_CODE                                   " + Environment.NewLine +
            "        ,       C.LOC_CODE2 TAG_LOC_CODE2                                  " + Environment.NewLine +
            "        ,       D.DUMMY_GBN                                                " + Environment.NewLine +
            "        ,       C.AUTO_TAG_GBN                                             " + Environment.NewLine +
            "        FROM    CM_LOCATION  A                                             " + Environment.NewLine +
            "        ,       CM_LOCATION2 B                                             " + Environment.NewLine +
            "        ,       IF_IHTAGS    C                                             " + Environment.NewLine +
            "        ,       IF_TAG_GBN   D                                             " + Environment.NewLine +
            "        WHERE   A.LOC_CODE = B.LOC_CODE(+)                                 " + Environment.NewLine +
            "        AND     A.SGCCD     = B.SGCCD(+)                                   " + Environment.NewLine +
            "        AND     A.LOC_CODE  = C.LOC_CODE2(+)                               " + Environment.NewLine +
            "        AND     C.TAGNAME   = D.TAGNAME(+)                                 " + Environment.NewLine +
            "        AND     A.SGCCD     = '{0}'                                        " + Environment.NewLine +
            "        GROUP BY    A.LOC_NAME                                             " + Environment.NewLine +
            "        ,           A.LOC_GBN                                              " + Environment.NewLine +
            "        ,           A.LOC_CODE                                             " + Environment.NewLine +
            "        ,           A.PLOC_CODE                                            " + Environment.NewLine +
            "        ,           A.FTR_CODE                                             " + Environment.NewLine +
            "        ,           A.PFTR_CODE                                            " + Environment.NewLine +
            "        ,           A.FTR_IDN                                              " + Environment.NewLine +
            "        ,           A.PFTR_IDN                                             " + Environment.NewLine +
            "        ,           A.REL_LOC_NAME                                         " + Environment.NewLine +
            "        ,           A.SGCCD                                                " + Environment.NewLine +
            "        ,           A.RES_CODE                                             " + Environment.NewLine +
            "        ,           A.KT_GBN                                               " + Environment.NewLine +
            "        ,           A.ORDERBY                                              " + Environment.NewLine +
            "        ,           B.LOC_CODE                                             " + Environment.NewLine +
            "        ,           C.TAGNAME                                              " + Environment.NewLine +
            "        ,           C.DESCRIPTION                                          " + Environment.NewLine +
            "        ,           C.USE_YN                                               " + Environment.NewLine +
            "        ,           C.COEFFICIENT                                          " + Environment.NewLine +
            "        ,           C.LOC_CODE                                             " + Environment.NewLine +
            "        ,           C.LOC_CODE2                                            " + Environment.NewLine +
            "        ,           D.DUMMY_GBN                                            " + Environment.NewLine +
            "        ,           C.AUTO_TAG_GBN                                         " + Environment.NewLine +
            "        ) A                                                                " + Environment.NewLine +
            "WHERE SUBSTR(LOC_GBN, LENGTH(LOC_GBN)-1,2) = '블록'                        " + Environment.NewLine +
            "                                                                           " + Environment.NewLine +
            "UNION ALL                                                                  " + Environment.NewLine +
            "                                                                           " + Environment.NewLine +
                    "SELECT  A.LOC_NAME                                                 " + Environment.NewLine +
            ",       A.LOC_GBN                                                          " + Environment.NewLine +
            ",       9 LV                                                               " + Environment.NewLine +
            ",       A.LOC_CODE                                                         " + Environment.NewLine +
            ",       A.PLOC_CODE                                                        " + Environment.NewLine +
            ",       A.FTR_CODE                                                         " + Environment.NewLine +
            ",       A.PFTR_CODE                                                        " + Environment.NewLine +
            ",       A.FTR_IDN                                                          " + Environment.NewLine +
            ",       A.PFTR_IDN                                                         " + Environment.NewLine +
            ",       A.REL_LOC_NAME                                                     " + Environment.NewLine +
            ",       A.SGCCD                                                            " + Environment.NewLine +
            ",       A.RES_CODE                                                         " + Environment.NewLine +
            ",       A.KT_GBN                                                           " + Environment.NewLine +
            ",       A.ORDERBY                                                          " + Environment.NewLine +
            ",       DECODE(B.LOC_CODE, NULL, '2', '1') CM_LOC_GBN                      " + Environment.NewLine +
            ",       C.TAGNAME                                                          " + Environment.NewLine +
            ",       C.DESCRIPTION                                                      " + Environment.NewLine +
            ",       C.USE_YN                                                           " + Environment.NewLine +
            ",       C.COEFFICIENT                                                      " + Environment.NewLine +
            ",       WM_CONCAT(D.TAG_GBN) TAG_GBN                                       " + Environment.NewLine +
            ",       C.LOC_CODE  TAG_LOC_CODE                                           " + Environment.NewLine +
            ",       C.LOC_CODE2 TAG_LOC_CODE2                                          " + Environment.NewLine +
            ",       D.DUMMY_GBN                                                        " + Environment.NewLine +
            ",       C.AUTO_TAG_GBN                                                     " + Environment.NewLine +
            "FROM    CM_LOCATION  A                                                     " + Environment.NewLine +
            ",       CM_LOCATION2 B                                                     " + Environment.NewLine +
            ",       IF_IHTAGS    C                                                     " + Environment.NewLine +
            ",       IF_TAG_GBN   D                                                     " + Environment.NewLine +
            "WHERE   A.LOC_CODE  = B.LOC_CODE(+)                                        " + Environment.NewLine +
            "AND     A.SGCCD     = B.SGCCD(+)                                           " + Environment.NewLine +
            "AND     A.LOC_CODE  = C.LOC_CODE2(+)                                       " + Environment.NewLine +
            "AND     C.TAGNAME   = D.TAGNAME(+)                                         " + Environment.NewLine +
            "AND     A.SGCCD     = '{0}'                                                " + Environment.NewLine +
            "AND     SUBSTR(A.LOC_GBN, LENGTH(A.LOC_GBN)-1,2) <> '블록'                 " + Environment.NewLine +
            "GROUP BY    A.LOC_NAME                                                     " + Environment.NewLine +
            ",           A.LOC_GBN                                                      " + Environment.NewLine +
            ",           A.LOC_CODE                                                     " + Environment.NewLine +
            ",           A.PLOC_CODE                                                    " + Environment.NewLine +
            ",           A.FTR_CODE                                                     " + Environment.NewLine +
            ",           A.PFTR_CODE                                                    " + Environment.NewLine +
            ",           A.FTR_IDN                                                      " + Environment.NewLine +
            ",           A.PFTR_IDN                                                     " + Environment.NewLine +
            ",           A.REL_LOC_NAME                                                 " + Environment.NewLine +
            ",           A.SGCCD                                                        " + Environment.NewLine +
            ",           A.RES_CODE                                                     " + Environment.NewLine +
            ",           A.KT_GBN                                                       " + Environment.NewLine +
            ",           A.ORDERBY                                                      " + Environment.NewLine +
            ",           B.LOC_CODE                                                     " + Environment.NewLine +
            ",           C.TAGNAME                                                      " + Environment.NewLine +
            ",           C.DESCRIPTION                                                  " + Environment.NewLine +
            ",           C.USE_YN                                                       " + Environment.NewLine +
            ",           C.COEFFICIENT                                                  " + Environment.NewLine +
            ",           C.LOC_CODE                                                     " + Environment.NewLine +
            ",           C.LOC_CODE2                                                    " + Environment.NewLine +
            ",           D.DUMMY_GBN                                                    " + Environment.NewLine +
            ",           C.AUTO_TAG_GBN                                                 " + Environment.NewLine +
            "ORDER BY LV, LOC_GBN, LOC_NAME                                             " + Environment.NewLine; 
        #endregion


        /// <summary>
        /// 블록 vs 태그 매핑 화면 - 태그 조회 그리드 조회
        /// </summary>
        #region public static string TAGMAPPING_TAG_SEARCH
        public static string TAGMAPPING_TAG_SEARCH = "" +
            "SELECT  A.TAGNAME                      " + Environment.NewLine +
            ",       A.DESCRIPTION                  " + Environment.NewLine +
            ",       A.USE_YN                       " + Environment.NewLine +
            ",       A.FTR_IDN                      " + Environment.NewLine +
            ",       A.COEFFICIENT                  " + Environment.NewLine +
            ",       A.LOC_CODE                     " + Environment.NewLine +
            ",       A.LOC_CODE2                    " + Environment.NewLine +
            ",       WM_CONCAT(B.TAG_GBN) TAG_GBN   " + Environment.NewLine +
            ",       B.DUMMY_GBN                    " + Environment.NewLine +
            ",       A.HTAGNAME                     " + Environment.NewLine +
            ",       A.AUTO_TAG_GBN                 " + Environment.NewLine +
            "FROM    IF_IHTAGS   A                  " + Environment.NewLine +
            ",       IF_TAG_GBN  B                  " + Environment.NewLine +
            "WHERE   A.TAGNAME   = B.TAGNAME(+)     " + Environment.NewLine +
            "AND     A.TAGNAME   LIKE '%{0}%'       " + Environment.NewLine +
            "AND     A.DESCRIPTION   LIKE '%{1}%'   " + Environment.NewLine +
            "GROUP BY    A.TAGNAME                  " + Environment.NewLine +
            ",           A.DESCRIPTION              " + Environment.NewLine +
            ",           A.USE_YN                   " + Environment.NewLine +
            ",           A.FTR_IDN                  " + Environment.NewLine +
            ",           A.COEFFICIENT              " + Environment.NewLine +
            ",           A.LOC_CODE                 " + Environment.NewLine +
            ",           A.LOC_CODE2                " + Environment.NewLine +
            ",           B.DUMMY_GBN                " + Environment.NewLine +
            ",           A.HTAGNAME                 " + Environment.NewLine +
            ",           A.AUTO_TAG_GBN             " + Environment.NewLine +
            "ORDER BY    TAGNAME                    "; 
        #endregion
        
        #region 블록 vs 태그 매핑 처리
        public const string TAGMAPPING_IF_IHTAGS_LOC_CODE_UPDATE = "" +
            "UPDATE  IF_IHTAGS          " +
            "SET     LOC_CODE    = :1   " +
            ",       LOC_CODE2   = :2   " +
            ",       AUTO_TAG_GBN= :3  " +
            "WHERE   TAGNAME     = :4   ";
        #endregion

        #region 태그 구분 삭제
        public const string TAGMAPPING_IF_TAG_GBN_DELETE = "" +
            "DELETE                     " +
            "FROM    IF_TAG_GBN         " +
            "WHERE   TAGNAME     = :1   " +
            "AND     TAG_GBN     = :2   ";
        #endregion

        #region 태그 구분 등록
        public const string TAGMAPPING_IF_TAG_GBN_INSERT = "" +
            "MERGE INTO IF_TAG_GBN          " +
            "USING                          " +
            "(                              " +
            "    SELECT  COUNT(*) CNT       " +
            "    FROM    IF_TAG_GBN         " +
            "    WHERE   TAGNAME     = :1   " +
            "    AND     TAG_GBN     = :2   " +
            ") S ON (S.CNT > 0)             " +
            "WHEN MATCHED THEN              " +
            "    UPDATE                     " +
            "    SET                        " +
            "            TAG_GBN     = :3   " +
            "    ,       TAG_DESC    = :4   " +
            "    WHERE   TAGNAME     = :5   " +
            "    AND     TAG_GBN     = :6   " +
            "WHEN NOT MATCHED THEN          " +
            "    INSERT                     " +
            "    (                          " +
            "        TAGNAME                " +
            "    ,   TAG_GBN                " +
            "    ,   TAG_DESC               " +
            "    )                          " +
            "    VALUES                     " +
            "    (                          " +
            "        :7                     " +
            "    ,   :8                     " +
            "    ,   :9                     " +
            "    )                          ";
        #endregion

        #region DUMMY_GBN 처리
        public const string TAGMAPPING_IF_TAG_GBN_DUMMY_GBN = "" +
            "UPDATE  IF_TAG_GBN         " +
            "SET     DUMMY_GBN   = :1   " +
            "WHERE   TAGNAME     = :2   "; 
        #endregion

        #region 태그 카운트
        public const string TAGMAPPING_TAG_COUNT = "" +
            "SELECT COUNT(*)    CNT " +
            "FROM   IF_IHTAGS       " +
            "WHERE  TAGNAME     = '{0}'";

        public const string TAGMAPPING_TAG_MAPPING_COUNT = "" +
            "SELECT COUNT(*)    CNT         " +
            "FROM   IF_IHTAGS               " +
            "WHERE  TAGNAME     = '{0}'     " +
            "AND    LOC_CODE2   IS NOT NULL";
        #endregion

        #region 태그 신규 등록
        public const string TAGMAPPING_IF_IHTAGS_INSERT = "" +
            "INSERT INTO IF_IHTAGS  " +
            "(                      " +
            "    TAGNAME            " +
            ",   DESCRIPTION        " +
            ",   USE_YN             " +
            ",   FTR_IDN            " +
            ",   COEFFICIENT        " +
            ",   LOC_CODE           " +
            ",   LOC_CODE2          " +
            ",   HTAGNAME           " +
            ")                      " +
            "VALUES                 " +
            "(                      " +
            "    :1                 " +
            ",   :2                 " +
            ",   :3                 " +
            ",   :4                 " +
            ",   :5                 " +
            ",   :6                 " +
            ",   :7                 " +
            ",   :8                 " +
            ")                      ";
        #endregion

        #region 태그 정보 수정
        public const string TAGMAPPING_IF_IHTAGS_UPDATE = "" +
            "UPDATE  IF_IHTAGS          " +
            "SET     DESCRIPTION = :1   " +
            ",       USE_YN      = :2   " +
            ",       FTR_IDN     = :3   " +
            ",       COEFFICIENT = :4   " +
            ",       LOC_CODE    = :5   " +
            ",       LOC_CODE2   = :6   " +
            ",       HTAGNAME     = :7   " +
            "WHERE   TAGNAME    = :8   ";
        #endregion

        #region 태그 정보 삭제
        public const string TAGMAPPING_IF_IHTAGS_DELETE = "" +
            "DELETE                 " +
            "FROM   IF_IHTAGS       " +
            "WHERE  TAGNAME     = :1";
        public const string TAGMAPPING_IF_TAG_GBN_DELETE_ALL = "" +
            "DELETE                 " +
            "FROM   IF_TAG_GBN      " +
            "WHERE  TAGNAME     = :1";
        public const string TAGMAPPING_IF_TAG_CAL_DELETE = "" +
            "DELETE                 " +
            "FROM   IF_TAG_CAL      " +
            "WHERE  TAGNAME     = :1";
        #endregion
        #endregion

        #region 태그 계산식 화면 (FrmTagCalcManager.cs)
        /// <summary>
        /// 블록 그리드를 구성하기 위한 블록 계층 조회
        /// </summary>
        #region TAGCALC_BLOCK_SELECT
        public static string TAGCALC_BLOCK_SELECT = "" +
            "SELECT  LOC_CODE                                                           " + Environment.NewLine +
            ",       PLOC_CODE                                                          " + Environment.NewLine +
            ",       DECODE(LOC_GBN,'대블록',1,'중블록',2,'소블록',3,'소소블록',3,9) LV " + Environment.NewLine +
            ",       LOC_NAME                                                           " + Environment.NewLine +
            ",       ORDERBY                                                            " + Environment.NewLine +
            "FROM    CM_LOCATION                                                        " + Environment.NewLine +
            "WHERE   SGCCD = '{0}'                                                      " + Environment.NewLine +
            "ORDER BY ORDERBY, LOC_CODE                                                 ";
        #endregion

        /// <summary>
        /// 블록 그리드를 구성하기 위한 전체 태그 계산식 조회
        /// </summary>
        #region TAGCALC_TAG_CALC_ALL_SELECT
        public static string TAGCALC_TAG_CALC_ALL_SELECT = "" +
            "SELECT  B.LOC_CODE                     " + Environment.NewLine +
            ",       B.LOC_NAME                     " + Environment.NewLine +
            ",       B.SGCCD                        " + Environment.NewLine +
            ",       A.TAGNAME                      " + Environment.NewLine +
            ",       A.DESCRIPTION                  " + Environment.NewLine +
            ",       C.TAG_GBN                      " + Environment.NewLine +
            ",       C.DUMMY_GBN                    " + Environment.NewLine +
            ",       D.CAL_GBN                      " + Environment.NewLine +
            ",       D.CTAGNAME                     " + Environment.NewLine +
            ",       E.DESCRIPTION TAG_DESCRIPTION  " + Environment.NewLine +
            ",       E.COEFFICIENT                  " + Environment.NewLine +
            "FROM    IF_IHTAGS   A                  " + Environment.NewLine +
            ",       CM_LOCATION B                  " + Environment.NewLine +
            ",       IF_TAG_GBN  C                  " + Environment.NewLine +
            ",       IF_TAG_CAL  D                  " + Environment.NewLine +
            ",       IF_IHTAGS   E                  " + Environment.NewLine +
            "WHERE   A.LOC_CODE  = B.LOC_CODE(+)    " + Environment.NewLine +
            "AND     A.TAGNAME   = C.TAGNAME(+)     " + Environment.NewLine +
            "AND     C.TAGNAME   = D.TAGNAME(+)     " + Environment.NewLine +
            "AND     D.CTAGNAME  = E.TAGNAME(+)     " + Environment.NewLine +
            "AND     (                              " + Environment.NewLine +
            "         C.TAG_GBN = 'RT'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRI'             " + Environment.NewLine +
            "OR       C.TAG_GBN = 'MNF'             " + Environment.NewLine +
            "OR       C.TAG_GBN = 'TT'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'TD'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'YD'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'YD_R'            " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRQ'             " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRQ_I'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRQ_O'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'SPL_D'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'SPL_I'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'SPL_O'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'RT'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'MNF'             " + Environment.NewLine + 
            "OR       C.TAG_GBN = 'TD'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRQ'             " + Environment.NewLine +      
            "        )                              " + Environment.NewLine +
            "ORDER BY B.LOC_CODE                    " + Environment.NewLine +
            ",        A.TAGNAME                     " + Environment.NewLine +
            ",        C.TAG_GBN                     " + Environment.NewLine +
            ",        D.CTAGNAME                    ";
        //public static string TAGCALC_TAG_CALC_ALL_SELECT = "" +
        //    "SELECT  A.LOC_CODE                                       " + Environment.NewLine +
        //    ",       A.LOC_NAME                                       " + Environment.NewLine +
        //    ",       A.SGCCD                                          " + Environment.NewLine +
        //    ",       B.TAGNAME                                        " + Environment.NewLine +
        //    ",       B.DESCRIPTION                                    " + Environment.NewLine +
        //    ",       C.TAG_GBN                                        " + Environment.NewLine +
        //    ",       C.DUMMY_GBN                                      " + Environment.NewLine +
        //    ",       D.CAL_GBN                                        " + Environment.NewLine +
        //    ",       D.CTAGNAME                                       " + Environment.NewLine +
        //    ",       E.DESCRIPTION TAG_DESCRIPTION                    " + Environment.NewLine +
        //    ",       E.COEFFICIENT                                    " + Environment.NewLine +
        //    "                                                         " + Environment.NewLine +
        //    "FROM    CM_LOCATION     A   -- 블록                      " + Environment.NewLine +
        //    ",       IF_IHTAGS       B   -- 태그                      " + Environment.NewLine +
        //    ",       IF_TAG_GBN      C   -- 태그구분                  " + Environment.NewLine +
        //    ",       IF_TAG_CAL      D   -- 태그 계산식               " + Environment.NewLine +
        //    ",       IF_IHTAGS       E   -- 태그 계산식의 태그        " + Environment.NewLine +
        //    ",       IF_TAG_GBN      F   -- 태그 계산식의 태그 구분   " + Environment.NewLine +
        //    "                                                         " + Environment.NewLine +
        //    "WHERE   A.LOC_CODE  = B.LOC_CODE                         " + Environment.NewLine +
        //    "AND     B.TAGNAME   = C.TAGNAME                          " + Environment.NewLine +
        //    "AND     C.TAGNAME   = D.TAGNAME                          " + Environment.NewLine +
        //    "AND     D.CTAGNAME  = E.TAGNAME                          " + Environment.NewLine +
        //    "AND     E.TAGNAME   = F.TAGNAME                          " + Environment.NewLine +
        //    "AND     C.TAG_GBN   = F.TAG_GBN                          " + Environment.NewLine +
        //    "                                                         " + Environment.NewLine +
        //    "AND B.TAGNAME = 'DUMMY_NS_001'                           " + Environment.NewLine +
        //    "ORDER BY A.LOC_CODE, B.TAGNAME, C.TAG_GBN, D.CTAGNAME    ";
        #endregion

        /// <summary>
        /// 태그 조회 쿼리문
        /// </summary>
        #region TAGCALC_TAG_SEARCH
        public static string TAGCALC_TAG_SEARCH = "" +
            "SELECT  B.LOC_CODE                         " + Environment.NewLine +
            ",       B.PLOC_CODE                        " + Environment.NewLine +
            ",       B.SGCCD                            " + Environment.NewLine +
            ",       B.LOC_NAME                         " + Environment.NewLine +
            ",       DECODE(NVL(B.LOC_GBN,'태그'),'대블록',1,'중블록',2,'소블록',3,'소소블록',3,'태그',8,9) LV " + Environment.NewLine +
            ",       A.TAGNAME                          " + Environment.NewLine +
            ",       A.DESCRIPTION                      " + Environment.NewLine +
            ",       A.COEFFICIENT                      " + Environment.NewLine +
            ",       C.DUMMY_GBN                        " + Environment.NewLine +
            ",       C.TAG_GBN                          " + Environment.NewLine +
            "FROM    IF_IHTAGS   A                      " + Environment.NewLine +
            ",       CM_LOCATION B                      " + Environment.NewLine +
            ",       (                                  " + Environment.NewLine +
            "        SELECT  TAGNAME                                                                         " + Environment.NewLine +
            "        ,       DUMMY_GBN                                                                       " + Environment.NewLine +
            "        ,       DECODE(TAG_GBN,'RT','RT','FRI','RT','MNF','RT',                                 " + Environment.NewLine +
            "                               'TT','TD','TD','TD',                                             " + Environment.NewLine +
            "                               'YD','FRQ','YD_R','FRQ','FRQ_I','FRQ','FRQ_O','FRQ','SPL_D','FRQ','SPL_I','FRQ','SPL_O','FRQ', " + Environment.NewLine +
            "                               'ETC'                                                            " + Environment.NewLine +
            "                ) TAG_GBN                                                  " + Environment.NewLine +
            "        FROM    IF_TAG_GBN                                                 " + Environment.NewLine +
            "        ) C                                                                " + Environment.NewLine +
            "WHERE   A.LOC_CODE      = B.LOC_CODE(+)    " + Environment.NewLine +
            "AND     A.TAGNAME       = C.TAGNAME        " + Environment.NewLine +
            "AND     A.TAGNAME       LIKE '%{0}%'       " + Environment.NewLine +
            "AND     A.DESCRIPTION   LIKE '%{1}%'       " + Environment.NewLine +
            "AND     C.TAG_GBN       IN ({2})           " + Environment.NewLine +
            "GROUP BY   B.LOC_CODE                      " + Environment.NewLine +
            ",          B.PLOC_CODE                     " + Environment.NewLine +
            ",          B.SGCCD                         " + Environment.NewLine +
            ",          B.LOC_NAME                      " + Environment.NewLine +
            ",          B.LOC_GBN                       " + Environment.NewLine +
            ",          A.TAGNAME                       " + Environment.NewLine +
            ",          A.DESCRIPTION                   " + Environment.NewLine +
            ",          A.COEFFICIENT                   " + Environment.NewLine +
            ",          C.DUMMY_GBN                     " + Environment.NewLine +
            ",          C.TAG_GBN                       ";
        #endregion

        /// <summary>
        /// 태그 계산식 등록/수정
        /// </summary>
        #region TAGCALC_MERGE_INS_UPD
        public static string TAGCALC_MERGE_INS_UPD = "" +
            "MERGE INTO IF_TAG_CAL T        " + Environment.NewLine +
            "USING                          " + Environment.NewLine +
            "(                              " + Environment.NewLine +
            "    SELECT  COUNT(*) CNT       " + Environment.NewLine +
            "    FROM    IF_TAG_CAL         " + Environment.NewLine +
            "    WHERE   TAGNAME     = :1   " + Environment.NewLine +
            "    AND     CTAGNAME    = :2   " + Environment.NewLine +
            ") S ON (S.CNT > 0)             " + Environment.NewLine +
            "WHEN MATCHED THEN              " + Environment.NewLine +
            "    UPDATE                     " + Environment.NewLine +
            "    SET                        " + Environment.NewLine +
            "        CAL_GBN = :3           " + Environment.NewLine +
            "    WHERE   TAGNAME = :1       " + Environment.NewLine +
            "    AND     CTAGNAME = :2      " + Environment.NewLine +
            "WHEN NOT MATCHED THEN          " + Environment.NewLine +
            "    INSERT                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        TAGNAME                " + Environment.NewLine +
            "    ,   CTAGNAME               " + Environment.NewLine +
            "    ,   CAL_GBN                " + Environment.NewLine +
            "    )                          " + Environment.NewLine +
            "    VALUES                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        :1                     " + Environment.NewLine +
            "    ,   :2                     " + Environment.NewLine +
            "    ,   :3                     " + Environment.NewLine +
            "    )                          "; 
        #endregion

        /// <summary>
        /// 태그 계산식 삭제
        /// </summary>
        #region TAGCALC_DELETE
        public static string TAGCALC_DELETE = "" +
            "DELETE                     " + Environment.NewLine +
            "FROM    IF_TAG_CAL         " + Environment.NewLine +
            "WHERE   TAGNAME     =  :1  " + Environment.NewLine +
            "AND     CTAGNAME    =  :2  "; 
        #endregion

        /// <summary>
        /// RT, MNF일 경우 복사 처리
        /// </summary>
        #region TAGCALC_RT_MNF_COPY
        public static string TAGCALC_RT_MNF_COPY = "" +
            "MERGE INTO IF_TAG_GBN T            " + Environment.NewLine +
            "USING                              " + Environment.NewLine +
            "(                                  " + Environment.NewLine +
            "    SELECT  COUNT(*) CNT           " + Environment.NewLine +
            "    FROM    IF_TAG_GBN             " + Environment.NewLine +
            "    WHERE   TAGNAME     = :1       " + Environment.NewLine +
            "    AND     TAG_GBN     = :2       " + Environment.NewLine +
            "    AND     DUMMY_GBN   = 'CAL'    " + Environment.NewLine +
            ") S ON (S.CNT > 0)                 " + Environment.NewLine +
            "WHEN MATCHED THEN                  " + Environment.NewLine +
            "    UPDATE                         " + Environment.NewLine +
            "    SET                            " + Environment.NewLine +
            "       TAG_GBN  = :3               " + Environment.NewLine +
            "    WHERE   TAGNAME     = :4       " + Environment.NewLine +
            "    AND     TAG_GBN     = :5       " + Environment.NewLine +
            "    AND     DUMMY_GBN   = 'CAL'    " + Environment.NewLine +
            "WHEN NOT MATCHED THEN              " + Environment.NewLine +
            "    INSERT                         " + Environment.NewLine +
            "    (                              " + Environment.NewLine +
            "        TAGNAME                    " + Environment.NewLine +
            "    ,   TAG_GBN                    " + Environment.NewLine +
            "    ,   TAG_DESC                   " + Environment.NewLine +
            "    ,   DUMMY_GBN                  " + Environment.NewLine +
            "    )                              " + Environment.NewLine +
            "    VALUES                         " + Environment.NewLine +
            "    (                              " + Environment.NewLine +
            "        :6                         " + Environment.NewLine +
            "    ,   :7                         " + Environment.NewLine +
            "    ,   :8                         " + Environment.NewLine +
            "    ,   'CAL'                      " + Environment.NewLine +
            "    )                              "; 
        #endregion
        #endregion

        #region 기타 태그 계산식 화면 (FrmTagCalcManagerEtc.cs)
        /// <summary>
        /// 블록 그리드를 구성하기 위한 전체 태그 계산식 조회
        /// </summary>
        #region TAGCALC_TAG_CALC_ALL_SELECT_ETC
        public static string TAGCALC_TAG_CALC_ALL_SELECT_ETC = "" +
            "SELECT  B.LOC_CODE                     " + Environment.NewLine +
            ",       B.LOC_NAME                     " + Environment.NewLine +
            ",       B.SGCCD                        " + Environment.NewLine +
            ",       A.TAGNAME                      " + Environment.NewLine +
            ",       A.DESCRIPTION                  " + Environment.NewLine +
            ",       C.TAG_GBN                      " + Environment.NewLine +
            ",       C.DUMMY_GBN                    " + Environment.NewLine +
            ",       D.CAL_GBN                      " + Environment.NewLine +
            ",       D.CTAGNAME                     " + Environment.NewLine +
            ",       E.DESCRIPTION TAG_DESCRIPTION  " + Environment.NewLine +
            ",       E.COEFFICIENT                  " + Environment.NewLine +
            "FROM    IF_IHTAGS   A                  " + Environment.NewLine +
            ",       CM_LOCATION B                  " + Environment.NewLine +
            ",       IF_TAG_GBN  C                  " + Environment.NewLine +
            ",       IF_TAG_CAL  D                  " + Environment.NewLine +
            ",       IF_IHTAGS   E                  " + Environment.NewLine +
            ",       (                              " + Environment.NewLine +
            "        SELECT  TAGNAME                " + Environment.NewLine +
            "        ,       TAG_GBN                " + Environment.NewLine +
            "        ,       COUNT(*) CNT           " + Environment.NewLine +
            "        FROM    IF_TAG_GBN             " + Environment.NewLine +
            "        WHERE   TAG_GBN IN (           " + Environment.NewLine +
            "                    'RT','MNF','TD','FRQ'  " + Environment.NewLine +
            "                )                      " + Environment.NewLine +
            "        GROUP BY TAGNAME, TAG_GBN      " + Environment.NewLine +
            "        ) F                            " + Environment.NewLine +
            "WHERE   A.LOC_CODE  = B.LOC_CODE(+)    " + Environment.NewLine +
            "AND     A.TAGNAME   = C.TAGNAME(+)     " + Environment.NewLine +
            "AND     C.TAGNAME   = D.TAGNAME(+)     " + Environment.NewLine +
            "AND     D.CTAGNAME  = E.TAGNAME(+)     " + Environment.NewLine +
            "AND     A.TAGNAME   = F.TAGNAME(+)     " + Environment.NewLine +
            "AND     (                              " + Environment.NewLine +
            "         C.TAG_GBN = 'FRI'             " + Environment.NewLine +
            "OR       C.TAG_GBN = 'TT'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'YD'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'YD_R'            " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRQ_I'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRQ_O'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'SPL_D'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'SPL_I'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'SPL_O'           " + Environment.NewLine +
            "OR       C.TAG_GBN = 'RT'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'MNF'             " + Environment.NewLine + 
            "OR       C.TAG_GBN = 'TD'              " + Environment.NewLine +
            "OR       C.TAG_GBN = 'FRQ'             " + Environment.NewLine +
            "        )                              " + Environment.NewLine +
            "AND     NVL(F.CNT,0) <= 0              " + Environment.NewLine +
            "ORDER BY B.LOC_CODE                    " + Environment.NewLine +
            ",        A.TAGNAME                     " + Environment.NewLine +
            ",        C.TAG_GBN                     " + Environment.NewLine +
            ",        D.CTAGNAME                    ";
        #endregion

        #endregion


        #region 선택 더미 태그화면 (FrmTagCalcManagerPop.cs)
        /// <summary>
        /// 해당 더미 태그에 속한 태그 내용
        /// </summary>
        #region TAGCALC_TAG_CALC_SELECT_POP
        public static string TAGCALC_TAG_CALC_SELECT_POP = "" +
            "SELECT  B.TAGNAME, C.TAG_GBN, D.CODE_NAME, B.DESCRIPTION,  A.CAL_GBN FROM      " + Environment.NewLine +
            "(SELECT CTAGNAME, CAL_GBN FROM IF_TAG_CAL WHERE TAGNAME = '{0}') A,   " + Environment.NewLine +
            "IF_IHTAGS B, IF_TAG_GBN C, CM_CODE D                                           " + Environment.NewLine +
            "WHERE A.CTAGNAME = B.TAGNAME                                                   " + Environment.NewLine +
            "    AND A.CTAGNAME = C.TAGNAME                                                 " + Environment.NewLine +
            "    AND D.PCODE = '7010'                                                       " + Environment.NewLine +
            "    AND C.TAG_GBN = D.CODE                                                     ";
        #endregion

        #endregion

        #region Water-Infos Code Mapping (FrmWaterInfosCode.cs)
        /// <summary>
        /// WaterInfos Code Mapping 리스트 조회
        /// </summary>
        #region CODE_WIS_SELECT
        public static string CODE_WIS_SELECT = "    " + Environment.NewLine +
            "SELECT  ''              TITLE0         " + Environment.NewLine +
            ",       ''              TITLE1         " + Environment.NewLine +
            ",       C.LOC_CODE      LOC_CODE1      " + Environment.NewLine +
            ",       B.LOC_CODE      LOC_CODE2      " + Environment.NewLine +
            ",       A.LOC_CODE      LOC_CODE3      " + Environment.NewLine +
            ",       C.FTR_IDN       FTR_IDN1       " + Environment.NewLine +
            ",       B.FTR_IDN       FTR_IDN2       " + Environment.NewLine +
            ",       A.FTR_IDN       FTR_IDN3       " + Environment.NewLine +
            ",       C.LOC_NAME      LOC_NAME1      " + Environment.NewLine +
            ",       B.LOC_NAME      LOC_NAME2      " + Environment.NewLine +
            ",       A.LOC_NAME      LOC_NAME3      " + Environment.NewLine +
            ",       F.MAPPING_GBN   MAPPING_GBN1   " + Environment.NewLine +
            ",       E.MAPPING_GBN   MAPPING_GBN2   " + Environment.NewLine +
            ",       D.MAPPING_GBN   MAPPING_GBN3   " + Environment.NewLine +
            ",       F.SOURCE_CODE   SOURCE_CODE1   " + Environment.NewLine +
            ",       E.SOURCE_CODE   SOURCE_CODE2   " + Environment.NewLine +
            ",       D.SOURCE_CODE   SOURCE_CODE3   " + Environment.NewLine +
            ",       F.REMARK        REMARK1        " + Environment.NewLine +
            ",       E.REMARK        REMARK2        " + Environment.NewLine +
            ",       D.REMARK        REMARK3        " + Environment.NewLine +
            "FROM    CM_LOCATION     A              " + Environment.NewLine +
            ",       CM_LOCATION     B              " + Environment.NewLine +
            ",       CM_LOCATION     C              " + Environment.NewLine +
            ",       IF_CODE_MAPPING D              " + Environment.NewLine +
            ",       IF_CODE_MAPPING E              " + Environment.NewLine +
            ",       IF_CODE_MAPPING F              " + Environment.NewLine +
            "WHERE   A.LOC_GBN   = '소블록'         " + Environment.NewLine +
            "AND     B.LOC_GBN   = '중블록'         " + Environment.NewLine +
            "AND     C.LOC_GBN   = '대블록'         " + Environment.NewLine +
            "AND     A.PLOC_CODE = B.LOC_CODE       " + Environment.NewLine +
            "AND     B.PLOC_CODE = C.LOC_CODE       " + Environment.NewLine +
            "AND     A.FTR_IDN   = D.DEST_CODE(+)   " + Environment.NewLine +
            "AND     B.FTR_IDN   = E.DEST_CODE(+)   " + Environment.NewLine +
            "AND     C.FTR_IDN   = F.DEST_CODE(+)   " + Environment.NewLine +
            "AND     A.SGCCD     = '{0}'            " + Environment.NewLine +
            "ORDER BY A.ORDERBY                     "; 
        #endregion

        #region CODE_WIS_INS_UPD
        public static string CODE_WIS_INS_UPD = "" + Environment.NewLine +
            "MERGE INTO IF_CODE_MAPPING T  " + Environment.NewLine +
            "USING                         " + Environment.NewLine +
            "(                             " + Environment.NewLine +
            "    SELECT  COUNT(*) CNT      " + Environment.NewLine +
            "    FROM    IF_CODE_MAPPING   " + Environment.NewLine +
            "    WHERE   MAPPING_GBN = :1  " + Environment.NewLine +
            "    AND     DEST_CODE   = :2  " + Environment.NewLine +
            ") S ON (S.CNT > 0)            " + Environment.NewLine +
            "WHEN MATCHED THEN             " + Environment.NewLine +
            "    UPDATE                    " + Environment.NewLine +
            "    SET     SOURCE_CODE = :3  " + Environment.NewLine +
            "    ,       REMARK      = :4  " + Environment.NewLine +
            "    WHERE   MAPPING_GBN = :5  " + Environment.NewLine +
            "    AND     DEST_CODE   = :6  " + Environment.NewLine +
            "WHEN NOT MATCHED THEN         " + Environment.NewLine +
            "    INSERT                    " + Environment.NewLine +
            "    (                         " + Environment.NewLine +
            "        MAPPING_GBN           " + Environment.NewLine +
            "    ,   SOURCE_CODE           " + Environment.NewLine +
            "    ,   DEST_CODE             " + Environment.NewLine +
            "    ,   REMARK                " + Environment.NewLine +
            "    )                         " + Environment.NewLine +
            "    VALUES                    " + Environment.NewLine +
            "    (                         " + Environment.NewLine +
            "        :7                    " + Environment.NewLine +
            "    ,   :8                    " + Environment.NewLine +
            "    ,   :9                    " + Environment.NewLine +
            "    ,   :10                   " + Environment.NewLine +
            "    )                         "; 
        #endregion
        #endregion

        #region 관망운영일보 설정
        /// <summary>
        /// 계통정보 목록 조회
        /// </summary>
        #region SYSTEM_SYSTEM_MASTER_SELECT
        public static string SYSTEM_SYSTEM_MASTER_SELECT = "" +
            "SELECT  '1' IS_TEMP                                                            " + Environment.NewLine +
            ",       SYSTEM_CODE SYSTEM_CODE1                                               " + Environment.NewLine +
            ",       SYSTEM_NAME SYSTEM_NAME1                                               " + Environment.NewLine +
            ",       LOC_CODE LOC_CODE1                                                     " + Environment.NewLine +
            ",       ORDERBY ORDERBY1                                                       " + Environment.NewLine +
            ",       LPAD(TO_CHAR(ORDERBY),3,'0')||'000' ORD1                               " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       NULL SYSTEM_CODE2                                                      " + Environment.NewLine +
            ",       NULL SYSTEM_NAME2                                                      " + Environment.NewLine +
            ",       NULL LOC_CODE2                                                         " + Environment.NewLine +
            ",       NULL ORDERBY2                                                          " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       '1' LV                                                                 " + Environment.NewLine +
            ",       SYSTEM_CODE                                                            " + Environment.NewLine +
            ",       SYSTEM_NAME                                                            " + Environment.NewLine +
            ",       LOC_CODE                                                               " + Environment.NewLine +
            ",       ORDERBY                                                                " + Environment.NewLine +
            "FROM    WV_SYSTEM_MASTER                                                       " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            "UNION ALL                                                                      " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            "SELECT  '1' IS_TEMP                                                            " + Environment.NewLine +
            ",       NULL SYSTEM_CODE1                                                      " + Environment.NewLine +
            ",       NULL SYSTEM_NAME1                                                      " + Environment.NewLine +
            ",       NULL LOC_CODE1                                                         " + Environment.NewLine +
            ",       NULL ORDERBY1                                                          " + Environment.NewLine +
            ",       LPAD(TO_CHAR(C.ORDERBY),3,'0')||LPAD(TO_CHAR(A.ORDERBY),3,'0') ORD1    " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       A.SYSTEM_CODE SYSTEM_CODE2                                             " + Environment.NewLine +
            ",       B.LOC_NAME  SYSTEM_NAME2                                               " + Environment.NewLine +
            ",       A.LOC_CODE LOC_CODE2                                                   " + Environment.NewLine +
            ",       A.ORDERBY ORDERBY2                                                     " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       '2' LV                                                                 " + Environment.NewLine +
            ",       A.SYSTEM_CODE                                                          " + Environment.NewLine +
            ",       B.LOC_NAME                                                             " + Environment.NewLine +
            ",       A.LOC_CODE                                                             " + Environment.NewLine +
            ",       A.ORDERBY                                                              " + Environment.NewLine +
            "FROM    WV_SYSTEM_MAPPING   A                                                  " + Environment.NewLine +
            ",       CM_LOCATION         B                                                  " + Environment.NewLine +
            ",       WV_SYSTEM_MASTER    C                                                  " + Environment.NewLine +
            "WHERE   A.LOC_CODE          = B.LOC_CODE                                       " + Environment.NewLine +
            "AND     A.SYSTEM_CODE       = C.SYSTEM_CODE                                    " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            "ORDER BY ORD1                                                                  ";
        #endregion

        /// <summary>
        /// 계통정보 목록 조회 (WV_SYSTEM_MASTER에 정보가 없을 경우 사용)
        /// </summary>
        #region SYSTEM_CM_LOCATION_SELECT
        public static string SYSTEM_CM_LOCATION_SELECT = "" +
            "SELECT  '0' IS_TEMP                                                            " + Environment.NewLine +
            ",       NULL SYSTEM_CODE1                                                      " + Environment.NewLine +
            ",       LOC_NAME SYSTEM_NAME1                                                  " + Environment.NewLine +
            ",       LOC_CODE LOC_CODE1                                                     " + Environment.NewLine +
            ",       ORDERBY ORDERBY1                                                       " + Environment.NewLine +
            ",       LPAD(TO_CHAR(ORDERBY),3,'0')||'000' ORD1                               " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       NULL SYSTEM_CODE2                                                      " + Environment.NewLine +
            ",       NULL SYSTEM_NAME2                                                      " + Environment.NewLine +
            ",       NULL LOC_CODE2                                                         " + Environment.NewLine +
            ",       NULL ORDERBY2                                                          " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       '1' LV                                                                 " + Environment.NewLine +
            ",       NULL SYSTEM_CODE                                                       " + Environment.NewLine +
            ",       LOC_NAME SYSTEM_NAME                                                   " + Environment.NewLine +
            ",       LOC_CODE                                                               " + Environment.NewLine +
            ",       ORDERBY                                                                " + Environment.NewLine +
            "FROM    CM_LOCATION                                                            " + Environment.NewLine +
            "WHERE   LOC_GBN = '중블록'                                                     " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            "UNION ALL                                                                      " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            "SELECT  '0' IS_TEMP                                                            " + Environment.NewLine +
            ",       NULL SYSTEM_CODE1                                                      " + Environment.NewLine +
            ",       NULL SYSTEM_NAME1                                                      " + Environment.NewLine +
            ",       NULL LOC_CODE1                                                         " + Environment.NewLine +
            ",       NULL ORDERBY1                                                          " + Environment.NewLine +
            ",       LPAD(TO_CHAR(B.ORDERBY),3,'0')||LPAD(TO_CHAR(A.ORDERBY),3,'0') ORD1    " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       NULL SYSTEM_CODE2                                                      " + Environment.NewLine +
            ",       A.LOC_NAME SYSTEM_NAME2                                                " + Environment.NewLine +
            ",       A.LOC_CODE LOC_CODE2                                                   " + Environment.NewLine +
            ",       A.ORDERBY ORDERBY2                                                     " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            ",       '2' LV                                                                 " + Environment.NewLine +
            ",       NULL SYSTEM_CODE                                                       " + Environment.NewLine +
            ",       A.LOC_NAME SYSTEM_NAME                                                 " + Environment.NewLine +
            ",       A.LOC_CODE                                                             " + Environment.NewLine +
            ",       A.ORDERBY                                                              " + Environment.NewLine +
            "FROM    CM_LOCATION A                                                          " + Environment.NewLine +
            ",       CM_LOCATION B                                                          " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            "WHERE   A.LOC_GBN = '소블록'                                                   " + Environment.NewLine +
            "AND     B.LOC_GBN = '중블록'                                                   " + Environment.NewLine +
            "AND     A.PLOC_CODE = B.LOC_CODE                                               " + Environment.NewLine +
            "                                                                               " + Environment.NewLine +
            "ORDER BY ORD1                                                                  ";
        #endregion

        /// <summary>
        /// 중블록 저장 처리
        /// </summary>
        #region SYSTEM_MASTER_INS_UPD
        public static string SYSTEM_MASTER_INS_UPD = "" +
            "MERGE INTO WV_SYSTEM_MASTER T  " + Environment.NewLine +
            "USING                          " + Environment.NewLine +
            "(                              " + Environment.NewLine +
            "    SELECT  COUNT(*) CNT       " + Environment.NewLine +
            "    FROM    WV_SYSTEM_MASTER   " + Environment.NewLine +
            "    WHERE   SYSTEM_CODE = :1   " + Environment.NewLine +
            "    AND     LOC_CODE    = :2   " + Environment.NewLine +
            ") S ON (S.CNT > 0)             " + Environment.NewLine +
            "WHEN MATCHED THEN              " + Environment.NewLine +
            "    UPDATE                     " + Environment.NewLine +
            "    SET     SYSTEM_NAME = :3   " + Environment.NewLine +
            "    ,       LOC_CODE    = :4   " + Environment.NewLine +
            "    ,       ORDERBY     = :5   " + Environment.NewLine +
            "    WHERE   SYSTEM_CODE = :6   " + Environment.NewLine +
            "    AND     LOC_CODE    = :7   " + Environment.NewLine +
            "WHEN NOT MATCHED THEN          " + Environment.NewLine +
            "    INSERT                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        SYSTEM_CODE            " + Environment.NewLine +
            "    ,   SYSTEM_NAME            " + Environment.NewLine +
            "    ,   LOC_CODE               " + Environment.NewLine +
            "    ,   ORDERBY                " + Environment.NewLine +
            "    )                          " + Environment.NewLine +
            "    VALUES                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        :8                     " + Environment.NewLine +
            "    ,   :9                     " + Environment.NewLine +
            "    ,   :10                    " + Environment.NewLine +
            "    ,   :11                    " + Environment.NewLine +
            "    )                          "; 
        #endregion

        #region SYSTEM_MASTER_DELETE
		public static string SYSTEM_MASTER_DELETE = "" +
            "DELETE                     " + Environment.NewLine +
            "FROM   WV_SYSTEM_MASTER    " + Environment.NewLine +
            "WHERE  SYSTEM_CODE = :1    " + Environment.NewLine +
            "AND    LOC_CODE    = :2    "; 
	    #endregion

        #region SYSTEM_MAPPING_INS_UPD
        public static string SYSTEM_MAPPING_INS_UPD = "" +
            "MERGE INTO WV_SYSTEM_MAPPING T " + Environment.NewLine +
            "USING                          " + Environment.NewLine +
            "(                              " + Environment.NewLine +
            "    SELECT  COUNT(*) CNT       " + Environment.NewLine +
            "    FROM    WV_SYSTEM_MAPPING  " + Environment.NewLine +
            "    WHERE   SYSTEM_CODE = :1   " + Environment.NewLine +
            "    AND     LOC_CODE    = :2   " + Environment.NewLine +
            ") S ON (S.CNT > 0)             " + Environment.NewLine +
            "WHEN MATCHED THEN              " + Environment.NewLine +
            "    UPDATE                     " + Environment.NewLine +
            "    SET     SYSTEM_CODE = :3   " + Environment.NewLine +
            "    ,       ORDERBY     = :4   " + Environment.NewLine +
            "    WHERE   SYSTEM_CODE = :5   " + Environment.NewLine +
            "    AND     LOC_CODE    = :6   " + Environment.NewLine +
            "WHEN NOT MATCHED THEN          " + Environment.NewLine +
            "    INSERT                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        SYSTEM_CODE            " + Environment.NewLine +
            "    ,   LOC_CODE               " + Environment.NewLine +
            "    ,   ORDERBY                " + Environment.NewLine +
            "    )                          " + Environment.NewLine +
            "    VALUES                     " + Environment.NewLine +
            "    (                          " + Environment.NewLine +
            "        :7                     " + Environment.NewLine +
            "    ,   :8                     " + Environment.NewLine +
            "    ,   :9                     " + Environment.NewLine +
            "    )                          ";
	    #endregion

        #region SYSTEM_MAPPING_DELETE
        public static string SYSTEM_MAPPING_DELETE = "" +
            "DELETE                     " + Environment.NewLine +
            "FROM   WV_SYSTEM_MAPPING   " + Environment.NewLine +
            "WHERE  SYSTEM_CODE = :1    " + Environment.NewLine +
            "AND    LOC_CODE    = :2    ";  
        #endregion

        /// <summary>
        /// SYSTEM_CODE(중블록) 삭제시 처리
        /// </summary>
        #region SYSTEM_MAPPING_DELETE_SYSTEM
        public static string SYSTEM_MAPPING_DELETE_SYSTEM = "" +
            "DELETE                     " + Environment.NewLine +
            "FROM   WV_SYSTEM_MAPPING   " + Environment.NewLine +
            "WHERE  SYSTEM_CODE = :1    "; 
        #endregion

        #endregion

        #region Excel Upload
        /// <summary>
        /// 모든 사용자 테이블 조회
        /// </summary>
        public static string EXCEL_ALL_TABLES = "" + Environment.NewLine +
            "SELECT TABLE_NAME  " + Environment.NewLine +
            "FROM   USER_TABLES " + Environment.NewLine +
            "ORDER BY TABLE_NAME";

        /// <summary>
        /// 테이블의 모든 컬럼 정보 조회
        /// </summary>
        public static string EXCEL_ALL_COLUMNS = "" + Environment.NewLine +
            "SELECT  COLUMN_NAME            " + Environment.NewLine +
            ",       DATA_TYPE              " + Environment.NewLine +
            ",       DATA_LENGTH            " + Environment.NewLine +
            ",       NULLABLE               " + Environment.NewLine +
            "FROM    DBA_TAB_COLUMNS        " + Environment.NewLine +
            "WHERE   OWNER       = '{0}'    " + Environment.NewLine +
            "AND     TABLE_NAME  = '{1}'    " + Environment.NewLine +
            "ORDER BY COLUMN_ID             ";
        #endregion

    }
}
