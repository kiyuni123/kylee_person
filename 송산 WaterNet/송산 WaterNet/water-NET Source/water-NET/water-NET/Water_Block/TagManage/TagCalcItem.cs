﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp.TagManage
{
    /// <summary>
    /// 계산 태그 아이템 (가상태그 아이템에 목록으로 관리됨)
    /// BlockTagCalcItem.태그구분List로써 관리됨
    /// </summary>
    public class TagCalcItem
    {
        /// <summary>
        /// 지역관리코드
        /// </summary>
        public string LOC_CODE { get; set; }
        /// <summary>
        /// 지역관리명
        /// </summary>
        public string LOC_NAME { get; set; }
        /// <summary>
        /// 태그일련번호
        /// </summary>
        public string TAGNAME { get; set; }
        /// <summary>
        /// 태그설명
        /// </summary>
        public string DESCRIPTION { get; set; }
        /// <summary>
        /// 태그구분
        /// </summary>
        public string TAG_GBN { get; set; }

        /// <summary>
        /// [계산식] 기호 (P,M)
        /// </summary>
        public string CAL_GBN { get; set; }
        /// <summary>
        /// [계산식] 변경전 기호 (P,M)
        /// </summary>
        public string CAL_GBN_ORG { get; set; }
        /// <summary>
        /// [계산식] 가상태그구분 (CAL)
        /// </summary>
        public string DUMMY_GBN { get; set; }
        /// <summary>
        /// [계산식] 태그일련번호
        /// </summary>
        public string CTAGNAME { get; set; }
        /// <summary>
        /// [계산식] 태그설명
        /// </summary>
        public string TAG_DESC { get; set; }
        /// <summary>
        /// [계산식] 계수
        /// </summary>
        public string COEFFICIENT { get; set; }

        /// <summary>
        /// 상태 구분
        /// </summary>
        public enum StateType
        {
            None = 0,   // None - 아무런 동작 없음
            New = 1,    // Insert - 신규
            Mod = 2,    // Update - 변경
            Del = 3,    // Delete - 삭제
        }

        /// <summary>
        /// 상태
        /// </summary>
        public StateType State { get; set; }

        /// <summary>
        /// DB 문자를 계산 기호로 반환
        /// </summary>
        /// <param name="calGbn">P or M</param>
        /// <returns>+ or - or null</returns>
        public static string toCalStr(string calGbn)
        {
            if (calGbn.ToUpper() == "P") return "+";
            else if (calGbn.ToUpper() == "M") return "-";
            else return calGbn;
        }

        /// <summary>
        /// 계산 기호를 DB 문자로 반환
        /// </summary>
        /// <param name="calStr">+ or - or null</param>
        /// <returns>P or M</returns>
        public static string toStrCal(string calStr)
        {
            if (calStr == "+") return "P";
            else if (calStr == "-") return "M";
            else return "";
        }

        public TagCalcItem()
        {
            this.State = StateType.None;
        }
    }
}
