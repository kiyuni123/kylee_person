﻿namespace WaterNet.BlockApp.TagManage
{
    partial class FrmTagManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAGNAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CAL_GBN", -1, 7927098);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CTAGNAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DESCRIPTION");
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("COEFFICIENT", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, false);
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueList valueList1 = new Infragistics.Win.ValueList(7927098);
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn1 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn2 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn3 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn4 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn5 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn6 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn7 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn8 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn9 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn10 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn11 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn12 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn13 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn14 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn15 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn16 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn17 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn18 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn19 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn20 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn21 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn22 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn23 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn24 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn25 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn26 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn27 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn28 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn29 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn30 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn31 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn32 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn33 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn34 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn35 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn36 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn37 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn38 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn39 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn40 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn41 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn42 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn43 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn44 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn45 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn46 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn47 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn48 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn49 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn50 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn51 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn52 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn53 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn54 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn55 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn56 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn57 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn58 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn59 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn60 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn61 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn62 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn63 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn64 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn65 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn37 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn38 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn39 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn57 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn58 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TYPE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn59 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn60 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LV");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DESCRIPTION");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAGNAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn65 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_GBN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn66 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AUTO_TAG_GBN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn67 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RT", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, false);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn68 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRI");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn69 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TT");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn70 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("YD");
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn71 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("YD_R");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn72 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TD");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn73 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MNF");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn74 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRQ");
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn75 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRQ_I");
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn76 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRQ_O");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn77 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPL_D");
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn78 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPL_I");
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn79 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPL_O");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn80 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEI");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn81 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PHI");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn82 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CLI");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn83 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TBI");
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn84 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PRI");
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn85 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PMB");
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn66 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn67 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn68 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn69 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn70 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn71 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn72 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn73 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn74 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn75 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn76 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn77 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn78 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn79 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn80 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("AUTO_TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn81 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn82 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn83 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn84 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn85 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn86 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn87 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn88 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn89 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn90 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn91 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn92 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn93 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn94 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn95 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn96 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn97 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn98 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn99 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand3 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn86 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_TAGNAME");
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn87 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_DESCRIPTION");
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn88 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_USE_YN");
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn89 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_COEFFICIENT");
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn90 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn91 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_LOC_CODE2");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn92 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_HTAGNAME", 0);
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance241 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance242 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance243 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance244 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance245 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance246 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance247 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance248 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance249 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance250 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance251 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn100 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn101 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn102 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_USE_YN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn103 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn104 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn105 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_LOC_CODE2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn106 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn107 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn108 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn109 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn110 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn111 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn112 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn113 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn114 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn115 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn116 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn117 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn118 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn119 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn120 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn121 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn122 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn123 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn124 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn125 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn126 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn127 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn128 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn129 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn130 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn131 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn132 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn133 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn134 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn135 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn136 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn137 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn138 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn139 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn140 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn141 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn142 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn143 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn144 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn145 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn146 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn147 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn148 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEVEL");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn149 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn150 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn151 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn152 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn153 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn154 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn155 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn156 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn157 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn158 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn159 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn160 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn161 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn162 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn163 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn164 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn165 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn166 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn167 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn168 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn169 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn170 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn171 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn172 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn173 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn174 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn175 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn176 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn177 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn178 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn179 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn180 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEVEL");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn181 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn182 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn183 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn184 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn185 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn186 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn187 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn188 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn189 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn190 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn191 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn192 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn193 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn194 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn195 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn196 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn197 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn198 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn199 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn200 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn201 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn202 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn203 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn204 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn205 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn206 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn207 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn208 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn209 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn210 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn211 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn212 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn213 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn214 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn215 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn216 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn217 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn218 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn219 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn220 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn221 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn222 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn223 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn224 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn225 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn226 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn227 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn228 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn229 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn230 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn231 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn232 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn233 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn234 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn235 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn236 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn237 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn238 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn239 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn240 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn241 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn242 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn243 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn244 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn245 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn246 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn247 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn248 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn249 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn250 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn251 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn252 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn253 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn254 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn255 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn256 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn257 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn258 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn259 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn260 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn261 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn262 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn263 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn264 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn265 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn266 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn267 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn268 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn269 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn270 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn271 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn272 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn273 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn274 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn275 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn276 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn277 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn278 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn279 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn280 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn281 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn282 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn283 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn284 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn285 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn286 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn287 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn288 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn289 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn290 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn291 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn292 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn293 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn294 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn295 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn296 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn297 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn298 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn299 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn300 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn301 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn302 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn303 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn304 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn305 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn306 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn307 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn308 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn309 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn310 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn311 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn312 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn313 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn314 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn315 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn316 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn317 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn318 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn319 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn320 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn321 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn322 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn323 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn324 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn325 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn326 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn327 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn328 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn329 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn330 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn331 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn332 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn333 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn334 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn335 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn336 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn337 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_ORG");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn338 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn339 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn340 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn341 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn342 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TYPE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn343 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn344 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CM_LOC_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn345 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn346 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn347 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn348 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn349 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn350 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("RT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn351 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn352 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn353 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn354 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn355 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn356 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("MNF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn357 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn358 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn359 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn360 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn361 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn362 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn363 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LEI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn364 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PHI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn365 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CLI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn366 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TBI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn367 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn368 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("PMB");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn369 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn370 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn371 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_USE_YN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn372 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn373 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn374 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_LOC_CODE2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn375 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn376 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn377 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_USE_YN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn378 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn379 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn380 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_LOC_CODE2");
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.dgvCalcTag = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraDataSource13 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource14 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource15 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource16 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource17 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource18 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource19 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource20 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource21 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource22 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource23 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource24 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource25 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvBlockTag = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraDataSource9 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.txtHtagname = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbUseYn = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTagname = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCoefficient = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocCode = new System.Windows.Forms.TextBox();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtFtrIdn = new System.Windows.Forms.TextBox();
            this.txtLocCode2 = new System.Windows.Forms.TextBox();
            this.tabTagCal = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnDummyTagDel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnReLoad = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnShowTag = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTagSearch = new System.Windows.Forms.Label();
            this.dgvTag = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraDataSource12 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTagHTagname = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbTagUseYn = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTagTagname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTagCoefficient = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTagLocCode = new System.Windows.Forms.TextBox();
            this.txtTagDesc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTagFtrIdn = new System.Windows.Forms.TextBox();
            this.txtTagLocCode2 = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnTagEdit = new System.Windows.Forms.Button();
            this.btnTagNew = new System.Windows.Forms.Button();
            this.btnTagSave = new System.Windows.Forms.Button();
            this.btnTagDel = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.cbBlock = new System.Windows.Forms.ComboBox();
            this.btnSearchTag = new System.Windows.Forms.Button();
            this.txtSearchTagDesc = new System.Windows.Forms.TextBox();
            this.txtSearchTagname = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtLabel10 = new System.Windows.Forms.Label();
            this.ultraDataSource5 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource1 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource2 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource3 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource4 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource6 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource7 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource8 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource10 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource11 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.dgvCalcSPL_O = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcSPL_I = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcSPL_D = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcFRQ_O = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcFRQ_I = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcFRI = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcFRQ = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcMNF = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcTT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcYD_R = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dgvCalcYD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource25)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlockTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource9)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabTagCal)).BeginInit();
            this.tabTagCal.SuspendLayout();
            this.ultraTabSharedControlsPage1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource12)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcSPL_O)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcSPL_I)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcSPL_D)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRQ_O)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRQ_I)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcMNF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcYD_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcYD)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.dgvCalcTag);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(509, 254);
            // 
            // dgvCalcTag
            // 
            this.dgvCalcTag.AllowDrop = true;
            this.dgvCalcTag.DataSource = this.ultraDataSource13;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgvCalcTag.DisplayLayout.Appearance = appearance35;
            ultraGridColumn1.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn1.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn1.Header.Fixed = true;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.Hidden = true;
            ultraGridColumn2.Header.Caption = "기호";
            ultraGridColumn2.Header.Fixed = true;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn2.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn2.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(132, 0);
            ultraGridColumn2.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn2.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn2.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            ultraGridColumn3.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn3.Header.Caption = "태그일련번호";
            ultraGridColumn3.Header.Fixed = true;
            ultraGridColumn3.Header.VisiblePosition = 2;
            ultraGridColumn3.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn3.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn3.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(112, 0);
            ultraGridColumn3.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn3.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn4.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance1.TextVAlignAsString = "Middle";
            ultraGridColumn4.CellAppearance = appearance1;
            ultraGridColumn4.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn4.Header.Caption = "태그설명";
            ultraGridColumn4.Header.Fixed = true;
            ultraGridColumn4.Header.VisiblePosition = 4;
            ultraGridColumn4.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn4.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn4.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn4.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn5.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn5.Header.Caption = "계수";
            ultraGridColumn5.Header.Fixed = true;
            ultraGridColumn5.Header.VisiblePosition = 3;
            ultraGridColumn5.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn5.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn5.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn5.RowLayoutColumnInfo.SpanY = 2;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            this.dgvCalcTag.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.dgvCalcTag.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvCalcTag.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvCalcTag.DisplayLayout.GroupByBox.Appearance = appearance37;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvCalcTag.DisplayLayout.GroupByBox.BandLabelAppearance = appearance38;
            this.dgvCalcTag.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvCalcTag.DisplayLayout.GroupByBox.Hidden = true;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance39.BackColor2 = System.Drawing.SystemColors.Control;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvCalcTag.DisplayLayout.GroupByBox.PromptAppearance = appearance39;
            this.dgvCalcTag.DisplayLayout.MaxColScrollRegions = 1;
            this.dgvCalcTag.DisplayLayout.MaxRowScrollRegions = 1;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgvCalcTag.DisplayLayout.Override.ActiveCellAppearance = appearance40;
            appearance41.BackColor = System.Drawing.SystemColors.Highlight;
            appearance41.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvCalcTag.DisplayLayout.Override.ActiveRowAppearance = appearance41;
            this.dgvCalcTag.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.dgvCalcTag.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.dgvCalcTag.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.dgvCalcTag.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            this.dgvCalcTag.DisplayLayout.Override.CardAreaAppearance = appearance42;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.dgvCalcTag.DisplayLayout.Override.CellAppearance = appearance43;
            this.dgvCalcTag.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.dgvCalcTag.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvCalcTag.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.dgvCalcTag.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.dgvCalcTag.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.dgvCalcTag.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.dgvCalcTag.DisplayLayout.Override.RowAppearance = appearance47;
            this.dgvCalcTag.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.dgvCalcTag.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvCalcTag.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.dgvCalcTag.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.dgvCalcTag.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.dgvCalcTag.DisplayLayout.UseFixedHeaders = true;
            valueList1.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            valueListItem1.DataValue = "";
            valueListItem2.DataValue = "P";
            valueListItem2.DisplayText = "+";
            valueListItem3.DataValue = "M";
            valueListItem3.DisplayText = "-";
            valueList1.ValueListItems.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.dgvCalcTag.DisplayLayout.ValueLists.AddRange(new Infragistics.Win.ValueList[] {
            valueList1});
            this.dgvCalcTag.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.dgvCalcTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcTag.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcTag.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcTag.Name = "dgvCalcTag";
            this.dgvCalcTag.Size = new System.Drawing.Size(509, 254);
            this.dgvCalcTag.TabIndex = 6;
            this.dgvCalcTag.Text = "ultraGrid1";
            // 
            // ultraDataSource13
            // 
            this.ultraDataSource13.Band.Columns.AddRange(new object[] {
            ultraDataColumn1,
            ultraDataColumn2,
            ultraDataColumn3,
            ultraDataColumn4,
            ultraDataColumn5});
            // 
            // ultraDataSource14
            // 
            this.ultraDataSource14.Band.Columns.AddRange(new object[] {
            ultraDataColumn6,
            ultraDataColumn7,
            ultraDataColumn8,
            ultraDataColumn9,
            ultraDataColumn10});
            // 
            // ultraDataSource15
            // 
            this.ultraDataSource15.Band.Columns.AddRange(new object[] {
            ultraDataColumn11,
            ultraDataColumn12,
            ultraDataColumn13,
            ultraDataColumn14,
            ultraDataColumn15});
            // 
            // ultraDataSource16
            // 
            this.ultraDataSource16.Band.Columns.AddRange(new object[] {
            ultraDataColumn16,
            ultraDataColumn17,
            ultraDataColumn18,
            ultraDataColumn19,
            ultraDataColumn20});
            // 
            // ultraDataSource17
            // 
            this.ultraDataSource17.Band.Columns.AddRange(new object[] {
            ultraDataColumn21,
            ultraDataColumn22,
            ultraDataColumn23,
            ultraDataColumn24,
            ultraDataColumn25});
            // 
            // ultraDataSource18
            // 
            this.ultraDataSource18.Band.Columns.AddRange(new object[] {
            ultraDataColumn26,
            ultraDataColumn27,
            ultraDataColumn28,
            ultraDataColumn29,
            ultraDataColumn30});
            // 
            // ultraDataSource19
            // 
            this.ultraDataSource19.Band.Columns.AddRange(new object[] {
            ultraDataColumn31,
            ultraDataColumn32,
            ultraDataColumn33,
            ultraDataColumn34,
            ultraDataColumn35});
            // 
            // ultraDataSource20
            // 
            this.ultraDataSource20.Band.Columns.AddRange(new object[] {
            ultraDataColumn36,
            ultraDataColumn37,
            ultraDataColumn38,
            ultraDataColumn39,
            ultraDataColumn40});
            // 
            // ultraDataSource21
            // 
            this.ultraDataSource21.Band.Columns.AddRange(new object[] {
            ultraDataColumn41,
            ultraDataColumn42,
            ultraDataColumn43,
            ultraDataColumn44,
            ultraDataColumn45});
            // 
            // ultraDataSource22
            // 
            this.ultraDataSource22.Band.Columns.AddRange(new object[] {
            ultraDataColumn46,
            ultraDataColumn47,
            ultraDataColumn48,
            ultraDataColumn49,
            ultraDataColumn50});
            // 
            // ultraDataSource23
            // 
            this.ultraDataSource23.Band.Columns.AddRange(new object[] {
            ultraDataColumn51,
            ultraDataColumn52,
            ultraDataColumn53,
            ultraDataColumn54,
            ultraDataColumn55});
            // 
            // ultraDataSource24
            // 
            this.ultraDataSource24.Band.Columns.AddRange(new object[] {
            ultraDataColumn56,
            ultraDataColumn57,
            ultraDataColumn58,
            ultraDataColumn59,
            ultraDataColumn60});
            // 
            // ultraDataSource25
            // 
            this.ultraDataSource25.Band.Columns.AddRange(new object[] {
            ultraDataColumn61,
            ultraDataColumn62,
            ultraDataColumn63,
            ultraDataColumn64,
            ultraDataColumn65});
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1647, 5);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 830);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1642, 5);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1642, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(5, 830);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(5, 825);
            this.panel4.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(5, 5);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvBlockTag);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.panel7);
            this.splitContainer1.Panel1.Controls.Add(this.panel5);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblTagSearch);
            this.splitContainer1.Panel2.Controls.Add(this.dgvTag);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Controls.Add(this.panel8);
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Size = new System.Drawing.Size(1637, 825);
            this.splitContainer1.SplitterDistance = 950;
            this.splitContainer1.TabIndex = 4;
            // 
            // dgvBlockTag
            // 
            this.dgvBlockTag.AllowDrop = true;
            this.dgvBlockTag.DataSource = this.ultraDataSource9;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgvBlockTag.DisplayLayout.Appearance = appearance29;
            ultraGridBand2.ColHeaderLines = 2;
            ultraGridColumn35.Header.Fixed = true;
            ultraGridColumn35.Header.VisiblePosition = 0;
            ultraGridColumn35.Hidden = true;
            ultraGridColumn35.RowLayoutColumnInfo.OriginX = 52;
            ultraGridColumn35.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn35.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 48);
            ultraGridColumn35.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn35.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn36.Header.Fixed = true;
            ultraGridColumn36.Header.VisiblePosition = 1;
            ultraGridColumn36.Hidden = true;
            ultraGridColumn36.RowLayoutColumnInfo.OriginX = 54;
            ultraGridColumn36.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn36.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 48);
            ultraGridColumn36.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn36.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn37.Header.Caption = "구 분";
            ultraGridColumn37.Header.Fixed = true;
            ultraGridColumn37.Header.VisiblePosition = 2;
            ultraGridColumn37.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.LabelOnly;
            ultraGridColumn37.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn37.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn37.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(336, 0);
            ultraGridColumn37.RowLayoutColumnInfo.SpanX = 6;
            ultraGridColumn37.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn38.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn38.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn38.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn38.Header.Caption = "대";
            ultraGridColumn38.Header.Fixed = true;
            ultraGridColumn38.Header.VisiblePosition = 3;
            ultraGridColumn38.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn38.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn38.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn38.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn38.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn39.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn39.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn39.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn39.Header.Caption = "중";
            ultraGridColumn39.Header.Fixed = true;
            ultraGridColumn39.Header.VisiblePosition = 4;
            ultraGridColumn39.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn39.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn39.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn39.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn39.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn57.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn57.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn57.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn57.Header.Caption = "소";
            ultraGridColumn57.Header.Fixed = true;
            ultraGridColumn57.Header.VisiblePosition = 5;
            ultraGridColumn57.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn57.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn57.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn57.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn57.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn58.Header.Fixed = true;
            ultraGridColumn58.Header.VisiblePosition = 6;
            ultraGridColumn58.Hidden = true;
            ultraGridColumn58.RowLayoutColumnInfo.OriginX = 56;
            ultraGridColumn58.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn58.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn58.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn59.Header.Fixed = true;
            ultraGridColumn59.Header.VisiblePosition = 7;
            ultraGridColumn59.Hidden = true;
            ultraGridColumn59.RowLayoutColumnInfo.OriginX = 58;
            ultraGridColumn59.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn59.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn59.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn60.Header.Fixed = true;
            ultraGridColumn60.Header.VisiblePosition = 8;
            ultraGridColumn60.Hidden = true;
            ultraGridColumn60.RowLayoutColumnInfo.OriginX = 60;
            ultraGridColumn60.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn60.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn60.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn61.Header.Caption = "LEVEL";
            ultraGridColumn61.Header.Fixed = true;
            ultraGridColumn61.Header.VisiblePosition = 9;
            ultraGridColumn61.Hidden = true;
            ultraGridColumn61.RowLayoutColumnInfo.OriginX = 50;
            ultraGridColumn61.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn61.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn61.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn62.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn62.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn62.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn62.Header.Caption = "태그설명";
            ultraGridColumn62.Header.Fixed = true;
            ultraGridColumn62.Header.VisiblePosition = 10;
            ultraGridColumn62.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn62.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn62.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(254, 20);
            ultraGridColumn62.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 48);
            ultraGridColumn62.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn62.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn63.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn63.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn63.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn63.Header.Caption = "태그일련번호";
            ultraGridColumn63.Header.Fixed = true;
            ultraGridColumn63.Header.VisiblePosition = 11;
            ultraGridColumn63.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn63.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn63.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(153, 20);
            ultraGridColumn63.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn63.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn63.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn64.DefaultCellValue = "False";
            ultraGridColumn64.Header.Caption = "가상구분";
            ultraGridColumn64.Header.VisiblePosition = 12;
            ultraGridColumn64.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn64.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn64.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(62, 20);
            ultraGridColumn64.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 48);
            ultraGridColumn64.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn64.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn64.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn65.Header.Caption = "태그구분";
            ultraGridColumn65.Header.VisiblePosition = 13;
            ultraGridColumn65.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.LabelOnly;
            ultraGridColumn65.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn65.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn65.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(2128, 0);
            ultraGridColumn65.RowLayoutColumnInfo.SpanX = 38;
            ultraGridColumn65.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn66.Header.Caption = "자동TAG";
            ultraGridColumn66.Header.VisiblePosition = 14;
            ultraGridColumn66.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn66.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn66.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn66.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn66.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            ultraGridColumn66.TabStop = false;
            ultraGridColumn66.VertScrollBar = true;
            ultraGridColumn67.DefaultCellValue = "False";
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            ultraGridColumn67.Header.Appearance = appearance2;
            ultraGridColumn67.Header.Caption = "순시유량\n(메인화면)";
            ultraGridColumn67.Header.VisiblePosition = 15;
            ultraGridColumn67.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn67.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn67.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(85, 20);
            ultraGridColumn67.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn67.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn67.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn68.DefaultCellValue = "False";
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            ultraGridColumn68.Header.Appearance = appearance3;
            ultraGridColumn68.Header.Caption = "순시유량";
            ultraGridColumn68.Header.VisiblePosition = 16;
            ultraGridColumn68.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn68.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn68.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(67, 20);
            ultraGridColumn68.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn68.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn68.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn69.DefaultCellValue = "False";
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            ultraGridColumn69.Header.Appearance = appearance4;
            ultraGridColumn69.Header.Caption = "적산";
            ultraGridColumn69.Header.VisiblePosition = 17;
            ultraGridColumn69.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn69.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn69.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(41, 20);
            ultraGridColumn69.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn69.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn69.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn70.DefaultCellValue = "False";
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            ultraGridColumn70.Header.Appearance = appearance5;
            ultraGridColumn70.Header.Caption = "전일적산";
            ultraGridColumn70.Header.VisiblePosition = 18;
            ultraGridColumn70.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn70.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn70.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(65, 20);
            ultraGridColumn70.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn70.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn70.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn71.DefaultCellValue = "False";
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            ultraGridColumn71.Header.Appearance = appearance6;
            ultraGridColumn71.Header.Caption = "배수지유출\n전일적산";
            ultraGridColumn71.Header.VisiblePosition = 19;
            ultraGridColumn71.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn71.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn71.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn71.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn71.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn71.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn72.DefaultCellValue = "False";
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            ultraGridColumn72.Header.Appearance = appearance7;
            ultraGridColumn72.Header.Caption = "적산\n(메인화면)";
            ultraGridColumn72.Header.VisiblePosition = 20;
            ultraGridColumn72.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn72.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn72.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(120, 20);
            ultraGridColumn72.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn72.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn72.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn73.DefaultCellValue = "False";
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            ultraGridColumn73.Header.Appearance = appearance8;
            ultraGridColumn73.Header.Caption = "블록별\n야간최소유량";
            ultraGridColumn73.Header.VisiblePosition = 21;
            ultraGridColumn73.RowLayoutColumnInfo.OriginX = 26;
            ultraGridColumn73.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn73.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn73.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn73.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn73.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn74.DefaultCellValue = "False";
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            ultraGridColumn74.Header.Appearance = appearance9;
            ultraGridColumn74.Header.Caption = "1시간적산\n공급";
            ultraGridColumn74.Header.VisiblePosition = 22;
            ultraGridColumn74.RowLayoutColumnInfo.OriginX = 28;
            ultraGridColumn74.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn74.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn74.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn74.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn74.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn75.DefaultCellValue = "False";
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            ultraGridColumn75.Header.Appearance = appearance10;
            ultraGridColumn75.Header.Caption = "1시간적산\n배수지유입";
            ultraGridColumn75.Header.VisiblePosition = 23;
            ultraGridColumn75.RowLayoutColumnInfo.OriginX = 30;
            ultraGridColumn75.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn75.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn75.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn75.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn75.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn76.DefaultCellValue = "False";
            appearance11.TextHAlignAsString = "Center";
            appearance11.TextVAlignAsString = "Middle";
            ultraGridColumn76.Header.Appearance = appearance11;
            ultraGridColumn76.Header.Caption = "1시간적산\n배수지유출";
            ultraGridColumn76.Header.VisiblePosition = 24;
            ultraGridColumn76.RowLayoutColumnInfo.OriginX = 32;
            ultraGridColumn76.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn76.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn76.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn76.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn76.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn77.DefaultCellValue = "False";
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            ultraGridColumn77.Header.Appearance = appearance12;
            ultraGridColumn77.Header.Caption = "분기유입\n일적산";
            ultraGridColumn77.Header.VisiblePosition = 25;
            ultraGridColumn77.RowLayoutColumnInfo.OriginX = 34;
            ultraGridColumn77.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn77.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn77.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn77.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn77.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn78.DefaultCellValue = "False";
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            ultraGridColumn78.Header.Appearance = appearance13;
            ultraGridColumn78.Header.Caption = "배수지유입\n일적산";
            ultraGridColumn78.Header.VisiblePosition = 26;
            ultraGridColumn78.RowLayoutColumnInfo.OriginX = 36;
            ultraGridColumn78.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn78.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn78.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn78.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn78.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn79.DefaultCellValue = "False";
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            ultraGridColumn79.Header.Appearance = appearance14;
            ultraGridColumn79.Header.Caption = "배수지유출\n일적산";
            ultraGridColumn79.Header.VisiblePosition = 27;
            ultraGridColumn79.RowLayoutColumnInfo.OriginX = 38;
            ultraGridColumn79.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn79.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn79.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn79.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn79.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn80.DefaultCellValue = "False";
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            ultraGridColumn80.Header.Appearance = appearance15;
            ultraGridColumn80.Header.Caption = "배수지수위";
            ultraGridColumn80.Header.VisiblePosition = 28;
            ultraGridColumn80.RowLayoutColumnInfo.OriginX = 40;
            ultraGridColumn80.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn80.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn80.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn80.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn80.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn81.DefaultCellValue = "False";
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            ultraGridColumn81.Header.Appearance = appearance16;
            ultraGridColumn81.Header.Caption = "PH값";
            ultraGridColumn81.Header.VisiblePosition = 29;
            ultraGridColumn81.RowLayoutColumnInfo.OriginX = 42;
            ultraGridColumn81.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn81.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn81.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn81.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn81.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn82.DefaultCellValue = "False";
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            ultraGridColumn82.Header.Appearance = appearance17;
            ultraGridColumn82.Header.Caption = "잔류염소";
            ultraGridColumn82.Header.VisiblePosition = 30;
            ultraGridColumn82.RowLayoutColumnInfo.OriginX = 44;
            ultraGridColumn82.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn82.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn82.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn82.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn82.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn83.DefaultCellValue = "False";
            appearance18.TextHAlignAsString = "Center";
            appearance18.TextVAlignAsString = "Middle";
            ultraGridColumn83.Header.Appearance = appearance18;
            ultraGridColumn83.Header.Caption = "탁도";
            ultraGridColumn83.Header.VisiblePosition = 31;
            ultraGridColumn83.RowLayoutColumnInfo.OriginX = 46;
            ultraGridColumn83.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn83.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn83.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn83.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn83.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn84.DefaultCellValue = "False";
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            ultraGridColumn84.Header.Appearance = appearance19;
            ultraGridColumn84.Header.Caption = "압력순시";
            ultraGridColumn84.Header.VisiblePosition = 32;
            ultraGridColumn84.RowLayoutColumnInfo.OriginX = 48;
            ultraGridColumn84.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn84.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn84.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn84.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn84.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn85.DefaultCellValue = "False";
            appearance20.TextHAlignAsString = "Center";
            appearance20.TextVAlignAsString = "Middle";
            ultraGridColumn85.Header.Appearance = appearance20;
            ultraGridColumn85.Header.Caption = "온도";
            ultraGridColumn85.Header.VisiblePosition = 33;
            ultraGridColumn85.RowLayoutColumnInfo.OriginX = 50;
            ultraGridColumn85.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn85.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn85.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn85.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn85.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn35,
            ultraGridColumn36,
            ultraGridColumn37,
            ultraGridColumn38,
            ultraGridColumn39,
            ultraGridColumn57,
            ultraGridColumn58,
            ultraGridColumn59,
            ultraGridColumn60,
            ultraGridColumn61,
            ultraGridColumn62,
            ultraGridColumn63,
            ultraGridColumn64,
            ultraGridColumn65,
            ultraGridColumn66,
            ultraGridColumn67,
            ultraGridColumn68,
            ultraGridColumn69,
            ultraGridColumn70,
            ultraGridColumn71,
            ultraGridColumn72,
            ultraGridColumn73,
            ultraGridColumn74,
            ultraGridColumn75,
            ultraGridColumn76,
            ultraGridColumn77,
            ultraGridColumn78,
            ultraGridColumn79,
            ultraGridColumn80,
            ultraGridColumn81,
            ultraGridColumn82,
            ultraGridColumn83,
            ultraGridColumn84,
            ultraGridColumn85});
            ultraGridBand2.GroupHeaderLines = 2;
            appearance21.TextVAlignAsString = "Middle";
            ultraGridBand2.Override.CellAppearance = appearance21;
            ultraGridBand2.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            this.dgvBlockTag.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.dgvBlockTag.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvBlockTag.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance67.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance67.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance67.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvBlockTag.DisplayLayout.GroupByBox.Appearance = appearance67;
            appearance68.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvBlockTag.DisplayLayout.GroupByBox.BandLabelAppearance = appearance68;
            this.dgvBlockTag.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvBlockTag.DisplayLayout.GroupByBox.Hidden = true;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance69.BackColor2 = System.Drawing.SystemColors.Control;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvBlockTag.DisplayLayout.GroupByBox.PromptAppearance = appearance69;
            this.dgvBlockTag.DisplayLayout.MaxColScrollRegions = 1;
            this.dgvBlockTag.DisplayLayout.MaxRowScrollRegions = 1;
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgvBlockTag.DisplayLayout.Override.ActiveCellAppearance = appearance70;
            appearance71.BackColor = System.Drawing.SystemColors.Highlight;
            appearance71.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvBlockTag.DisplayLayout.Override.ActiveRowAppearance = appearance71;
            this.dgvBlockTag.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.dgvBlockTag.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.dgvBlockTag.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            this.dgvBlockTag.DisplayLayout.Override.CardAreaAppearance = appearance72;
            appearance73.BorderColor = System.Drawing.Color.Silver;
            appearance73.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.dgvBlockTag.DisplayLayout.Override.CellAppearance = appearance73;
            this.dgvBlockTag.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.dgvBlockTag.DisplayLayout.Override.CellPadding = 0;
            appearance74.BackColor = System.Drawing.Color.LightYellow;
            appearance74.ForeColor = System.Drawing.Color.Blue;
            this.dgvBlockTag.DisplayLayout.Override.FixedCellAppearance = appearance74;
            this.dgvBlockTag.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.Blue;
            appearance75.BackColor = System.Drawing.Color.LightYellow;
            appearance75.ForeColor = System.Drawing.Color.Blue;
            this.dgvBlockTag.DisplayLayout.Override.FixedHeaderAppearance = appearance75;
            appearance76.BackColor = System.Drawing.SystemColors.Control;
            appearance76.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance76.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance76.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvBlockTag.DisplayLayout.Override.GroupByRowAppearance = appearance76;
            appearance77.TextHAlignAsString = "Left";
            this.dgvBlockTag.DisplayLayout.Override.HeaderAppearance = appearance77;
            this.dgvBlockTag.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.dgvBlockTag.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.BorderColor = System.Drawing.Color.Silver;
            this.dgvBlockTag.DisplayLayout.Override.RowAppearance = appearance78;
            this.dgvBlockTag.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.dgvBlockTag.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            this.dgvBlockTag.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            appearance79.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvBlockTag.DisplayLayout.Override.TemplateAddRowAppearance = appearance79;
            this.dgvBlockTag.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.dgvBlockTag.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.dgvBlockTag.DisplayLayout.UseFixedHeaders = true;
            this.dgvBlockTag.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.dgvBlockTag.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.dgvBlockTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBlockTag.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvBlockTag.Location = new System.Drawing.Point(0, 29);
            this.dgvBlockTag.Name = "dgvBlockTag";
            this.dgvBlockTag.Size = new System.Drawing.Size(950, 466);
            this.dgvBlockTag.TabIndex = 3;
            this.dgvBlockTag.Text = "ultraGrid1";
            this.dgvBlockTag.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.dgvBlockTag_CellListSelect);
            this.dgvBlockTag.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvBlockTag_DragDrop);
            this.dgvBlockTag.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvBlockTag_DragOver);
            this.dgvBlockTag.SelectionDrag += new System.ComponentModel.CancelEventHandler(this.dgvBlockTag_SelectionDrag);
            this.dgvBlockTag.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.dgvBlockTag_InitializeLayout);
            this.dgvBlockTag.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.dgvBlockTag_CellChange);
            this.dgvBlockTag.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.dgvBlockTag_AfterSelectChange);
            // 
            // ultraDataSource9
            // 
            ultraDataColumn78.DataType = typeof(bool);
            ultraDataColumn81.DataType = typeof(bool);
            ultraDataColumn82.DataType = typeof(bool);
            ultraDataColumn83.DataType = typeof(bool);
            ultraDataColumn84.DataType = typeof(bool);
            ultraDataColumn85.DataType = typeof(bool);
            ultraDataColumn86.DataType = typeof(bool);
            ultraDataColumn87.DataType = typeof(bool);
            ultraDataColumn88.DataType = typeof(bool);
            ultraDataColumn89.DataType = typeof(bool);
            ultraDataColumn90.DataType = typeof(bool);
            ultraDataColumn91.DataType = typeof(bool);
            ultraDataColumn92.DataType = typeof(bool);
            ultraDataColumn93.DataType = typeof(bool);
            ultraDataColumn94.DataType = typeof(bool);
            ultraDataColumn95.DataType = typeof(bool);
            ultraDataColumn96.DataType = typeof(bool);
            ultraDataColumn97.DataType = typeof(bool);
            ultraDataColumn97.DefaultValue = false;
            ultraDataColumn98.DataType = typeof(bool);
            ultraDataColumn99.DataType = typeof(bool);
            this.ultraDataSource9.Band.Columns.AddRange(new object[] {
            ultraDataColumn66,
            ultraDataColumn67,
            ultraDataColumn68,
            ultraDataColumn69,
            ultraDataColumn70,
            ultraDataColumn71,
            ultraDataColumn72,
            ultraDataColumn73,
            ultraDataColumn74,
            ultraDataColumn75,
            ultraDataColumn76,
            ultraDataColumn77,
            ultraDataColumn78,
            ultraDataColumn79,
            ultraDataColumn80,
            ultraDataColumn81,
            ultraDataColumn82,
            ultraDataColumn83,
            ultraDataColumn84,
            ultraDataColumn85,
            ultraDataColumn86,
            ultraDataColumn87,
            ultraDataColumn88,
            ultraDataColumn89,
            ultraDataColumn90,
            ultraDataColumn91,
            ultraDataColumn92,
            ultraDataColumn93,
            ultraDataColumn94,
            ultraDataColumn95,
            ultraDataColumn96,
            ultraDataColumn97,
            ultraDataColumn98,
            ultraDataColumn99});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splitContainer2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 495);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(950, 300);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 17);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.txtHtagname);
            this.splitContainer2.Panel1.Controls.Add(this.label11);
            this.splitContainer2.Panel1.Controls.Add(this.cbUseYn);
            this.splitContainer2.Panel1.Controls.Add(this.label12);
            this.splitContainer2.Panel1.Controls.Add(this.label13);
            this.splitContainer2.Panel1.Controls.Add(this.txtTagname);
            this.splitContainer2.Panel1.Controls.Add(this.label14);
            this.splitContainer2.Panel1.Controls.Add(this.txtCoefficient);
            this.splitContainer2.Panel1.Controls.Add(this.label15);
            this.splitContainer2.Panel1.Controls.Add(this.label16);
            this.splitContainer2.Panel1.Controls.Add(this.txtLocCode);
            this.splitContainer2.Panel1.Controls.Add(this.txtDesc);
            this.splitContainer2.Panel1.Controls.Add(this.label17);
            this.splitContainer2.Panel1.Controls.Add(this.label18);
            this.splitContainer2.Panel1.Controls.Add(this.label23);
            this.splitContainer2.Panel1.Controls.Add(this.txtFtrIdn);
            this.splitContainer2.Panel1.Controls.Add(this.txtLocCode2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tabTagCal);
            this.splitContainer2.Size = new System.Drawing.Size(944, 280);
            this.splitContainer2.SplitterDistance = 427;
            this.splitContainer2.TabIndex = 84;
            // 
            // txtHtagname
            // 
            this.txtHtagname.BackColor = System.Drawing.Color.White;
            this.txtHtagname.Location = new System.Drawing.Point(102, 61);
            this.txtHtagname.Name = "txtHtagname";
            this.txtHtagname.ReadOnly = true;
            this.txtHtagname.Size = new System.Drawing.Size(323, 21);
            this.txtHtagname.TabIndex = 100;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 99;
            this.label11.Text = "히스토리안태그";
            // 
            // cbUseYn
            // 
            this.cbUseYn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUseYn.Enabled = false;
            this.cbUseYn.FormattingEnabled = true;
            this.cbUseYn.Items.AddRange(new object[] {
            "",
            "예",
            "아니오"});
            this.cbUseYn.Location = new System.Drawing.Point(325, 7);
            this.cbUseYn.Name = "cbUseYn";
            this.cbUseYn.Size = new System.Drawing.Size(100, 20);
            this.cbUseYn.TabIndex = 98;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(224, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 12);
            this.label12.TabIndex = 96;
            this.label12.Text = "(지도에서 사용)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(227, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 97;
            this.label13.Text = "사용여부";
            // 
            // txtTagname
            // 
            this.txtTagname.BackColor = System.Drawing.Color.White;
            this.txtTagname.Location = new System.Drawing.Point(102, 6);
            this.txtTagname.Name = "txtTagname";
            this.txtTagname.ReadOnly = true;
            this.txtTagname.Size = new System.Drawing.Size(100, 21);
            this.txtTagname.TabIndex = 85;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 86;
            this.label14.Text = "태그설명";
            // 
            // txtCoefficient
            // 
            this.txtCoefficient.BackColor = System.Drawing.Color.White;
            this.txtCoefficient.Location = new System.Drawing.Point(323, 88);
            this.txtCoefficient.Name = "txtCoefficient";
            this.txtCoefficient.ReadOnly = true;
            this.txtCoefficient.Size = new System.Drawing.Size(100, 21);
            this.txtCoefficient.TabIndex = 91;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(224, 92);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 90;
            this.label15.Text = "계수";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 12);
            this.label16.TabIndex = 84;
            this.label16.Text = "태그일련번호";
            // 
            // txtLocCode
            // 
            this.txtLocCode.BackColor = System.Drawing.Color.White;
            this.txtLocCode.Location = new System.Drawing.Point(103, 115);
            this.txtLocCode.Name = "txtLocCode";
            this.txtLocCode.ReadOnly = true;
            this.txtLocCode.Size = new System.Drawing.Size(100, 21);
            this.txtLocCode.TabIndex = 93;
            // 
            // txtDesc
            // 
            this.txtDesc.BackColor = System.Drawing.Color.White;
            this.txtDesc.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.txtDesc.Location = new System.Drawing.Point(102, 33);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(323, 21);
            this.txtDesc.TabIndex = 87;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(2, 120);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 12);
            this.label17.TabIndex = 92;
            this.label17.Text = "지역관리코드";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(2, 94);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 12);
            this.label18.TabIndex = 88;
            this.label18.Text = "유량계 FTR_IDN";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(223, 114);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(83, 12);
            this.label23.TabIndex = 94;
            this.label23.Text = "지역관리코드2";
            // 
            // txtFtrIdn
            // 
            this.txtFtrIdn.BackColor = System.Drawing.Color.White;
            this.txtFtrIdn.Location = new System.Drawing.Point(103, 88);
            this.txtFtrIdn.Name = "txtFtrIdn";
            this.txtFtrIdn.ReadOnly = true;
            this.txtFtrIdn.Size = new System.Drawing.Size(100, 21);
            this.txtFtrIdn.TabIndex = 89;
            // 
            // txtLocCode2
            // 
            this.txtLocCode2.BackColor = System.Drawing.Color.White;
            this.txtLocCode2.Location = new System.Drawing.Point(323, 115);
            this.txtLocCode2.Name = "txtLocCode2";
            this.txtLocCode2.ReadOnly = true;
            this.txtLocCode2.Size = new System.Drawing.Size(100, 21);
            this.txtLocCode2.TabIndex = 95;
            // 
            // tabTagCal
            // 
            this.tabTagCal.AllowDrop = true;
            this.tabTagCal.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabTagCal.Controls.Add(this.ultraTabPageControl1);
            this.tabTagCal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabTagCal.Enabled = false;
            this.tabTagCal.Location = new System.Drawing.Point(0, 0);
            this.tabTagCal.Name = "tabTagCal";
            this.tabTagCal.SharedControls.AddRange(new System.Windows.Forms.Control[] {
            this.dgvCalcTag});
            this.tabTagCal.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabTagCal.Size = new System.Drawing.Size(513, 280);
            this.tabTagCal.TabIndex = 0;
            ultraTab1.Key = "DUMMY_CAL";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "더미태그 계산식";
            this.tabTagCal.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1});
            this.tabTagCal.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.tabTagCal_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Controls.Add(this.dgvCalcTag);
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(509, 254);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnDummyTagDel);
            this.panel7.Controls.Add(this.btnSave);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.btnReLoad);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 795);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(950, 30);
            this.panel7.TabIndex = 1;
            // 
            // btnDummyTagDel
            // 
            this.btnDummyTagDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDummyTagDel.Location = new System.Drawing.Point(705, 3);
            this.btnDummyTagDel.Name = "btnDummyTagDel";
            this.btnDummyTagDel.Size = new System.Drawing.Size(110, 23);
            this.btnDummyTagDel.TabIndex = 4;
            this.btnDummyTagDel.Text = "더미태그삭제";
            this.btnDummyTagDel.UseVisualStyleBackColor = true;
            this.btnDummyTagDel.Click += new System.EventHandler(this.btnDummyTagDel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(821, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "변경내용저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 27);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(950, 3);
            this.panel10.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(950, 3);
            this.panel9.TabIndex = 2;
            // 
            // btnReLoad
            // 
            this.btnReLoad.Location = new System.Drawing.Point(6, 4);
            this.btnReLoad.Name = "btnReLoad";
            this.btnReLoad.Size = new System.Drawing.Size(100, 23);
            this.btnReLoad.TabIndex = 1;
            this.btnReLoad.Text = "데이터 재구성";
            this.btnReLoad.UseVisualStyleBackColor = true;
            this.btnReLoad.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnShowTag);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(950, 29);
            this.panel5.TabIndex = 0;
            // 
            // btnShowTag
            // 
            this.btnShowTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowTag.Font = new System.Drawing.Font("굴림", 8F);
            this.btnShowTag.Location = new System.Drawing.Point(927, 4);
            this.btnShowTag.Name = "btnShowTag";
            this.btnShowTag.Size = new System.Drawing.Size(23, 23);
            this.btnShowTag.TabIndex = 2;
            this.btnShowTag.Text = "▶";
            this.btnShowTag.UseVisualStyleBackColor = true;
            this.btnShowTag.Click += new System.EventHandler(this.btnShowTag_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(950, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "블록 태그 정보";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTagSearch
            // 
            this.lblTagSearch.BackColor = System.Drawing.Color.White;
            this.lblTagSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTagSearch.Location = new System.Drawing.Point(214, 308);
            this.lblTagSearch.Name = "lblTagSearch";
            this.lblTagSearch.Size = new System.Drawing.Size(190, 36);
            this.lblTagSearch.TabIndex = 0;
            this.lblTagSearch.Text = "조회중입니다...";
            this.lblTagSearch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvTag
            // 
            this.dgvTag.AllowDrop = true;
            this.dgvTag.DataSource = this.ultraDataSource12;
            appearance106.BackColor = System.Drawing.SystemColors.Window;
            appearance106.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgvTag.DisplayLayout.Appearance = appearance106;
            ultraGridColumn86.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance22.TextHAlignAsString = "Center";
            appearance22.TextVAlignAsString = "Middle";
            ultraGridColumn86.CellAppearance = appearance22;
            ultraGridColumn86.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn86.Header.Caption = "태그일련번호";
            ultraGridColumn86.Header.VisiblePosition = 0;
            ultraGridColumn86.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 19);
            ultraGridColumn86.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn87.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance55.TextVAlignAsString = "Middle";
            ultraGridColumn87.CellAppearance = appearance55;
            ultraGridColumn87.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn87.Header.Caption = "태그설명";
            ultraGridColumn87.Header.VisiblePosition = 1;
            ultraGridColumn87.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(172, 20);
            ultraGridColumn87.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn88.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance56.TextVAlignAsString = "Middle";
            ultraGridColumn88.CellAppearance = appearance56;
            ultraGridColumn88.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn88.Header.Caption = "사용여부";
            ultraGridColumn88.Header.VisiblePosition = 2;
            ultraGridColumn88.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(67, 19);
            ultraGridColumn88.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn89.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance57.TextVAlignAsString = "Middle";
            ultraGridColumn89.CellAppearance = appearance57;
            ultraGridColumn89.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn89.Header.Caption = "계수";
            ultraGridColumn89.Header.VisiblePosition = 3;
            ultraGridColumn89.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(52, 19);
            ultraGridColumn89.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn90.Header.Caption = "블록코드";
            ultraGridColumn90.Header.VisiblePosition = 4;
            ultraGridColumn90.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(83, 0);
            ultraGridColumn90.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn91.Header.Caption = "블록코드2";
            ultraGridColumn91.Header.VisiblePosition = 5;
            ultraGridColumn91.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(82, 0);
            ultraGridColumn91.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn92.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance58.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance58.ImageVAlign = Infragistics.Win.VAlign.Middle;
            ultraGridColumn92.CellAppearance = appearance58;
            ultraGridColumn92.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn92.Header.Caption = "HistorianTag";
            ultraGridColumn92.Header.VisiblePosition = 6;
            ultraGridColumn92.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(130, 19);
            ultraGridColumn92.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridBand3.Columns.AddRange(new object[] {
            ultraGridColumn86,
            ultraGridColumn87,
            ultraGridColumn88,
            ultraGridColumn89,
            ultraGridColumn90,
            ultraGridColumn91,
            ultraGridColumn92});
            ultraGridBand3.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            this.dgvTag.DisplayLayout.BandsSerializer.Add(ultraGridBand3);
            this.dgvTag.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvTag.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance241.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance241.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance241.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance241.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvTag.DisplayLayout.GroupByBox.Appearance = appearance241;
            appearance242.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvTag.DisplayLayout.GroupByBox.BandLabelAppearance = appearance242;
            this.dgvTag.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvTag.DisplayLayout.GroupByBox.Hidden = true;
            appearance243.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance243.BackColor2 = System.Drawing.SystemColors.Control;
            appearance243.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance243.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvTag.DisplayLayout.GroupByBox.PromptAppearance = appearance243;
            this.dgvTag.DisplayLayout.MaxColScrollRegions = 1;
            this.dgvTag.DisplayLayout.MaxRowScrollRegions = 1;
            appearance244.BackColor = System.Drawing.SystemColors.Window;
            appearance244.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgvTag.DisplayLayout.Override.ActiveCellAppearance = appearance244;
            appearance245.BackColor = System.Drawing.SystemColors.Highlight;
            appearance245.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvTag.DisplayLayout.Override.ActiveRowAppearance = appearance245;
            this.dgvTag.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.dgvTag.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance246.BackColor = System.Drawing.SystemColors.Window;
            this.dgvTag.DisplayLayout.Override.CardAreaAppearance = appearance246;
            appearance247.BorderColor = System.Drawing.Color.Silver;
            appearance247.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.dgvTag.DisplayLayout.Override.CellAppearance = appearance247;
            this.dgvTag.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.dgvTag.DisplayLayout.Override.CellPadding = 0;
            appearance248.BackColor = System.Drawing.SystemColors.Control;
            appearance248.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance248.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance248.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance248.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvTag.DisplayLayout.Override.GroupByRowAppearance = appearance248;
            appearance249.TextHAlignAsString = "Left";
            this.dgvTag.DisplayLayout.Override.HeaderAppearance = appearance249;
            this.dgvTag.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.dgvTag.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance250.BackColor = System.Drawing.SystemColors.Window;
            appearance250.BorderColor = System.Drawing.Color.Silver;
            this.dgvTag.DisplayLayout.Override.RowAppearance = appearance250;
            this.dgvTag.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.dgvTag.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            appearance251.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvTag.DisplayLayout.Override.TemplateAddRowAppearance = appearance251;
            this.dgvTag.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.dgvTag.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.dgvTag.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.dgvTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTag.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvTag.Location = new System.Drawing.Point(0, 29);
            this.dgvTag.Name = "dgvTag";
            this.dgvTag.Size = new System.Drawing.Size(683, 466);
            this.dgvTag.TabIndex = 4;
            this.dgvTag.Text = "ultraGrid2";
            this.dgvTag.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvTag_DragDrop);
            this.dgvTag.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvTag_DragOver);
            this.dgvTag.SelectionDrag += new System.ComponentModel.CancelEventHandler(this.dgvTag_SelectionDrag);
            this.dgvTag.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.dgvTag_AfterSelectChange);
            // 
            // ultraDataSource12
            // 
            this.ultraDataSource12.Band.Columns.AddRange(new object[] {
            ultraDataColumn100,
            ultraDataColumn101,
            ultraDataColumn102,
            ultraDataColumn103,
            ultraDataColumn104,
            ultraDataColumn105});
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTagHTagname);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.cbTagUseYn);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtTagTagname);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtTagCoefficient);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtTagLocCode);
            this.groupBox2.Controls.Add(this.txtTagDesc);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtTagFtrIdn);
            this.groupBox2.Controls.Add(this.txtTagLocCode2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(0, 495);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(683, 300);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // txtTagHTagname
            // 
            this.txtTagHTagname.BackColor = System.Drawing.Color.White;
            this.txtTagHTagname.Location = new System.Drawing.Point(330, 20);
            this.txtTagHTagname.Name = "txtTagHTagname";
            this.txtTagHTagname.ReadOnly = true;
            this.txtTagHTagname.Size = new System.Drawing.Size(243, 21);
            this.txtTagHTagname.TabIndex = 83;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(232, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 12);
            this.label19.TabIndex = 82;
            this.label19.Text = "히스토리안태그";
            // 
            // cbTagUseYn
            // 
            this.cbTagUseYn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTagUseYn.Enabled = false;
            this.cbTagUseYn.FormattingEnabled = true;
            this.cbTagUseYn.Items.AddRange(new object[] {
            "",
            "예",
            "아니오"});
            this.cbTagUseYn.Location = new System.Drawing.Point(108, 128);
            this.cbTagUseYn.Name = "cbTagUseYn";
            this.cbTagUseYn.Size = new System.Drawing.Size(100, 20);
            this.cbTagUseYn.TabIndex = 81;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(232, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 12);
            this.label10.TabIndex = 80;
            this.label10.Text = "(지도에서 사용)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 80;
            this.label3.Text = "사용여부";
            // 
            // txtTagTagname
            // 
            this.txtTagTagname.BackColor = System.Drawing.Color.White;
            this.txtTagTagname.Location = new System.Drawing.Point(108, 20);
            this.txtTagTagname.Name = "txtTagTagname";
            this.txtTagTagname.ReadOnly = true;
            this.txtTagTagname.Size = new System.Drawing.Size(100, 21);
            this.txtTagTagname.TabIndex = 69;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 70;
            this.label4.Text = "태그설명";
            // 
            // txtTagCoefficient
            // 
            this.txtTagCoefficient.BackColor = System.Drawing.Color.White;
            this.txtTagCoefficient.Location = new System.Drawing.Point(331, 74);
            this.txtTagCoefficient.Name = "txtTagCoefficient";
            this.txtTagCoefficient.ReadOnly = true;
            this.txtTagCoefficient.Size = new System.Drawing.Size(100, 21);
            this.txtTagCoefficient.TabIndex = 75;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 74;
            this.label5.Text = "계수";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 68;
            this.label6.Text = "태그일련번호";
            // 
            // txtTagLocCode
            // 
            this.txtTagLocCode.BackColor = System.Drawing.Color.White;
            this.txtTagLocCode.Location = new System.Drawing.Point(108, 101);
            this.txtTagLocCode.Name = "txtTagLocCode";
            this.txtTagLocCode.ReadOnly = true;
            this.txtTagLocCode.Size = new System.Drawing.Size(100, 21);
            this.txtTagLocCode.TabIndex = 77;
            // 
            // txtTagDesc
            // 
            this.txtTagDesc.BackColor = System.Drawing.Color.White;
            this.txtTagDesc.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.txtTagDesc.Location = new System.Drawing.Point(108, 47);
            this.txtTagDesc.Name = "txtTagDesc";
            this.txtTagDesc.ReadOnly = true;
            this.txtTagDesc.Size = new System.Drawing.Size(323, 21);
            this.txtTagDesc.TabIndex = 71;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 76;
            this.label7.Text = "지역관리코드";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 12);
            this.label8.TabIndex = 72;
            this.label8.Text = "유량계 FTR_IDN";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(231, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 12);
            this.label9.TabIndex = 78;
            this.label9.Text = "지역관리코드2";
            // 
            // txtTagFtrIdn
            // 
            this.txtTagFtrIdn.BackColor = System.Drawing.Color.White;
            this.txtTagFtrIdn.Location = new System.Drawing.Point(108, 74);
            this.txtTagFtrIdn.Name = "txtTagFtrIdn";
            this.txtTagFtrIdn.ReadOnly = true;
            this.txtTagFtrIdn.Size = new System.Drawing.Size(100, 21);
            this.txtTagFtrIdn.TabIndex = 73;
            // 
            // txtTagLocCode2
            // 
            this.txtTagLocCode2.BackColor = System.Drawing.Color.White;
            this.txtTagLocCode2.Location = new System.Drawing.Point(331, 101);
            this.txtTagLocCode2.Name = "txtTagLocCode2";
            this.txtTagLocCode2.ReadOnly = true;
            this.txtTagLocCode2.Size = new System.Drawing.Size(100, 21);
            this.txtTagLocCode2.TabIndex = 79;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnClose);
            this.panel8.Controls.Add(this.btnTagEdit);
            this.panel8.Controls.Add(this.btnTagNew);
            this.panel8.Controls.Add(this.btnTagSave);
            this.panel8.Controls.Add(this.btnTagDel);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 795);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(683, 30);
            this.panel8.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(605, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnTagEdit
            // 
            this.btnTagEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTagEdit.Location = new System.Drawing.Point(199, 4);
            this.btnTagEdit.Name = "btnTagEdit";
            this.btnTagEdit.Size = new System.Drawing.Size(75, 23);
            this.btnTagEdit.TabIndex = 3;
            this.btnTagEdit.Text = "수정";
            this.btnTagEdit.UseVisualStyleBackColor = true;
            this.btnTagEdit.Click += new System.EventHandler(this.btnTagEdit_Click);
            // 
            // btnTagNew
            // 
            this.btnTagNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTagNew.Location = new System.Drawing.Point(118, 4);
            this.btnTagNew.Name = "btnTagNew";
            this.btnTagNew.Size = new System.Drawing.Size(75, 23);
            this.btnTagNew.TabIndex = 2;
            this.btnTagNew.Text = "신규";
            this.btnTagNew.UseVisualStyleBackColor = true;
            this.btnTagNew.Click += new System.EventHandler(this.btnTagNew_Click);
            // 
            // btnTagSave
            // 
            this.btnTagSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTagSave.Location = new System.Drawing.Point(280, 4);
            this.btnTagSave.Name = "btnTagSave";
            this.btnTagSave.Size = new System.Drawing.Size(75, 23);
            this.btnTagSave.TabIndex = 1;
            this.btnTagSave.Text = "저장";
            this.btnTagSave.UseVisualStyleBackColor = true;
            this.btnTagSave.Click += new System.EventHandler(this.btnTagSave_Click);
            // 
            // btnTagDel
            // 
            this.btnTagDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTagDel.Location = new System.Drawing.Point(361, 4);
            this.btnTagDel.Name = "btnTagDel";
            this.btnTagDel.Size = new System.Drawing.Size(75, 23);
            this.btnTagDel.TabIndex = 0;
            this.btnTagDel.Text = "삭제";
            this.btnTagDel.UseVisualStyleBackColor = true;
            this.btnTagDel.Click += new System.EventHandler(this.btnTagDel_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.cbBlock);
            this.panel6.Controls.Add(this.btnSearchTag);
            this.panel6.Controls.Add(this.txtSearchTagDesc);
            this.panel6.Controls.Add(this.txtSearchTagname);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.txtLabel10);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(683, 29);
            this.panel6.TabIndex = 0;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(68, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 12);
            this.label22.TabIndex = 16;
            this.label22.Text = "블록 :";
            // 
            // cbBlock
            // 
            this.cbBlock.FormattingEnabled = true;
            this.cbBlock.Location = new System.Drawing.Point(108, 4);
            this.cbBlock.Name = "cbBlock";
            this.cbBlock.Size = new System.Drawing.Size(116, 20);
            this.cbBlock.TabIndex = 15;
            this.cbBlock.SelectedIndexChanged += new System.EventHandler(this.cbBlock_SelectedIndexChanged);
            // 
            // btnSearchTag
            // 
            this.btnSearchTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchTag.Location = new System.Drawing.Point(631, 3);
            this.btnSearchTag.Name = "btnSearchTag";
            this.btnSearchTag.Size = new System.Drawing.Size(52, 23);
            this.btnSearchTag.TabIndex = 14;
            this.btnSearchTag.Text = "검색";
            this.btnSearchTag.UseVisualStyleBackColor = true;
            this.btnSearchTag.Click += new System.EventHandler(this.btnSearchTag_Click);
            // 
            // txtSearchTagDesc
            // 
            this.txtSearchTagDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchTagDesc.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.txtSearchTagDesc.Location = new System.Drawing.Point(486, 4);
            this.txtSearchTagDesc.Name = "txtSearchTagDesc";
            this.txtSearchTagDesc.Size = new System.Drawing.Size(611, 21);
            this.txtSearchTagDesc.TabIndex = 13;
            this.txtSearchTagDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchTagDesc_KeyDown);
            // 
            // txtSearchTagname
            // 
            this.txtSearchTagname.Location = new System.Drawing.Point(318, 4);
            this.txtSearchTagname.Name = "txtSearchTagname";
            this.txtSearchTagname.Size = new System.Drawing.Size(98, 21);
            this.txtSearchTagname.TabIndex = 11;
            this.txtSearchTagname.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchTagname_KeyUp);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(422, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 12);
            this.label20.TabIndex = 12;
            this.label20.Text = "태그설명 :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(230, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 12);
            this.label21.TabIndex = 10;
            this.label21.Text = "태그일련번호 :";
            // 
            // txtLabel10
            // 
            this.txtLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLabel10.Location = new System.Drawing.Point(0, 0);
            this.txtLabel10.Name = "txtLabel10";
            this.txtLabel10.Size = new System.Drawing.Size(683, 29);
            this.txtLabel10.TabIndex = 2;
            this.txtLabel10.Text = "태그 정보";
            this.txtLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ultraDataSource5
            // 
            ultraDataColumn118.DataType = typeof(bool);
            ultraDataColumn120.DataType = typeof(bool);
            this.ultraDataSource5.Band.Columns.AddRange(new object[] {
            ultraDataColumn106,
            ultraDataColumn107,
            ultraDataColumn108,
            ultraDataColumn109,
            ultraDataColumn110,
            ultraDataColumn111,
            ultraDataColumn112,
            ultraDataColumn113,
            ultraDataColumn114,
            ultraDataColumn115,
            ultraDataColumn116,
            ultraDataColumn117,
            ultraDataColumn118,
            ultraDataColumn119,
            ultraDataColumn120,
            ultraDataColumn121,
            ultraDataColumn122,
            ultraDataColumn123,
            ultraDataColumn124,
            ultraDataColumn125,
            ultraDataColumn126,
            ultraDataColumn127,
            ultraDataColumn128,
            ultraDataColumn129,
            ultraDataColumn130,
            ultraDataColumn131,
            ultraDataColumn132,
            ultraDataColumn133,
            ultraDataColumn134,
            ultraDataColumn135,
            ultraDataColumn136,
            ultraDataColumn137,
            ultraDataColumn138});
            // 
            // ultraDataSource1
            // 
            this.ultraDataSource1.Band.Columns.AddRange(new object[] {
            ultraDataColumn139,
            ultraDataColumn140,
            ultraDataColumn141,
            ultraDataColumn142,
            ultraDataColumn143,
            ultraDataColumn144,
            ultraDataColumn145,
            ultraDataColumn146,
            ultraDataColumn147,
            ultraDataColumn148,
            ultraDataColumn149,
            ultraDataColumn150,
            ultraDataColumn151,
            ultraDataColumn152,
            ultraDataColumn153,
            ultraDataColumn154,
            ultraDataColumn155,
            ultraDataColumn156,
            ultraDataColumn157,
            ultraDataColumn158,
            ultraDataColumn159,
            ultraDataColumn160,
            ultraDataColumn161,
            ultraDataColumn162,
            ultraDataColumn163,
            ultraDataColumn164,
            ultraDataColumn165,
            ultraDataColumn166,
            ultraDataColumn167,
            ultraDataColumn168,
            ultraDataColumn169,
            ultraDataColumn170});
            // 
            // ultraDataSource2
            // 
            this.ultraDataSource2.Band.Columns.AddRange(new object[] {
            ultraDataColumn171,
            ultraDataColumn172,
            ultraDataColumn173,
            ultraDataColumn174,
            ultraDataColumn175,
            ultraDataColumn176,
            ultraDataColumn177,
            ultraDataColumn178,
            ultraDataColumn179,
            ultraDataColumn180,
            ultraDataColumn181,
            ultraDataColumn182,
            ultraDataColumn183,
            ultraDataColumn184,
            ultraDataColumn185,
            ultraDataColumn186,
            ultraDataColumn187,
            ultraDataColumn188,
            ultraDataColumn189,
            ultraDataColumn190,
            ultraDataColumn191,
            ultraDataColumn192,
            ultraDataColumn193,
            ultraDataColumn194,
            ultraDataColumn195,
            ultraDataColumn196,
            ultraDataColumn197,
            ultraDataColumn198,
            ultraDataColumn199,
            ultraDataColumn200,
            ultraDataColumn201,
            ultraDataColumn202,
            ultraDataColumn203});
            // 
            // ultraDataSource3
            // 
            this.ultraDataSource3.Band.Columns.AddRange(new object[] {
            ultraDataColumn204,
            ultraDataColumn205,
            ultraDataColumn206,
            ultraDataColumn207,
            ultraDataColumn208,
            ultraDataColumn209,
            ultraDataColumn210,
            ultraDataColumn211,
            ultraDataColumn212,
            ultraDataColumn213,
            ultraDataColumn214,
            ultraDataColumn215,
            ultraDataColumn216,
            ultraDataColumn217,
            ultraDataColumn218,
            ultraDataColumn219,
            ultraDataColumn220,
            ultraDataColumn221,
            ultraDataColumn222,
            ultraDataColumn223,
            ultraDataColumn224,
            ultraDataColumn225,
            ultraDataColumn226,
            ultraDataColumn227,
            ultraDataColumn228,
            ultraDataColumn229,
            ultraDataColumn230,
            ultraDataColumn231,
            ultraDataColumn232,
            ultraDataColumn233,
            ultraDataColumn234,
            ultraDataColumn235,
            ultraDataColumn236});
            // 
            // ultraDataSource4
            // 
            ultraDataColumn240.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            ultraDataColumn241.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            ultraDataColumn242.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            ultraDataColumn247.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            ultraDataColumn248.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            ultraDataColumn249.DataType = typeof(bool);
            ultraDataColumn249.DefaultValue = false;
            ultraDataColumn251.DataType = typeof(bool);
            ultraDataColumn251.DefaultValue = false;
            this.ultraDataSource4.Band.Columns.AddRange(new object[] {
            ultraDataColumn237,
            ultraDataColumn238,
            ultraDataColumn239,
            ultraDataColumn240,
            ultraDataColumn241,
            ultraDataColumn242,
            ultraDataColumn243,
            ultraDataColumn244,
            ultraDataColumn245,
            ultraDataColumn246,
            ultraDataColumn247,
            ultraDataColumn248,
            ultraDataColumn249,
            ultraDataColumn250,
            ultraDataColumn251,
            ultraDataColumn252,
            ultraDataColumn253,
            ultraDataColumn254,
            ultraDataColumn255,
            ultraDataColumn256,
            ultraDataColumn257,
            ultraDataColumn258,
            ultraDataColumn259,
            ultraDataColumn260,
            ultraDataColumn261,
            ultraDataColumn262,
            ultraDataColumn263,
            ultraDataColumn264,
            ultraDataColumn265,
            ultraDataColumn266,
            ultraDataColumn267,
            ultraDataColumn268,
            ultraDataColumn269});
            // 
            // ultraDataSource6
            // 
            ultraDataColumn282.DataType = typeof(bool);
            ultraDataColumn282.DefaultValue = false;
            ultraDataColumn284.DataType = typeof(bool);
            ultraDataColumn284.DefaultValue = false;
            this.ultraDataSource6.Band.Columns.AddRange(new object[] {
            ultraDataColumn270,
            ultraDataColumn271,
            ultraDataColumn272,
            ultraDataColumn273,
            ultraDataColumn274,
            ultraDataColumn275,
            ultraDataColumn276,
            ultraDataColumn277,
            ultraDataColumn278,
            ultraDataColumn279,
            ultraDataColumn280,
            ultraDataColumn281,
            ultraDataColumn282,
            ultraDataColumn283,
            ultraDataColumn284,
            ultraDataColumn285,
            ultraDataColumn286,
            ultraDataColumn287,
            ultraDataColumn288,
            ultraDataColumn289,
            ultraDataColumn290,
            ultraDataColumn291,
            ultraDataColumn292,
            ultraDataColumn293,
            ultraDataColumn294,
            ultraDataColumn295,
            ultraDataColumn296,
            ultraDataColumn297,
            ultraDataColumn298,
            ultraDataColumn299,
            ultraDataColumn300,
            ultraDataColumn301,
            ultraDataColumn302});
            // 
            // ultraDataSource7
            // 
            ultraDataColumn315.DataType = typeof(bool);
            ultraDataColumn317.DataType = typeof(bool);
            ultraDataColumn318.DataType = typeof(bool);
            ultraDataColumn319.DataType = typeof(bool);
            ultraDataColumn320.DataType = typeof(bool);
            ultraDataColumn321.DataType = typeof(bool);
            ultraDataColumn322.DataType = typeof(bool);
            ultraDataColumn323.DataType = typeof(bool);
            ultraDataColumn324.DataType = typeof(bool);
            ultraDataColumn325.DataType = typeof(bool);
            ultraDataColumn326.DataType = typeof(bool);
            ultraDataColumn327.DataType = typeof(bool);
            ultraDataColumn328.DataType = typeof(bool);
            ultraDataColumn329.DataType = typeof(bool);
            ultraDataColumn330.DataType = typeof(bool);
            ultraDataColumn331.DataType = typeof(bool);
            ultraDataColumn332.DataType = typeof(bool);
            ultraDataColumn333.DataType = typeof(bool);
            ultraDataColumn334.DataType = typeof(bool);
            ultraDataColumn335.DataType = typeof(bool);
            this.ultraDataSource7.Band.Columns.AddRange(new object[] {
            ultraDataColumn303,
            ultraDataColumn304,
            ultraDataColumn305,
            ultraDataColumn306,
            ultraDataColumn307,
            ultraDataColumn308,
            ultraDataColumn309,
            ultraDataColumn310,
            ultraDataColumn311,
            ultraDataColumn312,
            ultraDataColumn313,
            ultraDataColumn314,
            ultraDataColumn315,
            ultraDataColumn316,
            ultraDataColumn317,
            ultraDataColumn318,
            ultraDataColumn319,
            ultraDataColumn320,
            ultraDataColumn321,
            ultraDataColumn322,
            ultraDataColumn323,
            ultraDataColumn324,
            ultraDataColumn325,
            ultraDataColumn326,
            ultraDataColumn327,
            ultraDataColumn328,
            ultraDataColumn329,
            ultraDataColumn330,
            ultraDataColumn331,
            ultraDataColumn332,
            ultraDataColumn333,
            ultraDataColumn334,
            ultraDataColumn335});
            // 
            // ultraDataSource8
            // 
            ultraDataColumn348.DataType = typeof(bool);
            ultraDataColumn350.DataType = typeof(bool);
            ultraDataColumn351.DataType = typeof(bool);
            ultraDataColumn352.DataType = typeof(bool);
            ultraDataColumn353.DataType = typeof(bool);
            ultraDataColumn354.DataType = typeof(bool);
            ultraDataColumn355.DataType = typeof(bool);
            ultraDataColumn356.DataType = typeof(bool);
            ultraDataColumn357.DataType = typeof(bool);
            ultraDataColumn358.DataType = typeof(bool);
            ultraDataColumn359.DataType = typeof(bool);
            ultraDataColumn360.DataType = typeof(bool);
            ultraDataColumn361.DataType = typeof(bool);
            ultraDataColumn362.DataType = typeof(bool);
            ultraDataColumn362.DefaultValue = false;
            ultraDataColumn363.DataType = typeof(bool);
            ultraDataColumn364.DataType = typeof(bool);
            ultraDataColumn365.DataType = typeof(bool);
            ultraDataColumn366.DataType = typeof(bool);
            ultraDataColumn367.DataType = typeof(bool);
            ultraDataColumn368.DataType = typeof(bool);
            this.ultraDataSource8.Band.Columns.AddRange(new object[] {
            ultraDataColumn336,
            ultraDataColumn337,
            ultraDataColumn338,
            ultraDataColumn339,
            ultraDataColumn340,
            ultraDataColumn341,
            ultraDataColumn342,
            ultraDataColumn343,
            ultraDataColumn344,
            ultraDataColumn345,
            ultraDataColumn346,
            ultraDataColumn347,
            ultraDataColumn348,
            ultraDataColumn349,
            ultraDataColumn350,
            ultraDataColumn351,
            ultraDataColumn352,
            ultraDataColumn353,
            ultraDataColumn354,
            ultraDataColumn355,
            ultraDataColumn356,
            ultraDataColumn357,
            ultraDataColumn358,
            ultraDataColumn359,
            ultraDataColumn360,
            ultraDataColumn361,
            ultraDataColumn362,
            ultraDataColumn363,
            ultraDataColumn364,
            ultraDataColumn365,
            ultraDataColumn366,
            ultraDataColumn367,
            ultraDataColumn368});
            // 
            // ultraDataSource10
            // 
            this.ultraDataSource10.Band.Columns.AddRange(new object[] {
            ultraDataColumn369,
            ultraDataColumn370,
            ultraDataColumn371,
            ultraDataColumn372,
            ultraDataColumn373,
            ultraDataColumn374});
            // 
            // ultraDataSource11
            // 
            ultraDataColumn380.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDataSource11.Band.Columns.AddRange(new object[] {
            ultraDataColumn375,
            ultraDataColumn376,
            ultraDataColumn377,
            ultraDataColumn378,
            ultraDataColumn379,
            ultraDataColumn380});
            // 
            // dgvCalcSPL_O
            // 
            this.dgvCalcSPL_O.AllowDrop = true;
            this.dgvCalcSPL_O.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcSPL_O.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcSPL_O.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcSPL_O.Name = "dgvCalcSPL_O";
            this.dgvCalcSPL_O.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcSPL_O.TabIndex = 7;
            // 
            // dgvCalcSPL_I
            // 
            this.dgvCalcSPL_I.AllowDrop = true;
            this.dgvCalcSPL_I.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcSPL_I.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcSPL_I.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcSPL_I.Name = "dgvCalcSPL_I";
            this.dgvCalcSPL_I.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcSPL_I.TabIndex = 7;
            // 
            // dgvCalcSPL_D
            // 
            this.dgvCalcSPL_D.AllowDrop = true;
            this.dgvCalcSPL_D.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcSPL_D.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcSPL_D.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcSPL_D.Name = "dgvCalcSPL_D";
            this.dgvCalcSPL_D.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcSPL_D.TabIndex = 7;
            // 
            // dgvCalcFRQ_O
            // 
            this.dgvCalcFRQ_O.AllowDrop = true;
            this.dgvCalcFRQ_O.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcFRQ_O.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcFRQ_O.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcFRQ_O.Name = "dgvCalcFRQ_O";
            this.dgvCalcFRQ_O.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcFRQ_O.TabIndex = 7;
            // 
            // dgvCalcFRQ_I
            // 
            this.dgvCalcFRQ_I.AllowDrop = true;
            this.dgvCalcFRQ_I.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcFRQ_I.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcFRQ_I.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcFRQ_I.Name = "dgvCalcFRQ_I";
            this.dgvCalcFRQ_I.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcFRQ_I.TabIndex = 7;
            // 
            // dgvCalcFRI
            // 
            this.dgvCalcFRI.AllowDrop = true;
            this.dgvCalcFRI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcFRI.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcFRI.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcFRI.Name = "dgvCalcFRI";
            this.dgvCalcFRI.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcFRI.TabIndex = 6;
            // 
            // dgvCalcFRQ
            // 
            this.dgvCalcFRQ.AllowDrop = true;
            this.dgvCalcFRQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcFRQ.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcFRQ.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcFRQ.Name = "dgvCalcFRQ";
            this.dgvCalcFRQ.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcFRQ.TabIndex = 6;
            // 
            // dgvCalcMNF
            // 
            this.dgvCalcMNF.AllowDrop = true;
            this.dgvCalcMNF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcMNF.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcMNF.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcMNF.Name = "dgvCalcMNF";
            this.dgvCalcMNF.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcMNF.TabIndex = 6;
            // 
            // dgvCalcTT
            // 
            this.dgvCalcTT.AllowDrop = true;
            this.dgvCalcTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcTT.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcTT.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcTT.Name = "dgvCalcTT";
            this.dgvCalcTT.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcTT.TabIndex = 6;
            // 
            // dgvCalcYD_R
            // 
            this.dgvCalcYD_R.AllowDrop = true;
            this.dgvCalcYD_R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcYD_R.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcYD_R.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcYD_R.Name = "dgvCalcYD_R";
            this.dgvCalcYD_R.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcYD_R.TabIndex = 7;
            // 
            // dgvCalcYD
            // 
            this.dgvCalcYD.AllowDrop = true;
            this.dgvCalcYD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalcYD.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvCalcYD.Location = new System.Drawing.Point(0, 0);
            this.dgvCalcYD.Name = "dgvCalcYD";
            this.dgvCalcYD.Size = new System.Drawing.Size(531, 240);
            this.dgvCalcYD.TabIndex = 6;
            // 
            // FrmTagManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1647, 835);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "FrmTagManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "태그맵핑";
            this.Load += new System.EventHandler(this.FrmTagManage_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource25)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlockTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource9)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabTagCal)).EndInit();
            this.tabTagCal.ResumeLayout(false);
            this.ultraTabSharedControlsPage1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource12)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcSPL_O)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcSPL_I)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcSPL_D)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRQ_O)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRQ_I)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcFRQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcMNF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcYD_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalcYD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnShowTag;
        private System.Windows.Forms.Button btnTagDel;
        private System.Windows.Forms.Button btnTagSave;
        private System.Windows.Forms.Button btnTagNew;
        private System.Windows.Forms.Button btnTagEdit;
        private System.Windows.Forms.Button btnSearchTag;
        private System.Windows.Forms.TextBox txtSearchTagDesc;
        private System.Windows.Forms.TextBox txtSearchTagname;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvBlockTag;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvTag;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource1;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource2;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource3;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource4;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource5;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource7;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource6;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource8;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource9;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource10;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource12;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource11;
        private System.Windows.Forms.Label lblTagSearch;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cbBlock;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnReLoad;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTagHTagname;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbTagUseYn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTagTagname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTagCoefficient;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTagLocCode;
        private System.Windows.Forms.TextBox txtTagDesc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTagFtrIdn;
        private System.Windows.Forms.TextBox txtTagLocCode2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox txtHtagname;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbUseYn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTagname;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCoefficient;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtLocCode;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtFtrIdn;
        private System.Windows.Forms.TextBox txtLocCode2;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource13;
        private System.Windows.Forms.Label txtLabel10;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabTagCal;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource14;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource15;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource16;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource17;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource18;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource19;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource20;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource21;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource22;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource23;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource24;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource25;
        private System.Windows.Forms.Button btnDummyTagDel;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcTag;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcSPL_O;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcSPL_I;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcSPL_D;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcFRQ_O;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcFRQ_I;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcFRI;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcFRQ;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcMNF;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcTT;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcYD_R;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalcYD;
    }
}