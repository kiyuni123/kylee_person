﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.BlockApp.TagManage
{
    public partial class FrmTagCalcManageEtc : Form
    {
        private List<TagCalcItem> _tagCalListAll = null;
        private List<BlockTagCalcItem> _blockList = null;

        #region Constructor
        public FrmTagCalcManageEtc()
        {
            InitializeComponent();
        } 
        #endregion

        #region Helper Methods
        /// <summary>
        /// 오류처리
        /// </summary>
        /// <param name="method">오류 메서드명</param>
        /// <param name="ex">오류내용</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion

        /// <summary>
        /// where절에서의 Like 조건절 함수
        /// </summary>
        /// <param name="src">Source text</param>
        /// <param name="find">find text</param>
        /// <returns>boolean</returns>
        #region private bool Like(string src, string find)
        private bool Like(string src, string find)
        {
            if (src.IndexOf(find) >= 0) return true;
            else return false;
        }
        #endregion

        /// <summary>
        /// UltraGrid 전체 Row 삭제
        /// </summary>
        /// <param name="grid">UltraGrid</param>
        #region private void RemoveRowsAll(UltraGrid grid)
        private void RemoveRowsAll(UltraGrid grid)
        {
            grid.Selected.Rows.AddRange((UltraGridRow[])grid.Rows.All);
            grid.DeleteSelectedRows(false);
        }
        #endregion

        
        #endregion



        #region private void FrmTagCalcManage_Load(object sender, EventArgs e)
        private void FrmTagCalcManage_Load(object sender, EventArgs e)
        {
            try
            {
                dgvTag.BringToFront();

                // 데이터 목록 생성
                DataTable dtAllTagCalc = SelectAllTagCalData();
                CreateTagCalList(dtAllTagCalc);

                DataTable dtBlock = SelectBlockData();
                CreateBlockTagList(dtBlock);

                // 그리드 목록 생성
                CreateBlockTagGrid();

                cbSearchGbn.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                FireException("FrmTagCalcManage_Load", ex);
            }
        } 
        #endregion

        /// <summary>
        /// 모든 데이터 다시 읽기 (DB 포함)
        /// </summary>
        #region private void Reload()
        private void Reload()
        {
            _blockList.Clear();
            _tagCalListAll.Clear();

            dgvTag.BringToFront();

            // 데이터 목록 생성
            DataTable dtAllTagCalc = SelectAllTagCalData();
            CreateTagCalList(dtAllTagCalc);

            DataTable dtBlock = SelectBlockData();
            CreateBlockTagList(dtBlock);

            // 그리드 목록 생성
            CreateBlockTagGrid();
        } 
        #endregion

        /// <summary>
        /// 변경내용 저장후 화면 초기화
        /// </summary>
        #region private void ClearForm()
        private void ClearForm()
        {
            RemoveRowsAll(dgvCalc);
            lblTagname.Text = "";
            lblTagDesc.Text = "";
            lblLocCode.Text = "";
            lblTagGbn.Text = "";
            lblDummyGbn.Text = "";
        } 
        #endregion




        #region 내부 데이터 생성
        /// <summary>
        /// [데이터0] 블록 정보 데이터 조회 (블록+태그+태그구분)
        /// </summary>
        /// <returns>DataTable</returns>
        #region private DataTable SelectBlockData()
        private DataTable SelectBlockData()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.TAGCALC_BLOCK_SELECT, EMFrame.statics.AppStatic.USER_SGCCD), null);
            }
            catch (Exception ex)
            {
                FireException("SelectBlockData", ex);
            }
            return dt;
        }
        #endregion

        /// <summary>
        /// [데이터1] 블록 그리드를 구성하기 위한 전체 태그 계산식 조회 
        /// </summary>
        /// <returns>DataTable</returns>
        #region private DataTable SelectAllTagCalData()
        private DataTable SelectAllTagCalData()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                dt = mapper.ExecuteScriptDataTable(Database.Queries.TAGCALC_TAG_CALC_ALL_SELECT_ETC, null);
            }
            catch (Exception ex)
            {
                FireException("SelectAllTagCalData", ex);
            }
            return dt;
        } 
        #endregion

        /// <summary>
        /// 전체 태그 계산 정보 목록 생성
        /// </summary>
        /// <param name="dt">DataTable</param>
        #region private void CreateTagCalList(DataTable dt)
        private void CreateTagCalList(DataTable dt)
        {
            try
            {
                if (_tagCalListAll == null) _tagCalListAll = new List<TagCalcItem>();
                else _tagCalListAll.Clear();

                if (dt == null || dt.Rows.Count <= 0) return;

                #region DB to List
                foreach (DataRow dr in dt.Rows)
                {
                    string tagName = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                    string tagGbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                    string cTagName = string.Format("{0}", dr[Database.Fields.TAG_CAL.CTAGNAME]);

                    // 태그일련번호, 태그구분, 태그일련번호2가 동일하면 패스
                    if (_tagCalListAll.FindIndex(item => item.TAGNAME == tagName && item.TAG_GBN == tagGbn && item.CTAGNAME == cTagName) > 0) continue;

                    TagCalcItem tagCalItem = new TagCalcItem();
                    tagCalItem.TAGNAME = tagName;
                    tagCalItem.TAG_GBN = tagGbn;
                    tagCalItem.DUMMY_GBN = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                    tagCalItem.CTAGNAME = cTagName;
                    tagCalItem.LOC_CODE = string.Format("{0}", dr[Database.Fields.TAG.LOC_CODE]);
                    tagCalItem.LOC_NAME = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_NAME]);
                    tagCalItem.DESCRIPTION = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                    tagCalItem.TAG_GBN = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                    tagCalItem.CAL_GBN = string.Format("{0}", dr[Database.Fields.TAG_CAL.CAL_GBN]);
                    tagCalItem.CAL_GBN_ORG = string.Format("{0}", dr[Database.Fields.TAG_CAL.CAL_GBN]);
                    tagCalItem.TAG_DESC = string.Format("{0}", dr[Database.Fields.TAG.TAG_DESCRIPTION]);
                    tagCalItem.COEFFICIENT = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);

                    _tagCalListAll.Add(tagCalItem);
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("CreateTagCalList", ex);
            }
        } 
        #endregion

        /// <summary>
        /// 블록 태그 계층 구조 목록 생성
        /// </summary>
        /// <param name="dt">Block DataTable</param>
        #region private void CreateBlockTagList(DataTable dt)
        private void CreateBlockTagList(DataTable dt)
        {
            try
            {
                if (dt == null || dt.Rows.Count <= 0) return;

                if (_blockList == null) _blockList = new List<BlockTagCalcItem>();
                else _blockList.Clear();

                #region DB to List
                string bfLocCode = "";
                foreach (DataRow dr in dt.Rows)
                {
                    string locCode = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_CODE]);
                    if (_blockList.FindIndex(item => item.LOC_CODE == locCode) < 0)
                    {
                        #region Block Tag Cal List 생성
                        // 가상 태그 아이템 생성
                        BlockTagCalcItem blockItem = new BlockTagCalcItem();
                        blockItem.LOC_CODE = locCode;
                        blockItem.PLOC_CODE = string.Format("{0}", dr[Database.Fields.BLOCK.PLOC_CODE]);
                        blockItem.ORDERBY = string.Format("{0}", dr[Database.Fields.BLOCK.ORDERBY]);
                        string lv = string.Format("{0}", dr[Database.Fields.BLOCK.LEVEL]);
                        string locName = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_NAME]);
                        blockItem.LEVEL = Convert.ToInt32(lv);

                        blockItem.LOC_NAME = locName;
                        if (lv == "1") blockItem.LOC_NAME1 = locName;
                        else if (lv == "2") blockItem.LOC_NAME2 = locName;
                        else if (lv == "3") blockItem.LOC_NAME3 = locName;
                        else blockItem.LOC_NAME1 = locName;

                        #region TagCalIItem 처리
                        // 계산태그 아이템을 생성하여 가상 태그에 추가

                        var objects = from TagCalcItem item in _tagCalListAll
                                      where item.LOC_CODE == locCode
                                      select item;

                        if (objects != null && objects.Count() > 0)
                        {
                            for (int i = 0; i < objects.Count(); i++)
                            {
                                TagCalcItem tagCalItem = objects.ElementAt(i);
                                // 계산 태그의 태그구분에 맞게 데이터 처리
                                switch (tagCalItem.TAG_GBN)
                                {
                                    case Database.Fields.TAG_GUBUN.FRI:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            // 가상 태그일 경우의 처리
                                            blockItem.FRICal = true;
                                            if (blockItem.FRI.Length > 0) blockItem.FRI = string.Format("{0}{1}{2}", blockItem.FRI, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.FRI = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            // 일반 태그일 경우의 처리
                                            blockItem.FRICal = false;
                                            blockItem.FRI = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.FRIList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.TT:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.TTCal = true;
                                            if (blockItem.TT.Length > 0) blockItem.TT = string.Format("{0}{1}{2}", blockItem.TT, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.TT = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.TTCal = false;
                                            blockItem.TT = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.TTList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.YD:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.YDCal = true;
                                            if (blockItem.YD.Length > 0) blockItem.YD = string.Format("{0}{1}{2}", blockItem.YD, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.YD = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.YDCal = false;
                                            blockItem.YD = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.YDList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.YD_R:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.YD_RCal = true;
                                            if (blockItem.YD_R.Length > 0) blockItem.YD_R = string.Format("{0}{1}{2}", blockItem.YD_R, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.YD_R = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.YD_RCal = false;
                                            blockItem.YD_R = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.YD_RList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.FRQ_I:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.FRQ_ICal = true;
                                            if (blockItem.FRQ_I.Length > 0) blockItem.FRQ_I = string.Format("{0}{1}{2}", blockItem.FRQ_I, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.FRQ_I = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.FRQ_ICal = false;
                                            blockItem.FRQ_I = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.FRQ_IList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.FRQ_O:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.FRQ_OCal = true;
                                            if (blockItem.FRQ_O.Length > 0) blockItem.FRQ_O = string.Format("{0}{1}{2}", blockItem.FRQ_O, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.FRQ_O = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.FRQ_OCal = false;
                                            blockItem.FRQ_O = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.FRQ_OList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.SPL_D:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.SPL_DCal = true;
                                            if (blockItem.SPL_D.Length > 0) blockItem.SPL_D = string.Format("{0}{1}{2}", blockItem.SPL_D, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.SPL_D = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.SPL_DCal = false;
                                            blockItem.SPL_D = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.SPL_DList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.SPL_I:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.SPL_ICal = true;
                                            if (blockItem.SPL_I.Length > 0) blockItem.SPL_I = string.Format("{0}{1}{2}", blockItem.SPL_I, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.SPL_I = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.SPL_ICal = false;
                                            blockItem.SPL_I  = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.SPL_IList.Add(tagCalItem);
                                        break;
                                    case Database.Fields.TAG_GUBUN.SPL_O:
                                        if (tagCalItem.DUMMY_GBN.ToUpper() == "CAL")
                                        {
                                            blockItem.SPL_OCal = true;
                                            if (blockItem.SPL_O.Length > 0) blockItem.SPL_O = string.Format("{0}{1}{2}", blockItem.SPL_O, TagCalcItem.toCalStr(tagCalItem.CAL_GBN), tagCalItem.CTAGNAME);
                                            else blockItem.SPL_O = string.Format("{0}", tagCalItem.CTAGNAME);
                                        }
                                        else
                                        {
                                            blockItem.SPL_OCal = false;
                                            blockItem.SPL_O = tagCalItem.TAGNAME;
                                        }
                                        if (tagCalItem.CTAGNAME != null && tagCalItem.CTAGNAME.Length > 0) blockItem.SPL_OList.Add(tagCalItem);
                                        break;
                                }
                            }
                        }
                        #endregion

                        _blockList.Add(blockItem);
                        #endregion
                    }

                    bfLocCode = locCode;
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("CreateBlockTagList", ex);
            }
        } 
        #endregion
        #endregion

        #region 데이터 표현 (Grid 설정)
        /// <summary>
        /// 블록 태그 그리드 생성
        /// </summary>
        #region private void CreateBlockTagGrid()
        private void CreateBlockTagGrid()
        {
            try
            {
                if (_blockList == null || _blockList.Count <= 0) return;

                dgvBlockTag.BeginUpdate();
                RemoveRowsAll(dgvBlockTag);

                #region 대블록
                // LEVEL 1 : 대블록
                var blockObj0 = from BlockTagCalcItem item in _blockList
                                where item.LEVEL == 1
                                orderby item.ORDERBY ascending
                                select item;
                if (blockObj0 != null && blockObj0.Count() > 0)
                {
                    for (int i = 0; i < blockObj0.Count(); i++)
                    {
                        BlockTagCalcItem item0 = blockObj0.ElementAt(i);
                        // 대블록 Row 생성
                        UltraGridRow blockRow0 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                        blockRow0.Cells[Database.Fields.BLOCK.LEVEL].Value = item0.LEVEL;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_1].Value = item0.LOC_NAME1;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_2].Value = item0.LOC_NAME2;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_3].Value = item0.LOC_NAME3;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item0.LOC_CODE;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.FRI].Value = item0.FRI;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.TT].Value = item0.TT;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.YD].Value = item0.YD;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Value = item0.YD_R;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Value = item0.FRQ_I;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Value = item0.FRQ_O;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_D].Value = item0.SPL_D;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_I].Value = item0.SPL_I;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_O].Value = item0.SPL_O;

                        #region CAL일 경우 배경색 변경
                        // 가상태그일 경우 배경색을 다르게 표현
                        if (item0.FRICal) blockRow0.Cells[Database.Fields.TAG_GUBUN.FRI].Appearance.BackColor = Color.SkyBlue;
                        if (item0.TTCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.TT].Appearance.BackColor = Color.SkyBlue;
                        if (item0.YDCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.YD].Appearance.BackColor = Color.SkyBlue;
                        if (item0.YD_RCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Appearance.BackColor = Color.SkyBlue;
                        if (item0.FRQ_ICal) blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Appearance.BackColor = Color.SkyBlue;
                        if (item0.FRQ_OCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Appearance.BackColor = Color.SkyBlue;
                        if (item0.SPL_DCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_D].Appearance.BackColor = Color.SkyBlue;
                        if (item0.SPL_ICal) blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_I].Appearance.BackColor = Color.SkyBlue;
                        if (item0.SPL_OCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_O].Appearance.BackColor = Color.SkyBlue;
                        #endregion

                        #region 중블록
                        var blockObj1 = from BlockTagCalcItem item in _blockList
                                        where item.LEVEL == 2 && item.PLOC_CODE == item0.LOC_CODE
                                        orderby item.ORDERBY ascending
                                        select item;
                        if (blockObj1 != null && blockObj1.Count() > 0)
                        {
                            for (int j = 0; j < blockObj1.Count(); j++)
                            {
                                BlockTagCalcItem item1 = blockObj1.ElementAt(j);
                                // 중블록 Row 생성
                                UltraGridRow blockRow1 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                blockRow1.Cells[Database.Fields.BLOCK.LEVEL].Value = item1.LEVEL;
                                blockRow1.Cells[Database.Fields.BLOCK.LOC_NAME_1].Value = item1.LOC_NAME1;
                                blockRow1.Cells[Database.Fields.BLOCK.LOC_NAME_2].Value = item1.LOC_NAME2;
                                blockRow1.Cells[Database.Fields.BLOCK.LOC_NAME_3].Value = item1.LOC_NAME3;
                                blockRow1.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item1.LOC_CODE;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.FRI].Value = item1.FRI;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.TT].Value = item1.TT;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.YD].Value = item1.YD;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.YD_R].Value = item1.YD_R;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Value = item1.FRQ_I;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Value = item1.FRQ_O;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_D].Value = item1.SPL_D;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_I].Value = item1.SPL_I;
                                blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_O].Value = item1.SPL_O;

                                #region CAL일 경우 배경색 변경
                                // 가상태그일 경우 배경색 다르게 표현
                                if (item1.FRICal) blockRow1.Cells[Database.Fields.TAG_GUBUN.FRI].Appearance.BackColor = Color.SkyBlue;
                                if (item1.TTCal) blockRow1.Cells[Database.Fields.TAG_GUBUN.TT].Appearance.BackColor = Color.SkyBlue;
                                if (item1.YDCal) blockRow1.Cells[Database.Fields.TAG_GUBUN.YD].Appearance.BackColor = Color.SkyBlue;
                                if (item1.YD_RCal) blockRow1.Cells[Database.Fields.TAG_GUBUN.YD_R].Appearance.BackColor = Color.SkyBlue;
                                if (item1.FRQ_ICal) blockRow1.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Appearance.BackColor = Color.SkyBlue;
                                if (item1.FRQ_OCal) blockRow1.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Appearance.BackColor = Color.SkyBlue;
                                if (item1.SPL_DCal) blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_D].Appearance.BackColor = Color.SkyBlue;
                                if (item1.SPL_ICal) blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_I].Appearance.BackColor = Color.SkyBlue;
                                if (item1.SPL_OCal) blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_O].Appearance.BackColor = Color.SkyBlue;
                                #endregion

                                #region 소블록
                                var blockObj2 = from BlockTagCalcItem item in _blockList
                                                where item.LEVEL == 3 && item.PLOC_CODE == item1.LOC_CODE
                                                orderby item.ORDERBY ascending
                                                select item;
                                if (blockObj2 != null && blockObj2.Count() > 0)
                                {
                                    for (int k = 0; k < blockObj2.Count(); k++)
                                    {
                                        BlockTagCalcItem item2 = blockObj2.ElementAt(k);
                                        // 소블록 Row 생성
                                        UltraGridRow blockRow2 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                        blockRow2.Cells[Database.Fields.BLOCK.LEVEL].Value = item2.LEVEL;
                                        blockRow2.Cells[Database.Fields.BLOCK.LOC_NAME_1].Value = item2.LOC_NAME1;
                                        blockRow2.Cells[Database.Fields.BLOCK.LOC_NAME_2].Value = item2.LOC_NAME2;
                                        blockRow2.Cells[Database.Fields.BLOCK.LOC_NAME_3].Value = item2.LOC_NAME3;
                                        blockRow2.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item2.LOC_CODE;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.FRI].Value = item2.FRI;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.TT].Value = item2.TT;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.YD].Value = item2.YD;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.YD_R].Value = item2.YD_R;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Value = item2.FRQ_I;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Value = item2.FRQ_O;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_D].Value = item2.SPL_D;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_I].Value = item2.SPL_I;
                                        blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_O].Value = item2.SPL_O;

                                        #region CAL일 경우 배경색 변경
                                        // 가상태그일 경우 배경색 다르게 표현
                                        if (item2.FRICal) blockRow2.Cells[Database.Fields.TAG_GUBUN.FRI].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.TTCal) blockRow2.Cells[Database.Fields.TAG_GUBUN.TT].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.YDCal) blockRow2.Cells[Database.Fields.TAG_GUBUN.YD].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.YD_RCal) blockRow2.Cells[Database.Fields.TAG_GUBUN.YD_R].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.FRQ_ICal) blockRow2.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.FRQ_OCal) blockRow2.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.SPL_DCal) blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_D].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.SPL_ICal) blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_I].Appearance.BackColor = Color.SkyBlue;
                                        if (item2.SPL_OCal) blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_O].Appearance.BackColor = Color.SkyBlue;
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                #region 그외
                /*
                var blockEtc = from BlockTagCalcItem item in _blockList
                                where item.LEVEL == 9
                                orderby item.ORDERBY ascending
                                select item;
                if (blockEtc != null && blockEtc.Count() > 0)
                {
                    for (int i = 0; i < blockEtc.Count(); i++)
                    {
                        BlockTagCalcItem item0 = blockEtc.ElementAt(i);
                        UltraGridRow blockRow0 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                        blockRow0.Cells[Database.Fields.BLOCK.LEVEL].Value = item0.LEVEL;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_1].Value = item0.LOC_NAME1;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_2].Value = item0.LOC_NAME2;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_3].Value = item0.LOC_NAME3;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item0.LOC_CODE;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.FRI].Value = item0.FRI;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.TT].Value = item0.TT;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.YD].Value = item0.YD;
                        blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Value = item0.YD_R;

                        #region CAL일 경우 배경색 변경
                        if (item0.FRICal) blockRow0.Cells[Database.Fields.TAG_GUBUN.FRI].Appearance.BackColor = Color.SkyBlue;
                        if (item0.TTCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.TT].Appearance.BackColor = Color.SkyBlue;
                        if (item0.YDCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.YD].Appearance.BackColor = Color.SkyBlue;
                        if (item0.YD_RCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Appearance.BackColor = Color.SkyBlue;
                        if (item0.FRQ_ICal) blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Appearance.BackColor = Color.SkyBlue;
                        if (item0.FRQ_OCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Appearance.BackColor = Color.SkyBlue;
                        if (item0.SPL_DCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_D].Appearance.BackColor = Color.SkyBlue;
                        if (item0.SPL_ICal) blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_I].Appearance.BackColor = Color.SkyBlue;
                        if (item0.SPL_OCal) blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_O].Appearance.BackColor = Color.SkyBlue;
                        #endregion
                    }
                }*/
                #endregion

                if (dgvBlockTag.Rows.Count > 0)
                    dgvBlockTag.ActiveRowScrollRegion.ScrollRowIntoView(dgvBlockTag.Rows[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                FireException("CreateBlockTagGrid", ex);
            }
            finally
            {
                dgvBlockTag.EndUpdate();
            }
        } 
        #endregion

        #endregion





        /// <summary>
        /// TAG 그리드 보이기/숨기기 버튼
        /// </summary>
        #region private void btnShowTag_Click(object sender, EventArgs e)
        private void btnShowTag_Click(object sender, EventArgs e)
        {
            if (btnShowTag.Text == "◀")
            {
                splitContainer1.Panel2Collapsed = false;
                btnShowTag.Text = "▶";
            }
            else
            {
                splitContainer1.Panel2Collapsed = true;
                btnShowTag.Text = "◀";
            }
        }
        #endregion






        #region 블록 그리드 이벤트
        private void dgvBlockTag_AfterCellActivate(object sender, EventArgs e)
        {
            try
            {
                dgvCalc.BeginUpdate();
                RemoveRowsAll(dgvCalc);

                if (dgvBlockTag.ActiveCell == null) return;
                if (dgvBlockTag.ActiveCell.Column.Index < 7 && dgvBlockTag.ActiveCell.Column.Index > 10) return;

                // 선택된 블록 Row에서 정보 추출
                string locCode = string.Format("{0}", dgvBlockTag.ActiveRow.Cells[Database.Fields.BLOCK.LOC_CODE].Value);
                string tagGbn = "";
                if (dgvBlockTag.ActiveCell.Column.Index == 7) tagGbn = Database.Fields.TAG_GUBUN.FRI;
                else if (dgvBlockTag.ActiveCell.Column.Index == 8) tagGbn = Database.Fields.TAG_GUBUN.TT;
                else if (dgvBlockTag.ActiveCell.Column.Index == 9) tagGbn = Database.Fields.TAG_GUBUN.YD;
                else if (dgvBlockTag.ActiveCell.Column.Index == 10) tagGbn = Database.Fields.TAG_GUBUN.YD_R;
                else if (dgvBlockTag.ActiveCell.Column.Index == 11) tagGbn = Database.Fields.TAG_GUBUN.FRQ_I;
                else if (dgvBlockTag.ActiveCell.Column.Index == 12) tagGbn = Database.Fields.TAG_GUBUN.FRQ_O;
                else if (dgvBlockTag.ActiveCell.Column.Index == 13) tagGbn = Database.Fields.TAG_GUBUN.SPL_D;
                else if (dgvBlockTag.ActiveCell.Column.Index == 14) tagGbn = Database.Fields.TAG_GUBUN.SPL_I;
                else if (dgvBlockTag.ActiveCell.Column.Index == 15) tagGbn = Database.Fields.TAG_GUBUN.SPL_O;

                // 선택된 태그 정보 찾기
                var objects = from BlockTagCalcItem item in _blockList
                              where item.LOC_CODE == locCode
                              select item;
                if (objects != null && objects.Count() > 0)
                {
                    BlockTagCalcItem item = objects.First();
                    if (item == null) return;

                    bool tagCal = false;
                    List<TagCalcItem> tagList = null;
                    // 가상태그 여부와 계산 태그 목록 찾기
                    switch (tagGbn)
                    {
                        case Database.Fields.TAG_GUBUN.FRI:
                            tagCal = item.FRICal;
                            tagList = item.FRIList;
                            break;
                        case Database.Fields.TAG_GUBUN.TT:
                            tagCal = item.TTCal;
                            tagList = item.TTList;
                            break;
                        case Database.Fields.TAG_GUBUN.YD:
                            tagCal = item.YDCal;
                            tagList = item.YDList;
                            break;
                        case Database.Fields.TAG_GUBUN.YD_R:
                            tagCal = item.YD_RCal;
                            tagList = item.YD_RList;
                            break;
                        case Database.Fields.TAG_GUBUN.FRQ_I:
                            tagCal = item.FRQ_ICal;
                            tagList = item.FRQ_IList;
                            break;
                        case Database.Fields.TAG_GUBUN.FRQ_O:
                            tagCal = item.FRQ_OCal;
                            tagList = item.FRQ_OList;
                            break;
                        case Database.Fields.TAG_GUBUN.SPL_D:
                            tagCal = item.SPL_DCal;
                            tagList = item.SPL_DList;
                            break;
                        case Database.Fields.TAG_GUBUN.SPL_I:
                            tagCal = item.SPL_ICal;
                            tagList = item.SPL_IList;
                            break;
                        case Database.Fields.TAG_GUBUN.SPL_O:
                            tagCal = item.SPL_OCal;
                            tagList = item.SPL_OList;
                            break;
                    }

                    string tagName = "";
                    string tagDesc = "";
                    string dummyGbn = "";
                    if (tagList == null || tagList.Count <= 0)
                    {
                        // 선택된 태그에 태그 계산이 없을 경우
                        var tagObjs = from TagCalcItem tagItem in _tagCalListAll
                                      where tagItem.LOC_CODE == locCode && tagItem.TAG_GBN == tagGbn
                                      select tagItem;
                        if (tagObjs != null && tagObjs.Count() > 0)
                        {
                            TagCalcItem findTag = tagObjs.First();
                            tagName = findTag.TAGNAME;
                            tagDesc = findTag.DESCRIPTION;
                            dummyGbn = findTag.DUMMY_GBN;
                        }
                    }
                    else
                    {
                        if (tagCal)
                        {
                            // 가상 태그일 경우
                            for (int i = 0; i < tagList.Count; i++)
                            {
                                TagCalcItem tagCalItem = tagList[i];
                                tagName = tagCalItem.TAGNAME;
                                tagDesc = tagCalItem.DESCRIPTION;
                                dummyGbn = tagCalItem.DUMMY_GBN;

                                // 삭제된 아이템일 경우 패스
                                if (tagCalItem.State == TagCalcItem.StateType.Del) continue;

                                // 계산 태그의 목록 생성
                                UltraGridRow row = dgvCalc.DisplayLayout.Bands[0].AddNew();
                                row.Cells[Database.Fields.TAG.TAGNAME].Value = tagCalItem.TAGNAME;
                                row.Cells[Database.Fields.TAG_CAL.CAL_GBN].Value = TagCalcItem.toCalStr(tagCalItem.CAL_GBN);
                                row.Cells[Database.Fields.TAG_CAL.CTAGNAME].Value = tagCalItem.CTAGNAME;
                                row.Cells[Database.Fields.TAG.DESC].Value = tagCalItem.TAG_DESC;
                                row.Cells[Database.Fields.TAG.COEFFICIENT].Value = tagCalItem.COEFFICIENT;
                            }
                        }
                        else
                        {
                            tagName = tagList[0].TAGNAME;
                            tagDesc = tagList[0].DESCRIPTION;
                            dummyGbn = tagList[0].DUMMY_GBN;
                        }
                    }
                    
                    lblTagname.Text = tagName;
                    lblTagDesc.Text = tagDesc;
                    lblDummyGbn.Text = dummyGbn;
                    lblTagGbn.Text = tagGbn;
                    lblLocCode.Text = locCode;
                }
            }
            catch (Exception ex)
            {
                FireException("dgvBlockTag_AfterCellActivate", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dgvCalc.EndUpdate();
            }
        }
        #endregion





        #region 태그 조회
        /// <summary>
        /// 태그 조회 (태그일련번호)
        /// </summary>
        #region private void txtSearchTagname_KeyUp(object sender, KeyEventArgs e)
        private void txtSearchTagname_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) txtSearchTagDesc.Focus();
        }
        #endregion

        /// <summary>
        /// 태그 조회 (태그업무설명)
        /// </summary>
        #region private void txtSearchTagDesc_KeyDown(object sender, KeyEventArgs e)
        private void txtSearchTagDesc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btnSearchTag_Click(null, null);
        }
        #endregion

        /// <summary>
        /// 태그 조회 버튼
        /// </summary>
        #region private void btnSearchTag_Click(object sender, EventArgs e)
        private void btnSearchTag_Click(object sender, EventArgs e)
        {
            lblTagSearch.BringToFront();
            Cursor = Cursors.WaitCursor;

            try
            {

                RemoveRowsAll(dgvTag);

                int popWidth = (dgvTag.Width / 2) - (lblTagSearch.Width / 2);
                int popHeight = (dgvTag.Height / 2) - (lblTagSearch.Height / 2);
                lblTagSearch.Location = new Point(popWidth + dgvTag.Location.X, popHeight + dgvTag.Location.Y);
                lblTagSearch.Update();

                #region DB 데이터 조회
                DataTable dt = new DataTable();
                EMFrame.dm.EMapper mapper = null;

                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                
                string searchGbn = "";
                if (cbSearchGbn.SelectedIndex == 0) // 전체
                {
                    //searchGbn = "'RT','FRI','MNF','TT','TD','FRQ','FRQ_I','FRQ_D','SPL_D'";
                    searchGbn = "'" +
                        Database.Fields.TAG_GUBUN.RT + "','" + // 순시
                        Database.Fields.TAG_GUBUN.FRI + "','" +
                        Database.Fields.TAG_GUBUN.MNF + "','" +
                        Database.Fields.TAG_GUBUN.TT + "','" + // 적산
                        Database.Fields.TAG_GUBUN.TD + "','" +
                        Database.Fields.TAG_GUBUN.YD + "','" + // 적산차
                        Database.Fields.TAG_GUBUN.YD_R + "','" +
                        Database.Fields.TAG_GUBUN.FRQ + "','" +
                        Database.Fields.TAG_GUBUN.FRQ_I + "','" +
                        Database.Fields.TAG_GUBUN.FRQ_O + "','" +
                        Database.Fields.TAG_GUBUN.SPL_D + "','" +
                        Database.Fields.TAG_GUBUN.SPL_I + "','" +
                        Database.Fields.TAG_GUBUN.SPL_O + "'";
                }
                else if (cbSearchGbn.SelectedIndex == 1) // 순시
                {
                    //searchGbn = "'RT','FRI','MNF'";
                    searchGbn = "'" +
                        Database.Fields.TAG_GUBUN.RT + "','" +
                        Database.Fields.TAG_GUBUN.FRI + "','" +
                        Database.Fields.TAG_GUBUN.MNF + "'";
                }
                else if (cbSearchGbn.SelectedIndex == 2) // 적산
                {
                    //searchGbn = "'TT','TD'";
                    searchGbn = "'" +
                        Database.Fields.TAG_GUBUN.TT + "','" +
                        Database.Fields.TAG_GUBUN.TD + "'";
                }
                else if (cbSearchGbn.SelectedIndex == 3) // 적산차
                {
                    //searchGbn = "'FRQ','FRQ_I','FRQ_D','SPL_D'";
                    searchGbn = "'" +
                        Database.Fields.TAG_GUBUN.YD + "','" +
                        Database.Fields.TAG_GUBUN.YD_R + "','" +
                        Database.Fields.TAG_GUBUN.FRQ + "','" +
                        Database.Fields.TAG_GUBUN.FRQ_I + "','" +
                        Database.Fields.TAG_GUBUN.FRQ_O + "','" +
                        Database.Fields.TAG_GUBUN.SPL_D + "','" +
                        Database.Fields.TAG_GUBUN.SPL_I + "','" +
                        Database.Fields.TAG_GUBUN.SPL_O + "'";
                }

                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.TAGCALC_TAG_SEARCH, txtSearchTagname.Text, txtSearchTagDesc.Text, searchGbn), null);
                #endregion


                DataTable dtSearch = new DataTable();
                dtSearch.Columns.Add(new DataColumn(Database.Fields.BLOCK.LOC_NAME_0, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.BLOCK.LOC_NAME_1, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.BLOCK.LOC_NAME_2, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.BLOCK.LOC_NAME_3, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.TAG.DESC, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.TAG.TAGNAME, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.TAG.COEFFICIENT, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.TAG_GUBUN.DUMMY_GBN, typeof(string)));
                dtSearch.Columns.Add(new DataColumn(Database.Fields.TAG_GUBUN.TAG_GBN, typeof(string)));

                DataRow drNew = null;

                if (dt != null && dt.Rows.Count > 0)
                {
                    #region 대블록
                    var blockObj0 = from BlockTagCalcItem item in _blockList
                                    where item.LEVEL == 1
                                    orderby item.ORDERBY ascending
                                    select item;
                    if (blockObj0 != null && blockObj0.Count() > 0)
                    {
                        for (int i = 0; i < blockObj0.Count(); i++)
                        {
                            // 대블록 Row 생성
                            BlockTagCalcItem block0 = blockObj0.ElementAt(i);
                            drNew = dtSearch.NewRow();
                            drNew[Database.Fields.BLOCK.LOC_NAME_1] = block0.LOC_NAME1;
                            drNew[Database.Fields.BLOCK.LOC_NAME_2] = block0.LOC_NAME2;
                            drNew[Database.Fields.BLOCK.LOC_NAME_3] = block0.LOC_NAME3;
                            dtSearch.Rows.Add(drNew);

                            #region TAG 처리
                            // 대블록의 Tag 찾기
                            DataRow[] drs0 = dt.Select(
                                Database.Fields.BLOCK.LOC_CODE + " = '" + block0.LOC_CODE                      + "' AND " +
                                Database.Fields.BLOCK.SGCCD    + " = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'"
                            );
                            if (drs0 != null && drs0.Length > 0)
                            {
                                foreach (DataRow dr in drs0)
                                {
                                    // 대블록 태그 Row 생성
                                    drNew = dtSearch.NewRow();
                                    drNew[Database.Fields.TAG.DESC] = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                                    drNew[Database.Fields.TAG.TAGNAME] = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                                    drNew[Database.Fields.TAG.COEFFICIENT] = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);
                                    drNew[Database.Fields.TAG_GUBUN.DUMMY_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                                    drNew[Database.Fields.TAG_GUBUN.TAG_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                                    dtSearch.Rows.Add(drNew);
                                }
                            }
                            #endregion

                            #region 중블록
                            var blockObj1 = from BlockTagCalcItem item in _blockList
                                            where item.LEVEL == 2 && item.PLOC_CODE == block0.LOC_CODE
                                            orderby item.ORDERBY ascending
                                            select item;
                            if (blockObj1 != null && blockObj1.Count() > 0)
                            {
                                for (int j = 0; j < blockObj1.Count(); j++)
                                {
                                    // 중블록 Row 생성
                                    BlockTagCalcItem block1 = blockObj1.ElementAt(j);
                                    drNew = dtSearch.NewRow();
                                    drNew[Database.Fields.BLOCK.LOC_NAME_1] = block1.LOC_NAME1;
                                    drNew[Database.Fields.BLOCK.LOC_NAME_2] = block1.LOC_NAME2;
                                    drNew[Database.Fields.BLOCK.LOC_NAME_3] = block1.LOC_NAME3;
                                    dtSearch.Rows.Add(drNew);

                                    #region TAG 처리
                                    // 중블록 Tag 찾기
                                    DataRow[] drs1 = dt.Select(
                                        Database.Fields.BLOCK.LOC_CODE + " = '" + block1.LOC_CODE                      + "' AND " +
                                        Database.Fields.BLOCK.SGCCD    + " = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'"
                                    );
                                    if (drs1 != null && drs1.Length > 0)
                                    {
                                        foreach (DataRow dr in drs1)
                                        {
                                            // 중블록 태그 Row 생성
                                            drNew = dtSearch.NewRow();
                                            drNew[Database.Fields.TAG.DESC] = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                                            drNew[Database.Fields.TAG.TAGNAME] = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                                            drNew[Database.Fields.TAG.COEFFICIENT] = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);
                                            drNew[Database.Fields.TAG_GUBUN.DUMMY_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                                            drNew[Database.Fields.TAG_GUBUN.TAG_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                                            dtSearch.Rows.Add(drNew);
                                        }
                                    }
                                    #endregion

                                    #region 소블록
                                    var blockObj2 = from BlockTagCalcItem item in _blockList
                                                    where item.LEVEL == 3 && item.PLOC_CODE == block1.LOC_CODE
                                                    orderby item.ORDERBY ascending
                                                    select item;
                                    if (blockObj2 != null && blockObj2.Count() > 0)
                                    {
                                        for (int k = 0; k < blockObj2.Count(); k++)
                                        {
                                            // 소블록 Row 생성
                                            BlockTagCalcItem block2 = blockObj2.ElementAt(k);
                                            drNew = dtSearch.NewRow();
                                            drNew[Database.Fields.BLOCK.LOC_NAME_1] = block2.LOC_NAME1;
                                            drNew[Database.Fields.BLOCK.LOC_NAME_2] = block2.LOC_NAME2;
                                            drNew[Database.Fields.BLOCK.LOC_NAME_3] = block2.LOC_NAME3;
                                            dtSearch.Rows.Add(drNew);

                                            #region TAG 처리
                                            // 소블록 태그 찾기
                                            DataRow[] drs2 = dt.Select(
                                                Database.Fields.BLOCK.LOC_CODE + " = '" + block2.LOC_CODE + "' AND " +
                                                Database.Fields.BLOCK.SGCCD + " = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'"
                                            );
                                            if (drs2 != null && drs2.Length > 0)
                                            {
                                                foreach (DataRow dr in drs2)
                                                {
                                                    // 소블록 태그 Row 생성
                                                    drNew = dtSearch.NewRow();
                                                    drNew[Database.Fields.TAG.DESC] = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                                                    drNew[Database.Fields.TAG.TAGNAME] = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                                                    drNew[Database.Fields.TAG.COEFFICIENT] = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);
                                                    drNew[Database.Fields.TAG_GUBUN.DUMMY_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                                                    drNew[Database.Fields.TAG_GUBUN.TAG_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                                                    dtSearch.Rows.Add(drNew);
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region [삭제] 그외 (배수지, 정수장..)
                    /*var etcObj = from BlockTagCalcItem item in _blockList
                                 where item.LEVEL == 9
                                 orderby item.ORDERBY ascending
                                 select item;
                    if (etcObj != null && etcObj.Count() > 0)
                    {
                        for (int i = 0; i < etcObj.Count(); i++)
                        {
                            BlockTagCalcItem etc = etcObj.ElementAt(i);
                            drNew = dtSearch.NewRow();
                            drNew[Database.Fields.BLOCK.LOC_NAME_1] = etc.LOC_NAME1;
                            drNew[Database.Fields.BLOCK.LOC_NAME_2] = etc.LOC_NAME2;
                            drNew[Database.Fields.BLOCK.LOC_NAME_3] = etc.LOC_NAME3;
                            dtSearch.Rows.Add(drNew);

                            #region TAG 처리
                            DataRow[] drs0 = dt.Select(
                                Database.Fields.BLOCK.LOC_CODE + " = '" + etc.LOC_CODE + "' AND " +
                                Database.Fields.BLOCK.SGCCD + " = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'"
                            );
                            if (drs0 != null && drs0.Length > 0)
                            {
                                foreach (DataRow dr in drs0)
                                {
                                    drNew = dtSearch.NewRow();
                                    drNew[Database.Fields.TAG.DESC] = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                                    drNew[Database.Fields.TAG.TAGNAME] = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                                    drNew[Database.Fields.TAG.COEFFICIENT] = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);
                                    drNew[Database.Fields.TAG_GUBUN.TAG_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                                    dtSearch.Rows.Add(drNew);
                                }
                            }
                            #endregion
                        }
                    }*/
                    #endregion

                    #region 미지정
                    DataRow[] drs = dt.Select(Database.Fields.BLOCK.LEVEL + " = '8'");
                    if (drs != null && drs.Length > 0)
                    {
                        drNew = dtSearch.NewRow();
                        drNew[Database.Fields.BLOCK.LOC_NAME_1] = "미지정";
                        drNew[Database.Fields.BLOCK.LOC_NAME_2] = "미지정";
                        drNew[Database.Fields.BLOCK.LOC_NAME_3] = "미지정";
                        dtSearch.Rows.Add(drNew);

                        foreach (DataRow dr in drs)
                        {
                            drNew = dtSearch.NewRow();
                            drNew[Database.Fields.TAG.DESC] = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                            drNew[Database.Fields.TAG.TAGNAME] = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                            drNew[Database.Fields.TAG.COEFFICIENT] = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);
                            drNew[Database.Fields.TAG_GUBUN.DUMMY_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                            drNew[Database.Fields.TAG_GUBUN.TAG_GBN] = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                            dtSearch.Rows.Add(drNew);
                        }
                    }
                    #endregion
                }

                dgvTag.DataSource = dtSearch;

                if (dgvTag.Rows.Count > 0)
                    dgvTag.ActiveRowScrollRegion.ScrollRowIntoView(dgvTag.Rows[0]);

                //Console.WriteLine(string.Format("[{0}] 태그조회 End", DateTime.Now.ToString("HH:mm:ss:fff")));
            }
            catch (Exception ex)
            {
                FireException("btnSearchTag_Click", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dgvTag.BringToFront();
                Cursor = Cursors.Arrow;
            }
        }
        #endregion

        #endregion






        #region Drag & Drop
        #region private void dgvTag_SelectionDrag(object sender, CancelEventArgs e)
        private void dgvTag_SelectionDrag(object sender, CancelEventArgs e)
        {
            // 선택 태그 없을시 종료
            if (dgvTag.Selected == null || dgvTag.Selected.Rows.Count <= 0) return;
            // Drag Drop 시작
            dgvTag.DoDragDrop(dgvTag.Selected.Rows, DragDropEffects.Copy);
        } 
        #endregion

        #region private void dgvCalc_DragOver(object sender, DragEventArgs e)
        private void dgvCalc_DragOver(object sender, DragEventArgs e)
        {
            // 태그 선택이 없을시 종료
            if (dgvTag.Selected == null || dgvTag.Selected.Rows.Count <= 0) return;
            e.Effect = DragDropEffects.Copy;
        } 
        #endregion

        /// <summary>
        /// 태그를 태그 계산 그리드에 놓기
        /// </summary>
        #region private void dgvCalc_DragDrop(object sender, DragEventArgs e)
        private void dgvCalc_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (lblDummyGbn.Text != "CAL") return;
                if (dgvTag.Selected == null || dgvTag.Selected.Rows.Count <= 0) return;

                if (e.Effect == DragDropEffects.Copy)
                {
                    string tagName = lblTagname.Text;
                    string tagGbnOrg = "";
                    string tagGbn = tagGbnOrg  = lblTagGbn.Text;
                    
                    // 순시, 적산, 적산차로 데이터 임의 조정 (세가지 구분으로 코드를 처리함)
                    if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.FRI) tagGbn = Database.Fields.TAG_GUBUN.RT;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.TT) tagGbn = Database.Fields.TAG_GUBUN.TD;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.YD) tagGbn = Database.Fields.TAG_GUBUN.FRQ;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.YD_R) tagGbn = Database.Fields.TAG_GUBUN.FRQ;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.FRQ_I) tagGbn = Database.Fields.TAG_GUBUN.FRQ;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.FRQ_O) tagGbn = Database.Fields.TAG_GUBUN.FRQ;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.SPL_D) tagGbn = Database.Fields.TAG_GUBUN.FRQ;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.SPL_I) tagGbn = Database.Fields.TAG_GUBUN.FRQ;
                    else if (tagGbn.ToUpper() == Database.Fields.TAG_GUBUN.SPL_O) tagGbn = Database.Fields.TAG_GUBUN.FRQ;

                    // 블록 찾기
                    BlockTagCalcItem blockTagCalcItem = _blockList.Find(item => item.LOC_CODE == lblLocCode.Text);
                    if (blockTagCalcItem == null || !blockTagCalcItem.GetCalc(tagGbnOrg)) return;

                    for (int i = 0; i < dgvTag.Selected.Rows.Count; i++)
                    {
                        // 태그의 정보 추출
                        string cTagName = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG.TAGNAME].Value);
                        string cTagDesc = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG.DESC].Value);
                        string cTagCoefficient = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG.COEFFICIENT].Value);
                        string cTagGbn = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG_GUBUN.TAG_GBN].Value);
                        string cTagDummyGbn = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Value);

                        // 계산식일경우 제외
                        if (cTagDummyGbn.ToUpper() == "CAL") continue;
                        // 태그구분이 다를 경우 제외
                        if (tagGbn != cTagGbn) continue;
                        // 태그명이 없을 경우 제외(블록일 경우)
                        if (cTagName == null || cTagName.Length <= 0) continue;

                        #region 중복 체크
                        bool isPass = false;
                        for (int j = 0; j < dgvCalc.Rows.Count; j++)
                        {
                            string checkTagName = string.Format("{0}", dgvCalc.Rows[j].Cells[Database.Fields.TAG_CAL.CTAGNAME].Value);
                            if (checkTagName == cTagName)
                            {
                                isPass = true;
                                break;
                            }
                        }
                        if (isPass) continue;
                        #endregion

                        #region 계산식 Row 추가
                        dgvCalc.BeginUpdate();
                        UltraGridRow newRow = dgvCalc.DisplayLayout.Bands[0].AddNew();
                        newRow.Cells[Database.Fields.TAG.TAGNAME].Value = tagName;
                        newRow.Cells[Database.Fields.TAG_CAL.CAL_GBN].Value = "P";
                        newRow.Cells[Database.Fields.TAG_CAL.CTAGNAME].Value = cTagName;
                        newRow.Cells[Database.Fields.TAG.DESC].Value = cTagDesc;
                        newRow.Cells[Database.Fields.TAG.COEFFICIENT].Value = cTagCoefficient;
                        dgvCalc.EndUpdate();
                        #endregion

                        #region 데이터 반영
                        List<TagCalcItem> list = blockTagCalcItem.GetItemList(lblTagGbn.Text);

                        // 새로운 태그 계산식 아이템 생성
                        TagCalcItem newItem = new TagCalcItem();
                        newItem.LOC_CODE = blockTagCalcItem.LOC_CODE;
                        newItem.LOC_NAME = blockTagCalcItem.LOC_NAME;
                        newItem.TAGNAME = tagName;
                        newItem.DESCRIPTION = lblTagDesc.Text;
                        newItem.TAG_GBN = lblTagGbn.Text;
                        newItem.CAL_GBN = "P";
                        newItem.CAL_GBN_ORG = "P";
                        newItem.DUMMY_GBN = lblDummyGbn.Text;
                        newItem.CTAGNAME = cTagName;
                        newItem.TAG_DESC = cTagDesc;
                        newItem.COEFFICIENT = cTagCoefficient;
                        newItem.State = TagCalcItem.StateType.New;

                        // 동일 태그가 계산식에 없을 경우에만 추가
                        if (list.FindIndex(item => item.CTAGNAME == cTagName) < 0)
                        {
                            list.Add(newItem);
                            // 블럭에도 갱신 설정
                            blockTagCalcItem.IsChanged = true;
                        }
                        #endregion
                    }

                    ResetTagCalc(blockTagCalcItem, dgvBlockTag.ActiveRow, tagGbnOrg, blockTagCalcItem.GetItemList(tagGbnOrg));
                }
            }
            catch (Exception ex)
            {
                FireException("dgvCalc_DragDrop", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion





        #region private void dgvCalc_SelectionDrag(object sender, CancelEventArgs e)
        private void dgvCalc_SelectionDrag(object sender, CancelEventArgs e)
        {
            if (dgvCalc.Selected == null || dgvCalc.Selected.Rows.Count <= 0) return;
            dgvCalc.DoDragDrop(dgvCalc.Selected.Rows, DragDropEffects.Copy);
        } 
        #endregion

        #region private void dgvTag_DragOver(object sender, DragEventArgs e)
        private void dgvTag_DragOver(object sender, DragEventArgs e)
        {
            if (dgvCalc.Selected == null || dgvCalc.Selected.Rows.Count <= 0) return;
            e.Effect = DragDropEffects.Copy;
        } 
        #endregion

        /// <summary>
        /// 태그 계산을 태그 조회 그리드에 놓기 (삭제)
        /// </summary>
        #region private void dgvTag_DragDrop(object sender, DragEventArgs e)
        private void dgvTag_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (dgvCalc.Selected == null || dgvCalc.Selected.Rows.Count <= 0) return;

                if (e.Effect == DragDropEffects.Copy)
                {
                    BlockTagCalcItem blockTagCalcItem = _blockList.Find(item => item.LOC_CODE == lblLocCode.Text);
                    if (blockTagCalcItem == null) return;

                    if (MessageBox.Show("태그 계산식을 삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                    List<TagCalcItem> list = blockTagCalcItem.GetItemList(lblTagGbn.Text);
                                        
                    for (int i = 0; i < dgvCalc.Selected.Rows.Count; i++)
                    {
                        string cTagName = string.Format("{0}", dgvCalc.Selected.Rows[i].Cells[Database.Fields.TAG_CAL.CTAGNAME].Value);
                        TagCalcItem tagCalcItem = list.Find(item => item.CTAGNAME == cTagName);
                        if (tagCalcItem == null) continue;

                        // 선택된 계산 태그가 신규 등록된 태그일 경우 목록에서 삭제, 아니면 'DEL' 표시
                        if (tagCalcItem.State == TagCalcItem.StateType.New) list.RemoveAt(list.FindIndex(item => item.CTAGNAME == cTagName));
                        else tagCalcItem.State = TagCalcItem.StateType.Del;
                    }
                    // 블록에도 변경 설정
                    blockTagCalcItem.IsChanged = true;

                    ResetTagCalc(blockTagCalcItem, dgvBlockTag.ActiveRow, lblTagGbn.Text, blockTagCalcItem.GetItemList(lblTagGbn.Text));

                    dgvCalc.DeleteSelectedRows(false);
                }
            }
            catch (Exception ex)
            {
                FireException("dgvTag_DragDrop", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion
        #endregion




        /// <summary>
        /// 블록 태그 그리드에 변경된 계산 태그정보 출력
        /// </summary>
        /// <param name="blockTagCalcItem">태그 아이템(가상태그 + 일반 태그)</param>
        /// <param name="row">Grid Row</param>
        /// <param name="tagGbn">태그 구분</param>
        /// <param name="list">계산 태그 목록</param>
        #region private void ResetTagCalc(BlockTagCalcItem blockTagCalcItem, UltraGridRow row, string tagGbn, List<TagCalcItem> list)
        private void ResetTagCalc(BlockTagCalcItem blockTagCalcItem, UltraGridRow row, string tagGbn, List<TagCalcItem> list)
        {
            // 계산 태그 정보 초기화
            row.Cells[tagGbn].Value = "";
            if (tagGbn == Database.Fields.TAG_GUBUN.FRI) blockTagCalcItem.FRI = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.TT) blockTagCalcItem.TT = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.YD) blockTagCalcItem.YD = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.YD_R) blockTagCalcItem.YD_R = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.FRQ_I) blockTagCalcItem.FRQ_I = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.FRQ_O) blockTagCalcItem.FRQ_O = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.SPL_D) blockTagCalcItem.SPL_D = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.SPL_I) blockTagCalcItem.SPL_I = "";
            else if (tagGbn == Database.Fields.TAG_GUBUN.SPL_O) blockTagCalcItem.SPL_O = "";

            string calcTags = "";
            for (int i = 0; i < list.Count; i++)
            {
                // 계산 태그의 상태가 '삭제'일 경우 종료
                if (list[i].State == TagCalcItem.StateType.Del) continue;
                switch (tagGbn)
                {
                    case Database.Fields.TAG_GUBUN.FRI:
                        if (blockTagCalcItem.FRI.Length <= 0) blockTagCalcItem.FRI = list[i].CTAGNAME;
                        else blockTagCalcItem.FRI = string.Format("{0}{1}{2}", blockTagCalcItem.FRI, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.FRI;
                        break;
                    case Database.Fields.TAG_GUBUN.TT:
                        if (blockTagCalcItem.TT.Length <= 0) blockTagCalcItem.TT = list[i].CTAGNAME;
                        else blockTagCalcItem.TT = string.Format("{0}{1}{2}", blockTagCalcItem.TT, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.TT;
                        break;
                    case Database.Fields.TAG_GUBUN.YD:
                        if (blockTagCalcItem.YD.Length <= 0) blockTagCalcItem.YD = list[i].CTAGNAME;
                        else blockTagCalcItem.YD = string.Format("{0}{1}{2}", blockTagCalcItem.YD, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.YD;
                        break;
                    case Database.Fields.TAG_GUBUN.YD_R:
                        if (blockTagCalcItem.YD_R.Length <= 0) blockTagCalcItem.YD_R = list[i].CTAGNAME;
                        else blockTagCalcItem.YD_R = string.Format("{0}{1}{2}", blockTagCalcItem.YD_R, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.YD_R;
                        break;

                    case Database.Fields.TAG_GUBUN.FRQ_I:
                        if (blockTagCalcItem.FRQ_I.Length <= 0) blockTagCalcItem.FRQ_I = list[i].CTAGNAME;
                        else blockTagCalcItem.FRQ_I = string.Format("{0}{1}{2}", blockTagCalcItem.FRQ_I, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.FRQ_I;
                        break;
                    case Database.Fields.TAG_GUBUN.FRQ_O:
                        if (blockTagCalcItem.FRQ_O.Length <= 0) blockTagCalcItem.FRQ_O = list[i].CTAGNAME;
                        else blockTagCalcItem.FRQ_O = string.Format("{0}{1}{2}", blockTagCalcItem.FRQ_O, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.FRQ_O;
                        break;
                    case Database.Fields.TAG_GUBUN.SPL_D:
                        if (blockTagCalcItem.SPL_D.Length <= 0) blockTagCalcItem.SPL_D = list[i].CTAGNAME;
                        else blockTagCalcItem.SPL_D = string.Format("{0}{1}{2}", blockTagCalcItem.SPL_D, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.SPL_D;
                        break;
                    case Database.Fields.TAG_GUBUN.SPL_I:
                        if (blockTagCalcItem.SPL_I.Length <= 0) blockTagCalcItem.SPL_I = list[i].CTAGNAME;
                        else blockTagCalcItem.SPL_I = string.Format("{0}{1}{2}", blockTagCalcItem.SPL_I, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.SPL_I;
                        break;
                    case Database.Fields.TAG_GUBUN.SPL_O:
                        if (blockTagCalcItem.SPL_O.Length <= 0) blockTagCalcItem.SPL_O = list[i].CTAGNAME;
                        else blockTagCalcItem.SPL_O = string.Format("{0}{1}{2}", blockTagCalcItem.SPL_O, TagCalcItem.toCalStr(list[i].CAL_GBN), list[i].CTAGNAME);
                        calcTags = blockTagCalcItem.SPL_O;
                        break;
                }
            }

            row.Cells[tagGbn].Value = calcTags;
        } 
        #endregion

        #region 태그 계산식 그리드 이벤트
        private void dgvCalc_CellChange(object sender, CellEventArgs e)
        {
            try
            {
                // 사용자가 입력한 정보 추출
                string locCode = lblLocCode.Text;
                string tagName = string.Format("{0}", dgvCalc.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Value);
                string cTagName = string.Format("{0}", dgvCalc.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG_CAL.CTAGNAME].Value);
                string tagGbn = lblTagGbn.Text;
                string calGbn = TagCalcItem.toStrCal(string.Format("{0}", e.Cell.Text));

                // 계산 태그 찾기
                BlockTagCalcItem blockTagCalcItem = _blockList.Find(item => item.LOC_CODE == locCode);
                if (blockTagCalcItem != null)
                {
                    List<TagCalcItem> list = blockTagCalcItem.GetItemList(tagGbn);
                    TagCalcItem tagCalcItem = list.Find(item => item.CTAGNAME == cTagName);
                    tagCalcItem.CAL_GBN = calGbn;
                    // 태그의 상태가 'None'일 경우에만 상태 변경
                    // (New, Del일 경우 그 상태 그대로)
                    if (tagCalcItem.CAL_GBN_ORG == calGbn)
                    {                        
                        if (tagCalcItem.State != TagCalcItem.StateType.New) tagCalcItem.State = TagCalcItem.StateType.None;
                    }
                    else
                    {
                        if (tagCalcItem.State == TagCalcItem.StateType.None)
                        {
                            tagCalcItem.State = TagCalcItem.StateType.Mod;
                            blockTagCalcItem.IsChanged = true;
                        }
                    }

                    // 블록 그리드의 계산 태그 재구성
                    ResetTagCalc(blockTagCalcItem, dgvBlockTag.ActiveRow, tagGbn, list);                    
                }
            }
            catch (Exception ex)
            {
                FireException("dgvCalc_CellChange", ex);
                MessageBox.Show(ex.Message);
            }
        }
        #endregion








        /// <summary>
        /// 저장 버튼 이벤트
        /// </summary>
        #region private void btnSave_Click(object sender, EventArgs e)
        private void btnSave_Click(object sender, EventArgs e)
        {
            EMFrame.dm.EMapper mapper = null;
            try
            {
                if (MessageBox.Show("저장하시겠습니까?", "변경내용 저장", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper.BeginTransaction();

                for (int i = 0; i < _blockList.Count; i++)
                {
                    BlockTagCalcItem blockTagCalcItem = _blockList[i];
                    if (!blockTagCalcItem.IsChanged) continue;

                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.FRI);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.TT);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.YD);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.YD_R);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.FRQ_I);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.FRQ_O);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.SPL_D);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.SPL_I);
                    Save(mapper, blockTagCalcItem, Database.Fields.TAG_GUBUN.SPL_O);
                }

                mapper.CommitTransaction();

                MessageBox.Show("처리되었습니다.", "변경내용 저장", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ClearForm();
                Reload();                
            }
            catch (Exception ex)
            {
                if (mapper != null) mapper.RollbackTransaction();
                FireException("btnSave_Click", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }
        } 
        #endregion

        /// <summary>
        /// 태그 계산식 변경 내용 저장
        /// </summary>
        /// <param name="mapper">EMapper</param>
        /// <param name="list">TagCalcItem List</param>
        /// <param name="tagGbn">태그 구분</param>
        /// <param name="isCopy">복사 여부(RT, MNF)</param>
        #region private void Save(EMFrame.dm.EMapper mapper, List<TagCalcItem> list, string tagGbn)
        private void Save(EMFrame.dm.EMapper mapper, BlockTagCalcItem blockTagCalcItem, string tagGbn)
        {
            Console.WriteLine(tagGbn);
            try
            {
                List<TagCalcItem> list = blockTagCalcItem.GetItemList(tagGbn);
                bool isCopy = blockTagCalcItem.GetCopyYesNo(tagGbn);

                for (int i = 0; i < list.Count; i++)
                {
                    TagCalcItem tagCalcItem = list[i];
                    if (tagCalcItem.State == TagCalcItem.StateType.None) continue;

                    Console.WriteLine(string.Format("[{0}] {1}", tagCalcItem.State, tagCalcItem.CTAGNAME));
                    
                    if (tagCalcItem.State == TagCalcItem.StateType.New ||
                        tagCalcItem.State == TagCalcItem.StateType.Mod)
                    {
                        IDataParameter[] parameters = {
                                                      new OracleParameter("1", OracleDbType.Varchar2),
                                                      new OracleParameter("2", OracleDbType.Varchar2),
                                                      new OracleParameter("3", OracleDbType.Varchar2)
                                                  };
                        parameters[0].Value = tagCalcItem.TAGNAME;
                        parameters[1].Value = tagCalcItem.CTAGNAME;
                        parameters[2].Value = tagCalcItem.CAL_GBN;

                        mapper.ExecuteScript(Database.Queries.TAGCALC_MERGE_INS_UPD, parameters);
                    }
                    else if (tagCalcItem.State == TagCalcItem.StateType.Del)
                    {
                        IDataParameter[] parameters = {
                                                          new OracleParameter("1", OracleDbType.Varchar2),
                                                          new OracleParameter("2", OracleDbType.Varchar2)
                                                      };
                        parameters[0].Value = tagCalcItem.TAGNAME;
                        parameters[1].Value = tagCalcItem.CTAGNAME;

                        mapper.ExecuteScript(Database.Queries.TAGCALC_DELETE, parameters);
                    }
                }                
            }
            catch (Exception ex)
            {
                FireException("Save", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        #region private void btnClose_Click(object sender, EventArgs e)
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        } 
        #endregion


        





    }
}
