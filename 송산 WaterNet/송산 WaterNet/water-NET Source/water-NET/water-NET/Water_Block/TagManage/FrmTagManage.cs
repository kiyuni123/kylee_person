﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.BlockApp.TagManage
{
    public partial class FrmTagManage : Form
    {
        private List<BlockItem> _blockList = null;
        // 블록에 매핑된 태그 목록 정보 (블록 그리드에서만 사용)
        private List<TagItem> _tagList = null;

        /// <summary>
        /// 조회된 태그 목록 
        /// (화면이 닫히기 전까지는 조회시 데이터를 유지하면서 갱신되므로 계속적으로 누적된다.
        ///  단, 전체조회시 갱신된다.)
        /// </summary>
        private List<TagItem> _searchTagList = new List<TagItem>();


        /// <summary>
        /// AUTO TAG 목록
        /// GRID에서 AUTO_TAG 컬럼에서 사용
        /// </summary>
        private ValueList autoTagValueList = null;

        private List<TagCalcItem> _tagCalListAll = null;


        /// <summary>
        /// Constructor
        /// </summary>
        #region public FrmTagManage()
        public FrmTagManage()
        {
            InitializeComponent();
        } 
        #endregion

        #region Helper Methods
        /// <summary>
        /// 오류처리
        /// </summary>
        /// <param name="method">오류 메서드명</param>
        /// <param name="ex">오류내용</param>
        #region private void FireException(string method, Exception ex)
        private void FireException(string method, Exception ex)
        {
            Console.WriteLine(string.Format("[Exception] {0} -> {1}", method, ex.Message));
        }
        #endregion

        /// <summary>
        /// where절에서의 Like 조건절 함수
        /// </summary>
        /// <param name="src">Source text</param>
        /// <param name="find">find text</param>
        /// <returns>boolean</returns>
        #region private bool Like(string src, string find)
        private bool Like(string src, string find)
        {
            if (src.IndexOf(find) >= 0) return true;
            else return false;
        }
        #endregion

        /// <summary>
        /// UltraGrid 전체 Row 삭제
        /// </summary>
        /// <param name="grid">UltraGrid</param>
        #region private void RemoveRowsAll(UltraGrid grid)
        private void RemoveRowsAll(UltraGrid grid)
        {
            grid.Selected.Rows.AddRange((UltraGridRow[])grid.Rows.All);
            grid.DeleteSelectedRows(false);
        } 
        #endregion
        #endregion



        /// <summary>
        /// Form Load
        /// </summary>
        #region private void FrmTagManage_Load(object sender, EventArgs e)
        private void FrmTagManage_Load(object sender, EventArgs e)
        {
            try
            {
                // 지역선택
                //OpenZone();

                // '조회중' Label 위에 데이터 그리드가 위치하도록 조정
                dgvTag.BringToFront();

                // 데이터 목록 생성
                DataTable dtBlock = SelectBlockData();
                CreateBlocTagList(dtBlock);

                // 계산 태그 데이터 목록 생성
                DataTable dtAllTagCalc = SelectAllTagCalData();
                CreateTagCalList(dtAllTagCalc);

                // 그리드 목록 생성
                CreateBlockTagGrid();

                // 콤보박스 설정
                InitializeCombo();

                // 블록 그리드의 셀선택 이벤트 연결
                dgvBlockTag.AfterCellUpdate += new CellEventHandler(dgvBlockTag_AfterCellUpdate);

                // CALC TAG GRID 셀변경 이벤트
                dgvBlockTag.AfterCellUpdate += new CellEventHandler(dgvBlockTag_AfterCellUpdate);

                dgvCalcTag.CellChange += new CellEventHandler(dgvCalc_CellChange);
                dgvCalcTag.DragDrop += new DragEventHandler(dgvCalc_DragDrop);
                dgvCalcTag.DragEnter += new DragEventHandler(dgvCalc_DragEnter);

                // 태그 목록 미리 조회하기 (전체 목록)
                System.Threading.Thread thProc = new System.Threading.Thread(new System.Threading.ThreadStart(InitTagList));
                thProc.Start();
            }
            catch (Exception ex)
            {
                FireException("FrmTagManage_Load", ex);
            }
        }

        void dgvCalcRT_DragLeave(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        } 
        #endregion

        /// <summary>
        /// 지역 열기
        /// </summary>
        #region private void OpenZone()
        private void OpenZone()
        {
#if _DEBUG
            FrmOpen frmOpen = new FrmOpen();
            // 팝업에서 지역 선택시
            if (frmOpen.ShowDialog(this) == DialogResult.OK)
            {
                EMFrame.statics.AppStatic.DATA_DIR = frmOpen.PATH;
                EMFrame.statics.AppStatic.USER_ID = "에너지";

                EMFrame.statics.AppStatic.ZONE_GBN = frmOpen.ZONE;
                EMFrame.statics.AppStatic.USER_SGCCD = frmOpen.SGCCD;

                string datadir = EMFrame.statics.AppStatic.DATA_DIR + System.IO.Path.DirectorySeparatorChar + EMFrame.statics.AppStatic.USER_SGCCD;
                WaterNet.WaterAOCore.VariableManager.m_Topographic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Topographic");
                WaterNet.WaterAOCore.VariableManager.m_Pipegraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Pipegraphic");
                WaterNet.WaterAOCore.VariableManager.m_INPgraphic = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic");
                WaterNet.WaterAOCore.VariableManager.m_INPgraphicRootDirectory = datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic";

                // Shape 정보 초기화
                InitializeShape();
                // 목록 생성 (ListView)
                InitializeList();
            }
#else
            FrmSelectSgccd frmSgccd = new FrmSelectSgccd();
            if (frmSgccd.ShowDialog(this) == DialogResult.OK)
            {
                EMFrame.statics.AppStatic.DATA_DIR = frmSgccd.PATH;
                EMFrame.statics.AppStatic.USER_ID = "에너지";

                EMFrame.statics.AppStatic.ZONE_GBN = frmSgccd.ZONE;
                EMFrame.statics.AppStatic.USER_SGCCD = frmSgccd.SGCCD;

                string datadir = EMFrame.statics.AppStatic.DATA_DIR + System.IO.Path.DirectorySeparatorChar + EMFrame.statics.AppStatic.USER_SGCCD;
                WaterAOCore.VariableManager.m_Topographic = WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Topographic");
                WaterAOCore.VariableManager.m_Pipegraphic = WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "Pipegraphic");
                WaterAOCore.VariableManager.m_INPgraphic = WaterAOCore.ArcManager.getShapeWorkspace(datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic");
                WaterAOCore.VariableManager.m_INPgraphicRootDirectory = datadir + System.IO.Path.DirectorySeparatorChar + "INPgraphic";
            }
#endif
        }
        #endregion


        #region 내부 데이터 생성
        /// <summary>
        /// 콤보박스 검색 초기화
        /// </summary>
        private void InitializeCombo()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                //mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);

                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.BLOCK_SELECT, EMFrame.statics.AppStatic.USER_SGCCD), null);
                cbBlock.ValueMember = "CODE";
                cbBlock.DisplayMember = "CODE_NAME";
                cbBlock.DataSource = dt;
            }
            catch (Exception ex)
            {
                FireException("SelectBlockData", ex);
            }            
        }

        /// <summary>
        /// [데이터1] 블록 그리드를 구성하기 위한 전체 태그 계산식 조회 
        /// </summary>
        /// <returns>DataTable</returns>
        #region private DataTable SelectAllTagCalData()
        private DataTable SelectAllTagCalData()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                //mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
                dt = mapper.ExecuteScriptDataTable(Database.Queries.TAGCALC_TAG_CALC_ALL_SELECT, null);
            }
            catch (Exception ex)
            {
                FireException("SelectAllTagCalData", ex);
            }
            return dt;
        }
        #endregion

        /// <summary>
        /// 전체 태그 계산 정보 목록 생성
        /// </summary>
        /// <param name="dt">DataTable</param>
        #region private void CreateTagCalList(DataTable dt)
        private void CreateTagCalList(DataTable dt)
        {
            try
            {
                if (_tagCalListAll == null) _tagCalListAll = new List<TagCalcItem>();
                else _tagCalListAll.Clear();

                if (dt == null || dt.Rows.Count <= 0) return;

                #region DB to List
                foreach (DataRow dr in dt.Rows)
                {
                    string tagName = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                    string tagGbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                    string cTagName = string.Format("{0}", dr[Database.Fields.TAG_CAL.CTAGNAME]);

                    // 태그일련번호, 태그구분, 태그일련번호2가 동일하면 패스
                    if (_tagCalListAll.FindIndex(item => item.TAGNAME == tagName && item.TAG_GBN == tagGbn && item.CTAGNAME == cTagName) > 0) continue;

                    TagCalcItem tagCalItem = new TagCalcItem();
                    tagCalItem.TAGNAME = tagName;
                    tagCalItem.TAG_GBN = tagGbn;
                    tagCalItem.DUMMY_GBN = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                    tagCalItem.CTAGNAME = cTagName;
                    tagCalItem.LOC_CODE = string.Format("{0}", dr[Database.Fields.TAG.LOC_CODE]);
                    tagCalItem.LOC_NAME = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_NAME]);
                    tagCalItem.DESCRIPTION = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                    tagCalItem.TAG_GBN = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                    tagCalItem.CAL_GBN = string.Format("{0}", dr[Database.Fields.TAG_CAL.CAL_GBN]);
                    tagCalItem.CAL_GBN_ORG = string.Format("{0}", dr[Database.Fields.TAG_CAL.CAL_GBN]);
                    tagCalItem.TAG_DESC = string.Format("{0}", dr[Database.Fields.TAG.TAG_DESCRIPTION]);
                    tagCalItem.COEFFICIENT = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);

                    _tagCalListAll.Add(tagCalItem);
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("CreateTagCalList", ex);
            }
        }
        #endregion

        /// <summary>
        /// [데이터0] 블록 정보 데이터 조회 (블록+태그+태그구분)
        /// </summary>
        /// <returns>DataTable</returns>
        #region private DataTable SelectBlockData()
        private DataTable SelectBlockData()
        {
            DataTable dt = new DataTable();
            EMFrame.dm.EMapper mapper = null;

            try
            {
                //mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);

                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.TAGMAPPING_BlOCK_TAG_SELECT, EMFrame.statics.AppStatic.USER_SGCCD), null);
            }
            catch (Exception ex)
            {
                FireException("SelectBlockData", ex);
            }
            return dt;
        }
        #endregion

        /// <summary>
        /// [데이터1] 블록, 태그 리스트 생성 (DB 조회 결과 사용)
        /// </summary>
        /// <param name="dt"></param>
        #region private void CreateBlocTagList(DataTable dt)
        private void CreateBlocTagList(DataTable dt)
        {
            try
            {
                if (dt == null || dt.Rows.Count <= 0) return;

                if (_blockList == null) _blockList = new List<BlockItem>();
                else _blockList.Clear();

                if (_tagList == null) _tagList = new List<TagItem>();
                else _tagList.Clear();
                

                #region DB to List
                string bfLocCode = "";
                foreach (DataRow dr in dt.Rows)
                {
                    string locCode = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_CODE]);
                    
                    if (_blockList.FindIndex(item => item.LOC_CODE == locCode) < 0)
                    {
                        #region BlockItem List 생성
                        BlockItem blockItem = new BlockItem();
                        blockItem.LEVEL = FrmShapeBlockManager.toInt32(string.Format("{0}", dr[Database.Fields.BLOCK.LEVEL]));// BlockItem.FIELDS.LEVEL]));
                        blockItem.FTR_CODE = string.Format("{0}", dr[Database.Fields.BLOCK.FTR_CODE]);// BlockItem.FIELDS.FTR_CODE]);
                        blockItem.FTR_IDN = string.Format("{0}", dr[Database.Fields.BLOCK.FTR_IDN]);// BlockItem.FIELDS.FTR_IDN]);
                        blockItem.PFTR_CODE = string.Format("{0}", dr[Database.Fields.BLOCK.PFTR_CODE]);// BlockItem.FIELDS.PFTR_CODE]);
                        blockItem.PFTR_IDN = string.Format("{0}", dr[Database.Fields.BLOCK.PFTR_IDN]);// BlockItem.FIELDS.PFTR_IDN]);
                        blockItem.LOC_CODE = locCode;
                        blockItem.PLOC_CODE = string.Format("{0}", dr[Database.Fields.BLOCK.PLOC_CODE]);// BlockItem.FIELDS.PLOC_CODE]);
                        blockItem.LOC_NAME = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_NAME]);// BlockItem.FIELDS.LOC_NAME]);
                        blockItem.REL_LOC_NAME = string.Format("{0}", dr[Database.Fields.BLOCK.REL_LOC_NAME]);// BlockItem.FIELDS.REL_LOC_NAME]);
                        blockItem.RES_CODE = string.Format("{0}", dr[Database.Fields.BLOCK.RES_CODE]);// BlockItem.FIELDS.RES_CODE]);
                        blockItem.KT_GBN = string.Format("{0}", dr[Database.Fields.BLOCK.KT_GBN]);// BlockItem.FIELDS.KT_GBN]);
                        blockItem.ORDERBY = FrmShapeBlockManager.toInt32(string.Format("{0}", dr[Database.Fields.BLOCK.ORDERBY]));// BlockItem.FIELDS.ORDERBY]));
                        blockItem.SGCCD = string.Format("{0}", dr[Database.Fields.BLOCK.SGCCD]);// BlockItem.FIELDS.SGCCD]);
                        blockItem.LOC_GBN = string.Format("{0}", dr[Database.Fields.BLOCK.LOC_GBN]);//BlockItem.FIELDS.LOC_GBN]);
                        blockItem.CM_LOC_GBN = string.Format("{0}", dr[Database.Fields.BLOCK.CM_LOC_GBN]);//BlockItem.FIELDS.CM_LOC_GBN]);
                        _blockList.Add(blockItem);
                        #endregion
                    }

                    #region TagItem List 생성
                    string tagName = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);// TagItem.FIELDS.TAGNAME]);
                    string desc = string.Format("{0}", dr[Database.Fields.TAG.DESC]);// TagItem.FIELDS.DESC]);
                    string useYn = string.Format("{0}", dr[Database.Fields.TAG.USE_YN]);//TagItem.FIELDS.USE_YN]);
                    string ftrIdn = string.Format("{0}", dr[Database.Fields.TAG.FTR_IDN]);//TagItem.FIELDS.FTR_IDN]);
                    string coefficient = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);//TagItem.FIELDS.COEFFICIENT]);
                    string tagLocCode = string.Format("{0}", dr[Database.Fields.TAG.TAG_LOC_CODE]); //"TAG_LOC_CODE"]);
                    string tagLocCode2 = string.Format("{0}", dr[Database.Fields.TAG.TAG_LOC_CODE2]);//"TAG_LOC_CODE2"]);                    
                    string tagGbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);// TagItem.FIELDS.TAG_GBN]);
                    string dummyGbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                    string autoTagGbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.AUTO_TAG_GBN]);

                    TagItem tagItem = new TagItem(tagName, desc, useYn, ftrIdn, coefficient, tagLocCode, tagLocCode2, tagGbn, "", autoTagGbn);
                    if (dummyGbn.ToUpper() == "CAL")
                    {
                        tagItem.DUMMY_GBN = true;
                        tagItem.DUMMY_GBN_ORG = true;
                    }

                    //Console.WriteLine(string.Format("TAGNAME:{0} / LOC_CODE:{1} / LOC_CODE2:{2}", tagName, tagLocCode, tagLocCode2));
                    _tagList.Add(tagItem);
                    #endregion

                    bfLocCode = locCode;
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("CreateBlocTagkList", ex);
            }
        }
        #endregion

        /// <summary>
        /// 최초 태그 목록 미리 가져오기
        /// </summary>
        #region private void InitTagList()
        private void InitTagList()
        {
            try
            {
                //EMFrame.dm.EMapper mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                EMFrame.dm.EMapper mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
                DataTable dt = new DataTable();

                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.TAGMAPPING_TAG_SEARCH, "", ""), null);

                #region 태그 리스트 생성 (DB to List)
                if (_searchTagList == null) _searchTagList = new List<TagItem>();
                foreach (DataRow dr in dt.Rows)
                {
                    string tagName = string.Format("{0}", dr[Database.Fields.TAG.TAGNAME]);
                    string desc = string.Format("{0}", dr[Database.Fields.TAG.DESC]);
                    string useYn = string.Format("{0}", dr[Database.Fields.TAG.USE_YN]);
                    string ftrIdn = string.Format("{0}", dr[Database.Fields.TAG.FTR_IDN]);
                    string coefficient = string.Format("{0}", dr[Database.Fields.TAG.COEFFICIENT]);
                    string locCode = string.Format("{0}", dr[Database.Fields.TAG.LOC_CODE]);
                    string locCode2 = string.Format("{0}", dr[Database.Fields.TAG.LOC_CODE2]);
                    string tagGbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.TAG_GBN]);
                    string dummyGbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.DUMMY_GBN]);
                    string htagname = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.HTAGANME]);
                    string autotaggbn = string.Format("{0}", dr[Database.Fields.TAG_GUBUN.AUTO_TAG_GBN]);

                    TagItem tagItem = new TagItem(tagName, desc, useYn, ftrIdn, coefficient, locCode, locCode2, tagGbn, htagname, autotaggbn);
                    if (dummyGbn.ToUpper() == "CAL")
                    {
                        tagItem.DUMMY_GBN = true;
                        tagItem.DUMMY_GBN_ORG = true;
                    }


                    int findIdx = _searchTagList.FindIndex(item => item.TAGNAME == tagName);
                    if (findIdx < 0)
                    {
                        _searchTagList.Add(tagItem);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("InitTagList", ex);
            }
        } 
        #endregion

        /// <summary>
        /// AUTO TAG DATA 생성
        /// </summary>
        #region private void GetAutoTagValueList()
        private ValueList GetAutoTagValueList()
        {
             // 이미 생성된 리스트인지 체크
            if (null != autoTagValueList)
                return autoTagValueList;

            this.autoTagValueList = new ValueList();

            this.AddAutoTag(autoTagValueList, "",                   new string[] {  });
            this.AddAutoTag(autoTagValueList, "순시유량",               new string[] { "MNF", "FRI", "RT"});
            this.AddAutoTag(autoTagValueList, "적산유량",               new string[] { "TD", "TT"});
            this.AddAutoTag(autoTagValueList, "적산차유량(유입)",       new string[] { "YD", "FRQ", "FRQ_I", "SPL_I"});
            this.AddAutoTag(autoTagValueList, "적산차유량(유입, 분기)", new string[] { "YD", "FRQ", "FRQ_I", "SPL_D" });
            this.AddAutoTag(autoTagValueList, "적산차유량(유출)",       new string[] { "YD", "FRQ", "FRQ_O", "SPL_O" });
            this.AddAutoTag(autoTagValueList, "압력순시",               new string[] { "PRI" });
            this.AddAutoTag(autoTagValueList, "배수지수위",             new string[] { "LEI" });
            this.AddAutoTag(autoTagValueList, "잔류염소",               new string[] { "CLI" });
            this.AddAutoTag(autoTagValueList, "탁도",                   new string[] { "TBI" });
            this.AddAutoTag(autoTagValueList, "PH값",                   new string[] { "PHI" });
            this.AddAutoTag(autoTagValueList, "온도",                   new string[] { "TEI" });

            return autoTagValueList;
        }

        private void AddAutoTag(ValueList autoTagValueList, string autoTag, string[] tags)
        {
            ValueListItem autoTagValueListItem = autoTagValueList.ValueListItems.Add(autoTag, autoTag);

            ValueList tagValueList = new ValueList();
            autoTagValueListItem.Tag = tagValueList;

            for (int i = 0; i < tags.Length; i++)
            {
                tagValueList.ValueListItems.Add(tags[i], tags[i]);
            }
        }

        #endregion


        #endregion

        #region 데이터 표현 (Grid 설정)
        /// <summary>
        /// 블록 태그 그리드 생성
        /// </summary>
        #region private void CreateBlockTagGrid()
        private void CreateBlockTagGrid()
        {
            try
            {
                if (_blockList == null || _blockList.Count <= 0) return;

                // 데이터 그리드 화면 갱신 중지
                dgvBlockTag.BeginUpdate();
                // 블록 그리드 초기화
                RemoveRowsAll(dgvBlockTag);

                #region 대블록
                var blockObj0 = from BlockItem item in _blockList
                                where item.LEVEL == 1
                                orderby item.ORDERBY ascending
                                select item;
                if (blockObj0 != null && blockObj0.Count() > 0)
                {
                    for (int i = 0; i < blockObj0.Count(); i++)
                    {
                        BlockItem item0 = blockObj0.ElementAt(i);
                        // 대블록 Row 생성
                        UltraGridRow blockRow0 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME].Value = item0.LOC_NAME;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item0.LOC_NAME;
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item0.LOC_CODE;
                        blockRow0.Cells[Database.Fields.BLOCK.TYPE].Value = "B";
                        blockRow0.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item0.CM_LOC_GBN;
                        blockRow0.Cells[Database.Fields.BLOCK.LEVEL].Value = item0.LEVEL.ToString();
                        blockRow0.Cells[Database.Fields.BLOCK.LOC_NAME_1].Value = item0.LOC_NAME;

                        #region Cell Click Action
                        // Cell 클릭시 Row 선택으로 이벤트 설정
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.RT].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.FRI].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.TT].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.YD].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.TD].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.MNF].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_D].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_I].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.SPL_O].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.LEI].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.PHI].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.CLI].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.TBI].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.PRI].Column.CellClickAction = CellClickAction.RowSelect;
                        //blockRow0.Cells[Database.Fields.TAG_GUBUN.PMB].Column.CellClickAction = CellClickAction.RowSelect;
                        #endregion

                        #region TAG
                        // 대블록의 태그 Row 생성
                        var tagObj0 = from TagItem item in _tagList
                                      where item.LOC_CODE2 == item0.LOC_CODE
                                      orderby item.TAGNAME
                                      select item;
                        if (tagObj0 != null && tagObj0.Count() > 0)
                        {
                            for (int ti = 0; ti < tagObj0.Count(); ti++)
                            {
                                TagItem tagItem = tagObj0.ElementAt(ti);
                                UltraGridRow tagRow0 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                tagRow0.Cells[Database.Fields.TAG.TAGNAME].Value = tagItem.TAGNAME;
                                tagRow0.Cells[Database.Fields.TAG.DESC].Value = tagItem.DESCRIPTION;
                                tagRow0.Cells[Database.Fields.TAG.LOC_CODE].Value = item0.LOC_CODE;
                                tagRow0.Cells[Database.Fields.BLOCK.LEVEL].Value = item0.LEVEL.ToString();
                                tagRow0.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item0.LOC_NAME;
                                tagRow0.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Value = tagItem.DUMMY_GBN;
                                tagRow0.Cells[Database.Fields.TAG_GUBUN.AUTO_TAG_GBN].Value = tagItem.AUTO_TAG_GBN;

                                tagRow0.Cells[Database.Fields.BLOCK.TYPE].Value = "T";
                                tagRow0.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item0.CM_LOC_GBN;

                                if (tagItem.DUMMY_GBN)
                                {
                                    // 태그가 계산 태그일 경우의 색상 설정
                                    tagRow0.Cells[Database.Fields.TAG.DESC].Appearance.BackColor = Color.SkyBlue;
                                    tagRow0.Cells[Database.Fields.TAG.TAGNAME].Appearance.BackColor = Color.SkyBlue;
                                    tagRow0.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Appearance.BackColor = Color.SkyBlue;
                                }

                                // 태그구분 체크박스 설정
                                SetGridTagGubun(tagRow0, tagItem);

                                // 자동TAG 버튼 설정
                           }
                        }
                        #endregion

                        #region 중블록
                        var blockObj1 = from BlockItem item in _blockList
                                        where item.LEVEL == 2 && item.PLOC_CODE == item0.LOC_CODE
                                        orderby item.ORDERBY ascending
                                        select item;
                        if (blockObj1 == null || blockObj1.Count() <= 0) continue;
                        for (int j = 0; j < blockObj1.Count(); j++)
                        {
                            BlockItem item1 = blockObj1.ElementAt(j);
                            UltraGridRow blockRow1 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                            blockRow1.Cells[Database.Fields.BLOCK.LOC_NAME].Value = item1.LOC_NAME;
                            blockRow1.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item1.LOC_NAME;
                            blockRow1.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item1.LOC_CODE;
                            blockRow1.Cells[Database.Fields.BLOCK.TYPE].Value = "B";
                            blockRow1.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item1.CM_LOC_GBN;
                            blockRow1.Cells[Database.Fields.BLOCK.LEVEL].Value = item1.LEVEL.ToString();
                            blockRow1.Cells[Database.Fields.BLOCK.LOC_NAME_2].Value = item1.LOC_NAME;

                            #region Cell Click Action
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.RT].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.FRI].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.TT].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.YD].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.TD].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.MNF].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.FRQ].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_D].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_I].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.SPL_O].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.LEI].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.PHI].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.CLI].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.TBI].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.PRI].Column.CellClickAction = CellClickAction.RowSelect;
                            //blockRow1.Cells[Database.Fields.TAG_GUBUN.PMB].Column.CellClickAction = CellClickAction.RowSelect;
                            #endregion

                            #region TAG
                            var tagObj1 = from TagItem item in _tagList
                                          where item.LOC_CODE2 == item1.LOC_CODE
                                          orderby item.TAGNAME
                                          select item;
                            if (tagObj1 != null && tagObj1.Count() > 0)
                            {
                                for (int tj = 0; tj < tagObj1.Count(); tj++)
                                {
                                    TagItem tagItem = tagObj1.ElementAt(tj);
                                    UltraGridRow tagRow1 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                    tagRow1.Cells[Database.Fields.TAG.TAGNAME].Value = tagItem.TAGNAME;
                                    tagRow1.Cells[Database.Fields.TAG.DESC].Value = tagItem.DESCRIPTION;
                                    tagRow1.Cells[Database.Fields.TAG.LOC_CODE].Value = item1.LOC_CODE;
                                    tagRow1.Cells[Database.Fields.BLOCK.LEVEL].Value = item1.LEVEL.ToString();
                                    tagRow1.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item1.LOC_NAME;
                                    tagRow1.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Value = tagItem.DUMMY_GBN;
                                    tagRow1.Cells[Database.Fields.TAG_GUBUN.AUTO_TAG_GBN].Value = tagItem.AUTO_TAG_GBN;

                                    tagRow1.Cells[Database.Fields.BLOCK.TYPE].Value = "T";
                                    tagRow1.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item1.CM_LOC_GBN;

                                    if (tagItem.DUMMY_GBN)
                                    {
                                        tagRow1.Cells[Database.Fields.TAG.DESC].Appearance.BackColor = Color.SkyBlue;
                                        tagRow1.Cells[Database.Fields.TAG.TAGNAME].Appearance.BackColor = Color.SkyBlue;
                                        tagRow1.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Appearance.BackColor = Color.SkyBlue;
                                    }

                                    // 태그구분 체크박스 설정
                                    SetGridTagGubun(tagRow1, tagItem);
                                }
                            }
                            #endregion

                            #region 소블록
                            var blockObj2 = from BlockItem item in _blockList
                                            where item.LEVEL == 3 && item.PLOC_CODE == item1.LOC_CODE
                                            orderby item.ORDERBY ascending
                                            select item;
                            if (blockObj2 == null || blockObj2.Count() <= 0) continue;
                            for (int k = 0; k < blockObj2.Count(); k++)
                            {
                                BlockItem item2 = blockObj2.ElementAt(k);
                                UltraGridRow blockRow2 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                blockRow2.Cells[Database.Fields.BLOCK.LOC_NAME].Value = item2.LOC_NAME;
                                blockRow2.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item2.LOC_NAME;
                                blockRow2.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item2.LOC_CODE;
                                blockRow2.Cells[Database.Fields.BLOCK.TYPE].Value = "B";
                                blockRow2.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item2.CM_LOC_GBN;
                                blockRow2.Cells[Database.Fields.BLOCK.LEVEL].Value = item2.LEVEL.ToString();
                                blockRow2.Cells[Database.Fields.BLOCK.LOC_NAME_3].Value = item2.LOC_NAME;

                                #region Cell Click Action
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.RT].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.FRI].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.TT].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.YD].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.TD].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.MNF].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.FRQ].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_D].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_I].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.SPL_O].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.LEI].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.PHI].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.CLI].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.TBI].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.PRI].Column.CellClickAction = CellClickAction.RowSelect;
                                //blockRow2.Cells[Database.Fields.TAG_GUBUN.PMB].Column.CellClickAction = CellClickAction.RowSelect;
                                #endregion

                                #region TAG
                                var tagObj2 = from TagItem item in _tagList
                                              where item.LOC_CODE2 == item2.LOC_CODE
                                              orderby item.TAGNAME
                                              select item;
                                if (tagObj2 != null && tagObj2.Count() > 0)
                                {
                                    for (int tk = 0; tk < tagObj2.Count(); tk++)
                                    {
                                        TagItem tagItem = tagObj2.ElementAt(tk);
                                        UltraGridRow tagRow2 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                        tagRow2.Cells[Database.Fields.TAG.TAGNAME].Value = tagItem.TAGNAME;
                                        tagRow2.Cells[Database.Fields.TAG.DESC].Value = tagItem.DESCRIPTION;
                                        tagRow2.Cells[Database.Fields.TAG.LOC_CODE].Value = item2.LOC_CODE;
                                        tagRow2.Cells[Database.Fields.BLOCK.LEVEL].Value = item2.LEVEL.ToString();
                                        tagRow2.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item2.LOC_NAME;
                                        tagRow2.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Value = tagItem.DUMMY_GBN;
                                        tagRow2.Cells[Database.Fields.TAG_GUBUN.AUTO_TAG_GBN].Value = tagItem.AUTO_TAG_GBN;

                                        tagRow2.Cells[Database.Fields.BLOCK.TYPE].Value = "T";
                                        tagRow2.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item2.CM_LOC_GBN;

                                        if (tagItem.DUMMY_GBN)
                                        {
                                            tagRow2.Cells[Database.Fields.TAG.DESC].Appearance.BackColor = Color.SkyBlue;
                                            tagRow2.Cells[Database.Fields.TAG.TAGNAME].Appearance.BackColor = Color.SkyBlue;
                                            tagRow2.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Appearance.BackColor = Color.SkyBlue;
                                        }

                                        // 태그구분 체크박스 설정
                                        SetGridTagGubun(tagRow2, tagItem);
                                    }
                                }
                                #endregion

                                #region 소소블록 (소블록과 동일하게 취급)
                                var blockObj3 = from BlockItem item in _blockList
                                                where item.LEVEL == 4 && item.PLOC_CODE == item2.LOC_CODE
                                                orderby item.ORDERBY ascending
                                                select item;
                                if (blockObj3 == null || blockObj3.Count() <= 0) continue;
                                for (int m = 0; m < blockObj3.Count(); m++)
                                {
                                    BlockItem item3 = blockObj3.ElementAt(m);
                                    UltraGridRow blockRow3 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                    blockRow3.Cells[Database.Fields.BLOCK.LOC_NAME].Value = item3.LOC_NAME;
                                    blockRow3.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item3.LOC_NAME;
                                    blockRow3.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item3.LOC_CODE;
                                    blockRow3.Cells[Database.Fields.BLOCK.TYPE].Value = "B";
                                    blockRow3.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item3.CM_LOC_GBN;
                                    blockRow3.Cells[Database.Fields.BLOCK.LEVEL].Value = item3.LEVEL.ToString();
                                    blockRow3.Cells[Database.Fields.BLOCK.LOC_NAME_3].Value = item3.LOC_NAME;

                                    #region Cell Click Action
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.RT].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.FRI].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.TT].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.YD].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow0.Cells[Database.Fields.TAG_GUBUN.YD_R].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.TD].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.MNF].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.FRQ].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.SPL_D].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.SPL_I].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.SPL_O].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.LEI].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.PHI].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.CLI].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.TBI].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.PRI].Column.CellClickAction = CellClickAction.RowSelect;
                                    //blockRow3.Cells[Database.Fields.TAG_GUBUN.PMB].Column.CellClickAction = CellClickAction.RowSelect;
                                    #endregion

                                    #region TAG
                                    var tagObj3 = from TagItem item in _tagList
                                                  where item.LOC_CODE2 == item3.LOC_CODE
                                                  orderby item.TAGNAME
                                                  select item;
                                    if (tagObj3 != null && tagObj3.Count() > 0)
                                    {
                                        for (int tk = 0; tk < tagObj3.Count(); tk++)
                                        {
                                            TagItem tagItem = tagObj3.ElementAt(tk);
                                            UltraGridRow tagRow3 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                                            tagRow3.Cells[Database.Fields.TAG.TAGNAME].Value = tagItem.TAGNAME;
                                            tagRow3.Cells[Database.Fields.TAG.DESC].Value = tagItem.DESCRIPTION;
                                            tagRow3.Cells[Database.Fields.TAG.LOC_CODE].Value = item3.LOC_CODE;
                                            tagRow3.Cells[Database.Fields.BLOCK.LEVEL].Value = item3.LEVEL.ToString();
                                            tagRow3.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item3.LOC_NAME;
                                            tagRow3.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Value = tagItem.DUMMY_GBN;

                                            tagRow3.Cells[Database.Fields.BLOCK.TYPE].Value = "T";
                                            tagRow3.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item3.CM_LOC_GBN;

                                            if (tagItem.DUMMY_GBN)
                                            {
                                                tagRow3.Cells[Database.Fields.TAG.DESC].Appearance.BackColor = Color.SkyBlue;
                                                tagRow3.Cells[Database.Fields.TAG.TAGNAME].Appearance.BackColor = Color.SkyBlue;
                                                tagRow3.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Appearance.BackColor = Color.SkyBlue;
                                            }

                                            // 태그구분 체크박스 설정
                                            SetGridTagGubun(tagRow3, tagItem);
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion

                #region 블록 이외 (제외됨)
                /*
                var blockObj9 = from BlockItem item in _blockList
                                where item.LEVEL == 9
                                orderby item.ORDERBY ascending
                                select item;
                if (blockObj9 == null || blockObj9.Count() <= 0) return;
                for (int i = 0; i < blockObj9.Count(); i++)
                {
                    BlockItem item9 = blockObj9.ElementAt(i);
                    UltraGridRow blockRow9 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                    blockRow9.Cells[Database.Fields.BLOCK.LOC_NAME].Value = item9.LOC_NAME;
                    blockRow9.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item9.LOC_NAME;
                    blockRow9.Cells[Database.Fields.BLOCK.LOC_CODE].Value = item9.LOC_CODE;
                    blockRow9.Cells[Database.Fields.BLOCK.TYPE].Value = "B";
                    blockRow9.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item9.CM_LOC_GBN;
                    blockRow9.Cells[Database.Fields.BLOCK.LEVEL].Value = item9.LEVEL.ToString();
                    blockRow9.Cells[Database.Fields.BLOCK.LOC_NAME_1].Value = item9.LOC_NAME;

                    #region TAG
                    var tagObj9 = from TagItem item in _tagList
                                  where item.LOC_CODE2 == item9.LOC_CODE
                                  orderby item.TAGNAME
                                  select item;
                    if (tagObj9 != null && tagObj9.Count() > 0)
                    {
                        for (int ti = 0; ti < tagObj9.Count(); ti++)
                        {
                            TagItem tagItem = tagObj9.ElementAt(ti);
                            UltraGridRow tagRow9 = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                            tagRow9.Cells[Database.Fields.TAG.TAGNAME].Value = tagItem.TAGNAME;
                            tagRow9.Cells[Database.Fields.TAG.DESC].Value = tagItem.DESCRIPTION;
                            tagRow9.Cells[Database.Fields.TAG.LOC_CODE].Value = item9.LOC_CODE;
                            tagRow9.Cells[Database.Fields.BLOCK.LEVEL].Value = item9.LEVEL.ToString();
                            tagRow9.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = item9.LOC_NAME;
                            tagRow9.Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Value = tagItem.DUMMY_GBN;

                            tagRow9.Cells[Database.Fields.BLOCK.TYPE].Value = "T";
                            tagRow9.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = item9.CM_LOC_GBN;

                            // 태그구분 체크박스 설정
                            SetGridTagGubun(tagRow9, tagItem);
                        }
                    }
                    #endregion
                }*/
                #endregion

                //dgvBlockTag_InitializeLayout();

                if (dgvBlockTag.Rows.Count > 0)
                    dgvBlockTag.ActiveRowScrollRegion.ScrollRowIntoView(dgvBlockTag.Rows[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                FireException("CreateBlockTagGrid", ex);
            }
            finally
            {
                dgvBlockTag.EndUpdate();
            }
        } 
        #endregion

        /// <summary>
        /// 블록 태그 그리드의 태그구분 체크박스 설정
        /// </summary>
        /// <param name="row">UltraGridRow</param>
        /// <param name="tagItem">TagItem</param>
        #region private void SetGridTagGubun(UltraGridRow row, TagItem tagItem)
        private void SetGridTagGubun(UltraGridRow row, TagItem tagItem)
        {
            try
            {
                row.Cells[Database.Fields.TAG_GUBUN.RT].Value = tagItem.RT;
                row.Cells[Database.Fields.TAG_GUBUN.FRI].Value = tagItem.FRI;
                row.Cells[Database.Fields.TAG_GUBUN.TT].Value = tagItem.TT;
                row.Cells[Database.Fields.TAG_GUBUN.YD].Value = tagItem.YD;
                row.Cells[Database.Fields.TAG_GUBUN.YD_R].Value = tagItem.YD_R;
                row.Cells[Database.Fields.TAG_GUBUN.TD].Value = tagItem.TD;
                row.Cells[Database.Fields.TAG_GUBUN.MNF].Value = tagItem.MNF;
                row.Cells[Database.Fields.TAG_GUBUN.FRQ].Value = tagItem.FRQ;
                row.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Value = tagItem.FRQ_I;
                row.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Value = tagItem.FRQ_O;
                row.Cells[Database.Fields.TAG_GUBUN.SPL_D].Value = tagItem.SPL_D;
                row.Cells[Database.Fields.TAG_GUBUN.SPL_I].Value = tagItem.SPL_I;
                row.Cells[Database.Fields.TAG_GUBUN.SPL_O].Value = tagItem.SPL_O;
                row.Cells[Database.Fields.TAG_GUBUN.LEI].Value = tagItem.LEI;
                row.Cells[Database.Fields.TAG_GUBUN.PHI].Value = tagItem.PHI;
                row.Cells[Database.Fields.TAG_GUBUN.CLI].Value = tagItem.CLI;
                row.Cells[Database.Fields.TAG_GUBUN.TBI].Value = tagItem.TBI;
                row.Cells[Database.Fields.TAG_GUBUN.PRI].Value = tagItem.PRI;
                row.Cells[Database.Fields.TAG_GUBUN.PMB].Value = tagItem.PMB;

                // 색상 변경
                if (tagItem.RT) row.Cells[Database.Fields.TAG_GUBUN.RT].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.RT].Appearance.BackColor = Color.Transparent;
                if (tagItem.FRI) row.Cells[Database.Fields.TAG_GUBUN.FRI].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.FRI].Appearance.BackColor = Color.Transparent;
                if (tagItem.TT) row.Cells[Database.Fields.TAG_GUBUN.TT].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.TT].Appearance.BackColor = Color.Transparent;
                if (tagItem.YD) row.Cells[Database.Fields.TAG_GUBUN.YD].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.YD].Appearance.BackColor = Color.Transparent;
                if (tagItem.YD_R) row.Cells[Database.Fields.TAG_GUBUN.YD_R].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.YD_R].Appearance.BackColor = Color.Transparent;
                if (tagItem.TD) row.Cells[Database.Fields.TAG_GUBUN.TD].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.TD].Appearance.BackColor = Color.Transparent;
                if (tagItem.MNF) row.Cells[Database.Fields.TAG_GUBUN.MNF].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.MNF].Appearance.BackColor = Color.Transparent;
                if (tagItem.FRQ) row.Cells[Database.Fields.TAG_GUBUN.FRQ].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.FRQ].Appearance.BackColor = Color.Transparent;
                if (tagItem.FRQ_I) row.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.FRQ_I].Appearance.BackColor = Color.Transparent;
                if (tagItem.FRQ_O) row.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.FRQ_O].Appearance.BackColor = Color.Transparent;
                if (tagItem.SPL_D) row.Cells[Database.Fields.TAG_GUBUN.SPL_D].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.SPL_D].Appearance.BackColor = Color.Transparent;
                if (tagItem.SPL_I) row.Cells[Database.Fields.TAG_GUBUN.SPL_I].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.SPL_I].Appearance.BackColor = Color.Transparent;
                if (tagItem.SPL_O) row.Cells[Database.Fields.TAG_GUBUN.SPL_O].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.SPL_O].Appearance.BackColor = Color.Transparent;
                if (tagItem.LEI) row.Cells[Database.Fields.TAG_GUBUN.LEI].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.LEI].Appearance.BackColor = Color.Transparent;
                if (tagItem.PHI) row.Cells[Database.Fields.TAG_GUBUN.PHI].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.PHI].Appearance.BackColor = Color.Transparent;
                if (tagItem.CLI) row.Cells[Database.Fields.TAG_GUBUN.CLI].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.CLI].Appearance.BackColor = Color.Transparent;
                if (tagItem.TBI) row.Cells[Database.Fields.TAG_GUBUN.TBI].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.TBI].Appearance.BackColor = Color.Transparent;
                if (tagItem.PRI) row.Cells[Database.Fields.TAG_GUBUN.PRI].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.PRI].Appearance.BackColor = Color.Transparent;
                if (tagItem.PMB) row.Cells[Database.Fields.TAG_GUBUN.PMB].Appearance.BackColor = Color.Red;
                else row.Cells[Database.Fields.TAG_GUBUN.PMB].Appearance.BackColor = Color.Transparent;
            }
            catch (Exception ex)
            {
                FireException("SetGridTagGubun", ex);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// 레벨에 따른 Tab 삽입
        /// </summary>
        /// <param name="level">블록 레벨</param>
        /// <returns>LOC_NAME</returns>
        #region private string toLevelTab(int level)
        private string toLevelTab(int level)
        {
            string retVal = " ";

            for (int i = 0; i < level + level; i++) retVal += " ";

            return retVal;
        }
        #endregion

        #endregion





        #region 태그 조회
        /// <summary>
        /// 태그 조회 (태그일련번호)
        /// </summary>
        #region private void txtSearchTagname_KeyUp(object sender, KeyEventArgs e)
        private void txtSearchTagname_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) txtSearchTagDesc.Focus();
        }
        #endregion

        /// <summary>
        /// 태그 조회 (태그업무설명)
        /// </summary>
        #region private void txtSearchTagDesc_KeyDown(object sender, KeyEventArgs e)
        private void txtSearchTagDesc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btnSearchTag_Click(null, null);
        }
        #endregion

        /// <summary>
        /// 태그 조회 버튼
        /// </summary>
        #region private void btnSearchTag_Click(object sender, EventArgs e)
        private void btnSearchTag_Click(object sender, EventArgs e)
        {
            lblTagSearch.BringToFront();
            Cursor = Cursors.WaitCursor;

            try
            {
                //Console.WriteLine(string.Format("[{0}] 태그조회 Start", DateTime.Now.ToString("HH:mm:ss:fff")));

                RemoveRowsAll(dgvTag);

                int popWidth = (dgvTag.Width / 2) - (lblTagSearch.Width / 2);
                int popHeight = (dgvTag.Height / 2) - (lblTagSearch.Height / 2);
                lblTagSearch.Location = new Point(popWidth + dgvTag.Location.X, popHeight + dgvTag.Location.Y);
                lblTagSearch.Update();

                 
                if (cbBlock.SelectedValue.Equals("0"))
                {
                    var objects = from TagItem item in _searchTagList
                                  where Like(item.TAGNAME, txtSearchTagname.Text) && Like(item.DESCRIPTION, txtSearchTagDesc.Text)
                                  orderby item.TAGNAME
                                  select item;
                    if (objects == null || objects.Count() <= 0) return;

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("TAG_TAGNAME", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_DESCRIPTION", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_USE_YN", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_COEFFICIENT", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_LOC_CODE", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_LOC_CODE2", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_HTAGNAME", typeof(string)));

                    for (int i = objects.Count() - 1; i >= 0; i--)
                    {
                        TagItem tagItem = objects.ElementAt(i);
                        DataRow dr = dt.NewRow();
                        dr[Database.Fields.TAG.TAG_TAGNAME] = tagItem.TAGNAME;
                        dr[Database.Fields.TAG.TAG_DESCRIPTION] = tagItem.DESCRIPTION;
                        dr[Database.Fields.TAG.TAG_USE_YN] = tagItem.USE_YN;
                        dr[Database.Fields.TAG.TAG_COEFFICIENT] = tagItem.COEFFICIENT;
                        dr[Database.Fields.TAG.TAG_LOC_CODE] = tagItem.LOC_CODE;
                        dr[Database.Fields.TAG.TAG_LOC_CODE2] = tagItem.LOC_CODE2;
                        dr[Database.Fields.TAG.TAG_HTAGNAME] = tagItem.HTAGNAME;
                        dt.Rows.Add(dr);
                    }

                    dgvTag.DataSource = dt;

                    if (dgvTag.Rows.Count > 0)
                        dgvTag.ActiveRowScrollRegion.ScrollRowIntoView(dgvTag.Rows[0]);

                }
                else
                {
                    var objects = from TagItem item in _searchTagList
                                  where Like(item.TAGNAME, txtSearchTagname.Text) && Like(item.DESCRIPTION, txtSearchTagDesc.Text) && Like(item.LOC_CODE2, cbBlock.SelectedValue.ToString())
                                  orderby item.TAGNAME
                                  select item;                    

                    if (objects == null || objects.Count() <= 0) return;

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("TAG_TAGNAME", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_DESCRIPTION", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_USE_YN", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_COEFFICIENT", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_LOC_CODE", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_LOC_CODE2", typeof(string)));
                    dt.Columns.Add(new DataColumn("TAG_HTAGNAME", typeof(string)));

                    for (int i = objects.Count() - 1; i >= 0; i--)
                    {
                        TagItem tagItem = objects.ElementAt(i);
                        DataRow dr = dt.NewRow();
                        dr[Database.Fields.TAG.TAG_TAGNAME] = tagItem.TAGNAME;
                        dr[Database.Fields.TAG.TAG_DESCRIPTION] = tagItem.DESCRIPTION;
                        dr[Database.Fields.TAG.TAG_USE_YN] = tagItem.USE_YN;
                        dr[Database.Fields.TAG.TAG_COEFFICIENT] = tagItem.COEFFICIENT;
                        dr[Database.Fields.TAG.TAG_LOC_CODE] = tagItem.LOC_CODE;
                        dr[Database.Fields.TAG.TAG_LOC_CODE2] = tagItem.LOC_CODE2;
                        dr[Database.Fields.TAG.TAG_HTAGNAME] = tagItem.HTAGNAME;
                        dt.Rows.Add(dr);

                    }

                    dgvTag.DataSource = dt;

                    if (dgvTag.Rows.Count > 0)
                        dgvTag.ActiveRowScrollRegion.ScrollRowIntoView(dgvTag.Rows[0]);

                }
                

                
                //Console.WriteLine(string.Format("[{0}] 태그조회 End", DateTime.Now.ToString("HH:mm:ss:fff")));
            }
            catch (Exception ex)
            {
                FireException("btnSearchTag_Click", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dgvTag.BringToFront();
                //dgvTag.EndUpdate();
                Cursor = Cursors.Arrow;
            }
        }
        #endregion
        #endregion





        #region 블록태그 그리드 이벤트
        private void dgvBlockTag_CellChange(object sender, CellEventArgs e)
        {
            try
            {
            //    bool val = false;

            //    if (bool.TryParse(string.Format("{0}", e.Cell.Value), out val))
            //    {
            //        string tagName = string.Format("{0}", dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Value);
            //        TagItem updateTag = _searchTagList.Find(item => item.TAGNAME == tagName);

            //        // 가상구분 변경
            //        if (e.Cell.Column.Index == 12)
            //        {
            //            // 가상태그일 경우 배경색상 변경
            //            if (val)
            //            {
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.DESC].Appearance.BackColor = Color.SkyBlue;
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Appearance.BackColor = Color.SkyBlue;
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Appearance.BackColor = Color.SkyBlue;
            //            }
            //            else
            //            {
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.DESC].Appearance.BackColor = Color.Transparent;
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Appearance.BackColor = Color.Transparent;
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Appearance.BackColor = Color.Transparent;
            //            }
            //            updateTag.DUMMY_GBN = true;
            //        }
            //        // 태그구분 변경
            //        if (e.Cell.Column.Index >= 14)
            //        {
            //            if (val)
            //            {
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Index].Appearance.BackColor = Color.Red;
                                                        
            //                if (updateTag != null)
            //                {
            //                    if (updateTag.ChangeGubun.Contains(e.Cell.Column.Header.Caption) == false)
            //                        updateTag.ChangeGubun.Add(e.Cell.Column.Header.Caption);
            //                    updateTag.SetTagGbn(e.Cell.Column.Header.Caption, val);
            //                    updateTag.IsGbnChanged = true;
            //                }
            //            }
            //            else
            //            {
            //                dgvBlockTag.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Index].Appearance.BackColor = Color.Transparent;
            //            }
            //        }
            //    }


                // 태그구분 변경
                string tagName = string.Format("{0}", dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Value);
                TagItem tagItem = _searchTagList.Find(item => item.TAGNAME == tagName);
                string taggbn = e.Cell.Column.Key;

                // TD 룰 : 하나의 블록의 하나의 TD 태그만 사용해야 함
                // MNF 룰 : 하나의 블록의 하나의 MNF 태그만 사용해야 함
                // FRQ 룰: 하나의 블록의 하나의 FRQ 태그만 사용해야 함 
                if (taggbn == "TD" || taggbn == "MNF" || taggbn == "FRQ")
                {
                    string loc_code = dgvBlockTag.Rows[e.Cell.Row.Index].Cells["LOC_CODE"].Value.ToString();
                    string tag_name = dgvBlockTag.Rows[e.Cell.Row.Index].Cells["TAGNAME"].Value.ToString();

                    int cnt = GetDuplTagCount(tag_name, taggbn, loc_code);

                    if (cnt > 0)
                    {
                        MessageBox.Show(string.Format("하나의 블록에 {0}태그는 하나만 설정할 수 있습니다.", taggbn));
                        dgvBlockTag.Rows[e.Cell.Row.Index].Cells[14].Column.ValueList.SelectedItemIndex = 0;
                        dgvBlockTag.Rows[e.Cell.Row.Index].Cells[taggbn].Value = false;
                        return;
                    }
                }

                // FRQ_O 룰 : 대/중 블록에만 허용
                // FRQ_I 룰 : 대/중 블록에만 허용
                // SPL_I 룰 : 대/중 블록에만 허용
                // SPL_D 룰 : 대/중 블록에만 허용
                if (taggbn == "FRQ_O" || taggbn == "FRQ_I" || taggbn == "SPL_I" || taggbn == "SPL_D")
                {
                    string loc_code = dgvBlockTag.Rows[e.Cell.Row.Index].Cells["LOC_CODE"].Value.ToString();
                    
                    var BlockLevel = from BlockItem item in _blockList
                                     where item.LOC_CODE == loc_code
                                     select item.LEVEL;

                    if (BlockLevel.First() == 3)
                    {
                        MessageBox.Show(string.Format("{0}태그는 대/중 블록에만 사용할 수 있습니다.", taggbn));
                        dgvBlockTag.Rows[e.Cell.Row.Index].Cells[14].Column.ValueList.SelectedItemIndex = 0;
                        dgvBlockTag.Rows[e.Cell.Row.Index].Cells[taggbn].Value = false;
                        return;
                    }
                }

                if (e.Cell.Text == "True") dgvBlockTag.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Index].Value = true;
                else dgvBlockTag.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Index].Value = false;

                string tag_gbn = string.Empty;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[15].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.RT;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[16].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.FRI;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[17].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.TT;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[18].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.YD;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[19].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.YD_R;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[20].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.TD;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[21].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.MNF;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[22].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.FRQ;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[23].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.FRQ_I;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[24].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.FRQ_O;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[25].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.SPL_D;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[26].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.SPL_I;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[27].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.SPL_O;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[28].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.LEI;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[29].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.PHI;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[30].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.CLI;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[31].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.TBI;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[32].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.PRI;
                if ((bool)dgvBlockTag.Rows[e.Cell.Row.Index].Cells[33].Value) tag_gbn = tag_gbn + "," + Database.Fields.TAG_GUBUN.PMB;
                
                if (tag_gbn != string.Empty && tag_gbn.Substring(0, 1) == ",")
                    tag_gbn = tag_gbn.Remove(0, 1);

                int findIdx = _searchTagList.FindIndex(item => item.TAGNAME == tagItem.TAGNAME);
                if (findIdx >= 0)
                {
                    _searchTagList[findIdx].TAG_GBN = tag_gbn;
                }
                

            }
            catch (Exception ex)
            {
                FireException("dgvBlockTag_CellChange", ex);
                MessageBox.Show(ex.Message);
            }
}

        /// <summary>
        /// 블록태그 값 변경 이벤트
        /// </summary>
        #region private void dgvBlockTag_AfterCellUpdate(object sender, CellEventArgs e)
        private void dgvBlockTag_AfterCellUpdate(object sender, CellEventArgs e)
        {
            try
            {
                bool val = false;

                if (bool.TryParse(string.Format("{0}", e.Cell.Value), out val))
                {
                    string tagName = string.Format("{0}", dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Value);
                    TagItem updateTag = _searchTagList.Find(item => item.TAGNAME == tagName);

                    // 가상구분 변경
                    if (e.Cell.Column.Index == 12)
                    {
                        Color backColor = Color.Transparent;

                        // 가상태그일 경우 배경색상 변경
                        if (val)
                        {
                            backColor = Color.SkyBlue;
                            backColor = Color.SkyBlue;
                            backColor = Color.SkyBlue;
                        }
                        else
                        {
                            backColor = Color.Transparent;
                            backColor = Color.Transparent;
                            backColor = Color.Transparent;
                        }
                        dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.DESC].Appearance.BackColor = backColor;
                        dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Appearance.BackColor = backColor;
                        dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Appearance.BackColor = backColor;

                        updateTag.DUMMY_GBN = val;
                        updateTag.IsGbnChanged = true;

                        // 다른 태그구분의 가상태그 처리
                        var dummyGbnItems = from TagItem item in _searchTagList
                                            where item.TAGNAME == tagName
                                            select item;
                        if (dummyGbnItems.Count() > 0)
                        {
                            for (int i = 0; i < dummyGbnItems.Count(); i++)
                            {
                                TagItem dummyGbnItem = dummyGbnItems.ElementAt(i);
                                dummyGbnItem.DUMMY_GBN = val;
                            }
                        }
                    }
                    // 태그구분 변경
                    if (e.Cell.Column.Index >= 14)
                    {
                        if (val)
                        {
                            dgvBlockTag.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Index].Appearance.BackColor = Color.Red;                            
                        }
                        else
                        {
                            dgvBlockTag.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Index].Appearance.BackColor = Color.Transparent;
                        }
                        if (updateTag != null)
                        {
                            if (updateTag.ChangeGubun.Contains(e.Cell.Column.Header.Column.ToString()) == false)
                                updateTag.ChangeGubun.Add(e.Cell.Column.Header.Column.ToString());
                            updateTag.SetTagGbn(e.Cell.Column.Header.Column.ToString(), val);
                            updateTag.IsGbnChanged = true;
                        }                        
                    }

                    // 태그계산 그리드 활성화 여부 설정
                    if (e.Cell.Column.Index == 12 )
                    {
                    if(val)
                        tabTagCal.Enabled = true;
                    else
                        tabTagCal.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                FireException("dgvBlockTag_CellChange", ex);
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// 블록태그 그리드 선택 변경 이벤트 (태그 상세 화면 출력)
        /// </summary>
        #region private void dgvBlockTag_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        private void dgvBlockTag_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                RemoveRowsAll(dgvCalcTag);

                

                if (dgvBlockTag.Selected.Rows.Count > 0)
                {
                    if ((bool)dgvBlockTag.Selected.Rows[0].Cells["DUMMY_GBN"].Value)
                        tabTagCal.Enabled = true;
                    else
                        tabTagCal.Enabled = false;
                }


                if (_isDraging) return;
                if (dgvBlockTag.Selected == null || dgvBlockTag.Selected.Rows.Count <= 0) return;
                string type = string.Format("{0}", dgvBlockTag.Selected.Rows[0].Cells[Database.Fields.BLOCK.TYPE].Value);
                string tagName = string.Format("{0}", dgvBlockTag.Selected.Rows[0].Cells[Database.Fields.TAG.TAGNAME].Value);

                ClearForm(0);

                // T : Tag
                if (type.ToUpper() != "T") return;

                var objects = from TagItem item in _searchTagList
                              where item.TAGNAME == tagName
                              select item;
                if (objects == null || objects.Count() <= 0)
                {
                    objects = from TagItem item in _tagList
                              where item.TAGNAME == tagName
                              select item;
                    if (objects == null || objects.Count() <= 0) return;
                }
                TagItem tagItem = objects.First();
                txtTagname.Text = tagItem.TAGNAME;
                txtDesc.Text = tagItem.DESCRIPTION;
                txtFtrIdn.Text = tagItem.FTR_IDN;
                txtCoefficient.Text = tagItem.COEFFICIENT;
                txtLocCode.Text = tagItem.LOC_CODE;
                txtLocCode2.Text = tagItem.LOC_CODE2;
                txtHtagname.Text = tagItem.HTAGNAME;
                if (tagItem.USE_YN == "Y") cbUseYn.SelectedIndex = 1;
                else if (tagItem.USE_YN == "N") cbUseYn.SelectedIndex = 2;
                else cbUseYn.SelectedIndex = 0;

                // if Dummy Tag 인경우 TagCalc 그리드에 정보를 표시
                if (tagItem.DUMMY_GBN)
                {
                    var objectsCal = from TagCalcItem item in _tagCalListAll
                                  where item.TAGNAME == tagItem.TAGNAME
                                  select item;
                    
                        if (objectsCal == null || objectsCal.Count() <= 0) return;

                    // 계산 태그의 목록 생성
                    foreach (TagCalcItem tagCalItem in objectsCal)
                    {
                        // 삭제된 아이템일 경우 패스
                        if (tagCalItem.State == TagCalcItem.StateType.Del) continue;

                        bool duplYn = true;
                        foreach (UltraGridRow caclRow in dgvCalcTag.Rows)
                        {
                            if (caclRow.Cells["CTAGNAME"].Value.ToString() == tagCalItem.CTAGNAME)
                            {
                                duplYn = false;
                                break;
                            }
                        }

                        if (duplYn)
                        {
                            UltraGridRow row = null;
                            row = dgvCalcTag.DisplayLayout.Bands[0].AddNew();
                            row.Cells[Database.Fields.TAG.TAGNAME].Value = tagCalItem.TAGNAME;
                            row.Cells[Database.Fields.TAG_CAL.CAL_GBN].Value = TagCalcItem.toCalStr(tagCalItem.CAL_GBN);
                            row.Cells[Database.Fields.TAG_CAL.CTAGNAME].Value = tagCalItem.CTAGNAME;
                            row.Cells[Database.Fields.TAG.DESC].Value = tagCalItem.TAG_DESC;
                            row.Cells[Database.Fields.TAG.COEFFICIENT].Value = tagCalItem.COEFFICIENT;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                FireException("dgvBlockTag_AfterSelectChange", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion
        #endregion

        #region 태그 그리드 이벤트
        /// <summary>
        /// 태그 그리드 선택 변경시 태그 정보 표현
        /// </summary>
        #region private void dgvTag_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        private void dgvTag_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                if (_isDraging) return;
                if (dgvTag.Selected == null || dgvTag.Selected.Rows.Count != 1) return;

                ClearForm(1);
                string tagName = string.Format("{0}", dgvTag.Selected.Rows[0].Cells[Database.Fields.TAG.TAG_TAGNAME].Value);

                txtTagTagname.ReadOnly = true;
                txtTagDesc.ReadOnly = true;
                txtTagFtrIdn.ReadOnly = true;
                txtHtagname.ReadOnly = true;
                txtTagCoefficient.ReadOnly = true;
                txtTagLocCode.ReadOnly = true;
                txtTagLocCode2.ReadOnly = true;
                cbTagUseYn.Enabled = false;

                var objects = from TagItem item in _searchTagList
                              where item.TAGNAME == tagName
                              select item;
                if (objects == null || objects.Count() <= 0) return;
                TagItem tagItem = objects.First();
                txtTagTagname.Text = tagItem.TAGNAME;
                txtTagDesc.Text = tagItem.DESCRIPTION;
                txtTagFtrIdn.Text = tagItem.FTR_IDN;
                txtTagCoefficient.Text = tagItem.COEFFICIENT;
                txtTagLocCode.Text = tagItem.LOC_CODE;
                txtTagLocCode2.Text = tagItem.LOC_CODE2;
                txtTagHTagname.Text = tagItem.HTAGNAME;
                if (tagItem.USE_YN == "Y") cbTagUseYn.SelectedIndex = 1;
                else if (tagItem.USE_YN == "N") cbTagUseYn.SelectedIndex = 2;
                else cbTagUseYn.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                FireException("dgvTag_AfterSelectChange", ex);
                MessageBox.Show(ex.Message);
            }
        } 
        #endregion

        /// <summary>
        /// 태그 계산식 그리드 이벤트
        /// </summary>
        #region private void dgvCalc_CellChange(object sender, CellEventArgs e)
        private void dgvCalc_CellChange(object sender, CellEventArgs e)
        {
            try
            {
                // 사용자가 입력한 정보 추출
                //string tagName = string.Format("{0}", ((UltraGrid)sender).Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Value);
                //string cTagName = string.Format("{0}", ((UltraGrid)sender).Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG_CAL.CTAGNAME].Value);
                //string calGbn = TagCalcItem.toStrCal(string.Format("{0}", e.Cell.Text));

                // 계산 태그 찾기
                //foreach (TagCalcItem tagCalcItem in _tagCalListAll)
                //{
                //    if (tagCalcItem.TAGNAME == tagName && tagCalcItem.CTAGNAME == cTagName)
                //    {
                //        tagCalcItem.CAL_GBN = calGbn;
                //
                //        if (tagCalcItem.State != TagCalcItem.StateType.New && tagCalcItem.State != TagCalcItem.StateType.Del)
                //            tagCalcItem.State = TagCalcItem.StateType.Mod;
                //    }
                //}
            }
            catch (Exception ex)
            {
                FireException("dgvCalc_CellChange", ex);
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void dgvCalc_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        #endregion





        #region Drag & Drap 처리
        /// <summary>
        /// Contrl Key 활성 여부 (Drag Data 초기화시 사용)
        /// </summary>
        private bool _isControl = false;
        /// <summary>
        /// Shift Key 활성 여부 (Drag Data 초기화시 사용)
        /// </summary>
        private bool _isShift = false;

        /// <summary>
        /// Drag된 태그의 태그일련번호
        /// </summary>
        private List<string> _dragTagnames = new List<string>();
        private List<string> _dragCTagnames = new List<string>();
        /// <summary>
        /// Drag된 태그의 그리드 로우 인덱스
        /// </summary>
        private List<int> _dragIndex = new List<int>();

        /// <summary>
        /// true : 태그 그리드 Drag / false : 블록 그리드 Drag
        /// </summary>
        private bool _isTagGridDrop = false;

        /// <summary>
        /// Drag & Drop중 (이벤트 해제)
        /// </summary>
        private bool _isDraging = false;

        ////////////////////////////////////////////////////////////////////////////////
        //
        // 태그 그리드 Drag 
        //
        ////////////////////////////////////////////////////////////////////////////////

        #region 태그 그리드에서 블록 그리드로 이동 처리 (순서대로 진행됨)
        /// <summary>
        /// 태드 그리드에서 끌기 선택
        /// </summary>
        #region private void dgvTag_SelectionDrag(object sender, CancelEventArgs e)
        private void dgvTag_SelectionDrag(object sender, CancelEventArgs e)
        {
            try
            {
                if (_isDraging) return;
                if (dgvTag.Selected == null || dgvTag.Selected.Rows.Count <= 0) return;

                if (_isShift == false && _isControl == false)
                {
                    _dragIndex.Clear();
                    _dragTagnames.Clear();
                }

                // 선택된 태그를 복사열에 추가
                for (int i = 0; i < dgvTag.Selected.Rows.Count; i++)
                {
                    string tagName = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG.TAG_TAGNAME].Value);
                    if (tagName == "" || tagName.Length <= 0) continue;
                    _dragTagnames.Add(tagName);
                    _dragIndex.Add(dgvTag.Selected.Rows[i].Index);
                }

                _isTagGridDrop = true;
            }
            catch (Exception ex)
            {
                FireException("dgvTag_SelectionDrag", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dgvTag.DoDragDrop(dgvTag.Selected.Rows, DragDropEffects.Copy);
            }
        } 
        #endregion

        /// <summary>
        /// 블록 그리드 위에서 데이터를 끌때 발생
        /// </summary>
        #region private void dgvBlockTag_DragOver(object sender, DragEventArgs e)
        private void dgvBlockTag_DragOver(object sender, DragEventArgs e)
        {
            // 복사 효과 설정
            e.Effect = DragDropEffects.Copy;
        } 
        #endregion

        /// <summary>
        /// 블록 그리드에 끌어놓기 처리 (태그 매핑(1)과 태그 위치 이동(2))
        /// </summary>
        #region private void dgvBlockTag_DragDrop(object sender, DragEventArgs e)
		private void dgvBlockTag_DragDrop(object sender, DragEventArgs e)
        {
            _isDraging = true;
            try
            {
                if (e.Effect == DragDropEffects.Copy)
                {
                    Point clientPoint = dgvBlockTag.PointToClient(new Point(e.X, e.Y));
                    if (_dragTagnames != null && _dragTagnames.Count > 0)
                    {
                        // 태그에서 블록으로 끌기 : 블록에 태그 매핑
                        // 블록에서 태그로 끌기 : 블록에서 태그 제거
                        if (_isTagGridDrop) DropTagToBlock(clientPoint);
                        else DropBlockToBlock(clientPoint);
                    }
                }
            }
            catch (Exception ex)
            {
                FireException("dgvBlockTag_DragDrop", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _isDraging = false;
            }
        } 
	    #endregion

        /// <summary>
        /// [처리1] 태그 그리드에서 끌어온 데이터를 블록 그리드에서 처리
        /// </summary>
        /// <param name="clientPoint">블록 그리드의 HitTest Point</param>
        #region private void DropTagToBlock(Point clientPoint)
        private void DropTagToBlock(Point clientPoint)
        {
            try
            {
                // 선택된 블록 Row 찾기
                UIElement uieOver = dgvBlockTag.DisplayLayout.UIElement.ElementFromPoint(clientPoint);
                UltraGridRow blockRow = uieOver.GetContext(typeof(UltraGridRow), true) as UltraGridRow;
                if (blockRow == null) return;

                // Row 정보 추출
                string type = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.TYPE].Value);
                string locName = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LOC_NAME].Value);
                string locNameOrg = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value);
                string locCode = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LOC_CODE].Value);
                string tbGbn = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value);
                int level = Convert.ToInt32(string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LEVEL].Value));

                if (_dragTagnames == null || _dragTagnames.Count <= 0) return;

                string msg = "'{0}' 태그는 '{1}' 블록과 매핑된 태그입니다.\r\n변경하시겠습니까?";
                List<string> dupList = new List<string>();

                // 그리드 업데이트 시작
                dgvBlockTag.BeginUpdate();
                // 처리후 스크롤 이동할 Row
                UltraGridRow lastRow = null;

                #region 선택된 블록으로 태그 매핑
                int idx = blockRow.Index + 1; // 끌어 놓은 Row의 다음 Row에 삽입
                for (int i = 0; i < _dragTagnames.Count; i++)
                {
                    var objects = from TagItem item in _searchTagList
                                  where item.TAGNAME == _dragTagnames[i]
                                  select item;
                    if (objects == null || objects.Count() <= 0) continue;
                    TagItem tagItem = objects.First();

                    // 동일 블록일 경우 Pass
                    if (tagItem.LOC_CODE2 == locCode) continue;
                    if (tagItem.LOC_CODE2.Length > 0 && tagItem.LOC_CODE2 != locCode)
                    {
                        if (MessageBox.Show(string.Format(msg, tagItem.TAGNAME, locName), "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) continue;
                        dupList.Add(_dragTagnames[i]);
                    }

                    // LOC_CODE2 설정
                    tagItem.LOC_CODE2 = locCode;
                    dgvTag.Rows[_dragIndex[i]].Cells[Database.Fields.TAG.TAG_LOC_CODE2].Value = locCode;

                    // 블록 그리드에 매핑한 태그 추가
                    UltraGridRow newRow = dgvBlockTag.DisplayLayout.Bands[0].AddNew();
                    newRow.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = locNameOrg;
                    newRow.Cells[Database.Fields.TAG.TAGNAME].Value = tagItem.TAGNAME;
                    newRow.Cells[Database.Fields.TAG.DESC].Value = tagItem.DESCRIPTION;
                    newRow.Cells[Database.Fields.TAG.LOC_CODE].Value = tagItem.LOC_CODE2;
                    newRow.Cells[Database.Fields.BLOCK.TYPE].Value = "T";
                    newRow.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = "2";
                    newRow.Cells[Database.Fields.BLOCK.LEVEL].Value = level;
                    SetGridTagGubun(newRow, tagItem);
                    // 위치 이동
                    newRow.ParentCollection.Move(newRow, idx);

                    // LOC_CODE 설정
                    if (tbGbn == "1")
                    {
                        tagItem.LOC_CODE = locCode;
                        dgvTag.Rows[_dragIndex[i]].Cells[Database.Fields.TAG.TAG_LOC_CODE].Value = locCode;
                        newRow.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value = "1";
                    }

                    // 태그는 LOC_NAME을 표현하지 않는다.


                    lastRow = newRow;
                    // 태그 매핑 변경 설정
                    tagItem.IsMapping = true;
                    idx += 1;
                }
                #endregion

                #region 이동 처리 (이전 태그정보 제거)
                if (dupList.Count > 0)
                {
                    // 이전 블록 매핑을 그리드에서 제거
                    for (int i = dgvBlockTag.Rows.Count - 1; i >= 0; i--)
                    {
                        if (dupList.Count <= 0) break;
                        string checkTagname = string.Format("{0}", dgvBlockTag.Rows[i].Cells[Database.Fields.TAG.TAGNAME].Value);
                        string checkLocCode = string.Format("{0}", dgvBlockTag.Rows[i].Cells[Database.Fields.TAG.LOC_CODE].Value);

                        for (int j = dupList.Count - 1; j >= 0; j--)
                        {
                            if (dupList[j] == checkTagname && locCode != checkLocCode)
                            {
                                dgvBlockTag.Rows[i].Delete(false);
                                dupList.RemoveAt(j);
                                break;
                            }
                        }
                    }
                }
                #endregion

                // 모든 처리를 끝낸 이후 스크롤 이동
                if (lastRow != null) dgvBlockTag.ActiveRowScrollRegion.ScrollRowIntoView(lastRow);
            }
            catch (Exception ex)
            {
                FireException("DropTagToBlock", ex);
                throw ex;
            }
            finally
            {
                dgvBlockTag.EndUpdate();
            }
        }
        #endregion

        /// <summary>
        /// 태그를 태그 계산 그리드에 놓기 (가상태그에 계산 태그 추가)
        /// </summary>
        #region private void dgvCalc_DragDrop(object sender, DragEventArgs e)
        private void dgvCalc_DragDrop(object sender, DragEventArgs e)
        {
            try
            {

               //UltraGrid dgvCalc = sender as UltraGrid;

               for (int i = 0; i < dgvTag.Selected.Rows.Count; i++)
                {
                    // 태그의 정보 추출
                    string cTagName = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG.TAG_TAGNAME].Value);
                    string cTagDesc = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG.TAG_DESCRIPTION].Value);
                    string cTagCoefficient = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG.TAG_COEFFICIENT].Value);
                    //string cTagGbn = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG_GUBUN.TAG_GBN].Value);
                    //string cTagDummyGbn = string.Format("{0}", dgvTag.Selected.Rows[i].Cells[Database.Fields.TAG_GUBUN.DUMMY_GBN].Value);

                    // 계산식일경우 제외
                    //if (cTagDummyGbn.ToUpper() == "CAL") continue;
                    // 태그구분이 다를 경우 제외
                    //if (tabTagCal.ActiveTab.Key.ToString() != cTagGbn) continue;
                    // 태그명이 없을 경우 제외(블록일 경우)
                    if (cTagName == null || cTagName.Length <= 0) continue;

                    #region 중복 체크
                    bool isPass = false;
                    for (int j = 0; j < dgvCalcTag.Rows.Count; j++)
                    {
                        string checkTagName = string.Format("{0}", dgvCalcTag.Rows[j].Cells[Database.Fields.TAG_CAL.CTAGNAME].Value);
                        if (checkTagName == cTagName)
                        {
                            isPass = true;
                            break;
                        }
                    }
                    if (isPass) continue;
                    #endregion

                    #region 계산식 Row 추가
                    dgvCalcTag.BeginUpdate();
                    UltraGridRow newRow = dgvCalcTag.DisplayLayout.Bands[0].AddNew();
                    newRow.Cells[Database.Fields.TAG.TAGNAME].Value = txtTagname.Text;
                    newRow.Cells[Database.Fields.TAG_CAL.CAL_GBN].Value = "P";
                    newRow.Cells[Database.Fields.TAG_CAL.CTAGNAME].Value = cTagName;
                    newRow.Cells[Database.Fields.TAG.DESC].Value = cTagDesc;
                    newRow.Cells[Database.Fields.TAG.COEFFICIENT].Value = cTagCoefficient;
                    dgvCalcTag.EndUpdate();
                    #endregion

                    #region 데이터 반영

                    // 새로운 태그 계산식 아이템 생성
                    TagCalcItem newItem = new TagCalcItem();
                    newItem.LOC_CODE = txtLocCode.Text;
                    //newItem.LOC_NAME = blockTagCalcItem.LOC_NAME;
                    newItem.TAGNAME = txtTagname.Text;
                    newItem.DESCRIPTION = txtDesc.Text;
                    newItem.TAG_GBN = tabTagCal.ActiveTab.Key.ToString();
                    newItem.CAL_GBN = "P";
                    newItem.CAL_GBN_ORG = "P";
                    newItem.DUMMY_GBN = "CAL";
                    newItem.CTAGNAME = cTagName;
                    newItem.TAG_DESC = cTagDesc;
                    newItem.COEFFICIENT = cTagCoefficient;
                    newItem.State = TagCalcItem.StateType.New;
                    _tagCalListAll.Add(newItem);

                    _dragCTagnames.Clear();
                    #endregion
 
                }

            }
            catch (Exception ex)
            {
                FireException("dgvCalc_DragDrop", ex);
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

      

        #endregion

        ////////////////////////////////////////////////////////////////////////////////
        //
        // 블록 그리드 Drag
        //
        ////////////////////////////////////////////////////////////////////////////////

        #region 블록 그리드에서 블록 그리드로 이동 처리
        /// <summary>
        /// 블록 그리드에서 끌기 시작
        /// </summary>
        #region private void dgvBlockTag_SelectionDrag(object sender, CancelEventArgs e)
        private void dgvBlockTag_SelectionDrag(object sender, CancelEventArgs e)
        {
            try
            {
                if (_isDraging) return;
                if (dgvBlockTag.Selected == null || dgvBlockTag.Selected.Rows.Count <= 0) return;

                if (_isShift == false && _isControl == false)
                {
                    _dragIndex.Clear();
                    _dragTagnames.Clear();
                }

                // 처리할 데이터열 생성
                for (int i = 0; i < dgvBlockTag.Selected.Rows.Count; i++)
                {
                    string tagName = string.Format("{0}", dgvBlockTag.Selected.Rows[i].Cells[Database.Fields.TAG.TAGNAME].Value);
                    if (tagName == "" || tagName.Length <= 0) continue;
                    _dragTagnames.Add(tagName);
                    _dragIndex.Add(dgvBlockTag.Selected.Rows[i].Index);
                }

                _isTagGridDrop = false;
            }
            catch (Exception ex)
            {
                FireException("dgvBlockTag_SelectionDrag", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dgvBlockTag.DoDragDrop(dgvBlockTag.Selected.Rows, DragDropEffects.Copy);
            }
        } 
        #endregion

        /// <summary>
        /// 태그 그리드 위에서 데이터를 끌때 발생
        /// </summary>
        #region private void dgvTag_DragOver(object sender, DragEventArgs e)
        private void dgvTag_DragOver(object sender, DragEventArgs e)
        {
            if (_isTagGridDrop) return;
            e.Effect = DragDropEffects.Copy;
        }
        #endregion

        /// <summary>
        /// 태그 그리드에 끌어 놓기 처리 (태그 매핑 제거)
        /// </summary>
        #region private void dgvTag_DragDrop(object sender, DragEventArgs e)
        private void dgvTag_DragDrop(object sender, DragEventArgs e)
        {
            UltraGrid grid = sender as UltraGrid;
            _isDraging = true;
            try
            {
                if (e.Effect == DragDropEffects.Copy)
                {
                    Point clientPoint = dgvBlockTag.PointToClient(new Point(e.X, e.Y));

                    //if (_dragCTagnames.Count > 0) 
                    //    DropCalcToTag(grid, clientPoint);

                    // 블록에서 태그 제거
                    if (_dragTagnames == null || _dragTagnames.Count <= 0 || _isTagGridDrop) return;
                        DropBlockToTag(clientPoint);
                }
            }
            catch (Exception ex)
            {
                FireException("dgvTag_DragDrop", ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _isDraging = false;
            }
        } 
        #endregion

        /// <summary>
        /// 블록 그리드에서 태그 그리드로 끌어놓기 처리 (태그 매핑 제거)
        /// </summary>
        /// <param name="clientPoint">블록 그리드 HitTest Point</param>
        #region private void DropBlockToTag(Point clientPoint)
        private void DropBlockToTag(Point clientPoint)
        {
            try
            {
                if (MessageBox.Show("태그의 블록정보를 삭제하시겠습니까?\n(태그를 삭제하면, 설정된 더미태그의 정보도 함꼐 삭제 됩니다.)", "삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                #region 해당 태그를 설정된 더미태그에서 삭제

                //if (_tagCalListAll.FindIndex(item => item.TAGNAME == tagName && item.TAG_GBN == tagGbn && item.CTAGNAME == cTagName) > 0) continue;

                for (int i = 0; i < _dragTagnames.Count; i++)
                {
                    var objects = from TagCalcItem item in _tagCalListAll
                                  where item.CTAGNAME == _dragTagnames[i]
                                  select item;
                    if (objects == null || objects.Count() <= 0) continue;

                    // 삭제상태로 변경
                    foreach (TagCalcItem tagitem in objects)
                        tagitem.State = TagCalcItem.StateType.Del;
                }
                // 더미 태그 리스트 Refresh
                dgvBlockTag_AfterSelectChange(dgvBlockTag, null);

                #endregion

                #region 블록 그리드와 TagItem 설정
                for (int i = 0; i < _dragTagnames.Count; i++)
                {
                    var objects = from TagItem item in _searchTagList
                                  where item.TAGNAME == _dragTagnames[i]
                                  select item;
                    if (objects == null || objects.Count() <= 0) continue;
                    TagItem tagItem = objects.First();

                    // TagItem 설정
                    tagItem.LOC_CODE2 = "";
                    tagItem.LOC_CODE = "";
                    // 태그 매핑 변경 설정
                    tagItem.IsMapping = true;                    
                }
                #endregion

                // 블록 그리드에서 선택 태그  삭제
                dgvBlockTag.Selected.Rows.Clear();
                for (int i = _dragIndex.Count - 1; i >= 0; i--)
                {
                    dgvBlockTag.Selected.Rows.Add(dgvBlockTag.Rows[_dragIndex[i]]);
                }
                dgvBlockTag.DeleteSelectedRows(false);

                #region 태그 그리드 설정
                for (int i = 0; i < dgvTag.Rows.Count; i++)
                {
                    for (int j = _dragTagnames.Count - 1; j >= 0; j--)
                    {
                        string checkTagname = string.Format("{0}", dgvTag.Rows[i].Cells[Database.Fields.TAG.TAG_TAGNAME].Value);
                        if (_dragTagnames[j] == checkTagname)
                        {
                            dgvTag.Rows[i].Cells[Database.Fields.TAG.TAG_LOC_CODE].Value = "";
                            dgvTag.Rows[i].Cells[Database.Fields.TAG.TAG_LOC_CODE2].Value = "";
                            _dragTagnames.RemoveAt(j);
                            break;
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("DropBlockToTag", ex);
                throw ex;
            }
        } 
        #endregion

        /// <summary>
        /// 블록 그리드에서 태그 그리드로 끌어놓기 처리 (태그 매핑 제거)
        /// </summary>
        /// <param name="clientPoint">블록 그리드 HitTest Point</param>
        #region private void DropBlockToTag(Point clientPoint)
        private void DropCalcToTag(UltraGrid grid, Point clientPoint)
        {
            try
            {
                if (MessageBox.Show("태그를 삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                // 계산태그 리스트에서 삭제
                string tagname = txtTagname.Text;
                string tag_gbn = tabTagCal.ActiveTab.Key.ToString();
                string ctagname = dgvCalcTag.Selected.Rows[0].Cells[2].Value.ToString();
                foreach (TagCalcItem item in _tagCalListAll)
                {
                    if (tagname == item.TAGNAME && ctagname == item.CTAGNAME)
                        item.State = TagCalcItem.StateType.Del;
                }
                dgvCalcTag.DeleteSelectedRows(false);
            }
            catch (Exception ex)
            {
                FireException("DropBlockToTag", ex);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// [처리2] 블록 그리드에서 블록 그리드로 데이터 끌어오기 처리
        /// </summary>
        /// <param name="clientPoint">블록 그리드의 HitTest Point</param>
        #region private void DropBlockToBlock(Point clientPoint)
        private void DropBlockToBlock(Point clientPoint)
        {
            try
            {
                // 선택된 위치의 Row 찾기
                UIElement uieOver = dgvBlockTag.DisplayLayout.UIElement.ElementFromPoint(clientPoint);
                UltraGridRow blockRow = uieOver.GetContext(typeof(UltraGridRow), true) as UltraGridRow;
                if (blockRow == null) return;

                // Row 정보 추출
                string type = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.TYPE].Value);
                string locName = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LOC_NAME].Value);
                string locNameOrg = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value);
                string locCode = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LOC_CODE].Value);
                string tbGbn = string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.CM_LOC_GBN].Value);
                int level = Convert.ToInt32(string.Format("{0}", blockRow.Cells[Database.Fields.BLOCK.LEVEL].Value));

                if (_dragTagnames == null || _dragTagnames.Count <= 0) return;

                // 그리드 업데이트 시작
                dgvBlockTag.BeginUpdate();
                // 처리후 스크롤 이동할 Row
                UltraGridRow lastRow = null;

                #region 선택된 블록으로 태그 매핑
                int idx = blockRow.Index; // 끌어 놓은 Row의 다음 Row에 삽입
                if (idx < dgvBlockTag.Selected.Rows[0].Index) idx += 1;
                for (int i = 0; i < _dragTagnames.Count; i++)
                {
                    var objects = from TagItem item in _searchTagList
                                  where item.TAGNAME == _dragTagnames[i]
                                  select item;
                    if (objects == null || objects.Count() <= 0)
                    {
                        objects = from TagItem item in _tagList
                                  where item.TAGNAME == _dragTagnames[i]
                                  select item;
                        if (objects == null || objects.Count() <= 0) continue;
                    }
                    TagItem tagItem = objects.First();

                    // 동일 블록일 경우 Pass
                    if (tagItem.LOC_CODE2 == locCode) continue;

                    // LOC_CODE2 설정
                    tagItem.LOC_CODE2 = locCode;
                    if (tagItem.CM_LOC_GBN == "2") tagItem.LOC_CODE = locCode;

                    // 이동할 블록의 정보로 교체
                    UltraGridRow moveRow = dgvBlockTag.Selected.Rows[i];
                    string moveRowType = string.Format("{0}", moveRow.Cells[Database.Fields.BLOCK.TYPE].Value);
                    if (moveRowType.ToUpper() != "T") continue;
                    moveRow.Cells[Database.Fields.TAG.LOC_CODE].Value = locCode;
                    moveRow.Cells[Database.Fields.BLOCK.LOC_NAME_ORG].Value = locNameOrg;
                    // Row 이동 처리
                    moveRow.ParentCollection.Move(moveRow, idx);

                    lastRow = moveRow;
                    // 태그 매핑 변경 설정
                    tagItem.IsMapping = true;
                    if (idx < moveRow.Index) idx += 1;
                }
                #endregion

                // 모든 처리를 끝낸 이후 스크롤 이동
                if (lastRow != null) dgvBlockTag.ActiveRowScrollRegion.ScrollRowIntoView(lastRow);
            }
            catch (Exception ex)
            {
                FireException("DropBlockToBlock", ex);
                throw ex;
            }
            finally
            {
                dgvBlockTag.EndUpdate();
            }
        }
        #endregion
        #endregion
        #endregion





        /// <summary>
        /// 화면 초기화
        /// </summary>
        /// <param name="target">0:BlockTag / 1:Tag</param>
        #region private void ClearForm(int target)
        private void ClearForm(int target)
        {
            if (target == 0)
            {
                txtTagname.Text = "";
                txtDesc.Text = "";
                txtHtagname.Text = "";
                txtFtrIdn.Text = "";                
                txtCoefficient.Text = "";
                txtLocCode.Text = "";
                txtLocCode2.Text = "";
                cbUseYn.SelectedIndex = 0;
            }
            else
            {
                txtTagTagname.Text = "";
                txtTagDesc.Text = "";
                txtTagHTagname.Text = "";
                txtTagFtrIdn.Text = "";
                txtTagCoefficient.Text = "";
                txtTagLocCode.Text = "";
                txtTagLocCode2.Text = "";
                cbTagUseYn.SelectedIndex = 0;
            }
        }
        #endregion







        /// <summary>
        /// TAG 그리드 보이기/숨기기 버튼
        /// </summary>
        #region private void btnShowTag_Click(object sender, EventArgs e)
        private void btnShowTag_Click(object sender, EventArgs e)
        {
            if (btnShowTag.Text == "◀")
            {
                splitContainer1.Panel2Collapsed = false;
                btnShowTag.Text = "▶";
                //pnlClose.Visible = false;
            }
            else
            {
                splitContainer1.Panel2Collapsed = true;
                btnShowTag.Text = "◀";
                //pnlClose.Visible = true;
            }
        }
        #endregion






        #region [DB] TAG
        /// <summary>
        /// 지정 태그의 DB Count
        /// </summary>
        /// <param name="mapper">EMapper</param>
        /// <param name="tagName">태그일련번호</param>
        /// <returns>태그 카운트</returns>
        #region private int ExecTagCount(EMFrame.dm.EMapper mapper, string tagName)
        private int ExecTagCount(EMFrame.dm.EMapper mapper, string tagName)
        {
            int retVal = 0;

            try
            {
                DataTable dt = new DataTable();
                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.TAGMAPPING_TAG_COUNT, tagName), null);

                if (dt != null && dt.Rows.Count > 0)
                {
                    string cnt = string.Format("{0}", dt.Rows[0]["CNT"]);
                    if (!int.TryParse(cnt, out retVal)) retVal = 0;
                }
            }
            catch (Exception ex)
            {
                FireException("ExecTagCount", ex);
                throw ex;
            }
            return retVal;
        }
        #endregion

        /// <summary>
        /// 태그 등록
        /// </summary>
        /// <param name="mapper">EMapper</param>
        #region private void ExecTagInsert(EMFrame.dm.EMapper mapper)
        private void ExecTagInsert(EMFrame.dm.EMapper mapper)
        {
            try
            {
                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),
                                                  new OracleParameter("2", OracleDbType.Varchar2),
                                                  new OracleParameter("3", OracleDbType.Varchar2),
                                                  new OracleParameter("4", OracleDbType.Varchar2),
                                                  new OracleParameter("5", OracleDbType.Int32),
                                                  new OracleParameter("6", OracleDbType.Varchar2),
                                                  new OracleParameter("7", OracleDbType.Varchar2),
                                                  new OracleParameter("8", OracleDbType.Varchar2)
                                              };
                parameters[0].Value = txtTagTagname.Text;
                parameters[1].Value = txtTagDesc.Text;
                string useYn = "";
                if (cbTagUseYn.Text == "예") useYn = "Y";
                else if (cbTagUseYn.Text == "아니오") useYn = "N";
                parameters[2].Value = useYn;
                parameters[3].Value = txtTagFtrIdn.Text;
                if (txtTagCoefficient.Text == "") parameters[4].Value = DBNull.Value;
                else parameters[4].Value = txtTagCoefficient.Text;
                parameters[5].Value = txtTagLocCode.Text;
                parameters[6].Value = txtTagLocCode2.Text;
                parameters[7].Value = txtTagHTagname.Text;

                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_IHTAGS_INSERT, parameters);

                #region 내부 데이터 반영
                TagItem newItem = new TagItem(txtTagTagname.Text, txtTagDesc.Text, useYn, txtTagFtrIdn.Text, txtTagCoefficient.Text, txtTagLocCode.Text, txtTagLocCode2.Text, "", txtTagHTagname.Text, "");
                _searchTagList.Add(newItem);
                
                UltraGridRow newRow = dgvTag.DisplayLayout.Bands[0].AddNew();
                newRow.Cells[Database.Fields.TAG.TAG_TAGNAME].Value = txtTagTagname.Text;
                newRow.Cells[Database.Fields.TAG.TAG_DESCRIPTION].Value = txtTagDesc.Text;
                newRow.Cells[Database.Fields.TAG.TAG_USE_YN].Value = useYn;
                newRow.Cells[Database.Fields.TAG.TAG_COEFFICIENT].Value = txtTagCoefficient.Text;
                newRow.Cells[Database.Fields.TAG.TAG_LOC_CODE].Value = txtTagLocCode.Text;
                newRow.Cells[Database.Fields.TAG.TAG_LOC_CODE2].Value = txtTagLocCode2.Text;
                newRow.Cells[Database.Fields.TAG.TAG_HTAGNAME].Value = txtTagHTagname.Text;
                dgvTag.ActiveRowScrollRegion.ScrollRowIntoView(newRow);
                #endregion
            }
            catch (Exception ex)
            {
                FireException("ExecTagInsert", ex);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// 태그 수정
        /// </summary>
        /// <param name="mapper">EMapper</param>
        #region private void ExecTagUpdate(EMFrame.dm.EMapper mapper)
        private void ExecTagUpdate(EMFrame.dm.EMapper mapper)
        {
            try
            {
                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),
                                                  new OracleParameter("2", OracleDbType.Varchar2),
                                                  new OracleParameter("3", OracleDbType.Varchar2),
                                                  new OracleParameter("4", OracleDbType.Int32),
                                                  new OracleParameter("5", OracleDbType.Varchar2),
                                                  new OracleParameter("6", OracleDbType.Varchar2),
                                                  new OracleParameter("7", OracleDbType.Varchar2),
                                                  new OracleParameter("8", OracleDbType.Varchar2)
                                              };

                parameters[0].Value = txtTagDesc.Text;
                string useYn = "";
                if (cbTagUseYn.Text == "예") useYn = "Y";
                else if (cbTagUseYn.Text == "아니오") useYn = "N";
                parameters[1].Value = useYn;
                parameters[2].Value = txtTagFtrIdn.Text;
                if (txtTagCoefficient.Text == "") parameters[3].Value = DBNull.Value;
                else parameters[3].Value = txtTagCoefficient.Text;
                parameters[4].Value = txtTagLocCode.Text;
                parameters[5].Value = txtTagLocCode2.Text;
                parameters[6].Value = txtTagHTagname.Text;
                parameters[7].Value = txtTagTagname.Text;

                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_IHTAGS_UPDATE, parameters);

                #region 내부 데이터 반영
                var objects = from TagItem item in _searchTagList
                              where item.TAGNAME == txtTagTagname.Text
                              select item;
                if (objects != null && objects.Count() > 0)
                {
                    TagItem item = objects.First();
                    item.DESCRIPTION = txtTagDesc.Text;
                    item.USE_YN = useYn;
                    item.COEFFICIENT = txtTagCoefficient.Text;
                    item.LOC_CODE = txtTagLocCode.Text;
                    item.LOC_CODE2 = txtTagLocCode2.Text;

                    if (dgvTag.Selected != null && dgvTag.Selected.Rows.Count == 1)
                    {
                        dgvTag.Selected.Rows[0].Cells[Database.Fields.TAG.TAG_DESCRIPTION].Value = txtTagDesc.Text;
                        dgvTag.Selected.Rows[0].Cells[Database.Fields.TAG.TAG_USE_YN].Value = useYn;
                        dgvTag.Selected.Rows[0].Cells[Database.Fields.TAG.TAG_COEFFICIENT].Value = txtTagCoefficient.Text;
                        dgvTag.Selected.Rows[0].Cells[Database.Fields.TAG.TAG_LOC_CODE].Value = txtTagLocCode.Text;
                        dgvTag.Selected.Rows[0].Cells[Database.Fields.TAG.TAG_LOC_CODE2].Value = txtTagLocCode2.Text;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                FireException("ExecTagUpdate", ex);
                throw ex;
            }
        }
        #endregion


        /// <summary>
        /// 지정 태그의 블록 매핑 DB Count
        /// </summary>
        /// <param name="mapper">EMapper</param>
        /// <param name="tagName">태그일련번호</param>
        /// <returns>태그 카운트</returns>
        #region private int ExecTagMappingCount(EMFrame.dm.EMapper mapper, string tagName)
        private int ExecTagMappingCount(EMFrame.dm.EMapper mapper, string tagName)
        {
            int retVal = 0;

            try
            {
                DataTable dt = new DataTable();
                dt = mapper.ExecuteScriptDataTable(string.Format(Database.Queries.TAGMAPPING_TAG_MAPPING_COUNT, tagName), null);

                if (dt != null && dt.Rows.Count > 0)
                {
                    string cnt = string.Format("{0}", dt.Rows[0]["CNT"]);
                    if (!int.TryParse(cnt, out retVal)) retVal = 0;
                }
            }
            catch (Exception ex)
            {
                FireException("ExecTagMappingCount", ex);
                throw ex;
            }
            return retVal;
        }
        #endregion

        /// <summary>
        /// 태그 삭제
        /// </summary>
        /// <param name="mapper">EMapper</param>
        #region private void ExecTagDelete(EMFrame.dm.EMapper mapper)
        private void ExecTagDelete(EMFrame.dm.EMapper mapper)
        {
            try
            {
                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2)
                                              };
                parameters[0].Value = txtTagTagname.Text;
                // IF_IHTAGS
                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_IHTAGS_DELETE, parameters);
                IDataParameter[] parameters1 = {
                                                  new OracleParameter("1", OracleDbType.Varchar2)
                                              };
                parameters1[0].Value = txtTagTagname.Text;
                // IF_TAG_GBN
                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_TAG_GBN_DELETE_ALL, parameters1);
                IDataParameter[] parameters2 = {
                                                  new OracleParameter("1", OracleDbType.Varchar2)
                                              };
                parameters2[0].Value = txtTagTagname.Text;
                // IF_TAG_CAL
                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_TAG_CAL_DELETE, parameters2);

                #region 내부 데이터 반영
                int idx = -1;
                for (int i = 0; i < dgvTag.Rows.Count; i++)
                {
                    string tagName = string.Format("{0}", dgvTag.Rows[i].Cells[Database.Fields.TAG.TAG_TAGNAME].Value);
                    if (tagName == txtTagTagname.Text)
                    {
                        idx = i;
                        break;
                    }
                }
                if (idx >= 0) dgvTag.Rows[idx].Delete(false);

                idx = -1;
                for (int i = 0; i < _searchTagList.Count; i++)
                {
                    if (_searchTagList[i].TAGNAME == txtTagTagname.Text)
                    {
                        idx = i;
                        break;
                    }
                }
                if (idx >= 0) _searchTagList.RemoveAt(idx);
                #endregion
            }
            catch (Exception ex)
            {
                FireException("ExecTagDelete", ex);
                throw ex;
            }
        }
        #endregion
        #endregion
        
        #region 태그 Button Event
        /// <summary>
        /// 태그 신규 등록
        /// </summary>
        #region private void btnTagNew_Click(object sender, EventArgs e)
        private void btnTagNew_Click(object sender, EventArgs e)
        {
            ClearForm(1);

            txtTagTagname.ReadOnly = false;
            txtTagDesc.ReadOnly = false;
            txtTagFtrIdn.ReadOnly = false;
            txtTagCoefficient.ReadOnly = false;
            txtTagHTagname.ReadOnly = false;
            txtTagLocCode.ReadOnly = true;
            txtTagLocCode2.ReadOnly = true;
            cbTagUseYn.Enabled = true;

            //txtTagLocCode.Text = cbBlock.SelectedValue.ToString();
            //txtTagLocCode2.Text = cbBlock.SelectedValue.ToString();

            txtTagTagname.Focus();
        }
        #endregion

        /// <summary>
        /// 태그 수정 (수정 가능토록 화면 설정
        /// </summary>
        #region private void btnTagEdit_Click(object sender, EventArgs e)
        private void btnTagEdit_Click(object sender, EventArgs e)
        {
            txtTagHTagname.ReadOnly = false;
            txtTagDesc.ReadOnly = false;
            txtTagFtrIdn.ReadOnly = false;
            txtTagCoefficient.ReadOnly = false;
            txtTagLocCode.ReadOnly = true;
            txtTagLocCode2.ReadOnly = true;
            cbTagUseYn.Enabled = true;

            txtTagHTagname.Focus();
        }
        #endregion

        /// <summary>
        /// 태그 저장
        /// </summary>
        #region private void btnTagSave_Click(object sender, EventArgs e)
        private void btnTagSave_Click(object sender, EventArgs e)
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                if (txtTagTagname.Text == "" || txtTagTagname.Text.Length <= 0)
                {
                    MessageBox.Show("태그일련번호는 필수 입력입니다.", "필수 입력", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTagTagname.Focus();
                    return;
                }
                if (txtTagDesc.Text == "" || txtTagDesc.Text.Length <= 0)
                {
                    MessageBox.Show("태그설명은 필수 입력입니다.", "필수 입력", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTagDesc.Focus();
                    return;
                }

                if (MessageBox.Show("저장하시겠습니까", "저장", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                #region Database 처리
                //mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
                mapper.BeginTransaction();

                int cnt = ExecTagCount(mapper, txtTagTagname.Text);

                if (cnt > 0) ExecTagUpdate(mapper);
                else ExecTagInsert(mapper);

                mapper.CommitTransaction();
                #endregion

                #region 내부 메모리 정보 수정 (_searchTagList)
                TagItem tagItem = _searchTagList.Find(item => item.TAGNAME == txtTagTagname.Text);
                if (tagItem == null)
                {
                    // 신규
                    tagItem = new TagItem();
                    tagItem.TAGNAME = txtTagTagname.Text;
                    tagItem.DESCRIPTION = txtTagDesc.Text;
                    tagItem.FTR_IDN = txtTagFtrIdn.Text;
                    tagItem.COEFFICIENT = txtTagCoefficient.Text;
                    tagItem.LOC_CODE = txtTagLocCode.Text;
                    tagItem.LOC_CODE2 = txtTagLocCode2.Text;
                    tagItem.HTAGNAME = txtTagHTagname.Text;
                    if (cbTagUseYn.Text == "예") tagItem.USE_YN = "Y";
                    else if (cbTagUseYn.Text == "아니오") tagItem.USE_YN = "N";
                    _searchTagList.Add(tagItem);
                }
                else
                {
                    // 수정
                    tagItem.HTAGNAME = txtTagHTagname.Text;
                    tagItem.DESCRIPTION = txtTagDesc.Text;
                    tagItem.FTR_IDN = txtTagFtrIdn.Text;
                    tagItem.COEFFICIENT = txtTagCoefficient.Text;
                    if (cbTagUseYn.Text == "예") tagItem.USE_YN = "Y";
                    else if (cbTagUseYn.Text == "아니오") tagItem.USE_YN = "N";
                }
                #endregion

                #region 블록 태그 정보 수정 (_tagList)
                tagItem = null;
                tagItem = _tagList.Find(item => item.TAGNAME == txtTagTagname.Text);
                if (tagItem == null)
                {
                    // 신규
                    tagItem = new TagItem();
                    tagItem.TAGNAME = txtTagTagname.Text;
                    tagItem.DESCRIPTION = txtTagDesc.Text;
                    tagItem.FTR_IDN = txtTagFtrIdn.Text;
                    tagItem.COEFFICIENT = txtTagCoefficient.Text;
                    tagItem.LOC_CODE = txtTagLocCode.Text;
                    tagItem.LOC_CODE2 = txtTagLocCode2.Text;
                    tagItem.HTAGNAME = txtTagHTagname.Text;
                    if (cbTagUseYn.Text == "예") tagItem.USE_YN = "Y";
                    else if (cbTagUseYn.Text == "아니오") tagItem.USE_YN = "N";
                    _tagList.Add(tagItem);
                }
                else
                {
                    // 수정
                    tagItem.DESCRIPTION = txtTagDesc.Text;
                    tagItem.FTR_IDN = txtTagFtrIdn.Text;
                    tagItem.COEFFICIENT = txtTagCoefficient.Text;
                    tagItem.HTAGNAME = txtHtagname.Text;
                    if (cbTagUseYn.Text == "예") tagItem.USE_YN = "Y";
                    else if (cbTagUseYn.Text == "아니오") tagItem.USE_YN = "N";
                }
                #endregion

                #region 블록 그리드 정보 수정
                for (int i = 0; i < dgvBlockTag.Rows.Count; i++)
                {
                    string tagName = string.Format("{0}", dgvBlockTag.Rows[i].Cells[Database.Fields.TAG.TAGNAME].Value);
                    if (tagName == txtTagTagname.Text)
                    {
                        dgvBlockTag.Rows[i].Cells[Database.Fields.TAG.DESC].Value = txtTagDesc.Text;
                        break;
                    }
                }
                #endregion

                MessageBox.Show("저장되었습니다.", "처리완료", MessageBoxButtons.OK, MessageBoxIcon.Information);

                #region 화면 수정불가 처리
                txtTagTagname.ReadOnly = true;
                txtTagHTagname.ReadOnly = true;
                txtTagDesc.ReadOnly = true;
                txtTagFtrIdn.ReadOnly = true;
                txtTagCoefficient.ReadOnly = true;
                txtTagLocCode.ReadOnly = true;
                txtTagLocCode2.ReadOnly = true;
                cbTagUseYn.Enabled = false;
                #endregion
            }
            catch (Exception ex)
            {
                if (mapper != null) mapper.RollbackTransaction();
                FireException("btnTagSave_Click", ex);
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }
        }
        #endregion

        /// <summary>
        /// 태그 삭제
        /// </summary>
        #region private void btnTagDel_Click(object sender, EventArgs e)
        private void btnTagDel_Click(object sender, EventArgs e)
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                if (txtTagTagname.Text == "" || txtTagTagname.Text.Length <= 0)
                {
                    MessageBox.Show("선택된 태그가 없습니다.", "태그 선택", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (MessageBox.Show("삭제하시겠습니까?\r\n태그구분등 모든 태그정보가 삭제됩니다.", "삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                //mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
                mapper.BeginTransaction();

                int cnt = ExecTagMappingCount(mapper, txtTagTagname.Text);
                if (cnt > 0)
                {
                    MessageBox.Show("블록과 매핑된 태그는 삭제가 불가능합니다.\r\n매핑 해제후 삭제하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                ExecTagDelete(mapper);

                mapper.CommitTransaction();
                ClearForm(1);

                MessageBox.Show("삭제되었습니다.", "처리완료", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                if (mapper != null) mapper.RollbackTransaction();
                FireException("btnTagDel_Click", ex);
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }
        }
        #endregion
        #endregion


        #region [DB] TAG Mapping
        /// <summary>
        /// Block vs Tag 매핑 DB 처리
        /// </summary>
        /// <param name="mapper">EMapper</param>
        #region private void ExecBlockTagMapping(EMFrame.dm.EMapper mapper)
        private int ExecBlockTagMapping(EMFrame.dm.EMapper mapper)
        {
            int retCnt = 0;
            try
            {
                Console.WriteLine("");
                Console.WriteLine("블록 vs 태그 매핑 처리 시작");

                var updateMapping = from TagItem item in _searchTagList
                                    where item.IsMapping == true
                                    select item;

                if (updateMapping != null && updateMapping.Count() > 0)
                {
                    retCnt = 1;
                    foreach (TagItem item in updateMapping)
                    {
                        IDataParameter[] parameters = {
                                                          new OracleParameter("1", OracleDbType.Varchar2),
                                                          new OracleParameter("2", OracleDbType.Varchar2),
                                                          new OracleParameter("3", OracleDbType.Varchar2),
                                                          new OracleParameter("4", OracleDbType.Varchar2)
                                                      };

                        parameters[0].Value = item.LOC_CODE;
                        parameters[1].Value = item.LOC_CODE2;
                        parameters[2].Value = item.AUTO_TAG_GBN;
                        parameters[3].Value = item.TAGNAME;
                        

                        mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_IHTAGS_LOC_CODE_UPDATE, parameters);

                        Console.WriteLine(string.Format("[{0}] B:{1} vs T:{2}", retCnt, item.LOC_CODE2, item.TAGNAME));

                        item.IsMapping = false;
                        ++retCnt;
                    }
                }
                Console.WriteLine("블록 vs 태그 매핑 처리 완료");
                Console.WriteLine("");
            }
            catch (Exception ex)
            {
                FireException("ExecBlockTagMapping", ex);
                throw ex;
            }
            return retCnt;
        }
        #endregion

        /// <summary>
        /// 태그 구분 변경 처리 (등록/삭제)
        /// </summary>
        #region private void ExecTagGbnMapping(EMFrame.dm.EMapper mapper)
        private int ExecTagGbnMapping(EMFrame.dm.EMapper mapper)
        {
            int retCnt = 0;
            try
            {
                Console.WriteLine("");
                Console.WriteLine("태그구분 처리 시작");

                var updateTagGbn = from TagItem item in _searchTagList
                                   where item.IsGbnChanged == true
                                   select item;

                if (updateTagGbn != null && updateTagGbn.Count() > 0)
                {
                    retCnt = 1;
                    foreach (TagItem item in updateTagGbn)
                    {
                        string[] taggbns = item.TAG_GBN.Split(',');

                        foreach (string taggbn in taggbns)
                        {
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.RT))
                            if (taggbn ==Database.Fields.TAG_GUBUN.RT)
                            {
                                if (item.RT) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.RT, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.RT);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.FRI))
                            if (taggbn == Database.Fields.TAG_GUBUN.FRI)
                            {
                                if (item.FRI) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRI, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRI);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.TT))
                            if (taggbn == Database.Fields.TAG_GUBUN.TT)
                            {
                                if (item.TT) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.TT, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.TT);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.YD_R))
                            if (taggbn == Database.Fields.TAG_GUBUN.YD_R)
                            {
                                if (item.YD_R) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.YD_R, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.YD_R);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.YD))
                            if (taggbn == Database.Fields.TAG_GUBUN.YD)
                            {
                                if (item.YD) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.YD, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.YD);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.TD))
                            if (taggbn == Database.Fields.TAG_GUBUN.TD)
                            {
                                if (item.TD) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.TD, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.TD);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.MNF))
                            if (taggbn == Database.Fields.TAG_GUBUN.MNF)
                            {
                                if (item.MNF) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.MNF, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.MNF);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.FRQ))
                            if (taggbn == Database.Fields.TAG_GUBUN.FRQ)
                            {
                                if (item.FRQ) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRQ, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRQ);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.FRQ_I))
                            if (taggbn == Database.Fields.TAG_GUBUN.FRQ_I)
                            {
                                if (item.FRQ_I) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRQ_I, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRQ_I);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.FRQ_O))
                            if (taggbn == Database.Fields.TAG_GUBUN.FRQ_O)
                            {
                                if (item.FRQ_O) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRQ_O, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.FRQ_O);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.SPL_D))
                            if (taggbn == Database.Fields.TAG_GUBUN.SPL_D)
                            {
                                if (item.SPL_D) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.SPL_D, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.SPL_D);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.SPL_I))
                            if (taggbn == Database.Fields.TAG_GUBUN.SPL_I)
                            {
                                if (item.SPL_I) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.SPL_I, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.SPL_I);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.SPL_O))
                            if (taggbn == Database.Fields.TAG_GUBUN.SPL_O)
                            {
                                if (item.SPL_O) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.SPL_O, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.SPL_O);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.LEI))
                            if (taggbn == Database.Fields.TAG_GUBUN.LEI)
                            {
                                if (item.LEI) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.LEI, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.LEI);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.PHI))
                            if (taggbn == Database.Fields.TAG_GUBUN.PHI)
                            {
                                if (item.PHI) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.PHI, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.PHI);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.CLI))
                            if (taggbn == Database.Fields.TAG_GUBUN.CLI)
                            {
                                if (item.CLI) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.CLI, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.CLI);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.TBI))
                            if (taggbn == Database.Fields.TAG_GUBUN.TBI)
                            {
                                if (item.TBI) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.TBI, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.TBI);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.PRI))
                            if (taggbn == Database.Fields.TAG_GUBUN.PRI)
                            {
                                if (item.PRI) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.PRI, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.PRI);
                            }
                            //if (item.ChangeGubun.Contains(Database.Fields.TAG_GUBUN.PMB))
                            if (taggbn == Database.Fields.TAG_GUBUN.PMB)
                            {
                                if (item.PMB) ExecTagGbnInsert(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.PMB, item.DESCRIPTION);
                                else ExecTagGbnDelete(mapper, item.TAGNAME, Database.Fields.TAG_GUBUN.PMB);
                            }

                            if (item.DUMMY_GBN != item.DUMMY_GBN_ORG)
                            {
                                // 가상태그 구분이 변경되었을 경우 처리
                                ExecDummyGbn(mapper, item.TAGNAME, item.DUMMY_GBN);
                                item.DUMMY_GBN_ORG = item.DUMMY_GBN;
                            }

                            item.IsGbnChanged = false;
                            item.ChangeGubun.Clear();
                            ++retCnt;
                        }
                    }
                }
                Console.WriteLine("태그구분 처리 완료");
            }
            catch (Exception ex)
            {
                FireException("ExecTagGbnMapping", ex);
                throw ex;
            }
            return retCnt;
        }
        #endregion

        /// <summary>
        /// 태그 구분 제거
        /// </summary>
        /// <param name="mapper">EMapper Instance</param>
        /// <param name="tagName">태그일련번호</param>
        /// <param name="tagGbn">업무구분</param>
        #region private void ExecTagGbnDelete(EMFrame.dm.EMapper mapper, string tagName, string tagGbn)
        private void ExecTagGbnDelete(EMFrame.dm.EMapper mapper, string tagName, string tagGbn)
        {
            try
            {
                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),
                                                  new OracleParameter("2", OracleDbType.Varchar2)
                                              };
                parameters[0].Value = tagName;
                parameters[1].Value = tagGbn;

                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_TAG_GBN_DELETE, parameters);

                Console.WriteLine(string.Format("[D] {0} / {1}", tagName, tagGbn));
            }
            catch (Exception ex)
            {
                FireException("ExecTagGbnDelete(mapper," + tagName + "," + tagGbn + ")", ex);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// 태그 구분 등록
        /// </summary>
        /// <param name="mapper">EMapper Instance</param>
        /// <param name="tagName">태그일련번호</param>
        /// <param name="tagGbn">업무구분</param>
        /// <param name="tagDesc">태그업무설명</param>
        #region private void ExecTagGbnInsert(EMFrame.dm.EMapper mapper, string tagName, string tagGbn, string tagDesc)
        private void ExecTagGbnInsert(EMFrame.dm.EMapper mapper, string tagName, string tagGbn, string tagDesc)
        {
            try
            {
                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),
                                                  new OracleParameter("2", OracleDbType.Varchar2),
                                                  new OracleParameter("3", OracleDbType.Varchar2),
                                                  new OracleParameter("4", OracleDbType.Varchar2),
                                                  new OracleParameter("5", OracleDbType.Varchar2),
                                                  new OracleParameter("6", OracleDbType.Varchar2),
                                                  new OracleParameter("7", OracleDbType.Varchar2),
                                                  new OracleParameter("8", OracleDbType.Varchar2),
                                                  new OracleParameter("9", OracleDbType.Varchar2)
                                              };
                parameters[0].Value = tagName;
                parameters[1].Value = tagGbn;
                parameters[2].Value = tagGbn;
                parameters[3].Value = tagDesc;
                parameters[4].Value = tagName;
                parameters[5].Value = tagGbn;
                parameters[6].Value = tagName;
                parameters[7].Value = tagGbn;
                parameters[8].Value = tagDesc;

                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_TAG_GBN_INSERT, parameters);

                Console.WriteLine(string.Format("[I] {0} / {1}", tagName, tagGbn));
            }
            catch (Exception ex)
            {
                FireException("ExecTagGbnInsert(mapper," + tagName + "," + tagGbn + "," + tagDesc + ")", ex);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// 가상태그 처리
        /// </summary>
        #region private void ExecDummyGbn(EMFrame.dm.EMapper mapper, string tagName, bool dummyGbn)
        private void ExecDummyGbn(EMFrame.dm.EMapper mapper, string tagName, bool dummyGbn)
        {
            try
            {
                IDataParameter[] parameters = {
                                                  new OracleParameter("1", OracleDbType.Varchar2),
                                                  new OracleParameter("2", OracleDbType.Varchar2)
                                              };
                parameters[0].Value = (dummyGbn == true ? "CAL" : "");
                parameters[1].Value = tagName; 

                mapper.ExecuteScript(Database.Queries.TAGMAPPING_IF_TAG_GBN_DUMMY_GBN, parameters);

                System.Diagnostics.Debug.WriteLine("ExecDummyGbn(mapper," + tagName + "," + dummyGbn.ToString() + ")");
            }
            catch (Exception ex)
            {
                FireException("ExecDummyGbn(mapper," + tagName + "," + dummyGbn.ToString() + ")", ex);
            }
        } 
        #endregion
        #endregion

        #region 블록 Button Event
        /// <summary>
        /// 블록 vs 태그 매핑 저장
        /// </summary>
        #region private void btnSave_Click(object sender, EventArgs e)
        private void btnSave_Click(object sender, EventArgs e)
        {
            EMFrame.dm.EMapper mapper = null;

            try
            {
                //mapper = new EMFrame.dm.EMapper(BlockApp.Program.CONNECTION_KEY);
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
                mapper.BeginTransaction();

                // 블록 vs 태그 매핑 처리
                int mapping = ExecBlockTagMapping(mapper);
                // 태그 구분 처리
                int gbn = ExecTagGbnMapping(mapper);
                // 태그 계산식 저장
                Save(mapper);

                mapper.CommitTransaction();

                string msg = string.Format("저장되었습니다.{0}(태그매핑:{1}건/태그구분:{2}건)", Environment.NewLine, mapper, gbn);
                MessageBox.Show(msg, "처리완료", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                if (mapper != null) mapper.RollbackTransaction();
                FireException("btnSave_Click", ex);
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }
        }
        #endregion

        /// <summary>
        /// 태그 계산식 변경 내용 저장
        /// </summary>
        /// <param name="mapper">EMapper</param>
        /// <param name="list">TagCalcItem List</param>
        /// <param name="tagGbn">태그 구분</param>
        /// <param name="isCopy">복사 여부(RT, MNF)</param>
        #region private void Save(EMFrame.dm.EMapper mapper, List<TagCalcItem> list, string tagGbn)
        private void Save(EMFrame.dm.EMapper mapper)
        {
            try
            {
                foreach(TagCalcItem item in _tagCalListAll)
                {
                    if (item.State == TagCalcItem.StateType.None) continue;

                    if (item.State == TagCalcItem.StateType.New || item.State == TagCalcItem.StateType.Mod)
                    {
                        IDataParameter[] parameters = {
                                                      new OracleParameter("1", OracleDbType.Varchar2),
                                                      new OracleParameter("2", OracleDbType.Varchar2),
                                                      new OracleParameter("3", OracleDbType.Varchar2)
                                                  };
                        parameters[0].Value = item.TAGNAME;
                        parameters[1].Value = item.CTAGNAME;
                        parameters[2].Value = item.CAL_GBN;

                        mapper.ExecuteScript(Database.Queries.TAGCALC_MERGE_INS_UPD, parameters);
                    }
                    else if (item.State == TagCalcItem.StateType.Del)
                    {
                        IDataParameter[] parameters = {
                                                          new OracleParameter("1", OracleDbType.Varchar2),
                                                          new OracleParameter("2", OracleDbType.Varchar2)
                                                      };
                        parameters[0].Value = item.TAGNAME;
                        parameters[1].Value = item.CTAGNAME;

                        mapper.ExecuteScript(Database.Queries.TAGCALC_DELETE, parameters);
                    }
                }
            }
            catch (Exception ex)
            {
                FireException("Save", ex);
                MessageBox.Show(ex.Message);
            }
        }
        #endregion



        #endregion





        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvBlockTag_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            dgvBlockTag.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;

            dgvBlockTag.DisplayLayout.UseFixedHeaders = true;

            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LOC_NAME].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LOC_NAME_ORG].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LOC_NAME_0].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LOC_NAME_1].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LOC_NAME_2].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LOC_NAME_3].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.TYPE].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LOC_CODE].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.CM_LOC_GBN].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.BLOCK.LEVEL].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.TAG.DESC].Header.Fixed = true;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.TAG.TAGNAME].Header.Fixed = true;

            dgvBlockTag.DisplayLayout.Override.FixedHeaderAppearance.BackColor = Color.LightYellow;
            dgvBlockTag.DisplayLayout.Override.FixedHeaderAppearance.ForeColor = Color.Blue;
            dgvBlockTag.DisplayLayout.Override.FixedCellAppearance.BackColor = Color.LightYellow;
            dgvBlockTag.DisplayLayout.Override.FixedCellAppearance.ForeColor = Color.Blue;

            // Set the color of the line that seperates the fixed cells from the non-fixed cells.
            //
            dgvBlockTag.DisplayLayout.Override.FixedCellSeparatorColor = Color.Blue;

            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.TAG.AUTO_TAG_GBN].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            dgvBlockTag.DisplayLayout.Bands[0].Columns[Database.Fields.TAG.AUTO_TAG_GBN].ValueList = this.GetAutoTagValueList();
           
        }

        /// <summary>
        /// CELL 내용 변경
        /// </summary>
        private void dgvBlockTag_CellListSelect(object sender, CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == Database.Fields.TAG.AUTO_TAG_GBN)
                {

                    // TAG 변경
                    object AutoTags = ((ValueList)e.Cell.Column.ValueList).ValueListItems[e.Cell.Column.ValueList.SelectedItemIndex].Tag;
                    ValueList AutoTagList = (ValueList)AutoTags;
                    string tagName = string.Format("{0}", dgvBlockTag.Rows[e.Cell.Row.Index].Cells[Database.Fields.TAG.TAGNAME].Value);

                    dgvBlockTagCheckAutoTag(AutoTagList, dgvBlockTag.ActiveCell.Row.Index, tagName);


                }
            }
            catch(Exception ex)
            {
                FireException("dgbBlockTag_CellChange", ex);
                MessageBox.Show(ex.Message);
            }


        }

        /// <summary>
        /// TAG CHECK 박스 변경
        /// </summary>
        private void dgvBlockTagCheckAutoTag(ValueList checkList, int rowCnt, string tagName)
        {
            try
            {
                ValueListItemsCollection TagItem = checkList.ValueListItems;
                if (TagItem.Count == 0)
                {
                    for (int i = 15; i <= 33; i++)
                        dgvBlockTag.Rows[rowCnt].Cells[i].Value = false;

                    return;
                }

                // TAG 룰 체크
                foreach (ValueListItem tagGbn in TagItem)
                {
                    string taggbn = tagGbn.ToString();
                    // TD 룰 : 하나의 블록의 하나의 TD 태그만 사용해야 함
                    // MNF 룰 : 하나의 블록의 하나의 MNF 태그만 사용해야 함
                    // FRQ 룰: 하나의 블록의 하나의 FRQ 태그만 사용해야 함              

                    if (taggbn == "TD" || taggbn == "MNF" || taggbn == "FRQ")
                    {
                        string loc_code = dgvBlockTag.Rows[rowCnt].Cells["LOC_CODE"].Value.ToString();
                        string tag_name = dgvBlockTag.Rows[rowCnt].Cells["TAGNAME"].Value.ToString();

                        int cnt = GetDuplTagCount(tag_name, taggbn, loc_code);

                        if (cnt > 0)
                        {
                            MessageBox.Show(string.Format("하나의 블록에 {0}태그는 하나만 설정할 수 있습니다.", taggbn));
                            dgvBlockTag.Rows[rowCnt].Cells[14].Column.ValueList.SelectedItemIndex= 0;

                            for(int i = 15; i <= 33; i++)
                            {
                                dgvBlockTag.Rows[rowCnt].Cells[i].Value = false;
                            }
                            return;
                        }
                    }
                }


                // TAG 구분 업데이트
                TagItem tagItem = _searchTagList.Find(item => item.TAGNAME == tagName);
                string tag_gbn = string.Empty;

                // FRQ_O 룰 : 대/중 블록에만 허용, 소블록 생략
                // FRQ_I 룰 : 대/중 블록에만 허용, 소블록 생략
                // SPL_I 룰 : 대/중 블록에만 허용, 소블록 생략
                // SPL_D 룰 : 대/중 블록에만 허용, 소블록 생략

                // 해당 태그 블록이 소블록이 확인(block_level = 3)
                string loc_code1 = dgvBlockTag.Rows[rowCnt].Cells["LOC_CODE"].Value.ToString();
                var BlockLevel = from BlockItem item in _blockList
                                 where item.LOC_CODE == loc_code1
                                 select item.LEVEL;

                foreach (ValueListItem tag in TagItem)
                {

                    switch (tag.ToString())
                    {
                        case "RT":
                            dgvBlockTag.Rows[rowCnt].Cells[15].Value = true;
                            tag_gbn = tag_gbn + ",RT";
                            break;
                        case "FRI":
                            dgvBlockTag.Rows[rowCnt].Cells[16].Value = true;
                            tag_gbn = tag_gbn + ",FRI";
                            break;
                        case "TT":
                            dgvBlockTag.Rows[rowCnt].Cells[17].Value = true;
                            tag_gbn = tag_gbn + ",TT";
                            break;
                        case "YD":
                            dgvBlockTag.Rows[rowCnt].Cells[18].Value = true;
                            tag_gbn = tag_gbn + ",YD";
                            break;
                        case "YD_R":
                            dgvBlockTag.Rows[rowCnt].Cells[19].Value = true;
                            tag_gbn = tag_gbn + ",YD_R";
                            break;
                        case "TD":
                            dgvBlockTag.Rows[rowCnt].Cells[20].Value = true;
                            tag_gbn = tag_gbn + ",TD";
                            break;
                        case "MNF":
                            dgvBlockTag.Rows[rowCnt].Cells[21].Value = true;
                            tag_gbn = tag_gbn + ",MNF";
                            break;
                        case "FRQ":
                            dgvBlockTag.Rows[rowCnt].Cells[22].Value = true;
                            tag_gbn = tag_gbn + ",FRQ";
                            break;
                        case "FRQ_I":
                            if (BlockLevel.First() != 3)
                            {
                                dgvBlockTag.Rows[rowCnt].Cells[23].Value = true;
                                tag_gbn = tag_gbn + ",FRQ_I";
                            }
                            else
                            {
                                MessageBox.Show(string.Format("소블록에 {0}태그는 하나만 설정할 수 있습니다.", tag.ToString()));
                                dgvBlockTag.Rows[rowCnt].Cells[26].Value = false;
                            }
                            break;
                        case "FRQ_O":
                            if (BlockLevel.First() != 3)
                            {
                                dgvBlockTag.Rows[rowCnt].Cells[24].Value = true;
                                tag_gbn = tag_gbn + ",FRQ_O";
                            }
                            else
                            {
                                MessageBox.Show(string.Format("소블록에 {0}태그는 하나만 설정할 수 있습니다.", tag.ToString()));
                                dgvBlockTag.Rows[rowCnt].Cells[26].Value = false;
                            }
                            break;
                        case "SPL_D":
                            if (BlockLevel.First() != 3)
                            {
                                dgvBlockTag.Rows[rowCnt].Cells[25].Value = true;
                                tag_gbn = tag_gbn + ",SPL_D";
                            }
                            else
                            {
                                MessageBox.Show(string.Format("소블록에 {0}태그는 하나만 설정할 수 있습니다.", tag.ToString()));
                                dgvBlockTag.Rows[rowCnt].Cells[26].Value = false;
                            }

                            break;
                        case "SPL_I":
                            if (BlockLevel.First() != 3)
                            {
                                dgvBlockTag.Rows[rowCnt].Cells[26].Value = true;
                                tag_gbn = tag_gbn + ",SPL_I";
                            }
                            else
                            {
                                MessageBox.Show(string.Format("소블록에 {0}태그는 하나만 설정할 수 있습니다.", tag.ToString()));
                                dgvBlockTag.Rows[rowCnt].Cells[26].Value = false;
                            }
                            break;
                        case "SPL_O":
                            dgvBlockTag.Rows[rowCnt].Cells[27].Value = true;
                            tag_gbn = tag_gbn + ",SPL_O";
                            break;
                        case "LEI":
                            dgvBlockTag.Rows[rowCnt].Cells[28].Value = true;
                            tag_gbn = tag_gbn + ",LEI";
                            break;
                        case "PHI":
                            dgvBlockTag.Rows[rowCnt].Cells[29].Value = true;
                            tag_gbn = tag_gbn + ",PHI";
                            break;
                        case "CLI":
                            dgvBlockTag.Rows[rowCnt].Cells[30].Value = true;
                            tag_gbn = tag_gbn + ",CLI";
                            break;
                        case "TBI":
                            dgvBlockTag.Rows[rowCnt].Cells[31].Value = true;
                            tag_gbn = tag_gbn + ",TBI";
                            break;
                        case "PRI":
                            dgvBlockTag.Rows[rowCnt].Cells[32].Value = true;
                            tag_gbn = tag_gbn + ",PRI";
                            break;
                        case "PMB":
                            dgvBlockTag.Rows[rowCnt].Cells[33].Value = true;
                            tag_gbn = tag_gbn + ",PMB";
                            break;
                    }
                }

                if (tag_gbn != string.Empty && tag_gbn.Substring(0, 1) == ",")
                    tag_gbn = tag_gbn.Remove(0, 1);

                int findIdx = _searchTagList.FindIndex(item => item.TAGNAME == tagName);
                if (findIdx >= 0)
                {
                    _searchTagList[findIdx].TAG_GBN = tag_gbn;
                    _searchTagList[findIdx].IsMapping = true;
                    _searchTagList[findIdx].AUTO_TAG_GBN 
                        = dgvBlockTag.Rows[rowCnt].Cells[14].Column.ValueList.GetText(dgvBlockTag.Rows[rowCnt].Cells[14].Column.ValueList.SelectedItemIndex);
                }
            }
            catch (Exception ex)
            {
                FireException("dgbBlockTag_CellChange", ex);
                MessageBox.Show(ex.Message);
            }

        }

        /// <summary>
        /// GRID에서 해당 태그 갯수
        /// </summary>
        private int GetDuplTagCount(string tagname, string taggbn, string loccode)
        {
            int DuplCnt = 0;
            foreach(UltraGridRow row in dgvBlockTag.Rows)
            {
                if (row.Cells["TAGNAME"].Value == tagname)
                    continue;

                if (row.Cells["LOC_CODE"].Value.ToString() == loccode && (bool)row.Cells[taggbn].Value == true)
                    DuplCnt++;
            }

            return DuplCnt;
        }

        private void tabTagCal_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {

        }


        private void btnDummyTagDel_Click(object sender, EventArgs e)
        {
            string tagname = txtTagname.Text;
            string ctagname = string.Empty;
            SelectedRowsCollection rows = dgvCalcTag.Selected.Rows;
            foreach (UltraGridRow row in rows)
            {
                ctagname = row.Cells[2].Value.ToString();
                
                foreach(TagCalcItem item in _tagCalListAll)
                {
                    if (item.TAGNAME == tagname && item.CTAGNAME == ctagname)
                    {
                        item.State = TagCalcItem.StateType.Del;
                    }
                }
            }

            dgvCalcTag.DeleteSelectedRows(false);
        }

        private void cbBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearchTag_Click(null, null);
        }
    }
}
