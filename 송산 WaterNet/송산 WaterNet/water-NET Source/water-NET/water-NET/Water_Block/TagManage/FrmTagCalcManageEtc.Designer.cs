﻿namespace WaterNet.BlockApp.TagManage
{
    partial class FrmTagCalcManageEtc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn1 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LV");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn2 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn3 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn4 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn5 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn6 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn7 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_CODE");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn8 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRI");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn9 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn10 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn11 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("YD_R");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn12 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn13 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("FRQ_O");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn14 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_D");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn15 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_I");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn16 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("SPL_O");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn52 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAGNAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn53 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CAL_GBN", -1, 7927098);
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn54 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CTAGNAME");
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn55 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DESCRIPTION");
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn56 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("COEFFICIENT", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, false);
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueList valueList1 = new Infragistics.Win.ValueList(7927098);
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn17 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn18 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CAL_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn19 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CTAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn20 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn21 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand3 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_1");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_2");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_3");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DESCRIPTION");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAGNAME");
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("COEFFICIENT");
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_GBN");
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn22 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn23 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn24 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn25 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("LOC_NAME_3");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn26 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DESCRIPTION");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn27 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAGNAME");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn28 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("COEFFICIENT");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn29 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("DUMMY_GBN");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn30 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("TAG_GBN");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn31 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LV");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn32 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_0");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_1");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_2");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME_3", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, false);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn37 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn43 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRI");
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn44 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TT");
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn45 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("YD");
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn46 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("YD_R");
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn47 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRQ_I");
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn48 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FRQ_O");
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn49 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPL_D");
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn50 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPL_I");
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn51 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPL_O");
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.ultraDataSource2 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnShowTag = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvCalc = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraDataSource3 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblTagDesc = new System.Windows.Forms.Label();
            this.lblLocCode = new System.Windows.Forms.Label();
            this.lblTagGbn = new System.Windows.Forms.Label();
            this.lblDummyGbn = new System.Windows.Forms.Label();
            this.lblTagname = new System.Windows.Forms.Label();
            this.lblTagSearch = new System.Windows.Forms.Label();
            this.dgvTag = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraDataSource1 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel6 = new System.Windows.Forms.Panel();
            this.cbSearchGbn = new System.Windows.Forms.ComboBox();
            this.btnSearchTag = new System.Windows.Forms.Button();
            this.txtSearchTagDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSearchTagname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvBlockTag = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource3)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlockTag)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1454, 5);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1449, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(5, 759);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 759);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1449, 5);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(5, 754);
            this.panel4.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(5, 5);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblTagSearch);
            this.splitContainer1.Panel2.Controls.Add(this.dgvTag);
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Size = new System.Drawing.Size(1444, 724);
            this.splitContainer1.SplitterDistance = 819;
            this.splitContainer1.TabIndex = 4;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgvBlockTag);
            this.splitContainer2.Panel1.Controls.Add(this.panel5);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvCalc);
            this.splitContainer2.Panel2.Controls.Add(this.panel7);
            this.splitContainer2.Size = new System.Drawing.Size(819, 724);
            this.splitContainer2.SplitterDistance = 487;
            this.splitContainer2.TabIndex = 0;
            // 
            // ultraDataSource2
            // 
            this.ultraDataSource2.Band.Columns.AddRange(new object[] {
            ultraDataColumn1,
            ultraDataColumn2,
            ultraDataColumn3,
            ultraDataColumn4,
            ultraDataColumn5,
            ultraDataColumn6,
            ultraDataColumn7,
            ultraDataColumn8,
            ultraDataColumn9,
            ultraDataColumn10,
            ultraDataColumn11,
            ultraDataColumn12,
            ultraDataColumn13,
            ultraDataColumn14,
            ultraDataColumn15,
            ultraDataColumn16});
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnShowTag);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(819, 29);
            this.panel5.TabIndex = 0;
            // 
            // btnShowTag
            // 
            this.btnShowTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowTag.Font = new System.Drawing.Font("굴림", 8F);
            this.btnShowTag.Location = new System.Drawing.Point(796, 3);
            this.btnShowTag.Name = "btnShowTag";
            this.btnShowTag.Size = new System.Drawing.Size(23, 23);
            this.btnShowTag.TabIndex = 3;
            this.btnShowTag.Text = "▶";
            this.btnShowTag.UseVisualStyleBackColor = true;
            this.btnShowTag.Click += new System.EventHandler(this.btnShowTag_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(819, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "블록 태그 정보";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvCalc
            // 
            this.dgvCalc.AllowDrop = true;
            this.dgvCalc.DataSource = this.ultraDataSource3;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgvCalc.DisplayLayout.Appearance = appearance29;
            ultraGridColumn52.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn52.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn52.Header.VisiblePosition = 0;
            ultraGridColumn52.Hidden = true;
            appearance71.TextVAlignAsString = "Middle";
            ultraGridColumn53.CellAppearance = appearance71;
            ultraGridColumn53.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            ultraGridColumn53.Header.Caption = "기호";
            ultraGridColumn53.Header.VisiblePosition = 2;
            ultraGridColumn53.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn53.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn53.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(56, 20);
            ultraGridColumn53.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn53.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn53.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            ultraGridColumn54.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance72.TextVAlignAsString = "Middle";
            ultraGridColumn54.CellAppearance = appearance72;
            ultraGridColumn54.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn54.Header.Caption = "태그일련번호";
            ultraGridColumn54.Header.VisiblePosition = 1;
            ultraGridColumn54.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn54.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn54.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn54.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn54.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn55.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance73.TextVAlignAsString = "Middle";
            ultraGridColumn55.CellAppearance = appearance73;
            ultraGridColumn55.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn55.Header.Caption = "태그설명";
            ultraGridColumn55.Header.VisiblePosition = 3;
            ultraGridColumn55.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn55.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn55.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(280, 20);
            ultraGridColumn55.RowLayoutColumnInfo.SpanX = 5;
            ultraGridColumn55.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn56.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance74.TextVAlignAsString = "Middle";
            ultraGridColumn56.CellAppearance = appearance74;
            ultraGridColumn56.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn56.Header.Caption = "계수";
            ultraGridColumn56.Header.VisiblePosition = 4;
            ultraGridColumn56.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn56.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn56.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn56.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn56.RowLayoutColumnInfo.SpanY = 2;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn52,
            ultraGridColumn53,
            ultraGridColumn54,
            ultraGridColumn55,
            ultraGridColumn56});
            ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            this.dgvCalc.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.dgvCalc.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvCalc.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvCalc.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvCalc.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.dgvCalc.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvCalc.DisplayLayout.GroupByBox.Hidden = true;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance32.BackColor2 = System.Drawing.SystemColors.Control;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvCalc.DisplayLayout.GroupByBox.PromptAppearance = appearance32;
            this.dgvCalc.DisplayLayout.MaxColScrollRegions = 1;
            this.dgvCalc.DisplayLayout.MaxRowScrollRegions = 1;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgvCalc.DisplayLayout.Override.ActiveCellAppearance = appearance33;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvCalc.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.dgvCalc.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.dgvCalc.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            this.dgvCalc.DisplayLayout.Override.CardAreaAppearance = appearance35;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            appearance36.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.dgvCalc.DisplayLayout.Override.CellAppearance = appearance36;
            this.dgvCalc.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.dgvCalc.DisplayLayout.Override.CellPadding = 0;
            appearance37.BackColor = System.Drawing.SystemColors.Control;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvCalc.DisplayLayout.Override.GroupByRowAppearance = appearance37;
            appearance38.TextHAlignAsString = "Left";
            this.dgvCalc.DisplayLayout.Override.HeaderAppearance = appearance38;
            this.dgvCalc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.dgvCalc.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            this.dgvCalc.DisplayLayout.Override.RowAppearance = appearance39;
            this.dgvCalc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.dgvCalc.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvCalc.DisplayLayout.Override.TemplateAddRowAppearance = appearance40;
            this.dgvCalc.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.dgvCalc.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            valueList1.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            valueListItem1.DataValue = "";
            valueListItem2.DataValue = "P";
            valueListItem2.DisplayText = "+";
            valueListItem3.DataValue = "M";
            valueListItem3.DisplayText = "-";
            valueList1.ValueListItems.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.dgvCalc.DisplayLayout.ValueLists.AddRange(new Infragistics.Win.ValueList[] {
            valueList1});
            this.dgvCalc.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.dgvCalc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCalc.Location = new System.Drawing.Point(0, 29);
            this.dgvCalc.Name = "dgvCalc";
            this.dgvCalc.Size = new System.Drawing.Size(819, 204);
            this.dgvCalc.TabIndex = 3;
            this.dgvCalc.Text = "ultraGrid1";
            this.dgvCalc.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvCalc_DragDrop);
            this.dgvCalc.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvCalc_DragOver);
            this.dgvCalc.SelectionDrag += new System.ComponentModel.CancelEventHandler(this.dgvCalc_SelectionDrag);
            this.dgvCalc.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.dgvCalc_CellChange);
            // 
            // ultraDataSource3
            // 
            this.ultraDataSource3.Band.Columns.AddRange(new object[] {
            ultraDataColumn17,
            ultraDataColumn18,
            ultraDataColumn19,
            ultraDataColumn20,
            ultraDataColumn21});
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.lblTagDesc);
            this.panel7.Controls.Add(this.lblLocCode);
            this.panel7.Controls.Add(this.lblTagGbn);
            this.panel7.Controls.Add(this.lblDummyGbn);
            this.panel7.Controls.Add(this.lblTagname);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(819, 29);
            this.panel7.TabIndex = 1;
            // 
            // lblTagDesc
            // 
            this.lblTagDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTagDesc.Location = new System.Drawing.Point(112, 0);
            this.lblTagDesc.Name = "lblTagDesc";
            this.lblTagDesc.Size = new System.Drawing.Size(514, 29);
            this.lblTagDesc.TabIndex = 3;
            this.lblTagDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocCode
            // 
            this.lblLocCode.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblLocCode.Location = new System.Drawing.Point(626, 0);
            this.lblLocCode.Name = "lblLocCode";
            this.lblLocCode.Size = new System.Drawing.Size(86, 29);
            this.lblLocCode.TabIndex = 6;
            this.lblLocCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLocCode.Visible = false;
            // 
            // lblTagGbn
            // 
            this.lblTagGbn.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblTagGbn.Location = new System.Drawing.Point(712, 0);
            this.lblTagGbn.Name = "lblTagGbn";
            this.lblTagGbn.Size = new System.Drawing.Size(47, 29);
            this.lblTagGbn.TabIndex = 5;
            this.lblTagGbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTagGbn.Visible = false;
            // 
            // lblDummyGbn
            // 
            this.lblDummyGbn.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblDummyGbn.Location = new System.Drawing.Point(759, 0);
            this.lblDummyGbn.Name = "lblDummyGbn";
            this.lblDummyGbn.Size = new System.Drawing.Size(60, 29);
            this.lblDummyGbn.TabIndex = 4;
            this.lblDummyGbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDummyGbn.Visible = false;
            // 
            // lblTagname
            // 
            this.lblTagname.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTagname.Location = new System.Drawing.Point(0, 0);
            this.lblTagname.Name = "lblTagname";
            this.lblTagname.Size = new System.Drawing.Size(112, 29);
            this.lblTagname.TabIndex = 2;
            this.lblTagname.Text = "태그 계산식";
            this.lblTagname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTagSearch
            // 
            this.lblTagSearch.BackColor = System.Drawing.Color.White;
            this.lblTagSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTagSearch.Location = new System.Drawing.Point(226, 332);
            this.lblTagSearch.Name = "lblTagSearch";
            this.lblTagSearch.Size = new System.Drawing.Size(208, 36);
            this.lblTagSearch.TabIndex = 6;
            this.lblTagSearch.Text = "조회중입니다...";
            this.lblTagSearch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvTag
            // 
            this.dgvTag.AllowDrop = true;
            this.dgvTag.DataSource = this.ultraDataSource1;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgvTag.DisplayLayout.Appearance = appearance1;
            ultraGridColumn1.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn1.Header.Caption = "구분";
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.LabelOnly;
            ultraGridColumn1.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn1.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn1.RowLayoutColumnInfo.SpanX = 6;
            ultraGridColumn1.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn2.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance3.TextVAlignAsString = "Middle";
            ultraGridColumn2.CellAppearance = appearance3;
            ultraGridColumn2.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn2.Header.Caption = "대";
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.MergedCellContentArea = Infragistics.Win.UltraWinGrid.MergedCellContentArea.VirtualRect;
            ultraGridColumn2.MergedCellEvaluationType = Infragistics.Win.UltraWinGrid.MergedCellEvaluationType.MergeSameText;
            ultraGridColumn2.MergedCellStyle = Infragistics.Win.UltraWinGrid.MergedCellStyle.Always;
            ultraGridColumn2.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn2.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn2.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 22);
            ultraGridColumn2.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn2.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn3.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance8.TextVAlignAsString = "Middle";
            ultraGridColumn3.CellAppearance = appearance8;
            ultraGridColumn3.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn3.Header.Caption = "중";
            ultraGridColumn3.Header.VisiblePosition = 2;
            ultraGridColumn3.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn3.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn3.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 22);
            ultraGridColumn3.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn3.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn4.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance16.TextVAlignAsString = "Middle";
            ultraGridColumn4.CellAppearance = appearance16;
            ultraGridColumn4.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn4.Header.Caption = "소";
            ultraGridColumn4.Header.VisiblePosition = 3;
            ultraGridColumn4.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn4.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn4.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(98, 20);
            ultraGridColumn4.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn4.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn5.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance17.TextVAlignAsString = "Middle";
            ultraGridColumn5.CellAppearance = appearance17;
            ultraGridColumn5.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn5.Header.Caption = "태그설명";
            ultraGridColumn5.Header.VisiblePosition = 4;
            ultraGridColumn5.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn5.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn5.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(224, 22);
            ultraGridColumn5.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn5.RowLayoutColumnInfo.SpanX = 4;
            ultraGridColumn5.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn6.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance18.TextVAlignAsString = "Middle";
            ultraGridColumn6.CellAppearance = appearance18;
            ultraGridColumn6.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn6.Header.Caption = "태그일련번호";
            ultraGridColumn6.Header.VisiblePosition = 5;
            ultraGridColumn6.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn6.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn6.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 22);
            ultraGridColumn6.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn6.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn6.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn7.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            ultraGridColumn7.CellAppearance = appearance19;
            ultraGridColumn7.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn7.Header.Caption = "계수";
            ultraGridColumn7.Header.VisiblePosition = 6;
            ultraGridColumn7.Hidden = true;
            ultraGridColumn7.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn7.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn7.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn7.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn7.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn8.Header.Caption = "가상태그구분";
            ultraGridColumn8.Header.VisiblePosition = 7;
            ultraGridColumn8.Hidden = true;
            ultraGridColumn9.Header.Caption = "태그구분";
            ultraGridColumn9.Header.VisiblePosition = 8;
            ultraGridColumn9.Hidden = true;
            ultraGridBand3.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn9});
            ultraGridBand3.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            this.dgvTag.DisplayLayout.BandsSerializer.Add(ultraGridBand3);
            this.dgvTag.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvTag.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvTag.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvTag.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.dgvTag.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvTag.DisplayLayout.GroupByBox.Hidden = true;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvTag.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.dgvTag.DisplayLayout.MaxColScrollRegions = 1;
            this.dgvTag.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgvTag.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance41.BackColor = System.Drawing.SystemColors.Highlight;
            appearance41.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvTag.DisplayLayout.Override.ActiveRowAppearance = appearance41;
            this.dgvTag.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.dgvTag.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            this.dgvTag.DisplayLayout.Override.CardAreaAppearance = appearance42;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.dgvTag.DisplayLayout.Override.CellAppearance = appearance43;
            this.dgvTag.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.dgvTag.DisplayLayout.Override.CellPadding = 0;
            appearance44.BackColor = System.Drawing.SystemColors.Control;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvTag.DisplayLayout.Override.GroupByRowAppearance = appearance44;
            appearance45.TextHAlignAsString = "Left";
            this.dgvTag.DisplayLayout.Override.HeaderAppearance = appearance45;
            this.dgvTag.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.dgvTag.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            this.dgvTag.DisplayLayout.Override.RowAppearance = appearance46;
            this.dgvTag.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.dgvTag.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            appearance47.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvTag.DisplayLayout.Override.TemplateAddRowAppearance = appearance47;
            this.dgvTag.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.dgvTag.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.dgvTag.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.dgvTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTag.Location = new System.Drawing.Point(0, 29);
            this.dgvTag.Name = "dgvTag";
            this.dgvTag.Size = new System.Drawing.Size(621, 695);
            this.dgvTag.TabIndex = 5;
            this.dgvTag.Text = "ultraGrid2";
            this.dgvTag.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvTag_DragDrop);
            this.dgvTag.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvTag_DragOver);
            this.dgvTag.SelectionDrag += new System.ComponentModel.CancelEventHandler(this.dgvTag_SelectionDrag);
            // 
            // ultraDataSource1
            // 
            this.ultraDataSource1.Band.Columns.AddRange(new object[] {
            ultraDataColumn22,
            ultraDataColumn23,
            ultraDataColumn24,
            ultraDataColumn25,
            ultraDataColumn26,
            ultraDataColumn27,
            ultraDataColumn28,
            ultraDataColumn29,
            ultraDataColumn30});
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.cbSearchGbn);
            this.panel6.Controls.Add(this.btnSearchTag);
            this.panel6.Controls.Add(this.txtSearchTagDesc);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.txtSearchTagname);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(621, 29);
            this.panel6.TabIndex = 0;
            // 
            // cbSearchGbn
            // 
            this.cbSearchGbn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSearchGbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSearchGbn.FormattingEnabled = true;
            this.cbSearchGbn.Items.AddRange(new object[] {
            "전체",
            "순시",
            "적산",
            "적산차"});
            this.cbSearchGbn.Location = new System.Drawing.Point(101, 5);
            this.cbSearchGbn.Name = "cbSearchGbn";
            this.cbSearchGbn.Size = new System.Drawing.Size(107, 20);
            this.cbSearchGbn.TabIndex = 8;
            // 
            // btnSearchTag
            // 
            this.btnSearchTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchTag.Location = new System.Drawing.Point(570, 3);
            this.btnSearchTag.Name = "btnSearchTag";
            this.btnSearchTag.Size = new System.Drawing.Size(52, 23);
            this.btnSearchTag.TabIndex = 7;
            this.btnSearchTag.Text = "검색";
            this.btnSearchTag.UseVisualStyleBackColor = true;
            this.btnSearchTag.Click += new System.EventHandler(this.btnSearchTag_Click);
            // 
            // txtSearchTagDesc
            // 
            this.txtSearchTagDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchTagDesc.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.txtSearchTagDesc.Location = new System.Drawing.Point(462, 4);
            this.txtSearchTagDesc.Name = "txtSearchTagDesc";
            this.txtSearchTagDesc.Size = new System.Drawing.Size(100, 21);
            this.txtSearchTagDesc.TabIndex = 6;
            this.txtSearchTagDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchTagDesc_KeyDown);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(398, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "태그설명 : ";
            // 
            // txtSearchTagname
            // 
            this.txtSearchTagname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchTagname.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtSearchTagname.Location = new System.Drawing.Point(309, 4);
            this.txtSearchTagname.Name = "txtSearchTagname";
            this.txtSearchTagname.Size = new System.Drawing.Size(80, 21);
            this.txtSearchTagname.TabIndex = 4;
            this.txtSearchTagname.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchTagname_KeyUp);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(220, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "태그일련번호 :";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(621, 29);
            this.label2.TabIndex = 2;
            this.label2.Text = "블록 태그 정보";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btnSave);
            this.panel9.Controls.Add(this.btnClose);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(5, 729);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1444, 30);
            this.panel9.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(1282, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1363, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvBlockTag
            // 
            this.dgvBlockTag.AllowDrop = true;
            this.dgvBlockTag.DataSource = this.ultraDataSource2;
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgvBlockTag.DisplayLayout.Appearance = appearance2;
            ultraGridColumn31.Header.Caption = "LEVEL";
            ultraGridColumn31.Header.VisiblePosition = 6;
            ultraGridColumn31.Hidden = true;
            ultraGridColumn31.RowLayoutColumnInfo.OriginX = 50;
            ultraGridColumn31.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn31.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn31.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn32.Header.VisiblePosition = 0;
            ultraGridColumn32.Hidden = true;
            ultraGridColumn32.RowLayoutColumnInfo.OriginX = 52;
            ultraGridColumn32.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn32.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 48);
            ultraGridColumn32.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn32.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn33.Header.Caption = "구 분";
            ultraGridColumn33.Header.VisiblePosition = 1;
            ultraGridColumn33.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.LabelOnly;
            ultraGridColumn33.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn33.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn33.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(336, 0);
            ultraGridColumn33.RowLayoutColumnInfo.SpanX = 6;
            ultraGridColumn33.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn34.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn34.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn34.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn34.Header.Caption = "대";
            ultraGridColumn34.Header.VisiblePosition = 2;
            ultraGridColumn34.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn34.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn34.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn34.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn34.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn35.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn35.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn35.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn35.Header.Caption = "중";
            ultraGridColumn35.Header.VisiblePosition = 3;
            ultraGridColumn35.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn35.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn35.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn35.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn35.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn36.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
            ultraGridColumn36.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn36.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn36.Header.Caption = "소";
            ultraGridColumn36.Header.VisiblePosition = 4;
            ultraGridColumn36.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn36.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn36.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(0, 20);
            ultraGridColumn36.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn36.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn37.Header.VisiblePosition = 5;
            ultraGridColumn37.Hidden = true;
            ultraGridColumn37.RowLayoutColumnInfo.OriginX = 58;
            ultraGridColumn37.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn37.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn37.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn43.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance57.TextVAlignAsString = "Middle";
            ultraGridColumn43.CellAppearance = appearance57;
            ultraGridColumn43.Header.VisiblePosition = 7;
            ultraGridColumn43.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn43.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn43.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn43.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn43.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn44.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance58.TextVAlignAsString = "Middle";
            ultraGridColumn44.CellAppearance = appearance58;
            ultraGridColumn44.Header.VisiblePosition = 8;
            ultraGridColumn44.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn44.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn44.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn44.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn44.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn45.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance59.TextVAlignAsString = "Middle";
            ultraGridColumn45.CellAppearance = appearance59;
            ultraGridColumn45.Header.VisiblePosition = 9;
            ultraGridColumn45.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn45.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn45.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn45.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn45.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn46.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance60.TextVAlignAsString = "Middle";
            ultraGridColumn46.CellAppearance = appearance60;
            ultraGridColumn46.Header.VisiblePosition = 10;
            ultraGridColumn46.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn46.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn46.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn46.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn46.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn47.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance65.TextVAlignAsString = "Middle";
            ultraGridColumn47.CellAppearance = appearance65;
            ultraGridColumn47.Header.VisiblePosition = 11;
            ultraGridColumn47.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn47.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn47.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn47.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn47.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn48.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance66.TextVAlignAsString = "Middle";
            ultraGridColumn48.CellAppearance = appearance66;
            ultraGridColumn48.Header.VisiblePosition = 12;
            ultraGridColumn48.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn48.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn48.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn48.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn48.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn49.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance67.TextVAlignAsString = "Middle";
            ultraGridColumn49.CellAppearance = appearance67;
            ultraGridColumn49.Header.VisiblePosition = 13;
            ultraGridColumn49.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn49.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn49.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn49.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn49.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn50.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance68.TextVAlignAsString = "Middle";
            ultraGridColumn50.CellAppearance = appearance68;
            ultraGridColumn50.Header.VisiblePosition = 14;
            ultraGridColumn50.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn50.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn50.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn50.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn50.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn51.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            appearance69.TextVAlignAsString = "Middle";
            ultraGridColumn51.CellAppearance = appearance69;
            ultraGridColumn51.Header.VisiblePosition = 15;
            ultraGridColumn51.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn51.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn51.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn51.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn51.RowLayoutColumnInfo.SpanY = 4;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn31,
            ultraGridColumn32,
            ultraGridColumn33,
            ultraGridColumn34,
            ultraGridColumn35,
            ultraGridColumn36,
            ultraGridColumn37,
            ultraGridColumn43,
            ultraGridColumn44,
            ultraGridColumn45,
            ultraGridColumn46,
            ultraGridColumn47,
            ultraGridColumn48,
            ultraGridColumn49,
            ultraGridColumn50,
            ultraGridColumn51});
            appearance70.TextVAlignAsString = "Middle";
            ultraGridBand1.Override.CellAppearance = appearance70;
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            this.dgvBlockTag.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.dgvBlockTag.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvBlockTag.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvBlockTag.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvBlockTag.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.dgvBlockTag.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.dgvBlockTag.DisplayLayout.GroupByBox.Hidden = true;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dgvBlockTag.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.dgvBlockTag.DisplayLayout.MaxColScrollRegions = 1;
            this.dgvBlockTag.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgvBlockTag.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvBlockTag.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.dgvBlockTag.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.dgvBlockTag.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.dgvBlockTag.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.dgvBlockTag.DisplayLayout.Override.CellAppearance = appearance11;
            this.dgvBlockTag.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.dgvBlockTag.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.dgvBlockTag.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.dgvBlockTag.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.dgvBlockTag.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.dgvBlockTag.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.dgvBlockTag.DisplayLayout.Override.RowAppearance = appearance14;
            this.dgvBlockTag.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.dgvBlockTag.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            this.dgvBlockTag.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvBlockTag.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.dgvBlockTag.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.dgvBlockTag.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.dgvBlockTag.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.dgvBlockTag.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.dgvBlockTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBlockTag.Location = new System.Drawing.Point(0, 29);
            this.dgvBlockTag.Name = "dgvBlockTag";
            this.dgvBlockTag.Size = new System.Drawing.Size(819, 458);
            this.dgvBlockTag.TabIndex = 4;
            this.dgvBlockTag.Text = "ultraGrid1";
            this.dgvBlockTag.AfterCellActivate += new System.EventHandler(this.dgvBlockTag_AfterCellActivate);
            // 
            // FrmTagCalcManageEtc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1454, 764);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FrmTagCalcManageEtc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmTagCalcManage";
            this.Load += new System.EventHandler(this.FrmTagCalcManage_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource2)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource3)).EndInit();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlockTag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSearchTag;
        private System.Windows.Forms.TextBox txtSearchTagDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSearchTagname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbSearchGbn;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblTagname;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvTag;
        private System.Windows.Forms.Label lblTagSearch;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvCalc;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource3;
        private System.Windows.Forms.Button btnShowTag;
        private System.Windows.Forms.Label lblTagDesc;
        private System.Windows.Forms.Label lblDummyGbn;
        private System.Windows.Forms.Label lblTagGbn;
        private System.Windows.Forms.Label lblLocCode;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource2;
        private Infragistics.Win.UltraWinGrid.UltraGrid dgvBlockTag;
    }
}