﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp.TagManage
{
    /// <summary>
    /// 태그 관리에 사용되는 태그 정보 (태그 + 태그구분)
    /// </summary>
    public class TagItem
    {
        /// <summary>
        /// 태그일련번호
        /// </summary>
        public string TAGNAME { get; set; }
        /// <summary>
        /// 태그설명_내부태그
        /// </summary>
        public string DESCRIPTION { get; set; }
        /// <summary>
        /// 사용여부
        /// </summary>
        public string USE_YN { get; set; }
        /// <summary>
        /// FTR_IDN
        /// </summary>
        public string FTR_IDN { get; set; }
        /// <summary>
        /// 계수 (유량에 곱해지는 인자)
        /// </summary>
        public string COEFFICIENT { get; set; }
        /// <summary>
        /// 지역관리코드 (CM_LOCATION)
        /// </summary>
        public string LOC_CODE { get; set; }
        /// <summary>
        /// 지역관리코드2 (CM_LOCATION2)
        /// </summary>
        public string LOC_CODE2 { get; set; }
        /// <summary>
        /// 히스토리안태그
        /// </summary>
        public string HTAGNAME { get; set; }
        /// <summary>
        /// 태그구분 (','로 구분됨)
        /// </summary>
        public string TAG_GBN { get; set; }
        /// <summary>
        /// 자동태그구분
        /// </summary>
        public string AUTO_TAG_GBN { get; set; }
        /// <summary>
        /// LOC_CODE:1 / LOC_CODE2:2 (TG_GBN -> CM_LOC 변경)
        /// </summary>
        public string CM_LOC_GBN { get; set; }
        /// <summary>
        /// IF_TAG_GBN.DUMMY_GBN (더미태그 여부)
        /// </summary>
        public bool DUMMY_GBN { get; set; }
        /// <summary>
        /// DUMMY_GBN 원본값
        /// </summary>
        public bool DUMMY_GBN_ORG { get; set; }

        /// <summary>
        /// 블록 매핑시 사용
        /// </summary>
        public bool IsMapping { get; set; }
        /// <summary>
        /// 태그 구분 변경
        /// </summary>
        public bool IsGbnChanged { get; set; }
        /// <summary>
        /// 변경된 태그구분 목록
        /// </summary>
        public List<string> ChangeGubun = new List<string>();

        #region 태그구분 속성 (TAG_GBN을 각각 태그구분으로 풀어서 선언)
        public bool RT { get; set; }
        public bool FRI { get; set; }
        public bool TT { get; set; }
        public bool YD { get; set; }
        public bool YD_R { get; set; }
        public bool TD { get; set; }
        public bool MNF { get; set; }
        public bool FRQ { get; set; }
        public bool FRQ_I { get; set; }
        public bool FRQ_O { get; set; }
        public bool SPL_D { get; set; }
        public bool SPL_I { get; set; }
        public bool SPL_O { get; set; }
        public bool LEI { get; set; }
        public bool PHI { get; set; }
        public bool CLI { get; set; }
        public bool TBI { get; set; }
        public bool PRI { get; set; }
        public bool PMB { get; set; }
        #endregion



        #region public TagItem()
        public TagItem()
        {
            RT = false;
            FRI = false;
            TT = false;
            YD = false;
            YD_R = false;
            TD = false;
            MNF = false;
            FRQ = false;
            FRQ_I = false;
            FRQ_O = false;
            SPL_D = false;
            SPL_I = false;
            SPL_O = false;
            LEI = false;
            PHI = false;
            CLI = false;
            TBI = false;
            PRI = false;
            PMB = false;

            IsMapping = false;
            IsGbnChanged = false;
        } 
        #endregion

        #region public TagItem(string tagName, string desc, string useYn, string ftrIdn, string coefficent, string locCode, string locCode2, string tagGbn, string htagname)
        public TagItem(string tagName, string desc, string useYn, string ftrIdn, string coefficent, string locCode, string locCode2, string tagGbn, string htagname, string autoTagGbn)
            : this()
        {
            this.TAGNAME = tagName;
            this.DESCRIPTION = desc;
            this.USE_YN = useYn;
            this.FTR_IDN = ftrIdn;
            this.COEFFICIENT = coefficent;
            this.LOC_CODE = locCode;
            this.LOC_CODE2 = locCode2;
            this.HTAGNAME = htagname;
            this.TAG_GBN = tagGbn;
            this.AUTO_TAG_GBN = autoTagGbn;

            if (locCode == "" || locCode.Length <= 0) this.CM_LOC_GBN = "1";
            else this.CM_LOC_GBN = "2";

            if (TAG_GBN != "" && TAG_GBN.Length > 0)
            {
                string[] tags = TAG_GBN.Split(',');
                if (tags == null || tags.Length <= 0) return;

                for (int i = 0; i < tags.Length; i++)
                {
                    switch (tags[i])
                    {
                        case "RT": RT = true; break;
                        case "FRI": FRI = true; break;
                        case "TT": TT = true; break;
                        case "YD": YD = true; break;
                        case "YD_R": YD_R = true; break;
                        case "TD": TD = true; break;
                        case "MNF": MNF = true; break;
                        case "FRQ": FRQ = true; break;
                        case "FRQ_I": FRQ_I = true; break;
                        case "FRQ_O": FRQ_O = true; break;
                        case "SPL_D": SPL_D = true; break;
                        case "SPL_I": SPL_I = true; break;
                        case "SPL_O": SPL_O = true; break;
                        case "LEI": LEI = true; break;
                        case "PHI": PHI = true; break;
                        case "CLI": CLI = true; break;
                        case "TBI": TBI = true; break;
                        case "PRI": PRI = true; break;
                        case "PMB": PMB = true; break;
                    }
                }
            }

            this.IsMapping = false;
            this.IsGbnChanged = false;
        } 
        #endregion

        /// <summary>
        /// 태그구분 변경시 값 설정 (그리드에서 체크시 실제 값 처리)
        /// </summary>
        /// <param name="tagGbn">태그 구분명</param>
        /// <param name="val">true or false</param>
        #region public void SetTagGbn(string tagGbn, bool val)
        public void SetTagGbn(string tagGbn, bool val)
        {
            switch (tagGbn)
            {
                case "RT": RT = val; break;
                case "FRI": FRI = val; break;
                case "TT": TT = val; break;
                case "YD": YD = val; break;
                case "YD_R": YD_R = val; break;
                case "TD": TD = val; break;
                case "MNF": MNF = val; break;
                case "FRQ": FRQ = val; break;
                case "FRQ_I": FRQ_I = val; break;
                case "FRQ_O": FRQ_O = val; break;
                case "SPL_D": SPL_D = val; break;
                case "SPL_I": SPL_I = val; break;
                case "SPL_O": SPL_O = val; break;
                case "LEI": LEI = val; break;
                case "PHI": PHI = val; break;
                case "CLI": CLI = val; break;
                case "TBI": TBI = val; break;
                case "PRI": PRI = val; break;
                case "PMB": PMB = val; break;
            }
        } 
        #endregion


        #region public string ToTagGbn()
        public string ToTagGbn()
        {
            string retVal = "";

            if (RT) retVal += "RT/";
            if (FRI) retVal += "FRI/";
            if (TT) retVal += "TT/";
            if (YD) retVal += "YD/";
            if (YD_R) retVal += "YD_R/";
            if (TD) retVal += "TD/";
            if (MNF) retVal += "MNF/";
            if (FRQ) retVal += "FRQ/";
            if (FRQ_I) retVal += "FRQ_I/";
            if (FRQ_O) retVal += "FRQ_O/";
            if (SPL_D) retVal += "SPL_D/";
            if (SPL_I) retVal += "SQL_I/";
            if (SPL_O) retVal += "SPL_O/";
            if (LEI) retVal += "LEI/";
            if (PHI) retVal += "PHI/";
            if (CLI) retVal += "CLI/";
            if (TBI) retVal += "TBI/";
            if (PRI) retVal += "PRI/";
            if (PMB) retVal += "PMB/";

            if (retVal.Length > 3)
            {
                retVal = retVal.Substring(0, retVal.Length - 1);
            }

            return retVal;
        } 
        #endregion
    }
}
