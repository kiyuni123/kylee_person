﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.BlockApp.TagManage
{
    /// <summary>
    /// 가상태그를 위한 태그 아이템 (일반 태그도 수용 가능)
    /// </summary>
    public class BlockTagCalcItem
    {
        /// <summary>
        /// 레벨 (블록 계층 레벨)
        /// </summary>
        public int LEVEL { get; set; }
        public string LOC_CODE { get; set; }
        public string PLOC_CODE { get; set; }
        public string LOC_NAME { get; set; }
        public string LOC_NAME1 { get; set; }
        public string LOC_NAME2 { get; set; }
        public string LOC_NAME3 { get; set; }
        public string ORDERBY { get; set; }

        public string RT { get; set; }
        public string MNF { get; set; }
        public string TD { get; set; }
        public string FRQ { get; set; }

        /// <summary>
        /// RT 계산식 여부
        /// </summary>
        public bool RTCal { get; set; }
        /// <summary>
        /// MNF 계산식 여부
        /// </summary>
        public bool MNFCal { get; set; }
        /// <summary>
        /// TD 계산식 여부
        /// </summary>
        public bool TDCal { get; set; }
        /// <summary>
        /// FRQ 계산식 여부
        /// </summary>
        public bool FRQCal { get; set; }

        public List<TagCalcItem> RTList { get; set; }
        public List<TagCalcItem> MNFList { get; set; }
        public List<TagCalcItem> TDList { get; set; }
        public List<TagCalcItem> FRQList { get; set; }

        #region 순시, 적산, 적산차 이외
        public string FRI { get; set; }
        public string TT { get; set; }
        public string YD { get; set; }
        public string YD_R { get; set; }
        public string FRQ_I { get; set; }
        public string FRQ_O { get; set; }
        public string SPL_D { get; set; }
        public string SPL_I { get; set; }
        public string SPL_O { get; set; }

        /// <summary>
        /// FRI 계산식 여부
        /// </summary>
        public bool FRICal { get; set; }
        /// <summary>
        /// TT 계산식 여부
        /// </summary>
        public bool TTCal { get; set; }
        /// <summary>
        /// YD 계산식 여부
        /// </summary>
        public bool YDCal { get; set; }
        /// <summary>
        /// YD_R 계산식 여부
        /// </summary>
        public bool YD_RCal { get; set; }
        /// <summary>
        /// FRQ_I 계산식 여부
        /// </summary>
        public bool FRQ_ICal { get; set; }
        /// <summary>
        /// FRQ_O 계산식 여부
        /// </summary>
        public bool FRQ_OCal { get; set; }
        /// <summary>
        /// SPL_D 계산식 여부
        /// </summary>
        public bool SPL_DCal { get; set; }
        /// <summary>
        /// SPL_I 계산식 여부
        /// </summary>
        public bool SPL_ICal { get; set; }
        /// <summary>
        /// SPL_O 계산식 여부
        /// </summary>
        public bool SPL_OCal { get; set; }

        public List<TagCalcItem> FRIList { get; set; }
        public List<TagCalcItem> TTList { get; set; }
        public List<TagCalcItem> YDList { get; set; }
        public List<TagCalcItem> YD_RList { get; set; }
        public List<TagCalcItem> FRQ_IList { get; set; }
        public List<TagCalcItem> FRQ_OList { get; set; }
        public List<TagCalcItem> SPL_DList { get; set; }
        public List<TagCalcItem> SPL_IList { get; set; }
        public List<TagCalcItem> SPL_OList { get; set; }
        #endregion

        public bool IsChanged { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        #region public BlockTagCalcItem()
        public BlockTagCalcItem()
        {
            this.RT = "";
            this.MNF = "";
            this.TD = "";
            this.FRQ = "";

            this.RTCal = false;
            this.MNFCal = false;
            this.TDCal = false;
            this.FRQCal = false;

            this.RTList = new List<TagCalcItem>();
            this.MNFList = new List<TagCalcItem>();
            this.TDList = new List<TagCalcItem>();
            this.FRQList = new List<TagCalcItem>();

            #region 순시, 적산, 적산차 이외
            this.FRI = "";
            this.TT = "";
            this.YD = "";
            this.YD_R = "";
            this.FRQ_I = "";
            this.FRQ_O = "";
            this.SPL_D = "";
            this.SPL_I = "";
            this.SPL_O = "";

            this.FRICal = false;
            this.TTCal = false;
            this.YDCal = false;
            this.YD_RCal = false;
            this.FRQ_ICal = false;
            this.FRQ_OCal = false;
            this.SPL_DCal = false;
            this.SPL_ICal = false;
            this.SPL_OCal = false;

            this.FRIList = new List<TagCalcItem>();
            this.TTList = new List<TagCalcItem>();
            this.YDList = new List<TagCalcItem>();
            this.YD_RList = new List<TagCalcItem>();
            this.FRQ_IList = new List<TagCalcItem>();
            this.FRQ_OList = new List<TagCalcItem>();
            this.SPL_DList = new List<TagCalcItem>();
            this.SPL_IList = new List<TagCalcItem>();
            this.SPL_OList = new List<TagCalcItem>();
            #endregion

            this.IsChanged = false;
        }
        #endregion

        /// <summary>
        /// 해당 태그가 계산식인지의 여부
        /// </summary>
        /// <param name="gbn">태그구분</param>
        /// <returns>계산식 여부</returns>
        #region public bool GetCalc(string gbn)
        public bool GetCalc(string gbn)
        {
            bool retVal = false;
            switch (gbn.ToUpper())
            {
                case Database.Fields.TAG_GUBUN.RT:
                    retVal = this.RTCal;
                    break;
                case Database.Fields.TAG_GUBUN.MNF:
                    retVal = this.MNFCal;
                    break;
                case Database.Fields.TAG_GUBUN.TD:
                    retVal = this.TDCal;
                    break;
                case Database.Fields.TAG_GUBUN.TT:
                    retVal = this.TTCal;
                    break;
                case Database.Fields.TAG_GUBUN.FRQ:
                    retVal = this.FRQCal;
                    break;
                case Database.Fields.TAG_GUBUN.FRI:
                    retVal = this.FRICal;
                    break;
                case Database.Fields.TAG_GUBUN.YD:
                    retVal = this.YDCal;
                    break;
                case Database.Fields.TAG_GUBUN.YD_R:
                    retVal = this.YD_RCal;
                    break;
                case Database.Fields.TAG_GUBUN.FRQ_I:
                    retVal = this.FRQ_ICal;
                    break;
                case Database.Fields.TAG_GUBUN.FRQ_O:
                    retVal = this.FRQ_OCal;
                    break;
                case Database.Fields.TAG_GUBUN.SPL_D:
                    retVal = this.SPL_DCal;
                    break;
                case Database.Fields.TAG_GUBUN.SPL_I:
                    retVal = this.SPL_ICal;
                    break;
                case Database.Fields.TAG_GUBUN.SPL_O:
                    retVal = this.SPL_OCal;
                    break;
                default:
                    retVal = false;
                    break;
            }
            return retVal;
        } 
        #endregion

        /// <summary>
        /// 태그 계산식 리스트 반환
        /// </summary>
        /// <param name="gbn">태그구분</param>
        /// <returns>태그 계산식 리스트</returns>
        #region public List<TagCalcItem> GetItemList(string gbn)
        public List<TagCalcItem> GetItemList(string gbn)
        {
            List<TagCalcItem> retVal = null;
            switch (gbn.ToUpper())
            {
                case Database.Fields.TAG_GUBUN.RT:
                    retVal = this.RTList;
                    break;
                case Database.Fields.TAG_GUBUN.MNF:
                    retVal = this.MNFList;
                    break;
                case Database.Fields.TAG_GUBUN.TD:
                    retVal = this.TDList;
                    break;
                case Database.Fields.TAG_GUBUN.FRQ:
                    retVal = this.FRQList;
                    break;
                case Database.Fields.TAG_GUBUN.FRI:
                    retVal = this.FRIList;
                    break;
                case Database.Fields.TAG_GUBUN.YD:
                    retVal = this.YDList;
                    break;
                case Database.Fields.TAG_GUBUN.YD_R:
                    retVal = this.YD_RList;
                    break;
                case Database.Fields.TAG_GUBUN.FRQ_I:
                    retVal = this.FRQ_IList;
                    break;
                case Database.Fields.TAG_GUBUN.FRQ_O:
                    retVal = this.FRQ_OList;
                    break;
                case Database.Fields.TAG_GUBUN.SPL_D:
                    retVal = this.SPL_DList;
                    break;
                case Database.Fields.TAG_GUBUN.SPL_I:
                    retVal = this.SPL_IList;
                    break;
                case Database.Fields.TAG_GUBUN.SPL_O:
                    retVal = this.SPL_OList;
                    break;
                default:
                    retVal = new List<TagCalcItem>();
                    break;
            }
            return retVal;
        } 
        #endregion

        /// <summary>
        /// 태그구분이 복사 대상인지 체크
        /// </summary>
        /// <param name="gbn"></param>
        /// <returns></returns>
        #region public bool GetCopyYesNo(string gbn)
        public bool GetCopyYesNo(string gbn)
        {
            bool isCopy = false;
            try
            {
                //if (gbn == Database.Fields.TAG_GUBUN.RT)
                //{
                //    if (this.MNFList.Count <= 0) return true;
                //    for (int i = 0; i < this.MNFList.Count; i++)
                //    {
                //        // 현재 DB에 데이터가 있을 경우에만 true
                //        // 화면상에서 데이터 삭제하고 저장하지 않을 경우에는 false
                //        // 이 경우 삭제 처리 이후에는 true로 처리됨
                //        if (this.MNFList[i].State == TagCalcItem.StateType.None)
                //        {
                //            isCopy = true;
                //            break;
                //        }
                //    }
                //}
                //else if (gbn == Database.Fields.TAG_GUBUN.MNF)
                //{
                //    if (this.RTList.Count <= 0) return true;
                //    for (int i = 0; i < this.RTList.Count; i++)
                //    {
                //        // 현재 DB에 데이터가 있을 경우에만 true
                //        // 화면상에서 데이터 삭제하고 저장하지 않을 경우에는 false
                //        // 이 경우 삭제 처리 이후에는 true로 처리됨
                //        if (this.RTList[i].State == TagCalcItem.StateType.None)
                //        {
                //            isCopy = true;
                //            break;
                //        }
                //    }
                //}

                // 20141211 재정의
                // RT, MNF 모두 없을 경우에만 복사
                // 삭제시에는 복사 처리 안함
                if (this.MNFList.Count <= 0 && this.RTList.Count <= 0) isCopy = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isCopy;
        } 
        #endregion
    }
}
