﻿namespace WaterNet.BlockApp
{
    partial class FrmShapeBlockManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmShapeBlockManager));
            Infragistics.Win.UltraWinTree.Override _override2 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tvShape = new Infragistics.Win.UltraWinTree.UltraTree();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtShapeSgccd = new System.Windows.Forms.TextBox();
            this.btnReload = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtShapeRelLocName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtShapeLocName = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtShapePFtrIdn = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtShapePFtrCode = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtShapeFtrIdn = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtShapeFtrCode = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnTagCalcEtc = new System.Windows.Forms.Button();
            this.btnTagCalc = new System.Windows.Forms.Button();
            this.btnTagManage = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.tvBlock = new Infragistics.Win.UltraWinTree.UltraTree();
            this.panel9 = new System.Windows.Forms.Panel();
            this.cbBlockKtGbn = new System.Windows.Forms.ComboBox();
            this.lblLocCodeOrg = new System.Windows.Forms.Label();
            this.btnResCode = new System.Windows.Forms.Button();
            this.lblBlockResCode = new System.Windows.Forms.Label();
            this.lblNew = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtBlockOrderBy = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBlockSgccd = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBlockRelLocName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBlockLocName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBlockPFtrIdn = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBlockPFtrCode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBlockFtrIdn = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBlockFtrCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBlockLocGbn = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBlockPLocCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBlockLocCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkbxTB2 = new System.Windows.Forms.CheckBox();
            this.chkbxTB1 = new System.Windows.Forms.CheckBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuTagMapping = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTagCalc = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTagCalcEtc = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuWaterInfosCode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSystemMaster = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExcelUpload = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvShape)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvBlock)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel8.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(889, 5);
            this.panel1.TabIndex = 0;
            this.panel1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(884, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(5, 609);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 633);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(884, 5);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 29);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(5, 604);
            this.panel4.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(5, 29);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel7);
            this.splitContainer1.Panel1.Controls.Add(this.panel6);
            this.splitContainer1.Panel1.Controls.Add(this.panel5);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel10);
            this.splitContainer1.Panel2.Controls.Add(this.panel9);
            this.splitContainer1.Panel2.Controls.Add(this.panel8);
            this.splitContainer1.Size = new System.Drawing.Size(879, 604);
            this.splitContainer1.SplitterDistance = 435;
            this.splitContainer1.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tvShape);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 30);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(435, 309);
            this.panel7.TabIndex = 2;
            // 
            // tvShape
            // 
            this.tvShape.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvShape.ImageList = this.imageList1;
            this.tvShape.Location = new System.Drawing.Point(0, 0);
            this.tvShape.Name = "tvShape";
            appearance2.Image = "link_green.png";
            _override1.NodeAppearance = appearance2;
            appearance3.Image = "link_blue.png";
            _override1.SelectedNodeAppearance = appearance3;
            this.tvShape.Override = _override1;
            this.tvShape.Size = new System.Drawing.Size(435, 309);
            this.tvShape.TabIndex = 1;
            this.tvShape.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.tvShape_AfterSelect);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "link_green.png");
            this.imageList1.Images.SetKeyName(1, "link_blue.png");
            this.imageList1.Images.SetKeyName(2, "unlink_chain.png");
            this.imageList1.Images.SetKeyName(3, "link_red.png");
            this.imageList1.Images.SetKeyName(4, "link_chain_red.png");
            this.imageList1.Images.SetKeyName(5, "1408453753_link_yellow.png");
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.txtShapeSgccd);
            this.panel6.Controls.Add(this.btnReload);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.txtShapeRelLocName);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.txtShapeLocName);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.txtShapePFtrIdn);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.txtShapePFtrCode);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.txtShapeFtrIdn);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.txtShapeFtrCode);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.panel11);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 339);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(435, 265);
            this.panel6.TabIndex = 1;
            // 
            // txtShapeSgccd
            // 
            this.txtShapeSgccd.BackColor = System.Drawing.SystemColors.Window;
            this.txtShapeSgccd.Location = new System.Drawing.Point(89, 110);
            this.txtShapeSgccd.Name = "txtShapeSgccd";
            this.txtShapeSgccd.ReadOnly = true;
            this.txtShapeSgccd.Size = new System.Drawing.Size(100, 21);
            this.txtShapeSgccd.TabIndex = 37;
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(6, 206);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(105, 23);
            this.btnReload.TabIndex = 0;
            this.btnReload.Text = "목록 다시 읽기";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Visible = false;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 115);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 12);
            this.label17.TabIndex = 36;
            this.label17.Text = "지자체코드";
            // 
            // txtShapeRelLocName
            // 
            this.txtShapeRelLocName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtShapeRelLocName.BackColor = System.Drawing.SystemColors.Window;
            this.txtShapeRelLocName.Location = new System.Drawing.Point(329, 83);
            this.txtShapeRelLocName.Name = "txtShapeRelLocName";
            this.txtShapeRelLocName.ReadOnly = true;
            this.txtShapeRelLocName.Size = new System.Drawing.Size(100, 21);
            this.txtShapeRelLocName.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(223, 88);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 12);
            this.label18.TabIndex = 34;
            this.label18.Text = "관련지역명";
            // 
            // txtShapeLocName
            // 
            this.txtShapeLocName.BackColor = System.Drawing.SystemColors.Window;
            this.txtShapeLocName.Location = new System.Drawing.Point(89, 83);
            this.txtShapeLocName.Name = "txtShapeLocName";
            this.txtShapeLocName.ReadOnly = true;
            this.txtShapeLocName.Size = new System.Drawing.Size(100, 21);
            this.txtShapeLocName.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 88);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 32;
            this.label19.Text = "지역관리명";
            // 
            // txtShapePFtrIdn
            // 
            this.txtShapePFtrIdn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtShapePFtrIdn.BackColor = System.Drawing.SystemColors.Window;
            this.txtShapePFtrIdn.Location = new System.Drawing.Point(329, 56);
            this.txtShapePFtrIdn.Name = "txtShapePFtrIdn";
            this.txtShapePFtrIdn.ReadOnly = true;
            this.txtShapePFtrIdn.Size = new System.Drawing.Size(100, 21);
            this.txtShapePFtrIdn.TabIndex = 31;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(223, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 12);
            this.label20.TabIndex = 30;
            this.label20.Text = "상위 FTR_IDN";
            // 
            // txtShapePFtrCode
            // 
            this.txtShapePFtrCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtShapePFtrCode.Location = new System.Drawing.Point(89, 56);
            this.txtShapePFtrCode.Name = "txtShapePFtrCode";
            this.txtShapePFtrCode.ReadOnly = true;
            this.txtShapePFtrCode.Size = new System.Drawing.Size(100, 21);
            this.txtShapePFtrCode.TabIndex = 29;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 12);
            this.label21.TabIndex = 28;
            this.label21.Text = "상위계층코드";
            // 
            // txtShapeFtrIdn
            // 
            this.txtShapeFtrIdn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtShapeFtrIdn.BackColor = System.Drawing.SystemColors.Window;
            this.txtShapeFtrIdn.Location = new System.Drawing.Point(329, 29);
            this.txtShapeFtrIdn.Name = "txtShapeFtrIdn";
            this.txtShapeFtrIdn.ReadOnly = true;
            this.txtShapeFtrIdn.Size = new System.Drawing.Size(100, 21);
            this.txtShapeFtrIdn.TabIndex = 27;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(223, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 12);
            this.label22.TabIndex = 26;
            this.label22.Text = "FTR_IDN";
            // 
            // txtShapeFtrCode
            // 
            this.txtShapeFtrCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtShapeFtrCode.Location = new System.Drawing.Point(89, 29);
            this.txtShapeFtrCode.Name = "txtShapeFtrCode";
            this.txtShapeFtrCode.ReadOnly = true;
            this.txtShapeFtrCode.Size = new System.Drawing.Size(100, 21);
            this.txtShapeFtrCode.TabIndex = 25;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 34);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 24;
            this.label23.Text = "계층코드";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnTagCalcEtc);
            this.panel11.Controls.Add(this.btnTagCalc);
            this.panel11.Controls.Add(this.btnTagManage);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(0, 235);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(435, 30);
            this.panel11.TabIndex = 0;
            // 
            // btnTagCalcEtc
            // 
            this.btnTagCalcEtc.Location = new System.Drawing.Point(176, 4);
            this.btnTagCalcEtc.Name = "btnTagCalcEtc";
            this.btnTagCalcEtc.Size = new System.Drawing.Size(101, 23);
            this.btnTagCalcEtc.TabIndex = 6;
            this.btnTagCalcEtc.Text = "태그 계산식2";
            this.btnTagCalcEtc.UseVisualStyleBackColor = true;
            this.btnTagCalcEtc.Visible = false;
            this.btnTagCalcEtc.Click += new System.EventHandler(this.btnTagCalcEtc_Click);
            // 
            // btnTagCalc
            // 
            this.btnTagCalc.Location = new System.Drawing.Point(84, 4);
            this.btnTagCalc.Name = "btnTagCalc";
            this.btnTagCalc.Size = new System.Drawing.Size(90, 23);
            this.btnTagCalc.TabIndex = 1;
            this.btnTagCalc.Text = "태그 계산식";
            this.btnTagCalc.UseVisualStyleBackColor = true;
            this.btnTagCalc.Visible = false;
            this.btnTagCalc.Click += new System.EventHandler(this.btnTagCalc_Click);
            // 
            // btnTagManage
            // 
            this.btnTagManage.Location = new System.Drawing.Point(7, 4);
            this.btnTagManage.Name = "btnTagManage";
            this.btnTagManage.Size = new System.Drawing.Size(75, 23);
            this.btnTagManage.TabIndex = 5;
            this.btnTagManage.Text = "태그매핑";
            this.btnTagManage.UseVisualStyleBackColor = true;
            this.btnTagManage.Visible = false;
            this.btnTagManage.Click += new System.EventHandler(this.btnTagManage_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(435, 30);
            this.panel5.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(435, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Shape";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.tvBlock);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 30);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(440, 309);
            this.panel10.TabIndex = 2;
            // 
            // tvBlock
            // 
            this.tvBlock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvBlock.ImageList = this.imageList1;
            this.tvBlock.Location = new System.Drawing.Point(0, 0);
            this.tvBlock.Name = "tvBlock";
            appearance4.Image = "link_green.png";
            _override2.NodeAppearance = appearance4;
            appearance5.Image = "link_blue.png";
            _override2.SelectedNodeAppearance = appearance5;
            this.tvBlock.Override = _override2;
            this.tvBlock.Size = new System.Drawing.Size(440, 309);
            this.tvBlock.TabIndex = 1;
            this.tvBlock.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.tvBlock_AfterSelect);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.cbBlockKtGbn);
            this.panel9.Controls.Add(this.lblLocCodeOrg);
            this.panel9.Controls.Add(this.btnResCode);
            this.panel9.Controls.Add(this.lblBlockResCode);
            this.panel9.Controls.Add(this.lblNew);
            this.panel9.Controls.Add(this.label24);
            this.panel9.Controls.Add(this.txtBlockOrderBy);
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.label14);
            this.panel9.Controls.Add(this.txtBlockSgccd);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Controls.Add(this.txtBlockRelLocName);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.txtBlockLocName);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Controls.Add(this.txtBlockPFtrIdn);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.txtBlockPFtrCode);
            this.panel9.Controls.Add(this.label10);
            this.panel9.Controls.Add(this.txtBlockFtrIdn);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.txtBlockFtrCode);
            this.panel9.Controls.Add(this.label7);
            this.panel9.Controls.Add(this.label6);
            this.panel9.Controls.Add(this.txtBlockLocGbn);
            this.panel9.Controls.Add(this.label5);
            this.panel9.Controls.Add(this.txtBlockPLocCode);
            this.panel9.Controls.Add(this.label4);
            this.panel9.Controls.Add(this.txtBlockLocCode);
            this.panel9.Controls.Add(this.label3);
            this.panel9.Controls.Add(this.chkbxTB2);
            this.panel9.Controls.Add(this.chkbxTB1);
            this.panel9.Controls.Add(this.panel12);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 339);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(440, 265);
            this.panel9.TabIndex = 1;
            // 
            // cbBlockKtGbn
            // 
            this.cbBlockKtGbn.FormattingEnabled = true;
            this.cbBlockKtGbn.Items.AddRange(new object[] {
            "없음",
            "계통",
            "읍면",
            "직결"});
            this.cbBlockKtGbn.Location = new System.Drawing.Point(91, 84);
            this.cbBlockKtGbn.Name = "cbBlockKtGbn";
            this.cbBlockKtGbn.Size = new System.Drawing.Size(100, 20);
            this.cbBlockKtGbn.TabIndex = 35;
            // 
            // lblLocCodeOrg
            // 
            this.lblLocCodeOrg.AutoSize = true;
            this.lblLocCodeOrg.Location = new System.Drawing.Point(198, 10);
            this.lblLocCodeOrg.Name = "lblLocCodeOrg";
            this.lblLocCodeOrg.Size = new System.Drawing.Size(44, 12);
            this.lblLocCodeOrg.TabIndex = 34;
            this.lblLocCodeOrg.Text = "label25";
            this.lblLocCodeOrg.Visible = false;
            // 
            // btnResCode
            // 
            this.btnResCode.Location = new System.Drawing.Point(331, 54);
            this.btnResCode.Name = "btnResCode";
            this.btnResCode.Size = new System.Drawing.Size(54, 23);
            this.btnResCode.TabIndex = 33;
            this.btnResCode.Text = "선택";
            this.btnResCode.UseVisualStyleBackColor = true;
            this.btnResCode.Click += new System.EventHandler(this.btnResCode_Click);
            // 
            // lblBlockResCode
            // 
            this.lblBlockResCode.AutoSize = true;
            this.lblBlockResCode.Location = new System.Drawing.Point(391, 60);
            this.lblBlockResCode.Name = "lblBlockResCode";
            this.lblBlockResCode.Size = new System.Drawing.Size(41, 12);
            this.lblBlockResCode.TabIndex = 32;
            this.lblBlockResCode.Text = "000000";
            // 
            // lblNew
            // 
            this.lblNew.AutoSize = true;
            this.lblNew.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblNew.ForeColor = System.Drawing.Color.Red;
            this.lblNew.Location = new System.Drawing.Point(10, 9);
            this.lblNew.Name = "lblNew";
            this.lblNew.Size = new System.Drawing.Size(63, 13);
            this.lblNew.TabIndex = 31;
            this.lblNew.Text = "신규등록";
            this.lblNew.Visible = false;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Location = new System.Drawing.Point(8, 231);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(425, 3);
            this.label24.TabIndex = 30;
            // 
            // txtBlockOrderBy
            // 
            this.txtBlockOrderBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBlockOrderBy.Location = new System.Drawing.Point(331, 84);
            this.txtBlockOrderBy.Name = "txtBlockOrderBy";
            this.txtBlockOrderBy.Size = new System.Drawing.Size(100, 21);
            this.txtBlockOrderBy.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(225, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 28;
            this.label15.Text = "정렬순번";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 26;
            this.label16.Text = "계통구분";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(225, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 12);
            this.label14.TabIndex = 24;
            this.label14.Text = "관련배수지코드";
            // 
            // txtBlockSgccd
            // 
            this.txtBlockSgccd.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockSgccd.Location = new System.Drawing.Point(91, 204);
            this.txtBlockSgccd.Name = "txtBlockSgccd";
            this.txtBlockSgccd.ReadOnly = true;
            this.txtBlockSgccd.Size = new System.Drawing.Size(100, 21);
            this.txtBlockSgccd.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 209);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 22;
            this.label13.Text = "지자체코드";
            // 
            // txtBlockRelLocName
            // 
            this.txtBlockRelLocName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBlockRelLocName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockRelLocName.Location = new System.Drawing.Point(331, 177);
            this.txtBlockRelLocName.Name = "txtBlockRelLocName";
            this.txtBlockRelLocName.ReadOnly = true;
            this.txtBlockRelLocName.Size = new System.Drawing.Size(100, 21);
            this.txtBlockRelLocName.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(225, 182);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 20;
            this.label11.Text = "관련지역명";
            // 
            // txtBlockLocName
            // 
            this.txtBlockLocName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockLocName.Location = new System.Drawing.Point(91, 177);
            this.txtBlockLocName.Name = "txtBlockLocName";
            this.txtBlockLocName.ReadOnly = true;
            this.txtBlockLocName.Size = new System.Drawing.Size(100, 21);
            this.txtBlockLocName.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 182);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 18;
            this.label12.Text = "지역관리명";
            // 
            // txtBlockPFtrIdn
            // 
            this.txtBlockPFtrIdn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBlockPFtrIdn.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockPFtrIdn.Location = new System.Drawing.Point(331, 150);
            this.txtBlockPFtrIdn.Name = "txtBlockPFtrIdn";
            this.txtBlockPFtrIdn.ReadOnly = true;
            this.txtBlockPFtrIdn.Size = new System.Drawing.Size(100, 21);
            this.txtBlockPFtrIdn.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(225, 155);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 12);
            this.label9.TabIndex = 16;
            this.label9.Text = "상위 FTR_IDN";
            // 
            // txtBlockPFtrCode
            // 
            this.txtBlockPFtrCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockPFtrCode.Location = new System.Drawing.Point(91, 150);
            this.txtBlockPFtrCode.Name = "txtBlockPFtrCode";
            this.txtBlockPFtrCode.ReadOnly = true;
            this.txtBlockPFtrCode.Size = new System.Drawing.Size(100, 21);
            this.txtBlockPFtrCode.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "상위계층코드";
            // 
            // txtBlockFtrIdn
            // 
            this.txtBlockFtrIdn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBlockFtrIdn.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockFtrIdn.Location = new System.Drawing.Point(331, 123);
            this.txtBlockFtrIdn.Name = "txtBlockFtrIdn";
            this.txtBlockFtrIdn.ReadOnly = true;
            this.txtBlockFtrIdn.Size = new System.Drawing.Size(100, 21);
            this.txtBlockFtrIdn.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(225, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "FTR_IDN";
            // 
            // txtBlockFtrCode
            // 
            this.txtBlockFtrCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockFtrCode.Location = new System.Drawing.Point(91, 123);
            this.txtBlockFtrCode.Name = "txtBlockFtrCode";
            this.txtBlockFtrCode.ReadOnly = true;
            this.txtBlockFtrCode.Size = new System.Drawing.Size(100, 21);
            this.txtBlockFtrCode.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 10;
            this.label7.Text = "계층코드";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(8, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(425, 3);
            this.label6.TabIndex = 9;
            // 
            // txtBlockLocGbn
            // 
            this.txtBlockLocGbn.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.txtBlockLocGbn.Location = new System.Drawing.Point(91, 55);
            this.txtBlockLocGbn.Name = "txtBlockLocGbn";
            this.txtBlockLocGbn.Size = new System.Drawing.Size(100, 21);
            this.txtBlockLocGbn.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "지역관리구분";
            // 
            // txtBlockPLocCode
            // 
            this.txtBlockPLocCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBlockPLocCode.Location = new System.Drawing.Point(331, 28);
            this.txtBlockPLocCode.Name = "txtBlockPLocCode";
            this.txtBlockPLocCode.Size = new System.Drawing.Size(100, 21);
            this.txtBlockPLocCode.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "상위지역관리코드";
            // 
            // txtBlockLocCode
            // 
            this.txtBlockLocCode.Location = new System.Drawing.Point(91, 28);
            this.txtBlockLocCode.Name = "txtBlockLocCode";
            this.txtBlockLocCode.Size = new System.Drawing.Size(100, 21);
            this.txtBlockLocCode.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "지역관리코드";
            // 
            // chkbxTB2
            // 
            this.chkbxTB2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkbxTB2.AutoSize = true;
            this.chkbxTB2.Enabled = false;
            this.chkbxTB2.Location = new System.Drawing.Point(361, 6);
            this.chkbxTB2.Name = "chkbxTB2";
            this.chkbxTB2.Size = new System.Drawing.Size(72, 16);
            this.chkbxTB2.TabIndex = 2;
            this.chkbxTB2.Text = "보고서용";
            this.chkbxTB2.UseVisualStyleBackColor = true;
            // 
            // chkbxTB1
            // 
            this.chkbxTB1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkbxTB1.AutoSize = true;
            this.chkbxTB1.Enabled = false;
            this.chkbxTB1.Location = new System.Drawing.Point(307, 6);
            this.chkbxTB1.Name = "chkbxTB1";
            this.chkbxTB1.Size = new System.Drawing.Size(48, 16);
            this.chkbxTB1.TabIndex = 1;
            this.chkbxTB1.Text = "범용";
            this.chkbxTB1.UseVisualStyleBackColor = true;
            this.chkbxTB1.CheckedChanged += new System.EventHandler(this.chkbxTB1_CheckedChanged);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.btnClose);
            this.panel12.Controls.Add(this.btnDel);
            this.panel12.Controls.Add(this.btnSave);
            this.panel12.Controls.Add(this.btnNew);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel12.Location = new System.Drawing.Point(0, 235);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(440, 30);
            this.panel12.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(374, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(60, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "닫기(&C)";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(133, 4);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(60, 23);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "삭제(&D)";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(70, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "저장(&S)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(7, 4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(60, 23);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "신규(&N)";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(440, 30);
            this.panel8.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(440, 30);
            this.label2.TabIndex = 0;
            this.label2.Text = "Block";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(889, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpen,
            this.toolStripMenuItem3,
            this.mnuTagMapping,
            this.mnuTagCalc,
            this.mnuTagCalcEtc,
            this.toolStripMenuItem1,
            this.mnuWaterInfosCode,
            this.toolStripMenuItem4,
            this.mnuSystemMaster,
            this.toolStripMenuItem2,
            this.mnuExcelUpload,
            this.toolStripMenuItem5,
            this.mnuExit});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // mnuOpen
            // 
            this.mnuOpen.Name = "mnuOpen";
            this.mnuOpen.Size = new System.Drawing.Size(174, 22);
            this.mnuOpen.Text = "지역 열기(&O)";
            this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(171, 6);
            // 
            // mnuTagMapping
            // 
            this.mnuTagMapping.Name = "mnuTagMapping";
            this.mnuTagMapping.Size = new System.Drawing.Size(174, 22);
            this.mnuTagMapping.Text = "태그매핑(&T)";
            this.mnuTagMapping.Click += new System.EventHandler(this.mnuTagMapping_Click);
            // 
            // mnuTagCalc
            // 
            this.mnuTagCalc.Name = "mnuTagCalc";
            this.mnuTagCalc.Size = new System.Drawing.Size(174, 22);
            this.mnuTagCalc.Text = "태그 계산식(&C)";
            this.mnuTagCalc.Click += new System.EventHandler(this.mnuTagCalc_Click);
            // 
            // mnuTagCalcEtc
            // 
            this.mnuTagCalcEtc.Name = "mnuTagCalcEtc";
            this.mnuTagCalcEtc.Size = new System.Drawing.Size(174, 22);
            this.mnuTagCalcEtc.Text = "태그 계산식2(&E)";
            this.mnuTagCalcEtc.Click += new System.EventHandler(this.mnuTagCalcEtc_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(171, 6);
            // 
            // mnuWaterInfosCode
            // 
            this.mnuWaterInfosCode.Name = "mnuWaterInfosCode";
            this.mnuWaterInfosCode.Size = new System.Drawing.Size(174, 22);
            this.mnuWaterInfosCode.Text = "WaterInfos 코드";
            this.mnuWaterInfosCode.Click += new System.EventHandler(this.mnuWaterInfosCode_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(171, 6);
            // 
            // mnuSystemMaster
            // 
            this.mnuSystemMaster.Name = "mnuSystemMaster";
            this.mnuSystemMaster.Size = new System.Drawing.Size(174, 22);
            this.mnuSystemMaster.Text = "관망운영일보 설정";
            this.mnuSystemMaster.Click += new System.EventHandler(this.mnuSystemMaster_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(171, 6);
            // 
            // mnuExcelUpload
            // 
            this.mnuExcelUpload.Name = "mnuExcelUpload";
            this.mnuExcelUpload.Size = new System.Drawing.Size(174, 22);
            this.mnuExcelUpload.Text = "엑셀 업로드(&L)";
            this.mnuExcelUpload.Click += new System.EventHandler(this.mnuExcelUpload_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(171, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(174, 22);
            this.mnuExit.Text = "종료(&X)";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // FrmShapeBlockManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 638);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmShapeBlockManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmBlockManager";
            this.Load += new System.EventHandler(this.FrmBlockManager_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvShape)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvBlock)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.CheckBox chkbxTB1;
        private System.Windows.Forms.CheckBox chkbxTB2;
        private System.Windows.Forms.TextBox txtBlockPLocCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBlockLocCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBlockLocGbn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBlockOrderBy;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBlockSgccd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBlockRelLocName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBlockLocName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBlockPFtrIdn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBlockPFtrCode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBlockFtrIdn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBlockFtrCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtShapeSgccd;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtShapeRelLocName;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtShapeLocName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtShapePFtrIdn;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtShapePFtrCode;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtShapeFtrIdn;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtShapeFtrCode;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblNew;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Button btnTagManage;
        private System.Windows.Forms.Button btnTagCalc;
        private Infragistics.Win.UltraWinTree.UltraTree tvShape;
        private Infragistics.Win.UltraWinTree.UltraTree tvBlock;
        private System.Windows.Forms.Button btnTagCalcEtc;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuTagMapping;
        private System.Windows.Forms.ToolStripMenuItem mnuTagCalc;
        private System.Windows.Forms.ToolStripMenuItem mnuTagCalcEtc;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.ToolStripMenuItem mnuWaterInfosCode;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mnuOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.Label lblBlockResCode;
        private System.Windows.Forms.Button btnResCode;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem mnuSystemMaster;
        private System.Windows.Forms.ToolStripMenuItem mnuExcelUpload;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.Label lblLocCodeOrg;
        private System.Windows.Forms.ComboBox cbBlockKtGbn;
    }
}