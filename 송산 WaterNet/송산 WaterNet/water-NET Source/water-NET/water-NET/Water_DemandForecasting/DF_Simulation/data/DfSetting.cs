﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

namespace WaterNet.DF_Simulation.data
{
    public class DfSetting
    {
        public static Hashtable DefaultSetting
        {
            get
            {
                Hashtable result = new Hashtable();
                result["STUDYMETHOD"] = "0";

                //이상치보정법
                //0 : 평균유량보정
                //1 : 예측유량보정
                //2 : 미보정
                result["ABNORMALDATA"] = "1";

                //시간예측알고리즘
                //0 : 고정분배방식
                //1 : 오차가중분배방식
                //2 : 오차율보정분배방식
                result["TIMEFORECASTING"] = "1";
                result["DAYKIND"] = "true";
                result["STUDYDAY"] = "90";
                result["STUDYPATTERN"] = "7";
                result["USE_YN"] = "N";
                return result;
            }
        }

        public static Hashtable DefaultSettingNN
        {
            get
            {
                Hashtable result = new Hashtable();
                result["NEURON"] = "25";
                result["LEARNINGRATE"] = "0.005";
                result["MOMENTUM"] = "0";
                result["ITERATIONS"] = "20000";
                return result;
            }
        }

        public static string DefaultSettingMRM
        {
            get
            {
                return "1";
            }
        }

        public static string DefaultSettingOutlier
        {
            get
            {
                return "90";
            }
        }
    }
}
