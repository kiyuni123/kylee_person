﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

namespace WaterNet.DF_Simulation.data
{
    public interface IDfData
    {
        string Key { set; get; }
        DateTime TargetDate { set; }
        int TargetTime { set; }
        Hashtable Setting_Default { get; }
        int StudyMethod { get; }
        int InputNum { get; }
        int[] Setting_MRM { get; }
        Hashtable Setting_NN { get; }
        double Momentum { get; }
        double LearningRate { get; }
        int Neuron { get; }
        int Iterations { get; }
        double[] Setting_Outlier { get; }
        DataTable Setting_Data { get; }
        double[,] Training_Data { get; }
        double[,] Input_Data { get; }
        double Prediction_Result { get; set; }
        double[] Hour_prediction { get; }
        double[] Hour_mean { get; }
        double[] Hour_stdv { get; }
        bool IsDataError { get; }
        List<string> Message { get; }
        double[] MaxValue { get; }
        double[] MinValue { get; }
        void Set_Hour_prediction();
        void UpdateDayPrediction();
        void UpdateHourPrediction_CurrentHour();
        void UpdateHourPrediction_AllHour();
    }
}
