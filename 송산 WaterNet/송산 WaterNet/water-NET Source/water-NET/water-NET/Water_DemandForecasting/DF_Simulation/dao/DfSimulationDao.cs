﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.dao;
using System.Collections;
using System.Data;
using Oracle.DataAccess.Client;
using WaterNet.WaterNetCore;

namespace WaterNet.DF_Simulation.dao
{
    public class DfSimulationDao : BaseDao
    {
        private static DfSimulationDao dao = null;
        private DfSimulationDao() { }

        public static DfSimulationDao GetInstance()
        {
            if (dao == null)
            {
                dao = new DfSimulationDao();
            }
            return dao;
        }

        //배수지명 조회
        public object SelectReservoirName(OracleDBManager manager, string CODE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT SERVNM FROM WE_SUPPLY_AREA WHERE SUPY_FTR_IDN = '" + CODE + "'");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        //블럭명 조회
        public object SelectBlockName(OracleDBManager manager, string CODE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT LOC_NAME FROM CM_LOCATION2 WHERE LOC_CODE = '" + CODE + "'");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        //수요예측관리 블록 조회
        public DataSet SelectDfSimulationBlock(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            
            query.AppendLine("with loc as                                                                                                                  ");
            query.AppendLine("(																															   ");
            query.AppendLine("select c1.loc_code																										   ");
            query.AppendLine("	  ,c1.sgccd																												   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock										   ");
            query.AppendLine("	  ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code	   ");
            query.AppendLine("	  ,c1.ord																												   ");
            query.AppendLine("	  ,c1.ftr_code	    																									   ");
            query.AppendLine("  from																													   ");
            query.AppendLine("	  (																														   ");
            query.AppendLine("	   select sgccd																											   ");
            query.AppendLine("			 ,loc_code																										   ");
            query.AppendLine("			 ,ploc_code																										   ");
            query.AppendLine("			 ,loc_name																										   ");
            query.AppendLine("			 ,ftr_idn																										   ");
            query.AppendLine("			 ,ftr_code																										   ");
            query.AppendLine("			 ,rel_loc_name																									   ");
            query.AppendLine("			 ,kt_gbn																										   ");
            query.AppendLine("			 ,rownum ord																									   ");
            query.AppendLine("		 from cm_location																									   ");
            query.AppendLine("		where 1 = 1																											   ");

            if (parameter["LOC_CODE"] != null && parameter["LOC_CODE"].ToString() == "O")
            {
                query.AppendLine("		start with ftr_code = decode(:LOC_CODE, 'O', 'BZ001')                                                              ");

            }
            else if (parameter["LOC_CODE"] != null && parameter["LOC_CODE"].ToString() != "O")
            {
                query.AppendLine("		start with loc_code = :LOC_CODE                                                                                    ");
            }

            query.AppendLine("		connect by prior loc_code = ploc_code																				   ");
            query.AppendLine("		order SIBLINGS by ftr_idn																							   ");
            query.AppendLine("	  ) c1																													   ");
            query.AppendLine("	   ,cm_location c2																										   ");
            query.AppendLine("	   ,cm_location c3																										   ");
            query.AppendLine(" where 1 = 1																												   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																						   ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																						   ");
            query.AppendLine(" order by c1.ord																											   ");
            query.AppendLine(")																															   ");
            query.AppendLine(" select loc.loc_code																										   ");
            query.AppendLine("	  ,loc.lblock																											   ");
            query.AppendLine("	  ,loc.mblock																											   ");
            query.AppendLine("	  ,loc.sblock																											   ");
            query.AppendLine("	  ,loc.ftr_code																											   ");
            query.AppendLine("    ,(select max(iih.tagname) from if_ihtags iih, if_tag_gbn itg where iih.loc_code = loc.tag_loc_code and iih.tagname = itg.tagname and itg.tag_gbn = 'FRQ') out_flow_tag ");
            query.AppendLine("    ,nvl((                                                                                                                   ");
            query.AppendLine("       select use_yn from df_setting where loc_code = loc.loc_code                                                           ");
            query.AppendLine("     ),'N') use_yn                                                                                                           ");
            query.AppendLine("   from loc loc                                                                                                              ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측관리 배수지 조회
        public DataSet SelectDfSimulationReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select a.res_code loc_code                                                                                                      ");
            //query.AppendLine("      ,(select ftr_idn from cm_location where loc_code = a.res_code) ftr_idn                                                    ");
            //query.AppendLine("      ,(select loc_name from cm_location where loc_code = a.res_code) loc_name                                                  ");
            //query.AppendLine("      ,(                                                                                                                        ");
            //query.AppendLine("        select c.tagname                                                                                                        ");
            //query.AppendLine("          from if_ihtags b                                                                                                      ");
            //query.AppendLine("              ,if_tag_gbn c                                                                                                     ");
            //query.AppendLine("         where b.loc_code = decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            //query.AppendLine("           and c.tagname = b.tagname                                                                                            ");
            //query.AppendLine("           and c.tag_gbn = 'FRQ_O'                                                                                              ");
            //query.AppendLine("       ) out_flow_tag                                                                                                           ");
            //query.AppendLine("      ,nvl((                                                                                                                    ");
            //query.AppendLine("       select use_yn from df_setting where loc_code = a.res_code                                                                ");
            //query.AppendLine("       ),'N') use_yn                                                                                                            ");
            //query.AppendLine(" from cm_location a                                                                                                             ");
            //query.AppendLine("where a.res_code is not null                                                                                                    ");
            //query.AppendLine("  and (select loc_gbn from cm_location where loc_code = a.res_code) = '배수지'                                                  ");

            query.AppendLine("SELECT A.SUPY_FTR_IDN LOC_CODE ");
            query.AppendLine("      ,(SELECT FTR_IDN FROM CM_LOCATION WHERE LOC_CODE = A.SUPY_FTR_IDN) FTR_IDN ");
            query.AppendLine("      ,A.SERVNM LOC_NAME ");
            query.AppendLine("      ,A.OUT_FLOW_TAG ");
            query.AppendLine("      ,NVL((SELECT USE_YN FROM DF_SETTING WHERE LOC_CODE = A.SUPY_FTR_IDN), 'N') USE_YN ");
            query.AppendLine("  FROM WE_SUPPLY_AREA A ");

            return manager.ExecuteScriptDataSet(query.ToString(), null);
        }

        //수요예측관리 유량계 조회
        public DataSet SelectDfSimulationResult(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select b.df_rl_dt                                                                                ");
            query.AppendLine("      ,decode(a.df_tdfq, null, 'F', 'T') result												   ");
            query.AppendLine("  from df_result_day a 																		   ");
            query.AppendLine("      ,(select to_date(:STARTDATE,'yyyymmdd') + rownum-1 df_rl_dt								   ");
            query.AppendLine("          from dual 																			   ");
            query.AppendLine("       connect by rownum <=  to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') +1 ) b");
            query.AppendLine(" where a.loc_code(+) = :LOC_CODE																   ");
            query.AppendLine("   and a.df_rl_dt(+) between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')	   ");
            query.AppendLine("   and a.df_rl_dt(+) = b.df_rl_dt																   ");


            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["LOC_CODE"];
            parameters[4].Value = parameter["STARTDATE"];
            parameters[5].Value = parameter["ENDDATE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public object SelectDfSimulationUse(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select nvl((select use_yn from df_setting where loc_code = :LOC_CODE),'N') from dual               ");


            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectDfSimulationTm(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select nvl((select tm_yn from wv_flowmeter where flowmeter_ftr_idn = :FTR_IDN),'N') from dual               ");


            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_IDN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public DataSet SelectDfSimulationSettingBlock(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select studymethod                              ");
            query.AppendLine("      ,abnormaldata							  ");
            query.AppendLine("      ,timeforecasting						  ");
            query.AppendLine("      ,daykind								  ");
            query.AppendLine("      ,studyday								  ");
            query.AppendLine("      ,studypattern							  ");
            query.AppendLine("      ,use_yn							          ");
            query.AppendLine("  from df_setting								  ");
            query.AppendLine(" where loc_code = :LOC_CODE						  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectDfSimulationSettingReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("  select a.studymethod                                                                     ");
            query.AppendLine("    ,a.abnormaldata                                                                        ");
            query.AppendLine("    ,a.timeforecasting                                                                     ");
            query.AppendLine("    ,a.daykind                                                                             ");
            query.AppendLine("    ,a.studyday                                                                            ");
            query.AppendLine("    ,a.studypattern                                                                        ");
            query.AppendLine("    ,a.use_yn                                                                              ");
            query.AppendLine("  from df_setting a                                                                        ");
            query.AppendLine(" where a.loc_code = :LOC_CODE                                                              ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectDfSimulationSetting(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("  select a.studymethod                                                                     ");
            query.AppendLine("    ,a.abnormaldata                                                                        ");
            query.AppendLine("    ,a.timeforecasting                                                                     ");
            query.AppendLine("    ,a.daykind                                                                             ");
            query.AppendLine("    ,a.studyday                                                                            ");
            query.AppendLine("    ,a.studypattern                                                                        ");
            query.AppendLine("    ,a.use_yn                                                                              ");
            query.AppendLine("  from df_setting a                                                                        ");
            query.AppendLine(" where a.loc_code = :LOC_CODE                                                              ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectDfSimulationSettingNN(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select neuron                                   ");
            query.AppendLine("      ,learningrate                             ");
            query.AppendLine("      ,momentum                                 ");
            query.AppendLine("      ,iterations                               ");
            query.AppendLine("  from df_setting_nn                            ");
            query.AppendLine(" where loc_code = :LOC_CODE                     ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectDfSimulationSettingMRM(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select attribute attribute                      ");
            query.AppendLine("      ,value value                              ");
            query.AppendLine("  from df_setting_mrm                           ");
            query.AppendLine(" where loc_code = :LOC_CODE                     ");
            query.AppendLine(" order by to_number(attribute)                  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectDfSimulationSettingOutlier(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select time time                                ");
            query.AppendLine("      ,alpha alpha                              ");
            query.AppendLine("  from df_setting_outlier                       ");
            query.AppendLine(" where loc_code = :LOC_CODE                     ");
            query.AppendLine(" order by to_number(time)                       ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public void DeleteDfSimulationSetting(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from df_setting                          ");
            query.AppendLine(" where loc_code = :LOC_CODE                     ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeleteDfSimulationSettingNN(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from df_setting_nn                       ");
            query.AppendLine(" where loc_code = :LOC_CODE                     ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeleteDfSimulationSettingMRM(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from df_setting_mrm                      ");
            query.AppendLine(" where loc_code = :LOC_CODE                     ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeleteDfSimulationSettingOutlier(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from df_setting_outlier                  ");
            query.AppendLine(" where loc_code = :LOC_CODE                     ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void InsertDfSimulationSetting(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into df_setting a                           ");
            query.AppendLine("select :LOC_CODE  								 ");
            query.AppendLine("      ,:STUDYMETHOD								 ");
            query.AppendLine("      ,:ABNORMALDATA								 ");
            query.AppendLine("      ,:TIMEFORECASTING							 ");
            query.AppendLine("      ,:DAYKIND									 ");
            query.AppendLine("      ,:STUDYDAY									 ");
            query.AppendLine("      ,:STUDYPATTERN								 ");
            query.AppendLine("      ,:USE_YN								     ");
            query.AppendLine("  from dual										 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("STUDYMETHOD", OracleDbType.Varchar2),
                     new OracleParameter("ABNORMALDATA", OracleDbType.Varchar2),
                     new OracleParameter("TIMEFORECASTING", OracleDbType.Varchar2),
                     new OracleParameter("DAYKIND", OracleDbType.Varchar2),
                     new OracleParameter("STUDYDAY", OracleDbType.Varchar2),
                     new OracleParameter("STUDYPATTERN", OracleDbType.Varchar2),
                     new OracleParameter("USE_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STUDYMETHOD"];
            parameters[2].Value = parameter["ABNORMALDATA"];
            parameters[3].Value = parameter["TIMEFORECASTING"];
            parameters[4].Value = parameter["DAYKIND"];
            parameters[5].Value = parameter["STUDYDAY"];
            parameters[6].Value = parameter["STUDYPATTERN"];
            parameters[7].Value = parameter["USE_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void InsertDfSimulationSettingNN(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into df_setting_nn                          ");
            query.AppendLine("select :LOC_CODE  								 ");
            query.AppendLine("      ,:NEURON									 ");
            query.AppendLine("      ,:LEARNINGRATE								 ");
            query.AppendLine("      ,:MOMENTUM									 ");
            query.AppendLine("      ,:ITERATIONS								 ");
            query.AppendLine("  from dual										 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("NEURON", OracleDbType.Varchar2),
                     new OracleParameter("LEARNINGRATE", OracleDbType.Varchar2),
                     new OracleParameter("MOMENTUM", OracleDbType.Varchar2),
                     new OracleParameter("ITERATIONS", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["NEURON"];
            parameters[2].Value = parameter["LEARNINGRATE"];
            parameters[3].Value = parameter["MOMENTUM"];
            parameters[4].Value = parameter["ITERATIONS"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void InsertDfSimulationSettingMRM(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into df_setting_mrm                         ");
            query.AppendLine("select :LOC_CODE									 ");
            query.AppendLine("      ,:ATTRIBUTE									 ");
            query.AppendLine("      ,:VALUE										 ");
            query.AppendLine("  from dual										 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("ATTRIBUTE", OracleDbType.Varchar2),
                     new OracleParameter("VALUE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["ATTRIBUTE"];
            parameters[2].Value = parameter["VALUE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void InsertDfSimulationSettingOutlier(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into df_setting_outlier                     ");
            query.AppendLine("select :LOC_CODE									 ");
            query.AppendLine("      ,:TIME										 ");
            query.AppendLine("      ,:ALPHA										 ");
            query.AppendLine("  from dual										 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("TIME", OracleDbType.Varchar2),
                     new OracleParameter("VALUE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["TIME"];
            parameters[2].Value = parameter["ALPHA"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 수요예측에 필요한 데이터를 조회한다.
        /// 01시의 적산값이 00~01사이의 값이라면 SQL에서 1시간을 빼서 조회해야한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public DataSet SelectDfSimulationSettingDataBlock(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as                                                                                                                 ");
            query.AppendLine("(                                                                                                                            ");
            query.AppendLine("select a.timestamp                                                                                                           ");
            query.AppendLine("      ,b.time                                                                                                                ");
            query.AppendLine("  from                                                                                                                       ");
            query.AppendLine("      (                                                                                                                      ");
            query.AppendLine("       select to_date(:TARGETDATE,'yyyymmdd') - (rownum-1) timestamp                                                         ");
            query.AppendLine("         from dual connect by rownum <= to_number(:STUDYDAY)+1                                                               ");
            query.AppendLine("      ) a,                                                                                                                   ");
            query.AppendLine("      (                                                                                                                      ");
            query.AppendLine("       select ltrim(to_char(rownum,'00')) time                                                                               ");
            query.AppendLine("         from dual connect by rownum <= 24                                                                                   ");
            query.AppendLine("      ) b                                                                                                                    ");
            query.AppendLine(")                                                                                                                            ");
            query.AppendLine("select temp.timestamp dlog_date                                                                                              ");
            query.AppendLine("    ,round(max(b.value),2) dlog_valu                                                                                         ");
            query.AppendLine("        ,round(nvl(max(decode(temp.time, '01', a.value, null)),0),2) vh01                                                    ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '02', a.value, null)),0),2) vh02                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '03', a.value, null)),0),2) vh03                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '04', a.value, null)),0),2) vh04                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '05', a.value, null)),0),2) vh05                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '06', a.value, null)),0),2) vh06                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '07', a.value, null)),0),2) vh07                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '08', a.value, null)),0),2) vh08                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '09', a.value, null)),0),2) vh09                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '10', a.value, null)),0),2) vh10                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '11', a.value, null)),0),2) vh11                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '12', a.value, null)),0),2) vh12                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '13', a.value, null)),0),2) vh13                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '14', a.value, null)),0),2) vh14                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '15', a.value, null)),0),2) vh15                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '16', a.value, null)),0),2) vh16                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '17', a.value, null)),0),2) vh17                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '18', a.value, null)),0),2) vh18                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '19', a.value, null)),0),2) vh19                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '20', a.value, null)),0),2) vh20                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '21', a.value, null)),0),2) vh21                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '22', a.value, null)),0),2) vh22                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '23', a.value, null)),0),2) vh23                                                        ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '24', a.value, null)),0),2) vh24                                                        ");
            query.AppendLine("    ,nvl(min(a.result),'F') result                                                                                           ");
            query.AppendLine("  from temp,                                                                                                                 ");
            query.AppendLine("(                                                                                                                            ");
            query.AppendLine(" select to_date(to_char(d.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') timestamp                                                 ");
            query.AppendLine("     ,ltrim(to_char(to_char(d.timestamp-(1/24),'hh24')+1,'00')) time                                                         ");
            query.AppendLine("     ,sum(d.value) value                                                                                                     ");
            query.AppendLine("     ,'T' result                                                                                                             ");
            query.AppendLine("  from cm_location a                                                                                                         ");
            query.AppendLine("      ,if_ihtags b                                                                                                           ");
            query.AppendLine("    ,if_tag_gbn c                                                                                                            ");
            query.AppendLine("    ,if_accumulation_hour d                                                                                                  ");
            query.AppendLine(" where a.loc_code = :LOC_CODE                                                                                                ");
            query.AppendLine("   and b.loc_code =                                                                                                          ");
            query.AppendLine("         decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)                ");
            query.AppendLine("   and c.tagname = b.tagname                                                                                                 ");
            query.AppendLine("   and c.tag_gbn = 'FRQ'                                                                                                     ");
            query.AppendLine("   and d.tagname = c.tagname                                                                                                 ");
            query.AppendLine("group by to_date(to_char(d.timestamp-(1/24),'yyyymmdd'),'yyyymmdd'),ltrim(to_char(to_char(d.timestamp-(1/24),'hh24')+1,'00'))");
            query.AppendLine(" ) a,                                                                                                                        ");
            query.AppendLine(" (                                                                                                                           ");
            query.AppendLine(" select to_char(d.timestamp-(1/24),'yyyymmdd') timestamp                                                                     ");
            query.AppendLine("     ,sum(d.value) value                                                                                                     ");
            query.AppendLine("   from cm_location a                                                                                                        ");
            query.AppendLine("     ,if_ihtags b                                                                                                            ");
            query.AppendLine("     ,if_tag_gbn c                                                                                                           ");
            query.AppendLine("     ,if_accumulation_hour d                                                                                                 ");
            query.AppendLine("  where a.loc_code = :LOC_CODE                                                                                               ");
            query.AppendLine("    and b.loc_code =                                                                                                         ");
            query.AppendLine("      decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)                   ");
            query.AppendLine("  and c.tagname = b.tagname                                                                                                  ");
            query.AppendLine("  and c.tag_gbn = 'FRQ'                                                                                                      ");
            query.AppendLine("      and d.tagname = c.tagname                                                                                              ");
            query.AppendLine("  group by to_char(d.timestamp-(1/24),'yyyymmdd')                                                                            ");
            query.AppendLine(" ) b                                                                                                                         ");
            query.AppendLine("where a.timestamp(+) = temp.timestamp                                                                                        ");
            query.AppendLine("  and a.time(+) = temp.time                                                                                                  ");
            query.AppendLine("  and b.timestamp(+) = temp.timestamp                                                                                        ");
            query.AppendLine("group by temp.timestamp                                                                                                      ");
            query.AppendLine("order by temp.timestamp                                                                                                      ");

            IDataParameter[] parameters =  {
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("STUDYDAY", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TARGETDATE"];
            parameters[1].Value = parameter["STUDYDAY"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        /// <summary>
        /// 수요예측에 필요한 데이터를 조회한다.
        /// 01시의 적산값이 00~01사이의 값이라면 SQL에서 1시간을 빼서 조회해야한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public DataSet SelectDfSimulationSettingDataReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as                                                                                                      ");
            query.AppendLine("(                                                                                                                 ");
            query.AppendLine("select a.timestamp                                                                                                ");
            query.AppendLine("  ,b.time                                                                                                         ");
            query.AppendLine("  from                                                                                                            ");
            query.AppendLine("  (                                                                                                               ");
            query.AppendLine("  select to_date(:TARGETDATE,'yyyymmdd') - (rownum-1) timestamp                                                   ");
            query.AppendLine("    from dual connect by rownum <= to_number(:STUDYDAY)+1                                                         ");
            query.AppendLine("  ) a,                                                                                                            ");
            query.AppendLine("  (                                                                                                               ");
            query.AppendLine("  select ltrim(to_char(rownum,'00')) time                                                                         ");
            query.AppendLine("    from dual connect by rownum <= 24                                                                             ");
            query.AppendLine("  ) b                                                                                                             ");
            query.AppendLine(")                                                                                                                 ");
            query.AppendLine("select temp.timestamp dlog_date                                                                                   ");
            query.AppendLine("    ,round(max(b.value),2) dlog_valu                                                                              ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '01', a.value, null)),0),2) vh01                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '02', a.value, null)),0),2) vh02                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '03', a.value, null)),0),2) vh03                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '04', a.value, null)),0),2) vh04                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '05', a.value, null)),0),2) vh05                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '06', a.value, null)),0),2) vh06                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '07', a.value, null)),0),2) vh07                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '08', a.value, null)),0),2) vh08                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '09', a.value, null)),0),2) vh09                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '10', a.value, null)),0),2) vh10                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '11', a.value, null)),0),2) vh11                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '12', a.value, null)),0),2) vh12                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '13', a.value, null)),0),2) vh13                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '14', a.value, null)),0),2) vh14                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '15', a.value, null)),0),2) vh15                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '16', a.value, null)),0),2) vh16                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '17', a.value, null)),0),2) vh17                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '18', a.value, null)),0),2) vh18                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '19', a.value, null)),0),2) vh19                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '20', a.value, null)),0),2) vh20                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '21', a.value, null)),0),2) vh21                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '22', a.value, null)),0),2) vh22                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '23', a.value, null)),0),2) vh23                                             ");
            query.AppendLine("    ,round(nvl(max(decode(temp.time, '24', a.value, null)),0),2) vh24                                             ");
            query.AppendLine("    ,nvl(min(a.result),'F') result                                                                                ");
            query.AppendLine("  from temp,                                                                                                      ");
            query.AppendLine("    (                                                                                                             ");

            query.AppendLine("     select to_date(to_char(d.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') timestamp                                  ");
            query.AppendLine("           ,ltrim(to_char(to_char(d.timestamp-(1/24),'hh24')+1,'00')) time                                        ");
            query.AppendLine("           ,d.value value                                                                                         ");
            query.AppendLine("           ,'T' result                                                                                            ");
            query.AppendLine("       from if_accumulation_hour d                                                                                ");
            query.AppendLine("      where d.tagname = (select out_flow_tag from we_supply_area where supy_ftr_idn = :LOC_CODE )                 ");

            //query.AppendLine("     select to_date(to_char(d.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') timestamp                                  ");
            //query.AppendLine("           ,ltrim(to_char(to_char(d.timestamp-(1/24),'hh24')+1,'00')) time                                        ");
            //query.AppendLine("           ,d.value value                                                                                         ");
            //query.AppendLine("           ,'T' result                                                                                            ");
            //query.AppendLine("         from cm_location a                                                                                       ");
            //query.AppendLine("             ,if_ihtags b                                                                                         ");
            //query.AppendLine("             ,if_tag_gbn c                                                                                        ");
            //query.AppendLine("             ,if_accumulation_hour d                                                                              ");
            //query.AppendLine("        where a.res_code = :LOC_CODE                                                                              ");
            //query.AppendLine("          and b.loc_code =                                                                                        ");
            //query.AppendLine("              decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            //query.AppendLine("          and c.tagname = b.tagname                                                                               ");
            //query.AppendLine("          and c.tag_gbn = 'FRQ_O'                                                                                 ");
            //query.AppendLine("        and d.tagname = c.tagname                                                                                 ");

            query.AppendLine("     ) a,                                                                                                         ");
            query.AppendLine("     (                                                                                                            ");

            query.AppendLine("     select to_char(d.timestamp-(1/24),'yyyymmdd') timestamp                                                      ");
            query.AppendLine("           ,sum(d.value) value                                                                                    ");
            query.AppendLine("       from if_accumulation_hour d                                                                                ");
            query.AppendLine("      where d.tagname = (select out_flow_tag from we_supply_area where supy_ftr_idn = :LOC_CODE )                 ");
            query.AppendLine("      group by to_char(d.timestamp-(1/24),'yyyymmdd')                                                             ");


            //query.AppendLine("     select to_char(d.timestamp-(1/24),'yyyymmdd') timestamp                                                      ");
            //query.AppendLine("           ,sum(d.value) value                                                                                    ");
            //query.AppendLine("         from cm_location a                                                                                       ");
            //query.AppendLine("             ,if_ihtags b                                                                                         ");
            //query.AppendLine("             ,if_tag_gbn c                                                                                        ");
            //query.AppendLine("             ,if_accumulation_hour d                                                                              ");
            //query.AppendLine("        where a.res_code = :LOC_CODE                                                                              ");
            //query.AppendLine("          and b.loc_code =                                                                                        ");
            //query.AppendLine("              decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            //query.AppendLine("          and c.tagname = b.tagname                                                                               ");
            //query.AppendLine("          and c.tag_gbn = 'FRQ_O'                                                                                 ");
            //query.AppendLine("        and d.tagname = c.tagname                                                                                 ");
            //query.AppendLine("      group by to_char(d.timestamp-(1/24),'yyyymmdd')                                                             ");
            
            query.AppendLine("     ) b                                                                                                          ");
            query.AppendLine(" where a.timestamp(+) = temp.timestamp                                                                            ");
            query.AppendLine("   and a.time(+) = temp.time                                                                                      ");
            query.AppendLine("   and b.timestamp(+) = temp.timestamp                                                                            ");
            query.AppendLine(" group by temp.timestamp                                                                                          ");
            query.AppendLine(" order by temp.timestamp                                                                                          ");

            IDataParameter[] parameters =  {
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("STUDYDAY", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TARGETDATE"];
            parameters[1].Value = parameter["STUDYDAY"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        /// <summary>
        /// 수요예측에 필요한 데이터를 조회한다.
        /// 01시의 적산값이 00~01사이의 값이라면 SQL에서 1시간을 빼서 조회해야한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object SelectDfSimulationTimeDataBlock(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select sum(round(nvl(value,0),2))                                                                                 ");
            query.AppendLine("  from (                                                                                                          ");
            query.AppendLine("       select d.timestamp - (1/24) timestamp                                                                      ");
            query.AppendLine("             ,d.value                                                                                             ");
            query.AppendLine("         from cm_location a                                                                                       ");
            query.AppendLine("             ,if_ihtags b                                                                                         ");
            query.AppendLine("             ,if_tag_gbn c                                                                                        ");
            query.AppendLine("             ,if_accumulation_hour d                                                                              ");
            query.AppendLine("        where a.loc_code = :LOC_CODE                                                                              ");
            query.AppendLine("          and b.loc_code =                                                                                        ");
            query.AppendLine("              decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            query.AppendLine("          and c.tagname = b.tagname                                                                               ");
            query.AppendLine("          and c.tag_gbn = 'FRQ'                                                                                   ");
            query.AppendLine("          and d.tagname = c.tagname                                                                               ");
            query.AppendLine(" )                                                                                                                ");
            query.AppendLine("  where timestamp = to_date(:TARGETDATE||:TARGERTIME,'yyyymmddhh24')                                              ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGERTIME", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["TARGETDATE"];
            parameters[2].Value = parameter["TARGERTIME"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        /// <summary>
        /// 수요예측에 필요한 데이터를 조회한다.
        /// 01시의 적산값이 00~01사이의 값이라면 SQL에서 1시간을 빼서 조회해야한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object SelectDfSimulationTimeDataReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select round(nvl(value,0),2)                                                                                      ");
            query.AppendLine("  from (                                                                                                          ");
            
            query.AppendLine("       select d.timestamp - (1/24) timestamp                                                                      ");
            query.AppendLine("             ,d.value                                                                                             ");
            query.AppendLine("         from if_accumulation_hour d                                                                              ");
            query.AppendLine("        where d.tagname = (select out_flow_tag from we_supply_area where supy_ftr_idn = :LOC_CODE )               ");

            //query.AppendLine("       select d.timestamp - (1/24) timestamp                                                                      ");
            //query.AppendLine("             ,d.value                                                                                             ");
            //query.AppendLine("         from cm_location a                                                                                       ");
            //query.AppendLine("             ,if_ihtags b                                                                                         ");
            //query.AppendLine("             ,if_tag_gbn c                                                                                        ");
            //query.AppendLine("             ,if_accumulation_hour d                                                                              ");
            //query.AppendLine("        where a.res_code = :LOC_CODE                                                                              ");
            //query.AppendLine("          and b.loc_code =                                                                                        ");
            //query.AppendLine("              decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            //query.AppendLine("          and c.tagname = b.tagname                                                                               ");
            //query.AppendLine("          and c.tag_gbn = 'FRQ_O'                                                                                 ");
            //query.AppendLine("          and d.tagname = c.tagname                                                                               ");
            
            query.AppendLine(" )                                                                                                                ");
            query.AppendLine("  where timestamp = to_date(:TARGETDATE||:TARGERTIME,'yyyymmddhh24')                                              ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGERTIME", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["TARGETDATE"];
            parameters[2].Value = parameter["TARGERTIME"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //일예측값 수정
        public void UpdateDfSimulationUse(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into df_setting a                                                         ");
            query.AppendLine("using (select :LOC_CODE loc_code                                                ");
            query.AppendLine("             ,:USE_YN use_yn                                                    ");
            query.AppendLine("         from dual                                                              ");
            query.AppendLine("      ) b                                                                       ");
            query.AppendLine("   on (a.loc_code = b.loc_code)	                                              ");
            query.AppendLine(" when matched then                                                              ");
            query.AppendLine("      update set a.use_yn = b.use_yn                                            ");
            query.AppendLine(" when not matched then                                                          ");
            query.AppendLine("      insert (a.loc_code, a.use_yn)                                             ");
            query.AppendLine("      values (b.loc_code, b.use_yn)                                             ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("USE_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["USE_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //일예측값 수정
        public void UpdateDayPrediction(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into df_result_day a                                                      ");
            query.AppendLine("using (select :LOC_CODE loc_code												  ");
            query.AppendLine("             ,:DF_RL_DT df_rl_dt												  ");
            query.AppendLine("             ,:DF_TDFQ df_tdfq												  ");
            query.AppendLine("         from dual															  ");
            query.AppendLine("      ) b																		  ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.df_rl_dt = to_date(b.df_rl_dt,'yyyymmdd')) ");
            query.AppendLine(" when matched then															  ");
            query.AppendLine("      update set a.df_tdfq = b.df_tdfq										  ");
            query.AppendLine(" when not matched then														  ");
            query.AppendLine("      insert (a.loc_code, a.df_rl_dt, a.df_tdfq)								  ");
            query.AppendLine("      values (b.loc_code, to_date(b.df_rl_dt,'yyyymmdd'), b.df_tdfq)			  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("DF_RL_DT", OracleDbType.Varchar2),
                     new OracleParameter("DF_TDFQ", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DF_RL_DT"];
            parameters[2].Value = parameter["DF_TDFQ"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //시간예측값 수정
        public void UpdateHourPrediction(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into df_result_hour a                                                                                    ");
            query.AppendLine("using (select :LOC_CODE loc_code																				 ");
            query.AppendLine("             ,:DF_RL_DT df_rl_dt																				 ");
            query.AppendLine("             ,:DF_RL_HOUR df_rl_hour																			 ");
            query.AppendLine("             ,:DF_THFQ df_thfq																				 ");
            query.AppendLine("         from dual																							 ");
            query.AppendLine("      ) b																										 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.df_rl_dt = to_date(b.df_rl_dt,'yyyymmdd') and a.df_rl_hour = b.df_rl_hour)");
            query.AppendLine(" when matched then																							 ");
            query.AppendLine("      update set a.df_thfq = b.df_thfq																		 ");
            query.AppendLine(" when not matched then																						 ");
            query.AppendLine("      insert (a.loc_code, a.df_rl_dt, a.df_rl_hour, a.df_thfq)													 ");
            query.AppendLine("      values (b.loc_code, to_date(b.df_rl_dt,'yyyymmdd'), b.df_rl_hour ,b.df_thfq)	                             ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("DF_RL_DT", OracleDbType.Varchar2),
                     new OracleParameter("DF_RL_HOUR", OracleDbType.Varchar2),
                     new OracleParameter("DF_THFQ", OracleDbType.Varchar2)

                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DF_RL_DT"];
            parameters[2].Value = parameter["DF_RL_HOUR"];
            parameters[3].Value = parameter["DF_THFQ"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 유량계의 태그 존재여부를 조회한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object IsTagErrorBlock(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select (select max(iih.tagname) from if_ihtags iih, if_tag_gbn itg where iih.loc_code = loc.tag_loc_code and iih.tagname = itg.tagname and itg.tag_gbn = 'FRQ') out_flow_tag ");
            query.AppendLine("  from (																																									   ");
            query.AppendLine("       select decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code										   ");
            query.AppendLine("         from cm_location c1																																				   ");
            query.AppendLine("        where loc_code = :LOC_CODE																																			   ");
            query.AppendLine("       ) loc																																								   ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        /// <summary>
        /// 배수지의 태그 존재여부를 조회한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object IsTagErrorReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select out_flow_tag tagname                                                                                 ");
            query.AppendLine("  from we_supply_area                                                                                       ");
            query.AppendLine(" where supy_ftr_idn = :LOC_CODE                                                                             ");
            
            //query.AppendLine("select c.tagname                                                                                            ");
            //query.AppendLine("  from cm_location a                                                                                        ");
            //query.AppendLine("      ,if_ihtags b                                                                                          ");
            //query.AppendLine("      ,if_tag_gbn c                                                                                         ");
            //query.AppendLine(" where a.res_code = :LOC_CODE                                                                               ");
            //query.AppendLine("   and b.loc_code =                                                                                         ");
            //query.AppendLine("       decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code) ");
            //query.AppendLine("   and c.tagname = b.tagname                                                                                ");
            //query.AppendLine("   and c.tag_gbn = 'FRQ_O'                                                                                  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }
    }
}
