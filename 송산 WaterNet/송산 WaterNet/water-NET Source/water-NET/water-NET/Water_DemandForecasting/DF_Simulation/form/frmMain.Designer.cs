﻿namespace WaterNet.DF_Simulation.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.sblock = new System.Windows.Forms.ComboBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.mblock = new System.Windows.Forms.ComboBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.lblock = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.gubun = new System.Windows.Forms.ComboBox();
            this.enddate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label5 = new System.Windows.Forms.Label();
            this.startdate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.logicBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.useupdateBtn = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pnlPropertyBase = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grpMRM = new System.Windows.Forms.GroupBox();
            this.ultraGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.grpNN = new System.Windows.Forms.GroupBox();
            this.neuron = new System.Windows.Forms.TextBox();
            this.iterations = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.learningrate = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.momentum = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.ultraGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.setting = new System.Windows.Forms.GroupBox();
            this.studyday = new System.Windows.Forms.TextBox();
            this.studypattern = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.daykind = new System.Windows.Forms.CheckBox();
            this.timeforecasting = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.abnormaldata = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.studymethod = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.texDfLog = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enddate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pnlPropertyBase.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grpMRM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).BeginInit();
            this.grpNN.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.setting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(937, 10);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 658);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(927, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 658);
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 658);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(917, 10);
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel8);
            this.groupBox1.Controls.Add(this.panel7);
            this.groupBox1.Controls.Add(this.panel6);
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Controls.Add(this.enddate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.startdate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(917, 83);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.sblock);
            this.panel8.Location = new System.Drawing.Point(727, 20);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(213, 22);
            this.panel8.TabIndex = 28;
            this.panel8.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "소블록";
            // 
            // sblock
            // 
            this.sblock.FormattingEnabled = true;
            this.sblock.Location = new System.Drawing.Point(50, 0);
            this.sblock.Name = "sblock";
            this.sblock.Size = new System.Drawing.Size(161, 20);
            this.sblock.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.mblock);
            this.panel7.Location = new System.Drawing.Point(508, 20);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(213, 22);
            this.panel7.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "중블록";
            // 
            // mblock
            // 
            this.mblock.FormattingEnabled = true;
            this.mblock.Location = new System.Drawing.Point(50, 0);
            this.mblock.Name = "mblock";
            this.mblock.Size = new System.Drawing.Size(161, 20);
            this.mblock.TabIndex = 5;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.lblock);
            this.panel6.Location = new System.Drawing.Point(289, 20);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(213, 22);
            this.panel6.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "대블록";
            // 
            // lblock
            // 
            this.lblock.FormattingEnabled = true;
            this.lblock.Location = new System.Drawing.Point(50, 0);
            this.lblock.Name = "lblock";
            this.lblock.Size = new System.Drawing.Size(161, 20);
            this.lblock.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.gubun);
            this.panel4.Location = new System.Drawing.Point(6, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(192, 22);
            this.panel4.TabIndex = 33;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "관리구분";
            // 
            // gubun
            // 
            this.gubun.FormattingEnabled = true;
            this.gubun.Location = new System.Drawing.Point(66, 0);
            this.gubun.Name = "gubun";
            this.gubun.Size = new System.Drawing.Size(124, 20);
            this.gubun.TabIndex = 5;
            // 
            // enddate
            // 
            this.enddate.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.enddate.Location = new System.Drawing.Point(183, 20);
            this.enddate.Name = "enddate";
            this.enddate.Size = new System.Drawing.Size(100, 21);
            this.enddate.TabIndex = 31;
            this.enddate.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 12);
            this.label5.TabIndex = 30;
            this.label5.Text = "-";
            // 
            // startdate
            // 
            this.startdate.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.startdate.Location = new System.Drawing.Point(67, 20);
            this.startdate.Name = "startdate";
            this.startdate.Size = new System.Drawing.Size(100, 21);
            this.startdate.TabIndex = 29;
            this.startdate.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 28;
            this.label6.Text = "검색기간";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox7.Location = new System.Drawing.Point(10, 93);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(917, 30);
            this.pictureBox7.TabIndex = 30;
            this.pictureBox7.TabStop = false;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.Location = new System.Drawing.Point(886, 96);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 33;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // updateBtn
            // 
            this.updateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.updateBtn.Location = new System.Drawing.Point(791, 96);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(89, 25);
            this.updateBtn.TabIndex = 34;
            this.updateBtn.TabStop = false;
            this.updateBtn.Text = "기본설정저장";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // logicBtn
            // 
            this.logicBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.logicBtn.Location = new System.Drawing.Point(588, 96);
            this.logicBtn.Name = "logicBtn";
            this.logicBtn.Size = new System.Drawing.Size(102, 25);
            this.logicBtn.TabIndex = 35;
            this.logicBtn.TabStop = false;
            this.logicBtn.Text = "수요예측재실행";
            this.logicBtn.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(10, 387);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(917, 271);
            this.panel1.TabIndex = 38;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(917, 23);
            this.panel2.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 8F);
            this.label4.Location = new System.Drawing.Point(87, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 11);
            this.label4.TabIndex = 31;
            this.label4.Text = ": 오류";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Red;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Location = new System.Drawing.Point(62, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(21, 10);
            this.panel5.TabIndex = 30;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Blue;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(-2, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(21, 10);
            this.panel3.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 8F);
            this.label2.Location = new System.Drawing.Point(23, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 11);
            this.label2.TabIndex = 27;
            this.label2.Text = ": 정상";
            // 
            // ultraGrid1
            // 
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Location = new System.Drawing.Point(10, 123);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(917, 264);
            this.ultraGrid1.TabIndex = 40;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // useupdateBtn
            // 
            this.useupdateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.useupdateBtn.Location = new System.Drawing.Point(696, 96);
            this.useupdateBtn.Name = "useupdateBtn";
            this.useupdateBtn.Size = new System.Drawing.Size(89, 25);
            this.useupdateBtn.TabIndex = 41;
            this.useupdateBtn.TabStop = false;
            this.useupdateBtn.Text = "실행여부저장";
            this.useupdateBtn.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(16, 101);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 16);
            this.checkBox1.TabIndex = 42;
            this.checkBox1.Text = "실행여부";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 23);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(917, 248);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pnlPropertyBase);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(909, 222);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "기본설정";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pnlPropertyBase
            // 
            this.pnlPropertyBase.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPropertyBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPropertyBase.Controls.Add(this.tabControl1);
            this.pnlPropertyBase.Controls.Add(this.pictureBox10);
            this.pnlPropertyBase.Controls.Add(this.setting);
            this.pnlPropertyBase.Controls.Add(this.pictureBox9);
            this.pnlPropertyBase.Controls.Add(this.pictureBox8);
            this.pnlPropertyBase.Controls.Add(this.pictureBox6);
            this.pnlPropertyBase.Controls.Add(this.pictureBox5);
            this.pnlPropertyBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPropertyBase.Location = new System.Drawing.Point(3, 3);
            this.pnlPropertyBase.Name = "pnlPropertyBase";
            this.pnlPropertyBase.Size = new System.Drawing.Size(903, 216);
            this.pnlPropertyBase.TabIndex = 180;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl1.Location = new System.Drawing.Point(371, 10);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(480, 194);
            this.tabControl1.TabIndex = 177;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.grpMRM);
            this.tabPage1.Controls.Add(this.grpNN);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(472, 168);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "알고리즘 설정";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // grpMRM
            // 
            this.grpMRM.Controls.Add(this.ultraGrid2);
            this.grpMRM.Location = new System.Drawing.Point(244, 8);
            this.grpMRM.Name = "grpMRM";
            this.grpMRM.Size = new System.Drawing.Size(217, 166);
            this.grpMRM.TabIndex = 169;
            this.grpMRM.TabStop = false;
            this.grpMRM.Text = "MRM설정";
            // 
            // ultraGrid2
            // 
            this.ultraGrid2.Location = new System.Drawing.Point(10, 20);
            this.ultraGrid2.Name = "ultraGrid2";
            this.ultraGrid2.Size = new System.Drawing.Size(198, 137);
            this.ultraGrid2.TabIndex = 0;
            this.ultraGrid2.Text = "ultraGrid2";
            // 
            // grpNN
            // 
            this.grpNN.Controls.Add(this.neuron);
            this.grpNN.Controls.Add(this.iterations);
            this.grpNN.Controls.Add(this.label20);
            this.grpNN.Controls.Add(this.label17);
            this.grpNN.Controls.Add(this.learningrate);
            this.grpNN.Controls.Add(this.label18);
            this.grpNN.Controls.Add(this.momentum);
            this.grpNN.Controls.Add(this.label19);
            this.grpNN.Location = new System.Drawing.Point(16, 8);
            this.grpNN.Name = "grpNN";
            this.grpNN.Size = new System.Drawing.Size(217, 166);
            this.grpNN.TabIndex = 168;
            this.grpNN.TabStop = false;
            this.grpNN.Text = "NN설정";
            // 
            // neuron
            // 
            this.neuron.Font = new System.Drawing.Font("Arial", 9F);
            this.neuron.Location = new System.Drawing.Point(131, 34);
            this.neuron.Name = "neuron";
            this.neuron.Size = new System.Drawing.Size(65, 21);
            this.neuron.TabIndex = 1;
            this.neuron.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // iterations
            // 
            this.iterations.Font = new System.Drawing.Font("Arial", 9F);
            this.iterations.Location = new System.Drawing.Point(131, 115);
            this.iterations.Name = "iterations";
            this.iterations.Size = new System.Drawing.Size(65, 21);
            this.iterations.TabIndex = 7;
            this.iterations.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(52, 93);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 12);
            this.label20.TabIndex = 4;
            this.label20.Text = "모멘텀계수 :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(36, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "레이어 뉴런수 :";
            // 
            // learningrate
            // 
            this.learningrate.Font = new System.Drawing.Font("Arial", 9F);
            this.learningrate.Location = new System.Drawing.Point(131, 61);
            this.learningrate.Name = "learningrate";
            this.learningrate.Size = new System.Drawing.Size(65, 21);
            this.learningrate.TabIndex = 3;
            this.learningrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(52, 117);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 12);
            this.label18.TabIndex = 6;
            this.label18.Text = "학습반복수 :";
            // 
            // momentum
            // 
            this.momentum.Font = new System.Drawing.Font("Arial", 9F);
            this.momentum.Location = new System.Drawing.Point(131, 88);
            this.momentum.Name = "momentum";
            this.momentum.Size = new System.Drawing.Size(65, 21);
            this.momentum.TabIndex = 5;
            this.momentum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(65, 66);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 12);
            this.label19.TabIndex = 2;
            this.label19.Text = "학습계수 :";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(472, 169);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "이상치 파라미터 설정";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.ultraGrid3);
            this.groupBox8.Location = new System.Drawing.Point(13, 9);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(276, 172);
            this.groupBox8.TabIndex = 170;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "이상치 파라미터 설정";
            // 
            // ultraGrid3
            // 
            this.ultraGrid3.Location = new System.Drawing.Point(13, 19);
            this.ultraGrid3.Name = "ultraGrid3";
            this.ultraGrid3.Size = new System.Drawing.Size(251, 142);
            this.ultraGrid3.TabIndex = 0;
            this.ultraGrid3.Text = "ultraGrid3";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox10.Location = new System.Drawing.Point(361, 10);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(10, 194);
            this.pictureBox10.TabIndex = 183;
            this.pictureBox10.TabStop = false;
            // 
            // setting
            // 
            this.setting.Controls.Add(this.studyday);
            this.setting.Controls.Add(this.studypattern);
            this.setting.Controls.Add(this.label14);
            this.setting.Controls.Add(this.label13);
            this.setting.Controls.Add(this.label9);
            this.setting.Controls.Add(this.label8);
            this.setting.Controls.Add(this.daykind);
            this.setting.Controls.Add(this.timeforecasting);
            this.setting.Controls.Add(this.label7);
            this.setting.Controls.Add(this.abnormaldata);
            this.setting.Controls.Add(this.label3);
            this.setting.Controls.Add(this.studymethod);
            this.setting.Controls.Add(this.label1);
            this.setting.Dock = System.Windows.Forms.DockStyle.Left;
            this.setting.Location = new System.Drawing.Point(10, 10);
            this.setting.Name = "setting";
            this.setting.Size = new System.Drawing.Size(351, 194);
            this.setting.TabIndex = 178;
            this.setting.TabStop = false;
            this.setting.Text = "수요예측 기본설정";
            // 
            // studyday
            // 
            this.studyday.Font = new System.Drawing.Font("Arial", 9F);
            this.studyday.Location = new System.Drawing.Point(111, 143);
            this.studyday.Name = "studyday";
            this.studyday.Size = new System.Drawing.Size(102, 21);
            this.studyday.TabIndex = 189;
            this.studyday.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // studypattern
            // 
            this.studypattern.Font = new System.Drawing.Font("Arial", 9F);
            this.studypattern.Location = new System.Drawing.Point(111, 171);
            this.studypattern.Name = "studypattern";
            this.studypattern.Size = new System.Drawing.Size(102, 21);
            this.studypattern.TabIndex = 8;
            this.studypattern.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(217, 176);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 188;
            this.label14.Text = "일";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 176);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 12);
            this.label13.TabIndex = 186;
            this.label13.Text = "학습데이터 패턴";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(217, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 185;
            this.label9.Text = "일";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 12);
            this.label8.TabIndex = 183;
            this.label8.Text = "학습데이터 일수";
            // 
            // daykind
            // 
            this.daykind.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.daykind.Location = new System.Drawing.Point(23, 110);
            this.daykind.Name = "daykind";
            this.daykind.Size = new System.Drawing.Size(102, 28);
            this.daykind.TabIndex = 182;
            this.daykind.Text = "요일정보 사용";
            this.daykind.UseVisualStyleBackColor = true;
            // 
            // timeforecasting
            // 
            this.timeforecasting.FormattingEnabled = true;
            this.timeforecasting.Location = new System.Drawing.Point(111, 83);
            this.timeforecasting.Name = "timeforecasting";
            this.timeforecasting.Size = new System.Drawing.Size(199, 20);
            this.timeforecasting.TabIndex = 181;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 12);
            this.label7.TabIndex = 180;
            this.label7.Text = "시간예측알고리즘";
            // 
            // abnormaldata
            // 
            this.abnormaldata.FormattingEnabled = true;
            this.abnormaldata.Location = new System.Drawing.Point(111, 53);
            this.abnormaldata.Name = "abnormaldata";
            this.abnormaldata.Size = new System.Drawing.Size(199, 20);
            this.abnormaldata.TabIndex = 179;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 12);
            this.label3.TabIndex = 178;
            this.label3.Text = "이상치 보정법";
            // 
            // studymethod
            // 
            this.studymethod.FormattingEnabled = true;
            this.studymethod.Location = new System.Drawing.Point(110, 23);
            this.studymethod.Name = "studymethod";
            this.studymethod.Size = new System.Drawing.Size(199, 20);
            this.studymethod.TabIndex = 177;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "구동모드";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox9.Location = new System.Drawing.Point(891, 10);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 194);
            this.pictureBox9.TabIndex = 182;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox8.Location = new System.Drawing.Point(0, 10);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 194);
            this.pictureBox8.TabIndex = 181;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox6.Location = new System.Drawing.Point(0, 204);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(901, 10);
            this.pictureBox6.TabIndex = 180;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(901, 10);
            this.pictureBox5.TabIndex = 179;
            this.pictureBox5.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.texDfLog);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(909, 222);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "실행로그";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // texDfLog
            // 
            this.texDfLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.texDfLog.Location = new System.Drawing.Point(3, 3);
            this.texDfLog.Name = "texDfLog";
            this.texDfLog.Size = new System.Drawing.Size(903, 216);
            this.texDfLog.TabIndex = 0;
            this.texDfLog.Text = "";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 668);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.useupdateBtn);
            this.Controls.Add(this.ultraGrid1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.logicBtn);
            this.Controls.Add(this.updateBtn);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmMain";
            this.Text = "수요예측관리";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enddate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.pnlPropertyBase.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.grpMRM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).EndInit();
            this.grpNN.ResumeLayout(false);
            this.grpNN.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.setting.ResumeLayout(false);
            this.setting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Button logicBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor enddate;
        private System.Windows.Forms.Label label5;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor startdate;
        private System.Windows.Forms.Label label6;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Button useupdateBtn;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox gubun;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox mblock;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox lblock;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox sblock;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel pnlPropertyBase;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox grpMRM;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid2;
        private System.Windows.Forms.GroupBox grpNN;
        public System.Windows.Forms.TextBox neuron;
        public System.Windows.Forms.TextBox iterations;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox learningrate;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox momentum;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox8;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid3;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.GroupBox setting;
        public System.Windows.Forms.TextBox studyday;
        public System.Windows.Forms.TextBox studypattern;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.CheckBox daykind;
        public System.Windows.Forms.ComboBox timeforecasting;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox abnormaldata;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox studymethod;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.RichTextBox texDfLog;
    }
}