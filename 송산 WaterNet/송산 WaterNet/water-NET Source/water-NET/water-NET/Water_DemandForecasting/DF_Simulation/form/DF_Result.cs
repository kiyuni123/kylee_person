﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.DF_Simulation.form
{
    public delegate void DelegateShowDFResult(string s);

    public partial class DF_Result : Form
    {
        public bool IsClose
        {
            set
            {
                this.ControlBox = value;
            }
        }

        public DelegateShowDFResult DelegateShowDFResultInstance = null;

        public DF_Result()
        {
            DelegateShowDFResultInstance = new DelegateShowDFResult(Add);
            InitializeComponent();
        }

        public void Add(string text)
        {
            this.richTextBox1.AppendText("\n" + text);
        }
    }
}
