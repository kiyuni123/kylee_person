﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.control;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.manager;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.DF_Simulation.work;
using WaterNet.WV_Common.enum1;
using WaterNet.DF_Simulation.data;
using WaterNet.DF_Simulation.control;
using WaterNet.DF_Algorithm.Simulation;
using System.Threading;
using WaterNet.WaterNetCore;
using WaterNet.DF_Common.interface1;
using EMFrame.log;

namespace WaterNet.DF_Simulation.form
{
    public partial class frmMain : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        //MainMap 을 제어하기 위한 인스턴스 변수
        private IMapControl mainMap = null;

        private BlockComboboxControl blockControl = null;

        private Hashtable currentThread = new Hashtable();

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControl MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================수요예측관리

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuManage"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
                this.useupdateBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeGrid();
            this.InitializeGridColumn();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);
            this.gridManager.Add(this.ultraGrid3);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid3.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Override.RowSizing = RowSizing.Fixed;
            this.ultraGrid1.DisplayLayout.Override.RowSizingArea = RowSizingArea.RowBordersOnly;

            this.ultraGrid1.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region GRID1 컬럼추가 내용
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LBLOCK";
                column.Header.Caption = "대블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("MBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "MBLOCK";
                column.Header.Caption = "중블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("SBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "SBLOCK";
                column.Header.Caption = "소블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 250;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측\n실행여부";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 5;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            #endregion

            #region GRID2 컬럼추가 내용
            if (this.ultraGrid2.DisplayLayout.Bands[0].Columns.IndexOf("ATTRIBUTE") == -1)
            {
                column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "ATTRIBUTE";
                column.Header.Caption = "Attribute";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.CellSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 60;
                column.Hidden = false;
            }

            if (this.ultraGrid2.DisplayLayout.Bands[0].Columns.IndexOf("VALUE") == -1)
            {
                column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "VALUE";
                column.Header.Caption = "Order";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.EditAndSelectText;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 80;
                column.Hidden = false;
            }
            #endregion

            #region GRID2 컬럼추가 내용
            if (this.ultraGrid3.DisplayLayout.Bands[0].Columns.IndexOf("TIME") == -1)
            {
                column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "TIME";
                column.Header.Caption = "시간";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.CellSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 60;
                column.Hidden = false;
            }

            if (this.ultraGrid3.DisplayLayout.Bands[0].Columns.IndexOf("ALPHA") == -1)
            {
                column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "ALPHA";
                column.Header.Caption = "Alpha";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.EditAndSelectText;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 100;
                column.Hidden = false;
            }

            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            this.ultraGrid1.DisplayLayout.ValueLists.Clear();
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN") &&
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") != -1)
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }

            ComboBoxUtils.SetMember(this.gubun, "CODE", "CODE_NAME");
            DataTable gubun = new DataTable();
            gubun.Columns.Add("CODE");
            gubun.Columns.Add("CODE_NAME");
            gubun.Rows.Add("1", "블록");
            gubun.Rows.Add("2", "배수지");
            this.gubun.DataSource = gubun;

            Utils.SetValueList(this.studymethod, VALUELIST_TYPE.CODE, "5001");
            Utils.SetValueList(this.abnormaldata, VALUELIST_TYPE.CODE, "5002");
            Utils.SetValueList(this.timeforecasting, VALUELIST_TYPE.CODE, "5003");
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.Disposed += new EventHandler(frmMain_Disposed);

            this.useupdateBtn.Click += new EventHandler(useupdateBtn_Click);
            this.checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);

            this.logicBtn.Click += new EventHandler(logicBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);

            this.studypattern.TextChanged += new EventHandler(studypattern_TextChanged);
            //this.ultraGrid1.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid1_DoubleClickRow);
        }

        private void frmMain_Disposed(object sender, EventArgs e)
        {
            try
            {
                if (this.currentThread.ContainsKey("manualThread"))
                {
                    ((Thread)(currentThread["manualThread"])).Abort();
                }
            }
            catch (ThreadAbortException ex)
            {
            }
        }

        private void useupdateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.parameter == null)
                {
                    return;
                }

                DfSimulationWork.GetInstance().UpdateDfSimulationUse(this.ultraGrid1);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //자동실행여부
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.parameter == null)
            {
                return;
            }

            this.Cursor = Cursors.WaitCursor;

            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                row.Hidden = false;
                if (this.checkBox1.Checked && row.Cells["USE_YN"].Value.ToString() == "N")
                {
                    row.Hidden = true;
                }
            }
            if (this.ultraGrid1.Rows.Count > 0)
            {
                this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(this.ultraGrid1.Rows[0]);
            }

            this.Cursor = Cursors.Default;
        }

        //수요에측재실행 버튼 이벤트 핸들러
        private void logicBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.parameter == null)
                {
                    return;
                }

                bool IsSelected = false;

                //그리드 컬럼이 bound 인지..
                foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
                {
                    if (!column.IsBound)
                    {
                        foreach (UltraGridCell cell in this.ultraGrid1.Selected.Cells)
                        {
                            if (cell.Column.Equals(column))
                            {
                                IsSelected = true;
                            }
                        }
                    }
                }

                if (!IsSelected)
                {
                    if (this.parameter["GUBUN"].ToString() == "1")
                    {
                        MessageBox.Show("블록별 재실행 일자가 선택되어 있지 않습니다.");
                    }
                    if (this.parameter["GUBUN"].ToString() == "2")
                    {
                        MessageBox.Show("배수지별 재실행 일자가 선택되어 있지 않습니다.");
                    }
                    return;
                }
                else
                {
                    Thread manualThread = new Thread(new ThreadStart(UpdateDfSimulation));
                    this.currentThread["manualThread"] = manualThread;
                    manualThread.Start();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void UpdateDfSimulation()
        {
            try
            {
                MessageBox.Show("수요예측 재실행 프로세스를 실행합니다.");

                this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n\n수요예측 재실행 프로세스를 실행합니다.\n"); });
                this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });

                foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
                {
                    DfControl con = new DfControl();
                    con.TexDfLog = this.texDfLog;

                    if (!column.IsBound)
                    {
                        DateTime targetDate = DateTime.ParseExact(column.Key, "yyyyMMdd", null);

                        foreach (UltraGridCell cell in this.ultraGrid1.Selected.Cells)
                        {
                            if (cell.Column.Equals(column))
                            {
                                if (this.parameter["GUBUN"].ToString() == "1")
                                {
                                    con.AddItem(new DfDataBlock(cell.Row.Cells["LOC_CODE"].Value.ToString()));
                                }
                                if (this.parameter["GUBUN"].ToString() == "2")
                                {
                                    con.AddItem(new DfDataReservoir(cell.Row.Cells["LOC_CODE"].Value.ToString()));
                                }
                            }
                        }

                        con.TargetDate = targetDate;
                        con.Predict_All();
                        con.UpdateDayPrediction();
                        con.UpdateHourPrediction_AllHour();
                    }
                }

                this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n\n수요예측 재실행 프로세스를 종료합니다.\n"); });
                this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });

                MessageBox.Show("수요예측 재실행 프로세스를 종료합니다.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                try
                {
                    this.searchBtn.Invoke((MethodInvoker)delegate { this.searchBtn.PerformClick(); });
                }
                catch (Exception e)
                {

                }
            }
        }

        private Hashtable parameter = null;

        //검색 버튼 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = Utils.ConverToHashtable(this.groupBox1);

                DataRowView selectedItem = null;

                if (this.sblock != null && this.sblock.SelectedIndex > 0)
                {
                    selectedItem = this.sblock.SelectedItem as DataRowView;
                }
                else if (this.mblock != null && this.mblock.SelectedIndex > 0)
                {
                    selectedItem = this.mblock.SelectedItem as DataRowView;
                }
                else
                {
                    selectedItem = this.lblock.SelectedItem as DataRowView;
                }

                if (selectedItem != null)
                {
                    this.parameter["LOC_CODE"] = selectedItem["LOC_CODE"];
                    this.parameter["FTR_IDN"] = selectedItem["FTR_IDN"];
                    this.parameter["FTR_CODE"] = selectedItem["FTR_CODE"];
                    this.parameter["LOC_NAME"] = selectedItem["LOC_NAME"];
                }

                this.SelectDfSimulation();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //수요예측관리 검색
        private void SelectDfSimulation()
        {
            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid1.DataSource = null;

            if (this.gubun.SelectedValue.ToString() == "1")
            {
                this.InitializeGrid();
                this.InitializeBlockGridColumn();
                this.InitializeBlockValueList();

                DataSet dataSet = new DataSet();
                DataTable temp_table = DfSimulationWork.GetInstance().SelectDfSimulationBlock(this.parameter);

                if (temp_table == null)
                {
                    return;
                }

                DataTable result = new DataTable();

                foreach (DataColumn column in temp_table.Columns)
                {
                    result.Columns.Add(column.ColumnName, column.DataType);
                }

                foreach (DataRow row in temp_table.Rows)
                {
                    result.Rows.Add(row.ItemArray);
                }

                DataTable dataTable = result.Copy();

                dataSet.Tables.Add(dataTable);
                dataSet.Tables.Add(result);

                //유량계 정보 제외 나머지 컬럼은 삭제한다.
                for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                {
                    if (dataTable.Columns[i].ColumnName != "LOC_CODE" &&
                        dataTable.Columns[i].ColumnName != "LBLOCK" &&
                        dataTable.Columns[i].ColumnName != "MBLOCK" &&
                        dataTable.Columns[i].ColumnName != "SBLOCK" &&
                        dataTable.Columns[i].ColumnName != "OUT_FLOW_TAG" &&
                        dataTable.Columns[i].ColumnName != "USE_YN"
                        )
                    {
                        dataTable.Columns.Remove(dataTable.Columns[i]);
                    }
                }

                this.ultraGrid1.DataSource = dataSet;
                this.InitGrid();
                this.SetGridColumns();
                this.SetGridData();
            }
            else if (this.gubun.SelectedValue.ToString() == "2")
            {
                this.InitializeGrid();
                this.InitializeReservoirGridColumn();
                this.InitializeReservoirValueList();

                DataSet dataSet = new DataSet();
                DataTable temp_table = DfSimulationWork.GetInstance().SelectDfSimulationReservior(this.parameter);

                if (temp_table == null)
                {
                    return;
                }

                DataTable result = new DataTable();

                foreach (DataColumn column in temp_table.Columns)
                {
                    result.Columns.Add(column.ColumnName, column.DataType);
                }

                foreach (DataRow row in temp_table.Rows)
                {
                    result.Rows.Add(row.ItemArray);
                }

                DataTable dataTable = result.Copy();

                dataSet.Tables.Add(dataTable);
                dataSet.Tables.Add(result);

                //배수지 정보 제외 나머지 컬럼은 삭제한다.
                for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                {
                    if (dataTable.Columns[i].ColumnName != "LOC_CODE" &&
                        dataTable.Columns[i].ColumnName != "FTR_IDN" &&
                        dataTable.Columns[i].ColumnName != "LOC_NAME" &&
                        dataTable.Columns[i].ColumnName != "OUT_FLOW_TAG" &&
                        dataTable.Columns[i].ColumnName != "USE_YN"
                        )
                    {
                        dataTable.Columns.Remove(dataTable.Columns[i]);
                    }
                }

                this.ultraGrid1.DataSource = dataSet;
                this.InitGrid();
                this.SetGridColumns();
                this.SetGridData();
            }

            this.Cursor = Cursors.Default;
        }

        private void InitializeBlockGridColumn()
        {
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LBLOCK";
                column.Header.Caption = "대블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("MBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "MBLOCK";
                column.Header.Caption = "중블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("SBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "SBLOCK";
                column.Header.Caption = "소블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 250;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측\n실행여부";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 5;
                column.RowLayoutColumnInfo.OriginY = 0;
            }
        }

        private void InitializeBlockValueList()
        {
            this.ultraGrid1.DisplayLayout.ValueLists.Clear();
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN") &&
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") != -1)
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }
        }

        private void InitializeReservoirGridColumn()
        {
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("FTR_IDN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "FTR_IDN";
                column.Header.Caption = "배수지관리번호";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 100;
                column.Hidden = true;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_NAME") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_NAME";
                column.Header.Caption = "배수지명";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 130;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 250;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측\n실행여부";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }
        }

        private void InitializeReservoirValueList()
        {
            this.ultraGrid1.DisplayLayout.ValueLists.Clear();
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN") &&
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") != -1)
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }
        }


        //수요예측설정 검색
        private void SelectDfSetting()
        {
            this.Cursor = Cursors.WaitCursor;

            //환경설정영역을 초기화한다.
            Utils.ClearControls(this.setting);
            Utils.ClearControls(this.grpNN);

            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.key;

            //기본설정값조회
            Utils.SetControlDataSetting(DfSimulationWork.GetInstance().SelectDfSimulationSetting(parameter), this.setting);

            //NN설정값조회
            Utils.SetControlDataSetting(DfSimulationWork.GetInstance().SelectDfSimulationSettingNN(parameter), this.grpNN);

            //MRM설정값조회
            DataTable mrm = DfSimulationWork.GetInstance().SelectDfSimulationSettingMRM(parameter);

            if (mrm == null)
            {
                mrm = new DataTable();
                mrm.Columns.Add("ATTRIBUTE");
                mrm.Columns.Add("VALUE");
            }

            int sp = Convert.ToInt32(this.studypattern.Text.Trim());

            if (mrm.Rows.Count < sp)
            {
                for (int i = 1; i <= sp; i++)
                {
                    mrm.Rows.Add(i.ToString(), DfSetting.DefaultSettingMRM);
                }
            }

            this.ultraGrid2.DataSource = mrm;

            //이상치조회
            DataTable outlier = DfSimulationWork.GetInstance().SelectDfSimulationSettingOutlier(parameter);

            if (outlier == null)
            {
                outlier = new DataTable();
                outlier.Columns.Add("TIME");
                outlier.Columns.Add("ALPHA");
            }

            if (outlier.Rows.Count == 0)
            {
                for (int i = 1; i <= 24; i++)
                {
                    outlier.Rows.Add(i.ToString(), DfSetting.DefaultSettingOutlier);
                }
            }
            this.ultraGrid3.DataSource = outlier;

            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 동적으로 생성한 그룹을 삭제한다.
        /// </summary>
        private void InitGrid()
        {
            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Count; i > 0; i--)
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Groups.Remove(i - 1);
            }

            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count; i > 0; i--)
            {
                if (!this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].IsBound)
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns.Remove(i - 1);
                }
            }
        }

        /// <summary>
        /// 그리드내 그룹 컬럼을 설정한다.
        /// </summary>
        private void SetGridColumns()
        {
            UltraGridColumn column = null;
            UltraGridGroup group = null;

            UltraGridLayout layout = this.ultraGrid1.DisplayLayout;

            DateTime st = DateTime.ParseExact(this.parameter["STARTDATE"].ToString(), "yyyyMMdd", null);
            DateTime et = DateTime.ParseExact(this.parameter["ENDDATE"].ToString(), "yyyyMMdd", null);

            int diff = (et - st).Days;

            for (; st <= et; st = st.AddDays(1))
            {
                if (layout.Bands[0].Groups.IndexOf((st.Year + st.Month).ToString()) == -1)
                {
                    group = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                    group.Key = (st.Year + st.Month).ToString();
                    group.Header.Caption = st.Year.ToString() + "년 " + st.Month + "월";
                    group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
                    group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                    group.RowLayoutGroupInfo.LabelSpan = 1;
                    group.RowLayoutGroupInfo.OriginX = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count;
                    group.RowLayoutGroupInfo.OriginY = 0;
                }

                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = st.ToString("yyyyMMdd");
                column.Header.Caption = st.ToString("dd") + "일";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.CellSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Right;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 45;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
                column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
                column.RowLayoutColumnInfo.ParentGroup = group;
                column.CellAppearance.BorderColor = Color.White;
                column.CellAppearance.BorderColor2 = Color.White;
                column.CellAppearance.BorderColor3DBase = Color.White;
                column.CellAppearance.ForeColor = Color.White;
            }
        }

        /// <summary>
        /// 그리드내 데이터 컬럼을 설정한다.
        /// </summary>
        private void SetGridData()
        {
            DataTable dataTable = ((DataSet)this.ultraGrid1.DataSource).Tables[1];

            foreach (DataRow row in dataTable.Rows)
            {
                //유량계ID를 기준으로 첫 행을 찾는다.
                int rowIndex = 0;

                foreach (UltraGridRow grow in this.ultraGrid1.Rows)
                {
                    if (grow.Cells["LOC_CODE"].Value.ToString() == row["LOC_CODE"].ToString())
                    {
                        rowIndex = grow.Index + 1;
                        break;
                    }
                }

                foreach (UltraGridColumn gcolumn in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
                {
                    if (!gcolumn.IsBound)
                    {
                        if (row[gcolumn.Key].ToString() == "T")
                        {
                            this.ultraGrid1.Rows[dataTable.Rows.IndexOf(row)].Cells[gcolumn.Key].Appearance.BackColor = Color.Blue;
                        }

                        if (row[gcolumn.Key].ToString() == "F")
                        {
                            this.ultraGrid1.Rows[dataTable.Rows.IndexOf(row)].Cells[gcolumn.Key].Appearance.BackColor = Color.Red;
                        }
                    }
                }
            }
        }

        //환경설정 저장
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.key == null || this.key == string.Empty)
                {
                    return;
                }
                this.UpdateDfSimulationSetting();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void UpdateDfSimulationSetting()
        {
            this.Cursor = Cursors.WaitCursor;

            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.key;

            Hashtable setting = Utils.ConverToHashtable(this.setting);
            Hashtable nn = Utils.ConverToHashtable(this.grpNN);

            DfSimulationWork.GetInstance().UpdateDfSimulationSetting(parameter, setting, nn, this.ultraGrid2, this.ultraGrid3);

            this.Cursor = Cursors.Default;
        }

        private bool isAfterSelectChange = false;
        private string key = string.Empty;

        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (this.isAfterSelectChange)
            {
                return;
            }

            this.isAfterSelectChange = true;

            if (e.Type.ToString() == "Infragistics.Win.UltraWinGrid.UltraGridRow")
            {
                this.ultraGrid1.Selected.Cells.Clear();
                UltraGridRow row = null;
                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    row = this.ultraGrid1.Selected.Rows[this.ultraGrid1.Selected.Rows.Count - 1];
                }
                
                this.ultraGrid1.Selected.Rows.Clear();

                if (row != null)
                {
                    row.Selected = true;
                }
                
                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    this.key = this.ultraGrid1.Selected.Rows[0].Cells["LOC_CODE"].Value.ToString();
                    this.SelectDfSetting();
                }
            }

            if (e.Type.ToString() == "Infragistics.Win.UltraWinGrid.UltraGridCell")
            {
                foreach (UltraGridCell cell in this.ultraGrid1.Selected.Cells)
                {
                    if (cell.Column.Key == "USE_YN")
                    {
                        cell.Selected = false;
                    }
                }
            }

            this.isAfterSelectChange = false;
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            foreach (UltraGridRow gridRow in e.Layout.Rows)
            {
                if (this.checkBox1.Checked)
                {
                    if (gridRow.Cells["USE_YN"].Value.ToString() == "N")
                    {
                        gridRow.Hidden = true;
                    }
                }

                gridRow.Height = 25;
            }
        }

        private void studypattern_TextChanged(object sender, EventArgs e)
        {
            if (this.ultraGrid2.DataSource == null)
            {
                return;
            }

            for (int i = this.ultraGrid2.Rows.Count - 1; i >= 0; i--)
            {
                this.ultraGrid2.Rows[i].Delete(false);
            }

            if (this.studypattern.Text == string.Empty)
            {
                return;
            }

            for (int i = 1; i <= Convert.ToInt32(this.studypattern.Text.Trim()); i++)
            {
                UltraGridRow row = this.ultraGrid2.DisplayLayout.Bands[0].AddNew();
                row.Cells["ATTRIBUTE"].Value = i.ToString();
                row.Cells["VALUE"].Value = DfSetting.DefaultSettingMRM;
            }
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.blockControl = new BlockComboboxControl(this.lblock, this.mblock, this.sblock);
            this.blockControl.AddDefaultOption(LOCATION_TYPE.All, false);


            this.startdate.Value = DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd");
            this.enddate.Value = DateTime.Now.AddDays(+1).ToString("yyyy-MM-dd");

            this.studyday.MaxLength = 3;
            this.studypattern.MaxLength = 3;
        }

        private void ultraGrid1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            string whereCase = "FTR_IDN = '" + e.Row.Cells["FTR_IDN"].Value.ToString() + "'";
            string layerName = e.Row.Cells["TYPE"].Value.ToString();
            this.mainMap.MoveFocusAt(layerName, whereCase);
        }
    }
}
