﻿using System;
using WaterNet.DF_Simulation.dao;
using WaterNet.WV_Common.interface1;
using System.Collections;
using System.Data;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WaterAOCore;
using WaterNet.WV_Common.util;
using WaterNet.DF_Simulation.data;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;
using WaterNet.WaterNetCore;
using WaterNet.DF_Common.interface1;

namespace WaterNet.DF_Simulation.work
{
    public class DfSimulationWork
    {
        private static DfSimulationWork work = null;
        private DfSimulationDao dao = null;

        private OracleDBManager dbManager = null;

        private DfSimulationWork()
        {
            dao = DfSimulationDao.GetInstance();
            dbManager = new OracleDBManager();
            dbManager.ConnectionString = FunctionManager.GetConnectionString();
        }

        public static DfSimulationWork GetInstance()
        {
            if (work == null)
            {
                work = new DfSimulationWork();
            }
            return work;
        }

        public string SelectReservoirName(string CODE)
        {
            string result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                object temp = dao.SelectReservoirName(this.dbManager, CODE);

                if (temp != null)
                {
                    result = temp.ToString();
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }

        public string SelectBlockName(string CODE)
        {
            string result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                object temp = dao.SelectBlockName(this.dbManager, CODE);

                if (temp != null)
                {
                    result = temp.ToString();
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }

        //수요예측관리 조회
        public DataTable SelectDfSimulationBlock(Hashtable parameter)
        {
            DataTable result = new DataTable();
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationBlock(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                if (result != null)
                {
                    foreach (DataRow dataRow in result.Rows)
                    {
                        parameter["LOC_CODE"] = dataRow["LOC_CODE"].ToString();

                        dataSet = dao.SelectDfSimulationResult(this.dbManager, parameter);

                        if (dataSet.Tables.Count > 0)
                        {
                            DataTable dt = dataSet.Tables[0];

                            foreach (DataRow row in dt.Rows)
                            {
                                string columnName = Convert.ToDateTime(row["DF_RL_DT"]).ToString("yyyyMMdd");

                                if (result.Columns.IndexOf(columnName) == -1)
                                {
                                    result.Columns.Add(columnName);
                                }

                                dataRow[columnName] = row["RESULT"];
                            }
                        }
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }

        //수요예측관리 조회
        public DataTable SelectDfSimulationReservior(Hashtable parameter)
        {
            DataTable result = new DataTable();
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationReservoir(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                if (result != null)
                {
                    foreach (DataRow dataRow in result.Rows)
                    {
                        parameter["LOC_CODE"] = dataRow["LOC_CODE"].ToString();

                        dataSet = dao.SelectDfSimulationResult(this.dbManager, parameter);

                        if (dataSet.Tables.Count > 0)
                        {
                            DataTable dt = dataSet.Tables[0];

                            foreach (DataRow row in dt.Rows)
                            {
                                string columnName = Convert.ToDateTime(row["DF_RL_DT"]).ToString("yyyyMMdd");

                                if (result.Columns.IndexOf(columnName) == -1)
                                {
                                    result.Columns.Add(columnName);
                                }

                                dataRow[columnName] = row["RESULT"];
                            }
                        }
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSettingBlock(Hashtable parameter)
        {
            Hashtable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingBlock(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);

                        if (result["STUDYMETHOD"] == DBNull.Value)
                        {
                            result["STUDYMETHOD"] = DfSetting.DefaultSetting["STUDYMETHOD"];
                        }

                        if (result["ABNORMALDATA"] == DBNull.Value)
                        {
                            result["ABNORMALDATA"] = DfSetting.DefaultSetting["ABNORMALDATA"];
                        }

                        if (result["TIMEFORECASTING"] == DBNull.Value)
                        {
                            result["TIMEFORECASTING"] = DfSetting.DefaultSetting["TIMEFORECASTING"];
                        }

                        if (result["DAYKIND"] == DBNull.Value)
                        {
                            result["DAYKIND"] = DfSetting.DefaultSetting["DAYKIND"];
                        }

                        if (result["STUDYDAY"] == DBNull.Value)
                        {
                            result["STUDYDAY"] = DfSetting.DefaultSetting["STUDYDAY"];
                        }

                        if (result["STUDYPATTERN"] == DBNull.Value)
                        {
                            result["STUDYPATTERN"] = DfSetting.DefaultSetting["STUDYPATTERN"];
                        }
                        if (result["USE_YN"] == DBNull.Value)
                        {
                            result["USE_YN"] = DfSetting.DefaultSetting["USE_YN"];
                        }
                    }
                    else
                    {
                        result = DfSetting.DefaultSetting;
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }

            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSettingReservoir(Hashtable parameter)
        {
            Hashtable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingReservoir(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);

                        if (result["STUDYMETHOD"] == DBNull.Value)
                        {
                            result["STUDYMETHOD"] = DfSetting.DefaultSetting["STUDYMETHOD"];
                        }

                        if (result["ABNORMALDATA"] == DBNull.Value)
                        {
                            result["ABNORMALDATA"] = DfSetting.DefaultSetting["ABNORMALDATA"];
                        }

                        if (result["TIMEFORECASTING"] == DBNull.Value)
                        {
                            result["TIMEFORECASTING"] = DfSetting.DefaultSetting["TIMEFORECASTING"];
                        }

                        if (result["DAYKIND"] == DBNull.Value)
                        {
                            result["DAYKIND"] = DfSetting.DefaultSetting["DAYKIND"];
                        }

                        if (result["STUDYDAY"] == DBNull.Value)
                        {
                            result["STUDYDAY"] = DfSetting.DefaultSetting["STUDYDAY"];
                        }

                        if (result["STUDYPATTERN"] == DBNull.Value)
                        {
                            result["STUDYPATTERN"] = DfSetting.DefaultSetting["STUDYPATTERN"];
                        }
                        if (result["USE_YN"] == DBNull.Value)
                        {
                            result["USE_YN"] = DfSetting.DefaultSetting["USE_YN"];
                        }
                    }
                    else
                    {
                        result = DfSetting.DefaultSetting;
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }

            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSetting(Hashtable parameter)
        {
            Hashtable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSetting(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);

                        if (result["STUDYMETHOD"] == DBNull.Value)
                        {
                            result["STUDYMETHOD"] = DfSetting.DefaultSetting["STUDYMETHOD"];
                        }

                        if (result["ABNORMALDATA"] == DBNull.Value)
                        {
                            result["ABNORMALDATA"] = DfSetting.DefaultSetting["ABNORMALDATA"];
                        }

                        if (result["TIMEFORECASTING"] == DBNull.Value)
                        {
                            result["TIMEFORECASTING"] = DfSetting.DefaultSetting["TIMEFORECASTING"];
                        }

                        if (result["DAYKIND"] == DBNull.Value)
                        {
                            result["DAYKIND"] = DfSetting.DefaultSetting["DAYKIND"];
                        }

                        if (result["STUDYDAY"] == DBNull.Value)
                        {
                            result["STUDYDAY"] = DfSetting.DefaultSetting["STUDYDAY"];
                        }

                        if (result["STUDYPATTERN"] == DBNull.Value)
                        {
                            result["STUDYPATTERN"] = DfSetting.DefaultSetting["STUDYPATTERN"];
                        }
                        if (result["USE_YN"] == DBNull.Value)
                        {
                            result["USE_YN"] = DfSetting.DefaultSetting["USE_YN"];
                        }
                    }
                    else
                    {
                        result = DfSetting.DefaultSetting;
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }

            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSettingNN(Hashtable parameter)
        {
            Hashtable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingNN(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);
                    }
                    else
                    {
                        result = DfSetting.DefaultSettingNN;
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }

            return result;
        }

        //수요예측기본설정 조회
        public DataTable SelectDfSimulationSettingMRM(Hashtable parameter)
        {
            DataTable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingMRM(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }

            return result;
        }

        //수요예측기본설정 조회
        public DataTable SelectDfSimulationSettingOutlier(Hashtable parameter)
        {
            DataTable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingOutlier(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }

            return result;
        }

        public void UpdateDfSimulationUse(UltraGrid use)
        {
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                foreach (UltraGridRow row in use.Rows)
                {
                    Hashtable use_parameter = Utils.ConverToHashtable(row);
                    dao.UpdateDfSimulationUse(this.dbManager, use_parameter);
                }

                this.dbManager.CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
        }

        //수요예측기본설정 수정
        public void UpdateDfSimulationSetting(Hashtable parameter, Hashtable setting, Hashtable nn, UltraGrid mrm, UltraGrid outlier)
        {
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                setting["LOC_CODE"] = parameter["LOC_CODE"];
                setting["USE_YN"] = dao.SelectDfSimulationUse(this.dbManager, parameter);
                nn["LOC_CODE"] = parameter["LOC_CODE"];

                dao.DeleteDfSimulationSetting(this.dbManager, parameter);
                dao.DeleteDfSimulationSettingNN(this.dbManager, parameter);
                dao.DeleteDfSimulationSettingMRM(this.dbManager, parameter);
                dao.DeleteDfSimulationSettingOutlier(this.dbManager, parameter);

                dao.InsertDfSimulationSetting(this.dbManager, setting);
                
                dao.InsertDfSimulationSettingNN(this.dbManager, nn);

                foreach (UltraGridRow row in mrm.Rows)
                {
                    Hashtable mrm_parameter = Utils.ConverToHashtable(row);
                    mrm_parameter["LOC_CODE"] = parameter["LOC_CODE"];
                    dao.InsertDfSimulationSettingMRM(this.dbManager, mrm_parameter);
                }

                foreach (UltraGridRow row in outlier.Rows)
                {
                    Hashtable outlier_parameter = Utils.ConverToHashtable(row);
                    outlier_parameter["LOC_CODE"] = parameter["LOC_CODE"];
                    dao.InsertDfSimulationSettingOutlier(this.dbManager, outlier_parameter);
                }

                this.dbManager.CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
        }

        //수요예측 실측 데이터조회
        public DataTable SelectDfSimulationSettingDataBlock(Hashtable parameter)
        {
            DataTable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingDataBlock(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }

        //수요예측 실측 데이터조회
        public DataTable SelectDfSimulationSettingDataReservoir(Hashtable parameter)
        {
            DataTable result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingDataReservoir(this.dbManager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }

        //수요예측 실측 데이터조회
        public double SelectDfSimulationTimeDataBlock(Hashtable parameter)
        {
            object result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                result = dao.SelectDfSimulationTimeDataBlock(this.dbManager, parameter);

                if (result == DBNull.Value)
                {
                    result = 0;
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return Convert.ToDouble(result);
        }

        //수요예측 실측 데이터조회
        public double SelectDfSimulationTimeDataReservoir(Hashtable parameter)
        {
            object result = null;

            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                result = dao.SelectDfSimulationTimeDataReservoir(this.dbManager, parameter);

                if (result == DBNull.Value)
                {
                    result = 0;
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return Convert.ToDouble(result);
        }

        //일예측량을 등록 및 수정 한다.
        public void UpdateDayPrediction(Hashtable parameter)
        {
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                dao.UpdateDayPrediction(this.dbManager, parameter);

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
        }

        //시간예측량을 등록 및 수정 한다.
        public void UpdateHourPrediction(Hashtable parameter)
        {
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                dao.UpdateHourPrediction(this.dbManager, parameter);

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
        }

        //시간예측량을 등록 및 수정 한다.
        public void UpdateHourPrediction(Hashtable parameter, double[] value)
        {
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                for (int i = 0; i < value.Length; i++)
                {
                    parameter["DF_RL_HOUR"] = i.ToString("00");
                    parameter["DF_THFQ"] = Math.Round(value[i], 3);

                    dao.UpdateHourPrediction(this.dbManager, parameter);
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
        }

        //태그존재여부를 조회한다.
        public bool IsTagErrorBlock(Hashtable parameter)
        {
            bool result = false;
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                object temp = dao.IsTagErrorBlock(this.dbManager, parameter);

                if (temp == null)
                {
                    result = true;
                }
                else if (temp != null)
                {
                    result = false;
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }

        //태그존재여부를 조회한다.
        public bool IsTagErrorReservoir(Hashtable parameter)
        {
            bool result = false;
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();

                object temp = dao.IsTagErrorReservoir(this.dbManager, parameter);

                if (temp == null)
                {
                    result = true;
                }
                else if (temp != null)
                {
                    result = false;
                }

                this.dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                this.dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                if (this.dbManager.Transaction != null)
                {
                    this.dbManager.Transaction.Dispose();
                    this.dbManager.Transaction = null;
                }
                this.dbManager.Close();
            }
            return result;
        }
    }
}
