﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.control;
using Infragistics.Win;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using WaterNet.WV_ReservoirPredictionManage.work;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using ChartFX.WinForms;
using WaterNet.WaterNetCore;
using WaterNet.DF_Common.interface1;
using EMFrame.log;

namespace WaterNet.WV_ReservoirPredictionManage.form
{
    public partial class frmMain : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        //차트 매니저
        private ChartManager chartManager = null;

        //테이블// 그래프 레이아웃 
        private TableLayout tableLayout = null;

        //MainMap 을 제어하기 위한 인스턴스 변수
        private IMapControl mainMap = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControl MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================수요예측관리

            object o = EMFrame.statics.AppStatic.USER_MENU["배수지운영시뮬레이션ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeGrid();
            this.InitializeChart();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.tableLayout = new TableLayout(this.tableLayoutPanel1, this.chartPanel1, this.tablePanel1, this.chartVisibleBtn, this.tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 40;
            this.tableLayout.DefaultTableHeight = 60;

            if (DateTime.Now.Minute < 30)
            {
                this.startDate.Value = DateTime.Now.AddHours(-2).ToString("yyyy-MM-dd HH:00");
            }
            if (DateTime.Now.Minute >= 30)
            {
                this.startDate.Value = DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd HH:00");
            }

            this.sgccd_SelectedIndexChanged(null, null);
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);
            this.gridManager.Add(this.ultraGrid3);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            //this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            this.ultraGrid1.DisplayLayout.Bands[0].ColHeaderLines = 2;

            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance.TextVAlign = VAlign.Middle;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;
            this.ultraGrid2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            this.ultraGrid2.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            this.ultraGrid2.DisplayLayout.Override.HeaderAppearance.TextVAlign = VAlign.Middle;

            this.ultraGrid3.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid3.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;
            this.ultraGrid3.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            this.ultraGrid3.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            this.ultraGrid3.DisplayLayout.Override.HeaderAppearance.TextVAlign = VAlign.Middle;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;
            UltraGridGroup group;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SGCCD";
            column.Header.Caption = "지자체";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SUPY_FTR_IDN";
            column.Header.Caption = "배수지\n관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SERVNM";
            column.Header.Caption = "배수지명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "IN_FLOW_TAG";
            column.Header.Caption = "유입유량적산차태그";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 200;
            column.NullText = "없음";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "OUT_FLOW_TAG";
            column.Header.Caption = "유출유량적산차태그";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 200;
            column.NullText = "없음";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "WR1ST_TAG";
            column.Header.Caption = "1지수위태그";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 200;
            column.NullText = "없음";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "WR2ND_TAG";
            column.Header.Caption = "2지수위태그";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 200;
            column.NullText = "없음";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FAC_VOLUMN";
            column.Header.Caption = "용량";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "HIGH";
            column.Header.Caption = "운영\n최고수위";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "N1";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "LOW";
            column.Header.Caption = "운영\n최저수위";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "N1";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "일자";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Format = "yyyy-MM-dd";
            column.RowLayoutColumnInfo.OriginX = 0;
            column.RowLayoutColumnInfo.OriginY = 0;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 2;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIME";
            column.Header.Caption = "시간";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 30;
            column.RowLayoutColumnInfo.OriginX = 1;
            column.RowLayoutColumnInfo.OriginY = 0;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 2;
            column.Hidden = false;

            group = this.ultraGrid2.DisplayLayout.Bands[0].Groups.Add();
            group.Key = "REALTIME";
            group.Header.Caption = "실시간 Data";
            group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
            group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
            group.RowLayoutGroupInfo.LabelSpan = 1;
            group.RowLayoutGroupInfo.OriginX = 2;
            group.RowLayoutGroupInfo.OriginY = 0;
            group.RowLayoutGroupInfo.SpanX = 1;
            group.RowLayoutGroupInfo.SpanY = 1;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "IN_FLOW";
            column.Header.Caption = "유입유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "OUT_FLOW";
            column.Header.Caption = "유출유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "LEI";
            column.Header.Caption = "수위(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.Format = "###,###,###.##";

            group = this.ultraGrid2.DisplayLayout.Bands[0].Groups.Add();
            group.Key = "RESERVIOR";
            group.Header.Caption = "배수지 운영 시뮬레이션";
            group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
            group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
            group.RowLayoutGroupInfo.LabelSpan = 1;
            group.RowLayoutGroupInfo.SpanX = 1;
            group.RowLayoutGroupInfo.SpanY = 1;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "IN_FLOW_CHANGE";
            column.Header.Caption = "유입유량변경(㎥/h)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.Format = "###,###,##0";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "LEI_CHANGE";
            column.Header.Caption = "수위변경(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = true;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.Format = "N2";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "P_OUT_FLOW";
            column.Header.Caption = "유출수요예측량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "P_LEI";
            column.Header.Caption = "예측수위(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.Format = "N2";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CS_IN_FLOW";
            column.Header.Caption = "누적 유입유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CS_FLOW";
            column.Header.Caption = "누적 유출유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PS_OUT_FLOW";
            column.Header.Caption = "누적 예측유출유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FAC_VOLUMN";
            column.Header.Caption = "용량";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = true;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "HIGH";
            column.Header.Caption = "HIGH";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "LOW";
            column.Header.Caption = "LOW";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = true;



            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TODAY_PREDICTION";
            column.Header.Caption = "금일 수요예측량(㎥/일)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.RowLayoutColumnInfo.OriginX = 0;
            column.RowLayoutColumnInfo.OriginY = 0;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 2;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CS_IN_FLOW";
            column.Header.Caption = "금일 실시간 공급량(㎥/일)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.RowLayoutColumnInfo.OriginX = 1;
            column.RowLayoutColumnInfo.OriginY = 0;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 2;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "D_FLOW";
            column.Header.Caption = "잔여 공급 예정량(㎥)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.RowLayoutColumnInfo.OriginX = 2;
            column.RowLayoutColumnInfo.OriginY = 0;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 2;
            column.Hidden = false;
            column.Format = "###,###,###";

            group = this.ultraGrid3.DisplayLayout.Bands[0].Groups.Add();
            group.Key = "TEMP";
            group.Header.Caption = "현재운영유지시";
            group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
            group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
            group.RowLayoutGroupInfo.LabelSpan = 1;
            group.RowLayoutGroupInfo.OriginX = 3;
            group.RowLayoutGroupInfo.OriginY = 0;
            group.RowLayoutGroupInfo.SpanX = 1;
            group.RowLayoutGroupInfo.SpanY = 1;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "HIGH_TIME";
            column.Header.Caption = "High 도달시간";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.Hidden = false;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "LOW_TIME";
            column.Header.Caption = "Low 도달시간";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.ParentGroup = group;
            column.Hidden = false;

            #endregion
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            DataTable sgc = ReservoirPredictionManageWork.GetInstance().SelectSgc();

            //지자체
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SGCCD"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SGCCD");

                foreach (DataRow row in sgc.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SGCCD"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["SGCCD"];
            }

            ComboBoxUtils.SetMember(this.sgccd, "CODE", "CODE_NAME");
            this.sgccd.DataSource = sgc;
            ComboBoxUtils.AddDefaultOption(this.sgccd, false);
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.updateBtn.Click += new EventHandler(updateBtn_Click);

            this.sgccd.SelectedIndexChanged += new EventHandler(sgccd_SelectedIndexChanged);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);

            this.checkBox1.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox2.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox3.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox4.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox6.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox7.CheckedChanged += new EventHandler(checkBox_CheckedChanged);

            this.ultraGrid2.AfterCellUpdate += new CellEventHandler(ultraGrid2_AfterCellUpdate);
            this.ultraGrid2.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid2_InitializeLayout);
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.ultraGrid1.Rows.Count == 0)
                {
                    return;
                }
                this.UpdateReservoir();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void UpdateReservoir()
        {
            ReservoirPredictionManageWork.GetInstance().UpdateReservoir(this.ultraGrid1);
        }

        private void sgccd_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectReservoir();
        }

        private void SelectReservoir()
        {
            this.Cursor = Cursors.WaitCursor;

            Hashtable parameter = Utils.ConverToHashtable(this.groupBox1);
            this.ultraGrid1.DataSource = ReservoirPredictionManageWork.GetInstance().SelectReservoir(parameter);

            this.Cursor = Cursors.Default;
        }

        private Hashtable parameter = null;
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.ultraGrid1.ActiveRow == null)
                {
                    MessageBox.Show("배수지를 선택해 주세요.");
                    return;
                }

                if (this.ultraGrid1.ActiveRow.Cells["IN_FLOW_TAG"].Value == DBNull.Value)
                {
                    MessageBox.Show("선택된 배수지에는 유입유량적산차태그가 존재하지 않습니다.");
                    return;
                }

                if (this.ultraGrid1.ActiveRow.Cells["OUT_FLOW_TAG"].Value == DBNull.Value)
                {
                    MessageBox.Show("선택된 배수지에는 유출유량적산차태그가 존재하지 않습니다.");
                    return;
                }

                if (this.ultraGrid1.ActiveRow.Cells["WR1ST_TAG"].Value == DBNull.Value && this.ultraGrid1.ActiveRow.Cells["WR2ND_TAG"].Value == DBNull.Value)
                {
                    MessageBox.Show("선택된 배수지에는 수위태그가 존재하지 않습니다.");
                    return;
                }

                //this.startDate.Value = Convert.ToDateTime(ReservoirPredictionManageWork.GetInstance().SelectLastTime()).ToString("yyyy-MM-dd HH:00");

                //유량적산차 최근시간을 설정한다.
                this.parameter = Utils.ConverToHashtable(this.groupBox1);
                this.parameter["IN_FLOW_TAG"] = this.ultraGrid1.ActiveRow.Cells["IN_FLOW_TAG"].Value;
                this.parameter["OUT_FLOW_TAG"] = this.ultraGrid1.ActiveRow.Cells["OUT_FLOW_TAG"].Value;
                this.parameter["WR1ST_TAG"] = this.ultraGrid1.ActiveRow.Cells["WR1ST_TAG"].Value;
                this.parameter["WR2ND_TAG"] = this.ultraGrid1.ActiveRow.Cells["WR2ND_TAG"].Value;
                this.parameter["FAC_VOLUMN"] = this.ultraGrid1.ActiveRow.Cells["FAC_VOLUMN"].Value;
                this.parameter["HIGH"] = this.ultraGrid1.ActiveRow.Cells["HIGH"].Value;
                this.parameter["LOW"] = this.ultraGrid1.ActiveRow.Cells["LOW"].Value;
                this.parameter["STARTDATE"] = Convert.ToDateTime(this.startDate.Value).ToString("yyyyMMddHHmm");
                this.parameter["FTR_IDN"] = this.ultraGrid1.ActiveRow.Cells["SUPY_FTR_IDN"].Value;
                this.SelectReservoirPredictionManage();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectReservoirPredictionManage()
        {
            this.Cursor = Cursors.WaitCursor;

            this.ultraGrid2.DataSource = ReservoirPredictionManageWork.GetInstance().SelectReservoirPredictionManage(this.parameter);
            this.gridManager.DefaultColumnsMapping(this.ultraGrid3);
            this.InitializeChartSetting();

            this.Cursor = Cursors.Default;
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid2, this.Text);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            this.InitializeChartSeries();
        }

        private bool isUpdate = false;
        private void ultraGrid2_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "IN_FLOW_CHANGE")
            {
                double in_flow_change = Utils.ToDouble(e.Cell.Row.Cells["IN_FLOW_CHANGE"].Value);
                double lei_change = Utils.ToDouble(e.Cell.Row.Cells["LEI_CHANGE"].Value);
                double p_out_flow = Utils.ToDouble(e.Cell.Row.Cells["P_OUT_FLOW"].Value);
                double siz = Utils.ToDouble(e.Cell.Row.Cells["FAC_VOLUMN"].Value);
                e.Cell.Row.Cells["P_LEI"].Value = lei_change + (in_flow_change - p_out_flow) / siz;

                double lei = lei_change + (in_flow_change - p_out_flow) / siz;

                if (Utils.ToDouble(e.Cell.Row.Cells["HIGH"].Value) != 0 && lei >= Utils.ToDouble(e.Cell.Row.Cells["HIGH"].Value))
                {
                    lei = Utils.ToDouble(e.Cell.Row.Cells["HIGH"].Value);
                }
                else if (lei <= Utils.ToDouble(e.Cell.Row.Cells["LOW"].Value))
                {
                    lei = Utils.ToDouble(e.Cell.Row.Cells["LOW"].Value);
                }

                e.Cell.Row.Cells["P_LEI"].Value = lei;

                if (this.ultraGrid2.Rows.Count - 1 > e.Cell.Row.Index)
                {
                    this.isUpdate = true;
                    for (int i = e.Cell.Row.Index + 1; i < this.ultraGrid2.Rows.Count; i++)
                    {
                        this.ultraGrid2.Rows[i].Cells[e.Cell.Column.Key].Value = e.Cell.Value;
                        this.ultraGrid2.Rows[i].Cells["LEI_CHANGE"].Value = this.ultraGrid2.Rows[i - 1].Cells["P_LEI"].Value;

                        lei_change = Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["LEI_CHANGE"].Value);
                        in_flow_change = Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["IN_FLOW_CHANGE"].Value);
                        p_out_flow = Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["P_OUT_FLOW"].Value);
                        siz = Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["FAC_VOLUMN"].Value);

                        lei = lei_change + (in_flow_change - p_out_flow) / siz;

                        if (Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["HIGH"].Value) != 0 && lei >= Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["HIGH"].Value))
                        {
                            lei = Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["HIGH"].Value);
                        }
                        else if (lei <= Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["LOW"].Value))
                        {
                            lei = Utils.ToDouble(this.ultraGrid2.Rows[i].Cells["LOW"].Value);
                        }

                        this.ultraGrid2.Rows[i].Cells["P_LEI"].Value = lei;
                    }

                    this.isUpdate = false;
                }
                this.InitializeChartSetting();
            }
        }

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.chart1.AxisY.Sections.Clear();
            this.chart1.ConditionalAttributes.Clear();
            this.chart1.Extensions.Clear();
            this.chart1.LegendBox.CustomItems.Clear();
            this.chart1.Data.Clear();
            this.chartManager.AllClear(this.chart1);

            this.chart1.Data.Series = 6;
            this.chart1.Data.Points = this.ultraGrid2.Rows.Count;
            this.chart1.AxesY.Add(new ChartFX.WinForms.AxisY());

            foreach (UltraGridRow row in this.ultraGrid2.Rows)
            {
                this.chart1.AxisX.Labels[row.Index] = Convert.ToDateTime(row.Cells["TIMESTAMP"].Value).ToString("yyyy-MM-dd ") + row.Cells["TIME"].Value.ToString() + ":00";

                if (Utils.ToDouble(row.Cells["IN_FLOW"].Value) != 0)
                {
                    this.chart1.Data[0, row.Index] = Utils.ToDouble(row.Cells["IN_FLOW"].Value);
                }
                if (Utils.ToDouble(row.Cells["OUT_FLOW"].Value) != 0)
                {
                    this.chart1.Data[1, row.Index] = Utils.ToDouble(row.Cells["OUT_FLOW"].Value);
                }
                if (Utils.ToDouble(row.Cells["LEI"].Value) != 0)
                {
                    this.chart1.Data[2, row.Index] = Utils.ToDouble(row.Cells["LEI"].Value);
                }
                if (Utils.ToDouble(row.Cells["IN_FLOW_CHANGE"].Value) != 0)
                {
                    this.chart1.Data[3, row.Index] = Utils.ToDouble(row.Cells["IN_FLOW_CHANGE"].Value);
                }
                if (Utils.ToDouble(row.Cells["P_OUT_FLOW"].Value) != 0)
                {
                    this.chart1.Data[4, row.Index] = Utils.ToDouble(row.Cells["P_OUT_FLOW"].Value);
                }
                if (Utils.ToDouble(row.Cells["P_LEI"].Value) != 0)
                {
                    this.chart1.Data[5, row.Index] = Utils.ToDouble(row.Cells["P_LEI"].Value);
                }
            }

            this.chart1.AxesY[0].LabelsFormat.Format = AxisFormat.Number;
            this.chart1.AxesY[0].LabelsFormat.CustomFormat = "####,####";

            this.chart1.AxesY[1].LabelsFormat.Format = AxisFormat.Number;
            this.chart1.AxesY[1].LabelsFormat.CustomFormat = "N2";

            this.chart1.AxesY[0].Title.Text = "유량(㎥/h)";
            this.chart1.AxesY[1].Title.Text = "수위(m)";

            this.chart1.Series[0].Text = "유입유량(㎥/h)";
            this.chart1.Series[0].Line.Width = 1;
            this.chart1.Series[0].MarkerSize = 1;

            this.chart1.Series[1].Text = "유출유량(㎥/h)";
            this.chart1.Series[1].Line.Width = 1;
            this.chart1.Series[1].MarkerSize = 1;

            this.chart1.Series[2].Text = "수위(m)";
            this.chart1.Series[2].Line.Width = 1;
            this.chart1.Series[2].MarkerSize = 1;

            this.chart1.Series[3].Text = "유입유량변경(㎥/h)";
            this.chart1.Series[3].Line.Width = 1;
            this.chart1.Series[3].MarkerSize = 1;

            this.chart1.Series[4].Text = "유출수요예측량(㎥/h)";
            this.chart1.Series[4].Line.Width = 1;
            this.chart1.Series[4].MarkerSize = 1;

            this.chart1.Series[5].Text = "예측수위(m)";
            this.chart1.Series[5].Line.Width = 1;
            this.chart1.Series[5].MarkerSize = 1;

            this.chart1.Series[2].AxisY = this.chart1.AxesY[1];
            this.chart1.Series[5].AxisY = this.chart1.AxesY[1];

            this.InitializeChartSeries();
            this.InitializeResultPlan();
        }

        private void InitializeChartSeries()
        {
            if (this.parameter == null)
            {
                return;
            }

            foreach (SeriesAttributes series in this.chart1.Series)
            {
                series.Visible = false;
            }

            if (this.checkBox1.Checked)
            {
                this.chart1.Series[0].Visible = true;
            }
            if (this.checkBox2.Checked)
            {
                this.chart1.Series[1].Visible = true;
            }
            if (this.checkBox3.Checked)
            {
                this.chart1.Series[2].Visible = true;
            }
            if (this.checkBox4.Checked)
            {
                this.chart1.Series[3].Visible = true;
            }
            if (this.checkBox6.Checked)
            {
                this.chart1.Series[4].Visible = true;
            }
            if (this.checkBox7.Checked)
            {
                this.chart1.Series[5].Visible = true;
            }
        }

        private void InitializeResultPlan()
        {
            DataTable table = new DataTable();
            table.Columns.Add("TODAY_PREDICTION" ,typeof(double));
            table.Columns.Add("CS_IN_FLOW", typeof(double));
            table.Columns.Add("D_FLOW", typeof(double));
            table.Columns.Add("HIGH_TIME");
            table.Columns.Add("LOW_TIME");

            DataRow row = table.NewRow();

            double today_prediction = 0;
            double cs_in_flow = 0;

            foreach (UltraGridRow gridRow in this.ultraGrid2.Rows)
            {
                if (Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value).ToString("yyyyMMddHH")
                    == Convert.ToDateTime(this.startDate.Value).ToString("yyyyMMdd23"))
                {
                    today_prediction = Utils.ToDouble(gridRow.Cells["PS_OUT_FLOW"].Value);
                    cs_in_flow = Utils.ToDouble(gridRow.Cells["CS_IN_FLOW"].Value);
                }
            }

            row["TODAY_PREDICTION"] = today_prediction;
            row["CS_IN_FLOW"] = cs_in_flow;
            row["D_FLOW"] = today_prediction - cs_in_flow;

            foreach (UltraGridRow gridRow in this.ultraGrid2.Rows)
            {
                if (Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value) > Convert.ToDateTime(this.startDate.Value))
                {
                    if (Utils.ToDouble(gridRow.Cells["HIGH"].Value) <= Utils.ToDouble(gridRow.Cells["P_LEI"].Value))
                    {
                        TimeSpan span = Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value) - Convert.ToDateTime(this.startDate.Value);
                        row["HIGH_TIME"] = span.Hours.ToString("00") + " 시간 후(" + Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value).ToString("MM월 dd일 HH시경)");
                        break;
                    }
                }
            }

            foreach (UltraGridRow gridRow in this.ultraGrid2.Rows)
            {
                if (Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value) > Convert.ToDateTime(this.startDate.Value))
                {
                    if (Utils.ToDouble(gridRow.Cells["LOW"].Value) >= Utils.ToDouble(gridRow.Cells["P_LEI"].Value))
                    {
                        TimeSpan span = Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value) - Convert.ToDateTime(this.startDate.Value);
                        row["LOW_TIME"] = span.Hours.ToString("00") + " 시간 후(" + Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value).ToString("MM월 dd일 HH시경)");
                        break;
                    }
                }
            }

            if (row["HIGH_TIME"] == DBNull.Value)
            {
                row["HIGH_TIME"] = "익일 24시간 이내 도달안함";
            }
            if (row["LOW_TIME"] == DBNull.Value)
            {
                row["LOW_TIME"] = "익일 24시간 이내 도달안함";
            }

            table.Rows.Add(row);
            this.ultraGrid3.DataSource = table;
        }

        private void ultraGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            DateTime start_time = Convert.ToDateTime(this.startDate.Value);

            foreach (UltraGridRow gridRow in this.ultraGrid2.Rows)
            {
                if (Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value) > Convert.ToDateTime(this.startDate.Value))
                {
                    gridRow.Cells["LEI_CHANGE"].Value = this.ultraGrid2.Rows[gridRow.Index - 1].Cells["P_LEI"].Value;
                    gridRow.Appearance.BackColor = Color.Yellow;
                }
                else
                {
                    gridRow.Activation = Activation.NoEdit;
                }

                double lei_change = Utils.ToDouble(gridRow.Cells["LEI_CHANGE"].Value);
                double in_flow_change = Utils.ToDouble(gridRow.Cells["IN_FLOW_CHANGE"].Value);
                double p_out_flow = Utils.ToDouble(gridRow.Cells["P_OUT_FLOW"].Value);
                double siz = Utils.ToDouble(gridRow.Cells["FAC_VOLUMN"].Value);

                double lei = lei_change + (in_flow_change - p_out_flow) / siz;

                if (Utils.ToDouble(gridRow.Cells["HIGH"].Value) != 0 && lei >= Utils.ToDouble(gridRow.Cells["HIGH"].Value))
                {
                    lei = Utils.ToDouble(gridRow.Cells["HIGH"].Value);
                }
                else if (lei <= Utils.ToDouble(gridRow.Cells["LOW"].Value))
                {
                    lei = Utils.ToDouble(gridRow.Cells["LOW"].Value);
                }

                gridRow.Cells["P_LEI"].Value = lei;
            }
        }
    }
}
