﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_ReservoirPredictionManage.dao
{
    public class ReservoirPredictionManageDao
    {
        private static ReservoirPredictionManageDao dao = null;
        private ReservoirPredictionManageDao() { }

        public static ReservoirPredictionManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ReservoirPredictionManageDao();
            }
            return dao;
        }

        public void UpdateReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update we_supply_area");
            query.AppendLine("   set hwl = :HIGH");
            query.AppendLine("      ,lwl = :LOW");
            query.AppendLine(" where supy_ftr_idn = :SUPY_FTR_IDN");

            IDataParameter[] parameters =  {
                     new OracleParameter("HIGH", OracleDbType.Varchar2),
                     new OracleParameter("LOW", OracleDbType.Varchar2),
                     new OracleParameter("SUPY_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["HIGH"];
            parameters[1].Value = parameter["LOW"];
            parameters[2].Value = parameter["SUPY_FTR_IDN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public object SelectLastTime(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select timestamp - 1/24                                                    ");
            query.AppendLine("  from (select timestamp from if_accumulation_hour order by timestamp desc)");
            query.AppendLine(" where rownum = 1                                                          ");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        //지자체 조회
        public DataSet SelectSgc(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" select si_code code                                                       ");
            query.AppendLine("       ,si_name code_name                                                  ");
            query.AppendLine("  from cm_dongcode                                                         ");
            query.AppendLine(" group by si_name, si_code                                                 ");
            query.AppendLine(" order by si_name                                                          ");

            return manager.ExecuteScriptDataSet(query.ToString(), null);
        }

        //배수지 조회
        public DataSet SelectReservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select a.sgccd                                                             ");
            query.AppendLine("      ,a.supy_ftr_idn                                                      ");
            query.AppendLine("      ,a.servnm                                                            ");
            query.AppendLine("      ,a.in_flow_tag                                                       ");
            query.AppendLine("      ,a.out_flow_tag                                                      ");
            query.AppendLine("      ,a.wr1st_tag                                                         ");
            query.AppendLine("      ,a.wr2nd_tag                                                         ");
            query.AppendLine("      ,to_number(nvl(a.fac_volumn,0)) fac_volumn                           ");
            query.AppendLine("      ,a.hwl high                                                          ");
            query.AppendLine("      ,a.lwl low                                                           ");
            query.AppendLine("  from we_supply_area a, df_setting b                                      ");
            query.AppendLine(" where a.sgccd = decode(:SGCCD, 'O', sgccd, :SGCCD)                        ");
            query.AppendLine(" and a.supy_ftr_idn = b.loc_code                                           ");
            query.AppendLine(" and b.use_yn='Y'						                                     ");
            //query.AppendLine("   and a.df_yn = 'Y'                                                     ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("SGCCD", OracleDbType.Varchar2),
                     new OracleParameter("SGCCD", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["SGCCD"];
            parameters[1].Value = parameter["SGCCD"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //배수지 조회
        public DataSet SelectReservoirPredictionManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as                                                                                                                    ");
            query.AppendLine("(																																  ");
            query.AppendLine("select :IN_FLOW_TAG in_flow_tag																								  ");
            query.AppendLine("      ,:OUT_FLOW_TAG out_flow_tag																								  ");
            query.AppendLine("      ,:WR1ST_TAG wr1st_tag																									  ");
            query.AppendLine("      ,:WR2ND_TAG wr2nd_tag																									  ");
            query.AppendLine("      ,:FAC_VOLUMN fac_volumn																				    				  ");
            query.AppendLine("      ,:HIGH high																												  ");
            query.AppendLine("      ,:LOW low																												  ");
            query.AppendLine("      ,to_char(to_date(:STARTDATE,'yyyymmddhh24mi'),'hh24') hh																  ");
            query.AppendLine("      ,to_date(to_char(to_date(:STARTDATE,'yyyymmddhh24mi'),'yyyymmdd'),'yyyymmdd') + ((1/24)*(rownum-1)) timestamp			  ");
            query.AppendLine("      ,:FTR_IDN ftr_idn                                                                                                         ");
            query.AppendLine("  from dual																													  ");
            query.AppendLine("connect by rownum < ((to_date(to_char(to_date(:STARTDATE,'yyyymmddhh24mi') + 1,'yyyymmdd')||'2300','yyyymmddhh24mi') + ( 1 / 24)");
            query.AppendLine("                   -to_date(to_char(to_date(:STARTDATE,'yyyymmddhh24mi'),'yyyymmdd'),'yyyymmdd')) * 24) + ( 1 / 24)			  ");
            query.AppendLine(")																																  ");
            query.AppendLine("select timestamp																												  ");
            query.AppendLine("      ,to_number(to_char(time ,'00')) time																					  ");
            query.AppendLine("      ,in_flow																												  ");
            query.AppendLine("      ,out_flow																												  ");
            query.AppendLine("      ,lei																													  ");
            query.AppendLine("      ,in_flow_change																											  ");
            query.AppendLine("      ,lei_change																												  ");
            query.AppendLine("      ,p_out_flow																												  ");
            query.AppendLine("      ,to_number(0) p_lei														                                                  ");
            query.AppendLine("      ,cs_flow																												  ");
            query.AppendLine("      ,cs_in_flow																												  ");
            query.AppendLine("      ,ps_out_flow																											  ");
            query.AppendLine("      ,fac_volumn																												  ");
            query.AppendLine("      ,high																													  ");
            query.AppendLine("      ,low																													  ");
            query.AppendLine("  from (																														  ");
            query.AppendLine("        select a.timestamp																									  ");
            query.AppendLine("              ,a.time																											  ");
            query.AppendLine("              ,a.in_flow																										  ");
            query.AppendLine("              ,decode(a.in_flow, 0, lag(a.in_flow, decode(sign(rownum - 1 - a.hh), -1, 0, rownum - 1 - a.hh)) 				  ");
            query.AppendLine("               over (order by a.timestamp), a.in_flow) in_flow_change															  ");
            query.AppendLine("              ,b.out_flow																										  ");
            query.AppendLine("              ,lei lei_change                                                                                                   ");
            query.AppendLine("              ,c.lei																											  ");
            query.AppendLine("              ,b.p_out_flow																									  ");
            query.AppendLine("              ,sum(round(nvl(b.out_flow,0))) over(order by a.timestamp rows unbounded preceding) cs_flow						  ");
            query.AppendLine("              ,a.cs_in_flow																									  ");
            query.AppendLine("              ,b.ps_out_flow																									  ");
            query.AppendLine("              ,to_number(a.fac_volumn) fac_volumn																				  ");
            query.AppendLine("              ,to_number(a.high) high																							  ");
            query.AppendLine("              ,to_number(a.low) low																							  ");
            query.AppendLine("              ,a.hh																											  ");
            query.AppendLine("         from (																												  ");
            query.AppendLine("               select a.timestamp																								  ");
            query.AppendLine("                     ,to_char(a.timestamp,'hh24') time																		  ");
            query.AppendLine("                     ,to_number(nvl(b.value,0)) in_flow																		  ");
            query.AppendLine("                     ,sum(round(nvl(b.value,0))) over(order by a.timestamp rows unbounded preceding) cs_in_flow				  ");
            query.AppendLine("                     ,a.fac_volumn																							  ");
            query.AppendLine("                     ,a.high																									  ");
            query.AppendLine("                     ,a.low																									  ");
            query.AppendLine("                     ,a.hh																									  ");
            query.AppendLine("                 from temp a																									  ");
            query.AppendLine("                     ,if_accumulation_hour b																					  ");
            query.AppendLine("                where b.tagname(+) = a.in_flow_tag																			  ");
            query.AppendLine("                  and b.timestamp(+) = a.timestamp + (1/24)																	  ");
            query.AppendLine("               order by a.timestamp																							  ");
            query.AppendLine("              ) a																												  ");
            query.AppendLine("             ,(																												  ");
            query.AppendLine("               select a.timestamp																								  ");
            query.AppendLine("                     ,to_char(a.timestamp,'hh24') time																		  ");
            query.AppendLine("                     ,to_number(nvl(b.value,0)) out_flow																		  ");
            query.AppendLine("                     ,round(nvl(c.df_thfq,0)) p_out_flow																		  ");
            query.AppendLine("                     ,sum(round(nvl(c.df_thfq,0))) over(order by a.timestamp rows unbounded preceding) ps_out_flow			  ");
            query.AppendLine("                 from temp a																									  ");
            query.AppendLine("                     ,if_accumulation_hour b																					  ");
            query.AppendLine("                     ,df_result_hour c																						  ");
            query.AppendLine("                where b.tagname(+) = a.out_flow_tag																			  ");
            query.AppendLine("                  and b.timestamp(+) = a.timestamp + (1/24)																	  ");
            query.AppendLine("                  and c.loc_code(+) = a.ftr_idn  																			      ");
            query.AppendLine("                  and c.df_rl_dt(+) = to_date(to_char(a.timestamp,'yyyymmdd'),'yyyymmdd')										  ");
            query.AppendLine("                  and c.df_rl_hour(+) = to_char(a.timestamp,'hh24')															  ");
            query.AppendLine("                order by a.timestamp																							  ");
            query.AppendLine("              ) b																												  ");
            query.AppendLine("             ,(																												  ");
            query.AppendLine("               select a.timestamp																								  ");
            query.AppendLine("                     ,to_number(max(nvl(value,0))) lei																		  ");
            query.AppendLine("                 from temp a																									  ");
            query.AppendLine("                     ,if_gather_realtime b																					  ");
            query.AppendLine("               where b.tagname(+) = nvl(a.wr1st_tag, a.wr2nd_tag)																  ");
            query.AppendLine("                 and b.timestamp(+) = a.timestamp + (1/24)																	  ");
            query.AppendLine("               group by a.timestamp																							  ");
            query.AppendLine("              ) c																												  ");
            query.AppendLine("         where b.timestamp = a.timestamp																						  ");
            query.AppendLine("           and c.timestamp = b.timestamp																						  ");
            query.AppendLine("    )																															  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("IN_FLOW_TAG", OracleDbType.Varchar2),
                     new OracleParameter("OUT_FLOW_TAG", OracleDbType.Varchar2),
                     new OracleParameter("WR1ST_TAG", OracleDbType.Varchar2),
                     new OracleParameter("WR2ND_TAG", OracleDbType.Varchar2),
                     new OracleParameter("FAC_VOLUMN", OracleDbType.Varchar2),
                     new OracleParameter("HIGH", OracleDbType.Varchar2),
                     new OracleParameter("LOW", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["IN_FLOW_TAG"];
            parameters[1].Value = parameter["OUT_FLOW_TAG"];
            parameters[2].Value = parameter["WR1ST_TAG"];
            parameters[3].Value = parameter["WR2ND_TAG"];
            parameters[4].Value = parameter["FAC_VOLUMN"];
            parameters[5].Value = parameter["HIGH"];
            parameters[6].Value = parameter["LOW"];
            parameters[7].Value = parameter["STARTDATE"];
            parameters[8].Value = parameter["STARTDATE"];
            parameters[9].Value = parameter["FTR_IDN"];
            parameters[10].Value = parameter["STARTDATE"];
            parameters[11].Value = parameter["STARTDATE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }
    }
}
