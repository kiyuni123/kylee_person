﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_ReservoirPredictionManage.dao;
using System.Data;
using WaterNet.WV_Common.work;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_ReservoirPredictionManage.work
{
    public class ReservoirPredictionManageWork : BaseWork
    {
        private static ReservoirPredictionManageWork work = null;
        private ReservoirPredictionManageDao dao = null;

        private ReservoirPredictionManageWork()
        {
            dao = ReservoirPredictionManageDao.GetInstance();
        }

        public static ReservoirPredictionManageWork GetInstance()
        {
            if (work == null)
            {
                work = new ReservoirPredictionManageWork();
            }
            return work;
        }

        public void UpdateReservoir(UltraGrid ultraGrid)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow row in ultraGrid.Rows)
                {
                    Hashtable parameter = Utils.ConverToHashtable(row);
                    dao.UpdateReservoir(base.DataBaseManager, parameter);
                }

                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public object SelectLastTime()
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = dao.SelectLastTime(base.DataBaseManager);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //지자체조회
        public DataTable SelectSgc()
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectSgc(base.DataBaseManager);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectReservoir(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectReservoir(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectReservoirPredictionManage(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectReservoirPredictionManage(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
