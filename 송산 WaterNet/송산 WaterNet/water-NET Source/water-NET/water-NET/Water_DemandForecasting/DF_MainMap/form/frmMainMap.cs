﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WaterAOCore;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using System.Collections;
using ESRI.ArcGIS.Display;

namespace WaterNet.DF_MainMap.form
{
    /// <summary>
    /// 메인지도의 화면이며 수요예측관리에서 공통적으로 사용된다.
    /// 공통적으로 사용될 지도관련 기능을 구현하며 각 업무화면은 해당객체의 인스턴스를 가지고 기능을 제어한다.
    /// 각 업무폼에서의 인스턴스이름은 MainMap이다.
    /// </summary>
    public partial class frmMainMap : WaterNet.WaterAOCore.frmMap, WaterNetCore.IForminterface, DF_Common.interface1.IMapControl
    {
        private IMapControl3 imapControl3 = null;

        public frmMainMap()
        {
            InitializeComponent();
            InitializeSetting();
        }

        public IMapControl3 IMapControl3
        {
            get
            {
                return this.imapControl3;
            }
        }

        public AxMapControl AxMap
        {
            get
            {
                return base.axMap;
            }
        }

        public ToolStripButton ToolActionCommand
        {
            get
            {
                return base.toolActionCommand;
            }
        }

        public override void Open()
        {
            base.Open();
            this.imapControl3 = (IMapControl3)axMap.Object;
            this.Layer_Visible_Setting();
        }

        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            this.InitializeEvent();
        }

        /// <summary>
        /// 이벤트 초기 설정
        /// </summary>
        private void InitializeEvent()
        {
        }


        /// <summary>
        /// 축척변경
        /// </summary>
        /// <param name="scale">축척</param>
        private void MapScale(int scale)
        {
            base.axMap.MapScale = scale;
            base.axMap.Refresh();
        }

        #region Implementation of ICollection IForminterface
        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            //WaterNet 탭에 보여질 이름
            get { return "수요예측"; }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        /// <summary>
        /// 레이어에서 특정 퓨처를 찾아 포커스를 이동시켜 준다. 
        /// </summary>
        /// <param name="layerName">레이어명</param>
        /// <param name="whereCase">조건</param>
        public void MoveFocusAt(string layerName, string whereCase)
        {
            this.MoveFocusAt(layerName, whereCase, 10000);
        }

        /// <summary>
        /// 레이어에서 특정 퓨처를 찾아 포커스를 이동시켜 준다. 
        /// </summary>
        /// <param name="layerName">레이어명</param>
        /// <param name="whereCase">조건</param>
        public void MoveFocusAt(string layerName, string whereCase, int scale)
        {
            ILayer layer = ArcManager.GetMapLayer(imapControl3, layerName);

            if (layer != null)
            {
                ESRI.ArcGIS.Geodatabase.IFeature feature = ArcManager.GetFeature(layer, whereCase);

                if (feature != null)
                {
                    //if (this.imapControl3.MapScale > 10000)
                    //{
                    //    this.MapScale(scale);
                    //}

                    //ArcManager.MoveCenterAt(this.imapControl3, feature);
                    //Application.DoEvents();
                    //ArcManager.FlashShape(axMap.ActiveView, (IGeometry)feature.Shape, 8, 100);

                    IRelationalOperator pRelOp = feature.Shape as IRelationalOperator;
                    if (!pRelOp.Within(this.imapControl3.ActiveView.Extent.Envelope as IGeometry))
                    {
                        ArcManager.MoveCenterAt(this.imapControl3, feature);
                        Application.DoEvents();
                    }

                    ArcManager.FlashShape(this.imapControl3.ActiveView, (IGeometry)feature.Shape, 8, 100);
                }
                if (feature == null)
                {
                    MessageBox.Show("GIS에 선택한 객체가 존재하지 않습니다.");
                }
            }
        }


        private void Layer_Visible_Setting()
        {
            ILayer pLayer = null;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "재염소처리지점");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "중요시설");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "감시지점");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "누수지점");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "민원지점");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "관세척구간");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "수압계");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "소방시설");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "밸브실");
            if (pLayer != null) pLayer.Visible = false;

            //pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "행정읍면동");
            //if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "취수장");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "수원지");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "밸브");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "유량계");
            if (pLayer != null) pLayer.Visible = false;

            //pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "건물");
            //if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "상수관로");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "급수관로");
            if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "수도계량기");
            if (pLayer != null) pLayer.Visible = false;

            //pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "지형지번");
            //if (pLayer != null) pLayer.Visible = false;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "배수지");
            if (pLayer != null) pLayer.Visible = true;

            IColor pLabelColor = null; IColor pOutColor = null; IColor pRenderColor = null;
            ISymbol pLineSymbol = null; ISymbol pFillSymbol = null;

            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(0, 0, 0);
            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "SRV_NAM", "굴림", 9, true, pLabelColor, 0, 0, string.Empty);
            //외곽선 심볼
            pOutColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 142, 30);
            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 13, pOutColor);
            //내부 심볼
            pRenderColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 142, 30);
            pFillSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, (ILineSymbol)pLineSymbol, pRenderColor);
            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pFillSymbol);

            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }
    }
}
