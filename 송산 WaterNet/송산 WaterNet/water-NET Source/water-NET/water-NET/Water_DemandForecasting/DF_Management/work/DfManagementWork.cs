﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.work;
using WaterNet.DF_Management.dao;
using System.Data;
using System.Collections;
using WaterNet.WaterAOCore;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.interface1;
using WaterNet.DF_Common.interface1;

namespace WaterNet.DF_Management.work
{
    public class DfManagementWork : BaseWork
    {
        private static DfManagementWork work = null;
        private DfManagementDao dao = null;

        private DfManagementWork()
        {
            dao = DfManagementDao.GetInstance();
        }

        public static DfManagementWork GetInstance()
        {
            if (work == null)
            {
                work = new DfManagementWork();
            }
            return work;
        }

        //지자체조회
        public DataTable SelectReservoir()
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectReservoir(base.DataBaseManager);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }


        public DataTable SelectDfManagement(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectBlock(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectDfManagement()
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectReservoir(base.DataBaseManager);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //수요예측결과조회
        public DataTable SelectDfManagementDay(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = null;

                dataSet = dao.SelectDfManagementDay_Reservior(base.DataBaseManager, parameter);

                if (parameter["GUBUN"].ToString() == "1")
                {
                    dataSet = dao.SelectDfManagementDay_Block(base.DataBaseManager, parameter);
                }
                else if (parameter["GUBUN"].ToString() == "2")
                {
                    dataSet = dao.SelectDfManagementDay_Reservior(base.DataBaseManager, parameter);
                }
                
                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //수요예측결과조회
        public DataTable SelectDfManagementHour(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = null;

                if (parameter["GUBUN"].ToString() == "1")
                {
                    dataSet = dao.SelectDfManagementHour_Block(base.DataBaseManager, parameter);
                }
                else if (parameter["GUBUN"].ToString() == "2")
                {
                    dataSet = dao.SelectDfManagementHour_Reservoir(base.DataBaseManager, parameter);
                }

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //수요예측결과조회
        public DataTable SelectDfManagementResultHour(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = null;

                if (parameter["TYPE"].ToString() == "F")
                {
                    dataSet = dao.SelectDfManagementResultHour_FlowMeter(base.DataBaseManager, parameter);
                }
                else if (parameter["TYPE"].ToString() == "R")
                {
                    dataSet = dao.SelectDfManagementResultHour_Reservoir(base.DataBaseManager, parameter);
                }

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //수요예측결과조회
        public DataTable SelectFlowMeta(IMapControl mainMap, Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                //result = mainMap.GetResultTable(parameter);

                if (result != null)
                {
                    result.Columns.Add("TAG1");
                    result.Columns.Add("TAG2");
                    result.Columns.Add("TAG3");
                    result.Columns.Add("USE_YN");

                    foreach (DataRow dataRow in result.Rows)
                    {
                        parameter["FTR_IDN"] = dataRow["FTR_IDN"];

                        DataSet dataSet =
                            dao.SelectFlowMetaTag(base.DataBaseManager, parameter);

                        Hashtable linkInfo = null;

                        if (dataSet != null && dataSet.Tables.Count > 0)
                        {
                            if (dataSet.Tables[0].Rows.Count > 0)
                            {
                                linkInfo = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);
                            }
                        }

                        if (linkInfo != null)
                        {
                            dataRow["TAG1"] = linkInfo["TAG1"];
                            dataRow["TAG2"] = linkInfo["TAG2"];
                            dataRow["TAG3"] = linkInfo["TAG3"];
                        }
                        dataRow["USE_YN"] = dao.SelectDfSimulationUse(base.DataBaseManager, parameter);
                    }
                    for (int i = result.Rows.Count - 1; i >= 0; i--)
                    {
                        if (result.Rows[i]["USE_YN"].ToString() != "Y")
                        {
                            result.Rows.RemoveAt(i);
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
