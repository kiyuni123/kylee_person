﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.dao;
using System.Collections;
using WaterNet.WaterNetCore;
using System.Data;
using Oracle.DataAccess.Client;

namespace WaterNet.DF_Management.dao
{
    public class DfManagementDao : BaseDao
    {
        private static DfManagementDao dao = null;
        private DfManagementDao() { }

        public static DfManagementDao GetInstance()
        {
            if (dao == null)
            {
                dao = new DfManagementDao();
            }
            return dao;
        }

        //배수지 조회
        public DataSet SelectReservoir(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select a.res_code loc_code");
            //query.AppendLine("      ,(select ftr_idn from cm_location where loc_code = a.res_code) ftr_idn");
            //query.AppendLine("      ,(select loc_name from cm_location where loc_code = a.res_code) loc_name");
            //query.AppendLine("      ,(");
            //query.AppendLine("        select c.tagname");
            //query.AppendLine("          from if_ihtags b");
            //query.AppendLine("              ,if_tag_gbn c");
            //query.AppendLine("         where b.loc_code = decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            //query.AppendLine("           and c.tagname = b.tagname");
            //query.AppendLine("           and c.tag_gbn = 'FRQ_O'");
            //query.AppendLine("       ) out_flow_tag");
            //query.AppendLine("      ,nvl((");
            //query.AppendLine("       select use_yn from df_setting where loc_code = a.res_code");
            //query.AppendLine("       ),'N') use_yn");
            //query.AppendLine(" from cm_location a");
            //query.AppendLine("where a.res_code is not null");
            //query.AppendLine("  and (select loc_gbn from cm_location where loc_code = a.res_code) = '배수지'");

            query.AppendLine("SELECT A.SUPY_FTR_IDN LOC_CODE ");
            query.AppendLine("      ,(SELECT FTR_IDN FROM CM_LOCATION WHERE LOC_CODE = A.SUPY_FTR_IDN) FTR_IDN ");
            query.AppendLine("      ,A.SERVNM LOC_NAME ");
            query.AppendLine("      ,A.OUT_FLOW_TAG ");
            query.AppendLine("      ,NVL((SELECT USE_YN FROM DF_SETTING WHERE LOC_CODE = A.SUPY_FTR_IDN), 'N') USE_YN ");
            query.AppendLine("  FROM WE_SUPPLY_AREA A, DF_SETTING B ");
            query.AppendLine("  WHERE A.SUPY_FTR_IDN = B.LOC_CODE ");
            query.AppendLine("  AND   B.USE_YN = 'Y' ");

            return manager.ExecuteScriptDataSet(query.ToString(), null);
        }

        //블록 조회
        public DataSet SelectBlock(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as");
            query.AppendLine("(");
            query.AppendLine("select c1.loc_code");
            query.AppendLine("	  ,c1.sgccd");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock");
            query.AppendLine("	  ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code");
            query.AppendLine("	  ,c1.ord");
            query.AppendLine("	  ,c1.ftr_code");
            query.AppendLine("  from");
            query.AppendLine("	  (");
            query.AppendLine("	   select sgccd");
            query.AppendLine("			 ,loc_code");
            query.AppendLine("			 ,ploc_code");
            query.AppendLine("			 ,loc_name");
            query.AppendLine("			 ,ftr_idn");
            query.AppendLine("			 ,ftr_code");
            query.AppendLine("			 ,rel_loc_name");
            query.AppendLine("			 ,kt_gbn");
            query.AppendLine("			 ,rownum ord");
            query.AppendLine("		 from cm_location");
            query.AppendLine("		where 1 = 1");

            if (parameter["LOC_CODE"].ToString() == "O")
            {
                query.AppendLine("          and ftr_code = decode(:FTR_CODE, null, 'BZ001', decode(:FTR_CODE, 'BZ001', 'BZ002', 'BZ003'))");
                query.AppendLine("		start with ftr_code = decode(:LOC_CODE, 'O', 'BZ001')");
            }
            else
            {
                query.AppendLine("          and ftr_code = decode(:FTR_CODE, 'BZ000', 'BZ001', decode(:FTR_CODE, 'BZ001', 'BZ002', 'BZ003'))");
                query.AppendLine("		start with loc_code = :LOC_CODE");
            }

            query.AppendLine("		connect by prior loc_code = ploc_code");
            query.AppendLine("		order SIBLINGS by ftr_idn");
            query.AppendLine("	  ) c1");
            query.AppendLine("	   ,cm_location c2");
            query.AppendLine("	   ,cm_location c3");
            query.AppendLine(" where 1 = 1");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)");
            query.AppendLine(" order by c1.ord");
            query.AppendLine(")");
            query.AppendLine(" select loc.loc_code");
            query.AppendLine("	  ,loc.lblock");
            query.AppendLine("	  ,loc.mblock");
            query.AppendLine("	  ,loc.sblock");
            query.AppendLine("    ,(select max(iih.tagname) from if_ihtags iih, if_tag_gbn itg where iih.loc_code = loc.tag_loc_code and iih.tagname = itg.tagname and itg.tag_gbn = 'FRQ') out_flow_tag ");
            query.AppendLine("    ,nvl((");
            query.AppendLine("       select use_yn from df_setting where loc_code = loc.loc_code");
            query.AppendLine("     ),'N') use_yn");
            query.AppendLine("   from loc loc");

            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["FTR_CODE"];
            parameters[2].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측결과조회
        public DataSet SelectDfManagementDay_FlowMeter(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2	");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,round(to_number(a.df_tdfq)) df_tdfq");
            query.AppendLine("      ,round(to_number(b.rl_tdfq)) rl_tdfq");
            query.AppendLine("      ,(round(to_number(b.rl_tdfq)) - round(to_number(a.df_tdfq))) mi");
            query.AppendLine("      ,round((abs(b.rl_tdfq-a.df_tdfq) / abs(b.rl_tdfq))*100,2) error");
            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_tdfq");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_day a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.ftr_idn(+) = :FTR_IDN");
            query.AppendLine("       ) a");
            query.AppendLine("      ,(");
            query.AppendLine("       select to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,sum(c.value) rl_tdfq");
            query.AppendLine("         from if_tag_mapping a");
            query.AppendLine("             ,if_wide_ihtags b");
            query.AppendLine("             ,if_accumulation_hour c");
            query.AppendLine("        where a.ftr_idn = :FTR_IDN");
            query.AppendLine("          and b.tagname = a.tagname");
            query.AppendLine("          and b.metdvicd = '000004'");
            query.AppendLine("          and c.tagname = b.tagname");
            query.AppendLine("         group by to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd')");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine(" order by a.df_rl_dt");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                };
            
            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["FTR_IDN"];
            parameters[4].Value = parameter["FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측결과조회
        public DataSet SelectDfManagementDay_Block(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,round(to_number(a.df_tdfq)) df_tdfq");
            query.AppendLine("      ,round(to_number(b.rl_tdfq)) rl_tdfq");
            query.AppendLine("      ,(round(to_number(b.rl_tdfq)) - round(to_number(a.df_tdfq))) mi");
            query.AppendLine("      ,decode(b.rl_tdfq,0,null,round((abs(b.rl_tdfq-a.df_tdfq) / abs(b.rl_tdfq))*100,2)) error");
            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_tdfq");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_day a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.loc_code(+) = :LOC_CODE");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select to_date(to_char(d.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,sum(d.value) rl_tdfq");
            query.AppendLine("         from cm_location a");
            query.AppendLine("             ,if_ihtags b");
            query.AppendLine("             ,if_tag_gbn c");
            query.AppendLine("             ,if_accumulation_hour d");
            query.AppendLine("        where a.loc_code = :LOC_CODE");
            query.AppendLine("          and b.loc_code =");
            query.AppendLine("              decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            query.AppendLine("          and c.tagname = b.tagname");
            query.AppendLine("          and c.tag_gbn = 'FRQ'");
            query.AppendLine("          and d.tagname = c.tagname");
            query.AppendLine("        group by to_date(to_char(d.timestamp-(1/24),'yyyymmdd'),'yyyymmdd')");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine(" order by a.df_rl_dt");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["LOC_CODE"];
            parameters[4].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectDfManagementDay_Reservior(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2	");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,round(to_number(a.df_tdfq)) df_tdfq");
            query.AppendLine("      ,round(to_number(b.rl_tdfq)) rl_tdfq");
            query.AppendLine("      ,(round(to_number(b.rl_tdfq)) - round(to_number(a.df_tdfq))) mi");
            query.AppendLine("      ,decode(b.rl_tdfq,0,null,round((abs(b.rl_tdfq-a.df_tdfq) / abs(b.rl_tdfq))*100,2)) error");
            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_tdfq");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_day a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.loc_code(+) = :LOC_CODE");
            query.AppendLine("       ) a");
            query.AppendLine("      ,(");
            query.AppendLine("       select to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,sum(c.value) rl_tdfq");
            query.AppendLine("         from if_accumulation_hour c");
            query.AppendLine("             ,(");

            query.AppendLine("                select out_flow_tag tagname ");
            query.AppendLine("                  from we_supply_area ");
            query.AppendLine("                 where supy_ftr_idn = :LOC_CODE ");

            //query.AppendLine("              select c.tagname");
            //query.AppendLine("                from cm_location a");
            //query.AppendLine("                    ,if_ihtags b");
            //query.AppendLine("	                  ,if_tag_gbn c");
            //query.AppendLine("               where a.res_code = :LOC_CODE");
            //query.AppendLine("                 and b.loc_code = decode(a.kt_gbn, '002',");
            //query.AppendLine("                     (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code) ");
            //query.AppendLine("                 and c.tagname = b.tagname");
            //query.AppendLine("                 and c.tag_gbn = 'FRQ_O'");
            

            query.AppendLine("              ) d");
            query.AppendLine("        where c.tagname = d.tagname");
            query.AppendLine("         group by to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd')");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine(" order by a.df_rl_dt");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };
            
            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["LOC_CODE"];
            parameters[4].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측결과조회
        public DataSet SelectDfManagementHour_FlowMeter(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select a.timestamp");
            query.AppendLine("      ,b.time");
            query.AppendLine("  from (");
            query.AppendLine("       select to_date(:TARGETDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("       connect by rownum < to_date(:TARGETDATE,'yyyymmdd') - to_date(:TARGETDATE,'yyyymmdd') + 2 ");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select ltrim(to_char(rownum-1,'00')) time");
            query.AppendLine("         from dual connect by rownum <= 24");
            query.AppendLine("       ) b");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,a.df_rl_hour + 1 df_rl_hour");
            query.AppendLine("      ,round(to_number(a.df_thfq)) df_thfq");
            query.AppendLine("      ,round(to_number(b.rl_thfq)) rl_thfq");
            query.AppendLine("      ,(round(to_number(b.rl_thfq))-round(to_number(a.df_thfq))) mi");
            query.AppendLine("      ,decode(b.rl_thfq, 0, null, round((abs(b.rl_thfq-a.df_thfq)/abs(b.rl_thfq))*100,2)) error");
            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_thfq");
            query.AppendLine("             ,temp.time df_rl_hour");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_hour a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.df_rl_hour(+) = temp.time");
            query.AppendLine("          and a.ftr_idn(+) = :FTR_IDN");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,to_char(c.timestamp-(1/24),'hh24') df_rl_hour");
            query.AppendLine("             ,c.value rl_thfq");
            query.AppendLine("         from if_tag_mapping a");
            query.AppendLine("             ,if_wide_ihtags b");
            query.AppendLine("             ,if_accumulation_hour c");
            query.AppendLine("        where a.ftr_idn = :FTR_IDN");
            query.AppendLine("          and b.tagname = a.tagname");
            query.AppendLine("          and b.metdvicd = '000004'");
            query.AppendLine("          and c.tagname = b.tagname");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine("   and b.df_rl_hour(+) = a.df_rl_hour");
            query.AppendLine(" order by a.df_rl_dt, a.df_rl_hour");

            IDataParameter[] parameters =  {
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TARGETDATE"];
            parameters[1].Value = parameter["TARGETDATE"];
            parameters[2].Value = parameter["TARGETDATE"];
            parameters[3].Value = parameter["FTR_IDN"];
            parameters[4].Value = parameter["FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측결과조회
        public DataSet SelectDfManagementHour_Block(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select a.timestamp");
            query.AppendLine("      ,b.time");
            query.AppendLine("  from (");
            query.AppendLine("       select to_date(:TARGETDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("       connect by rownum < to_date(:TARGETDATE,'yyyymmdd') - to_date(:TARGETDATE,'yyyymmdd') + 2");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select ltrim(to_char(rownum-1,'00')) time");
            query.AppendLine("         from dual connect by rownum <= 24");
            query.AppendLine("       ) b");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,a.df_rl_hour + 1 df_rl_hour");
            query.AppendLine("      ,round(to_number(a.df_thfq)) df_thfq");
            query.AppendLine("      ,round(to_number(b.rl_thfq)) rl_thfq");
            query.AppendLine("      ,(round(to_number(b.rl_thfq))-round(to_number(a.df_thfq))) mi");
            query.AppendLine("      ,decode(b.rl_thfq, 0, null, round((abs(b.rl_thfq-a.df_thfq)/abs(b.rl_thfq))*100,2)) error");
            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_thfq");
            query.AppendLine("             ,temp.time df_rl_hour");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_hour a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.df_rl_hour(+) = temp.time");
            query.AppendLine("          and a.loc_code(+) = :LOC_CODE");
            query.AppendLine("       ) a");
            query.AppendLine("      ,(");
            query.AppendLine("       select to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,to_char(c.timestamp-(1/24),'hh24') df_rl_hour");
            query.AppendLine("             ,sum(c.value) rl_thfq");
            query.AppendLine("         from if_accumulation_hour c");
            query.AppendLine("             ,(");
            query.AppendLine("              select c.tagname");
            query.AppendLine("                from cm_location a");
            query.AppendLine("                    ,if_ihtags b");
            query.AppendLine("	                  ,if_tag_gbn c");
            query.AppendLine("               where a.loc_code = :LOC_CODE");
            query.AppendLine("                 and b.loc_code = decode(a.kt_gbn, '002',");
            query.AppendLine("                     (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code)");
            query.AppendLine("                 and c.tagname = b.tagname");
            query.AppendLine("                 and c.tag_gbn = 'FRQ'");
            query.AppendLine("              ) d");
            query.AppendLine("        where c.tagname = d.tagname");
            query.AppendLine("        group by to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd'), to_char(c.timestamp-(1/24),'hh24')");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine("   and b.df_rl_hour(+) = a.df_rl_hour");
            query.AppendLine(" order by a.df_rl_dt, a.df_rl_hour");

            IDataParameter[] parameters =  {
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TARGETDATE"];
            parameters[1].Value = parameter["TARGETDATE"];
            parameters[2].Value = parameter["TARGETDATE"];
            parameters[3].Value = parameter["LOC_CODE"];
            parameters[4].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측결과조회
        public DataSet SelectDfManagementHour_Reservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select a.timestamp");
            query.AppendLine("      ,b.time");
            query.AppendLine("  from (");
            query.AppendLine("       select to_date(:TARGETDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("       connect by rownum < to_date(:TARGETDATE,'yyyymmdd') - to_date(:TARGETDATE,'yyyymmdd') + 2 ");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select ltrim(to_char(rownum-1,'00')) time");
            query.AppendLine("         from dual connect by rownum <= 24");
            query.AppendLine("       ) b");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,a.df_rl_hour + 1 df_rl_hour");
            query.AppendLine("      ,round(to_number(a.df_thfq)) df_thfq");
            query.AppendLine("      ,round(to_number(b.rl_thfq)) rl_thfq");
            query.AppendLine("      ,(round(to_number(b.rl_thfq))-round(to_number(a.df_thfq))) mi");

            query.AppendLine("      ,decode(b.rl_thfq, 0, null, round((abs(b.rl_thfq-a.df_thfq)/abs(b.rl_thfq))*100,2)) error");

            //query.AppendLine("      ,round((abs(b.rl_thfq-a.df_thfq)/abs(a.df_thfq))*100,2) error");

            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_thfq");
            query.AppendLine("             ,temp.time df_rl_hour");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_hour a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.df_rl_hour(+) = temp.time");
            query.AppendLine("          and a.loc_code(+) = :LOC_CODE");
            query.AppendLine("       ) a");
            query.AppendLine("      ,(");
            query.AppendLine("       select to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,to_char(c.timestamp-(1/24),'hh24') df_rl_hour");
            query.AppendLine("             ,c.value rl_thfq");
            query.AppendLine("         from if_accumulation_hour c");
            query.AppendLine("             ,(");

            query.AppendLine("                select out_flow_tag tagname ");
            query.AppendLine("                  from we_supply_area ");
            query.AppendLine("                 where supy_ftr_idn = :LOC_CODE ");

            //query.AppendLine("              select c.tagname");
            //query.AppendLine("                from cm_location a");
            //query.AppendLine("                    ,if_ihtags b");
            //query.AppendLine("	                  ,if_tag_gbn c");
            //query.AppendLine("               where a.res_code = :LOC_CODE");
            //query.AppendLine("                 and b.loc_code = decode(a.kt_gbn, '002',");
            //query.AppendLine("                     (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code) ");
            //query.AppendLine("                 and c.tagname = b.tagname");
            //query.AppendLine("                 and c.tag_gbn = 'FRQ_O'");

            query.AppendLine("              ) d");
            query.AppendLine("        where c.tagname = d.tagname");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine("   and b.df_rl_hour(+) = a.df_rl_hour");
            query.AppendLine(" order by a.df_rl_dt, a.df_rl_hour");

            IDataParameter[] parameters =  {
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("TARGETDATE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2),
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TARGETDATE"];
            parameters[1].Value = parameter["TARGETDATE"];
            parameters[2].Value = parameter["TARGETDATE"];
            parameters[3].Value = parameter["LOC_CODE"];
            parameters[4].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측결과조회
        public DataSet SelectDfManagementResultHour_FlowMeter(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select a.timestamp");
            query.AppendLine("      ,b.time");
            query.AppendLine("  from (");
            query.AppendLine("       select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("       connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2 ");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select ltrim(to_char(rownum-1,'00')) time");
            query.AppendLine("         from dual connect by rownum <= 24");
            query.AppendLine("       ) b");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,a.df_rl_hour + 1 df_rl_hour");
            query.AppendLine("      ,round(to_number(a.df_thfq)) df_thfq");
            query.AppendLine("      ,round(to_number(b.rl_thfq)) rl_thfq");
            query.AppendLine("      ,(round(to_number(b.rl_thfq))-round(to_number(a.df_thfq))) mi");
            query.AppendLine("      ,decode(b.rl_thfq, null, null, round((abs(b.rl_thfq-a.df_thfq)/abs(b.rl_thfq))*100,2)) error");
            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_thfq");
            query.AppendLine("             ,temp.time df_rl_hour");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_hour a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.df_rl_hour(+) = temp.time");
            query.AppendLine("          and a.ftr_idn(+) = :FTR_IDN");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,to_char(c.timestamp-(1/24),'hh24') df_rl_hour");
            query.AppendLine("             ,c.value rl_thfq");
            query.AppendLine("         from if_tag_mapping a");
            query.AppendLine("             ,if_wide_ihtags b");
            query.AppendLine("             ,if_accumulation_hour c");
            query.AppendLine("        where a.ftr_idn = :FTR_IDN");
            query.AppendLine("          and b.tagname = a.tagname");
            query.AppendLine("          and b.metdvicd = '000004'");
            query.AppendLine("          and c.tagname = b.tagname");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine("   and b.df_rl_hour(+) = a.df_rl_hour");
            query.AppendLine(" order by a.df_rl_dt, a.df_rl_hour");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["FTR_IDN"];
            parameters[4].Value = parameter["FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        //수요예측결과조회
        public DataSet SelectDfManagementResultHour_Reservoir(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with temp as");
            query.AppendLine("(");
            query.AppendLine("select a.timestamp");
            query.AppendLine("      ,b.time");
            query.AppendLine("  from (");
            query.AppendLine("       select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 timestamp from dual");
            query.AppendLine("       connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2 ");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select ltrim(to_char(rownum-1,'00')) time");
            query.AppendLine("         from dual connect by rownum <= 24");
            query.AppendLine("       ) b");
            query.AppendLine(")");
            query.AppendLine("select a.df_rl_dt");
            query.AppendLine("      ,a.df_rl_hour + 1 df_rl_hour");
            query.AppendLine("      ,round(to_number(a.df_thfq)) df_thfq");
            query.AppendLine("      ,round(to_number(b.rl_thfq)) rl_thfq");
            query.AppendLine("      ,(round(to_number(b.rl_thfq))-round(to_number(a.df_thfq))) mi");
            query.AppendLine("      ,round((abs(b.rl_thfq-a.df_thfq)/abs(b.rl_thfq))*100,2) error");
            query.AppendLine("  from (");
            query.AppendLine("       select temp.timestamp df_rl_dt");
            query.AppendLine("             ,a.df_thfq");
            query.AppendLine("             ,temp.time df_rl_hour");
            query.AppendLine("         from temp");
            query.AppendLine("             ,df_result_hour a");
            query.AppendLine("        where a.df_rl_dt(+) = temp.timestamp");
            query.AppendLine("          and a.df_rl_hour(+) = temp.time");
            query.AppendLine("          and a.ftr_idn(+) = :FTR_IDN");
            query.AppendLine("       ) a,");
            query.AppendLine("       (");
            query.AppendLine("       select to_date(to_char(c.timestamp-(1/24),'yyyymmdd'),'yyyymmdd') df_rl_dt");
            query.AppendLine("             ,to_char(c.timestamp-(1/24),'hh24') df_rl_hour");
            query.AppendLine("             ,c.value rl_thfq");
            query.AppendLine("         from if_accumulation_hour c");
            query.AppendLine("        where c.tagname = :OUT_FLOW_TAG");
            query.AppendLine("       ) b");
            query.AppendLine(" where b.df_rl_dt(+) = a.df_rl_dt");
            query.AppendLine("   and b.df_rl_hour(+) = a.df_rl_hour");
            query.AppendLine(" order by a.df_rl_dt, a.df_rl_hour");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("OUT_FLOW_TAG", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["FTR_IDN"];
            parameters[4].Value = parameter["OUT_FLOW_TAG"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public object SelectDfSimulationUse(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select nvl((select use_yn from df_setting where ftr_idn = :FTR_IDN),'N') from dual               ");


            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_IDN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //유량계 태그조회
        public DataSet SelectFlowMetaTag(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
              
            query.AppendLine("select max(decode(b.metdvicd, '000001', a.tagname)) tag1");
            query.AppendLine("      ,max(decode(b.metdvicd, '000004', a.tagname)) tag2");
            query.AppendLine("      ,max(decode(b.metdvicd, '000003', a.tagname)) tag3");
            query.AppendLine("  from if_tag_mapping a");
            query.AppendLine("      ,if_wide_ihtags b");
            query.AppendLine(" where a.ftr_idn = :FTR_IDN");
            query.AppendLine("   and a.ftr_gbn = '000001'");
            query.AppendLine("   and b.tagname = a.tagname");

            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }
    }
}
