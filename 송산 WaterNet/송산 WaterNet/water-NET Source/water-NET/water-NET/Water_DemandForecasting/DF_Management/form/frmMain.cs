﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using System.Data;
using WaterNet.WV_Common.interface1;
using WaterNet.DF_Management.work;
using ChartFX.WinForms.Annotation;
using WaterNet.WaterNetCore;
using WaterNet.DF_Common.interface1;
using WaterNet.WV_Common.cal;
using WaterNet.WV_Common.control;
using EMFrame.log;

namespace WaterNet.DF_Management.form
{
    public partial class frmMain : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        //차트 매니저(차트 스타일 설정및 기타공통기능설정)
        private ChartManager chartManager = null;

        //MainMap 을 제어하기 위한 인스턴스 변수
        private IMapControl mainMap = null;

        //테이블// 그래프 레이아웃 
        private TableLayout tableLayout = null;

        private BlockComboboxControl blockControl = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControl MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        public frmMain(Hashtable flowmeterInfo)
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeGridColumn();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);
            this.gridManager.Add(this.ultraGrid3);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance.TextVAlign = VAlign.Middle;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;

            this.ultraGrid2.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.Top;
            this.ultraGrid2.DisplayLayout.Bands[0].ColHeaderLines = 2;

            this.ultraGrid3.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid3.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid3.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;

            this.ultraGrid3.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.Top;
            this.ultraGrid3.DisplayLayout.Bands[0].ColHeaderLines = 2;

            this.ultraGrid2.DisplayLayout.CaptionVisible = DefaultableBoolean.True;
            this.ultraGrid3.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            this.ultraGrid2.Text = "일공급량 수요예측";
            this.ultraGrid3.Text = "시간공급량 수요예측";
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LBLOCK";
                column.Header.Caption = "대블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("MBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "MBLOCK";
                column.Header.Caption = "중블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("SBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "SBLOCK";
                column.Header.Caption = "소블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 520;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측실행여부";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 120;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 5;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            //수요예측 일별결과
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DF_RL_DT";
            column.Header.Caption = "일자";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "RL_TDFQ";
            column.Header.Caption = "실측량\n(㎥/일)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DF_TDFQ";
            column.Header.Caption = "예측량\n(㎥/일)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MI";
            column.Header.Caption = "실측량-예측량\n(㎥/일)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ERROR";
            column.Header.Caption = "오차율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "###,###,###.00";

            //수요예측 시간결과
            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DF_RL_DT";
            column.Header.Caption = "일자";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DF_RL_HOUR";
            column.Header.Caption = "시간";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "RL_THFQ";
            column.Header.Caption = "실측량\n(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DF_THFQ";
            column.Header.Caption = "예측량\n(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MI";
            column.Header.Caption = "실측량-예측량\n(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;
            column.Format = "###,###,###";

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ERROR";
            column.Header.Caption = "오차율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            column.Format = "###,###,###.00";

            #endregion
        }

        private void InitializeBlockGridColumn()
        {
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LBLOCK";
                column.Header.Caption = "대블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("MBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "MBLOCK";
                column.Header.Caption = "중블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("SBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "SBLOCK";
                column.Header.Caption = "소블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 520;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측실행여부";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 120;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 5;
                column.RowLayoutColumnInfo.OriginY = 0;
            }
        }

        private void InitializeBlockValueList()
        {
            this.ultraGrid1.DisplayLayout.ValueLists.Clear();
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN") &&
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") != -1)
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }
        }

        private void InitializeReservoirGridColumn()
        {
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("FTR_IDN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "FTR_IDN";
                column.Header.Caption = "배수지관리번호";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 100;
                column.Hidden = true;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_NAME") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_NAME";
                column.Header.Caption = "배수지명";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 130;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 590;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측실행여부";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 120;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 1;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }
        }

        private void InitializeReservoirValueList()
        {
            this.ultraGrid1.DisplayLayout.ValueLists.Clear();
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN") &&
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") != -1)
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }
        }

        private void InitializeValueList()
        {
            //DataTable reservoir = DfManagementWork.GetInstance().SelectReservoir();
            //ComboBoxUtils.SetMember(this.res_code, "CODE", "CODE_NAME");
            //this.res_code.DataSource = reservoir;
            ComboBoxUtils.SetMember(this.gubun, "CODE", "CODE_NAME");
            DataTable gubun = new DataTable();
            gubun.Columns.Add("CODE");
            gubun.Columns.Add("CODE_NAME");
            gubun.Rows.Add("1", "블록");
            gubun.Rows.Add("2", "배수지");
            this.gubun.DataSource = gubun;
        }

        private bool isInitSummaryRows = false;

        /// <summary>
        /// 그리드내 계산식이 포함된 행을 추가한다.
        /// </summary>
        private void SetSummaryRows()
        {
            this.ultraGrid3.DisplayLayout.Bands[0].Summaries.Clear();

            SummarySettings summary = null;

            UltraGridColumn gridColumn = null;
            
            gridColumn = this.ultraGrid3.DisplayLayout.Bands[0].Columns[0];
            summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new DF(), gridColumn, SummaryPosition.UseSummaryPositionColumn, gridColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Key = gridColumn.Key + "_DF";

            if (gridColumn.Index == 0)
            {
                summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_DF"];
                summary.DisplayFormat = "잔여 예측량";
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            }

            if (gridColumn.Index != 0 && !this.isInitSummaryRows)
            {
                summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_DF"];
                summary.DisplayFormat = "  ";
            }
            else if (gridColumn.Index != 0 && this.isInitSummaryRows)
            {
                summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_DF"];
                summary.DisplayFormat = "{0:###,###,##0}";
            }

            gridColumn = this.ultraGrid3.DisplayLayout.Bands[0].Columns[1];
            summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new DF(), this.ultraGrid3.DisplayLayout.Bands[0].Columns[5], SummaryPosition.UseSummaryPositionColumn, gridColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Key = gridColumn.Key + "_DF";

            if (gridColumn.Index == 0)
            {
                summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_DF"];
                summary.DisplayFormat = "잔여 예측량";
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            }

            if (gridColumn.Index != 0 && !this.isInitSummaryRows)
            {
                summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_DF"];
                summary.DisplayFormat = "  ";
            }
            else if (gridColumn.Index != 0 && this.isInitSummaryRows)
            {
                summary = this.ultraGrid3.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_DF"];
                summary.DisplayFormat = "{0:###,###,##0}";
            }

            this.isInitSummaryRows = true;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.gubun.SelectedValueChanged += new EventHandler(SelectedValueChanged);
            this.lblock.SelectedValueChanged += new EventHandler(SelectedValueChanged);
            this.mblock.SelectedValueChanged += new EventHandler(SelectedValueChanged);

            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);

            this.radioButton1.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.radioButton1.CheckedChanged += new EventHandler(radioButton_CheckedChanged);

            this.ultraGrid2.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid2_InitializeLayout);
            this.ultraGrid2.AfterRowActivate += new EventHandler(ultraGrid2_AfterRowActivate);

            this.ultraGrid3.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid3_InitializeLayout);
            this.chart1.SizeChanged += new EventHandler(chart1_SizeChanged);
        }

        private void SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.gubun.SelectedValue.ToString() == "1")
            {
                this.ultraGrid1.DataSource = null;

                this.InitializeBlockGridColumn();
                this.InitializeBlockValueList();

                Hashtable parameter = new Hashtable();

                DataRowView selectedItem = null;

                if (this.sblock != null && this.sblock.SelectedIndex > 0)
                {
                    selectedItem = this.sblock.SelectedItem as DataRowView;
                }
                else if (this.mblock != null && this.mblock.SelectedIndex > 0)
                {
                    selectedItem = this.mblock.SelectedItem as DataRowView;
                }
                else
                {
                    selectedItem = this.lblock.SelectedItem as DataRowView;
                }

                if (selectedItem != null)
                {
                    parameter["LOC_CODE"] = selectedItem["LOC_CODE"];
                    parameter["FTR_IDN"] = selectedItem["FTR_IDN"];
                    parameter["FTR_CODE"] = selectedItem["FTR_CODE"];
                    parameter["LOC_NAME"] = selectedItem["LOC_NAME"];
                }

                this.ultraGrid1.DataSource = DfManagementWork.GetInstance().SelectDfManagement(parameter);
            }
            else if (this.gubun.SelectedValue.ToString() == "2")
            {
                if (sender.Equals(this.lblock) || sender.Equals(this.mblock))
                {
                    return;
                }

                this.ultraGrid1.DataSource = null;

                this.InitializeReservoirGridColumn();
                this.InitializeReservoirValueList();

                this.ultraGrid1.DataSource = DfManagementWork.GetInstance().SelectDfManagement();
            }
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.blockControl = new BlockComboboxControl(this.lblock, this.mblock, this.sblock);
            this.blockControl.AddDefaultOption(LOCATION_TYPE.All, false);

            this.tableLayout = new TableLayout(this.tableLayoutPanel1, this.chartPanel1, this.tablePanel1, this.chartVisibleBtn, this.tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            this.startdate.Value = DateTime.Now.AddDays(-6).ToString("yyyy-MM-dd");
            this.enddate.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }

        private Hashtable parameter = null;

        //검색 버튼 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = Utils.ConverToHashtable(this.groupBox1);

                this.parameter["LOC_CODE"] = this.ultraGrid1.ActiveRow.Cells["LOC_CODE"].Value.ToString();
                this.parameter["GUBUN"] = this.gubun.SelectedValue.ToString();

                this.SelectDfManagementDay();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //엑셀 버튼 이벤트 핸들러
        private void excelBtn_Click(object sender, EventArgs e)
        {
            FormManager.ExportToExcelFromUltraGrid(this.ultraGrid2,this.ultraGrid3, this.Text, 3);
        }

        //일수요에측, 시간수요예측 체크 변경 이벤트 핸들러
        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.InitializeChartSetting();
        }

        private void ultraGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["DF_RL_DT"].SortIndicator = SortIndicator.Descending;

            if (e.Layout.Rows.Count > 0)
            {
                e.Layout.Rows[0].Selected = true;
            }
            if (e.Layout.Rows.Count > 0)
            {
                e.Layout.Rows[0].Activated = true;
            }
        }

        private void ultraGrid2_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid2.ActiveRow == null)
            {
                return;
            }

            this.parameter["TARGETDATE"] = Convert.ToDateTime(this.ultraGrid2.ActiveRow.Cells["DF_RL_DT"].Value).ToString("yyyyMMdd");

            this.SelectDfManagementHour();
        }

        private void ultraGrid3_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["DF_RL_HOUR"].SortIndicator = SortIndicator.Descending;
        }

        //수요예측결과조회
        private void SelectDfManagementDay()
        {
            this.Cursor = Cursors.WaitCursor;

            this.ultraGrid2.DataSource = DfManagementWork.GetInstance().SelectDfManagementDay(this.parameter);

            this.Cursor = Cursors.Default;
        }

        //수요예측결과조회
        private void SelectDfManagementHour()
        {
            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid3.DataSource = DfManagementWork.GetInstance().SelectDfManagementHour(this.parameter);
            this.InitializeChartSetting();

            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.chartManager.AllClear(this.chart1);
            this.chart1.Extensions.Clear();
            this.chart1.Data.Clear();
            if (this.ultraGrid2.DataSource == null || this.ultraGrid3.DataSource == null)
            {
                return;
            }

            if (this.radioButton1.Checked)
            {
                this.chart1.Data.Series = 2;
                this.chart1.Data.Points = this.ultraGrid2.Rows.Count;

                int pointIndex = 0;

                DataTable table = null;

                table = (DataTable)this.ultraGrid2.DataSource;

                foreach (DataRow row in table.Rows)
                {
                    this.chart1.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["DF_RL_DT"]).ToString("yyyy-MM-dd");

                    if (Utils.ToDouble(row["RL_TDFQ"]) != 0)
                    {
                        this.chart1.Data[0, pointIndex] = Utils.ToDouble(row["RL_TDFQ"]);
                    }

                    if (Utils.ToDouble(row["DF_TDFQ"]) != 0)
                    {
                        this.chart1.Data[1, pointIndex] = Utils.ToDouble(row["DF_TDFQ"]);
                    }
                    pointIndex++;
                }

                this.chart1.Series[0].Text = "실측량";
                this.chart1.Series[1].Text = "예측량";

                this.chart1.AxisY.Title.Text = "유량(㎥/일)";
            }
            if (this.radioButton2.Checked)
            {
                Annotations annots = new Annotations();
                this.chart1.Extensions.Add(annots);
                this.chart1.Data.Series = 2;
                this.chart1.Data.Points = this.ultraGrid3.Rows.Count;

                int pointIndex = 0;

                DataTable table = null;

                table = (DataTable)this.ultraGrid3.DataSource;

                double rl_thfq = 0;
                double df_thfq = 0;

                foreach (DataRow row in table.Rows)
                {
                    this.chart1.AxisX.Labels[pointIndex] = row["DF_RL_HOUR"].ToString();

                    if (Utils.ToDouble(row["RL_THFQ"]) != 0)
                    {
                        this.chart1.Data[0, pointIndex] = Utils.ToDouble(row["RL_THFQ"]);
                        rl_thfq += Utils.ToDouble(row["RL_THFQ"]);
                    }

                    if (Utils.ToDouble(row["DF_THFQ"]) != 0)
                    {
                        this.chart1.Data[1, pointIndex] = Utils.ToDouble(row["DF_THFQ"]);
                        df_thfq += Utils.ToDouble(row["DF_THFQ"]);
                    }
                    pointIndex++;
                }

                this.chart1.Series[0].Text = "실측량";
                this.chart1.Series[1].Text = "예측량";

                this.chart1.AxisY.Title.Text = "유량(㎥/h)";

                DateTime df_rl_dt = Convert.ToDateTime(this.ultraGrid2.Selected.Rows[0].Cells["DF_RL_DT"].Value);
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                string result = string.Empty;

                result = "금일예측량       : " + Math.Round(df_thfq).ToString("###,###,###") + "\n현재누적공급량 : " + rl_thfq.ToString("###,###,###") + "\n잔여예측량       : " + Math.Round((df_thfq - rl_thfq)).ToString("###,###,###");

                AnnotationText text = new AnnotationText();
                text.Text = result;
                text.Align = StringAlignment.Near;
                text.Left = this.chart1.Right - 175;
                text.Top = this.chart1.Height - (this.chart1.Height - 30);

                annots.List.Add(text);
            }
        }

        private void chart1_SizeChanged(object sender, EventArgs e)
        {
            Annotations a = this.chart1.Extensions[0] as Annotations;

            if (a == null)
            {
                return;
            }

            foreach (AnnotationObject ao in a.List)
            {
                ao.Left = this.chart1.Right - 175;
                ao.Top = this.chart1.Height - (this.chart1.Height - 30);
            }
        }
    }
}
