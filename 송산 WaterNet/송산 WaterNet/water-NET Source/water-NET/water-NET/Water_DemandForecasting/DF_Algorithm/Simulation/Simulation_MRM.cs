﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
//using WaterNet.DF_Algorithm.MatrixLibrary;
using MatrixLibrary;
using System.IO;
using System.Windows.Forms;


//namespace WaterNet.DF_Algorithm.MRM_Core
namespace WaterNet.DF_Algorithm.Simulation
{
    /// <summary>
    /// 다중 회귀 모델 시뮬레이션 클래스
    /// </summary>
    /// 
    /// <remarks>다중 회귀 모델을 구성하고 시뮬레이션 하기 위한 객체이다.
    /// </remarks>
    public class Simulation_MRM
    {

        /// <summary>
        /// 모델 학습을 위한 유량 및 기상 데이터 입력 값 및 원하는 유량 출력(원시값)
        /// 마지막 column은 원하는 유량 출력 값(원시값)
        /// </summary>
        public double[,] data;
        /// <summary>
        /// 모델 차수를 고려하여 확장한 데이터
        /// </summary>
        public double[,] data_dimension;
        /// <summary>
        /// 모델 차수를 고려하여 확장한 데이터(Normalize값)
        /// </summary>
        public double[,] normalize_data_dimension;

        /// <summary>
        /// 입력 변수 별로 차수를 저장하는 배열
        /// </summary>
        public int[] dimension;

        /// <summary>
        /// 모든 입력 변수 별 차수를 더한 값
        /// </summary>
        public int dimension_sum;

        /// <summary>
        /// 수요 예측을 위한 유량 및 기상 데이터 입력 배열 (원시값)
        /// </summary>
        public double[,] prediction_data;
        /// <summary>
        /// 수요 예측을 위한 유량 및 기상 데이터 입력 값을 각 차수만큼 확장한 배열 (원시값)
        /// </summary>
        public double[,] prediction_data_dimension;
        /// <summary>
        /// 수요 예측을 위한 유량 및 기상 데이터 입력 값을 각 차수만큼 확장한 배열 (Noramlize값)
        /// </summary>
        public double[,] normalize_prediction_data_dimension;

        /// <summary>
        /// 입력 변수별 최대치(배열 형태)
        /// </summary>
        public double[] xMax;
        /// <summary>
        /// 입력 변수별 최소치(배열 형태)
        /// </summary>
        public double[] xMin;
        /// <summary>
        /// 출력 변수별 최대치
        /// </summary>
        public double yMax;
        /// <summary>
        /// 출력 변수별 최소치
        /// </summary>
        public double yMin;
        public double[] xFactor;
        public double yFactor;

        /// <summary>
        /// 수요 예측 결과 일 유량 값
        /// </summary>
        public double Prediction_Result;

        /// <summary>
        /// Training 에러 평균값
        /// </summary>
        public double Error_Training_Mean;

        /// <summary>
        /// Training 정확도 평균값
        /// </summary>
        public double ACC_Training_MAPE;
        /// <summary>
        /// Data별 Training 에러값
        /// </summary>
        public double[] Error_Training;

        /// <summary>
        /// Test 에러 평균값
        /// </summary>
        public double Error_Test_Mean;
        /// <summary>
        /// Test 정확도 평균값
        /// </summary>
        public double ACC_Test_MAPE;
        /// <summary>
        /// Data별 Test 에러 평균값
        /// </summary>
        public double[] Error_Test;

        /// <summary>
        /// 일 유량 수요 예측 에러 값
        /// </summary>
        public double Error_Prediction;

        /// <summary>
        /// 모델 학습 입력 행렬
        /// </summary>
        public Matrix M_train_input;
        /// <summary>
        /// 모델 학습 원하는 출력 행렬
        /// </summary>
        public Matrix M_train_output;
        /// <summary>
        /// 테스트용 입력 행렬
        /// </summary>
        public Matrix M_test_input;
        /// <summary>
        /// 테스트용 출력 행렬
        /// </summary>
        public Matrix M_test_output;
        /// <summary>
        /// 다중 회귀 모델 계수 행렬
        /// </summary>
        public Matrix M_weight;

        /// <summary>
        /// 입력 수
        /// </summary>
        public int n_input;

        /// <summary>
        /// 샘플 개수
        /// </summary>
        public int n_samples;

        /// <summary>
        ///  <see cref="Simulation_MRM"/> 클래스의 객체를 초기화한다.
        /// </summary>
        public Simulation_MRM()
        {

        }
        /// <summary>
        /// Simulation_MRM 클래스의 파라미터 지정함수
        /// </summary>
        /// <param name="_data">학습시킬 유량 및 기상데이터 입력과 원하는 유량 출력으로 이루어진 2차 배열</param>
        /// <param name="_dimension">입력 변수별 차수</param>
        public void Set_Data(double[,] _data, int[] _dimension)
        {
            data = _data;
            n_samples = data.GetLength(0);

            dimension_sum = 0;
            dimension = _dimension;
            //입력 변수들의 차수의 총합 구하기
            for (int i = 0; i < dimension.GetLength(0); i++)
            {
                dimension_sum += dimension[i];
            }

        }


        // 입력 변수 수를 클래스 변수에 저장
        public void Set_Num_Input(int _input)
        {
            n_input = _input;
        }

        // 입력 변수의 최소치, 최대치를 가져와 클래스 변수에 저장
        public void Set_Param_MinMax_Input(double[] _minX, double[] _maxX)
        {
            xMin = new double[dimension_sum];
            xMax = new double[dimension_sum];
            xFactor = new double[dimension_sum];
            int idx = 0;
            for (int i = 0; i < n_input; i++)
            {
                for (int j = 1; j <= dimension[i]; j++)
                {
                    // 입력 변수, 입력 변수 제곱, 입력 변수 세제곱....
                    xMin[idx] = Math.Pow(_minX[i], j) * 1.0;
                    xMax[idx] = Math.Pow(_maxX[i], j) * 1.0;
                    xFactor[idx] = 2.0 / (xMax[idx] - xMin[idx]);
                    idx++;
                }
            }
        }

        // 출력 변수의 최소치, 최대치를 가져와 클래스 변수에 저장
        public void Set_Param_MinMax_Output(double _minY, double _maxY)
        {
            yMin = _minY * 1.0;
            yMax = _maxY * 1.0;
            yFactor = 2.0 / (yMax - yMin);

        }

        // 수요 예측을 위한 입력 데이터 클래스 변수에 저장
        public void Set_Prediction_Data(double[,] _prediction_data)
        {
            prediction_data = _prediction_data;
        }

        /// <summary>
        /// 입력 변수 별 차수에 맞게 데이터를 확장하여 저장한다.
        /// </summary>
        /// <param name="_data">학습시킬 유량 및 기상데이터 입력과 원하는 유량 출력으로 이루어진 2차 배열</param>
        public void Set_Expand_Data(double[,] _data)
        {
            // 입력 데이터가 이틀 이상 분량이라면 학습을 위한 데이터
            if (_data.GetLength(0) > 1)
            {
                // 확장된 입력 데이터는 입력 변수들의 차수를 모두 더하고 출력 변수 포함한 열을 가져야 한다.
                data_dimension = new double[_data.GetLength(0), dimension_sum + 1];
                for (int i = 0; i < _data.GetLength(0); i++)
                {
                    int idx = 0;
                    for (int j = 1; j <= n_input; j++)
                    {
                        for (int k = 1; k <= dimension[j - 1]; k++)
                        {
                            // 입력 A, 입력 A 제곱, 입력 A 세제곱, ... , 입력 B, 입력 B 제곱, .... 입력 C, 입력 C 제곱, ....
                            data_dimension[i, idx] = Math.Pow(_data[i, j - 1], k);
                            idx += 1;
                        }


                    }
                    // 출력 데이터는 원시값 그대로
                    data_dimension[i, dimension_sum] = _data[i, n_input];
                }
            }
            // 입력 데이터가 하루 분량이면 수요 예측을 위한 데이터
            else if (_data.GetLength(0) == 1)
            {
                // 확장된 수요 예측 입력 데이터는 입력 변수들의 차수를 모두 더한 열을 가져야 한다.
                prediction_data_dimension = new double[_data.GetLength(0), dimension_sum];
                for (int i = 0; i < _data.GetLength(0); i++)
                {
                    int idx = 0;
                    for (int j = 1; j <= n_input; j++)
                    {
                        for (int k = 1; k <= dimension[j - 1]; k++)
                        {
                            // 입력 A, 입력 A 제곱, 입력 A 세제곱, ... , 입력 B, 입력 B 제곱, .... 입력 C, 입력 C 제곱, ....
                            prediction_data_dimension[i, idx] = Math.Pow(_data[i, j - 1], k);
                            idx += 1;
                        }
                        //수요 예측을 위한 데이터이므로 출력 변수는 존재하지 않음

                    }

                }
            }



        }
        //public void Set_Normalize_Data()
        //{
        //    Data_Normalize(data_dimension);
        //}

        /// <summary>
        /// 데이터들을 Normalize하는 함수
        /// </summary>
        /// <param name="_data">학습시킬 유량 및 기상데이터 입력과 원하는 유량 출력, 또는 수요 예측에 필요한 입력 데이터 </param>
        /// 
        /// <remarks>입력 개수와 _data의 크기가 동일하면 수요 예측에 필요한 Normalization 데이터로 변환
        /// 입력 개수보다 _data의 크기가 하나 더 크면 네트워크 학습에 필요한 Noramlization 데이터로 변환</remarks>
        /// 
        /// 
        public void Data_Normalize(double[,] _data)
        {
            // 입력 데이터가 이틀 이상 분량이라면 학습을 위한 데이터
            if (_data.GetLength(0) > 1)
            {

                normalize_data_dimension = new double[_data.GetLength(0), dimension_sum + 1];
                // 데이터 전체 일수에 대해
                for (int i = 0; i < _data.GetLength(0); i++)
                {
                    // 데이터 입력 변수 및 그 차수들에 대해
                    for (int j = 0; j < dimension_sum; j++)
                    {
                        // -1 ~ 1 까지 노말라이즈
                        normalize_data_dimension[i, j] = (_data[i, j] - xMin[j]) * xFactor[j] - 1.0;
                    }

                    // 데이터 출력 변수에 대해
                    normalize_data_dimension[i, dimension_sum] = (_data[i, dimension_sum] - yMin) * yFactor - 1.0;
                }

            }
            // 입력 데이터가 하루 분량이면 수요 예측을 위한 데이터
            else if (_data.GetLength(0) == 1)
            {
                normalize_prediction_data_dimension = new double[_data.GetLength(0), dimension_sum];
                // 데이터 전체 일수에 대해
                for (int i = 0; i < _data.GetLength(0); i++)
                {
                    // 데이터 입력 변수 및 그 차수들에 대해
                    for (int j = 0; j < dimension_sum; j++)
                    {
                        // -1 ~ 1 까지 노말라이즈
                        normalize_prediction_data_dimension[i, j] = (_data[i, j] - xMin[j]) * xFactor[j] - 1.0;
                    }
                    // 수요 예측을 위한 데이터에는 출력 변수가 존재하지 않음
                }
            }

        }
        /// <summary>
        /// 데이터들을 Training과 Test 용도로 분리하고 각각을 행렬에 대입
        /// </summary>
        /// 
        ///<remarks>여러 날짜로 이루어진 학습데이터를 10등분하여 임의로 하나의 집합은 test set으로, 나머지 9개의 집합은
        /// training set로 쓴다. Test set은 10일 간격으로 배당된 데이터들로 이루어진다.</remarks>
        public void Initialize()
        {


            ////Data_Min_Max(data);
            //data = Data_Normalize(data);

            // 10-fold Cross Validation
            Random r = new Random();
            int randomidx = r.Next(0, 9);
            int n_test_data, n_train_data;

            // 전체 데이터의 1/10은 테스트용, 9/10은 학습용
            if (data_dimension.GetLength(0) % 10 > randomidx)
            {
                n_test_data = data_dimension.GetLength(0) / 10 + 1;
            }
            else
            {
                n_test_data = data_dimension.GetLength(0) / 10;
            }

            n_train_data = data_dimension.GetLength(0) - n_test_data;

            double[,] test_data = new double[n_test_data, dimension_sum + 1];
            double[,] train_data = new double[n_train_data, dimension_sum + 1];

            int test_data_idx = 0;
            int train_data_idx = 0;

            double[,] unnormalize_train_data_input = new double[n_train_data, dimension_sum + 1];
            double[,] unnormalize_train_data_output = new double[n_train_data, 1];
            double[,] unnormalize_test_data_input = new double[n_test_data, dimension_sum + 1];
            double[,] unnormalize_test_data_output = new double[n_test_data, 1];
            double[,] train_data_input = new double[n_train_data, dimension_sum + 1];
            double[,] train_data_output = new double[n_train_data, 1];
            double[,] test_data_input = new double[n_test_data, dimension_sum + 1];
            double[,] test_data_output = new double[n_test_data, 1];

            // 데이터의 전체 일수에 대하여
            for (int i = 0, k = data_dimension.GetLength(0); i < k; i++)
            {
                // 테스트용 데이터에 대해 노말라이즈된 입력과 원시 입력 값들 저장
                // [1 x1 x1^2 x1^3 ... x2 x2^2 ... x3 x3^2 ... y]
                if (i % 10 == randomidx)
                {
                    test_data_input[test_data_idx, 0] = 1.0;
                    unnormalize_test_data_input[test_data_idx, 0] = 1.0;
                    for (int j = 0; j < dimension_sum; j++)
                    {
                        test_data_input[test_data_idx, j + 1] = normalize_data_dimension[i, j];
                        unnormalize_test_data_input[test_data_idx, j + 1] = data_dimension[i, j];
                    }
                    test_data_output[test_data_idx, 0] = normalize_data_dimension[i, dimension_sum];
                    unnormalize_test_data_output[test_data_idx, 0] = data_dimension[i, dimension_sum];
                    test_data_idx++;
                }
                // 학습용 데이터에 대해 노말라이즈된 입력과 원시 입력 값들 저장
                else
                {

                    train_data_input[train_data_idx, 0] = 1.0;
                    unnormalize_train_data_input[train_data_idx, 0] = 1.0;
                    for (int j = 0; j < dimension_sum; j++)
                    {
                        train_data_input[train_data_idx, j + 1] = normalize_data_dimension[i, j];
                        unnormalize_train_data_input[train_data_idx, j + 1] = data_dimension[i, j];

                    }
                    train_data_output[train_data_idx, 0] = normalize_data_dimension[i, dimension_sum];
                    unnormalize_train_data_output[train_data_idx, 0] = data_dimension[i, dimension_sum];

                    train_data_idx++;
                }
            }

            //HSJ // double[,] input, output;

            M_train_input = new Matrix(train_data_input);
            M_train_output = new Matrix(train_data_output);
            M_weight = new Matrix(dimension_sum + 1, 1);
            M_test_input = new Matrix(test_data_input);
            M_test_output = new Matrix(test_data_output);

        }




        /// <summary>
        /// 다중 회귀 모델의 계수를 계산한다.
        /// </summary>
        public void Weight_Calculate()
        {
            //string strSaveFilePath = Application.StartupPath + "\\DF_data\\" + "training_MR.txt";
            //StreamWriter file = new StreamWriter(strSaveFilePath, false, System.Text.Encoding.ASCII);

            //string strSaveFilePath2 = Application.StartupPath + "\\DF_data\\" + "test_MR.txt";
            //StreamWriter file2 = new StreamWriter(strSaveFilePath2, false, System.Text.Encoding.ASCII);

            // Y=AX
            // A = [X'X]^(-1) * X * Y 
            Matrix M_temp = new Matrix(dimension_sum + 1, dimension_sum + 1);
            M_temp = Matrix.Multiply(Matrix.Transpose(M_train_input), M_train_input);
            M_temp = Matrix.PINV(M_temp);

            M_weight = Matrix.Multiply(Matrix.Multiply(Matrix.PINV(Matrix.Multiply
                (Matrix.Transpose(M_train_input), M_train_input)), Matrix.Transpose(M_train_input)), M_train_output);


            Error_Training = new double[M_train_input.NoRows];
            Error_Training_Mean = 0.0;
            ACC_Training_MAPE = 0.0;

            //file.Write("Training Error");
            //file.WriteLine();

            for (int i = 0, k = M_train_input.NoRows; i < k; i++)
            {
                // 다중 회귀 모델*학습용 입력 데이터와 실제 학습용 출력 데이터와의 오차
                Error_Training[i] = Math.Abs((M_train_output[i, 0] + 1.0) / yFactor + yMin - ((Matrix.Multiply(M_train_input, M_weight)[i, 0] + 1.0) / yFactor + yMin));
                Error_Training_Mean += Math.Abs((M_train_output[i, 0] + 1.0) / yFactor + yMin - ((Matrix.Multiply(M_train_input, M_weight)[i, 0] + 1.0) / yFactor + yMin));
                ACC_Training_MAPE += Math.Abs((M_train_output[i, 0] + 1.0) / yFactor + yMin - ((Matrix.Multiply(M_train_input, M_weight)[i, 0] + 1.0) / yFactor + yMin)) / ((M_train_output[i, 0] + 1.0) / yFactor + yMin);
            }
            // 학습 오차 평균
            Error_Training_Mean /= M_train_input.NoRows;
            ACC_Training_MAPE /= M_train_input.NoRows;
            // 학습 정확도
            ACC_Training_MAPE = 100.0 * (1.0 - ACC_Training_MAPE);

            for (int i = 0, k = M_train_input.NoRows; i < k; i++)
            {
                //file.Write("Error" + ":" + i.ToString() + "th data :" + Error_Training[i].ToString());
                //file.WriteLine();
            }
            //file.Write("Mean Error" + ":" + Error_Training_Mean + System.Environment.NewLine);
            //file.Close();


            Error_Test = new double[M_test_input.NoRows];
            Error_Test_Mean = 0.0;
            ACC_Test_MAPE = 0.0;

            //file2.Write("Test Error");
            //file2.WriteLine();

            for (int i = 0, k = M_test_input.NoRows; i < k; i++)
            {
                // 다중 회귀 모델*테스트용 입력 데이터와 실제 테스트용 출력 데이터와의 오차
                Error_Test[i] = Math.Abs((M_test_output[i, 0] + 1.0) / yFactor + yMin - ((Matrix.Multiply(M_test_input, M_weight)[i, 0] + 1.0) / yFactor + yMin));
                Error_Test_Mean += Math.Abs((M_test_output[i, 0] + 1.0) / yFactor + yMin - ((Matrix.Multiply(M_test_input, M_weight)[i, 0] + 1.0) / yFactor + yMin));
                ACC_Test_MAPE += Math.Abs((M_test_output[i, 0] + 1.0) / yFactor + yMin - ((Matrix.Multiply(M_test_input, M_weight)[i, 0] + 1.0) / yFactor + yMin)) / ((M_test_output[i, 0] + 1.0) / yFactor + yMin);
            }
            //테스트 오차 평균
            Error_Test_Mean /= M_test_input.NoRows;
            ACC_Test_MAPE /= M_test_input.NoRows;
            //테스트 정확도
            ACC_Test_MAPE = 100.0 * (1.0 - ACC_Test_MAPE);

            for (int i = 0, k = M_test_input.NoRows; i < k; i++)
            {
                //file2.Write("Error" + ":" + i.ToString() + "th data :" + Error_Test[i].ToString());
                //file2.WriteLine();
            }
            //file2.Write("Mean Error" + ":" + Error_Test_Mean + System.Environment.NewLine);
            //file2.Close();
        }

        /// <summary>
        /// 학습된 모델을 가지고 일 유량을 예측한다.
        /// </summary>
        public void Flow_Prediction()
        {
            Matrix M_prediction_input = new Matrix(1, dimension_sum + 1);

            // 수요 예측 위한 입력 데이터를 확장
            Set_Expand_Data(prediction_data);

            // 확장된 데이터를 정규화
            Data_Normalize(prediction_data_dimension);

            // [1 x1 x1^2 ... x2 x2^2 ... ] 
            M_prediction_input[0, 0] = 1;
            for (int i = 0; i < dimension_sum; i++)
            {
                M_prediction_input[0, i + 1] = normalize_prediction_data_dimension[0, i];
            }

            // y=AX를 통한 수요 예측 값 계산
            Matrix M_result = Matrix.Multiply(M_prediction_input, M_weight);
            Prediction_Result = (M_result[0, 0] + 1.0) / yFactor + yMin;

        }

        /// <summary>
        /// 모델 정보를 지정된 파일로부터 불러온다.
        /// </summary>
        /// <param name="FilePath">모델 정보가 저장된 파일 이름</param>
        public void Load_Model(string FilePath)
        {
            DataTable ModelInfo = new DataTable();
            ModelInfo.ReadXml(FilePath);

            // 입력 변수 개수
            n_input = int.Parse(ModelInfo.Rows[0]["NumInputs"].ToString());

            // 입력 변수별 차수
            for (int i = 1; i <= n_input; i++)
            {
                dimension[i - 1] = int.Parse(ModelInfo.Rows[0]["NumDimension" + i.ToString()].ToString());
            }
            // 입력 변수들의 차수 총합
            dimension_sum = int.Parse(ModelInfo.Rows[0]["SumDimension"].ToString());

            // 상수항 : 1
            M_weight[0, 0] = double.Parse(ModelInfo.Rows[0]["W0"].ToString());
            int idx = 1;
            for (int i = 1; i <= n_input; i++)
            {
                for (int j = 1; j <= dimension[i - 1]; j++)
                {
                    // i번째 입력의 j번째 차수에 해당하는 계수
                    M_weight[idx, 0] = double.Parse(ModelInfo.Rows[0]["W" + i.ToString() + j.ToString()].ToString());
                    idx++;
                }
            }


            for (int i = 0; i < n_input; i++)
            {
                // 입력 변수들의 각 차수에 대한 최소치, 최대치
                xMin[i] = double.Parse(ModelInfo.Rows[0]["MinX" + (i + 1).ToString()].ToString());
                xMax[i] = double.Parse(ModelInfo.Rows[0]["MaxX" + (i + 1).ToString()].ToString());
                xFactor[i] = 2.0 / (xMax[i] - xMin[i]);

            }
            // 출력 변수의 최소치, 최대치
            yMin = double.Parse(ModelInfo.Rows[0]["MinY"].ToString());
            yMax = double.Parse(ModelInfo.Rows[0]["MaxY"].ToString());
            yFactor = 2.0 / (yMax - yMin);




        }
        /// <summary>
        /// 모델 정보를 xml 형태의 파일로 저장한다.
        /// </summary>
        /// <param name="FilePath">모델 정보가 저장될 파일 이름</param>
        public void Save_Model(string FilePath)
        {
            DataRow temp_row;

            DataTable ModelInfo = new DataTable("ModelInfo");

            // 입력 변수 개수
            ModelInfo.Columns.Add("NumInputs", typeof(System.Int32));

            // 입력 변수별 차수
            for (int i = 1; i <= n_input; i++)
            {
                ModelInfo.Columns.Add("NumDimension" + i.ToString(), typeof(System.Int32));
            }
            // 입력 변수들의 차수 총합
            ModelInfo.Columns.Add("SumDimension", typeof(System.Int32));

            // 상수항 : 1
            ModelInfo.Columns.Add("W0", typeof(System.Double));
            for (int i = 1; i <= n_input; i++)
            {
                for (int j = 1; j <= dimension[i - 1]; j++)
                {
                    // i번째 입력의 j번째 차수에 해당하는 계수
                    ModelInfo.Columns.Add("W" + i.ToString() + j.ToString(), typeof(System.Double));

                }
            }

            for (int i = 0; i < n_input; i++)
            {
                // 입력 변수들의 각 차수에 대한 최소치, 최대치
                ModelInfo.Columns.Add("MinX" + (i + 1).ToString(), typeof(System.Double));
                ModelInfo.Columns.Add("MaxX" + (i + 1).ToString(), typeof(System.Double));
            }
            // 출력 변수의 최소치, 최대치
            ModelInfo.Columns.Add("MinY", typeof(System.Double));
            ModelInfo.Columns.Add("MaxY", typeof(System.Double));

            temp_row = ModelInfo.NewRow();
            temp_row["NumInputs"] = n_input;
            for (int i = 1; i <= n_input; i++)
            {
                temp_row["NumDimension" + i.ToString()] = dimension[i - 1];
            }

            temp_row["SumDimension"] = dimension_sum;

            temp_row["W0"] = M_weight[0, 0];
            int idx = 1;
            for (int i = 1; i <= n_input; i++)
            {
                for (int j = 1; j <= dimension[i - 1]; j++)
                {
                    temp_row["W" + (i).ToString() + j.ToString()] = M_weight[idx, 0];
                    idx++;
                }

            }


            for (int i = 0; i < n_input; i++)
            {
                temp_row["MinX" + (i + 1).ToString()] = xMin[i];
                temp_row["MaxX" + (i + 1).ToString()] = xMin[i] + 2.0 / xFactor[i];

            }
            temp_row["MinY"] = yMin;
            temp_row["MaxY"] = yMin + 2.0 / yFactor;

            ModelInfo.Rows.Add(temp_row);

            ModelInfo.WriteXml(FilePath, XmlWriteMode.WriteSchema);



        }

    }


}
