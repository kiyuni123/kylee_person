﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.DF_Algorithm.NN_Core
{
    /// <summary>
    /// Back propagation 학습 알고리즘
    /// </summary>
    /// 
    /// <remarks>이 클래스는 back propagation 학습 알고리즘을 구현한다.
    /// 다중 레이어 neural network를 학습하는데 많이 쓰인다.</remarks>
    /// 
    public class BackPropagationLearning : ISupervisedLearning
    {
        /// <summary>
        /// 학습시킬 네트워크 
        /// </summary>
        public ActivationNetwork network;

        /// <summary>
        /// 학습 계수
        /// </summary>
        public double f_learningRate = 0.1;

        /// <summary>
        /// 모멘텀 계수 
        /// </summary>
        public double f_momentum = 0.0;

        /// <summary>
        /// 뉴런 에러 
        /// </summary>
        public double[][] f_neuronErrors = null;

        /// <summary>
        /// 뉴런 계수 업데이트 
        /// </summary>
        public double[][][] f_weightsUpdates = null;

        /// <summary>
        /// 역치 업데이트
        /// </summary>
        public double[][] f_thresholdUpdates = null;

        /// <summary>
        /// <see cref="BackPropagationLearning"/> 클래스의 새 객체 초기화
        /// </summary>
        /// 
        /// <param name="network">학습시킬 네트워크</param>
        /// 
        public BackPropagationLearning(ActivationNetwork network)
        {
            this.network = network;

            // 각종 배열 작성
            f_neuronErrors = new double[network.n_Layers][];
            f_weightsUpdates = new double[network.n_Layers][][];
            f_thresholdUpdates = new double[network.n_Layers][];

            // 오류 초기화
            for (int i = 0, n = network.n_Layers; i < n; i++)
            {
                Layer layer = network.layers[i];

                f_neuronErrors[i] = new double[layer.n_Neurons];
                f_weightsUpdates[i] = new double[layer.n_Neurons][];
                f_thresholdUpdates[i] = new double[layer.n_Neurons];

                // 각 뉴런에 대하여
                for (int j = 0; j < layer.n_Neurons; j++)
                {
                    f_weightsUpdates[i][j] = new double[layer.n_Inputs];
                }

            }
        }

        /// <summary>
        /// 학습을 시행한다.
        /// </summary>
        /// 
        /// <param name="input">입력 벡터</param>
        /// <param name="output">원하는 출력 벡터</param>
        /// 
        /// <returns>학습 에러 반환</returns>
        /// 
        ///<remarks> 한 번 학습 시행하고 뉴런의 계수를 업데이트</remarks>
        public double Run(double[] input, double[] output)
        {
            // 네트워크 출력 계산
            network.Compute(input);

            // 네트워크 에러 계산
            double error = CalculateError(output);

            // 뉴런 계수 업데이트
            CalculateUpdates(input);

            // 네트워크 업데이트
            UpdateNetwork();

            return error;
        }

        /// <summary>
        /// 학습 여러 epoch 동안 시행한다.
        /// </summary>
        /// 
        /// <param name="input">입력 벡터의 배열array of input vectors</param>
        /// <param name="output">출력 벡터의 배열</param>
        /// 
        /// <returns>학습 에러의 합을 반환</returns>
        /// 
        ///<remarks> 여러번 학습을 시행하고 뉴런의 계수를 업데이트</remarks>
        public double RunEpoch(double[][] input, double[][] output)
        {
            double error = 0.0;

            // 모든 샘플에 대해 학습 시행
            for (int i = 0, n = input.Length; i < n; i++)
            {
                error += Run(input[i], output[i]);
            }

            return error;
        }

        /// <summary>
        /// 네트워크의 모든 뉴런의 에러 값 계산
        /// </summary>
        /// 
        /// <param name="desiredOutput">원하는 출력 벡터</param>
        /// 
        /// <returns>학습 에러 반환</returns>
        /// 
        public double CalculateError(double[] desiredOutput)
        {
            // 현재와 다음 레이어
            ActivationLayer layer, layerNext;

            // 현재와 다음 에러 배열
            double[] f_errors, f_errorsNext;

            // 에러 값
            double f_error = 0.0, e, sum;

            // 뉴런의 출력 값
            double f_output;

            // 레이어 수
            int n_Layers = network.n_Layers;

            //    IActivationFunction function = network.layers[0].neurons[0].function;
            // 네트워크의 모든 뉴런들은 같은 활성화 함수 형태이다.
            IActivationFunction function = network[0][0].function;

            // 마지막 레이어의 에러 값을 먼저 계산
            layer = network[n_Layers - 1];
            f_errors = f_neuronErrors[n_Layers - 1];

            for (int i = 0, n = layer.n_Neurons; i < n; i++)
            {
                f_output = layer[i].f_output;
                // 뉴런의 에러
                e = desiredOutput[i] - f_output;
                // 활성 함수와 곱해진 에러
                f_errors[i] = e * function.Derivative2(f_output);
                // 제곱하고 더함
                f_error += (e * e);
            }

            // 다른 에러의 레이어 에러 계산
            for (int j = n_Layers - 2; j >= 0; j--)
            {
                layer = network[j];
                layerNext = network[j + 1];
                f_errors = f_neuronErrors[j];
                f_errorsNext = f_neuronErrors[j + 1];

                // 레이어의 모든 뉴런에 대해
                for (int i = 0, n = layer.n_Neurons; i < n; i++)
                {
                    sum = 0.0;
                    // 다음 레이어의 모든 뉴런에 대해
                    for (int k = 0, m = layerNext.n_Neurons; k < m; k++)
                    {
                        sum += f_errorsNext[k] * layerNext[k][i];
                    }
                    f_errors[i] = sum * function.Derivative2(layer[i].f_output);

                }
            }
            return f_error / 2.0;
        }

        /// <summary>
        /// 뉴런 계수 업데이트
        /// </summary>
        /// 
        /// <param name="input">네트워크의 입력 벡터</param>
        /// 
        public void CalculateUpdates(double[] input)
        {
            // 현재 뉴런
            ActivationNeuron neuron;

            // 현재와 과거 레이어들
            ActivationLayer layer, layerPrev;

            // 레이어의 계수 업데이트
            double[][] f_layerWeightsUpdates;
            // 레이어의 역치 업데이트            
            double[] f_layerThresholdUpdates;
            // 레이어의 에러
            double[] f_errors;
            // 뉴런 계수 업데이트
            double[] f_neuronWeightUpdates;
            // 에러 값
            double f_error;

            // 마지막 레이어 업데이트
            layer = network[0];
            f_errors = f_neuronErrors[0];
            f_layerWeightsUpdates = f_weightsUpdates[0];
            f_layerThresholdUpdates = f_thresholdUpdates[0];

            // 레이어의 각 뉴런에 대해
            for (int i = 0, n = layer.n_Neurons; i < n; i++)
            {
                neuron = layer[i];
                f_error = f_errors[i];
                f_neuronWeightUpdates = f_layerWeightsUpdates[i];

                // 뉴런의 각 계수에 대해
                for (int j = 0, m = neuron.n_Inputs; j < m; j++)
                {
                    // 계수 업데이트
                    f_neuronWeightUpdates[j] = f_learningRate * (f_momentum * f_neuronWeightUpdates[j] +
                        (1.0 - f_momentum) * f_error * input[j]);

                }

                // 역치 업데이트
                f_layerThresholdUpdates[i] = f_learningRate * (f_momentum * f_layerThresholdUpdates[i] +
                    (1.0 - f_momentum) * f_error);
            }

            // 다른 레이어들에 대해
            for (int k = 1, l = network.n_Layers; k < l; k++)
            {
                layerPrev = network[k - 1];
                layer = network[k];
                f_errors = f_neuronErrors[k];
                f_layerWeightsUpdates = f_weightsUpdates[k];
                f_layerThresholdUpdates = f_thresholdUpdates[k];

                // 레이어의 각 뉴런에 대해
                for (int i = 0, n = layer.n_Neurons; i < n; i++)
                {
                    neuron = layer[i];
                    f_error = f_errors[i];
                    f_neuronWeightUpdates = f_layerWeightsUpdates[i];

                    // 뉴런의 각 시냅스에 대해
                    for (int j = 0, m = neuron.n_Inputs; j < m; j++)
                    {
                        // 뉴런 계수 업데이트
                        f_neuronWeightUpdates[j] = f_learningRate * (f_momentum * f_neuronWeightUpdates[j] +
                            (1.0 - f_momentum) * f_error * layerPrev.neurons[j].f_output);
                    }

                    // 역치 업데이트
                    f_layerThresholdUpdates[i] = f_learningRate * (f_momentum * f_layerThresholdUpdates[i] +
                        (1.0 - f_momentum) * f_error);

                }
            }
        }

        /// <summary>
        /// 네트워크 계수 업데이트
        /// </summary>
        /// 
        public void UpdateNetwork()
        {
            // 현재 뉴런
            ActivationNeuron neuron;
            // 현재 레이어
            ActivationLayer layer;
            // 레이어 계수 업데이트
            double[][] f_layerWeightsUpdates;
            // 레이어 역치 업데이트
            double[] f_layerThresholdUpdates;
            // 뉴런의 계수 업데이트
            double[] f_neuronWeightUpdates;

            // 네트워크의 각 레이어에 대해
            for (int i = 0, n = network.n_Layers; i < n; i++)
            {
                layer = network[i];
                f_layerWeightsUpdates = f_weightsUpdates[i];
                f_layerThresholdUpdates = f_thresholdUpdates[i];

                // 레이어의 각 뉴런에 대해
                for (int j = 0, m = layer.n_Neurons; j < m; j++)
                {
                    neuron = layer[j];
                    f_neuronWeightUpdates = f_layerWeightsUpdates[j];

                    // 뉴런의 각 계수에 대해
                    for (int k = 0, s = neuron.n_Inputs; k < s; k++)
                    {
                        // 계수 업데이트
                        neuron.f_weights[k] += f_neuronWeightUpdates[k];

                    }

                    //역치 업데이트
                    neuron.f_threshold += f_layerThresholdUpdates[j];
                }
            }
        }
    }
}
