﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.DF_Algorithm.NN_Core
{
    /// <summary>
    /// 기본 신경 회로망 네트워크
    /// </summary>
    /// 
    /// <remarks>기본 네트워크는 뉴런의 레이어의 집합체이다.
    /// <see cref="Layer">레이어</see>.</remarks>
    ///

    public class Network
    {
        /// <summary>
        /// 네트워크의 입력 수
        /// </summary>
        public int n_Inputs;

        /// <summary>
        /// 네트워크의 레이어 수
        /// </summary>        
        public int n_Layers;

        /// <summary>
        /// 네트워크의 레이어
        /// </summary>            
        public Layer[] layers;

        /// <summary>
        /// 네트워크의 출력 벡터
        /// </summary>    
        public double[] f_Output;

        /// <summary>
        /// <see cref="Network"/> 클래스의 객체 초기화
        /// </summary>
        /// 
        /// <param name="inputsCount">네트워크의 입력 수</param>
        /// <param name="layersCount">네트워크 레이어 수</param>
        /// 
        /// 
        public Network(int n_Inputs, int n_Layers)
        {
            this.n_Inputs = Math.Max(1, n_Inputs);
            this.n_Layers = Math.Max(1, n_Layers);

            layers = new Layer[this.n_Layers];
        }

        /// <summary>
        /// 네트워크의 출력 벡터 계산
        /// </summary>
        /// 
        /// <param name="input">입력 벡터</param>
        /// 
        /// <returns>네트워크의 출력 벡터 반환</returns>
        /// 
        /// <remarks>네트워크의 실질 출력 벡터는 네트워크의 마지막 레이어의 출력 벡터이다.</remarks>
        /// 
        public virtual double[] Compute(double[] input)
        {
            f_Output = input;

            foreach (Layer layer in layers)
            {
                f_Output = layer.Compute(f_Output);
            }

            return f_Output;
        }

        /// <summary>
        /// 네트워크의 레이어 랜덤화하기
        /// </summary>
        /// 
        /// <remarks>각 레이어의 <see cref="Layer.Randomize"/> 를 이용해 네트워크의 레이어 랜덤화하기</remarks>
        /// 
        public virtual void Randomize()
        {
            foreach (Layer layer in layers)
            {
                layer.Randomize();
            }

        }
    }
}
