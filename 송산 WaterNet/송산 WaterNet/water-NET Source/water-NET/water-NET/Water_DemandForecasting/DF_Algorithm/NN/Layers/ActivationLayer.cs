﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.DF_Algorithm.NN_Core
{
    /// <summary>
    /// 활성 레이어
    /// </summary>
    /// 
    /// <remarks>활성 레이어는 <see cref="ActivationNeuron"/>을 담고 있다.
    /// 이 레이어는 다중 레이어 신경망에서 쓰인다.</remarks>
    ///
    public class ActivationLayer : Layer
    {
        /// <summary>
        /// 레이어의 뉴런들 접근자
        /// </summary>
        /// 
        /// <param name="index">뉴런 인덱스</param>
        /// 
        /// <remarks>레이어의 뉴런들에 접근 가능케한다.</remarks>
        /// 

        //HSj// public new ActivationNeuron this[int index]
        public  ActivationNeuron this[int index]
        {
            get { return (ActivationNeuron)neurons[index]; }
        }
        // public ActivationNeuron[] neurons;

        /// <summary>
        /// <see cref="ActivationLayer"/> 클래스의 객체를 초기화한다.
        /// </summary>
        /// <param name="neuronsCount">레이어의 뉴런 수</param>
        /// <param name="inputsCount">레이어의 입력 수</param>
        /// <param name="function">레이어의 뉴런들의 활성 함수</param>
        /// 
        /// <remarks>새 레이어는 만들어진 다음 랜덤화됩니다. (see <see cref="ActivationNeuron.Randomize"/>
        /// method) </remarks>
        /// 
        public ActivationLayer(int n_Neurons, int n_Inputs, IActivationFunction function)
            : base(n_Neurons, n_Inputs)
        {
            for (int i = 0; i < n_Neurons; i++)
                neurons[i] = new ActivationNeuron(n_Inputs, function);


        }
    }
}
