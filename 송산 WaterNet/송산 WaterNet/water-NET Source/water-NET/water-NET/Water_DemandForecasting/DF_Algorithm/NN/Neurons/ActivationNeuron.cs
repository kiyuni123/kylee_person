﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.DF_Algorithm.NN_Core
{

    /// <summary>
    /// 활성 뉴런
    /// </summary>
    /// 
    /// <remarks> 활성 뉴런은 입력의 weighted sum에 역치 값을 더하고 
    /// 활성 함수를 가함으로써 계산된다. 다중 레이어 신경 회로망서 많이 쓰인다.
    /// </remarks>
    /// 
    public class ActivationNeuron : Neuron
    {
        /// <summary>
        /// 역치 값
        /// </summary>
        /// 
        /// <remarks> 입력의 weighted sum에 더해진다.</remarks>
        /// 
        public double f_threshold = 0.0f;

        /// <summary>
        /// 활성 함수
        /// </summary>
        /// 
        /// <remarks> 입력의 weighted sum에 역치 값을 더하고 그 값에 적용된다.
        /// </remarks>
        /// 
        public IActivationFunction function = null;

        /// <summary>
        ///  <see cref="ActivationNeuron"/> 클래스의 새 객체를 초기화한다.
        /// </summary>
        /// 
        /// <param name="inputs">뉴런의 입력 수</param>
        /// <param name="function">뉴런의 활성 함수</param>
        /// 
        public ActivationNeuron(int inputs, IActivationFunction function)
            : base(inputs)
        {
            this.function = function;
        }

        /// <summary>
        /// 뉴런 랜덤화하기 
        /// </summary>
        /// 
        /// <remarks><see cref="Neuron.Randomize">Randomize</see> 메서드를 호출하여 뉴런의 계수와 역치 값을 랜덤화한다.</remarks>
        /// 
        public override void Randomize()
        {
            base.Randomize();

            f_threshold = rand.NextDouble() * (f_maxRange - f_minRange) + f_minRange;
        }

        /// <summary>
        /// 뉴런의 출력 값 계산
        /// </summary>
        /// 
        /// <param name="input">입력 벡터</param>
        /// 
        /// <returns> 뉴런의 출력 값 반환</returns>
        /// 
        /// <remarks> 활성 뉴런의 출력 값은 뉴런의 활성 함수 값과 같고, 이 활성 함수 값은
        /// 입력의 weighted sum과 역치 값을 더한 값이다.
        ///</remarks>
        /// 
        public override double Compute(double[] input)
        {
            // 입력 벡터가 올바른지 체크
            if (input.Length != n_Inputs)
                throw new ArgumentException();

            // 초기 합
            double sum = 0.0;

            // 입력의 weighted sum 계산
            for (int i = 0; i < n_Inputs; i++)
            {
                sum += f_weights[i] * input[i];
            }
            sum += f_threshold;

            return (f_output = function.Function(sum));

        }
    }
}
