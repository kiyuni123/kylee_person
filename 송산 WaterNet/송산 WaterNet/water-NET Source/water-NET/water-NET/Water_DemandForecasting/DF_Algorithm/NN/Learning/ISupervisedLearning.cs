﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.DF_Algorithm.NN_Core
{
    /// <summary>
    /// 
    /// Supervised 학습 인터페이스
    /// </summary>
    /// 
    /// 
    public interface ISupervisedLearning
    {
        /// <summary>
        /// 학습을 시행한다.
        /// </summary>
        /// 
        /// <param name="input">입력 벡터</param>
        /// <param name="output">원하는 출력 벡터</param>
        /// 
        /// <returns>학습 에러 반환</returns>
        /// 
        double Run(double[] input, double[] output);

        /// <summary>
        /// 학습 여러 epoch 동안 시행한다.
        /// </summary>
        /// 
        /// <param name="input">입력 벡터의 배열array of input vectors</param>
        /// <param name="output">출력 벡터의 배열</param>
        /// 
        /// <returns>학습 에러의 합을 반환</returns>
        /// 
        double RunEpoch(double[][] input, double[][] output);
    }
}