﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.DF_Algorithm.NN_Core
{
    public interface IActivationFunction
    {
        /// <summary>
        /// 함수값 계산
        /// </summary>
        /// <param name="x">함수 입력 값</param>
        /// <returns>함수 출력 값</returns>
        /// <remarks>이 메서드는 특정 점에서 함수 값을 계산합니다.</remarks>
        double Function(double x);

        /// <summary>
        /// 함수의 기울기 계산
        /// </summary>
        /// <param name="x">함수 입력 값</param>
        /// <returns>함수 기울기</returns>
        /// <remarks> 함수는 특정 점에서 함수 기울기를 계산합니다.</remarks>
        double Derivative(double x);

        /// <summary>
        /// 함수의 기울기 계산
        /// </summary>
        /// <param name="y">함수 출력 값</param>
        /// <returns>함수 기울기, <i>f'(x)</i></returns>
        double Derivative2(double y);
    }
}