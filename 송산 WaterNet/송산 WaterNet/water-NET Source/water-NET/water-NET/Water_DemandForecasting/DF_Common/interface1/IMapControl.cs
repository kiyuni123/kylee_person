﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Controls;
using System.Windows.Forms;

namespace WaterNet.DF_Common.interface1
{
    public interface IMapControl
    {
        void MoveFocusAt(string layerName, string whereCase);
        IMapControl3 IMapControl3
        {
            get;
        }
        ToolStripButton ToolActionCommand
        {
            get;
        }
        AxMapControl AxMap
        {
            get;
        }
    }
}
