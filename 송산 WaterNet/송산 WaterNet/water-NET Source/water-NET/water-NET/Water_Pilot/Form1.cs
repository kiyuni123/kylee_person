﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterAOCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;

namespace Water_Pilot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IWorkspace pWorkspace = ArcManager.getShapeWorkspace("dir");
            if (pWorkspace == null) return;
            ILayer pLayer = ArcManager.GetShapeLayer(pWorkspace, "filename");
            if (pLayer == null) return;
            if (!(pLayer is IFeatureClass)) return;

            IFeatureCursor pFeautreCursor = ArcManager.GetCursor(pLayer, "AAA = 'aaa'") as IFeatureCursor;
            if (pFeautreCursor == null) return;

            IFeature pFeature = pFeautreCursor.NextFeature();
            while (pFeature != null)
            {
                MessageBox.Show(Convert.ToString(ArcManager.GetValue(pFeature, "AAA")));
                pFeature = pFeautreCursor.NextFeature();
            }

        }
    }
}
