﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Win;

namespace WaterNet.WQ_Analysis.util
{
    //Grid가 Multiline일 경우 Text를 가운데정렬하기위한 Filter
    public class GridMiddleAlignDrawFilter : IUIElementDrawFilter
    {
        bool IUIElementDrawFilter.DrawElement(DrawPhase drawPhase, ref UIElementDrawParams drawParams)
        {
            drawParams.AppearanceData.TextVAlign = VAlign.Middle;

            return false;
        }

        DrawPhase IUIElementDrawFilter.GetPhasesToFilter(ref UIElementDrawParams drawParams)
        {
            if (drawParams.Element is EditorWithTextDisplayTextUIElement)
                return DrawPhase.BeforeDrawForeground;

            return DrawPhase.None;
        }
    }
}
