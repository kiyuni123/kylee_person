﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;

namespace WaterNet.WQ_Analysis.util
{
    public class CommonUtils
    {
        static Random pRandom = new Random();

        //combo box의 데이터에 전체 항목을 추가
        public static void AddAllRow(DataTable table)
        {
            DataRow row = table.NewRow();
            row["CODE"] = "";
            row["TEXT"] = "전체";

            table.Rows.InsertAt(row, 0);
        }

        //숫자인지 확인
        public static bool IsNumber(string value)
        {
            value = value.Replace("-", "");
            string check = value.Trim();

            bool returnVal = false;
            int dotCount = 0;
            for (int i = 0; i < check.Length; i++)
            {
                if (System.Char.IsNumber(check, i))
                    returnVal = true;
                else
                {
                    if (check.Substring(i, 1) == ".")
                    {
                        returnVal = true;
                        dotCount++;
                    }
                    else
                    {
                        returnVal = false;
                        break;
                    }
                }
            }

            if (dotCount > 1)
            {
                returnVal = false;
            }

            return returnVal;
        }
    
        //Hashtable의 내용을 Console에 출력
        public static void WriteHashtableToConsole(Hashtable data)
        {
            Console.WriteLine("===========write start===========");
            
            foreach(string key in data.Keys)
            {
                Console.WriteLine(key + " : " + data[key]);
            }

            Console.WriteLine("===========write end===========");
        }
    
        //grid에 행추가
        public static void AddGridRow(UltraGrid grid)
        {
            DataTable dataTable = grid.DataSource as DataTable;
            dataTable.Rows.Add(dataTable.NewRow());

            //grid.Rows[dataTable.Rows.Count - 1].Selected = true;
            //grid.Rows[dataTable.Rows.Count - 1].Activate();

            grid.Rows[grid.Rows.Count - 1].Selected = true;
            grid.Rows[grid.Rows.Count - 1].Activate();
        }

        //grid에 행추가 (초기값 존재)
        public static void AddGridRow(UltraGrid grid, Hashtable initValueSet)
        {
            DataTable dataTable = grid.DataSource as DataTable;
            DataRow newRow = dataTable.NewRow();

            foreach(string key in initValueSet.Keys)
            {
                newRow[key] = initValueSet[key].ToString();
            }

            dataTable.Rows.Add(newRow);

            grid.Rows[grid.Rows.Count - 1].Selected = true;
            grid.Rows[grid.Rows.Count - 1].Activate();
        }

        //grid에 행삭제
        public static Hashtable DeleteGridRow(UltraGrid grid, string message)
        {
            DataTable dt = grid.DataSource as DataTable;
            Hashtable deleteData = null;

            if (grid.Selected.Rows.Count != 0)
            {

                if(!"".Equals(message))
                {
                    if (MessageBox.Show(message, "데이터삭제", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    {
                        return null;
                    }
                }

                int idx = grid.Selected.Rows[0].Index;
                DataRow realDataRow = ((DataRowView)grid.Rows[idx].ListObject).Row;

                if (realDataRow.RowState != DataRowState.Added)
                {

                    deleteData = new Hashtable();

                    foreach (DataColumn column in dt.Columns)
                    {
                        deleteData.Add(column.ToString(), ((DataRowView)grid.Rows[idx].ListObject).Row[column.ToString()].ToString());
                    }
                }

                dt.Rows.RemoveAt(dt.Rows.IndexOf(realDataRow));

                if (dt.Rows.Count > 0)
                {
                    int newIdx = 0;

                    if (idx - 1 < 0)
                    {
                        newIdx = 0;
                    }
                    else
                    {
                        newIdx = idx - 1;
                    }

                    grid.Rows[newIdx].Selected = true;
                    grid.Rows[newIdx].Activate();
                }
            }

            return deleteData;
        }

        //grid data 저장 전 확인
        public static bool SaveGridData(UltraGrid grid, Hashtable validationSet)
        {
            if (grid.Rows.Count == 0)
            {
                return false;
            }

            bool result = false;

            if (!HaveChangedData(grid.DataSource as DataTable))
            {
                MessageBox.Show("변경된 데이터가 없습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //validation
                if (validationSet != null)
                {
                    if (!ValidationGridData(grid, validationSet))
                    {
                        return false;
                    }
                }

                if (MessageBox.Show("저장하시겠습니까?", "데이터저장", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    result = true;
                }
            }

            return result;
        }

        //grid data validation
        public static bool ValidationGridData(UltraGrid grid, Hashtable validationSet)
        {
            DataTable tmpDt = grid.DataSource as DataTable;

            foreach (DataRow row in tmpDt.Rows)
            {
                if (row.RowState == DataRowState.Unchanged)
                {
                    continue;
                }

                foreach (DataColumn column in tmpDt.Columns)
                {
                    if (validationSet[column.ToString()] != null)
                    {
                        string headerCaption = grid.DisplayLayout.Bands[0].Columns[column.ColumnName].Header.Caption;
                        string[] validationData = validationSet[column.ToString()].ToString().Split('|');

                        //사이즈 check
                        if (int.Parse(validationData[0]) < System.Text.Encoding.GetEncoding(949).GetByteCount((row[column.ToString()].ToString())))
                        {
                            MessageBox.Show("입력값이 너무 큽니다. \n" + headerCaption + " : " + row[column], "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }

                        //Null 허용 Check
                        if ("N".Equals(validationData[1].ToUpper()) && "".Equals(row[column].ToString()))
                        {
                            MessageBox.Show("[" + headerCaption + "] 항목에 빈값이 올 수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }

                        //숫자여부 확인
                        if ("Y".Equals(validationData[2].ToUpper()) && !IsNumber(row[column].ToString()))
                        {
                            MessageBox.Show("[" + headerCaption + "] 항목은 숫자만 입력가능합니다. \n" + headerCaption + " : " + row[column], "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        //저장 전 변경데이터 확인
        public static bool HaveChangedData(DataTable data)
        {
            bool result = false;

            foreach (DataRow row in data.Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                {
                    result = true;
                }
            }

            return result;
        }

        //그리드와 컨트롤의 값을 비교하여 값이 다를경우 컨트롤 텍스트 체인지 이벤트를 허용한다. : 2014-12-15_shpark
        public static void ControlTextChanged(UltraGrid grid, string key, string value)
        {
            if (grid.Selected.Rows.Count > 0)
            {
                if (!grid.Selected.Rows[0].Cells[key].Value.ToString().Equals(value))
                {
                    SetGridCellValue(grid, key, value);
                }
            }
        }

        //grid에서 선택된 행/컬럼의 값을 발췌
        public static string GetGridCellValue(UltraGrid grid, string columnName)
        {
            string result = "";

            if(grid.Selected.Rows.Count > 0)
            {
                result = grid.Selected.Rows[0].Cells[columnName].Value.ToString();
            }

            return result;
        }
    
        //해당 값을 grid의 값으로 할당
        public static void SetGridCellValue(UltraGrid grid, string columnName, string value)
        {
            if (grid.Selected.Rows.Count > 0)
            {
                grid.Selected.Rows[0].Cells[columnName].Value = value;
                grid.UpdateData();
            }
        }

        //grid의 선택 행 상태 반환
        public static DataRowState GetRowState(UltraGrid grid)
        {
            DataTable table = grid.DataSource as DataTable;
            return table.Rows[grid.Selected.Rows[0].Index].RowState;
        }

        //grid의 선택된 로우의 위치를 위 또는 아래로 한칸씩 이동    : 2015-02-04_shpark
        public static void MoveRowUpDown(UltraGrid grid, string upDown)
        {
            if (grid.Selected.Rows.Count == 0) return;

            UltraGridRow row = grid.ActiveRow;
            int rowIndex = grid.Selected.Rows[0].Index;

            if (upDown.Equals("UP"))
            {
                rowIndex = rowIndex == 0 ? 0 : rowIndex - 1;
            }
            else if (upDown.Equals("DOWN"))
            {
                rowIndex = rowIndex == grid.Rows.Count - 1 ? rowIndex : rowIndex + 1;
            }

            grid.Rows.Move(row, rowIndex);

            grid.Rows[rowIndex].Activate();
            grid.Rows[rowIndex].Selected = true;
        }

        //날짜 반환
        public static string StringToDateTime(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;
        }
    
        //Exception 처리
        public static void ShowExceptionMessage(string message)
        {
            MessageBox.Show(message, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Exception 처리 (Console 출력 포함)
        public static void ShowExceptionMessageWithConsole(Exception e)
        {
            Console.WriteLine(e);
            MessageBox.Show(e.Message, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //메세지 처리
        public static void ShowInformationMessage(string message)
        {
            MessageBox.Show(message, "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Yes/No Dialog Box 처리
        public static DialogResult ShowYesNoDialog(string message)
        {
            return MessageBox.Show(message, "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        //수질항목에 맞는 단위 반환
        public static string GetQualityUnit(string qualityCode)
        {
            string result = "";

            if(qualityCode.ToUpper().IndexOf("CL") != -1)
            {
                result = "mg/ℓ";
            }
            else if (qualityCode.ToUpper().IndexOf("CU") != -1)
            {
                result = "㎲/cm";
            }
            else if (qualityCode.ToUpper().IndexOf("PH") != -1)
            {
                result = "pH";
            }
            else if (qualityCode.ToUpper().IndexOf("TB") != -1)
            {
                result = "NTU";
            }
            else if (qualityCode.ToUpper().IndexOf("TE") != -1)
            {
                result = "℃";
            }

            return result;
        }
    
        //조회 후 grid 첫째행 선택
        public static void SelectFirstGridRow(UltraGrid grid)
        {
            if(grid.Rows.Count > 0)
            {
                grid.Rows[0].Selected = true;
                grid.Rows[0].Activate();
            }
        }

        //grid 특정행을 선택
        public static void SelectGridRow(UltraGrid grid, int idx)
        {
            if (grid.Rows.Count > 0)
            {
                grid.Rows[idx].Selected = true;
                grid.Rows[idx].Activate();
            }
        }

        //null을 공백문자로
        public static object NullToString(object obj)
        {
            if (obj == null)
            {
                return "";
            }
            else
            {
                return obj;
            }
        }

        //일련번호 생성
        public static String GetSerialNumber(string workFlag)
        {
            //static Random pRandom = new Random();

            string strRtn = string.Empty;
            string strWQSN = string.Empty;

            strWQSN = StringToDateTime(DateTime.Now)
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0');
            strWQSN += (pRandom.Next(1, 9).ToString()).PadLeft(1, '0');

            strRtn = workFlag + strWQSN;

            return strRtn;
        }
    }
}
