﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WQ_Analysis.dao
{
    public class PipeCleaningDao
    {
        private static PipeCleaningDao dao = null;

        private PipeCleaningDao()
        {
        }

        public static PipeCleaningDao GetInstance()
        {
            if(dao == null)
            {
                dao = new PipeCleaningDao();
            }

            return dao;
        }

        //관로세척정보 조회
        public DataTable SelectPipeCleaningDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select		PCLEAN_NO						                                                ");
            queryString.AppendLine("		    ,PCLEAN_TITLE						                                            ");
            queryString.AppendLine("		    ,to_char(REG_DAT, 'yyyy-mm-dd')		as REG_DAT		                            ");
            queryString.AppendLine("		    ,PROC_METHOD						                                            ");
            queryString.AppendLine("		    ,PROC_METHOD_TITLE						                                        ");
            queryString.AppendLine("		    ,PROC_RESULT						                                            ");
            queryString.AppendLine("		    ,PROC_RESULT_TITLE						                                        ");
            queryString.AppendLine("		    ,to_char(PROC_DAT, 'yyyy-mm-dd')	as PROC_DAT		                            "); 
            queryString.AppendLine("		    ,REMARK						                                                    ");
            queryString.AppendLine("from		WQ_PCLEAN						                                                ");
            queryString.AppendLine("where		1 = 1						                                                    ");
            
            if(!((bool)conditions["selectAll"]))
            {
                queryString.AppendLine("and         PROC_DAT    between to_date('" + conditions["startDate"] + "','yyyymmdd')       ");
                queryString.AppendLine("                        and     to_date('" + conditions["endDate"] + "','yyyymmdd')         ");

                if (conditions["PCLEAN_TITLE"] != null)
                {
                    queryString.AppendLine("and     PCLEAN_TITLE    like '%" + conditions["PCLEAN_TITLE"] + "%'                     ");
                }
            }

            queryString.AppendLine("order by    REG_DAT                                                                         ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //관로세척정보 입력
        public void InsertPipeCleaningData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WQ_PCLEAN	(							");
            queryString.AppendLine("				PCLEAN_NO						    ");
            queryString.AppendLine("				,PCLEAN_TITLE						");
            queryString.AppendLine("				,REG_DAT						    ");
            queryString.AppendLine("				,PROC_METHOD_TITLE					");
            queryString.AppendLine("				,PROC_METHOD						");
            queryString.AppendLine("				,PROC_RESULT_TITLE					");
            queryString.AppendLine("				,PROC_RESULT						");
            queryString.AppendLine("				,PROC_DAT						    ");
            queryString.AppendLine("				,REMARK						        ");
            queryString.AppendLine("			) values (						        ");
            queryString.AppendLine("				:1						            ");
            queryString.AppendLine("				,:2						            ");
            queryString.AppendLine("				,to_char(sysdate,'yyyymmdd')		");
            queryString.AppendLine("				,:3						            ");
            queryString.AppendLine("				,:4						            ");
            queryString.AppendLine("				,:5						            ");
            queryString.AppendLine("				,:6						            ");
            queryString.AppendLine("				,:7						            ");
            queryString.AppendLine("				,:8						            ");
            queryString.AppendLine("			)						                ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
                 ,new OracleParameter("7", OracleDbType.Varchar2)
                 ,new OracleParameter("8", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];
            parameters[1].Value = (string)conditions["PCLEAN_TITLE"];
            parameters[2].Value = (string)conditions["PROC_METHOD_TITLE"];
            parameters[3].Value = (string)conditions["PROC_METHOD"];
            parameters[4].Value = (string)conditions["PROC_RESULT_TITLE"];
            parameters[5].Value = (string)conditions["PROC_RESULT"];
            parameters[6].Value = (string)conditions["PROC_DAT"];
            parameters[7].Value = (string)conditions["REMARK"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }
    
        //관로세적정보 수정
        public void UpdatePipeCleaningData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update	WQ_PCLEAN					    ");
            queryString.AppendLine("set	    PCLEAN_TITLE	    = :1		");
            queryString.AppendLine("	    ,PROC_METHOD_TITLE	= :2		");
            queryString.AppendLine("	    ,PROC_METHOD	    = :3		");
            queryString.AppendLine("	    ,PROC_RESULT_TITLE	= :4		");
            queryString.AppendLine("	    ,PROC_RESULT	    = :5		");
            queryString.AppendLine("	    ,PROC_DAT		    = :6		");
            queryString.AppendLine("	    ,REMARK			    = :7		");
            queryString.AppendLine("where	PCLEAN_NO		    = :8		");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
                 ,new OracleParameter("7", OracleDbType.Varchar2)
                 ,new OracleParameter("8", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_TITLE"];
            parameters[1].Value = (string)conditions["PROC_METHOD_TITLE"];
            parameters[2].Value = (string)conditions["PROC_METHOD"];
            parameters[3].Value = (string)conditions["PROC_RESULT_TITLE"];
            parameters[4].Value = (string)conditions["PROC_RESULT"];
            parameters[5].Value = (string)conditions["PROC_DAT"];
            parameters[6].Value = (string)conditions["REMARK"];
            parameters[7].Value = (string)conditions["PCLEAN_NO"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //관로세척정보 삭제
        public void DeletePipeCleaningData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WQ_PCLEAN       ");
            queryString.AppendLine("where       PCLEAN_NO = :1  ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //세척정보에 관련된 관로정보 모두 삭제
        public void DeleteAllPipeData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WQ_PCLEAN_PIPE          ");
            queryString.AppendLine("where       PCLEAN_NO       = :1    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //세척대상 관로정보 조회
        public DataTable SelectPipeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          PCLEAN_NO               ");
            queryString.AppendLine("                ,FTR_IDN                ");
            queryString.AppendLine("from            WQ_PCLEAN_PIPE          ");
            queryString.AppendLine("where           PCLEAN_NO       = :1    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //해당 세척정보에 관로정보가 존재하는지 확인
        public DataTable IsExistPipeInCleaningData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          FTR_IDN                     ");
            queryString.AppendLine("from            WQ_PCLEAN_PIPE              ");
            queryString.AppendLine("where           PCLEAN_NO       = :1        ");
            queryString.AppendLine("and             FTR_IDN         = :2        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];
            parameters[1].Value = (string)conditions["FTR_IDN"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //세척대상 관로정보 입력
        public void InsertPipeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WQ_PCLEAN_PIPE  (                   ");
            queryString.AppendLine("                                PCLEAN_NO       ");
            queryString.AppendLine("                                ,FTR_IDN        ");
            queryString.AppendLine("                            ) values (          ");
            queryString.AppendLine("                                :1              ");
            queryString.AppendLine("                                ,:2             ");
            queryString.AppendLine("                            )                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];
            parameters[1].Value = (string)conditions["FTR_IDN"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //세척대상 관로정보 삭제
        public void DeletePipeData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_PCLEAN_PIPE          ");
            queryString.AppendLine("where           PCLEAN_NO       = :1    ");
            queryString.AppendLine("and             FTR_IDN         = :2    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];
            parameters[1].Value = (string)conditions["FTR_IDN"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //세척관로정보 조회
        public DataTable SelectAllCleaningPipeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.FTR_IDN						                    ");
            queryString.AppendLine("            ,nvl(b.TYPE, '0')               as TYPE				");
            queryString.AppendLine("from        (						                            ");
            queryString.AppendLine("              select      distinct FTR_IDN  as FTR_IDN			");
            queryString.AppendLine("                          ,'1'              as TYPE				");
            queryString.AppendLine("              from        WQ_PCLEAN_PIPE						");
            queryString.AppendLine("            )             a						                ");
            queryString.AppendLine("            ,(						                            ");
            queryString.AppendLine("              select      FTR_IDN						        ");
            queryString.AppendLine("                          ,'1'              as TYPE				");
            queryString.AppendLine("              from        WQ_PCLEAN_PIPE						");
            queryString.AppendLine("              where       1 = 1						            ");
            queryString.AppendLine("              and         PCLEAN_NO         = :1				");
            queryString.AppendLine("            )             b						                ");
            queryString.AppendLine("where       a.FTR_IDN     = b.FTR_IDN(+)						");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCLEAN_NO"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
   
        //모델 정보 조회
        public DataTable SelectModelList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          INP_NUMBER                                                                                      ");
            queryString.AppendLine("                ,TITLE                                                                                          "); 
            queryString.AppendLine("                ,to_char(to_date(INS_DATE, 'yyyymmddhh24miss'), 'yyyy-mm-dd hh24:mi')     as CON_INS_DATE       ");
            queryString.AppendLine("from            WH_TITLE                                                                                        ");
            queryString.AppendLine("where           USE_GBN = 'WQ'                                                                                  ");
            queryString.AppendLine("and             TITLE like '%'||:1||'%'                                                                         ");
            queryString.AppendLine("order by        to_date(INS_DATE, 'yyyymmddhh24miss') desc                                                      ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            if(conditions["TITLE"] != null)
            {
                parameters[0].Value = (string)conditions["TITLE"];
            }

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    
        //블록정보 조회
        public DataTable SelectBlockData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      FTR_IDN							    ");
            queryString.AppendLine("            ,LOC_NAME							");
            queryString.AppendLine("from        CM_LOCATION							");
            queryString.AppendLine("where       FTR_CODE    = :1					");
            queryString.AppendLine("and         PLOC_CODE   = :2                    ");
            queryString.AppendLine("order by    LOC_NAME							");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["FTR_CODE"];
            parameters[1].Value = (string)conditions["PLOC_CODE"];
            
            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //수질이상 항목 조회
        public DataTable SelectComplaintType(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      CODE                                                    ");
            queryString.AppendLine("            ,CODE_NAME                                              "); 
            queryString.AppendLine("from        CM_CODE                                                 ");
            queryString.AppendLine("where       PCODE       = '9004'                                    ");
            queryString.AppendLine("and         CODE        in ('2010','2015','2030','2040','2050','2099')     ");
            queryString.AppendLine("order by    CODE_NAME                                               ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //블록별 민원건수 조회
        public DataTable SelectComplaintCountByBlock(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("with TMP1 as        (							                                                                            ");
            queryString.AppendLine("				        select      a.LOC_CODE							                                                    ");
            queryString.AppendLine("					                ,a.PLOC_CODE							                                                ");
            queryString.AppendLine("					                ,a.LOC_NAME							                                                    ");
            queryString.AppendLine("					                ,b.CNT							                                                        ");
            queryString.AppendLine("				        from        CM_LOCATION     a							                                            ");
            queryString.AppendLine("					                ,(							                                                            ");
            queryString.AppendLine("					                    select      SFTRIDN							                                        ");
            queryString.AppendLine("					                                ,count(SFTRIDN)     as CNT					                            ");
            queryString.AppendLine("					                    from        CM_LOCATION         a						                            ");
            queryString.AppendLine("							                        ,WI_DMINFO          b						                            ");
            queryString.AppendLine("							                        ,WI_CAINFO          c						                            ");
            queryString.AppendLine("					                    where       a.FTR_IDN           = b.SFTRIDN    			                            ");
            queryString.AppendLine("					                    and         b.DMNO              = c.DMNO				                            ");
            queryString.AppendLine("                                        and         c.CALRGCD           = '2000'                                            ");

            if ("".Equals(conditions["CAMIDCD"].ToString()))
            {
                queryString.AppendLine("                                    and         c.CAMIDCD in ('2010', '2015', '2030', '2040', '2050', '2099')                   ");
            }
            else
            {
                queryString.AppendLine("                                    and         c.CAMIDCD = '" + conditions["CAMIDCD"] + "'                             ");
            }
            
            
            queryString.AppendLine("                                        and         c.CAAPPLDT          between :1 and :2                                   ");
            queryString.AppendLine("					                    group by    SFTRIDN							                                        ");
            queryString.AppendLine("					                )               b							                                            ");
            queryString.AppendLine("				        where       a.FTR_CODE      is not null  							                                ");
            queryString.AppendLine("				        and         a.FTR_IDN       = b.SFTRIDN(+)							                                ");
            queryString.AppendLine("			        )							                                                                            ");
            queryString.AppendLine("select			    PLOC_CODE							                                                                    ");
            queryString.AppendLine("			        ,LOC_CODE							                                                                    ");
            queryString.AppendLine("			        ,LOC_NAME || ' (' || nvl( 							                                                    "); //LOC_NAME에 민원건수를 같이 표시
            queryString.AppendLine("			                                    (							                                                ");
            queryString.AppendLine("				                                    select      sum(CNT)						                            ");
            queryString.AppendLine("				                                    from        TMP1							                            ");
            queryString.AppendLine("				                                    start with  LOC_CODE        = a.LOC_CODE	                            ");
            queryString.AppendLine("			    	                                connect by  prior LOC_CODE  = PLOC_CODE		                            ");
            queryString.AppendLine("			                                    ), '0') || ')'	as LOC_NAME						                            ");
            queryString.AppendLine("                    ,nvl((                                                                                                  "); //Tree에 건수별로 색을 달리 표시하기 위해 민원 합산을 조회
            queryString.AppendLine("                        select      sum(CNT)										                                        ");
            queryString.AppendLine("                        from        TMP1											                                        ");
            queryString.AppendLine("                        start with  LOC_CODE        = a.LOC_CODE								                            ");
            queryString.AppendLine("                        connect by  prior LOC_CODE  = PLOC_CODE								                                ");
            queryString.AppendLine("                    ), '0')                                     as SUMCNT                                                   ");
            queryString.AppendLine("from                TMP1                a							                                                        ");
            queryString.AppendLine("start with		    PLOC_CODE is        null							                                                    ");
            queryString.AppendLine("connect by prior	LOC_CODE        =   PLOC_CODE							                                                ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["startDate"];
            parameters[1].Value = (string)conditions["endDate"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //선택된 블록의 하위블록 리스트 조회
        public DataTable SelectLowerBlockList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      level							                    ");
            queryString.AppendLine("            ,FTR_IDN							                ");
            queryString.AppendLine("            ,loc_code							                ");
            queryString.AppendLine("from        CM_LOCATION											");
            queryString.AppendLine("start with  LOC_CODE        = :1							    ");
            queryString.AppendLine("connect by  prior LOC_CODE  = PLOC_CODE							");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["LOC_CODE"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //선택 블록(하위 포함)의 민원 현황 조회
        public DataTable SelectComplaintState(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          decode(YM, null, '총계', YM)        as YM							                                    ");
            queryString.AppendLine("                ,sum(TMP_2010)  as COL_2010							                                                    ");
            queryString.AppendLine("                ,sum(TMP_2015)  as COL_2015							                                                    ");
            queryString.AppendLine("                ,sum(TMP_2030)  as COL_2030							                                                    ");
            queryString.AppendLine("                ,sum(TMP_2040)  as COL_2040							                                                    ");
            queryString.AppendLine("                ,sum(TMP_2050)  as COL_2050							                                                    ");
            queryString.AppendLine("                ,sum(TMP_2099)  as COL_2099							                                                    ");
            queryString.AppendLine("                ,sum(TMP_2010) + sum(TMP_2015) + sum(TMP_2030) + sum(TMP_2040) + sum(TMP_2050) + sum(TMP_2099) as TOT_SUM				");
            queryString.AppendLine("from            (							                                                                            ");
            queryString.AppendLine("                  select          to_char(a.CAAPPLDT, 'yyyy-mm')  as YM							                        ");
            queryString.AppendLine("                                  ,nvl(decode(a.CAMIDCD, '2010', count(CAMIDCD)),0) as TMP_2010							");
            queryString.AppendLine("                                  ,nvl(decode(a.CAMIDCD, '2015', count(CAMIDCD)),0) as TMP_2015							");
            queryString.AppendLine("                                  ,nvl(decode(a.CAMIDCD, '2030', count(CAMIDCD)),0) as TMP_2030							");
            queryString.AppendLine("                                  ,nvl(decode(a.CAMIDCD, '2040', count(CAMIDCD)),0) as TMP_2040							");
            queryString.AppendLine("                                  ,nvl(decode(a.CAMIDCD, '2050', count(CAMIDCD)),0) as TMP_2050							");
            queryString.AppendLine("                                  ,nvl(decode(a.CAMIDCD, '2099', count(CAMIDCD)),0) as TMP_2099							");
            queryString.AppendLine("                  from            WI_CAINFO             a							                                    ");
            queryString.AppendLine("                                  ,WI_DMINFO            b							                                    ");
            queryString.AppendLine("                                  ,CM_LOCATION          c							                                    ");
            queryString.AppendLine("                  where           1 = 1                                                                                 ");

            if (!"".Equals(conditions["CAMIDCD"].ToString()))
            {
                queryString.AppendLine("              and             a.CAMIDCD             = '" + conditions["CAMIDCD"] + "'                               ");
            }

            queryString.AppendLine("                  and             a.DMNO                = b.DMNO							                            ");
            queryString.AppendLine("                  and             b.SFTRIDN             = c.FTR_IDN							                            ");
            queryString.AppendLine("                  and             a.CALRGCD             = '2000'                                                        ");

            if (conditions["lowerBlocks"] != null)
            {
                DataTable lowerBlockList = conditions["lowerBlocks"] as DataTable;
                queryString.AppendLine("and     b.SFTRIDN in (                                                                                              ");
                foreach (DataRow row in lowerBlockList.Rows)
                {
                    queryString.Append("'" + row["FTR_IDN"] + "'");
                    if (lowerBlockList.Rows.IndexOf(row) != lowerBlockList.Rows.Count - 1) queryString.Append(",");
                }
                queryString.Append(")                                                                                                                       ");
            }

            queryString.AppendLine("                  and             a.CAAPPLDT            between :1 and :2                                               ");
            queryString.AppendLine("                  group by        to_char(a.CAAPPLDT, 'yyyy-mm')							                            ");
            queryString.AppendLine("                                  ,a.CAMIDCD							                                                ");
            queryString.AppendLine("                )							                                                                            ");
            queryString.AppendLine("group by rollup(YM)							                                                                            ");
            queryString.AppendLine("order by        YM							                                                                            ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["startDate"];
            parameters[1].Value = (string)conditions["endDate"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //민원 리스트 조회
        public DataTable SelectComplaintList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.CANO							                                                             ");
            queryString.AppendLine("            ,e.SFTRIDN							                                                         ");
            queryString.AppendLine("            ,f.LOC_NAME							                                                         ");
            queryString.AppendLine("            ,to_char(a.CAAPPLDT, 'yyyy-mm-dd')    as CAAPPLDT				                             ");
            queryString.AppendLine("            ,a.DMNO							                                                             ");
            queryString.AppendLine("            ,a.CANM							                                                             ");
            queryString.AppendLine("            ,a.CAADDR							                                                         ");
            queryString.AppendLine("            ,a.CALRGCD							                                                         ");
            queryString.AppendLine("            ,c.CODE_NAME      as CALRGCD_NM							                                     ");
            queryString.AppendLine("            ,a.CAMIDCD							                                                         ");
            queryString.AppendLine("            ,d.CODE_NAME      as CAMIDCD_NM							                                     ");
            queryString.AppendLine("            ,replace(a.cacont,'개인정보 기입 금지' || chr(10),'') cacont                                            ");
            queryString.AppendLine("from        WI_CAINFO         a   --민원							                                     ");
            queryString.AppendLine("            ,CM_CODE          c   --민원구분 코드							                             ");
            queryString.AppendLine("            ,CM_CODE          d   --민원분류 코드							                             ");
            queryString.AppendLine("            ,WI_DMINFO        e   --수용가 정보							                                 ");
            queryString.AppendLine("            ,CM_LOCATION      f   --지역정보(블록)							                             ");
            queryString.AppendLine("where       1 = 1                                                                                        ");

            if (!"".Equals(conditions["CAMIDCD"].ToString()))
            {
                queryString.AppendLine("and     a.CAMIDCD         = '" + conditions["CAMIDCD"] + "'                                          ");
            }

            queryString.AppendLine("and         c.PCODE           = '9003'     							                                     ");
            queryString.AppendLine("and         d.PCODE           = '9004'							                                         ");
            queryString.AppendLine("and         a.CALRGCD         = '2000'                                                                   ");    //수질민원만 처리
            queryString.AppendLine("and         a.CAAPPLDT        between :1 and :2                                                          ");
            queryString.AppendLine("and         a.CALRGCD         = c.CODE							                                         ");
            queryString.AppendLine("and         a.CAMIDCD         = d.CODE							                                         ");
            queryString.AppendLine("and         a.DMNO            = e.DMNO							                                         ");
            queryString.AppendLine("and         e.SFTRIDN         = f.FTR_IDN							                                     ");

            if(conditions["lowerBlocks"] != null)
            {
                DataTable lowerBlockList = conditions["lowerBlocks"] as DataTable;
                queryString.AppendLine("and     e.SFTRIDN in (                                                                               ");
                foreach (DataRow row in lowerBlockList.Rows)
                {
                    queryString.Append("'" +row["FTR_IDN"] + "'");
                    if(lowerBlockList.Rows.IndexOf(row) != lowerBlockList.Rows.Count - 1 ) queryString.Append(",");
                }
                queryString.Append(")                                                                                                        ");
            }

            queryString.AppendLine("order by    LOC_NAME							                                                         ");
            queryString.AppendLine("            ,CAAPPLDT desc							                                                     ");
            queryString.AppendLine("            ,CALRGCD    							                                                     ");
            queryString.AppendLine("            ,CAMIDCD    							                                                     ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["startDate"];
            parameters[1].Value = (string)conditions["endDate"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }    
    }
}
