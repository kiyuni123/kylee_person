﻿/**************************************************************************
 * 파일명   : chainManageDao.cs
 * 작성자   : shpark
 * 작성일자 : 2015.01.27
 * 설명     : 계통관리 DAO
 * 변경이력 : 2015.01.27 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using WaterNet.WaterNetCore;
using Oracle.DataAccess.Client;

namespace WaterNet.WQ_Analysis.dao
{
    public class ChainManageDao
    {
        private static ChainManageDao dao = null;

        public static ChainManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ChainManageDao();
            }

            return dao;
        }


        #region 외부인터페이스

        //계통마스터 리스트 조회
        public DataTable SelectChainMasterList(OracleDBManager dbManager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      CHAIN_NO                                        ");
            queryString.AppendLine("            ,CHAIN_NM                                       ");
            queryString.AppendLine("            ,REG_DAT                                        ");
            queryString.AppendLine("            ,REMARK                                         ");
            queryString.AppendLine("  from      WQ_QUALITY_CHAIN                                ");
            queryString.AppendLine(" order by   CHAIN_NM                                        ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //계통등록지점 리스트 조회
        public DataTable SelectChainDetailList(OracleDBManager dbManager, Hashtable condition)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.CHAIN_NO                                          ");
            queryString.AppendLine("            ,a.MONITOR_NO                                       ");
            queryString.AppendLine("            ,a.IDX                                              ");
            queryString.AppendLine("            ,b.MONPNT                                           ");
            queryString.AppendLine("  from      WQ_QUALITY_CHAIN_MAPPING a                          ");
            queryString.AppendLine("            ,WQ_RT_MONITOR_POINT b                              ");
            queryString.AppendLine(" where      a.MONITOR_NO    =   b.MONITOR_NO                    ");
            queryString.AppendLine("   and      a.CHAIN_NO      =   :1                              ");
            queryString.AppendLine(" order by   to_number(a.IDX)                                    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)condition["CHAIN_NO"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //추가가능지점 리스트 조회
        public DataTable SelectMonitorPointList(OracleDBManager dbManager, Hashtable condition)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      b.MONITOR_NO                                            ");
            queryString.AppendLine("            ,b.MONPNT                                               ");
            queryString.AppendLine("  from      WQ_QUALITY_CHAIN_MAPPING    a                           ");
            queryString.AppendLine("            ,WQ_RT_MONITOR_POINT    b                               ");
            queryString.AppendLine(" where      a.MONITOR_NO(+)     =   b.MONITOR_NO                    ");
            queryString.AppendLine("   and      a.CHAIN_NO(+)       =   '" + condition["CHAIN_NO"] + "' ");
            queryString.AppendLine("   and      a.MONITOR_NO        IS NULL                             ");
            queryString.AppendLine("   and      b.MONITOR_YN        =   'Y'                             ");
            queryString.AppendLine(" order by   b.MONPNT                                                ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //계통마스터 데이터 입력
        public void InsertChainMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into     WQ_QUALITY_CHAIN (                  ");
            queryString.AppendLine("                CHAIN_NO                            ");
            queryString.AppendLine("                ,CHAIN_NM                           ");
            queryString.AppendLine("                ,REG_DAT                            ");
            queryString.AppendLine("                ,REMARK                             ");
            queryString.AppendLine("            ) values (                              ");
            queryString.AppendLine("				:1						            ");
            queryString.AppendLine("				,:2						            ");
            queryString.AppendLine("				,to_date(:3,'yyyy-mm-dd')			");
            queryString.AppendLine("				,:4					                ");
            queryString.AppendLine("			)						                ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["CHAIN_NO"];
            parameters[1].Value = (string)conditions["CHAIN_NM"];
            parameters[2].Value = (string)conditions["REG_DAT"];
            parameters[3].Value = (string)conditions["REMARK"];

            dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //계통마스터 데이터 수정
        public void UpdateChainMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update      WQ_QUALITY_CHAIN                    ");
            queryString.AppendLine("   set      CHAIN_NM        = :1                ");
            queryString.AppendLine("            ,REMARK         = :2                ");
            queryString.AppendLine(" where      CHAIN_NO        = :3                ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["CHAIN_NM"];
            parameters[1].Value = (string)conditions["REMARK"];
            parameters[2].Value = (string)conditions["CHAIN_NO"];

            dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //계통마스터 데이터 삭제
        public void DeleteChainMasterData(OracleDBManager dbManager, Hashtable condition)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_QUALITY_CHAIN                ");
            queryString.AppendLine(" where          CHAIN_NO        = :1            ");

            IDataParameter[] parameter =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameter[0].Value = (string)condition["CHAIN_NO"];

            dbManager.ExecuteScript(queryString.ToString(), parameter);
        }

        //계통등록지점 데이터 저장
        public void SaveChainDetailData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();
            
            queryString.AppendLine("insert into     WQ_QUALITY_CHAIN_MAPPING (          ");
            queryString.AppendLine("                CHAIN_NO                            ");
            queryString.AppendLine("                ,MONITOR_NO                         ");
            queryString.AppendLine("                ,IDX                                ");
            queryString.AppendLine("            ) values (                              ");
            queryString.AppendLine("				:1						            ");
            queryString.AppendLine("				,:2						            ");
            queryString.AppendLine("				,:3                                 ");
            queryString.AppendLine("			)					                    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["CHAIN_NO"];
            parameters[1].Value = (string)conditions["MONITOR_NO"];
            parameters[2].Value = (string)conditions["IDX"];

            dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //계통등록지점 데이터 삭제
        public void DeleteChainDetailData(OracleDBManager dbManager, Hashtable condition)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_QUALITY_CHAIN_MAPPING            ");
            queryString.AppendLine(" where          CHAIN_NO        = :1                ");

            IDataParameter[] parameter =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameter[0].Value = (string)condition["CHAIN_NO"];

            dbManager.ExecuteScript(queryString.ToString(), parameter);
        }

        #endregion 외부인터페이스
    }
}
