﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WQ_Analysis.dao
{
    public class RechlorineSimulationDao
    {
        private static RechlorineSimulationDao dao = null;

        private RechlorineSimulationDao()
        {
        }

        public static RechlorineSimulationDao GetInstance()
        {
            if(dao == null)
            {
                dao = new RechlorineSimulationDao();
            }

            return dao;
        }

        //재염소모의 리스트 조회
        public DataTable SelectRechlorineSimulationList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.SIMULATION_NO										                    ");
            queryString.AppendLine("            ,a.SIMULATION_NM            										    ");
            queryString.AppendLine("            ,to_char(a.REG_DAT, 'yyyy-mm-dd hh24:mi')   as REG_DAT			        ");
            queryString.AppendLine("            ,b.INP_NUMBER										                    ");
            queryString.AppendLine("            ,b.TITLE										                        ");
            queryString.AppendLine("            ,a.REMARK            										            ");
            queryString.AppendLine("            ,a.USER_NAME                                                            ");
            queryString.AppendLine("from        WQ_RECHLORINE_SIMULATION   a									        ");
            queryString.AppendLine("            ,WH_TITLE                  b									        ");
            queryString.AppendLine("where       a.INP_NUMBER               = b.INP_NUMBER            			        ");

            if(conditions["SIMULATION_NM"] != null)
            {
                queryString.AppendLine("and      a.SIMULATION_NM           like '%" + conditions["SIMULATION_NM"] + "%' ");
            }

            queryString.AppendLine("order by    REG_DAT desc							                                ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }
    
        //수질모델 리스트 조회
        public DataTable SelectQualityModelList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      INP_NUMBER								                                                ");
            queryString.AppendLine("            ,TITLE									                                                ");
            queryString.AppendLine("             ,to_char(to_date(INS_DATE,'yyyymmddhh24miss'), 'yyyy-mm-dd hh24:mi')     as INS_DATE   ");
            queryString.AppendLine("            ,REMARK								                                                    ");
            queryString.AppendLine("from        WH_TITLE								                                                ");
            queryString.AppendLine("where       1 = 1									                                                ");
            queryString.AppendLine("and         USE_GBN     = 'WQ'						                                                ");
            queryString.AppendLine("order by    INS_DATE    desc						                                                ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }
    
        //재염소모의 입력
        public void InsertRechlorineSimulationData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WQ_RECHLORINE_SIMULATION	(                                               ");
            queryString.AppendLine("						                    SIMULATION_NO  				                ");
            queryString.AppendLine("						                    ,REG_DAT        			                ");
            queryString.AppendLine("						                    ,INP_NUMBER     			                ");
            queryString.AppendLine("						                    ,REMARK         			                ");
            queryString.AppendLine("						                    ,SIMULATION_NM   		                    ");
            queryString.AppendLine("						                    ,USER_ID   		                            ");
            queryString.AppendLine("						                    ,USER_NAME   		                        ");
            queryString.AppendLine("					                    ) values (						                ");
            queryString.AppendLine("						                    :1							                ");
            queryString.AppendLine("						                    ,sysdate							        ");
            queryString.AppendLine("						                    ,:2							                ");
            queryString.AppendLine("						                    ,:3							                ");
            queryString.AppendLine("						                    ,:4							                ");
            queryString.AppendLine("						                    ,:5							                ");
            queryString.AppendLine("						                    ,:6							                ");
            queryString.AppendLine("					                    )						                        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["SIMULATION_NO"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["REMARK"];
            parameters[3].Value = (string)conditions["SIMULATION_NM"];
            parameters[4].Value = EMFrame.statics.AppStatic.USER_ID;
            parameters[5].Value = EMFrame.statics.AppStatic.USER_NAME;

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //재염소모의 수정
        public void UpdateRechlorineSimulationData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WQ_RECHLORINE_SIMULATION 			                        ");
            queryString.AppendLine("set     REMARK            = :1										");
            queryString.AppendLine("        ,SIMULATION_NM    = :2										");
            queryString.AppendLine("where   SIMULATION_NO     = :3										");
            queryString.AppendLine("and     INP_NUMBER        = :4										");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["REMARK"];
            parameters[1].Value = (string)conditions["SIMULATION_NM"];
            parameters[2].Value = (string)conditions["SIMULATION_NO"];
            parameters[3].Value = (string)conditions["INP_NUMBER"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //재염소모의 삭제
        public void DeleteRechlorineSimulationData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_RECHLORINE_SIMULATION                ");
            queryString.AppendLine("where           SIMULATION_NO               = :1        ");
            queryString.AppendLine("and             INP_NUMBER                  = :2        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["SIMULATION_NO"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }
    
        //재염소 모의 등록 전 대상모델 중복 확인
        public DataTable IsExistQualityModel(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          INP_NUMBER                          ");
            queryString.AppendLine("from            WQ_RECHLORINE_SIMULATION            ");
            queryString.AppendLine("where           INP_NUMBER      = :1                ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    
        //재염소 처리지점 리스트 조회
        public DataTable SelectRechlorinePositionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select		SIMULATION_NO					        ");
            queryString.AppendLine("		    ,INP_NUMBER								");
            queryString.AppendLine("		    ,ID									    ");
            queryString.AppendLine("		    ,REMARK									");
            queryString.AppendLine("		    ,TAGNAME								");
            queryString.AppendLine("		    ,POINT_NM								");
            queryString.AppendLine("		    ,VOLUME									");
            queryString.AppendLine("            ,MAP_X                                  ");
            queryString.AppendLine("            ,MAP_Y                                  ");
            queryString.AppendLine("from		WQ_RECHLORINE_POSITION					");
            queryString.AppendLine("where		SIMULATION_NO		        = :1		");
            queryString.AppendLine("and		    INP_NUMBER		            = :2		");
            queryString.AppendLine("order by	ID							            ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["SIMULATION_NO"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //재염소 처리지점 입력
        public void InsertRechlorinePositionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WQ_RECHLORINE_POSITION	(										");
            queryString.AppendLine("						                SIMULATION_NO						");
            queryString.AppendLine("						                ,INP_NUMBER							");
            queryString.AppendLine("						                ,ID									");
            queryString.AppendLine("						                ,REMARK								");
            queryString.AppendLine("						                ,TAGNAME							");
            queryString.AppendLine("						                ,POINT_NM							");
            queryString.AppendLine("						                ,VOLUME								");
            queryString.AppendLine("						                ,MAP_X								");
            queryString.AppendLine("						                ,MAP_Y								");
            queryString.AppendLine("					                ) values (								");
            queryString.AppendLine("						                :1									");
            queryString.AppendLine("						                ,:2									");
            queryString.AppendLine("						                ,:3									");
            queryString.AppendLine("						                ,:4									");
            queryString.AppendLine("						                ,:5									");
            queryString.AppendLine("						                ,:6									");
            queryString.AppendLine("						                ,:7									");
            queryString.AppendLine("						                ,:8									");
            queryString.AppendLine("						                ,:9									");
            queryString.AppendLine("					                )										");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
                 ,new OracleParameter("7", OracleDbType.Varchar2)
                 ,new OracleParameter("8", OracleDbType.Varchar2)
                 ,new OracleParameter("9", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["SIMULATION_NO"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["ID"];
            parameters[3].Value = (string)conditions["REMARK"];
            parameters[4].Value = (string)conditions["TAGNAME"];
            parameters[5].Value = (string)conditions["POINT_NM"];
            parameters[6].Value = (string)conditions["VOLUME"];
            parameters[7].Value = (string)conditions["MAP_X"];
            parameters[8].Value = (string)conditions["MAP_Y"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }
    
        //재염소 처리지점 수정
        public void UpdateRechlorinePositionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update	WQ_RECHLORINE_POSITION				");
            queryString.AppendLine("set	REMARK		                = :1		");
            queryString.AppendLine("	    ,TAGNAME	            = :2		");
            queryString.AppendLine("	    ,POINT_NM	            = :3		");
            queryString.AppendLine("	    ,VOLUME		            = :4		");
            queryString.AppendLine("where	SIMULATION_NO	        = :5		");
            queryString.AppendLine("and	INP_NUMBER	                = :6		");
            queryString.AppendLine("and	ID		                    = :7        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
                 ,new OracleParameter("7", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["REMARK"];
            parameters[1].Value = (string)conditions["TAGNAME"];
            parameters[2].Value = (string)conditions["POINT_NM"];
            parameters[3].Value = (string)conditions["VOLUME"];
            parameters[4].Value = (string)conditions["SIMULATION_NO"];
            parameters[5].Value = (string)conditions["INP_NUMBER"];
            parameters[6].Value = (string)conditions["ID"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }
    
        //재염소 처리지점 삭제
        public void DeleteRechlorinePositionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_RECHLORINE_POSITION          ");
            queryString.AppendLine("where           SIMULATION_NO       = :1        ");
            queryString.AppendLine("and             INP_NUMBER          = :2        ");
            queryString.AppendLine("and             ID                  = :3        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["SIMULATION_NO"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["ID"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }
    
        //재염소 처리지점 존재여부 확인
        public DataTable IsExistRechlorinePositionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          SIMULATION_NO                           ");
            queryString.AppendLine("from            WQ_RECHLORINE_POSITION                  ");
            queryString.AppendLine("where           SIMULATION_NO               = :1        ");
            queryString.AppendLine("and             INP_NUMBER                  = :2        ");
            queryString.AppendLine("and             ID                          = :3        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["SIMULATION_NO"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["ID"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    
        //수질(염소) TAG List 조회
        public DataTable SelectQualityTagList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select		TAGNAME						    ");
            queryString.AppendLine("		    ,TAG_DESC						");
            queryString.AppendLine("from		IF_TAG_GBN						");
            queryString.AppendLine("where		TAG_GBN		    = 'CLI'			");
            queryString.AppendLine("order by	TAG_DESC                        ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }
    
        //재염소처리지점 일괄삭제
        public void DeleteAllRechlorinePositionDatas(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_RECHLORINE_POSITION          ");
            queryString.AppendLine("where           SIMULATION_NO       = :1        ");
            queryString.AppendLine("and             INP_NUMBER          = :2        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["SIMULATION_NO"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }
    
        //해당 모델의 Quality섹션 정보 조회
        public DataTable SelectInitialQuality(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select		a.ID						            ");
            queryString.AppendLine("		    ,a.INITQUAL						        ");
            queryString.AppendLine("		    ,b.X						            ");
            queryString.AppendLine("		    ,b.Y						            ");
            queryString.AppendLine("from		WH_QUALITY          a			        ");
            queryString.AppendLine("            ,WH_COORDINATES     b                   ");
            queryString.AppendLine("where		a.INP_NUMBER	    = :1		        ");
            queryString.AppendLine("and         a.INP_NUMBER        = b.INP_NUMBER      ");
            queryString.AppendLine("and         a.ID                = b.ID              ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    
        //Junction 테이블에 ID가 존재하는지 확인
        public DataTable IsExistJunctionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                      ");
            queryString.AppendLine("from            WH_JUNCTIONS            ");
            queryString.AppendLine("where           INP_NUMBER      = :1    ");
            queryString.AppendLine("and             ID              = :2    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ID"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //Reservoir 테이블에 ID가 존재하는지 확인
        public DataTable IsExistReservoirData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                      ");
            queryString.AppendLine("from            WH_RESERVOIRS           ");
            queryString.AppendLine("where           INP_NUMBER      = :1    ");
            queryString.AppendLine("and             ID              = :2    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ID"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //Tank 테이블에 ID가 존재하는지 확인
        public DataTable IsExistTankData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                      ");
            queryString.AppendLine("from            WH_TANK                 ");
            queryString.AppendLine("where           INP_NUMBER      = :1    ");
            queryString.AppendLine("and             ID              = :2    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ID"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    
        //잔류염소 감시지점으로 등록
        public void InsertMonitorPoint(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WQ_RT_MONITOR_POINT (						            ");
            queryString.AppendLine("					                MONITOR_NO				        ");
            queryString.AppendLine("					                ,MONPNT_GBN			            ");
            queryString.AppendLine("					                ,SBLOCK_CODE			        ");
            queryString.AppendLine("					                ,SELECT_DATE			        ");
            queryString.AppendLine("					                ,MONPNT					        ");
            queryString.AppendLine("					                ,TAG_ID_CL				        ");
            queryString.AppendLine("					                ,NODE_ID				        ");
            queryString.AppendLine("					                ,MAP_X      			        ");
            queryString.AppendLine("					                ,MAP_Y      			        ");
            queryString.AppendLine("					                ,MONITOR_YN 			        ");
            queryString.AppendLine("				                ) values (					        ");
            queryString.AppendLine("					                :1						        ");
            queryString.AppendLine("					                ,:2						        ");
            queryString.AppendLine("					                ,:3						        ");
            queryString.AppendLine("					                ,to_char(sysdate,'yyyymmdd')    ");
            queryString.AppendLine("					                ,:4						        ");
            queryString.AppendLine("					                ,:5						        ");
            queryString.AppendLine("					                ,:6 					        ");
            queryString.AppendLine("					                ,:7						        ");
            queryString.AppendLine("					                ,:8	 					        ");
            queryString.AppendLine("					                ,:9	    			            ");
            queryString.AppendLine("				                )					                ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
                 ,new OracleParameter("7", OracleDbType.Varchar2)
                 ,new OracleParameter("8", OracleDbType.Varchar2)
                 ,new OracleParameter("9", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["MONITOR_NO"];
            parameters[1].Value = (string)conditions["MONPNT_GBN"];
            parameters[2].Value = (string)conditions["SBLOCK_CODE"];
            parameters[3].Value = (string)conditions["MONPNT"];
            parameters[4].Value = (string)conditions["TAG_ID_CL"];
            parameters[5].Value = (string)conditions["NODE_ID"];
            parameters[6].Value = (string)conditions["MAP_X"];
            parameters[7].Value = (string)conditions["MAP_Y"];
            parameters[8].Value = (string)conditions["MONITOR_YN"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }
    }
}
