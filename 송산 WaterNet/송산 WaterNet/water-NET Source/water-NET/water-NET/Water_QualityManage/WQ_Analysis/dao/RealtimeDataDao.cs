﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using WaterNet.WaterNetCore;
using Oracle.DataAccess.Client;

namespace WaterNet.WQ_Analysis.dao
{
    public class RealtimeDataDao
    {
        private static RealtimeDataDao dao = null;

        private RealtimeDataDao()
        {
        }

        public static RealtimeDataDao GetInstance()
        {
            if(dao == null)
            {
                dao = new RealtimeDataDao();
            }

            return dao;
        }

        //실시간 수질감시대상 지점 리스트 조회
        //conditons 체크 쿼리 추가, SELECT_DATE 조회 형식 변경 : shaprk_2015-03-12
        public DataTable SelectMainRealtimeCheckpointList(OracleDBManager dbManager, Hashtable Conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select		MONITOR_NO 					                            ");
            queryString.AppendLine("		    ,MONPNT_GBN      					                    ");
            queryString.AppendLine("		    ,SBLOCK_CODE     					                    ");
            queryString.AppendLine("		    ,to_date(SELECT_DATE, 'yyyy-mm-dd')     as SELECT_DATE  ");
            queryString.AppendLine("		    ,MONPNT          					                    ");
            queryString.AppendLine("		    ,TAG_ID_TE       					                    ");
            queryString.AppendLine("		    ,TAG_ID_PH       					                    ");
            queryString.AppendLine("		    ,TAG_ID_TB       					                    ");
            queryString.AppendLine("		    ,TAG_ID_CU       					                    ");
            queryString.AppendLine("		    ,TAG_ID_CL       					                    ");
            queryString.AppendLine("		    ,NODE_ID         					                    ");
            queryString.AppendLine("		    ,MAP_X           					                    ");
            queryString.AppendLine("		    ,MAP_Y           					                    ");
            queryString.AppendLine("		    ,MONITOR_YN					                            ");
            queryString.AppendLine("from		WQ_RT_MONITOR_POINT					                    ");

            if (Conditions != null && !Conditions["MONITOR_YN"].Equals(""))     //표출여부 검색조건 조회 : 수질감시지점관리 에서 사용
            {
                queryString.AppendLine("where		MONITOR_YN		= '" + Conditions["MONITOR_YN"].ToString() + "'     ");
            }
            else if (Conditions != null && Conditions["MONITOR_YN"].Equals(""))     //표출여부 전체 조회 : 수질감시지점관리 에서 사용
            {
                queryString.AppendLine("where		MONITOR_YN		in('Y', 'N')				                        ");
            }
            else     //기존에 사용하던 쿼리 : 표출여부 'Y' 만 조회
            {
                queryString.AppendLine("where		MONITOR_YN		= 'Y'				                                ");
            }

            if (Conditions != null && !Conditions["MONPNT"].Equals(""))     //제목 검색조건 조회 : 수질감시지점관리 에서 사용
            {
                queryString.AppendLine("and         MONPNT  like    '%" + Conditions["MONPNT"].ToString() + "%'         ");
            }

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //실시간 수질감시지점 데이터 입력 : 2015-03-12_shpark
        public void InsertMonitorPointData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into     WQ_RT_MONITOR_POINT (				");
            queryString.AppendLine("				MONITOR_NO                          ");
            queryString.AppendLine("				,MONPNT_GBN                         ");
            queryString.AppendLine("				,SELECT_DATE                        ");
            queryString.AppendLine("				,MONPNT                             ");
            queryString.AppendLine("				,TAG_ID_CL                          ");
            queryString.AppendLine("				,TAG_ID_TB                          ");
            queryString.AppendLine("				,TAG_ID_PH                          ");
            queryString.AppendLine("				,TAG_ID_TE                          ");
            queryString.AppendLine("				,TAG_ID_CU                          ");
            queryString.AppendLine("				,MAP_X                              ");
            queryString.AppendLine("				,MAP_Y                              ");
            queryString.AppendLine("				,MONITOR_YN                         ");
            queryString.AppendLine("			) values (						        ");
            queryString.AppendLine("				:1						            ");
            queryString.AppendLine("				,'2'						        ");
            queryString.AppendLine("				,:2						            ");
            queryString.AppendLine("				,:3						            ");
            queryString.AppendLine("				,:4						            ");
            queryString.AppendLine("				,:5						            ");
            queryString.AppendLine("				,:6						            ");
            queryString.AppendLine("				,:7						            ");
            queryString.AppendLine("				,:8						            ");
            queryString.AppendLine("				,:9						            ");
            queryString.AppendLine("				,:10						        ");
            queryString.AppendLine("				,:11						        ");
            queryString.AppendLine("			)						                ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
                 ,new OracleParameter("7", OracleDbType.Varchar2)
                 ,new OracleParameter("8", OracleDbType.Varchar2)
                 ,new OracleParameter("9", OracleDbType.Varchar2)
                 ,new OracleParameter("10", OracleDbType.Varchar2)
                 ,new OracleParameter("11", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["MONITOR_NO"];
            parameters[1].Value = (string)conditions["SELECT_DATE"];
            parameters[2].Value = (string)conditions["MONPNT"];
            parameters[3].Value = (string)conditions["TAG_ID_CL"];
            parameters[4].Value = (string)conditions["TAG_ID_TB"];
            parameters[5].Value = (string)conditions["TAG_ID_PH"];
            parameters[6].Value = (string)conditions["TAG_ID_TE"];
            parameters[7].Value = (string)conditions["TAG_ID_CU"];
            parameters[8].Value = (string)conditions["MAP_X"];
            parameters[9].Value = (string)conditions["MAP_Y"];
            parameters[10].Value = (string)conditions["MONITOR_YN"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //실시간 수질감시지점 데이터 수정 : 2015-03-12_shpark
        public void UpdateMonitorPointData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  	WQ_RT_MONITOR_POINT					    ");
            queryString.AppendLine("set	        MONPNT              = :1		        ");
            queryString.AppendLine("	        ,MONPNT_GBN         = '2'		        ");
            queryString.AppendLine("	        ,TAG_ID_CL          = :2		        ");
            queryString.AppendLine("	        ,TAG_ID_TB          = :3		        ");
            queryString.AppendLine("	        ,TAG_ID_PH          = :4		        ");
            queryString.AppendLine("	        ,TAG_ID_TE          = :5		        ");
            queryString.AppendLine("	        ,TAG_ID_CU          = :6		        ");
            queryString.AppendLine("	        ,MAP_X              = :7		        ");
            queryString.AppendLine("	        ,MAP_Y              = :8		        ");
            queryString.AppendLine("	        ,MONITOR_YN         = :9		        ");
            queryString.AppendLine("where	    MONITOR_NO          = :10		        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
                 ,new OracleParameter("7", OracleDbType.Varchar2)
                 ,new OracleParameter("8", OracleDbType.Varchar2)
                 ,new OracleParameter("9", OracleDbType.Varchar2)
                 ,new OracleParameter("10", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["MONPNT"];
            parameters[1].Value = (string)conditions["TAG_ID_CL"];
            parameters[2].Value = (string)conditions["TAG_ID_TB"];
            parameters[3].Value = (string)conditions["TAG_ID_PH"];
            parameters[4].Value = (string)conditions["TAG_ID_TE"];
            parameters[5].Value = (string)conditions["TAG_ID_CU"];
            parameters[6].Value = (string)conditions["MAP_X"];
            parameters[7].Value = (string)conditions["MAP_Y"];
            parameters[8].Value = (string)conditions["MONITOR_YN"];
            parameters[9].Value = (string)conditions["MONITOR_NO"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //실시간 수질감시지점 데이터 삭제 : 2015-03-12_shpark
        public void DeleteMonitorPointData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_RT_MONITOR_POINT             ");
            queryString.AppendLine("where           MONITOR_NO          = :1        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["MONITOR_NO"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //수질 항목별 태그 리스트 조회 : 2015-03-12_shpark
        public DataTable SelectMonitorPointTagList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      DISTINCT    b.TAGNAME                                                   ");
            queryString.AppendLine("            ,b.TAG_DESC                                                             ");
            queryString.AppendLine("  from      IF_IHTAGS a                                                             ");
            queryString.AppendLine("            ,IF_TAG_GBN b                                                           ");
            queryString.AppendLine(" where      a.USE_YN                = 'Y'                                           ");
            queryString.AppendLine("   and      b.TAG_GBN               = '" + conditions["TAG_GBN"] +                "'");
            queryString.AppendLine("   and      a.TAGNAME               = b.TAGNAME                                     ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //감시지점/항목 별 실시간 계측데이터 조회
        public DataTable SelectMainRealtimeDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //이후 특정시간 기준으로 데이터를 조회해야 할 경우 주석의 로직을 사용해야함

            //queryString.AppendLine("select      a.MONITOR_NO					                            ");
            //queryString.AppendLine("            ,b.VALUE                as TE_VALUE					        ");
            //queryString.AppendLine("            ,c.VALUE                as PH_VALUE					        ");
            //queryString.AppendLine("            ,d.VALUE                as TB_VALUE					        ");
            //queryString.AppendLine("            ,e.VALUE                as CU_VALUE					        ");
            //queryString.AppendLine("            ,f.VALUE                as CL_VALUE					        ");
            //queryString.AppendLine("from        WQ_RT_MONITOR_POINT     a					                ");
            //queryString.AppendLine("            ,IF_GATHER_REALTIME     b					                ");
            //queryString.AppendLine("            ,IF_GATHER_REALTIME     c					                ");
            //queryString.AppendLine("            ,IF_GATHER_REALTIME     d					                ");
            //queryString.AppendLine("            ,IF_GATHER_REALTIME     e					                ");
            //queryString.AppendLine("            ,IF_GATHER_REALTIME     f					                ");
            //queryString.AppendLine("where       1 = 1					                                    ");
            //queryString.AppendLine("and         b.TIMESTAMP(+)          = to_date(:1, 'yyyymmddhh24mi')	    ");
            //queryString.AppendLine("and         c.TIMESTAMP(+)          = to_date(:1, 'yyyymmddhh24mi')		");
            //queryString.AppendLine("and         d.TIMESTAMP(+)          = to_date(:1, 'yyyymmddhh24mi')		");
            //queryString.AppendLine("and         e.TIMESTAMP(+)          = to_date(:1, 'yyyymmddhh24mi')		");
            //queryString.AppendLine("and         f.TIMESTAMP(+)          = to_date(:1, 'yyyymmddhh24mi')		");
            //queryString.AppendLine("and         a.TAG_ID_TE             = b.TAGNAME(+)					    ");
            //queryString.AppendLine("and         a.TAG_ID_PH             = c.TAGNAME(+)					    ");
            //queryString.AppendLine("and         a.TAG_ID_TB             = d.TAGNAME(+)					    ");
            //queryString.AppendLine("and         a.TAG_ID_CU             = e.TAGNAME(+)					    ");
            //queryString.AppendLine("and         a.TAG_ID_CL             = f.TAGNAME(+)					    ");

            //IDataParameter[] parameters =  {
            //     new OracleParameter("1", OracleDbType.Varchar2)
            //};

            //parameters[0].Value = (string)conditions["timestamp"];

            //return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);


            queryString.AppendLine("select		MONITOR_NO					                                                                                        ");
            queryString.AppendLine("		    ,sum(decode(GBN, 'PH', value))  as PH_VALUE					                                                        ");
            queryString.AppendLine("		    ,sum(decode(GBN, 'CL', value))  as CL_VALUE					                                                        ");
            queryString.AppendLine("		    ,sum(decode(GBN, 'CU', value))  as CU_VALUE					                                                        ");
            queryString.AppendLine("		    ,sum(decode(GBN, 'TB', value))  as TB_VALUE					                                                        ");
            queryString.AppendLine("		    ,sum(decode(GBN, 'TE', value))  as TE_VALUE					                                                        ");
            queryString.AppendLine("from		(            					                                                                                    ");
            queryString.AppendLine("			    select	a.MONITOR_NO					                                                                        ");
            queryString.AppendLine("				        ,a.GBN					                                                                                ");
            queryString.AppendLine("				        ,a.TAGNAME					                                                                            ");
            queryString.AppendLine("				        ,(						                                                                                ");
            queryString.AppendLine("					        select	/*+ index_desc ( aa if_gather_realtime_pk) */ 					                            ");
            queryString.AppendLine("						            to_char(TIMESTAMP, 'yyyy/mm/dd hh24:mi') 					                                ");
            queryString.AppendLine("					        from	IF_GATHER_REALTIME aa 					                                                    ");
            queryString.AppendLine("					        where	aa.TAGNAME = a.TAGNAME 					                                                    ");
            queryString.AppendLine("					        and	    rownum = 1 					                                                                ");
            queryString.AppendLine("				        )								as TIMESTAMP					                                        ");
            queryString.AppendLine("				        ,(						                                                                                ");
            queryString.AppendLine("					        select	/*+ index_desc ( aa if_gather_realtime_pk) */ 					                            ");
            queryString.AppendLine("						            VALUE 					                                                                    ");
            queryString.AppendLine("					        from	IF_GATHER_REALTIME aa 					                                                    ");
            queryString.AppendLine("					        where	aa.TAGNAME = a.TAGNAME 					                                                    ");
            queryString.AppendLine("					        and	    rownum = 1 					                                                                ");
            queryString.AppendLine("				        )								as VALUE					                                            ");
            queryString.AppendLine("			    from	(						                                                                                ");
            queryString.AppendLine("					        select 'TE' as GBN, MONITOR_NO, TAG_ID_TE as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("					        select 'PH' as GBN, MONITOR_NO, TAG_ID_PH as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("					        select 'TB' as GBN, MONITOR_NO, TAG_ID_TB as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("					        select 'CU' as GBN, MONITOR_NO, TAG_ID_CU as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("					        select 'CL' as GBN, MONITOR_NO, TAG_ID_CL as TAGNAME from WQ_RT_MONITOR_POINT					    ");
            queryString.AppendLine("				        )		a					                                                                            ");
            queryString.AppendLine("		    )					                                                                                                ");
            queryString.AppendLine("group by	MONITOR_NO					                                                                                        ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //감시지점 구분 조회 (검색조건)
        public DataTable SelectMonitorGbnList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          CODE                        ");
            queryString.AppendLine("                ,CODE_NAME  as TEXT         ");
            queryString.AppendLine("from            CM_CODE                     ");
            queryString.AppendLine("where           PCODE       = '3002'        ");
            queryString.AppendLine("order by        CODE_NAME                   ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //수질항목 조회 (검색조건)
        public DataTable SelectQualityItemList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          CODE                        ");
            queryString.AppendLine("                ,CODE_NAME  as TEXT         ");
            queryString.AppendLine("from            CM_CODE                     ");
            queryString.AppendLine("where           PCODE       = '1020'        ");
            queryString.AppendLine("order by        CODE_NAME                   ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //실시간 수질감시지점 리스트
        public DataTable SelectCheckpointList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select	    'false'           as CHK                                                                                                    ");
            queryString.AppendLine("            ,a.MONITOR_NO					                  					                                                        ");
            queryString.AppendLine("            ,a.MONPNT_GBN					                                                                                            ");
            queryString.AppendLine("            ,b.CODE_NAME      as MONPNT_GBN_NM					                                                                        ");
            queryString.AppendLine("            ,a.MONPNT					                                                                                                ");
            queryString.AppendLine("            ,a.ITEM					                                                                                                    ");
            queryString.AppendLine("            ,c.CODE_NAME      as ITEM_NM					                                                                            ");
            queryString.AppendLine("            ,a.TAGNAME					                                   					                                            ");
            queryString.AppendLine("            ,d.DESCRIPTION				                                   					                                            ");
            queryString.AppendLine("from	      (						                                           					                                        ");
            queryString.AppendLine("              select 'TEI' as ITEM, MONITOR_NO, MONPNT_GBN, MONPNT, TAG_ID_TE as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("              select 'PHI' as ITEM, MONITOR_NO, MONPNT_GBN, MONPNT, TAG_ID_PH as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("              select 'TBI' as ITEM, MONITOR_NO, MONPNT_GBN, MONPNT, TAG_ID_TB as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("              select 'CUI' as ITEM, MONITOR_NO, MONPNT_GBN, MONPNT, TAG_ID_CU as TAGNAME from WQ_RT_MONITOR_POINT union					");
            queryString.AppendLine("              select 'CLI' as ITEM, MONITOR_NO, MONPNT_GBN, MONPNT, TAG_ID_CL as TAGNAME from WQ_RT_MONITOR_POINT					    ");
            queryString.AppendLine("            )		      a									                                                                            ");
            queryString.AppendLine("            ,CM_CODE  b					                                                                                                ");
            queryString.AppendLine("            ,CM_CODE  c					                                                                                                ");
            queryString.AppendLine("            ,IF_IHTAGS d				                                                                                                ");
            queryString.AppendLine("where       1 = 1					                                                                                                    ");

            if (conditions["ITEM"] != null)
            {
                queryString.AppendLine("and     ITEM          = '" + conditions["ITEM"] + "'                                                                                ");
            }

            if (conditions["MONPNT_GBN"] != null)
            {
                queryString.AppendLine("and     MONPNT_GBN    = '" + conditions["MONPNT_GBN"] + "'                                                                          ");
            }

            if (conditions["MONPNT"] != null)
            {
                queryString.AppendLine("and     MONPNT        like '%" + conditions["MONPNT"] + "%'                                                                         ");
            }
            
            queryString.AppendLine("and         b.PCODE       = '3002'					                                                                                    ");
            queryString.AppendLine("and         c.PCODE       = '1020'					                                                                                    ");
            queryString.AppendLine("and         a.MONPNT_GBN  = b.CODE					                                                                                    ");
            queryString.AppendLine("and         a.ITEM        = c.CODE					                                                                                    ");
            queryString.AppendLine("and         a.TAGNAME     = d.TAGNAME				                                                                                    ");
            queryString.AppendLine("order by    b.CODE_NAME					                                                                                                ");
            queryString.AppendLine("            ,a.MONPNT					                                                                                                ");
            queryString.AppendLine("            ,c.CODE_NAME					                                                                                            ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }
    
        //실시간 수질데이터 조회
        public DataTable SelectRealtimeDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            Hashtable checkpointDatas = conditions["checkpointList"] as Hashtable;
            StringBuilder queryString = new StringBuilder();
            string INSentence = "";

            queryString.AppendLine("select      to_char(b.timestamp, 'yyyy-mm-dd hh24:mi')      as TIMESTAMP				            ");

            //동적 컬럼생성 (컬럼명 -> "C_" + TAGNAME)
            foreach (string key in checkpointDatas.Keys)
            {
                Hashtable checkpointData = checkpointDatas[key] as Hashtable;
                queryString.AppendLine("        ,sum(decode(a.TAGNAME, '" + checkpointData["TAGNAME"] + "', b.VALUE))       as " + key + "    ");

                //in 구분에 들어갈 내용 생성
                if(!"".Equals(INSentence)) INSentence = INSentence + ",";
                INSentence = INSentence + "'" + checkpointData["TAGNAME"] + "'";
            }

            queryString.AppendLine("from        (														                                ");
            queryString.AppendLine("              select TAG_ID_TE as TAGNAME from WQ_RT_MONITOR_POINT union				            ");
            queryString.AppendLine("              select TAG_ID_PH as TAGNAME from WQ_RT_MONITOR_POINT union				            ");
            queryString.AppendLine("              select TAG_ID_TB as TAGNAME from WQ_RT_MONITOR_POINT union				            ");
            queryString.AppendLine("              select TAG_ID_CU as TAGNAME from WQ_RT_MONITOR_POINT union				            ");
            queryString.AppendLine("              select TAG_ID_CL as TAGNAME from WQ_RT_MONITOR_POINT						            ");
            queryString.AppendLine("            )                     a														            ");
            queryString.AppendLine("            ,IF_GATHER_REALTIME   b														            ");
            queryString.AppendLine("where       1 = 1														                            ");
            queryString.AppendLine("and         b.TIMESTAMP between to_date('" + conditions["startDate"] + "0000', 'yyyymmddhh24mi')    ");
            queryString.AppendLine("                        and     to_date('" + conditions["endDate"] + "2359', 'yyyymmddhh24mi')      ");
            queryString.AppendLine("and         a.TAGNAME   in (" + INSentence + ")            						                    ");
            queryString.AppendLine("and         a.TAGNAME   = b.TAGNAME														            ");
            queryString.AppendLine("group by    b.TIMESTAMP														                        ");
            queryString.AppendLine("order by    b.TIMESTAMP														                        ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //수질 계통 리스트 조회
        public DataTable SelectQualityChainList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.CHAIN_NO						                                                                                                ");
            queryString.AppendLine("            ,a.CHAIN_NM						                                                                                                ");
            queryString.AppendLine("            ,a.REMARK						                                                                                                ");
            queryString.AppendLine("            ,trim('~' from xmlagg(xmlelement(MONPNT,MONPNT||'~') order by to_number(IDX)).extract('//text()').GetStringVal()) CHAINDATA		");
            queryString.AppendLine("from        WQ_QUALITY_CHAIN            a						                                                                            ");
            queryString.AppendLine("            ,WQ_QUALITY_CHAIN_MAPPING   b						                                                                            ");
            //수질감시지점 기준으로 변경 2014-10-14 KHB
            //queryString.AppendLine("            ,WQ_QUALITY_CHAIN_POINT     c           						                                                                ");
            queryString.AppendLine("            ,WQ_RT_MONITOR_POINT        c           						                                                                ");
            queryString.AppendLine("where       a.CHAIN_NO                  = b.CHAIN_NO					    	                                                            ");
            queryString.AppendLine("and         b.MONITOR_NO                = c.MONITOR_NO             						                                                    ");
            queryString.AppendLine("group by    a.CHAIN_NO						                                                                                                ");
            queryString.AppendLine("            ,a.CHAIN_NM						                                                                                                ");
            queryString.AppendLine("            ,a.REMARK					                                                                                                    ");
            queryString.AppendLine("order by    a.CHAIN_NO                                                                                                                      ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //수질 계통 실시간 데이터 조회
        public DataTable SelectChainRealtimeDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.MONITOR_NO						                                ");
            queryString.AppendLine("            ,b.MONPNT						                                    ");
            queryString.AppendLine("            ,b.TAG_ID_CL						                                ");
            queryString.AppendLine("            ,to_number(c.VALUE)         as VALUE						        ");
            queryString.AppendLine("from        WQ_QUALITY_CHAIN_MAPPING    a						                ");
            //수질감시지점 기준으로 변경 2014-10-14 KHB
            //queryString.AppendLine("            ,WQ_QUALITY_CHAIN_POINT     b						                ");
            queryString.AppendLine("            ,WQ_RT_MONITOR_POINT        b						                ");
            queryString.AppendLine("            ,IF_GATHER_REALTIME         c						                ");
            queryString.AppendLine("where       a.MONITOR_NO                = b.MONITOR_NO						    ");
            queryString.AppendLine("and         b.TAG_ID_CL                 = c.TAGNAME(+)						    ");
            queryString.AppendLine("and         a.CHAIN_NO                  = :1						            ");
            queryString.AppendLine("and         c.TIMESTAMP(+)              = to_date(:2, 'yyyymmddhh24miss')		");
            queryString.AppendLine("order by    to_number(a.IDX)                                                    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["CHAIN_NO"];
            parameters[1].Value = (string)conditions["TIMESTAMP"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    
        //실시간 수질 데이터 조회 (Dashboard)
        public DataTable SelectRealtimeQualityDataForDashboard(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      MONITOR_NO							                                                                                                                        ");
            queryString.AppendLine("            ,MONPNT							                                                                                                                            ");
            queryString.AppendLine("            ,TAG_ID_CL							                                                                                                                        ");
            queryString.AppendLine("            ,TAG_ID_TB							                                                                                                                        ");
            queryString.AppendLine("            ,TAG_ID_PH							                                                                                                                        ");
            queryString.AppendLine("            ,nvl((select /*+ index_desc (IF_GATHER_REALTIME if_gather_realtime_pk) */ value from IF_GATHER_REALTIME where TAG_ID_CL = TAGNAME and rownum = 1),'')    as  CL_VALUE		");
            queryString.AppendLine("            ,nvl((select /*+ index_desc (IF_GATHER_REALTIME if_gather_realtime_pk) */ value from IF_GATHER_REALTIME where TAG_ID_TB = TAGNAME and rownum = 1),'')    as  TB_VALUE		");
            queryString.AppendLine("            ,nvl((select /*+ index_desc (IF_GATHER_REALTIME if_gather_realtime_pk) */ value from IF_GATHER_REALTIME where TAG_ID_PH = TAGNAME and rownum = 1),'')    as  PH_VALUE		");
            queryString.AppendLine("from        WQ_RT_MONITOR_POINT     							                                                                                                        ");
            queryString.AppendLine("where       1 = 1							                                                                                                                            ");
            queryString.AppendLine("and         MONITOR_YN    = 'Y'							                                                                                                                ");
            queryString.AppendLine("order by    MONPNT							                                                                                                                            ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //수질경보 설정정보 조회 (Dashboard)
        public DataTable SelectQualityWarningSettingForDashboard(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select		MONITOR_NO							    ");
            queryString.AppendLine("		    ,WQ_ITEM_CODE							");
            queryString.AppendLine("		    ,N1_TOP_LIMIT							");
            queryString.AppendLine("		    ,N1_LOW_LIMIT							");
            queryString.AppendLine("		    ,N2_TOP_LIMIT							");
            queryString.AppendLine("		    ,N2_LOW_LIMIT							");
            queryString.AppendLine("from		WQ_MONPNT_MAPPING						");
            queryString.AppendLine("where		MONITOR_NO	        = :1				");
            queryString.AppendLine("and		    WQ_ITEM_CODE    	= :2				");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["MONITOR_NO"];
            parameters[1].Value = (string)conditions["WQ_ITEM_CODE"];


            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //시계열 수질분석 데이터 조회 (Dashboard)
        public DataTable SelectTimeSeriesQualityDataForDashboard(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select        TAGNAME					                                		                ");
            queryString.AppendLine("              ,to_char(TIMESTAMP, 'mm-dd hh24:mi')   as TIMESTAMP				                ");
            queryString.AppendLine("              ,VALUE							                                                ");
            queryString.AppendLine("from          IF_GATHER_REALTIME							                                    ");
            queryString.AppendLine("where         TAGNAME   = :1							                                        ");
            queryString.AppendLine("and           TIMESTAMP between to_date(:2, 'yyyymmddhh24mi') and to_date(:3, 'yyyymmddhh24mi') ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["TAGNAME"];
            parameters[1].Value = (string)conditions["startDate"];
            parameters[2].Value = (string)conditions["endDate"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //최상위 수질계통 1개 조회
        public DataTable SelectTopQualityChainForDashboard(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          CHAIN_NO                ");
            queryString.AppendLine("                ,CHAIN_NM               ");
            queryString.AppendLine("from            WQ_QUALITY_CHAIN        ");
            queryString.AppendLine("where           rownum = 1              ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //수질경보범위 설정정보 입력 : shpark_2015-03-12
        public DataTable InsertQualityWarningSetting(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WQ_MONPNT_MAPPING (         ");
            queryString.AppendLine("            MONITOR_NO                  ");
            queryString.AppendLine("            ,WQ_ITEM_CODE               ");
            queryString.AppendLine("            ,N1_TOP_LIMIT               ");
            queryString.AppendLine("            ,N1_LOW_LIMIT               ");
            queryString.AppendLine("            ,N2_TOP_LIMIT               ");
            queryString.AppendLine("            ,N2_LOW_LIMIT               ");
            queryString.AppendLine("            ,N3_TOP_LIMIT               ");
            queryString.AppendLine("            ,N3_LOW_LIMIT               ");
            queryString.AppendLine("            ,ALARM_YN                   ");
            queryString.AppendLine("            ) values (                  ");
            queryString.AppendLine("            :1                          ");
            queryString.AppendLine("            ,:2                         ");
            queryString.AppendLine("            ,:3                         ");
            queryString.AppendLine("            ,:4                         ");
            queryString.AppendLine("            ,:5                         ");
            queryString.AppendLine("            ,:6                         ");
            queryString.AppendLine("            ,'5'                        ");
            queryString.AppendLine("            ,'5'                        ");
            queryString.AppendLine("            ,'Y'    )                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["MONITOR_NO"];
            parameters[1].Value = (string)conditions["WQ_ITEM_CODE"];
            parameters[2].Value = (string)conditions["N1_TOP_LIMIT"];
            parameters[3].Value = (string)conditions["N1_LOW_LIMIT"];
            parameters[4].Value = (string)conditions["N2_TOP_LIMIT"];
            parameters[5].Value = (string)conditions["N2_LOW_LIMIT"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //수질경보범위 설정정보 저장 : shpark_2015-03-12
        public DataTable UpdateQualityWarningSetting(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update      WQ_MONPNT_MAPPING           ");
            queryString.AppendLine("   set      N1_TOP_LIMIT    = :1        ");
            queryString.AppendLine("            ,N1_LOW_LIMIT   = :2        ");
            queryString.AppendLine("            ,N2_TOP_LIMIT   = :3        ");
            queryString.AppendLine("            ,N2_LOW_LIMIT   = :4        ");
            queryString.AppendLine(" where      MONITOR_NO      = :5        ");
            queryString.AppendLine("   and      WQ_ITEM_CODE    = :6        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
                 ,new OracleParameter("4", OracleDbType.Varchar2)
                 ,new OracleParameter("5", OracleDbType.Varchar2)
                 ,new OracleParameter("6", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["N1_TOP_LIMIT"];
            parameters[1].Value = (string)conditions["N1_LOW_LIMIT"];
            parameters[2].Value = (string)conditions["N2_TOP_LIMIT"];
            parameters[3].Value = (string)conditions["N2_LOW_LIMIT"];
            parameters[4].Value = (string)conditions["MONITOR_NO"];
            parameters[5].Value = (string)conditions["WQ_ITEM_CODE"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //수질경보범위 설정정보 삭제 : shpark_2015-03-12
        public DataTable DeleteQualityWarningSetting(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WQ_MONPNT_MAPPING           ");
            queryString.AppendLine(" where          MONITOR_NO      = :1        ");
            queryString.AppendLine("   and          WQ_ITEM_CODE    = :2        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["MONITOR_NO"];
            parameters[1].Value = (string)conditions["WQ_ITEM_CODE"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    }
}
