﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using WaterNet.WaterNetCore;
using Oracle.DataAccess.Client;

namespace WaterNet.WQ_Analysis.dao
{
    public class CommonAnalysisDao
    {
        private static CommonAnalysisDao dao = null;

        private CommonAnalysisDao()
        {
        }

        public static CommonAnalysisDao GetInstance()
        {
            if(dao == null)
            {
                dao = new CommonAnalysisDao();
            }

            return dao;
        }
        
        //블록정보 조회
        public DataTable SelectBlockList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      FTR_IDN							    ");
            queryString.AppendLine("            ,LOC_NAME							");
            queryString.AppendLine("from        CM_LOCATION							");
            queryString.AppendLine("where       FTR_CODE    = :1					");
            queryString.AppendLine("and         PLOC_CODE   = :2                    ");
            queryString.AppendLine("order by    LOC_NAME							");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["FTR_CODE"];
            parameters[1].Value = (string)conditions["PLOC_CODE"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    
        //공통코드 조회 (Combobox용)
        public DataTable SelectCommonCodeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          CODE                    ");
            queryString.AppendLine("                ,CODE_NAME              ");
            queryString.AppendLine("from            CM_CODE                 ");
            queryString.AppendLine("where           PCODE       = :1        ");
            queryString.AppendLine("order by        CODE_NAME               ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["PCODE"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }
    }
}
