﻿/**************************************************************************
 * 파일명   : frmAlertRangeSetting.cs
 * 작성자   : shpark
 * 작성일자 : 2015.02.23
 * 설명     : 수질감시지점관리 경보범위설정 팝업 폼
 * 변경이력 : 2015.02.23 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using WaterNet.WQ_Analysis.util;
using WaterNet.WQ_Analysis.work;
using EMFrame.log;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmAlertRangeSetting : Form
    {
        #region 1. 전역변수

        private RealtimeDataWork work = null;
        private Hashtable selectedMonpntInfo = null;
        private int comWQItemListIndex = 0;
        private int comAfterTypeIndex = 0;
        private string[] texAfterList;

        private bool compareTextBox = true;

        #endregion 전역변수


        #region 2. 초기화

        //생성자
        public frmAlertRangeSetting(Hashtable selectedMonpntInfo)
        {
            InitializeComponent();
            work = RealtimeDataWork.GetInstance();
            this.selectedMonpntInfo = selectedMonpntInfo;
        }

        //폼 로드
        private void frmAlertRangeSetting_Load(object sender, EventArgs e)
        {
            InitailizeWQItemComboBox();
            InitailizeAfterTypeComboBox();
            SelectMonpntMapping();

            InitializeBeforeType();

            texMonpnt.Text = selectedMonpntInfo["MONPNT"].ToString();
        }

        #region + 선택지점 수질항목 콤보박스 초기화

        private void InitailizeWQItemComboBox()
        {
            DataTable wqItemList = new DataTable("wqItemList");

            DataColumn itemCode = new DataColumn();
            itemCode.DataType = System.Type.GetType("System.String");
            itemCode.ColumnName = "CODE";
            wqItemList.Columns.Add(itemCode);

            DataColumn itemText = new DataColumn();
            itemText.DataType = System.Type.GetType("System.String");
            itemText.ColumnName = "TEXT";
            wqItemList.Columns.Add(itemText);

            if (!selectedMonpntInfo["TAG_ID_CL"].ToString().Equals("") )
            {
                DataRow wqItemRow1 = wqItemList.NewRow();
                wqItemRow1["CODE"] = "CL";
                wqItemRow1["TEXT"] = "잔류염소";
                wqItemList.Rows.Add(wqItemRow1);
            }
            if (!selectedMonpntInfo["TAG_ID_TB"].ToString().Equals(""))
            {
                DataRow wqItemRow2 = wqItemList.NewRow();
                wqItemRow2["CODE"] = "TB";
                wqItemRow2["TEXT"] = "탁도";
                wqItemList.Rows.Add(wqItemRow2);
            }
            if (!selectedMonpntInfo["TAG_ID_PH"].ToString().Equals(""))
            {
                DataRow wqItemRow3 = wqItemList.NewRow();
                wqItemRow3["CODE"] = "PH";
                wqItemRow3["TEXT"] = "pH";
                wqItemList.Rows.Add(wqItemRow3);
            }

            comWQItemList.DataSource = wqItemList;
            comWQItemList.ValueMember = "CODE";
            comWQItemList.DisplayMember = "TEXT";
        }

        #endregion 선택지점 수질항목 콤보박스 초기화

        #region + 변경설정 경보타입 콤보박스 초기화

        private void InitailizeAfterTypeComboBox()
        {
            DataTable typeAfterList = new DataTable("typeAfterList");

            DataColumn typeBeforCode = new DataColumn();
            typeBeforCode.DataType = System.Type.GetType("System.String");
            typeBeforCode.ColumnName = "CODE";
            typeAfterList.Columns.Add(typeBeforCode);

            DataColumn typeBeforText = new DataColumn();
            typeBeforText.DataType = System.Type.GetType("System.String");
            typeBeforText.ColumnName = "TEXT";
            typeAfterList.Columns.Add(typeBeforText);

            DataRow typeAfterRow1 = typeAfterList.NewRow();
            typeAfterRow1["CODE"] = "type1";
            typeAfterRow1["TEXT"] = "TYPE1";

            DataRow typeAfterRow2 = typeAfterList.NewRow();
            typeAfterRow2["CODE"] = "type2";
            typeAfterRow2["TEXT"] = "TYPE2";

            DataRow typeAfterRow3 = typeAfterList.NewRow();
            typeAfterRow3["CODE"] = "type3";
            typeAfterRow3["TEXT"] = "TYPE3";

            if (comWQItemList.SelectedValue.ToString().Equals("TB"))
            {
                typeAfterList.Rows.Add(typeAfterRow3);
            }
            else
            {
                typeAfterList.Rows.Add(typeAfterRow1);
                typeAfterList.Rows.Add(typeAfterRow2);
                typeAfterList.Rows.Add(typeAfterRow3);
            }

            comAfterType.DataSource = typeAfterList;
            comAfterType.ValueMember = "CODE";
            comAfterType.DisplayMember = "TEXT";
        }

        #endregion 변경설정 경보타입 콤보박스 초기화

        #endregion 초기화


        #region 3. 객체 이벤트

        //수질항목 콤보박스 엔터 이벤트
        private void comWQItemList_Enter(object sender, EventArgs e)
        {
            CompareBeforeAfterTextBox();        //현재 경보설정값과 변경 경보설정값을 비교한다.
            comWQItemListIndex = comWQItemList.SelectedIndex;
        }

        //수질항목 콤보박스 선택 변경 이벤트
        //선택된 수질항목에 해당하는 경보범위 설정정보를 조회한다.
        private void comWQItemList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                if (!compareTextBox)
                {
                    if (CommonUtils.ShowYesNoDialog("변경설정값을 저장하지 않았습니다.\n수질항목을 변경하시겠습니까?") == DialogResult.No)
                    {
                        comWQItemList.SelectedIndex = comWQItemListIndex;
                        return;
                    }
                }

                compareTextBox = true;

                InitailizeAfterTypeComboBox();      //변경설정값 콤보박스 초기화

                SelectMonpntMapping();      //수질항목 콤보박스의 선택 수질 설정값 조회

                InitializeBeforeType();     //현재설정값 경보타입 화면 초기화
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //변경설정값 경보타입 콤보박스 엔터 이벤트
        private void comAfterType_Enter(object sender, EventArgs e)
        {
            CompareBeforeAfterTextBox();        //현재설정값과 변경설정값의 텍스트박스 입력값을 비교한다.
            comAfterTypeIndex = comAfterType.SelectedIndex;     //콤보박스의 현재 선택 인덱스를 저장한다.
        }

        //변경설정값 경보타입 콤보박스 선택변경 이벤트
        //경보범위 설정값을 저장하지 않았을경우 확인 후 처리한다.
        private void comAfterType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                if (!compareTextBox)
                {
                    if (CommonUtils.ShowYesNoDialog("변경설정값을 저장하지 않았습니다.\n경보타입을 변경하시겠습니까?") == DialogResult.No)
                    {
                        comAfterType.SelectedIndex = comAfterTypeIndex;

                        //타입 변경 시 경보범위 숫자 입력 텍스트 박스의 값을 공백으로 변경한다.
                        SetTexAfterValue(texAfterList[0], texAfterList[1], texAfterList[2], texAfterList[3]); return;
                    }
                }

                compareTextBox = true;
                SetTexAfterValue(string.Empty, string.Empty, string.Empty, string.Empty);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //변경설정값 경보타입 콤보박스 선택값변경 이벤트
        //콤보박스의 선택 타입 값에 맞게 경보설정 타입을 보여준다.
        private void comAfterType_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (comAfterType.Items.Count == 0) return;

                if (comAfterType.Text.Equals("TYPE1"))
                {
                    picAfterType1.Visible = true;
                    picAfterType2.Visible = false;
                    picAfterType3.Visible = false;

                    texAfter1.Visible = true;
                    texAfter2.Visible = true;
                    texAfter3.Visible = true;
                    texAfter4.Visible = true;
                }
                else if (comAfterType.Text.Equals("TYPE2"))
                {
                    picAfterType1.Visible = false;
                    picAfterType2.Visible = true;
                    picAfterType3.Visible = false;

                    texAfter1.Visible = true;
                    texAfter2.Visible = true;
                    texAfter3.Visible = true;
                    texAfter4.Visible = false;
                }
                else
                {
                    picAfterType1.Visible = false;
                    picAfterType2.Visible = false;
                    picAfterType3.Visible = true;

                    texAfter1.Visible = false;
                    texAfter2.Visible = true;
                    texAfter3.Visible = true;
                    texAfter4.Visible = false;
                }

                texAfterList = new string[4];
                texAfterList[0] = texAfter1.Text;
                texAfterList[1] = texAfter2.Text;
                texAfterList[2] = texAfter3.Text;
                texAfterList[3] = texAfter4.Text;
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //설정값삭제 버튼 클릭 이벤트
        private void butDeleteAlertRange_Click(object sender, EventArgs e)
        {
            try
            {
                if (texBeforeType.Text.Equals("설정값 없음"))
                {
                    CommonUtils.ShowInformationMessage("삭제할 수 있는 설정값이 없습니다."); return;
                }

                if (CommonUtils.ShowYesNoDialog("설정되어있는 수질경보 설정값을 삭제하시겠습니까?") == DialogResult.No) return;

                Hashtable conditions = new Hashtable();

                conditions.Add("MONITOR_NO", selectedMonpntInfo["MONITOR_NO"].ToString());
                conditions.Add("WQ_ITEM_CODE", comWQItemList.SelectedValue.ToString());

                work.DeleteQualityWarningSetting(conditions);

                CommonUtils.ShowInformationMessage("정상적으로 처리되었습니다.");

                SelectMonpntMapping();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //기본값 버튼 클릭 이벤트 : 2015-03-04 - 기능 사용안함. 버튼 히든으로 변경.
        //선택된 수질항목에 맞는 타입의 설정값 입력 텍스트박스에 기본 경보수치를 입력한다.
        private void butDefaultAlertRange_Click(object sender, EventArgs e)
        {
            try
            {
                if (comWQItemList.Text.Equals("잔류염소") && comAfterType.Text.Equals("TYPE2"))
                {
                    SetTexAfterValue("4", "1", "0.1", string.Empty);
                }
                else if (comWQItemList.Text.Equals("pH") && comAfterType.Text.Equals("TYPE1"))
                {
                    SetTexAfterValue("8.5", "8.4", "6.0", "5.8");
                }
                else if (comWQItemList.Text.Equals("탁도") && comAfterType.Text.Equals("TYPE3"))
                {
                    SetTexAfterValue(string.Empty, "0.5", "0.3", string.Empty);
                }
                else
                {
                    CommonUtils.ShowInformationMessage("설정된 기본값이 없습니다.");
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //저장 버튼 클릭 이벤트
        private void butSaveAlertRange_Click(object sender, EventArgs e)
        {
            try
            {
                string[] values;    //선택된 경보타입에 따라 입력값을 검사하기위한 배열

                if (comAfterType.Text.Equals("TYPE1"))
                {
                    values = new string[4];
                    values[0] = texAfter1.Text;
                    values[1] = texAfter2.Text;
                    values[2] = texAfter3.Text;
                    values[3] = texAfter4.Text;
                }
                else if (comAfterType.Text.Equals("TYPE2"))
                {
                    values = new string[3];
                    values[0] = texAfter1.Text;
                    values[1] = texAfter2.Text;
                    values[2] = texAfter3.Text;
                }
                else
                {
                    values = new string[2];
                    values[0] = texAfter2.Text;
                    values[1] = texAfter3.Text;
                }

                foreach (string value in values)
                {
                    if (value.Equals(string.Empty))
                    {
                        CommonUtils.ShowInformationMessage("값을 입력 하십시오."); return;
                    }

                    if (!CommonUtils.IsNumber(value))
                    {
                        CommonUtils.ShowInformationMessage("숫자만 입력할 수 있습니다."); return;
                    }
                }

                if (!compareTextBox)
                {
                    CommonUtils.ShowInformationMessage("변경된 설정값이 없습니다."); return;
                } 

                for (int i = 0; i < values.Length - 1; i++)
                {
                    decimal num1 = Convert.ToDecimal(values[i]);
                    decimal num2 = Convert.ToDecimal(values[i + 1]);

                    if (num1 <= num2)
                    {
                        CommonUtils.ShowInformationMessage("상위 항목의 설정값은 하위 항목의 설정값보다 커야 합니다."); return;
                    }
                }

                if (CommonUtils.ShowYesNoDialog("수질경보 설정값을 저장하시겠습니까?") == DialogResult.No) return;

                Hashtable conditions = new Hashtable();

                conditions.Add("MONITOR_NO", selectedMonpntInfo["MONITOR_NO"].ToString());
                conditions.Add("WQ_ITEM_CODE", comWQItemList.SelectedValue.ToString());
 
                if (comAfterType.Text.Equals("TYPE3"))
                {
                    conditions.Add("N1_TOP_LIMIT", texAfter2.Text);
                    conditions.Add("N1_LOW_LIMIT", texAfter3.Text);
                    conditions.Add("N2_TOP_LIMIT", string.Empty);
                    conditions.Add("N2_LOW_LIMIT", string.Empty);
                }
                else
                {
                    conditions.Add("N1_TOP_LIMIT", texAfter1.Text);
                    conditions.Add("N1_LOW_LIMIT", texAfter2.Text);
                    conditions.Add("N2_TOP_LIMIT", texAfter3.Text);
                    conditions.Add("N2_LOW_LIMIT", texAfter4.Text);
                }

                work.SaveQualityWarningSetting(conditions);

                CommonUtils.ShowInformationMessage("정상적으로 처리되었습니다.");

                SelectMonpntMapping();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }            
        }

        //닫기 버튼 클릭 이벤트
        private void butSelectTagClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //폼 클로징 이벤트
        private void frmAlertRangeSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!compareTextBox)
            {
                if (CommonUtils.ShowYesNoDialog("변경설정값을 저장하지 않았습니다.\n현재 창을 닫으시겠습니까?") == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            } 

            this.Dispose();
        }

        #endregion 객체 이벤트


        #region 4. 상속
        #endregion 상속


        #region 5. 외부호출
        #endregion 외부호출


        #region 6. 내부사용

        //선택한 수질감시지점의 선택 수질 설정값을 조회한다.
        private void SelectMonpntMapping()
        {
            try
            {
                SetTexAfterValue(string.Empty, string.Empty, string.Empty, string.Empty);

                texBeforeType.Text = "설정값 없음";

                texBefore1.Text = string.Empty;
                texBefore2.Text = string.Empty;
                texBefore3.Text = string.Empty;
                texBefore4.Text = string.Empty;

                Hashtable selectConditions = new Hashtable();
                selectConditions.Add("MONITOR_NO", selectedMonpntInfo["MONITOR_NO"].ToString());
                selectConditions.Add("WQ_ITEM_CODE", comWQItemList.SelectedValue.ToString());

                DataTable selectWQSetting = work.SelectRealtimeQualityStateForSetting(selectConditions);

                if (selectWQSetting.Rows.Count != 0)
                {
                    int count = 0;

                    for (int i = 0; i < selectWQSetting.Columns.Count; i++)
                    {
                        //경보타입을 설정하기 위해 조회된 값을 검사한다.
                        if (!selectWQSetting.Rows[0][i].ToString().Equals(string.Empty)) count += 1;
                    }

                    if (count == 6)
                    {
                        texBeforeType.Text = "TYPE1";

                        texBefore1.Text = selectWQSetting.Rows[0]["N1_TOP_LIMIT"].ToString();
                        texBefore2.Text = selectWQSetting.Rows[0]["N1_LOW_LIMIT"].ToString();
                        texBefore3.Text = selectWQSetting.Rows[0]["N2_TOP_LIMIT"].ToString();
                        texBefore4.Text = selectWQSetting.Rows[0]["N2_LOW_LIMIT"].ToString();
                    }
                    else if (count == 5)
                    {
                        texBeforeType.Text = "TYPE2";

                        texBefore1.Text = selectWQSetting.Rows[0]["N1_TOP_LIMIT"].ToString();
                        texBefore2.Text = selectWQSetting.Rows[0]["N1_LOW_LIMIT"].ToString();
                        texBefore3.Text = selectWQSetting.Rows[0]["N2_TOP_LIMIT"].ToString();
                    }
                    else if (count == 4)
                    {
                        texBeforeType.Text = "TYPE3";

                        texBefore2.Text = selectWQSetting.Rows[0]["N1_TOP_LIMIT"].ToString();
                        texBefore3.Text = selectWQSetting.Rows[0]["N1_LOW_LIMIT"].ToString();
                    }

                    SetTexAfterValue(texBefore1.Text, texBefore2.Text, texBefore3.Text, texBefore4.Text);
                }          
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //전달받은 네개의 설정값을 변경설정 수질값 입력 텍스트박스에 설정한다.
        private void SetTexAfterValue(string value1, string value2, string value3, string value4)
        {
            texAfter1.Text = value1;
            texAfter2.Text = value2;
            texAfter3.Text = value3;
            texAfter4.Text = value4;
        }

        //수질항목 콤보박스의 선택 수질을 확인한 후 현재설정값 경보타입 화면을 설정한다.
        private void InitializeBeforeType()
        {
            if (!compareTextBox) return;

            try
            {
                if (texBeforeType.Text.Equals("TYPE1"))
                {
                    picBeforeType1.Visible = true;
                    picBeforeType2.Visible = false;
                    picBeforeType3.Visible = false;

                    texBefore1.Visible = true;
                    texBefore2.Visible = true;
                    texBefore3.Visible = true;
                    texBefore4.Visible = true;
                }
                else if (texBeforeType.Text.Equals("TYPE2"))
                {
                    picBeforeType1.Visible = false;
                    picBeforeType2.Visible = true;
                    picBeforeType3.Visible = false;

                    texBefore1.Visible = true;
                    texBefore2.Visible = true;
                    texBefore3.Visible = true;
                    texBefore4.Visible = false;
                }
                else if (texBeforeType.Text.Equals("TYPE3"))
                {
                    picBeforeType1.Visible = false;
                    picBeforeType2.Visible = false;
                    picBeforeType3.Visible = true;

                    texBefore1.Visible = false;
                    texBefore2.Visible = true;
                    texBefore3.Visible = true;
                    texBefore4.Visible = false;
                }
                else
                {
                    picBeforeType1.Visible = false;
                    picBeforeType2.Visible = false;
                    picBeforeType3.Visible = false;

                    texBefore1.Visible = false;
                    texBefore2.Visible = false;
                    texBefore3.Visible = false;
                    texBefore4.Visible = false;
                }

                if (texBeforeType.Text.Equals("설정값 없음"))
                {
                    comAfterType.SelectedIndex = 0;
                }
                else
                {
                    comAfterType.SelectedValue = texBeforeType.Text.ToLower();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //현재설정값과 변경설정값의 텍스트박스 입력값을 비교한다.
        private void CompareBeforeAfterTextBox()
        {
            if (!texAfter1.Text.Equals(string.Empty) || !texAfter2.Text.Equals(string.Empty)
            || !texAfter3.Text.Equals(string.Empty) || !texAfter4.Text.Equals(string.Empty))
            {
                string[] texBeforeArray = new string[] { texBefore1.Text, texBefore2.Text, texBefore3.Text, texBefore4.Text };
                string[] texAfterArray = new string[] { texAfter1.Text, texAfter2.Text, texAfter3.Text, texAfter4.Text };

                for (int i = 0; i < texBeforeArray.Length; i++)
                {
                    if (!texBeforeArray[i].Equals(texAfterArray[i]))
                    {                 
                        compareTextBox = false;
                        return;
                    }
                }
            }

            compareTextBox = true;
        }

        #endregion 내부사용

    }
}