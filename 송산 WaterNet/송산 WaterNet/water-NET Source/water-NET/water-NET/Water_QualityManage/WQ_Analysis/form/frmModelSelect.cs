﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.work;
using System.Collections;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmModelSelect : Form
    {
        
        #region 1.전역번수

        private RechlorineSimulationWork work = null;

        #endregion

        #region 2.초기화

        public frmModelSelect()
        {
            InitializeComponent();
            InitializeSetting();
            work = RechlorineSimulationWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //폼 로드 시 실행
        private void frmModelSelect_Load(object sender, EventArgs e)
        {
            SelectQualityModelList();
        }

        //선택 버튼 클릭 시 실행
        private void butSelectModel_Click(object sender, EventArgs e)
        {
            if(griQualityModelList.Selected.Rows.Count > 0)
            {
                string inpNumber = CommonUtils.GetGridCellValue(griQualityModelList, "INP_NUMBER");
                string title = CommonUtils.GetGridCellValue(griQualityModelList, "TITLE");
                (Owner as frmRechlorineSimulation).SetModelData(inpNumber, title);
                this.Close();
            }
        }

        #endregion

        #region 4.상속구현

        #endregion
        
        #region 5.외부호출

        #endregion

        #region 6.내부사용

        //수질 모델리스트 조회
        private void SelectQualityModelList()
        {
            griQualityModelList.DataSource = work.SelectQualityModel(new Hashtable());

            if(griQualityModelList.Rows.Count > 0)
            {
                griQualityModelList.Rows[0].Selected = true;
                griQualityModelList.Rows[0].Activate();
            }
        }

        //Grid 초기화
        private void InitializeSetting()
        {
            UltraGridColumn modelListColumn;

            modelListColumn = griQualityModelList.DisplayLayout.Bands[0].Columns.Add();
            modelListColumn.Key = "INP_NUMBER";
            modelListColumn.Header.Caption = "모델번호";
            modelListColumn.CellActivation = Activation.NoEdit;
            modelListColumn.CellClickAction = CellClickAction.RowSelect;
            modelListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            modelListColumn.CellAppearance.TextHAlign = HAlign.Center;
            modelListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            modelListColumn.Width = 120;
            modelListColumn.Hidden = false;   //필드 보이기

            modelListColumn = griQualityModelList.DisplayLayout.Bands[0].Columns.Add();
            modelListColumn.Key = "TITLE";
            modelListColumn.Header.Caption = "모델명";
            modelListColumn.CellActivation = Activation.NoEdit;
            modelListColumn.CellClickAction = CellClickAction.RowSelect;
            modelListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            modelListColumn.CellAppearance.TextHAlign = HAlign.Left;
            modelListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            modelListColumn.Width = 200;
            modelListColumn.Hidden = false;   //필드 보이기

            modelListColumn = griQualityModelList.DisplayLayout.Bands[0].Columns.Add();
            modelListColumn.Key = "INS_DATE";
            modelListColumn.Header.Caption = "등록일";
            modelListColumn.CellActivation = Activation.NoEdit;
            modelListColumn.CellClickAction = CellClickAction.RowSelect;
            modelListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            modelListColumn.CellAppearance.TextHAlign = HAlign.Center;
            modelListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            modelListColumn.Width = 120;
            modelListColumn.Hidden = false;   //필드 보이기

            modelListColumn = griQualityModelList.DisplayLayout.Bands[0].Columns.Add();
            modelListColumn.Key = "REMARK";
            modelListColumn.Header.Caption = "비고";
            modelListColumn.CellActivation = Activation.NoEdit;
            modelListColumn.CellClickAction = CellClickAction.RowSelect;
            modelListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            modelListColumn.CellAppearance.TextHAlign = HAlign.Left;
            modelListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            modelListColumn.Width = 200;
            modelListColumn.Hidden = false;   //필드 보이기
        }

        #endregion
    }
}
