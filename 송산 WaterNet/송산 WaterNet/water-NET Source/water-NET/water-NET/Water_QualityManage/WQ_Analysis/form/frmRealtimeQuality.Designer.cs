﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmRealtimeQuality
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butSelectCheckpoint = new System.Windows.Forms.Button();
            this.texMonpnt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comQualityItem = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comMonpntGbn = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.griCheckpoint = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.butOpenRealtimeQuality = new System.Windows.Forms.Button();
            this.butOpenQualityChain = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griCheckpoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(792, 9);
            this.pictureBox2.TabIndex = 116;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 564);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(792, 9);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 555);
            this.picFrLeftM1.TabIndex = 118;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(782, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 555);
            this.pictureBox3.TabIndex = 119;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butSelectCheckpoint);
            this.groupBox1.Controls.Add(this.texMonpnt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comQualityItem);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comMonpntGbn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(772, 57);
            this.groupBox1.TabIndex = 120;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butSelectCheckpoint
            // 
            this.butSelectCheckpoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectCheckpoint.Location = new System.Drawing.Point(706, 19);
            this.butSelectCheckpoint.Name = "butSelectCheckpoint";
            this.butSelectCheckpoint.Size = new System.Drawing.Size(52, 23);
            this.butSelectCheckpoint.TabIndex = 149;
            this.butSelectCheckpoint.Text = "조회";
            this.butSelectCheckpoint.UseVisualStyleBackColor = true;
            this.butSelectCheckpoint.Click += new System.EventHandler(this.butSelectCheckpoint_Click);
            // 
            // texMonpnt
            // 
            this.texMonpnt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texMonpnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMonpnt.Location = new System.Drawing.Point(451, 20);
            this.texMonpnt.Name = "texMonpnt";
            this.texMonpnt.Size = new System.Drawing.Size(239, 21);
            this.texMonpnt.TabIndex = 135;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(403, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 134;
            this.label3.Text = "지점명";
            // 
            // comQualityItem
            // 
            this.comQualityItem.FormattingEnabled = true;
            this.comQualityItem.Location = new System.Drawing.Point(258, 20);
            this.comQualityItem.Name = "comQualityItem";
            this.comQualityItem.Size = new System.Drawing.Size(114, 20);
            this.comQualityItem.TabIndex = 133;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(199, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 132;
            this.label2.Text = "감시항목";
            // 
            // comMonpntGbn
            // 
            this.comMonpntGbn.FormattingEnabled = true;
            this.comMonpntGbn.Location = new System.Drawing.Point(72, 20);
            this.comMonpntGbn.Name = "comMonpntGbn";
            this.comMonpntGbn.Size = new System.Drawing.Size(114, 20);
            this.comMonpntGbn.TabIndex = 131;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 130;
            this.label1.Text = "지점구분";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 66);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(772, 9);
            this.pictureBox4.TabIndex = 121;
            this.pictureBox4.TabStop = false;
            // 
            // griCheckpoint
            // 
            this.griCheckpoint.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griCheckpoint.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griCheckpoint.DisplayLayout.MaxColScrollRegions = 1;
            this.griCheckpoint.DisplayLayout.MaxRowScrollRegions = 1;
            this.griCheckpoint.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griCheckpoint.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griCheckpoint.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griCheckpoint.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griCheckpoint.DisplayLayout.Override.CellPadding = 0;
            this.griCheckpoint.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griCheckpoint.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griCheckpoint.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griCheckpoint.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griCheckpoint.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griCheckpoint.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griCheckpoint.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griCheckpoint.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griCheckpoint.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griCheckpoint.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griCheckpoint.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griCheckpoint.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griCheckpoint.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griCheckpoint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griCheckpoint.Location = new System.Drawing.Point(10, 75);
            this.griCheckpoint.Name = "griCheckpoint";
            this.griCheckpoint.Size = new System.Drawing.Size(772, 453);
            this.griCheckpoint.TabIndex = 148;
            this.griCheckpoint.Text = "ultraGrid1";
            this.griCheckpoint.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.griCheckpoint_CellChange);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(10, 528);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(772, 10);
            this.pictureBox5.TabIndex = 149;
            this.pictureBox5.TabStop = false;
            // 
            // butOpenRealtimeQuality
            // 
            this.butOpenRealtimeQuality.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butOpenRealtimeQuality.Location = new System.Drawing.Point(660, 2);
            this.butOpenRealtimeQuality.Name = "butOpenRealtimeQuality";
            this.butOpenRealtimeQuality.Size = new System.Drawing.Size(113, 23);
            this.butOpenRealtimeQuality.TabIndex = 150;
            this.butOpenRealtimeQuality.Text = "실시간데이터조회";
            this.butOpenRealtimeQuality.UseVisualStyleBackColor = true;
            this.butOpenRealtimeQuality.Click += new System.EventHandler(this.butOpenRealtimeQuality_Click);
            // 
            // butOpenQualityChain
            // 
            this.butOpenQualityChain.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butOpenQualityChain.Location = new System.Drawing.Point(568, 2);
            this.butOpenQualityChain.Name = "butOpenQualityChain";
            this.butOpenQualityChain.Size = new System.Drawing.Size(90, 23);
            this.butOpenQualityChain.TabIndex = 151;
            this.butOpenQualityChain.Text = "수질계통조회";
            this.butOpenQualityChain.UseVisualStyleBackColor = true;
            this.butOpenQualityChain.Visible = false;
            this.butOpenQualityChain.Click += new System.EventHandler(this.butOpenQualityChain_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butOpenQualityChain);
            this.panel1.Controls.Add(this.butOpenRealtimeQuality);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(10, 538);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(772, 26);
            this.panel1.TabIndex = 152;
            // 
            // frmRealtimeQuality
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.griCheckpoint);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(808, 611);
            this.Name = "frmRealtimeQuality";
            this.Text = "실시간수질감시";
            this.Load += new System.EventHandler(this.frmRealtimeQuality_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRealtimeQuality_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griCheckpoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid griCheckpoint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comQualityItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comMonpntGbn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butSelectCheckpoint;
        private System.Windows.Forms.TextBox texMonpnt;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button butOpenRealtimeQuality;
        private System.Windows.Forms.Button butOpenQualityChain;
        private System.Windows.Forms.Panel panel1;
    }
}