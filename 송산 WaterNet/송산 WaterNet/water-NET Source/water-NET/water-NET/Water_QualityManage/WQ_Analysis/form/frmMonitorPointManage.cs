﻿/**************************************************************************
 * 파일명   : frmMonitorPointManage.cs
 * 작성자   : shpark
 * 작성일자 : 2014.12.09
 * 설명     : 수질감시지점관리 메인폼
 * 변경이력 : 2014.12.09 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.work;
using WaterNet.WQ_Analysis.util;
using EMFrame.log;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmMonitorPointManage : Form
    {
        #region 1. 전역변수

        private frmQualityAnalysisMap ownerForm = null;     //메인맵 메소드 사용을 위한 전역 변수
        private RealtimeDataWork work = null;

        #endregion 전역변수


        #region 2. 초기화

        public frmMonitorPointManage()
        {
            InitializeComponent();
            InitializeSetting();
            work = RealtimeDataWork.GetInstance();
        }

        //수질감시지점 목록 그리드 초기화
        private void InitializeSetting()
        {
            UltraGridColumn monitorPointColumn;

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MONITOR_NO";
            monitorPointColumn.Header.Caption = "지점일련번호";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 10;
            monitorPointColumn.Hidden = true;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MONPNT_GBN";
            monitorPointColumn.Header.Caption = "감시지점구분";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 10;
            monitorPointColumn.Hidden = true;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "SBLOCK_CODE";
            monitorPointColumn.Header.Caption = "소블록코드";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 10;
            monitorPointColumn.Hidden = true;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MONPNT";
            monitorPointColumn.Header.Caption = "감시지점명";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Left;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 300;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "SELECT_DATE";
            monitorPointColumn.Header.Caption = "선정일자";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 100;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "TAG_ID_CL";
            monitorPointColumn.Header.Caption = "잔류염소 Tag";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 95;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "TAG_ID_TB";
            monitorPointColumn.Header.Caption = "탁도 Tag";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 95;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "TAG_ID_PH";
            monitorPointColumn.Header.Caption = "pH Tag";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 95;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "TAG_ID_TE";
            monitorPointColumn.Header.Caption = "수온 Tag";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 95;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "TAG_ID_CU";
            monitorPointColumn.Header.Caption = "전기전도도 Tag";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 95;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "NODE_ID";
            monitorPointColumn.Header.Caption = "절점 ID";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 10;
            monitorPointColumn.Hidden = true;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MAP_X";
            monitorPointColumn.Header.Caption = "X 좌표";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Right;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 100;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MAP_Y";
            monitorPointColumn.Header.Caption = "Y 좌표";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Right;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 100;
            monitorPointColumn.Hidden = false;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MONITOR_YN";
            monitorPointColumn.Header.Caption = "표시여부";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 67;
            monitorPointColumn.Hidden = false;   //필드 보이기
        }

        //콤보박스 초기화
        private void InitializeComboBox()
        {
            # region 수질감시지점 콤보박스 초기화

            DataTable monitorYNList = new DataTable("comboList");

            DataColumn code1 = new DataColumn();
            code1.DataType = System.Type.GetType("System.String");
            code1.ColumnName = "CODE";
            monitorYNList.Columns.Add(code1);

            DataColumn text1 = new DataColumn();
            text1.DataType = System.Type.GetType("System.String");
            text1.ColumnName = "TEXT";
            monitorYNList.Columns.Add(text1);

            DataRow changeRow1 = monitorYNList.NewRow();
            changeRow1["CODE"] = "";
            changeRow1["TEXT"] = "선택";
            monitorYNList.Rows.Add(changeRow1);

            DataRow changeRow2 = monitorYNList.NewRow();
            changeRow2["CODE"] = "Y";
            changeRow2["TEXT"] = "Y";
            monitorYNList.Rows.Add(changeRow2);

            DataRow changeRow3 = monitorYNList.NewRow();
            changeRow3["CODE"] = "N";
            changeRow3["TEXT"] = "N";
            monitorYNList.Rows.Add(changeRow3);

            comChangeMonitorYN.DataSource = monitorYNList;
            comChangeMonitorYN.ValueMember = "CODE";
            comChangeMonitorYN.DisplayMember = "TEXT";

            #endregion 수질감시지점 콤보박스 초기화

            # region 검색조건 콤보박스 초기화

            DataTable selectMonitorYNList = new DataTable("comboList");

            DataColumn code = new DataColumn();
            code.DataType = System.Type.GetType("System.String");
            code.ColumnName = "CODE";
            selectMonitorYNList.Columns.Add(code);

            DataColumn text = new DataColumn();
            text.DataType = System.Type.GetType("System.String");
            text.ColumnName = "TEXT";
            selectMonitorYNList.Columns.Add(text);

            DataRow selectRow1 = selectMonitorYNList.NewRow();
            selectRow1["CODE"] = "";
            selectRow1["TEXT"] = "전체";
            selectMonitorYNList.Rows.Add(selectRow1);

            DataRow selectRow2 = selectMonitorYNList.NewRow();
            selectRow2["CODE"] = "Y";
            selectRow2["TEXT"] = "Y";
            selectMonitorYNList.Rows.Add(selectRow2);

            DataRow selectRow3 = selectMonitorYNList.NewRow();
            selectRow3["CODE"] = "N";
            selectRow3["TEXT"] = "N";
            selectMonitorYNList.Rows.Add(selectRow3);

            comSelectMonitorYN.DataSource = selectMonitorYNList;
            comSelectMonitorYN.ValueMember = "CODE";
            comSelectMonitorYN.DisplayMember = "TEXT";

            #endregion 검색조건 콤보박스 초기화
        }

        #endregion 초기화


        #region 3. 객체 이벤트

        #region + 일반 이벤트

        //폼 로드 이벤트
        private void frmMonitorPointManage_Load(object sender, EventArgs e)
        {
            ownerForm = (this.Owner as frmQualityAnalysisMap);      //자신의 부모Form 변수 할당
            InitializeComboBox();       //검색조건, 수질감시지점 상세정보 콤보박스 초기화
        }

        //폼 클로즈 이벤트
        private void frmMonitorPointManage_FormClosed(object sender, FormClosedEventArgs e)
        {
            ownerForm.CloseFormAction("frmMonitorPointManage");
        }

        //수질감시지점 그리드 행 선택 변경 이벤트
        private void griMonitorPointList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                texMonitorPointNM.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "MONPNT");
                texMapX.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "MAP_X");
                texMapY.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "MAP_Y");
                texCLTag.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "TAG_ID_CL");
                texTBTag.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "TAG_ID_TB");
                texPHTag.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "TAG_ID_PH");
                texTETag.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "TAG_ID_TE");
                texCUTag.Text = CommonUtils.GetGridCellValue(griMonitorPointList, "TAG_ID_CU");
                comChangeMonitorYN.SelectedValue = CommonUtils.GetGridCellValue(griMonitorPointList, "MONITOR_YN");
            }
            catch(Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //수질감시지점 그리드 로우 더블클릭 이벤트 : 선택 지점으로 맵을 이동시킨다.
        private void griMonitorPointList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //기능 사용 안함 : 2015-01-27_shpark
            //ownerForm.MoveFocusAt("지점정보", "NAMES = '" + e.Row.Cells["MONPNT"].Value + "'");
        }

        //검색조건 표시여부 콤보박스 선택 변경 이벤트
        private void comSelectMonitorYN_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectMonitorPointList();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //수질감시지점 세부정보 표출여부 콤보박스 선택 변경 이벤트
        private void comChangeMonitorYN_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griMonitorPointList, "MONITOR_YN", (sender as ComboBox).SelectedValue.ToString());
        }

        //제목 검색 텍스트 엔터키 입력 이벤트
        private void texMonpnt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == '\r')
                {
                    SelectMonitorPointList();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        #endregion 일반 이벤트

        #region + 버튼 이벤트

        //조회 버튼 클릭 이벤트
        private void butSelectMonitorPoint_Click(object sender, EventArgs e)
        {
            try
            {
                SelectMonitorPointList();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //저장 버튼 클릭 이벤트
        private void butMonitorPointSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //validation 정보 (사이즈|공백허용|숫자여부)
                Hashtable validationSet = new Hashtable();
                validationSet.Add("MONPNT", "400|N|N");
                validationSet.Add("MAP_X", "60|N|N");
                validationSet.Add("MAP_Y", "60|N|N");
                validationSet.Add("MONITOR_YN", "1|N|N");

                if (CommonUtils.SaveGridData(griMonitorPointList, validationSet))
                {
                    Hashtable saveConditions = new Hashtable();
                    saveConditions.Add("monitorPointManageData", griMonitorPointList.DataSource as DataTable);

                    work.SaveMonitorPointData(saveConditions);

                    CommonUtils.ShowInformationMessage("정상적으로 처리되었습니다.");

                    string selectedMonitorNO = griMonitorPointList.Selected.Rows[0].Cells["MONITOR_NO"].Value.ToString();

                    SelectMonitorPointList();

                    foreach (UltraGridRow row in griMonitorPointList.Rows)
                    {
                        if (row.Cells["MONITOR_NO"].Value.ToString().Equals(selectedMonitorNO))
                        {
                            row.Activate();
                            row.Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        
        //추가 버튼 클릭 이벤트
        private void butAddMonitorPoint_Click(object sender, EventArgs e)
        {
            try
            {
                string monitor_NO = CommonUtils.GetSerialNumber("MP"); //수질감시지점 관리번호 생성
                CommonUtils.AddGridRow(griMonitorPointList); //그리드에 빈 로우 추가
                CommonUtils.SetGridCellValue(griMonitorPointList, "MONITOR_NO", monitor_NO); //감시지점 일련번호 입력
                CommonUtils.SetGridCellValue(griMonitorPointList, "SELECT_DATE", DateTime.Now.ToString("yyyy-MM-dd")); //선정일자 입력
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //삭제 버튼 클릭 이벤트
        private void butDeleteMonitorPoint_Click(object sender, EventArgs e)
        {
            try
            {
                Hashtable deleteData = null;

                if ((deleteData = CommonUtils.DeleteGridRow(griMonitorPointList, "실시간 수실감시지점 정보를 삭제하시겠습니까?")) != null)
                {
                    Hashtable deleteCondition = new Hashtable();
                    deleteCondition.Add("MONITOR_NO", deleteData["MONITOR_NO"].ToString());

                    work.DeleteMonitorPointData(deleteCondition);

                    if (griMonitorPointList.Selected.Rows.Count == 0)      //모두 삭제되었을때 하단 Textbox 초기화
                    {
                        texMonitorPointNM.Text = "";
                        texMapX.Text = "";
                        texMapY.Text = "";
                        texCLTag.Text = "";
                        texTBTag.Text = "";
                        texPHTag.Text = "";
                        texTETag.Text = "";
                        texCUTag.Text = "";
                        comChangeMonitorYN.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //잔류염소 Tag선택 버튼 클릭 이벤트
        private void butSelectCLTag_Click(object sender, EventArgs e)
        {
            OpenSelectWQTagPopup("CLI", texCLTag);
        }

        //탁도 Tag선택 버튼 클릭 이벤트
        private void butSelectTBTag_Click(object sender, EventArgs e)
        {
            OpenSelectWQTagPopup("TBI", texTBTag);
        }

        //pH Tag선택 버튼 클릭 이벤트
        private void butSelectPHTag_Click(object sender, EventArgs e)
        {
            OpenSelectWQTagPopup("PHI", texPHTag);
        }

        //온도 Tag선택 버튼 클릭 이벤트
        private void butSelectTETag_Click(object sender, EventArgs e)
        {
            OpenSelectWQTagPopup("TEI", texTETag);
        }

        //전기전도도 Tag선택 버튼 클릭 이벤트
        private void butSelectCUTag_Click(object sender, EventArgs e)
        {
            OpenSelectWQTagPopup("CUI", texCUTag);
        }

        //경보범위설정 버튼 클릭 이벤트 : 2015-02-23_shpark
        private void butSetAlertRange_Click(object sender, EventArgs e)
        {
            if (griMonitorPointList.Rows.Count < 1)
            {
                CommonUtils.ShowInformationMessage("경보범위를 설정할 수 있는 감시지점이 없습니다."); return;
            }

            try
            {
                Hashtable selectedMonpntInfo = new Hashtable();     //선택된 감시지점의 지점일련변호, 지점명, 수질태그를 저장한다.
                selectedMonpntInfo.Add("MONITOR_NO", griMonitorPointList.Selected.Rows[0].Cells["MONITOR_NO"].Value);
                selectedMonpntInfo.Add("MONPNT", griMonitorPointList.Selected.Rows[0].Cells["MONPNT"].Value);
                selectedMonpntInfo.Add("TAG_ID_CL", griMonitorPointList.Selected.Rows[0].Cells["TAG_ID_CL"].Value);
                selectedMonpntInfo.Add("TAG_ID_TB", griMonitorPointList.Selected.Rows[0].Cells["TAG_ID_TB"].Value);
                selectedMonpntInfo.Add("TAG_ID_PH", griMonitorPointList.Selected.Rows[0].Cells["TAG_ID_PH"].Value);

                if (selectedMonpntInfo["MONITOR_NO"].ToString().Equals(""))
                {
                    CommonUtils.ShowInformationMessage("선택된 수질감시지점이 없습니다.");
                    return;
                }

                if (selectedMonpntInfo["TAG_ID_CL"].ToString().Equals("") &&
                    selectedMonpntInfo["TAG_ID_TB"].ToString().Equals("") &&
                    selectedMonpntInfo["TAG_ID_PH"].ToString().Equals(""))
                {
                    CommonUtils.ShowInformationMessage("경보범위를 설정할 수 있는 수질 항목이 없습니다.\n수질 태그를 등록하세요.");
                    return;
                }

                frmAlertRangeSetting alertRangeForm = new frmAlertRangeSetting(selectedMonpntInfo);
                alertRangeForm.Owner = this;
                alertRangeForm.ShowDialog();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }


        #endregion 버튼 이벤트

        #region + 텍스트 수정 이벤트

        //감시지점명 텍스트박스 텍스트 변경 이벤트
        private void texMonitorPointNM_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "MONPNT", (sender as TextBox).Text);
        }

        //X 좌표 텍스트박스 텍스트 변경 이벤트
        private void texMapX_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "MAP_X", (sender as TextBox).Text);
        }

        //Y 좌표 텍스트박스 텍스트 변경 이벤트
        private void texMapY_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "MAP_Y", (sender as TextBox).Text);
        }

        //잔류염소 Tag 텍스트박스 텍스트 변경 이벤트
        private void texCLTag_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "TAG_ID_CL", (sender as TextBox).Text);
        }

        //탁도 Tag 텍스트박스 텍스트 변경 이벤트
        private void texTBTag_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "TAG_ID_TB", (sender as TextBox).Text);
        }

        //pH Tag 텍스트박스 텍스트 변경 이벤트
        private void texPHTag_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "TAG_ID_PH", (sender as TextBox).Text);
        }

        //수온 Tag 텍스트박스 텍스트 변경 이벤트
        private void texTETag_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "TAG_ID_TE", (sender as TextBox).Text);
        }

        //전기전도도 Tag 텍스트박스 텍스트 변경 이벤트
        private void texCUTag_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griMonitorPointList, "TAG_ID_CU", (sender as TextBox).Text);
        }

        #endregion 텍스트 수정 이벤트

        #endregion 객체 이벤트


        #region 4. 상속
        #endregion 상속


        #region 5. 외부호출
        #endregion 외부호출


        #region 6. 내부사용

        //수질관리지점 목록을 조회한다.
        private void SelectMonitorPointList()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable selectConditions = new Hashtable();

                selectConditions.Add("MONITOR_YN", comSelectMonitorYN.SelectedValue.ToString());
                selectConditions.Add("MONPNT", texMonpnt.Text);

                griMonitorPointList.DataSource = work.SelectMainRealtimeCheckpointList(selectConditions);

                CommonUtils.SelectFirstGridRow(griMonitorPointList);    
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //수질항목별 태그를 조회하는 팝업창을 호출한다.
        private void OpenSelectWQTagPopup(string tagType, Control wqTextBox)
        {
            if (griMonitorPointList.Rows.Count < 1)
            {
                CommonUtils.ShowInformationMessage("수질태그를 등록할 수 있는 감시지점이 없습니다."); return;
            }

            try
            {
                Hashtable wqTypeConditions = new Hashtable();       //수질타입과 수질명을 저장한다.
                wqTypeConditions.Add("TAG_GBN", tagType);

                frmMonitorPointTagSelect selectForm = new frmMonitorPointTagSelect(wqTextBox, wqTypeConditions);
                selectForm.Owner = this;
                selectForm.ShowDialog();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        #endregion 내부사용

    }
}
