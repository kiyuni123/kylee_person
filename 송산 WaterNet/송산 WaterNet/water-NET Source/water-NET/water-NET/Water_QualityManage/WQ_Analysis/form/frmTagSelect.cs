﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WQ_Analysis.work;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmTagSelect : Form
    {
        #region 1.전역번수

        private RechlorineSimulationWork work = null;

        #endregion

        #region 2.초기화

        public frmTagSelect()
        {
            InitializeComponent();
            InitializeSetting();
            work = RechlorineSimulationWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //폼 최초 로드 시 실행
        private void frmTagSelect_Load(object sender, EventArgs e)
        {
            SelectTagList();
        }

        //선택 버튼 클릭 시 실행
        private void butSelectTag_Click(object sender, EventArgs e)
        {
            if (griQualityTagList.Selected.Rows.Count > 0)
            {
                string tagname = CommonUtils.GetGridCellValue(griQualityTagList, "TAGNAME");
                (Owner as frmRechlorineSimulation).SetTagData(tagname);
                this.Close();
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출
        #endregion

        #region 6.내부사용

        //Grid 초기화
        private void InitializeSetting()
        {
            UltraGridColumn tagListColumn;

            tagListColumn = griQualityTagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "TAGNAME";
            tagListColumn.Header.Caption = "TAG";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 150;
            tagListColumn.Hidden = false;   //필드 보이기

            tagListColumn = griQualityTagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "TAG_DESC";
            tagListColumn.Header.Caption = "명칭";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 400;
            tagListColumn.Hidden = false;   //필드 보이기
        }

        //TAG List 조회
        private void SelectTagList()
        {
            griQualityTagList.DataSource = work.SelectQualityTagList(new Hashtable());

            if (griQualityTagList.Rows.Count > 0)
            {
                griQualityTagList.Rows[0].Selected = true;
                griQualityTagList.Rows[0].Activate();
            }
        }

        #endregion
    }
}
