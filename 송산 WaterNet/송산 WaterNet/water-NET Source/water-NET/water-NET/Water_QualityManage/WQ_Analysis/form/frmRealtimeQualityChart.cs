﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.work;
using ChartFX.WinForms;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmRealtimeQualityChart : Form
    {
        #region 1.전역번수

        private RealtimeDataWork work = null;
        private Hashtable checkPointDatas = new Hashtable();        //실시간 데이터 표출지점 (key : tagname)
        Hashtable tmpYAxis = new Hashtable();                       //차트에 복수의 Y축을 표현하기 위해 각 수질항목별 AxisY 객체를 저장

        public frmChartSetting chartSettingForm = null;
        #endregion

        #region 2.초기화

        public frmRealtimeQualityChart()
        {
            InitializeComponent();
            work = RealtimeDataWork.GetInstance();

            datStartDate.Value = DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd");
            datEndDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }

        #endregion

        #region 3.객체 이벤트

        //조회버튼 클릭 시
        private void butSelectRealtimeQuality_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectRealtimeDataList();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //차트 이벤트 (더블클릭)
        private void chaRealtimeQuality_MouseDoubleClick(object sender, HitTestEventArgs e)
        {
            e = chaRealtimeQuality.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);
            if (e.HitType.ToString() == "Axis")
            {
                Axis hitAxis = (Axis)e.Object;

                if(chartSettingForm == null)
                {
                    chartSettingForm = new frmChartSetting();
                    chartSettingForm.SetAxisData(hitAxis);
                    chartSettingForm.Owner = this;
                    chartSettingForm.Show();
                }
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        //실시간 데이터 표출대상 지점정보를 세팅
        public void SetCheckpointList(Hashtable checkPointDatas)
        {
            this.checkPointDatas = checkPointDatas;
            InitializeGridColumnAndChart();

            SelectRealtimeDataList();   //세팅과 동시에 조회 실행
        }

        #endregion

        #region 6.내부사용

        //수질데이터 조회
        private void SelectRealtimeDataList()
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("checkpointList", checkPointDatas);
            conditions.Add("startDate", ((DateTime)datStartDate.Value).ToString("yyyyMMdd"));
            conditions.Add("endDate", ((DateTime)datEndDate.Value).ToString("yyyyMMdd"));

            DataTable result = work.SelectRealtimeDataList(conditions);

            griRealtimeQuality.DataSource = result;
            chaRealtimeQuality.DataSource = result;

            ChartSettingAfterSelect();      //데이터조회 후 차트 재설정
        }

        //동적 그리드 초기화 (넘겨받은 지점정보 기준)
        private void InitializeGridColumnAndChart()
        {
            //차트 초기화
            chaRealtimeQuality.LegendBox.MarginY = 1;
            chaRealtimeQuality.AxisX.DataFormat.Format = AxisFormat.DateTime;
            chaRealtimeQuality.AxisX.DataFormat.CustomFormat = "yyyy-MM-dd hh:mm";
            chaRealtimeQuality.AxisX.Staggered = true;
            chaRealtimeQuality.Data.Clear();
            chaRealtimeQuality.Data.Series = checkPointDatas.Count;

            //차트 X축 컬럼 설정
            chaRealtimeQuality.DataSourceSettings.Fields.Add(new FieldMap("TIMESTAMP", FieldUsage.Label));

            //기본 레전드박스를 비활성화 (커스텀 레전드를 추가하기위해 필요)
            LegendItemAttributes legendItem = new LegendItemAttributes();
            legendItem.Visible = false;
            chaRealtimeQuality.LegendBox.ItemAttributes[chaRealtimeQuality.Series] = legendItem;

            //그리드 초기화
            UltraGridColumn realtimeQualityColumn;

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "TIMESTAMP";
            realtimeQualityColumn.Header.Caption = "측정시간";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 130;
            realtimeQualityColumn.Hidden = false;   //필드 보이기

            foreach (string key in checkPointDatas.Keys)
            {
                Hashtable checkpointData = checkPointDatas[key] as Hashtable; 
                string columnCaption = checkpointData["MONPNT"] + " - " + checkpointData["ITEM_NM"] + (!"".Equals(checkpointData["UNIT"].ToString()) ? " (" + checkpointData["UNIT"] + ")" : "");

                realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
                realtimeQualityColumn.Key = key;
                realtimeQualityColumn.Header.Caption = columnCaption;
                realtimeQualityColumn.CellActivation = Activation.NoEdit;
                realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
                realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Right;
                realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
                realtimeQualityColumn.Width = 200;
                realtimeQualityColumn.Hidden = false;   //필드 보이기

                //항목에 따른 포맷 설정 (소수점 이하 표현)
                if ("CLI".Equals(checkpointData["ITEM"].ToString()))
                {
                    realtimeQualityColumn.Format = "F4";        //염소 (소수점 이하 4자리)
                }
                else if ("CUI".Equals(checkpointData["ITEM"].ToString()))
                {
                    realtimeQualityColumn.Format = "F1";        //전기전도도 (소수점 이하 1자리)
                }
                else if ("TBI".Equals(checkpointData["ITEM"].ToString()))
                {
                    realtimeQualityColumn.Format = "F3";        //탁도 (소수점 이하 3자리)
                }
                else if ("PHI".Equals(checkpointData["ITEM"].ToString()))
                {
                    realtimeQualityColumn.Format = "F1";        //pH (소수점 이하 1자리)
                }
                else if ("TEI".Equals(checkpointData["ITEM"].ToString()))
                {
                    realtimeQualityColumn.Format = "F1";        //온도 (소수점 이하 1자리)
                }

                //차트 Y축 추가
                string item = checkpointData["ITEM"].ToString();

                if(tmpYAxis[item] == null)
                {
                    AxisY newAxisY = new AxisY();
                    newAxisY.Visible = true;
                    newAxisY.Position = AxisPosition.Far;
                    newAxisY.AutoScale = true;
                    newAxisY.Title.Text = "" + checkpointData["ITEM_NM"] + (!"".Equals(checkpointData["UNIT"].ToString()) ? " (" + checkpointData["UNIT"] + ")" : "");
                    newAxisY.LabelsFormat.Decimals = 2;
                    newAxisY.Grids.Major.Visible = false;
                    newAxisY.Grids.Minor.Visible = false;

                    //항목에 따른 포맷 설정
                    if ("CLI".Equals(checkpointData["ITEM"].ToString()))
                    {
                        newAxisY.LabelsFormat.Decimals = 4;        //염소 (소수점 이하 4자리)
                    }
                    else if ("CUI".Equals(checkpointData["ITEM"].ToString()))
                    {
                        newAxisY.LabelsFormat.Decimals = 1;        //전기전도도 (소수점 이하 1자리)
                    }
                    else if ("TBI".Equals(checkpointData["ITEM"].ToString()))
                    {
                        newAxisY.LabelsFormat.Decimals = 3;        //탁도 (소수점 이하 3자리)
                    }
                    else if ("PHI".Equals(checkpointData["ITEM"].ToString()))
                    {
                        newAxisY.LabelsFormat.Decimals = 1;        //pH (소수점 이하 1자리)
                    }
                    else if ("TEI".Equals(checkpointData["ITEM"].ToString()))
                    {
                        newAxisY.LabelsFormat.Decimals = 1;        //온도 (소수점 이하 1자리)
                    }

                    tmpYAxis.Add(item, newAxisY);
                    chaRealtimeQuality.AxesY.Add(newAxisY);
                }

                //차트 데이터컬럼 설정
                chaRealtimeQuality.DataSourceSettings.Fields.Add(new FieldMap(key, FieldUsage.Value));

                //레전드박스 설정
                CustomLegendItem ci = new CustomLegendItem();
                ci.Text = checkpointData["MONPNT"] + "-" + checkpointData["ITEM_NM"];
                ci.MarkerShape = MarkerShape.HorizontalLine;
                chaRealtimeQuality.LegendBox.CustomItems.Add(ci);
            }
        }

        //데이터 조회 후 차트 재설정
        private void ChartSettingAfterSelect()
        {
            //데이터에 해당되는 시리즈가 어떻게 할당될지 알 수 없기때문에 조회한 후에 다시 설정해준다.
            foreach (SeriesAttributes sa in chaRealtimeQuality.Series)
            {
                if (sa.Text == null) continue;

                Hashtable checkpointData = checkPointDatas[sa.Text] as Hashtable;

                string item = checkpointData["ITEM"] as string;
                sa.AxisY = tmpYAxis[item] as AxisY;

                chaRealtimeQuality.Series.IndexOf(sa);
                chaRealtimeQuality.LegendBox.CustomItems[chaRealtimeQuality.Series.IndexOf(sa)].Color = sa.Color;
            }
        }

        #endregion
    }
}
