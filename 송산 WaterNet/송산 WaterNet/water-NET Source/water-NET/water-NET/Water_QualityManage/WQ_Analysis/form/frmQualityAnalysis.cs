﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.util;
using System.Collections;
using WaterNet.WQ_Analysis.work;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmQualityAnalysis : Form
    {
        #region 1.전역번수

        private string inpNumber = "";
        private string simulationNo = "";
        private RechlorineSimulationWork work = null;
        private CommonAnalysisWork cWork = null;
        private frmQualityAnalysisMap ownerForm = null;

        #endregion

        #region 2.초기화

        public frmQualityAnalysis()
        {
            InitializeComponent();
            InitializeSetting();

            work = RechlorineSimulationWork.GetInstance();
            cWork = CommonAnalysisWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //Form로드 시 실행
        private void frmQualityAnalysis_Load(object sender, EventArgs e)
        {
            ownerForm = (this.Owner as frmRechlorineSimulation).Owner as frmQualityAnalysisMap;     //지도Form 객체 할당

            //필터 수식콤보 할당
            DataTable filterFormula = new DataTable();
            filterFormula.Columns.Add("text", typeof(string));
            filterFormula.Columns.Add("value", typeof(string));

            DataRow row0 = filterFormula.NewRow();
            row0["text"] = "선택";
            row0["value"] = "";

            DataRow row1 = filterFormula.NewRow();
            row1["text"] = "보다 큰";
            row1["value"] = "<";

            DataRow row2 = filterFormula.NewRow();
            row2["text"] = "보다 작은";
            row2["value"] = ">";

            DataRow row3 = filterFormula.NewRow();
            row3["text"] = "이상인";
            row3["value"] = "<=";

            DataRow row4 = filterFormula.NewRow();
            row4["text"] = "이하인";
            row4["value"] = ">=";

            DataRow row5 = filterFormula.NewRow();
            row5["text"] = "인";
            row5["value"] = "=";

            filterFormula.Rows.Add(row0);
            filterFormula.Rows.Add(row1);
            filterFormula.Rows.Add(row2);
            filterFormula.Rows.Add(row3);
            filterFormula.Rows.Add(row4);
            filterFormula.Rows.Add(row5);

            comQualityFormula.ValueMember = "value";
            comQualityFormula.DisplayMember = "text";
            comQualityFormula.DataSource = filterFormula;
            comQualityFormula.SelectedIndex = 0;

            comFlowFormula.ValueMember = "value";
            comFlowFormula.DisplayMember = "text";
            comFlowFormula.DataSource = filterFormula.Copy();
            comFlowFormula.SelectedIndex = 0;
        }

        //Form이 닫길때 처리
        private void frmQualityAnalysis_FormClosed(object sender, FormClosedEventArgs e)
        {
            cWork.InitAnalysisResult();      //해석결과 초기화 (안하면 다음번 불렀을때 해석안하고 필터링 걸면 꼬임)

            frmRechlorineSimulation parentForm = (this.Owner as frmRechlorineSimulation);
            parentForm.UnloadQualityModel();
            parentForm.qualityAnalysisForm = null;
        }

        //grid row 변경 시 실행
        private void griRechlorinePosition_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (griRechlorinePosition.Rows.Count > 0)
            {
                texId.Text = CommonUtils.GetGridCellValue(griRechlorinePosition, "ID");
                texVolume.Text = CommonUtils.GetGridCellValue(griRechlorinePosition, "VOLUME");
                texPointNm.Text = CommonUtils.GetGridCellValue(griRechlorinePosition, "POINT_NM");
                texPositionRemark.Text = CommonUtils.GetGridCellValue(griRechlorinePosition, "REMARK");
            }
            else
            {
                texId.Text = "";
                texVolume.Text = "";
                texPointNm.Text = "";
                texPositionRemark.Text = "";
            }
        }

        //grid cell 더블클릭 시 실행
        private void griRechlorinePosition_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            if("ID".Equals(e.Cell.Column.Key))
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", inpNumber);
                conditions.Add("ID", e.Cell.Value.ToString());

                string layerNm = work.GetNodeLayerData(conditions);

                if(!"".Equals(layerNm))
                {
                    ownerForm.MoveFocusAt(layerNm, "ID = '" + e.Cell.Value + "'");
                }
            }
        }

        //ID Text 변경 시 실행
        private void texId_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griRechlorinePosition, "ID", (sender as TextBox).Text);
        }

        //주입량 Text 변경 시 실행
        private void texVolume_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griRechlorinePosition, "VOLUME", (sender as TextBox).Text);
        }

        //지점명 Text 변경 시 실행
        private void texPointNm_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griRechlorinePosition, "POINT_NM", (sender as TextBox).Text);
        }

        //비고 Text 변경 시 실행
        private void texPositionRemark_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griRechlorinePosition, "REMARK", (sender as TextBox).Text);
        }

        //추가버튼 클릭 시 실행
        private void butAddPosition_Click(object sender, EventArgs e)
        {
            Hashtable initValueSet = new Hashtable();
            initValueSet.Add("SIMULATION_NO", simulationNo);
            initValueSet.Add("INP_NUMBER", inpNumber);

            CommonUtils.AddGridRow(griRechlorinePosition, initValueSet);
        }

        //삭제버튼 클릭 시 실행
        private void butDeletePosition_Click(object sender, EventArgs e)
        {
            CommonUtils.DeleteGridRow(griRechlorinePosition, "");
        }

        //해석 실행 버튼 클릭 시 실행
        private void butRunAnalysis_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cheQuality.Checked && !cheFlow.Checked)
                {
                    CommonUtils.ShowInformationMessage("표출항목을 선택하십시오.");
                    return;
                }

                this.Cursor = Cursors.WaitCursor;

                //validation 정보 (사이즈|공백허용|숫자여부)
                Hashtable validationSet = new Hashtable();
                validationSet.Add("ID", "50|N|N");
                validationSet.Add("VOLUME", "10|N|Y");

                if (!CommonUtils.ValidationGridData(griRechlorinePosition, validationSet))
                {
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", inpNumber);
                conditions.Add("initQuality", this.griRechlorinePosition.DataSource as DataTable);
                conditions.Add("qualityBaseValue", texQualityBaseValue.Text);
                conditions.Add("qualityFormula", comQualityFormula.SelectedValue);
                conditions.Add("flowBaseValue", texFlowBaseValue.Text);
                conditions.Add("flowFormula", comFlowFormula.SelectedValue);

                Hashtable showConditions = new Hashtable();
                showConditions.Add("quality", cheQuality.Checked);
                showConditions.Add("flow", cheFlow.Checked);
                showConditions.Add("annotation", cheAnnotation.Checked);

                ownerForm.MapRendering(cWork.ExecuteQualityAnalysis(conditions), showConditions);

                //if(cheQuality.Checked == false)
                //{
                //    ownerForm.ChangeDiableAnalysisResult("node");
                //}

                //if (cheFlow.Checked == false)
                //{
                //    ownerForm.ChangeDiableAnalysisResult("link");
                //}
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //재염소지점 저장 버튼 클릭 시 실행
        private void butSaveRechlorinePosition_Click(object sender, EventArgs e)
        {
            try
            {
                if(griRechlorinePosition.Rows.Count < 1)
                {
                    return;
                }

                //validation 정보 (사이즈|공백허용|숫자여부)
                Hashtable validationSet = new Hashtable();
                validationSet.Add("ID", "50|N|N");
                validationSet.Add("VOLUME", "10|N|Y");
                validationSet.Add("POINT_NM", "400|N|N");
                validationSet.Add("REMARK", "400|Y|N");

                if (CommonUtils.ValidationGridData(griRechlorinePosition, validationSet))
                {
                    if (MessageBox.Show("기존 지점정보는 모두 삭제되고 신규로 생성됩니다.\n진행하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("SIMULATION_NO", simulationNo);
                conditions.Add("INP_NUMBER", inpNumber);
                conditions.Add("positionData", griRechlorinePosition.DataSource as DataTable);

                work.InsertRechlorinePositionListAfterDelete(conditions);
                (this.Owner as frmRechlorineSimulation).ReSelectRechlorineSimulationList(simulationNo);

                MessageBox.Show("정상적으로 처리되었습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //해석 필터 잔류염소 Textbox 엔터키 입력 시 실행
        private void texBaseValue_KeyUp(object sender, KeyEventArgs e)
        {
            if ("".Equals(texQualityBaseValue.Text) || "".Equals(comQualityFormula.SelectedValue.ToString()) || !cheQuality.Checked || !cWork.IsAnalysisModel()) return;

            if (e.KeyCode == Keys.Enter)    //엔터키 입력 시에만 동작
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texQualityBaseValue.Text, comQualityFormula.SelectedValue.ToString(), "quality");

                    if (filtedDatas.Count != 0)
                    {
                        Hashtable tmpTable = new Hashtable();
                        tmpTable.Add("quality", filtedDatas);

                        Hashtable showConditions = new Hashtable();
                        showConditions.Add("quality", cheQuality.Checked);
                        showConditions.Add("annotation", cheAnnotation.Checked);

                        ownerForm.MapRendering(tmpTable, showConditions);
                    }
                }
                catch (Exception ex)
                {
                    CommonUtils.ShowExceptionMessage(ex.Message);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        //해석 필터 유량 Textbox 엔터키 입력 시 실행
        private void texFlowBaseValue_KeyUp(object sender, KeyEventArgs e)
        {
            if ("".Equals(texFlowBaseValue.Text) || "".Equals(comFlowFormula.SelectedValue.ToString()) || !cWork.IsAnalysisModel()) return;

            if (e.KeyCode == Keys.Enter)    //엔터키 입력 시에만 동작
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texFlowBaseValue.Text, comFlowFormula.SelectedValue.ToString(), "flow");

                    if (filtedDatas.Count != 0)
                    {
                        Hashtable tmpTable = new Hashtable();
                        tmpTable.Add("flow", filtedDatas);

                        Hashtable showConditions = new Hashtable();
                        showConditions.Add("flow", cheFlow.Checked);
                        showConditions.Add("annotation", cheAnnotation.Checked);

                        ownerForm.MapRendering(tmpTable, showConditions);
                    }
                }
                catch (Exception ex)
                {
                    CommonUtils.ShowExceptionMessage(ex.Message);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        //해석 필터 잔류염소 Combobox 선택 변경 시 실행
        private void comFormula_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ("".Equals(texQualityBaseValue.Text) || "".Equals(comQualityFormula.SelectedValue.ToString()) || !cWork.IsAnalysisModel()) return;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texQualityBaseValue.Text, comQualityFormula.SelectedValue.ToString(), "quality");

                if (filtedDatas.Count != 0)
                {
                    Hashtable tmpTable = new Hashtable();
                    tmpTable.Add("quality", filtedDatas);

                    Hashtable showConditions = new Hashtable();
                    showConditions.Add("quality", cheQuality.Checked);
                    showConditions.Add("annotation", cheAnnotation.Checked);

                    ownerForm.MapRendering(tmpTable, showConditions);
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //해석 필터 유량 Combobox 선택 변경 시 실행
        private void comFlowFormula_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ("".Equals(texFlowBaseValue.Text) || "".Equals(comFlowFormula.SelectedValue.ToString()) || !cWork.IsAnalysisModel()) return;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texFlowBaseValue.Text, comFlowFormula.SelectedValue.ToString(), "flow");

                if (filtedDatas.Count != 0)
                {
                    Hashtable tmpTable = new Hashtable();
                    tmpTable.Add("flow", filtedDatas);

                    Hashtable showConditions = new Hashtable();
                    showConditions.Add("flow", cheFlow.Checked);
                    showConditions.Add("annotation", cheAnnotation.Checked);

                    ownerForm.MapRendering(tmpTable, showConditions);
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //잔류염소 표출 checkbox 변경 시 실행
        private void cheQuality_CheckedChanged(object sender, EventArgs e)
        {
            if (!cWork.IsAnalysisModel())     //해석이 실행되지 않았을 경우 이벤트 종료
            {
                //((CheckBox)sender).Checked = true;
                return;
            }

            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (((CheckBox)sender).Checked == true)
                {
                    //Render및 annotation을 활성화하기 위해 해석결과값을 다시 할당
                    //Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texQualityBaseValue.Text, comQualityFormula.SelectedValue.ToString(), "quality");

                    //if (filtedDatas.Count != 0)
                    //{
                    //    Hashtable tmpTable = new Hashtable();
                    //    tmpTable.Add("quality", filtedDatas);
                    //    ownerForm.ChangeEnableAnalysisResult("node", tmpTable);                  
                    //}

                    ownerForm.ChangeEnableAnalysisResult("node", cheAnnotation.Checked);

                    texQualityBaseValue.Enabled = true;
                    comQualityFormula.Enabled = true;
                }
                else if (((CheckBox)sender).Checked == false)
                {
                    ownerForm.ChangeDiableAnalysisResult("node");

                    texQualityBaseValue.Enabled = false;
                    comQualityFormula.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //유량 표출 checkbox 변경 시 실행
        private void cheFlow_CheckedChanged(object sender, EventArgs e)
        {
            if (!cWork.IsAnalysisModel())        //해석이 실행되지 않았을 경우 이벤트 종료
            {
                //((CheckBox)sender).Checked = true;
                return;
            }

            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (((CheckBox)sender).Checked == true)
                {
                    //Render및 annotation을 활성화하기 위해 해석결과값을 다시 할당
                    //Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texFlowBaseValue.Text, comFlowFormula.SelectedValue.ToString(), "flow");

                    //if (filtedDatas.Count != 0)
                    //{
                    //    Hashtable tmpTable = new Hashtable();
                    //    tmpTable.Add("flow", filtedDatas);
                    //    ownerForm.ChangeEnableAnalysisResult("link", tmpTable);
                    //}

                    ownerForm.ChangeEnableAnalysisResult("link", cheAnnotation.Checked);

                    texFlowBaseValue.Enabled = true;
                    comFlowFormula.Enabled = true;
                }
                else if (((CheckBox)sender).Checked == false)
                {
                    ownerForm.ChangeDiableAnalysisResult("link");

                    texFlowBaseValue.Enabled = false;
                    comFlowFormula.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //값표출 checkbox 변경 시 실행
        private void cheAnnotation_CheckedChanged(object sender, EventArgs e)
        {
            if (!cWork.IsAnalysisModel()) return;       //해석이 실행되지 않았을 경우 이벤트 종료

            try
            {
                this.Cursor = Cursors.WaitCursor;
                ownerForm.ShowAnnotation(((CheckBox)sender).Checked, cheQuality.Checked, cheFlow.Checked);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        //모의 설정값 Setting
        public void SetSimulationDatas(DataTable positionData, string simulationNo, string simulationNm, string inpNumber)
        {
            this.inpNumber = inpNumber;
            this.simulationNo = simulationNo;
            this.groPosition.Text = simulationNm;
            griRechlorinePosition.DataSource = positionData;

            if(positionData.Rows.Count > 0)
            {
                griRechlorinePosition.Rows[0].Selected = true;
                griRechlorinePosition.Rows[0].Activate();
            }
        }

        //지도의 선택지점 ID,XY좌표 할당
        public void SetObjectId(string id, string mapX, string mapY)
        {
            if(griRechlorinePosition.Selected.Rows.Count > 0)
            {
                CommonUtils.SetGridCellValue(griRechlorinePosition, "MAP_X", mapX);     //X좌표 할당
                CommonUtils.SetGridCellValue(griRechlorinePosition, "MAP_Y", mapY);     //Y좌표 할당
                texId.Text = id;
            }
        }

        #endregion

        #region 6.내부사용

        private void InitializeSetting()
        {
            //재염소지점 Grid 초기화
            UltraGridColumn positiontColumn;

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "SIMULATION_NO";
            positiontColumn.Header.Caption = "모의번호";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "INP_NUMBER";
            positiontColumn.Header.Caption = "관련모델번호";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "ID";
            positiontColumn.Header.Caption = "절점ID";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "POINT_NM";
            positiontColumn.Header.Caption = "지점명";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "";
            positiontColumn.Header.Caption = "이전투입량";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Right;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "VOLUME";
            positiontColumn.Header.Caption = "주입량(mg/ℓ)";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Right;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "TAGNAME";
            positiontColumn.Header.Caption = "TAG";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "REMARK";
            positiontColumn.Header.Caption = "비고";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "MAP_X";
            positiontColumn.Header.Caption = "X좌표";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "MAP_Y";
            positiontColumn.Header.Caption = "Y좌표";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = true;   //필드 보이기
        }

        #endregion
    }
}
