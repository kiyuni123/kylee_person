﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WQ_Analysis.work;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmRealtimeQuality : Form
    {
        #region 1.전역번수

        RealtimeDataWork work = null;

        #endregion

        #region 2.초기화

        public frmRealtimeQuality()
        {
            InitializeComponent();
            InitializeSetting();
            work = RealtimeDataWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //Form 최초로딩 시
        private void frmRealtimeQuality_Load(object sender, EventArgs e)
        {
            //검색조건 초기화에 필요한 데이터 조회
            Hashtable initDatas = work.InitRealtimeDataList();

            if (initDatas["monitorGbn"] != null)
            {
                comMonpntGbn.DataSource = initDatas["monitorGbn"] as DataTable;
                comMonpntGbn.DisplayMember = "TEXT";
                comMonpntGbn.ValueMember = "CODE";

                comMonpntGbn.SelectedIndex = 0;
            }

            if (initDatas["qualityItem"] != null)
            {
                comQualityItem.DataSource = initDatas["qualityItem"] as DataTable;
                comQualityItem.DisplayMember = "TEXT";
                comQualityItem.ValueMember = "CODE";

                comQualityItem.SelectedIndex = 0;
            }

            SelectCheckpointList();
        }

        //수질감시지점 리스트 조회
        private void butSelectCheckpoint_Click(object sender, EventArgs e)
        {
            SelectCheckpointList();
        }

        //실시간 수질감시 데이터 팝업 호출
        private void butOpenRealtimeQuality_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable checkpointDatas = new Hashtable();

                foreach (UltraGridRow row in griCheckpoint.Rows)
                {
                    if (bool.Parse(row.GetCellValue("CHK").ToString()))
                    {
                        Hashtable checkpointData = new Hashtable();
                        checkpointData.Add("MONITOR_NO", row.GetCellValue("MONITOR_NO").ToString());
                        checkpointData.Add("TAGNAME", row.GetCellValue("TAGNAME").ToString());
                        checkpointData.Add("MONPNT", row.GetCellValue("MONPNT").ToString());
                        checkpointData.Add("ITEM", row.GetCellValue("ITEM").ToString());
                        checkpointData.Add("ITEM_NM", row.GetCellValue("ITEM_NM").ToString());
                        checkpointData.Add("UNIT", CommonUtils.GetQualityUnit(row.GetCellValue("ITEM").ToString()));

                        checkpointDatas.Add(row.GetCellValue("MONITOR_NO").ToString() + row.GetCellValue("ITEM").ToString(), checkpointData);
                    }
                }

                if (checkpointDatas.Count != 0)
                {
                    frmRealtimeQualityChart qualityChartForm = new frmRealtimeQualityChart();
                    qualityChartForm.Owner = this;
                    qualityChartForm.SetCheckpointList(checkpointDatas);     //선택된 지점 정보 할당
                    qualityChartForm.Show();
                }
                else
                {
                    MessageBox.Show("지점을 Check후에 실행하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //계통도 선택 화면 호출
        private void butOpenQualityChain_Click(object sender, EventArgs e)
        {
            frmQualityChainSelect qualityChainForm = new frmQualityChainSelect();
            qualityChainForm.Owner = this;
            qualityChainForm.Show();
        }

        //grid 내 checkbox 클릭 시 이벤트
        private void griCheckpoint_CellChange(object sender, CellEventArgs e)
        {
            //TAG가 비어있지 않은경우만 활성화
            if ("".Equals(griCheckpoint.Rows[e.Cell.Row.Index].Cells["TAGNAME"].Value.ToString().Trim()))
            {
                e.Cell.Value = false;
            }
        }

        //Form close 시
        private void frmRealtimeQuality_FormClosing(object sender, FormClosingEventArgs e)
        {
            (this.Owner as frmQualityAnalysisMap).CloseFormAction("frmRealtimeQuality");
        }
        
        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        #endregion

        #region 6.내부사용

        //지점 리스트 조회
        private void SelectCheckpointList()
        {
            Hashtable conditions = new Hashtable();

            if (!"".Equals(comMonpntGbn.SelectedValue.ToString()))
            {
                conditions.Add("MONPNT_GBN", comMonpntGbn.SelectedValue.ToString());
            }

            if (!"".Equals(comQualityItem.SelectedValue.ToString()))
            {
                conditions.Add("ITEM", comQualityItem.SelectedValue.ToString());
            }

            if (!"".Equals(texMonpnt.Text.Trim()))
            {
                conditions.Add("MONPNT", texMonpnt.Text);
            }

            griCheckpoint.DataSource = work.SelectCheckpointList(conditions);
        }

        //Grid 초기화
        private void InitializeSetting()
        {
            //유량계지점 그리드 초기화
            UltraGridColumn checkpointColumn;

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "CHK";
            checkpointColumn.Header.Caption = "";
            checkpointColumn.CellActivation = Activation.AllowEdit;
            checkpointColumn.CellClickAction = CellClickAction.Edit;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 10;
            checkpointColumn.Hidden = false;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "MONITOR_NO";
            checkpointColumn.Header.Caption = "지점일련번호";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 100;
            checkpointColumn.Hidden = true;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "MONPNT_GBN";
            checkpointColumn.Header.Caption = "지점구분";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Left;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 149;
            checkpointColumn.Hidden = true;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "MONPNT_GBN_NM";
            checkpointColumn.Header.Caption = "지점구분";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 100;
            checkpointColumn.Hidden = false;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "MONPNT";
            checkpointColumn.Header.Caption = "지점명";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Left;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 200;
            checkpointColumn.Hidden = false;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "ITEM";
            checkpointColumn.Header.Caption = "감시항목";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Left;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Hidden = true;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "ITEM_NM";
            checkpointColumn.Header.Caption = "감시항목";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 100;
            checkpointColumn.Hidden = false;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "TAGNAME";
            checkpointColumn.Header.Caption = "TAG";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Left;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 50;
            checkpointColumn.Hidden = false;   //필드 보이기

            checkpointColumn = griCheckpoint.DisplayLayout.Bands[0].Columns.Add();
            checkpointColumn.Key = "DESCRIPTION";
            checkpointColumn.Header.Caption = "설명";
            checkpointColumn.CellActivation = Activation.NoEdit;
            checkpointColumn.CellClickAction = CellClickAction.RowSelect;
            checkpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            checkpointColumn.CellAppearance.TextHAlign = HAlign.Left;
            checkpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            checkpointColumn.Width = 200;
            checkpointColumn.Hidden = false;   //필드 보이기
        }

        #endregion
    }
}
