﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmMonitorPointManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butSelectMonitorPoint = new System.Windows.Forms.Button();
            this.texMonpnt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comSelectMonitorYN = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.butSetAlertRange = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.texMapX = new System.Windows.Forms.TextBox();
            this.comChangeMonitorYN = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.butSelectCUTag = new System.Windows.Forms.Button();
            this.texCUTag = new System.Windows.Forms.TextBox();
            this.butSelectTETag = new System.Windows.Forms.Button();
            this.butSelectPHTag = new System.Windows.Forms.Button();
            this.butSelectTBTag = new System.Windows.Forms.Button();
            this.texTETag = new System.Windows.Forms.TextBox();
            this.texPHTag = new System.Windows.Forms.TextBox();
            this.texMapY = new System.Windows.Forms.TextBox();
            this.butSelectCLTag = new System.Windows.Forms.Button();
            this.texTBTag = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.texCLTag = new System.Windows.Forms.TextBox();
            this.texMonitorPointNM = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butDeleteMonitorPoint = new System.Windows.Forms.Button();
            this.butSaveMonitorPoint = new System.Windows.Forms.Button();
            this.butAddMonitorPoint = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.griMonitorPointList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griMonitorPointList)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(1174, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 544);
            this.pictureBox3.TabIndex = 123;
            this.pictureBox3.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 544);
            this.picFrLeftM1.TabIndex = 122;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 553);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1184, 9);
            this.pictureBox1.TabIndex = 121;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1184, 9);
            this.pictureBox2.TabIndex = 120;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butSelectMonitorPoint);
            this.groupBox1.Controls.Add(this.texMonpnt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comSelectMonitorYN);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1164, 57);
            this.groupBox1.TabIndex = 126;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butSelectMonitorPoint
            // 
            this.butSelectMonitorPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectMonitorPoint.Location = new System.Drawing.Point(1116, 20);
            this.butSelectMonitorPoint.Name = "butSelectMonitorPoint";
            this.butSelectMonitorPoint.Size = new System.Drawing.Size(42, 23);
            this.butSelectMonitorPoint.TabIndex = 167;
            this.butSelectMonitorPoint.Text = "조회";
            this.butSelectMonitorPoint.UseVisualStyleBackColor = true;
            this.butSelectMonitorPoint.Click += new System.EventHandler(this.butSelectMonitorPoint_Click);
            // 
            // texMonpnt
            // 
            this.texMonpnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMonpnt.Location = new System.Drawing.Point(221, 20);
            this.texMonpnt.Name = "texMonpnt";
            this.texMonpnt.Size = new System.Drawing.Size(350, 21);
            this.texMonpnt.TabIndex = 135;
            this.texMonpnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.texMonpnt_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 134;
            this.label3.Text = "감시지점명";
            // 
            // comSelectMonitorYN
            // 
            this.comSelectMonitorYN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comSelectMonitorYN.FormattingEnabled = true;
            this.comSelectMonitorYN.Location = new System.Drawing.Point(72, 20);
            this.comSelectMonitorYN.Name = "comSelectMonitorYN";
            this.comSelectMonitorYN.Size = new System.Drawing.Size(60, 20);
            this.comSelectMonitorYN.TabIndex = 131;
            this.comSelectMonitorYN.SelectedIndexChanged += new System.EventHandler(this.comSelectMonitorYN_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 130;
            this.label1.Text = "표시여부";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.butSetAlertRange);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.texMapX);
            this.groupBox2.Controls.Add(this.comChangeMonitorYN);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.butSelectCUTag);
            this.groupBox2.Controls.Add(this.texCUTag);
            this.groupBox2.Controls.Add(this.butSelectTETag);
            this.groupBox2.Controls.Add(this.butSelectPHTag);
            this.groupBox2.Controls.Add(this.butSelectTBTag);
            this.groupBox2.Controls.Add(this.texTETag);
            this.groupBox2.Controls.Add(this.texPHTag);
            this.groupBox2.Controls.Add(this.texMapY);
            this.groupBox2.Controls.Add(this.butSelectCLTag);
            this.groupBox2.Controls.Add(this.texTBTag);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.texCLTag);
            this.groupBox2.Controls.Add(this.texMonitorPointNM);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(10, 423);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1164, 130);
            this.groupBox2.TabIndex = 169;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "수질감시지점 세부정보";
            // 
            // butSetAlertRange
            // 
            this.butSetAlertRange.Location = new System.Drawing.Point(746, 97);
            this.butSetAlertRange.Name = "butSetAlertRange";
            this.butSetAlertRange.Size = new System.Drawing.Size(86, 23);
            this.butSetAlertRange.TabIndex = 181;
            this.butSetAlertRange.Text = "경보범위설정";
            this.butSetAlertRange.UseVisualStyleBackColor = true;
            this.butSetAlertRange.Click += new System.EventHandler(this.butSetAlertRange_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(838, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 180;
            this.label10.Text = "Y 좌표";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(617, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 179;
            this.label9.Text = "X 좌표";
            // 
            // texMapX
            // 
            this.texMapX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMapX.Location = new System.Drawing.Point(664, 27);
            this.texMapX.Name = "texMapX";
            this.texMapX.ReadOnly = true;
            this.texMapX.Size = new System.Drawing.Size(120, 21);
            this.texMapX.TabIndex = 178;
            this.texMapX.TextChanged += new System.EventHandler(this.texMapX_TextChanged);
            // 
            // comChangeMonitorYN
            // 
            this.comChangeMonitorYN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comChangeMonitorYN.FormattingEnabled = true;
            this.comChangeMonitorYN.Location = new System.Drawing.Point(664, 99);
            this.comChangeMonitorYN.Name = "comChangeMonitorYN";
            this.comChangeMonitorYN.Size = new System.Drawing.Size(60, 20);
            this.comChangeMonitorYN.TabIndex = 177;
            this.comChangeMonitorYN.SelectedIndexChanged += new System.EventHandler(this.comChangeMonitorYN_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(605, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 176;
            this.label8.Text = "표시여부";
            // 
            // butSelectCUTag
            // 
            this.butSelectCUTag.Location = new System.Drawing.Point(488, 98);
            this.butSelectCUTag.Name = "butSelectCUTag";
            this.butSelectCUTag.Size = new System.Drawing.Size(62, 23);
            this.butSelectCUTag.TabIndex = 175;
            this.butSelectCUTag.Text = "Tag선택";
            this.butSelectCUTag.UseVisualStyleBackColor = true;
            this.butSelectCUTag.Click += new System.EventHandler(this.butSelectCUTag_Click);
            // 
            // texCUTag
            // 
            this.texCUTag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texCUTag.Location = new System.Drawing.Point(382, 99);
            this.texCUTag.Name = "texCUTag";
            this.texCUTag.ReadOnly = true;
            this.texCUTag.Size = new System.Drawing.Size(100, 21);
            this.texCUTag.TabIndex = 174;
            this.texCUTag.TextChanged += new System.EventHandler(this.texCUTag_TextChanged);
            // 
            // butSelectTETag
            // 
            this.butSelectTETag.Location = new System.Drawing.Point(197, 98);
            this.butSelectTETag.Name = "butSelectTETag";
            this.butSelectTETag.Size = new System.Drawing.Size(62, 23);
            this.butSelectTETag.TabIndex = 173;
            this.butSelectTETag.Text = "Tag선택";
            this.butSelectTETag.UseVisualStyleBackColor = true;
            this.butSelectTETag.Click += new System.EventHandler(this.butSelectTETag_Click);
            // 
            // butSelectPHTag
            // 
            this.butSelectPHTag.Location = new System.Drawing.Point(770, 62);
            this.butSelectPHTag.Name = "butSelectPHTag";
            this.butSelectPHTag.Size = new System.Drawing.Size(62, 23);
            this.butSelectPHTag.TabIndex = 172;
            this.butSelectPHTag.Text = "Tag선택";
            this.butSelectPHTag.UseVisualStyleBackColor = true;
            this.butSelectPHTag.Click += new System.EventHandler(this.butSelectPHTag_Click);
            // 
            // butSelectTBTag
            // 
            this.butSelectTBTag.Location = new System.Drawing.Point(488, 62);
            this.butSelectTBTag.Name = "butSelectTBTag";
            this.butSelectTBTag.Size = new System.Drawing.Size(62, 23);
            this.butSelectTBTag.TabIndex = 171;
            this.butSelectTBTag.Text = "Tag선택";
            this.butSelectTBTag.UseVisualStyleBackColor = true;
            this.butSelectTBTag.Click += new System.EventHandler(this.butSelectTBTag_Click);
            // 
            // texTETag
            // 
            this.texTETag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texTETag.Location = new System.Drawing.Point(91, 99);
            this.texTETag.Name = "texTETag";
            this.texTETag.ReadOnly = true;
            this.texTETag.Size = new System.Drawing.Size(100, 21);
            this.texTETag.TabIndex = 170;
            this.texTETag.TextChanged += new System.EventHandler(this.texTETag_TextChanged);
            // 
            // texPHTag
            // 
            this.texPHTag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texPHTag.Location = new System.Drawing.Point(664, 63);
            this.texPHTag.Name = "texPHTag";
            this.texPHTag.ReadOnly = true;
            this.texPHTag.Size = new System.Drawing.Size(100, 21);
            this.texPHTag.TabIndex = 169;
            this.texPHTag.TextChanged += new System.EventHandler(this.texPHTag_TextChanged);
            // 
            // texMapY
            // 
            this.texMapY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMapY.Location = new System.Drawing.Point(885, 27);
            this.texMapY.Name = "texMapY";
            this.texMapY.ReadOnly = true;
            this.texMapY.Size = new System.Drawing.Size(120, 21);
            this.texMapY.TabIndex = 168;
            this.texMapY.TextChanged += new System.EventHandler(this.texMapY_TextChanged);
            // 
            // butSelectCLTag
            // 
            this.butSelectCLTag.Location = new System.Drawing.Point(197, 62);
            this.butSelectCLTag.Name = "butSelectCLTag";
            this.butSelectCLTag.Size = new System.Drawing.Size(62, 23);
            this.butSelectCLTag.TabIndex = 167;
            this.butSelectCLTag.Text = "Tag선택";
            this.butSelectCLTag.UseVisualStyleBackColor = true;
            this.butSelectCLTag.Click += new System.EventHandler(this.butSelectCLTag_Click);
            // 
            // texTBTag
            // 
            this.texTBTag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texTBTag.Location = new System.Drawing.Point(382, 63);
            this.texTBTag.Name = "texTBTag";
            this.texTBTag.ReadOnly = true;
            this.texTBTag.Size = new System.Drawing.Size(100, 21);
            this.texTBTag.TabIndex = 10;
            this.texTBTag.TextChanged += new System.EventHandler(this.texTBTag_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(612, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "pH Tag";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(285, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "전기전도도 Tag";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(321, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "탁도 Tag";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "수온 Tag";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 67);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 12);
            this.label11.TabIndex = 5;
            this.label11.Text = "잔류염소 Tag";
            // 
            // texCLTag
            // 
            this.texCLTag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texCLTag.Location = new System.Drawing.Point(91, 63);
            this.texCLTag.Name = "texCLTag";
            this.texCLTag.ReadOnly = true;
            this.texCLTag.Size = new System.Drawing.Size(100, 21);
            this.texCLTag.TabIndex = 3;
            this.texCLTag.TextChanged += new System.EventHandler(this.texCLTag_TextChanged);
            // 
            // texMonitorPointNM
            // 
            this.texMonitorPointNM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMonitorPointNM.Location = new System.Drawing.Point(91, 27);
            this.texMonitorPointNM.Name = "texMonitorPointNM";
            this.texMonitorPointNM.Size = new System.Drawing.Size(391, 21);
            this.texMonitorPointNM.TabIndex = 1;
            this.texMonitorPointNM.TextChanged += new System.EventHandler(this.texMonitorPointNM_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "감시지점명";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 418);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1164, 5);
            this.pictureBox4.TabIndex = 170;
            this.pictureBox4.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butDeleteMonitorPoint);
            this.panel1.Controls.Add(this.butSaveMonitorPoint);
            this.panel1.Controls.Add(this.butAddMonitorPoint);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(10, 393);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1164, 25);
            this.panel1.TabIndex = 171;
            // 
            // butDeleteMonitorPoint
            // 
            this.butDeleteMonitorPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butDeleteMonitorPoint.Location = new System.Drawing.Point(1122, 1);
            this.butDeleteMonitorPoint.Name = "butDeleteMonitorPoint";
            this.butDeleteMonitorPoint.Size = new System.Drawing.Size(42, 23);
            this.butDeleteMonitorPoint.TabIndex = 169;
            this.butDeleteMonitorPoint.Text = "삭제";
            this.butDeleteMonitorPoint.UseVisualStyleBackColor = true;
            this.butDeleteMonitorPoint.Click += new System.EventHandler(this.butDeleteMonitorPoint_Click);
            // 
            // butSaveMonitorPoint
            // 
            this.butSaveMonitorPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSaveMonitorPoint.Location = new System.Drawing.Point(1074, 1);
            this.butSaveMonitorPoint.Name = "butSaveMonitorPoint";
            this.butSaveMonitorPoint.Size = new System.Drawing.Size(42, 23);
            this.butSaveMonitorPoint.TabIndex = 168;
            this.butSaveMonitorPoint.Text = "저장";
            this.butSaveMonitorPoint.UseVisualStyleBackColor = true;
            this.butSaveMonitorPoint.Click += new System.EventHandler(this.butMonitorPointSave_Click);
            // 
            // butAddMonitorPoint
            // 
            this.butAddMonitorPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butAddMonitorPoint.Location = new System.Drawing.Point(1026, 1);
            this.butAddMonitorPoint.Name = "butAddMonitorPoint";
            this.butAddMonitorPoint.Size = new System.Drawing.Size(42, 23);
            this.butAddMonitorPoint.TabIndex = 167;
            this.butAddMonitorPoint.Text = "추가";
            this.butAddMonitorPoint.UseVisualStyleBackColor = true;
            this.butAddMonitorPoint.Click += new System.EventHandler(this.butAddMonitorPoint_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(10, 384);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(1164, 9);
            this.pictureBox5.TabIndex = 172;
            this.pictureBox5.TabStop = false;
            // 
            // griMonitorPointList
            // 
            this.griMonitorPointList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMonitorPointList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griMonitorPointList.DisplayLayout.MaxColScrollRegions = 1;
            this.griMonitorPointList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griMonitorPointList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griMonitorPointList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMonitorPointList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BorderColor = System.Drawing.Color.DarkGray;
            this.griMonitorPointList.DisplayLayout.Override.CellAppearance = appearance3;
            this.griMonitorPointList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griMonitorPointList.DisplayLayout.Override.CellPadding = 0;
            this.griMonitorPointList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griMonitorPointList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griMonitorPointList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griMonitorPointList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griMonitorPointList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance4.BorderColor = System.Drawing.Color.DarkGray;
            this.griMonitorPointList.DisplayLayout.Override.RowAppearance = appearance4;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griMonitorPointList.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griMonitorPointList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griMonitorPointList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griMonitorPointList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griMonitorPointList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griMonitorPointList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griMonitorPointList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griMonitorPointList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griMonitorPointList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griMonitorPointList.Location = new System.Drawing.Point(10, 66);
            this.griMonitorPointList.Name = "griMonitorPointList";
            this.griMonitorPointList.Size = new System.Drawing.Size(1164, 318);
            this.griMonitorPointList.TabIndex = 173;
            this.griMonitorPointList.Text = "ultraGrid1";
            this.griMonitorPointList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griMonitorPointList_DoubleClickRow);
            this.griMonitorPointList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griMonitorPointList_AfterSelectChange);
            // 
            // frmMonitorPointManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 562);
            this.Controls.Add(this.griMonitorPointList);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(1050, 450);
            this.Name = "frmMonitorPointManage";
            this.Text = "수질감지지점관리";
            this.Load += new System.EventHandler(this.frmMonitorPointManage_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMonitorPointManage_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griMonitorPointList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox texMonpnt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comSelectMonitorYN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butSelectMonitorPoint;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox texMapX;
        private System.Windows.Forms.ComboBox comChangeMonitorYN;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button butSelectCUTag;
        private System.Windows.Forms.TextBox texCUTag;
        private System.Windows.Forms.Button butSelectTETag;
        private System.Windows.Forms.Button butSelectPHTag;
        private System.Windows.Forms.Button butSelectTBTag;
        private System.Windows.Forms.TextBox texTETag;
        private System.Windows.Forms.TextBox texPHTag;
        public System.Windows.Forms.TextBox texMapY;
        private System.Windows.Forms.Button butSelectCLTag;
        private System.Windows.Forms.TextBox texTBTag;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox texCLTag;
        private System.Windows.Forms.TextBox texMonitorPointNM;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butDeleteMonitorPoint;
        private System.Windows.Forms.Button butSaveMonitorPoint;
        private System.Windows.Forms.Button butAddMonitorPoint;
        private System.Windows.Forms.PictureBox pictureBox5;
        private Infragistics.Win.UltraWinGrid.UltraGrid griMonitorPointList;
        private System.Windows.Forms.Button butSetAlertRange;
    }
}