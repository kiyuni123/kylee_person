﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmRechlorineSimulation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butQualityAnalysis = new System.Windows.Forms.Button();
            this.butSelectSimulation = new System.Windows.Forms.Button();
            this.texCondSimulationNm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.griRechlorineSimulation = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.texTitle = new System.Windows.Forms.TextBox();
            this.butAddSimulation = new System.Windows.Forms.Button();
            this.butModelSelect = new System.Windows.Forms.Button();
            this.butDeleteSimulation = new System.Windows.Forms.Button();
            this.butSaveSimulation = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.texRemark = new System.Windows.Forms.TextBox();
            this.texSimulationNm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.griRechlorinePosition = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butRegRtPoint = new System.Windows.Forms.Button();
            this.butTagSelect = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.texTagname = new System.Windows.Forms.TextBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griRechlorineSimulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griRechlorinePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(784, 9);
            this.pictureBox2.TabIndex = 117;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 535);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(784, 9);
            this.pictureBox1.TabIndex = 118;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 526);
            this.picFrLeftM1.TabIndex = 119;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(774, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 526);
            this.pictureBox3.TabIndex = 120;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butQualityAnalysis);
            this.groupBox1.Controls.Add(this.butSelectSimulation);
            this.groupBox1.Controls.Add(this.texCondSimulationNm);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(764, 57);
            this.groupBox1.TabIndex = 121;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butQualityAnalysis
            // 
            this.butQualityAnalysis.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butQualityAnalysis.Location = new System.Drawing.Point(608, 19);
            this.butQualityAnalysis.Name = "butQualityAnalysis";
            this.butQualityAnalysis.Size = new System.Drawing.Size(103, 23);
            this.butQualityAnalysis.TabIndex = 150;
            this.butQualityAnalysis.Text = "재염소지점모의";
            this.butQualityAnalysis.UseVisualStyleBackColor = true;
            this.butQualityAnalysis.Click += new System.EventHandler(this.butQualityAnalysis_Click);
            // 
            // butSelectSimulation
            // 
            this.butSelectSimulation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSelectSimulation.Location = new System.Drawing.Point(713, 19);
            this.butSelectSimulation.Name = "butSelectSimulation";
            this.butSelectSimulation.Size = new System.Drawing.Size(45, 23);
            this.butSelectSimulation.TabIndex = 149;
            this.butSelectSimulation.Text = "조회";
            this.butSelectSimulation.UseVisualStyleBackColor = true;
            this.butSelectSimulation.Click += new System.EventHandler(this.butSelectSimulation_Click);
            // 
            // texCondSimulationNm
            // 
            this.texCondSimulationNm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texCondSimulationNm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texCondSimulationNm.Location = new System.Drawing.Point(50, 22);
            this.texCondSimulationNm.Name = "texCondSimulationNm";
            this.texCondSimulationNm.Size = new System.Drawing.Size(239, 21);
            this.texCondSimulationNm.TabIndex = 135;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 134;
            this.label3.Text = "명칭";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 66);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(764, 9);
            this.pictureBox4.TabIndex = 122;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.griRechlorineSimulation);
            this.groupBox2.Controls.Add(this.pictureBox13);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.splitter1);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.pictureBox6);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(10, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(764, 460);
            this.groupBox2.TabIndex = 123;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "모의이력";
            // 
            // griRechlorineSimulation
            // 
            this.griRechlorineSimulation.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorineSimulation.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorineSimulation.DisplayLayout.MaxColScrollRegions = 1;
            this.griRechlorineSimulation.DisplayLayout.MaxRowScrollRegions = 1;
            this.griRechlorineSimulation.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorineSimulation.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorineSimulation.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorineSimulation.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griRechlorineSimulation.DisplayLayout.Override.CellPadding = 0;
            this.griRechlorineSimulation.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griRechlorineSimulation.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griRechlorineSimulation.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griRechlorineSimulation.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griRechlorineSimulation.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griRechlorineSimulation.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griRechlorineSimulation.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griRechlorineSimulation.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griRechlorineSimulation.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorineSimulation.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griRechlorineSimulation.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griRechlorineSimulation.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griRechlorineSimulation.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griRechlorineSimulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griRechlorineSimulation.Location = new System.Drawing.Point(13, 26);
            this.griRechlorineSimulation.Name = "griRechlorineSimulation";
            this.griRechlorineSimulation.Size = new System.Drawing.Size(738, 144);
            this.griRechlorineSimulation.TabIndex = 149;
            this.griRechlorineSimulation.Text = "ultraGrid1";
            this.griRechlorineSimulation.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griRechlorineSimulation_AfterSelectChange);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox13.Location = new System.Drawing.Point(13, 170);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(738, 9);
            this.pictureBox13.TabIndex = 164;
            this.pictureBox13.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.texTitle);
            this.panel1.Controls.Add(this.butAddSimulation);
            this.panel1.Controls.Add(this.butModelSelect);
            this.panel1.Controls.Add(this.butDeleteSimulation);
            this.panel1.Controls.Add(this.butSaveSimulation);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.texRemark);
            this.panel1.Controls.Add(this.texSimulationNm);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(13, 179);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(738, 85);
            this.panel1.TabIndex = 162;
            // 
            // texTitle
            // 
            this.texTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.texTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texTitle.Location = new System.Drawing.Point(378, 32);
            this.texTitle.Name = "texTitle";
            this.texTitle.ReadOnly = true;
            this.texTitle.Size = new System.Drawing.Size(239, 21);
            this.texTitle.TabIndex = 160;
            // 
            // butAddSimulation
            // 
            this.butAddSimulation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAddSimulation.Location = new System.Drawing.Point(589, 2);
            this.butAddSimulation.Name = "butAddSimulation";
            this.butAddSimulation.Size = new System.Drawing.Size(45, 23);
            this.butAddSimulation.TabIndex = 151;
            this.butAddSimulation.Text = "추가";
            this.butAddSimulation.UseVisualStyleBackColor = true;
            this.butAddSimulation.Click += new System.EventHandler(this.butAddSimulation_Click);
            // 
            // butModelSelect
            // 
            this.butModelSelect.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butModelSelect.Location = new System.Drawing.Point(623, 30);
            this.butModelSelect.Name = "butModelSelect";
            this.butModelSelect.Size = new System.Drawing.Size(61, 23);
            this.butModelSelect.TabIndex = 161;
            this.butModelSelect.Text = "모델선택";
            this.butModelSelect.UseVisualStyleBackColor = true;
            this.butModelSelect.Click += new System.EventHandler(this.butModelSelect_Click);
            // 
            // butDeleteSimulation
            // 
            this.butDeleteSimulation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butDeleteSimulation.Location = new System.Drawing.Point(636, 2);
            this.butDeleteSimulation.Name = "butDeleteSimulation";
            this.butDeleteSimulation.Size = new System.Drawing.Size(45, 23);
            this.butDeleteSimulation.TabIndex = 152;
            this.butDeleteSimulation.Text = "삭제";
            this.butDeleteSimulation.UseVisualStyleBackColor = true;
            this.butDeleteSimulation.Click += new System.EventHandler(this.butDeleteSimulation_Click);
            // 
            // butSaveSimulation
            // 
            this.butSaveSimulation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSaveSimulation.Location = new System.Drawing.Point(683, 2);
            this.butSaveSimulation.Name = "butSaveSimulation";
            this.butSaveSimulation.Size = new System.Drawing.Size(45, 23);
            this.butSaveSimulation.TabIndex = 153;
            this.butSaveSimulation.Text = "저장";
            this.butSaveSimulation.UseVisualStyleBackColor = true;
            this.butSaveSimulation.Click += new System.EventHandler(this.butSaveSimulation_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(319, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 159;
            this.label5.Text = "관련모델";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 153;
            this.label2.Text = "명칭";
            // 
            // texRemark
            // 
            this.texRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texRemark.Location = new System.Drawing.Point(48, 60);
            this.texRemark.Name = "texRemark";
            this.texRemark.Size = new System.Drawing.Size(569, 21);
            this.texRemark.TabIndex = 158;
            this.texRemark.TextChanged += new System.EventHandler(this.texRemark_TextChanged);
            // 
            // texSimulationNm
            // 
            this.texSimulationNm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texSimulationNm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texSimulationNm.Location = new System.Drawing.Point(48, 32);
            this.texSimulationNm.Name = "texSimulationNm";
            this.texSimulationNm.Size = new System.Drawing.Size(239, 21);
            this.texSimulationNm.TabIndex = 154;
            this.texSimulationNm.TextChanged += new System.EventHandler(this.texSimulationNm_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 157;
            this.label4.Text = "비고";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.Control;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(13, 264);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(738, 10);
            this.splitter1.TabIndex = 163;
            this.splitter1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.griRechlorinePosition);
            this.groupBox3.Controls.Add(this.pictureBox14);
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.Controls.Add(this.pictureBox9);
            this.groupBox3.Controls.Add(this.pictureBox10);
            this.groupBox3.Controls.Add(this.pictureBox11);
            this.groupBox3.Controls.Add(this.pictureBox12);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(13, 274);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(738, 174);
            this.groupBox3.TabIndex = 124;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "재염소지점";
            // 
            // griRechlorinePosition
            // 
            this.griRechlorinePosition.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorinePosition.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorinePosition.DisplayLayout.MaxColScrollRegions = 1;
            this.griRechlorinePosition.DisplayLayout.MaxRowScrollRegions = 1;
            this.griRechlorinePosition.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorinePosition.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorinePosition.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorinePosition.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griRechlorinePosition.DisplayLayout.Override.CellPadding = 0;
            this.griRechlorinePosition.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griRechlorinePosition.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griRechlorinePosition.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griRechlorinePosition.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griRechlorinePosition.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectorAppearance = appearance2;
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorinePosition.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griRechlorinePosition.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griRechlorinePosition.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griRechlorinePosition.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griRechlorinePosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griRechlorinePosition.Location = new System.Drawing.Point(13, 26);
            this.griRechlorinePosition.Name = "griRechlorinePosition";
            this.griRechlorinePosition.Size = new System.Drawing.Size(712, 98);
            this.griRechlorinePosition.TabIndex = 150;
            this.griRechlorinePosition.Text = "ultraGrid1";
            this.griRechlorinePosition.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griRechlorinePosition_AfterSelectChange);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox14.Location = new System.Drawing.Point(13, 124);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(712, 9);
            this.pictureBox14.TabIndex = 174;
            this.pictureBox14.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.butRegRtPoint);
            this.panel2.Controls.Add(this.butTagSelect);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.texTagname);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(13, 133);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(712, 29);
            this.panel2.TabIndex = 173;
            // 
            // butRegRtPoint
            // 
            this.butRegRtPoint.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butRegRtPoint.Location = new System.Drawing.Point(597, 4);
            this.butRegRtPoint.Name = "butRegRtPoint";
            this.butRegRtPoint.Size = new System.Drawing.Size(109, 23);
            this.butRegRtPoint.TabIndex = 172;
            this.butRegRtPoint.Text = "감시지점으로등록";
            this.butRegRtPoint.UseVisualStyleBackColor = true;
            this.butRegRtPoint.Click += new System.EventHandler(this.butRegRtPoint_Click);
            // 
            // butTagSelect
            // 
            this.butTagSelect.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butTagSelect.Location = new System.Drawing.Point(324, 4);
            this.butTagSelect.Name = "butTagSelect";
            this.butTagSelect.Size = new System.Drawing.Size(65, 23);
            this.butTagSelect.TabIndex = 162;
            this.butTagSelect.Text = "TAG선택";
            this.butTagSelect.UseVisualStyleBackColor = true;
            this.butTagSelect.Click += new System.EventHandler(this.butTagSelect_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 12);
            this.label8.TabIndex = 168;
            this.label8.Text = "TAG";
            // 
            // texTagname
            // 
            this.texTagname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texTagname.Location = new System.Drawing.Point(42, 5);
            this.texTagname.Name = "texTagname";
            this.texTagname.ReadOnly = true;
            this.texTagname.Size = new System.Drawing.Size(276, 21);
            this.texTagname.TabIndex = 169;
            this.texTagname.TextChanged += new System.EventHandler(this.texTagname_TextChanged);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox9.Location = new System.Drawing.Point(13, 162);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(712, 9);
            this.pictureBox9.TabIndex = 123;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox10.Location = new System.Drawing.Point(13, 17);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(712, 9);
            this.pictureBox10.TabIndex = 122;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox11.Location = new System.Drawing.Point(725, 17);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(10, 154);
            this.pictureBox11.TabIndex = 121;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox12.Location = new System.Drawing.Point(3, 17);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(10, 154);
            this.pictureBox12.TabIndex = 120;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox8.Location = new System.Drawing.Point(13, 448);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(738, 9);
            this.pictureBox8.TabIndex = 123;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox7.Location = new System.Drawing.Point(13, 17);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(738, 9);
            this.pictureBox7.TabIndex = 122;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox6.Location = new System.Drawing.Point(751, 17);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 440);
            this.pictureBox6.TabIndex = 121;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox5.Location = new System.Drawing.Point(3, 17);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(10, 440);
            this.pictureBox5.TabIndex = 120;
            this.pictureBox5.TabStop = false;
            // 
            // frmRechlorineSimulation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 544);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(800, 582);
            this.Name = "frmRechlorineSimulation";
            this.Text = "재염소처리모의";
            this.Load += new System.EventHandler(this.frmChlorineAnalysis_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRechlorineSimulation_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griRechlorineSimulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griRechlorinePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butSelectSimulation;
        private System.Windows.Forms.TextBox texCondSimulationNm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private Infragistics.Win.UltraWinGrid.UltraGrid griRechlorineSimulation;
        private Infragistics.Win.UltraWinGrid.UltraGrid griRechlorinePosition;
        private System.Windows.Forms.Button butQualityAnalysis;
        private System.Windows.Forms.Button butDeleteSimulation;
        private System.Windows.Forms.Button butAddSimulation;
        private System.Windows.Forms.Button butSaveSimulation;
        private System.Windows.Forms.TextBox texTitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox texRemark;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox texSimulationNm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button butModelSelect;
        private System.Windows.Forms.Button butTagSelect;
        private System.Windows.Forms.TextBox texTagname;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button butRegRtPoint;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Splitter splitter1;
    }
}