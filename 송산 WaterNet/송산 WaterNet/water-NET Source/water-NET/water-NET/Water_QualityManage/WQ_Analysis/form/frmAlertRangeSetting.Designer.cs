﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmAlertRangeSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlertRangeSetting));
            this.panel1 = new System.Windows.Forms.Panel();
            this.butDeleteAlertRange = new System.Windows.Forms.Button();
            this.butDefaultAlertRange = new System.Windows.Forms.Button();
            this.butCloseAlertRange = new System.Windows.Forms.Button();
            this.butSaveAlertRange = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.picBeforeType3 = new System.Windows.Forms.PictureBox();
            this.picBeforeType2 = new System.Windows.Forms.PictureBox();
            this.picBeforeType1 = new System.Windows.Forms.PictureBox();
            this.texBeforeType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.texBefore4 = new System.Windows.Forms.TextBox();
            this.texBefore3 = new System.Windows.Forms.TextBox();
            this.texBefore2 = new System.Windows.Forms.TextBox();
            this.texBefore1 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.picAfterType3 = new System.Windows.Forms.PictureBox();
            this.picAfterType2 = new System.Windows.Forms.PictureBox();
            this.picAfterType1 = new System.Windows.Forms.PictureBox();
            this.texAfter4 = new System.Windows.Forms.TextBox();
            this.texAfter3 = new System.Windows.Forms.TextBox();
            this.texAfter2 = new System.Windows.Forms.TextBox();
            this.texAfter1 = new System.Windows.Forms.TextBox();
            this.comAfterType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comWQItemList = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.texMonpnt = new System.Windows.Forms.TextBox();
            this.labMONPNT = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBeforeType3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBeforeType2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBeforeType1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAfterType3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAfterType2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAfterType1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butDeleteAlertRange);
            this.panel1.Controls.Add(this.butDefaultAlertRange);
            this.panel1.Controls.Add(this.butCloseAlertRange);
            this.panel1.Controls.Add(this.butSaveAlertRange);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(10, 478);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(454, 25);
            this.panel1.TabIndex = 171;
            // 
            // butDeleteAlertRange
            // 
            this.butDeleteAlertRange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butDeleteAlertRange.Location = new System.Drawing.Point(0, 1);
            this.butDeleteAlertRange.Name = "butDeleteAlertRange";
            this.butDeleteAlertRange.Size = new System.Drawing.Size(75, 23);
            this.butDeleteAlertRange.TabIndex = 171;
            this.butDeleteAlertRange.Text = "설정값삭제";
            this.butDeleteAlertRange.UseVisualStyleBackColor = true;
            this.butDeleteAlertRange.Click += new System.EventHandler(this.butDeleteAlertRange_Click);
            // 
            // butDefaultAlertRange
            // 
            this.butDefaultAlertRange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butDefaultAlertRange.Location = new System.Drawing.Point(305, 1);
            this.butDefaultAlertRange.Name = "butDefaultAlertRange";
            this.butDefaultAlertRange.Size = new System.Drawing.Size(53, 23);
            this.butDefaultAlertRange.TabIndex = 170;
            this.butDefaultAlertRange.Text = "기본값";
            this.butDefaultAlertRange.UseVisualStyleBackColor = true;
            this.butDefaultAlertRange.Visible = false;
            this.butDefaultAlertRange.Click += new System.EventHandler(this.butDefaultAlertRange_Click);
            // 
            // butCloseAlertRange
            // 
            this.butCloseAlertRange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butCloseAlertRange.Location = new System.Drawing.Point(412, 1);
            this.butCloseAlertRange.Name = "butCloseAlertRange";
            this.butCloseAlertRange.Size = new System.Drawing.Size(42, 23);
            this.butCloseAlertRange.TabIndex = 169;
            this.butCloseAlertRange.Text = "닫기";
            this.butCloseAlertRange.UseVisualStyleBackColor = true;
            this.butCloseAlertRange.Click += new System.EventHandler(this.butSelectTagClose_Click);
            // 
            // butSaveAlertRange
            // 
            this.butSaveAlertRange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSaveAlertRange.Location = new System.Drawing.Point(364, 1);
            this.butSaveAlertRange.Name = "butSaveAlertRange";
            this.butSaveAlertRange.Size = new System.Drawing.Size(42, 23);
            this.butSaveAlertRange.TabIndex = 168;
            this.butSaveAlertRange.Text = "저장";
            this.butSaveAlertRange.UseVisualStyleBackColor = true;
            this.butSaveAlertRange.Click += new System.EventHandler(this.butSaveAlertRange_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.pictureBox5);
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(454, 469);
            this.panel2.TabIndex = 172;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 113);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Size = new System.Drawing.Size(454, 346);
            this.splitContainer1.SplitterDistance = 224;
            this.splitContainer1.TabIndex = 133;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.picBeforeType3);
            this.groupBox2.Controls.Add(this.picBeforeType2);
            this.groupBox2.Controls.Add(this.picBeforeType1);
            this.groupBox2.Controls.Add(this.texBeforeType);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.texBefore4);
            this.groupBox2.Controls.Add(this.texBefore3);
            this.groupBox2.Controls.Add(this.texBefore2);
            this.groupBox2.Controls.Add(this.texBefore1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(224, 346);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "현재설정값";
            // 
            // picBeforeType3
            // 
            this.picBeforeType3.Image = global::WaterNet.WQ_Analysis.Properties.Resources.AlertRangeLineType3;
            this.picBeforeType3.Location = new System.Drawing.Point(81, 44);
            this.picBeforeType3.Margin = new System.Windows.Forms.Padding(0);
            this.picBeforeType3.Name = "picBeforeType3";
            this.picBeforeType3.Size = new System.Drawing.Size(110, 300);
            this.picBeforeType3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picBeforeType3.TabIndex = 161;
            this.picBeforeType3.TabStop = false;
            this.picBeforeType3.Visible = false;
            // 
            // picBeforeType2
            // 
            this.picBeforeType2.Image = ((System.Drawing.Image)(resources.GetObject("picBeforeType2.Image")));
            this.picBeforeType2.Location = new System.Drawing.Point(81, 44);
            this.picBeforeType2.Margin = new System.Windows.Forms.Padding(0);
            this.picBeforeType2.Name = "picBeforeType2";
            this.picBeforeType2.Size = new System.Drawing.Size(110, 300);
            this.picBeforeType2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picBeforeType2.TabIndex = 160;
            this.picBeforeType2.TabStop = false;
            this.picBeforeType2.Visible = false;
            // 
            // picBeforeType1
            // 
            this.picBeforeType1.Image = ((System.Drawing.Image)(resources.GetObject("picBeforeType1.Image")));
            this.picBeforeType1.Location = new System.Drawing.Point(81, 44);
            this.picBeforeType1.Margin = new System.Windows.Forms.Padding(0);
            this.picBeforeType1.Name = "picBeforeType1";
            this.picBeforeType1.Size = new System.Drawing.Size(110, 300);
            this.picBeforeType1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picBeforeType1.TabIndex = 159;
            this.picBeforeType1.TabStop = false;
            this.picBeforeType1.Visible = false;
            // 
            // texBeforeType
            // 
            this.texBeforeType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texBeforeType.Location = new System.Drawing.Point(80, 20);
            this.texBeforeType.MaxLength = 4;
            this.texBeforeType.Name = "texBeforeType";
            this.texBeforeType.ReadOnly = true;
            this.texBeforeType.Size = new System.Drawing.Size(100, 21);
            this.texBeforeType.TabIndex = 158;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 157;
            this.label1.Text = "경보타입";
            // 
            // texBefore4
            // 
            this.texBefore4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texBefore4.Location = new System.Drawing.Point(33, 269);
            this.texBefore4.Name = "texBefore4";
            this.texBefore4.ReadOnly = true;
            this.texBefore4.Size = new System.Drawing.Size(45, 21);
            this.texBefore4.TabIndex = 156;
            this.texBefore4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // texBefore3
            // 
            this.texBefore3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texBefore3.Location = new System.Drawing.Point(33, 212);
            this.texBefore3.Name = "texBefore3";
            this.texBefore3.ReadOnly = true;
            this.texBefore3.Size = new System.Drawing.Size(45, 21);
            this.texBefore3.TabIndex = 155;
            this.texBefore3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // texBefore2
            // 
            this.texBefore2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texBefore2.Location = new System.Drawing.Point(33, 155);
            this.texBefore2.Name = "texBefore2";
            this.texBefore2.ReadOnly = true;
            this.texBefore2.Size = new System.Drawing.Size(45, 21);
            this.texBefore2.TabIndex = 154;
            this.texBefore2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // texBefore1
            // 
            this.texBefore1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texBefore1.Location = new System.Drawing.Point(33, 98);
            this.texBefore1.Name = "texBefore1";
            this.texBefore1.ReadOnly = true;
            this.texBefore1.Size = new System.Drawing.Size(45, 21);
            this.texBefore1.TabIndex = 153;
            this.texBefore1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.picAfterType3);
            this.groupBox3.Controls.Add(this.picAfterType2);
            this.groupBox3.Controls.Add(this.picAfterType1);
            this.groupBox3.Controls.Add(this.texAfter4);
            this.groupBox3.Controls.Add(this.texAfter3);
            this.groupBox3.Controls.Add(this.texAfter2);
            this.groupBox3.Controls.Add(this.texAfter1);
            this.groupBox3.Controls.Add(this.comAfterType);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(226, 346);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "변경설정값";
            // 
            // picAfterType3
            // 
            this.picAfterType3.Image = global::WaterNet.WQ_Analysis.Properties.Resources.AlertRangeLineType3;
            this.picAfterType3.Location = new System.Drawing.Point(81, 44);
            this.picAfterType3.Margin = new System.Windows.Forms.Padding(0);
            this.picAfterType3.Name = "picAfterType3";
            this.picAfterType3.Size = new System.Drawing.Size(110, 300);
            this.picAfterType3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picAfterType3.TabIndex = 174;
            this.picAfterType3.TabStop = false;
            this.picAfterType3.Visible = false;
            // 
            // picAfterType2
            // 
            this.picAfterType2.Image = global::WaterNet.WQ_Analysis.Properties.Resources.AlertRangeLineType2;
            this.picAfterType2.Location = new System.Drawing.Point(81, 44);
            this.picAfterType2.Margin = new System.Windows.Forms.Padding(0);
            this.picAfterType2.Name = "picAfterType2";
            this.picAfterType2.Size = new System.Drawing.Size(110, 300);
            this.picAfterType2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picAfterType2.TabIndex = 173;
            this.picAfterType2.TabStop = false;
            this.picAfterType2.Visible = false;
            // 
            // picAfterType1
            // 
            this.picAfterType1.Image = global::WaterNet.WQ_Analysis.Properties.Resources.AlertRangeLineType1;
            this.picAfterType1.Location = new System.Drawing.Point(81, 44);
            this.picAfterType1.Margin = new System.Windows.Forms.Padding(0);
            this.picAfterType1.Name = "picAfterType1";
            this.picAfterType1.Size = new System.Drawing.Size(110, 300);
            this.picAfterType1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picAfterType1.TabIndex = 172;
            this.picAfterType1.TabStop = false;
            this.picAfterType1.Visible = false;
            // 
            // texAfter4
            // 
            this.texAfter4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texAfter4.Location = new System.Drawing.Point(33, 269);
            this.texAfter4.MaxLength = 5;
            this.texAfter4.Name = "texAfter4";
            this.texAfter4.Size = new System.Drawing.Size(45, 21);
            this.texAfter4.TabIndex = 171;
            this.texAfter4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // texAfter3
            // 
            this.texAfter3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texAfter3.Location = new System.Drawing.Point(33, 212);
            this.texAfter3.MaxLength = 5;
            this.texAfter3.Name = "texAfter3";
            this.texAfter3.Size = new System.Drawing.Size(45, 21);
            this.texAfter3.TabIndex = 170;
            this.texAfter3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // texAfter2
            // 
            this.texAfter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texAfter2.Location = new System.Drawing.Point(33, 155);
            this.texAfter2.MaxLength = 5;
            this.texAfter2.Name = "texAfter2";
            this.texAfter2.Size = new System.Drawing.Size(45, 21);
            this.texAfter2.TabIndex = 169;
            this.texAfter2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // texAfter1
            // 
            this.texAfter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texAfter1.Location = new System.Drawing.Point(33, 98);
            this.texAfter1.MaxLength = 5;
            this.texAfter1.Name = "texAfter1";
            this.texAfter1.Size = new System.Drawing.Size(45, 21);
            this.texAfter1.TabIndex = 168;
            this.texAfter1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // comAfterType
            // 
            this.comAfterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comAfterType.FormattingEnabled = true;
            this.comAfterType.Location = new System.Drawing.Point(79, 20);
            this.comAfterType.Name = "comAfterType";
            this.comAfterType.Size = new System.Drawing.Size(100, 20);
            this.comAfterType.TabIndex = 167;
            this.comAfterType.SelectionChangeCommitted += new System.EventHandler(this.comAfterType_SelectionChangeCommitted);
            this.comAfterType.Enter += new System.EventHandler(this.comAfterType_Enter);
            this.comAfterType.SelectedValueChanged += new System.EventHandler(this.comAfterType_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 166;
            this.label2.Text = "경보타입";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(0, 459);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(454, 10);
            this.pictureBox5.TabIndex = 130;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(0, 104);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(454, 9);
            this.pictureBox4.TabIndex = 129;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comWQItemList);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.texMonpnt);
            this.groupBox1.Controls.Add(this.labMONPNT);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 104);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "선택지점";
            // 
            // comWQItemList
            // 
            this.comWQItemList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comWQItemList.FormattingEnabled = true;
            this.comWQItemList.Location = new System.Drawing.Point(80, 63);
            this.comWQItemList.Name = "comWQItemList";
            this.comWQItemList.Size = new System.Drawing.Size(100, 20);
            this.comWQItemList.TabIndex = 141;
            this.comWQItemList.SelectionChangeCommitted += new System.EventHandler(this.comWQItemList_SelectionChangeCommitted);
            this.comWQItemList.Enter += new System.EventHandler(this.comWQItemList_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 140;
            this.label6.Text = "수질항목";
            // 
            // texMonpnt
            // 
            this.texMonpnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMonpnt.Location = new System.Drawing.Point(80, 23);
            this.texMonpnt.Name = "texMonpnt";
            this.texMonpnt.ReadOnly = true;
            this.texMonpnt.Size = new System.Drawing.Size(300, 21);
            this.texMonpnt.TabIndex = 139;
            // 
            // labMONPNT
            // 
            this.labMONPNT.AutoSize = true;
            this.labMONPNT.Location = new System.Drawing.Point(9, 28);
            this.labMONPNT.Name = "labMONPNT";
            this.labMONPNT.Size = new System.Drawing.Size(65, 12);
            this.labMONPNT.TabIndex = 138;
            this.labMONPNT.Text = "감시지점명";
            // 
            // groupBox4
            // 
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 356);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "현재설정값";
            // 
            // groupBox5
            // 
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(203, 356);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "변경설정값";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(464, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 494);
            this.pictureBox3.TabIndex = 131;
            this.pictureBox3.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 494);
            this.picFrLeftM1.TabIndex = 130;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 503);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(474, 9);
            this.pictureBox1.TabIndex = 129;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(474, 9);
            this.pictureBox2.TabIndex = 128;
            this.pictureBox2.TabStop = false;
            // 
            // frmAlertRangeSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 512);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAlertRangeSetting";
            this.Text = "경보범위설정";
            this.Load += new System.EventHandler(this.frmAlertRangeSetting_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAlertRangeSetting_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBeforeType3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBeforeType2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBeforeType1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAfterType3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAfterType2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAfterType1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butCloseAlertRange;
        private System.Windows.Forms.Button butSaveAlertRange;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox texMonpnt;
        private System.Windows.Forms.Label labMONPNT;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button butDefaultAlertRange;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox texBefore4;
        private System.Windows.Forms.TextBox texBefore3;
        private System.Windows.Forms.TextBox texBefore2;
        private System.Windows.Forms.TextBox texBefore1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comWQItemList;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comAfterType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox texBeforeType;
        private System.Windows.Forms.Button butDeleteAlertRange;
        private System.Windows.Forms.TextBox texAfter1;
        private System.Windows.Forms.TextBox texAfter4;
        private System.Windows.Forms.TextBox texAfter3;
        private System.Windows.Forms.TextBox texAfter2;
        private System.Windows.Forms.PictureBox picBeforeType1;
        private System.Windows.Forms.PictureBox picBeforeType2;
        private System.Windows.Forms.PictureBox picAfterType3;
        private System.Windows.Forms.PictureBox picAfterType2;
        private System.Windows.Forms.PictureBox picAfterType1;
        private System.Windows.Forms.PictureBox picBeforeType3;
    }
}