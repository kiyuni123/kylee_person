﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WQ_Analysis.util;
using WaterNet.WQ_Analysis.work;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using ChartFX.WinForms;
using System.Timers;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmDashboard : Form
    {
        #region 1.전역번수

        private RealtimeDataWork work = null;
        private System.Timers.Timer timer = new System.Timers.Timer();
        public string chainNo = "";
        public string chainNm = "";

        private int stateScrollPos;
        private int stateIndex = 0;

        delegate void SetRealtimeQualityStateCrossThreadCallback();

        #endregion

        #region 2.초기화

        public frmDashboard()
        {
            try
            {
                InitializeComponent();
                InitializeSetting();
                InitializeCombo();

                datStartDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                datEndDate.Value = DateTime.Now.ToString("yyyy-MM-dd");

                work = RealtimeDataWork.GetInstance();

                timer.Interval = 1000 * 60 * 5;
                timer.Elapsed += new ElapsedEventHandler(ExecuteTimerEvent);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                CommonUtils.ShowExceptionMessage(e.Message);
            }
        }

        #endregion

        #region 3.객체 이벤트

        //Form 최초 로딩 시 실행
        private void frmDashboard_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectRealtimeQualityState();
                SelectQualityChainDataForDashboard(chainNo);

                timer.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //실시간 수질정보 Grid 행 변경 시 실행
        private void griRealtimeQuality_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectTimeSeriesQualityDataForDashboard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //시작일 선택 후 enter key 입력 시 (시계열)
        private void datStartDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue != 13) return;

                this.Cursor = Cursors.WaitCursor;
                SelectTimeSeriesQualityDataForDashboard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //종료일 선택 후 enter key 입력 시 (시계열)
        private void datEndDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue != 13) return;

                this.Cursor = Cursors.WaitCursor;
                SelectTimeSeriesQualityDataForDashboard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //수질항목 선택 후 enter key 입력 시 (시계열)
        private void comQualityItem_KeyDown(object sender, KeyEventArgs e)
        {            
            try
            {
                if (e.KeyValue != 13) return;

                this.Cursor = Cursors.WaitCursor;

                if ("CL".Equals((sender as ComboBox).SelectedValue.ToString()))
                {
                    chaTimeSeriesQuality.AxisY.Title.Text = "잔류염소(mg/ℓ)";
                    chaTimeSeriesQuality.AxisY.LabelsFormat.Decimals = 4;        //y축 값을 소수점 4째자리까지 설정
                }
                else if ("TB".Equals((sender as ComboBox).SelectedValue.ToString()))
                {
                    chaTimeSeriesQuality.AxisY.Title.Text = "탁도(NTU)";
                    chaTimeSeriesQuality.AxisY.LabelsFormat.Decimals = 3;        //y축 값을 소수점 4째자리까지 설정
                }
                else if ("PH".Equals((sender as ComboBox).SelectedValue.ToString()))
                {
                    chaTimeSeriesQuality.AxisY.Title.Text = "pH(pH)";
                    chaTimeSeriesQuality.AxisY.LabelsFormat.Decimals = 1;        //y축 값을 소수점 4째자리까지 설정
                }

                SelectTimeSeriesQualityDataForDashboard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //적용일시 선택 후 enter key 입력 시 (계통)
        private void datTimestamp_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue != 13) return;

                this.Cursor = Cursors.WaitCursor;
                SelectQualityChainDataForDashboard(chainNo);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //계통선택 버튼 클릭 시 실행
        private void butSelectChain_Click(object sender, EventArgs e)
        {
            frmQualityChainSelect chainSelect = new frmQualityChainSelect();
            chainSelect.Owner = this;
            chainSelect.ShowDialog();
        }

        //수질현황 Row 더블클릭 시
        private void griRealtimeQuality_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable checkpointDatas = new Hashtable();

                Hashtable checkpointData1 = new Hashtable();
                checkpointData1.Add("MONITOR_NO", CommonUtils.GetGridCellValue(griRealtimeQuality, "MONITOR_NO"));
                checkpointData1.Add("TAGNAME", CommonUtils.GetGridCellValue(griRealtimeQuality, "TAG_ID_CL"));
                checkpointData1.Add("ITEM", "CLI");
                checkpointData1.Add("ITEM_NM", "잔류염소");
                checkpointData1.Add("MONPNT", CommonUtils.GetGridCellValue(griRealtimeQuality, "MONPNT"));
                checkpointData1.Add("UNIT", "mg/ℓ");

                checkpointDatas.Add(CommonUtils.GetGridCellValue(griRealtimeQuality, "MONITOR_NO") + "CLI", checkpointData1);

                Hashtable checkpointData2 = new Hashtable();
                checkpointData2.Add("MONITOR_NO", CommonUtils.GetGridCellValue(griRealtimeQuality, "MONITOR_NO"));
                checkpointData2.Add("TAGNAME", CommonUtils.GetGridCellValue(griRealtimeQuality, "TAG_ID_TB"));
                checkpointData2.Add("ITEM", "TBI");
                checkpointData2.Add("ITEM_NM", "탁도");
                checkpointData2.Add("MONPNT", CommonUtils.GetGridCellValue(griRealtimeQuality, "MONPNT"));
                checkpointData2.Add("UNIT", "NTU");

                checkpointDatas.Add(CommonUtils.GetGridCellValue(griRealtimeQuality, "MONITOR_NO") + "TBI", checkpointData2);

                Hashtable checkpointData3 = new Hashtable();
                checkpointData3.Add("MONITOR_NO", CommonUtils.GetGridCellValue(griRealtimeQuality, "MONITOR_NO"));
                checkpointData3.Add("TAGNAME", CommonUtils.GetGridCellValue(griRealtimeQuality, "TAG_ID_PH"));
                checkpointData3.Add("ITEM", "PHI");
                checkpointData3.Add("ITEM_NM", "pH");
                checkpointData3.Add("MONPNT", CommonUtils.GetGridCellValue(griRealtimeQuality, "MONPNT"));
                checkpointData3.Add("UNIT", "pH");

                checkpointDatas.Add(CommonUtils.GetGridCellValue(griRealtimeQuality, "MONITOR_NO") + "PHI", checkpointData3);

                frmRealtimeQualityChart qualityChartForm = new frmRealtimeQualityChart();
                qualityChartForm.Owner = this.Owner;
                qualityChartForm.SetCheckpointList(checkpointDatas);     //선택된 지점 정보 할당
                qualityChartForm.Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        
        //계통별 수질분석 상세보기 버튼 클릭 시
        private void butQualityChainDetail_Click(object sender, EventArgs e)
        {
            try
            {
                if ("".Equals(chainNo)) return;

                this.Cursor = Cursors.WaitCursor;

                frmRealtimeQualityChainChart chainChartForm = new frmRealtimeQualityChainChart();
                chainChartForm.Owner = this.Owner;
                chainChartForm.chainNo = chainNo;
                chainChartForm.chainNm = chainNm;
                chainChartForm.Show();

                frmQualityAnalysisMap mapForm = this.Owner as frmQualityAnalysisMap;
                mapForm.qualityChainForms.Add(CommonUtils.GetSerialNumber("CH"), chainChartForm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //Dashboard Form 종료 시
        private void frmDashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Dispose();
                timer = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        #endregion

        #region 6.내부사용

        //Grid/Chart 초기화
        private void InitializeSetting()
        {
            #region Grid 초기화
            UltraGridColumn realtimeQualityColumn;
            
            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "MONITOR_NO";
            realtimeQualityColumn.Header.Caption = "지점번호";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 100;
            realtimeQualityColumn.Hidden = true;   //필드 보이기

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "MONPNT";
            realtimeQualityColumn.Header.Caption = "구분";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 180;
            realtimeQualityColumn.Hidden = false;   //필드 보이기

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "CL_VALUE";
            realtimeQualityColumn.Header.Caption = "잔류염소\n(mg/ℓ)";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 60;
            realtimeQualityColumn.Hidden = false;   //필드 보이기

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "TB_VALUE";
            realtimeQualityColumn.Header.Caption = "탁도\n(NTU)";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 60;
            realtimeQualityColumn.Hidden = false;   //필드 보이기

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "PH_VALUE";
            realtimeQualityColumn.Header.Caption = "pH\n(pH)";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 60;
            realtimeQualityColumn.Hidden = false;   //필드 보이기

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "TAG_ID_CL";
            realtimeQualityColumn.Header.Caption = "잔류염소TAG";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Left;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 100;
            realtimeQualityColumn.Hidden = true;   //필드 보이기

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "TAG_ID_TB";
            realtimeQualityColumn.Header.Caption = "탁도TAG";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Left;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 100;
            realtimeQualityColumn.Hidden = true;   //필드 보이기

            realtimeQualityColumn = griRealtimeQuality.DisplayLayout.Bands[0].Columns.Add();
            realtimeQualityColumn.Key = "TAG_ID_PH";
            realtimeQualityColumn.Header.Caption = "pHTAG";
            realtimeQualityColumn.CellActivation = Activation.NoEdit;
            realtimeQualityColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeQualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeQualityColumn.CellAppearance.TextHAlign = HAlign.Left;
            realtimeQualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeQualityColumn.Width = 100;
            realtimeQualityColumn.Hidden = true;   //필드 보이기

            griRealtimeQuality.DrawFilter = new GridMiddleAlignDrawFilter();        //1행에 2줄이상 표출하게 되면 valign이 동작하지 않는다. 따라서 필터를 써야함
            #endregion

            #region 시계열수질분석 Chart 초기화
            //Chart 초기화
            chaTimeSeriesQuality.Data.Clear();
            chaTimeSeriesQuality.Data.Series = 1;
            chaTimeSeriesQuality.Series[0].Color = Color.Crimson;
            chaTimeSeriesQuality.AxisX.Staggered = true;
            chaTimeSeriesQuality.DataSourceSettings.Fields.Add(new FieldMap("TIMESTAMP", FieldUsage.Label));
            chaTimeSeriesQuality.DataSourceSettings.Fields.Add(new FieldMap("VALUE", FieldUsage.Value));

            //Y축 설정
            chaTimeSeriesQuality.AxisY.Title.Text = "잔류염소(mg/ℓ)";
            chaTimeSeriesQuality.AxisY.LabelsFormat.Decimals = 4;        //y축 값을 소수점 4째자리까지 설정
            #endregion

            #region 계통별 수질분석 Chart 초기화
            //차트 초기화
            chaQualityChain.Data.Clear();
            chaQualityChain.Data.Series = 1;
            chaQualityChain.Series[0].Color = Color.Crimson;
            chaQualityChain.Series[0].MarkerShape = MarkerShape.Circle;
            chaQualityChain.Series[0].Line.Width = 3;

            chaQualityChain.AxesX[0].LabelTrimming = StringTrimming.None;
            chaQualityChain.AxesY[0].LabelTrimming = StringTrimming.None;
            
            //차트 Data Field 설정
            chaQualityChain.DataSourceSettings.Fields.Add(new FieldMap("MONPNT", FieldUsage.Label));
            chaQualityChain.DataSourceSettings.Fields.Add(new FieldMap("VALUE", FieldUsage.Value));

            //Y축 설정
            chaQualityChain.AxisY.Title.Text = "잔류염소(mg/ℓ)";
            chaQualityChain.AxisY.LabelsFormat.Decimals = 4;        //y축 값을 소수점 2째자리까지 설정
            #endregion
        }

        //Combo 초기화
        private void InitializeCombo()
        {
            comQualityItem.DisplayMember = "DISPLAY";
            comQualityItem.ValueMember = "VALUE";

            DataTable qualityComboData = new DataTable();
            DataColumn col1 = new DataColumn("DISPLAY", typeof(string));
            DataColumn col2 = new DataColumn("VALUE", typeof(string));

            qualityComboData.Columns.Add(col1);
            qualityComboData.Columns.Add(col2);

            DataRow row1 = qualityComboData.NewRow();
            DataRow row2 = qualityComboData.NewRow();
            DataRow row3 = qualityComboData.NewRow();

            row1["DISPLAY"] = "잔류염소";
            row1["VALUE"] = "CL";
            row2["DISPLAY"] = "탁도";
            row2["VALUE"] = "TB";
            row3["DISPLAY"] = "pH";
            row3["VALUE"] = "PH";

            qualityComboData.Rows.Add(row1);
            qualityComboData.Rows.Add(row2);
            qualityComboData.Rows.Add(row3);

            comQualityItem.DataSource = qualityComboData;
        }
        
        //실시간 수질정보 현황 데이터 조회
        private void SelectRealtimeQualityState()
        {
            if (griRealtimeQuality.ActiveRowScrollRegion != null) stateScrollPos = griRealtimeQuality.ActiveRowScrollRegion.ScrollPosition;

            if (griRealtimeQuality.Selected.Rows.Count != 0) stateIndex = griRealtimeQuality.Selected.Rows[0].Index;
            else stateIndex = 0;

            griRealtimeQuality.DataSource = work.SelectRealtimeQualityStateForDashboard(null);

            //상태에 따라 Text색을 변경
            if (griRealtimeQuality.Rows.Count != 0)
            {
                DataTable tmpDt = griRealtimeQuality.DataSource as DataTable;

                foreach (UltraGridRow row in griRealtimeQuality.Rows)
                {
                    foreach (DataColumn column in tmpDt.Columns)
                    {
                        if (row.GetCellValue(column.ToString()).ToString().Contains("위험"))
                        {
                            row.Cells[column.ToString()].Appearance.ForeColor = Color.Red;
                        }
                        else if (row.GetCellValue(column.ToString()).ToString().Contains("관심"))
                        {
                            row.Cells[column.ToString()].Appearance.ForeColor = Color.Fuchsia;
                        }
                    }
                }
            }

            griRealtimeQuality.ActiveRowScrollRegion.ScrollPosition = stateScrollPos;
            CommonUtils.SelectGridRow(griRealtimeQuality, stateIndex);        
        }

        //시계열 수질분석 데이터 조회
        private void SelectTimeSeriesQualityDataForDashboard()
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("startDate", ((DateTime)datStartDate.Value).ToString("yyyyMMdd") + "0000");
            conditions.Add("endDate", ((DateTime)datEndDate.Value).ToString("yyyyMMdd") + "2359");

            if ("CL".Equals(comQualityItem.SelectedValue)) conditions.Add("TAGNAME", CommonUtils.GetGridCellValue(griRealtimeQuality, "TAG_ID_CL"));
            else if ("TB".Equals(comQualityItem.SelectedValue)) conditions.Add("TAGNAME", CommonUtils.GetGridCellValue(griRealtimeQuality, "TAG_ID_TB"));
            else if ("PH".Equals(comQualityItem.SelectedValue)) conditions.Add("TAGNAME", CommonUtils.GetGridCellValue(griRealtimeQuality, "TAG_ID_PH"));

            chaTimeSeriesQuality.DataSource = work.SelectTimeSeriesQualityDataForDashboard(conditions);
        }

        //계통별  수질분석 데이터 조회 (계통 선택창에서 선택 시 호출됨)
        public void SelectQualityChainDataForDashboard(string sendedChainNo)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("TIMESTAMP", ((DateTime)datTimestamp.Value).ToString("yyyyMMddHHmm"));
            if (!"".Equals(sendedChainNo)) conditions.Add("CHAIN_NO", sendedChainNo);

            Hashtable result = work.SelectQualityChainDataForDashboard(conditions);

            if (result["CHAIN_NO"] != null) chainNo = result["CHAIN_NO"].ToString();
            if (result["CHAIN_NM"] != null) chainNm = result["CHAIN_NM"].ToString();

            chaQualityChain.DataSource = result["qualityChainData"] as DataTable;
        }

        //Timer가 실행될때마다 상태변경
        private void ExecuteTimerEvent(System.Object sender, EventArgs eArgs)
        {
            //SelectRealtimeQualityState();       //현재상태 반영
            SetRealtimeQualityStateCrossThread();
        }

        //Cross thread 참조를 막기위한 invoker
        private void SetRealtimeQualityStateCrossThread()
        {
            if (griRealtimeQuality.InvokeRequired)
            {
                SetRealtimeQualityStateCrossThreadCallback d = new SetRealtimeQualityStateCrossThreadCallback(SetRealtimeQualityStateCrossThread);
                this.Invoke(d, null);
            }
            else
            {
                SelectRealtimeQualityState();
            }
        }

        #endregion
    }
}
