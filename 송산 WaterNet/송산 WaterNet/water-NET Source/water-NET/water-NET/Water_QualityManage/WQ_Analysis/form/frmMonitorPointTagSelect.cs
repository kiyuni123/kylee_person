﻿/**************************************************************************
 * 파일명   : frmMonitorPointTagSelect.cs
 * 작성자   : shpark
 * 작성일자 : 2014.12.09
 * 설명     : 수질감시지점관리 수질태그선택 팝업 폼
 * 변경이력 : 2014.12.09 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.util;
using WaterNet.WQ_Analysis.work;
using EMFrame.log;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmMonitorPointTagSelect : Form
    {
        #region 1. 전역변수

        private RealtimeDataWork work = null;
        private Hashtable wqTypeConditions = null;      //메인폼에서 전달받은 수질타입(BR_CODE)과 수질명(DESCRIPTION)을 할당하기 위한 전역변수
        private Control wqTextBox = null;       //메인폼에서 전달받은 텍스트 박스를 할당하기 위한 전역변수

        #endregion 전역변수


        #region 2. 초기화

        public frmMonitorPointTagSelect(Control wqTextBox, Hashtable wqTypeConditions)
        {
            InitializeComponent();
            InitializeSetting();
            work = RealtimeDataWork.GetInstance();

            this.wqTextBox = wqTextBox;
            this.wqTypeConditions = wqTypeConditions;
        }

        //수질태그 목록 그리드 초기화
        private void InitializeSetting()
        {
            UltraGridColumn selectTagColumn;

            selectTagColumn = griSelectWQTagList.DisplayLayout.Bands[0].Columns.Add();
            selectTagColumn.Key = "TAGNAME";
            selectTagColumn.Header.Caption = "태그명";
            selectTagColumn.CellActivation = Activation.NoEdit;
            selectTagColumn.CellClickAction = CellClickAction.RowSelect;
            selectTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            selectTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            selectTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            selectTagColumn.Width = 112;
            selectTagColumn.Hidden = false;   //필드 보이기

            selectTagColumn = griSelectWQTagList.DisplayLayout.Bands[0].Columns.Add();
            selectTagColumn.Key = "TAG_DESC";
            selectTagColumn.Header.Caption = "태그설명";
            selectTagColumn.CellActivation = Activation.NoEdit;
            selectTagColumn.CellClickAction = CellClickAction.RowSelect;
            selectTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            selectTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            selectTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            selectTagColumn.Width = 300;
            selectTagColumn.Hidden = false;   //필드 보이기
        }

        #endregion 초기화


        #region 3. 이벤트

        //폼 로드 이벤트
        private void frmMonitorPointTagSelect_Load(object sender, EventArgs e)
        {
            try
            {
                if (wqTypeConditions != null)
                {
                    griSelectWQTagList.DataSource = work.SelectMonitorPointTagList(wqTypeConditions);

                    CommonUtils.SelectFirstGridRow(griSelectWQTagList);
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //등록태그삭제 버튼 클릭 이벤트
        private void butDelectRegTag_Click(object sender, EventArgs e)
        {
            try
            {
                if (wqTextBox.Text.Equals(string.Empty))
                {
                    CommonUtils.ShowInformationMessage("등록된 수질 태그가 없습니다."); return;
                }

                if (CommonUtils.ShowYesNoDialog("등록된 수질 태그를 삭제하시겠습니까?") == DialogResult.Yes)
                {
                    wqTextBox.Text = string.Empty;

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //선택 버튼 클릭 이벤트
        private void butSelectTag_Click(object sender, EventArgs e)
        {
            try
            {
                if (griSelectWQTagList.Selected.Rows.Count == 0)
                {
                    CommonUtils.ShowInformationMessage("선택된 수질 태그가 없습니다."); return;
                }

                if (!wqTextBox.Text.Equals(string.Empty))
                {
                    if (CommonUtils.ShowYesNoDialog("이미 등록되어 있는 수질태그가 있습니다.\n선택한 수질태그로 바꾸시겠습니까?") == DialogResult.No) return;
                }
                else if (CommonUtils.ShowYesNoDialog("선택된 수질 태그를 설정하시겠습니까?") == DialogResult.No) return;

                wqTextBox.Text = griSelectWQTagList.Selected.Rows[0].Cells["TAGNAME"].Value.ToString();

                this.Close();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //닫기 버튼 클릭 이벤트
        private void butSelectTagClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion 이벤트


        #region 4. 상속
        #endregion 상속


        #region 5. 외부호출
        #endregion 외부호출


        #region 6. 내부사용
        #endregion 내부사용

    }
}
