﻿/**************************************************************************
 * 파일명   : frmChainManage.cs
 * 작성자   : shpark
 * 작성일자 : 2015.01.27
 * 설명     : 계통관리 메인폼
 * 변경이력 : 2015.01.27 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WQ_Analysis.work;
using WaterNet.WQ_Analysis.util;
using EMFrame.log;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmChainManage : Form
    {
        #region 1. 전역변수

        private frmQualityAnalysisMap ownerForm = null;     //메인맵 메소드 사용을 위한 전역 변수
        private ChainManageWork work = null;
        private string[] arrChainList = null;
        private bool checkAddRow = false;

        #endregion 전역변수


        #region 2. 초기화

        public frmChainManage()
        {
            InitializeComponent();
            InitializeSetting();
            work = ChainManageWork.GetInstance();
        }

        //계통관리 그리드 초기화
        private void InitializeSetting()
        {
            UltraGridColumn chainMasterColumn;  //계통관리 마스터 그리드

            chainMasterColumn = griChainMasterList.DisplayLayout.Bands[0].Columns.Add();
            chainMasterColumn.Key = "CHAIN_NO";
            chainMasterColumn.Header.Caption = "계통일련번호";
            chainMasterColumn.CellActivation = Activation.NoEdit;
            chainMasterColumn.CellClickAction = CellClickAction.RowSelect;
            chainMasterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainMasterColumn.CellAppearance.TextHAlign = HAlign.Center;
            chainMasterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainMasterColumn.Width = 100;
            chainMasterColumn.Hidden = true;   //필드 보이기

            chainMasterColumn = griChainMasterList.DisplayLayout.Bands[0].Columns.Add();
            chainMasterColumn.Key = "CHAIN_NM";
            chainMasterColumn.Header.Caption = "계통명";
            chainMasterColumn.CellActivation = Activation.NoEdit;
            chainMasterColumn.CellClickAction = CellClickAction.RowSelect;
            chainMasterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainMasterColumn.CellAppearance.TextHAlign = HAlign.Left;
            chainMasterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainMasterColumn.Width = 250;
            chainMasterColumn.Hidden = false;   //필드 보이기

            chainMasterColumn = griChainMasterList.DisplayLayout.Bands[0].Columns.Add();
            chainMasterColumn.Key = "REG_DAT";
            chainMasterColumn.Header.Caption = "등록일자";
            chainMasterColumn.CellActivation = Activation.NoEdit;
            chainMasterColumn.CellClickAction = CellClickAction.RowSelect;
            chainMasterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainMasterColumn.CellAppearance.TextHAlign = HAlign.Center;
            chainMasterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainMasterColumn.Width = 100;
            chainMasterColumn.Hidden = false;   //필드 보이기

            chainMasterColumn = griChainMasterList.DisplayLayout.Bands[0].Columns.Add();
            chainMasterColumn.Key = "REMARK";
            chainMasterColumn.Header.Caption = "비고";
            chainMasterColumn.CellActivation = Activation.NoEdit;
            chainMasterColumn.CellClickAction = CellClickAction.RowSelect;
            chainMasterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainMasterColumn.CellAppearance.TextHAlign = HAlign.Left;
            chainMasterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainMasterColumn.Width = 200;
            chainMasterColumn.Hidden = false;   //필드 보이기

            UltraGridColumn chainDetailColumn;  //계통관리 상세 그리드

            chainDetailColumn = griChainDetailList.DisplayLayout.Bands[0].Columns.Add();
            chainDetailColumn.Key = "CHAIN_NO";
            chainDetailColumn.Header.Caption = "계통일련번호";
            chainDetailColumn.CellActivation = Activation.NoEdit;
            chainDetailColumn.CellClickAction = CellClickAction.RowSelect;
            chainDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            chainDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainDetailColumn.Width = 100;
            chainDetailColumn.Hidden = true;   //필드 보이기

            chainMasterColumn = griChainDetailList.DisplayLayout.Bands[0].Columns.Add();
            chainMasterColumn.Key = "MONITOR_NO";
            chainMasterColumn.Header.Caption = "감시지점 관리번호";
            chainMasterColumn.CellActivation = Activation.NoEdit;
            chainMasterColumn.CellClickAction = CellClickAction.RowSelect;
            chainMasterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainMasterColumn.CellAppearance.TextHAlign = HAlign.Center;
            chainMasterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainMasterColumn.Width = 10;
            chainMasterColumn.Hidden = true;   //필드 보이기

            chainMasterColumn = griChainDetailList.DisplayLayout.Bands[0].Columns.Add();
            chainMasterColumn.Key = "MONPNT";
            chainMasterColumn.Header.Caption = "관리지점명";
            chainMasterColumn.CellActivation = Activation.NoEdit;
            chainMasterColumn.CellClickAction = CellClickAction.RowSelect;
            chainMasterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainMasterColumn.CellAppearance.TextHAlign = HAlign.Left;
            chainMasterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainMasterColumn.Width = 235;
            chainMasterColumn.Hidden = false;   //필드 보이기

            chainDetailColumn = griChainDetailList.DisplayLayout.Bands[0].Columns.Add();
            chainDetailColumn.Key = "IDX";
            chainDetailColumn.Header.Caption = "인덱스";
            chainDetailColumn.CellActivation = Activation.NoEdit;
            chainDetailColumn.CellClickAction = CellClickAction.RowSelect;
            chainDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            chainDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainDetailColumn.Width = 30;
            chainDetailColumn.Hidden = true;   //필드 보이기

            UltraGridColumn monitorPointColumn;  //추가 가능 지점 그리드

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MONITOR_NO";
            monitorPointColumn.Header.Caption = "감시지점 관리번호";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Center;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 10;
            monitorPointColumn.Hidden = true;   //필드 보이기

            monitorPointColumn = griMonitorPointList.DisplayLayout.Bands[0].Columns.Add();
            monitorPointColumn.Key = "MONPNT";
            monitorPointColumn.Header.Caption = "관리지점명";
            monitorPointColumn.CellActivation = Activation.NoEdit;
            monitorPointColumn.CellClickAction = CellClickAction.RowSelect;
            monitorPointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            monitorPointColumn.CellAppearance.TextHAlign = HAlign.Left;
            monitorPointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            monitorPointColumn.Width = 235;
            monitorPointColumn.Hidden = false;   //필드 보이기
        }

        #endregion 초기화


        #region 3. 객체이벤트

        //계통관리 폼 로드 이벤트
        private void frmChainManage_Load(object sender, EventArgs e)
        {
            if (ownerForm == null)
            {
                ownerForm = (this.Owner as frmQualityAnalysisMap);
            }

            SelectChainMasterList();
        }

        //계통관리 폼 클로즈 전 이벤트 : 계통마스터, 계통상세 그리드의 상태를 확인한다.
        private void frmChainManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!CheckGridListChanged(arrChainList, griChainDetailList) ||
                CommonUtils.HaveChangedData(griChainMasterList.DataSource as DataTable))
            {
                if (CommonUtils.ShowYesNoDialog("변경된 내용을 저장하지 않았습니다.\n현재 창을 닫으시겠습니까?") == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        //계통관리 폼 클로즈 이벤트
        private void frmChainManage_FormClosed(object sender, FormClosedEventArgs e)
        {
            ownerForm.CloseFormAction("frmChainManage");
        }

        //계통마스터 선택 로우 변경전 이벤트
        private void griChainMasterList_BeforeSelectChange(object sender, BeforeSelectChangeEventArgs e)
        {
            if (griChainMasterList.DataSource == null) return;
            
            if (griChainDetailList.DataSource == null && griMonitorPointList.DataSource == null) return;

            if (!CheckGridListChanged(arrChainList, griChainDetailList) && !checkAddRow)
            {
                if (CommonUtils.ShowYesNoDialog("계통등록지점의 변경내용을 저장하지 않았습니다.\n계속 하시겠습니까?") == DialogResult.No)
                {
                    (sender as UltraGrid).Rows[(sender as UltraGrid).Selected.Rows[0].Index].Activate();

                    e.Cancel = true;
                }
            }
        }

        //계통마스터 선택 로우 변경후 이벤트
        private void griChainMasterList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                if (griChainMasterList.Rows.Count != 0)
                {
                    texChainMasterName.Text = CommonUtils.GetGridCellValue(griChainMasterList, "CHAIN_NM");
                    texChainMasterRemark.Text = CommonUtils.GetGridCellValue(griChainMasterList, "REMARK");

                    SelectChainDetailList();
                    SelectMonitorPointList();

                    arrChainList = new string[griChainDetailList.Rows.Count];

                    for (int i = 0; i < griChainDetailList.Rows.Count; i++)
                    {
                        arrChainList[i] = griChainDetailList.Rows[i].Cells["MONPNT"].Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //계통마스터 계통명 텍스트박스 변경 이벤트
        private void texChainMasterName_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griChainMasterList, "CHAIN_NM", (sender as TextBox).Text);
        }

        //계통마스터 비고 텍스트박스 변경 이벤트
        private void texChainMasterRemark_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.ControlTextChanged(griChainMasterList, "REMARK", (sender as TextBox).Text);
        }

        #region + 버튼 이벤트

        //계통마스터 추가 버튼 클릭 이벤트
        private void butChainMasterAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if ((!CheckGridListChanged(arrChainList, griChainDetailList)))
                {
                    if (CommonUtils.ShowYesNoDialog("계통등록지점의 변경내용을 저장하지 않았습니다.\n계속 하시겠습니까?") == DialogResult.No)
                    {
                        return;
                    }
                    else
                    {
                        checkAddRow = true;
                    }
                }         

                string chain_NO = CommonUtils.GetSerialNumber("QC"); //계통 마스터 Chain_No 생성

                CommonUtils.AddGridRow(griChainMasterList);
                CommonUtils.SetGridCellValue(griChainMasterList, "CHAIN_NO", chain_NO);
                CommonUtils.SetGridCellValue(griChainMasterList, "REG_DAT", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));

                checkAddRow = false;
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //계통마스터 삭제 버튼 클릭 이벤트
        private void butChainMasterDel_Click(object sender, EventArgs e)
        {
            try
            {
                Hashtable deleteData = null;

                if ((deleteData = CommonUtils.DeleteGridRow
                    (griChainMasterList, "계통 마스터에 속한 계통상세 정보가 모두 삭제됩니다.\n계통 마스터 정보를 삭제하시겠습니까?")) != null)
                {
                    Hashtable deleteCondition = new Hashtable();
                    deleteCondition.Add("CHAIN_NO", deleteData["CHAIN_NO"].ToString());
                    deleteCondition.Add("chainDetailCount", griChainDetailList.Rows.Count);

                    work.DeleteChainMasterData(deleteCondition);

                    if (griChainMasterList.Selected.Rows.Count == 0)
                    {
                        texChainMasterName.Text = string.Empty;
                        texChainMasterRemark.Text = string.Empty;
                    }

                    CommonUtils.ShowInformationMessage("정상적으로 처리되었습니다.");
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //계통마스터 저장 버튼 클릭 이벤트
        private void butChainMasterSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //validation 정보 (사이즈|공백허용|숫자여부)
                Hashtable validationSet = new Hashtable();
                validationSet.Add("CHAIN_NO", "18|N|N");
                validationSet.Add("CHAIN_NM", "400|N|N");
                validationSet.Add("REG_DAT", "50|N|N");
                validationSet.Add("REMARK", "400|Y|N");

                if (CommonUtils.SaveGridData(griChainMasterList, validationSet))
                {
                    Hashtable saveConditions = new Hashtable();
                    saveConditions.Add("chainMasterData", griChainMasterList.DataSource as DataTable);

                    work.SaveChainMasterData(saveConditions);

                    CommonUtils.ShowInformationMessage("정상적으로 처리되었습니다.");

                    string selectedCM = griChainMasterList.ActiveRow.Cells["CHAIN_NO"].Value.ToString();

                    SelectChainMasterList();

                    foreach (UltraGridRow row in griChainMasterList.Rows)
                    {
                        if (row.Cells["CHAIN_NO"].Value.Equals(selectedCM))
                        {
                            row.Activate();
                            row.Selected = true;
                        }
                    }

                    checkAddRow = true;
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //계통등록지점 선택 로우 위로 이동 버튼 클릭 이벤트
        private void butMoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckChainMasterGridChanged()) return;

                CommonUtils.MoveRowUpDown(griChainDetailList, "UP");
                ChangeChainMasterTextboxState();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
        }

        //계통등록지점 선택 로우 아래로 이동 버튼 클릭 이벤트
        private void butMoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckChainMasterGridChanged()) return;

                CommonUtils.MoveRowUpDown(griChainDetailList, "DOWN");
                ChangeChainMasterTextboxState();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
        }

        //계통등록지점 지점정보 추가(좌로 이동) 버튼 클릭 이벤트
        private void butMoveLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckChainMasterGridChanged()) return;

                if (griMonitorPointList.Selected.Rows.Count == 0)
                {
                    CommonUtils.ShowInformationMessage("추가 할 수 있는 지점정보가 없습니다."); 
                    return;
                }

                Hashtable addRowConditions = new Hashtable();

                addRowConditions.Add("CHAIN_NO", CommonUtils.GetGridCellValue(griChainMasterList, "CHAIN_NO"));
                addRowConditions.Add("MONITOR_NO", CommonUtils.GetGridCellValue(griMonitorPointList, "MONITOR_NO"));
                addRowConditions.Add("MONPNT", CommonUtils.GetGridCellValue(griMonitorPointList, "MONPNT"));
                addRowConditions.Add("IDX", "");

                ChainDetailDataMove(griMonitorPointList, griChainDetailList, addRowConditions);
                ChangeChainMasterTextboxState();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
        }

        //계통등록지점 지점정보 삭제(우로 이동) 버튼 클릭 이벤트
        private void butMoveRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckChainMasterGridChanged()) return;

                if (griChainDetailList.Selected.Rows.Count == 0)
                {
                    CommonUtils.ShowInformationMessage("삭제 할 수 있는 계통상세 지점정보가 없습니다.");
                    return;
                }

                Hashtable delRowConditions = new Hashtable();

                delRowConditions.Add("MONITOR_NO", CommonUtils.GetGridCellValue(griChainDetailList, "MONITOR_NO"));
                delRowConditions.Add("MONPNT", CommonUtils.GetGridCellValue(griChainDetailList, "MONPNT"));

                ChainDetailDataMove(griChainDetailList, griMonitorPointList, delRowConditions);
                ChangeChainMasterTextboxState();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
        }

        //계통등록지점 저장 버튼 클릭 이벤트
        private void butChainDetailSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (arrChainList == null || CheckGridListChanged(arrChainList, griChainDetailList))
                {
                    CommonUtils.ShowInformationMessage("변경된 내용이 없습니다.");
                    return;
                }
                
                //validation 정보 (사이즈|공백허용|숫자여부)
                Hashtable validationSet = new Hashtable();
                validationSet.Add("CHAIN_NO", "18|N|N");
                validationSet.Add("MONITOR_NO", "18|N|N");

                if (!CommonUtils.ValidationGridData(griChainDetailList, validationSet)) return;

                if (CommonUtils.ShowYesNoDialog("저장하시겠습니까?") == DialogResult.Yes)
                {
                    Hashtable deleteCondition = new Hashtable();
                    deleteCondition.Add("CHAIN_NO", CommonUtils.GetGridCellValue(griChainMasterList, "CHAIN_NO"));

                    work.SaveChainDetailData(deleteCondition, griChainDetailList);

                    CommonUtils.ShowInformationMessage("정상적으로 처리되었습니다.");

                    arrChainList = new string[griChainDetailList.Rows.Count];
                    for (int i = 0; i < griChainDetailList.Rows.Count; i++)
                    {
                        arrChainList[i] = griChainDetailList.Rows[i].Cells["MONPNT"].Value.ToString();
                    }

                    ChangeChainMasterTextboxState();
                }               
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion 버튼 이벤트

        #region + 계통관리 상세 그리드 마우스 드래그로 로우 이동

        private void griChainDetailList_SelectionDrag(object sender, CancelEventArgs e)
        {
            griChainDetailList.DoDragDrop(griChainDetailList.Selected.Rows, DragDropEffects.Move);
        }

        private void griChainDetailList_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
            UltraGrid detailGrid = sender as UltraGrid;
            Point pointInGridCoords = detailGrid.PointToClient(new Point(e.X, e.Y));
            if (pointInGridCoords.Y < 10)
            {
                this.griChainDetailList.ActiveRowScrollRegion.Scroll(RowScrollAction.LineUp);
            }
            else if (pointInGridCoords.Y > detailGrid.Height - 10)
            {
                this.griChainDetailList.ActiveRowScrollRegion.Scroll(RowScrollAction.PageDown);
            }
        }

        private void griChainDetailList_DragDrop(object sender, DragEventArgs e)
        {
            if (!CheckChainMasterGridChanged()) return;

            int dropIndex;

            UIElement uieOver = griChainDetailList.DisplayLayout.UIElement.ElementFromPoint(griChainDetailList.PointToClient(new Point(e.X, e.Y)));

            UltraGridRow ugrOver = uieOver.GetContext(typeof(UltraGridRow), true) as UltraGridRow;
            if (ugrOver != null)
            {
                dropIndex = ugrOver.Index;

                SelectedRowsCollection SelRows = (SelectedRowsCollection)e.Data.GetData(typeof(SelectedRowsCollection)) as SelectedRowsCollection;

                foreach (UltraGridRow aRow in SelRows)
                {
                    griChainDetailList.Rows.Move(aRow, dropIndex);
                }

                griChainDetailList.Rows[dropIndex].Activate();
                griChainDetailList.Rows[dropIndex].Selected = true;

                ChangeChainMasterTextboxState();
            }
        }

        #endregion 계통관리 상세 그리드 마우스 드래그로 로우 이동

        #endregion 객체이벤트


        #region 4. 상속
        #endregion 상속


        #region 5. 외부호출
        #endregion 외부호출


        #region 6. 내부사용

        //계통관리 마스터 리스트 조회
        private void SelectChainMasterList()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                griChainMasterList.DataSource = work.SelectChainMasterList();

                CommonUtils.SelectFirstGridRow(griChainMasterList);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //계통관리 상세 리스트 조회
        private void SelectChainDetailList()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable condition = new Hashtable();

                condition.Add("CHAIN_NO", CommonUtils.GetGridCellValue(griChainMasterList, "CHAIN_NO"));

                griChainDetailList.DataSource = work.SelectChainDetailList(condition);

                CommonUtils.SelectFirstGridRow(griChainDetailList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //계통관리 추가 가능 지점 리스트 조회
        private void SelectMonitorPointList()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable condition = new Hashtable();

                condition.Add("CHAIN_NO", CommonUtils.GetGridCellValue(griChainMasterList, "CHAIN_NO"));

                griMonitorPointList.DataSource = work.SelectMonitorPointList(condition);

                CommonUtils.SelectFirstGridRow(griMonitorPointList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //두개의 그리드에서 선택로우의 데이터를 해쉬로 저장하여 이동한다.
        private void ChainDetailDataMove(UltraGrid souGrid, UltraGrid desGrid, Hashtable rowConditions)
        {
            try
            {
                int souIndex =  souGrid.Selected.Rows[0].Index == 0 ? 0 : souGrid.Selected.Rows[0].Index - 1;

                CommonUtils.AddGridRow(desGrid, rowConditions);

                souGrid.Selected.Rows[0].Delete(false);
                (souGrid.DataSource as DataTable).AcceptChanges();

                if (souGrid.Rows.Count != 0)
                {
                    souGrid.Rows[souIndex].Activate();
                    souGrid.Rows[souIndex].Selected = true;
                }

                ChangeChainMasterTextboxState();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //그리드 생성시 저장된 배열과 현재 그리드의 값을 비교하여 그리드 변경여부를 체크한다.
        private bool CheckGridListChanged(string[] array, UltraGrid grid)
        {
            bool result = true;

            if (array == null)
            {
                return result;
            }

            if ((array.Length != grid.Rows.Count))
            {
                result = false;
                return result;
            }

            for (int i = 0; i < array.Length; i++)
            {
                if (!array[i].Equals(grid.Rows[i].Cells["MONPNT"].Value.ToString()))
                {
                    result = false;
                }
            }

            return result;
        }

        //계통 마스터 텍스트박스의 상태를 상황에 따라 변경한다.
        private void ChangeChainMasterTextboxState()
        {
            if (!CheckGridListChanged(arrChainList, griChainDetailList))
            {
                texChainMasterName.ReadOnly = true;
                texChainMasterRemark.ReadOnly = true;
            }
            else
            {
                texChainMasterName.ReadOnly = false;
                texChainMasterRemark.ReadOnly = false;
            }
        }

        //계통관리 마스터의 그리드 상태를 확인한다.
        private bool CheckChainMasterGridChanged()
        {
            bool result = true;

            if (CommonUtils.HaveChangedData(griChainMasterList.DataSource as DataTable))
            {
                CommonUtils.ShowInformationMessage("계통 마스터의 변경내용을 저장하십시오.");
                result = false;
            }

            return result;
        }

        #endregion 내부사용

    }
}
