﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmRealtimeQualityChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ChartFX.WinForms.Adornments.SolidBackground solidBackground1 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.datStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.datEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.butSelectRealtimeQuality = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.chaRealtimeQuality = new ChartFX.WinForms.Chart();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.griRealtimeQuality = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chaRealtimeQuality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeQuality)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(784, 9);
            this.pictureBox2.TabIndex = 117;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 564);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(784, 9);
            this.pictureBox1.TabIndex = 118;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 555);
            this.picFrLeftM1.TabIndex = 119;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(774, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 555);
            this.pictureBox3.TabIndex = 120;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ultraLabel7);
            this.groupBox1.Controls.Add(this.datStartDate);
            this.groupBox1.Controls.Add(this.datEndDate);
            this.groupBox1.Controls.Add(this.ultraLabel1);
            this.groupBox1.Controls.Add(this.butSelectRealtimeQuality);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(764, 57);
            this.groupBox1.TabIndex = 121;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.AutoSize = true;
            this.ultraLabel7.Location = new System.Drawing.Point(168, 24);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(12, 14);
            this.ultraLabel7.TabIndex = 153;
            this.ultraLabel7.Text = "-";
            // 
            // datStartDate
            // 
            this.datStartDate.Location = new System.Drawing.Point(68, 20);
            this.datStartDate.MaskInput = "{date}";
            this.datStartDate.Name = "datStartDate";
            this.datStartDate.Size = new System.Drawing.Size(100, 21);
            this.datStartDate.TabIndex = 150;
            // 
            // datEndDate
            // 
            this.datEndDate.Location = new System.Drawing.Point(180, 20);
            this.datEndDate.MaskInput = "{date}";
            this.datEndDate.Name = "datEndDate";
            this.datEndDate.Size = new System.Drawing.Size(100, 21);
            this.datEndDate.TabIndex = 151;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Location = new System.Drawing.Point(10, 24);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(54, 16);
            this.ultraLabel1.TabIndex = 152;
            this.ultraLabel1.Text = "검색기간";
            // 
            // butSelectRealtimeQuality
            // 
            this.butSelectRealtimeQuality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectRealtimeQuality.Location = new System.Drawing.Point(698, 19);
            this.butSelectRealtimeQuality.Name = "butSelectRealtimeQuality";
            this.butSelectRealtimeQuality.Size = new System.Drawing.Size(52, 23);
            this.butSelectRealtimeQuality.TabIndex = 149;
            this.butSelectRealtimeQuality.Text = "조회";
            this.butSelectRealtimeQuality.UseVisualStyleBackColor = true;
            this.butSelectRealtimeQuality.Click += new System.EventHandler(this.butSelectRealtimeQuality_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 66);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(764, 9);
            this.pictureBox4.TabIndex = 122;
            this.pictureBox4.TabStop = false;
            // 
            // chaRealtimeQuality
            // 
            this.chaRealtimeQuality.AllSeries.Line.Width = ((short)(1));
            this.chaRealtimeQuality.AllSeries.MarkerShape = ChartFX.WinForms.MarkerShape.None;
            this.chaRealtimeQuality.AllSeries.MarkerSize = ((short)(0));
            solidBackground1.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chaRealtimeQuality.Background = solidBackground1;
            this.chaRealtimeQuality.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chaRealtimeQuality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaRealtimeQuality.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chaRealtimeQuality.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chaRealtimeQuality.Location = new System.Drawing.Point(10, 75);
            this.chaRealtimeQuality.Name = "chaRealtimeQuality";
            this.chaRealtimeQuality.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chaRealtimeQuality.Size = new System.Drawing.Size(764, 250);
            this.chaRealtimeQuality.TabIndex = 127;
            this.chaRealtimeQuality.MouseDoubleClick += new ChartFX.WinForms.HitTestEventHandler(this.chaRealtimeQuality_MouseDoubleClick);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(10, 325);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(764, 10);
            this.splitter1.TabIndex = 128;
            this.splitter1.TabStop = false;
            // 
            // griRealtimeQuality
            // 
            this.griRealtimeQuality.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQuality.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQuality.DisplayLayout.MaxColScrollRegions = 1;
            this.griRealtimeQuality.DisplayLayout.MaxRowScrollRegions = 1;
            this.griRealtimeQuality.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQuality.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQuality.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQuality.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griRealtimeQuality.DisplayLayout.Override.CellPadding = 0;
            this.griRealtimeQuality.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griRealtimeQuality.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griRealtimeQuality.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griRealtimeQuality.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griRealtimeQuality.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQuality.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griRealtimeQuality.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griRealtimeQuality.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griRealtimeQuality.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griRealtimeQuality.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.griRealtimeQuality.Location = new System.Drawing.Point(10, 335);
            this.griRealtimeQuality.Name = "griRealtimeQuality";
            this.griRealtimeQuality.Size = new System.Drawing.Size(764, 229);
            this.griRealtimeQuality.TabIndex = 149;
            this.griRealtimeQuality.Text = "ultraGrid1";
            // 
            // frmRealtimeQualityChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 573);
            this.Controls.Add(this.chaRealtimeQuality);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.griRealtimeQuality);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(800, 611);
            this.Name = "frmRealtimeQualityChart";
            this.Text = "실시간수질감시";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chaRealtimeQuality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeQuality)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butSelectRealtimeQuality;
        private System.Windows.Forms.PictureBox pictureBox4;
        private ChartFX.WinForms.Chart chaRealtimeQuality;
        private System.Windows.Forms.Splitter splitter1;
        private Infragistics.Win.UltraWinGrid.UltraGrid griRealtimeQuality;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datStartDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    }
}