﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmMonitorPointTagSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butDelectRegTag = new System.Windows.Forms.Button();
            this.butSelectTagClose = new System.Windows.Forms.Button();
            this.butSelectTag = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.griSelectWQTagList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griSelectWQTagList)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(424, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 494);
            this.pictureBox3.TabIndex = 127;
            this.pictureBox3.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 494);
            this.picFrLeftM1.TabIndex = 126;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 503);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(434, 9);
            this.pictureBox1.TabIndex = 125;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(434, 9);
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butDelectRegTag);
            this.panel1.Controls.Add(this.butSelectTagClose);
            this.panel1.Controls.Add(this.butSelectTag);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(10, 478);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(414, 25);
            this.panel1.TabIndex = 170;
            // 
            // butDelectRegTag
            // 
            this.butDelectRegTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butDelectRegTag.Location = new System.Drawing.Point(230, 1);
            this.butDelectRegTag.Name = "butDelectRegTag";
            this.butDelectRegTag.Size = new System.Drawing.Size(88, 23);
            this.butDelectRegTag.TabIndex = 170;
            this.butDelectRegTag.Text = "등록태그삭제";
            this.butDelectRegTag.UseVisualStyleBackColor = true;
            this.butDelectRegTag.Click += new System.EventHandler(this.butDelectRegTag_Click);
            // 
            // butSelectTagClose
            // 
            this.butSelectTagClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectTagClose.Location = new System.Drawing.Point(372, 1);
            this.butSelectTagClose.Name = "butSelectTagClose";
            this.butSelectTagClose.Size = new System.Drawing.Size(42, 23);
            this.butSelectTagClose.TabIndex = 169;
            this.butSelectTagClose.Text = "닫기";
            this.butSelectTagClose.UseVisualStyleBackColor = true;
            this.butSelectTagClose.Click += new System.EventHandler(this.butSelectTagClose_Click);
            // 
            // butSelectTag
            // 
            this.butSelectTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectTag.Location = new System.Drawing.Point(324, 1);
            this.butSelectTag.Name = "butSelectTag";
            this.butSelectTag.Size = new System.Drawing.Size(42, 23);
            this.butSelectTag.TabIndex = 168;
            this.butSelectTag.Text = "선택";
            this.butSelectTag.UseVisualStyleBackColor = true;
            this.butSelectTag.Click += new System.EventHandler(this.butSelectTag_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 469);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(414, 9);
            this.pictureBox4.TabIndex = 171;
            this.pictureBox4.TabStop = false;
            // 
            // griSelectWQTagList
            // 
            this.griSelectWQTagList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griSelectWQTagList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griSelectWQTagList.DisplayLayout.MaxColScrollRegions = 1;
            this.griSelectWQTagList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griSelectWQTagList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griSelectWQTagList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griSelectWQTagList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance2.BorderColor = System.Drawing.Color.DarkGray;
            this.griSelectWQTagList.DisplayLayout.Override.CellAppearance = appearance2;
            this.griSelectWQTagList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griSelectWQTagList.DisplayLayout.Override.CellPadding = 0;
            this.griSelectWQTagList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griSelectWQTagList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griSelectWQTagList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griSelectWQTagList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griSelectWQTagList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance3.BorderColor = System.Drawing.Color.DarkGray;
            this.griSelectWQTagList.DisplayLayout.Override.RowAppearance = appearance3;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griSelectWQTagList.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griSelectWQTagList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griSelectWQTagList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griSelectWQTagList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griSelectWQTagList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griSelectWQTagList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griSelectWQTagList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griSelectWQTagList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griSelectWQTagList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griSelectWQTagList.Location = new System.Drawing.Point(10, 9);
            this.griSelectWQTagList.Name = "griSelectWQTagList";
            this.griSelectWQTagList.Size = new System.Drawing.Size(414, 460);
            this.griSelectWQTagList.TabIndex = 172;
            this.griSelectWQTagList.Text = "ultraGrid1";
            // 
            // frmMonitorPointTagSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 512);
            this.Controls.Add(this.griSelectWQTagList);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMonitorPointTagSelect";
            this.Text = "수질태그선택";
            this.Load += new System.EventHandler(this.frmMonitorPointTagSelect_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griSelectWQTagList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butSelectTagClose;
        private System.Windows.Forms.Button butSelectTag;
        private System.Windows.Forms.PictureBox pictureBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid griSelectWQTagList;
        private System.Windows.Forms.Button butDelectRegTag;
    }
}