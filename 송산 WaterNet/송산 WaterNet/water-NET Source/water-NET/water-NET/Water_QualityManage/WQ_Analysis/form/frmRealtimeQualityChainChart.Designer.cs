﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmRealtimeQualityChainChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ChartFX.WinForms.Adornments.SolidBackground solidBackground1 = new ChartFX.WinForms.Adornments.SolidBackground();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butAfterOneMonth = new System.Windows.Forms.Button();
            this.butAfterOneWeek = new System.Windows.Forms.Button();
            this.butAfterOneDay = new System.Windows.Forms.Button();
            this.datTimestamp = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.butBeforeOneMonth = new System.Windows.Forms.Button();
            this.butBeforeOneWeek = new System.Windows.Forms.Button();
            this.butBeforeOneDay = new System.Windows.Forms.Button();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.butSelectRealtimeQualityChain = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.chaRealtimeQualityChain = new ChartFX.WinForms.Chart();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.griRealtimeQualityChain = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datTimestamp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chaRealtimeQualityChain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeQualityChain)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(792, 9);
            this.pictureBox2.TabIndex = 118;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 564);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(792, 9);
            this.pictureBox1.TabIndex = 119;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 555);
            this.picFrLeftM1.TabIndex = 120;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(782, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 555);
            this.pictureBox3.TabIndex = 121;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butAfterOneMonth);
            this.groupBox1.Controls.Add(this.butAfterOneWeek);
            this.groupBox1.Controls.Add(this.butAfterOneDay);
            this.groupBox1.Controls.Add(this.datTimestamp);
            this.groupBox1.Controls.Add(this.butBeforeOneMonth);
            this.groupBox1.Controls.Add(this.butBeforeOneWeek);
            this.groupBox1.Controls.Add(this.butBeforeOneDay);
            this.groupBox1.Controls.Add(this.ultraLabel1);
            this.groupBox1.Controls.Add(this.butSelectRealtimeQualityChain);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(772, 57);
            this.groupBox1.TabIndex = 122;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butAfterOneMonth
            // 
            this.butAfterOneMonth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butAfterOneMonth.Location = new System.Drawing.Point(459, 19);
            this.butAfterOneMonth.Name = "butAfterOneMonth";
            this.butAfterOneMonth.Size = new System.Drawing.Size(55, 23);
            this.butAfterOneMonth.TabIndex = 160;
            this.butAfterOneMonth.Text = "1개월후";
            this.butAfterOneMonth.UseVisualStyleBackColor = true;
            this.butAfterOneMonth.Click += new System.EventHandler(this.butAfterOneMonth_Click);
            // 
            // butAfterOneWeek
            // 
            this.butAfterOneWeek.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butAfterOneWeek.Location = new System.Drawing.Point(353, 19);
            this.butAfterOneWeek.Name = "butAfterOneWeek";
            this.butAfterOneWeek.Size = new System.Drawing.Size(43, 23);
            this.butAfterOneWeek.TabIndex = 159;
            this.butAfterOneWeek.Text = "1주후";
            this.butAfterOneWeek.UseVisualStyleBackColor = true;
            this.butAfterOneWeek.Click += new System.EventHandler(this.butAfterOneWeek_Click);
            // 
            // butAfterOneDay
            // 
            this.butAfterOneDay.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butAfterOneDay.Location = new System.Drawing.Point(259, 19);
            this.butAfterOneDay.Name = "butAfterOneDay";
            this.butAfterOneDay.Size = new System.Drawing.Size(43, 23);
            this.butAfterOneDay.TabIndex = 158;
            this.butAfterOneDay.Text = "1일후";
            this.butAfterOneDay.UseVisualStyleBackColor = true;
            this.butAfterOneDay.Click += new System.EventHandler(this.butAfterOneDay_Click);
            // 
            // datTimestamp
            // 
            this.datTimestamp.DateTime = new System.DateTime(2014, 8, 19, 22, 53, 15, 0);
            this.datTimestamp.Location = new System.Drawing.Point(70, 19);
            this.datTimestamp.MaskInput = "{LOC}yyyy-mm-dd hh:mm";
            this.datTimestamp.Name = "datTimestamp";
            this.datTimestamp.Size = new System.Drawing.Size(138, 21);
            this.datTimestamp.TabIndex = 157;
            this.datTimestamp.Value = new System.DateTime(2014, 8, 19, 22, 53, 15, 0);
            // 
            // butBeforeOneMonth
            // 
            this.butBeforeOneMonth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butBeforeOneMonth.Location = new System.Drawing.Point(402, 19);
            this.butBeforeOneMonth.Name = "butBeforeOneMonth";
            this.butBeforeOneMonth.Size = new System.Drawing.Size(55, 23);
            this.butBeforeOneMonth.TabIndex = 156;
            this.butBeforeOneMonth.Text = "1개월전";
            this.butBeforeOneMonth.UseVisualStyleBackColor = true;
            this.butBeforeOneMonth.Click += new System.EventHandler(this.butBeforeOneMonth_Click);
            // 
            // butBeforeOneWeek
            // 
            this.butBeforeOneWeek.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butBeforeOneWeek.Location = new System.Drawing.Point(308, 19);
            this.butBeforeOneWeek.Name = "butBeforeOneWeek";
            this.butBeforeOneWeek.Size = new System.Drawing.Size(43, 23);
            this.butBeforeOneWeek.TabIndex = 155;
            this.butBeforeOneWeek.Text = "1주전";
            this.butBeforeOneWeek.UseVisualStyleBackColor = true;
            this.butBeforeOneWeek.Click += new System.EventHandler(this.butBeforeOneWeek_Click);
            // 
            // butBeforeOneDay
            // 
            this.butBeforeOneDay.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butBeforeOneDay.Location = new System.Drawing.Point(214, 19);
            this.butBeforeOneDay.Name = "butBeforeOneDay";
            this.butBeforeOneDay.Size = new System.Drawing.Size(43, 23);
            this.butBeforeOneDay.TabIndex = 154;
            this.butBeforeOneDay.Text = "1일전";
            this.butBeforeOneDay.UseVisualStyleBackColor = true;
            this.butBeforeOneDay.Click += new System.EventHandler(this.butBeforeOneDay_Click);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Location = new System.Drawing.Point(24, 21);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(42, 16);
            this.ultraLabel1.TabIndex = 152;
            this.ultraLabel1.Text = "검색일";
            // 
            // butSelectRealtimeQualityChain
            // 
            this.butSelectRealtimeQualityChain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectRealtimeQualityChain.Location = new System.Drawing.Point(706, 19);
            this.butSelectRealtimeQualityChain.Name = "butSelectRealtimeQualityChain";
            this.butSelectRealtimeQualityChain.Size = new System.Drawing.Size(52, 23);
            this.butSelectRealtimeQualityChain.TabIndex = 149;
            this.butSelectRealtimeQualityChain.Text = "조회";
            this.butSelectRealtimeQualityChain.UseVisualStyleBackColor = true;
            this.butSelectRealtimeQualityChain.Click += new System.EventHandler(this.butSelectRealtimeQualityChain_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 66);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(772, 9);
            this.pictureBox4.TabIndex = 123;
            this.pictureBox4.TabStop = false;
            // 
            // chaRealtimeQualityChain
            // 
            this.chaRealtimeQualityChain.AllSeries.Line.Width = ((short)(1));
            this.chaRealtimeQualityChain.AllSeries.MarkerShape = ChartFX.WinForms.MarkerShape.None;
            this.chaRealtimeQualityChain.AllSeries.MarkerSize = ((short)(2));
            solidBackground1.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chaRealtimeQualityChain.Background = solidBackground1;
            this.chaRealtimeQualityChain.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chaRealtimeQualityChain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaRealtimeQualityChain.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chaRealtimeQualityChain.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chaRealtimeQualityChain.Location = new System.Drawing.Point(10, 75);
            this.chaRealtimeQualityChain.Name = "chaRealtimeQualityChain";
            this.chaRealtimeQualityChain.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chaRealtimeQualityChain.Size = new System.Drawing.Size(772, 250);
            this.chaRealtimeQualityChain.TabIndex = 128;
            this.chaRealtimeQualityChain.MouseDoubleClick += new ChartFX.WinForms.HitTestEventHandler(this.chaRealtimeQualityChain_MouseDoubleClick);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(10, 325);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(772, 10);
            this.splitter1.TabIndex = 129;
            this.splitter1.TabStop = false;
            // 
            // griRealtimeQualityChain
            // 
            this.griRealtimeQualityChain.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQualityChain.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQualityChain.DisplayLayout.MaxColScrollRegions = 1;
            this.griRealtimeQualityChain.DisplayLayout.MaxRowScrollRegions = 1;
            this.griRealtimeQualityChain.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQualityChain.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQualityChain.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQualityChain.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griRealtimeQualityChain.DisplayLayout.Override.CellPadding = 0;
            this.griRealtimeQualityChain.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griRealtimeQualityChain.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griRealtimeQualityChain.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griRealtimeQualityChain.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griRealtimeQualityChain.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griRealtimeQualityChain.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griRealtimeQualityChain.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griRealtimeQualityChain.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griRealtimeQualityChain.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQualityChain.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griRealtimeQualityChain.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griRealtimeQualityChain.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griRealtimeQualityChain.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griRealtimeQualityChain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.griRealtimeQualityChain.Location = new System.Drawing.Point(10, 335);
            this.griRealtimeQualityChain.Name = "griRealtimeQualityChain";
            this.griRealtimeQualityChain.Size = new System.Drawing.Size(772, 229);
            this.griRealtimeQualityChain.TabIndex = 150;
            this.griRealtimeQualityChain.Text = "ultraGrid1";
            // 
            // frmRealtimeQualityChainChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.chaRealtimeQualityChain);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.griRealtimeQualityChain);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(808, 611);
            this.Name = "frmRealtimeQualityChainChart";
            this.Text = "수질계통데이터";
            this.Load += new System.EventHandler(this.frmRealtimeQualityChainChart_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRealtimeQualityChainChart_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datTimestamp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chaRealtimeQualityChain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeQualityChain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private System.Windows.Forms.Button butSelectRealtimeQualityChain;
        private System.Windows.Forms.PictureBox pictureBox4;
        private ChartFX.WinForms.Chart chaRealtimeQualityChain;
        private System.Windows.Forms.Splitter splitter1;
        private Infragistics.Win.UltraWinGrid.UltraGrid griRealtimeQualityChain;
        private System.Windows.Forms.Button butBeforeOneMonth;
        private System.Windows.Forms.Button butBeforeOneWeek;
        private System.Windows.Forms.Button butBeforeOneDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datTimestamp;
        private System.Windows.Forms.Button butAfterOneMonth;
        private System.Windows.Forms.Button butAfterOneWeek;
        private System.Windows.Forms.Button butAfterOneDay;
    }
}