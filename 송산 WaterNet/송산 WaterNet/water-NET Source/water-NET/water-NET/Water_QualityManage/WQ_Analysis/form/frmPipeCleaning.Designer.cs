﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmPipeCleaning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.butSavePcleanData = new System.Windows.Forms.Button();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.butSavePipe = new System.Windows.Forms.Button();
            this.butDelPipe = new System.Windows.Forms.Button();
            this.griPipe = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.texRemark = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.texProcResult = new System.Windows.Forms.TextBox();
            this.griPipeCleaning = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butDelPcleanData = new System.Windows.Forms.Button();
            this.butAddPcleanData = new System.Windows.Forms.Button();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.texProcMethodTitle = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.texProcResultTitle = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.texProcMethod = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.datProcDat = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.texPcleanTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cheSelectAll = new System.Windows.Forms.CheckBox();
            this.texCondPcleanTitle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.datStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.datEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.butSelectPcleanData = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.griComplaintList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.griComplaintState = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.treBlockList = new Infragistics.Win.UltraWinTree.UltraTree();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.comComplaintType = new System.Windows.Forms.ComboBox();
            this.datComEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cheShowComplaint = new System.Windows.Forms.CheckBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.datComStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.butSelectComplaint = new System.Windows.Forms.Button();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.griModelList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checAnnotation = new System.Windows.Forms.CheckBox();
            this.cheVelocity = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comVelocityFormula = new System.Windows.Forms.ComboBox();
            this.texVelocityBaseValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.butExecuteAnalysis = new System.Windows.Forms.Button();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.texTitle = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.butSelectModelList = new System.Windows.Forms.Button();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griPipe)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griPipeCleaning)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datProcDat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griComplaintList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griComplaintState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treBlockList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datComEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datComStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griModelList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // butSavePcleanData
            // 
            this.butSavePcleanData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSavePcleanData.Location = new System.Drawing.Point(642, 0);
            this.butSavePcleanData.Name = "butSavePcleanData";
            this.butSavePcleanData.Size = new System.Drawing.Size(52, 23);
            this.butSavePcleanData.TabIndex = 170;
            this.butSavePcleanData.Text = "저장";
            this.butSavePcleanData.UseVisualStyleBackColor = true;
            this.butSavePcleanData.Click += new System.EventHandler(this.butSavePcleanData_Click);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox14.Location = new System.Drawing.Point(13, 515);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(174, 9);
            this.pictureBox14.TabIndex = 154;
            this.pictureBox14.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.butSavePipe);
            this.panel3.Controls.Add(this.butDelPipe);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(13, 524);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(174, 24);
            this.panel3.TabIndex = 153;
            // 
            // butSavePipe
            // 
            this.butSavePipe.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.butSavePipe.Location = new System.Drawing.Point(92, 0);
            this.butSavePipe.Name = "butSavePipe";
            this.butSavePipe.Size = new System.Drawing.Size(52, 23);
            this.butSavePipe.TabIndex = 173;
            this.butSavePipe.Text = "저장";
            this.butSavePipe.UseVisualStyleBackColor = true;
            this.butSavePipe.Click += new System.EventHandler(this.butSavePipe_Click);
            // 
            // butDelPipe
            // 
            this.butDelPipe.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.butDelPipe.Location = new System.Drawing.Point(36, 0);
            this.butDelPipe.Name = "butDelPipe";
            this.butDelPipe.Size = new System.Drawing.Size(52, 23);
            this.butDelPipe.TabIndex = 172;
            this.butDelPipe.Text = "삭제";
            this.butDelPipe.UseVisualStyleBackColor = true;
            this.butDelPipe.Click += new System.EventHandler(this.butDelPipe_Click);
            // 
            // griPipe
            // 
            this.griPipe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griPipe.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griPipe.DisplayLayout.MaxColScrollRegions = 1;
            this.griPipe.DisplayLayout.MaxRowScrollRegions = 1;
            this.griPipe.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griPipe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griPipe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griPipe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griPipe.DisplayLayout.Override.CellPadding = 0;
            this.griPipe.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griPipe.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griPipe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griPipe.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griPipe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griPipe.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griPipe.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griPipe.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griPipe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griPipe.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griPipe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griPipe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griPipe.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griPipe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griPipe.Location = new System.Drawing.Point(13, 26);
            this.griPipe.Name = "griPipe";
            this.griPipe.Size = new System.Drawing.Size(174, 489);
            this.griPipe.TabIndex = 171;
            this.griPipe.Text = "ultraGrid1";
            this.griPipe.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.griPipe_DoubleClickCell);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.griPipe);
            this.groupBox3.Controls.Add(this.pictureBox14);
            this.groupBox3.Controls.Add(this.panel3);
            this.groupBox3.Controls.Add(this.pictureBox12);
            this.groupBox3.Controls.Add(this.pictureBox11);
            this.groupBox3.Controls.Add(this.pictureBox10);
            this.groupBox3.Controls.Add(this.pictureBox9);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(744, 78);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 560);
            this.groupBox3.TabIndex = 155;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "대상관로";
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox12.Location = new System.Drawing.Point(3, 26);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(10, 522);
            this.pictureBox12.TabIndex = 127;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox11.Location = new System.Drawing.Point(187, 26);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(10, 522);
            this.pictureBox11.TabIndex = 126;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox10.Location = new System.Drawing.Point(3, 548);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(194, 9);
            this.pictureBox10.TabIndex = 125;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(3, 17);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(194, 9);
            this.pictureBox9.TabIndex = 124;
            this.pictureBox9.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(10, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(485, 13);
            this.label7.TabIndex = 169;
            this.label7.Text = "* 대상관로를 저장하지 않고 행 이동 시 대상관로 입력정보가 초기화됩니다.";
            // 
            // texRemark
            // 
            this.texRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texRemark.Location = new System.Drawing.Point(72, 269);
            this.texRemark.Name = "texRemark";
            this.texRemark.Size = new System.Drawing.Size(598, 21);
            this.texRemark.TabIndex = 167;
            this.texRemark.TextChanged += new System.EventHandler(this.texRemark_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 167;
            this.label6.Text = "비고";
            // 
            // texProcResult
            // 
            this.texProcResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texProcResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texProcResult.Location = new System.Drawing.Point(72, 191);
            this.texProcResult.Multiline = true;
            this.texProcResult.Name = "texProcResult";
            this.texProcResult.Size = new System.Drawing.Size(598, 70);
            this.texProcResult.TabIndex = 166;
            this.texProcResult.TextChanged += new System.EventHandler(this.texProcResult_TextChanged);
            // 
            // griPipeCleaning
            // 
            this.griPipeCleaning.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griPipeCleaning.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griPipeCleaning.DisplayLayout.MaxColScrollRegions = 1;
            this.griPipeCleaning.DisplayLayout.MaxRowScrollRegions = 1;
            this.griPipeCleaning.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griPipeCleaning.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griPipeCleaning.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griPipeCleaning.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griPipeCleaning.DisplayLayout.Override.CellPadding = 0;
            this.griPipeCleaning.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griPipeCleaning.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griPipeCleaning.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griPipeCleaning.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griPipeCleaning.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.griPipeCleaning.DisplayLayout.Override.RowSelectorAppearance = appearance2;
            this.griPipeCleaning.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griPipeCleaning.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griPipeCleaning.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griPipeCleaning.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griPipeCleaning.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griPipeCleaning.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griPipeCleaning.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griPipeCleaning.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griPipeCleaning.Location = new System.Drawing.Point(13, 26);
            this.griPipeCleaning.Name = "griPipeCleaning";
            this.griPipeCleaning.Size = new System.Drawing.Size(695, 177);
            this.griPipeCleaning.TabIndex = 151;
            this.griPipeCleaning.Text = "ultraGrid1";
            this.griPipeCleaning.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griPipeCleaning_AfterSelectChange);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.butSavePcleanData);
            this.panel2.Controls.Add(this.butDelPcleanData);
            this.panel2.Controls.Add(this.butAddPcleanData);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(13, 524);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(695, 24);
            this.panel2.TabIndex = 152;
            // 
            // butDelPcleanData
            // 
            this.butDelPcleanData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butDelPcleanData.Location = new System.Drawing.Point(586, 0);
            this.butDelPcleanData.Name = "butDelPcleanData";
            this.butDelPcleanData.Size = new System.Drawing.Size(52, 23);
            this.butDelPcleanData.TabIndex = 169;
            this.butDelPcleanData.Text = "삭제";
            this.butDelPcleanData.UseVisualStyleBackColor = true;
            this.butDelPcleanData.Click += new System.EventHandler(this.butDelPcleanData_Click);
            // 
            // butAddPcleanData
            // 
            this.butAddPcleanData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butAddPcleanData.Location = new System.Drawing.Point(530, 0);
            this.butAddPcleanData.Name = "butAddPcleanData";
            this.butAddPcleanData.Size = new System.Drawing.Size(52, 23);
            this.butAddPcleanData.TabIndex = 168;
            this.butAddPcleanData.Text = "추가";
            this.butAddPcleanData.UseVisualStyleBackColor = true;
            this.butAddPcleanData.Click += new System.EventHandler(this.butAddPcleanData_Click);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox15.Location = new System.Drawing.Point(13, 548);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(695, 9);
            this.pictureBox15.TabIndex = 124;
            this.pictureBox15.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.griPipeCleaning);
            this.groupBox2.Controls.Add(this.splitter1);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.splitter5);
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Controls.Add(this.pictureBox15);
            this.groupBox2.Controls.Add(this.pictureBox16);
            this.groupBox2.Controls.Add(this.pictureBox17);
            this.groupBox2.Controls.Add(this.pictureBox18);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(13, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(721, 560);
            this.groupBox2.TabIndex = 157;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "관세척정보";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(13, 203);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(695, 10);
            this.splitter1.TabIndex = 153;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.texProcMethodTitle);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.texProcResultTitle);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.texRemark);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.texProcResult);
            this.panel1.Controls.Add(this.texProcMethod);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.datProcDat);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.texPcleanTitle);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(13, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(695, 301);
            this.panel1.TabIndex = 124;
            // 
            // texProcMethodTitle
            // 
            this.texProcMethodTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texProcMethodTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texProcMethodTitle.Location = new System.Drawing.Point(72, 58);
            this.texProcMethodTitle.Name = "texProcMethodTitle";
            this.texProcMethodTitle.Size = new System.Drawing.Size(333, 21);
            this.texProcMethodTitle.TabIndex = 163;
            this.texProcMethodTitle.TextChanged += new System.EventHandler(this.texProcMethodTitle_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 174;
            this.label15.Text = "세척방법";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 173;
            this.label14.Text = "상세";
            // 
            // texProcResultTitle
            // 
            this.texProcResultTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texProcResultTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texProcResultTitle.Location = new System.Drawing.Point(72, 163);
            this.texProcResultTitle.Name = "texProcResultTitle";
            this.texProcResultTitle.Size = new System.Drawing.Size(333, 21);
            this.texProcResultTitle.TabIndex = 165;
            this.texProcResultTitle.TextChanged += new System.EventHandler(this.texProcResultTitle_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 171;
            this.label13.Text = "세척결과";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 232);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 170;
            this.label12.Text = "상세";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 166;
            this.label5.Text = "세척결과";
            // 
            // texProcMethod
            // 
            this.texProcMethod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texProcMethod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texProcMethod.Location = new System.Drawing.Point(72, 86);
            this.texProcMethod.Multiline = true;
            this.texProcMethod.Name = "texProcMethod";
            this.texProcMethod.Size = new System.Drawing.Size(598, 70);
            this.texProcMethod.TabIndex = 164;
            this.texProcMethod.TextChanged += new System.EventHandler(this.texProcMethod_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 163;
            this.label4.Text = "세척방법";
            // 
            // datProcDat
            // 
            this.datProcDat.DateTime = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            this.datProcDat.Location = new System.Drawing.Point(385, 29);
            this.datProcDat.MaskInput = "{date}";
            this.datProcDat.Name = "datProcDat";
            this.datProcDat.Size = new System.Drawing.Size(100, 21);
            this.datProcDat.TabIndex = 162;
            this.datProcDat.Value = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            this.datProcDat.ValueChanged += new System.EventHandler(this.datProcDat_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(326, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 162;
            this.label2.Text = "관세척일";
            // 
            // texPcleanTitle
            // 
            this.texPcleanTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texPcleanTitle.Location = new System.Drawing.Point(72, 31);
            this.texPcleanTitle.Name = "texPcleanTitle";
            this.texPcleanTitle.Size = new System.Drawing.Size(239, 21);
            this.texPcleanTitle.TabIndex = 161;
            this.texPcleanTitle.TextChanged += new System.EventHandler(this.texPcleanTitle_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 160;
            this.label1.Text = "명칭";
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter5.Location = new System.Drawing.Point(13, 514);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(695, 10);
            this.splitter5.TabIndex = 154;
            this.splitter5.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox16.Location = new System.Drawing.Point(13, 17);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(695, 9);
            this.pictureBox16.TabIndex = 123;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox17.Location = new System.Drawing.Point(708, 17);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(10, 540);
            this.pictureBox17.TabIndex = 121;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox18.Location = new System.Drawing.Point(3, 17);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(10, 540);
            this.pictureBox18.TabIndex = 120;
            this.pictureBox18.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(734, 78);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(10, 560);
            this.splitter2.TabIndex = 156;
            this.splitter2.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox7.Location = new System.Drawing.Point(944, 12);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 626);
            this.pictureBox7.TabIndex = 121;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox6.Location = new System.Drawing.Point(3, 12);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 626);
            this.pictureBox6.TabIndex = 120;
            this.pictureBox6.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.splitter2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.pictureBox8);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.pictureBox7);
            this.tabPage1.Controls.Add(this.pictureBox6);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(957, 650);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "관세척정보";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox8.Location = new System.Drawing.Point(13, 69);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(931, 9);
            this.pictureBox8.TabIndex = 123;
            this.pictureBox8.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.cheSelectAll);
            this.groupBox1.Controls.Add(this.texCondPcleanTitle);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ultraLabel7);
            this.groupBox1.Controls.Add(this.datStartDate);
            this.groupBox1.Controls.Add(this.datEndDate);
            this.groupBox1.Controls.Add(this.ultraLabel1);
            this.groupBox1.Controls.Add(this.butSelectPcleanData);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(931, 57);
            this.groupBox1.TabIndex = 122;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // cheSelectAll
            // 
            this.cheSelectAll.AutoSize = true;
            this.cheSelectAll.Checked = true;
            this.cheSelectAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheSelectAll.Location = new System.Drawing.Point(655, 25);
            this.cheSelectAll.Name = "cheSelectAll";
            this.cheSelectAll.Size = new System.Drawing.Size(72, 16);
            this.cheSelectAll.TabIndex = 191;
            this.cheSelectAll.Text = "전체조회";
            this.cheSelectAll.UseVisualStyleBackColor = true;
            // 
            // texCondPcleanTitle
            // 
            this.texCondPcleanTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texCondPcleanTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texCondPcleanTitle.Location = new System.Drawing.Point(311, 22);
            this.texCondPcleanTitle.Name = "texCondPcleanTitle";
            this.texCondPcleanTitle.Size = new System.Drawing.Size(338, 21);
            this.texCondPcleanTitle.TabIndex = 159;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 158;
            this.label3.Text = "명칭";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.AutoSize = true;
            this.ultraLabel7.Location = new System.Drawing.Point(162, 25);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(12, 14);
            this.ultraLabel7.TabIndex = 157;
            this.ultraLabel7.Text = "-";
            // 
            // datStartDate
            // 
            this.datStartDate.DateTime = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            this.datStartDate.Location = new System.Drawing.Point(62, 21);
            this.datStartDate.MaskInput = "{date}";
            this.datStartDate.Name = "datStartDate";
            this.datStartDate.Size = new System.Drawing.Size(100, 21);
            this.datStartDate.TabIndex = 154;
            this.datStartDate.Value = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            // 
            // datEndDate
            // 
            this.datEndDate.DateTime = new System.DateTime(2014, 10, 20, 0, 0, 0, 0);
            this.datEndDate.Location = new System.Drawing.Point(174, 21);
            this.datEndDate.MaskInput = "{date}";
            this.datEndDate.Name = "datEndDate";
            this.datEndDate.Size = new System.Drawing.Size(100, 21);
            this.datEndDate.TabIndex = 155;
            this.datEndDate.Value = new System.DateTime(2014, 10, 20, 0, 0, 0, 0);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Location = new System.Drawing.Point(7, 25);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(54, 16);
            this.ultraLabel1.TabIndex = 156;
            this.ultraLabel1.Text = "관세척일";
            // 
            // butSelectPcleanData
            // 
            this.butSelectPcleanData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectPcleanData.Location = new System.Drawing.Point(874, 21);
            this.butSelectPcleanData.Name = "butSelectPcleanData";
            this.butSelectPcleanData.Size = new System.Drawing.Size(52, 23);
            this.butSelectPcleanData.TabIndex = 149;
            this.butSelectPcleanData.Text = "조회";
            this.butSelectPcleanData.UseVisualStyleBackColor = true;
            this.butSelectPcleanData.Click += new System.EventHandler(this.butSelectPcleanData_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(3, 638);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(951, 9);
            this.pictureBox5.TabIndex = 119;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(3, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(951, 9);
            this.pictureBox4.TabIndex = 118;
            this.pictureBox4.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Controls.Add(this.splitter4);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.splitter3);
            this.tabPage3.Controls.Add(this.treBlockList);
            this.tabPage3.Controls.Add(this.pictureBox39);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.pictureBox38);
            this.tabPage3.Controls.Add(this.pictureBox37);
            this.tabPage3.Controls.Add(this.pictureBox36);
            this.tabPage3.Controls.Add(this.pictureBox35);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(863, 650);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "수질민원";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox9.Controls.Add(this.griComplaintList);
            this.groupBox9.Controls.Add(this.pictureBox49);
            this.groupBox9.Controls.Add(this.pictureBox48);
            this.groupBox9.Controls.Add(this.pictureBox47);
            this.groupBox9.Controls.Add(this.pictureBox46);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Location = new System.Drawing.Point(244, 213);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(606, 425);
            this.groupBox9.TabIndex = 155;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "민원상세";
            // 
            // griComplaintList
            // 
            this.griComplaintList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griComplaintList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griComplaintList.DisplayLayout.MaxColScrollRegions = 1;
            this.griComplaintList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griComplaintList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griComplaintList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griComplaintList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griComplaintList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griComplaintList.DisplayLayout.Override.CellPadding = 0;
            this.griComplaintList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griComplaintList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griComplaintList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griComplaintList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griComplaintList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.griComplaintList.DisplayLayout.Override.RowSelectorAppearance = appearance3;
            this.griComplaintList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griComplaintList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griComplaintList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griComplaintList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griComplaintList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griComplaintList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griComplaintList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griComplaintList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griComplaintList.Location = new System.Drawing.Point(13, 26);
            this.griComplaintList.Name = "griComplaintList";
            this.griComplaintList.Size = new System.Drawing.Size(580, 387);
            this.griComplaintList.TabIndex = 152;
            this.griComplaintList.Text = "ultraGrid1";
            this.griComplaintList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.griComplaintList_DoubleClickCell);
            // 
            // pictureBox49
            // 
            this.pictureBox49.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox49.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox49.Location = new System.Drawing.Point(593, 26);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(10, 387);
            this.pictureBox49.TabIndex = 160;
            this.pictureBox49.TabStop = false;
            // 
            // pictureBox48
            // 
            this.pictureBox48.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox48.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox48.Location = new System.Drawing.Point(3, 26);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(10, 387);
            this.pictureBox48.TabIndex = 159;
            this.pictureBox48.TabStop = false;
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox47.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox47.Location = new System.Drawing.Point(3, 413);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(600, 9);
            this.pictureBox47.TabIndex = 158;
            this.pictureBox47.TabStop = false;
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox46.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox46.Location = new System.Drawing.Point(3, 17);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(600, 9);
            this.pictureBox46.TabIndex = 157;
            this.pictureBox46.TabStop = false;
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter4.Location = new System.Drawing.Point(244, 203);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(606, 10);
            this.splitter4.TabIndex = 159;
            this.splitter4.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox8.Controls.Add(this.griComplaintState);
            this.groupBox8.Controls.Add(this.pictureBox44);
            this.groupBox8.Controls.Add(this.pictureBox43);
            this.groupBox8.Controls.Add(this.pictureBox42);
            this.groupBox8.Controls.Add(this.pictureBox41);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox8.Location = new System.Drawing.Point(244, 78);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(606, 125);
            this.groupBox8.TabIndex = 154;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "기간 내 수질민원 현황";
            // 
            // griComplaintState
            // 
            this.griComplaintState.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griComplaintState.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griComplaintState.DisplayLayout.MaxColScrollRegions = 1;
            this.griComplaintState.DisplayLayout.MaxRowScrollRegions = 1;
            this.griComplaintState.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griComplaintState.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griComplaintState.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griComplaintState.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griComplaintState.DisplayLayout.Override.CellPadding = 0;
            this.griComplaintState.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griComplaintState.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griComplaintState.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griComplaintState.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griComplaintState.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.griComplaintState.DisplayLayout.Override.RowSelectorAppearance = appearance4;
            this.griComplaintState.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griComplaintState.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griComplaintState.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griComplaintState.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griComplaintState.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griComplaintState.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griComplaintState.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griComplaintState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griComplaintState.Location = new System.Drawing.Point(13, 26);
            this.griComplaintState.Name = "griComplaintState";
            this.griComplaintState.Size = new System.Drawing.Size(580, 87);
            this.griComplaintState.TabIndex = 158;
            this.griComplaintState.Text = "ultraGrid1";
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox44.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox44.Location = new System.Drawing.Point(13, 113);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(580, 9);
            this.pictureBox44.TabIndex = 157;
            this.pictureBox44.TabStop = false;
            // 
            // pictureBox43
            // 
            this.pictureBox43.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox43.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox43.Location = new System.Drawing.Point(13, 17);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(580, 9);
            this.pictureBox43.TabIndex = 156;
            this.pictureBox43.TabStop = false;
            // 
            // pictureBox42
            // 
            this.pictureBox42.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox42.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox42.Location = new System.Drawing.Point(593, 17);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(10, 105);
            this.pictureBox42.TabIndex = 155;
            this.pictureBox42.TabStop = false;
            // 
            // pictureBox41
            // 
            this.pictureBox41.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox41.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox41.Location = new System.Drawing.Point(3, 17);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(10, 105);
            this.pictureBox41.TabIndex = 154;
            this.pictureBox41.TabStop = false;
            // 
            // splitter3
            // 
            this.splitter3.Location = new System.Drawing.Point(234, 78);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(10, 560);
            this.splitter3.TabIndex = 158;
            this.splitter3.TabStop = false;
            // 
            // treBlockList
            // 
            this.treBlockList.Dock = System.Windows.Forms.DockStyle.Left;
            this.treBlockList.Location = new System.Drawing.Point(13, 78);
            this.treBlockList.Name = "treBlockList";
            this.treBlockList.Size = new System.Drawing.Size(221, 560);
            this.treBlockList.TabIndex = 0;
            this.treBlockList.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treBlockList_AfterSelect);
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox39.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox39.Location = new System.Drawing.Point(13, 69);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(837, 9);
            this.pictureBox39.TabIndex = 125;
            this.pictureBox39.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox7.Controls.Add(this.ultraLabel4);
            this.groupBox7.Controls.Add(this.comComplaintType);
            this.groupBox7.Controls.Add(this.datComEndDate);
            this.groupBox7.Controls.Add(this.cheShowComplaint);
            this.groupBox7.Controls.Add(this.ultraLabel2);
            this.groupBox7.Controls.Add(this.datComStartDate);
            this.groupBox7.Controls.Add(this.ultraLabel3);
            this.groupBox7.Controls.Add(this.butSelectComplaint);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Location = new System.Drawing.Point(13, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(837, 57);
            this.groupBox7.TabIndex = 124;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "검색조건";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.AutoSize = true;
            this.ultraLabel4.Location = new System.Drawing.Point(298, 25);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(54, 16);
            this.ultraLabel4.TabIndex = 193;
            this.ultraLabel4.Text = "민원구분";
            // 
            // comComplaintType
            // 
            this.comComplaintType.FormattingEnabled = true;
            this.comComplaintType.Location = new System.Drawing.Point(358, 23);
            this.comComplaintType.Name = "comComplaintType";
            this.comComplaintType.Size = new System.Drawing.Size(134, 20);
            this.comComplaintType.TabIndex = 192;
            // 
            // datComEndDate
            // 
            this.datComEndDate.DateTime = new System.DateTime(2014, 10, 20, 0, 0, 0, 0);
            this.datComEndDate.Location = new System.Drawing.Point(174, 21);
            this.datComEndDate.MaskInput = "{date}";
            this.datComEndDate.Name = "datComEndDate";
            this.datComEndDate.Size = new System.Drawing.Size(100, 21);
            this.datComEndDate.TabIndex = 191;
            this.datComEndDate.Value = new System.DateTime(2014, 10, 20, 0, 0, 0, 0);
            // 
            // cheShowComplaint
            // 
            this.cheShowComplaint.AutoSize = true;
            this.cheShowComplaint.Location = new System.Drawing.Point(541, 25);
            this.cheShowComplaint.Name = "cheShowComplaint";
            this.cheShowComplaint.Size = new System.Drawing.Size(100, 16);
            this.cheShowComplaint.TabIndex = 190;
            this.cheShowComplaint.Text = "민원내역 표출";
            this.cheShowComplaint.UseVisualStyleBackColor = true;
            this.cheShowComplaint.CheckedChanged += new System.EventHandler(this.cheShowComplaint_CheckedChanged);
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Location = new System.Drawing.Point(162, 25);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 14);
            this.ultraLabel2.TabIndex = 157;
            this.ultraLabel2.Text = "-";
            // 
            // datComStartDate
            // 
            this.datComStartDate.DateTime = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            this.datComStartDate.Location = new System.Drawing.Point(62, 21);
            this.datComStartDate.MaskInput = "{date}";
            this.datComStartDate.Name = "datComStartDate";
            this.datComStartDate.Size = new System.Drawing.Size(100, 21);
            this.datComStartDate.TabIndex = 154;
            this.datComStartDate.Value = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.AutoSize = true;
            this.ultraLabel3.Location = new System.Drawing.Point(7, 25);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(54, 16);
            this.ultraLabel3.TabIndex = 156;
            this.ultraLabel3.Text = "등록일자";
            // 
            // butSelectComplaint
            // 
            this.butSelectComplaint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectComplaint.Location = new System.Drawing.Point(779, 22);
            this.butSelectComplaint.Name = "butSelectComplaint";
            this.butSelectComplaint.Size = new System.Drawing.Size(52, 23);
            this.butSelectComplaint.TabIndex = 149;
            this.butSelectComplaint.Text = "조회";
            this.butSelectComplaint.UseVisualStyleBackColor = true;
            this.butSelectComplaint.Click += new System.EventHandler(this.butSelectComplaint_Click);
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox38.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox38.Location = new System.Drawing.Point(850, 12);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(10, 626);
            this.pictureBox38.TabIndex = 123;
            this.pictureBox38.TabStop = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox37.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox37.Location = new System.Drawing.Point(3, 12);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(10, 626);
            this.pictureBox37.TabIndex = 122;
            this.pictureBox37.TabStop = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox36.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox36.Location = new System.Drawing.Point(3, 638);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(857, 9);
            this.pictureBox36.TabIndex = 121;
            this.pictureBox36.TabStop = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox35.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox35.Location = new System.Drawing.Point(3, 3);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(857, 9);
            this.pictureBox35.TabIndex = 120;
            this.pictureBox35.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(10, 9);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(965, 676);
            this.tabControl1.TabIndex = 127;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.pictureBox34);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.pictureBox33);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.pictureBox22);
            this.tabPage2.Controls.Add(this.pictureBox21);
            this.tabPage2.Controls.Add(this.pictureBox20);
            this.tabPage2.Controls.Add(this.pictureBox19);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(863, 650);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "관망해석";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.griModelList);
            this.groupBox4.Controls.Add(this.pictureBox25);
            this.groupBox4.Controls.Add(this.pictureBox26);
            this.groupBox4.Controls.Add(this.pictureBox27);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(13, 78);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(837, 468);
            this.groupBox4.TabIndex = 158;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "수질모델";
            // 
            // griModelList
            // 
            this.griModelList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griModelList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griModelList.DisplayLayout.MaxColScrollRegions = 1;
            this.griModelList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griModelList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griModelList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griModelList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griModelList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griModelList.DisplayLayout.Override.CellPadding = 0;
            this.griModelList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griModelList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griModelList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griModelList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griModelList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.griModelList.DisplayLayout.Override.RowSelectorAppearance = appearance5;
            this.griModelList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griModelList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griModelList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griModelList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griModelList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griModelList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griModelList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griModelList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griModelList.Location = new System.Drawing.Point(13, 26);
            this.griModelList.Name = "griModelList";
            this.griModelList.Size = new System.Drawing.Size(811, 439);
            this.griModelList.TabIndex = 151;
            this.griModelList.Text = "ultraGrid1";
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox25.Location = new System.Drawing.Point(13, 17);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(811, 9);
            this.pictureBox25.TabIndex = 123;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox26.Location = new System.Drawing.Point(824, 17);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(10, 448);
            this.pictureBox26.TabIndex = 121;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox27.Location = new System.Drawing.Point(3, 17);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(10, 448);
            this.pictureBox27.TabIndex = 120;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox34.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox34.Location = new System.Drawing.Point(13, 546);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(837, 9);
            this.pictureBox34.TabIndex = 163;
            this.pictureBox34.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox5.Controls.Add(this.checAnnotation);
            this.groupBox5.Controls.Add(this.cheVelocity);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.comVelocityFormula);
            this.groupBox5.Controls.Add(this.texVelocityBaseValue);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.butExecuteAnalysis);
            this.groupBox5.Controls.Add(this.pictureBox29);
            this.groupBox5.Controls.Add(this.pictureBox30);
            this.groupBox5.Controls.Add(this.pictureBox31);
            this.groupBox5.Controls.Add(this.pictureBox32);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox5.Location = new System.Drawing.Point(13, 555);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(837, 83);
            this.groupBox5.TabIndex = 159;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "수질해석";
            // 
            // checAnnotation
            // 
            this.checAnnotation.AutoSize = true;
            this.checAnnotation.Checked = true;
            this.checAnnotation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checAnnotation.Location = new System.Drawing.Point(525, 42);
            this.checAnnotation.Name = "checAnnotation";
            this.checAnnotation.Size = new System.Drawing.Size(83, 16);
            this.checAnnotation.TabIndex = 190;
            this.checAnnotation.Text = "Label 표출";
            this.checAnnotation.UseVisualStyleBackColor = true;
            this.checAnnotation.CheckedChanged += new System.EventHandler(this.checAnnotation_CheckedChanged);
            // 
            // cheVelocity
            // 
            this.cheVelocity.AutoSize = true;
            this.cheVelocity.Checked = true;
            this.cheVelocity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheVelocity.Location = new System.Drawing.Point(423, 42);
            this.cheVelocity.Name = "cheVelocity";
            this.cheVelocity.Size = new System.Drawing.Size(96, 16);
            this.cheVelocity.TabIndex = 189;
            this.cheVelocity.Text = "해석결과표출";
            this.cheVelocity.UseVisualStyleBackColor = true;
            this.cheVelocity.CheckedChanged += new System.EventHandler(this.cheVelocity_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(200, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 188;
            this.label8.Text = "m/s";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(330, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 12);
            this.label9.TabIndex = 187;
            this.label9.Text = "항목만 표출";
            // 
            // comVelocityFormula
            // 
            this.comVelocityFormula.FormattingEnabled = true;
            this.comVelocityFormula.Location = new System.Drawing.Point(243, 40);
            this.comVelocityFormula.Name = "comVelocityFormula";
            this.comVelocityFormula.Size = new System.Drawing.Size(81, 20);
            this.comVelocityFormula.TabIndex = 186;
            this.comVelocityFormula.SelectedIndexChanged += new System.EventHandler(this.comVelocityFormula_SelectedIndexChanged);
            // 
            // texVelocityBaseValue
            // 
            this.texVelocityBaseValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texVelocityBaseValue.Location = new System.Drawing.Point(131, 39);
            this.texVelocityBaseValue.Name = "texVelocityBaseValue";
            this.texVelocityBaseValue.Size = new System.Drawing.Size(68, 21);
            this.texVelocityBaseValue.TabIndex = 185;
            this.texVelocityBaseValue.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texVelocityBaseValue_KeyUp);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(88, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 184;
            this.label10.Text = "유속이";
            // 
            // butExecuteAnalysis
            // 
            this.butExecuteAnalysis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butExecuteAnalysis.Location = new System.Drawing.Point(757, 38);
            this.butExecuteAnalysis.Name = "butExecuteAnalysis";
            this.butExecuteAnalysis.Size = new System.Drawing.Size(67, 23);
            this.butExecuteAnalysis.TabIndex = 171;
            this.butExecuteAnalysis.Text = "해석실행";
            this.butExecuteAnalysis.UseVisualStyleBackColor = true;
            this.butExecuteAnalysis.Click += new System.EventHandler(this.butExecuteAnalysis_Click);
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox29.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox29.Location = new System.Drawing.Point(13, 71);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(811, 9);
            this.pictureBox29.TabIndex = 124;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox30.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox30.Location = new System.Drawing.Point(13, 17);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(811, 9);
            this.pictureBox30.TabIndex = 123;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox31.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox31.Location = new System.Drawing.Point(824, 17);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(10, 63);
            this.pictureBox31.TabIndex = 121;
            this.pictureBox31.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox32.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox32.Location = new System.Drawing.Point(3, 17);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(10, 63);
            this.pictureBox32.TabIndex = 120;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox33.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox33.Location = new System.Drawing.Point(13, 69);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(837, 9);
            this.pictureBox33.TabIndex = 162;
            this.pictureBox33.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox6.Controls.Add(this.texTitle);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.butSelectModelList);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(13, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(837, 57);
            this.groupBox6.TabIndex = 161;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "검색조건";
            // 
            // texTitle
            // 
            this.texTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texTitle.Location = new System.Drawing.Point(39, 22);
            this.texTitle.Name = "texTitle";
            this.texTitle.Size = new System.Drawing.Size(409, 21);
            this.texTitle.TabIndex = 159;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 158;
            this.label11.Text = "제목";
            // 
            // butSelectModelList
            // 
            this.butSelectModelList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectModelList.Location = new System.Drawing.Point(780, 21);
            this.butSelectModelList.Name = "butSelectModelList";
            this.butSelectModelList.Size = new System.Drawing.Size(52, 23);
            this.butSelectModelList.TabIndex = 149;
            this.butSelectModelList.Text = "조회";
            this.butSelectModelList.UseVisualStyleBackColor = true;
            this.butSelectModelList.Click += new System.EventHandler(this.butSelectModelList_Click);
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox22.Location = new System.Drawing.Point(850, 12);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(10, 626);
            this.pictureBox22.TabIndex = 122;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox21.Location = new System.Drawing.Point(3, 12);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(10, 626);
            this.pictureBox21.TabIndex = 121;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox20.Location = new System.Drawing.Point(3, 638);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(857, 9);
            this.pictureBox20.TabIndex = 120;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox19.Location = new System.Drawing.Point(3, 3);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(857, 9);
            this.pictureBox19.TabIndex = 119;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(975, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 676);
            this.pictureBox3.TabIndex = 126;
            this.pictureBox3.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 676);
            this.picFrLeftM1.TabIndex = 125;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 685);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(985, 9);
            this.pictureBox1.TabIndex = 124;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(985, 9);
            this.pictureBox2.TabIndex = 123;
            this.pictureBox2.TabStop = false;
            // 
            // frmPipeCleaning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 694);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(808, 611);
            this.Name = "frmPipeCleaning";
            this.Text = "관세척구간관리";
            this.Load += new System.EventHandler(this.frmPipeCleaning_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPipeCleaning_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griPipe)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griPipeCleaning)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datProcDat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griComplaintList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griComplaintState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treBlockList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datComEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datComStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griModelList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button butSavePcleanData;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button butSavePipe;
        private System.Windows.Forms.Button butDelPipe;
        private Infragistics.Win.UltraWinGrid.UltraGrid griPipe;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox texRemark;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox texProcResult;
        public Infragistics.Win.UltraWinGrid.UltraGrid griPipeCleaning;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button butDelPcleanData;
        private System.Windows.Forms.Button butAddPcleanData;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox texProcMethod;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datProcDat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox texPcleanTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox texCondPcleanTitle;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datStartDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private System.Windows.Forms.Button butSelectPcleanData;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.GroupBox groupBox4;
        public Infragistics.Win.UltraWinGrid.UltraGrid griModelList;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.Button butExecuteAnalysis;
        private System.Windows.Forms.CheckBox cheVelocity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comVelocityFormula;
        private System.Windows.Forms.TextBox texVelocityBaseValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox texTitle;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button butSelectModelList;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.GroupBox groupBox7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datComStartDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private System.Windows.Forms.Button butSelectComplaint;
        private System.Windows.Forms.PictureBox pictureBox39;
        private Infragistics.Win.UltraWinTree.UltraTree treBlockList;
        private System.Windows.Forms.GroupBox groupBox9;
        public Infragistics.Win.UltraWinGrid.UltraGrid griComplaintList;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.GroupBox groupBox8;
        public Infragistics.Win.UltraWinGrid.UltraGrid griComplaintState;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.CheckBox cheShowComplaint;
        private System.Windows.Forms.Splitter splitter4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datComEndDate;
        private System.Windows.Forms.CheckBox checAnnotation;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox texProcMethodTitle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox texProcResultTitle;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Splitter splitter5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private System.Windows.Forms.ComboBox comComplaintType;
        private System.Windows.Forms.CheckBox cheSelectAll;

    }
}