﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.work;
using System.Collections;
using ChartFX.WinForms;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmRealtimeQualityChainChart : Form
    {
        #region 1.전역번수

        private RealtimeDataWork work = null;
        public string chainNo = "";
        public string chainNm = "";
        public string formNo = "";              //여러개가 열릴 수 있기에 열릴때 고유번호를 할당받는다. (Close될때 Pool에 있는 자신의 객체를 찾아내서 처리)

        public frmChartSetting chartSettingForm = null;

        #endregion

        #region 2.초기화

        public frmRealtimeQualityChainChart()
        {
            InitializeComponent();
            InitializeSetting();

            work = RealtimeDataWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //폼 최초 로딩 시 실행
        private void frmRealtimeQualityChainChart_Load(object sender, EventArgs e)
        {
            //*****임시로직*****
            //datTimestamp.Value = "2014-07-01 01:01";
            datTimestamp.Value = DateTime.Now.ToString("yyyy-MM-dd");

            SelectRealtimeQualityChainData();

            this.Text = chainNm;        //계통 제목 표시
        }

        //폼이 닫길때 실행
        private void frmRealtimeQualityChainChart_FormClosing(object sender, FormClosingEventArgs e)
        {
            (this.Owner as frmQualityAnalysisMap).qualityChainForms.Remove(formNo);
        }

        //1일 전 버튼 클릭 시 실행
        private void butBeforeOneDay_Click(object sender, EventArgs e)
        {
            datTimestamp.Value = ((DateTime)datTimestamp.Value).AddDays(-1).ToString("yyyy-MM-dd HH:mm");
        }

        //1주 전 버튼 클릭 시 실행
        private void butBeforeOneWeek_Click(object sender, EventArgs e)
        {
            datTimestamp.Value = ((DateTime)datTimestamp.Value).AddDays(-7).ToString("yyyy-MM-dd HH:mm");
        }

        //1개월 전 버튼 클릭 시 실행
        private void butBeforeOneMonth_Click(object sender, EventArgs e)
        {
            datTimestamp.Value = ((DateTime)datTimestamp.Value).AddMonths(-1).ToString("yyyy-MM-dd HH:mm");
        }

        //1일 후 버튼 클릭 시 실행
        private void butAfterOneDay_Click(object sender, EventArgs e)
        {
            datTimestamp.Value = ((DateTime)datTimestamp.Value).AddDays(1).ToString("yyyy-MM-dd HH:mm");
        }

        //1주 후 버튼 클릭 시 실행
        private void butAfterOneWeek_Click(object sender, EventArgs e)
        {
            datTimestamp.Value = ((DateTime)datTimestamp.Value).AddDays(7).ToString("yyyy-MM-dd HH:mm");
        }

        //1개월 후 버튼 클릭 시 실행
        private void butAfterOneMonth_Click(object sender, EventArgs e)
        {
            datTimestamp.Value = ((DateTime)datTimestamp.Value).AddMonths(1).ToString("yyyy-MM-dd HH:mm");
        }

        //조회버튼 클릭 시 실행
        private void butSelectRealtimeQualityChain_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectRealtimeQualityChainData();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //차트 더블클릭 시 실행
        private void chaRealtimeQualityChain_MouseDoubleClick(object sender, HitTestEventArgs e)
        {
            e = chaRealtimeQualityChain.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);
            if (e.HitType.ToString() == "Axis")
            {
                Axis hitAxis = (Axis)e.Object;

                if (chartSettingForm == null)
                {
                    chartSettingForm = new frmChartSetting();
                    chartSettingForm.SetAxisData(hitAxis);
                    chartSettingForm.Owner = this;
                    chartSettingForm.Show();
                }
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        #endregion

        #region 6.내부사용

        //Grid 초기화
        private void InitializeSetting()
        {
            UltraGridColumn realtimeColumn;

            realtimeColumn = griRealtimeQualityChain.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "MONITOR_NO";
            realtimeColumn.Header.Caption = "지점번호";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeColumn.Width = 100;
            realtimeColumn.Hidden = true;   //필드 보이기

            realtimeColumn = griRealtimeQualityChain.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "MONPNT";
            realtimeColumn.Header.Caption = "지점명";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Left;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeColumn.Width = 300;
            realtimeColumn.Hidden = false;   //필드 보이기

            realtimeColumn = griRealtimeQualityChain.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "TAG_ID_CL";
            realtimeColumn.Header.Caption = "TAG";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Left;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeColumn.Width = 100;
            realtimeColumn.Hidden = true;   //필드 보이기

            realtimeColumn = griRealtimeQualityChain.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "VALUE";
            realtimeColumn.Header.Caption = "잔류염소 (mg/ℓ)";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Right;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            realtimeColumn.Width = 100;
            realtimeColumn.Hidden = false;   //필드 보이기
            realtimeColumn.Format = "F4";    //소수점 이하 두째자리까지 표현

            //차트 초기화
            chaRealtimeQualityChain.LegendBox.MarginY = 1;
            chaRealtimeQualityChain.Data.Clear();
            chaRealtimeQualityChain.Data.Series = 1;
            chaRealtimeQualityChain.Series[0].Color = Color.Crimson;

            chaRealtimeQualityChain.AxesX[0].LabelTrimming = StringTrimming.None;
            chaRealtimeQualityChain.AxesY[0].LabelTrimming = StringTrimming.None;

            //차트 Data Field 설정
            chaRealtimeQualityChain.DataSourceSettings.Fields.Add(new FieldMap("MONPNT", FieldUsage.Label));
            chaRealtimeQualityChain.DataSourceSettings.Fields.Add(new FieldMap("VALUE", FieldUsage.Value));

            //Y축 설정
            chaRealtimeQualityChain.AxisY.Title.Text = "잔류염소(mg/ℓ)";
            chaRealtimeQualityChain.AxisY.LabelsFormat.Decimals = 4;        //y축 값을 소수점 2째자리까지 설정

            //legend 설정
            LegendItemAttributes basicItem = new LegendItemAttributes();
            basicItem.Visible = false;
            chaRealtimeQualityChain.LegendBox.ItemAttributes[chaRealtimeQualityChain.Series] = basicItem;        //기본 legend 비활성화

            CustomLegendItem legendItem = new CustomLegendItem();
            legendItem.Text = "잔류염소";
            legendItem.Color = Color.Crimson;
            legendItem.MarkerShape = MarkerShape.HorizontalLine;
            chaRealtimeQualityChain.LegendBox.CustomItems.Add(legendItem);
        }

        //실시간 계통데이터 조회
        private void SelectRealtimeQualityChainData()
        {
            try
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("TIMESTAMP", ((DateTime)datTimestamp.Value).ToString("yyyyMMddHHmm"));
                conditions.Add("CHAIN_NO", chainNo);

                DataTable realtimeData = work.SelectChainRealtimeDataList(conditions);

                griRealtimeQualityChain.DataSource = realtimeData;
                chaRealtimeQualityChain.DataSource = realtimeData;

                if (griRealtimeQualityChain.Rows.Count > 0)
                {
                    griRealtimeQualityChain.Rows[0].Selected = true;
                    griRealtimeQualityChain.Rows[0].Activate();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                CommonUtils.ShowExceptionMessage(e.Message);
            }
        }

        #endregion       
    }
}
