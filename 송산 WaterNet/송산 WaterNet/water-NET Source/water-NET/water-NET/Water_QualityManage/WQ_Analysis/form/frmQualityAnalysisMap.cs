﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Timers;

using WaterNet.WaterAOCore;
using WaterNet.WQ_Analysis.work;
using WaterNet.WQ_Analysis.form;

using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;

using stdole;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmQualityAnalysisMap : WaterAOCore.frmMap, WaterNetCore.IForminterface
    {
        #region 1.전역번수

        private Hashtable monitoringCpList = new Hashtable();           //수질계측지점 폼배열
        private System.Timers.Timer timer = new System.Timers.Timer();  //실시간 데이터를 반영하기 위한 타이머
       
        private RealtimeDataWork work = null;
        private AnalysisMapWork mWork = null;

        //화면객체
        public frmRealtimeQuality realtimeQualityForm = null;               //실시간 데이터 조회
        public Hashtable qualityChainForms = new Hashtable();        //
        public frmRechlorineSimulation rechlorineSimulationForm = null;     //재염소 모의
        public frmPipeCleaning pipeCleaningForm = null;                     //관세척구간 관리
        public frmMonitorPointManage monitorPointManageForm = null;         //수질감시지점 관리 : 2015-03-12_shpark
        public frmChainManage chainManageForm = null;                       //계통관리 : 2015-03-12_shpark

        // -- Layer 조작 후 원래상태로 돌아가기 위해 원래 상태값을 저장

        //지도 기본 랜더러 
        private byte[] junctionDefaultRenderer = null;                      
        private byte[] reservoirDefaultRenderer = null;                     
        private byte[] tankDefaultRenderer = null;                          
        private byte[] pipeDefaultRenderer = null;                          
        private byte[] valveDefaultRenderer = null;                         
        private byte[] pumpDefaultRenderer = null;

        private byte[] junctionRenderer = null;
        private byte[] reservoirRenderer = null;
        private byte[] tankRenderer = null;
        private byte[] pipeRenderer = null;
        private byte[] valveRenderer = null;
        private byte[] pumpRenderer = null;          

        private byte[] shapePipeDefaultRenderer = null;                     //파이프 랜더러
        private byte[] shapeMeterDefaultRenderer = null;                    //수도계량기 랜더러

        private double shapePipeLayerMin = 0;                               //상수관로 심볼 표시 최저스케일
        private double shapePipeLayerMax = 0;                               //상수관로 심볼 표시 최대스케일

        private double shapeMeterLayerMin = 0;                              //수도계량기(수용가) 심볼 표시 최저스케일
        private double shapeMeterLayerMax = 0;                              //수도계량기(수용가) 심볼 표시 최대스케일

        //지도에 모델이 로드되었는지 확인하기 위한 변수
        private string loadedInpNumber = "";

        private bool isShowRtMonitor = false;

        #endregion

        #region 2.초기화

        public frmQualityAnalysisMap()
        {
            InitializeComponent();
            InitializeSetting();

            work = RealtimeDataWork.GetInstance();
            mWork = AnalysisMapWork.GetInstance();

            //실시간 수질 표출에 사용할 타이머
            timer.Interval = 1000 * 60 * 1;
            timer.Elapsed += new ElapsedEventHandler(ExecuteTimerEvent);
            //timer.Start();
        }

        #endregion

        #region 3.객체 이벤트

        //폼 로드 시 실행
        private void frmQualityAnalysisMap_Load(object sender, EventArgs e)
        {
            //특정 Layer 비활성화 (임시)
            IFeatureLayer tmpLayer1 = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "대블록");
            IFeatureLayer tmpLayer2 = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
            IFeatureLayer tmpLayer3 = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "가압장");
            IFeatureLayer tmpLayer4 = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "수압계");
            IFeatureLayer tmpLayer5 = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "급수관로");
            tmpLayer1.Visible = false;
            tmpLayer2.Visible = false;
            tmpLayer3.Visible = false;
            tmpLayer4.Visible = false;
            tmpLayer5.Visible = false;

            ILayer positionLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "지점정보");
            if (positionLayer != null) positionLayer.Visible = true;

            //교하지구로 Focus 이동 (임시)
            IFeatureLayer pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");

            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '345M0001'");
            if (pFeature == null) return;

            axMap.Extent = pFeature.Shape.Envelope;
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);

            //사용자 확인. 개발팀이 아닐경우 '수질감시지점관리', '수질계통관리' 버튼을 숨긴다. 2015-03-16_shpark
            //if (!"개발팀".Equals(EMFrame.statics.AppStatic.USER_DIVISION))
            //{
            //    butMonitorPointManage.Visible = false;
            //    butChainManage.Visible = false;
            //}

            frmDashboard dashboard = new frmDashboard();
            dashboard.Owner = this;
            dashboard.Show();
        }

        //지도의 축척이 변경되었을 경우
        private void axMap_OnExtentUpdated(object sender, IMapControlEvents2_OnExtentUpdatedEvent e)
        {
            IEnvelope pEnvelope = e.newEnvelope as IEnvelope;
            ExtentUpdatedEvent(pEnvelope as IEnvelope);
        }

        //전체 폼의 사이즈가 변경되었을 경우
        private void frmQualityAnalysisMap_SizeChanged(object sender, EventArgs e)
        {
            IEnvelope pEnvelope = axMap.ActiveView.Extent;
            ExtentUpdatedEvent(pEnvelope as IEnvelope);
        }

        //실시간 데이터를 적용하기 위해 타이머에 걸린 이벤트
        private void ExecuteTimerEvent(System.Object sender, EventArgs eArgs)
        {
            ApplyRealtimeData();        //실시간 데이터 적용
        }

        //실시간 수질현황 버튼 클릭 시
        private void butRealtimeState_Click(object sender, EventArgs e)
        {
            //실시간 모니터링이 활성화된 상태라면 비활성화 시킴 (Toggle기능)
            if (isShowRtMonitor == true)
            {
                isShowRtMonitor = false;
                DisableRealtimeViewing();
            }
            else
            {
                //다른 Form들 전부다 닫음
                CloseFormAction("frmRealtimeQuality");
                CloseFormAction("frmRechlorineSimulation");
                CloseFormAction("frmPipeCleaning");
                CloseFormAction("qualityChainForm");
                CloseFormAction("frmMonitorPointManage");       //수질감시지점관리 : 2015-03-16_shpark
                CloseFormAction("frmChainManage");              //계통관리 : 2015-03-16_shpark
                
                isShowRtMonitor = true;

                ApplyRealtimeData();
                EnableRealtimeViewing();
            }
        }

        //실시간 데이터 조회 버튼 클릭 시
        private void butRealtimeData_Click(object sender, EventArgs e)
        {
            try
            {
                isShowRtMonitor = false;        //실시간 감시모드 비활성화
                OpenFormAction("frmRealtimeQuality");

                if (realtimeQualityForm == null)
                {
                    realtimeQualityForm = new frmRealtimeQuality();
                    realtimeQualityForm.Owner = this;
                    realtimeQualityForm.Show();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.ToString());
            }
        }

        //계통별 시점조회 (계통도) 버튼 클릭 시
        private void butSelectQualityChain_Click(object sender, EventArgs e)
        {
            try
            {
                isShowRtMonitor = false;        //실시간 감시모드 비활성화
                OpenFormAction("frmQualityChain");

                frmQualityChainSelect qualityChainForm = new frmQualityChainSelect();
                qualityChainForm.Owner = this;
                qualityChainForm.ShowDialog();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //잔류염소 모의 버튼 클릭 시
        private void butChlorineAnalysis_Click(object sender, EventArgs e)
        {
            try
            {
                isShowRtMonitor = false;        //실시간 감시모드 비활성화
                OpenFormAction("frmRechlorineSimulation");

                if (rechlorineSimulationForm == null)
                {
                    DisableRealtimeViewing();

                    rechlorineSimulationForm = new frmRechlorineSimulation();
                    rechlorineSimulationForm.Owner = this;
                    rechlorineSimulationForm.Show();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //관세척 관리 버튼 클릭 시
        private void butPipeCleaning_Click(object sender, EventArgs e)
        {
            try
            {
                isShowRtMonitor = false;        //실시간 감시모드 비활성화
                OpenFormAction("frmPipeCleaning");

                if (pipeCleaningForm == null)
                {
                    DisableRealtimeViewing();

                    pipeCleaningForm = new frmPipeCleaning();
                    pipeCleaningForm.Owner = this;
                    pipeCleaningForm.Show();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //수질감시지점관리 버튼 클릭 시 : 2014-12-04_shpark
        private void butMonitorPointManage_Click(object sender, EventArgs e)
        {
            try
            {
                isShowRtMonitor = false;        //실시간 감시모드 비활성화
                OpenFormAction("frmMonitorPointManage");

                if (monitorPointManageForm == null)
                {
                    monitorPointManageForm = new frmMonitorPointManage();
                    monitorPointManageForm.Owner = this;
                    monitorPointManageForm.Show();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.ToString());
            }
        }

        //계통관리 버튼 클릭 시 : 2015-1-27_shpark
        private void butChainManage_Click(object sender, EventArgs e)
        {
            try
            {
                isShowRtMonitor = false;        //실시간 감시모드 비활성화
                OpenFormAction("frmChainManage");

                if (chainManageForm == null)
                {
                    chainManageForm = new frmChainManage();
                    chainManageForm.Owner = this;
                    chainManageForm.Show();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.ToString());
            }
        }

        //Map을 Click 시
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            try
            {
                if (toolActionCommand.Checked)
                {
                    //checkpoint form이 열려있고 Toolbar가 Select Features일때만 구동
                    ////GIS 상 좌표
                    IPoint pPoint = new PointClass();
                    pPoint.X = e.mapX;
                    pPoint.Y = e.mapY;
                    pPoint.Z = 0;

                    double dblContains = 0;

                    if (ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)100000)
                    {
                        dblContains = 300;
                    }
                    else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)100000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)50000)
                    {
                        dblContains = 200;
                    }
                    else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)50000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)15000)
                    {
                        dblContains = 100;
                    }
                    else
                    {
                        dblContains = 10;
                    }

                    if (rechlorineSimulationForm != null)       //재염소 모의가 열린경우
                    {
                        ILayer junctionLayer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "JUNCTION");
                        ILayer tankLayer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "TANK");
                        ILayer reservoirLayer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "RESERVOIR");

                        IGeometry pGeom = (IGeometry)pPoint;

                        string result = "";

                        if (!"".Equals(result = GetMapObjectId(reservoirLayer, pGeom, dblContains))) { }
                        else if (!"".Equals(result = GetMapObjectId(tankLayer, pGeom, dblContains))) { }
                        else if (!"".Equals(result = GetMapObjectId(junctionLayer, pGeom, dblContains))) { }

                        rechlorineSimulationForm.SetMapPointId(result, e.mapX.ToString(), e.mapY.ToString());
                    }
                    else if (pipeCleaningForm != null)
                    {
                        //관세척정보를 저장하지 않은경우 저장 후 실행하라는 메세지 호출
                        if (CommonUtils.GetRowState(pipeCleaningForm.griPipeCleaning) == DataRowState.Added)
                        {
                            CommonUtils.ShowInformationMessage("관세척정보를 저장하고 실행하십시오.");
                            return;
                        }

                        ILayer pipeLayer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "상수관로");

                        IRubberBand pRubberBand = new RubberEnvelopeClass();
                        IEnvelope env = (IEnvelope)pRubberBand.TrackNew(this.axMap.ActiveView.ScreenDisplay, null);

                        IGeometry pGeom = (IGeometry)env;

                        if (env == null | env.IsEmpty) return;

                        pipeCleaningForm.SetPipeList(GetMapObjectIds(pipeLayer, pGeom, dblContains));
                    }
                    else if (monitorPointManageForm != null)        //수질감시지점관리 : 2015-03-16_shpark
                    {
                        //맵 클릭 시 해당 좌표를 X,Y 좌표 텍스트 박스에 각각 입력해준다.
                        if (!monitorPointManageForm.texMapX.Text.Equals(string.Empty))
                        {
                            if (CommonUtils.ShowYesNoDialog("이미 좌표가 등록되어 있습니다.\n새로운 좌표로 교체하시겠습니까?") == DialogResult.No)
                            {
                                return;
                            }
                        }

                        monitorPointManageForm.texMapX.Text = e.mapX.ToString();
                        monitorPointManageForm.texMapY.Text = e.mapY.ToString();
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #region 4.상속구현

        public override void Open()
        {
            base.Open();
            //추가적인 구현은 여기에....
            //Viewmap_Load();

            MakeControls();
        }

        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
            //DockManagerCommand.Visible = true;
        }

        public string FormID
        {
            get { return "수질모의"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        #region 5.외부호출

        //Form이 Open되었을때 다른Form에 대한 작업을 수행 
        public void OpenFormAction(string openedFormName)
        {
            if ("frmRealtimeQuality".Equals(openedFormName))
            {
                if (rechlorineSimulationForm != null)
                {
                    rechlorineSimulationForm.Close();
                    CloseFormAction("frmRechlorineSimulation");
                }

                if (pipeCleaningForm != null)
                {
                    pipeCleaningForm.Close();
                    CloseFormAction("frmPipeCleaning");
                }

                if (qualityChainForms.Count != 0)
                {
                    CloseFormAction("qualityChainForm");
                }

                if (monitorPointManageForm != null)     //수질감시지점관리 : 2015-03-16_shpark
                {
                    monitorPointManageForm.Close();
                    CloseFormAction("frmMonitorPointManage");
                }

                if (chainManageForm != null)            //계통관리 : 2015-03-16_shpark
                {
                    chainManageForm.Close();
                    CloseFormAction("frmChainManage");
                }

                DisableRealtimeViewing();
            }

            if("frmQualityChain".Equals(openedFormName))
            {
                if (realtimeQualityForm != null)
                {
                    realtimeQualityForm.Close();
                    CloseFormAction("realtimeQualityForm");
                }

                if (rechlorineSimulationForm != null)
                {
                    rechlorineSimulationForm.Close();
                    CloseFormAction("frmRechlorineSimulation");
                }

                if (pipeCleaningForm != null)
                {
                    pipeCleaningForm.Close();
                    CloseFormAction("frmPipeCleaning");
                }

                if (monitorPointManageForm != null)     //수질감시지점관리 : 2015-03-16_shpark
                {
                    monitorPointManageForm.Close();
                    CloseFormAction("frmMonitorPointManage");
                }

                DisableRealtimeViewing();
            }

            if ("frmRechlorineSimulation".Equals(openedFormName))
            {
                if (realtimeQualityForm != null)
                {
                    realtimeQualityForm.Close();
                    CloseFormAction("realtimeQualityForm");
                }

                if (pipeCleaningForm != null)
                {
                    pipeCleaningForm.Close();
                    CloseFormAction("pipeCleaningForm");
                }

                if (qualityChainForms.Count != 0)
                {
                    CloseFormAction("qualityChainForm");
                }

                if (monitorPointManageForm != null)     //수질감시지점관리 : 2015-03-16_shpark
                {
                    monitorPointManageForm.Close();
                    CloseFormAction("frmMonitorPointManage");
                }

                if (chainManageForm != null)            //계통관리 : 2015-03-16_shpark
                {
                    chainManageForm.Close();
                    CloseFormAction("frmChainMagage");
                }

                DisableRealtimeViewing();
            }

            if ("frmPipeCleaning".Equals(openedFormName))
            {
                if (realtimeQualityForm != null)
                {
                    realtimeQualityForm.Close();
                    CloseFormAction("realtimeQualityForm");
                }

                if (rechlorineSimulationForm != null)
                {
                    rechlorineSimulationForm.Close();
                    CloseFormAction("rechlorineSimulationForm");
                }

                if (qualityChainForms.Count != 0)
                {
                    CloseFormAction("qualityChainForm");
                }

                if (monitorPointManageForm != null)     //수질감시지점관리 : 2015-03-16_shpark
                {
                    monitorPointManageForm.Close();
                    CloseFormAction("frmMonitorPointManage");
                }

                if (chainManageForm != null)            //계통관리 : 2015-03-16_shpark
                {
                    chainManageForm.Close();
                    CloseFormAction("frmChainMagage");
                }

                DisableRealtimeViewing();
            }

            if ("frmMonitorPointManage".Equals(openedFormName))     //수질감시지점관리 : 2015-03-16_shpark
            {
                if (realtimeQualityForm != null)
                {
                    realtimeQualityForm.Close();
                    CloseFormAction("realtimeQualityForm");
                }

                if (rechlorineSimulationForm != null)
                {
                    rechlorineSimulationForm.Close();
                    CloseFormAction("rechlorineSimulationForm");
                }

                if (qualityChainForms.Count != 0)
                {
                    CloseFormAction("qualityChainForm");
                }

                if (pipeCleaningForm != null)
                {
                    pipeCleaningForm.Close();
                    CloseFormAction("pipeCleaningForm");
                }

                if (chainManageForm != null)            //계통관리 : 2015-03-16_shpark
                {
                    chainManageForm.Close();
                    CloseFormAction("frmChainMagage");
                }

                DisableRealtimeViewing();
            }

            if ("frmChainManage".Equals(openedFormName))            //계통관리 : 2015-03-16_shpark
            {
                if (realtimeQualityForm != null)
                {
                    realtimeQualityForm.Close();
                    CloseFormAction("realtimeQualityForm");
                }

                if (rechlorineSimulationForm != null)
                {
                    rechlorineSimulationForm.Close();
                    CloseFormAction("rechlorineSimulationForm");
                }

                //if (qualityChainForms.Count != 0)
                //{
                //    CloseFormAction("qualityChainForm");
                //}

                if (pipeCleaningForm != null)
                {
                    pipeCleaningForm.Close();
                    CloseFormAction("pipeCleaningForm");
                }

                if (monitorPointManageForm != null)     //수질감시지점관리 : 2015-03-16_shpark
                {
                    monitorPointManageForm.Close();
                    CloseFormAction("frmMonitorPointManage");
                }

                DisableRealtimeViewing();
            }
        }

        //Form이 Close되었을때 해당 Form에 대한 작업 (자기 Form에 대한 Action만 취하면 됨 - 어차피 다른창은 동시에 안열림) - 팝업 단독으로도 사용되기에
        //From Close가 들어가면 오동작을 하게 됨 (두번 닫을려구 시도함)
        public void CloseFormAction(string closedFormName)
        {
            try
            {
                if ("frmRealtimeQuality".Equals(closedFormName))
                {
                    realtimeQualityForm = null;
                }

                if ("frmRechlorineSimulation".Equals(closedFormName))
                {
                    rechlorineSimulationForm = null;
                    UnloadModelLayer();
                }

                if ("frmPipeCleaning".Equals(closedFormName))
                {
                    pipeCleaningForm = null;
                    UnloadModelLayer();
                    ReloadingPipeLayer();
                    ReloadingMeterLayer();
                }
                    
                if("qualityChainForm".Equals(closedFormName))
                {
                    foreach(string key in qualityChainForms.Keys)      //Pool에 들어있는 Form들을 초기화한다.
                    {
                        (qualityChainForms[key] as Form).Close();
                    }

                    qualityChainForms.Clear();
                }

                if ("frmMonitorPointManage".Equals(closedFormName))     //수질감시지점관리 : 2015-03-16_shpark
                {
                    monitorPointManageForm = null;
                }

                if ("frmChainManage".Equals(closedFormName))       //계통관리 : 2015-03-16_shpark
                {
                    chainManageForm = null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
        }

        //INP layer 로딩
        public void loadModelLayer(string inpNumber)
        {
            try
            {
                mWork.GenerateModelShape(inpNumber);        //모델존재여부 확인 후 없을경우 생성

                WaterAOCore.VariableManager.m_INPgraphic = WaterAOCore.ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + inpNumber);

                if (WaterAOCore.VariableManager.m_INPgraphic != null)
                {
                    ILayer junctionLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "JUNCTION", "JUNCTION");
                    ILayer pipeLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PIPE", "PIPE");
                    ILayer pumpLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PUMP", "PUMP");
                    ILayer reservoirLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "RESERVOIR", "RESERVOIR");
                    ILayer tankLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "TANK", "TANK");
                    ILayer valveLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "VALVE", "VALVE");

                    junctionDefaultRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)junctionLayer).Renderer);
                    reservoirDefaultRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)reservoirLayer).Renderer);
                    tankDefaultRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)tankLayer).Renderer);
                    pipeDefaultRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)pipeLayer).Renderer);
                    valveDefaultRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)valveLayer).Renderer);
                    pumpDefaultRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)pumpLayer).Renderer);

                    if (pipeLayer != null)
                    {
                        axMap.AddLayer(pipeLayer);
                    }
                    if (pumpLayer != null)
                    {
                        axMap.AddLayer(pumpLayer);
                    }
                    if (valveLayer != null)
                    {
                        axMap.AddLayer(valveLayer);
                    }
                    if (junctionLayer != null)
                    {
                        axMap.AddLayer(junctionLayer);
                    }
                    if (reservoirLayer != null)
                    {
                        axMap.AddLayer(reservoirLayer);
                    }
                    if (tankLayer != null)
                    {
                        axMap.AddLayer(tankLayer);
                    }

                    this.axMap.ActiveView.Extent = ((IFeatureLayer)junctionLayer).AreaOfInterest;
                }
                
                loadedInpNumber = inpNumber;        //현재 로딩된 inp번호를 update
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                CommonUtils.ShowExceptionMessage(e1.Message);
            }
        }

        //INP layer 언로딩
        public void UnloadModelLayer()
        {
            try
            {
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "JUNCTION");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "PIPE");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "PUMP");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "RESERVOIR");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "TANK");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "VALVE");

                loadedInpNumber = "";       //로딩된 inp번호를 초기화

                junctionRenderer = null;
                reservoirRenderer = null;
                tankRenderer = null;
                pipeRenderer = null;
                valveRenderer = null;
                pumpRenderer = null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
        }

        //해석결과를 지도에 Rendering
        public void MapRendering(Hashtable analysisResult, Hashtable showConditions)
        {
            bool annotation = true;
            if (showConditions["annotation"] != null) annotation = (bool)showConditions["annotation"];

            if (analysisResult["quality"] != null)
            {
                if (showConditions["quality"] != null && (bool)showConditions["quality"])
                {
                    Hashtable qualityResult = analysisResult["quality"] as Hashtable;
                    DrawSymbolNode(qualityResult["correct"] as DataTable, qualityResult["incorrect"] as DataTable);
                    DrawAnnotationNode(qualityResult["correct"] as DataTable, qualityResult["incorrect"] as DataTable, "QUALITY", annotation);
                }
                else
                {
                    ChangeDiableAnalysisResult("node");
                }
            }

            if (analysisResult["flow"] != null)
            {
                if (showConditions["flow"] != null && (bool)showConditions["flow"])
                {
                    Hashtable flowResult = analysisResult["flow"] as Hashtable;
                    DrawSymbolLink(flowResult["correct"] as DataTable, flowResult["incorrect"] as DataTable);
                    DrawAnnotationLink(flowResult["correct"] as DataTable, flowResult["incorrect"] as DataTable, "FLOW", annotation);
                }
                else
                {
                    ChangeDiableAnalysisResult("link");
                }
            }

            if (analysisResult["velocity"] != null)
            {
                if (showConditions["velocity"] != null && (bool)showConditions["velocity"])
                {
                    Hashtable velocityResult = analysisResult["velocity"] as Hashtable;
                    DrawSymbolLink(velocityResult["correct"] as DataTable, velocityResult["incorrect"] as DataTable);
                    DrawAnnotationLink(velocityResult["correct"] as DataTable, velocityResult["incorrect"] as DataTable, "VELOCITY", annotation);
                }
                else
                {
                    ChangeDiableAnalysisResult("link");
                }
            }
        }

        //해당하는 feature를 찾아서 중앙에 위치시킨다.
        public void MoveFocusAt(string layerName, string whereCase)
        {
            try
            {
                ILayer layer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, layerName);

                if (layer != null)
                {
                    ESRI.ArcGIS.Geodatabase.IFeature feature = ArcManager.GetFeature(layer, whereCase);
                    if (feature != null)
                    {
                        if (axMap.MapScale > 10000) ArcManager.SetMapScale((IMapControl3)axMap.Object, (double)10000);

                        IRelationalOperator pRelOp = feature.Shape as IRelationalOperator;
                        if (!pRelOp.Within(((IActiveView)this.axMap.Map).Extent.Envelope as IGeometry))
                        {
                            ArcManager.MoveCenterAt((IActiveView)this.axMap.Map, feature.Shape);
                        }
                        ArcManager.FlashShape(axMap.ActiveView, (IGeometry)feature.Shape, 10, 100);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //해석결과 표출 활성화
        public void ChangeEnableAnalysisResult(string gbn, bool isShow)
        {
            if ("node".Equals(gbn))
            {
                //layer 호출
                for (int i = 0; i < axMap.LayerCount; i++)
                {
                    if ("JUNCTION".Equals(axMap.get_Layer(i).Name))
                    {
                        //Junction layer
                        ILayer junctionLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)junctionLayer).DisplayAnnotation = isShow;
                        if (junctionRenderer != null) ((IGeoFeatureLayer)junctionLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(junctionRenderer);
                    }
                    else if ("RESERVOIR".Equals(axMap.get_Layer(i).Name))
                    {
                        //Reservoir layer
                        ILayer reservoirLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)reservoirLayer).DisplayAnnotation = isShow;
                        if (reservoirRenderer != null) ((IGeoFeatureLayer)reservoirLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(reservoirRenderer);
                    }
                    else if ("TANK".Equals(axMap.get_Layer(i).Name))
                    {
                        //Tank layer
                        ILayer tankLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)tankLayer).DisplayAnnotation = isShow;
                        if (tankRenderer != null) ((IGeoFeatureLayer)tankLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(tankRenderer);
                    }
                }
            }
            else if ("link".Equals(gbn))
            {
                //layer 호출
                for (int i = 0; i < axMap.LayerCount; i++)
                {
                    if ("PIPE".Equals(axMap.get_Layer(i).Name))
                    {
                        //Junction layer
                        ILayer pipeLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)pipeLayer).DisplayAnnotation = isShow;
                        if (pipeRenderer != null) ((IGeoFeatureLayer)pipeLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(pipeRenderer);
                    }
                    else if ("PUMP".Equals(axMap.get_Layer(i).Name))
                    {
                        //Reservoir layer
                        ILayer pumpLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)pumpLayer).DisplayAnnotation = isShow;
                        if (pumpRenderer != null) ((IGeoFeatureLayer)pumpLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(pumpRenderer);
                    }
                    else if ("VALVE".Equals(axMap.get_Layer(i).Name))
                    {
                        //Tank layer
                        ILayer valveLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)valveLayer).DisplayAnnotation = isShow;
                        if (valveRenderer != null) ((IGeoFeatureLayer)valveLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(valveRenderer);
                    }
                }
            }

            //MapRendering(analysisResult);
            //axMap.ActiveView.Refresh();
            ArcManager.PartialRefresh(this.axMap.Map, esriViewDrawPhase.esriViewGeography);
        }

        //해석결과 표출 비활성화
        public void ChangeDiableAnalysisResult(string gbn)
        {
            if ("node".Equals(gbn))
            {
                //layer 호출
                for (int i = 0; i < axMap.LayerCount; i++)
                {
                    if ("JUNCTION".Equals(axMap.get_Layer(i).Name))
                    {
                        //Junction layer
                        ILayer junctionLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)junctionLayer).DisplayAnnotation = false;
                        junctionRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)junctionLayer).Renderer);
                        ((IGeoFeatureLayer)junctionLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(junctionDefaultRenderer);
                    }
                    else if ("RESERVOIR".Equals(axMap.get_Layer(i).Name))
                    {
                        //Reservoir layer
                        ILayer reservoirLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)reservoirLayer).DisplayAnnotation = false;
                        reservoirRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)reservoirLayer).Renderer);
                        ((IGeoFeatureLayer)reservoirLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(reservoirDefaultRenderer);
                    }
                    else if ("TANK".Equals(axMap.get_Layer(i).Name))
                    {
                        //Tank layer
                        ILayer tankLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)tankLayer).DisplayAnnotation = false;
                        tankRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)tankLayer).Renderer);
                        ((IGeoFeatureLayer)tankLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(tankDefaultRenderer);
                    }
                }
            }
            else if ("link".Equals(gbn))
            {
                //layer 호출
                for (int i = 0; i < axMap.LayerCount; i++)
                {
                    if ("PIPE".Equals(axMap.get_Layer(i).Name))
                    {
                        //Junction layer
                        ILayer pipeLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)pipeLayer).DisplayAnnotation = false;
                        pipeRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)pipeLayer).Renderer);
                        ((IGeoFeatureLayer)pipeLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(pipeDefaultRenderer);
                    }
                    else if ("PUMP".Equals(axMap.get_Layer(i).Name))
                    {
                        //Reservoir layer
                        ILayer pumpLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)pumpLayer).DisplayAnnotation = false;
                        pumpRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)pumpLayer).Renderer);
                        ((IGeoFeatureLayer)pumpLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(valveDefaultRenderer);
                    }
                    else if ("VALVE".Equals(axMap.get_Layer(i).Name))
                    {
                        //Tank layer
                        ILayer valveLayer = axMap.get_Layer(i);
                        ((IGeoFeatureLayer)valveLayer).DisplayAnnotation = false;
                        valveRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)valveLayer).Renderer);
                        ((IGeoFeatureLayer)valveLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(pumpDefaultRenderer);
                    }
                }
            }

            ArcManager.PartialRefresh(this.axMap.Map, esriViewDrawPhase.esriViewGeography);
        }

        //관세척구간 랜더링
        public void PipeCleaningDataRendering(Hashtable cleaningDatas)
        {
            if (cleaningDatas["allPipeCleaningData"] != null)
            {
                if (shapePipeDefaultRenderer == null)
                {
                    IGeoFeatureLayer layer = (IGeoFeatureLayer)ArcManager.GetMapLayer(axMap.Map, "상수관로");

                    shapePipeDefaultRenderer = ArcManager.SerializeFeatureRenderer(layer.Renderer);
                    shapePipeLayerMax = layer.MaximumScale;
                    shapePipeLayerMin = layer.MinimumScale;
                }

                DrawAllPipeCleanData(cleaningDatas["allPipeCleaningData"] as DataTable);
            }
        }

        //상수관로 Layer 원래대로 만듦 (랜더러를 바꿔끼움)
        public void ReloadingPipeLayer()
        {
            ILayer shapePipeLayer = ArcManager.GetMapLayer(axMap.Map, "상수관로");

            if (shapePipeLayer != null && shapePipeDefaultRenderer != null)
            {
                ((IGeoFeatureLayer)shapePipeLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(shapePipeDefaultRenderer); ;
                shapePipeLayer.MaximumScale = shapePipeLayerMax;
                shapePipeLayer.MinimumScale = shapePipeLayerMin;

                this.axMap.ActiveView.Refresh();
                this.axMap.ActiveView.ContentsChanged();
            }
        }

        //현재 로딩된 모델번호를 반환한다. (로딩되지 않았을 경우 빈문자열 반환)
        public string GetLoadedInpNumber()
        {
            return this.loadedInpNumber;
        }

        //민원지점 랜더링
        public void ComplaintDataRendering(DataTable complaintData)
        {
            if (complaintData != null)
            {
                IGeoFeatureLayer layer = (IGeoFeatureLayer)ArcManager.GetMapLayer(axMap.Map, "수도계량기");

                shapeMeterDefaultRenderer = ArcManager.SerializeFeatureRenderer(layer.Renderer);
                shapeMeterLayerMax = layer.MaximumScale;
                shapeMeterLayerMin = layer.MinimumScale;
            }

            DrawComplaintData(complaintData);
        }

        //수도계량기(수용가) Layer. 원래대로 만듦 (랜더러 바꿔끼움)
        public void ReloadingMeterLayer()
        {
            ILayer shapeMeterLayer = ArcManager.GetMapLayer(axMap.Map, "수도계량기");

            if (shapeMeterLayer != null && shapeMeterDefaultRenderer != null)
            {
                ((IGeoFeatureLayer)shapeMeterLayer).Renderer = ArcManager.DeSerializeFeatureRenderer(shapeMeterDefaultRenderer);
                shapeMeterLayer.MinimumScale = shapeMeterLayerMin;
                shapeMeterLayer.MaximumScale = shapeMeterLayerMax;

                this.axMap.ActiveView.Refresh();
                this.axMap.ActiveView.ContentsChanged();
            }
        }

        //해석결과 Annotation 활성/비활성
        public void ShowAnnotation(bool isShow, bool showNode, bool showLink)
        {
            if(showNode)
            {
                ILayer junctionLayer = ArcManager.GetMapLayer(axMap.Map, "JUNCTION");
                ILayer reservoirLayer = ArcManager.GetMapLayer(axMap.Map, "RESERVOIR");
                ILayer tankLayer = ArcManager.GetMapLayer(axMap.Map, "TANK");

                if (junctionLayer != null) ((IGeoFeatureLayer)((IFeatureLayer)junctionLayer)).DisplayAnnotation = isShow;
                if (reservoirLayer != null) ((IGeoFeatureLayer)((IFeatureLayer)reservoirLayer)).DisplayAnnotation = isShow;
                if (tankLayer != null) ((IGeoFeatureLayer)((IFeatureLayer)tankLayer)).DisplayAnnotation = isShow;
            }

            if(showLink)
            {
                ILayer pipeLayer = ArcManager.GetMapLayer(axMap.Map, "PIPE");
                ILayer valveLayer = ArcManager.GetMapLayer(axMap.Map, "VALVE");
                ILayer pumpLayer = ArcManager.GetMapLayer(axMap.Map, "PUMP");

                if (pipeLayer != null) ((IGeoFeatureLayer)((IFeatureLayer)pipeLayer)).DisplayAnnotation = isShow;
                if (valveLayer != null) ((IGeoFeatureLayer)((IFeatureLayer)valveLayer)).DisplayAnnotation = isShow;
                if (pumpLayer != null) ((IGeoFeatureLayer)((IFeatureLayer)pumpLayer)).DisplayAnnotation = isShow;
            }

            this.axMap.Refresh();

            //ArcManager.PartialRefresh(this.axMap.Map, esriViewDrawPhase.esriViewGeography);
        }

        #endregion

        #region 6.내부사용

        //화면에 표시할 지점 컨트롤을 생성하고 전역 Hashtable에 저장
        private void MakeControls()
        {
            DataTable checkpointList = work.SelectMainRealtimeCheckpointList(null);

            foreach(DataRow row in checkpointList.Rows)
            {
                frmQualityCheckpoint cpForm = new frmQualityCheckpoint(row["MONPNT"].ToString());
                cpForm.TopLevel = false;
                cpForm.Parent = spcContents.Panel2;
                cpForm.SetMapXY(row["MAP_X"].ToString(), row["MAP_Y"].ToString());
                monitoringCpList.Add(row["MONITOR_NO"].ToString(), cpForm);
            }
        }

        private bool GetContains(IEnvelope env, IPoint p)
        {
            IRelationalOperator relOp = env as IRelationalOperator;
            return relOp.Contains(p);
        }

        //지도 및 폼 조작 시 컨트롤을 다시 그려줌
        private void ExtentUpdatedEvent(IEnvelope pEnvelope)
        {
            //실시간 지점감시 표출모드가 활성화가 아니거나 다른 Form들이 떠 있는경우는 지점모니터링을 하지 않는다.
            //수질감지지점, 계통관리 추가 2015-03-16_shpark
            if (!isShowRtMonitor || realtimeQualityForm != null || rechlorineSimulationForm != null || pipeCleaningForm != null || monitorPointManageForm != null || chainManageForm !=null) return;

            //먼저 화면상의 유량계 폼 Control을 지도상에서 모두지운다.
            foreach(string key in monitoringCpList.Keys)
            {
                ((Form)monitoringCpList[key]).Visible = false;
            }
            
            Application.DoEvents();

            foreach (string key in monitoringCpList.Keys)
            {
                if (monitoringCpList[key] != null)
                {
                    frmQualityCheckpoint monitoringCp = (frmQualityCheckpoint)monitoringCpList[key];

                    double mapx = double.Parse(monitoringCp.GetMapX()); double mapy = double.Parse(monitoringCp.GetMapY());
                    IPoint pPoint = new PointClass();
                    pPoint.PutCoords(mapx, mapy);

                    if (GetContains(pEnvelope, pPoint))
                    {
                        int x; int y;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.Units = ESRI.ArcGIS.esriSystem.esriUnits.esriMeters;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.FromMapPoint(pPoint, out x, out y);

                        monitoringCp.Visible = true;
                        monitoringCp.SetBounds(x + 10, y + 10, monitoringCp.Width, monitoringCp.Height);
                        monitoringCp.BringToFront();
                    }
                }                
            }
        }

        //현재시간 기준 실시간 데이터를 적용 (분단위)
        public void ApplyRealtimeData()
        {
            try
            {
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmm");
                
                Hashtable realtimeDataConditions = new Hashtable();
                realtimeDataConditions.Add("timestamp", timestamp);
                DataTable realtimeDataList = work.SelectMainRealtimeDataList(realtimeDataConditions);

                foreach (DataRow row in realtimeDataList.Rows)
                {
                    if (monitoringCpList[row["MONITOR_NO"].ToString()] != null)
                    {
                        frmQualityCheckpoint cpForm = (frmQualityCheckpoint)monitoringCpList[row["MONITOR_NO"].ToString()];
                        cpForm.SetQualityDatas(row["CL_VALUE"].ToString(), row["PH_VALUE"].ToString(), row["TE_VALUE"].ToString(), row["CU_VALUE"].ToString(), row["TB_VALUE"].ToString());
                    }
                }
            }
            catch (Exception e)
            {
                timer.Stop();
                //timer.Dispose();
                Console.WriteLine(e.ToString());
            }
        }

        //실시간 데이터 표출기능을 활성화
        private void EnableRealtimeViewing()
        {
            if(!timer.Enabled)
            {
                timer.Start();

                IEnvelope pEnvelope = axMap.ActiveView.Extent;
                ExtentUpdatedEvent(pEnvelope as IEnvelope);
            }
        }

        //실시간 데이터 표출기능을 비활성화
        private void DisableRealtimeViewing()
        {
            foreach (string key in monitoringCpList.Keys)
            {
                ((Form)monitoringCpList[key]).Visible = false;
            }

            Application.DoEvents();
            timer.Stop();
        }

        //선택모드가 활성화 된 상태에서 click된 객체 ID 반환
        private string GetMapObjectId(ILayer layer, IGeometry pGeom, double dblContains)
        {
            string result = "";

            IFeatureCursor pIFCursorMP = ArcManager.GetSpatialCursor(layer, pGeom, dblContains, esriSpatialRelEnum.esriSpatialRelIntersects, string.Empty);
            IFeature pFeatureMP = pIFCursorMP.NextFeature();

            if (pFeatureMP != null)
            {
                ArcManager.FlashShape(axMap.ActiveView, (IGeometry)pFeatureMP.Shape, 10);

                object oNO = WaterAOCore.ArcManager.GetValue(pFeatureMP, "ID");

                result = oNO.ToString();
            }

            return result;
        }

        //선택모드가 활성화 된 상태에서 drag된 객체 ID 반환
        private ArrayList GetMapObjectIds(ILayer layer, IGeometry pGeom, double dblContains)
        {
            ArrayList featureList = new ArrayList();
            IFeatureCursor pIFCursorMP = ArcManager.GetSpatialCursor(layer, pGeom, esriSpatialRelEnum.esriSpatialRelIntersects, string.Empty);

            IFeature pFeatureMP = pIFCursorMP.NextFeature();
            while (pFeatureMP != null)
            {
                object oNO = WaterAOCore.ArcManager.GetValue(pFeatureMP, "FTR_IDN");
                if(!"".Equals(oNO.ToString().Trim()))
                {
                    featureList.Add(oNO.ToString());
                    //ArcManager.FlashShape(axMap.ActiveView, (IGeometry)pFeatureMP.Shape, 1);
                }

                pFeatureMP = pIFCursorMP.NextFeature();
            }

            return featureList;
        }

        //Node와 Link가 별다른 차이는 없지만 기능 추가로 인한 복잡도 증가를 피하기 위해 분리
        //지도위에 해석결과 심볼 랜더링(Node)
        private void DrawSymbolNode(DataTable correctDatas, DataTable incorrectDatas)
        {
            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
            vrm.Fields = new string[] { "ID" };

            if (correctDatas.Rows.Count != 0)
            {
                List<object> datas = new List<object>();
                foreach (DataRow row in correctDatas.Rows) datas.Add(row["NODE_ID"].ToString());

                AddPointRenderer(vrm, datas, Color.Red, 5, "correct");


                //EAGL.Display.Rendering.SimplePointRenderer spr = new EAGL.Display.Rendering.SimplePointRenderer();
                //spr.Color = Color.Red;
                //spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
                //spr.Size = 5;
                //ISymbol sym = spr.Symbol;

                //object key = correctDatas.Rows[0]["NODE_ID"];
                //List<object> refValues = new List<object>();
                //for (int i = 1; i < correctDatas.Rows.Count; i++)
                //{
                //    refValues.Add(correctDatas.Rows[i]["NODE_ID"]);
                //}

                //vrm.Add(key, sym, "correct");
                //vrm.AddReferanceValue2(key, refValues);
            }

            if (incorrectDatas.Rows.Count != 0)
            {
                List<object> datas = new List<object>();
                foreach (DataRow row in incorrectDatas.Rows) datas.Add(row["NODE_ID"].ToString());

                AddPointRenderer(vrm, datas, Color.Black, 1, "incorrect");

                //EAGL.Display.Rendering.SimplePointRenderer spr = new EAGL.Display.Rendering.SimplePointRenderer();
                //spr.Color = Color.Black;
                //spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
                //spr.Size = 2;
                //ISymbol sym = spr.Symbol;

                //object key = incorrectDatas.Rows[0]["NODE_ID"];
                //List<object> refValues = new List<object>();
                //for (int i = 1; i < incorrectDatas.Rows.Count; i++)
                //{
                //    refValues.Add(incorrectDatas.Rows[i]["NODE_ID"]);
                //}

                //vrm.Add(key, sym, "incorrect");
                //vrm.AddReferanceValue2(key, refValues);
            }

            try
            {
                ILayer layer = null;
                layer = ArcManager.GetMapLayer(axMap.Map, "JUNCTION");

                if (layer != null) ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                
                layer = ArcManager.GetMapLayer(axMap.Map, "RESERVOIR");
                if (layer != null) ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                
                layer = ArcManager.GetMapLayer(axMap.Map, "TANK");
                if (layer != null) ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
            }
            catch { }

            ArcManager.PartialRefresh(this.axMap.Map, esriViewDrawPhase.esriViewGeography);
            this.axMap.ActiveView.ContentsChanged();
            
        }

        //지도위에 해석결과 심볼 랜더링(Link)
        private void DrawSymbolLink(DataTable correctDatas, DataTable incorrectDatas)
        {
            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
            vrm.Fields = new string[] { "ID" };

            if (correctDatas.Rows.Count != 0)
            {
                ISimpleLineSymbol cartoGraphicLineSymbol = new SimpleLineSymbolClass();
                cartoGraphicLineSymbol.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Blue);
                cartoGraphicLineSymbol.Width = 2;
                ISymbol sym = cartoGraphicLineSymbol as ISymbol;

                object key = correctDatas.Rows[0]["LINK_ID"];
                List<object> refValues = new List<object>();
                for (int i = 1; i < correctDatas.Rows.Count; i++)
                {
                    refValues.Add(correctDatas.Rows[i]["LINK_ID"]);
                }

                vrm.Add(key, sym, "correct");
                vrm.AddReferanceValue2(key, refValues);
            }

            if (incorrectDatas.Rows.Count != 0)
            {
                ISimpleLineSymbol cartoGraphicLineSymbol = new SimpleLineSymbolClass();
                cartoGraphicLineSymbol.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Black);
                cartoGraphicLineSymbol.Width = 1;
                ISymbol sym = cartoGraphicLineSymbol as ISymbol;

                object key = incorrectDatas.Rows[0]["LINK_ID"];
                List<object> refValues = new List<object>();
                for (int i = 1; i < incorrectDatas.Rows.Count; i++)
                {
                    refValues.Add(incorrectDatas.Rows[i]["LINK_ID"]);
                }

                vrm.Add(key, sym, "incorrect");
                vrm.AddReferanceValue2(key, refValues);
            }

            try
            {
                ILayer layer = null;
                layer = ArcManager.GetMapLayer(axMap.Map, "PIPE");
                if (layer != null)
                {
                    ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                }
                layer = ArcManager.GetMapLayer(axMap.Map, "VALVE");
                if (layer != null)
                {
                    ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                }
                layer = ArcManager.GetMapLayer(axMap.Map, "PUMP");
                if (layer != null)
                {
                    ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                }
            }
            catch { }

            ArcManager.PartialRefresh(this.axMap.Map, esriViewDrawPhase.esriViewGeography);
            this.axMap.ActiveView.ContentsChanged();
        }

        //지도위에 해석결과값 랜더링(Node)
        private void DrawAnnotationNode(DataTable correctDatas, DataTable incorrectDatas, string valueFieldName, bool annotation)
        {
            correctDatas.Merge(incorrectDatas); //데이터테이블 합치기 (값은 맞던 틀리던 보여줘야함)

            var q = (from table in correctDatas.AsEnumerable()
                     select new
                     {
                         ID = table.Field<string>("NODE_ID"),
                         VALUE = table.Field<string>(valueFieldName)
                     });

            Dictionary<string, string> values= q.ToDictionary(k => k.ID, k => k.VALUE);

            if(correctDatas.Rows.Count != 0)
            {
                EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
                LRM.Extent = this.axMap.Extent;
                LRM.valueMap = values;

                IFormattedTextSymbol pText = new TextSymbolClass();
                pText.Font.Bold = false;
                pText.Font.Name = "굴림";
                pText.Size = 8;
                pText.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Black);

                LRM.Add("0", string.Empty, "ID", (ITextSymbol)pText);

                ILayer layer = null;
                layer = ArcManager.GetMapLayer(axMap.Map, "JUNCTION");
                if (layer != null)
                {
                    LRM.FeatureLayer = (IFeatureLayer)layer;
                    IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                    for (int i = 0; i < ac.Count; i++)
                    {
                        IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                        ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                    }
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = annotation;
                }
                layer = ArcManager.GetMapLayer(axMap.Map, "RESERVOIR");
                if (layer != null)
                {
                    LRM.FeatureLayer = (IFeatureLayer)layer;
                    IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                    for (int i = 0; i < ac.Count; i++)
                    {
                        IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                        ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                    }
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = annotation;
                }
                layer = ArcManager.GetMapLayer(axMap.Map, "TANK");
                if (layer != null)
                {
                    LRM.FeatureLayer = (IFeatureLayer)layer;
                    IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                    for (int i = 0; i < ac.Count; i++)
                    {
                        IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                        ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                    }
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = annotation;
                }
            }  
        }

        //지도위에 해석결과값 랜더링(Link)
        private void DrawAnnotationLink(DataTable correctDatas, DataTable incorrectDatas, string valueFieldName, bool annotation)
        {
            correctDatas.Merge(incorrectDatas); //데이터테이블 합치기 (값은 맞던 틀리던 보여줘야함)

            var q = (from table in correctDatas.AsEnumerable()
                     select new
                     {
                         ID = table.Field<string>("LINK_ID"),
                         VALUE = table.Field<string>(valueFieldName)
                     });

            Dictionary<string, string> values = q.ToDictionary(k => k.ID, k => k.VALUE);

            if (correctDatas.Rows.Count != 0)
            {
                EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
                LRM.Extent = this.axMap.Extent;
                LRM.valueMap = values;

                IFormattedTextSymbol pText = new TextSymbolClass();
                pText.Font.Bold = false;
                pText.Font.Name = "굴림";
                pText.Size = 8;
                pText.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Black);

                LRM.Add("0", string.Empty, "ID", (ITextSymbol)pText);

                ILayer layer = null;
                layer = ArcManager.GetMapLayer(axMap.Map, "PIPE");
                if (layer != null)
                {
                    LRM.FeatureLayer = (IFeatureLayer)layer;
                    IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                    for (int i = 0; i < ac.Count; i++)
                    {
                        IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                        ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                    }
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = annotation;
                }
                layer = ArcManager.GetMapLayer(axMap.Map, "VALVE");
                if (layer != null)
                {
                    LRM.FeatureLayer = (IFeatureLayer)layer;
                    IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                    for (int i = 0; i < ac.Count; i++)
                    {
                        IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                        ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                    }
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = annotation;
                }
                layer = ArcManager.GetMapLayer(axMap.Map, "PUMP");
                if (layer != null)
                {
                    LRM.FeatureLayer = (IFeatureLayer)layer;
                    IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                    for (int i = 0; i < ac.Count; i++)
                    {
                        IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                        ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                    }
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = annotation;
                }
            }
        }

        //지도위에 세쳑구간 랜더링(상수관로)
        private void DrawAllPipeCleanData(DataTable pipeCleaningData)
        {
            try
            {
                //지도에 다시그릴 데이터 정비
                List<object> normalPipeList = new List<object>();             //세척이력이 없는 파이프
                List<object> currntCleanedPipeList = new List<object>();      //현재 선택된 세척 파이프 리스트
                List<object> cleanedPipeList = new List<object>();            //선택되지 않은 세척 파이프 리스트

                Hashtable tmpPipeCleaningData = new Hashtable();

                foreach (DataRow row in pipeCleaningData.Rows)
                {
                    tmpPipeCleaningData.Add(row["FTR_IDN"].ToString(), row["TYPE"].ToString());
                }

                ILayer pipeLayer = ArcManager.GetMapLayer(axMap.Map, "상수관로");
                if (pipeLayer == null) return;

                IFeatureCursor pipeCursor = (IFeatureCursor)ArcManager.GetCursor(pipeLayer, "");

                IFeature tmpPipeFeature = null;

                while ((tmpPipeFeature = pipeCursor.NextFeature()) != null)
                {
                    string tmpFtrIdn = tmpPipeFeature.get_Value(tmpPipeFeature.Fields.FindField("FTR_IDN")).ToString();

                    if (tmpPipeCleaningData[tmpFtrIdn] != null)
                    {
                        if ("1".Equals(tmpPipeCleaningData[tmpFtrIdn].ToString()))
                        {
                            currntCleanedPipeList.Add(tmpFtrIdn);
                        }
                        else
                        {
                            cleanedPipeList.Add(tmpFtrIdn);
                        }
                    }
                    else
                    {
                        normalPipeList.Add(tmpFtrIdn);
                    }
                }

                //GIS상에 표출
                EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
                vrm.Fields = new string[] { "FTR_IDN" };

                //각 상황에 맞는 Renderer 추가
                if (normalPipeList.Count != 0) AddLineRenderer(vrm, normalPipeList, Color.Black, 1, "none");
                if (cleanedPipeList.Count != 0) AddLineRenderer(vrm, cleanedPipeList, Color.Green, 5, "other");
                if (currntCleanedPipeList.Count != 0) AddLineRenderer(vrm, currntCleanedPipeList, Color.Red, 5, "current");

                //Layer에 표현
                //ILayer layer = ArcManager.GetMapLayer(axMap.Map, "상수관로"); ;
                if (pipeLayer != null) ((IGeoFeatureLayer)pipeLayer).Renderer = vrm.EsriRenderer;

                pipeLayer.MaximumScale = 0;
                pipeLayer.MinimumScale = 0;

                ArcManager.PartialRefresh(this.axMap.Map, esriViewDrawPhase.esriViewGeography);
                this.axMap.ActiveView.ContentsChanged();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //지도위에 민원지점 랜더링(수용가)
        private void DrawComplaintData(DataTable complaintData)
        {
            try
            {
                List<object> complaintMeterList = new List<object>();          //민원대상 수용가
                List<object> normalMeterList = new List<object>();             //일반 수용가

                Hashtable tmpComplaintData = new Hashtable();

                foreach (DataRow row in complaintData.Rows)
                {
                    if (tmpComplaintData[row["DMNO"].ToString()] == null) tmpComplaintData.Add(row["DMNO"].ToString(), "");
                }

                ILayer meterLayer = ArcManager.GetMapLayer(axMap.Map, "수도계량기");
                if (meterLayer == null) return;

                IFeatureCursor meterCursor = (IFeatureCursor)ArcManager.GetCursor(meterLayer, "");

                IFeature tmpMeterFeature = null;

                while ((tmpMeterFeature = meterCursor.NextFeature()) != null)
                {
                    string tmpDmno = tmpMeterFeature.get_Value(tmpMeterFeature.Fields.FindField("DMNO")).ToString();

                    if (tmpComplaintData[tmpDmno] != null) complaintMeterList.Add(tmpDmno);
                    else normalMeterList.Add(tmpDmno);
                }

                //GIS상에 표출
                EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
                vrm.Fields = new string[] { "DMNO" };

                //각 상황에 맞는 Renderer 추가
                if (normalMeterList.Count != 0) AddPointRenderer(vrm, normalMeterList, Color.Black, 0.1, "normal");
                if (complaintMeterList.Count != 0) AddPointRenderer(vrm, complaintMeterList, Color.DeepPink, 10, "complaint");

                //Layer에 표현
                //ILayer layer = ArcManager.GetMapLayer(axMap.Map, "수도계량기"); ;
                if (meterLayer != null) ((IGeoFeatureLayer)meterLayer).Renderer = vrm.EsriRenderer;

                meterLayer.MaximumScale = 0;
                meterLayer.MinimumScale = 0;

                ArcManager.PartialRefresh(this.axMap.Map, esriViewDrawPhase.esriViewGeography);
                this.axMap.ActiveView.ContentsChanged();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //선형 Feature에 대한 Renderer 추가
        private void AddLineRenderer(EAGL.Display.Rendering.ValueRenderingMap vrm, List<object> datas, Color color, double symbolSize, string title)
        {
            ISimpleLineSymbol cartoGraphicLineSymbol = new SimpleLineSymbolClass();
            cartoGraphicLineSymbol.Color = EAGL.Display.ColorManager.GetESRIColor(color);
            cartoGraphicLineSymbol.Width = symbolSize;
            ISymbol sym = cartoGraphicLineSymbol as ISymbol;

            object key = datas[0].ToString();
            datas.RemoveAt(0);
            vrm.Add(key, sym, title);
            vrm.AddReferanceValue2(key, datas);
        }

        //점형 Feature에 대한 Renderer 추가
        private void AddPointRenderer(EAGL.Display.Rendering.ValueRenderingMap vrm, List<object> datas, Color color, double symbolSize, string title)
        {
            EAGL.Display.Rendering.SimplePointRenderer spr = new EAGL.Display.Rendering.SimplePointRenderer();
            spr.Color = color;
            spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
            spr.Size = symbolSize;
            ISymbol sym = spr.Symbol;

            object key = datas[0].ToString();
            datas.RemoveAt(0);
            vrm.Add(key, sym, title);
            vrm.AddReferanceValue2(key, datas);
        }

        #endregion

    }
}
