﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WQ_Analysis.work;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WQ_Analysis.util;
using Infragistics.Win.UltraWinTree;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmPipeCleaning : Form
    {
        #region 1.전역번수

        private PipeCleaningWork work = null;
        private CommonAnalysisWork cWork = null;
        private frmQualityAnalysisMap ownerForm = null;

        #endregion

        #region 2.초기화

        public frmPipeCleaning()
        {
            InitializeComponent();
            InitializeSetting();
            work = PipeCleaningWork.GetInstance();
            cWork = CommonAnalysisWork.GetInstance();

            datStartDate.Value = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");         //관세척 조회 시작날짜 초기화
            datComStartDate.Value = DateTime.Now.AddMonths(-12).ToString("yyyy-MM-dd");     //민원 조회 시작날짜 초기화
        }

        #endregion

        #region 3.객체 이벤트

        //form 최초실행 시 로드
        private void frmPipeCleaning_Load(object sender, EventArgs e)
        {
            try
            {
                ownerForm = (this.Owner as frmQualityAnalysisMap);      //자신의 부모Form 변수 할당

                //관망해석 Component 초기화
                DataTable filterFormula = new DataTable();
                filterFormula.Columns.Add("text", typeof(string));
                filterFormula.Columns.Add("value", typeof(string));

                DataRow row0 = filterFormula.NewRow();
                row0["text"] = "선택";
                row0["value"] = "";

                DataRow row1 = filterFormula.NewRow();
                row1["text"] = "보다 큰";
                row1["value"] = "<";

                DataRow row2 = filterFormula.NewRow();
                row2["text"] = "보다 작은";
                row2["value"] = ">";

                DataRow row3 = filterFormula.NewRow();
                row3["text"] = "이상인";
                row3["value"] = "<=";

                DataRow row4 = filterFormula.NewRow();
                row4["text"] = "이하인";
                row4["value"] = ">=";

                DataRow row5 = filterFormula.NewRow();
                row5["text"] = "인";
                row5["value"] = "=";

                filterFormula.Rows.Add(row0);
                filterFormula.Rows.Add(row1);
                filterFormula.Rows.Add(row2);
                filterFormula.Rows.Add(row3);
                filterFormula.Rows.Add(row4);
                filterFormula.Rows.Add(row5);

                comVelocityFormula.ValueMember = "value";
                comVelocityFormula.DisplayMember = "text";
                comVelocityFormula.DataSource = filterFormula;
                comVelocityFormula.SelectedIndex = 0;

                SelectPipeCleaningDataList();       //관세척정보 조회
                
                SelectModelList();                  //관망해석 모델 리스트 조회

                comComplaintType.ValueMember = "CODE";
                comComplaintType.DisplayMember = "CODE_NAME";

                Hashtable complaintInitDatas = work.InitComplaintTab(null);
                comComplaintType.DataSource = complaintInitDatas["complaintType"] as DataTable;

                SelectComplaintCountByBlock();      //블록별 민원 리스트 조회 (Tree)
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //form이 닫길때 실행
        private void frmPipeCleaning_FormClosed(object sender, FormClosedEventArgs e)
        {
            cWork.InitAnalysisResult();     //해석결과 초기화
            ownerForm.CloseFormAction("frmPipeCleaning");
        }

        //==관세척정보

        //관세척정보 grid 행 변경 시 실행
        private void griPipeCleaning_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                datProcDat.Value = CommonUtils.GetGridCellValue(griPipeCleaning, "PROC_DAT");
                texPcleanTitle.Text = CommonUtils.GetGridCellValue(griPipeCleaning, "PCLEAN_TITLE");
                texProcMethodTitle.Text = CommonUtils.GetGridCellValue(griPipeCleaning, "PROC_METHOD_TITLE");
                texProcMethod.Text = CommonUtils.GetGridCellValue(griPipeCleaning, "PROC_METHOD");
                texProcResultTitle.Text = CommonUtils.GetGridCellValue(griPipeCleaning, "PROC_RESULT_TITLE");
                texProcResult.Text = CommonUtils.GetGridCellValue(griPipeCleaning, "PROC_RESULT");
                texRemark.Text = CommonUtils.GetGridCellValue(griPipeCleaning, "REMARK");

                if (griPipeCleaning.Selected.Rows.Count > 0 && CommonUtils.GetRowState(griPipeCleaning) != DataRowState.Added)
                {
                    SelectPipeList();           //관련 관로정보 조회
                    LoadAllPipeCleaningData();  //관세척 정보를 GIS에 표출
                }
                else
                {
                    if (griPipe.DataSource != null) (griPipe.DataSource as DataTable).Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //명칭 textbox 변경 시 실행
        private void texPcleanTitle_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griPipeCleaning, "PCLEAN_TITLE", (sender as TextBox).Text);
        }

        //처리일자 date 변경 시 실행
        private void datProcDat_ValueChanged(object sender, EventArgs e)
        {
            if (datProcDat.Value == null) return;
            CommonUtils.SetGridCellValue(griPipeCleaning, "PROC_DAT", ((DateTime)datProcDat.Value).ToString("yyyy-MM-dd"));
        }

        //처리결과 textbox 변경 시 실행
        private void texProcMethodTitle_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griPipeCleaning, "PROC_METHOD_TITLE", (sender as TextBox).Text);
        }

        //처리방법상세 textbox 변경 시 실행
        private void texProcMethod_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griPipeCleaning, "PROC_METHOD", (sender as TextBox).Text);
        }

        //처리방법 textbox 변경 시 실행
        private void texProcResultTitle_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griPipeCleaning, "PROC_RESULT_TITLE", (sender as TextBox).Text);
        }

        //처리결과상세 textbox 변경 시 실행
        private void texProcResult_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griPipeCleaning, "PROC_RESULT", (sender as TextBox).Text);
        }

        //비고 textbox 변경 시 실행
        private void texRemark_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griPipeCleaning, "REMARK", (sender as TextBox).Text);
        }

        //관세척정보 추가 버튼 클릭 시 실행
        private void butAddPcleanData_Click(object sender, EventArgs e)
        {
            CommonUtils.AddGridRow(griPipeCleaning);
            CommonUtils.SetGridCellValue(griPipeCleaning, "PROC_DAT", DateTime.Now.ToString("yyyy-MM-dd"));
            datProcDat.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }

        //관세척정보 삭제 버튼 클릭 시 실행
        private void butDelPcleanData_Click(object sender, EventArgs e)
        {
            try
            {
                Hashtable deleteData = null;

                if ((deleteData = CommonUtils.DeleteGridRow(griPipeCleaning, "삭제 시 설정된 관로정보도 모두 삭제됩니다.\n삭제하시겠습니까?")) != null)
                {
                    Hashtable deleteConditions = new Hashtable();
                    deleteConditions.Add("PCLEAN_NO", deleteData["PCLEAN_NO"].ToString());

                    work.DeletePipeCleaningDataList(deleteConditions);

                    //모두 삭제되었을때 하단 Textbox 초기화
                    if (griPipeCleaning.Selected.Rows.Count == 0)
                    {
                        this.texPcleanTitle.Text = "";
                        this.texProcMethod.Text = "";
                        this.texProcResult.Text = "";
                        this.texRemark.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //관세척정보 저장 버튼 클릭 시 실행
        private void butSavePcleanData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //validation 정보 (사이즈|공백허용|숫자여부)
                Hashtable validationSet = new Hashtable();
                validationSet.Add("PCLEAN_TITLE", "400|N|N");
                validationSet.Add("PROC_METHOD_TITLE", "200|Y|N");
                validationSet.Add("PROC_METHOD", "512|Y|N");
                validationSet.Add("PROC_RESULT_TITLE", "200|Y|N");
                validationSet.Add("PROC_RESULT", "512|Y|N");
                validationSet.Add("REMARK", "400|Y|N");

                if (CommonUtils.SaveGridData(griPipeCleaning, validationSet))
                {
                    Hashtable saveConditions = new Hashtable();
                    saveConditions.Add("pipeCleaningData", griPipeCleaning.DataSource as DataTable);

                    work.SavePipeCleaningDataList(saveConditions);

                    SelectPipeCleaningDataList();
                    MessageBox.Show("정상적으로 처리되었습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //관세척정보 조회 버튼 클릭 시 실행
        private void butSelectPcleanData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectPipeCleaningDataList();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //관련 파이프 저장 버튼 클릭 시 실행
        private void butSavePipe_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CommonUtils.SaveGridData(griPipe, null)) return;

                Hashtable conditions = new Hashtable();
                conditions.Add("pipeData", griPipe.DataSource as DataTable);

                work.SavePipeList(conditions);
                CommonUtils.ShowInformationMessage("정상적으로 처리되었습니다.");

                SelectPipeList();
                LoadAllPipeCleaningData();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //관련 파이프 삭제 버튼 클릭 시 실행
        private void butDelPipe_Click(object sender, EventArgs e)
        {
            try
            {
                Hashtable deleteData = new Hashtable();

                if ((deleteData = CommonUtils.DeleteGridRow(griPipe, "해당 파이프정보를 삭제하시겠습니까?")) == null) return;

                work.DeletePipeData(deleteData);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //관련 파이프 정보 더블클릭 시 실행
        private void griPipe_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            if ("FTR_IDN".Equals(e.Cell.Column.Key))
            {
                ownerForm.MoveFocusAt("상수관로", "FTR_IDN = '" + e.Cell.Value + "'");
            }
        }

        //==관망해석

        //관망해석 모델리스트 조회 버튼 클릭 시 실행
        private void butSelectModelList_Click(object sender, EventArgs e)
        {
            SelectModelList();
        }

        //관망해석 모델로딩 버튼 클릭 시 실행
        private void butLoadModel_Click(object sender, EventArgs e)
        {
            try
            {
                if (griModelList.Rows.Count == 0) return;

                this.Cursor = Cursors.WaitCursor;

                ownerForm.UnloadModelLayer();
                ownerForm.loadModelLayer(CommonUtils.GetGridCellValue(griModelList, "INP_NUMBER"));
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //관망해석 모델 로딩해제 버튼 클릭 시 실행
        private void butUnloadModel_Click(object sender, EventArgs e)
        {
            try
            {
                if ("".Equals(ownerForm.GetLoadedInpNumber())) return;

                this.Cursor = Cursors.WaitCursor;
                ownerForm.UnloadModelLayer();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //해석 필터 유속 Textbox 엔터키 입력 시 실행
        private void texVelocityBaseValue_KeyUp(object sender, KeyEventArgs e)
        {
            if ("".Equals(texVelocityBaseValue.Text) || "".Equals(comVelocityFormula.SelectedValue.ToString()) || !cheVelocity.Checked || !cWork.IsAnalysisModel()) return;

            if (e.KeyCode == Keys.Enter)    //엔터키 입력 시에만 동작
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texVelocityBaseValue.Text, comVelocityFormula.SelectedValue.ToString(), "velocity");

                    if (filtedDatas.Count != 0)
                    {
                        Hashtable tmpTable = new Hashtable();
                        tmpTable.Add("velocity", filtedDatas);

                        Hashtable showConditions = new Hashtable();
                        showConditions.Add("velocity", cheVelocity.Checked);
                        showConditions.Add("annotation", checAnnotation.Checked);

                        ownerForm.MapRendering(tmpTable, showConditions);
                    }
                }
                catch (Exception ex)
                {
                    CommonUtils.ShowExceptionMessage(ex.Message);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        //해석 필터 유속 Combobox 선택 변경 시 실행
        private void comVelocityFormula_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ("".Equals(texVelocityBaseValue.Text) || "".Equals(comVelocityFormula.SelectedValue.ToString()) || !cheVelocity.Checked || !cWork.IsAnalysisModel()) return;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texVelocityBaseValue.Text, comVelocityFormula.SelectedValue.ToString(), "velocity");

                if (filtedDatas.Count != 0)
                {
                    Hashtable tmpTable = new Hashtable();
                    tmpTable.Add("velocity", filtedDatas);

                    Hashtable showConditions = new Hashtable();
                    showConditions.Add("velocity", cheVelocity.Checked);
                    showConditions.Add("annotation", checAnnotation.Checked);

                    ownerForm.MapRendering(tmpTable, showConditions);
                    //ownerForm.ChangeDiableAnalysisResult("velocity");
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //유속 표출 checkbox 변경 시 실행
        private void cheVelocity_CheckedChanged(object sender, EventArgs e)
        {
            if (!cWork.IsAnalysisModel())     //해석이 실행되지 않았을 경우 checkbox를 check된 상태로 유지한다
            {
                //((CheckBox)sender).Checked = true;
                return;
            }

            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (((CheckBox)sender).Checked == true)
                {
                    //Render및 annotation을 활성화하기 위해 해석결과값을 다시 할당
                    //Hashtable filtedDatas = cWork.GetFilteredQualityDatas(texVelocityBaseValue.Text, comVelocityFormula.SelectedValue.ToString(), "velocity");

                    //if (filtedDatas.Count != 0)
                    //{
                    //    Hashtable tmpTable = new Hashtable();
                    //    tmpTable.Add("velocity", filtedDatas);
                    //    ownerForm.ChangeEnableAnalysisResult("node", tmpTable);
                    //}

                    ownerForm.ChangeEnableAnalysisResult("link", checAnnotation.Checked);

                    texVelocityBaseValue.Enabled = true;
                    comVelocityFormula.Enabled = true;
                }
                else if (((CheckBox)sender).Checked == false)
                {
                    ownerForm.ChangeDiableAnalysisResult("link");

                    texVelocityBaseValue.Enabled = false;
                    comVelocityFormula.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //관망해석 해석실행 버튼 클릭 시 실행
        private void butExecuteAnalysis_Click(object sender, EventArgs e)
        {
            if (griModelList.Rows.Count == 0) return;

            try
            {
                if(!cheVelocity.Checked)
                {
                    CommonUtils.ShowInformationMessage("표출항목을 선택하십시오.");
                    return;
                }

                this.Cursor = Cursors.WaitCursor;

                string inpNumber = "";

                //해석모델이 로딩되었는지 확인 후 로딩이 안된경우 모델 리스트에서 현재 선택된 모델을 로딩시킨다.
                if ("".Equals(inpNumber = ownerForm.GetLoadedInpNumber()))
                {
                    inpNumber = CommonUtils.GetGridCellValue(griModelList, "INP_NUMBER");
                    ownerForm.loadModelLayer(inpNumber);
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", inpNumber);
                conditions.Add("velocityBaseValue", texVelocityBaseValue.Text);
                conditions.Add("velocityFormula", comVelocityFormula.SelectedValue);

                Hashtable showConditions = new Hashtable();
                showConditions.Add("velocity", cheVelocity.Checked);
                showConditions.Add("annotation", checAnnotation.Checked);

                ownerForm.MapRendering(cWork.ExecuteQualityAnalysis(conditions), showConditions);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //값표시 checkbox 선택 시 실행
        private void checAnnotation_CheckedChanged(object sender, EventArgs e)
        {
            if (!cWork.IsAnalysisModel()) return;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                ownerForm.ShowAnnotation(((CheckBox)sender).Checked, false, cheVelocity.Checked);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        // --Ultra Tree 관련 이벤트

        private void treBlockList_AfterDataNodesCollectionPopulated(object sender, Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventArgs e)
        {
            foreach (UltraTreeNodeColumn column in e.Nodes.ColumnSetResolved.Columns)
                column.PerformAutoResize();
        }

        private void treBlockList_BeforeDataNodesCollectionPopulated(object sender, Infragistics.Win.UltraWinTree.BeforeDataNodesCollectionPopulatedEventArgs e)
        {
        }

        private void treBlockList_ColumnSetGenerated(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            e.ColumnSet.Columns["LOC_CODE"].Visible = false;
            e.ColumnSet.Columns["PLOC_CODE"].Visible = false;
            e.ColumnSet.Columns["SUMCNT"].Visible = false;
            e.ColumnSet.NodeTextColumn = e.ColumnSet.Columns["LOC_NAME"];
        }

        private void treBlockList_InitializeDataNode(object sender, Infragistics.Win.UltraWinTree.InitializeDataNodeEventArgs e)
        {
            if (e.Node.Parent == null &&
                e.Node.Cells["PLOC_CODE"].Value != DBNull.Value)
            {
                e.Node.Visible = false;
                return;
            }

            if (e.Node.Nodes.Count > 0)
            {
                if (e.Node.Parent != null)
                    e.Node.Override.NodeSpacingBefore = 0;
            }

            if (e.Node.Parent == null)
                e.Node.Expanded = true;

            int complaintCnt = int.Parse(e.Node.Cells["SUMCNT"].Value.ToString());

            if (complaintCnt > 10)
            {
                e.Node.Override.NodeAppearance.ForeColor = Color.Red;
            }
            else if (complaintCnt < 11 && complaintCnt > 5)
            {
                e.Node.Override.NodeAppearance.ForeColor = Color.DarkOrange;
            }
            else if (complaintCnt > 0 && complaintCnt < 6)
            {
                e.Node.Override.NodeAppearance.ForeColor = Color.Blue;
            }
            else
            {
                e.Node.Override.NodeAppearance.ForeColor = Color.Black;
            }
        }

        //Tree의 Node를 선택 시 실행
        private void treBlockList_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                if (e.NewSelections.Count != 0)
                {
                    this.Cursor = Cursors.WaitCursor;

                    SelectComplaintData(e.NewSelections[0].Cells["LOC_CODE"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //민원 조회버튼 클릭 시
        private void butSelectComplaint_Click(object sender, EventArgs e)
        {
            try
            {
                SelectComplaintCountByBlock();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        //민원내역 표출 Checkbox 클릭 시
        private void cheShowComplaint_CheckedChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (cheShowComplaint.Checked == true)
            {
                if (griComplaintList.Rows.Count > 0) ownerForm.ComplaintDataRendering(griComplaintList.DataSource as DataTable);
                else ownerForm.ReloadingMeterLayer();
            }
            else
            {
                ownerForm.ReloadingMeterLayer(); 
            }

            this.Cursor = Cursors.Default;
        }

        //민원 상세 Grid Cell 더블클릭 시
        private void griComplaintList_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            if("DMNO".Equals(e.Cell.Column.Key))
            {
                ownerForm.MoveFocusAt("수도계량기", "DMNO = '" + e.Cell.Value + "'");
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        //Pipe List Grid에 Drag된 Pipe의 FTR_IDN을 할당한다.
        public void SetPipeList(ArrayList pipeList)
        {
            foreach(string pipeFtrIdn in pipeList)
            {
                DataTable pipeGridData = this.griPipe.DataSource as DataTable;

                DataRow newRow = pipeGridData.NewRow();
                newRow["PCLEAN_NO"] = CommonUtils.GetGridCellValue(griPipeCleaning, "PCLEAN_NO");
                newRow["FTR_IDN"] = pipeFtrIdn;
                pipeGridData.Rows.Add(newRow);
            }

            CommonUtils.SelectFirstGridRow(griPipe);
        }

        #endregion

        #region 6.내부사용

        //그리드/트리 초기화
        private void InitializeSetting()
        {
            #region 관세척 리스트
            UltraGridColumn pipeCleaningColumn;

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "PCLEAN_NO";
            pipeCleaningColumn.Header.Caption = "관세척번호";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Center;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 100;
            pipeCleaningColumn.Hidden = true;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "PCLEAN_TITLE";
            pipeCleaningColumn.Header.Caption = "명칭";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 200;
            pipeCleaningColumn.Hidden = false;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "PROC_DAT";
            pipeCleaningColumn.Header.Caption = "관세척일";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Center;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 120;
            pipeCleaningColumn.Hidden = false;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "REG_DAT";
            pipeCleaningColumn.Header.Caption = "등록일";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Center;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 120;
            pipeCleaningColumn.Hidden = false;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "PROC_METHOD";
            pipeCleaningColumn.Header.Caption = "세척방법상세";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 100;
            pipeCleaningColumn.Hidden = true;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "PROC_RESULT";
            pipeCleaningColumn.Header.Caption = "세척결과상세";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 100;
            pipeCleaningColumn.Hidden = true;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "PROC_METHOD_TITLE";
            pipeCleaningColumn.Header.Caption = "세척방법";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 100;
            pipeCleaningColumn.Hidden = false;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "PROC_RESULT_TITLE";
            pipeCleaningColumn.Header.Caption = "세척결과";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 100;
            pipeCleaningColumn.Hidden = false;   //필드 보이기

            pipeCleaningColumn = griPipeCleaning.DisplayLayout.Bands[0].Columns.Add();
            pipeCleaningColumn.Key = "REMARK";
            pipeCleaningColumn.Header.Caption = "비고";
            pipeCleaningColumn.CellActivation = Activation.NoEdit;
            pipeCleaningColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeCleaningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeCleaningColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeCleaningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeCleaningColumn.Width = 200;
            pipeCleaningColumn.Hidden = false;   //필드 보이기

            

            #endregion

            #region 세척대상 관로 리스트
            UltraGridColumn pipeColumn;

            pipeColumn = griPipe.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "PCLEAN_NO";
            pipeColumn.Header.Caption = "관세척번호";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeColumn.Width = 100;
            pipeColumn.Hidden = true;   //필드 보이기

            pipeColumn = griPipe.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "FTR_IDN";
            pipeColumn.Header.Caption = "관로번호";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Center;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            pipeColumn.Width = 150;
            pipeColumn.Hidden = false;   //필드 보이기
            #endregion

            #region 모델 리스트
            UltraGridColumn modelColumn;

            modelColumn = griModelList.DisplayLayout.Bands[0].Columns.Add();
            modelColumn.Key = "INP_NUMBER";
            modelColumn.Header.Caption = "모델번호";
            modelColumn.CellActivation = Activation.NoEdit;
            modelColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            modelColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            modelColumn.CellAppearance.TextHAlign = HAlign.Left;
            modelColumn.CellAppearance.TextVAlign = VAlign.Middle;
            modelColumn.Width = 100;
            modelColumn.Hidden = false;   //필드 보이기

            modelColumn = griModelList.DisplayLayout.Bands[0].Columns.Add();
            modelColumn.Key = "CON_INS_DATE";
            modelColumn.Header.Caption = "등록일";
            modelColumn.CellActivation = Activation.NoEdit;
            modelColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            modelColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            modelColumn.CellAppearance.TextHAlign = HAlign.Center;
            modelColumn.CellAppearance.TextVAlign = VAlign.Middle;
            modelColumn.Width = 150;
            modelColumn.Hidden = false;   //필드 보이기

            modelColumn = griModelList.DisplayLayout.Bands[0].Columns.Add();
            modelColumn.Key = "TITLE";
            modelColumn.Header.Caption = "제목";
            modelColumn.CellActivation = Activation.NoEdit;
            modelColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            modelColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            modelColumn.CellAppearance.TextHAlign = HAlign.Left;
            modelColumn.CellAppearance.TextVAlign = VAlign.Middle;
            modelColumn.Width = 400;
            modelColumn.Hidden = false;   //필드 보이기
            #endregion

            #region 블록별 민원 건수 (Tree)
            treBlockList.Override.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnDisplay;
            treBlockList.ViewStyle = Infragistics.Win.UltraWinTree.ViewStyle.Standard;

            treBlockList.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(this.treBlockList_AfterDataNodesCollectionPopulated);
            treBlockList.BeforeDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.BeforeDataNodesCollectionPopulatedEventHandler(this.treBlockList_BeforeDataNodesCollectionPopulated);
            treBlockList.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.treBlockList_ColumnSetGenerated);
            treBlockList.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.treBlockList_InitializeDataNode);
            #endregion

            #region 민원 현황

            UltraGridColumn complaintStatColumn;

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "YM";
            complaintStatColumn.Header.Caption = "발생년월";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 60;
            complaintStatColumn.Hidden = false;   //필드 보이기

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "COL_2010";
            complaintStatColumn.Header.Caption = "흑수발생";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 65;
            complaintStatColumn.Hidden = false;   //필드 보이기

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "COL_2015";
            complaintStatColumn.Header.Caption = "적수발생";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 65;
            complaintStatColumn.Hidden = false;   //필드 보이기

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "COL_2030";
            complaintStatColumn.Header.Caption = "이물질발생";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 70;
            complaintStatColumn.Hidden = false;   //필드 보이기

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "COL_2040";
            complaintStatColumn.Header.Caption = "흙탕물발생";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 70;
            complaintStatColumn.Hidden = false;   //필드 보이기

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "COL_2050";
            complaintStatColumn.Header.Caption = "악취발생";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 65;
            complaintStatColumn.Hidden = false;   //필드 보이기

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "COL_2099";
            complaintStatColumn.Header.Caption = "수질기타";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 65;
            complaintStatColumn.Hidden = false;   //필드 보이기

            complaintStatColumn = griComplaintState.DisplayLayout.Bands[0].Columns.Add();
            complaintStatColumn.Key = "TOT_SUM";
            complaintStatColumn.Header.Caption = "합";
            complaintStatColumn.CellActivation = Activation.NoEdit;
            complaintStatColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintStatColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintStatColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintStatColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintStatColumn.Width = 60;
            complaintStatColumn.Hidden = false;   //필드 보이기

            #endregion

            #region 민원상세
            UltraGridColumn complaintColumn;

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CANO";
            complaintColumn.Header.Caption = "관리번호";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = true;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "SFTRIDN";
            complaintColumn.Header.Caption = "블록번호";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = true;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "LOC_NAME";
            complaintColumn.Header.Caption = "블록명";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = false;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CAAPPLDT";
            complaintColumn.Header.Caption = "민원신청일";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = false;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "DMNO";
            complaintColumn.Header.Caption = "수용가번호";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = false;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CANM";
            complaintColumn.Header.Caption = "신청자명";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = true;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CAADDR";
            complaintColumn.Header.Caption = "신청자주소";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Left;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 200;
            complaintColumn.Hidden = true;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CALRGCD";
            complaintColumn.Header.Caption = "민원구분코드";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = true;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CALRGCD_NM";
            complaintColumn.Header.Caption = "민원구분";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 100;
            complaintColumn.Hidden = false;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CAMIDCD";
            complaintColumn.Header.Caption = "민원분류코드";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 150;
            complaintColumn.Hidden = true;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CAMIDCD_NM";
            complaintColumn.Header.Caption = "민원분류";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Center;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 150;
            complaintColumn.Hidden = false;   //필드 보이기

            complaintColumn = griComplaintList.DisplayLayout.Bands[0].Columns.Add();
            complaintColumn.Key = "CACONT";
            complaintColumn.Header.Caption = "일반민원내용";
            complaintColumn.CellActivation = Activation.NoEdit;
            complaintColumn.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            complaintColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            complaintColumn.CellAppearance.TextHAlign = HAlign.Left;
            complaintColumn.CellAppearance.TextVAlign = VAlign.Middle;
            complaintColumn.Width = 300;
            complaintColumn.Hidden = false;   //필드 보이기
            #endregion
        }

        //==관세척 정보

        //관세척 정보 조회
        private void SelectPipeCleaningDataList()
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("startDate", ((DateTime)datStartDate.Value).ToString("yyyyMMdd"));
            conditions.Add("endDate", ((DateTime)datEndDate.Value).ToString("yyyyMMdd"));
            conditions.Add("selectAll", cheSelectAll.Checked);

            if (!"".Equals(texCondPcleanTitle.Text.Trim())) conditions.Add("PCLEAN_TITLE", texCondPcleanTitle.Text);

            griPipeCleaning.DataSource = work.SelectPipeCleaningDataList(conditions);
            CommonUtils.SelectFirstGridRow(griPipeCleaning);
        }

        //관련 Pipe정보 조회
        private void SelectPipeList()
        {
            //세척대상 관로정보 조회
            Hashtable pipeConditions = new Hashtable();
            pipeConditions.Add("PCLEAN_NO", CommonUtils.GetGridCellValue(griPipeCleaning, "PCLEAN_NO"));

            griPipe.DataSource = work.SelectPipeList(pipeConditions);

            CommonUtils.SelectFirstGridRow(griPipe);
        }

        //전체 세척 파이프 정보를 조회하여 GIS에 표출
        private void LoadAllPipeCleaningData()
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PCLEAN_NO", CommonUtils.GetGridCellValue(griPipeCleaning, "PCLEAN_NO"));

            DataTable allCleaningPipeData = work.SelectAllCleaningPipeList(conditions);

            if (allCleaningPipeData.Rows.Count < 1) return;
                
            Hashtable cleaningDatas = new Hashtable();
            cleaningDatas.Add("allPipeCleaningData", allCleaningPipeData);

            ownerForm.PipeCleaningDataRendering(cleaningDatas);
        }

        //==관망해석

        //관망해석 모델 리스트 조회
        private void SelectModelList()
        {
            Hashtable conditions = new Hashtable();

            if(!"".Equals(texTitle.Text.Trim()))
            {
                conditions.Add("TITLE", texTitle.Text);
            }

            griModelList.DataSource = work.SelectModelList(conditions);
            CommonUtils.SelectFirstGridRow(griModelList);
        }

        //==민원

        //민원 블록 리스트 조회
        private void SelectComplaintCountByBlock()
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("startDate", ((DateTime)datComStartDate.Value).ToString("yyyyMMdd"));
            conditions.Add("endDate", ((DateTime)datComEndDate.Value).ToString("yyyyMMdd"));
            conditions.Add("CAMIDCD", comComplaintType.SelectedValue.ToString());

            DataTable complaintBlockList = work.SelectComplaintCountByBlock(conditions);

            DataSet ds = new DataSet();
            complaintBlockList.TableName = "complaintBlockList";
            ds.Tables.Add(complaintBlockList);
            ds.Relations.Add("complaintBlockList", ds.Tables["complaintBlockList"].Columns["LOC_CODE"], ds.Tables["complaintBlockList"].Columns["PLOC_CODE"], false);

            treBlockList.SetDataBinding(ds, "complaintBlockList");

            if (treBlockList.Nodes.Count > 0)
            {
                treBlockList.Nodes[0].Selected = true;
                treBlockList.Nodes[0].CheckedState = CheckState.Checked;
            }
        }

        //민원 현황 및 상세 조회
        private void SelectComplaintData(string locCode)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("LOC_CODE", locCode);
            conditions.Add("startDate", ((DateTime)datComStartDate.Value).ToString("yyyyMMdd"));
            conditions.Add("endDate", ((DateTime)datComEndDate.Value).ToString("yyyyMMdd"));
            conditions.Add("CAMIDCD", comComplaintType.SelectedValue.ToString());

            Hashtable complaintData = work.SelectComplaintData(conditions);

            griComplaintList.DataSource = complaintData["complaintList"] as DataTable;
            griComplaintState.DataSource = complaintData["complaintState"] as DataTable;

            //GIS상 민원내역 표출
            if (cheShowComplaint.Checked == true) ownerForm.ComplaintDataRendering(complaintData["complaintList"] as DataTable);
        }
        
        #endregion        
    }
}
