﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground1 = new ChartFX.WinForms.Adornments.SolidBackground();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground2 = new ChartFX.WinForms.Adornments.SolidBackground();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.griRealtimeQuality = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chaTimeSeriesQuality = new ChartFX.WinForms.Chart();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.comQualityItem = new System.Windows.Forms.ComboBox();
            this.datStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.datEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chaQualityChain = new ChartFX.WinForms.Chart();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butQualityChainDetail = new System.Windows.Forms.Button();
            this.butSelectChain = new System.Windows.Forms.Button();
            this.datTimestamp = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeQuality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaTimeSeriesQuality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaQualityChain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datTimestamp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(425, 9);
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 1009);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(425, 9);
            this.pictureBox1.TabIndex = 125;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox6.Location = new System.Drawing.Point(0, 9);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 1000);
            this.pictureBox6.TabIndex = 126;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(415, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 1000);
            this.pictureBox3.TabIndex = 127;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.griRealtimeQuality);
            this.groupBox1.Controls.Add(this.pictureBox8);
            this.groupBox1.Controls.Add(this.pictureBox7);
            this.groupBox1.Controls.Add(this.pictureBox5);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(405, 466);
            this.groupBox1.TabIndex = 128;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "실시간수질정보";
            // 
            // griRealtimeQuality
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.griRealtimeQuality.DisplayLayout.Appearance = appearance1;
            this.griRealtimeQuality.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQuality.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQuality.DisplayLayout.MaxColScrollRegions = 1;
            this.griRealtimeQuality.DisplayLayout.MaxRowScrollRegions = 1;
            this.griRealtimeQuality.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQuality.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQuality.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRealtimeQuality.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griRealtimeQuality.DisplayLayout.Override.CellMultiLine = Infragistics.Win.DefaultableBoolean.True;
            this.griRealtimeQuality.DisplayLayout.Override.CellPadding = 0;
            this.griRealtimeQuality.DisplayLayout.Override.DefaultRowHeight = 30;
            this.griRealtimeQuality.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griRealtimeQuality.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griRealtimeQuality.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griRealtimeQuality.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griRealtimeQuality.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectorAppearance = appearance2;
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griRealtimeQuality.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griRealtimeQuality.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griRealtimeQuality.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;
            this.griRealtimeQuality.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griRealtimeQuality.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griRealtimeQuality.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griRealtimeQuality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griRealtimeQuality.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.griRealtimeQuality.Location = new System.Drawing.Point(13, 26);
            this.griRealtimeQuality.Name = "griRealtimeQuality";
            this.griRealtimeQuality.Size = new System.Drawing.Size(379, 428);
            this.griRealtimeQuality.TabIndex = 152;
            this.griRealtimeQuality.Text = "ultraGrid1";
            this.griRealtimeQuality.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.griRealtimeQuality_DoubleClickCell);
            this.griRealtimeQuality.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griRealtimeQuality_AfterSelectChange);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox8.Location = new System.Drawing.Point(392, 26);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 428);
            this.pictureBox8.TabIndex = 128;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(3, 26);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 428);
            this.pictureBox7.TabIndex = 127;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(3, 454);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(399, 9);
            this.pictureBox5.TabIndex = 126;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(3, 17);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(399, 9);
            this.pictureBox4.TabIndex = 125;
            this.pictureBox4.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(10, 475);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(405, 10);
            this.splitter1.TabIndex = 129;
            this.splitter1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chaTimeSeriesQuality);
            this.groupBox2.Controls.Add(this.pictureBox13);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.pictureBox12);
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.Controls.Add(this.pictureBox10);
            this.groupBox2.Controls.Add(this.pictureBox9);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(10, 485);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(405, 257);
            this.groupBox2.TabIndex = 130;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "시계열수질분석";
            // 
            // chaTimeSeriesQuality
            // 
            this.chaTimeSeriesQuality.AllSeries.Line.Width = ((short)(1));
            this.chaTimeSeriesQuality.AllSeries.MarkerShape = ChartFX.WinForms.MarkerShape.None;
            this.chaTimeSeriesQuality.AllSeries.MarkerSize = ((short)(2));
            solidBackground1.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chaTimeSeriesQuality.Background = solidBackground1;
            this.chaTimeSeriesQuality.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chaTimeSeriesQuality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaTimeSeriesQuality.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chaTimeSeriesQuality.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chaTimeSeriesQuality.LegendBox.Visible = false;
            this.chaTimeSeriesQuality.Location = new System.Drawing.Point(13, 66);
            this.chaTimeSeriesQuality.Name = "chaTimeSeriesQuality";
            this.chaTimeSeriesQuality.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chaTimeSeriesQuality.Size = new System.Drawing.Size(379, 179);
            this.chaTimeSeriesQuality.TabIndex = 132;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox13.Location = new System.Drawing.Point(13, 57);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(379, 9);
            this.pictureBox13.TabIndex = 131;
            this.pictureBox13.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraLabel2);
            this.panel1.Controls.Add(this.comQualityItem);
            this.panel1.Controls.Add(this.datStartDate);
            this.panel1.Controls.Add(this.datEndDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(13, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(379, 31);
            this.panel1.TabIndex = 130;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Location = new System.Drawing.Point(104, 8);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 14);
            this.ultraLabel2.TabIndex = 194;
            this.ultraLabel2.Text = "-";
            // 
            // comQualityItem
            // 
            this.comQualityItem.FormattingEnabled = true;
            this.comQualityItem.Location = new System.Drawing.Point(239, 6);
            this.comQualityItem.Name = "comQualityItem";
            this.comQualityItem.Size = new System.Drawing.Size(134, 20);
            this.comQualityItem.TabIndex = 193;
            this.comQualityItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comQualityItem_KeyDown);
            // 
            // datStartDate
            // 
            this.datStartDate.DateTime = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            this.datStartDate.Location = new System.Drawing.Point(4, 5);
            this.datStartDate.MaskInput = "{date}";
            this.datStartDate.Name = "datStartDate";
            this.datStartDate.Size = new System.Drawing.Size(100, 21);
            this.datStartDate.TabIndex = 156;
            this.datStartDate.Value = new System.DateTime(2014, 10, 8, 0, 0, 0, 0);
            this.datStartDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.datStartDate_KeyDown);
            // 
            // datEndDate
            // 
            this.datEndDate.DateTime = new System.DateTime(2014, 10, 20, 0, 0, 0, 0);
            this.datEndDate.Location = new System.Drawing.Point(116, 5);
            this.datEndDate.MaskInput = "{date}";
            this.datEndDate.Name = "datEndDate";
            this.datEndDate.Size = new System.Drawing.Size(100, 21);
            this.datEndDate.TabIndex = 157;
            this.datEndDate.Value = new System.DateTime(2014, 10, 20, 0, 0, 0, 0);
            this.datEndDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.datEndDate_KeyDown);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox12.Location = new System.Drawing.Point(392, 26);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(10, 219);
            this.pictureBox12.TabIndex = 129;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox11.Location = new System.Drawing.Point(3, 26);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(10, 219);
            this.pictureBox11.TabIndex = 128;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox10.Location = new System.Drawing.Point(3, 245);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(399, 9);
            this.pictureBox10.TabIndex = 127;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(3, 17);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(399, 9);
            this.pictureBox9.TabIndex = 126;
            this.pictureBox9.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(10, 742);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(405, 10);
            this.splitter2.TabIndex = 131;
            this.splitter2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chaQualityChain);
            this.groupBox3.Controls.Add(this.pictureBox18);
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.Controls.Add(this.pictureBox17);
            this.groupBox3.Controls.Add(this.pictureBox16);
            this.groupBox3.Controls.Add(this.pictureBox15);
            this.groupBox3.Controls.Add(this.pictureBox14);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(10, 752);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(405, 257);
            this.groupBox3.TabIndex = 132;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "계통별수질분석";
            // 
            // chaQualityChain
            // 
            this.chaQualityChain.AllSeries.Line.Width = ((short)(1));
            this.chaQualityChain.AllSeries.MarkerShape = ChartFX.WinForms.MarkerShape.None;
            this.chaQualityChain.AllSeries.MarkerSize = ((short)(2));
            solidBackground2.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chaQualityChain.Background = solidBackground2;
            this.chaQualityChain.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chaQualityChain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaQualityChain.Font = new System.Drawing.Font("굴림", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chaQualityChain.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chaQualityChain.LegendBox.Visible = false;
            this.chaQualityChain.Location = new System.Drawing.Point(13, 66);
            this.chaQualityChain.Name = "chaQualityChain";
            this.chaQualityChain.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chaQualityChain.Size = new System.Drawing.Size(379, 179);
            this.chaQualityChain.TabIndex = 133;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox18.Location = new System.Drawing.Point(13, 57);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(379, 9);
            this.pictureBox18.TabIndex = 134;
            this.pictureBox18.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.butQualityChainDetail);
            this.panel2.Controls.Add(this.butSelectChain);
            this.panel2.Controls.Add(this.datTimestamp);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(13, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(379, 31);
            this.panel2.TabIndex = 131;
            // 
            // butQualityChainDetail
            // 
            this.butQualityChainDetail.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.butQualityChainDetail.Location = new System.Drawing.Point(314, 5);
            this.butQualityChainDetail.Name = "butQualityChainDetail";
            this.butQualityChainDetail.Size = new System.Drawing.Size(62, 23);
            this.butQualityChainDetail.TabIndex = 160;
            this.butQualityChainDetail.Text = "상세보기";
            this.butQualityChainDetail.UseVisualStyleBackColor = true;
            this.butQualityChainDetail.Click += new System.EventHandler(this.butQualityChainDetail_Click);
            // 
            // butSelectChain
            // 
            this.butSelectChain.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.butSelectChain.Location = new System.Drawing.Point(251, 5);
            this.butSelectChain.Name = "butSelectChain";
            this.butSelectChain.Size = new System.Drawing.Size(62, 23);
            this.butSelectChain.TabIndex = 159;
            this.butSelectChain.Text = "계통선택";
            this.butSelectChain.UseVisualStyleBackColor = true;
            this.butSelectChain.Click += new System.EventHandler(this.butSelectChain_Click);
            // 
            // datTimestamp
            // 
            this.datTimestamp.Location = new System.Drawing.Point(5, 5);
            this.datTimestamp.MaskInput = "{LOC}yyyy-mm-dd hh:mm";
            this.datTimestamp.Name = "datTimestamp";
            this.datTimestamp.Size = new System.Drawing.Size(138, 21);
            this.datTimestamp.TabIndex = 158;
            this.datTimestamp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.datTimestamp_KeyDown);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox17.Location = new System.Drawing.Point(392, 26);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(10, 219);
            this.pictureBox17.TabIndex = 130;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox16.Location = new System.Drawing.Point(3, 26);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(10, 219);
            this.pictureBox16.TabIndex = 129;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox15.Location = new System.Drawing.Point(3, 245);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(399, 9);
            this.pictureBox15.TabIndex = 128;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox14.Location = new System.Drawing.Point(3, 17);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(399, 9);
            this.pictureBox14.TabIndex = 127;
            this.pictureBox14.TabStop = false;
            // 
            // frmDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 1018);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmDashboard";
            this.Text = "수질정보현황";
            this.Load += new System.EventHandler(this.frmDashboard_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDashboard_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeQuality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaTimeSeriesQuality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaQualityChain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datTimestamp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        public Infragistics.Win.UltraWinGrid.UltraGrid griRealtimeQuality;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox13;
        private ChartFX.WinForms.Chart chaTimeSeriesQuality;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datStartDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndDate;
        private System.Windows.Forms.ComboBox comQualityItem;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private ChartFX.WinForms.Chart chaQualityChain;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datTimestamp;
        private System.Windows.Forms.Button butSelectChain;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Button butQualityChainDetail;
    }
}