﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmChartSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groChartSetting = new System.Windows.Forms.GroupBox();
            this.butApply = new System.Windows.Forms.Button();
            this.texDec = new System.Windows.Forms.TextBox();
            this.texMin = new System.Windows.Forms.TextBox();
            this.texMax = new System.Windows.Forms.TextBox();
            this.texStep = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groChartSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(165, 9);
            this.pictureBox2.TabIndex = 117;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 158);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(165, 9);
            this.pictureBox1.TabIndex = 118;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 149);
            this.picFrLeftM1.TabIndex = 120;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(155, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 149);
            this.pictureBox3.TabIndex = 121;
            this.pictureBox3.TabStop = false;
            // 
            // groChartSetting
            // 
            this.groChartSetting.Controls.Add(this.texDec);
            this.groChartSetting.Controls.Add(this.texMin);
            this.groChartSetting.Controls.Add(this.texMax);
            this.groChartSetting.Controls.Add(this.texStep);
            this.groChartSetting.Controls.Add(this.label4);
            this.groChartSetting.Controls.Add(this.label3);
            this.groChartSetting.Controls.Add(this.label2);
            this.groChartSetting.Controls.Add(this.label1);
            this.groChartSetting.Location = new System.Drawing.Point(10, 9);
            this.groChartSetting.Name = "groChartSetting";
            this.groChartSetting.Size = new System.Drawing.Size(145, 120);
            this.groChartSetting.TabIndex = 122;
            this.groChartSetting.TabStop = false;
            this.groChartSetting.Text = "Y축 설정";
            // 
            // butApply
            // 
            this.butApply.Location = new System.Drawing.Point(58, 135);
            this.butApply.Name = "butApply";
            this.butApply.Size = new System.Drawing.Size(40, 23);
            this.butApply.TabIndex = 8;
            this.butApply.Text = "적용";
            this.butApply.UseVisualStyleBackColor = true;
            this.butApply.Click += new System.EventHandler(this.butApply_Click);
            // 
            // texDec
            // 
            this.texDec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texDec.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texDec.Location = new System.Drawing.Point(77, 89);
            this.texDec.Name = "texDec";
            this.texDec.Size = new System.Drawing.Size(58, 20);
            this.texDec.TabIndex = 7;
            this.texDec.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texDec_KeyUp);
            // 
            // texMin
            // 
            this.texMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMin.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texMin.Location = new System.Drawing.Point(77, 66);
            this.texMin.Name = "texMin";
            this.texMin.Size = new System.Drawing.Size(58, 20);
            this.texMin.TabIndex = 6;
            this.texMin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texMin_KeyUp);
            // 
            // texMax
            // 
            this.texMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texMax.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texMax.Location = new System.Drawing.Point(77, 42);
            this.texMax.Name = "texMax";
            this.texMax.Size = new System.Drawing.Size(58, 20);
            this.texMax.TabIndex = 5;
            this.texMax.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texMax_KeyUp);
            // 
            // texStep
            // 
            this.texStep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texStep.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texStep.Location = new System.Drawing.Point(77, 19);
            this.texStep.Name = "texStep";
            this.texStep.Size = new System.Drawing.Size(58, 20);
            this.texStep.TabIndex = 4;
            this.texStep.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texStep_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "소수점이하";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "최소값";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "최대값";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "증가단위";
            // 
            // frmChartSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(165, 167);
            this.Controls.Add(this.butApply);
            this.Controls.Add(this.groChartSetting);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChartSetting";
            this.Text = "차트설정";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmChartSetting_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmChartSetting_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groChartSetting.ResumeLayout(false);
            this.groChartSetting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groChartSetting;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox texMin;
        private System.Windows.Forms.TextBox texMax;
        private System.Windows.Forms.TextBox texStep;
        private System.Windows.Forms.Button butApply;
        private System.Windows.Forms.TextBox texDec;
    }
}