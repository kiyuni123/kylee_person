﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WQ_Analysis.work;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmQualityChainSelect : Form
    {
        #region 1.전역번수

        private RealtimeDataWork work = null;


        #endregion

        #region 2.초기화

        public frmQualityChainSelect()
        {
            InitializeComponent();
            InitializeSetting();
            work = RealtimeDataWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //폼 최초 로딩 시 실행
        private void frmQualityChainSelect_Load(object sender, EventArgs e)
        {
            griQualityChainList.DataSource = work.SelectQualityChainList(new Hashtable());

            if(griQualityChainList.Rows.Count > 0)
            {
                griQualityChainList.Rows[0].Selected = true;
                griQualityChainList.Rows[0].Activate();
            }
        }

        //선택버튼 클릭 시 실행
        private void butSelectQualityChain_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (griQualityChainList.Selected.Rows.Count > 0)
                {
                    if ("frmDashboard".Equals(this.Owner.Name))
                    {
                        //Dashboard에서 호출 시
                        frmDashboard ownerForm = (frmDashboard)this.Owner;
                        string chainNo = CommonUtils.GetGridCellValue(griQualityChainList, "CHAIN_NO");
                        string chainNm = CommonUtils.GetGridCellValue(griQualityChainList, "CHAIN_NM");

                        ownerForm.chainNo = chainNo;
                        ownerForm.chainNm = chainNm;
                        ownerForm.SelectQualityChainDataForDashboard(chainNo);
                    }
                    else
                    {
                        frmRealtimeQualityChainChart chainChartForm = new frmRealtimeQualityChainChart();
                        chainChartForm.Owner = this.Owner;
                        chainChartForm.chainNo = CommonUtils.GetGridCellValue(griQualityChainList, "CHAIN_NO");
                        chainChartForm.chainNm = CommonUtils.GetGridCellValue(griQualityChainList, "CHAIN_NM");
                        chainChartForm.Show();

                        frmQualityAnalysisMap mapForm = this.Owner as frmQualityAnalysisMap;
                        mapForm.qualityChainForms.Add(CommonUtils.GetSerialNumber("CH"), chainChartForm);
                    }

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.WaitCursor;
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        #endregion

        #region 6.내부사용

        //Grid 초기화
        private void InitializeSetting()
        {
            UltraGridColumn chainListColumn;

            chainListColumn = griQualityChainList.DisplayLayout.Bands[0].Columns.Add();
            chainListColumn.Key = "CHAIN_NO";
            chainListColumn.Header.Caption = "계통일련번호";
            chainListColumn.CellActivation = Activation.NoEdit;
            chainListColumn.CellClickAction = CellClickAction.RowSelect;
            chainListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainListColumn.CellAppearance.TextHAlign = HAlign.Center;
            chainListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainListColumn.Width = 100;
            chainListColumn.Hidden = true;   //필드 보이기

            chainListColumn = griQualityChainList.DisplayLayout.Bands[0].Columns.Add();
            chainListColumn.Key = "CHAIN_NM";
            chainListColumn.Header.Caption = "계통명";
            chainListColumn.CellActivation = Activation.NoEdit;
            chainListColumn.CellClickAction = CellClickAction.RowSelect;
            chainListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainListColumn.CellAppearance.TextHAlign = HAlign.Left;
            chainListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainListColumn.Width = 100;
            chainListColumn.Hidden = false;   //필드 보이기

            chainListColumn = griQualityChainList.DisplayLayout.Bands[0].Columns.Add();
            chainListColumn.Key = "REMARK";
            chainListColumn.Header.Caption = "비고";
            chainListColumn.CellActivation = Activation.NoEdit;
            chainListColumn.CellClickAction = CellClickAction.RowSelect;
            chainListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainListColumn.CellAppearance.TextHAlign = HAlign.Left;
            chainListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainListColumn.Width = 200;
            chainListColumn.Hidden = false;   //필드 보이기

            chainListColumn = griQualityChainList.DisplayLayout.Bands[0].Columns.Add();
            chainListColumn.Key = "CHAINDATA";
            chainListColumn.Header.Caption = "계통정보";
            chainListColumn.CellActivation = Activation.NoEdit;
            chainListColumn.CellClickAction = CellClickAction.RowSelect;
            chainListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            chainListColumn.CellAppearance.TextHAlign = HAlign.Left;
            chainListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            chainListColumn.Width = 500;
            chainListColumn.Hidden = false;   //필드 보이기
        }

        #endregion
    }
}
