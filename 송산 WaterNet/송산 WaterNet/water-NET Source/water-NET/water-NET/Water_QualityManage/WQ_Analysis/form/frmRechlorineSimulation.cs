﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WQ_Analysis.work;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using WaterNet.WQ_Analysis.util;
using Infragistics.Win.UltraWinEditors;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmRechlorineSimulation : Form
    {
        #region 1.전역번수

        private RechlorineSimulationWork work = null;
        public frmQualityAnalysis qualityAnalysisForm = null;

        #endregion

        #region 2.초기화

        public frmRechlorineSimulation()
        {
            InitializeComponent();
            InitializeSetting();
            work = RechlorineSimulationWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //폼 최초 로딩 시 실행
        private void frmChlorineAnalysis_Load(object sender, EventArgs e)
        {
            SelectRechlorineSimulationList();

            Console.WriteLine("USER ID : " + EMFrame.statics.AppStatic.USER_ID);
            Console.WriteLine("USER NAME : " + EMFrame.statics.AppStatic.USER_NAME);
        }

        //폼 close 시 실행
        private void frmRechlorineSimulation_FormClosing(object sender, FormClosingEventArgs e)
        {
            (this.Owner as frmQualityAnalysisMap).CloseFormAction("frmRechlorineSimulation");
        }

        //조회버튼 클릭 시 실행
        private void butSelectSimulation_Click(object sender, EventArgs e)
        {
            SelectRechlorineSimulationList();
        }

        //시뮬레이션 추가버튼 클릭 시 실행
        private void butAddSimulation_Click(object sender, EventArgs e)
        {
            CommonUtils.AddGridRow(griRechlorineSimulation);
        }

        //시뮬레이션 삭제버튼 클릭 시 실행
        private void butDeleteSimulation_Click(object sender, EventArgs e)
        {
            //모의 창이 떠있는경우 삭제가 실행되면 안됨
            if(qualityAnalysisForm != null)
            {
                MessageBox.Show("재염소지점 모의 창이 열려있습니다.\n종료 후 실행하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Hashtable deleteData = null;

            if((deleteData = CommonUtils.DeleteGridRow(griRechlorineSimulation, "삭제 시 설정된 지점정보도 모두 삭제됩니다.\n삭제하시겠습니까?")) != null)
            {
                Hashtable deleteConditions = new Hashtable();
                deleteConditions.Add("SIMULATION_NO", deleteData["SIMULATION_NO"].ToString());
                deleteConditions.Add("INP_NUMBER", deleteData["INP_NUMBER"].ToString());

                work.DeleteRechlorineSimulationData(deleteConditions);
            }
        }

        //시뮬레이션 저장버튼 클릭 시 실행
        private void butSaveSimulation_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //validation 정보 (사이즈|공백허용|숫자여부)
                Hashtable validationSet = new Hashtable();
                validationSet.Add("SIMULATION_NM", "400|N|N");
                validationSet.Add("TITLE", "48|N|N");
                validationSet.Add("REMARK", "400|Y|N");

                if (CommonUtils.SaveGridData(griRechlorineSimulation, validationSet))
                {
                    Hashtable saveConditions = new Hashtable();
                    saveConditions.Add("simulationData", griRechlorineSimulation.DataSource as DataTable);

                    work.SaveRechlorineSimulationList(saveConditions);

                    SelectRechlorineSimulationList();
                    MessageBox.Show("정상적으로 처리되었습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //simulation grid row 변경 시 실행
        private void griRechlorineSimulation_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            texSimulationNm.Text = CommonUtils.GetGridCellValue(griRechlorineSimulation, "SIMULATION_NM");
            texTitle.Text = CommonUtils.GetGridCellValue(griRechlorineSimulation, "TITLE");
            texRemark.Text = CommonUtils.GetGridCellValue(griRechlorineSimulation, "REMARK");

            //재염소 처리지점 리스트 조회
            if (!"".Equals(CommonUtils.GetGridCellValue(griRechlorineSimulation, "SIMULATION_NO")))
            {
                SelectRechlorinePositionList();
            }
            else
            {
                if (griRechlorinePosition.DataSource == null) return;       //재염소모의 최초 생성 시 행을 추가할때

                (griRechlorinePosition.DataSource as DataTable).Clear();
                texTagname.Text = "";
            }
        }

        //명칭 Text 변경 시 실행
        private void texSimulationNm_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griRechlorineSimulation, "SIMULATION_NM", (sender as TextBox).Text);
        }

        //비고 Text 변경 시 실행
        private void texRemark_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griRechlorineSimulation, "REMARK", (sender as TextBox).Text);
        }

        //모델선택 버튼 클릭 시
        private void butModelSelect_Click(object sender, EventArgs e)
        {
            if (griRechlorineSimulation.Selected.Rows.Count > 0 && CommonUtils.GetRowState(griRechlorineSimulation) == DataRowState.Added)
            {
                frmModelSelect modelSelectForm = new frmModelSelect();
                modelSelectForm.Owner = this;
                modelSelectForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("신규등록 시에만 모델 선택이 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        } 

        //position grid row 변경 시 실행
        private void griRechlorinePosition_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            texTagname.Text = CommonUtils.GetGridCellValue(griRechlorinePosition, "TAGNAME");
        }

        //TAG Text 변경 시 실행
        private void texTagname_TextChanged(object sender, EventArgs e)
        {
            CommonUtils.SetGridCellValue(griRechlorinePosition, "TAGNAME", (sender as TextBox).Text);
        }

        //재염소지점 모의 버튼 클릭 시 실행
        private void butQualityAnalysis_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (griRechlorineSimulation.Selected.Rows.Count > 0 && CommonUtils.GetRowState(griRechlorineSimulation) != DataRowState.Added)
                {
                    //재염소지점 모의창이 열려있는 경우
                    if (qualityAnalysisForm != null)
                    {
                        qualityAnalysisForm.Close();        //close 이벤트에 레이어 언로드 기능이 있기때문에 그냥 닫아준다.
                        qualityAnalysisForm = null;
                    }

                    string inpNumber = CommonUtils.GetGridCellValue(griRechlorineSimulation, "INP_NUMBER");
                    string simulationNo = CommonUtils.GetGridCellValue(griRechlorineSimulation, "SIMULATION_NO");
                    string simulationNm = CommonUtils.GetGridCellValue(griRechlorineSimulation, "SIMULATION_NM");

                    //지도에 관망도 표출
                    LoadQualityModel(inpNumber);

                    qualityAnalysisForm = new frmQualityAnalysis();
                    qualityAnalysisForm.Owner = this;
                    qualityAnalysisForm.SetSimulationDatas((griRechlorinePosition.DataSource as DataTable).Copy(), simulationNo, simulationNm, inpNumber);
                    qualityAnalysisForm.Show();
                }
                else
                {
                    MessageBox.Show("재염소 모의를 저장 후 실행하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //TAG선택 버튼 클릭 시 실행
        private void butTagSelect_Click(object sender, EventArgs e)
        {
            if(griRechlorinePosition.Selected.Rows.Count > 0)
            {
                frmTagSelect tagSelectForm = new frmTagSelect();
                tagSelectForm.Owner = this;
                tagSelectForm.ShowDialog();
            }
        }

        //감시지점으로 등록 버튼 클릭 시 실행
        private void butRegRtPoint_Click(object sender, EventArgs e)
        {
            try
            {
                if("".Equals(CommonUtils.GetGridCellValue(griRechlorinePosition, "TAGNAME")))
                {
                    CommonUtils.ShowInformationMessage("잔류염소TAG를 선택하십시오.");
                    return;
                }

                if(MessageBox.Show("감시지점으로 등록하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("MONPNT", CommonUtils.GetGridCellValue(griRechlorinePosition, "POINT_NM"));      //지점명
                conditions.Add("TAG_ID_CL", CommonUtils.GetGridCellValue(griRechlorinePosition, "TAGNAME"));    //염소TAG
                conditions.Add("NODE_ID", CommonUtils.GetGridCellValue(griRechlorinePosition, "ID"));           //절점 ID
                conditions.Add("MAP_X", CommonUtils.GetGridCellValue(griRechlorinePosition, "MAP_X"));          //절점 X좌표
                conditions.Add("MAP_Y", CommonUtils.GetGridCellValue(griRechlorinePosition, "MAP_Y"));          //절점 Y좌표

                work.InsertMonitorPoint(conditions);

                MessageBox.Show("정상적으로 처리되었습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        //모델선택 팝업창에서 선택된 모델데이터 할당
        public void SetModelData(string inpNumber, string title)
        {
            CommonUtils.SetGridCellValue(griRechlorineSimulation, "INP_NUMBER", inpNumber);
            CommonUtils.SetGridCellValue(griRechlorineSimulation, "TITLE", title);
            texTitle.Text = title;  
        }

        //TAG선택 팝업창에서 선택된 TAG데이터 할당
        public void SetTagData(string tagname)
        {
            try
            {
                CommonUtils.SetGridCellValue(griRechlorinePosition, "TAGNAME", tagname);
                texTagname.Text = tagname;

                //바로 저장시킴 (기존 저장버튼 로직 차용... 그래서 데이터셋이 통째로 날아감...ㅠ.ㅠ 만들기 귀찮음!!!)
                Hashtable saveConditions = new Hashtable();
                saveConditions.Add("positionData", griRechlorinePosition.DataSource as DataTable);

                work.SaveRechlorinePositionList(saveConditions);
                SelectRechlorinePositionList();
            }
            catch (Exception e)
            {
                CommonUtils.ShowExceptionMessage(e.Message);
            }
        }

        //로딩되어있는 모델 Shape를 unload한다.
        public void UnloadQualityModel()
        {
            frmQualityAnalysisMap map = this.Owner as frmQualityAnalysisMap;
            map.UnloadModelLayer();
        }

        //지도의 선택모드가 활성화되고 특정지점의 객체를 선택한 경우
        public void SetMapPointId(string id, string mapX, string mapY)
        {
            if (qualityAnalysisForm != null)
            {
                qualityAnalysisForm.SetObjectId(id, mapX, mapY);
            }
        }

        //재염소지점 설정 변경 후 재조회 (잔류염소지점의 내역이 바뀌었기 때문에 재조회를 해야함) 
        //조회 후 원래 선택되었던 모의를 선택
        public void ReSelectRechlorineSimulationList(string simulationNo)
        {
            SelectRechlorineSimulationList();

            foreach(UltraGridRow row in griRechlorineSimulation.Rows)
            {
                if(simulationNo.Equals(row.GetCellValue("SIMULATION_NO").ToString()))
                {
                    row.Selected = true;
                    row.Activate();
                    break;
                }
            }
        }

        #endregion

        #region 6.내부사용

        //재염소 모의 리스트 조회
        private void SelectRechlorineSimulationList()
        {
            Hashtable conditions = new Hashtable();

            if (!"".Equals(texCondSimulationNm.Text.Trim()))
            {
                conditions.Add("SIMULATION_NM", texCondSimulationNm.Text);
            }

            griRechlorineSimulation.DataSource = work.SelectRechlorineSimulationList(conditions);

            if(griRechlorineSimulation.Rows.Count > 0)
            {
                griRechlorineSimulation.Rows[0].Selected = true;
                griRechlorineSimulation.Rows[0].Activate();
            }

        }

        //재염소 지점 리스트 조회
        private void SelectRechlorinePositionList()
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("SIMULATION_NO", CommonUtils.GetGridCellValue(griRechlorineSimulation, "SIMULATION_NO"));
            conditions.Add("INP_NUMBER", CommonUtils.GetGridCellValue(griRechlorineSimulation, "INP_NUMBER"));

            griRechlorinePosition.DataSource = work.SelectRechlorinePositionList(conditions);

            if (griRechlorinePosition.Rows.Count > 0)
            {
                griRechlorinePosition.Rows[0].Selected = true;
                griRechlorinePosition.Rows[0].Activate();
            }
        }

        //부모창 지도에 관망도 표출
        private void LoadQualityModel(string inpNumber)
        {
            frmQualityAnalysisMap map = this.Owner as frmQualityAnalysisMap;
            map.UnloadModelLayer();
            map.loadModelLayer(inpNumber);
        }

        //Grid 초기화
        private void InitializeSetting()
        {
            //Simluation 그리드 초기화
            UltraGridColumn simulationColumn;

            simulationColumn = griRechlorineSimulation.DisplayLayout.Bands[0].Columns.Add();
            simulationColumn.Key = "SIMULATION_NO";
            simulationColumn.Header.Caption = "모의번호";
            simulationColumn.CellActivation = Activation.AllowEdit;
            simulationColumn.CellClickAction = CellClickAction.Edit;
            simulationColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            simulationColumn.CellAppearance.TextHAlign = HAlign.Center;
            simulationColumn.CellAppearance.TextVAlign = VAlign.Middle;
            simulationColumn.Width = 10;
            simulationColumn.Hidden = true;   //필드 보이기

            simulationColumn = griRechlorineSimulation.DisplayLayout.Bands[0].Columns.Add();
            simulationColumn.Key = "SIMULATION_NM";
            simulationColumn.Header.Caption = "명칭";
            simulationColumn.CellActivation = Activation.NoEdit;
            simulationColumn.CellClickAction = CellClickAction.RowSelect;
            simulationColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            simulationColumn.CellAppearance.TextHAlign = HAlign.Left;
            simulationColumn.CellAppearance.TextVAlign = VAlign.Middle;
            simulationColumn.Width = 200;
            simulationColumn.Hidden = false;   //필드 보이기

            simulationColumn = griRechlorineSimulation.DisplayLayout.Bands[0].Columns.Add();
            simulationColumn.Key = "REG_DAT";
            simulationColumn.Header.Caption = "등록일자";
            simulationColumn.CellActivation = Activation.NoEdit;
            simulationColumn.CellClickAction = CellClickAction.RowSelect;
            simulationColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            simulationColumn.CellAppearance.TextHAlign = HAlign.Center;
            simulationColumn.CellAppearance.TextVAlign = VAlign.Middle;
            simulationColumn.Width = 150;
            simulationColumn.Hidden = false;   //필드 보이기

            simulationColumn = griRechlorineSimulation.DisplayLayout.Bands[0].Columns.Add();
            simulationColumn.Key = "INP_NUMBER";
            simulationColumn.Header.Caption = "INP_NUMBER";
            simulationColumn.CellActivation = Activation.NoEdit;
            simulationColumn.CellClickAction = CellClickAction.RowSelect;
            simulationColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            simulationColumn.CellAppearance.TextHAlign = HAlign.Left;
            simulationColumn.CellAppearance.TextVAlign = VAlign.Middle;
            simulationColumn.Width = 200;
            simulationColumn.Hidden = true;   //필드 보이기

            simulationColumn = griRechlorineSimulation.DisplayLayout.Bands[0].Columns.Add();
            simulationColumn.Key = "TITLE";
            simulationColumn.Header.Caption = "관련모델";
            simulationColumn.CellActivation = Activation.NoEdit;
            simulationColumn.CellClickAction = CellClickAction.RowSelect;
            simulationColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            simulationColumn.CellAppearance.TextHAlign = HAlign.Left;
            simulationColumn.CellAppearance.TextVAlign = VAlign.Middle;
            simulationColumn.Width = 200;
            simulationColumn.Hidden = false;   //필드 보이기

            simulationColumn = griRechlorineSimulation.DisplayLayout.Bands[0].Columns.Add();
            simulationColumn.Key = "USER_NAME";
            simulationColumn.Header.Caption = "등록자";
            simulationColumn.CellActivation = Activation.NoEdit;
            simulationColumn.CellClickAction = CellClickAction.RowSelect;
            simulationColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            simulationColumn.CellAppearance.TextHAlign = HAlign.Center;
            simulationColumn.CellAppearance.TextVAlign = VAlign.Middle;
            simulationColumn.Width = 100;
            simulationColumn.Hidden = false;   //필드 보이기

            simulationColumn = griRechlorineSimulation.DisplayLayout.Bands[0].Columns.Add();
            simulationColumn.Key = "REMARK";
            simulationColumn.Header.Caption = "비고";
            simulationColumn.CellActivation = Activation.NoEdit;
            simulationColumn.CellClickAction = CellClickAction.RowSelect;
            simulationColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            simulationColumn.CellAppearance.TextHAlign = HAlign.Left;
            simulationColumn.CellAppearance.TextVAlign = VAlign.Middle;
            simulationColumn.Width = 200;
            simulationColumn.Hidden = false;   //필드 보이기

            //재염소지점 Grid 초기화
            UltraGridColumn positiontColumn;

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "SIMULATION_NO";
            positiontColumn.Header.Caption = "모의번호";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "INP_NUMBER";
            positiontColumn.Header.Caption = "관련모델번호";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "ID";
            positiontColumn.Header.Caption = "절점ID";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "POINT_NM";
            positiontColumn.Header.Caption = "지점명";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "";
            positiontColumn.Header.Caption = "이전투입량";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Right;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "VOLUME";
            positiontColumn.Header.Caption = "주입량";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Right;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "TAGNAME";
            positiontColumn.Header.Caption = "TAG";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Center;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 100;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "REMARK";
            positiontColumn.Header.Caption = "비고";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = false;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "MAP_X";
            positiontColumn.Header.Caption = "X좌표";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = true;   //필드 보이기

            positiontColumn = griRechlorinePosition.DisplayLayout.Bands[0].Columns.Add();
            positiontColumn.Key = "MAP_Y";
            positiontColumn.Header.Caption = "Y좌표";
            positiontColumn.CellActivation = Activation.NoEdit;
            positiontColumn.CellClickAction = CellClickAction.RowSelect;
            positiontColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            positiontColumn.CellAppearance.TextHAlign = HAlign.Left;
            positiontColumn.CellAppearance.TextVAlign = VAlign.Middle;
            positiontColumn.Width = 200;
            positiontColumn.Hidden = true;   //필드 보이기
        }

        #endregion
    }
}
