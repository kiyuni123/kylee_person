﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmQualityCheckpoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labTitle = new System.Windows.Forms.Label();
            this.texCli = new System.Windows.Forms.TextBox();
            this.texPhi = new System.Windows.Forms.TextBox();
            this.texTei = new System.Windows.Forms.TextBox();
            this.texCui = new System.Windows.Forms.TextBox();
            this.texTbi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 11);
            this.label1.TabIndex = 0;
            this.label1.Text = "잔류염소";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(39, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 11);
            this.label2.TabIndex = 1;
            this.label2.Text = "pH";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(37, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 11);
            this.label3.TabIndex = 2;
            this.label3.Text = "수온";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 11);
            this.label4.TabIndex = 3;
            this.label4.Text = "전기전도도";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(37, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 11);
            this.label5.TabIndex = 4;
            this.label5.Text = "탁도";
            // 
            // labTitle
            // 
            this.labTitle.AutoSize = true;
            this.labTitle.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labTitle.ForeColor = System.Drawing.Color.White;
            this.labTitle.Location = new System.Drawing.Point(2, 3);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(68, 12);
            this.labTitle.TabIndex = 5;
            this.labTitle.Text = "123456789";
            // 
            // texCli
            // 
            this.texCli.BackColor = System.Drawing.SystemColors.Window;
            this.texCli.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.texCli.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texCli.ForeColor = System.Drawing.Color.Black;
            this.texCli.Location = new System.Drawing.Point(68, 25);
            this.texCli.Name = "texCli";
            this.texCli.ReadOnly = true;
            this.texCli.Size = new System.Drawing.Size(52, 13);
            this.texCli.TabIndex = 7;
            this.texCli.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.texCli.WordWrap = false;
            // 
            // texPhi
            // 
            this.texPhi.BackColor = System.Drawing.SystemColors.Window;
            this.texPhi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.texPhi.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texPhi.ForeColor = System.Drawing.Color.Black;
            this.texPhi.Location = new System.Drawing.Point(68, 63);
            this.texPhi.Name = "texPhi";
            this.texPhi.ReadOnly = true;
            this.texPhi.Size = new System.Drawing.Size(52, 13);
            this.texPhi.TabIndex = 8;
            this.texPhi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.texPhi.WordWrap = false;
            // 
            // texTei
            // 
            this.texTei.BackColor = System.Drawing.SystemColors.Window;
            this.texTei.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.texTei.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texTei.ForeColor = System.Drawing.Color.Black;
            this.texTei.Location = new System.Drawing.Point(68, 82);
            this.texTei.Name = "texTei";
            this.texTei.ReadOnly = true;
            this.texTei.Size = new System.Drawing.Size(52, 13);
            this.texTei.TabIndex = 9;
            this.texTei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.texTei.WordWrap = false;
            // 
            // texCui
            // 
            this.texCui.BackColor = System.Drawing.SystemColors.Window;
            this.texCui.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.texCui.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texCui.ForeColor = System.Drawing.Color.Black;
            this.texCui.Location = new System.Drawing.Point(68, 101);
            this.texCui.Name = "texCui";
            this.texCui.ReadOnly = true;
            this.texCui.Size = new System.Drawing.Size(52, 13);
            this.texCui.TabIndex = 10;
            this.texCui.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.texCui.WordWrap = false;
            // 
            // texTbi
            // 
            this.texTbi.BackColor = System.Drawing.SystemColors.Window;
            this.texTbi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.texTbi.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.texTbi.ForeColor = System.Drawing.Color.Black;
            this.texTbi.Location = new System.Drawing.Point(68, 44);
            this.texTbi.Name = "texTbi";
            this.texTbi.ReadOnly = true;
            this.texTbi.Size = new System.Drawing.Size(52, 13);
            this.texTbi.TabIndex = 11;
            this.texTbi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.texTbi.WordWrap = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(120, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 11);
            this.label6.TabIndex = 12;
            this.label6.Text = "mg/ℓ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(120, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 11);
            this.label7.TabIndex = 13;
            this.label7.Text = "℃";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(120, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 11);
            this.label8.TabIndex = 14;
            this.label8.Text = "㎲/cm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(120, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 11);
            this.label9.TabIndex = 15;
            this.label9.Text = "NTU";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(122, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 11);
            this.label10.TabIndex = 16;
            this.label10.Text = "pH";
            // 
            // frmQualityCheckpoint
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Navy;
            this.ClientSize = new System.Drawing.Size(163, 123);
            this.ControlBox = false;
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.texTbi);
            this.Controls.Add(this.texCui);
            this.Controls.Add(this.texTei);
            this.Controls.Add(this.texPhi);
            this.Controls.Add(this.texCli);
            this.Controls.Add(this.labTitle);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmQualityCheckpoint";
            this.Text = "frmQualityCheckpoint";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.TextBox texCli;
        private System.Windows.Forms.TextBox texPhi;
        private System.Windows.Forms.TextBox texTei;
        private System.Windows.Forms.TextBox texCui;
        private System.Windows.Forms.TextBox texTbi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}