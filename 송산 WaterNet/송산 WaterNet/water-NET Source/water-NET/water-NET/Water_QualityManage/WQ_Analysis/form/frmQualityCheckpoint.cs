﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmQualityCheckpoint : Form
    {
        #region 1.전역번수

        private string mapX = "";      //x좌표
        private string mapY = "";      //y좌표

        #endregion

        #region 2.초기화

        public frmQualityCheckpoint()
        {
            InitializeComponent();
        }

        public frmQualityCheckpoint(string title)
        {
            InitializeComponent();
            labTitle.Text = title;
        }

        #endregion

        #region 3.객체 이벤트

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출
        
        public string GetMapX()
        {
            return this.mapX;
        }

        public string GetMapY()
        {
            return this.mapY;
        }

        //수질감시값 설정
        public void SetQualityDatas(string cli, string phi, string tei, string cui, string tbi)
        {
            texCli.Text = "".Equals(cli) ? cli : String.Format("{0:N2}", double.Parse(cli)); ;
            texPhi.Text = "".Equals(phi) ? phi : String.Format("{0:N1}", double.Parse(phi));
            texTei.Text = "".Equals(tei) ? tei : String.Format("{0:N1}", double.Parse(tei)); ;
            texCui.Text = "".Equals(cui) ? cui : String.Format("{0:N1}", double.Parse(cui)); ;
            texTbi.Text = "".Equals(tbi) ? tbi : String.Format("{0:N3}", double.Parse(tbi));

            //TextBox 컬러 설정
            SetTextboxColorSetting(texCli, cli);
            SetTextboxColorSetting(texPhi, phi);
            SetTextboxColorSetting(texTei, tei);
            SetTextboxColorSetting(texCui, cui);
            SetTextboxColorSetting(texTbi, tbi);
        }

        //X,Y 좌표값 설정
        public void SetMapXY(string mapX, string mapY)
        {
            this.mapX = mapX;
            this.mapY = mapY;
        }

        #endregion

        #region 6.내부사용

        //넘어오는 값에 따라 Textbox의 값을 변경한다.
        private void SetTextboxColorSetting(TextBox tb, string value)
        {
            if ("".Equals(value))
            {
                tb.BackColor = Color.HotPink;
            }
            else
            {
                tb.BackColor = Color.GreenYellow;
            }
        }

        #endregion
    }
}
