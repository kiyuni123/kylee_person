﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartFX.WinForms;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.form
{
    public partial class frmChartSetting : Form
    {
        #region 1.전역번수

        private Axis axis = null;

        #endregion

        #region 2.초기화

        public frmChartSetting()
        {
            InitializeComponent();
        }

        #endregion

        #region 3.객체 이벤트

        //폼 로드 시 실행
        private void frmChartSetting_Load(object sender, EventArgs e)
        {
            groChartSetting.Text = axis.Title.ToString();
        }

        //적용버튼 클릭 시
        private void butApply_Click(object sender, EventArgs e)
        {
            ApplySetting();
        }

        //Step textbox에서 엔터키 입력 시
        private void texStep_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ApplySetting();
            }
        }

        //Max textbox에서 엔터키 입력 시
        private void texMax_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ApplySetting();
            }
        }

        //Min textbox에서 엔터키 입력 시
        private void texMin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ApplySetting();
            }
        }

        //Dec textbox에서 엔터키 입력 시
        private void texDec_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ApplySetting();
            }
        }

        //폼 종료 시
        private void frmChartSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.Owner.GetType().ToString().IndexOf("frmRealtimeQualityChainChart") != -1)
            {
                (this.Owner as frmRealtimeQualityChainChart).chartSettingForm = null;
            }
            else if (this.Owner.GetType().ToString().IndexOf("frmRealtimeQualityChart") != -1)
            {
                (this.Owner as frmRealtimeQualityChart).chartSettingForm = null;
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        public void SetSettingDatas(double step, double max, double min, double dec)
        {
            texStep.Text = step.ToString();
            texMax.Text = max.ToString();
            texMin.Text = min.ToString();
            texDec.Text = dec.ToString();
        }

        public void SetAxisData(Axis axis)
        {
            texStep.Text = axis.Step.ToString();
            texMax.Text = axis.Max.ToString();
            texMin.Text = axis.Min.ToString();
            texDec.Text = axis.LabelsFormat.Decimals.ToString();

            this.axis = axis;
        }

        #endregion

        #region 6.내부사용

        //변경사항 차트 적용
        private void ApplySetting()
        {
            //validation
            if (!CommonUtils.IsNumber(texStep.Text))
            {
                MessageBox.Show("숫자만 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                texStep.Focus();
                return;
            }

            if (!CommonUtils.IsNumber(texMax.Text))
            {
                MessageBox.Show("숫자만 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                texMax.Focus();
                return;
            }

            if (!CommonUtils.IsNumber(texMin.Text))
            {
                MessageBox.Show("숫자만 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                texMin.Focus();
                return;
            }

            if (!CommonUtils.IsNumber(texDec.Text))
            {
                MessageBox.Show("숫자만 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                texDec.Focus();
                return;
            }

            this.axis.Step = double.Parse(texStep.Text);
            this.axis.Max = double.Parse(texMax.Text);
            this.axis.Min = double.Parse(texMin.Text);
            this.axis.LabelsFormat.Decimals = int.Parse(texDec.Text);
        }

        #endregion
    }
}
