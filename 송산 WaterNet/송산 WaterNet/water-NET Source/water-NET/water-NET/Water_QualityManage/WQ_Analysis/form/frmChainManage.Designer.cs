﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmChainManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.texChainMasterRemark = new System.Windows.Forms.TextBox();
            this.texChainMasterName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.butChainMasterAdd = new System.Windows.Forms.Button();
            this.butChainMasterDel = new System.Windows.Forms.Button();
            this.butChainMasterSave = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.griChainMasterList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butMoveLeft = new System.Windows.Forms.Button();
            this.butMoveUp = new System.Windows.Forms.Button();
            this.butChainDetailSave = new System.Windows.Forms.Button();
            this.butMoveDown = new System.Windows.Forms.Button();
            this.butMoveRight = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.griMonitorPointList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.griChainDetailList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griChainMasterList)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griMonitorPointList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griChainDetailList)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(584, 9);
            this.pictureBox2.TabIndex = 120;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 553);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(584, 9);
            this.pictureBox1.TabIndex = 121;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 544);
            this.picFrLeftM1.TabIndex = 122;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(574, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 544);
            this.pictureBox3.TabIndex = 123;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(0, 248);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(564, 10);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.texChainMasterRemark);
            this.groupBox1.Controls.Add(this.texChainMasterName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.butChainMasterAdd);
            this.groupBox1.Controls.Add(this.butChainMasterDel);
            this.groupBox1.Controls.Add(this.butChainMasterSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 168);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(564, 80);
            this.groupBox1.TabIndex = 159;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "계통 마스터";
            // 
            // texChainMasterRemark
            // 
            this.texChainMasterRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texChainMasterRemark.Location = new System.Drawing.Point(63, 49);
            this.texChainMasterRemark.Name = "texChainMasterRemark";
            this.texChainMasterRemark.Size = new System.Drawing.Size(311, 21);
            this.texChainMasterRemark.TabIndex = 10;
            this.texChainMasterRemark.TextChanged += new System.EventHandler(this.texChainMasterRemark_TextChanged);
            // 
            // texChainMasterName
            // 
            this.texChainMasterName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texChainMasterName.Location = new System.Drawing.Point(63, 22);
            this.texChainMasterName.Name = "texChainMasterName";
            this.texChainMasterName.Size = new System.Drawing.Size(311, 21);
            this.texChainMasterName.TabIndex = 9;
            this.texChainMasterName.TextChanged += new System.EventHandler(this.texChainMasterName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "비고 :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "계통명 :";
            // 
            // butChainMasterAdd
            // 
            this.butChainMasterAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butChainMasterAdd.Location = new System.Drawing.Point(423, 49);
            this.butChainMasterAdd.Name = "butChainMasterAdd";
            this.butChainMasterAdd.Size = new System.Drawing.Size(41, 23);
            this.butChainMasterAdd.TabIndex = 6;
            this.butChainMasterAdd.Text = "추가";
            this.butChainMasterAdd.UseVisualStyleBackColor = true;
            this.butChainMasterAdd.Click += new System.EventHandler(this.butChainMasterAdd_Click);
            // 
            // butChainMasterDel
            // 
            this.butChainMasterDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butChainMasterDel.Location = new System.Drawing.Point(470, 49);
            this.butChainMasterDel.Name = "butChainMasterDel";
            this.butChainMasterDel.Size = new System.Drawing.Size(41, 23);
            this.butChainMasterDel.TabIndex = 5;
            this.butChainMasterDel.Text = "삭제";
            this.butChainMasterDel.UseVisualStyleBackColor = true;
            this.butChainMasterDel.Click += new System.EventHandler(this.butChainMasterDel_Click);
            // 
            // butChainMasterSave
            // 
            this.butChainMasterSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butChainMasterSave.Location = new System.Drawing.Point(517, 49);
            this.butChainMasterSave.Name = "butChainMasterSave";
            this.butChainMasterSave.Size = new System.Drawing.Size(41, 23);
            this.butChainMasterSave.TabIndex = 4;
            this.butChainMasterSave.Text = "저장";
            this.butChainMasterSave.UseVisualStyleBackColor = true;
            this.butChainMasterSave.Click += new System.EventHandler(this.butChainMasterSave_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(0, 158);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(564, 10);
            this.pictureBox5.TabIndex = 166;
            this.pictureBox5.TabStop = false;
            // 
            // griChainMasterList
            // 
            this.griChainMasterList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griChainMasterList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griChainMasterList.DisplayLayout.MaxColScrollRegions = 1;
            this.griChainMasterList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griChainMasterList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griChainMasterList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griChainMasterList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BorderColor = System.Drawing.Color.DarkGray;
            this.griChainMasterList.DisplayLayout.Override.CellAppearance = appearance3;
            this.griChainMasterList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griChainMasterList.DisplayLayout.Override.CellPadding = 0;
            this.griChainMasterList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griChainMasterList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griChainMasterList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griChainMasterList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griChainMasterList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance4.BorderColor = System.Drawing.Color.DarkGray;
            this.griChainMasterList.DisplayLayout.Override.RowAppearance = appearance4;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griChainMasterList.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griChainMasterList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griChainMasterList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griChainMasterList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griChainMasterList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griChainMasterList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griChainMasterList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griChainMasterList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griChainMasterList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griChainMasterList.Location = new System.Drawing.Point(0, 0);
            this.griChainMasterList.Name = "griChainMasterList";
            this.griChainMasterList.Size = new System.Drawing.Size(564, 158);
            this.griChainMasterList.TabIndex = 167;
            this.griChainMasterList.Text = "ultraGrid1";
            this.griChainMasterList.BeforeSelectChange += new Infragistics.Win.UltraWinGrid.BeforeSelectChangeEventHandler(this.griChainMasterList_BeforeSelectChange);
            this.griChainMasterList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griChainMasterList_AfterSelectChange);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(10, 9);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.griChainMasterList);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox5);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(564, 544);
            this.splitContainer1.SplitterDistance = 258;
            this.splitContainer1.SplitterWidth = 9;
            this.splitContainer1.TabIndex = 124;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer2.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer2.Size = new System.Drawing.Size(564, 277);
            this.splitContainer2.SplitterDistance = 315;
            this.splitContainer2.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.butMoveLeft);
            this.panel2.Controls.Add(this.butMoveUp);
            this.panel2.Controls.Add(this.butChainDetailSave);
            this.panel2.Controls.Add(this.butMoveDown);
            this.panel2.Controls.Add(this.butMoveRight);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(265, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(50, 277);
            this.panel2.TabIndex = 7;
            // 
            // butMoveLeft
            // 
            this.butMoveLeft.Location = new System.Drawing.Point(6, 110);
            this.butMoveLeft.Name = "butMoveLeft";
            this.butMoveLeft.Size = new System.Drawing.Size(41, 23);
            this.butMoveLeft.TabIndex = 5;
            this.butMoveLeft.Text = "◀";
            this.butMoveLeft.UseVisualStyleBackColor = true;
            this.butMoveLeft.Click += new System.EventHandler(this.butMoveLeft_Click);
            // 
            // butMoveUp
            // 
            this.butMoveUp.Location = new System.Drawing.Point(6, 32);
            this.butMoveUp.Name = "butMoveUp";
            this.butMoveUp.Size = new System.Drawing.Size(41, 23);
            this.butMoveUp.TabIndex = 4;
            this.butMoveUp.Text = "▲";
            this.butMoveUp.UseVisualStyleBackColor = true;
            this.butMoveUp.Click += new System.EventHandler(this.butMoveUp_Click);
            // 
            // butChainDetailSave
            // 
            this.butChainDetailSave.Location = new System.Drawing.Point(6, 188);
            this.butChainDetailSave.Name = "butChainDetailSave";
            this.butChainDetailSave.Size = new System.Drawing.Size(41, 23);
            this.butChainDetailSave.TabIndex = 3;
            this.butChainDetailSave.Text = "저장";
            this.butChainDetailSave.UseVisualStyleBackColor = true;
            this.butChainDetailSave.Click += new System.EventHandler(this.butChainDetailSave_Click);
            // 
            // butMoveDown
            // 
            this.butMoveDown.Location = new System.Drawing.Point(6, 71);
            this.butMoveDown.Name = "butMoveDown";
            this.butMoveDown.Size = new System.Drawing.Size(41, 23);
            this.butMoveDown.TabIndex = 2;
            this.butMoveDown.Text = "▼";
            this.butMoveDown.UseVisualStyleBackColor = true;
            this.butMoveDown.Click += new System.EventHandler(this.butMoveDown_Click);
            // 
            // butMoveRight
            // 
            this.butMoveRight.Location = new System.Drawing.Point(6, 149);
            this.butMoveRight.Name = "butMoveRight";
            this.butMoveRight.Size = new System.Drawing.Size(41, 23);
            this.butMoveRight.TabIndex = 0;
            this.butMoveRight.Text = "▶";
            this.butMoveRight.UseVisualStyleBackColor = true;
            this.butMoveRight.Click += new System.EventHandler(this.butMoveRight_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.griMonitorPointList);
            this.groupBox3.Controls.Add(this.pictureBox13);
            this.groupBox3.Controls.Add(this.pictureBox10);
            this.groupBox3.Controls.Add(this.pictureBox11);
            this.groupBox3.Controls.Add(this.pictureBox12);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(245, 277);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "등록가능지점";
            // 
            // griMonitorPointList
            // 
            this.griMonitorPointList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMonitorPointList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griMonitorPointList.DisplayLayout.MaxColScrollRegions = 1;
            this.griMonitorPointList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griMonitorPointList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griMonitorPointList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMonitorPointList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BorderColor = System.Drawing.Color.DarkGray;
            this.griMonitorPointList.DisplayLayout.Override.CellAppearance = appearance8;
            this.griMonitorPointList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griMonitorPointList.DisplayLayout.Override.CellPadding = 0;
            this.griMonitorPointList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griMonitorPointList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griMonitorPointList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griMonitorPointList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griMonitorPointList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance7.BorderColor = System.Drawing.Color.DarkGray;
            this.griMonitorPointList.DisplayLayout.Override.RowAppearance = appearance7;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.griMonitorPointList.DisplayLayout.Override.RowSelectorAppearance = appearance6;
            this.griMonitorPointList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griMonitorPointList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griMonitorPointList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griMonitorPointList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griMonitorPointList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griMonitorPointList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griMonitorPointList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griMonitorPointList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griMonitorPointList.Location = new System.Drawing.Point(10, 24);
            this.griMonitorPointList.Name = "griMonitorPointList";
            this.griMonitorPointList.Size = new System.Drawing.Size(225, 243);
            this.griMonitorPointList.TabIndex = 156;
            this.griMonitorPointList.Text = "ultraGrid1";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox13.Location = new System.Drawing.Point(10, 17);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(225, 7);
            this.pictureBox13.TabIndex = 155;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox10.Location = new System.Drawing.Point(10, 267);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(225, 7);
            this.pictureBox10.TabIndex = 5;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox11.Location = new System.Drawing.Point(235, 17);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(7, 257);
            this.pictureBox11.TabIndex = 4;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox12.Location = new System.Drawing.Point(3, 17);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(7, 257);
            this.pictureBox12.TabIndex = 3;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox9.Location = new System.Drawing.Point(3, 17);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(7, 257);
            this.pictureBox9.TabIndex = 3;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox8.Location = new System.Drawing.Point(255, 17);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(7, 257);
            this.pictureBox8.TabIndex = 4;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox7.Location = new System.Drawing.Point(10, 267);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(245, 7);
            this.pictureBox7.TabIndex = 5;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(10, 17);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(245, 7);
            this.pictureBox6.TabIndex = 155;
            this.pictureBox6.TabStop = false;
            // 
            // griChainDetailList
            // 
            this.griChainDetailList.AllowDrop = true;
            this.griChainDetailList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griChainDetailList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griChainDetailList.DisplayLayout.MaxColScrollRegions = 1;
            this.griChainDetailList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griChainDetailList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griChainDetailList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griChainDetailList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BorderColor = System.Drawing.Color.DarkGray;
            this.griChainDetailList.DisplayLayout.Override.CellAppearance = appearance9;
            this.griChainDetailList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griChainDetailList.DisplayLayout.Override.CellPadding = 0;
            this.griChainDetailList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griChainDetailList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griChainDetailList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
            this.griChainDetailList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griChainDetailList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance5.BorderColor = System.Drawing.Color.DarkGray;
            this.griChainDetailList.DisplayLayout.Override.RowAppearance = appearance5;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.griChainDetailList.DisplayLayout.Override.RowSelectorAppearance = appearance2;
            this.griChainDetailList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griChainDetailList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griChainDetailList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.griChainDetailList.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.griChainDetailList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griChainDetailList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griChainDetailList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griChainDetailList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griChainDetailList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griChainDetailList.Location = new System.Drawing.Point(10, 24);
            this.griChainDetailList.Name = "griChainDetailList";
            this.griChainDetailList.Size = new System.Drawing.Size(245, 243);
            this.griChainDetailList.TabIndex = 156;
            this.griChainDetailList.Text = "ultraGrid1";
            this.griChainDetailList.DragDrop += new System.Windows.Forms.DragEventHandler(this.griChainDetailList_DragDrop);
            this.griChainDetailList.DragOver += new System.Windows.Forms.DragEventHandler(this.griChainDetailList_DragOver);
            this.griChainDetailList.SelectionDrag += new System.ComponentModel.CancelEventHandler(this.griChainDetailList_SelectionDrag);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.griChainDetailList);
            this.groupBox2.Controls.Add(this.pictureBox6);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.pictureBox9);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(265, 277);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "계통등록지점";
            // 
            // frmChainManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 562);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(600, 600);
            this.Name = "frmChainManage";
            this.Text = "수질계통관리";
            this.Load += new System.EventHandler(this.frmChainManage_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmChainManage_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmChainManage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griChainMasterList)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griMonitorPointList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griChainDetailList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox texChainMasterRemark;
        private System.Windows.Forms.TextBox texChainMasterName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butChainMasterAdd;
        private System.Windows.Forms.Button butChainMasterDel;
        private System.Windows.Forms.Button butChainMasterSave;
        private System.Windows.Forms.PictureBox pictureBox5;
        private Infragistics.Win.UltraWinGrid.UltraGrid griChainMasterList;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button butMoveLeft;
        private System.Windows.Forms.Button butMoveUp;
        private System.Windows.Forms.Button butChainDetailSave;
        private System.Windows.Forms.Button butMoveDown;
        private System.Windows.Forms.Button butMoveRight;
        private System.Windows.Forms.GroupBox groupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid griMonitorPointList;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.GroupBox groupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid griChainDetailList;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
    }
}