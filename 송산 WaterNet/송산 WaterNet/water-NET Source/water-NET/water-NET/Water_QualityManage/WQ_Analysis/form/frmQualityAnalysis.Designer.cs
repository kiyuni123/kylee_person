﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmQualityAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groPosition = new System.Windows.Forms.GroupBox();
            this.griRechlorinePosition = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.texPositionRemark = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.texPointNm = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.butSaveRechlorinePosition = new System.Windows.Forms.Button();
            this.butDeletePosition = new System.Windows.Forms.Button();
            this.butAddPosition = new System.Windows.Forms.Button();
            this.texId = new System.Windows.Forms.TextBox();
            this.texVolume = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cheAnnotation = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cheFlow = new System.Windows.Forms.CheckBox();
            this.cheQuality = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comFlowFormula = new System.Windows.Forms.ComboBox();
            this.texFlowBaseValue = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comQualityFormula = new System.Windows.Forms.ComboBox();
            this.texQualityBaseValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.butRunAnalysis = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groPosition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griRechlorinePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(605, 9);
            this.pictureBox2.TabIndex = 118;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 458);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(605, 9);
            this.pictureBox1.TabIndex = 119;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 449);
            this.picFrLeftM1.TabIndex = 121;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(595, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 449);
            this.pictureBox3.TabIndex = 122;
            this.pictureBox3.TabStop = false;
            // 
            // groPosition
            // 
            this.groPosition.Controls.Add(this.griRechlorinePosition);
            this.groPosition.Controls.Add(this.pictureBox13);
            this.groPosition.Controls.Add(this.panel1);
            this.groPosition.Controls.Add(this.pictureBox7);
            this.groPosition.Controls.Add(this.pictureBox6);
            this.groPosition.Controls.Add(this.pictureBox5);
            this.groPosition.Controls.Add(this.pictureBox4);
            this.groPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groPosition.Location = new System.Drawing.Point(10, 9);
            this.groPosition.Name = "groPosition";
            this.groPosition.Size = new System.Drawing.Size(585, 338);
            this.groPosition.TabIndex = 123;
            this.groPosition.TabStop = false;
            this.groPosition.Text = "재염소설정";
            // 
            // griRechlorinePosition
            // 
            this.griRechlorinePosition.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorinePosition.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorinePosition.DisplayLayout.MaxColScrollRegions = 1;
            this.griRechlorinePosition.DisplayLayout.MaxRowScrollRegions = 1;
            this.griRechlorinePosition.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorinePosition.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorinePosition.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griRechlorinePosition.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griRechlorinePosition.DisplayLayout.Override.CellPadding = 0;
            this.griRechlorinePosition.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griRechlorinePosition.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griRechlorinePosition.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griRechlorinePosition.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griRechlorinePosition.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griRechlorinePosition.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griRechlorinePosition.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griRechlorinePosition.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griRechlorinePosition.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griRechlorinePosition.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griRechlorinePosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griRechlorinePosition.Location = new System.Drawing.Point(13, 26);
            this.griRechlorinePosition.Name = "griRechlorinePosition";
            this.griRechlorinePosition.Size = new System.Drawing.Size(559, 191);
            this.griRechlorinePosition.TabIndex = 150;
            this.griRechlorinePosition.Text = "ultraGrid1";
            this.griRechlorinePosition.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.griRechlorinePosition_DoubleClickCell);
            this.griRechlorinePosition.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griRechlorinePosition_AfterSelectChange);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox13.Location = new System.Drawing.Point(13, 217);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(559, 9);
            this.pictureBox13.TabIndex = 160;
            this.pictureBox13.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.texPositionRemark);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.texPointNm);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.butSaveRechlorinePosition);
            this.panel1.Controls.Add(this.butDeletePosition);
            this.panel1.Controls.Add(this.butAddPosition);
            this.panel1.Controls.Add(this.texId);
            this.panel1.Controls.Add(this.texVolume);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(13, 226);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(559, 100);
            this.panel1.TabIndex = 159;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(253, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 176;
            this.label5.Text = "mg/ℓ";
            // 
            // texPositionRemark
            // 
            this.texPositionRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texPositionRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texPositionRemark.Location = new System.Drawing.Point(57, 72);
            this.texPositionRemark.Name = "texPositionRemark";
            this.texPositionRemark.Size = new System.Drawing.Size(489, 21);
            this.texPositionRemark.TabIndex = 175;
            this.texPositionRemark.TextChanged += new System.EventHandler(this.texPositionRemark_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 174;
            this.label9.Text = "비고";
            // 
            // texPointNm
            // 
            this.texPointNm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.texPointNm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texPointNm.Location = new System.Drawing.Point(57, 43);
            this.texPointNm.Name = "texPointNm";
            this.texPointNm.Size = new System.Drawing.Size(276, 21);
            this.texPointNm.TabIndex = 173;
            this.texPointNm.TextChanged += new System.EventHandler(this.texPointNm_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 172;
            this.label6.Text = "지점명";
            // 
            // butSaveRechlorinePosition
            // 
            this.butSaveRechlorinePosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSaveRechlorinePosition.Location = new System.Drawing.Point(445, 12);
            this.butSaveRechlorinePosition.Name = "butSaveRechlorinePosition";
            this.butSaveRechlorinePosition.Size = new System.Drawing.Size(101, 23);
            this.butSaveRechlorinePosition.TabIndex = 161;
            this.butSaveRechlorinePosition.Text = "재염소지점 저장";
            this.butSaveRechlorinePosition.UseVisualStyleBackColor = true;
            this.butSaveRechlorinePosition.Click += new System.EventHandler(this.butSaveRechlorinePosition_Click);
            // 
            // butDeletePosition
            // 
            this.butDeletePosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butDeletePosition.Location = new System.Drawing.Point(398, 12);
            this.butDeletePosition.Name = "butDeletePosition";
            this.butDeletePosition.Size = new System.Drawing.Size(45, 23);
            this.butDeletePosition.TabIndex = 160;
            this.butDeletePosition.Text = "삭제";
            this.butDeletePosition.UseVisualStyleBackColor = true;
            this.butDeletePosition.Click += new System.EventHandler(this.butDeletePosition_Click);
            // 
            // butAddPosition
            // 
            this.butAddPosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butAddPosition.Location = new System.Drawing.Point(351, 12);
            this.butAddPosition.Name = "butAddPosition";
            this.butAddPosition.Size = new System.Drawing.Size(45, 23);
            this.butAddPosition.TabIndex = 159;
            this.butAddPosition.Text = "추가";
            this.butAddPosition.UseVisualStyleBackColor = true;
            this.butAddPosition.Click += new System.EventHandler(this.butAddPosition_Click);
            // 
            // texId
            // 
            this.texId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texId.Location = new System.Drawing.Point(57, 15);
            this.texId.Name = "texId";
            this.texId.ReadOnly = true;
            this.texId.Size = new System.Drawing.Size(68, 21);
            this.texId.TabIndex = 156;
            this.texId.TextChanged += new System.EventHandler(this.texId_TextChanged);
            // 
            // texVolume
            // 
            this.texVolume.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texVolume.Location = new System.Drawing.Point(182, 15);
            this.texVolume.Name = "texVolume";
            this.texVolume.Size = new System.Drawing.Size(68, 21);
            this.texVolume.TabIndex = 158;
            this.texVolume.TextChanged += new System.EventHandler(this.texVolume_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 12);
            this.label2.TabIndex = 155;
            this.label2.Text = "절점ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 157;
            this.label1.Text = "주입량";
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox7.Location = new System.Drawing.Point(572, 26);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 300);
            this.pictureBox7.TabIndex = 123;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox6.Location = new System.Drawing.Point(3, 26);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 300);
            this.pictureBox6.TabIndex = 122;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(3, 326);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(579, 9);
            this.pictureBox5.TabIndex = 120;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(3, 17);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(579, 9);
            this.pictureBox4.TabIndex = 119;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox8.Location = new System.Drawing.Point(10, 347);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(585, 9);
            this.pictureBox8.TabIndex = 124;
            this.pictureBox8.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cheAnnotation);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cheFlow);
            this.groupBox2.Controls.Add(this.cheQuality);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.comFlowFormula);
            this.groupBox2.Controls.Add(this.texFlowBaseValue);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.comQualityFormula);
            this.groupBox2.Controls.Add(this.texQualityBaseValue);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.butRunAnalysis);
            this.groupBox2.Controls.Add(this.pictureBox9);
            this.groupBox2.Controls.Add(this.pictureBox10);
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.Controls.Add(this.pictureBox12);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(10, 356);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(585, 102);
            this.groupBox2.TabIndex = 125;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "수질해석";
            // 
            // cheAnnotation
            // 
            this.cheAnnotation.AutoSize = true;
            this.cheAnnotation.Checked = true;
            this.cheAnnotation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheAnnotation.Location = new System.Drawing.Point(486, 36);
            this.cheAnnotation.Name = "cheAnnotation";
            this.cheAnnotation.Size = new System.Drawing.Size(83, 16);
            this.cheAnnotation.TabIndex = 186;
            this.cheAnnotation.Text = "Label 표출";
            this.cheAnnotation.UseVisualStyleBackColor = true;
            this.cheAnnotation.CheckedChanged += new System.EventHandler(this.cheAnnotation_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(169, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 12);
            this.label8.TabIndex = 185;
            this.label8.Text = "㎥/h";
            // 
            // cheFlow
            // 
            this.cheFlow.AutoSize = true;
            this.cheFlow.Checked = true;
            this.cheFlow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheFlow.Location = new System.Drawing.Point(380, 64);
            this.cheFlow.Name = "cheFlow";
            this.cheFlow.Size = new System.Drawing.Size(96, 16);
            this.cheFlow.TabIndex = 184;
            this.cheFlow.Text = "해석결과표출";
            this.cheFlow.UseVisualStyleBackColor = true;
            this.cheFlow.CheckedChanged += new System.EventHandler(this.cheFlow_CheckedChanged);
            // 
            // cheQuality
            // 
            this.cheQuality.AutoSize = true;
            this.cheQuality.Checked = true;
            this.cheQuality.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheQuality.Location = new System.Drawing.Point(380, 36);
            this.cheQuality.Name = "cheQuality";
            this.cheQuality.Size = new System.Drawing.Size(96, 16);
            this.cheQuality.TabIndex = 183;
            this.cheQuality.Text = "해석결과표출";
            this.cheQuality.UseVisualStyleBackColor = true;
            this.cheQuality.CheckedChanged += new System.EventHandler(this.cheQuality_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(299, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 12);
            this.label11.TabIndex = 182;
            this.label11.Text = "항목만 표출";
            // 
            // comFlowFormula
            // 
            this.comFlowFormula.FormattingEnabled = true;
            this.comFlowFormula.Location = new System.Drawing.Point(212, 62);
            this.comFlowFormula.Name = "comFlowFormula";
            this.comFlowFormula.Size = new System.Drawing.Size(81, 20);
            this.comFlowFormula.TabIndex = 181;
            this.comFlowFormula.SelectedIndexChanged += new System.EventHandler(this.comFlowFormula_SelectedIndexChanged);
            // 
            // texFlowBaseValue
            // 
            this.texFlowBaseValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texFlowBaseValue.Location = new System.Drawing.Point(100, 61);
            this.texFlowBaseValue.Name = "texFlowBaseValue";
            this.texFlowBaseValue.Size = new System.Drawing.Size(68, 21);
            this.texFlowBaseValue.TabIndex = 180;
            this.texFlowBaseValue.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texFlowBaseValue_KeyUp);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(29, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 179;
            this.label12.Text = "유량이";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(169, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 177;
            this.label7.Text = "mg/ℓ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(299, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 12);
            this.label4.TabIndex = 176;
            this.label4.Text = "항목만 표출";
            // 
            // comQualityFormula
            // 
            this.comQualityFormula.FormattingEnabled = true;
            this.comQualityFormula.Location = new System.Drawing.Point(212, 34);
            this.comQualityFormula.Name = "comQualityFormula";
            this.comQualityFormula.Size = new System.Drawing.Size(81, 20);
            this.comQualityFormula.TabIndex = 175;
            this.comQualityFormula.SelectedIndexChanged += new System.EventHandler(this.comFormula_SelectedIndexChanged);
            // 
            // texQualityBaseValue
            // 
            this.texQualityBaseValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texQualityBaseValue.Location = new System.Drawing.Point(100, 33);
            this.texQualityBaseValue.Name = "texQualityBaseValue";
            this.texQualityBaseValue.Size = new System.Drawing.Size(68, 21);
            this.texQualityBaseValue.TabIndex = 174;
            this.texQualityBaseValue.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texBaseValue_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 173;
            this.label3.Text = "잔류염소가";
            // 
            // butRunAnalysis
            // 
            this.butRunAnalysis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butRunAnalysis.Location = new System.Drawing.Point(492, 58);
            this.butRunAnalysis.Name = "butRunAnalysis";
            this.butRunAnalysis.Size = new System.Drawing.Size(67, 23);
            this.butRunAnalysis.TabIndex = 162;
            this.butRunAnalysis.Text = "해석실행";
            this.butRunAnalysis.UseVisualStyleBackColor = true;
            this.butRunAnalysis.Click += new System.EventHandler(this.butRunAnalysis_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox9.Location = new System.Drawing.Point(572, 26);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 64);
            this.pictureBox9.TabIndex = 123;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox10.Location = new System.Drawing.Point(3, 26);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(10, 64);
            this.pictureBox10.TabIndex = 122;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox11.Location = new System.Drawing.Point(3, 90);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(579, 9);
            this.pictureBox11.TabIndex = 120;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox12.Location = new System.Drawing.Point(3, 17);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(579, 9);
            this.pictureBox12.TabIndex = 119;
            this.pictureBox12.TabStop = false;
            // 
            // frmQualityAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 467);
            this.Controls.Add(this.groPosition);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(621, 505);
            this.Name = "frmQualityAnalysis";
            this.Text = "수질해석";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmQualityAnalysis_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmQualityAnalysis_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groPosition.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griRechlorinePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groPosition;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid griRechlorinePosition;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.TextBox texVolume;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox texId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butSaveRechlorinePosition;
        private System.Windows.Forms.Button butDeletePosition;
        private System.Windows.Forms.Button butAddPosition;
        private System.Windows.Forms.Button butRunAnalysis;
        private System.Windows.Forms.TextBox texPositionRemark;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox texPointNm;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox texQualityBaseValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comQualityFormula;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cheQuality;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comFlowFormula;
        private System.Windows.Forms.TextBox texFlowBaseValue;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox cheFlow;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox cheAnnotation;
    }
}