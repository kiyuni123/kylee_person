﻿namespace WaterNet.WQ_Analysis.form
{
    partial class frmQualityAnalysisMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQualityAnalysisMap));
            this.butRealtimeData = new System.Windows.Forms.Button();
            this.butChlorineAnalysis = new System.Windows.Forms.Button();
            this.butPipeCleaning = new System.Windows.Forms.Button();
            this.butRealtimeState = new System.Windows.Forms.Button();
            this.butSelectQualityChain = new System.Windows.Forms.Button();
            this.butMonitorPointManage = new System.Windows.Forms.Button();
            this.butChainManage = new System.Windows.Forms.Button();
            this.panelCommand.SuspendLayout();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.butChainManage);
            this.panelCommand.Controls.Add(this.butMonitorPointManage);
            this.panelCommand.Controls.Add(this.butSelectQualityChain);
            this.panelCommand.Controls.Add(this.butRealtimeState);
            this.panelCommand.Controls.Add(this.butPipeCleaning);
            this.panelCommand.Controls.Add(this.butChlorineAnalysis);
            this.panelCommand.Controls.Add(this.butRealtimeData);
            // 
            // spcContents
            // 
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockableWindow1.TabIndex = 11;
            // 
            // spcTOC
            // 
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            // 
            // spcIndexMap
            // 
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(661, 524);
            this.axMap.OnExtentUpdated += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnExtentUpdatedEventHandler(this.axMap_OnExtentUpdated);
            this.axMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axMap_OnMouseDown);
            // 
            // butRealtimeData
            // 
            this.butRealtimeData.Location = new System.Drawing.Point(4, 61);
            this.butRealtimeData.Name = "butRealtimeData";
            this.butRealtimeData.Size = new System.Drawing.Size(61, 47);
            this.butRealtimeData.TabIndex = 12;
            this.butRealtimeData.Text = "실시간 데이터 조회";
            this.butRealtimeData.UseVisualStyleBackColor = true;
            this.butRealtimeData.Click += new System.EventHandler(this.butRealtimeData_Click);
            // 
            // butChlorineAnalysis
            // 
            this.butChlorineAnalysis.Location = new System.Drawing.Point(4, 168);
            this.butChlorineAnalysis.Name = "butChlorineAnalysis";
            this.butChlorineAnalysis.Size = new System.Drawing.Size(61, 47);
            this.butChlorineAnalysis.TabIndex = 13;
            this.butChlorineAnalysis.Text = "잔류염소모의";
            this.butChlorineAnalysis.UseVisualStyleBackColor = true;
            this.butChlorineAnalysis.Click += new System.EventHandler(this.butChlorineAnalysis_Click);
            // 
            // butPipeCleaning
            // 
            this.butPipeCleaning.Location = new System.Drawing.Point(4, 222);
            this.butPipeCleaning.Name = "butPipeCleaning";
            this.butPipeCleaning.Size = new System.Drawing.Size(61, 47);
            this.butPipeCleaning.TabIndex = 14;
            this.butPipeCleaning.Text = "관세척 관리";
            this.butPipeCleaning.UseVisualStyleBackColor = true;
            this.butPipeCleaning.Click += new System.EventHandler(this.butPipeCleaning_Click);
            // 
            // butRealtimeState
            // 
            this.butRealtimeState.Location = new System.Drawing.Point(4, 8);
            this.butRealtimeState.Name = "butRealtimeState";
            this.butRealtimeState.Size = new System.Drawing.Size(61, 47);
            this.butRealtimeState.TabIndex = 15;
            this.butRealtimeState.Text = "실시간 수질현황";
            this.butRealtimeState.UseVisualStyleBackColor = true;
            this.butRealtimeState.Click += new System.EventHandler(this.butRealtimeState_Click);
            // 
            // butSelectQualityChain
            // 
            this.butSelectQualityChain.Location = new System.Drawing.Point(4, 114);
            this.butSelectQualityChain.Name = "butSelectQualityChain";
            this.butSelectQualityChain.Size = new System.Drawing.Size(61, 47);
            this.butSelectQualityChain.TabIndex = 16;
            this.butSelectQualityChain.Text = "계통별 시점조회";
            this.butSelectQualityChain.UseVisualStyleBackColor = true;
            this.butSelectQualityChain.Click += new System.EventHandler(this.butSelectQualityChain_Click);
            // 
            // butMonitorPointManage
            // 
            this.butMonitorPointManage.Location = new System.Drawing.Point(4, 275);
            this.butMonitorPointManage.Name = "butMonitorPointManage";
            this.butMonitorPointManage.Size = new System.Drawing.Size(61, 47);
            this.butMonitorPointManage.TabIndex = 18;
            this.butMonitorPointManage.Text = "수질감시 지점관리";
            this.butMonitorPointManage.UseVisualStyleBackColor = true;
            this.butMonitorPointManage.Click += new System.EventHandler(this.butMonitorPointManage_Click);
            // 
            // butChainManage
            // 
            this.butChainManage.Location = new System.Drawing.Point(4, 328);
            this.butChainManage.Name = "butChainManage";
            this.butChainManage.Size = new System.Drawing.Size(61, 47);
            this.butChainManage.TabIndex = 19;
            this.butChainManage.Text = "수질계통관리";
            this.butChainManage.UseVisualStyleBackColor = true;
            this.butChainManage.Click += new System.EventHandler(this.butChainManage_Click);
            // 
            // frmQualityAnalysisMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 583);
            this.Name = "frmQualityAnalysisMap";
            this.Text = "frmQualityAnalysisMap";
            this.Load += new System.EventHandler(this.frmQualityAnalysisMap_Load);
            this.SizeChanged += new System.EventHandler(this.frmQualityAnalysisMap_SizeChanged);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            this.panelCommand.ResumeLayout(false);
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button butRealtimeData;
        private System.Windows.Forms.Button butChlorineAnalysis;
        private System.Windows.Forms.Button butPipeCleaning;
        private System.Windows.Forms.Button butRealtimeState;
        private System.Windows.Forms.Button butSelectQualityChain;
        private System.Windows.Forms.Button butChainManage;
        private System.Windows.Forms.Button butMonitorPointManage;



    }
}