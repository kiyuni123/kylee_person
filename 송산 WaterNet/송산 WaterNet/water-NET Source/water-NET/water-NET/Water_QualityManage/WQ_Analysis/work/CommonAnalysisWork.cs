﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using WaterNet.WH_PipingNetworkAnalysis.epanet;
using WaterNet.WaterNetCore;
using WaterNet.WQ_Analysis.dao;

namespace WaterNet.WQ_Analysis.work
{
    public class CommonAnalysisWork
    {
        private AnalysisEngine analysisEngine = null;
        private static CommonAnalysisWork work = null;

        //해석결과 필터링을 위한 전역변수
        private ArrayList junctionResult = null;
        private ArrayList reservoirResult = null;
        private ArrayList tankResult = null;

        private ArrayList pipeResult = null;
        private ArrayList valveResult = null;
        private ArrayList pumpResult = null;

        private CommonAnalysisWork()
        {
            analysisEngine = new AnalysisEngine();
            dao = CommonAnalysisDao.GetInstance();
        }

        private CommonAnalysisDao dao = null;

        public static CommonAnalysisWork GetInstance()
        {
            if(work == null)
            {
                work = new CommonAnalysisWork();
            }

            return work;
        }

        #region 외부인터페이스

        //수질해석 실행
        public Hashtable ExecuteQualityAnalysis(Hashtable conditions)
        {
            Hashtable result = new Hashtable();

            try
            {
                Hashtable resetValues = new Hashtable();

                //재염소지점 resetValue
                if (conditions["initQuality"] != null) resetValues.Add("quality", GetInitQualityResetValues(conditions["initQuality"] as DataTable));

                //필요할 경우 이부분에 추가

                Hashtable analysisConditions = new Hashtable();

                analysisConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                analysisConditions.Add("AUTO_MANUAL", "M");
                analysisConditions.Add("analysisType", 0);
                analysisConditions.Add("analysisFlag", "Q");
                analysisConditions.Add("saveReport", false); //DB에 저장
                analysisConditions.Add("resetValues", resetValues);

                Hashtable analysisResult = analysisEngine.Execute(analysisConditions);
                ArrayList sorter = new ArrayList(analysisResult.Keys);

                if (sorter.Count != 0)
                {
                    sorter.Sort();
                    string key = sorter[sorter.Count - 1].ToString();

                    junctionResult = (ArrayList)((Hashtable)((Hashtable)analysisResult[key])["node"])["junction"];
                    tankResult = (ArrayList)((Hashtable)((Hashtable)analysisResult[key])["node"])["tank"];
                    reservoirResult = (ArrayList)((Hashtable)((Hashtable)analysisResult[key])["node"])["reservior"];

                    pipeResult = (ArrayList)((Hashtable)((Hashtable)analysisResult[key])["link"])["pipe"];
                    valveResult = (ArrayList)((Hashtable)((Hashtable)analysisResult[key])["link"])["valve"];
                    pumpResult = (ArrayList)((Hashtable)((Hashtable)analysisResult[key])["link"])["pump"];
                }

                //해석결과 중 quality가 필요할 경우
                if (conditions["qualityBaseValue"] != null && conditions["qualityFormula"] != null)
                result.Add("quality", GetFilteredQualityDatas(conditions["qualityBaseValue"].ToString(), conditions["qualityFormula"].ToString(), "quality"));

                //해석결과 중 flow가 필요할 경우
                if (conditions["flowBaseValue"] != null && conditions["flowFormula"] != null)
                result.Add("flow", GetFilteredQualityDatas(conditions["flowBaseValue"].ToString(), conditions["flowFormula"].ToString(), "flow"));

                //해석결과 중 velocity가 필요할 경우
                if (conditions["velocityBaseValue"] != null && conditions["velocityFormula"] != null)
                result.Add("velocity", GetFilteredQualityDatas(conditions["velocityBaseValue"].ToString(), conditions["velocityFormula"].ToString(), "velocity"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }

            return result;
        }

        //필터링된 해석결과 데이터를 반환 (해석이 끝났을때 및 필터링 조건이 바뀌었을때 반응)
        public Hashtable GetFilteredQualityDatas(string baseValue, string formula, string type)
        {
            Hashtable analysisResult = new Hashtable();
            DataTable correctTable = new DataTable();
            DataTable incorrectTable = new DataTable();

            if ("quality".Equals(type))          //수질값 필터링
            {
                if (junctionResult == null && reservoirResult == null && tankResult == null)
                {
                    return analysisResult;
                }

                Hashtable fieldSetting = new Hashtable();           //필터링용 필드정보 세팅
                fieldSetting.Add("originIdField", "NODE_ID");
                fieldSetting.Add("originValueField", "EN_QUALITY");
                fieldSetting.Add("filtedIdField", "NODE_ID");
                fieldSetting.Add("filtedValueField", "QUALITY");
                fieldSetting.Add("valueFormat", "{0:N2}");

                correctTable.TableName = "WH_RPT_NODES";
                correctTable.Columns.Add("NODE_ID", typeof(string));
                correctTable.Columns.Add("QUALITY", typeof(string));

                incorrectTable.TableName = "WH_RPT_NODES";
                incorrectTable.Columns.Add("NODE_ID", typeof(string));
                incorrectTable.Columns.Add("QUALITY", typeof(string));

                FilteringQualityAnalysisResult(junctionResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
                FilteringQualityAnalysisResult(tankResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
                FilteringQualityAnalysisResult(reservoirResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
            }
            else if ("flow".Equals(type))        //유량값 필터링
            {
                if (pipeResult == null && valveResult == null && pumpResult == null)
                {
                    return analysisResult;
                }

                Hashtable fieldSetting = new Hashtable();           //필터링용 필드정보 세팅
                fieldSetting.Add("originIdField", "LINK_ID");
                fieldSetting.Add("originValueField", "EN_FLOW");
                fieldSetting.Add("filtedIdField", "LINK_ID");
                fieldSetting.Add("filtedValueField", "FLOW");
                fieldSetting.Add("valueFormat", "{0:N2}");

                correctTable.TableName = "WH_RPT_LINKS";
                correctTable.Columns.Add("LINK_ID", typeof(string));
                correctTable.Columns.Add("FLOW", typeof(string));

                incorrectTable.TableName = "WH_RPT_LINKS";
                incorrectTable.Columns.Add("LINK_ID", typeof(string));
                incorrectTable.Columns.Add("FLOW", typeof(string));

                FilteringQualityAnalysisResult(pipeResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
                FilteringQualityAnalysisResult(valveResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
                FilteringQualityAnalysisResult(pumpResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
            }
            else if ("velocity".Equals(type))        //유속값 필터링
            {
                if (pipeResult == null && valveResult == null && pumpResult == null)
                {
                    return analysisResult;
                }

                Hashtable fieldSetting = new Hashtable();           //필터링용 필드정보 세팅
                fieldSetting.Add("originIdField", "LINK_ID");
                fieldSetting.Add("originValueField", "EN_VELOCITY");
                fieldSetting.Add("filtedIdField", "LINK_ID");
                fieldSetting.Add("filtedValueField", "VELOCITY");
                fieldSetting.Add("valueFormat", "{0:N2}");

                correctTable.TableName = "WH_RPT_LINKS";
                correctTable.Columns.Add("LINK_ID", typeof(string));
                correctTable.Columns.Add("VELOCITY", typeof(string));

                incorrectTable.TableName = "WH_RPT_LINKS";
                incorrectTable.Columns.Add("LINK_ID", typeof(string));
                incorrectTable.Columns.Add("VELOCITY", typeof(string));

                FilteringQualityAnalysisResult(pipeResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
                FilteringQualityAnalysisResult(valveResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
                FilteringQualityAnalysisResult(pumpResult, correctTable, incorrectTable, baseValue, formula, fieldSetting);
            }



            analysisResult.Add("correct", correctTable);
            analysisResult.Add("incorrect", incorrectTable);

            return analysisResult;
        }

        //해석결과 초기화 (work는 싱글톤으로 동작하기 때문에 초기화시켜줘야함 - 해석관련창이 닫길때 호출됨)
        public void InitAnalysisResult()
        {
            if (junctionResult != null) junctionResult.Clear();
            if (reservoirResult != null) reservoirResult.Clear();
            if (tankResult != null) tankResult.Clear();
            if (pipeResult != null) pipeResult.Clear();
            if (valveResult != null) valveResult.Clear();
            if (pumpResult != null) pumpResult.Clear();
        }

        //로딩된 모델이 해석실행이 됐는지 여부
        public bool IsAnalysisModel()
        {
            if (junctionResult == null && reservoirResult == null && tankResult == null && pipeResult == null && valveResult == null && pumpResult == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region 내부사용

        //해석결과 내부 필터링 (correct, incorrect 구분해서 담아줌 - * correct/incorrect는 call by reference임)
        //해석결과항목을 구분하여 flow일 경우 해석결과를 절대치로 바꿔줌 (유량이 음수가 흐르는 경우는 파이프의 그리는 순서가 바뀌었기때문이라 무시해야함)
        private void FilteringQualityAnalysisResult(ArrayList targetData, DataTable correct, DataTable incorrect, string baseValue, string formula, Hashtable fieldSetting)
        {
            if (targetData == null)
            {
                return;
            }

            string originIdField = fieldSetting["originIdField"].ToString();                    //해석결과의 ID 필드    
            string originValueField = fieldSetting["originValueField"].ToString();              //해석결과의 값 필드
            string filtedIdField = fieldSetting["filtedIdField"].ToString();                    //필터링 후 ID 필드
            string filtedValueField = fieldSetting["filtedValueField"].ToString();              //필터링 후 값 필드      
            string valueFormat = fieldSetting["valueFormat"].ToString();                        //소수점 표출 format

            foreach (Hashtable data in targetData)
            {
                double originValue = double.Parse(String.Format(valueFormat, double.Parse(data[originValueField].ToString())));

                if ("FLOW".Equals(filtedValueField)) originValue = Math.Abs(originValue);       //유량인 경우 절대치로 만듦


                //수질 결과가 0일 경우 incorrect로 무조건 옮긴다.
                //if ("QUALITY".Equals(filtedValueField))
                //{
                //    if (originValue == 0)
                //    {
                //        DataRow row = incorrect.NewRow();
                //        row[filtedIdField] = data[originIdField].ToString();
                //        row[filtedValueField] = originValue;

                //        incorrect.Rows.Add(row);
                //        continue;
                //    }
                //}

                //수질 결과가 0.1이하일 경우 incorrect로 무조건 옮긴다. - 임시로직 (2014.11.04 khb)
                if ("QUALITY".Equals(filtedValueField))
                {
                    if (originValue <= 0.1)
                    {
                        DataRow row = incorrect.NewRow();
                        row[filtedIdField] = data[originIdField].ToString();
                        row[filtedValueField] = originValue;

                        incorrect.Rows.Add(row);
                        continue;
                    }
                }

                if ("".Equals(baseValue) || "".Equals(formula))         //기준값이나 식이 없는경우 전체가 부합하지않는 데이터로 처리
                {
                    DataRow row = incorrect.NewRow();
                    row[filtedIdField] = data[originIdField].ToString();
                    row[filtedValueField] = originValue;

                    incorrect.Rows.Add(row);
                    continue;
                }

                double conBaseValue = double.Parse(baseValue);          //기준값과 식이 입력이 된 경우 연산 시작

                if (">=".Equals(formula) && conBaseValue >= originValue)
                {
                    DataRow row = correct.NewRow();
                    row[filtedIdField] = data[originIdField].ToString();
                    row[filtedValueField] = originValue;

                    correct.Rows.Add(row);
                }
                else if (">".Equals(formula) && conBaseValue > originValue)
                {
                    DataRow row = correct.NewRow();
                    row[filtedIdField] = data[originIdField].ToString();
                    row[filtedValueField] = originValue;

                    correct.Rows.Add(row);
                }
                else if ("<=".Equals(formula) && conBaseValue <= originValue)
                {
                    DataRow row = correct.NewRow();
                    row[filtedIdField] = data[originIdField].ToString();
                    row[filtedValueField] = originValue;

                    correct.Rows.Add(row);
                }
                else if ("<".Equals(formula) && conBaseValue < originValue)
                {
                    DataRow row = correct.NewRow();
                    row[filtedIdField] = data[originIdField].ToString();
                    row[filtedValueField] = originValue;

                    correct.Rows.Add(row);
                }
                else if ("=".Equals(formula) && conBaseValue == originValue)
                {
                    DataRow row = correct.NewRow();
                    row[filtedIdField] = data[originIdField].ToString();
                    row[filtedValueField] = originValue;

                    correct.Rows.Add(row);
                }
                else
                {
                    DataRow row = incorrect.NewRow();
                    row[filtedIdField] = data[originIdField].ToString();
                    row[filtedValueField] = originValue;

                    incorrect.Rows.Add(row);
                }
            }
        }

        //수질해석 시 재염소지점을 할당
        private Hashtable GetInitQualityResetValues(DataTable qualityData)
        {
            Hashtable initQualitys = new Hashtable();

            if (qualityData.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                foreach (DataRow row in qualityData.Rows)
                {
                    initQualitys.Add(row["ID"].ToString(), row["VOLUME"].ToString());
                }
            }

            return initQualitys;
        }

        #endregion
    }
}
