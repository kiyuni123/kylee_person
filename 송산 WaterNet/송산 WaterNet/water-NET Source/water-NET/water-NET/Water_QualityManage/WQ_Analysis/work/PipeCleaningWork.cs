﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using WaterNet.WQ_Analysis.dao;
using System.Data;
using WaterNet.WaterNetCore;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.work
{
    public class PipeCleaningWork
    {
        private static PipeCleaningWork work = null;
        private PipeCleaningDao dao = null;
        private CommonAnalysisDao cDao = null; 

        private PipeCleaningWork()
        {
            dao = PipeCleaningDao.GetInstance();
            cDao = CommonAnalysisDao.GetInstance();
        }

        #region 외부인터페이스

        public static PipeCleaningWork GetInstance()
        {
            if(work == null)
            {
                work = new PipeCleaningWork();
            }

            return work;
        }

        //민원Tab 초기화 (검색조건 등 조회)
        public Hashtable InitComplaintTab(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            Hashtable result = new Hashtable();

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                DataTable complaintType = dao.SelectComplaintType(dbManager, conditions);
                DataRow newRow = complaintType.NewRow();
                newRow["CODE"] = "";
                newRow["CODE_NAME"] = "전체";
                complaintType.Rows.InsertAt(newRow, 0);

                result.Add("complaintType", complaintType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }

            return result;
        }
    
        //관로세척정보 조회
        public DataTable SelectPipeCleaningDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                return dao.SelectPipeCleaningDataList(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }
    
        //관로세척정보 저장
        public void SavePipeCleaningDataList(Hashtable conditions)
        {
            DataTable saveData = conditions["pipeCleaningData"] as DataTable;
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in saveData.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        Hashtable insertConditions = new Hashtable();
                        insertConditions.Add("PCLEAN_NO", CommonUtils.GetSerialNumber("PC"));
                        insertConditions.Add("PCLEAN_TITLE", row["PCLEAN_TITLE"].ToString());
                        insertConditions.Add("PROC_METHOD_TITLE", row["PROC_METHOD_TITLE"].ToString());
                        insertConditions.Add("PROC_METHOD", row["PROC_METHOD"].ToString());
                        insertConditions.Add("PROC_RESULT_TITLE", row["PROC_RESULT_TITLE"].ToString());
                        insertConditions.Add("PROC_RESULT", row["PROC_RESULT"].ToString());
                        insertConditions.Add("PROC_DAT", row["PROC_DAT"].ToString());
                        insertConditions.Add("REMARK", row["REMARK"].ToString());

                        dao.InsertPipeCleaningData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        Hashtable updateConditions = new Hashtable();
                        updateConditions.Add("PCLEAN_TITLE", row["PCLEAN_TITLE"].ToString());
                        updateConditions.Add("PROC_METHOD_TITLE", row["PROC_METHOD_TITLE"].ToString());
                        updateConditions.Add("PROC_METHOD", row["PROC_METHOD"].ToString());
                        updateConditions.Add("PROC_RESULT_TITLE", row["PROC_RESULT_TITLE"].ToString());
                        updateConditions.Add("PROC_RESULT", row["PROC_RESULT"].ToString());
                        updateConditions.Add("PROC_DAT", row["PROC_DAT"].ToString());
                        updateConditions.Add("REMARK", row["REMARK"].ToString());
                        updateConditions.Add("PCLEAN_NO", row["PCLEAN_NO"].ToString());

                        dao.UpdatePipeCleaningData(dbManager, updateConditions);
                    }
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //관로세척정보 삭제
        public void DeletePipeCleaningDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeletePipeCleaningData(dbManager, conditions);
                dao.DeleteAllPipeData(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //세척대상 파이프 정보 조회
        public DataTable SelectPipeList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                return dao.SelectPipeList(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //세척대상 파이프 정보 저장
        public void SavePipeList(Hashtable conditions)
        {
            DataTable saveData = conditions["pipeData"] as DataTable;
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in saveData.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        Hashtable checkConditions = new Hashtable();
                        checkConditions.Add("PCLEAN_NO", row["PCLEAN_NO"].ToString());
                        checkConditions.Add("FTR_IDN", row["FTR_IDN"].ToString());
                        if (dao.IsExistPipeInCleaningData(dbManager, checkConditions).Rows.Count > 0)
                        {
                            throw new Exception("이미 관로정보가 존재합니다. 관로번호 : " + row["FTR_IDN"]);
                        }

                        Hashtable insertConditions = new Hashtable();
                        insertConditions.Add("PCLEAN_NO", row["PCLEAN_NO"].ToString());
                        insertConditions.Add("FTR_IDN", row["FTR_IDN"].ToString());

                        dao.InsertPipeList(dbManager, insertConditions);
                    }
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //세척대상 파이프 정보 삭제
        public void DeletePipeData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeletePipeData(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }
        
        //전체 세척 파이프정보 조회
        public DataTable SelectAllCleaningPipeList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                return dao.SelectAllCleaningPipeList(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //모델 정보 조회
        public DataTable SelectModelList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                return dao.SelectModelList(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //블록별 민원 리스트 조회 (Tree)
        public DataTable SelectComplaintCountByBlock(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                return dao.SelectComplaintCountByBlock(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //해당 블록의 하위블록 리스트 조회
        public Hashtable SelectComplaintData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            Hashtable result = new Hashtable();

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                //하위 블록코드를 조회 후 검색조건에 추가
                DataTable lowerBlockList =  dao.SelectLowerBlockList(dbManager, conditions);
                if (lowerBlockList.Rows.Count > 0) conditions.Add("lowerBlocks", lowerBlockList);

                result.Add("complaintList", dao.SelectComplaintList(dbManager, conditions));
                result.Add("complaintState", dao.SelectComplaintState(dbManager, conditions));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        #endregion

        #region 내부사용

        #endregion
    }
}
