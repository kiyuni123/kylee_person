﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WaterNetCore;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WaterAOCore;

namespace WaterNet.WQ_Analysis.work
{
    public class AnalysisMapWork
    {
        private static AnalysisMapWork work = null;

        private AnalysisMapWork()
        {
        }

        public static AnalysisMapWork GetInstance()
        {
            if(work == null)
            {
                work = new AnalysisMapWork();
            }

            return work;
        }

        //해당 INP번호의 모델정보를 조회
        public void GenerateModelShape(string inpNumber)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                IWorkspace pWorkspace = ArcManager.getShapeWorkspace(VariableManager.m_INPgraphicRootDirectory + "\\" + inpNumber);

                if (pWorkspace == null)
                {
                    CreateINPLayerManager manager = new CreateINPLayerManager(dbManager, inpNumber);
                    manager.CreateINP_Shape();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }
    }
}
