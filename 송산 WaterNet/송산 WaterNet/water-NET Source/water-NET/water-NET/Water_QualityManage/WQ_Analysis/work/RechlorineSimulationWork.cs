﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using WaterNet.WQ_Analysis.dao;
using WaterNet.WaterNetCore;
using WaterNet.WQ_Analysis.util;
using WaterNet.WH_PipingNetworkAnalysis.epanet;

namespace WaterNet.WQ_Analysis.work
{
    public class RechlorineSimulationWork
    {
        private static RechlorineSimulationWork work = null;
        private RechlorineSimulationDao dao = null;
        private AnalysisEngine analysisEngine = null;

        private RechlorineSimulationWork()
        {
            dao = RechlorineSimulationDao.GetInstance();
            analysisEngine = new AnalysisEngine();
        }

        public static RechlorineSimulationWork GetInstance()
        {
            if (work == null)
            {
                work = new RechlorineSimulationWork();
            }

            return work;
        }

        #region 외부인터페이스

        //재염소 모의 리스트 조회
        public DataTable SelectRechlorineSimulationList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectRechlorineSimulationList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //재염소 모의 리스트 저장
        public void SaveRechlorineSimulationList(Hashtable conditions)
        {
            DataTable saveData = conditions["simulationData"] as DataTable;
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in saveData.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Hashtable checkConditions = new Hashtable();
                        //checkConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());

                        //if (dao.IsExistQualityModel(dbManager, checkConditions).Rows.Count != 0) throw new Exception("이미 모델이 등록되어있습니다. [" + row["TITLE"] + "]");

                        string simulationNo = CommonUtils.GetSerialNumber("QS");        //지점정보에도 입력하기 위해 일련번호를 생성 후 변수에 할당

                        Hashtable insertConditions = new Hashtable();
                        insertConditions.Add("SIMULATION_NO", simulationNo);
                        insertConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                        insertConditions.Add("REMARK", row["REMARK"].ToString());
                        insertConditions.Add("SIMULATION_NM", row["SIMULATION_NM"].ToString());

                        dao.InsertRechlorineSimulationData(dbManager, insertConditions);

                        //모델에 기본적으로 Quality항목이 존재하는 경우 자동으로 지점정보에 입력해준다.
                        Hashtable initQualConditions = new Hashtable();
                        initQualConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());

                        DataTable initQualList = dao.SelectInitialQuality(dbManager, initQualConditions);
                        
                        if(initQualList.Rows.Count != 0)
                        {
                            foreach(DataRow qualityRow in initQualList.Rows)
                            {
                                Hashtable insertDefaultQualConditions = new Hashtable();
                                insertDefaultQualConditions.Add("SIMULATION_NO", simulationNo);
                                insertDefaultQualConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                                insertDefaultQualConditions.Add("ID", qualityRow["ID"].ToString());
                                insertDefaultQualConditions.Add("VOLUME", qualityRow["INITQUAL"].ToString());
                                insertDefaultQualConditions.Add("MAP_X", qualityRow["X"].ToString());
                                insertDefaultQualConditions.Add("MAP_Y", qualityRow["Y"].ToString());
                                insertDefaultQualConditions.Add("REMARK", null);
                                insertDefaultQualConditions.Add("TAGNAME", null);
                                insertDefaultQualConditions.Add("POINT_NM", null);

                                dao.InsertRechlorinePositionData(dbManager, insertDefaultQualConditions);
                            }
                        }

                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        Hashtable updateConditions = new Hashtable();
                        updateConditions.Add("REMARK", row["REMARK"].ToString());
                        updateConditions.Add("SIMULATION_NM", row["SIMULATION_NM"].ToString());
                        updateConditions.Add("SIMULATION_NO", row["SIMULATION_NO"].ToString());
                        updateConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());

                        dao.UpdateRechlorineSimulationData(dbManager, updateConditions);
                    }
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally 
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //재염소 모의 데이터 삭제
        public void DeleteRechlorineSimulationData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeleteRechlorineSimulationData(dbManager, conditions);
                dao.DeleteAllRechlorinePositionDatas(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }
        
        //수질모델 리스트 조회
        public DataTable SelectQualityModel(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectQualityModelList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //재염소 처리지점 리스트 조회
        public DataTable SelectRechlorinePositionList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectRechlorinePositionList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //재염소 처리지점 저장
        public void SaveRechlorinePositionList(Hashtable conditions)
        {
            DataTable saveData = conditions["positionData"] as DataTable;
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in saveData.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        Hashtable checkConditions = new Hashtable();
                        checkConditions.Add("SIMULATION_NO", row["SIMULATION_NO"].ToString());
                        checkConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                        checkConditions.Add("ID", row["ID"].ToString());

                        if (dao.IsExistRechlorinePositionData(dbManager, checkConditions).Rows.Count != 0) throw new Exception("이미 지점이 등록되어있습니다. [" + row["ID"] + "]"); ;

                        Hashtable insertConditions = new Hashtable();
                        insertConditions.Add("SIMULATION_NO", row["SIMULATION_NO"].ToString());
                        insertConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                        insertConditions.Add("ID", row["ID"].ToString());
                        insertConditions.Add("REMARK", row["REMARK"].ToString());
                        insertConditions.Add("TAGNAME", row["TAGNAME"].ToString());
                        insertConditions.Add("POINT_NM", row["POINT_NM"].ToString());
                        insertConditions.Add("VOLUME", row["VOLUME"].ToString());

                        dao.InsertRechlorinePositionData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        Hashtable updateConditions = new Hashtable();
                        updateConditions.Add("REMARK", row["REMARK"].ToString());
                        updateConditions.Add("TAGNAME", row["TAGNAME"].ToString());
                        updateConditions.Add("POINT_NM", row["POINT_NM"].ToString());
                        updateConditions.Add("VOLUME", row["VOLUME"].ToString());
                        updateConditions.Add("SIMULATION_NO", row["SIMULATION_NO"].ToString());
                        updateConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateRechlorinePositionData(dbManager, updateConditions);
                    }
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally 
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //재염소 처리지점 저장 (일괄삭제 후 저장)
        public void InsertRechlorinePositionListAfterDelete(Hashtable conditions)
        {
            DataTable saveData = conditions["positionData"] as DataTable;
            string simulationNo = conditions["SIMULATION_NO"] as string;
            string inpNumber = conditions["INP_NUMBER"] as string;

            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                //일괄삭제
                Hashtable deleteConditions = new Hashtable();
                deleteConditions.Add("SIMULATION_NO", simulationNo);
                deleteConditions.Add("INP_NUMBER", inpNumber);

                dao.DeleteAllRechlorinePositionDatas(dbManager, deleteConditions);

                foreach (DataRow row in saveData.Rows)
                {
                    Hashtable checkConditions = new Hashtable();
                    checkConditions.Add("SIMULATION_NO", row["SIMULATION_NO"].ToString());
                    checkConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                    checkConditions.Add("ID", row["ID"].ToString());

                    if (dao.IsExistRechlorinePositionData(dbManager, checkConditions).Rows.Count != 0) throw new Exception("중복된 지점입니다. [" + row["ID"] + "]"); ;

                    Hashtable insertConditions = new Hashtable();
                    insertConditions.Add("SIMULATION_NO", row["SIMULATION_NO"].ToString());
                    insertConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                    insertConditions.Add("ID", row["ID"].ToString());
                    insertConditions.Add("REMARK", row["REMARK"].ToString());
                    insertConditions.Add("TAGNAME", row["TAGNAME"].ToString());
                    insertConditions.Add("POINT_NM", row["POINT_NM"].ToString());
                    insertConditions.Add("VOLUME", row["VOLUME"].ToString());
                    insertConditions.Add("MAP_X", row["MAP_X"].ToString());
                    insertConditions.Add("MAP_Y", row["MAP_Y"].ToString());
                
                    dao.InsertRechlorinePositionData(dbManager, insertConditions);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //재염소 처리지점 삭제
        public void DeleteRechlorinePositionData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeleteRechlorinePositionData(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //수질(염소) TAG List 조회
        public DataTable SelectQualityTagList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectQualityTagList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //해당 절점ID의 레이어 조회
        public string GetNodeLayerData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            string result = "";

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                if(dao.IsExistJunctionData(dbManager, conditions).Rows.Count > 0)
                {
                    result = "JUNCTION";
                }
                else if (dao.IsExistReservoirData(dbManager, conditions).Rows.Count > 0)
                {
                    result = "RESERVOIR";
                }
                else if (dao.IsExistTankData(dbManager, conditions).Rows.Count > 0)
                {
                    result = "TANK";
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //잔류염소 감시지점으로 등록
        public void InsertMonitorPoint(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                //지점정보 보충
                conditions.Add("MONITOR_NO", CommonUtils.GetSerialNumber("RP"));
                conditions.Add("MONPNT_GBN", "2");
                conditions.Add("SBLOCK_CODE", "");
                conditions.Add("MONITOR_YN", "N");

                dao.InsertMonitorPoint(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        #endregion

        #region 내부사용
        #endregion
    }
}
