﻿/**************************************************************************
 * 파일명   : chainManageWork.cs
 * 작성자   : shpark
 * 작성일자 : 2015.01.27
 * 설명     : 계통관리 WORK
 * 변경이력 : 2015.01.27 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Analysis.dao;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.work
{
    class ChainManageWork
    {
        private static ChainManageWork work = null;
        private ChainManageDao dao = null;

        private ChainManageWork()
        {
            dao = ChainManageDao.GetInstance();
        }

        public static ChainManageWork GetInstance()
        {
            if (work == null)
            {
                work = new ChainManageWork();
            }

            return work;
        }

        #region 외부인터페이스

        //계통마스터 리스트 조회
        public DataTable SelectChainMasterList()
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectChainMasterList(dbManager);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //계통등록지점 리스트 조회
        public DataTable SelectChainDetailList(Hashtable condition)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectChainDetailList(dbManager, condition);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //등록가능지점 리스트 조회
        public DataTable SelectMonitorPointList(Hashtable condition)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMonitorPointList(dbManager, condition);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //계통마스터 데이터 저장
        public void SaveChainMasterData(Hashtable conditions)
        {
            DataTable saveData = conditions["chainMasterData"] as DataTable;
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in saveData.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        Hashtable insertConditions = new Hashtable();
                        insertConditions.Add("CHAIN_NO", row["CHAIN_NO"].ToString());
                        insertConditions.Add("CHAIN_NM", row["CHAIN_NM"].ToString());
                        insertConditions.Add("REG_DAT", ((row["REG_DAT"].ToString()).Replace("-", "")).Substring(0, 8));
                        insertConditions.Add("REMARK", row["REMARK"].ToString());

                        dao.InsertChainMasterData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        Hashtable updateConditions = new Hashtable();
                        updateConditions.Add("CHAIN_NM", row["CHAIN_NM"].ToString());
                        updateConditions.Add("REMARK", row["REMARK"].ToString());
                        updateConditions.Add("CHAIN_NO", row["CHAIN_NO"].ToString());

                        dao.UpdateChainMasterData(dbManager, updateConditions);
                    }
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //계통마스터 데이터 삭제
        public void DeleteChainMasterData(Hashtable condition)
        { 
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                if (!condition["chainDetailCount"].ToString().Equals(0))
                {
                    dao.DeleteChainDetailData(dbManager, condition);
                }

                dao.DeleteChainMasterData(dbManager, condition);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //계통등록지점 데이터 입력. 계통 순위 인덱스 생성을 위해 기존 데이터는 모두 삭제 후 입력한다.
        public void SaveChainDetailData(Hashtable deleteCondition, UltraGrid grid)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeleteChainDetailData(dbManager, deleteCondition);

                for (int i = 0; i < grid.Rows.Count; i++)
                {
                    Hashtable saveConditions = new Hashtable();
                    saveConditions.Add("CHAIN_NO", grid.Rows[i].Cells["CHAIN_NO"].Value);
                    saveConditions.Add("MONITOR_NO", grid.Rows[i].Cells["MONITOR_NO"].Value);
                    saveConditions.Add("IDX", (i + 1).ToString());

                    dao.SaveChainDetailData(dbManager, saveConditions);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        #endregion 외부인터페이스
    }
}
