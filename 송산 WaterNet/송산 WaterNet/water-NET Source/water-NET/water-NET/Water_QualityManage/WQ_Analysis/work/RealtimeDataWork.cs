﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using WaterNet.WaterNetCore;
using WaterNet.WQ_Analysis.dao;
using WaterNet.WQ_Analysis.util;

namespace WaterNet.WQ_Analysis.work
{
    public class RealtimeDataWork
    {
        private static RealtimeDataWork work = null;
        private RealtimeDataDao dao = null;

        private RealtimeDataWork()
        {
            dao = RealtimeDataDao.GetInstance();
        }

        public static RealtimeDataWork GetInstance()
        {
            if(work == null)
            {
                work = new RealtimeDataWork();
            }

            return work;
        }

        #region 외부인터페이스

        //실시간 수질감시대상 지점 리스트 조회
        public DataTable SelectMainRealtimeCheckpointList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;
            
            try
            {
                dbManager = new OracleDBManager();                                                   
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMainRealtimeCheckpointList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실시간 수질감시지점 데이터 입력/저장 : 2015-03-12_shpark
        public void SaveMonitorPointData(Hashtable conditions)
        {
            DataTable saveData = conditions["monitorPointManageData"] as DataTable;
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in saveData.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        Hashtable insertConditions = new Hashtable();
                        insertConditions.Add("MONITOR_NO", row["MONITOR_NO"].ToString());
                        insertConditions.Add("SELECT_DATE", ((row["SELECT_DATE"].ToString()).Replace("-", "")).Substring(0, 8));
                        insertConditions.Add("MONPNT", row["MONPNT"].ToString());
                        insertConditions.Add("TAG_ID_CL", row["TAG_ID_CL"].ToString());
                        insertConditions.Add("TAG_ID_TB", row["TAG_ID_TB"].ToString());
                        insertConditions.Add("TAG_ID_PH", row["TAG_ID_PH"].ToString());
                        insertConditions.Add("TAG_ID_TE", row["TAG_ID_TE"].ToString());
                        insertConditions.Add("TAG_ID_CU", row["TAG_ID_CU"].ToString());
                        insertConditions.Add("MAP_X", row["MAP_X"].ToString());
                        insertConditions.Add("MAP_Y", row["MAP_Y"].ToString());
                        insertConditions.Add("MONITOR_YN", row["MONITOR_YN"].ToString());

                        dao.InsertMonitorPointData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        Hashtable updateConditions = new Hashtable();
                        updateConditions.Add("MONPNT", row["MONPNT"].ToString());
                        updateConditions.Add("TAG_ID_CL", row["TAG_ID_CL"].ToString());
                        updateConditions.Add("TAG_ID_TB", row["TAG_ID_TB"].ToString());
                        updateConditions.Add("TAG_ID_PH", row["TAG_ID_PH"].ToString());
                        updateConditions.Add("TAG_ID_TE", row["TAG_ID_TE"].ToString());
                        updateConditions.Add("TAG_ID_CU", row["TAG_ID_CU"].ToString());
                        updateConditions.Add("MAP_X", row["MAP_X"].ToString());
                        updateConditions.Add("MAP_Y", row["MAP_Y"].ToString());
                        updateConditions.Add("MONITOR_YN", row["MONITOR_YN"].ToString());
                        updateConditions.Add("MONITOR_NO", row["MONITOR_NO"].ToString());

                        dao.UpdateMonitorPointData(dbManager, updateConditions);
                    }
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //수질감시지점 데이터 삭제 : 2015-03-12_shpark
        public void DeleteMonitorPointData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeleteMonitorPointData(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //감시지점/항목 별 실시간 계측데이터 조회
        public DataTable SelectMainRealtimeDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMainRealtimeDataList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실시간 수질데이터 조회 초기화
        public Hashtable InitRealtimeDataList()
        {
            Hashtable result = new Hashtable();
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                //지점구분 데이터 조회
                DataTable monitorGbn = dao.SelectMonitorGbnList(dbManager, null);
                CommonUtils.AddAllRow(monitorGbn);                                  //전체선택 추가

                //수질항목 데이터 조회
                DataTable qualityItem = dao.SelectQualityItemList(dbManager, null);
                CommonUtils.AddAllRow(qualityItem);

                result.Add("monitorGbn", monitorGbn);
                result.Add("qualityItem", qualityItem);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실시간 감시지점 리스트 조회
        public DataTable SelectCheckpointList(Hashtable conditions)
        {
            Hashtable result = new Hashtable();
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                return dao.SelectCheckpointList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //수질항목별 태그 리트스 조회 : 2015-03-12_shpark
        public DataTable SelectMonitorPointTagList(Hashtable conditions)
        {
            Hashtable result = new Hashtable();
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                return dao.SelectMonitorPointTagList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //실시간 데이터 리스트 조회
        public DataTable SelectRealtimeDataList(Hashtable conditions)
        {
            Hashtable result = new Hashtable();
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                return dao.SelectRealtimeDataList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //수질 계통 리스트 조회
        public DataTable SelectQualityChainList(Hashtable conditions)
        {
            Hashtable result = new Hashtable();
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                return dao.SelectQualityChainList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //수질 계통 실시간 데이터 조회
        public DataTable SelectChainRealtimeDataList(Hashtable conditions)
        {
            Hashtable result = new Hashtable();
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                return dao.SelectChainRealtimeDataList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //수질정보 현황 조회(Dashboard)
        public DataTable SelectRealtimeQualityStateForDashboard(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                DataTable realtimeQualityData = dao.SelectRealtimeQualityDataForDashboard(dbManager, null);

                foreach(DataRow row in realtimeQualityData.Rows)
                {
                    row["CL_VALUE"] = row["CL_VALUE"] + "\n" + GetQualityState(dbManager, row, "CL_VALUE", "CL");
                    row["TB_VALUE"] = row["TB_VALUE"] + "\n" + GetQualityState(dbManager, row, "TB_VALUE", "TB");
                    row["PH_VALUE"] = row["PH_VALUE"] + "\n" + GetQualityState(dbManager, row, "PH_VALUE", "PH");
                }

                return realtimeQualityData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //시계열 수질데이터 조회(Dashboard)
        public DataTable SelectTimeSeriesQualityDataForDashboard(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                return dao.SelectTimeSeriesQualityDataForDashboard(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //계통별 수질데이터 조회(Dashboard)
        public Hashtable SelectQualityChainDataForDashboard(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            Hashtable result = new Hashtable();

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                if (conditions["CHAIN_NO"] == null)
                {
                    DataTable topChainData = dao.SelectTopQualityChainForDashboard(dbManager, null);

                    if(topChainData.Rows.Count == 0) return null;

                    conditions.Add("CHAIN_NO", topChainData.Rows[0]["CHAIN_NO"].ToString());

                    //최초 검색(최초 로딩될때 기본 조회)시 계통이 선택되지 않았기때문에 최상위 계통 1개를 적용하고 계통정보를 결과와 같이 넘긴다.
                    result.Add("CHAIN_NO", topChainData.Rows[0]["CHAIN_NO"].ToString());
                    result.Add("CHAIN_NM", topChainData.Rows[0]["CHAIN_NM"].ToString());
                }

                result.Add("qualityChainData", dao.SelectChainRealtimeDataList(dbManager, conditions));

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //수질정보 현황 조회(수질경보설정값) : shpark_2015-03-12
        public DataTable SelectRealtimeQualityStateForSetting(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                DataTable monitorPointSettingData = dao.SelectQualityWarningSettingForDashboard(dbManager, conditions);

                return monitorPointSettingData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //경보범위설정 데이터 입력/저장 : shpark_2015-03-12
        public void SaveQualityWarningSetting(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                DataTable dt = dao.SelectQualityWarningSettingForDashboard(dbManager, conditions);

                if (dt.Rows.Count == 0)
                {
                    dao.InsertQualityWarningSetting(dbManager, conditions);
                }
                else
                {
                    dao.UpdateQualityWarningSetting(dbManager, conditions);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //경보범위설정 데이터 삭제 : shpark_2015-03-12
        public void DeleteQualityWarningSetting(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeleteQualityWarningSetting(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        #endregion

        #region 내부사용

        //경고정보를 기준으로 현재상태 Setting
        private string GetQualityState(OracleDBManager dbManager, DataRow qualityData, string itemFieldName, string itemCode)
        {
            string state = "";

            if (!"".Equals(qualityData[itemFieldName].ToString()))
            {
                double qualityValue = double.Parse(qualityData[itemFieldName].ToString());

                //Warning 설정치 조회
                Hashtable settingConditons = new Hashtable();
                settingConditons.Add("MONITOR_NO", qualityData["MONITOR_NO"].ToString());
                settingConditons.Add("WQ_ITEM_CODE", itemCode);

                DataTable settingData = dao.SelectQualityWarningSettingForDashboard(dbManager, settingConditons);        //잔류염소 설정치

                if (settingData.Rows.Count != 0)
                {
                    if("CL".Equals(itemCode) || "PH".Equals(itemCode))      //잔류염소/pH
                    {
                        if ((settingData.Rows[0]["N1_TOP_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N1_TOP_LIMIT"].ToString()))
                        && (settingData.Rows[0]["N1_LOW_LIMIT"] == null || "".Equals(settingData.Rows[0]["N1_LOW_LIMIT"].ToString()))
                        && (settingData.Rows[0]["N2_TOP_LIMIT"] == null || "".Equals(settingData.Rows[0]["N2_TOP_LIMIT"].ToString()))
                        && (settingData.Rows[0]["N2_LOW_LIMIT"] == null || "".Equals(settingData.Rows[0]["N2_LOW_LIMIT"].ToString())))
                        {
                            //수질항목 상태 설정 (type 1 - 설정치 1개만 존재 : 설정치이하 위험 그외 정상)
                            if (qualityValue <= double.Parse(settingData.Rows[0]["N1_TOP_LIMIT"].ToString())) state = "위험";
                            else state = "양호";
                        }
                        else if ((settingData.Rows[0]["N1_TOP_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N1_TOP_LIMIT"].ToString()))
                        && (settingData.Rows[0]["N1_LOW_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N1_LOW_LIMIT"].ToString()))
                        && (settingData.Rows[0]["N2_TOP_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N2_TOP_LIMIT"].ToString()))
                        && (settingData.Rows[0]["N2_LOW_LIMIT"] == null || "".Equals(settingData.Rows[0]["N2_LOW_LIMIT"].ToString())))
                        {
                            //재염소 상태 설정 (type 2 - 설정치 3개 존재 : 상단 위험영역이 없음)
                            double value1 = double.Parse(settingData.Rows[0]["N1_TOP_LIMIT"].ToString());
                            double value2 = double.Parse(settingData.Rows[0]["N1_LOW_LIMIT"].ToString());
                            double value3 = double.Parse(settingData.Rows[0]["N2_TOP_LIMIT"].ToString());

                            if (qualityValue <= value2 && qualityValue > value3) state = "양호";
                            else if (qualityValue <= value1 && qualityValue > value2) state = "관심";
                            //else if (qualityValue <= value3) state = "위험";
                            else if (qualityValue <= value3) state = "관심";        //2014.11.07 자동드레인/관말수용가가 위험상태일 경우 관심으로 표시하도록 변경 khb
                        }
                        else if ((settingData.Rows[0]["N1_TOP_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N1_TOP_LIMIT"].ToString()))
                            && (settingData.Rows[0]["N1_LOW_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N1_LOW_LIMIT"].ToString()))
                             && (settingData.Rows[0]["N2_TOP_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N2_TOP_LIMIT"].ToString()))
                            && (settingData.Rows[0]["N2_LOW_LIMIT"] != null && !"".Equals(settingData.Rows[0]["N2_LOW_LIMIT"].ToString())))
                        {
                            //재염소 상태 설정 (type 3 - 설정치 4개 존재 : 모든 영역 존재)
                            double value1 = double.Parse(settingData.Rows[0]["N1_TOP_LIMIT"].ToString());
                            double value2 = double.Parse(settingData.Rows[0]["N1_LOW_LIMIT"].ToString());
                            double value3 = double.Parse(settingData.Rows[0]["N2_TOP_LIMIT"].ToString());
                            double value4 = double.Parse(settingData.Rows[0]["N2_LOW_LIMIT"].ToString());

                            if (qualityValue > value1 || qualityValue <= value4) state = "위험";
                            else if ((qualityValue > value2 && qualityValue <= value1) || (qualityValue <= value3 && qualityValue > value4)) state = "관심";
                            else if (qualityValue <= value2 && qualityValue > value3) state = "양호";
                        }
                    }
                    else if("TB".Equals(itemCode))  //탁도 (탁도는 항목이 2개임)
                    {
                        double value1 = double.Parse(settingData.Rows[0]["N1_TOP_LIMIT"].ToString());
                        double value2 = double.Parse(settingData.Rows[0]["N1_LOW_LIMIT"].ToString());
                        
                        if (qualityValue <= value2) state = "양호";
                        else if (qualityValue <= value1 && qualityValue > value2) state = "관심";
                        else if (qualityValue > value1) state = "위험";
                    }
                }
            }

            return state;
        }

        #endregion
    }
}
