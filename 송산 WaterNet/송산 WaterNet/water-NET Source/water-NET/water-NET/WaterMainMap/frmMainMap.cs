﻿using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Oracle.DataAccess.Client;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

namespace WaterNet.WaterMainMap
{
    public partial class frmMainMap : WaterAOCore.frmMap, WaterNetCore.IForminterface
    {
        private OracleDBManager m_oDBManager = new OracleDBManager(); //DataBase 

        private ArrayList m_ArrayContents = new ArrayList(); //유량계 폼배열
        private ArrayList m_CurrentArrayContents = new ArrayList(); //블록선택시 초기지도에 표시된 유량계폼
        private ArrayList m_ArrayBlockContents = new ArrayList(); //블록(대,중,소) 현황정보 폼 배열
        private ILayer m_Layer = null; //유량계 레이어

        public frmMainMap()
        {
            InitializeComponent();
            InitializeSetting();
        }

        //------------------------------------------------------------------
        //메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.
        //FormID : 탭에 보여지는 이름
        //FormKey : 현재 프로젝트 이름
        //------------------------------------------------------------------
        #region IForminterface 멤버

        public string FormID
        {
            get { return "블록유량현황"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();
            DockManagerCommand.Visible = false;
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();
            //추가적인 구현은 여기에....
            //Viewmap_Load();

            

            m_Layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "유량계");
            ArcManager.SetLayer2Scale(m_Layer, 0, 0);

            MakeControls();
            MakeBlockControl();

        }


        /// <summary>
        /// 대.중.소블록의 현황정보 화면을 Array에 생성
        /// </summary>
        private void MakeBlockControl()
        {
            Form oControl = default(Form);
            oControl = new frmBigBlockInfo();
            oControl.TopLevel = false;
            oControl.Parent = spcContents.Panel2;
            oControl.Tag = Convert.ToString(oControl.Name);
            m_ArrayBlockContents.Add(oControl);

            oControl = new frmMiddleBlockInfo();
            oControl.TopLevel = false;
            oControl.Parent = spcContents.Panel2;
            oControl.Tag = Convert.ToString(oControl.Name);
            m_ArrayBlockContents.Add(oControl);

            oControl = new frmSmallBlockInfo();
            oControl.TopLevel = false;
            oControl.Parent = spcContents.Panel2;
            oControl.Tag = Convert.ToString(oControl.Name);
            m_ArrayBlockContents.Add(oControl);

        }

        /// <summary>
        /// MakeControl - 유량계 실시간수량 화면 생성하여 Array에 저장
        /// </summary>
        private void MakeControls()
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;  //유량계 레이어
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor pFCursor = pFeatureClass.Search(null, true);
                comReleaser.ManageLifetime(pFCursor);

                IFeature pFeature = null;

                while ((pFeature = pFCursor.NextFeature()) != null)
                {
                    frmControl oControl = new frmControl();
                    oControl.TopLevel = false;
                    oControl.Parent = spcContents.Panel2;
                    oControl.varFTR_IDN = Convert.ToString(ArcManager.GetValue(pFeature, "FTR_IDN"));
                    oControl.varName = Convert.ToString(ArcManager.GetValue(pFeature, "FLO_NM"));
                    getIHTAG4Flow(ref oControl);

                    if ((oControl.varFI_TAGNAME.Length == 0) | (oControl.varFQ_TAGNAME.Length == 0)) continue;

                    m_ArrayContents.Add(oControl);
                }
            }
        }

        private void getIHTAG4Flow(ref frmControl oControl)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDataReader oReader = null;

            try
            {
                oStringBuilder.AppendLine("SELECT B.TAGNAME, B.TAG_GBN, A.FTR_IDN, C.LOC_CODE, C.PLOC_CODE, C.LOC_GBN,    ");
                oStringBuilder.AppendLine("       C.FTR_IDN AS BFTR_IDN, C.LOC_NAME, C.PFTR_CODE, C.PFTR_IDN, B.TAG_DESC  ");
                oStringBuilder.AppendLine("  FROM IF_IHTAGS A                                                             ");
                oStringBuilder.AppendLine("      ,IF_TAG_GBN B                                                            ");
                oStringBuilder.AppendLine("      ,CM_LOCATION C                                                           ");
                oStringBuilder.AppendLine(" WHERE A.TAGNAME = B.TAGNAME                                                   ");
                oStringBuilder.AppendLine("   AND A.USE_YN = 'Y'                                                          ");
                oStringBuilder.AppendLine("   AND A.FTR_IDN = '" + oControl.varFTR_IDN + "'");
                oStringBuilder.AppendLine("   and a.tag_gbn = '유량계'");
                oStringBuilder.AppendLine("   AND A.LOC_CODE = C.LOC_CODE                                                 ");
                oStringBuilder.AppendLine("   AND B.TAG_GBN IN ('PRI','FRI','TD')                                         ");

                //oStringBuilder.AppendLine("SELECT DISTINCT A.TAGNAME, A.FTR_IDN, BR_CODE, FN_CODE, IO_GBN, B.LOC_CODE, PLOC_CODE, LOC_GBN, B.FTR_IDN AS BFTR_IDN, B.LOC_NAME, PFTR_CODE, PFTR_IDN, A.DESCRIPTION");
                //oStringBuilder.AppendLine("FROM IF_IHTAGS A");
                //oStringBuilder.AppendLine("   , CM_LOCATION B");
                //oStringBuilder.AppendLine("WHERE A.LOC_CODE = B.LOC_CODE AND A.FTR_IDN IS NOT NULL AND USE_YN = 'Y'");
                //oStringBuilder.AppendLine("  AND A.FN_CODE IN ('I','Q') AND BR_CODE IN ('FR', 'PR')");
                //oStringBuilder.AppendLine("  AND A.FTR_IDN = '" + oControl.varFTR_IDN + "'");

                oReader = m_oDBManager.ExecuteScriptDataReader(oStringBuilder.ToString(), null);
                if (oReader == null) return;
                if (!oReader.HasRows) return;

                while (oReader.Read())
                {
                    switch (Convert.ToString(oReader["TAG_GBN"]))
                    {
                        case "PRI":  //수압
                            oControl.varPR_TAGNAME = Convert.ToString(oReader["TAGNAME"]);
                            break;
                        case "FRI":  //유량
                            //oControl.varIO_GBN = "INF";
                            oControl.varFI_TAGNAME = Convert.ToString(oReader["TAGNAME"]);
                            break;
                        case "TD":
                            oControl.varFQ_TAGNAME = Convert.ToString(oReader["TAGNAME"]);
                            break;
                    }

                    //if (Convert.ToString(oReader["BR_CODE"]).Equals("FR") && Convert.ToString(oReader["FN_CODE"]).Equals("I")) //유량 - 유입
                    //{
                    //    oControl.varIO_GBN = ((Convert.IsDBNull(oReader["IO_GBN"])) | (!Convert.ToString(oReader["IO_GBN"]).Equals("OUT"))) ? "INF" : "OUT";
                    //    oControl.varFI_TAGNAME = Convert.ToString(oReader["TAGNAME"]);
                    //}
                    //else if (Convert.ToString(oReader["BR_CODE"]).Equals("FR") && Convert.ToString(oReader["FN_CODE"]).Equals("Q")) //유량 - 유출
                    //{
                    //    oControl.varFQ_TAGNAME = Convert.ToString(oReader["TAGNAME"]);
                    //}
                    //else if (Convert.ToString(oReader["BR_CODE"]).Equals("PR") && Convert.ToString(oReader["FN_CODE"]).Equals("I"))  //수압
                    //{
                    //    oControl.varPR_TAGNAME = Convert.ToString(oReader["TAGNAME"]);
                    //}
                    oControl.varLOC_GBN = Convert.ToString(oReader["LOC_GBN"]);
                    oControl.varPFTR_IDN = Convert.ToString(oReader["PFTR_IDN"]);
                    oControl.varPFTR_CODE = Convert.ToString(oReader["PFTR_CODE"]);
                    oControl.varBFTR_IDN = Convert.ToString(oReader["BFTR_IDN"]);
                    oControl.varLOC_CODE = Convert.ToString(oReader["LOC_CODE"]);
                }
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.Message);
            }
            finally
            {
                if (oReader != null) oReader.Close();
            }
            
        }

        private void frmMainMap_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_ArrayContents.Clear();
            m_ArrayBlockContents.Clear();
        }

        private void frmMainMap_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_oDBManager != null) m_oDBManager.Close();
        }

        ///현재 보여지고 있는 실시간유량 화면은 Move로 처리하는 로직으로 변경 필요
        ///대블록일경우, 배수지단의 실시간유량화면만 표시되도록 처리로직 필요
        /// <summary>
        /// 현재 Extent에 Intersect되는 유량계의 실시간유량화면을 표시한다.
        /// </summary>
        /// <param name="pEnvelope"></param>
        private void ExtentUpdatedEvent(IEnvelope pEnvelope)
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;  //유량계 레이어
            //if (pFeatureLayer.MaximumScale > axMap.MapScale && pFeatureLayer.MaximumScale != 0) return;
            //if (pFeatureLayer.MinimumScale < axMap.MapScale && pFeatureLayer.MinimumScale != 0) return;

            ///먼저 화면상의 유량계 폼 Control을 지도상에서 모두지운다.
            foreach (var item in m_ArrayContents)   ((Form)item).Visible = false;
            Application.DoEvents();
            if (!pFeatureLayer.Visible) return; //Layer.Visible = false => 콤포넌트를 지운상태로 리턴
            

            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = axMap.Extent;
            pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;
            pSpatialFilter.WhereClause = string.Empty;

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor pFCursor = pFeatureClass.Search(pSpatialFilter, true);
                comReleaser.ManageLifetime(pFCursor);

                IFeature pFeature = pFCursor.NextFeature();

                while (pFeature != null)
                {
                    IGeometry pGeom = pFeature.Shape;
                    if (pGeom is IPoint)
                    {
                        IPoint pPoint = pGeom as IPoint;
                        int x; int y;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.Units = esriUnits.esriMeters;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.FromMapPoint(pPoint, out x, out y);

                        Form oControl = GetControl(Convert.ToString(ArcManager.GetValue(pFeature, "FTR_IDN")));
                        if (oControl != null)
                        {
                            oControl.Visible = true;
                            oControl.SetBounds(x + 10, y + 10, oControl.Width, oControl.Height);
                            oControl.BringToFront();
                        }
                    }
                    pFeature = pFCursor.NextFeature();
                }
            }
            
        }

        /// <summary>
        /// 지도의 Extent가 변경시 ExtentUpdatedEvent를 호출한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMap_OnExtentUpdated(object sender, IMapControlEvents2_OnExtentUpdatedEvent e)
        {
            ExtentUpdatedEvent(e.newEnvelope as IEnvelope);
        }

        private void frmMainMap_Load(object sender, EventArgs e)
        {

        }

        private void frmMainMap_ResizeEnd(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// 화면의 크기변경시 ExtentUpdatedEvent를 호출한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMainMap_SizeChanged(object sender, EventArgs e)
        {
            IEnvelope pEnv = axMap.Extent;
            ExtentUpdatedEvent(pEnv as IEnvelope);
        }

        /// <summary>
        /// TOC(블록)에서 블록을 선택시 블록현황화면을 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void tvBlock_AfterSelect(object sender, TreeViewEventArgs e)
        {
            base.tvBlock_AfterSelect(sender, e);

        }

        private void tvBlock_Click(object sender, EventArgs e)
        {

        }

        private void tvBlock_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
           
        }

        /// <summary>
        /// Array에 저장된 실시간수량화면을 현재 Array폼에 적용한다.
        /// </summary>
        /// <param name="nOID">유량계 OID</param>
        /// <returns></returns>
        private Form GetControl(string nOID)
        {
            foreach (var item in m_CurrentArrayContents)
            {
                if (((frmControl)item).varFTR_IDN == nOID)
                {
                    return (frmControl)item;
                }
            }
            return null;
        }

        /// <summary>
        /// 선택된 블록에 따라 보여질 컨트롤 폼 배열정의
        /// </summary>
        /// <param name="oNode"></param>
        private void ReSetCurrentControl(TreeNode oNode)
        {
            m_CurrentArrayContents.Clear();

            switch (oNode.Name)
            {
                case "BZ001":    //대블록
                    foreach (var item in m_ArrayContents)
                    {
                        switch (((frmControl)item).varLOC_GBN)
                        {
                            case "중블록":
                            case "배수지":
                                m_CurrentArrayContents.Add(item);
                                break;
                        }
                    }
                    break;
                case "BZ002":    //중블록
                    foreach (var item in m_ArrayContents)
                    {
                        switch (((frmControl)item).varLOC_GBN)
                        {
                            case "소블록":
                                if (((frmControl)item).varPFTR_IDN == Convert.ToString(oNode.Tag)) // && ((frmControl)item).varPFTR_CODE == "BZ002")
                                {
                                    m_CurrentArrayContents.Add(item);
                                }
                                break;
                            case "배수지":
                                if (((frmControl)item).varBFTR_IDN == Convert.ToString(oNode.Tag))
                                {
                                    m_CurrentArrayContents.Add(item);                                    
                                }
                                break;
                        }
                    }
                    break;
                case "BZ003":    //소블록
                    foreach (var item in m_ArrayContents)
                    {
                        switch (((frmControl)item).varLOC_GBN)
                        {
                            case "소블록":
                                if (((frmControl)item).varBFTR_IDN == Convert.ToString(oNode.Tag))
                                {
                                    m_CurrentArrayContents.Add(item);
                                }
                                break;
                        }
                    }
                    break;
            }

            //MessageBox.Show(m_CurrentArrayContents.Count.ToString());
            
        }

        /// <summary>
        /// 블록 트리뷰 노드를 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvBlock_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode oNode = e.Node;
            if (oNode == null) return;

            IEnvelope pEnv = axMap.Extent;

            switch (oNode.Name)
            {
                case "BZ001":    //대블록
                    //foreach (var item in m_ArrayBlockContents)
                    //{
                    //    if (((Form)item).Name == "frmBigBlockInfo")
                    //    {
                    //        ((Form)item).Visible = true;
                    //        ((Form)item).SetBounds(((Form)item).Parent.Location.X + 10, ((Form)item).Parent.Location.Y + 10, ((Form)item).Width, ((Form)item).Height);
                    //        ((Form)item).BringToFront();
                    //        ((frmBigBlockInfo)item).Open(oNode.Text);
                    //    }
                    //    else ((Form)item).Visible = false;
                    //}

                    break;
                case "BZ002":    //중블록
                    //foreach (var item in m_ArrayBlockContents)
                    //{
                    //    if (((Form)item).Name == "frmMiddleBlockInfo")
                    //    {
                    //        ((Form)item).Visible = true;
                    //        ((Form)item).SetBounds(((Form)item).Parent.Location.X + 10, ((Form)item).Parent.Location.Y + 10, ((Form)item).Width, ((Form)item).Height);
                    //        ((Form)item).BringToFront();
                    //        ((frmMiddleBlockInfo)item).Open(oNode.Text);
                    //    }
                    //    else ((Form)item).Visible = false;
                    //}
                    break;
                case "BZ003":    //소블록
                    //foreach (var item in m_ArrayBlockContents)
                    //{
                    //    if (((Form)item).Name == "frmSmallBlockInfo")
                    //    {
                    //        ((Form)item).Visible = true;
                    //        ((Form)item).SetBounds(((Form)item).Parent.Location.X + 10, ((Form)item).Parent.Location.Y + 10, ((Form)item).Width, ((Form)item).Height);
                    //        ((Form)item).BringToFront();
                    //        ((frmSmallBlockInfo)item).Open(oNode.Text);
                    //    }
                    //    else ((Form)item).Visible = false;
                    //}

                    break;
            }
            ReSetCurrentControl(oNode);
            ExtentUpdatedEvent(pEnv);
        }


    }
}
