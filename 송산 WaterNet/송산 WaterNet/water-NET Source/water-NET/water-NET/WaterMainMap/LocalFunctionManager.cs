﻿using System;
using System.Text;

using WaterNet.WaterNetCore;

namespace WaterNet.WaterMainMap
{
    public static class LocalFunctionManager
    {
        /// <summary>
        /// 계측 TAG enum
        /// </summary>
        public enum TAG_BRCODE
        {
            TB = 1, //"탁도",
            CL = 2, //"잔류염소",
            PH = 3, //"수소이온농도",
            CU = 4, //"전기전도도",
            TE = 5, //"온도",
            FR = 6, //"유량",
            LE = 7, //"수위",
            PR = 8 //"압력"
        }

        /// <summary>
        /// 계측 TAG enum
        /// </summary>
        public enum TAG_IOCODE
        {
            INF = 1, //"유입",
            OUT = 2 //"유출"
        }

        /// <summary>
        /// 계측 TAG enum
        /// </summary>
        public enum TAG_FNCODE
        {
            I = 1, //"순시",
            Q = 2 //"적산"
        }
        #region "IHTAG 값 가져오기 - Get_IHTagName"

        /// <summary>
        /// IHTAG 값 가져오기
        /// </summary>
        /// <param name="strSQLScript">SQL 스크립트</param>
        /// <remarks></remarks>
        public static object Get_IHTagName(string tag_gbn, string ftr_idn,TAG_BRCODE br_code, TAG_FNCODE fn_code)
        {
            OracleDBManager oDBManager = null;

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT TAGNAME");
            oStringBuilder.AppendLine("  FROM IF_IHTAGS ");
            oStringBuilder.AppendLine(" WHERE    USE_YN   = 'Y'");
            if (!string.IsNullOrEmpty(tag_gbn))
            {
                oStringBuilder.AppendLine("AND TAG_GBN = '" + tag_gbn + "'");
            }

            switch (br_code)
            {
                case TAG_BRCODE.TB:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'TB'");
                    break;
                case TAG_BRCODE.CL:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'CL'");
                    break;
                case TAG_BRCODE.PH:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'PH'");
                    break;
                case TAG_BRCODE.CU:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'CU'");
                    break;
                case TAG_BRCODE.TE:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'TE'");
                    break;
                case TAG_BRCODE.FR:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'FR'");
                    break;
                case TAG_BRCODE.LE:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'LE'");
                    break;
                case TAG_BRCODE.PR:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'PR'");
                    break;
            }

            switch (fn_code)
            {
                case TAG_FNCODE.I:
                    oStringBuilder.AppendLine("AND    FN_CODE   = 'I'");
                    break;
                case TAG_FNCODE.Q:
                    oStringBuilder.AppendLine("AND    FN_CODE   = 'Q'");
                    break;
            }
            oStringBuilder.AppendLine("AND    FTR_IDN   = '" + ftr_idn + "'");

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                Console.WriteLine(oStringBuilder.ToString());
                return oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null);
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.WaterNetCore", "FunctionManager", "Get_IHTagName()", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
        }
        /// <summary>
        /// IHTAG 값 가져오기
        /// </summary>
        /// <param name="strSQLScript">SQL 스크립트</param>
        /// <remarks></remarks>
        public static object Get_IHTagName(string tag_gbn, string ftr_idn, TAG_BRCODE br_code, TAG_FNCODE fn_code, TAG_IOCODE io_gbn)
        {
            OracleDBManager oDBManager = null;

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT TAGNAME");
            oStringBuilder.AppendLine("  FROM IF_IHTAGS");
            oStringBuilder.AppendLine(" WHERE    USE_YN   = 'Y'");
            oStringBuilder.AppendLine("   AND    FTR_IDN   = '" + ftr_idn + "'");
            if (!string.IsNullOrEmpty(tag_gbn))
            {
                oStringBuilder.AppendLine("AND TAG_GBN = '" + tag_gbn + "'");    
            }

            switch (br_code)
            {
                case TAG_BRCODE.TB:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'TB'");
                    break;
                case TAG_BRCODE.CL:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'CL'");
                    break;
                case TAG_BRCODE.PH:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'PH'");
                    break;
                case TAG_BRCODE.CU:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'CU'");
                    break;
                case TAG_BRCODE.TE:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'TE'");
                    break;
                case TAG_BRCODE.FR:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'FR'");
                    break;
                case TAG_BRCODE.LE:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'LE'");
                    break;
                case TAG_BRCODE.PR:
                    oStringBuilder.AppendLine("AND    BR_CODE   = 'PR'");
                    break;
            }

            switch (fn_code)
            {
                case TAG_FNCODE.I:
                    oStringBuilder.AppendLine("AND    FN_CODE   = 'I'");

                    switch (io_gbn)
                    {
                        case TAG_IOCODE.INF:
                            oStringBuilder.AppendLine("AND IO_GBN <> 'OUT'");
                            break;
                        case TAG_IOCODE.OUT:
                            oStringBuilder.AppendLine("AND IO_GBN = 'OUT'");
                            break;
                    }
                    break;
                case TAG_FNCODE.Q:
                    oStringBuilder.AppendLine("AND    FN_CODE   = 'Q'");
                    break;
            }
            

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                //Console.WriteLine(oStringBuilder.ToString());
                return oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null);
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.WaterNetCore", "FunctionManager", "Get_IHTagName()", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
        }
        #endregion
    }
}
