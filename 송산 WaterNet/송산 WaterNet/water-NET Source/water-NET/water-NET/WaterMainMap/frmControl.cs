﻿using System;
using System.Text;
using System.Windows.Forms;


using WaterNet.WaterNetCore;

namespace WaterNet.WaterMainMap
{
    public partial class frmControl : Form
    {
        private Single m_oldX;
        private Single m_oldY;
        
        private WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();

        //------------------------------------------------------
        private string m_FTR_IDN = string.Empty;
        private string m_LOC_GBN = string.Empty;
        private string m_BFTR_IDN;
        private string m_LOC_NAME;
        private string m_PFTR_CODE;
        private string m_PFTR_IDN;
        private string m_LOC_CODE;
        private string m_PLOC_CODE;
        private string m_DESCRIPTION;
        private string m_PR_TAGNAME = string.Empty; //수압
        private string m_FI_TAGNAME = string.Empty; //순시유량(입력)
        private string m_FQ_TAGNAME = string.Empty; //금일적산

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="FTR_IDN"></param>
        public frmControl()
        {
            InitializeComponent();
        }

        public string varFTR_IDN
        {
            get { return m_FTR_IDN; }
            set { m_FTR_IDN = value; }
        }

        public string varLOC_GBN
        {
            get { return m_LOC_GBN; }
            set { m_LOC_GBN = value; }
        }

        public string varLOC_NAME
        {
            get { return m_LOC_NAME; }
            set { m_LOC_NAME = value; }
        }

        public string varBFTR_IDN
        {
            get { return m_BFTR_IDN; }
            set { m_BFTR_IDN = value; }
        }

        public string varPFTR_CODE
        {
            get { return m_PFTR_CODE; }
            set { m_PFTR_CODE = value; }
        }

        public string varPFTR_IDN
        {
            get { return m_PFTR_IDN; }
            set { m_PFTR_IDN = value; }
        }

        public string varLOC_CODE
        {
            get { return m_LOC_CODE; }
            set { m_LOC_CODE = value; }
        }

        public string varPLOC_CODE
        {
            get { return m_PLOC_CODE; }
            set { m_PLOC_CODE = value; }
        }

        public string varPR_TAGNAME
        {
            get { return m_PR_TAGNAME; }
            set { m_PR_TAGNAME = value; }
        }

        public string varFI_TAGNAME
        {
            get { return m_FI_TAGNAME; }
            set { m_FI_TAGNAME = value; }
        }

        public string varFQ_TAGNAME
        {
            get { return m_FQ_TAGNAME; }
            set { m_FQ_TAGNAME = value; }
        }

        public string varDESCRIPTION
        {
            get { return m_DESCRIPTION; }
            set 
            {
                m_DESCRIPTION = value;
                lblName.Text = value; 
            }
        }

        public string varName
        {
            set { lblName.Text = value; }
        }

        #region Timer
        private void timer1_Tick(object sender, EventArgs e)
        {
            Execute_QuerySelect();
        }
        #endregion

        /// <summary>
        /// 유량계 실시간 데이터 쿼리
        /// </summary>
        private void Execute_QuerySelect()
        {
            //if (m_FTR_IDN.Equals("2006016424"))
            //{
                //Console.WriteLine(m_FTR_IDN);
            //}
            //m_IN_Tag =Convert.ToString(LocalFunctionManager.Get_IHTagName(string.Empty, m_FTR_IDN, LocalFunctionManager.TAG_BRCODE.FR, LocalFunctionManager.TAG_FNCODE.I, LocalFunctionManager.TAG_IOCODE.INF));
            //m_OUT_Tag = Convert.ToString(LocalFunctionManager.Get_IHTagName(string.Empty, m_FTR_IDN, LocalFunctionManager.TAG_BRCODE.FR, LocalFunctionManager.TAG_FNCODE.I, LocalFunctionManager.TAG_IOCODE.OUT));
            //m_SUM_Tag = Convert.ToString(LocalFunctionManager.Get_IHTagName(string.Empty, m_FTR_IDN, LocalFunctionManager.TAG_BRCODE.FR, LocalFunctionManager.TAG_FNCODE.Q, LocalFunctionManager.TAG_IOCODE.INF));
            //Console.WriteLine(m_FTR_IDN + " => " + m_IN_Tag + " : " + m_OUT_Tag + " : " + m_SUM_Tag);

            StringBuilder oStringBuilder = new StringBuilder();
            if (!string.IsNullOrEmpty(m_FI_TAGNAME)) //유입 or 유출 TagName
            {
                //lblInput.Text = "유량";
                oStringBuilder.AppendLine("SELECT /*if_gather_realtime if_gather_realtime_pk*/ VALUE");
                oStringBuilder.AppendLine("FROM IF_GATHER_REALTIME");
                oStringBuilder.AppendLine("WHERE TAGNAME = '" + m_FI_TAGNAME + "'");
                oStringBuilder.AppendLine("AND TIMESTAMP = (SELECT /*if_gather_realtime if_gather_realtime_pk*/ ");
                oStringBuilder.AppendLine("                  MAX(TIMESTAMP) FROM IF_GATHER_REALTIME WHERE TAGNAME = '" + m_FI_TAGNAME + "' )");

                txtInput.Text = Convert.ToString(Math.Round(Convert.ToDouble(m_oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null)),2));
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            if (!string.IsNullOrEmpty(m_FQ_TAGNAME)) //적산 TagName
            {
                oStringBuilder.AppendLine("SELECT /*if_accumulation_today if_accu_today_pk */ VALUE");
                oStringBuilder.AppendLine("FROM IF_ACCUMULATION_TODAY");
                oStringBuilder.AppendLine("WHERE TAGNAME =  '" + m_FQ_TAGNAME + "'");
                oStringBuilder.AppendLine("AND TIMESTAMP = (SELECT /*if_accumulation_today if_accu_today_pk */ ");
                oStringBuilder.AppendLine("                        MAX(TIMESTAMP) FROM IF_ACCUMULATION_TODAY WHERE TAGNAME = '" + m_FQ_TAGNAME + "' )");
                txtSum.Text = WaterNetCore.FormManager.StringInsertComma(Convert.ToString(m_oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null)));
            }

            ///수압
            oStringBuilder.Remove(0, oStringBuilder.Length);
            if (!string.IsNullOrEmpty(m_FQ_TAGNAME)) //수압 TagName
            {
                oStringBuilder.AppendLine("SELECT /*if_gather_realtime if_gather_realtime_pk*/ VALUE");
                oStringBuilder.AppendLine("FROM IF_GATHER_REALTIME");
                oStringBuilder.AppendLine("WHERE TAGNAME =  '" + m_PR_TAGNAME + "'");
                oStringBuilder.AppendLine("AND TIMESTAMP = (SELECT /*if_gather_realtime if_gather_realtime_pk*/");
                oStringBuilder.AppendLine("                        MAX(TIMESTAMP) FROM IF_GATHER_REALTIME WHERE TAGNAME = '" + m_PR_TAGNAME + "')");
                txtPres.Text = WaterNetCore.FormManager.StringInsertComma(Convert.ToString(m_oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null)));
            }
        }

        private void frmControl_Load(object sender, EventArgs e)
        {
            lblName.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            lblName.MouseMove += new MouseEventHandler(frmControl_MouseMove);
            lblPres.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //lblPres.MouseMove += new MouseEventHandler(frmControl_MouseMove);
            lblInput.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //lblInput.MouseMove += new MouseEventHandler(frmControl_MouseMove);
            lblSum.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //lblSum.MouseMove += new MouseEventHandler(frmControl_MouseMove);   
            lblInUnit.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //lblInUnit.MouseMove += new MouseEventHandler(frmControl_MouseMove); 
            lblSumUnit.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //lblSumUnit.MouseMove += new MouseEventHandler(frmControl_MouseMove);
            lblPresUnit.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //lblPresUnit.MouseMove += new MouseEventHandler(frmControl_MouseMove);          
            txtInput.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //txtInput.MouseMove += new MouseEventHandler(frmControl_MouseMove); 
            txtPres.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //txtPres.MouseMove += new MouseEventHandler(frmControl_MouseMove);
            txtSum.MouseDown += new MouseEventHandler(frmControl_MouseDown);
            //txtSum.MouseMove += new MouseEventHandler(frmControl_MouseMove);  
                
            try
            {
                m_oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                m_oDBManager.Open();
            }
            catch (Exception ex)
            {    
                throw ex;
            }
            Execute_QuerySelect();
            timer1.Start();
        }

        private void frmControl_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible) timer1.Start();
            else timer1.Stop();
	    }

        private void frmControl_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_oDBManager != null) m_oDBManager.Close();
        }

        private void tsMenuGraph_Click(object sender, EventArgs e)
        {
            Form oform = new frmFlowGraph();
            oform.Owner = this;
            ((frmFlowGraph)oform).varLOC_CODE = m_LOC_CODE;
            oform.ShowDialog();
        }

        private void frmControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                m_oldX = e.X;
                m_oldY = e.Y;
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (sender is Form)
                    contextMenuStrip1.Show(this, e.X, e.Y);
                else
                    contextMenuStrip1.Show(((Control)sender).Parent, e.X, e.Y);
            }
        }

        private void frmControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left = (int)(this.Left + e.X - m_oldX);
                this.Top = (int)(this.Top + e.Y - m_oldY);
            }
        }
    }
}
