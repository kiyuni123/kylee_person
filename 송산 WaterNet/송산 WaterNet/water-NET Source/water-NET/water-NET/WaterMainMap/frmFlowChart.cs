﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinGrid.ExcelExport;
using EMFrame.log;
#endregion

namespace WaterNet.WaterMainMap
{
    public partial class frmFlowGraph : Form
    {
        private string m_LOC_CODE;

        public frmFlowGraph()
        {
            InitializeComponent();
            InitializeSetting();
        }

        public string varLOC_CODE
        {
            //get { return m_LOC_CODE; }
            set { m_LOC_CODE = value; }
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LBLOCK";
            oUltraGridColumn.Header.Caption = "대블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.ValueList = GetValuelist_Block("BZ001");

            oUltraGridColumn = ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MBLOCK";
            oUltraGridColumn.Header.Caption = "중블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.ValueList = GetValuelist_Block("BZ002");

            oUltraGridColumn = ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SBLOCK";
            oUltraGridColumn.Header.Caption = "소블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.ValueList = GetValuelist_Block("BZ003");

            oUltraGridColumn = ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REL_LOC_NAME";
            oUltraGridColumn.Header.Caption = "배수지";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TIMESTAMP";
            oUltraGridColumn.Header.Caption = "계측시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "yyyy-MM-dd hh:mm";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 150;

            oUltraGridColumn = ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FRI";
            oUltraGridColumn.Header.Caption = "유량(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,###";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PRI";
            oUltraGridColumn.Header.Caption = "수압(㎏/㎠)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,###.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;
                                       
            WaterNet.WaterNetCore.FormManager.SetGridStyle(ultraGrid1);

            #endregion
            oStartTime.Value = DateTime.Now.AddDays(-1);
            oEndTime.Value = DateTime.Now;

            splitContainer1.Panel2Collapsed = true;
        }
        #endregion 초기화설정

        /// <summary>
        /// 블록 ValueList 설정 : 그리드 콤보박스
        /// </summary>
        private ValueList GetValuelist_Block(string blockcode)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT DISTINCT LOC_CODE AS CODE, LOC_NAME AS VALUE ");
            oStringBuilder.AppendLine("  FROM   CM_LOCATION                                ");
            oStringBuilder.AppendLine(" WHERE FTR_CODE = '" + blockcode.ToUpper() + "'");
            oStringBuilder.AppendLine(" ORDER BY LOC_NAME ASC  ");

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(oStringBuilder.ToString());

            return objValue;
        }

        private void InitializeChart()
        {
            chart1.LegendBox.Visible = true;
            chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            chart1.LegendBox.MarginY = 1;
            chart1.AxisX.Staggered = true;

            chart1.AxisX.DataFormat.Format = ChartFX.WinForms.AxisFormat.DateTime;
            chart1.AxisX.DataFormat.CustomFormat = "hh:MM";
        }

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            DataTable table = this.ultraGrid1.DataSource as DataTable;

            if (table == null)      return;

            this.chart1.Data.Series = 2;
            this.chart1.Data.Points = table.Rows.Count;

            string value1 = string.Empty;
            string value2 = string.Empty;

            this.chart1.Series[0].Gallery = ChartFX.WinForms.Gallery.Area;
            this.chart1.Series[1].AxisY = chart1.AxisY2;

            if (table.Columns.Contains("FRI") && table.Columns.Contains("PRI"))
            {
                value1 = "FRI";
                value2 = "PRI";

                this.chart1.AxesY[0].Title.Text = "유량(㎥/h)";
                this.chart1.AxesY[1].Title.Text = "수압(㎏/㎠)";
                this.chart1.Series[0].Text = "유량(㎥/h)";
                this.chart1.Series[1].Text = "수압(㎏/㎠)";
            }

            foreach (DataRow row in table.Rows)
            {
                int pointIndex = table.Rows.IndexOf(row);
                this.chart1.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("hh:mm");
                this.chart1.Data[0, pointIndex] = Convert.ToDouble(row[value1]);
                this.chart1.Data[1, pointIndex] = Convert.ToDouble(row[value2]);
            }

            chart1.Series[0].Visible = true;
            chart1.Series[1].Visible = true;
        }

        //배수지가 아닌 경우 검색
        private DataTable SelectFlowPatternStatus(OracleDBManager manager, string LOC_CODE, string startTime, string endTime)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                                                          ");
            query.AppendLine("(                                                                                                                                                                    ");
            query.AppendLine("select c1.loc_code                                                                                                                                                   ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))                                      ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                             ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                                                              ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                                                   ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                          ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                                                               ");
            query.AppendLine("      ,c1.ord                                                                                                                                                        ");
            query.AppendLine("  from                                                                                                                                                               ");
            query.AppendLine("      (                                                                                                                                                              ");
            query.AppendLine("       select sgccd                                                                                                                                                  ");
            query.AppendLine("             ,loc_code                                                                                                                                               ");
            query.AppendLine("             ,ploc_code                                                                                                                                              ");
            query.AppendLine("             ,loc_name                                                                                                                                               ");
            query.AppendLine("             ,ftr_idn                                                                                                                                                ");
            query.AppendLine("             ,ftr_code                                                                                                                                               ");
            query.AppendLine("             ,res_code                                                                                                                                               ");
            query.AppendLine("             ,kt_gbn                                                                                                                                                 ");
            query.AppendLine("             ,rel_loc_name                                                                                                                                           ");
            query.AppendLine("             ,rownum ord                                                                                                                                             ");
            query.AppendLine("         from cm_location                                                                                                                                            ");
            query.AppendLine("        where 1 = 1                                                                                                                                                  ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                                                   ");
            query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                                ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                                        ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                                    ");
            query.AppendLine("      ) c1                                                                                                                                                           ");
            query.AppendLine("       ,cm_location c2                                                                                                                                               ");
            query.AppendLine("       ,cm_location c3                                                                                                                                               ");
            query.AppendLine(" where 1 = 1                                                                                                                                                         ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                                 ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                                 ");
            query.AppendLine(" order by c1.ord                                                                                                                                                     ");
            query.AppendLine(")                                                                                                                                                                    ");
            query.AppendLine("select --loc_code                                                                                                                                                      ");
            query.AppendLine("      --,sgccd                                                                                                                                                         ");
            query.AppendLine("       lblock                                                                                                                                                        ");
            query.AppendLine("      ,mblock                                                                                                                                                        ");
            query.AppendLine("      ,sblock                                                                                                                                                        ");
            query.AppendLine("      ,rel_loc_name                                                                                                                                                  ");
            query.AppendLine("      ,timestamp                                                                                                                                                     ");
            query.AppendLine("      ,max(fri) fri                                                                                                                                                  ");
            query.AppendLine("      ,max(pri) pri                                                                                                                                                  ");
            query.AppendLine("  from                                                                                                                                                               ");
            query.AppendLine("    (                                                                                                                                                                ");
            query.AppendLine("    select iih.loc_code                                                                                                                                              ");
            query.AppendLine("          ,iih.sgccd                                                                                                                                                 ");
            query.AppendLine("          ,iih.lblock                                                                                                                                                ");
            query.AppendLine("          ,iih.mblock                                                                                                                                                ");
            query.AppendLine("          ,iih.sblock                                                                                                                                                ");
            query.AppendLine("          ,iih.rel_loc_name                                                                                                                                          ");
            query.AppendLine("          ,igr.tagname                                                                                                                                               ");
            query.AppendLine("          ,igr.timestamp                                                                                                                                             ");
            query.AppendLine("          ,iih.tag_gbn                                                                                                                                               ");
            query.AppendLine("          ,round(to_number(decode(iih.tag_gbn, 'FRI', igr.value)),2) fri                                                                                             ");
            query.AppendLine("          ,round(to_number(decode(iih.tag_gbn, 'PRI', igr.value)),2) pri                                                                                             ");
            query.AppendLine("      from                                                                                                                                                           ");
            query.AppendLine("        (                                                                                                                                                            ");
            query.AppendLine("        select loc.loc_code                                                                                                                                          ");
            query.AppendLine("              ,loc.sgccd                                                                                                                                             ");
            query.AppendLine("              ,loc.lblock                                                                                                                                            ");
            query.AppendLine("              ,loc.mblock                                                                                                                                            ");
            query.AppendLine("              ,loc.sblock                                                                                                                                            ");
            query.AppendLine("              ,loc.rel_loc_name                                                                                                                                      ");
            query.AppendLine("              ,iih.tagname                                                                                                                                           ");
            query.AppendLine("              ,itg.tag_gbn                                                                                                                                           ");
            query.AppendLine("          from loc                                                                                                                                                   ");
            query.AppendLine("              ,if_ihtags iih                                                                                                                                         ");
            query.AppendLine("              ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                         ");
            query.AppendLine("         where 1 = 1                                                                                                                                                 ");
            query.AppendLine("           and iih.loc_code = loc.tag_loc_code                                                                                                                       ");
            query.AppendLine("           and itg.tag_gbn in ('FRI', 'PRI')                                                                                                                         ");
            query.AppendLine("           and iih.tagname = itg.tagname                                                                       ");
            query.AppendLine("        ) iih                                                                                                                                                        ");
            query.AppendLine("          ,if_gather_realtime igr                                                                                                                                    ");
            query.AppendLine("     where igr.tagname = iih.tagname                                                                                                                                 ");
            query.AppendLine("       and igr.timestamp between to_date(:STARTDATE,'yyyymmddhh24mi') and to_date(:ENDDATE,'yyyymmddhh24mi')                                         ");
            query.AppendLine("    )                                                                                                                                                                ");
            query.AppendLine(" group by                                                                                                                                                            ");
            //query.AppendLine("       loc_code                                                                                                                                                      ");
            //query.AppendLine("      ,sgccd                                                                                                                                                         ");
            query.AppendLine("       lblock                                                                                                                                                        ");
            query.AppendLine("      ,mblock                                                                                                                                                        ");
            query.AppendLine("      ,sblock                                                                                                                                                        ");
            query.AppendLine("      ,rel_loc_name                                                                                                                                                  ");
            query.AppendLine("      ,timestamp                                                                                                                                                     ");
            query.AppendLine(" order by                                                                                                                                                            ");
            query.AppendLine("       timestamp                                                                                                                                                     ");

            IDataParameter[] parameters =  {
	                  new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = LOC_CODE;
            parameters[1].Value = startTime;
            parameters[2].Value = endTime;

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        private string GetNowDateTimeToString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int year = oDateTime.Year;
            int month = oDateTime.Month;
            int day = oDateTime.Day;
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = year.ToString();
            nowtime += month.ToString().PadLeft(2, '0');
            nowtime += day.ToString().PadLeft(2, '0');
            nowtime += hour.ToString().PadLeft(2, '0');
            nowtime += min.ToString().PadLeft(2, '0');

            return nowtime;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (((DateTime)oEndTime.Value) < ((DateTime)oStartTime.Value))
                {
                    MessageManager.ShowInformationMessage("시작일이 종료일보다 큽니다. 다시 입력하십시오.");
                    return;
                }

                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                OracleDBManager oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

                try
                {
                    oDBManager.Open();

                    ultraGrid1.DataSource = SelectFlowPatternStatus(oDBManager, m_LOC_CODE, GetNowDateTimeToString((DateTime)oStartTime.Value), GetNowDateTimeToString((DateTime)oEndTime.Value));

                    InitializeChartSetting(); //챠트그리기
                }
                catch (Exception oException)
                {
                    Console.WriteLine(oException.ToString());
                }
                finally
                {
                    this.Cursor = System.Windows.Forms.Cursors.Default;
                    if (oDBManager != null) oDBManager.Close();
                }            
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            

        }

        private void btnGraph_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1Collapsed = false;
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;
        }

        private void btnTable_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = false;
            splitContainer1.Panel1Collapsed = !splitContainer1.Panel1Collapsed;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmFlowGraph_Load(object sender, EventArgs e)
        {
            InitializeChart();

            btnSearch_Click(this, new EventArgs());
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                UltraGridExcelExporter exporter = new UltraGridExcelExporter();
                Infragistics.Excel.Workbook content = new Infragistics.Excel.Workbook();
                //Worksheet worksheet =
                //.AddWorksheet(this.chart1, "공급량수압트랜드현황", 0, 0, 9, 20);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //public void AddWorksheet(Chart chart, string worksheetName, int startRow, int startCell, int endRow, int endCell, WORKBOOK_TYPE type)
        //{
        //    Worksheet worksheet = null;

        //    //if (type == WORKBOOK_TYPE.CONTENT)
        //    //{
        //    //    worksheet = this.GetWorksheet(this.content, worksheetName);
        //    //}
        //    //if (type == WORKBOOK_TYPE.TEMPLATE)
        //    //{
        //    //    worksheet = this.GetWorksheet(this.template, worksheetName);
        //    //}

        //    //if (worksheet == null)
        //    //{
        //    //    return;
        //    //}

        //    //string tempFile = Utils.GetTempFileName("waternet-chart", FILE_TYPE.BITMAP);

        //    //chart.Export(FileFormat.Bitmap, tempFile);

        //    //if (!File.Exists(tempFile))
        //    //{
        //    //    return;
        //    //}

        //    //Stream stream = File.OpenRead(tempFile);
        //    //Image image = new Bitmap(stream);
        //    //WorksheetShape shape = new WorksheetImage(image);
        //    //shape.TopLeftCornerCell = worksheet.Rows[startRow].Cells[startCell];
        //    //shape.BottomRightCornerCell = worksheet.Rows[endRow].Cells[endCell];
        //    //worksheet.Shapes.Add(shape);
        //}
    }
}
