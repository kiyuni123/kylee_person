﻿namespace WaterNet.WaterMainMap
{
    partial class frmMainMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainMap));
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            this.SuspendLayout();
            // 
            // spcContents
            // 
            this.spcContents.Location = new System.Drawing.Point(24, 0);
            this.spcContents.Size = new System.Drawing.Size(948, 583);
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            this.axToolbar.Size = new System.Drawing.Size(685, 28);
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // _frmMapUnpinnedTabAreaRight
            // 
            this._frmMapUnpinnedTabAreaRight.Location = new System.Drawing.Point(972, 0);
            // 
            // _frmMapUnpinnedTabAreaTop
            // 
            this._frmMapUnpinnedTabAreaTop.Size = new System.Drawing.Size(948, 0);
            // 
            // _frmMapUnpinnedTabAreaBottom
            // 
            this._frmMapUnpinnedTabAreaBottom.Size = new System.Drawing.Size(948, 0);
            // 
            // _frmMapAutoHideControl
            // 
            this._frmMapAutoHideControl.Size = new System.Drawing.Size(11, 583);
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.TabIndex = 11;
            // 
            // spcTOC
            // 
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            this.tvBlock.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvBlock_AfterSelect);
            this.tvBlock.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvBlock_NodeMouseClick);
            this.tvBlock.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvBlock_BeforeSelect);
            this.tvBlock.Click += new System.EventHandler(this.tvBlock_Click);
            // 
            // tabPageTOC
            // 
            this.tabPageTOC.Size = new System.Drawing.Size(243, 376);
            // 
            // pnlLayerFunc
            // 
            this.pnlLayerFunc.Location = new System.Drawing.Point(0, 355);
            // 
            // spcIndexMap
            // 
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            this.axTOC.Size = new System.Drawing.Size(243, 355);
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(685, 524);
            this.axMap.OnExtentUpdated += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnExtentUpdatedEventHandler(this.axMap_OnExtentUpdated);
            // 
            // frmMainMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 583);
            this.Name = "frmMainMap";
            this.Text = "frmMainMap";
            this.Load += new System.EventHandler(this.frmMainMap_Load);
            this.SizeChanged += new System.EventHandler(this.frmMainMap_SizeChanged);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMainMap_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMainMap_FormClosing);
            this.ResizeEnd += new System.EventHandler(this.frmMainMap_ResizeEnd);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}