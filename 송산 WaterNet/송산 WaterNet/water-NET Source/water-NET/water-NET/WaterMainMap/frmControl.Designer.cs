﻿namespace WaterNet.WaterMainMap
{
    partial class frmControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblInput = new System.Windows.Forms.Label();
            this.lblSum = new System.Windows.Forms.Label();
            this.lblSumUnit = new System.Windows.Forms.Label();
            this.lblInUnit = new System.Windows.Forms.Label();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.txtSum = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblName = new System.Windows.Forms.Label();
            this.txtPres = new System.Windows.Forms.TextBox();
            this.lblPresUnit = new System.Windows.Forms.Label();
            this.lblPres = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsMenuGraph = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInput
            // 
            this.lblInput.BackColor = System.Drawing.Color.Transparent;
            this.lblInput.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInput.ForeColor = System.Drawing.Color.Red;
            this.lblInput.Location = new System.Drawing.Point(2, 19);
            this.lblInput.Name = "lblInput";
            this.lblInput.Size = new System.Drawing.Size(31, 12);
            this.lblInput.TabIndex = 0;
            this.lblInput.Text = "유량";
            // 
            // lblSum
            // 
            this.lblSum.BackColor = System.Drawing.Color.Transparent;
            this.lblSum.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSum.ForeColor = System.Drawing.Color.Red;
            this.lblSum.Location = new System.Drawing.Point(2, 52);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(31, 11);
            this.lblSum.TabIndex = 2;
            this.lblSum.Text = "적산";
            this.lblSum.UseCompatibleTextRendering = true;
            // 
            // lblSumUnit
            // 
            this.lblSumUnit.BackColor = System.Drawing.Color.Transparent;
            this.lblSumUnit.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSumUnit.ForeColor = System.Drawing.Color.Red;
            this.lblSumUnit.Location = new System.Drawing.Point(86, 53);
            this.lblSumUnit.Name = "lblSumUnit";
            this.lblSumUnit.Size = new System.Drawing.Size(31, 11);
            this.lblSumUnit.TabIndex = 5;
            this.lblSumUnit.Text = "㎥";
            // 
            // lblInUnit
            // 
            this.lblInUnit.BackColor = System.Drawing.Color.Transparent;
            this.lblInUnit.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInUnit.ForeColor = System.Drawing.Color.Red;
            this.lblInUnit.Location = new System.Drawing.Point(86, 19);
            this.lblInUnit.Name = "lblInUnit";
            this.lblInUnit.Size = new System.Drawing.Size(31, 11);
            this.lblInUnit.TabIndex = 3;
            this.lblInUnit.Text = "㎥/h";
            // 
            // txtInput
            // 
            this.txtInput.BackColor = System.Drawing.SystemColors.Window;
            this.txtInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInput.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtInput.ForeColor = System.Drawing.Color.Black;
            this.txtInput.Location = new System.Drawing.Point(30, 18);
            this.txtInput.Name = "txtInput";
            this.txtInput.ReadOnly = true;
            this.txtInput.Size = new System.Drawing.Size(52, 13);
            this.txtInput.TabIndex = 6;
            this.txtInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInput.WordWrap = false;
            // 
            // txtSum
            // 
            this.txtSum.BackColor = System.Drawing.SystemColors.Window;
            this.txtSum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSum.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtSum.ForeColor = System.Drawing.Color.Black;
            this.txtSum.Location = new System.Drawing.Point(30, 50);
            this.txtSum.Name = "txtSum";
            this.txtSum.ReadOnly = true;
            this.txtSum.Size = new System.Drawing.Size(52, 13);
            this.txtSum.TabIndex = 8;
            this.txtSum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSum.WordWrap = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(3, 3);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(122, 11);
            this.lblName.TabIndex = 9;
            this.lblName.Text = "123457890";
            // 
            // txtPres
            // 
            this.txtPres.BackColor = System.Drawing.SystemColors.Window;
            this.txtPres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPres.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtPres.ForeColor = System.Drawing.Color.Black;
            this.txtPres.Location = new System.Drawing.Point(30, 34);
            this.txtPres.Name = "txtPres";
            this.txtPres.ReadOnly = true;
            this.txtPres.Size = new System.Drawing.Size(52, 13);
            this.txtPres.TabIndex = 12;
            this.txtPres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPres.WordWrap = false;
            // 
            // lblPresUnit
            // 
            this.lblPresUnit.BackColor = System.Drawing.Color.Transparent;
            this.lblPresUnit.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPresUnit.ForeColor = System.Drawing.Color.Red;
            this.lblPresUnit.Location = new System.Drawing.Point(86, 36);
            this.lblPresUnit.Name = "lblPresUnit";
            this.lblPresUnit.Size = new System.Drawing.Size(31, 11);
            this.lblPresUnit.TabIndex = 11;
            this.lblPresUnit.Text = "㎏f/㎠";
            // 
            // lblPres
            // 
            this.lblPres.BackColor = System.Drawing.Color.Transparent;
            this.lblPres.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPres.ForeColor = System.Drawing.Color.Red;
            this.lblPres.Location = new System.Drawing.Point(2, 35);
            this.lblPres.Name = "lblPres";
            this.lblPres.Size = new System.Drawing.Size(31, 11);
            this.lblPres.TabIndex = 10;
            this.lblPres.Text = "수압";
            this.lblPres.UseCompatibleTextRendering = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuGraph});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(131, 26);
            // 
            // tsMenuGraph
            // 
            this.tsMenuGraph.Name = "tsMenuGraph";
            this.tsMenuGraph.Size = new System.Drawing.Size(130, 22);
            this.tsMenuGraph.Text = "그래프보기";
            this.tsMenuGraph.Click += new System.EventHandler(this.tsMenuGraph_Click);
            // 
            // frmControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(119, 67);
            this.ControlBox = false;
            this.Controls.Add(this.txtPres);
            this.Controls.Add(this.lblPresUnit);
            this.Controls.Add(this.lblPres);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtSum);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.lblSumUnit);
            this.Controls.Add(this.lblInUnit);
            this.Controls.Add(this.lblSum);
            this.Controls.Add(this.lblInput);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmControl";
            this.Text = "frmControl";
            this.TransparencyKey = System.Drawing.Color.RoyalBlue;
            this.Load += new System.EventHandler(this.frmControl_Load);
            this.VisibleChanged += new System.EventHandler(this.frmControl_VisibleChanged);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmControl_FormClosed);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmControl_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmControl_MouseMove);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInput;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label lblSumUnit;
        private System.Windows.Forms.Label lblInUnit;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TextBox txtSum;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtPres;
        private System.Windows.Forms.Label lblPresUnit;
        private System.Windows.Forms.Label lblPres;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsMenuGraph;
    }
}