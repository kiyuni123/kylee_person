﻿using System.Windows.Forms;

namespace WaterNet.WaterMainMap
{
    public partial class frmSmallBlockInfo : Form
    {
        public frmSmallBlockInfo()
        {
            InitializeComponent();

            InitializeSetting();
        }

        private void InitializeSetting()
        {

        }

        public void Open(string blockName)
        {
            this.Text = "소블록(" + blockName + ") 현황";

        }

        private void frmSmallBlockInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
        }
    }
}
