﻿namespace WaterNet.WaterMainMap
{
    partial class frmMiddleBlockInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // frmMiddleBlockInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 385);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMiddleBlockInfo";
            this.Text = "중블록 정보";
            this.Load += new System.EventHandler(this.frmMiddleBlockInfo_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMiddleBlockInfo_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

    }
}