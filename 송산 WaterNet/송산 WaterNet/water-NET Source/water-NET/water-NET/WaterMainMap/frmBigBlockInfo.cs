﻿using System;
using System.Windows.Forms;



#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WaterMainMap
{
    public partial class frmBigBlockInfo : Form
    {
        public frmBigBlockInfo()
        {
            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경 설정
        /// </summary>
        private void InitializeSetting()
        {
            UltraGridColumn oUltraGridColumn;

            #region - 관로정보 그리드
            oUltraGridColumn = ultraGridPipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD1";
            oUltraGridColumn.Header.Caption = "용도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ultraGridPipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD2";
            oUltraGridColumn.Header.Caption = "연장(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellDisplayStyle = CellDisplayStyle.FormattedText;
            oUltraGridColumn.Format = "###,###,##0";
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ultraGridPipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD3";
            oUltraGridColumn.Header.Caption = "개수(개)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellDisplayStyle = CellDisplayStyle.FormattedText;
            oUltraGridColumn.Format = "##,##0";
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(ultraGridPipe);
            #endregion          
            
            #region - 밸브정보 그리드
            oUltraGridColumn = ultraGridValv.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD1";
            oUltraGridColumn.Header.Caption = "용도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ultraGridValv.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD2";
            oUltraGridColumn.Header.Caption = "개수(개)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellDisplayStyle = CellDisplayStyle.FormattedText;
            oUltraGridColumn.Format = "##,##0";
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(ultraGridValv);
            #endregion   

            #region - 사업장정보 그리드
            oUltraGridColumn = ultraGridOffice.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD1";
            oUltraGridColumn.Header.Caption = "용도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ultraGridOffice.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD2";
            oUltraGridColumn.Header.Caption = "개수(개)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellDisplayStyle = CellDisplayStyle.FormattedText;
            oUltraGridColumn.Format = "##,##0";
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(ultraGridOffice);
            #endregion   

            #region - 시설물정보 그리드
            oUltraGridColumn = ultraGridFaci.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD1";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ultraGridFaci.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FIELD2";
            oUltraGridColumn.Header.Caption = "개수(개)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellDisplayStyle = CellDisplayStyle.FormattedText;
            oUltraGridColumn.Format = "##,##0";
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(ultraGridFaci);
            #endregion   

        }

        public void Open(string blockName)
        {
            this.Text = "대블록(" + blockName + ") 현황";

        }

        private void frmBigBlockInfo_Load(object sender, EventArgs e)
        {

        }

        private void frmBigBlockInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
        }

        private void frmBigBlockInfo_FormClosed(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show("삭제됨");
        }

    }
}
