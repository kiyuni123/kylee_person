﻿namespace WaterNet.WaterMainMap
{
    partial class frmBigBlockInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.panelPipe = new System.Windows.Forms.Panel();
            this.panelValv = new System.Windows.Forms.Panel();
            this.panelOffice = new System.Windows.Forms.Panel();
            this.panelFaci = new System.Windows.Forms.Panel();
            this.panelWater = new System.Windows.Forms.Panel();
            this.ultralblPipe = new Infragistics.Win.Misc.UltraLabel();
            this.ultralblValv = new Infragistics.Win.Misc.UltraLabel();
            this.ultralblOffice = new Infragistics.Win.Misc.UltraLabel();
            this.ultralblFaci = new Infragistics.Win.Misc.UltraLabel();
            this.ultralblWater = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGridPipe = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGridValv = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGridOffice = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGridFaci = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelPipe.SuspendLayout();
            this.panelValv.SuspendLayout();
            this.panelOffice.SuspendLayout();
            this.panelFaci.SuspendLayout();
            this.panelWater.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridPipe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridValv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridOffice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridFaci)).BeginInit();
            this.SuspendLayout();
            // 
            // panelPipe
            // 
            this.panelPipe.Controls.Add(this.ultraGridPipe);
            this.panelPipe.Controls.Add(this.ultralblPipe);
            this.panelPipe.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPipe.Location = new System.Drawing.Point(0, 0);
            this.panelPipe.Name = "panelPipe";
            this.panelPipe.Size = new System.Drawing.Size(298, 145);
            this.panelPipe.TabIndex = 0;
            // 
            // panelValv
            // 
            this.panelValv.Controls.Add(this.ultraGridValv);
            this.panelValv.Controls.Add(this.ultralblValv);
            this.panelValv.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelValv.Location = new System.Drawing.Point(0, 145);
            this.panelValv.Name = "panelValv";
            this.panelValv.Size = new System.Drawing.Size(298, 145);
            this.panelValv.TabIndex = 1;
            // 
            // panelOffice
            // 
            this.panelOffice.Controls.Add(this.ultraGridOffice);
            this.panelOffice.Controls.Add(this.ultralblOffice);
            this.panelOffice.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelOffice.Location = new System.Drawing.Point(0, 290);
            this.panelOffice.Name = "panelOffice";
            this.panelOffice.Size = new System.Drawing.Size(298, 145);
            this.panelOffice.TabIndex = 2;
            // 
            // panelFaci
            // 
            this.panelFaci.Controls.Add(this.ultraGridFaci);
            this.panelFaci.Controls.Add(this.ultralblFaci);
            this.panelFaci.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFaci.Location = new System.Drawing.Point(0, 435);
            this.panelFaci.Name = "panelFaci";
            this.panelFaci.Size = new System.Drawing.Size(298, 145);
            this.panelFaci.TabIndex = 3;
            // 
            // panelWater
            // 
            this.panelWater.Controls.Add(this.label2);
            this.panelWater.Controls.Add(this.label1);
            this.panelWater.Controls.Add(this.ultralblWater);
            this.panelWater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWater.Location = new System.Drawing.Point(0, 580);
            this.panelWater.Name = "panelWater";
            this.panelWater.Size = new System.Drawing.Size(298, 75);
            this.panelWater.TabIndex = 4;
            // 
            // ultralblPipe
            // 
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.ultralblPipe.Appearance = appearance1;
            this.ultralblPipe.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Raised;
            this.ultralblPipe.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            this.ultralblPipe.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultralblPipe.Location = new System.Drawing.Point(0, 0);
            this.ultralblPipe.Name = "ultralblPipe";
            this.ultralblPipe.Size = new System.Drawing.Size(298, 23);
            this.ultralblPipe.TabIndex = 0;
            this.ultralblPipe.Text = "관로정보";
            // 
            // ultralblValv
            // 
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.ultralblValv.Appearance = appearance3;
            this.ultralblValv.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Raised;
            this.ultralblValv.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            this.ultralblValv.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultralblValv.Location = new System.Drawing.Point(0, 0);
            this.ultralblValv.Name = "ultralblValv";
            this.ultralblValv.Size = new System.Drawing.Size(298, 23);
            this.ultralblValv.TabIndex = 1;
            this.ultralblValv.Text = "밸브정보";
            // 
            // ultralblOffice
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultralblOffice.Appearance = appearance4;
            this.ultralblOffice.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Raised;
            this.ultralblOffice.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            this.ultralblOffice.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultralblOffice.Location = new System.Drawing.Point(0, 0);
            this.ultralblOffice.Name = "ultralblOffice";
            this.ultralblOffice.Size = new System.Drawing.Size(298, 23);
            this.ultralblOffice.TabIndex = 2;
            this.ultralblOffice.Text = "사업장";
            // 
            // ultralblFaci
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultralblFaci.Appearance = appearance5;
            this.ultralblFaci.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Raised;
            this.ultralblFaci.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            this.ultralblFaci.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultralblFaci.Location = new System.Drawing.Point(0, 0);
            this.ultralblFaci.Name = "ultralblFaci";
            this.ultralblFaci.Size = new System.Drawing.Size(298, 23);
            this.ultralblFaci.TabIndex = 3;
            this.ultralblFaci.Text = "시설물";
            // 
            // ultralblWater
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultralblWater.Appearance = appearance2;
            this.ultralblWater.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Raised;
            this.ultralblWater.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            this.ultralblWater.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultralblWater.Location = new System.Drawing.Point(0, 0);
            this.ultralblWater.Name = "ultralblWater";
            this.ultralblWater.Size = new System.Drawing.Size(298, 23);
            this.ultralblWater.TabIndex = 4;
            this.ultralblWater.Text = "공급량/유수율";
            // 
            // ultraGridPipe
            // 
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridPipe.DisplayLayout.Appearance = appearance18;
            this.ultraGridPipe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridPipe.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridPipe.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridPipe.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.ultraGridPipe.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridPipe.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.ultraGridPipe.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridPipe.DisplayLayout.MaxRowScrollRegions = 1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridPipe.DisplayLayout.Override.ActiveCellAppearance = appearance22;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridPipe.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.ultraGridPipe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridPipe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridPipe.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridPipe.DisplayLayout.Override.CellAppearance = appearance25;
            this.ultraGridPipe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridPipe.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridPipe.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            appearance27.TextHAlignAsString = "Left";
            this.ultraGridPipe.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.ultraGridPipe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridPipe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridPipe.DisplayLayout.Override.RowAppearance = appearance28;
            this.ultraGridPipe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridPipe.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.ultraGridPipe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridPipe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridPipe.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGridPipe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridPipe.Location = new System.Drawing.Point(0, 23);
            this.ultraGridPipe.Name = "ultraGridPipe";
            this.ultraGridPipe.Size = new System.Drawing.Size(298, 122);
            this.ultraGridPipe.TabIndex = 1;
            this.ultraGridPipe.Text = "ultraGrid1";
            // 
            // ultraGridValv
            // 
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridValv.DisplayLayout.Appearance = appearance30;
            this.ultraGridValv.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridValv.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridValv.DisplayLayout.GroupByBox.Appearance = appearance31;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridValv.DisplayLayout.GroupByBox.BandLabelAppearance = appearance32;
            this.ultraGridValv.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance33.BackColor2 = System.Drawing.SystemColors.Control;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridValv.DisplayLayout.GroupByBox.PromptAppearance = appearance33;
            this.ultraGridValv.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridValv.DisplayLayout.MaxRowScrollRegions = 1;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridValv.DisplayLayout.Override.ActiveCellAppearance = appearance34;
            appearance35.BackColor = System.Drawing.SystemColors.Highlight;
            appearance35.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridValv.DisplayLayout.Override.ActiveRowAppearance = appearance35;
            this.ultraGridValv.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridValv.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridValv.DisplayLayout.Override.CardAreaAppearance = appearance36;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            appearance37.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridValv.DisplayLayout.Override.CellAppearance = appearance37;
            this.ultraGridValv.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridValv.DisplayLayout.Override.CellPadding = 0;
            appearance38.BackColor = System.Drawing.SystemColors.Control;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridValv.DisplayLayout.Override.GroupByRowAppearance = appearance38;
            appearance39.TextHAlignAsString = "Left";
            this.ultraGridValv.DisplayLayout.Override.HeaderAppearance = appearance39;
            this.ultraGridValv.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridValv.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridValv.DisplayLayout.Override.RowAppearance = appearance40;
            this.ultraGridValv.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridValv.DisplayLayout.Override.TemplateAddRowAppearance = appearance41;
            this.ultraGridValv.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridValv.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridValv.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGridValv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridValv.Location = new System.Drawing.Point(0, 23);
            this.ultraGridValv.Name = "ultraGridValv";
            this.ultraGridValv.Size = new System.Drawing.Size(298, 122);
            this.ultraGridValv.TabIndex = 2;
            this.ultraGridValv.Text = "ultraGrid1";
            // 
            // ultraGridOffice
            // 
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridOffice.DisplayLayout.Appearance = appearance42;
            this.ultraGridOffice.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridOffice.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridOffice.DisplayLayout.GroupByBox.Appearance = appearance43;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridOffice.DisplayLayout.GroupByBox.BandLabelAppearance = appearance44;
            this.ultraGridOffice.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance45.BackColor2 = System.Drawing.SystemColors.Control;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridOffice.DisplayLayout.GroupByBox.PromptAppearance = appearance45;
            this.ultraGridOffice.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridOffice.DisplayLayout.MaxRowScrollRegions = 1;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridOffice.DisplayLayout.Override.ActiveCellAppearance = appearance46;
            appearance47.BackColor = System.Drawing.SystemColors.Highlight;
            appearance47.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridOffice.DisplayLayout.Override.ActiveRowAppearance = appearance47;
            this.ultraGridOffice.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridOffice.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridOffice.DisplayLayout.Override.CardAreaAppearance = appearance48;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            appearance49.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridOffice.DisplayLayout.Override.CellAppearance = appearance49;
            this.ultraGridOffice.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridOffice.DisplayLayout.Override.CellPadding = 0;
            appearance50.BackColor = System.Drawing.SystemColors.Control;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridOffice.DisplayLayout.Override.GroupByRowAppearance = appearance50;
            appearance51.TextHAlignAsString = "Left";
            this.ultraGridOffice.DisplayLayout.Override.HeaderAppearance = appearance51;
            this.ultraGridOffice.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridOffice.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridOffice.DisplayLayout.Override.RowAppearance = appearance52;
            this.ultraGridOffice.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance53.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridOffice.DisplayLayout.Override.TemplateAddRowAppearance = appearance53;
            this.ultraGridOffice.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridOffice.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridOffice.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGridOffice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridOffice.Location = new System.Drawing.Point(0, 23);
            this.ultraGridOffice.Name = "ultraGridOffice";
            this.ultraGridOffice.Size = new System.Drawing.Size(298, 122);
            this.ultraGridOffice.TabIndex = 3;
            this.ultraGridOffice.Text = "ultraGrid1";
            // 
            // ultraGridFaci
            // 
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridFaci.DisplayLayout.Appearance = appearance6;
            this.ultraGridFaci.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridFaci.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridFaci.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridFaci.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.ultraGridFaci.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridFaci.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.ultraGridFaci.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridFaci.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridFaci.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridFaci.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.ultraGridFaci.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridFaci.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridFaci.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridFaci.DisplayLayout.Override.CellAppearance = appearance13;
            this.ultraGridFaci.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridFaci.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridFaci.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.ultraGridFaci.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.ultraGridFaci.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridFaci.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridFaci.DisplayLayout.Override.RowAppearance = appearance16;
            this.ultraGridFaci.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridFaci.DisplayLayout.Override.TemplateAddRowAppearance = appearance17;
            this.ultraGridFaci.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridFaci.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridFaci.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGridFaci.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridFaci.Location = new System.Drawing.Point(0, 23);
            this.ultraGridFaci.Name = "ultraGridFaci";
            this.ultraGridFaci.Size = new System.Drawing.Size(298, 122);
            this.ultraGridFaci.TabIndex = 4;
            this.ultraGridFaci.Text = "ultraGrid1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "label2";
            // 
            // frmBigBlockInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 655);
            this.Controls.Add(this.panelWater);
            this.Controls.Add(this.panelFaci);
            this.Controls.Add(this.panelOffice);
            this.Controls.Add(this.panelValv);
            this.Controls.Add(this.panelPipe);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBigBlockInfo";
            this.Text = "대블록 정보";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmBigBlockInfo_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBigBlockInfo_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBigBlockInfo_FormClosing);
            this.panelPipe.ResumeLayout(false);
            this.panelValv.ResumeLayout(false);
            this.panelOffice.ResumeLayout(false);
            this.panelFaci.ResumeLayout(false);
            this.panelWater.ResumeLayout(false);
            this.panelWater.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridPipe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridValv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridOffice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridFaci)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPipe;
        private System.Windows.Forms.Panel panelValv;
        private System.Windows.Forms.Panel panelOffice;
        private System.Windows.Forms.Panel panelFaci;
        private System.Windows.Forms.Panel panelWater;
        private Infragistics.Win.Misc.UltraLabel ultralblPipe;
        private Infragistics.Win.Misc.UltraLabel ultralblValv;
        private Infragistics.Win.Misc.UltraLabel ultralblOffice;
        private Infragistics.Win.Misc.UltraLabel ultralblFaci;
        private Infragistics.Win.Misc.UltraLabel ultralblWater;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridPipe;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridValv;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridOffice;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridFaci;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;

    }
}