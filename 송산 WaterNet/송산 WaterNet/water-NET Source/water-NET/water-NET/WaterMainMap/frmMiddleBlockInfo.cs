﻿using System;
using System.Windows.Forms;

namespace WaterNet.WaterMainMap
{
    public partial class frmMiddleBlockInfo : Form
    {
        public frmMiddleBlockInfo()
        {
            InitializeComponent();

            InitializeSetting();
        }

        private void InitializeSetting()
        {

        }

        public void Open(string blockName)
        {
            this.Text = "중블록(" + blockName + ") 현황";

        }

        private void frmMiddleBlockInfo_Load(object sender, EventArgs e)
        {

        }

        private void frmMiddleBlockInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
        }
    }
}
