﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WQ_ReChlorine
{
    /// <summary>
    /// Project ID : WN_WQ_A04
    /// Project Explain : 재염소지점 및 농도 관리, 기본적으로 WaterAOCore.frmMap을 사용
    /// Project Developer : 오두석
    /// Project Create Date : 2010.10.13
    /// Form Explain : 재염소지점 및 농도 관리 Main Form
    /// </summary>
    public partial class frmWQMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        ILayer m_Layer = null; //재염소지점 레이어

        public frmWQMain()
        {
            WQ_AppStatic.IS_SHOW_FORM_RC_PREDICBASE = false;

            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버 (메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.)

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();

            #region - WQ_AppStatic에 필요한 Object Set
            //Database Setting
            WQ_AppStatic.ORACLE_MANAGER = new OracleDBManager();
            WQ_AppStatic.ORACLE_MANAGER.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            WQ_AppStatic.ORACLE_MANAGER.Open();
            if (WQ_AppStatic.ORACLE_MANAGER == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다. 확인 후 다시 시작하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            //IMAP Set
            WQ_AppStatic.IMAP = (IMapControl3)axMap.Object;
            WQ_AppStatic.ITOC = (ITOCControl2)axTOC.Object;

            #endregion

            m_Layer = WaterAOCore.ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_Pipegraphic, "재염소처리지점");
            axMap.AddLayer(m_Layer);
            WaterAOCore.ArcManager.SetDefaultRenderer(axMap.ActiveView.FocusMap, (IFeatureLayer)m_Layer);
            m_Layer.Visible = true;

            this.SetLayerFromDB();

            FormPopup.frmWQPRCPredictBase oForm = new FormPopup.frmWQPRCPredictBase();
            oForm.TopLevel = false;
            oForm.Parent = this;
            oForm.StartPosition = FormStartPosition.Manual;
            oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
            oForm.BringToFront();
            oForm.Show();
        }

        #region Button Events
        
        private void btnPredictBase_Click(object sender, EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_RC_PREDICBASE == false)
            {
                FormPopup.frmWQPRCPredictBase oForm = new FormPopup.frmWQPRCPredictBase();
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        #endregion


        #region Control Events

        /// <summary>
        /// Map에서 오른쪽 마우스 버튼 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            ///*****************************************************************************
            ///GIS MAP에서 Mouse 다운이 일어난 위치
            ///----------------------------------------------------------------------------S
            ///GIS 상 좌표
            WQ_AppStatic.IMAP_X = e.mapX;
            WQ_AppStatic.IMAP_Y = e.mapY;
            ///Windows 상 좌표
            WQ_AppStatic.WIN_X = e.x;
            WQ_AppStatic.WIN_Y = e.y;
            ///----------------------------------------------------------------------------E

            WQ_AppStatic.CURRENT_IGEOMETRY = null;

            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;

            //#region 위치선택 - 지도에 사고지점을 선택하고, 별표 표시

            /////기존 객체 및 환경 초기화
            //ArcManager.ClearSelection(axMap.ActiveView.FocusMap);
            //IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
            //pGraphicsContainer.DeleteAllElements();

            //IActiveView activeView = axMap.ActiveView;
            //IScreenDisplay screenDisplay = activeView.ScreenDisplay;
            //IColor pColor = ArcManager.GetColor(255, 0, 0);

            //ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
            //IRubberBand pRubberBand = new RubberPointClass();
            //IPoint pSearchPoint = (IPoint)pRubberBand.TrackNew(screenDisplay, pSymbol);

            //if (pSearchPoint == null | pSearchPoint.IsEmpty) return;

            //IElement pElement = new MarkerElementClass();
            //pElement.Geometry = pSearchPoint as IGeometry;
            //IMarkerElement pMarkerElement = pElement as IMarkerElement;
            //pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;

            //pGraphicsContainer.AddElement(pElement, 0);
            //ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
            //Application.DoEvents();
            //ArcManager.FlashShape(axMap.ActiveView, pSearchPoint as IPoint, 3);

            //#endregion 지도에 사고지점을 선택하고, 별표 표시 끝

            //IPoint pPoint = new PointClass();
            //pPoint.X = WQ_AppStatic.IMAP_X;
            //pPoint.Y = WQ_AppStatic.IMAP_Y;
            //pPoint.Z = 0;

            //IGeometry pGeom = (IGeometry)pPoint;

            //WQ_AppStatic.CURRENT_IGEOMETRY = pGeom;

            switch (e.button)
            {
                case 1:    //왼쪽버튼
                    break;

                case 2:    //오른쪽버튼
                    //ContextMenuStripPopup.Show(axMap, WQ_AppStatic.WIN_X, WQ_AppStatic.WIN_Y);
                    break;
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 지점정보를 취득해 레이어에 정보를 SET한다.
        /// </summary>
        private void SetLayerFromDB()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            ArcManager.DeleteAllFeatures(m_Layer);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.SBLOCK_CODE, A.ID, A.MAP_X, A.MAP_Y");
            oStringBuilder.AppendLine("FROM      WQ_RECHLORINE_POINT A");
            oStringBuilder.AppendLine("ORDER BY  A.SBLOCK_CODE, A.ID");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "SET_LAYER");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {

                IWorkspaceEdit pWorkspaceEdit = ArcManager.getWorkspaceEdit(m_Layer as IFeatureLayer);
                if (pWorkspaceEdit == null) return;

                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureBuffer pFeatureBuffer = ((IFeatureLayer)m_Layer).FeatureClass.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = ((IFeatureLayer)m_Layer).FeatureClass.Insert(true);
                    IFeature pFeaturebuffer = pFeatureBuffer as IFeature;

                    comReleaser.ManageLifetime(pFeatureCursor);

                    IPoint pPoint = new PointClass();

                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        //레이어의 지점 좌표
                        pPoint.PutCoords(Convert.ToDouble(oDRow["MAP_X"].ToString()), Convert.ToDouble(oDRow["MAP_Y"].ToString()));

                        //레이어의 지점 데이터
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("SBLOCK"), (object)oDRow["SBLOCK_CODE"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("NODE_ID"), (object)oDRow["ID"].ToString());

                        //레이어의 지점 좌표로 IGeometry
                        pFeaturebuffer.Shape = pPoint as IGeometry;

                        pFeatureCursor.InsertFeature(pFeatureBuffer);
                    }
                    pFeatureCursor.Flush();
                }

                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
            }

            ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();

            pDS.Dispose();
        }

        #endregion
    }
}
