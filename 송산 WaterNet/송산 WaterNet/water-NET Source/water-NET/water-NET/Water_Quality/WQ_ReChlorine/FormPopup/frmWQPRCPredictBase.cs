﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using System.IO;

using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;
using WaterNet.WH_INPManage;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_ReChlorine.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A04
    /// Project Explain : 재염소지점 및 농도 관리
    /// Project Developer : 오두석
    /// Project Create Date : 2010.10.27
    /// Form Explain : 소독능 부족지역(예측기반) Popup Form
    /// </summary>
    public partial class frmWQPRCPredictBase : Form
    {
        /// <summary>
        /// 관망해석 : INP_NO
        /// </summary>
        private string m_INP_NO = string.Empty;

        private Hashtable m_MODEL_LAYERS = new Hashtable();

        public frmWQPRCPredictBase()
        {
            InitializeComponent();

            WQ_AppStatic.IS_SHOW_FORM_RC_PREDICBASE = true;

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region Grid Set

            #region uGridPoint1 : 관망해석결과 : 잔류염소 Grid

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "해석모델";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "절점 ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SELECT_DATE";
            oUltraGridColumn.Header.Caption = "선정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPoint1);

            #endregion

            #endregion               
        }

        private void frmWQPRCPredictBase_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================잔류염소예측(잔류염소 예측 및 재염소 지점관리)

            object o = EMFrame.statics.AppStatic.USER_MENU["잔류염소예측ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            //===========================================================================

            WQ_Function.SetCombo_LargeBlock(this.cboLBlock1, EMFrame.statics.AppStatic.USER_SGCCD);
        }

        private void frmWQPRCPredictBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_RC_PREDICBASE = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetReChlorinePointData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnRunEPANET_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.LoadAnalysisLayer(this.txtINPNO.Text);

                this.ClearLayerDatas();

                ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();

                string strFilePath = string.Empty;

                strFilePath = Application.StartupPath + @"\Epanet2w.exe";

                this.StartInstallationProcess(strFilePath, "EPANET");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.txtJunctionID.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() != true) return;

            ILayer pLayer = WaterAOCore.ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "JUNCTION");

            IFeature pFeature = WaterAOCore.ArcManager.GetFeature(pLayer, "ID='" + this.txtJunctionID.Text.Trim() + "'");

            if (pFeature != null)
            {
                IGeometry oIGeometry = pFeature.Shape;

                WQ_AppStatic.CURRENT_IGEOMETRY = oIGeometry;

                if (ArcManager.GetMapScale(WQ_AppStatic.IMAP) > (double)500)
                {
                    ArcManager.SetMapScale(WQ_AppStatic.IMAP, (double)500);
                }

                ArcManager.MoveCenterAt(WQ_AppStatic.IMAP, pFeature);
                Application.DoEvents();

                this.SaveReChlorinePointOnDB();

                this.SaveReChlorinePointOnLayer();
            }
            else
            {
                MessageBox.Show("올바른 재염소 지점의 절점 ID를 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.txtINPNO.Text = m_INP_NO;
            this.txtJunctionID.Text = "";

            MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.GetReChlorinePointData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() != true) return;

            this.DeleteReChlorinePointOnDB();

            this.DeleteReChlorinePointOnLayer();

            this.txtINPNO.Text = m_INP_NO;
            this.txtJunctionID.Text = "";

            MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.GetReChlorinePointData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void cboLBlock1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock1.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock1, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock1.Text);

            WQ_Function.SetCombo_SmallBlock(this.cboSBlockM, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 0);

            string strINP_NO = this.GetINPNumber(strMFTRIDN);
            if (strINP_NO == "")
            {
                //MessageBox.Show("선택하신 중블록에는 관망해석모델이 없습니다. 확인 후 해석을 실행하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.btnRunEPANET.Enabled = false;
            }
            else
            {
                //this.LoadAnalysisLayer(strINP_NO);
                this.btnRunEPANET.Enabled = true;
            }

            this.txtINPNO.Text = strINP_NO;
        }

        private void uGridPoint1_Click(object sender, EventArgs e)
        {
            if (this.uGridPoint1.Rows.Count <= 0) return;

            string strSBLOCK = this.uGridPoint1.ActiveRow.Cells[0].Text;
            string strMBLOCKCD = WQ_Function.GetMediumBlockBySmallBlockCode(WQ_Function.SplitToCode(strSBLOCK));
            string strMBLOCK = WQ_Function.GetBlockNameCodeByFTR_IDN(WQ_Function.GetMediumBlockFTR_IDN(strMBLOCKCD));
            WQ_Function.SetCombo_SelectFindData(this.cboMBlock1, strMBLOCK);
            WQ_Function.SetCombo_SelectFindData(this.cboSBlockM, strSBLOCK);

            this.txtINPNO.Text = this.uGridPoint1.ActiveRow.Cells[1].Text;
            string strNODE_ID = this.uGridPoint1.ActiveRow.Cells[2].Text;
            this.txtJunctionID.Text = strNODE_ID;
        }

        private void uGridPoint1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPoint1.Rows.Count <= 0) return;

            this.Viewmap_SelectedPoint(this.uGridPoint1.ActiveRow.Cells[2].Text);
        }

        #endregion


        #region User Function

        /// <summary>
        /// 중블록 코드로 INP Number를 취득한다.
        /// </summary>
        /// <param name="strMFTRIDN"></param>
        /// <returns></returns>
        private string GetINPNumber(string strMFTRIDN)
        {
            string strRTN = string.Empty;

            if (WQ_AppStatic.ORACLE_MANAGER == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return string.Empty;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   INP_NUMBER AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WH_TITLE");
            oStringBuilder.AppendLine("WHERE    MFTRIDN = '" + strMFTRIDN + "' AND USE_GBN = 'WQ' AND ROWNUM = 1");
            oStringBuilder.AppendLine("ORDER BY INS_DATE DESC");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WH_TITLE");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["RTNDATA"].ToString().Trim();
                    break;
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            return strRTN;
        }

        #region - SQL Function (조회 저장 삭제)

        /// <summary>
        /// 재염소지점 Data를 Select해서 Grid에 Set
        /// </summary>
        private void GetReChlorinePointData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSFTRIDN = string.Empty;
            string strSQL = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlockM.Text);
            if (strSFTRIDN.Trim() != "")
            {
                strSQL = "A.SBLOCK_CODE= '" + strSFTRIDN + "' AND";
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);

            oStringBuilder.AppendLine("SELECT   B.LOC_NAME || ' (' || A.SBLOCK_CODE || ')' AS BLOCK, A.INP_NUMBER, A.ID, TO_CHAR(TO_DATE(A.SELECT_DATE, 'RRRR-MM-DD'), 'RRRR-MM-DD') AS SELECT_DATE");
            oStringBuilder.AppendLine("FROM     WQ_RECHLORINE_POINT A, CM_LOCATION B");
            oStringBuilder.AppendLine("WHERE    " + strSQL + " B.LOC_CODE = A.SBLOCK_CODE ");
            oStringBuilder.AppendLine("ORDER BY A.ID");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_RECHLORINE_POINT");

            this.uGridPoint1.DataSource = pDS.Tables["WQ_RECHLORINE_POINT"].DefaultView;

            FormManager.SetGridStyle_PerformAutoResize(this.uGridPoint1);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 재염소지점 Data를 DB에 저장
        /// </summary>
        private void SaveReChlorinePointOnDB()
        {
            string strSBLOCK = string.Empty;
            string strToDay = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            strSBLOCK = WQ_Function.SplitToCode(this.cboSBlockM.Text);
            strToDay = WQ_Function.StringToDateTime(DateTime.Now);

            IPoint pPoint = new PointClass();
            pPoint = (IPoint)WQ_AppStatic.CURRENT_IGEOMETRY;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO  WQ_RECHLORINE_POINT");
            oStringBuilder.AppendLine("(SBLOCK_CODE, INP_NUMBER, ID, SELECT_DATE, MAP_X, MAP_Y)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("('" + strSBLOCK + "', '" + this.txtINPNO.Text + "', '" + this.txtJunctionID.Text + "', '" + strToDay + "', '" + pPoint.X.ToString() + "', '" + pPoint.Y.ToString() + "')");

            WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 재염소지점 Data를 DB에서 삭제
        /// </summary>
        private void DeleteReChlorinePointOnDB()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strBlock = WQ_Function.SplitToCode(this.cboSBlockM.Text);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE");
            oStringBuilder.AppendLine("FROM     WQ_RECHLORINE_POINT");
            oStringBuilder.AppendLine("WHERE    ID = '" + this.txtJunctionID.Text + "' AND SBLOCK_CODE = '" + strBlock + "'");

            WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        #endregion

        #region - ETC Fuction

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_RC_PREDICBASE = false;
            this.Dispose();
            this.Close();
        }

        /// <summary>
        /// 설치프로그램을 실행하고 설치프로그램이 종료될 때까지 Wait한다.
        /// </summary>
        /// <param name="strFilePath">설치프로그램의 Full Path</param>
        /// <param name="strFilePath">설치프로그램의 Name</param>
        private void StartInstallationProcess(string strFilePath, string strPGMName)
        {
            if (File.Exists(strFilePath) != true)
            {
                MessageBox.Show(strPGMName + " 프로그램 파일이 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            ProcessStartInfo pStartInfo = new ProcessStartInfo();
            pStartInfo.FileName = strFilePath;

            string strMFTR_IDN = WQ_Function.SplitToCode(this.cboMBlock1.Text);
            string strINP_NO = this.GetINPNumber(strMFTR_IDN);
            string strINP_PATH = WQ_AppStatic.INP_PATH + strINP_NO + ".INP";

            if (FunctionManager.IsDirectoryExists(WQ_AppStatic.INP_PATH) != true) FunctionManager.CreateDirectory(WQ_AppStatic.INP_PATH);
            
            Hashtable conditions = new Hashtable();

            //EPANET K용 INP파일
            //conditions.Add("type", "K");
            conditions.Add("type", string.Empty);
            conditions.Add("INP_NUMBER", strINP_NO);
            conditions.Add("fileName", strINP_PATH);
            
            WH_INPManage.work.INPManagetWork work = WH_INPManage.work.INPManagetWork.GetInstance();
            work.GenerateINPFile(conditions);

            pStartInfo.Arguments = string.Format(" " + strINP_PATH + "{0}", "");

            Process Proc = new Process();

            Proc.StartInfo = pStartInfo;

            Proc.Start();

            Proc.Dispose();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 저장하는 Data를 검증한다.
        /// </summary>
        /// <returns>True / False</returns>
        private bool IsValidation()
        {
            bool blRtn = true;

            if (this.cboSBlockM.Text == "") blRtn = false;
            if (this.txtINPNO.Text.Trim() == "") blRtn = false;
            if (this.txtJunctionID.Text.Trim() == "") blRtn = false;

            if (blRtn == false)
            {
                MessageBox.Show("재염소지점 정보를 입력해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return blRtn;
        }

        #endregion

        #endregion


        #region Map Function

        /// <summary>
        /// 그리드에서 선택된 재염소지점 View
        /// </summary>
        /// <param name="strNODE_ID"></param>
        private void Viewmap_SelectedPoint(string strNODE_ID)
        {
            if (WQ_AppStatic.IMAP == null) return;

            ILayer pLayer = WaterAOCore.ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "재염소처리지점");

            IFeature pFeature = ArcManager.GetFeature(pLayer, "NODE_ID='" + strNODE_ID + "'");

            if (pFeature == null) return;

            if (ArcManager.GetMapScale(WQ_AppStatic.IMAP) > (double)500)
            {
                ArcManager.SetMapScale(WQ_AppStatic.IMAP, (double)500);
            }

            ArcManager.MoveCenterAt(WQ_AppStatic.IMAP, pFeature);
            Application.DoEvents();

            //WQ_AppStatic.IMAP.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);

            ArcManager.FlashShape(WQ_AppStatic.IMAP.ActiveView, (IGeometry)pFeature.Shape, 10);

            ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();
        }

        /// <summary>
        /// 재염소지점을 Layer에 저장
        /// </summary>
        private void SaveReChlorinePointOnLayer()
        {
            string strLAYER_NAME = "재염소처리지점";

            if (WQ_AppStatic.IMAP != null && WQ_AppStatic.CURRENT_IGEOMETRY != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strLAYER_NAME);

                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeature pFeature = pFeatureClass.CreateFeature();

                //Shpae에 지점 등록
                pFeature.Shape = WQ_AppStatic.CURRENT_IGEOMETRY;

                //등록된 지점에 밸류 등록
                ArcManager.SetValue(pFeature, "SBLOCK", (object)WQ_Function.SplitToCode(this.cboSBlockM.Text));
                ArcManager.SetValue(pFeature, "NODE_ID", (object)this.txtJunctionID.Text);

                //Shape 저장
                pFeature.Store();

                //GIS Map Refresh
                //WQ_AppStatic.ITOC.Update();
                ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();
            }
        }

        /// <summary>
        /// 재염소지점을 Layer에서 삭제
        /// </summary>
        private void DeleteReChlorinePointOnLayer()
        {
            string strLAYER_NAME = "재염소처리지점";

            if (WQ_AppStatic.IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strLAYER_NAME);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "SBLOCK = '" + WQ_Function.SplitToCode(this.cboSBlockM.Text) + "' AND NODE_ID='" + this.txtJunctionID.Text + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    pFeature.Delete();

                    //GIS Map Refresh
                    //WQ_AppStatic.ITOC.Update();
                    ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                    Application.DoEvents();
                }

                this.BringToFront();
            }
        }

        /// <summary>
        /// INP NUMBER에 해당하는 LAYER를 MAP에 등록
        /// </summary>
        /// <param name="strINP_NUM"></param>
        private void LoadAnalysisLayer(string strINP_NUM)
        {
            try
            {
                //INP Number로 Shape을 생성
                IWorkspace pWorkspace = ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + strINP_NUM);
                if (pWorkspace == null)
                {
                    CreateINPLayerManager oINPLayer = new CreateINPLayerManager(strINP_NUM);
                    try
                    {
                        oINPLayer.StartEditor();
                        oINPLayer.CreateINP_Shape();
                        oINPLayer.StopEditor(true);
                    }
                    catch
                    {
                        oINPLayer.AbortEditor();
                        oINPLayer.StopEditor(false);
                    }
                }

                WaterAOCore.VariableManager.m_INPgraphic = WaterAOCore.ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + strINP_NUM);

                if (WaterAOCore.VariableManager.m_INPgraphic != null)
                {
                    this.RemoveAnalysisLayer();

                    ILayer pJTLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "JUNCTION", "JUNCTION");
                    ILayer pPPLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PIPE", "PIPE");
                    ILayer pPMLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PUMP", "PUMP");
                    ILayer pRRLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "RESERVOIR", "RESERVOIR");
                    ILayer pTNLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "TANK", "TANK");
                    ILayer pVVLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "VALVE", "VALVE");

                    IColor pLabelColor = null; ISymbol pMarkSymbol = null; ISymbol pLineSymbol = null;

                    if (pJTLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(System.Drawing.Color.DarkBlue.R, System.Drawing.Color.DarkBlue.G, System.Drawing.Color.DarkBlue.B);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pJTLayer, "EN_QUALITY", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(6, esriSimpleMarkerStyle.esriSMSCircle, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pJTLayer, pMarkSymbol);
                        WQ_AppStatic.IMAP.AddLayer(pJTLayer, 0);
                    }
                    if (pPPLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 110);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pPPLayer, "EN_FLOW", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pPPLayer, pLineSymbol);
                        WQ_AppStatic.IMAP.AddLayer(pPPLayer, 1);
                    }
                    if (pPMLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 222);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pPMLayer, "EN_FLOW", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pPMLayer, pLineSymbol);
                        WQ_AppStatic.IMAP.AddLayer(pPMLayer, 2);
                    }
                    if (pRRLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(System.Drawing.Color.DarkTurquoise.R, System.Drawing.Color.DarkTurquoise.G, System.Drawing.Color.DarkTurquoise.B);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pRRLayer, "EN_QUALITY", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(6, esriSimpleMarkerStyle.esriSMSDiamond, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pRRLayer, pMarkSymbol);
                        WQ_AppStatic.IMAP.AddLayer(pRRLayer, 3);
                    }
                    if (pTNLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(System.Drawing.Color.DarkRed.R, System.Drawing.Color.DarkRed.G, System.Drawing.Color.DarkRed.B);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pTNLayer, "EN_QUALITY", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(6, esriSimpleMarkerStyle.esriSMSSquare, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pTNLayer, pMarkSymbol);
                        WQ_AppStatic.IMAP.AddLayer(pTNLayer, 4);
                    }
                    if (pVVLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 200, 0);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pVVLayer, "EN_FLOW", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pVVLayer, pLineSymbol);
                        WQ_AppStatic.IMAP.AddLayer(pVVLayer, 5);
                    }
                    this.m_MODEL_LAYERS.Clear();

                    this.m_MODEL_LAYERS.Add("junction", (ITable)pJTLayer);
                    this.m_MODEL_LAYERS.Add("pipe", (ITable)pPPLayer);
                    this.m_MODEL_LAYERS.Add("pump", (ITable)pPMLayer);
                    this.m_MODEL_LAYERS.Add("reservoir", (ITable)pRRLayer);
                    this.m_MODEL_LAYERS.Add("tank", (ITable)pTNLayer);
                    this.m_MODEL_LAYERS.Add("valve", (ITable)pVVLayer);

                    //this.ClearLayerDatas();
                }

                //WQ_AppStatic.IMAP.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, null);
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
            }
        }

        /// <summary>
        /// 이미 등록된 INP_NO의 레이어를 리무브
        /// </summary>
        private void RemoveAnalysisLayer()
        {
            ArcManager.RemoveMapLayer(WQ_AppStatic.IMAP, "JUNCTION");
            ArcManager.RemoveMapLayer(WQ_AppStatic.IMAP, "PIPE");
            ArcManager.RemoveMapLayer(WQ_AppStatic.IMAP, "PUMP");
            ArcManager.RemoveMapLayer(WQ_AppStatic.IMAP, "RESERVOIR");
            ArcManager.RemoveMapLayer(WQ_AppStatic.IMAP, "TANK");
            ArcManager.RemoveMapLayer(WQ_AppStatic.IMAP, "VALVE");
        }

        /// <summary>
        /// 지도상의 INP Layer의 실시간 감시항목 데이터를 전부 초기화한다.
        /// </summary>
        private void ClearLayerDatas()
        {
            try
            {
                DataSet junctionList = this.SelectJunctionList();
                DataSet reservoirList = this.SelectReservoirList();
                DataSet tankList = this.SelectTankList();
                DataSet pipeList = this.SelectPipeList();
                DataSet pumpList = this.SelectPumpList();
                DataSet valveList = this.SelectValveList();

                ITable junctionTable = (ITable)this.m_MODEL_LAYERS["junction"];
                ITable tankTable = (ITable)this.m_MODEL_LAYERS["tank"];
                ITable reservoirTable = (ITable)this.m_MODEL_LAYERS["reservoir"];
                ITable pipeTable = (ITable)this.m_MODEL_LAYERS["pipe"];
                ITable pumpTable = (ITable)this.m_MODEL_LAYERS["pump"];
                ITable valveTable = (ITable)this.m_MODEL_LAYERS["valve"];

                UpdateNodeData(junctionTable, null, junctionList.Tables["WH_JUNCTIONS"], true);
                UpdateNodeData(tankTable, null, tankList.Tables["WH_TANKS"], true);
                UpdateNodeData(reservoirTable, null, reservoirList.Tables["WH_RESERVOIRS"], true);

                UpdateLinkData(pipeTable, null, pipeList.Tables["WH_PIPES"], true);
                UpdateLinkData(pumpTable, null, pumpList.Tables["WH_PUMPS"], true);
                UpdateLinkData(valveTable, null, valveList.Tables["WH_VALVES"], true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
        }

        /// <summary>
        /// INP 파일에 해당하는 Junction List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectJunctionList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("SELECT      a.ID				                                ");
            queryString.AppendLine("            ,a.ELEV				                                ");
            queryString.AppendLine("            ,a.DEMAND				                            ");
            queryString.AppendLine("            ,a.PATTERN_ID			                            ");
            queryString.AppendLine("            ,a.SFTRIDN			                                ");
            queryString.AppendLine("            ,b.LOC_NAME             AS SFTR_NAME			    ");
            queryString.AppendLine("            ,a.REMARK				                            ");
            queryString.AppendLine("            ,''                     AS ACTION                   ");
            queryString.AppendLine("FROM        WH_JUNCTIONS            a		                    ");
            queryString.AppendLine("            ,CM_LOCATION            b                           ");
            queryString.AppendLine("WHERE       b.FTR_CODE(+)       = 'BZ003'                       ");
            queryString.AppendLine("            AND a.SFTRIDN       = b.FTR_IDN(+)                  ");
            queryString.AppendLine("            AND a.INP_NUMBER    = '" + this.m_INP_NO + "'            ");
            queryString.AppendLine("ORDER BY    a.ID				                                    ");

            return WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(queryString.ToString(), null, "WH_JUNCTIONS");
        }

        /// <summary>
        /// INP 파일에 해당하는 Reservoir List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectReservoirList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,HEAD                                                   ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_RESERVOIRS                                           ");
            queryString.AppendLine("where       INP_NUMBER      = '" + this.m_INP_NO + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RESERVOIRS");
        }

        /// <summary>
        /// INP 파일에 해당하는 Tank List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectTankList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,ELEV                                                   ");
            queryString.AppendLine("            ,INITLVL                                                ");
            queryString.AppendLine("            ,MINLVL                                                 ");
            queryString.AppendLine("            ,MAXLVL                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,MINVOL                                                 ");
            queryString.AppendLine("            ,VOLCURVE_ID                                            ");
            queryString.AppendLine("from        WH_TANK                                                 ");
            queryString.AppendLine("where       INP_NUMBER      = '" + this.m_INP_NO + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TANKS");
        }

        /// <summary>
        /// INP 파일에 해당하는 Pipe List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectPipeList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,LENGTH                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,ROUGHNESS                                              ");
            queryString.AppendLine("            ,MLOSS                                                  ");
            queryString.AppendLine("            ,STATUS                                                 ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("            ,LEAKAGE_COEFFICIENT                                    ");
            queryString.AppendLine("from        WH_PIPES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + this.m_INP_NO + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PIPES");
        }

        /// <summary>
        /// INP 파일에 해당하는 Pump List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectPumpList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,PROPERTIES                                             ");
            queryString.AppendLine("from        WH_PUMPS                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + this.m_INP_NO + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PUMPS");
        }

        /// <summary>
        /// INP 파일에 해당하는 Valve List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectValveList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,DIAMETER                                               ");
            queryString.AppendLine("            ,TYPE                                                   ");
            queryString.AppendLine("            ,SETTING                                                ");
            queryString.AppendLine("            ,MINORLOSS                                              ");
            queryString.AppendLine("from        WH_VALVES                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + this.m_INP_NO + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VALVES");
        }


        private void UpdateNodeData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        {
            if (oDataTable == null) return;
            EAGL.Data.Wrapper.DataTableWrapper tW = new EAGL.Data.Wrapper.DataTableWrapper();
            ITable nodetable = tW.CreateITable(oDataTable);

            EAGL.Join.AttributeJoin.GetInstance().RemoveAllJoins(oTable);
            EAGL.Join.AttributeJoin.GetInstance().getJoinedLayer(oTable, nodetable, "ID", "ID", esriRelCardinality.esriRelCardinalityOneToOne);
        }

        private void UpdateLinkData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        {
            if (oDataTable == null) return;
            EAGL.Data.Wrapper.DataTableWrapper tW = new EAGL.Data.Wrapper.DataTableWrapper();
            ITable linktable = tW.CreateITable(oDataTable);

            EAGL.Join.AttributeJoin.GetInstance().RemoveAllJoins(oTable);
            EAGL.Join.AttributeJoin.GetInstance().getJoinedLayer(oTable, linktable, "ID", "ID", esriRelCardinality.esriRelCardinalityOneToOne);
        }

        ///// <summary>
        ///// 수리모델 상 절점에 해석결과를 표출
        ///// </summary>
        ///// <param name="oTable"></param>
        ///// <param name="oQueryFilter"></param>
        ///// <param name="oDataTable"></param>
        ///// <param name="Initialize"></param>
        //private void UpdateNodeData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        //{
        //    ICursor pCusror = oTable.Update(oQueryFilter, true);
        //    IRow oRow = null;
        //    string idField = "";

        //    if (Initialize)
        //    {
        //        idField = "ID";
        //    }
        //    else
        //    {
        //        idField = "NODE_ID";
        //    }

        //    try
        //    {
        //        while ((oRow = pCusror.NextRow()) != null)
        //        {
        //            DataRow nRow = getDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), oDataTable, idField);
        //            if (nRow != null)
        //            {
        //                if (Initialize)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ELEVATI"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_BASEDEM"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PATTERN"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_EMITTER"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITQUA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEQ"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEP"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCET"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKLEV"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DEMAND"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEAD"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_QUALITY"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEM"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITVOL"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXMODE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXZONE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKDIA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINVOLU"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VOLCURV"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINLEVE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MAXLEVE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXFRAC"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANK_KB"), "0");
        //                }
        //                else
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ELEVATI"), nRow["ELEVATION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_BASEDEM"), nRow["BASEDEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PATTERN"), nRow["PATTERN"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_EMITTER"), nRow["EMITTER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITQUA"), nRow["INITQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEQ"), nRow["SOURCEQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEP"), nRow["SOURCEPAT"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCET"), nRow["SOURCETYPE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKLEV"), nRow["TANKLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DEMAND"), nRow["DEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEAD"), nRow["HEAD"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), nRow["PRESSURE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_QUALITY"), nRow["QUALITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEM"), nRow["SOURCEMASS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITVOL"), nRow["INITVOLUME"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXMODE"), nRow["MIXMODEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXZONE"), nRow["MIXZONEVOL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKDIA"), nRow["TANKDIAM"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINVOLU"), nRow["MINVOLUME"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VOLCURV"), nRow["VOLCURVE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINLEVE"), nRow["MINLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MAXLEVE"), nRow["MAXLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXFRAC"), nRow["MIXFRACTION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANK_KB"), nRow["TANK_KBULK"]);
        //                }

        //                pCusror.UpdateRow(oRow);
        //            }
        //        }
        //        pCusror.Flush();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        Marshal.ReleaseComObject(pCusror);
        //    }
        //}

        ///// <summary>
        ///// 수리모델 상 링크에 해석결과를 표출
        ///// </summary>
        ///// <param name="oTable"></param>
        ///// <param name="oQueryFilter"></param>
        ///// <param name="oDataTable"></param>
        ///// <param name="Initialize"></param>
        //private void UpdateLinkData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        //{
        //    ICursor pCusror = oTable.Update(oQueryFilter, true);
        //    IRow oRow = null;
        //    string idField = "";

        //    if (Initialize)
        //    {
        //        idField = "ID";
        //    }
        //    else
        //    {
        //        idField = "LINK_ID";
        //    }

        //    try
        //    {
        //        while ((oRow = pCusror.NextRow()) != null)
        //        {
        //            DataRow nRow = getDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), oDataTable, idField);
        //            if (nRow != null)
        //            {
        //                if (Initialize)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DIAMETE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_LENGTH"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ROUGHNE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINORLO"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSTA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSET"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KBULK"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KWALL"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_FLOW"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VELOCIT"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEADLOS"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_STATUS"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SETTING"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ENERGY"), "0");
        //                }
        //                else
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DIAMETE"), nRow["DIAMETER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_LENGTH"), nRow["LENGTH"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ROUGHNE"), nRow["ROUGHNESS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINORLO"), nRow["MINORLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSTA"), nRow["INITSTATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSET"), nRow["INITSETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KBULK"), nRow["KBULK"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KWALL"), nRow["KWALL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_FLOW"), nRow["FLOW"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VELOCIT"), nRow["VELOCITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEADLOS"), nRow["HEADLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_STATUS"), nRow["STATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SETTING"), nRow["SETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ENERGY"), nRow["ENERGY"]);
        //                }

        //                pCusror.UpdateRow(oRow);
        //            }
        //        }
        //        pCusror.Flush();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        Marshal.ReleaseComObject(pCusror);
        //    }
        //}

        private DataRow getDataRow(string ID, DataTable oDataTable, string dataID)
        {
            DataRow oRow = null;
            DataRow[] oRows = oDataTable.Select(dataID + " = '" + ID + "'");
            if (oRows.Length == 1)
            {
                oRow = oRows[0];
            }
            return oRow;
        }
        
        #endregion
    }
}
