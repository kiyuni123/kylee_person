﻿namespace WaterNet.WQ_GamsiPosition.FormPopup
{
    partial class frmMPBrief
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDT = new System.Windows.Forms.TextBox();
            this.lblDT = new System.Windows.Forms.Label();
            this.lblNM = new System.Windows.Forms.Label();
            this.txtGBN = new System.Windows.Forms.TextBox();
            this.lblGBN = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtNO);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtDT);
            this.panel1.Controls.Add(this.lblDT);
            this.panel1.Controls.Add(this.lblNM);
            this.panel1.Controls.Add(this.txtGBN);
            this.panel1.Controls.Add(this.lblGBN);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(286, 110);
            this.panel1.TabIndex = 8;
            // 
            // txtNO
            // 
            this.txtNO.Location = new System.Drawing.Point(109, 28);
            this.txtNO.Name = "txtNO";
            this.txtNO.ReadOnly = true;
            this.txtNO.Size = new System.Drawing.Size(165, 21);
            this.txtNO.TabIndex = 128;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(8, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 12);
            this.label2.TabIndex = 127;
            this.label2.Text = "감시지점번호 :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDT
            // 
            this.txtDT.Location = new System.Drawing.Point(109, 80);
            this.txtDT.Name = "txtDT";
            this.txtDT.ReadOnly = true;
            this.txtDT.Size = new System.Drawing.Size(165, 21);
            this.txtDT.TabIndex = 126;
            // 
            // lblDT
            // 
            this.lblDT.AutoSize = true;
            this.lblDT.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDT.Location = new System.Drawing.Point(34, 84);
            this.lblDT.Name = "lblDT";
            this.lblDT.Size = new System.Drawing.Size(67, 12);
            this.lblDT.TabIndex = 125;
            this.lblDT.Text = "선정일자 :";
            this.lblDT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNM
            // 
            this.lblNM.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNM.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblNM.Location = new System.Drawing.Point(3, 3);
            this.lblNM.Name = "lblNM";
            this.lblNM.Size = new System.Drawing.Size(279, 22);
            this.lblNM.TabIndex = 123;
            this.lblNM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGBN
            // 
            this.txtGBN.Location = new System.Drawing.Point(109, 54);
            this.txtGBN.Name = "txtGBN";
            this.txtGBN.ReadOnly = true;
            this.txtGBN.Size = new System.Drawing.Size(165, 21);
            this.txtGBN.TabIndex = 118;
            // 
            // lblGBN
            // 
            this.lblGBN.AutoSize = true;
            this.lblGBN.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblGBN.Location = new System.Drawing.Point(8, 58);
            this.lblGBN.Name = "lblGBN";
            this.lblGBN.Size = new System.Drawing.Size(93, 12);
            this.lblGBN.TabIndex = 11;
            this.lblGBN.Text = "감시지점구분 :";
            this.lblGBN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmMPBrief
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(292, 116);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMPBrief";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "수질 감시 지점 정보";
            this.Load += new System.EventHandler(this.frmMPBrief_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblGBN;
        private System.Windows.Forms.Label lblNM;
        private System.Windows.Forms.TextBox txtGBN;
        private System.Windows.Forms.TextBox txtDT;
        private System.Windows.Forms.Label lblDT;
        private System.Windows.Forms.TextBox txtNO;
        private System.Windows.Forms.Label label2;
    }
}