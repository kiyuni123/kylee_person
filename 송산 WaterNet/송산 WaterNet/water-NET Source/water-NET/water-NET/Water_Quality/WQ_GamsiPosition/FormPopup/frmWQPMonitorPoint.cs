﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;
using WaterNet.WH_PipingNetworkAnalysis.epanet;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_GamsiPosition.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A02
    /// Project Explain : 감시지점선정
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.28
    /// Form Explain : 감시지점 Popup Form
    /// </summary>
    public partial class frmWQPMonitorPoint : Form
    {
        private OracleDBManager m_oDBManager = null;

        private AnalysisEngine m_Analysis = new AnalysisEngine();

        /// <summary>
        /// 관망해석 : INP, RPT NO
        /// </summary>
        private string m_INP_NO = string.Empty;
        private string m_RPT_NO = string.Empty;

        private Hashtable m_MODEL_LAYERS = new Hashtable();

        #region 맵 프로퍼티

        private IMapControl3 _Map;

        public IMapControl3 IMAP
        {
            get
            {
                return _Map;
            }
            set
            {
                _Map = value;
            }
        }

        #endregion

        public frmWQPMonitorPoint()
        {
            WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = false;
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            InitializeComponent();

            InitializeSetting();

            //this.GetTCPInfor();

            //if (m_STRC_TCP_ENV_COMMON.FTP_IP == null)
            //{
            //    WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = false;
            //    MessageBox.Show("FTP 서버의 정보 취득에 실패했습니다. 현재 창을 닫습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    this.Close();
            //}
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region Grid Set

            #region uGridPoint : 감시지점 Grid

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 154;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MONPNT_GBN";
            oUltraGridColumn.Header.Caption = "감시지점구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MONPNT";
            oUltraGridColumn.Header.Caption = "감시지점명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 160;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MONITOR_NO";
            oUltraGridColumn.Header.Caption = "감시지점일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 160;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE_ID";
            oUltraGridColumn.Header.Caption = "절점 ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SELECT_DATE";
            oUltraGridColumn.Header.Caption = "선정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_CL";
            oUltraGridColumn.Header.Caption = "잔류염소태그";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_PH";
            oUltraGridColumn.Header.Caption = "수소이온농도태그";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_TB";
            oUltraGridColumn.Header.Caption = "탁도태그";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_TE";
            oUltraGridColumn.Header.Caption = "온도태그";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_CU";
            oUltraGridColumn.Header.Caption = "전기전도도태그";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPoint);

            #endregion

            #region uGridPoint1 : 관망해석결과 : 체류시간 Grid

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 154;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALUE";
            oUltraGridColumn.Header.Caption = "체류시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ANALYSIS_TYPE";
            oUltraGridColumn.Header.Caption = "해석타입";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPoint1);

            #endregion

            #region uGridPoint3 : 관망해석결과 : 유량기여율 Grid

            oUltraGridColumn = uGridPoint3.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 154;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint3.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint3.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE1";
            oUltraGridColumn.Header.Caption = "Start Node";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint3.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE2";
            oUltraGridColumn.Header.Caption = "End Node";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기            

            oUltraGridColumn = uGridPoint3.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALUE";
            oUltraGridColumn.Header.Caption = "유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint3.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ANALYSIS_TYPE";
            oUltraGridColumn.Header.Caption = "해석타입";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPoint3);

            #endregion

            #region uGridPoint2_1 : 중요시설 Grid

            oUltraGridColumn = uGridPoint2_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RANK";
            oUltraGridColumn.Header.Caption = "순위";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint2_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 154;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint2_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FACILITIES_GBN";
            oUltraGridColumn.Header.Caption = "시설구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint2_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FACILITIES_NAME";
            oUltraGridColumn.Header.Caption = "시설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 230;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint2_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NO";
            oUltraGridColumn.Header.Caption = "시설일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 130;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPoint2_1);

            #endregion

            #region uGridPoint2_2 : 민원상세기본정보조회

            oUltraGridColumn = this.uGridPoint2_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 154;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridPoint2_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANO";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = this.uGridPoint2_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAAPPLDT";
            oUltraGridColumn.Header.Caption = "접수일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridPoint2_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridPoint2_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANM";
            oUltraGridColumn.Header.Caption = "민원인 성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridPoint2_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAMIDCD";
            oUltraGridColumn.Header.Caption = "민원분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridPoint2_2);

            #endregion
            
            #endregion          
        }

        private void frmWQPMonitorPoint_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       '1'으로 바꿔줘야함.
            //=========================================================수질관리(중요시설 선정)

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간감시관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            //===========================================================================

            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock1, EMFrame.statics.AppStatic.USER_SGCCD);
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock2, EMFrame.statics.AppStatic.USER_SGCCD);
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock3, EMFrame.statics.AppStatic.USER_SGCCD);

            WQ_Function.SetCombo_MonitorPartition(this.cboPartition, 1);
            WQ_Function.SetCombo_MonitorPartition(this.cboPartitionM, 0);

            WQ_Function.SetUDateTime_MaskInput(this.udtSDate1, 0, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtSDate1, -12);
            WQ_Function.SetUDateTime_MaskInput(this.udtEDate1, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtEDate1, -1);

            WQ_Function.SetUDateTime_MaskInput(this.udtSDate2, 0, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtSDate2, -12);
            WQ_Function.SetUDateTime_MaskInput(this.udtEDate2, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtEDate2, 0);

            WQ_Function.SetUDateTime_MaskInput(this.udtSDate3, 0, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtSDate3, -12);
            WQ_Function.SetUDateTime_MaskInput(this.udtEDate3, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtEDate3, -1);

            this.chkGItem1.Enabled = false;
            this.chkGItem2.Enabled = false;
            this.chkGItem3.Enabled = false;
            this.chkGItem4.Enabled = false;
            this.chkGItem5.Enabled = false;
            this.chkGItem1.Checked = true;
            this.chkGItem2.Checked = true;
            this.chkGItem3.Checked = true;
            this.chkGItem4.Checked = true;
            this.chkGItem5.Checked = true;
        }

        private void frmWQPMonitorPoint_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = false;
        }

        
        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.txtGPosName.Text = "";
                this.txtGPosNo.Text = "";
                this.txtNODEID.Text = "";

                this.GetQueryData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
        
        private void btnAnalysis_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string strMFTRIDN = string.Empty;
                string strINP_NUM = string.Empty;
                string strFilter = string.Empty;

                int iType = 0;  // 1 : 체류시간, 2 : 유량
                UltraGrid oGrid = new UltraGrid();

                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                switch (this.utabRTMonitorPoint.ActiveTab.Index)
                {
                    case 1:
                        strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock1.Text);
                        strFilter = txtAFilter1.Text;

                        strINP_NUM = this.GetINPNumber(strMFTRIDN);

                        m_INP_NO = strINP_NUM;

                        if (this.IsAnalysisValidation(0) == false) return;

                        iType = 1;
                        oGrid = this.uGridPoint1;
                        break;

                    case 3:
                        strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock3.Text);
                        strFilter = txtAFilter2.Text;

                        strINP_NUM = this.GetINPNumber(strMFTRIDN);

                        m_INP_NO = strINP_NUM;

                        if (this.IsAnalysisValidation(1) == false) return;

                        iType = 2;
                        oGrid = this.uGridPoint3;
                        break;
                }

                WaterNet.WQ_GamsiPosition.FormPopup.frmAnalysisProgress oForm = new WaterNet.WQ_GamsiPosition.FormPopup.frmAnalysisProgress();
                oForm.TopLevel = true;
                oForm.StartPosition = FormStartPosition.CenterScreen;
                oForm.BringToFront();
                oForm.Show();

                this.LoadAnalysisLayer(strINP_NUM);

                Hashtable oHTConditions = new Hashtable();

                oHTConditions.Add("INP_NUMBER", strINP_NUM);
                oHTConditions.Add("AUTO_MANUAL", "M");
                oHTConditions.Add("analysisType", 0);
                oHTConditions.Add("analysisFlag", "Q");
                oHTConditions.Add("resetValues", this.GetRealtimeDischargeValue(strINP_NUM, strMFTRIDN));
                oHTConditions.Add("saveReport", true); //DB에 저장

                Hashtable result = m_Analysis.Execute(oHTConditions);

                this.GetAnalysisResult(strINP_NUM, strMFTRIDN, strFilter, oGrid, iType);

                //this.ClearLayerDatas();

                this.SetLayerDatas();

                ArcManager.PartialRefresh(this._Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();

                oForm.Dispose();
                oForm.Close();

                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnQuery2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetFacilitiesData();
                this.GetMinwonData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.txtGPosName.Text = "";
            this.txtGPosNo.Text = "";
            this.txtNODEID.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.utabRTMonitorPoint.SelectedTab.Index != 0)
            {
                this.txtGPosNo.Text = WQ_Function.GenerationWQSerialNumber("MP");
            }

            string strMNT_NO = string.Empty;
            string strMNT_NM = string.Empty;

            //감시지점선정시 WQ_RT_MONITOR_POINT, WQ_MONPNT_MAPPING 에 기본 정보 저장한다.
            if (this.IsValidation(0) == false) return;

            if (_Map != null && WQ_AppStatic.CURRENT_IGEOMETRY != null)
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                this.SaveMonitorPointData();
                this.SaveMonitorItemData();

                if (this.utabRTMonitorPoint.SelectedTab.Index != 0)
                {
                    ILayer pLayer = ArcManager.GetMapLayer(_Map, "감시지점");

                    IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                    IFeature pFeature = pFeatureClass.CreateFeature();

                    //Shpae에 지점 등록
                    pFeature.Shape = WQ_AppStatic.CURRENT_IGEOMETRY;

                    //등록된 지점에 밸류 등록
                    ArcManager.SetValue(pFeature, "SBLOCK", (object)WQ_Function.SplitToCode(this.cboSBlockM.Text));
                    ArcManager.SetValue(pFeature, "GBN", (object)WQ_Function.SplitToCode(this.cboPartitionM.Text));
                    ArcManager.SetValue(pFeature, "NOS", (object)this.txtGPosNo.Text.Trim());
                    ArcManager.SetValue(pFeature, "NAMES", (object)this.txtGPosName.Text.Trim());
                    strMNT_NO = this.txtGPosNo.Text.Trim();
                    strMNT_NM = this.txtGPosName.Text.Trim();
                    //Shape 저장
                    pFeature.Store();

                    //GIS Map Refresh
                    //WQ_AppStatic.ITOC.Update();
                    _Map.ActiveView.ContentsChanged();
                    ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                    Application.DoEvents();
                }

                this.GetQueryData();

                this.txtGPosName.Text = "";
                this.txtGPosNo.Text = "";
                this.txtNODEID.Text = "";

                this.Cursor = System.Windows.Forms.Cursors.Default;

                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.IsValidation(1) == false) return;

            DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oDRtn == DialogResult.Yes)
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                this.DeleteMonitorPointData();
                this.DeleteMonitorItemData();

                if (_Map != null)
                {
                    ILayer pLayer = ArcManager.GetMapLayer(_Map, "감시지점");
                    IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                    IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "NOS='" + this.txtGPosNo.Text + "'") as IFeatureCursor;

                    IFeature pFeature = pCursor.NextFeature();

                    if (pFeature != null)
                    {
                        pFeature.Delete();

                        //GIS Map Refresh
                       // WQ_AppStatic.ITOC.Update();
                        ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                        Application.DoEvents();

                        this.GetQueryData();

                        this.txtGPosName.Text = "";
                        this.txtGPosNo.Text = "";
                        this.txtNODEID.Text = "";

                        MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    this.BringToFront();
                }

                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion


        #region Control Events

        #region - Grid : 지점조회 및 관리 Tab

        private void uGridPoint_Click(object sender, EventArgs e)
        {
            if (this.uGridPoint.Rows.Count <= 0) return;

            this.cboSBlockM.Items.Clear();
            this.cboSBlockM.Items.Add(this.uGridPoint.ActiveRow.Cells[0].Text.Trim());
            this.cboSBlockM.SelectedIndex = 0;

            WQ_Function.SetCombo_SelectFindData(this.cboPartitionM, this.uGridPoint.ActiveRow.Cells[1].Text);
            this.txtGPosName.Text = this.uGridPoint.ActiveRow.Cells[2].Text;
            this.txtGPosNo.Text = this.uGridPoint.ActiveRow.Cells[3].Text;
            this.txtNODEID.Text = this.uGridPoint.ActiveRow.Cells[4].Text;

            this.cboTAGCL.Items.Clear(); this.cboTAGPH.Items.Clear(); this.cboTAGTB.Items.Clear(); this.cboTAGTE.Items.Clear(); this.cboTAGCU.Items.Clear();
            this.cboTAGCL.Items.Add(this.uGridPoint.ActiveRow.Cells[6].Text); this.cboTAGCL.SelectedIndex = 0;
            this.cboTAGPH.Items.Add(this.uGridPoint.ActiveRow.Cells[7].Text); this.cboTAGPH.SelectedIndex = 0;
            this.cboTAGTB.Items.Add(this.uGridPoint.ActiveRow.Cells[8].Text); this.cboTAGTB.SelectedIndex = 0;
            this.cboTAGTE.Items.Add(this.uGridPoint.ActiveRow.Cells[9].Text); this.cboTAGTE.SelectedIndex = 0;
            this.cboTAGCU.Items.Add(this.uGridPoint.ActiveRow.Cells[10].Text); this.cboTAGCU.SelectedIndex = 0;

            Application.DoEvents();
        }

        private void uGridPoint_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPoint.Rows.Count <= 0) return;

            this.Viewmap_GridPoint("감시지점", this.txtGPosNo.Text, "NOS");
        }

        #endregion

        #region - Grid : 지점선정 체류시간 Tab

        private void uGridPoint1_Click(object sender, EventArgs e)
        {
            if (this.uGridPoint1.Rows.Count <= 0) return;

            string strSBLOCK = string.Empty;
            string strID = string.Empty;

            strSBLOCK = this.uGridPoint1.ActiveRow.Cells[0].Text;
            strID = this.uGridPoint1.ActiveRow.Cells[1].Text;
            this.txtNODEID.Text = strID;

            WQ_Function.SetCombo_SelectFindData(this.cboSBlockM, strSBLOCK);
        }

        private void uGridPoint1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPoint1.Rows.Count <= 0) return;

            string strID = string.Empty;
            string strShape = string.Empty;

            strID = this.uGridPoint1.ActiveRow.Cells[1].Text;
            strShape = this.uGridPoint1.ActiveRow.Cells[3].Text;

            this.txtNODEID.Text = strID;
            Application.DoEvents();

            switch (strShape)
            {
                case "PRV":
                case "PSV":
                case "PBV":
                case "FCV":
                case "TCV":
                case "GPV":
                    strShape = "VALVE";
                    break;
                case "CVPIPE":
                    strShape = "PIPE";
                    break;
            }

            this.Viewmap_AnalysisResult(strShape, strID, "ID");
        }

        #endregion

        #region - Grid : 지점선정 운영자 Tab

        private void uGridPoint2_1_Click(object sender, EventArgs e)
        {
            if (this.uGridPoint2_1.Rows.Count <= 0) return;

            string strMBLOCKCD = WQ_Function.GetMediumBlockBySmallBlockCode(WQ_Function.SplitToCode(this.uGridPoint2_1.ActiveRow.Cells[1].Text));
            string strMBLOCK = WQ_Function.GetBlockNameCodeByFTR_IDN(WQ_Function.GetMediumBlockFTR_IDN(strMBLOCKCD));
            WQ_Function.SetCombo_SelectFindData(this.cboMBlock2, strMBLOCK);
            WQ_Function.SetCombo_SelectFindData(this.cboSBlock2, this.uGridPoint2_1.ActiveRow.Cells[1].Text);

            this.cboSBlockM.Items.Clear();
            this.cboSBlockM.Items.Add(this.uGridPoint2_1.ActiveRow.Cells[1].Text.Trim());
            this.cboSBlockM.SelectedIndex = 0;
        }

        private void uGridPoint2_2_Click(object sender, EventArgs e)
        {
            if (this.uGridPoint2_2.Rows.Count <= 0) return;

            string strMBLOCKCD = WQ_Function.GetMediumBlockBySmallBlockCode(WQ_Function.SplitToCode(this.uGridPoint2_2.ActiveRow.Cells[0].Text));
            string strMBLOCK = WQ_Function.GetBlockNameCodeByFTR_IDN(WQ_Function.GetMediumBlockFTR_IDN(strMBLOCKCD));
            WQ_Function.SetCombo_SelectFindData(this.cboMBlock2, strMBLOCK);
            WQ_Function.SetCombo_SelectFindData(this.cboSBlock2, this.uGridPoint2_2.ActiveRow.Cells[0].Text);

            this.cboSBlockM.Items.Clear();
            this.cboSBlockM.Items.Add(this.uGridPoint2_2.ActiveRow.Cells[0].Text.Trim());
            this.cboSBlockM.SelectedIndex = 0;
        }

        private void uGridPoint2_1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPoint2_1.Rows.Count <= 0) return;

            string strFacNo = this.uGridPoint2_1.ActiveRow.Cells[4].Text;
            this.Viewmap_GridPoint("중요시설", strFacNo, "NOS");
        }

        private void uGridPoint2_2_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPoint2_2.Rows.Count <= 0) return;

            string strKEY = this.uGridPoint2_2.ActiveRow.Cells[3].Text.Trim();
            this.Viewmap_GridPoint("수도계량기", strKEY, "DMNO");
        }

        #endregion

        #region - Grid : 지점선정 유량 Tab

        private void uGridPoint3_Click(object sender, EventArgs e)
        {
            if (this.uGridPoint3.Rows.Count <= 0) return;

            string strSBLOCK = string.Empty;
            string strSNode = string.Empty;
            
            strSBLOCK = this.uGridPoint3.ActiveRow.Cells[0].Text;
            strSNode = this.uGridPoint3.ActiveRow.Cells[2].Text;

            this.txtNODEID.Text = strSNode;
            Application.DoEvents();

            WQ_Function.SetCombo_SelectFindData(this.cboSBlockM, strSBLOCK);
        }

        private void uGridPoint3_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPoint3.Rows.Count <= 0) return;

            string strSNode = string.Empty;

            strSNode = this.uGridPoint3.ActiveRow.Cells[2].Text;

            this.txtNODEID.Text = strSNode;
            Application.DoEvents();

            this.Viewmap_AnalysisResult("PIPE", this.uGridPoint3.ActiveRow.Cells[1].Text, "ID");
            this.Viewmap_AnalysisResult("JUNCTION", strSNode, "ID");
        }

        #endregion

        #region - Combo : 공통

        private void cboSBlockM_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlockM.Text);
            string strMFTRIDN = string.Empty;
            string strReservoir = string.Empty;

            switch (this.utabRTMonitorPoint.ActiveTab.Index)
            {
                case 1:
                    strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock1.Text);
                    break;
                case 2:
                    strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock2.Text);
                    break;
                case 3:
                    strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock3.Text);
                    break;
            }

            strReservoir = WQ_Function.GetReservoirFromMediumBlockCode(strMFTRIDN);

            WQ_Function.SetCombo_TAGID(this.cboTAGCL, "CL", strSFTRIDN, strMFTRIDN, strReservoir);
            WQ_Function.SetCombo_TAGID(this.cboTAGTE, "TE", strSFTRIDN, strMFTRIDN, strReservoir);
            WQ_Function.SetCombo_TAGID(this.cboTAGPH, "PH", strSFTRIDN, strMFTRIDN, strReservoir);
            WQ_Function.SetCombo_TAGID(this.cboTAGTB, "TB", strSFTRIDN, strMFTRIDN, strReservoir);
            WQ_Function.SetCombo_TAGID(this.cboTAGCU, "CU", strSFTRIDN, strMFTRIDN, strReservoir);
        }

        private void cboPartition_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlock.Text);
            string strMONPNT_GBN = WQ_Function.SplitToCode(this.cboPartition.Text).Trim();
        }

        #endregion

        #region - Combo : 지점조회 및 관리 Tab

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);
            this.cboSBlockM.Items.Clear();
            //WQ_Function.SetCombo_SmallBlock(this.cboSBlockM, AppStatic.USER_SGCCD, strMFTRIDN, 0);
            //this.Viewmap_MonitorBlock(strMFTRIDN, 0);
        }

        private void cboSBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboSBlockM.Items.Clear();
            //string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlock.Text);
            //if (this.cboSBlockM.Items.Count > 0)
            //{
            //    this.cboSBlockM.SelectedIndex = this.cboSBlock.SelectedIndex;
            //}
            //this.Viewmap_MonitorBlock(strSFTRIDN, 1);
        }

        #endregion

        #region - Combo : 지점선정 체류시간 Tab

        private void cboLBlock1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock1.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock1, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN);
        }

        private void cboMBlock1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock1.Text);

            WQ_Function.SetCombo_SmallBlock(this.cboSBlockM, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 0);
            //this.Viewmap_MonitorBlock(strMFTRIDN, 0);

            if (this.GetINPNumber(strMFTRIDN) == "")
            {
                //MessageBox.Show("선택하신 중블록에는 관망해석모델이 없습니다. 확인 후 해석을 실행하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.btnAnalysis1.Enabled = false;
            }
            else
            {
                this.btnAnalysis1.Enabled = true;
            }
        }

        #endregion 
        
        #region - Combo : 지점선정 운영자 Tab

        private void cboLBlock2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock2.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock2, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock2.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock2, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);
            //WQ_Function.SetCombo_SmallBlock(this.cboSBlockM, AppStatic.USER_SGCCD, strMFTRIDN, 0);
            //this.Viewmap_MonitorBlock(strMFTRIDN, 0);

            this.cboSBlockM.Items.Clear();
            this.cboTAGCL.Items.Clear();
            this.cboTAGTB.Items.Clear();
            this.cboTAGPH.Items.Clear();
            this.cboTAGTE.Items.Clear();
            this.cboTAGCU.Items.Clear();
        }

        private void cboSBlock2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlock2.Text);
            //if (this.cboSBlockM.Items.Count > 0)
            //{
            //    WQ_Function.SetCombo_SelectFindData(this.cboSBlockM, this.cboSBlock2.Text);
            //}

            //this.Viewmap_MonitorBlock(strSFTRIDN, 1);

            this.cboSBlockM.Items.Clear();
        }
       
        #endregion

        #region - Combo : 지점선정 유량 Tab

        private void cboLBlock3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock3.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock3, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN);
        }

        private void cboMBlock3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock3.Text);

            WQ_Function.SetCombo_SmallBlock(this.cboSBlockM, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 0);
            //this.Viewmap_MonitorBlock(strMFTRIDN, 0);

            if (this.GetINPNumber(strMFTRIDN) == "")
            {
                //MessageBox.Show("선택하신 중블록에는 관망해석모델이 없습니다. 확인 후 해석을 실행하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.btnAnalysis3.Enabled = false;
            }
            else
            {
                this.btnAnalysis3.Enabled = true;
            }
        }

        #endregion 

        private void utabRTMonitorPoint_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            this.panel2.Enabled = true;
            this.panel6.Enabled = true;
            this.txtGPosName.Text = "";
            this.txtGPosNo.Text = "";
            this.txtNODEID.Text = "";

            if (e.Tab.Index == 0)
            {
                this.panel7.Enabled = true;
                //=========================================================
                //
                //                    동진 수정_2012.6.08
                //			권한박탈(조회만 가능)
                //=========================================================
                object o = EMFrame.statics.AppStatic.USER_MENU["누수량산정결과조회ToolStripMenuItem"];
                if (o != null && (Convert.ToString(o).Equals("0") ? true : false))      //관리자 권한일때 접속
                {
                    this.btnSave.Enabled = true;
                    this.btnDelete.Enabled = true;
                }

                //============================================================================
                
                this.txtGPosNo.ReadOnly = true;
                this.txtGPosName.ReadOnly = true;
                this.txtNODEID.ReadOnly = true;
                if (this.cboPartitionM.Items.Count > 0) this.cboPartitionM.SelectedIndex = 0;
             }
            else
            {
                string strMFTRIDN = string.Empty;

                switch (e.Tab.Index)
                {
                    case 1:
                        strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock1.Text);
                        break;
                    case 2:
                        this.cboMBlock2.SelectedIndex = 0;
                        strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock2.Text);
                        //WQ_Function.SetCombo_SmallBlock(this.cboSBlock2, AppStatic.USER_SGCCD, strMFTRIDN, 1);
                        this.cboTAGCL.Items.Clear();
                        this.cboTAGTB.Items.Clear();
                        this.cboTAGPH.Items.Clear();
                        this.cboTAGTE.Items.Clear();
                        this.cboTAGCU.Items.Clear();
                        break;
                    case 3:
                        strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock3.Text);
                        break;
                }

                WQ_Function.SetCombo_SmallBlock(this.cboSBlockM, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 0);

                this.panel7.Enabled = true;
                //=========================================================
                //
                //                    동진 수정_2012.6.08
                //			권한박탈(조회만 가능)
                //=========================================================

                object o = EMFrame.statics.AppStatic.USER_MENU["누수량산정결과조회ToolStripMenuItem"];
                if (o != null && (Convert.ToString(o).Equals("0") ? true : false))     //관리자 권한일때 접속
                {
                    this.btnSave.Enabled = true;
                }

                //=========================================================================
                this.btnDelete.Enabled = false;                
                if (this.cboPartitionM.Items.Count > 0) 
                {
                    if (e.PreviousSelectedTab.Index != e.Tab.Index)
                    {
                        this.cboPartitionM.SelectedIndex = e.Tab.Index - 1;
                    }
                }

                this.txtGPosNo.ReadOnly = true;
                this.txtGPosName.ReadOnly = false;

                if (e.Tab.Index == 2)
                {

                    this.txtNODEID.ReadOnly = true;
                }
                else
                {
                    this.txtNODEID.ReadOnly = false;
                }
            }
        }

        #endregion


        #region User Function

        #region - SQL Function

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// (감시지점)
        /// </summary>
        private void GetQueryData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            string strMONPNT_GBN = WQ_Function.SplitToCode(this.cboPartition.Text);
            string strMONPNT_SQL = string.Empty;

            if (strMONPNT_GBN.Trim() != "")
            {
                strMONPNT_SQL = " AND A.MONPNT_GBN = '" + strMONPNT_GBN + "'";
            }

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = " AND A.SBLOCK_CODE IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = " AND A.SBLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   C.LOC_NAME || ' (' || C.LOC_CODE || ')' AS BLOCK, B.CODE_NAME || ' (' || B.CODE || ')' AS MONPNT_GBN, A.MONPNT, A.MONITOR_NO, A.NODE_ID, TO_DATE(A.SELECT_DATE, 'RRRR-MM-DD') AS SELECT_DATE,");
            oStringBuilder.AppendLine("         A.TAG_ID_CL, A.TAG_ID_PH, A.TAG_ID_TB, A.TAG_ID_TE, A.TAG_ID_CU");
            oStringBuilder.AppendLine("FROM     WQ_RT_MONITOR_POINT A, CM_CODE B, CM_LOCATION C");
            oStringBuilder.AppendLine("WHERE    C.LOC_CODE = A.SBLOCK_CODE AND B.PCODE = '3002' AND B.CODE = A.MONPNT_GBN" + strBLOCK_SQL + strMONPNT_SQL);

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RTMONPOINT");

            this.uGridPoint.DataSource = pDS.Tables["RTMONPOINT"].DefaultView;

            //FormManager.SetGridStyle_PerformAutoResize(this.uGridPoint);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 감시지점 Data를 저장 한다.
        /// </summary>
        private void SaveMonitorPointData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strSBLKCD = string.Empty;
            string strMONITOR_NO = string.Empty;
            string strMONPNT_GBN = string.Empty;
            string strMONPNT = string.Empty;
            string strMPOS = string.Empty;

            DataSet pDS = new DataSet();

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            strMONITOR_NO = this.txtGPosNo.Text;
            strMONPNT_GBN = this.cboPartitionM.Text;
            strMONPNT_GBN = WQ_Function.SplitToCode(strMONPNT_GBN);
            strSBLKCD = this.cboSBlockM.Text.ToString();
            strSBLKCD = WQ_Function.SplitToCode(strSBLKCD);
            strMONPNT = this.txtGPosName.Text.Trim();

            strParam = "WHERE    SBLOCK_CODE = '" + strSBLKCD + "' AND MONITOR_NO = '" + strMONITOR_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            IPoint pPoint = new PointClass();
            pPoint = (IPoint)WQ_AppStatic.CURRENT_IGEOMETRY;

            strParam = "('" + strMONITOR_NO + "', '" + strMONPNT_GBN + "', '" + strSBLKCD + "', TO_CHAR(SYSDATE, 'RRRRMMDD'), '" + strMONPNT + "', '"
                     + this.cboTAGCL.Text + "', '" + this.cboTAGTE.Text + "', '" + this.cboTAGPH.Text + "', '" + this.cboTAGTB.Text + "', '" + this.cboTAGCU.Text + "', '" + this.txtNODEID.Text + "', '" + pPoint.X.ToString() + "', '" + pPoint.Y.ToString() + "')";

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("(MONITOR_NO, MONPNT_GBN, SBLOCK_CODE, SELECT_DATE, MONPNT, TAG_ID_CL, TAG_ID_TE, TAG_ID_PH, TAG_ID_TB, TAG_ID_CU, NODE_ID, MAP_X, MAP_Y)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감시항목 Data를 저장 한다.
        /// </summary>
        private void SaveMonitorItemData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strMITEMCD = string.Empty;
            string strMONITOR_NO = string.Empty;

            DataSet pDS = new DataSet();

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            strMONITOR_NO = this.txtGPosNo.Text.Trim();

            for (int i = 0; i < 5; i++)
            {
                strMITEMCD = string.Empty;

                switch (i)
                {
                    case 0:
                        if (this.chkGItem1.Checked == true)
                        {
                            strMITEMCD = "TE";
                        }
                        break;

                    case 1:
                        if (this.chkGItem2.Checked == true)
                        {
                            strMITEMCD = "PH";
                        }
                        break;

                    case 2:
                        if (this.chkGItem3.Checked == true)
                        {
                            strMITEMCD = "TB";
                        }
                        break;

                    case 3:
                        if (this.chkGItem4.Checked == true)
                        {
                            strMITEMCD = "CL";
                        }
                        break;

                    case 4:
                        if (this.chkGItem5.Checked == true)
                        {
                            strMITEMCD = "CU";
                        }
                        break;

                }

                strParam = "WHERE    MONITOR_NO = '" + strMONITOR_NO + "' AND WQ_ITEM_CODE = '" + strMITEMCD + "'";
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE   WQ_MONPNT_MAPPING");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                if (strMITEMCD != string.Empty)
                {
                    strParam = "('" + strMONITOR_NO + "', '" + strMITEMCD + "', '', '', '', '', '', '', '')";
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("INSERT   INTO    WQ_MONPNT_MAPPING");
                    oStringBuilder.AppendLine("(MONITOR_NO, WQ_ITEM_CODE, N1_TOP_LIMIT, N1_LOW_LIMIT, N2_TOP_LIMIT, N2_LOW_LIMIT, N3_TOP_LIMIT, N3_LOW_LIMIT, ALARM_YN)");
                    oStringBuilder.AppendLine("VALUES");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                }
            }
        }

        /// <summary>
        /// 감시지점 Data를 삭제 한다.
        /// </summary>
        private void DeleteMonitorPointData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strSBLKCD = string.Empty;
            string strMONITOR_NO = string.Empty;
            string strMONPNT_GBN = string.Empty;
            string strMONPNT = string.Empty;
            string strMPOS = string.Empty;

            DataSet pDS = new DataSet();

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //감시지점일련번호 생성 조건을 알아서 나중에 코딩 필요
            strMONITOR_NO = this.txtGPosNo.Text;
            strMONPNT_GBN = this.cboPartitionM.Text;
            strMONPNT_GBN = WQ_Function.SplitToCode(strMONPNT_GBN);
            strSBLKCD = this.cboSBlockM.Text.ToString();
            strSBLKCD = WQ_Function.SplitToCode(strSBLKCD);
            strMONPNT = this.txtGPosName.Text.Trim();

            strParam = "WHERE    SBLOCK_CODE = '" + strSBLKCD + "' AND MONITOR_NO = '" + strMONITOR_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감시항목 Data를 삭제 한다.
        /// </summary>
        private void DeleteMonitorItemData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strMONITOR_NO = string.Empty;

            DataSet pDS = new DataSet();

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            strMONITOR_NO = this.txtGPosNo.Text.Trim();

            strParam = "WHERE    MONITOR_NO = '" + strMONITOR_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_MONPNT_MAPPING");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            strParam = "WHERE    MONITOR_NO = '" + strMONITOR_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_PATTERN");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            strParam = "WHERE    MONITOR_NO = '" + strMONITOR_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_MONITOR_OPTION");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 중요시설
        /// </summary>
        private void GetFacilitiesData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock2.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock2.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = " AND A.SBLOCK_CODE IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = " AND A.SBLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }
            
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.RANK, C.LOC_NAME || ' (' || C.LOC_CODE || ')' AS BLOCK, B.CODE_NAME || ' (' || B.CODE || ')' AS FACILITIES_GBN, A.FACILITIES_NAME, A.NO");
            oStringBuilder.AppendLine("FROM     WQ_IMPORTANT_FACILITIES A, CM_CODE B, CM_LOCATION C");
            oStringBuilder.AppendLine("WHERE    C.LOC_CODE = A.SBLOCK_CODE AND B.PCODE = '3001' AND B.CODE = A.FACILITIES_GBN" + strBLOCK_SQL);
            oStringBuilder.AppendLine("ORDER BY A.RANK");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "FACILITIES");

            this.uGridPoint2_1.DataSource = pDS.Tables["FACILITIES"].DefaultView;

            //FormManager.SetGridStyle_PerformAutoResize(this.uGridPoint2_1);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 수질민원
        /// </summary>
        private void GetMinwonData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;
            string strParam = string.Empty;

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock2.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock2.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockFTRIDNInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "         AND B.SFTRIDN IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = "         AND B.SFTRIDN = '" + strBLOCK_CD_S + "'";
                }
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   C.LOC_NAME || ' (' || C.LOC_CODE || ')' AS BLOCK, A.CANO, TO_CHAR(A.CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, E.CODE_NAME AS CAMIDCD");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A, WI_DMINFO B, CM_LOCATION C, CM_CODE E");
            oStringBuilder.AppendLine("WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("         AND A.DMNO IS NOT NULL");
            oStringBuilder.AppendLine("         AND A.CALRGCD = '2000' AND SUBSTR(A.CAMIDCD, 0, 2) = '20' AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("         AND B.DMNO = A.DMNO");
            oStringBuilder.AppendLine("         AND C.FTR_IDN = B.SFTRIDN");
            oStringBuilder.AppendLine("         " + strBLOCK_SQL);
            oStringBuilder.AppendLine("         AND E.PCODE = '9004' AND E.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT DESC, A.CANO");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridPoint2_2.DataSource = pDS.Tables["WI_CAINFO"].DefaultView;

            //AutoResizeColumes
            //FormManager.SetGridStyle_PerformAutoResize(this.uGridPoint2_2);

            if (this.uGridPoint2_2.Rows.Count > 0) this.uGridPoint2_2.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region - Analysis Function

        /// <summary>
        /// 저장하는 Data를 검증한다.
        /// </summary>
        /// <param name="iValType">Validation Type. 0 : Save, 1 : Delete</param>
        /// <returns>True / False</returns>
        private bool IsAnalysisValidation(int iValType)
        {
            bool blRtn = true;

            switch (iValType)
            {
                case 0: //체류시간 Validation
                    if (this.txtDR1.Text == "") blRtn = false;
                    if (this.txtRS1.Text.Trim() == "") blRtn = false;
                    if (this.txtRT1.Text.Trim() == "") blRtn = false;

                    break;

                case 1: //유량 Validation
                    if (this.txtDR2.Text == "") blRtn = false;
                    if (this.txtRS2.Text.Trim() == "") blRtn = false;
                    if (this.txtRT2.Text.Trim() == "") blRtn = false;
                    break;

                default:
                    blRtn = false;
                    break;
            }

            if (blRtn == false)
            {
                MessageBox.Show("관망해석 옵션을 입력해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return blRtn;
        }

        /// <summary>
        /// 실시간 유량 데이터를 Select 한다.(배수지 out)
        /// </summary>
        private void GetRTDischargeData(int iType)
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strReservoir = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            UltraGrid oGrid = new UltraGrid();

            DataSet pDS = new DataSet();

            switch (iType)
            {
                case 0:
                    strSDate = WQ_Function.StringToDateTime(this.udtSDate1.DateTime);
                    strEDate = WQ_Function.StringToDateTime(this.udtEDate1.DateTime);
                    oGrid = this.uGridTemp1;
                    break;
                case 1:
                    strSDate = WQ_Function.StringToDateTime(this.udtSDate3.DateTime);
                    strEDate = WQ_Function.StringToDateTime(this.udtEDate3.DateTime);
                    oGrid = this.uGridTemp2;
                    break;
            }

            //oStringBuilder.AppendLine("SELECT   A.FTR_IDN AS BLOCK, TO_CHAR(B.TIMESTAMP, 'RRRR-MM-DD') AS DT, ROUND(AVG(B.VALUE), 3) AS AVGVAL");
            //oStringBuilder.AppendLine("FROM     IF_IHTAGS A");
            //oStringBuilder.AppendLine("         LEFT OUTER JOIN IF_ACCUMULATION_YESTERDAY B ON B.TAGNAME = A.TAGNAME AND TRANSLATE(B.VALUE, 'a0123456789', 'a') IS NULL");
            //oStringBuilder.AppendLine("         LEFT OUTER JOIN IF_TAG_GBN C ON C.TAG_GBN = 'YD' AND C.TAGNAME = A.TAGNAME");
            //oStringBuilder.AppendLine("WHERE    A.FTR_IDN = '" + this.GetFTRIDNFromINP() + "' AND A.USE_YN = 'Y'");
            //oStringBuilder.AppendLine("         AND TO_CHAR(B.TIMESTAMP, 'RRRRMMDD') BETWEEN '" + strSDate + "' AND '" + strEDate + "'");
            //oStringBuilder.AppendLine("GROUP BY A.FTR_IDN, TO_CHAR(B.TIMESTAMP, 'RRRR-MM-DD')");
            //oStringBuilder.AppendLine("ORDER BY BLOCK, DT");

            oStringBuilder.Remove(0, oStringBuilder.Length);

            strReservoir = this.GetFTRIDNFromINP();
            switch (strReservoir.Substring(0, 1))
            {
                case "R":
                    oStringBuilder.AppendLine("SELECT   A.FTR_IDN AS BLOCK, TO_CHAR(C.TIMESTAMP, 'RRRR-MM-DD') AS DT, ROUND(AVG(C.VALUE), 3) AS AVGVAL");
                    oStringBuilder.AppendLine("FROM     IF_IHTAGS A,");
                    oStringBuilder.AppendLine("         IF_TAG_GBN B,");
                    oStringBuilder.AppendLine("         IF_ACCUMULATION_YESTERDAY C");
                    oStringBuilder.AppendLine("WHERE    A.TAGNAME = B.TAGNAME");
                    oStringBuilder.AppendLine("         AND A.LOC_CODE = (SELECT LOC_CODE FROM CM_LOCATION WHERE RES_CODE = (SELECT LOC_CODE FROM CM_LOCATION WHERE FTR_IDN = '" + this.GetFTRIDNFromINP().Substring(1).Trim() + "'))");
                    oStringBuilder.AppendLine("         AND B.TAG_GBN = 'YD_R'");
                    oStringBuilder.AppendLine("         AND C.TAGNAME = B.TAGNAME");
                    oStringBuilder.AppendLine("         AND C.VALUE > 0");
                    oStringBuilder.AppendLine("         AND TO_CHAR(C.TIMESTAMP, 'RRRRMMDD') BETWEEN '" + strSDate + "' AND '" + strEDate + "'");
                    oStringBuilder.AppendLine("GROUP BY A.FTR_IDN, TO_CHAR(C.TIMESTAMP, 'RRRR-MM-DD')");
                    oStringBuilder.AppendLine("ORDER BY BLOCK, DT");
                    break;
                case "F":
                    oStringBuilder.AppendLine("SELECT   A.FTR_IDN AS BLOCK, TO_CHAR(C.TIMESTAMP, 'RRRR-MM-DD') AS DT, ROUND(AVG(C.VALUE), 3) AS AVGVAL");
                    oStringBuilder.AppendLine("FROM     IF_IHTAGS A,");
                    oStringBuilder.AppendLine("         IF_TAG_GBN B,");
                    oStringBuilder.AppendLine("         IF_ACCUMULATION_YESTERDAY C");
                    oStringBuilder.AppendLine("WHERE    A.TAGNAME = B.TAGNAME");
                    oStringBuilder.AppendLine("         AND A.FTR_IDN = '" + this.GetFTRIDNFromINP().Substring(1).Trim() + "'");
                    oStringBuilder.AppendLine("         AND (B.TAG_GBN = 'YD' OR B.TAG_GBN = 'YD_R')");
                    oStringBuilder.AppendLine("         AND C.TAGNAME = B.TAGNAME");
                    oStringBuilder.AppendLine("         AND C.VALUE > 0");
                    oStringBuilder.AppendLine("         AND TO_CHAR(C.TIMESTAMP, 'RRRRMMDD') BETWEEN '" + strSDate + "' AND '" + strEDate + "'");
                    oStringBuilder.AppendLine("GROUP BY A.FTR_IDN, TO_CHAR(C.TIMESTAMP, 'RRRR-MM-DD')");
                    oStringBuilder.AppendLine("ORDER BY BLOCK, DT");
                    break;
            }

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            oGrid.DataSource = pDS.Tables["IF_IHTAGS"].DefaultView;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 중블록 코드로 INP Number를 취득한다.
        /// </summary>
        /// <param name="strMFTRIDN"></param>
        /// <returns></returns>
        private string GetINPNumber(string strMFTRIDN)
        {
            string strRTN = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return string.Empty;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   INP_NUMBER AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WH_TITLE");
            oStringBuilder.AppendLine("WHERE    MFTRIDN = '" + strMFTRIDN + "' AND USE_GBN = 'WQ' AND ROWNUM = 1");
            oStringBuilder.AppendLine("ORDER BY INS_DATE DESC");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WH_TITLE");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["RTNDATA"].ToString().Trim();
                    break;
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            return strRTN;
        }

        //업무별 실시간 계측값을 반환한다. (모델번호의 접두어 2자리를 이용한다.)
        private Hashtable GetRealtimeDischargeValue(string strINP_NUM, string strMFTRIDN)
        {
            Hashtable result = new Hashtable();

            try
            {
                //수질
                Hashtable AnalysisTime = new Hashtable();
                Hashtable AnalysisOption = new Hashtable();

                AnalysisTime.Add("Quality Timestep", "0:05");
                AnalysisTime.Add("Hydraulic Timestep", "1:00");
                switch (this.utabRTMonitorPoint.ActiveTab.Index)
                {
                    case 1:
                        //AnalysisTime.Add("Quality Timestep", this.txtQT1.Text);
                        //AnalysisTime.Add("Hydraulic Timestep", this.txtQT1.Text);
                        AnalysisTime.Add("Report Timestep", this.txtRT1.Text);
                        AnalysisTime.Add("Duration", this.txtDR1.Text);
                        AnalysisTime.Add("Report Start", this.txtRS1.Text);
                        break;

                    case 3:
                        //AnalysisTime.Add("Quality Timestep", this.txtQT2.Text);
                        //AnalysisTime.Add("Hydraulic Timestep", this.txtQT2.Text);
                        AnalysisTime.Add("Report Timestep", this.txtRT2.Text);
                        AnalysisTime.Add("Duration", this.txtDR2.Text);
                        AnalysisTime.Add("Report Start", this.txtRS2.Text);
                        break;
                }

                AnalysisOption.Add("Quality", "AGE");
                //AnalysisOption.Add("Units", "CMD"); //칠보에서 에러가 나서 뺌

                if (AnalysisTime.Count > 0)
                {
                    result.Add("time", AnalysisTime);
                }

                if (AnalysisOption.Count > 0)
                {
                    result.Add("option", AnalysisOption);
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
                throw e;
            }

            return result;
        }

        /// <summary>
        /// 관망해석 결과 중 체류시간 및 유량을 Select해서 Grid에 Set
        /// </summary>
        private void GetAnalysisResult(string strINP_NUM, string strMFTRIDN, string strFilter, UltraGrid oGrid, int iType)
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strRPT_NO = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strRPT_NO = this.GetReportNumberAfterAnalysis(strINP_NUM, strMFTRIDN);

            m_RPT_NO = strRPT_NO;

            if (strFilter == "") strFilter = "0";

            oStringBuilder.Remove(0, oStringBuilder.Length);

            switch (iType)
            {
                case 1: //체류시간 결과
                    oStringBuilder.AppendLine("SELECT   C.LOC_NAME || ' (' || C.LOC_CODE || ')' AS BLOCK, A.NODE_ID AS ID, A.QUALITY AS VALUE, UPPER(A.ANALYSIS_TYPE) AS ANALYSIS_TYPE");
                    oStringBuilder.AppendLine("FROM     WH_RPT_NODES A, WH_TAGS B, CM_LOCATION C");
                    oStringBuilder.AppendLine("WHERE    A.INP_NUMBER = '" + strINP_NUM + "' AND A.RPT_NUMBER = '" + strRPT_NO + "' AND TO_NUMBER(A.QUALITY) >= " + strFilter);
                    oStringBuilder.AppendLine("         AND SUBSTR(A.ANALYSIS_TIME, 0, INSTR(A.ANALYSIS_TIME, ':', 1, 1) - 1) = (SELECT MAX(TO_NUMBER(SUBSTR(X.ANALYSIS_TIME, 0, INSTR(X.ANALYSIS_TIME, ':', 1, 1) - 1))) FROM WH_RPT_NODES X WHERE X.INP_NUMBER = '" + strINP_NUM + "' AND X.RPT_NUMBER = '" + strRPT_NO + "') ");
                    oStringBuilder.AppendLine("         AND B.INP_NUMBER = A.INP_NUMBER AND B.ID = A.NODE_ID AND B.TYPE = 'NODE' ");
                    oStringBuilder.AppendLine("         AND C.FTR_CODE = SUBSTR(B.POSITION_INFO, INSTR(B.POSITION_INFO, '|', 1, 1) + 1, INSTR(B.POSITION_INFO, '|', 1, 2) - 1 - INSTR(B.POSITION_INFO, '|', 1, 1)) ");
                    oStringBuilder.AppendLine("         AND C.FTR_IDN = SUBSTR(B.POSITION_INFO, INSTR(B.POSITION_INFO, '|', 1, 2) + 1, INSTR(B.POSITION_INFO, '|', 1, 2)) ");
                    oStringBuilder.AppendLine("         AND C.PLOC_CODE = '" + strMFTRIDN + "' ");
                    oStringBuilder.AppendLine("ORDER BY BLOCK, TO_NUMBER(VALUE) DESC");
                    break;
                case 2: //유량 결과
                    oStringBuilder.AppendLine("SELECT   C.LOC_NAME || ' (' || C.LOC_CODE || ')' AS BLOCK, A.LINK_ID AS ID, D.NODE1, D.NODE2, A.FLOW AS VALUE, UPPER(A.ANALYSIS_TYPE) AS ANALYSIS_TYPE");
                    oStringBuilder.AppendLine("FROM     WH_RPT_LINKS A, WH_TAGS B, CM_LOCATION C, WH_PIPES D");
                    oStringBuilder.AppendLine("WHERE    A.INP_NUMBER = '" + strINP_NUM + "' AND A.RPT_NUMBER = '" + strRPT_NO + "' AND TO_NUMBER(A.FLOW) >= " + strFilter);
                    oStringBuilder.AppendLine("         AND SUBSTR(A.ANALYSIS_TIME, 0, INSTR(A.ANALYSIS_TIME, ':', 1, 1) - 1) = (SELECT MAX(TO_NUMBER(SUBSTR(X.ANALYSIS_TIME, 0, INSTR(X.ANALYSIS_TIME, ':', 1, 1) - 1))) FROM WH_RPT_NODES X WHERE X.INP_NUMBER = '" + strINP_NUM + "' AND X.RPT_NUMBER = '" + strRPT_NO + "') ");
                    oStringBuilder.AppendLine("         AND B.INP_NUMBER = A.INP_NUMBER AND B.ID = A.LINK_ID AND B.TYPE = 'LINK' ");
                    oStringBuilder.AppendLine("         AND C.FTR_CODE = SUBSTR(B.POSITION_INFO, INSTR(B.POSITION_INFO, '|', 1, 5) + 1, INSTR(B.POSITION_INFO, '|', 1, 6) - 1 - INSTR(B.POSITION_INFO, '|', 1, 5)) ");
                    oStringBuilder.AppendLine("         AND C.FTR_IDN = SUBSTR(B.POSITION_INFO, INSTR(B.POSITION_INFO, '|', 1, 6) + 1, INSTR(B.POSITION_INFO, '|', 1, 6)) ");
                    oStringBuilder.AppendLine("         AND C.PLOC_CODE = '" + strMFTRIDN + "' ");
                    oStringBuilder.AppendLine("         AND D.INP_NUMBER = A.INP_NUMBER AND D.ID = A.LINK_ID ");
                    oStringBuilder.AppendLine("ORDER BY BLOCK, TO_NUMBER(VALUE) DESC");
                    break;
            }
            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RESULT");

            oGrid.DataSource = pDS.Tables["RESULT"].DefaultView;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// INP NUMBER에 해당하는 LAYER를 MAP에 등록
        /// </summary>
        /// <param name="strINP_NUM"></param>
        private void LoadAnalysisLayer(string strINP_NUM)
        {
            try
            {
                //INP Number로 Shape을 생성
                IWorkspace pWorkspace = ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + strINP_NUM);
                if (pWorkspace == null)
                {
                    CreateINPLayerManager oINPLayer = new CreateINPLayerManager(strINP_NUM);
                    try
                    {
                        oINPLayer.StartEditor();
                        oINPLayer.CreateINP_Shape();
                        oINPLayer.StopEditor(true);
                    }
                    catch
                    {
                        oINPLayer.AbortEditor();
                        oINPLayer.StopEditor(false);
                    }
                }



                WaterAOCore.VariableManager.m_INPgraphic = WaterAOCore.ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + strINP_NUM);

                if (WaterAOCore.VariableManager.m_INPgraphic != null)
                {
                    this.RemoveAnalysisLayer();

                    ILayer pJTLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "JUNCTION", "JUNCTION");
                    ILayer pPPLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PIPE", "PIPE");
                    ILayer pPMLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PUMP", "PUMP");
                    ILayer pRRLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "RESERVOIR", "RESERVOIR");
                    ILayer pTNLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "TANK", "TANK");
                    ILayer pVVLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "VALVE", "VALVE");

                    IColor pLabelColor = null; ISymbol pMarkSymbol = null; ISymbol pLineSymbol = null;

                    if (pJTLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(System.Drawing.Color.DarkBlue.R, System.Drawing.Color.DarkBlue.G, System.Drawing.Color.DarkBlue.B);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pJTLayer, "EN_QUALITY", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(6, esriSimpleMarkerStyle.esriSMSCircle, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pJTLayer, pMarkSymbol);
                        this._Map.AddLayer(pJTLayer, 0);
                    }
                    if (pPPLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 110);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pPPLayer, "EN_FLOW", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pPPLayer, pLineSymbol);
                        this._Map.AddLayer(pPPLayer, 1);
                    }
                    if (pPMLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 222);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pPMLayer, "EN_FLOW", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pPMLayer, pLineSymbol);
                        this._Map.AddLayer(pPMLayer, 2);
                    }
                    if (pRRLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(System.Drawing.Color.DarkTurquoise.R, System.Drawing.Color.DarkTurquoise.G, System.Drawing.Color.DarkTurquoise.B);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pRRLayer, "EN_QUALITY", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(6, esriSimpleMarkerStyle.esriSMSDiamond, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pRRLayer, pMarkSymbol);
                        this._Map.AddLayer(pRRLayer, 3);
                    }
                    if (pTNLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(System.Drawing.Color.DarkRed.R, System.Drawing.Color.DarkRed.G, System.Drawing.Color.DarkRed.B);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pTNLayer, "EN_QUALITY", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(6, esriSimpleMarkerStyle.esriSMSSquare, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pTNLayer, pMarkSymbol);
                        this._Map.AddLayer(pTNLayer, 4);
                    }
                    if (pVVLayer != null)
                    {
                        pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 200, 0);
                        //WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pVVLayer, "EN_FLOW", "굴림", 8, true, pLabelColor, 0, 0, string.Empty);
                        pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pLabelColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pVVLayer, pLineSymbol);
                        this._Map.AddLayer(pVVLayer, 5);
                    }
                    this.m_MODEL_LAYERS.Clear();

                    this.m_MODEL_LAYERS.Add("junction", (ITable)pJTLayer);
                    this.m_MODEL_LAYERS.Add("pipe", (ITable)pPPLayer);
                    this.m_MODEL_LAYERS.Add("pump", (ITable)pPMLayer);
                    this.m_MODEL_LAYERS.Add("reservoir", (ITable)pRRLayer);
                    this.m_MODEL_LAYERS.Add("tank", (ITable)pTNLayer);
                    this.m_MODEL_LAYERS.Add("valve", (ITable)pVVLayer);

                    //this.ClearLayerDatas();
                }

                this._Map.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, null);
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
            }
        }

        /// <summary>
        /// 이미 등록된 INP_NO의 레이어를 리무브
        /// </summary>
        private void RemoveAnalysisLayer()
        {
            ArcManager.RemoveMapLayer(_Map, "JUNCTION");
            ArcManager.RemoveMapLayer(_Map, "PIPE");
            ArcManager.RemoveMapLayer(_Map, "PUMP");
            ArcManager.RemoveMapLayer(_Map, "RESERVOIR");
            ArcManager.RemoveMapLayer(_Map, "TANK");
            ArcManager.RemoveMapLayer(_Map, "VALVE");
        }

        /// <summary>
        /// 관망해석 완료 후 생성된 RPT_NO를 취득해서 반환
        /// </summary>
        /// <param name="strINP_NUM"></param>
        /// <param name="strMFTRIDN"></param>
        /// <returns></returns>
        private string GetReportNumberAfterAnalysis(string strINP_NUM, string strMFTRIDN)
        {
            string strRTN = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return string.Empty;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   RPT_NUMBER AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WH_RPT_MASTER");
            oStringBuilder.AppendLine("WHERE    MFTRIDN = '" + strMFTRIDN + "' AND INP_NUMBER = '" + strINP_NUM + "'");
            oStringBuilder.AppendLine("ORDER BY TO_DATE(RPT_DATE, 'RRRRMMDDHH24MISS') DESC");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WH_RPT_MASTER");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["RTNDATA"].ToString().Trim();
                    break;
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            return strRTN;
        }

        ///// <summary>
        ///// 지도상의 INP Layer의 실시간 감시항목 데이터를 전부 초기화한다.
        ///// </summary>
        //private void ClearLayerDatas()
        //{
        //    try
        //    {
        //        DataSet junctionList = this.SelectJunctionList();
        //        DataSet reservoirList = this.SelectReservoirList();
        //        DataSet tankList = this.SelectTankList();
        //        DataSet pipeList = this.SelectPipeList();
        //        DataSet pumpList = this.SelectPumpList();
        //        DataSet valveList = this.SelectValveList();

        //        ITable junctionTable = (ITable)this.m_MODEL_LAYERS["junction"];
        //        ITable tankTable = (ITable)this.m_MODEL_LAYERS["tank"];
        //        ITable reservoirTable = (ITable)this.m_MODEL_LAYERS["reservoir"];
        //        ITable pipeTable = (ITable)this.m_MODEL_LAYERS["pipe"];
        //        ITable pumpTable = (ITable)this.m_MODEL_LAYERS["pump"];
        //        ITable valveTable = (ITable)this.m_MODEL_LAYERS["valve"];

        //        UpdateNodeData(junctionTable, null, junctionList.Tables["WH_JUNCTIONS"], true);
        //        UpdateNodeData(tankTable, null, tankList.Tables["WH_TANKS"], true);
        //        UpdateNodeData(reservoirTable, null, reservoirList.Tables["WH_RESERVOIRS"], true);

        //        UpdateLinkData(pipeTable, null, pipeList.Tables["WH_PIPES"], true);
        //        UpdateLinkData(pumpTable, null, pumpList.Tables["WH_PUMPS"], true);
        //        UpdateLinkData(valveTable, null, valveList.Tables["WH_VALVES"], true);
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //}

        /// <summary>
        /// INP 파일에 해당하는 Junction List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectJunctionList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("SELECT      a.ID				                                ");
            queryString.AppendLine("            ,a.ELEV				                                ");
            queryString.AppendLine("            ,a.DEMAND				                            ");
            queryString.AppendLine("            ,a.PATTERN_ID			                            ");
            queryString.AppendLine("            ,a.SFTRIDN			                                ");
            queryString.AppendLine("            ,b.LOC_NAME             AS SFTR_NAME			    ");
            queryString.AppendLine("            ,a.REMARK				                            ");
            queryString.AppendLine("            ,''                     AS ACTION                   ");
            queryString.AppendLine("FROM        WH_JUNCTIONS            a		                    ");
            queryString.AppendLine("            ,CM_LOCATION            b                           ");
            queryString.AppendLine("WHERE       b.FTR_CODE(+)       = 'BZ003'                       ");
            queryString.AppendLine("            AND a.SFTRIDN       = b.FTR_IDN(+)                  ");
            queryString.AppendLine("            AND a.INP_NUMBER    = '" + this.m_INP_NO + "'            ");
            queryString.AppendLine("ORDER BY    a.ID				                                    ");

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_JUNCTIONS");
        }

        /// <summary>
        /// INP 파일에 해당하는 Reservoir List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectReservoirList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,HEAD                                                   ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_RESERVOIRS                                           ");
            queryString.AppendLine("where       INP_NUMBER      = '" + this.m_INP_NO + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RESERVOIRS");
        }

        /// <summary>
        /// INP 파일에 해당하는 Tank List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectTankList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,ELEV                                                   ");
            queryString.AppendLine("            ,INITLVL                                                ");
            queryString.AppendLine("            ,MINLVL                                                 ");
            queryString.AppendLine("            ,MAXLVL                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,MINVOL                                                 ");
            queryString.AppendLine("            ,VOLCURVE_ID                                            ");
            queryString.AppendLine("from        WH_TANK                                                 ");
            queryString.AppendLine("where       INP_NUMBER      = '" + this.m_INP_NO + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TANK");
        }

        /// <summary>
        /// INP 파일에 해당하는 Pipe List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectPipeList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,LENGTH                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,ROUGHNESS                                              ");
            queryString.AppendLine("            ,MLOSS                                                  ");
            queryString.AppendLine("            ,STATUS                                                 ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("            ,LEAKAGE_COEFFICIENT                                    ");
            queryString.AppendLine("from        WH_PIPES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + this.m_INP_NO + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PIPES");
        }

        /// <summary>
        /// INP 파일에 해당하는 Pump List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectPumpList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,PROPERTIES                                             ");
            queryString.AppendLine("from        WH_PUMPS                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + this.m_INP_NO + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PUMPS");
        }

        /// <summary>
        /// INP 파일에 해당하는 Valve List
        /// </summary>
        /// <returns></returns>
        private DataSet SelectValveList()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,DIAMETER                                               ");
            queryString.AppendLine("            ,TYPE                                                   ");
            queryString.AppendLine("            ,SETTING                                                ");
            queryString.AppendLine("            ,MINORLOSS                                              ");
            queryString.AppendLine("from        WH_VALVES                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + this.m_INP_NO + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VALVES");
        }

        private void UpdateNodeData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        {
            EAGL.Data.Wrapper.DataTableWrapper tW = new EAGL.Data.Wrapper.DataTableWrapper();
            ITable nodetable = tW.CreateITable(oDataTable);

            EAGL.Join.AttributeJoin.GetInstance().RemoveAllJoins(oTable);
            EAGL.Join.AttributeJoin.GetInstance().getJoinedLayer(oTable, nodetable, "NODE_ID", "ID", esriRelCardinality.esriRelCardinalityOneToOne);
        }

        private void UpdateLinkData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        {
            EAGL.Data.Wrapper.DataTableWrapper tW = new EAGL.Data.Wrapper.DataTableWrapper();
            ITable linktable = tW.CreateITable(oDataTable);

            EAGL.Join.AttributeJoin.GetInstance().RemoveAllJoins(oTable);
            EAGL.Join.AttributeJoin.GetInstance().getJoinedLayer(oTable, linktable, "LINK_ID", "ID", esriRelCardinality.esriRelCardinalityOneToOne);
        }

        ///// <summary>
        ///// 수리모델 상 절점에 해석결과를 표출
        ///// </summary>
        ///// <param name="oTable"></param>
        ///// <param name="oQueryFilter"></param>
        ///// <param name="oDataTable"></param>
        ///// <param name="Initialize"></param>
        //private void UpdateNodeData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        //{
        //    ICursor pCusror = oTable.Update(oQueryFilter, true);
        //    IRow oRow = null;
        //    string idField = "";

        //    if (Initialize)
        //    {
        //        idField = "ID";
        //    }
        //    else
        //    {
        //        idField = "NODE_ID";
        //    }

        //    try
        //    {
        //        while ((oRow = pCusror.NextRow()) != null)
        //        {
        //            DataRow nRow = getDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), oDataTable, idField);
        //            if (nRow != null)
        //            {
        //                if (Initialize)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ELEVATI"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_BASEDEM"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PATTERN"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_EMITTER"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITQUA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEQ"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEP"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCET"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKLEV"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DEMAND"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEAD"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_QUALITY"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEM"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITVOL"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXMODE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXZONE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKDIA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINVOLU"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VOLCURV"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINLEVE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MAXLEVE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXFRAC"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANK_KB"), "0");
        //                }
        //                else
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ELEVATI"), nRow["ELEVATION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_BASEDEM"), nRow["BASEDEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PATTERN"), nRow["PATTERN"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_EMITTER"), nRow["EMITTER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITQUA"), nRow["INITQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEQ"), nRow["SOURCEQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEP"), nRow["SOURCEPAT"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCET"), nRow["SOURCETYPE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKLEV"), nRow["TANKLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DEMAND"), nRow["DEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEAD"), nRow["HEAD"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), nRow["PRESSURE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_QUALITY"), nRow["QUALITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEM"), nRow["SOURCEMASS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITVOL"), nRow["INITVOLUME"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXMODE"), nRow["MIXMODEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXZONE"), nRow["MIXZONEVOL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKDIA"), nRow["TANKDIAM"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINVOLU"), nRow["MINVOLUME"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VOLCURV"), nRow["VOLCURVE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINLEVE"), nRow["MINLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MAXLEVE"), nRow["MAXLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXFRAC"), nRow["MIXFRACTION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANK_KB"), nRow["TANK_KBULK"]);
        //                }

        //                pCusror.UpdateRow(oRow);
        //            }
        //        }
        //        pCusror.Flush();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        Marshal.ReleaseComObject(pCusror);
        //    }
        //}

        ///// <summary>
        ///// 수리모델 상 링크에 해석결과를 표출
        ///// </summary>
        ///// <param name="oTable"></param>
        ///// <param name="oQueryFilter"></param>
        ///// <param name="oDataTable"></param>
        ///// <param name="Initialize"></param>
        //private void UpdateLinkData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        //{
        //    ICursor pCusror = oTable.Update(oQueryFilter, true);
        //    IRow oRow = null;
        //    string idField = "";

        //    if (Initialize)
        //    {
        //        idField = "ID";
        //    }
        //    else
        //    {
        //        idField = "LINK_ID";
        //    }

        //    try
        //    {
        //        while ((oRow = pCusror.NextRow()) != null)
        //        {
        //            DataRow nRow = getDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), oDataTable, idField);
        //            if (nRow != null)
        //            {
        //                if (Initialize)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DIAMETE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_LENGTH"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ROUGHNE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINORLO"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSTA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSET"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KBULK"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KWALL"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_FLOW"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VELOCIT"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEADLOS"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_STATUS"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SETTING"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ENERGY"), "0");
        //                }
        //                else
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DIAMETE"), nRow["DIAMETER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_LENGTH"), nRow["LENGTH"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ROUGHNE"), nRow["ROUGHNESS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINORLO"), nRow["MINORLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSTA"), nRow["INITSTATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSET"), nRow["INITSETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KBULK"), nRow["KBULK"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KWALL"), nRow["KWALL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_FLOW"), nRow["FLOW"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VELOCIT"), nRow["VELOCITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEADLOS"), nRow["HEADLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_STATUS"), nRow["STATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SETTING"), nRow["SETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ENERGY"), nRow["ENERGY"]);
        //                }

        //                pCusror.UpdateRow(oRow);
        //            }
        //        }
        //        pCusror.Flush();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        Marshal.ReleaseComObject(pCusror);
        //    }
        //}

        private DataRow getDataRow(string ID, DataTable oDataTable, string dataID)
        {
            DataRow oRow = null;
            DataRow[] oRows = oDataTable.Select(dataID + " = '" + ID + "'");
            if (oRows.Length == 1)
            {
                oRow = oRows[0];
            }
            return oRow;
        }

        /// <summary>
        /// 해당시간의 관망해석 결과를 지도상에 표출한다.
        /// </summary>
        private void SetLayerDatas()
        {
            ITable junctionTable = (ITable)this.m_MODEL_LAYERS["junction"];
            ITable tankTable = (ITable)this.m_MODEL_LAYERS["tank"];
            ITable reservoirTable = (ITable)this.m_MODEL_LAYERS["reservoir"];
            ITable pipeTable = (ITable)this.m_MODEL_LAYERS["pipe"];
            ITable pumpTable = (ITable)this.m_MODEL_LAYERS["pump"];
            ITable valveTable = (ITable)this.m_MODEL_LAYERS["valve"];

            switch (this.utabRTMonitorPoint.ActiveTab.Index)
            {
                case 1:
                    DataSet nodeDataSet = this.GetAnalysisNodeResultData(txtRT1.Text, txtAFilter1.Text);

                    UpdateNodeData(junctionTable, null, nodeDataSet.Tables["WH_RPT_NODES"], false);

                    SetLabelProperty((IFeatureLayer)this.m_MODEL_LAYERS["junction"], 0, txtAFilter1.Text);
                    //SetLabelProperty((IFeatureLayer)this.m_MODEL_LAYERS["pipe"], 1, "99999999999");
                    break;

                case 3:
                    DataSet linkDataSet = this.GetAnalysisLinkResultData(txtRT2.Text, txtAFilter2.Text);
                    
                    UpdateLinkData(pipeTable, null, linkDataSet.Tables["WH_RPT_LINKS"], false);

                    //SetLabelProperty((IFeatureLayer)this.m_MODEL_LAYERS["junction"], 0, "99999999999");
                    SetLabelProperty((IFeatureLayer)this.m_MODEL_LAYERS["pipe"], 1, txtAFilter2.Text);
                    break;
            }
        }

        /// <summary>
        /// 수압값에 따라 색상을 다르게 렌더링한다.
        /// </summary>
        /// <param name="pFeatureLayer"></param>
        private void SetLayerRenderer(IFeatureLayer pFeatureLayer, int iType, string strFilter)
        {
            //if (!(pFeatureLayer is IGeoFeatureLayer)) return;

            //using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            //{
            //    string strField = string.Empty;
            //    IQueryFilter pQueryFilter = new QueryFilterClass();

            //    if (iType == 0) //NODE (체류시간)
            //    {
            //        strField = "EN_QUALITY";
            //    }
            //    else // LINK (유량)
            //    {
            //        strField = "EN_FLOW";
            //    }

            //    pQueryFilter.WhereClause = "(" + strField + " >= " + strFilter + ")";

            //    IColor pColor1 = ArcManager.GetColor(Color.Blue);
            //    ISymbol pSymbol1 = ArcManager.MakeCharacterMarkerSymbol(9, 36, 0.0, pColor1);
            //    IColor pColor2 = ArcManager.GetColor(Color.Red);
            //    ISymbol pSymbol2 = ArcManager.MakeCharacterMarkerSymbol(10, 36, 0.0, pColor2);

            //    IUniqueValueRenderer pUniqueRenderer = new UniqueValueRendererClass();
            //    pUniqueRenderer.FieldCount = 1;
            //    pUniqueRenderer.set_Field(0, strField);

            //    IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;
            //    System.Collections.IEnumerator pEnumVar = ArcManager.GetSortedUniqueValues((ITable)pFeatureClass, strField, pQueryFilter);
            //    pEnumVar.Reset();

            //    double sValue;
            //    while (pEnumVar.MoveNext())
            //    {
            //        sValue = Convert.ToDouble(pEnumVar.Current);
            //        if (sValue >= Convert.ToDouble(strFilter))
            //        {
            //            pUniqueRenderer.AddValue(sValue.ToString(), strField, pSymbol2);
            //            pUniqueRenderer.set_Symbol(sValue.ToString(), pSymbol2);
            //            pUniqueRenderer.set_Label(sValue.ToString(), strFilter + "이상");
            //        }
            //    }

            //    if (pUniqueRenderer.ValueCount == 0) return;

            //    ((IGeoFeatureLayer)pFeatureLayer).Renderer = pUniqueRenderer as IFeatureRenderer;
            //}

        }

        /// <summary>
        /// 수압값을 라벨로 지도상에 표시한다.
        /// </summary>
        /// <param name="pFeatureLayer"></param>
        private void SetLabelProperty(IFeatureLayer pFeatureLayer, int iType, string strFilter)
        {
            if (!(pFeatureLayer is IGeoFeatureLayer)) return;
            //IFields fields = ((IGeoFeatureLayer)pFeatureLayer).DisplayFeatureClass.Fields;
            //for (int i = 0; i < fields.FieldCount; i++)
            //{
            //    Console.WriteLine(fields.get_Field(i).Name);
            //}
            string strField = string.Empty;
            IQueryFilter pQueryFilter = new QueryFilterClass();

            if (iType == 0) //NODE (체류시간)
            {
                strField = "WH_RPT_NODES.QUALITY";
            }
            else // LINK (유량)
            {
                strField = "WH_RPT_LINKS.FLOW";
            }

            pQueryFilter.WhereClause = "(" + strField + " >= " + strFilter + ")";

            IGeoFeatureLayer pGeoFeatureLayer = pFeatureLayer as IGeoFeatureLayer;
            pGeoFeatureLayer.DisplayField = strField;

            //라벨의 특성(위치, Conflict 등등)
            IBasicOverposterLayerProperties pBOLayerProps = new BasicOverposterLayerPropertiesClass();
            pBOLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPoint;
            pBOLayerProps.NumLabelsOption = esriBasicNumLabelsOption.esriOneLabelPerShape;
            pBOLayerProps.FeatureWeight = esriBasicOverposterWeight.esriNoWeight;
            pBOLayerProps.LabelWeight = esriBasicOverposterWeight.esriLowWeight;
            pBOLayerProps.GenerateUnplacedLabels = true;

            //라벨 폰트 & 칼라 정의
            stdole.StdFont pFont = new stdole.StdFontClass();
            pFont.Bold = true;
            pFont.Name = "굴림";    //굴림
            pFont.Size = 10;

            IAnnotateLayerPropertiesCollection pAnnoLayerPropsColl = new AnnotateLayerPropertiesCollection();

            ////항목이 필터이상 - RED
            IFormattedTextSymbol pText2 = new TextSymbolClass();
            pText2.Font = (stdole.IFontDisp)pFont;
            pText2.Color = ArcManager.GetColor(Color.Red);

            IAnnotateLayerProperties pAnnoLayerProps;
            pAnnoLayerProps = new LabelEngineLayerPropertiesClass();
            pAnnoLayerProps.Class = "Higher";
            pAnnoLayerProps.AnnotationMinimumScale = 0;
            pAnnoLayerProps.AnnotationMaximumScale = 0;
            pAnnoLayerProps.DisplayAnnotation = true;
            //어느 Feature를 Labeling할 것인가?
            pAnnoLayerProps.WhereClause = "(" + strField + " >= " + strFilter + ")";

            ILabelEngineLayerProperties pLabelEng2 = pAnnoLayerProps as ILabelEngineLayerProperties;
            pLabelEng2.Symbol = pText2;
            pLabelEng2.Expression = "[" + strField + "]";
            pLabelEng2.BasicOverposterLayerProperties = pBOLayerProps;

            pAnnoLayerPropsColl.Add(pLabelEng2 as IAnnotateLayerProperties);

            ////항목이 필터이하 - 안보이게
            IFormattedTextSymbol pText3 = new TextSymbolClass();
            pText3.Font = (stdole.IFontDisp)pFont;
           // pText3.Color = ArcManager.GetColor(Color.Red);

            pAnnoLayerProps = new LabelEngineLayerPropertiesClass();
            pAnnoLayerProps.Class = "Lower";
            pAnnoLayerProps.AnnotationMinimumScale = 0;
            pAnnoLayerProps.AnnotationMaximumScale = 0;
            pAnnoLayerProps.DisplayAnnotation = false;
            //어느 Feature를 Labeling할 것인가?
            pAnnoLayerProps.WhereClause = "(" + strField + " < " + strFilter + ")";

            ILabelEngineLayerProperties pLabelEng3 = pAnnoLayerProps as ILabelEngineLayerProperties;
            pLabelEng3.Symbol = pText3;
            pLabelEng3.Expression = "[" + strField + "]";
            pLabelEng3.BasicOverposterLayerProperties = pBOLayerProps;

            pAnnoLayerPropsColl.Add(pLabelEng3 as IAnnotateLayerProperties);

            pGeoFeatureLayer.AnnotationProperties = pAnnoLayerPropsColl;
            pGeoFeatureLayer.DisplayAnnotation = true;
        }

        /// <summary>
        /// 특정 시간의 절점 해석결과를 조회
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="strFilter"></param>
        /// <returns></returns>
        private DataSet GetAnalysisNodeResultData(string strTime, string strFilter)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.RPT_NUMBER			                                                                                        ");
            queryString.AppendLine("                ,a.NODE_ID			                                                                                            ");
            queryString.AppendLine("                ,a.RPT_NUMBER				                                                                                    ");
            queryString.AppendLine("                ,a.ANALYSIS_TIME			                                                                                    ");
            queryString.AppendLine("                ,a.ELEVATION				                                                                                    ");
            queryString.AppendLine("                ,a.BASEDEMAND				                                                                                    ");
            queryString.AppendLine("                ,a.PATTERN				                                                                                        ");
            queryString.AppendLine("                ,a.EMITTER				                                                                                        ");
            queryString.AppendLine("                ,a.INITQUAL				                                                                                        ");
            queryString.AppendLine("                ,a.SOURCEQUAL				                                                                                    ");
            queryString.AppendLine("                ,a.SOURCEPAT				                                                                                    ");
            queryString.AppendLine("                ,a.SOURCETYPE				                                                                                    ");
            queryString.AppendLine("                ,a.TANKLEVEL				                                                                                    ");
            queryString.AppendLine("                ,a.DEMAND					                                                                                    ");
            queryString.AppendLine("                ,a.HEAD					                                                                                        ");
            queryString.AppendLine("                ,a.PRESSURE				                                                                                        ");
            queryString.AppendLine("                ,a.QUALITY				                                                                                        ");
            queryString.AppendLine("                ,a.SOURCEMASS				                                                                                    ");
            queryString.AppendLine("                ,a.INITVOLUME				                                                                                    ");
            queryString.AppendLine("                ,a.MIXMODEL				                                                                                        ");
            queryString.AppendLine("                ,a.MIXZONEVOL				                                                                                    ");
            queryString.AppendLine("                ,a.TANKDIAM				                                                                                        ");
            queryString.AppendLine("                ,a.MINVOLUME				                                                                                    ");
            queryString.AppendLine("                ,a.VOLCURVE				                                                                                        ");
            queryString.AppendLine("                ,a.MINLEVEL				                                                                                        ");
            queryString.AppendLine("                ,a.MAXLEVEL				                                                                                        ");
            queryString.AppendLine("                ,a.MIXFRACTION				                                                                                    ");
            queryString.AppendLine("                ,a.TANK_KBULK				                                                                                    ");
            queryString.AppendLine("                ,a.ANALYSIS_TYPE			                                                                                    ");
            queryString.AppendLine("from            WH_RPT_NODES           a			                                                                            ");
            queryString.AppendLine("where           1 = 1			                                                                                                ");
            queryString.AppendLine("and             a.RPT_NUMBER   = '" + this.m_RPT_NO + "'		                                                    ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + this.m_INP_NO + "'                                                            ");
            queryString.AppendLine("and             a.ANALYSIS_TIME = '" + strTime + "'                                                                            ");
            queryString.AppendLine("and             TO_NUMBER(a.QUALITY) >= " + strFilter);

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_NODES");
        }

        /// <summary>
        /// 특정 시간의 링크 해석결과를 조회
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="strFilter"></param>
        /// <returns></returns>
        private DataSet GetAnalysisLinkResultData(string strTime, string strFilter)
        {
            StringBuilder queryString = new StringBuilder();

            //마곡 1블럭만 출력
            queryString.AppendLine("select          a.RPT_NUMBER			                                                                                        ");
            queryString.AppendLine("                ,a.LINK_ID					                                                                                    ");
            queryString.AppendLine("                ,a.ANALYSIS_TIME				                                                                                ");
            queryString.AppendLine("                ,a.ANALYSIS_TYPE				                                                                                ");
            queryString.AppendLine("                ,a.DIAMETER				                                                                                        ");
            queryString.AppendLine("                ,a.LENGTH					                                                                                    ");
            queryString.AppendLine("                ,a.ROUGHNESS				                                                                                    ");
            queryString.AppendLine("                ,a.MINORLOSS				                                                                                    ");
            queryString.AppendLine("                ,a.INITSTATUS				                                                                                    ");
            queryString.AppendLine("                ,a.INITSETTING				                                                                                    ");
            queryString.AppendLine("                ,a.KBULK					                                                                                    ");
            queryString.AppendLine("                ,a.KWALL					                                                                                    ");
            queryString.AppendLine("                ,a.FLOW					                                                                                        ");
            queryString.AppendLine("                ,a.VELOCITY				                                                                                        ");
            queryString.AppendLine("                ,a.HEADLOSS				                                                                                        ");
            queryString.AppendLine("                ,a.STATUS					                                                                                    ");
            queryString.AppendLine("                ,a.SETTING					                                                                                    ");
            queryString.AppendLine("                ,a.ENERGY					                                                                                    ");
            queryString.AppendLine("from            WH_RPT_LINKS          a			                                                                            ");
            queryString.AppendLine("where           1 = 1			                                                                                                ");
            queryString.AppendLine("and             a.RPT_NUMBER   = '" + this.m_RPT_NO + "'		                                                    ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + this.m_INP_NO + "'                                                            ");
            queryString.AppendLine("and             a.ANALYSIS_TIME = '" + strTime + "'                                                                            ");
            queryString.AppendLine("and             TO_NUMBER(a.FLOW) >= " + strFilter);

            return m_oDBManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_LINKS");
        }

        /// <summary>
        /// 해석결과 그리드 클릭시 맵 이동
        /// </summary>
        /// <param name="strShape"></param>
        private void Viewmap_AnalysisResult(string strShape, string strKey, string strKeyName)
        {
            double dblMAP_SCALE = 2000;

            if (_Map != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(_Map, strShape);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strKeyName + "='" + strKey + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (_Map != null)
                {
                    if (pFeature != null)
                    {
                        IGeometry oIGeometry = pFeature.Shape;

                        WQ_AppStatic.CURRENT_IGEOMETRY = oIGeometry;

                        ArcManager.FlashShape(_Map.ActiveView, oIGeometry, 10);

                        if (ArcManager.GetMapScale(_Map) > dblMAP_SCALE)
                        {
                            ArcManager.SetMapScale(_Map, dblMAP_SCALE);
                        }

                        ArcManager.MoveCenterAt(_Map, pFeature);

                        ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                        Application.DoEvents();
                    }
                    else
                    {
                        WQ_AppStatic.CURRENT_IGEOMETRY = null;
                    }
                    this.BringToFront();
                }
                else
                {
                    WQ_AppStatic.CURRENT_IGEOMETRY = null;
                }
            }
        }

        /// <summary>
        /// INP Number로 해당 INP의 배수지 FTR IDN을 찾아 리턴
        /// </summary>
        /// <returns></returns>
        private string GetFTRIDNFromINP()
        {
            string strRTN = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return string.Empty;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);

            //가상 배수지인 경우 CM_LOCATION에 없으므로 조건은 무의미
            //oStringBuilder.AppendLine("SELECT   SUBSTR(A.ID, 2) AS RTNDATA");
            //oStringBuilder.AppendLine("FROM     WH_RESERVOIRS A, CM_LOCATION B");
            //oStringBuilder.AppendLine("WHERE    A.INP_NUMBER = '" + this.m_INP_NO + "' AND B.FTR_IDN = SUBSTR(A.ID, 2)");

            oStringBuilder.AppendLine("SELECT   A.ID AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WH_RESERVOIRS A");
            oStringBuilder.AppendLine("WHERE    A.INP_NUMBER = '" + this.m_INP_NO + "'");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WH_RESERVOIRS");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["RTNDATA"].ToString().Trim();
                    break;
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            return strRTN;
        }

        #endregion

        #region - Map Function

        /// <summary>
        /// 맵 이동
        /// </summary>
        /// <param name="strLOC_CODE">중, 소블록 LOC_CODE</param>
        /// <param name="iType">0 : 중블록, 1 : 소블록</param>
        private void Viewmap_MonitorBlock(string strLOC_CODE, int iType)
        {
            string strFTR_IDN = string.Empty;

            IFeatureLayer pFeatureLayer = default(IFeatureLayer);

            if (iType == 0)
            {
                pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(_Map, "중블록");
                strFTR_IDN = WQ_Function.GetMediumBlockFTR_IDN(strLOC_CODE);
            }
            else
            {
                pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(_Map, "소블록");
                strFTR_IDN = WQ_Function.GetSmallBlockFTR_IDN(strLOC_CODE);
            }

            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + strFTR_IDN + "'");
            if (pFeature == null) return;

            _Map.Extent = pFeature.Shape.Envelope;
            _Map.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);
        }

        /// <summary>
        /// 감시지점, 중요시설, 수질민원 그리드 클릭시 맵 이동
        /// </summary>
        /// <param name="strShape"></param>
        private void Viewmap_GridPoint(string strShape, string strKey, string strKeyName)
        {
            if (_Map != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(_Map, strShape);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strKeyName + "='" + strKey + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    IGeometry oIGeometry = pFeature.Shape;

                    WQ_AppStatic.CURRENT_IGEOMETRY = oIGeometry;

                    ArcManager.FlashShape(_Map.ActiveView, oIGeometry, 10);

                    if (ArcManager.GetMapScale(_Map) > (double)1000)
                    {
                        ArcManager.SetMapScale(_Map, (double)1000);
                    }

                    ArcManager.MoveCenterAt(_Map, pFeature);

                    ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                    Application.DoEvents();
                }
                else
                {
                    WQ_AppStatic.CURRENT_IGEOMETRY = null;
                }
                this.BringToFront();
            }
            else
            {
                WQ_AppStatic.CURRENT_IGEOMETRY = null;
            }
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// 저장하는 Data를 검증한다.
        /// </summary>
        /// <param name="iValType">Validation Type. 0 : Save, 1 : Delete</param>
        /// <returns>True / False</returns>
        private bool IsValidation(int iValType)
        {
            bool blRtn = true;
            
            switch (iValType)
            {
                case 0: //Save Validation
                    bool blChk = false;

                    if (this.cboSBlockM.Text == "") blRtn = false;
                    if (this.cboPartitionM.Text == "") blRtn = false;
                    if (this.txtGPosName.Text.Trim() == "") blRtn = false;
                    if (this.txtGPosNo.Text.Trim() == "") blRtn = false;

                    if (this.utabRTMonitorPoint.ActiveTab.Index != 2)
                    {
                        if (this.txtNODEID.Text.Trim() == "") blRtn = false;
                    }

                    if (blChk == false) blChk = this.chkGItem1.Checked;
                    if (blChk == false) blChk = this.chkGItem2.Checked;
                    if (blChk == false) blChk = this.chkGItem3.Checked;
                    if (blChk == false) blChk = this.chkGItem4.Checked;
                    if (blChk == false) blChk = this.chkGItem5.Checked;

                    //if (this.cboTAGCL.Text == "") blRtn = false;
                    //if (this.cboTAGPH.Text == "") blRtn = false;
                    //if (this.cboTAGTB.Text == "") blRtn = false;
                    //if (this.cboTAGTE.Text == "") blRtn = false;
                    //if (this.cboTAGCU.Text == "") blRtn = false;

                    if (blChk == false) blRtn = false;
                    
                    break;

                case 1: //Delete Validation
                    if (this.cboSBlockM.Text == "") blRtn = false;
                    if (this.cboPartitionM.Text == "") blRtn = false;
                    if (this.txtGPosNo.Text.Trim() == "") blRtn = false;

                    break;

                default:
                    blRtn = false;
                    break;
            }

            if (blRtn == false)
            {
                MessageBox.Show("감시지점 정보를 입력(또는 선택)해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return blRtn;
        }

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = false;
            this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        #endregion
        
        #endregion
    }
}
