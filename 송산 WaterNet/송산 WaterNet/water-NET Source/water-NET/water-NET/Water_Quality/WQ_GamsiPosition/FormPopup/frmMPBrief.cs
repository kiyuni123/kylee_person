﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;

namespace WaterNet.WQ_GamsiPosition.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A03
    /// Project Explain : 감시지점선정
    /// Project Developer : 오두석
    /// Project Create Date : 2010.11.17
    /// Form Explain : 감시지점에 지점의 간략 정보 Popup Form
    /// </summary>
    public partial class frmMPBrief : Form
    {
        private OracleDBManager m_oDBManager = null;

        private string _NO;
        private string _NAME;

        public string NO
        {
            get
            {
                return _NO;
            }
            set
            {
                _NO = value;
            }
        }

        public string NAME
        {
            get
            {
                return _NAME;
            }
            set
            {
                _NAME = value;
            }
        }

        public frmMPBrief()
        {
            InitializeComponent();
        }

        private void frmMPBrief_Load(object sender, EventArgs e)
        {
            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }

            this.GetMPBriefData();
        }

        private void GetMPBriefData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   B.CODE_NAME || '선정지점' AS MONPNT_GBN, TO_DATE(A.SELECT_DATE, 'RRRR-MM-DD') AS SELECT_DATE");
            oStringBuilder.AppendLine("FROM     WQ_RT_MONITOR_POINT A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE B ON B.PCODE = '3002' AND B.CODE = A.MONPNT_GBN");
            oStringBuilder.AppendLine("WHERE    A.MONITOR_NO = '" + _NO + "'");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "MP");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.lblNM.Text = _NAME;
                    this.txtNO.Text = _NO;
                    this.txtGBN.Text = oDRow["MONPNT_GBN"].ToString();
                    this.txtDT.Text = oDRow["SELECT_DATE"].ToString(); 
                }
            }
        }
    }
}
