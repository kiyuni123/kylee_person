﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_GamsiPosition.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A02
    /// Project Explain : 중요시설선정
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.28
    /// Form Explain : 중요시설 Popup Form
    /// </summary>
    public partial class frmWQPFacilities : Form
    {
        private OracleDBManager m_oDBManager = null;

        #region 맵 프로퍼티

        private IMapControl3 _Map;

        public IMapControl3 IMAP
        {
            get
            {
                return _Map;
            }
            set
            {
                _Map = value;
            }
        }

        #endregion

        public frmWQPFacilities()
        {
            WQ_AppStatic.IS_SHOW_FORM_FACILITIES = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                WQ_AppStatic.IS_SHOW_FORM_FACILITIES = false;
                this.Close();
                return;
            }

            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region Grid Set

            #region uGridPopup : 중요시설 Grid

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RANK";
            oUltraGridColumn.Header.Caption = "순위";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SBLOCK_CODE";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 154;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FACILITIES_GBN";
            oUltraGridColumn.Header.Caption = "시설구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FACILITIES_NAME";
            oUltraGridColumn.Header.Caption = "시설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 200;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NO";
            oUltraGridColumn.Header.Caption = "시설일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 130;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPopup);

            #endregion

            //ActiionKeyMapping();

            #endregion          
        }

        private void frmWQPFacilities_Load(object sender, EventArgs e)
        {
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);

            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================수질관리(중요시설 선정)

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간감시관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            //===========================================================================
        }

        private void frmWQPFacilities_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_FACILITIES = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetQueryData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.txtFacName.Text = "";
            this.txtFacNo.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() == false) return;

            this.txtFacNo.Text = WQ_Function.GenerationWQSerialNumber("IF");

            if (_Map != null && WQ_AppStatic.CURRENT_IGEOMETRY != null)
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                this.SaveFacilitiesData();

                ILayer pLayer = ArcManager.GetMapLayer(_Map, "중요시설");

                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeature pFeature = pFeatureClass.CreateFeature();

                //Shpae에 지점 등록
                pFeature.Shape = WQ_AppStatic.CURRENT_IGEOMETRY;

                //등록된 지점에 밸류 등록
                ArcManager.SetValue(pFeature, "SBLOCK", (object)WQ_Function.SplitToCode(this.cboSBlockM.Text));
                ArcManager.SetValue(pFeature, "GBN", (object)WQ_Function.SplitToCode(this.cboPartitionM.Text));
                ArcManager.SetValue(pFeature, "NOS", (object)this.txtFacNo.Text.Trim());
                ArcManager.SetValue(pFeature, "NAMES", (object)this.txtFacName.Text.Trim());

                //Shape 저장
                pFeature.Store();

                //GIS Map Refresh
                //WQ_AppStatic.ITOC.Update();
                ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();

                this.GetQueryData();

                this.txtFacName.Text = "";
                this.txtFacNo.Text = "";

                this.Cursor = System.Windows.Forms.Cursors.Default;

                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() == false) return;

            DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oDRtn == DialogResult.Yes)
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                this.DeleteFacilitiesData();

                if (_Map != null)
                {
                    ILayer pLayer = ArcManager.GetMapLayer(_Map, "중요시설");
                    IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                    IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "NOS='" + this.txtFacNo.Text + "'") as IFeatureCursor;

                    IFeature pFeature = pCursor.NextFeature();

                    if (pFeature != null)
                    {
                        pFeature.Delete();

                        //GIS Map Refresh
                        //WQ_AppStatic.ITOC.Update();
                        ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                        Application.DoEvents();

                        this.GetQueryData();

                        this.txtFacName.Text = "";
                        this.txtFacNo.Text = "";

                        MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    this.BringToFront();
                }

                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }
        
        #endregion


        #region Control Events

        private void uGridPopup_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            WQ_Function.SetCombo_SelectFindData(this.cboSBlockM, this.uGridPopup.ActiveRow.Cells[1].Text);
            string strMBLOCKCD = WQ_Function.GetMediumBlockBySmallBlockCode(WQ_Function.SplitToCode(this.uGridPopup.ActiveRow.Cells[1].Text));
            string strMBLOCK = WQ_Function.GetBlockNameCodeByFTR_IDN(WQ_Function.GetMediumBlockFTR_IDN(strMBLOCKCD));
            WQ_Function.SetCombo_SelectFindData(this.cboMBlock, strMBLOCK);
            WQ_Function.SetCombo_SelectFindData(this.cboPartitionM, this.uGridPopup.ActiveRow.Cells[2].Text);
            this.txtFacName.Text = this.uGridPopup.ActiveRow.Cells[3].Text;
            this.txtFacNo.Text = this.uGridPopup.ActiveRow.Cells[4].Text;
            this.txtRank.Text = this.uGridPopup.ActiveRow.Cells[0].Text;
        }

        private void uGridPopup_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            if (_Map != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(_Map, "중요시설");
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "NOS='" + this.txtFacNo.Text + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    IGeometry oIGeometry = pFeature.Shape;

                    WQ_AppStatic.CURRENT_IGEOMETRY = oIGeometry;

                    ArcManager.FlashShape(_Map.ActiveView, oIGeometry, 10);

                    if (ArcManager.GetMapScale(_Map) > (double)1000)
                    {
                        ArcManager.SetMapScale(_Map, (double)1000);
                    }

                    ArcManager.MoveCenterAt(_Map, pFeature);

                    ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                    Application.DoEvents();
                }
                else
                {
                    WQ_AppStatic.CURRENT_IGEOMETRY = null;
                }
                this.BringToFront();
            }
            else
            {
                WQ_AppStatic.CURRENT_IGEOMETRY = null;
            }
        }

        #region - Combo

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlockM, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 0);
            //this.Viewmap_MonitorBlock(strMFTRIDN, 0);
        }
        
        private void cboSBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            WQ_Function.SetCombo_FacilitiesPartition(this.cboPartition, 1);
            WQ_Function.SetCombo_FacilitiesPartition(this.cboPartitionM, 0);
            string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlock.Text);
            if (this.cboSBlockM.Items.Count > 0)
            {
                this.cboSBlockM.SelectedIndex = this.cboSBlock.SelectedIndex - 1;
            }
            //this.Viewmap_MonitorBlock(strSFTRIDN, 1);
        }

        private void cboSBlockM_SelectedIndexChanged(object sender, EventArgs e)
        {
            WQ_Function.SetCombo_FacilitiesPartition(this.cboPartitionM, 0);
            string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlockM.Text);
            //this.Viewmap_MonitorBlock(strSFTRIDN, 1);
        }
        
        private void cboPartition_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlock.Text);
            string strIF_GBN = WQ_Function.SplitToCode(this.cboPartition.Text).Trim();
            WQ_Function.SetCombo_FacilitiesPosition(this.cboFacName, strSFTRIDN, strIF_GBN, true);
        }

        #endregion

        private void txtRank_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자, 백스페이스, 엔터만 허용
            if ((Char.IsDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }
        }

        #endregion


        #region User Function

        #region - SQL Function

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetQueryData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            string strFACILITIES_GBN = string.Empty;
            string strFACILITIES_NAME = string.Empty;

            DataSet pDS = new DataSet();

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "A.SBLOCK_CODE IN (" + strBLOCK_CD_S + ") AND";
                }
                else
                {
                    strBLOCK_SQL = "A.SBLOCK_CODE = '" + strBLOCK_CD_S + "' AND";
                }
            }

            strFACILITIES_GBN = WQ_Function.SplitToCode(this.cboPartition.Text);
            if (strFACILITIES_GBN.Trim() == "")
            {
                strFACILITIES_GBN = "";
            }
            else
            {
                strFACILITIES_GBN = " A.FACILITIES_GBN  = '" + strFACILITIES_GBN + "' AND";
            }

            strFACILITIES_NAME = " A.FACILITIES_NAME LIKE '%" + this.txtFacName.Text.Trim() + "%'";

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.RANK, C.LOC_NAME || ' (' || C.LOC_CODE || ')' AS SBLOCK_CODE,  B.CODE_NAME || ' (' || B.CODE || ')' AS FACILITIES_GBN, A.FACILITIES_NAME, A.NO");
            oStringBuilder.AppendLine("FROM     WQ_IMPORTANT_FACILITIES A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE B ON B.PCODE = '3001' AND B.CODE = A.FACILITIES_GBN");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_LOCATION C ON C.LOC_CODE = A.SBLOCK_CODE");
            oStringBuilder.AppendLine("WHERE    " + strBLOCK_SQL + strFACILITIES_GBN + strFACILITIES_NAME);
            oStringBuilder.AppendLine("ORDER BY A.RANK");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "FACILITIES");

            this.uGridPopup.DataSource = pDS.Tables["FACILITIES"].DefaultView;

            //FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 중요지점 Data를 저장 한다.
        /// </summary>
        private void SaveFacilitiesData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strSBLKCD = string.Empty;
            string strFACGBN = string.Empty;
            string strFACNM = string.Empty;
            string strFACNO = string.Empty;
            string strRank = string.Empty;

            DataSet pDS = new DataSet();

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            strFACNO = this.txtFacNo.Text;
            strSBLKCD = this.cboSBlockM.Text.ToString();
            strSBLKCD = WQ_Function.SplitToCode(strSBLKCD);
            strFACGBN = this.cboPartitionM.Text.ToString();
            strFACGBN = WQ_Function.SplitToCode(strFACGBN);
            strFACNM = this.cboPartition.Text.ToString();
            strFACNM = this.txtFacName.Text.Trim();
            strRank = this.txtRank.Text.Trim();

            strParam = "WHERE    SBLOCK_CODE = '" + strSBLKCD + "' AND FACILITIES_GBN = '" + strFACGBN + "' AND NO = '" + strFACNO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_IMPORTANT_FACILITIES");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            IPoint pPoint = new PointClass();
            pPoint = (IPoint)WQ_AppStatic.CURRENT_IGEOMETRY;

            strParam = "('" + strFACNO + "', '" + strFACGBN + "', '" + strSBLKCD + "', '" + strFACNM + "', '" + strRank + "', '" + pPoint.X.ToString() + "', '" + pPoint.Y.ToString() + "')";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WQ_IMPORTANT_FACILITIES");
            oStringBuilder.AppendLine("(NO, FACILITIES_GBN, SBLOCK_CODE, FACILITIES_NAME, RANK, MAP_X, MAP_Y)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 중요지점 Data를 삭제 한다.
        /// </summary>
        private void DeleteFacilitiesData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strSBLKCD = string.Empty;
            string strFACGBN = string.Empty;
            string strFACNM = string.Empty;
            string strFACNO = string.Empty;

            DataSet pDS = new DataSet();

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            strFACNO = this.txtFacNo.Text;
            strSBLKCD = this.cboSBlockM.Text.ToString();
            strSBLKCD = WQ_Function.SplitToCode(strSBLKCD);
            strFACGBN = this.cboPartitionM.Text.ToString();
            strFACGBN = WQ_Function.SplitToCode(strFACGBN);
            strFACNM = this.cboPartition.Text.ToString();
            strFACNM = this.txtFacName.Text.Trim();

            strParam = "WHERE    SBLOCK_CODE = '" + strSBLKCD + "' AND FACILITIES_GBN = '" + strFACGBN + "' AND NO = '" + strFACNO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_IMPORTANT_FACILITIES");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        #endregion

        /// <summary>
        /// 맵 이동
        /// </summary>
        /// <param name="strLOC_CODE">중, 소블록 LOC_CODE</param>
        /// <param name="iType">0 : 중블록, 1 : 소블록</param>
        private void Viewmap_MonitorBlock(string strLOC_CODE, int iType)
        {
            string strFTR_IDN = string.Empty;

            IFeatureLayer pFeatureLayer = default(IFeatureLayer);

            if (iType == 0)
            {
                pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(_Map, "중블록");
                strFTR_IDN = WQ_Function.GetMediumBlockFTR_IDN(strLOC_CODE);
            }
            else
            {
                pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(_Map, "소블록");
                strFTR_IDN = WQ_Function.GetSmallBlockFTR_IDN(strLOC_CODE);
            }

            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + strFTR_IDN + "'");
            if (pFeature == null) return;

            _Map.Extent = pFeature.Shape.Envelope;
            _Map.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);
        }

        /// <summary>
        /// 저장하는 Data를 검증한다.
        /// </summary>
        /// <returns>True / False</returns>
        private bool IsValidation()
        {
            bool blRtn = true;

            if (this.cboSBlockM.Text == "") blRtn = false;
            if (this.cboPartitionM.Text == "") blRtn = false;
            if (this.txtFacName.Text.Trim() == "") blRtn = false;
            //if (this.txtFacNo.Text.Trim() == "") blRtn = false;
            if (this.txtRank.Text.Trim() == "") blRtn = false;

            if (blRtn == false)
            {
                MessageBox.Show("중요시설 정보를 입력(또는 선택)해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return blRtn;
        }

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_FACILITIES = false;
            this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        #endregion
    }
}
