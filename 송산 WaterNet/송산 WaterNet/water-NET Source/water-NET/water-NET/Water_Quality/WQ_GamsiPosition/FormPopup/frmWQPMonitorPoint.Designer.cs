﻿namespace WaterNet.WQ_GamsiPosition.FormPopup
{
    partial class frmWQPMonitorPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridPoint = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQuery = new System.Windows.Forms.Button();
            this.cboPartition = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridTemp1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridPoint1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtRS1 = new System.Windows.Forms.TextBox();
            this.txtDR1 = new System.Windows.Forms.TextBox();
            this.txtRT1 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtAFilter1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cboMBlock1 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cboLBlock1 = new System.Windows.Forms.ComboBox();
            this.udtSDate1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.udtEDate1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btnAnalysis1 = new System.Windows.Forms.Button();
            this.btnClose1 = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel9 = new System.Windows.Forms.Panel();
            this.uGridPoint2_2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridPoint2_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.udtSDate2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.udtEDate2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label12 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboSBlock2 = new System.Windows.Forms.ComboBox();
            this.btnQuery2 = new System.Windows.Forms.Button();
            this.btnClose2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cboMBlock2 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cboLBlock2 = new System.Windows.Forms.ComboBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridTemp2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridPoint3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtRS2 = new System.Windows.Forms.TextBox();
            this.txtDR2 = new System.Windows.Forms.TextBox();
            this.txtRT2 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtAFilter2 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.udtSDate3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.udtEDate3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btnAnalysis3 = new System.Windows.Forms.Button();
            this.btnClose3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cboMBlock3 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboLBlock3 = new System.Windows.Forms.ComboBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtNODEID = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cboTAGCU = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cboTAGTB = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cboTAGTE = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cboTAGPH = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboTAGCL = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.chkGItem4 = new System.Windows.Forms.CheckBox();
            this.chkGItem3 = new System.Windows.Forms.CheckBox();
            this.chkGItem2 = new System.Windows.Forms.CheckBox();
            this.chkGItem1 = new System.Windows.Forms.CheckBox();
            this.txtGPosNo = new System.Windows.Forms.TextBox();
            this.txtGPosName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cboPartitionM = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboSBlockM = new System.Windows.Forms.ComboBox();
            this.chkGItem5 = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.utabRTMonitorPoint = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemp1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemp2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utabRTMonitorPoint)).BeginInit();
            this.utabRTMonitorPoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridPoint);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox2);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox4);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox7);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox8);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(742, 301);
            // 
            // uGridPoint
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPoint.DisplayLayout.Appearance = appearance1;
            this.uGridPoint.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPoint.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPoint.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPoint.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPoint.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPoint.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPoint.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPoint.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPoint.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPoint.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPoint.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPoint.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPoint.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPoint.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPoint.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPoint.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPoint.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPoint.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPoint.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPoint.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPoint.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPoint.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPoint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPoint.Location = new System.Drawing.Point(4, 60);
            this.uGridPoint.Name = "uGridPoint";
            this.uGridPoint.Size = new System.Drawing.Size(734, 237);
            this.uGridPoint.TabIndex = 144;
            this.uGridPoint.Text = "ultraGrid1";
            this.uGridPoint.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPoint_DoubleClickRow);
            this.uGridPoint.Click += new System.EventHandler(this.uGridPoint_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(4, 56);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(734, 4);
            this.pictureBox2.TabIndex = 146;
            this.pictureBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Controls.Add(this.cboPartition);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cboSBlock);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cboMBlock);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cboLBlock);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(734, 52);
            this.panel2.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Close2;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(641, 23);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 26);
            this.btnClose.TabIndex = 154;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(4, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 12);
            this.label1.TabIndex = 159;
            this.label1.Text = "감시지점구분 :";
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Query;
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(551, 23);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(90, 26);
            this.btnQuery.TabIndex = 153;
            this.btnQuery.Text = "지점 조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // cboPartition
            // 
            this.cboPartition.FormattingEnabled = true;
            this.cboPartition.Location = new System.Drawing.Point(101, 24);
            this.cboPartition.Name = "cboPartition";
            this.cboPartition.Size = new System.Drawing.Size(140, 19);
            this.cboPartition.TabIndex = 158;
            this.cboPartition.SelectedIndexChanged += new System.EventHandler(this.cboPartition_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(466, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 148;
            this.label9.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(526, 3);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 19);
            this.cboSBlock.TabIndex = 147;
            this.cboSBlock.SelectedIndexChanged += new System.EventHandler(this.cboSBlock_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(260, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 146;
            this.label8.Text = "중블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(320, 3);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock.TabIndex = 145;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(43, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 144;
            this.label7.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(101, 3);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock.TabIndex = 143;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox4.Location = new System.Drawing.Point(0, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(4, 293);
            this.pictureBox4.TabIndex = 110;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox6.Location = new System.Drawing.Point(738, 4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(4, 293);
            this.pictureBox6.TabIndex = 111;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox7.Location = new System.Drawing.Point(0, 297);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(742, 4);
            this.pictureBox7.TabIndex = 109;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox8.Location = new System.Drawing.Point(0, 0);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(742, 4);
            this.pictureBox8.TabIndex = 108;
            this.pictureBox8.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridTemp1);
            this.ultraTabPageControl2.Controls.Add(this.uGridPoint1);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox21);
            this.ultraTabPageControl2.Controls.Add(this.panel3);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox9);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox10);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox11);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox12);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(742, 301);
            // 
            // uGridTemp1
            // 
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridTemp1.DisplayLayout.Appearance = appearance49;
            this.uGridTemp1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridTemp1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTemp1.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTemp1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.uGridTemp1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTemp1.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.uGridTemp1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridTemp1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridTemp1.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridTemp1.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.uGridTemp1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridTemp1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.uGridTemp1.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridTemp1.DisplayLayout.Override.CellAppearance = appearance56;
            this.uGridTemp1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridTemp1.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTemp1.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.uGridTemp1.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.uGridTemp1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridTemp1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.uGridTemp1.DisplayLayout.Override.RowAppearance = appearance59;
            this.uGridTemp1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridTemp1.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.uGridTemp1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridTemp1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridTemp1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridTemp1.Location = new System.Drawing.Point(300, 100);
            this.uGridTemp1.Name = "uGridTemp1";
            this.uGridTemp1.Size = new System.Drawing.Size(437, 175);
            this.uGridTemp1.TabIndex = 150;
            this.uGridTemp1.Text = "ultraGrid1";
            this.uGridTemp1.Visible = false;
            // 
            // uGridPoint1
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPoint1.DisplayLayout.Appearance = appearance37;
            this.uGridPoint1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPoint1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint1.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.uGridPoint1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint1.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.uGridPoint1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPoint1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPoint1.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPoint1.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.uGridPoint1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPoint1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPoint1.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPoint1.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGridPoint1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPoint1.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint1.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.uGridPoint1.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.uGridPoint1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPoint1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.uGridPoint1.DisplayLayout.Override.RowAppearance = appearance47;
            this.uGridPoint1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPoint1.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.uGridPoint1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPoint1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPoint1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPoint1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPoint1.Location = new System.Drawing.Point(4, 81);
            this.uGridPoint1.Name = "uGridPoint1";
            this.uGridPoint1.Size = new System.Drawing.Size(734, 216);
            this.uGridPoint1.TabIndex = 148;
            this.uGridPoint1.Text = "ultraGrid1";
            this.uGridPoint1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPoint1_DoubleClickRow);
            this.uGridPoint1.Click += new System.EventHandler(this.uGridPoint1_Click);
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Gold;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox21.Location = new System.Drawing.Point(4, 77);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(734, 4);
            this.pictureBox21.TabIndex = 149;
            this.pictureBox21.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtRS1);
            this.panel3.Controls.Add(this.txtDR1);
            this.panel3.Controls.Add(this.txtRT1);
            this.panel3.Controls.Add(this.label39);
            this.panel3.Controls.Add(this.label40);
            this.panel3.Controls.Add(this.label42);
            this.panel3.Controls.Add(this.txtAFilter1);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.cboMBlock1);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.cboLBlock1);
            this.panel3.Controls.Add(this.udtSDate1);
            this.panel3.Controls.Add(this.udtEDate1);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.btnAnalysis1);
            this.panel3.Controls.Add(this.btnClose1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(734, 73);
            this.panel3.TabIndex = 147;
            // 
            // txtRS1
            // 
            this.txtRS1.Location = new System.Drawing.Point(423, 24);
            this.txtRS1.Name = "txtRS1";
            this.txtRS1.Size = new System.Drawing.Size(44, 20);
            this.txtRS1.TabIndex = 198;
            this.txtRS1.Text = "0:00";
            // 
            // txtDR1
            // 
            this.txtDR1.Location = new System.Drawing.Point(86, 24);
            this.txtDR1.Name = "txtDR1";
            this.txtDR1.Size = new System.Drawing.Size(44, 20);
            this.txtDR1.TabIndex = 197;
            this.txtDR1.Text = "72:00";
            // 
            // txtRT1
            // 
            this.txtRT1.Location = new System.Drawing.Point(270, 24);
            this.txtRT1.Name = "txtRT1";
            this.txtRT1.Size = new System.Drawing.Size(44, 20);
            this.txtRT1.TabIndex = 196;
            this.txtRT1.Text = "72:00";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.Location = new System.Drawing.Point(320, 27);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(97, 12);
            this.label39.TabIndex = 194;
            this.label39.Text = " Report Start :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.Location = new System.Drawing.Point(136, 27);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(128, 12);
            this.label40.TabIndex = 193;
            this.label40.Text = " Report Timestep :";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.Location = new System.Drawing.Point(6, 27);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(74, 12);
            this.label42.TabIndex = 191;
            this.label42.Text = " Duration :";
            // 
            // txtAFilter1
            // 
            this.txtAFilter1.Location = new System.Drawing.Point(577, 24);
            this.txtAFilter1.Name = "txtAFilter1";
            this.txtAFilter1.Size = new System.Drawing.Size(44, 20);
            this.txtAFilter1.TabIndex = 190;
            this.txtAFilter1.Text = "50";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.Location = new System.Drawing.Point(473, 27);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(98, 12);
            this.label33.TabIndex = 189;
            this.label33.Text = "체류시간 필터 :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(215, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 12);
            this.label18.TabIndex = 188;
            this.label18.Text = "중블록 :";
            // 
            // cboMBlock1
            // 
            this.cboMBlock1.FormattingEnabled = true;
            this.cboMBlock1.Location = new System.Drawing.Point(275, 3);
            this.cboMBlock1.Name = "cboMBlock1";
            this.cboMBlock1.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock1.TabIndex = 187;
            this.cboMBlock1.SelectedIndexChanged += new System.EventHandler(this.cboMBlock1_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(11, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 12);
            this.label20.TabIndex = 186;
            this.label20.Text = "대블록 :";
            // 
            // cboLBlock1
            // 
            this.cboLBlock1.FormattingEnabled = true;
            this.cboLBlock1.Location = new System.Drawing.Point(69, 3);
            this.cboLBlock1.Name = "cboLBlock1";
            this.cboLBlock1.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock1.TabIndex = 185;
            this.cboLBlock1.SelectedIndexChanged += new System.EventHandler(this.cboLBlock1_SelectedIndexChanged);
            // 
            // udtSDate1
            // 
            this.udtSDate1.Location = new System.Drawing.Point(500, 3);
            this.udtSDate1.Name = "udtSDate1";
            this.udtSDate1.Size = new System.Drawing.Size(100, 20);
            this.udtSDate1.TabIndex = 156;
            this.udtSDate1.Visible = false;
            // 
            // udtEDate1
            // 
            this.udtEDate1.Location = new System.Drawing.Point(612, 3);
            this.udtEDate1.Name = "udtEDate1";
            this.udtEDate1.Size = new System.Drawing.Size(100, 20);
            this.udtEDate1.TabIndex = 157;
            this.udtEDate1.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(422, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 12);
            this.label21.TabIndex = 159;
            this.label21.Text = "산출 기간 :";
            this.label21.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(599, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 12);
            this.label22.TabIndex = 158;
            this.label22.Text = "~";
            this.label22.Visible = false;
            // 
            // btnAnalysis1
            // 
            this.btnAnalysis1.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Analysis;
            this.btnAnalysis1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnalysis1.Location = new System.Drawing.Point(551, 44);
            this.btnAnalysis1.Name = "btnAnalysis1";
            this.btnAnalysis1.Size = new System.Drawing.Size(90, 26);
            this.btnAnalysis1.TabIndex = 155;
            this.btnAnalysis1.Text = "관망 해석";
            this.btnAnalysis1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAnalysis1.UseVisualStyleBackColor = true;
            this.btnAnalysis1.Click += new System.EventHandler(this.btnAnalysis_Click);
            // 
            // btnClose1
            // 
            this.btnClose1.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Close2;
            this.btnClose1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose1.Location = new System.Drawing.Point(641, 44);
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.Size = new System.Drawing.Size(90, 26);
            this.btnClose1.TabIndex = 154;
            this.btnClose1.Text = "닫기";
            this.btnClose1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose1.UseVisualStyleBackColor = true;
            this.btnClose1.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox9.Location = new System.Drawing.Point(0, 4);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(4, 293);
            this.pictureBox9.TabIndex = 110;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox10.Location = new System.Drawing.Point(738, 4);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(4, 293);
            this.pictureBox10.TabIndex = 111;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox11.Location = new System.Drawing.Point(0, 297);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(742, 4);
            this.pictureBox11.TabIndex = 109;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox12.Location = new System.Drawing.Point(0, 0);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(742, 4);
            this.pictureBox12.TabIndex = 108;
            this.pictureBox12.TabStop = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.panel9);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox23);
            this.ultraTabPageControl3.Controls.Add(this.panel8);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox13);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox14);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox15);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox16);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(742, 301);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.uGridPoint2_2);
            this.panel9.Controls.Add(this.uGridPoint2_1);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(4, 60);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(734, 237);
            this.panel9.TabIndex = 152;
            // 
            // uGridPoint2_2
            // 
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPoint2_2.DisplayLayout.Appearance = appearance73;
            this.uGridPoint2_2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPoint2_2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance74.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint2_2.DisplayLayout.GroupByBox.Appearance = appearance74;
            appearance75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint2_2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance75;
            this.uGridPoint2_2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance76.BackColor2 = System.Drawing.SystemColors.Control;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint2_2.DisplayLayout.GroupByBox.PromptAppearance = appearance76;
            this.uGridPoint2_2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPoint2_2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPoint2_2.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            appearance78.BackColor = System.Drawing.SystemColors.Highlight;
            appearance78.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPoint2_2.DisplayLayout.Override.ActiveRowAppearance = appearance78;
            this.uGridPoint2_2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPoint2_2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPoint2_2.DisplayLayout.Override.CardAreaAppearance = appearance79;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            appearance80.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPoint2_2.DisplayLayout.Override.CellAppearance = appearance80;
            this.uGridPoint2_2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPoint2_2.DisplayLayout.Override.CellPadding = 0;
            appearance81.BackColor = System.Drawing.SystemColors.Control;
            appearance81.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance81.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance81.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint2_2.DisplayLayout.Override.GroupByRowAppearance = appearance81;
            appearance82.TextHAlignAsString = "Left";
            this.uGridPoint2_2.DisplayLayout.Override.HeaderAppearance = appearance82;
            this.uGridPoint2_2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPoint2_2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.Color.Silver;
            this.uGridPoint2_2.DisplayLayout.Override.RowAppearance = appearance83;
            this.uGridPoint2_2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPoint2_2.DisplayLayout.Override.TemplateAddRowAppearance = appearance84;
            this.uGridPoint2_2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPoint2_2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPoint2_2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPoint2_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridPoint2_2.Location = new System.Drawing.Point(0, 118);
            this.uGridPoint2_2.Name = "uGridPoint2_2";
            this.uGridPoint2_2.Size = new System.Drawing.Size(734, 118);
            this.uGridPoint2_2.TabIndex = 153;
            this.uGridPoint2_2.Text = "ultraGrid2";
            this.uGridPoint2_2.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPoint2_2_DoubleClickRow);
            this.uGridPoint2_2.Click += new System.EventHandler(this.uGridPoint2_2_Click);
            // 
            // uGridPoint2_1
            // 
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPoint2_1.DisplayLayout.Appearance = appearance61;
            this.uGridPoint2_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPoint2_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint2_1.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint2_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.uGridPoint2_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint2_1.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.uGridPoint2_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPoint2_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPoint2_1.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPoint2_1.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.uGridPoint2_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPoint2_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPoint2_1.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPoint2_1.DisplayLayout.Override.CellAppearance = appearance68;
            this.uGridPoint2_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPoint2_1.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint2_1.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.uGridPoint2_1.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.uGridPoint2_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPoint2_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.uGridPoint2_1.DisplayLayout.Override.RowAppearance = appearance71;
            this.uGridPoint2_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPoint2_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.uGridPoint2_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPoint2_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPoint2_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPoint2_1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridPoint2_1.Location = new System.Drawing.Point(0, 0);
            this.uGridPoint2_1.Name = "uGridPoint2_1";
            this.uGridPoint2_1.Size = new System.Drawing.Size(734, 118);
            this.uGridPoint2_1.TabIndex = 152;
            this.uGridPoint2_1.Text = "ultraGrid2";
            this.uGridPoint2_1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPoint2_1_DoubleClickRow);
            this.uGridPoint2_1.Click += new System.EventHandler(this.uGridPoint2_1_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(115, 121);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(514, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "중요시설, 수질민원 데이터를 확인하고 운영자가 임의로 감시지점을 선택합니다.";
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.Gold;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox23.Location = new System.Drawing.Point(4, 56);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(734, 4);
            this.pictureBox23.TabIndex = 151;
            this.pictureBox23.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.udtSDate2);
            this.panel8.Controls.Add(this.udtEDate2);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.label43);
            this.panel8.Controls.Add(this.label4);
            this.panel8.Controls.Add(this.cboSBlock2);
            this.panel8.Controls.Add(this.btnQuery2);
            this.panel8.Controls.Add(this.btnClose2);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.cboMBlock2);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.cboLBlock2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(4, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(734, 52);
            this.panel8.TabIndex = 150;
            // 
            // udtSDate2
            // 
            this.udtSDate2.Location = new System.Drawing.Point(88, 25);
            this.udtSDate2.Name = "udtSDate2";
            this.udtSDate2.Size = new System.Drawing.Size(100, 20);
            this.udtSDate2.TabIndex = 160;
            this.udtSDate2.Visible = false;
            // 
            // udtEDate2
            // 
            this.udtEDate2.Location = new System.Drawing.Point(200, 25);
            this.udtEDate2.Name = "udtEDate2";
            this.udtEDate2.Size = new System.Drawing.Size(100, 20);
            this.udtEDate2.TabIndex = 161;
            this.udtEDate2.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(10, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 12);
            this.label12.TabIndex = 163;
            this.label12.Text = "조회 기간 :";
            this.label12.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.Location = new System.Drawing.Point(187, 29);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(15, 12);
            this.label43.TabIndex = 162;
            this.label43.Text = "~";
            this.label43.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(428, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 12);
            this.label4.TabIndex = 156;
            this.label4.Text = "소블록 :";
            // 
            // cboSBlock2
            // 
            this.cboSBlock2.FormattingEnabled = true;
            this.cboSBlock2.Location = new System.Drawing.Point(488, 3);
            this.cboSBlock2.Name = "cboSBlock2";
            this.cboSBlock2.Size = new System.Drawing.Size(140, 19);
            this.cboSBlock2.TabIndex = 155;
            this.cboSBlock2.SelectedIndexChanged += new System.EventHandler(this.cboSBlock2_SelectedIndexChanged);
            // 
            // btnQuery2
            // 
            this.btnQuery2.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Query;
            this.btnQuery2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery2.Location = new System.Drawing.Point(551, 23);
            this.btnQuery2.Name = "btnQuery2";
            this.btnQuery2.Size = new System.Drawing.Size(90, 26);
            this.btnQuery2.TabIndex = 153;
            this.btnQuery2.Text = "조회";
            this.btnQuery2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery2.UseVisualStyleBackColor = true;
            this.btnQuery2.Click += new System.EventHandler(this.btnQuery2_Click);
            // 
            // btnClose2
            // 
            this.btnClose2.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Close2;
            this.btnClose2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose2.Location = new System.Drawing.Point(641, 23);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.Size = new System.Drawing.Size(90, 26);
            this.btnClose2.TabIndex = 154;
            this.btnClose2.Text = "닫기";
            this.btnClose2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose2.UseVisualStyleBackColor = true;
            this.btnClose2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(215, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 12);
            this.label11.TabIndex = 146;
            this.label11.Text = "중블록 :";
            // 
            // cboMBlock2
            // 
            this.cboMBlock2.FormattingEnabled = true;
            this.cboMBlock2.Location = new System.Drawing.Point(275, 3);
            this.cboMBlock2.Name = "cboMBlock2";
            this.cboMBlock2.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock2.TabIndex = 145;
            this.cboMBlock2.SelectedIndexChanged += new System.EventHandler(this.cboMBlock2_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(11, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 12);
            this.label15.TabIndex = 144;
            this.label15.Text = "대블록 :";
            // 
            // cboLBlock2
            // 
            this.cboLBlock2.FormattingEnabled = true;
            this.cboLBlock2.Location = new System.Drawing.Point(69, 3);
            this.cboLBlock2.Name = "cboLBlock2";
            this.cboLBlock2.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock2.TabIndex = 143;
            this.cboLBlock2.SelectedIndexChanged += new System.EventHandler(this.cboLBlock2_SelectedIndexChanged);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox13.Location = new System.Drawing.Point(0, 4);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(4, 293);
            this.pictureBox13.TabIndex = 110;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox14.Location = new System.Drawing.Point(738, 4);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(4, 293);
            this.pictureBox14.TabIndex = 111;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox15.Location = new System.Drawing.Point(0, 297);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(742, 4);
            this.pictureBox15.TabIndex = 109;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox16.Location = new System.Drawing.Point(0, 0);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(742, 4);
            this.pictureBox16.TabIndex = 108;
            this.pictureBox16.TabStop = false;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uGridTemp2);
            this.ultraTabPageControl4.Controls.Add(this.uGridPoint3);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox22);
            this.ultraTabPageControl4.Controls.Add(this.panel5);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox18);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox19);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox20);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(742, 301);
            // 
            // uGridTemp2
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridTemp2.DisplayLayout.Appearance = appearance25;
            this.uGridTemp2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridTemp2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTemp2.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTemp2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGridTemp2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTemp2.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGridTemp2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridTemp2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridTemp2.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridTemp2.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.uGridTemp2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridTemp2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGridTemp2.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridTemp2.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGridTemp2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridTemp2.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTemp2.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.uGridTemp2.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.uGridTemp2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridTemp2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.uGridTemp2.DisplayLayout.Override.RowAppearance = appearance35;
            this.uGridTemp2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridTemp2.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.uGridTemp2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridTemp2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridTemp2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridTemp2.Location = new System.Drawing.Point(300, 100);
            this.uGridTemp2.Name = "uGridTemp2";
            this.uGridTemp2.Size = new System.Drawing.Size(437, 175);
            this.uGridTemp2.TabIndex = 153;
            this.uGridTemp2.Text = "ultraGrid1";
            this.uGridTemp2.Visible = false;
            // 
            // uGridPoint3
            // 
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPoint3.DisplayLayout.Appearance = appearance85;
            this.uGridPoint3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPoint3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance86.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance86.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance86.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint3.DisplayLayout.GroupByBox.Appearance = appearance86;
            appearance87.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance87;
            this.uGridPoint3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance88.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance88.BackColor2 = System.Drawing.SystemColors.Control;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance88.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint3.DisplayLayout.GroupByBox.PromptAppearance = appearance88;
            this.uGridPoint3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPoint3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPoint3.DisplayLayout.Override.ActiveCellAppearance = appearance89;
            appearance90.BackColor = System.Drawing.SystemColors.Highlight;
            appearance90.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPoint3.DisplayLayout.Override.ActiveRowAppearance = appearance90;
            this.uGridPoint3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPoint3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPoint3.DisplayLayout.Override.CardAreaAppearance = appearance91;
            appearance92.BorderColor = System.Drawing.Color.Silver;
            appearance92.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPoint3.DisplayLayout.Override.CellAppearance = appearance92;
            this.uGridPoint3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPoint3.DisplayLayout.Override.CellPadding = 0;
            appearance93.BackColor = System.Drawing.SystemColors.Control;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint3.DisplayLayout.Override.GroupByRowAppearance = appearance93;
            appearance94.TextHAlignAsString = "Left";
            this.uGridPoint3.DisplayLayout.Override.HeaderAppearance = appearance94;
            this.uGridPoint3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPoint3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.Color.Silver;
            this.uGridPoint3.DisplayLayout.Override.RowAppearance = appearance95;
            this.uGridPoint3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPoint3.DisplayLayout.Override.TemplateAddRowAppearance = appearance96;
            this.uGridPoint3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPoint3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPoint3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPoint3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPoint3.Location = new System.Drawing.Point(4, 81);
            this.uGridPoint3.Name = "uGridPoint3";
            this.uGridPoint3.Size = new System.Drawing.Size(734, 216);
            this.uGridPoint3.TabIndex = 151;
            this.uGridPoint3.Text = "ultraGrid2";
            this.uGridPoint3.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPoint3_DoubleClickRow);
            this.uGridPoint3.Click += new System.EventHandler(this.uGridPoint3_Click);
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.Gold;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox22.Location = new System.Drawing.Point(4, 77);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(734, 4);
            this.pictureBox22.TabIndex = 152;
            this.pictureBox22.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.txtRS2);
            this.panel5.Controls.Add(this.txtDR2);
            this.panel5.Controls.Add(this.txtRT2);
            this.panel5.Controls.Add(this.label35);
            this.panel5.Controls.Add(this.label36);
            this.panel5.Controls.Add(this.label38);
            this.panel5.Controls.Add(this.txtAFilter2);
            this.panel5.Controls.Add(this.label34);
            this.panel5.Controls.Add(this.udtSDate3);
            this.panel5.Controls.Add(this.udtEDate3);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.btnAnalysis3);
            this.panel5.Controls.Add(this.btnClose3);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.cboMBlock3);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.cboLBlock3);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(734, 73);
            this.panel5.TabIndex = 150;
            // 
            // txtRS2
            // 
            this.txtRS2.Location = new System.Drawing.Point(423, 24);
            this.txtRS2.Name = "txtRS2";
            this.txtRS2.Size = new System.Drawing.Size(44, 20);
            this.txtRS2.TabIndex = 206;
            this.txtRS2.Text = "0:00";
            // 
            // txtDR2
            // 
            this.txtDR2.Location = new System.Drawing.Point(86, 24);
            this.txtDR2.Name = "txtDR2";
            this.txtDR2.Size = new System.Drawing.Size(44, 20);
            this.txtDR2.TabIndex = 205;
            this.txtDR2.Text = "72:00";
            // 
            // txtRT2
            // 
            this.txtRT2.Location = new System.Drawing.Point(270, 24);
            this.txtRT2.Name = "txtRT2";
            this.txtRT2.Size = new System.Drawing.Size(44, 20);
            this.txtRT2.TabIndex = 204;
            this.txtRT2.Text = "72:00";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.Location = new System.Drawing.Point(320, 27);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(97, 12);
            this.label35.TabIndex = 202;
            this.label35.Text = " Report Start :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.Location = new System.Drawing.Point(136, 27);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(128, 12);
            this.label36.TabIndex = 201;
            this.label36.Text = " Report Timestep :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.Location = new System.Drawing.Point(6, 27);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(74, 12);
            this.label38.TabIndex = 199;
            this.label38.Text = " Duration :";
            // 
            // txtAFilter2
            // 
            this.txtAFilter2.Location = new System.Drawing.Point(551, 24);
            this.txtAFilter2.Name = "txtAFilter2";
            this.txtAFilter2.Size = new System.Drawing.Size(44, 20);
            this.txtAFilter2.TabIndex = 192;
            this.txtAFilter2.Text = "100";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.Location = new System.Drawing.Point(473, 27);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 12);
            this.label34.TabIndex = 191;
            this.label34.Text = "유량 필터 :";
            // 
            // udtSDate3
            // 
            this.udtSDate3.Location = new System.Drawing.Point(501, 3);
            this.udtSDate3.Name = "udtSDate3";
            this.udtSDate3.Size = new System.Drawing.Size(100, 20);
            this.udtSDate3.TabIndex = 160;
            this.udtSDate3.Visible = false;
            // 
            // udtEDate3
            // 
            this.udtEDate3.Location = new System.Drawing.Point(613, 3);
            this.udtEDate3.Name = "udtEDate3";
            this.udtEDate3.Size = new System.Drawing.Size(100, 20);
            this.udtEDate3.TabIndex = 161;
            this.udtEDate3.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(423, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 12);
            this.label23.TabIndex = 163;
            this.label23.Text = "산출 기간 :";
            this.label23.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(600, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(15, 12);
            this.label24.TabIndex = 162;
            this.label24.Text = "~";
            this.label24.Visible = false;
            // 
            // btnAnalysis3
            // 
            this.btnAnalysis3.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Analysis;
            this.btnAnalysis3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnalysis3.Location = new System.Drawing.Point(551, 44);
            this.btnAnalysis3.Name = "btnAnalysis3";
            this.btnAnalysis3.Size = new System.Drawing.Size(90, 26);
            this.btnAnalysis3.TabIndex = 156;
            this.btnAnalysis3.Text = "관망 해석";
            this.btnAnalysis3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAnalysis3.UseVisualStyleBackColor = true;
            this.btnAnalysis3.Click += new System.EventHandler(this.btnAnalysis_Click);
            // 
            // btnClose3
            // 
            this.btnClose3.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Close2;
            this.btnClose3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose3.Location = new System.Drawing.Point(641, 44);
            this.btnClose3.Name = "btnClose3";
            this.btnClose3.Size = new System.Drawing.Size(90, 26);
            this.btnClose3.TabIndex = 154;
            this.btnClose3.Text = "닫기";
            this.btnClose3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose3.UseVisualStyleBackColor = true;
            this.btnClose3.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(217, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 12);
            this.label2.TabIndex = 146;
            this.label2.Text = "중블록 :";
            // 
            // cboMBlock3
            // 
            this.cboMBlock3.FormattingEnabled = true;
            this.cboMBlock3.Location = new System.Drawing.Point(277, 3);
            this.cboMBlock3.Name = "cboMBlock3";
            this.cboMBlock3.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock3.TabIndex = 145;
            this.cboMBlock3.SelectedIndexChanged += new System.EventHandler(this.cboMBlock3_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(11, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 12);
            this.label10.TabIndex = 144;
            this.label10.Text = "대블록 :";
            // 
            // cboLBlock3
            // 
            this.cboLBlock3.FormattingEnabled = true;
            this.cboLBlock3.Location = new System.Drawing.Point(71, 3);
            this.cboLBlock3.Name = "cboLBlock3";
            this.cboLBlock3.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock3.TabIndex = 143;
            this.cboLBlock3.SelectedIndexChanged += new System.EventHandler(this.cboLBlock3_SelectedIndexChanged);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox17.Location = new System.Drawing.Point(0, 4);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(4, 293);
            this.pictureBox17.TabIndex = 110;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox18.Location = new System.Drawing.Point(738, 4);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(4, 293);
            this.pictureBox18.TabIndex = 111;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox19.Location = new System.Drawing.Point(0, 297);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(742, 4);
            this.pictureBox19.TabIndex = 109;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox20.Location = new System.Drawing.Point(0, 0);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(742, 4);
            this.pictureBox20.TabIndex = 108;
            this.pictureBox20.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(746, 471);
            this.panel1.TabIndex = 131;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.utabRTMonitorPoint);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(746, 471);
            this.panel4.TabIndex = 106;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.txtNODEID);
            this.panel7.Controls.Add(this.label32);
            this.panel7.Controls.Add(this.cboTAGCU);
            this.panel7.Controls.Add(this.label31);
            this.panel7.Controls.Add(this.cboTAGTB);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.cboTAGTE);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.cboTAGPH);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.cboTAGCL);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.pictureBox3);
            this.panel7.Controls.Add(this.chkGItem4);
            this.panel7.Controls.Add(this.chkGItem3);
            this.panel7.Controls.Add(this.chkGItem2);
            this.panel7.Controls.Add(this.chkGItem1);
            this.panel7.Controls.Add(this.txtGPosNo);
            this.panel7.Controls.Add(this.txtGPosName);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.cboPartitionM);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.cboSBlockM);
            this.panel7.Controls.Add(this.chkGItem5);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 355);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(746, 116);
            this.panel7.TabIndex = 148;
            // 
            // txtNODEID
            // 
            this.txtNODEID.Location = new System.Drawing.Point(597, 29);
            this.txtNODEID.Name = "txtNODEID";
            this.txtNODEID.Size = new System.Drawing.Size(140, 20);
            this.txtNODEID.TabIndex = 193;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.Location = new System.Drawing.Point(532, 32);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 12);
            this.label32.TabIndex = 192;
            this.label32.Text = "절점 ID :";
            // 
            // cboTAGCU
            // 
            this.cboTAGCU.FormattingEnabled = true;
            this.cboTAGCU.Location = new System.Drawing.Point(519, 93);
            this.cboTAGCU.Name = "cboTAGCU";
            this.cboTAGCU.Size = new System.Drawing.Size(218, 19);
            this.cboTAGCU.TabIndex = 191;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.Location = new System.Drawing.Point(471, 97);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(43, 12);
            this.label31.TabIndex = 190;
            this.label31.Text = "TAG :";
            // 
            // cboTAGTB
            // 
            this.cboTAGTB.FormattingEnabled = true;
            this.cboTAGTB.Location = new System.Drawing.Point(137, 73);
            this.cboTAGTB.Name = "cboTAGTB";
            this.cboTAGTB.Size = new System.Drawing.Size(218, 19);
            this.cboTAGTB.TabIndex = 189;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(89, 77);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(43, 12);
            this.label30.TabIndex = 188;
            this.label30.Text = "TAG :";
            // 
            // cboTAGTE
            // 
            this.cboTAGTE.FormattingEnabled = true;
            this.cboTAGTE.Location = new System.Drawing.Point(519, 72);
            this.cboTAGTE.Name = "cboTAGTE";
            this.cboTAGTE.Size = new System.Drawing.Size(218, 19);
            this.cboTAGTE.TabIndex = 187;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(471, 76);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 12);
            this.label29.TabIndex = 186;
            this.label29.Text = "TAG :";
            // 
            // cboTAGPH
            // 
            this.cboTAGPH.FormattingEnabled = true;
            this.cboTAGPH.Location = new System.Drawing.Point(519, 51);
            this.cboTAGPH.Name = "cboTAGPH";
            this.cboTAGPH.Size = new System.Drawing.Size(218, 19);
            this.cboTAGPH.TabIndex = 185;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(471, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 12);
            this.label5.TabIndex = 184;
            this.label5.Text = "TAG :";
            // 
            // cboTAGCL
            // 
            this.cboTAGCL.FormattingEnabled = true;
            this.cboTAGCL.Location = new System.Drawing.Point(137, 52);
            this.cboTAGCL.Name = "cboTAGCL";
            this.cboTAGCL.Size = new System.Drawing.Size(218, 19);
            this.cboTAGCL.TabIndex = 183;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(89, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "TAG :";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(746, 4);
            this.pictureBox3.TabIndex = 180;
            this.pictureBox3.TabStop = false;
            // 
            // chkGItem4
            // 
            this.chkGItem4.AutoSize = true;
            this.chkGItem4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem4.Location = new System.Drawing.Point(12, 54);
            this.chkGItem4.Name = "chkGItem4";
            this.chkGItem4.Size = new System.Drawing.Size(76, 16);
            this.chkGItem4.TabIndex = 177;
            this.chkGItem4.Text = "잔류염소";
            this.chkGItem4.UseVisualStyleBackColor = true;
            // 
            // chkGItem3
            // 
            this.chkGItem3.AutoSize = true;
            this.chkGItem3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem3.Location = new System.Drawing.Point(38, 75);
            this.chkGItem3.Name = "chkGItem3";
            this.chkGItem3.Size = new System.Drawing.Size(50, 16);
            this.chkGItem3.TabIndex = 176;
            this.chkGItem3.Text = "탁도";
            this.chkGItem3.UseVisualStyleBackColor = true;
            // 
            // chkGItem2
            // 
            this.chkGItem2.AutoSize = true;
            this.chkGItem2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem2.Location = new System.Drawing.Point(368, 54);
            this.chkGItem2.Name = "chkGItem2";
            this.chkGItem2.Size = new System.Drawing.Size(102, 16);
            this.chkGItem2.TabIndex = 175;
            this.chkGItem2.Text = "수소이온농도";
            this.chkGItem2.UseVisualStyleBackColor = true;
            // 
            // chkGItem1
            // 
            this.chkGItem1.AutoSize = true;
            this.chkGItem1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem1.Location = new System.Drawing.Point(420, 75);
            this.chkGItem1.Name = "chkGItem1";
            this.chkGItem1.Size = new System.Drawing.Size(50, 16);
            this.chkGItem1.TabIndex = 174;
            this.chkGItem1.Text = "온도";
            this.chkGItem1.UseVisualStyleBackColor = true;
            // 
            // txtGPosNo
            // 
            this.txtGPosNo.Location = new System.Drawing.Point(365, 29);
            this.txtGPosNo.Name = "txtGPosNo";
            this.txtGPosNo.ReadOnly = true;
            this.txtGPosNo.Size = new System.Drawing.Size(140, 20);
            this.txtGPosNo.TabIndex = 172;
            // 
            // txtGPosName
            // 
            this.txtGPosName.Location = new System.Drawing.Point(90, 29);
            this.txtGPosName.Name = "txtGPosName";
            this.txtGPosName.Size = new System.Drawing.Size(140, 20);
            this.txtGPosName.TabIndex = 171;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(240, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 12);
            this.label13.TabIndex = 167;
            this.label13.Text = "감시지점일련번호 :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(6, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 12);
            this.label14.TabIndex = 166;
            this.label14.Text = "감시지점명 :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(266, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 12);
            this.label16.TabIndex = 165;
            this.label16.Text = "감시지점구분 :";
            // 
            // cboPartitionM
            // 
            this.cboPartitionM.Enabled = false;
            this.cboPartitionM.FormattingEnabled = true;
            this.cboPartitionM.Location = new System.Drawing.Point(365, 7);
            this.cboPartitionM.Name = "cboPartitionM";
            this.cboPartitionM.Size = new System.Drawing.Size(140, 19);
            this.cboPartitionM.TabIndex = 164;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(32, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 12);
            this.label17.TabIndex = 163;
            this.label17.Text = "소블록 :";
            // 
            // cboSBlockM
            // 
            this.cboSBlockM.FormattingEnabled = true;
            this.cboSBlockM.Location = new System.Drawing.Point(90, 7);
            this.cboSBlockM.Name = "cboSBlockM";
            this.cboSBlockM.Size = new System.Drawing.Size(140, 19);
            this.cboSBlockM.TabIndex = 162;
            this.cboSBlockM.SelectedIndexChanged += new System.EventHandler(this.cboSBlockM_SelectedIndexChanged);
            // 
            // chkGItem5
            // 
            this.chkGItem5.AutoSize = true;
            this.chkGItem5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem5.Location = new System.Drawing.Point(381, 96);
            this.chkGItem5.Name = "chkGItem5";
            this.chkGItem5.Size = new System.Drawing.Size(89, 16);
            this.chkGItem5.TabIndex = 178;
            this.chkGItem5.Text = "전기전도도";
            this.chkGItem5.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnNew);
            this.panel6.Controls.Add(this.btnSave);
            this.panel6.Controls.Add(this.btnDelete);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 327);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(746, 28);
            this.panel6.TabIndex = 147;
            // 
            // btnNew
            // 
            this.btnNew.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Clear;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNew.Location = new System.Drawing.Point(474, 0);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 26);
            this.btnNew.TabIndex = 159;
            this.btnNew.Text = "초기화";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(564, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 26);
            this.btnSave.TabIndex = 158;
            this.btnSave.Text = "지점 등록";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(654, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 26);
            this.btnDelete.TabIndex = 157;
            this.btnDelete.Text = "지점 삭제";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(2, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "수질 감시 지점 정보 관리";
            // 
            // utabRTMonitorPoint
            // 
            this.utabRTMonitorPoint.Controls.Add(this.ultraTabSharedControlsPage1);
            this.utabRTMonitorPoint.Controls.Add(this.ultraTabPageControl1);
            this.utabRTMonitorPoint.Controls.Add(this.ultraTabPageControl2);
            this.utabRTMonitorPoint.Controls.Add(this.ultraTabPageControl3);
            this.utabRTMonitorPoint.Controls.Add(this.ultraTabPageControl4);
            this.utabRTMonitorPoint.Dock = System.Windows.Forms.DockStyle.Top;
            this.utabRTMonitorPoint.Location = new System.Drawing.Point(0, 0);
            this.utabRTMonitorPoint.Name = "utabRTMonitorPoint";
            this.utabRTMonitorPoint.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.utabRTMonitorPoint.Size = new System.Drawing.Size(746, 327);
            this.utabRTMonitorPoint.TabIndex = 149;
            ultraTab5.TabPage = this.ultraTabPageControl1;
            ultraTab5.Text = "수질 감시 지점 조회 및 삭제";
            ultraTab6.TabPage = this.ultraTabPageControl2;
            ultraTab6.Text = "수질 감시 지점 등록(체류시간)";
            ultraTab7.TabPage = this.ultraTabPageControl3;
            ultraTab7.Text = "수질 감시 지점 등록(운영자)";
            ultraTab8.TabPage = this.ultraTabPageControl4;
            ultraTab8.Text = "수질 감시 지점 등록(유량)";
            this.utabRTMonitorPoint.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab5,
            ultraTab6,
            ultraTab7,
            ultraTab8});
            this.utabRTMonitorPoint.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.utabRTMonitorPoint_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(742, 301);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Location = new System.Drawing.Point(0, 68);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1123, 4);
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 471);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(750, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 471);
            this.PicFrRight.TabIndex = 107;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 475);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(754, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(754, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // frmWQPMonitorPoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 479);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmWQPMonitorPoint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "수질 감시 지점 선정";
            this.Load += new System.EventHandler(this.frmWQPMonitorPoint_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQPMonitorPoint_FormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemp1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemp2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utabRTMonitorPoint)).EndInit();
            this.utabRTMonitorPoint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboPartitionM;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboSBlockM;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.TextBox txtGPosNo;
        private System.Windows.Forms.TextBox txtGPosName;
        private System.Windows.Forms.CheckBox chkGItem5;
        private System.Windows.Forms.CheckBox chkGItem4;
        private System.Windows.Forms.CheckBox chkGItem3;
        private System.Windows.Forms.CheckBox chkGItem2;
        private System.Windows.Forms.CheckBox chkGItem1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboTAGCU;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cboTAGTB;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cboTAGTE;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cboTAGPH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboTAGCL;
        private System.Windows.Forms.TextBox txtNODEID;
        private System.Windows.Forms.Label label32;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl utabRTMonitorPoint;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPoint;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox cboPartition;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTemp1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPoint1;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtRS1;
        private System.Windows.Forms.TextBox txtDR1;
        private System.Windows.Forms.TextBox txtRT1;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtAFilter1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cboMBlock1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cboLBlock1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtSDate1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtEDate1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnAnalysis1;
        private System.Windows.Forms.Button btnClose1;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private System.Windows.Forms.Panel panel9;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPoint2_2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPoint2_1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Panel panel8;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtSDate2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtEDate2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboSBlock2;
        private System.Windows.Forms.Button btnQuery2;
        private System.Windows.Forms.Button btnClose2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboMBlock2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboLBlock2;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTemp2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPoint3;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtRS2;
        private System.Windows.Forms.TextBox txtDR2;
        private System.Windows.Forms.TextBox txtRT2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtAFilter2;
        private System.Windows.Forms.Label label34;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtSDate3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtEDate3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnAnalysis3;
        private System.Windows.Forms.Button btnClose3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboMBlock3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboLBlock3;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
    }
}