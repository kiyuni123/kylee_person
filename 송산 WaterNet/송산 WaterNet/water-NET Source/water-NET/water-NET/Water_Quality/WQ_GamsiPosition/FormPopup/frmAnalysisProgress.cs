﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WQ_GamsiPosition.FormPopup
{
    public partial class frmAnalysisProgress : Form
    {
        public frmAnalysisProgress()
        {
            InitializeComponent();
        }

        private void frmAnalysisProgress_Load(object sender, EventArgs e)
        {
            this.tmrProgress.Stop();

            this.progressBar.Maximum = 50;
            this.progressBar.Minimum = 0;

            this.tmrProgress.Interval = 10;
            this.tmrProgress.Start();
        }

        private void tmrProgress_Tick(object sender, EventArgs e)
        {
            this.tmrProgress.Stop();
            if (this.progressBar.Value != this.progressBar.Maximum)
            {
                this.progressBar.Value = this.progressBar.Value + 10;
            }
            else
            {
                this.progressBar.Value = 0;
            }
            this.progressBar.Refresh();
            Application.DoEvents();
            this.tmrProgress.Start();
        }
    }
}
