﻿namespace WaterNet.WQ_GamsiPosition.FormPopup
{
    partial class frmWQPOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cFXRT = new ChartFX.WinForms.Chart();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.uGridItemP = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cboITEM = new System.Windows.Forms.ComboBox();
            this.udtSDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.udtEDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSaveP = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCloseP = new System.Windows.Forms.Button();
            this.cboMBlockP = new System.Windows.Forms.ComboBox();
            this.btnQueryP = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboMNTPP = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboSBlockP = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboLBlockP = new System.Windows.Forms.ComboBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.rdoOption2 = new System.Windows.Forms.RadioButton();
            this.rdoOption1 = new System.Windows.Forms.RadioButton();
            this.uGridItem = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.txtLevel1Top = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cboAlertFlag = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtLevel3Low = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtLevel3Top = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtLevel2Low = new System.Windows.Forms.TextBox();
            this.btnSaveL = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtLevel2Top = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtLevel1Low = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.uGridGSPoint = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCloseL = new System.Windows.Forms.Button();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.btnQueryL = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.utabOption = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage4 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl7.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXRT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridItemP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.ultraTabPageControl8.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridGSPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utabOption)).BeginInit();
            this.utabOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.panel3);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox16);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox18);
            this.ultraTabPageControl7.Controls.Add(this.pictureBox19);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(642, 604);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cFXRT);
            this.panel3.Controls.Add(this.pictureBox6);
            this.panel3.Controls.Add(this.uGridItemP);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(634, 596);
            this.panel3.TabIndex = 112;
            // 
            // cFXRT
            // 
            this.cFXRT.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.cFXRT.AxisX.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cFXRT.AxisX.Staggered = true;
            this.cFXRT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cFXRT.Location = new System.Drawing.Point(0, 260);
            this.cFXRT.Name = "cFXRT";
            this.cFXRT.Size = new System.Drawing.Size(634, 336);
            this.cFXRT.TabIndex = 118;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Gold;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(0, 256);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(634, 4);
            this.pictureBox6.TabIndex = 217;
            this.pictureBox6.TabStop = false;
            // 
            // uGridItemP
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridItemP.DisplayLayout.Appearance = appearance4;
            this.uGridItemP.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridItemP.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItemP.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItemP.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGridItemP.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItemP.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.uGridItemP.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridItemP.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridItemP.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridItemP.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridItemP.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridItemP.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.uGridItemP.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridItemP.DisplayLayout.Override.CellAppearance = appearance5;
            this.uGridItemP.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridItemP.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItemP.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.uGridItemP.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridItemP.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridItemP.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGridItemP.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGridItemP.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridItemP.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.uGridItemP.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridItemP.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridItemP.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridItemP.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridItemP.Location = new System.Drawing.Point(0, 78);
            this.uGridItemP.Name = "uGridItemP";
            this.uGridItemP.Size = new System.Drawing.Size(634, 178);
            this.uGridItemP.TabIndex = 113;
            this.uGridItemP.Text = "ultraGrid1";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 74);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(634, 4);
            this.pictureBox2.TabIndex = 117;
            this.pictureBox2.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.cboITEM);
            this.panel7.Controls.Add(this.udtSDate);
            this.panel7.Controls.Add(this.udtEDate);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.btnSaveP);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.btnCloseP);
            this.panel7.Controls.Add(this.cboMBlockP);
            this.panel7.Controls.Add(this.btnQueryP);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.cboMNTPP);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.cboSBlockP);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.cboLBlockP);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(634, 74);
            this.panel7.TabIndex = 107;
            // 
            // cboITEM
            // 
            this.cboITEM.FormattingEnabled = true;
            this.cboITEM.Location = new System.Drawing.Point(78, 48);
            this.cboITEM.Name = "cboITEM";
            this.cboITEM.Size = new System.Drawing.Size(165, 19);
            this.cboITEM.TabIndex = 201;
            // 
            // udtSDate
            // 
            this.udtSDate.Location = new System.Drawing.Point(428, 24);
            this.udtSDate.Name = "udtSDate";
            this.udtSDate.Size = new System.Drawing.Size(93, 20);
            this.udtSDate.TabIndex = 198;
            this.udtSDate.Visible = false;
            // 
            // udtEDate
            // 
            this.udtEDate.Location = new System.Drawing.Point(540, 24);
            this.udtEDate.Name = "udtEDate";
            this.udtEDate.Size = new System.Drawing.Size(89, 20);
            this.udtEDate.TabIndex = 199;
            this.udtEDate.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(524, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 12);
            this.label6.TabIndex = 200;
            this.label6.Text = "~";
            this.label6.Visible = false;
            // 
            // btnSaveP
            // 
            this.btnSaveP.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Save;
            this.btnSaveP.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveP.Location = new System.Drawing.Point(488, 45);
            this.btnSaveP.Name = "btnSaveP";
            this.btnSaveP.Size = new System.Drawing.Size(70, 26);
            this.btnSaveP.TabIndex = 197;
            this.btnSaveP.Text = "저장";
            this.btnSaveP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSaveP.UseVisualStyleBackColor = true;
            this.btnSaveP.Click += new System.EventHandler(this.btnSaveP_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 12);
            this.label5.TabIndex = 194;
            this.label5.Text = "감시항목 :";
            // 
            // btnCloseP
            // 
            this.btnCloseP.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Close2;
            this.btnCloseP.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCloseP.Location = new System.Drawing.Point(559, 45);
            this.btnCloseP.Name = "btnCloseP";
            this.btnCloseP.Size = new System.Drawing.Size(70, 26);
            this.btnCloseP.TabIndex = 193;
            this.btnCloseP.Text = "닫기";
            this.btnCloseP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCloseP.UseVisualStyleBackColor = true;
            this.btnCloseP.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cboMBlockP
            // 
            this.cboMBlockP.FormattingEnabled = true;
            this.cboMBlockP.Location = new System.Drawing.Point(284, 2);
            this.cboMBlockP.Name = "cboMBlockP";
            this.cboMBlockP.Size = new System.Drawing.Size(140, 19);
            this.cboMBlockP.TabIndex = 171;
            this.cboMBlockP.SelectedIndexChanged += new System.EventHandler(this.cboMBlockP_SelectedIndexChanged);
            // 
            // btnQueryP
            // 
            this.btnQueryP.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Query;
            this.btnQueryP.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQueryP.Location = new System.Drawing.Point(417, 45);
            this.btnQueryP.Name = "btnQueryP";
            this.btnQueryP.Size = new System.Drawing.Size(70, 26);
            this.btnQueryP.TabIndex = 170;
            this.btnQueryP.Text = "조회";
            this.btnQueryP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQueryP.UseVisualStyleBackColor = true;
            this.btnQueryP.Click += new System.EventHandler(this.btnQueryP_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 12);
            this.label1.TabIndex = 168;
            this.label1.Text = "감시지점 :";
            // 
            // cboMNTPP
            // 
            this.cboMNTPP.FormattingEnabled = true;
            this.cboMNTPP.Location = new System.Drawing.Point(78, 25);
            this.cboMNTPP.Name = "cboMNTPP";
            this.cboMNTPP.Size = new System.Drawing.Size(346, 19);
            this.cboMNTPP.TabIndex = 167;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(430, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 12);
            this.label2.TabIndex = 166;
            this.label2.Text = "소블록 :";
            // 
            // cboSBlockP
            // 
            this.cboSBlockP.FormattingEnabled = true;
            this.cboSBlockP.Location = new System.Drawing.Point(490, 2);
            this.cboSBlockP.Name = "cboSBlockP";
            this.cboSBlockP.Size = new System.Drawing.Size(140, 19);
            this.cboSBlockP.TabIndex = 165;
            this.cboSBlockP.SelectedIndexChanged += new System.EventHandler(this.cboSBlockP_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(224, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 12);
            this.label3.TabIndex = 164;
            this.label3.Text = "중블록 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(19, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 12);
            this.label4.TabIndex = 162;
            this.label4.Text = "대블록 :";
            // 
            // cboLBlockP
            // 
            this.cboLBlockP.FormattingEnabled = true;
            this.cboLBlockP.Location = new System.Drawing.Point(78, 2);
            this.cboLBlockP.Name = "cboLBlockP";
            this.cboLBlockP.Size = new System.Drawing.Size(140, 19);
            this.cboLBlockP.TabIndex = 161;
            this.cboLBlockP.SelectedIndexChanged += new System.EventHandler(this.cboLBlockP_SelectedIndexChanged);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox16.Location = new System.Drawing.Point(0, 4);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(4, 596);
            this.pictureBox16.TabIndex = 110;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox17.Location = new System.Drawing.Point(638, 4);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(4, 596);
            this.pictureBox17.TabIndex = 111;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox18.Location = new System.Drawing.Point(0, 600);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(642, 4);
            this.pictureBox18.TabIndex = 109;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox19.Location = new System.Drawing.Point(0, 0);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(642, 4);
            this.pictureBox19.TabIndex = 108;
            this.pictureBox19.TabStop = false;
            // 
            // ultraTabPageControl8
            // 
            this.ultraTabPageControl8.Controls.Add(this.panel6);
            this.ultraTabPageControl8.Controls.Add(this.panel5);
            this.ultraTabPageControl8.Controls.Add(this.pictureBox1);
            this.ultraTabPageControl8.Controls.Add(this.panel4);
            this.ultraTabPageControl8.Controls.Add(this.pictureBox4);
            this.ultraTabPageControl8.Controls.Add(this.pictureBox20);
            this.ultraTabPageControl8.Controls.Add(this.pictureBox21);
            this.ultraTabPageControl8.Controls.Add(this.pictureBox22);
            this.ultraTabPageControl8.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl8.Name = "ultraTabPageControl8";
            this.ultraTabPageControl8.Size = new System.Drawing.Size(642, 604);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.pictureBox3);
            this.panel6.Controls.Add(this.rdoOption2);
            this.panel6.Controls.Add(this.rdoOption1);
            this.panel6.Controls.Add(this.uGridItem);
            this.panel6.Controls.Add(this.pictureBox5);
            this.panel6.Controls.Add(this.txtLevel1Top);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.cboAlertFlag);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.txtLevel3Low);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.txtLevel3Top);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.txtLevel2Low);
            this.panel6.Controls.Add(this.btnSaveL);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.txtLevel2Top);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.txtLevel1Low);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 408);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(634, 192);
            this.panel6.TabIndex = 110;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(0, 117);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(634, 4);
            this.pictureBox3.TabIndex = 219;
            this.pictureBox3.TabStop = false;
            // 
            // rdoOption2
            // 
            this.rdoOption2.AutoSize = true;
            this.rdoOption2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoOption2.Location = new System.Drawing.Point(526, 126);
            this.rdoOption2.Name = "rdoOption2";
            this.rdoOption2.Size = new System.Drawing.Size(104, 16);
            this.rdoOption2.TabIndex = 218;
            this.rdoOption2.TabStop = true;
            this.rdoOption2.Text = "2, 3차로 경보";
            this.rdoOption2.UseVisualStyleBackColor = true;
            // 
            // rdoOption1
            // 
            this.rdoOption1.AutoSize = true;
            this.rdoOption1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoOption1.Location = new System.Drawing.Point(433, 126);
            this.rdoOption1.Name = "rdoOption1";
            this.rdoOption1.Size = new System.Drawing.Size(87, 16);
            this.rdoOption1.TabIndex = 217;
            this.rdoOption1.TabStop = true;
            this.rdoOption1.Text = "1차로 경보";
            this.rdoOption1.UseVisualStyleBackColor = true;
            // 
            // uGridItem
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridItem.DisplayLayout.Appearance = appearance25;
            this.uGridItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGridItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItem.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGridItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridItem.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridItem.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.uGridItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridItem.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGridItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridItem.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.uGridItem.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.uGridItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.uGridItem.DisplayLayout.Override.RowAppearance = appearance35;
            this.uGridItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.uGridItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridItem.Location = new System.Drawing.Point(0, 4);
            this.uGridItem.Name = "uGridItem";
            this.uGridItem.Size = new System.Drawing.Size(634, 113);
            this.uGridItem.TabIndex = 143;
            this.uGridItem.Text = "ultraGrid1";
            this.uGridItem.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGridItem_AfterSelectChange);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Gold;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(634, 4);
            this.pictureBox5.TabIndex = 216;
            this.pictureBox5.TabStop = false;
            // 
            // txtLevel1Top
            // 
            this.txtLevel1Top.Location = new System.Drawing.Point(202, 123);
            this.txtLevel1Top.Name = "txtLevel1Top";
            this.txtLevel1Top.Size = new System.Drawing.Size(29, 20);
            this.txtLevel1Top.TabIndex = 193;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(5, 174);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(110, 12);
            this.label25.TabIndex = 215;
            this.label25.Text = "3차 경보 설정치 :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(5, 151);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(110, 12);
            this.label26.TabIndex = 214;
            this.label26.Text = "2차 경보 설정치 :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(5, 128);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 12);
            this.label27.TabIndex = 213;
            this.label27.Text = "1차 경보 설정치 :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(431, 151);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(93, 12);
            this.label24.TabIndex = 212;
            this.label24.Text = "경보발신여부 :";
            // 
            // cboAlertFlag
            // 
            this.cboAlertFlag.FormattingEnabled = true;
            this.cboAlertFlag.Location = new System.Drawing.Point(433, 169);
            this.cboAlertFlag.Name = "cboAlertFlag";
            this.cboAlertFlag.Size = new System.Drawing.Size(103, 19);
            this.cboAlertFlag.TabIndex = 211;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(250, 174);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 12);
            this.label23.TabIndex = 209;
            this.label23.Text = "기준치 하한값";
            // 
            // txtLevel3Low
            // 
            this.txtLevel3Low.Location = new System.Drawing.Point(350, 169);
            this.txtLevel3Low.Name = "txtLevel3Low";
            this.txtLevel3Low.Size = new System.Drawing.Size(29, 20);
            this.txtLevel3Low.TabIndex = 208;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(115, 174);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 12);
            this.label21.TabIndex = 206;
            this.label21.Text = "기준치 상한값";
            // 
            // txtLevel3Top
            // 
            this.txtLevel3Top.Location = new System.Drawing.Point(202, 169);
            this.txtLevel3Top.Name = "txtLevel3Top";
            this.txtLevel3Top.Size = new System.Drawing.Size(29, 20);
            this.txtLevel3Top.TabIndex = 205;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(250, 149);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 12);
            this.label19.TabIndex = 203;
            this.label19.Text = "기준치 하한값";
            // 
            // txtLevel2Low
            // 
            this.txtLevel2Low.Location = new System.Drawing.Point(350, 146);
            this.txtLevel2Low.Name = "txtLevel2Low";
            this.txtLevel2Low.Size = new System.Drawing.Size(29, 20);
            this.txtLevel2Low.TabIndex = 202;
            // 
            // btnSaveL
            // 
            this.btnSaveL.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Save;
            this.btnSaveL.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveL.Location = new System.Drawing.Point(560, 164);
            this.btnSaveL.Name = "btnSaveL";
            this.btnSaveL.Size = new System.Drawing.Size(70, 26);
            this.btnSaveL.TabIndex = 192;
            this.btnSaveL.Text = "저장";
            this.btnSaveL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSaveL.UseVisualStyleBackColor = true;
            this.btnSaveL.Click += new System.EventHandler(this.btnSaveL_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(115, 151);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 12);
            this.label17.TabIndex = 200;
            this.label17.Text = "기준치 상한값";
            // 
            // txtLevel2Top
            // 
            this.txtLevel2Top.Location = new System.Drawing.Point(202, 146);
            this.txtLevel2Top.Name = "txtLevel2Top";
            this.txtLevel2Top.Size = new System.Drawing.Size(29, 20);
            this.txtLevel2Top.TabIndex = 199;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(380, 128);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 12);
            this.label14.TabIndex = 198;
            this.label14.Text = "(%)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(261, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 12);
            this.label15.TabIndex = 197;
            this.label15.Text = "수질패턴 하한";
            // 
            // txtLevel1Low
            // 
            this.txtLevel1Low.Location = new System.Drawing.Point(350, 123);
            this.txtLevel1Low.Name = "txtLevel1Low";
            this.txtLevel1Low.Size = new System.Drawing.Size(29, 20);
            this.txtLevel1Low.TabIndex = 196;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(232, 128);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 12);
            this.label13.TabIndex = 195;
            this.label13.Text = "(%)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(115, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 12);
            this.label12.TabIndex = 194;
            this.label12.Text = "수질패턴 상한";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.uGridGSPoint);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(4, 58);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(634, 350);
            this.panel5.TabIndex = 108;
            // 
            // uGridGSPoint
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridGSPoint.DisplayLayout.Appearance = appearance37;
            this.uGridGSPoint.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridGSPoint.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridGSPoint.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridGSPoint.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.uGridGSPoint.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridGSPoint.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.uGridGSPoint.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridGSPoint.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridGSPoint.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridGSPoint.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.uGridGSPoint.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridGSPoint.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.uGridGSPoint.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridGSPoint.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGridGSPoint.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridGSPoint.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridGSPoint.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.uGridGSPoint.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.uGridGSPoint.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridGSPoint.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.uGridGSPoint.DisplayLayout.Override.RowAppearance = appearance47;
            this.uGridGSPoint.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridGSPoint.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.uGridGSPoint.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridGSPoint.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridGSPoint.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridGSPoint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridGSPoint.Location = new System.Drawing.Point(0, 0);
            this.uGridGSPoint.Name = "uGridGSPoint";
            this.uGridGSPoint.Size = new System.Drawing.Size(634, 350);
            this.uGridGSPoint.TabIndex = 143;
            this.uGridGSPoint.Text = "ultraGrid1";
            this.uGridGSPoint.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGridGSPoint_AfterSelectChange);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(4, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(634, 4);
            this.pictureBox1.TabIndex = 116;
            this.pictureBox1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnCloseL);
            this.panel4.Controls.Add(this.cboMBlock);
            this.panel4.Controls.Add(this.btnQueryL);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.cboSBlock);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.cboLBlock);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(634, 50);
            this.panel4.TabIndex = 106;
            // 
            // btnCloseL
            // 
            this.btnCloseL.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Close2;
            this.btnCloseL.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCloseL.Location = new System.Drawing.Point(561, 21);
            this.btnCloseL.Name = "btnCloseL";
            this.btnCloseL.Size = new System.Drawing.Size(70, 26);
            this.btnCloseL.TabIndex = 193;
            this.btnCloseL.Text = "닫기";
            this.btnCloseL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCloseL.UseVisualStyleBackColor = true;
            this.btnCloseL.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(284, 2);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock.TabIndex = 171;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // btnQueryL
            // 
            this.btnQueryL.Image = global::WaterNet.WQ_GamsiPosition.Properties.Resources.Query;
            this.btnQueryL.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQueryL.Location = new System.Drawing.Point(489, 21);
            this.btnQueryL.Name = "btnQueryL";
            this.btnQueryL.Size = new System.Drawing.Size(70, 26);
            this.btnQueryL.TabIndex = 170;
            this.btnQueryL.Text = "조회";
            this.btnQueryL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQueryL.UseVisualStyleBackColor = true;
            this.btnQueryL.Click += new System.EventHandler(this.btnQueryL_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(430, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 166;
            this.label9.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(490, 2);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 19);
            this.cboSBlock.TabIndex = 165;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(224, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 164;
            this.label8.Text = "중블록 :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(19, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 162;
            this.label7.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(78, 2);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock.TabIndex = 161;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox4.Location = new System.Drawing.Point(0, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(4, 596);
            this.pictureBox4.TabIndex = 114;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox20.Location = new System.Drawing.Point(638, 4);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(4, 596);
            this.pictureBox20.TabIndex = 115;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox21.Location = new System.Drawing.Point(0, 600);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(642, 4);
            this.pictureBox21.TabIndex = 113;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox22.Location = new System.Drawing.Point(0, 0);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(642, 4);
            this.pictureBox22.TabIndex = 112;
            this.pictureBox22.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.utabOption);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(646, 630);
            this.panel1.TabIndex = 131;
            // 
            // utabOption
            // 
            this.utabOption.Controls.Add(this.ultraTabSharedControlsPage4);
            this.utabOption.Controls.Add(this.ultraTabPageControl7);
            this.utabOption.Controls.Add(this.ultraTabPageControl8);
            this.utabOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.utabOption.Location = new System.Drawing.Point(0, 0);
            this.utabOption.Name = "utabOption";
            this.utabOption.SharedControlsPage = this.ultraTabSharedControlsPage4;
            this.utabOption.Size = new System.Drawing.Size(646, 630);
            this.utabOption.TabIndex = 144;
            ultraTab9.TabPage = this.ultraTabPageControl7;
            ultraTab9.Text = "패턴산정";
            ultraTab10.TabPage = this.ultraTabPageControl8;
            ultraTab10.Text = "경보레벨";
            this.utabOption.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab9,
            ultraTab10});
            // 
            // ultraTabSharedControlsPage4
            // 
            this.ultraTabSharedControlsPage4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage4.Name = "ultraTabSharedControlsPage4";
            this.ultraTabSharedControlsPage4.Size = new System.Drawing.Size(642, 604);
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 630);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(650, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 630);
            this.PicFrRight.TabIndex = 107;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 634);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(654, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(654, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 100);
            this.panel2.TabIndex = 0;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(200, 100);
            this.ultraTabControl1.TabIndex = 0;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(196, 77);
            // 
            // frmWQPOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 638);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWQPOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "실시간감시지점별 경보레벨";
            this.Load += new System.EventHandler(this.frmWQPOption_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQPOption_FormClosing);
            this.ultraTabPageControl7.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXRT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridItemP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtSDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtEDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.ultraTabPageControl8.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridGSPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utabOption)).EndInit();
            this.utabOption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridGSPoint;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnQueryL;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Button btnCloseL;
        private System.Windows.Forms.Panel panel6;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl utabOption;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl8;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.TextBox txtLevel1Top;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cboAlertFlag;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtLevel3Low;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtLevel3Top;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtLevel2Low;
        private System.Windows.Forms.Button btnSaveL;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtLevel2Top;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtLevel1Low;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnCloseP;
        private System.Windows.Forms.ComboBox cboMBlockP;
        private System.Windows.Forms.Button btnQueryP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboMNTPP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboSBlockP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboLBlockP;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSaveP;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridItemP;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtSDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtEDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rdoOption2;
        private System.Windows.Forms.RadioButton rdoOption1;
        private System.Windows.Forms.ComboBox cboITEM;
        private ChartFX.WinForms.Chart cFXRT;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}