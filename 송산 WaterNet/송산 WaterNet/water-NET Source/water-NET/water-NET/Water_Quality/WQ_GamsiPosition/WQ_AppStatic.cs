﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WQ_GamsiPosition
{
    class WQ_AppStatic
    {
        #region 팝업 폼 오픈여부 선언부

        /// <summary>
        /// frmWQPFacilities 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_FACILITIES = false;
        /// <summary>
        /// frmWQPMonitorPoint 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_MONITOR_POINT = false;

        #endregion

        public static double MAP_X = 0; //GIS에서 Mouse Down시 구한 Map X 좌표
        public static double MAP_Y = 0; //GIS에서 Mouse Down시 구한 Map Y 좌표

        /// <summary>
        /// frmWQMain의 (ITOCControl2)axTOC.Object
        /// </summary>
        public static ITOCControl2 ITOC;

        /// <summary>
        /// 그리드에서 선택한 지점의 IGeometry
        /// </summary>
        public static IGeometry CURRENT_IGEOMETRY;

        /// <summary>
        /// 중요시설, 감시지점 Shape 파일이 있는 경로
        /// </summary>
        public static string SHAPE_PATH = @"C:\Water-NET\WaterNet-Data\Pipegraphic\";
        public static string SHAPE_TEMP_PATH = @"C:\Water-NET\Upload\";

        /// <summary>
        /// Directory를 생성한다.
        /// </summary>
        /// <param name="strDirPath">생성할 Directory의 Full Path</param>
        public static void CreateDirectory(string strDirPath)
        {
            Directory.CreateDirectory(strDirPath);
        }

        /// <summary>
        /// File를 Copy 한다.
        /// </summary>
        /// <param name="strSrcFilePath">Copy할 Source File Path</param>
        /// <param name="strDstFilePath">Copy할 Destination File Path</param>
        public static void CopyFile(string strSrcFilePath, string strDstFilePath, bool OverWrite)
        {
            File.Copy(strSrcFilePath, strDstFilePath, OverWrite);
        }
    }
}
