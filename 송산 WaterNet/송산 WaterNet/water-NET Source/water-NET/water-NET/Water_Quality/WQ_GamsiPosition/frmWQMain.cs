﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WQ_GamsiPosition
{
    /// <summary>
    /// Project ID : WN_WQ_A03
    /// Project Explain : 감시지점선정 , 기본적으로 WaterAOCore.frmMap을 사용
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.28
    /// Form Explain : 감시지점선정  Main Form (참고 자료 : WaterBreak)
    /// </summary>
    public partial class frmWQMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        /// <summary>
        /// frmWQPChooseINP 에서 선택된 INP File의 Name
        /// </summary>
        private string m_INP_FILENAME = string.Empty;
        /// <summary>
        /// m_INP_FILENAME과 동일한 파일명의 SHP(Shape) File 의 Name
        /// </summary>
        private string m_SHP_FILENAME = string.Empty;

        int m_X; //GIS의 윈도우 X 좌표
        int m_Y; //GIS의 윈도우 Y 좌표

        ArrayList m_ArrayContentsMP = new ArrayList(); //감시지점 간략 폼배열
        ArrayList m_ArrayContentsIF = new ArrayList(); //중요시설 간략 폼배열
        ILayer m_LayerMP = null; //감시지점 레이어
        ILayer m_LayerIF = null; //중요시설 레이어

        public frmWQMain()
        {
            WQ_AppStatic.IS_SHOW_FORM_FACILITIES = false;
            WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = false;

            InitializeComponent();
            InitializeSetting();
        }

        //=========================================================
        //
        //                    동진 수정_2012.6.08
        //			권한박탈(조회만 가능)
        //=========================================================

        private void frmWQMain_Load(object sender, EventArgs e)
        {


        }

        //============================================================================

        #region IForminterface 멤버 (메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.)

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();

            WQ_AppStatic.ITOC = (ITOCControl2)axTOC.Object;

            m_LayerMP = WaterAOCore.ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_Pipegraphic, "감시지점");
            WaterAOCore.ArcManager.SetDefaultRenderer(axMap.ActiveView.FocusMap, (IFeatureLayer)m_LayerMP);
            axMap.AddLayer(m_LayerMP);
            m_LayerMP.Visible = true;

            m_LayerIF = WaterAOCore.ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_Pipegraphic, "중요시설");
            WaterAOCore.ArcManager.SetDefaultRenderer(axMap.ActiveView.FocusMap, (IFeatureLayer)m_LayerIF);
            axMap.AddLayer(m_LayerIF);
            m_LayerIF.Visible = true;

            this.SetLayerFromDBMP();
            this.SetLayerFromDBIF();
         }


        #region Button Events

        //감시지점
        private void btnMNTPOINT_Click(object sender, System.EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT == false)
            {
                WaterNet.WQ_GamsiPosition.FormPopup.frmWQPMonitorPoint oForm = new WaterNet.WQ_GamsiPosition.FormPopup.frmWQPMonitorPoint();
                oForm.IMAP = (IMapControl3)axMap.Object;
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        //중요시설
        private void btnFacilities_Click(object sender, System.EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_FACILITIES == false)
            {
                WaterNet.WQ_GamsiPosition.FormPopup.frmWQPFacilities oForm = new WaterNet.WQ_GamsiPosition.FormPopup.frmWQPFacilities();
                oForm.IMAP = (IMapControl3)axMap.Object;
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        #endregion


        #region Control Events

        /// <summary>
        /// Map에서 오른쪽 마우스 버튼 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            ///GIS 상 좌표
            WQ_AppStatic.MAP_X = e.mapX;
            WQ_AppStatic.MAP_Y = e.mapY;
            ///Windows 상 좌표
            m_X = e.x;
            m_Y = e.y;

            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;

            #region 위치선택 - 지도에 사고지점을 선택하고, 별표 표시

            if (WQ_AppStatic.IS_SHOW_FORM_FACILITIES == true)
            {
                ///기존 객체 및 환경 초기화
                ArcManager.ClearSelection(axMap.ActiveView.FocusMap);
                IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
                pGraphicsContainer.DeleteAllElements();

                IActiveView activeView = axMap.ActiveView;
                IScreenDisplay screenDisplay = activeView.ScreenDisplay;
                IColor pColor = ArcManager.GetColor(255, 0, 0);

                ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
                IRubberBand pRubberBand = new RubberPointClass();
                IPoint pSearchPoint = (IPoint)pRubberBand.TrackNew(screenDisplay, pSymbol);

                if (pSearchPoint == null | pSearchPoint.IsEmpty) return;

                IElement pElement = new MarkerElementClass();
                pElement.Geometry = pSearchPoint as IGeometry;
                IMarkerElement pMarkerElement = pElement as IMarkerElement;
                pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;

                pGraphicsContainer.AddElement(pElement, 0);
                ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
                Application.DoEvents();
                ArcManager.FlashShape(axMap.ActiveView, pSearchPoint as IPoint, 3);
            }

            #endregion 지도에 사고지점을 선택하고, 별표 표시 끝

            IPoint pPoint = new PointClass();
            pPoint.X = WQ_AppStatic.MAP_X;
            pPoint.Y = WQ_AppStatic.MAP_Y;
            pPoint.Z = 0;

            IGeometry pGeom = (IGeometry)pPoint;

            WQ_AppStatic.CURRENT_IGEOMETRY = pGeom;
            
            double dblContains = 0;

            if (ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)100000)
            {
                dblContains = 300;
            }
            else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)100000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)50000)
            {
                dblContains = 200;
            }
            else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)50000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)15000)
            {
                dblContains = 100;
            }
            else
            {
                dblContains = 10;
            }
            
            ///*****************************************************************************
            ///ArcManager.GetSpatialCursor()로 Map에 마우스 클릭한 Map 상 좌표에서 반경 50m 
            ///지점의 특정 값을 가져와 FeatureCursor로 만들고 FeatureCursor로 Feature를
            ///만든 다음, ArcManager.GetValue()로 Layer 상의 특정 Field값을 Object로 받는다.
            ///----------------------------------------------------------------------------S
            //감시지점 레이어
            IFeatureCursor pIFCursorMP = ArcManager.GetSpatialCursor(m_LayerMP, pGeom, dblContains, esriSpatialRelEnum.esriSpatialRelContains, string.Empty);

            IFeature pFeatureMP = pIFCursorMP.NextFeature();

            if (pFeatureMP != null)
            {
                object oNO = WaterAOCore.ArcManager.GetValue(pFeatureMP, "NOS");

                if (oNO != null)
                {
                    foreach (var item in m_ArrayContentsMP) ((Form)item).Visible = false;
                    this.MakeControlsOnlySelectedPoint(oNO.ToString(), 0);
                    IEnvelope pEnvelope = axMap.Extent;
                    this.ExtentUpdatedEvent(pEnvelope, 0);
                }
                else
                {
                    foreach (var item in m_ArrayContentsMP) ((Form)item).Visible = false;
                }
            }
            else
            {
                foreach (var item in m_ArrayContentsMP) ((Form)item).Visible = false;
            }

            //중요시설 레이어
            IFeatureCursor pIFCursorIF = ArcManager.GetSpatialCursor(m_LayerIF, pGeom, dblContains, esriSpatialRelEnum.esriSpatialRelContains, string.Empty);

            IFeature pFeatureIF = pIFCursorIF.NextFeature();

            if (pFeatureIF != null)
            {
                object oNO = WaterAOCore.ArcManager.GetValue(pFeatureIF, "NOS");

                if (oNO != null)
                {
                    foreach (var item in m_ArrayContentsIF) ((Form)item).Visible = false;
                    this.MakeControlsOnlySelectedPoint(oNO.ToString(), 1);
                    IEnvelope pEnvelope = axMap.Extent;
                    this.ExtentUpdatedEvent(pEnvelope, 1);
                }
                else
                {
                    foreach (var item in m_ArrayContentsIF) ((Form)item).Visible = false;
                }
            }
            else
            {
                foreach (var item in m_ArrayContentsIF) ((Form)item).Visible = false;
            }

            switch (e.button)
            {
                case 1:    //왼쪽버튼
                    break;

                case 2:    //오른쪽버튼
                    //ContextMenuStripPopup.Show(axMap, m_X, m_Y);
                    break;
            }
        }

        private void axMap_OnExtentUpdated(object sender, IMapControlEvents2_OnExtentUpdatedEvent e)
        {
            //Map의 축척이 15000 이하일 경우에만 간략 폼을 뷰한다.
            //if (ArcManager.GetMapScale((IMapControl3)axMap.Object) < (double)15000)
            //{
            //    this.MakeControls(0);
            //    this.ExtentUpdatedEvent(e.newEnvelope as IEnvelope, 0);
            //    this.MakeControls(1);
            //    this.ExtentUpdatedEvent(e.newEnvelope as IEnvelope, 1);
            //}
            //else
            //{
            //    foreach (var item in m_ArrayContentsMP) ((Form)item).Visible = false;
            //    foreach (var item in m_ArrayContentsIF) ((Form)item).Visible = false;
            //}
        }
        
        #endregion


        #region User Function

        /// <summary>
        /// Array에 저장된 실시간감시 간략화면을 가져온다
        /// </summary>
        /// <param name="nOID">유량계 OID</param>
        /// <returns></returns>
        private Form GetControl(string nOID, int iLType)
        {
            switch (iLType)
            {
                case 0:
                    foreach (var item in m_ArrayContentsMP)
                    {
                        if (((Form)item).Tag.ToString() == nOID)
                        {
                            return (Form)item;
                        }
                    }
                    break;
                case 1:
                    foreach (var item in m_ArrayContentsIF)
                    {
                        if (((Form)item).Tag.ToString() == nOID)
                        {
                            return (Form)item;
                        }
                    }
                    break;
            }

            return null;
        }

        /// <summary>
        /// 현재 Extent에 Intersect되는 감시지점의 실시간감시 간략화면 표시한다.
        /// </summary>
        /// <param name="pEnvelope"></param>
        private void ExtentUpdatedEvent(IEnvelope pEnvelope, int iLType)
        {
            IFeatureLayer pFeatureLayer = null;
            switch (iLType)
            {
                case 0:
                    if (m_LayerMP == null) return;
                    if (!(m_LayerMP is IFeatureLayer)) return;

                    pFeatureLayer = (IFeatureLayer)m_LayerMP;

                    foreach (var item in m_ArrayContentsMP) ((Form)item).Visible = false;
                    break;
                case 1:
                    if (m_LayerIF == null) return;
                    if (!(m_LayerIF is IFeatureLayer)) return;

                    pFeatureLayer = (IFeatureLayer)m_LayerIF;
                    foreach (var item in m_ArrayContentsIF) ((Form)item).Visible = false;
                    break;
            }

            if (!pFeatureLayer.Visible) return;
            if (pFeatureLayer.MaximumScale > axMap.MapScale && pFeatureLayer.MaximumScale != 0) return;
            if (pFeatureLayer.MinimumScale < axMap.MapScale && pFeatureLayer.MinimumScale != 0) return;

            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pEnvelope;
            pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;
            pSpatialFilter.WhereClause = string.Empty;

            IFeatureCursor pFCursor = pFeatureClass.Search(pSpatialFilter, true);
            IFeature pFeature = pFCursor.NextFeature();
            try
            {
                while (pFeature != null)
                {
                    IGeometry pGeom = pFeature.Shape;
                    if (pGeom is IPoint)
                    {
                        IPoint pPoint = pGeom as IPoint;
                        int x; int y;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.Units = esriUnits.esriMeters;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.FromMapPoint(pPoint, out x, out y);

                        Form oControl = GetControl(Convert.ToString(pFeature.OID), iLType);
                        if (oControl != null)
                        {
                            switch (iLType)
                            {
                                case 0:
                                    oControl.SetBounds(x + 11, y + 25, 298, 140);
                                    break;
                                case 1:
                                    oControl.SetBounds(x + 11, y + 25, 298, 115);
                                    break;
                            }
                            oControl.BringToFront();
                            oControl.Visible = true;
                            Application.DoEvents();
                        }
                    }
                    pFeature = pFCursor.NextFeature();
                }
            }
            catch (Exception oExec)
            {
                //Console.WriteLine(oExec.Message);
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pFCursor);
            }
        }

        /// <summary>
        /// MakeControl - 실시간감시 감시 데이터 화면 생성하여 Array에 저장
        /// </summary>
        private void MakeControls(int iLType)
        {
            IFeatureLayer pFeatureLayer = null;
            switch (iLType)
            {
                case 0:
                    if (m_LayerMP == null) return;
                    if (!(m_LayerMP is IFeatureLayer)) return;

                    pFeatureLayer = (IFeatureLayer)m_LayerMP;
                    break;
                case 1:
                    if (m_LayerIF == null) return;
                    if (!(m_LayerIF is IFeatureLayer)) return;

                    pFeatureLayer = (IFeatureLayer)m_LayerIF;
                    break;
            }

            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ICursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, string.Empty);
            IRow pRow = pCursor.NextRow();
            try
            {
                switch (iLType)
                {
                    case 0:
                        foreach (var item in m_ArrayContentsMP) ((Form)item).Close();
                        m_ArrayContentsMP.Clear();
                        while (pRow != null)
                        {
                            object o = WaterAOCore.ArcManager.GetValue(pRow, "FID");
                            object oNAME = WaterAOCore.ArcManager.GetValue(pRow, "NAMES");
                            object oNO = WaterAOCore.ArcManager.GetValue(pRow, "NOS");
                            if (o != null)
                            {
                                FormPopup.frmMPBrief oControl = new FormPopup.frmMPBrief();
                                oControl.TopLevel = false;
                                oControl.Parent = this;
                                oControl.Tag = Convert.ToString(o);
                                oControl.NO = oNO.ToString();
                                oControl.NAME = oNAME.ToString();
                                m_ArrayContentsMP.Add(oControl);
                            }
                            pRow = pCursor.NextRow();
                        }
                        break;
                    case 1:
                        foreach (var item in m_ArrayContentsIF) ((Form)item).Close();
                        m_ArrayContentsIF.Clear();
                        while (pRow != null)
                        {
                            object o = WaterAOCore.ArcManager.GetValue(pRow, "FID");
                            object oNAME = WaterAOCore.ArcManager.GetValue(pRow, "NAMES");
                            object oNO = WaterAOCore.ArcManager.GetValue(pRow, "NOS");
                            if (o != null)
                            {
                                FormPopup.frmIFBrief oControl = new FormPopup.frmIFBrief();
                                oControl.TopLevel = false;
                                oControl.Parent = this;
                                oControl.Tag = Convert.ToString(o);
                                oControl.NO = oNO.ToString();
                                oControl.NAME = oNAME.ToString();
                                m_ArrayContentsIF.Add(oControl);
                            }
                            pRow = pCursor.NextRow();
                        }
                        break;
                }
            }
            catch (Exception)
            {
                //Console.WriteLine("MakeControl");
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
            }
        }

        /// <summary>
        /// MakeControl - 실시간감시 감시 데이터 화면 생성하여 Array에 저장
        /// </summary>
        private void MakeControlsOnlySelectedPoint(string strNO, int iLType)
        {
            IFeatureLayer pFeatureLayer = null;
            switch (iLType)
            {
                case 0:
                    if (m_LayerMP == null) return;
                    if (!(m_LayerMP is IFeatureLayer)) return;

                    pFeatureLayer = (IFeatureLayer)m_LayerMP;

                    foreach (var item in m_ArrayContentsMP) ((Form)item).Close();
                    m_ArrayContentsMP.Clear();
                    break;
                case 1:
                    if (m_LayerIF == null) return;
                    if (!(m_LayerIF is IFeatureLayer)) return;

                    pFeatureLayer = (IFeatureLayer)m_LayerIF;

                    foreach (var item in m_ArrayContentsIF) ((Form)item).Close();
                    m_ArrayContentsIF.Clear();
                    break;
            }

            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "NOS='" + strNO + "'") as IFeatureCursor;
            IFeature pFeature = pCursor.NextFeature();
            
            try
            {
                if (pFeature != null)
                {
                    switch (iLType)
                    {
                        case 0:
                            FormPopup.frmMPBrief oControlMP = new FormPopup.frmMPBrief();
                            oControlMP.TopLevel = false;
                            oControlMP.Parent = this;
                            oControlMP.Tag = WaterAOCore.ArcManager.GetValue(pFeature, "FID").ToString();
                            oControlMP.NO = strNO;
                            oControlMP.NAME = WaterAOCore.ArcManager.GetValue(pFeature, "NAMES").ToString();

                            m_ArrayContentsMP.Add(oControlMP);
                            break;
                        case 1:
                            FormPopup.frmIFBrief oControlIF = new FormPopup.frmIFBrief();
                            oControlIF.TopLevel = false;
                            oControlIF.Parent = this;
                            oControlIF.Tag = WaterAOCore.ArcManager.GetValue(pFeature, "FID").ToString();
                            oControlIF.NO = strNO;
                            oControlIF.NAME = WaterAOCore.ArcManager.GetValue(pFeature, "NAMES").ToString();

                            m_ArrayContentsIF.Add(oControlIF);
                            break;
                    }
                }
            }
            catch (Exception)
            {
                //Console.WriteLine("MakeControl");
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
            }
        }

        /// <summary>
        /// 지점정보를 취득해 레이어에 정보를 SET한다.(중요시설)
        /// </summary>
        private void SetLayerFromDBIF()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            ArcManager.DeleteAllFeatures(m_LayerIF);

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.SBLOCK_CODE, A.FACILITIES_GBN, A.NO, A.FACILITIES_NAME, A.MAP_X, A.MAP_Y");
            oStringBuilder.AppendLine("FROM      WQ_IMPORTANT_FACILITIES A");
            oStringBuilder.AppendLine("ORDER BY  A.SBLOCK_CODE, A.FACILITIES_GBN, A.NO");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "SET_LAYER");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {

                IWorkspaceEdit pWorkspaceEdit = ArcManager.getWorkspaceEdit(m_LayerIF as IFeatureLayer);
                if (pWorkspaceEdit == null) return;

                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureBuffer pFeatureBuffer = ((IFeatureLayer)m_LayerIF).FeatureClass.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = ((IFeatureLayer)m_LayerIF).FeatureClass.Insert(true);
                    IFeature pFeaturebuffer = pFeatureBuffer as IFeature;

                    comReleaser.ManageLifetime(pFeatureCursor);

                    IPoint pPoint = new PointClass();

                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        //레이어의 지점 좌표
                        pPoint.PutCoords(Convert.ToDouble(oDRow["MAP_X"].ToString()), Convert.ToDouble(oDRow["MAP_Y"].ToString()));

                        //레이어의 지점 데이터
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("SBLOCK"), (object)oDRow["SBLOCK_CODE"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("GBN"), (object)oDRow["FACILITIES_GBN"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("NOS"), (object)oDRow["NO"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("NAMES"), (object)oDRow["FACILITIES_NAME"].ToString());

                        //레이어의 지점 좌표로 IGeometry
                        pFeaturebuffer.Shape = pPoint as IGeometry;

                        pFeatureCursor.InsertFeature(pFeatureBuffer);
                    }
                    pFeatureCursor.Flush();
                }

                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
            }

            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();

            pDS.Dispose();
        }

        /// <summary>
        /// 지점정보를 취득해 레이어에 정보를 SET한다.(감시지점)
        /// </summary>
        private void SetLayerFromDBMP()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            ArcManager.DeleteAllFeatures(m_LayerMP);

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.SBLOCK_CODE, A.MONPNT_GBN, A.MONITOR_NO, A.MONPNT, A.MAP_X, A.MAP_Y");
            oStringBuilder.AppendLine("FROM      WQ_RT_MONITOR_POINT A");
            oStringBuilder.AppendLine("WHERE     A.MONPNT_GBN <> '0'");
            oStringBuilder.AppendLine("ORDER BY  A.SBLOCK_CODE, A.MONPNT_GBN, A.MONITOR_NO");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "SET_LAYER");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {

                IWorkspaceEdit pWorkspaceEdit = ArcManager.getWorkspaceEdit(m_LayerMP as IFeatureLayer);
                if (pWorkspaceEdit == null) return;

                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureBuffer pFeatureBuffer = ((IFeatureLayer)m_LayerMP).FeatureClass.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = ((IFeatureLayer)m_LayerMP).FeatureClass.Insert(true);
                    IFeature pFeaturebuffer = pFeatureBuffer as IFeature;

                    comReleaser.ManageLifetime(pFeatureCursor);

                    IPoint pPoint = new PointClass();

                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        //레이어의 지점 좌표
                        pPoint.PutCoords(Convert.ToDouble(oDRow["MAP_X"].ToString()), Convert.ToDouble(oDRow["MAP_Y"].ToString()));

                        //레이어의 지점 데이터
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("SBLOCK"), (object)oDRow["SBLOCK_CODE"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("GBN"), (object)oDRow["MONPNT_GBN"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("NOS"), (object)oDRow["MONITOR_NO"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("NAMES"), (object)oDRow["MONPNT"].ToString());

                        //레이어의 지점 좌표로 IGeometry
                        pFeaturebuffer.Shape = pPoint as IGeometry;

                        pFeatureCursor.InsertFeature(pFeatureBuffer);
                    }
                    pFeatureCursor.Flush();
                }

                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
            }

            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();

            pDS.Dispose();
        }

        #endregion
    }
}
