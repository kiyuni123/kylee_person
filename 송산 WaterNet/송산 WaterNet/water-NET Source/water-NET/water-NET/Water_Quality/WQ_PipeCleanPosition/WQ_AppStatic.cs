﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WQ_PipeCleanSection
{
    class WQ_AppStatic
    {
        #region 선언부

        /// <summary>
        /// Oracle Database Manager
        /// </summary>
        public static OracleDBManager ORACLE_MANAGER = null;

        /// <summary>
        /// frmWQMain의 (IMapControl3)axMap.Object
        /// </summary>
        public static IMapControl3 IMAP;

        /// <summary>
        /// frmWQMain의 (ITOCControl2)axTOC.Object
        /// </summary>
        public static ITOCControl2 ITOC;

        /// <summary>
        /// frmWQMain의 axMap_OnMouseDown Event 또는 User의 동작에 의한 GIS상의 X 좌표
        /// 이후 GetCurrentIGeometry()에서 활용해 IGeometry를 생성하는데 사용
        /// </summary>
        public static double IMAP_X = 0;

        /// <summary>
        /// frmWQMain의 axMap_OnMouseDown Event 또는 User의 동작에 의한 GIS상의 X 좌표
        /// 이후 GetCurrentIGeometry()에서 활용해 IGeometry를 생성하는데 사용
        /// </summary>
        public static double IMAP_Y = 0;

        /// <summary>
        /// frmWQMain의 axMap_OnMouseDown Event에서 취득한 Window상의 X 좌표
        /// 이후 Control을 배치시킬때 사용
        /// </summary>
        public static int WIN_X = 0;

        /// <summary>
        /// frmWQMain의 axMap_OnMouseDown Event에서 취득한 Window상의 Y 좌표
        /// 이후 Control을 배치시킬때 사용
        /// </summary>
        public static int WIN_Y = 0;

        /// <summary>
        /// 그리드에서 선택한 지점의 IGeometry
        /// </summary>
        public static IGeometry CURRENT_IGEOMETRY;

        /// <summary>
        /// 관세척구간 Shape 파일이 있는 경로
        /// </summary>
        public static string SHAPE_PATH = @"C:\Water-NET\WaterNet-Data\Pipegraphic\";
        public static string SHAPE_TEMP_PATH = @"C:\Water-NET\Upload\";

        /// <summary>
        /// 모의시 INP 파일이 생성될 PATH
        /// </summary>
        public static string INP_PATH = @"C:\Water-NET\TEMP\SIMULATE\";

        #endregion

        #region 팝업 폼 오픈여부 선언부

        /// <summary>
        /// frmWQPPCResultManage 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_PC_RESULT_MANAGE = false;
        /// <summary>
        /// frmWQPPCSetSection 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_PC_SET_SECTION = false;

        #endregion

        #region User Function

        /// <summary>
        /// 가장 최근 axMap_OnMouseDown Event에서 취득한 GIS 상의 X, Y 좌표로 IGeometry를 반환한다.
        /// </summary>
        /// <returns></returns>
        public static IGeometry GetCurrentIGeometryFromXY()
        {
            IPoint pPoint = new PointClass();

            pPoint.X = WQ_AppStatic.IMAP_X;
            pPoint.Y = WQ_AppStatic.IMAP_Y;
            pPoint.Z = 0;

            IGeometry pGeom = (IGeometry)pPoint;

            return pGeom;
        }

        /// <summary>
        /// Directory를 생성한다.
        /// </summary>
        /// <param name="strDirPath">생성할 Directory의 Full Path</param>
        public static void CreateDirectory(string strDirPath)
        {
            Directory.CreateDirectory(strDirPath);
        }

        /// <summary>
        /// File를 Copy 한다.
        /// </summary>
        /// <param name="strSrcFilePath">Copy할 Source File Path</param>
        /// <param name="strDstFilePath">Copy할 Destination File Path</param>
        public static void CopyFile(string strSrcFilePath, string strDstFilePath, bool OverWrite)
        {
            File.Copy(strSrcFilePath, strDstFilePath, OverWrite);
        }

        #endregion
    }
}
