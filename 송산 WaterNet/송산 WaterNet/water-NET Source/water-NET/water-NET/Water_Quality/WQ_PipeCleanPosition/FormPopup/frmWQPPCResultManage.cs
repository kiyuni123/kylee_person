﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_PipeCleanSection.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A05
    /// Project Explain : 관세척구간관리
    /// Project Developer : 오두석
    /// Project Create Date : 2010.10.26
    /// Form Explain : 관세척구간현황 Popup Form
    /// </summary>
    public partial class frmWQPPCResultManage : Form
    {
        public frmWQPPCResultManage()
        {
            InitializeComponent();

            this.InitializeSetting();
        }

        private void frmWQPPCResultManaget_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================관세척구간관리(관세척 구간 처리 관리)

            object o = EMFrame.statics.AppStatic.USER_MENU["관세척구간관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }

            //===========================================================================

            WQ_AppStatic.IS_SHOW_FORM_PC_RESULT_MANAGE = true;

            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);

            WQ_Function.SetUDateTime_MaskInput(this.uDTS, 0, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.uDTS, -3);
            WQ_Function.SetUDateTime_MaskInput(this.uDTE, 0, 0);
            WQ_Function.SetUDateTime_MaskInput(this.uDTP, 0, 0);

            this.InitControlData();
        }

        private void frmWQPPCResultManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_PC_RESULT_MANAGE = false;
        }


        #region Button Events
        
        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.InitControlData();
                this.GetPCSection();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
 
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.uGridD.Rows.Count <= 0) return;

            if (this.IsValidation() != true)
            {
                MessageBox.Show("처리방법 또는 처리결과를 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.SavePCSectionProcData();
            //this.InitControlData();
            this.GetPCSection();

            MessageBox.Show("처리방법 또는 처리결과를 정상적으로 저장했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);
        }

        private void uGridD_Click(object sender, EventArgs e)
        {
            if (this.uGridD.Rows.Count <= 0) return;

            string strPC_NO = this.uGridD.ActiveRow.Cells[1].Text;

            this.InitControlData();
            this.GetPCSectionDetailData(strPC_NO);
        }

        private void uGridD_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridD.Rows.Count <= 0) return;

            string strPC_NO = this.uGridD.ActiveRow.Cells[1].Text;
            this.Viewmap_SelectedPipeCleanPoint("관세척구간", strPC_NO, "PC_NO");
        }

        #endregion


        #region User Function

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region Grid Set

            #region uGridD : 관세척구간 현황 Grid

            oUltraGridColumn = uGridD.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridD.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PCLEAN_NO";
            oUltraGridColumn.Header.Caption = "관세척 지점 일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridD.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "해석모델";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridD.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RPT_NUMBER";
            oUltraGridColumn.Header.Caption = "해석결과";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridD.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIPE_NUMBER";
            oUltraGridColumn.Header.Caption = "절점 ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridD.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SELECT_DATE";
            oUltraGridColumn.Header.Caption = "선정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridD);

            #endregion

            #endregion
        }

        /// <summary>
        /// Detail Data가 Set되는 Control의 Data를 모두 Clear
        /// </summary>
        private void InitControlData()
        {
            this.txtProcMethod.Text = "";
            this.txtProcResult.Text = "";
            WQ_Function.SetUDateTime_MaskInput(this.uDTP, 0, 0);
        }

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetPCSection()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSDate = string.Empty;
            string strEDate = string.Empty;

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strSDate = WQ_Function.StringToDateTime(this.uDTS.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.uDTE.DateTime);

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "         AND A.BLOCK_CODE IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = "         AND A.BLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   B.LOC_NAME || ' (' || A.BLOCK_CODE || ')' AS BLOCK, A.PCLEAN_NO, A.INP_NUMBER, A.RPT_NUMBER, A.PIPE_NUMBER, TO_CHAR(TO_DATE(A.SELECT_DATE, 'RRRR-MM-DD'), 'RRRR-MM-DD') AS SELECT_DATE");
            oStringBuilder.AppendLine("FROM     WQ_PCLEAN_SECTION A, CM_LOCATION B");
            oStringBuilder.AppendLine("WHERE    A.SELECT_DATE BETWEEN '" + strSDate + "' AND '" + strEDate + "'");
            oStringBuilder.AppendLine("         " + strBLOCK_SQL + " AND B.LOC_CODE = A.BLOCK_CODE");
            oStringBuilder.AppendLine("ORDER BY PCLEAN_NO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PC_POINT");

            this.uGridD.DataSource = pDS.Tables["PC_POINT"].DefaultView;

            FormManager.SetGridStyle_PerformAutoResize(this.uGridD);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 그리드에서 선택한 관세척지점에 대해 상세정보를 Select해서 Control에 Set
        /// </summary>
        /// <param name="strPC_NO"></param>
        private void GetPCSectionDetailData(string strPC_NO)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);

            oStringBuilder.AppendLine("SELECT   PROC_METHOD, PROC_RESULT, TO_CHAR(TO_DATE(DECODE(PROC_DATE, NULL, SYSDATE, PROC_DATE), 'RRRR-MM-DD'), 'RRRR-MM-DD') AS PROC_DATE");
            oStringBuilder.AppendLine("FROM     WQ_PCLEAN_SECTION");
            oStringBuilder.AppendLine("WHERE    PCLEAN_NO = '" + strPC_NO + "'");
            oStringBuilder.AppendLine("ORDER BY PCLEAN_NO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PC_POINT");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.txtProcMethod.Text = oDRow["PROC_METHOD"].ToString();
                    this.txtProcResult.Text = oDRow["PROC_RESULT"].ToString();
                    this.uDTP.Value = oDRow["PROC_DATE"].ToString();
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 처리결과 Data를 체크한다.
        /// </summary>
        /// <returns></returns>
        private bool IsValidation()
        {
            if (this.txtProcMethod.Text == "") return false;
            if (this.txtProcResult.Text == "") return false;

            return true;
        }

        /// <summary>
        /// 그리드에서 선택한 관세척지점의 처리결과를 Update한다.
        /// </summary>
        private void SavePCSectionProcData()
        {
            string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlock.Text);
            string strPC_NO = this.uGridD.ActiveRow.Cells[0].Text;
            string strPDate = WQ_Function.StringToDateTime(this.uDTP.DateTime);

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("UPDATE   WQ_PCLEAN_SECTION");
            oStringBuilder.AppendLine("SET      PROC_METHOD = '" + this.txtProcMethod.Text.Trim() + "', PROC_RESULT = '" + this.txtProcResult.Text.Trim() + "', PROC_DATE = '" + strPDate + "'");
            oStringBuilder.AppendLine("WHERE    BLOCK_CODE = '" + strSFTRIDN + "'");
            oStringBuilder.AppendLine("         AND PCLEAN_NO = '" + strPC_NO + "'");

            WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 관세척구간 그리드 클릭시 맵 이동
        /// </summary>
        /// <param name="strShape"></param>
        private void Viewmap_SelectedPipeCleanPoint(string strShape, string strKey, string strKeyName)
        {
            double dblMAP_SCALE = 2000;

            if (WQ_AppStatic.IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strShape);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strKeyName + "='" + strKey + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (WQ_AppStatic.IMAP != null)
                {
                    if (pFeature != null)
                    {
                        IGeometry oIGeometry = pFeature.Shape;

                        WQ_AppStatic.CURRENT_IGEOMETRY = oIGeometry;

                        ArcManager.FlashShape(WQ_AppStatic.IMAP.ActiveView, oIGeometry, 10);

                        if (ArcManager.GetMapScale(WQ_AppStatic.IMAP) > dblMAP_SCALE)
                        {
                            ArcManager.SetMapScale(WQ_AppStatic.IMAP, dblMAP_SCALE);
                        }

                        ArcManager.MoveCenterAt(WQ_AppStatic.IMAP, pFeature);

                        ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                        Application.DoEvents();
                    }
                    else
                    {
                        WQ_AppStatic.CURRENT_IGEOMETRY = null;
                    }
                    this.BringToFront();
                }
                else
                {
                    WQ_AppStatic.CURRENT_IGEOMETRY = null;
                }
            }
        }

        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_PC_RESULT_MANAGE = false;
            this.Dispose();
            this.Close();
        }

        #endregion

    }
}
