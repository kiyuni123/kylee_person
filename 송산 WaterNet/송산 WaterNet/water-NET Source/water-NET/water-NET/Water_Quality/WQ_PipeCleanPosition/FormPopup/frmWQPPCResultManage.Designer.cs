﻿namespace WaterNet.WQ_PipeCleanSection.FormPopup
{
    partial class frmWQPPCResultManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtProcResult = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.uDTP = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProcMethod = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.uGridD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.uDTE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDTS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDTS)).BeginInit();
            this.SuspendLayout();
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 450);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(636, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 450);
            this.PicFrRight.TabIndex = 107;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 454);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(640, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(640, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(632, 450);
            this.panel1.TabIndex = 131;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.pictureBox4);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.uGridD);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 57);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(632, 393);
            this.panel4.TabIndex = 106;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.txtProcResult);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 323);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(632, 70);
            this.panel6.TabIndex = 149;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(6, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 12);
            this.label6.TabIndex = 183;
            this.label6.Text = "관세척 결과";
            // 
            // txtProcResult
            // 
            this.txtProcResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtProcResult.Location = new System.Drawing.Point(0, 22);
            this.txtProcResult.MaxLength = 512;
            this.txtProcResult.Multiline = true;
            this.txtProcResult.Name = "txtProcResult";
            this.txtProcResult.Size = new System.Drawing.Size(632, 48);
            this.txtProcResult.TabIndex = 3;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Gold;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(0, 319);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(632, 4);
            this.pictureBox4.TabIndex = 148;
            this.pictureBox4.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.uDTP);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.txtProcMethod);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 249);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(632, 70);
            this.panel5.TabIndex = 146;
            // 
            // uDTP
            // 
            this.uDTP.Location = new System.Drawing.Point(528, 1);
            this.uDTP.Name = "uDTP";
            this.uDTP.Size = new System.Drawing.Size(100, 20);
            this.uDTP.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(456, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 12);
            this.label4.TabIndex = 183;
            this.label4.Text = "처리일자 :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(6, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 12);
            this.label1.TabIndex = 182;
            this.label1.Text = "관세척 방법";
            // 
            // txtProcMethod
            // 
            this.txtProcMethod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtProcMethod.Location = new System.Drawing.Point(0, 22);
            this.txtProcMethod.MaxLength = 512;
            this.txtProcMethod.Multiline = true;
            this.txtProcMethod.Name = "txtProcMethod";
            this.txtProcMethod.Size = new System.Drawing.Size(632, 48);
            this.txtProcMethod.TabIndex = 2;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(0, 245);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(632, 4);
            this.pictureBox3.TabIndex = 145;
            this.pictureBox3.TabStop = false;
            // 
            // uGridD
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridD.DisplayLayout.Appearance = appearance37;
            this.uGridD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridD.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.uGridD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridD.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.uGridD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridD.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridD.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.uGridD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.uGridD.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridD.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGridD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridD.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridD.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.uGridD.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.uGridD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.uGridD.DisplayLayout.Override.RowAppearance = appearance47;
            this.uGridD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridD.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.uGridD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridD.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridD.Location = new System.Drawing.Point(0, 0);
            this.uGridD.Name = "uGridD";
            this.uGridD.Size = new System.Drawing.Size(632, 245);
            this.uGridD.TabIndex = 142;
            this.uGridD.Text = "ultraGrid1";
            this.uGridD.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridD_DoubleClickRow);
            this.uGridD.Click += new System.EventHandler(this.uGridD_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 53);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(632, 4);
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.uDTE);
            this.panel2.Controls.Add(this.uDTS);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cboSBlock);
            this.panel2.Controls.Add(this.cboMBlock);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cboLBlock);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(632, 53);
            this.panel2.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Image = global::WaterNet.WQ_PipeCleanSection.Properties.Resources.Save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(447, 23);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 26);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "결과 저장";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uDTE
            // 
            this.uDTE.Location = new System.Drawing.Point(190, 24);
            this.uDTE.Name = "uDTE";
            this.uDTE.Size = new System.Drawing.Size(100, 20);
            this.uDTE.TabIndex = 161;
            // 
            // uDTS
            // 
            this.uDTS.Location = new System.Drawing.Point(75, 24);
            this.uDTS.Name = "uDTS";
            this.uDTS.Size = new System.Drawing.Size(100, 20);
            this.uDTS.TabIndex = 160;
            // 
            // btnClose
            // 
            this.btnClose.Image = global::WaterNet.WQ_PipeCleanSection.Properties.Resources.Close2;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(537, 23);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 26);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(176, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 12);
            this.label2.TabIndex = 159;
            this.label2.Text = "~";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(3, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 12);
            this.label3.TabIndex = 158;
            this.label3.Text = "선정일자 :";
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::WaterNet.WQ_PipeCleanSection.Properties.Resources.Query;
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(357, 23);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(90, 26);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "지점 조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(427, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 148;
            this.label9.Text = "소블록 :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(221, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 146;
            this.label8.Text = "중블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(487, 3);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 19);
            this.cboSBlock.TabIndex = 147;
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(281, 3);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock.TabIndex = 145;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(16, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 144;
            this.label7.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(75, 3);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock.TabIndex = 143;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // frmWQPPCResultManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 458);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmWQPPCResultManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "관세척 구간 처리 관리";
            this.Load += new System.EventHandler(this.frmWQPPCResultManaget_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQPPCResultManage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDTS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnClose;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDTE;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDTS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridD;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox txtProcMethod;
        private System.Windows.Forms.TextBox txtProcResult;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDTP;
        private System.Windows.Forms.Label label4;
    }
}