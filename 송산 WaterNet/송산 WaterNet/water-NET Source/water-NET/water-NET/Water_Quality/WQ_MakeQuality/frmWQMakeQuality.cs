﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WH_PipingNetworkAnalysis.epanet;

namespace WaterNet.WQ_MakeQuality
{
    public partial class frmWQMakeQuality : Form
    {
        private string filename = string.Empty;
        private string m_INP_NUMBER = string.Empty;
        public frmWQMakeQuality()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //해석모델 선택화면
            frmINP_Title oform = new frmINP_Title();
            oform.Open();
            if (oform.ShowDialog() == DialogResult.Cancel) return;

            //해석모델(INP_NUMBER)번호 멤버변수 저장
            this.m_INP_NUMBER = oform.m_INP_NUMBER;
            string nowtime = string.Empty;
            nowtime = WaterNet.WC_Common.WC_FunctionManager.GetNowDateTimeToString();
            nowtime += " " + WaterNet.WC_Common.WC_FunctionManager.GetNowTimeToString();
            localVariable.m_AccidentTime = nowtime;

            this.button3.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            #region 사고시간 입력 화면
            Form form = Application.OpenForms["frmAccidentTime"];
            if (form == null) form = new frmAccidentTime();
            form.TopMost = true;
            form.StartPosition = FormStartPosition.CenterParent;
            form.Activate();
            string strTime = ((frmAccidentTime)form).Open();
            if (form.DialogResult == DialogResult.Yes) localVariable.m_AccidentTime = strTime;
            #endregion 사고시간 입력 화면

            this.button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            saveFileDialog.ShowDialog();

            if (!"".Equals(saveFileDialog.FileName))
            {
                filename = saveFileDialog.FileName;
                //수리해석 실행
                if (!ExecuteAnalysis()) return;

                MessageBox.Show("모델생성완료");
            }           
            
        }

        private bool ExecuteAnalysis()
        {
            if (localVariable.m_AnalysisData != null) localVariable.m_AnalysisData.Clear();

            /////수리 모델 해석실행
            AnalysisEngine engine = new AnalysisEngine();
            
            WaterNet.WC_Common.WC_VariableManager.m_SplashForm.Open();
            WaterNet.WC_Common.WC_VariableManager.m_SplashForm.Message = "수질모델 생성 중입니다. 잠시만 기다리세요!";

            try
            {
                

                Hashtable conditions = new Hashtable();
                conditions.Add("SAVE_NAME", filename);
                conditions.Add("INP_NUMBER", this.m_INP_NUMBER);
                conditions.Add("AUTO_MANUAL", "M");
                conditions.Add("analysisType", 0);
                conditions.Add("analysisFlag", "Q");
                conditions.Add("resetValues", CreateResetValues());

                conditions.Add("WQ_MODEL_MAKE", "Y");
                conditions.Add("saveReport", false);
                conditions.Add("TARGET_DATE", WaterNet.WC_Common.WC_FunctionManager.GetNowDateTimeToString(DateTime.Now));

                Hashtable result = engine.Execute(conditions);
            }
            catch (Exception ex)
            {
                WaterNet.WaterNetCore.MessageManager.ShowErrorMessage("수리해석 수행중 오류가 발생했습니다.");
                return false;
            }
            finally
            {
                WaterNet.WC_Common.WC_VariableManager.m_SplashForm.Hide();
            }

            return true;
        }


        private Hashtable CreateResetValues()
        {
            Hashtable resetValue = new Hashtable();  //최상위 hashtable
            Hashtable AnalysisOption = new Hashtable();
            
            //INP Quality Section
            Hashtable qaulityValue = SelectReservoir();
            resetValue.Add("quality", qaulityValue);

            ///INP Patterns Section
            Hashtable pattern = SelectRealtimeFlowPattern();
            resetValue.Add("pattern", pattern);

            ///INP Junctions Section
            Hashtable JunctionData = SelectRealtimeFlowJunction();
            ICollection collKey = JunctionData.Keys;
            ArrayList keyList = new ArrayList();
            foreach (string nKey in collKey) keyList.Add(nKey);

            for (int i = 0; i < keyList.Count; i++)
            {
                string pattid = JunctionData[keyList[i]] as string;

                if (!pattern.ContainsKey(pattid))
                {
                    JunctionData[keyList[i]] = "Dumy";
                }
            }


            resetValue.Add("junctionPattern", JunctionData);

            AnalysisOption.Add("Quality", "Chemical");

            if (AnalysisOption.Count > 0)
            {
                resetValue.Add("option", AnalysisOption);
            }

            ///INP Times Section
            CreateSectionTimes(ref resetValue);
                        
            return resetValue;
        }

        private Hashtable SelectReservoir()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.Append("SELECT ID, 1 AS INITQUAL    ");
            queryString.Append("FROM   WH_RESERVOIRS        ");
            queryString.Append("WHERE                       ");
            queryString.Append("INP_NUMBER = '" + this.m_INP_NUMBER + "'");

            Hashtable quality = new Hashtable();

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            
            try
            {
                oDBManager.Open();
                DataTable otable = oDBManager.ExecuteScriptDataTable(queryString.ToString(), null);
                DataTable tableIDN = otable.DefaultView.ToTable(true, "ID");

                ///데이터테이블의 Select가 안됨으로 순차적 방법으로 변경함
                foreach (DataRow item in tableIDN.Rows)
                {
                    ArrayList oPatternList = new ArrayList();
                    foreach (DataRow row in otable.Rows)
                    {
                        if (Convert.ToString(row["ID"]) == Convert.ToString(item["ID"]))  //동일 ID일때
                        {
                            oPatternList.Add(row["INITQUAL"]);
                        }
                    }
                    quality.Add(item["ID"], oPatternList);
                }
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                    if (oDBManager != null) oDBManager.Close();
            }
            
            return quality;
        }

        private Hashtable SelectRealtimeFlowPattern()
        {
            Hashtable pattern = new Hashtable();     //서브 Pattern 파라메터

            ///정상적으로 수집되는 블록의 패턴생성
            SelectRealtimeFlowPattern1(ref pattern);

            ///위에 해당되지 않는 블록의 패턴 생성
            SelectRealtimeFlowPattern2(ref pattern);

            ///Default 로 Dumy 패턴 생성
            SelectRealtimeFlowPattern3(ref pattern);

            return pattern;
        }


        #region 수질사고 해석 Junction - 패턴ID
        /// <summary>
        /// 수질위기 모형의 Junction 데이터를 생성한다.
        /// INP에 등록된 절점이 포함된 블록의 관리번호를 패턴ID로 생성한다.
        /// Junction의 블록이 null or !소블록(BZ003) 이면 Dumy패턴으로 정의한다.
        /// </summary>
        /// <returns></returns>
        
        /// <summary>
        /// INP Times Section을 생성한다.
        /// resetValue : HashTable에 추가한다.
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreateSectionTimes(ref Hashtable resetValue)
        {
            Hashtable times = new Hashtable(); //INP Times Section

            //times.Add("Report Timestep", "1:00");
           
            localVariable.m_Duration = 48;
            times.Add("Duration", Convert.ToString(localVariable.m_Duration) + ":00");
            //times.Add("Pattern Timestep", "1:00");
            //times.Add("Hydraulic Timestep", "1:00");
            //times.Add("Quality Timestep", "1:00");
            //times.Add("Pattern Start", "00:00");
            //times.Add("Report Start", "00:00");
            //times.Add("Start ClockTime", "12:00 AM");
            //times.Add("Statistic", "None");
            resetValue.Add("time", times);
        }

        

        private Hashtable SelectRealtimeFlowJunction()
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select           b.id                                                                                                                                                    ");
            queryString.AppendLine("                ,b.elev                                                                                                                                                  ");
            queryString.AppendLine("                ,b.demand                                                                                                                                                ");
            queryString.AppendLine("                ,substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 2)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 2, 1))                 as BSM_IDN      ");
            queryString.AppendLine("                ,substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 1)+1,instr(a.POSITION_INFO, '|', 1, 2)-1 - instr(a.POSITION_INFO, '|', 1, 1))    as BSM_CDE       ");
            queryString.AppendLine("from            WH_TAGS         a                                                                                                                                        ");
            queryString.AppendLine("                ,WH_JUNCTIONS   b                                                                                                                                        ");
            queryString.AppendLine("where           a.TYPE          = 'NODE'                                                                                                                                 ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + this.m_INP_NUMBER + "'");
            queryString.AppendLine("and             b.INP_NUMBER    = '" + this.m_INP_NUMBER + "'");
            queryString.AppendLine("and             a.ID            = b.ID                                                                                                                                   ");
            queryString.AppendLine("and             substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 2)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 2, 1))             is not null          ");
            queryString.AppendLine("and             substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 1)+1,instr(a.POSITION_INFO, '|', 1, 2)-1 - instr(a.POSITION_INFO, '|', 1, 1)) is not null          ");

            Hashtable junctionData = new Hashtable();     //서브 Junction 파라메터

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                DataTable otable = oDBManager.ExecuteScriptDataTable(queryString.ToString(), null);

                ///Junction의 블록 != (BZ003 or null) 이면 Dumy패턴으로 정의
                foreach (DataRow row in otable.Rows)
                {
                    if (Convert.IsDBNull(row["BSM_CDE"])) junctionData.Add(Convert.ToString(row["id"]), "Dumy");
                    else
                    {
                        if (Convert.ToString(row["BSM_CDE"]).Equals("BZ003"))
                            junctionData.Add(Convert.ToString(row["id"]), Convert.ToString(row["BSM_IDN"]));
                        else
                            junctionData.Add(Convert.ToString(row["id"]), "Dumy");
                    }
                }
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }

            return junctionData;

        }
        #endregion 수질사고 해석 Junction - 패턴ID
        


        /// <summary>
        /// 수질사고 소블록 수용량 패턴 생성
        /// 소블록에 실시간 사용량이 들어오는 경우만
        /// IF_TAG가 있는경우 => 정상적
        /// </summary>
        /// <returns></returns>
        private void SelectRealtimeFlowPattern1(ref Hashtable pattern)
        {
            DateTime date = getStringToDateTime(localVariable.m_AccidentTime);

            StringBuilder queryString = new StringBuilder();
            queryString.AppendLine("WITH TEMP_1                                                                                           ");
            queryString.AppendLine("AS                                                                                                    ");
            queryString.AppendLine(" (                                                                                                    ");
            queryString.AppendLine("   SELECT   A.FTR_CODE AS BSM_CDE	                                                                    ");
            queryString.AppendLine("           ,A.FTR_IDN	AS BSM_IDN		                                                                  ");
            queryString.AppendLine("           ,B.LOC_CODE AS LOC_CODE			                                                              ");
            queryString.AppendLine("           ,TO_CHAR(D.TIMESTAMP,'YYYY-MM-DD HH24') AS DATE1                                           ");
            queryString.AppendLine("           ,ROUND(AVG(D.VALUE),4) AS VALUE                                                            ");
            queryString.AppendLine("     FROM                                                                                             ");
            queryString.AppendLine("           CM_LOCATION A                                                                              ");
            queryString.AppendLine("          ,IF_IHTAGS   B                                                                              ");
            queryString.AppendLine("          ,IF_TAG_GBN  C                                                                              ");
            queryString.AppendLine("          ,IF_GATHER_REALTIME D                                                                       ");
            queryString.AppendLine("          ,WH_TITLE    E                                                                              ");
            queryString.AppendLine("    WHERE A.FTR_CODE = 'BZ003'                                                                        ");
            //queryString.AppendLine("      AND A.PLOC_CODE = E.MFTRIDN                                                                     ");
            queryString.AppendLine("      AND E.INP_NUMBER = '" + this.m_INP_NUMBER + "'");
            queryString.AppendLine("      AND A.LOC_CODE = B.LOC_CODE                                                                     ");
            queryString.AppendLine("      AND B.TAGNAME = C.TAGNAME                                                                       ");
            queryString.AppendLine("      AND C.TAG_GBN = 'FRI'                                                                           ");
            queryString.AppendLine("      AND C.TAGNAME = D.TAGNAME                                                                       ");
            queryString.AppendLine("      AND D.TIMESTAMP     BETWEEN TO_DATE('" + WaterNet.WC_Common.WC_FunctionManager.GetNowDateTimeToFormatString(date.AddHours(-24)) + "', 'yyyy-mm-dd hh24:mi:ss') ");
            queryString.AppendLine("      AND                         TO_DATE('" + WaterNet.WC_Common.WC_FunctionManager.GetNowDateTimeToFormatString(date.AddHours(-1)) + "', 'yyyy-mm-dd hh24:mi:ss') ");
            queryString.AppendLine("   GROUP BY        A.FTR_CODE			                                                              ");
            queryString.AppendLine("                  ,A.FTR_IDN			                                                              ");
            queryString.AppendLine("                  ,B.LOC_CODE			                                                              ");
            queryString.AppendLine("                  ,TO_CHAR(D.TIMESTAMP,'YYYY-MM-DD HH24')                                             ");
            queryString.AppendLine("   ORDER BY BSM_CDE, BSM_IDN, TO_DATE(DATE1, 'YYYY-MM-DD HH24') ASC                                   ");
            queryString.AppendLine(" )                                                                                                    ");
            queryString.AppendLine("                                                                                                      ");
            queryString.AppendLine("SELECT                                                                                                ");
            queryString.AppendLine("       TEMP_1.BSM_CDE AS BSM_CDE,                                                                     ");
            queryString.AppendLine("       TEMP_1.BSM_IDN AS BSM_IDN,                                                                     ");
            queryString.AppendLine("       TEMP_1.LOC_CODE AS LOC_CODE,                                                                   ");
            queryString.AppendLine("       TEMP_1.DATE1 AS DATE1,                                                                         ");
            queryString.AppendLine("       CASE WHEN TEMP_1.VALUE = 0 THEN 0                                                              ");
            queryString.AppendLine("            WHEN TEMP_2.VALUE = 0 THEN 0                                                              ");
            queryString.AppendLine("            ELSE ROUND(TEMP_1.VALUE/TEMP_2.VALUE, 4)                                                  ");
            queryString.AppendLine("       END AS VALUE                                                                                   ");
            queryString.AppendLine("  FROM                                                                                                ");
            queryString.AppendLine("       TEMP_1,                                                                                        ");
            queryString.AppendLine("       ( SELECT BSM_CDE, BSM_IDN, LOC_CODE, AVG(VALUE) AS VALUE                                       ");
            queryString.AppendLine("           FROM TEMP_1                                                                                ");
            queryString.AppendLine("         GROUP BY BSM_CDE, BSM_IDN, LOC_CODE ) TEMP_2                                                 ");
            queryString.AppendLine(" WHERE TEMP_1.BSM_CDE = TEMP_2.BSM_CDE                                                                ");
            queryString.AppendLine("   AND TEMP_1.BSM_IDN = TEMP_2.BSM_IDN                                                                ");
            queryString.AppendLine("   AND TEMP_1.LOC_CODE = TEMP_2.LOC_CODE                                                              ");
            queryString.AppendLine("ORDER BY  1, 2, 3, 4                                                                                  ");


            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                DataTable otable = oDBManager.ExecuteScriptDataTable(queryString.ToString(), null);
                DataTable tableIDN = otable.DefaultView.ToTable(true, "BSM_IDN");

                ///데이터테이블의 Select가 안됨으로 순차적 방법으로 변경함
                foreach (DataRow item in tableIDN.Rows)
                {
                    ArrayList oPatternList = new ArrayList();
                    foreach (DataRow row in otable.Rows)
                    {
                        if (Convert.ToString(row["BSM_IDN"]) == Convert.ToString(item["BSM_IDN"]))  //동일 ID일때
                        {
                            oPatternList.Add(row["VALUE"]);
                        }
                    }
                    pattern.Add(item["BSM_IDN"], oPatternList);
                }
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 수질사고 소블록 수용량 패턴 생성
        /// 소블록에 실시간 사용량이 들어오지 않는경우만
        /// IF_TAG가 없는경우 => 소블록 사용량이 감시되지 않는경우
        /// ex)마곡직결 소블록
        /// </summary>
        /// <param name="pattern"></param>
        private void SelectRealtimeFlowPattern2(ref Hashtable pattern)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("SELECT A.FTR_IDN AS BSM_IDN                                   ");
            queryString.AppendLine("  FROM                                                        ");
            queryString.AppendLine("       (SELECT                                                ");
            queryString.AppendLine("                 A.FTR_IDN AS FTR_IDN                         ");
            queryString.AppendLine("                ,B.TAGNAME AS TAGNAME                         ");
            queryString.AppendLine("          FROM                                                ");
            queryString.AppendLine("                 CM_LOCATION A                                ");
            queryString.AppendLine("                ,IF_IHTAGS   B                                ");
            queryString.AppendLine("                ,IF_TAG_GBN  C                                ");
            queryString.AppendLine("                ,WH_TITLE    E                                ");
            queryString.AppendLine("         WHERE                                                ");
            queryString.AppendLine("                 A.FTR_CODE = 'BZ003'                         ");
            //queryString.AppendLine("           AND   A.PLOC_CODE = E.MFTRIDN                      ");
            queryString.AppendLine("           AND   E.INP_NUMBER = '" + this.m_INP_NUMBER + "'");
            //queryString.AppendLine("           AND   A.FTR_IDN  = B.FTR_IDN(+)                    ");
            queryString.AppendLine("           AND   A.LOC_CODE = B.LOC_CODE(+)                   ");
            queryString.AppendLine("           AND   B.TAGNAME = C.TAGNAME(+)                     ");
            queryString.AppendLine("           AND   (C.TAG_GBN = 'FRI' OR C.TAG_GBN IS NULL)     ");
            queryString.AppendLine("       ) A                                                    ");
            queryString.AppendLine(" WHERE A.TAGNAME IS NULL                                      ");

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                DataTable otable = oDBManager.ExecuteScriptDataTable(queryString.ToString(), null);
                foreach (DataRow row in otable.Rows)
                {
                    ArrayList oPatternList = new ArrayList();
                    for (int i = 0; i < localVariable.m_Duration; i++)
                    {
                        oPatternList.Add(1.0);
                    }

                    pattern.Add(row["BSM_IDN"], oPatternList);
                }
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 수질사고 소블록 수용량 패턴 생성
        /// Dumy 패턴
        /// Dumy 패턴의 사용량은 1.0으로 설정
        /// </summary>
        /// <param name="pattern"></param>
        private void SelectRealtimeFlowPattern3(ref Hashtable pattern)
        {
            ///Dumy 패턴생성하기
            ArrayList oPatternList = new ArrayList();
            for (int i = 0; i < localVariable.m_Duration; i++)
            {
                oPatternList.Add(1.0);
            }
            pattern.Add("Dumy", oPatternList);
            ///Dumy 패턴생성하기
        }

        /// <summary>
        /// 시간 문자열을 DataTime으로 반환한다.
        /// yyyy-mm-dd hh24:mi
        /// </summary>
        /// <param name="date">yyyy-mm-dd hh24:mi</param>
        /// <returns></returns>
        private DateTime getStringToDateTime(string date)
        {
            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(5, 2));
            int day = Convert.ToInt32(date.Substring(8, 2));
            int hour = Convert.ToInt32(date.Substring(11, 2));
            int min = Convert.ToInt32(date.Substring(14, 2));
            int sec = 0;

            return new DateTime(year, month, day, hour, min, sec);
        }

        

        

        

    }




}
