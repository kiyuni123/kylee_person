﻿namespace WaterNet.WQ_MakeQuality
{
    partial class frmAccidentTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.label_Time = new System.Windows.Forms.Label();
            this.maskedTime = new System.Windows.Forms.MaskedTextBox();
            this.maskedDate = new System.Windows.Forms.MaskedTextBox();
            this.panelCommand.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Controls.Add(this.btnSelect);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelCommand.Location = new System.Drawing.Point(0, 76);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(210, 37);
            this.panelCommand.TabIndex = 19;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(154, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(53, 25);
            this.btnClose.TabIndex = 39;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.Location = new System.Drawing.Point(99, 6);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(53, 25);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "선택";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // label_Time
            // 
            this.label_Time.Location = new System.Drawing.Point(3, 20);
            this.label_Time.Name = "label_Time";
            this.label_Time.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_Time.Size = new System.Drawing.Size(38, 47);
            this.label_Time.TabIndex = 40;
            this.label_Time.Text = "사고시간";
            this.label_Time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // maskedTime
            // 
            this.maskedTime.Location = new System.Drawing.Point(54, 43);
            this.maskedTime.Mask = "90:90";
            this.maskedTime.Name = "maskedTime";
            this.maskedTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTime.Size = new System.Drawing.Size(146, 21);
            this.maskedTime.TabIndex = 42;
            this.maskedTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maskedTime.ValidatingType = typeof(System.DateTime);
            // 
            // maskedDate
            // 
            this.maskedDate.Location = new System.Drawing.Point(54, 12);
            this.maskedDate.Mask = "0000-90-90";
            this.maskedDate.Name = "maskedDate";
            this.maskedDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedDate.Size = new System.Drawing.Size(146, 21);
            this.maskedDate.TabIndex = 43;
            this.maskedDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maskedDate.ValidatingType = typeof(System.DateTime);
            // 
            // frmAccidentTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(210, 113);
            this.ControlBox = false;
            this.Controls.Add(this.maskedTime);
            this.Controls.Add(this.maskedDate);
            this.Controls.Add(this.label_Time);
            this.Controls.Add(this.panelCommand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAccidentTime";
            this.Text = "사용시간 선택";
            this.panelCommand.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Label label_Time;
        private System.Windows.Forms.MaskedTextBox maskedTime;
        private System.Windows.Forms.MaskedTextBox maskedDate;
    }
}