﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WQ_MakeQuality
{
    public partial class frmAccidentTime : Form
    {
        public frmAccidentTime()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            maskedDate.Text = WC_Common.WC_FunctionManager.GetNowDateTimeToString();
            maskedDate.ValidatingType = typeof(System.DateTime);
            maskedTime.Text = WC_Common.WC_FunctionManager.GetNowTimeToString();
            maskedTime.ValidatingType = typeof(System.DateTime);
        }
        #endregion 초기화설정

        /// <summary>
        /// 실행
        /// Modal 폼으로 오픈
        /// </summary>
        public string Open()
        {
            this.ShowDialog();

            string nowtime = string.Empty;
            // Get current time
            //// Format current time into string
            //nowtime = year.ToString();
            //nowtime += "-" + month.ToString().PadLeft(2, '0');
            //nowtime += "-" + day.ToString().PadLeft(2, '0');

            //nowtime += " " + comboBox_Hour.SelectedItem.ToString();


            //nowtime += hour.ToString().PadLeft(2, '0');
            //nowtime += min.ToString().PadLeft(2, '0');
            //nowtime += sec.ToString().PadLeft(2, '0');


            nowtime = maskedDate.Text;
            nowtime += " " + maskedTime.Text;

            return nowtime;
            

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (!ValidateDateMaskedText(maskedDate))
            {
                WaterNetCore.MessageManager.ShowInformationMessage("잘못된 입력입니다.");
                maskedDate.Focus();
                return;
            }

            if (!ValidateTimeMaskedText(maskedTime))
            {
                WaterNetCore.MessageManager.ShowInformationMessage("잘못된 입력입니다.");
                maskedTime.Focus();
                return;
            }

            DialogResult = DialogResult.Yes;
            this.Close();
        }

        private bool ValidateDateMaskedText(MaskedTextBox textBox)
        {
            if (!textBox.MaskCompleted) return false;
            if (!textBox.MaskFull) return false;
            if (!WC_Common.WC_FunctionManager.ValidateCheckDate(textBox.Text)) return false;
            
            return true;
        }

        private bool ValidateTimeMaskedText(MaskedTextBox textBox)
        {
            if (!textBox.MaskCompleted) return false;
            if (!textBox.MaskFull) return false;
            if (!WC_Common.WC_FunctionManager.ValidateCheckTime(textBox.Text)) return false;

            return true;
        }
    }
}
