﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WQ_GumsaData
{
    /// <summary>
    /// Project ID : WN_WQ_A01
    /// Project Explain : 수질검사자료관리
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.15
    /// Form Explain : 수질검사자료관리 Main Form
    /// </summary>
    public partial class frmWQMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        public frmWQMain()
        {
            WQ_AppStatic.IS_SHOW_FORM_CODE = false;
            WQ_AppStatic.IS_SHOW_FORM_FAUCET = false;
            WQ_AppStatic.IS_SHOW_FORM_TEST_RESULT = false;

            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버 (메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.)

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();
        }


        #region Button Events

        private void btnCode_Click(object sender, EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_CODE == false)
            {
                WaterNet.WQ_GumsaData.FormPopup.frmWQCode oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQCode();
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        private void btnWQ_Click(object sender, EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_TEST_RESULT == false)
            {
                WaterNet.WQ_GumsaData.FormPopup.frmWQTestResult oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQTestResult();
                oForm.IMAP = (IMapControl3)axMap.Object;
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        private void btnFaucet_Click(object sender, EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_FAUCET == false)
            {
                WaterNet.WQ_GumsaData.FormPopup.frmWQFaucet oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQFaucet();
                oForm.IMAP = (IMapControl3)axMap.Object;
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        #endregion
    }
}
