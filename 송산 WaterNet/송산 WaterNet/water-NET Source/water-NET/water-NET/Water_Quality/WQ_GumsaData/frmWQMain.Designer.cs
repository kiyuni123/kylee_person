﻿namespace WaterNet.WQ_GumsaData
{
    partial class frmWQMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWQMain));
            this.btnCode = new System.Windows.Forms.Button();
            this.btnWQ = new System.Windows.Forms.Button();
            this.btnFaucet = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.panelCommand.SuspendLayout();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnFaucet);
            this.panelCommand.Controls.Add(this.btnWQ);
            this.panelCommand.Controls.Add(this.btnCode);
            // 
            // spcContents
            // 
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.TabIndex = 11;
            // 
            // spcTOC
            // 
            this.spcTOC.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            // 
            // spcIndexMap
            // 
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(661, 524);
            // 
            // btnCode
            // 
            this.btnCode.Location = new System.Drawing.Point(3, 2);
            this.btnCode.Name = "btnCode";
            this.btnCode.Size = new System.Drawing.Size(62, 54);
            this.btnCode.TabIndex = 12;
            this.btnCode.Text = "코드관리";
            this.btnCode.UseVisualStyleBackColor = true;
            this.btnCode.Click += new System.EventHandler(this.btnCode_Click);
            // 
            // btnWQ
            // 
            this.btnWQ.Location = new System.Drawing.Point(3, 57);
            this.btnWQ.Name = "btnWQ";
            this.btnWQ.Size = new System.Drawing.Size(62, 54);
            this.btnWQ.TabIndex = 13;
            this.btnWQ.Text = "사업장 수질시험관리";
            this.btnWQ.UseVisualStyleBackColor = true;
            this.btnWQ.Click += new System.EventHandler(this.btnWQ_Click);
            // 
            // btnFaucet
            // 
            this.btnFaucet.Location = new System.Drawing.Point(3, 112);
            this.btnFaucet.Name = "btnFaucet";
            this.btnFaucet.Size = new System.Drawing.Size(62, 54);
            this.btnFaucet.TabIndex = 14;
            this.btnFaucet.Text = "수도꼭지시험관리";
            this.btnFaucet.UseVisualStyleBackColor = true;
            this.btnFaucet.Click += new System.EventHandler(this.btnFaucet_Click);
            // 
            // frmWQMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.ClientSize = new System.Drawing.Size(948, 583);
            this.Name = "frmWQMain";
            this.Text = "수질 검사 자료 관리";
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.panelCommand.ResumeLayout(false);
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFaucet;
        private System.Windows.Forms.Button btnWQ;
        private System.Windows.Forms.Button btnCode;


    }
}
