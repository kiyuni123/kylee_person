﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WQ_GumsaData
{
    class WQ_AppStatic
    {
        #region 팝업 폼 오픈여부 선언부

        /// <summary>
        /// frmWQCode.cs 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_CODE = false;
        /// <summary>
        /// frmWQFaucet.cs 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_FAUCET = false;
        /// <summary>
        /// frmWQTestResult.cs 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_TEST_RESULT = false;

        #endregion
    }
}
