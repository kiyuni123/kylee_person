﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

#endregion

namespace WaterNet.WQ_GumsaData.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A01
    /// Project Explain : 수질검사자료관리
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.17
    /// Form Explain : 수도꼭지시험신규 Popup Form
    /// </summary>
    public partial class frmWQPFaucetTestItem : Form
    {
        /// <summary>
        /// Project ID : WN_WQ_A01
        /// Project Explain : 수질검사자료관리
        /// Project Developer : 오두석
        /// Project Create Date : 2010.09.16
        /// Form Explain : 수질시험 신규 Popup Form
        /// </summary>
        private OracleDBManager m_oDBManager = null;

        string _Consumer = string.Empty;
        string _Title = string.Empty;
        string _Establishment = string.Empty;
        string _WaterCollectDate = string.Empty;        

        /// <summary>
        /// 수용가
        /// </summary>
        public string Consumer
        {
            get
            {
                return _Consumer;
            }
            set
            {
                _Consumer = value;
            }
        }

        /// <summary>
        /// 수질관리 Sub Tab들의 Title
        /// </summary>
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                _Title = value;
            }
        }

        /// <summary>
        /// 사업장
        /// </summary>
        public string Establishment
        {
            get
            {
                return _Establishment;
            }
            set
            {
                _Establishment = value;
            }
        }

        /// <summary>
        /// 채수일자
        /// </summary>
        public string WaterCollectDate
        {
            get
            {
                return _WaterCollectDate;
            }
            set
            {
                _WaterCollectDate = value;
            }
        }

        public frmWQPFaucetTestItem()
        {
            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region - 그리드 설정

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITCD";
            oUltraGridColumn.Header.Caption = "항목코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITMNM";
            oUltraGridColumn.Header.Caption = "항목명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVAL";
            oUltraGridColumn.Header.Caption = "측정치";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HMNTSVAL";
            oUltraGridColumn.Header.Caption = "상한기준값";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LMNTSVAL";
            oUltraGridColumn.Header.Caption = "하한기준값";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LTRSVAL";
            oUltraGridColumn.Header.Caption = "문자기준값";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VLDFGR";
            oUltraGridColumn.Header.Caption = "유효자리";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPopup);

            //ActiionKeyMapping();
            #endregion          
        }

        private void frmWQPFaucetTestItem_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================수질관리(신규)

            object o = EMFrame.statics.AppStatic.USER_MENU["수질검사자료관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }

            //===========================================================================

            // frmWQMain 에서 받은 값을을 Set
            this.Text = _Title;
            this.cbEstablishment.Text = _Establishment;
            this.txtConsumer.Text = _Consumer;
            this.udtWaterCollectDate.Text = _WaterCollectDate;

            this.cbEstablishment.Enabled = false;
           
            if (this.Text.Substring(this.Text.Length - 2) == "신규")
            {
                FormManager.SetGridStyle_ColumeAllowEdit(this.uGridPopup, 2);

                this.txtConsumer.Enabled = true;
                this.btnSearch.Enabled = true;
                this.btnSave.Enabled = true;
                this.udtWaterCollectDate.Enabled = true;                
                this.GetFaucetNewTestItemData();
                this.uGridPopup.DisplayLayout.Bands[0].Columns[2].CellAppearance.BackColor = Color.Lime;
            }
            else
            {
                this.txtConsumer.Enabled = false;
                this.btnSearch.Enabled = false;
                this.btnSave.Enabled = false;
                this.udtWaterCollectDate.Enabled = false;
                this.GetFaucetDetailTestItemData();
            }
            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);
        }

        #region Button Events

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() == true)
            {
                this.SaveFaucetNewTestItemData();
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.OpenToSearchPopup();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string strTitle = string.Empty;

                UltraGrid oUGrid = new UltraGrid();

                if (oUGrid.Rows.Count > 0)
                {
                    strTitle = _Title;
                    oUGrid = this.uGridPopup;

                    FormManager.ExportToExcelFromUltraGrid(oUGrid, strTitle);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion

        #region User Function

        /// <summary>
        /// 수도꼭지시험항목을 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetFaucetNewTestItemData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITE = string.Empty;
            string strSITECD = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);

            strParam = "WHERE    A.TITCD = B.TITCD AND B.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND B.SITECD = '" + strSITECD + "' AND B.CWPRCCD = '09'";

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.TITCD, A.TITMNM, '' AS SVAL, A.HMNTSVAL, A.LMNTSVAL, A.LTRSVAL, VLDFGR");
            oStringBuilder.AppendLine("FROM     WQCTIT A, WQCPPRTIT B");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY B.LINESEQ, A.TITCD");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            this.uGridPopup.DataSource = pDS.Tables["WQCTIT"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            //첫번째 Row에 Activate 부여
            if (this.uGridPopup.Rows.Count > 0) this.uGridPopup.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 수도꼭지시험항목을 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetFaucetDetailTestItemData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITE = string.Empty;
            string strSITECD = string.Empty;
            string strDMNO = string.Empty;
            string strCLTDT = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strDMNO = this.txtConsumer.Text.ToString();
            strCLTDT = WQ_Function.StringToDateTime(this.udtWaterCollectDate.DateTime);

            strParam = "WHERE    A.TITCD = B.TITCD AND B.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND B.SITECD = '" + strSITECD + "' AND B.CWPRCCD = '09'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.TITCD, A.TITMNM, (SELECT SVAL FROM WQCTPWQIPR WHERE TITCD = A.TITCD AND SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND DMNO = '" + strDMNO + "' AND CLTDT = '" + strCLTDT + "' AND SPOT = '1') AS SVAL, A.HMNTSVAL, A.LMNTSVAL, A.LTRSVAL, VLDFGR");
            oStringBuilder.AppendLine("FROM     WQCTIT A, WQCPPRTIT B");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY B.LINESEQ, A.TITCD");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            this.uGridPopup.DataSource = pDS.Tables["WQCTIT"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            //첫번째 Row에 Activate 부여
            if (this.uGridPopup.Rows.Count > 0) this.uGridPopup.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 신규 수도꼭지 Test Data를 Database에 Insert 한다.
        /// </summary>
        private void SaveFaucetNewTestItemData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strSITE = string.Empty;
            string strSITECD = string.Empty;
            string strDMNO = string.Empty;
            string strCLTDT = string.Empty;
            string strParam = string.Empty;
            string strTITCD = string.Empty;
            string strSVAL = string.Empty;
            string strSERNO = string.Empty;

            int i = 0;

            strSITE = this.cbEstablishment.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strCLTDT = WQ_Function.StringToDateTime(this.udtWaterCollectDate.DateTime);
            strDMNO = this.txtConsumer.Text.ToString();
            strSERNO = WQ_Function.GenWQGumsaSequenceNo(strCLTDT, 0);

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND DMNO = '" + strDMNO + "' AND CLTDT = '" + strCLTDT + "' AND SERNO = '" + strSERNO + "' AND SPOT = '1'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE");
            oStringBuilder.AppendLine("FROM     WQCTPWQIPS");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strDMNO + "', '" + strCLTDT + "', '" + strSERNO + "', '1', '', '', '', '', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE, '" + strSITECD + "', '', '')";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO WQCTPWQIPS");
            oStringBuilder.AppendLine("(SGCCD, DMNO, CLTDT, SERNO, SPOT, WQSEL, WQJD, SECONDWQ, RUST, REGMNGR, REGMNGRIP, REGDT, SITECD, RMK, WPLINK)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            for (i = 0; i < this.uGridPopup.Rows.Count; i++)
            {                
                strTITCD = this.uGridPopup.Rows[i].Cells[0].Text.ToString();
                strSVAL = this.uGridPopup.Rows[i].Cells[2].Text.ToString();

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND DMNO = '" + strDMNO + "' AND CLTDT = '" + strCLTDT + "' AND SERNO = '" + strSERNO + "' AND TITCD = '" + strTITCD + "' AND SPOT = '1'";
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCTPWQIPR");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strDMNO + "', '" + strCLTDT + "', '" + strSERNO + "', '" + strTITCD + "', '1', '" + strSVAL + "', '" + strSITECD + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE, '')";
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT INTO WQCTPWQIPR");
                oStringBuilder.AppendLine("(SGCCD, DMNO, CLTDT, SERNO, TITCD, SPOT, SVAL, SITECD, REGMNGR, REGMNGRIP, REGDT, FIRSTCHAGUMSANO)");
                oStringBuilder.AppendLine("VALUES");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        /// <summary>
        /// 특정 값을 부모 Form으로 Retun할 수 있게 하고, 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            _Establishment = this.cbEstablishment.Text.ToString();
            _WaterCollectDate = this.udtWaterCollectDate.Text.ToString();
            _Consumer = this.txtConsumer.Text.ToString();
            this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        /// <summary>
        /// Open to Search Popup (수도꼭지관리)
        /// </summary>
        private void OpenToSearchPopup()
        {
            WaterNet.WQ_GumsaData.FormPopup.frmWQPConsumer oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQPConsumer();
            oForm.ShowDialog();
            this.txtConsumer.Text = oForm.Consumer;
        }

        /// <summary>
        /// Insert할 Data를 검증한다
        /// </summary>
        /// <returns>True : Ok, False : Fail</returns>
        private bool IsValidation()
        {
            if (this.txtConsumer.Text == "")
            {
                return false;
            }

            return true;
        }

        #endregion

    }
}
