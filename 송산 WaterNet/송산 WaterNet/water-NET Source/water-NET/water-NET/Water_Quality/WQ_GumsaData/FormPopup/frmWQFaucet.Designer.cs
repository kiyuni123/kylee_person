﻿namespace WaterNet.WQ_GumsaData.FormPopup
{
    partial class frmWQFaucet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.uTabPageM3S1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel29 = new System.Windows.Forms.Panel();
            this.uGrid31_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.btnAppend4 = new System.Windows.Forms.Button();
            this.btnExcel9 = new System.Windows.Forms.Button();
            this.btnSave5 = new System.Windows.Forms.Button();
            this.btnDelete8 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.panel31 = new System.Windows.Forms.Panel();
            this.txtConsumer1 = new System.Windows.Forms.TextBox();
            this.btnSearch1 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.cbEstablishment7 = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cbDivisionName = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.btnQuery9 = new System.Windows.Forms.Button();
            this.btnClose9 = new System.Windows.Forms.Button();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.uTabPageM3S2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel32 = new System.Windows.Forms.Panel();
            this.uGrid32_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.panel33 = new System.Windows.Forms.Panel();
            this.btnDetailData6 = new System.Windows.Forms.Button();
            this.btnExcel10 = new System.Windows.Forms.Button();
            this.btnNewTest6 = new System.Windows.Forms.Button();
            this.btnDelete9 = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.udtWaterCollectDateE6 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.udtWaterCollectDateS6 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtConsumer2 = new System.Windows.Forms.TextBox();
            this.btnSearch2 = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.cbEstablishment8 = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.btnQuery10 = new System.Windows.Forms.Button();
            this.btnClose10 = new System.Windows.Forms.Button();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.picFrRightM3 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM3 = new System.Windows.Forms.PictureBox();
            this.picFrBottomM3 = new System.Windows.Forms.PictureBox();
            this.picFrTopM3 = new System.Windows.Forms.PictureBox();
            this.uTabMenuS3 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage4 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTabPageM3S1.SuspendLayout();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid31_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            this.uTabPageM3S2.SuspendLayout();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid32_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            this.panel34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightM3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomM3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopM3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenuS3)).BeginInit();
            this.uTabMenuS3.SuspendLayout();
            this.SuspendLayout();
            // 
            // uTabPageM3S1
            // 
            this.uTabPageM3S1.Controls.Add(this.panel29);
            this.uTabPageM3S1.Controls.Add(this.pictureBox50);
            this.uTabPageM3S1.Controls.Add(this.panel31);
            this.uTabPageM3S1.Controls.Add(this.pictureBox22);
            this.uTabPageM3S1.Controls.Add(this.pictureBox23);
            this.uTabPageM3S1.Controls.Add(this.pictureBox24);
            this.uTabPageM3S1.Controls.Add(this.pictureBox25);
            this.uTabPageM3S1.Location = new System.Drawing.Point(1, 23);
            this.uTabPageM3S1.Name = "uTabPageM3S1";
            this.uTabPageM3S1.Size = new System.Drawing.Size(790, 375);
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.SystemColors.Control;
            this.panel29.Controls.Add(this.uGrid31_1);
            this.panel29.Controls.Add(this.pictureBox51);
            this.panel29.Controls.Add(this.panel30);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(4, 36);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(782, 335);
            this.panel29.TabIndex = 129;
            // 
            // uGrid31_1
            // 
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid31_1.DisplayLayout.Appearance = appearance85;
            this.uGrid31_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid31_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance86.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance86.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance86.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid31_1.DisplayLayout.GroupByBox.Appearance = appearance86;
            appearance87.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid31_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance87;
            this.uGrid31_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance88.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance88.BackColor2 = System.Drawing.SystemColors.Control;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance88.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid31_1.DisplayLayout.GroupByBox.PromptAppearance = appearance88;
            this.uGrid31_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid31_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid31_1.DisplayLayout.Override.ActiveCellAppearance = appearance89;
            appearance90.BackColor = System.Drawing.SystemColors.Highlight;
            appearance90.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid31_1.DisplayLayout.Override.ActiveRowAppearance = appearance90;
            this.uGrid31_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid31_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid31_1.DisplayLayout.Override.CardAreaAppearance = appearance91;
            appearance92.BorderColor = System.Drawing.Color.Silver;
            appearance92.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid31_1.DisplayLayout.Override.CellAppearance = appearance92;
            this.uGrid31_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid31_1.DisplayLayout.Override.CellPadding = 0;
            appearance93.BackColor = System.Drawing.SystemColors.Control;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid31_1.DisplayLayout.Override.GroupByRowAppearance = appearance93;
            appearance94.TextHAlignAsString = "Left";
            this.uGrid31_1.DisplayLayout.Override.HeaderAppearance = appearance94;
            this.uGrid31_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid31_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.Color.Silver;
            this.uGrid31_1.DisplayLayout.Override.RowAppearance = appearance95;
            this.uGrid31_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid31_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance96;
            this.uGrid31_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid31_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid31_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid31_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid31_1.Location = new System.Drawing.Point(0, 32);
            this.uGrid31_1.Name = "uGrid31_1";
            this.uGrid31_1.Size = new System.Drawing.Size(782, 303);
            this.uGrid31_1.TabIndex = 4;
            this.uGrid31_1.Text = "ultraGrid1";
            this.uGrid31_1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGrid31_1.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid31_1_ClickCellButton);
            // 
            // pictureBox51
            // 
            this.pictureBox51.BackColor = System.Drawing.Color.Gold;
            this.pictureBox51.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox51.Location = new System.Drawing.Point(0, 28);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(782, 4);
            this.pictureBox51.TabIndex = 126;
            this.pictureBox51.TabStop = false;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.SystemColors.Control;
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.btnAppend4);
            this.panel30.Controls.Add(this.btnExcel9);
            this.panel30.Controls.Add(this.btnSave5);
            this.panel30.Controls.Add(this.btnDelete8);
            this.panel30.Controls.Add(this.label37);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(782, 28);
            this.panel30.TabIndex = 1;
            // 
            // btnAppend4
            // 
            this.btnAppend4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnAppend4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.AddRow;
            this.btnAppend4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAppend4.Location = new System.Drawing.Point(500, 0);
            this.btnAppend4.Name = "btnAppend4";
            this.btnAppend4.Size = new System.Drawing.Size(70, 26);
            this.btnAppend4.TabIndex = 7;
            this.btnAppend4.Text = "추가";
            this.btnAppend4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAppend4.UseVisualStyleBackColor = true;
            this.btnAppend4.Click += new System.EventHandler(this.btnAppend4_Click);
            // 
            // btnExcel9
            // 
            this.btnExcel9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcel9.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel9.Location = new System.Drawing.Point(570, 0);
            this.btnExcel9.Name = "btnExcel9";
            this.btnExcel9.Size = new System.Drawing.Size(70, 26);
            this.btnExcel9.TabIndex = 14;
            this.btnExcel9.Text = "엑셀";
            this.btnExcel9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel9.UseVisualStyleBackColor = true;
            this.btnExcel9.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnSave5
            // 
            this.btnSave5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSave5.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Save;
            this.btnSave5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave5.Location = new System.Drawing.Point(640, 0);
            this.btnSave5.Name = "btnSave5";
            this.btnSave5.Size = new System.Drawing.Size(70, 26);
            this.btnSave5.TabIndex = 13;
            this.btnSave5.Text = "저장";
            this.btnSave5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave5.UseVisualStyleBackColor = true;
            this.btnSave5.Click += new System.EventHandler(this.btnSave5_Click);
            // 
            // btnDelete8
            // 
            this.btnDelete8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete8.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete8.Location = new System.Drawing.Point(710, 0);
            this.btnDelete8.Name = "btnDelete8";
            this.btnDelete8.Size = new System.Drawing.Size(70, 26);
            this.btnDelete8.TabIndex = 12;
            this.btnDelete8.Text = "삭제";
            this.btnDelete8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete8.UseVisualStyleBackColor = true;
            this.btnDelete8.Click += new System.EventHandler(this.btnDelete8_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.Location = new System.Drawing.Point(6, 7);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(109, 12);
            this.label37.TabIndex = 5;
            this.label37.Text = "수도꼭지지점관리";
            // 
            // pictureBox50
            // 
            this.pictureBox50.BackColor = System.Drawing.Color.Gold;
            this.pictureBox50.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox50.Location = new System.Drawing.Point(4, 32);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(782, 4);
            this.pictureBox50.TabIndex = 130;
            this.pictureBox50.TabStop = false;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.SystemColors.Control;
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Controls.Add(this.txtConsumer1);
            this.panel31.Controls.Add(this.btnSearch1);
            this.panel31.Controls.Add(this.label38);
            this.panel31.Controls.Add(this.cbEstablishment7);
            this.panel31.Controls.Add(this.label39);
            this.panel31.Controls.Add(this.cbDivisionName);
            this.panel31.Controls.Add(this.label40);
            this.panel31.Controls.Add(this.btnQuery9);
            this.panel31.Controls.Add(this.btnClose9);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(4, 4);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(782, 28);
            this.panel31.TabIndex = 128;
            // 
            // txtConsumer1
            // 
            this.txtConsumer1.Location = new System.Drawing.Point(496, 3);
            this.txtConsumer1.Name = "txtConsumer1";
            this.txtConsumer1.Size = new System.Drawing.Size(118, 21);
            this.txtConsumer1.TabIndex = 30;
            // 
            // btnSearch1
            // 
            this.btnSearch1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Find;
            this.btnSearch1.Location = new System.Drawing.Point(614, 1);
            this.btnSearch1.Name = "btnSearch1";
            this.btnSearch1.Size = new System.Drawing.Size(26, 24);
            this.btnSearch1.TabIndex = 29;
            this.btnSearch1.UseVisualStyleBackColor = true;
            this.btnSearch1.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.Location = new System.Drawing.Point(436, 7);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(54, 12);
            this.label38.TabIndex = 27;
            this.label38.Text = "수용가 :";
            // 
            // cbEstablishment7
            // 
            this.cbEstablishment7.FormattingEnabled = true;
            this.cbEstablishment7.Location = new System.Drawing.Point(282, 3);
            this.cbEstablishment7.Name = "cbEstablishment7";
            this.cbEstablishment7.Size = new System.Drawing.Size(150, 20);
            this.cbEstablishment7.TabIndex = 23;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.Location = new System.Drawing.Point(6, 7);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 12);
            this.label39.TabIndex = 22;
            this.label39.Text = "부서명 :";
            // 
            // cbDivisionName
            // 
            this.cbDivisionName.FormattingEnabled = true;
            this.cbDivisionName.Location = new System.Drawing.Point(66, 3);
            this.cbDivisionName.Name = "cbDivisionName";
            this.cbDivisionName.Size = new System.Drawing.Size(150, 20);
            this.cbDivisionName.TabIndex = 21;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.Location = new System.Drawing.Point(222, 7);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 12);
            this.label40.TabIndex = 18;
            this.label40.Text = "사업장 :";
            // 
            // btnQuery9
            // 
            this.btnQuery9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery9.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery9.Location = new System.Drawing.Point(640, 0);
            this.btnQuery9.Name = "btnQuery9";
            this.btnQuery9.Size = new System.Drawing.Size(70, 26);
            this.btnQuery9.TabIndex = 12;
            this.btnQuery9.Text = "조회";
            this.btnQuery9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery9.UseVisualStyleBackColor = true;
            this.btnQuery9.Click += new System.EventHandler(this.btnQuery9_Click);
            // 
            // btnClose9
            // 
            this.btnClose9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose9.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose9.Location = new System.Drawing.Point(710, 0);
            this.btnClose9.Name = "btnClose9";
            this.btnClose9.Size = new System.Drawing.Size(70, 26);
            this.btnClose9.TabIndex = 3;
            this.btnClose9.Text = "닫기";
            this.btnClose9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose9.UseVisualStyleBackColor = true;
            this.btnClose9.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox22.Location = new System.Drawing.Point(786, 4);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(4, 367);
            this.pictureBox22.TabIndex = 127;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox23.Location = new System.Drawing.Point(0, 4);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(4, 367);
            this.pictureBox23.TabIndex = 126;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox24.Location = new System.Drawing.Point(0, 371);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(790, 4);
            this.pictureBox24.TabIndex = 125;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox25.Location = new System.Drawing.Point(0, 0);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(790, 4);
            this.pictureBox25.TabIndex = 124;
            this.pictureBox25.TabStop = false;
            // 
            // uTabPageM3S2
            // 
            this.uTabPageM3S2.Controls.Add(this.panel32);
            this.uTabPageM3S2.Controls.Add(this.pictureBox52);
            this.uTabPageM3S2.Controls.Add(this.panel34);
            this.uTabPageM3S2.Controls.Add(this.pictureBox26);
            this.uTabPageM3S2.Controls.Add(this.pictureBox27);
            this.uTabPageM3S2.Controls.Add(this.pictureBox28);
            this.uTabPageM3S2.Controls.Add(this.pictureBox29);
            this.uTabPageM3S2.Location = new System.Drawing.Point(-10000, -10000);
            this.uTabPageM3S2.Name = "uTabPageM3S2";
            this.uTabPageM3S2.Size = new System.Drawing.Size(790, 375);
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.SystemColors.Control;
            this.panel32.Controls.Add(this.uGrid32_1);
            this.panel32.Controls.Add(this.pictureBox53);
            this.panel32.Controls.Add(this.panel33);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(4, 60);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(782, 311);
            this.panel32.TabIndex = 135;
            // 
            // uGrid32_1
            // 
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid32_1.DisplayLayout.Appearance = appearance73;
            this.uGrid32_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid32_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance74.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid32_1.DisplayLayout.GroupByBox.Appearance = appearance74;
            appearance75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid32_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance75;
            this.uGrid32_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance76.BackColor2 = System.Drawing.SystemColors.Control;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid32_1.DisplayLayout.GroupByBox.PromptAppearance = appearance76;
            this.uGrid32_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid32_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid32_1.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            appearance78.BackColor = System.Drawing.SystemColors.Highlight;
            appearance78.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid32_1.DisplayLayout.Override.ActiveRowAppearance = appearance78;
            this.uGrid32_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid32_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid32_1.DisplayLayout.Override.CardAreaAppearance = appearance79;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            appearance80.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid32_1.DisplayLayout.Override.CellAppearance = appearance80;
            this.uGrid32_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid32_1.DisplayLayout.Override.CellPadding = 0;
            appearance81.BackColor = System.Drawing.SystemColors.Control;
            appearance81.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance81.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance81.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid32_1.DisplayLayout.Override.GroupByRowAppearance = appearance81;
            appearance82.TextHAlignAsString = "Left";
            this.uGrid32_1.DisplayLayout.Override.HeaderAppearance = appearance82;
            this.uGrid32_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid32_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.Color.Silver;
            this.uGrid32_1.DisplayLayout.Override.RowAppearance = appearance83;
            this.uGrid32_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid32_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance84;
            this.uGrid32_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid32_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid32_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid32_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid32_1.Location = new System.Drawing.Point(0, 32);
            this.uGrid32_1.Name = "uGrid32_1";
            this.uGrid32_1.Size = new System.Drawing.Size(782, 279);
            this.uGrid32_1.TabIndex = 4;
            this.uGrid32_1.Text = "ultraGrid2";
            this.uGrid32_1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // pictureBox53
            // 
            this.pictureBox53.BackColor = System.Drawing.Color.Gold;
            this.pictureBox53.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox53.Location = new System.Drawing.Point(0, 28);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(782, 4);
            this.pictureBox53.TabIndex = 126;
            this.pictureBox53.TabStop = false;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.SystemColors.Control;
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Controls.Add(this.btnDetailData6);
            this.panel33.Controls.Add(this.btnExcel10);
            this.panel33.Controls.Add(this.btnNewTest6);
            this.panel33.Controls.Add(this.btnDelete9);
            this.panel33.Controls.Add(this.label42);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(782, 28);
            this.panel33.TabIndex = 1;
            // 
            // btnDetailData6
            // 
            this.btnDetailData6.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDetailData6.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Infor;
            this.btnDetailData6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetailData6.Location = new System.Drawing.Point(500, 0);
            this.btnDetailData6.Name = "btnDetailData6";
            this.btnDetailData6.Size = new System.Drawing.Size(70, 26);
            this.btnDetailData6.TabIndex = 13;
            this.btnDetailData6.Text = "상세";
            this.btnDetailData6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetailData6.UseVisualStyleBackColor = true;
            this.btnDetailData6.Click += new System.EventHandler(this.btnDetailData_Click);
            // 
            // btnExcel10
            // 
            this.btnExcel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExcel10.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel10.Location = new System.Drawing.Point(570, 0);
            this.btnExcel10.Name = "btnExcel10";
            this.btnExcel10.Size = new System.Drawing.Size(70, 26);
            this.btnExcel10.TabIndex = 14;
            this.btnExcel10.Text = "엑셀";
            this.btnExcel10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel10.UseVisualStyleBackColor = true;
            this.btnExcel10.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnNewTest6
            // 
            this.btnNewTest6.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnNewTest6.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.NewPopup;
            this.btnNewTest6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNewTest6.Location = new System.Drawing.Point(640, 0);
            this.btnNewTest6.Name = "btnNewTest6";
            this.btnNewTest6.Size = new System.Drawing.Size(70, 26);
            this.btnNewTest6.TabIndex = 7;
            this.btnNewTest6.Text = "신규";
            this.btnNewTest6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewTest6.UseVisualStyleBackColor = true;
            this.btnNewTest6.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnDelete9
            // 
            this.btnDelete9.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDelete9.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete9.Location = new System.Drawing.Point(710, 0);
            this.btnDelete9.Name = "btnDelete9";
            this.btnDelete9.Size = new System.Drawing.Size(70, 26);
            this.btnDelete9.TabIndex = 12;
            this.btnDelete9.Text = "삭제";
            this.btnDelete9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete9.UseVisualStyleBackColor = true;
            this.btnDelete9.Click += new System.EventHandler(this.btnDelete9_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.Location = new System.Drawing.Point(6, 7);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(109, 12);
            this.label42.TabIndex = 5;
            this.label42.Text = "수도꼭지시험관리";
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackColor = System.Drawing.Color.Gold;
            this.pictureBox52.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox52.Location = new System.Drawing.Point(4, 56);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(782, 4);
            this.pictureBox52.TabIndex = 136;
            this.pictureBox52.TabStop = false;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.SystemColors.Control;
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.udtWaterCollectDateE6);
            this.panel34.Controls.Add(this.label47);
            this.panel34.Controls.Add(this.label44);
            this.panel34.Controls.Add(this.udtWaterCollectDateS6);
            this.panel34.Controls.Add(this.txtConsumer2);
            this.panel34.Controls.Add(this.btnSearch2);
            this.panel34.Controls.Add(this.label43);
            this.panel34.Controls.Add(this.cbEstablishment8);
            this.panel34.Controls.Add(this.label45);
            this.panel34.Controls.Add(this.btnQuery10);
            this.panel34.Controls.Add(this.btnClose10);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel34.Location = new System.Drawing.Point(4, 4);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(782, 52);
            this.panel34.TabIndex = 134;
            // 
            // udtWaterCollectDateE6
            // 
            this.udtWaterCollectDateE6.Location = new System.Drawing.Point(193, 26);
            this.udtWaterCollectDateE6.Name = "udtWaterCollectDateE6";
            this.udtWaterCollectDateE6.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateE6.TabIndex = 35;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.Location = new System.Drawing.Point(180, 30);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(15, 12);
            this.label47.TabIndex = 36;
            this.label47.Text = "~";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.Location = new System.Drawing.Point(6, 30);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 12);
            this.label44.TabIndex = 34;
            this.label44.Text = "채수일자 :";
            // 
            // udtWaterCollectDateS6
            // 
            this.udtWaterCollectDateS6.Location = new System.Drawing.Point(79, 26);
            this.udtWaterCollectDateS6.Name = "udtWaterCollectDateS6";
            this.udtWaterCollectDateS6.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateS6.TabIndex = 33;
            // 
            // txtConsumer2
            // 
            this.txtConsumer2.Location = new System.Drawing.Point(282, 3);
            this.txtConsumer2.Name = "txtConsumer2";
            this.txtConsumer2.Size = new System.Drawing.Size(118, 21);
            this.txtConsumer2.TabIndex = 37;
            // 
            // btnSearch2
            // 
            this.btnSearch2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Find;
            this.btnSearch2.Location = new System.Drawing.Point(400, 1);
            this.btnSearch2.Name = "btnSearch2";
            this.btnSearch2.Size = new System.Drawing.Size(26, 24);
            this.btnSearch2.TabIndex = 29;
            this.btnSearch2.UseVisualStyleBackColor = true;
            this.btnSearch2.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.Location = new System.Drawing.Point(222, 7);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 12);
            this.label43.TabIndex = 27;
            this.label43.Text = "수용가 :";
            // 
            // cbEstablishment8
            // 
            this.cbEstablishment8.FormattingEnabled = true;
            this.cbEstablishment8.Location = new System.Drawing.Point(66, 3);
            this.cbEstablishment8.Name = "cbEstablishment8";
            this.cbEstablishment8.Size = new System.Drawing.Size(150, 20);
            this.cbEstablishment8.TabIndex = 23;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.Location = new System.Drawing.Point(6, 7);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 12);
            this.label45.TabIndex = 18;
            this.label45.Text = "사업장 :";
            // 
            // btnQuery10
            // 
            this.btnQuery10.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery10.Location = new System.Drawing.Point(640, 12);
            this.btnQuery10.Name = "btnQuery10";
            this.btnQuery10.Size = new System.Drawing.Size(70, 26);
            this.btnQuery10.TabIndex = 12;
            this.btnQuery10.Text = "조회";
            this.btnQuery10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery10.UseVisualStyleBackColor = true;
            this.btnQuery10.Click += new System.EventHandler(this.btnQuery10_Click);
            // 
            // btnClose10
            // 
            this.btnClose10.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose10.Location = new System.Drawing.Point(710, 12);
            this.btnClose10.Name = "btnClose10";
            this.btnClose10.Size = new System.Drawing.Size(70, 26);
            this.btnClose10.TabIndex = 3;
            this.btnClose10.Text = "닫기";
            this.btnClose10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose10.UseVisualStyleBackColor = true;
            this.btnClose10.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox26.Location = new System.Drawing.Point(786, 4);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(4, 367);
            this.pictureBox26.TabIndex = 133;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox27.Location = new System.Drawing.Point(0, 4);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(4, 367);
            this.pictureBox27.TabIndex = 132;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox28.Location = new System.Drawing.Point(0, 371);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(790, 4);
            this.pictureBox28.TabIndex = 131;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox29.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox29.Location = new System.Drawing.Point(0, 0);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(790, 4);
            this.pictureBox29.TabIndex = 130;
            this.pictureBox29.TabStop = false;
            // 
            // picFrRightM3
            // 
            this.picFrRightM3.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightM3.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightM3.Location = new System.Drawing.Point(798, 4);
            this.picFrRightM3.Name = "picFrRightM3";
            this.picFrRightM3.Size = new System.Drawing.Size(4, 401);
            this.picFrRightM3.TabIndex = 112;
            this.picFrRightM3.TabStop = false;
            // 
            // picFrLeftM3
            // 
            this.picFrLeftM3.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM3.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM3.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftM3.Name = "picFrLeftM3";
            this.picFrLeftM3.Size = new System.Drawing.Size(4, 401);
            this.picFrLeftM3.TabIndex = 111;
            this.picFrLeftM3.TabStop = false;
            // 
            // picFrBottomM3
            // 
            this.picFrBottomM3.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomM3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomM3.Location = new System.Drawing.Point(0, 405);
            this.picFrBottomM3.Name = "picFrBottomM3";
            this.picFrBottomM3.Size = new System.Drawing.Size(802, 4);
            this.picFrBottomM3.TabIndex = 110;
            this.picFrBottomM3.TabStop = false;
            // 
            // picFrTopM3
            // 
            this.picFrTopM3.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopM3.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopM3.Location = new System.Drawing.Point(0, 0);
            this.picFrTopM3.Name = "picFrTopM3";
            this.picFrTopM3.Size = new System.Drawing.Size(802, 4);
            this.picFrTopM3.TabIndex = 109;
            this.picFrTopM3.TabStop = false;
            // 
            // uTabMenuS3
            // 
            this.uTabMenuS3.Controls.Add(this.ultraTabSharedControlsPage4);
            this.uTabMenuS3.Controls.Add(this.uTabPageM3S1);
            this.uTabMenuS3.Controls.Add(this.uTabPageM3S2);
            this.uTabMenuS3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabMenuS3.Location = new System.Drawing.Point(4, 4);
            this.uTabMenuS3.Name = "uTabMenuS3";
            this.uTabMenuS3.SharedControlsPage = this.ultraTabSharedControlsPage4;
            this.uTabMenuS3.Size = new System.Drawing.Size(794, 401);
            this.uTabMenuS3.TabIndex = 113;
            ultraTab1.TabPage = this.uTabPageM3S1;
            ultraTab1.Text = "수도꼭지지점관리";
            ultraTab2.TabPage = this.uTabPageM3S2;
            ultraTab2.Text = "수도꼭지시험관리";
            this.uTabMenuS3.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage4
            // 
            this.ultraTabSharedControlsPage4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage4.Name = "ultraTabSharedControlsPage4";
            this.ultraTabSharedControlsPage4.Size = new System.Drawing.Size(790, 375);
            // 
            // frmWQFaucet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 409);
            this.Controls.Add(this.uTabMenuS3);
            this.Controls.Add(this.picFrRightM3);
            this.Controls.Add(this.picFrLeftM3);
            this.Controls.Add(this.picFrBottomM3);
            this.Controls.Add(this.picFrTopM3);
            this.MinimizeBox = false;
            this.Name = "frmWQFaucet";
            this.Text = "수도꼭지시험결과관리";
            this.Load += new System.EventHandler(this.frmWQFaucet_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQFaucet_FormClosing);
            this.uTabPageM3S1.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid31_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            this.uTabPageM3S2.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid32_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightM3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomM3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopM3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenuS3)).EndInit();
            this.uTabMenuS3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrRightM3;
        private System.Windows.Forms.PictureBox picFrLeftM3;
        private System.Windows.Forms.PictureBox picFrBottomM3;
        private System.Windows.Forms.PictureBox picFrTopM3;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenuS3;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM3S1;
        private System.Windows.Forms.Panel panel29;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid31_1;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Button btnAppend4;
        private System.Windows.Forms.Button btnExcel9;
        private System.Windows.Forms.Button btnSave5;
        private System.Windows.Forms.Button btnDelete8;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.TextBox txtConsumer1;
        private System.Windows.Forms.Button btnSearch1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbEstablishment7;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cbDivisionName;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btnQuery9;
        private System.Windows.Forms.Button btnClose9;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM3S2;
        private System.Windows.Forms.Panel panel32;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid32_1;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Button btnDetailData6;
        private System.Windows.Forms.Button btnExcel10;
        private System.Windows.Forms.Button btnNewTest6;
        private System.Windows.Forms.Button btnDelete9;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.Panel panel34;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateE6;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateS6;
        private System.Windows.Forms.TextBox txtConsumer2;
        private System.Windows.Forms.Button btnSearch2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cbEstablishment8;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btnQuery10;
        private System.Windows.Forms.Button btnClose10;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
    }
}