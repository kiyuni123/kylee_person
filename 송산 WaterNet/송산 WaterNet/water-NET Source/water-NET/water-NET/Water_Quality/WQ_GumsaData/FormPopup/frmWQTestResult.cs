﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_GumsaData.FormPopup
{
    public partial class frmWQTestResult : Form
    {
        private OracleDBManager m_oDBManager = null;

        #region 맵 프로퍼티

        private IMapControl3 _Map;

        public IMapControl3 IMAP
        {
            get
            {
                return _Map;
            }
            set
            {
                _Map = value;
            }
        }

        #endregion

        public frmWQTestResult()
        {
            WQ_AppStatic.IS_SHOW_FORM_TEST_RESULT = true;
            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                WQ_AppStatic.IS_SHOW_FORM_TEST_RESULT = false;
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            InitializeComponent();

            InitializeSetting();
        }

        private void frmWQTestResult_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================수질관리(수질시험 결과관리)

            object o = EMFrame.statics.AppStatic.USER_MENU["수질검사자료관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnDelete3.Enabled = false;
                this.btnDelete4.Enabled = false;
                this.btnDelete5.Enabled = false;
                this.btnDelete6.Enabled = false;
                this.btnDelete7.Enabled = false;
            }

            //===========================================================================

            WQ_Function.SetUDateTime_MaskInput(this.udtWaterCollectDateS1, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtWaterCollectDateS1, -30);
            WQ_Function.SetUDateTime_MaskInput(this.udtWaterCollectDateS2, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtWaterCollectDateS2, -30);
            WQ_Function.SetUDateTime_MaskInput(this.udtWaterCollectDateS3, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtWaterCollectDateS3, -30);
            WQ_Function.SetUDateTime_MaskInput(this.udtWaterCollectDateS4, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtWaterCollectDateS4, -30);
            WQ_Function.SetUDateTime_MaskInput(this.udtWaterCollectDateS5, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtWaterCollectDateS5, -30);

            //수질관리 DATA 조회
            this.GetDailyWQTestData();
            this.GetWeeklyWQTestData();
            this.GetMonthlyWQTestData();
            this.GetQuarterlyWQTestData();
            this.GetHalfYearlyWQTestData();
        }

        private void frmWQTestResult_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_TEST_RESULT = false;
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            // TODO
            InitializeGridSetting();
            InitializeControlSetting();
        }

        /// <summary>
        /// frmWQMain의 UltraTab Control에 종속된 UltraGrid의 Header 등 기본 정보를 Setting한다.
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            #region 수질관리 Grid Set

            #region 수질관리/일일수질시험관리

            oUltraGridColumn = uGrid21_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid21_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CLTDT";
            oUltraGridColumn.Header.Caption = "채수일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid21_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REGMNGR";
            oUltraGridColumn.Header.Caption = "담당자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid21_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMVOL";
            oUltraGridColumn.Header.Caption = "항목수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid21_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMGRP";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid21_1);

            #endregion

            #region 수질관리/주간수질시험관리

            oUltraGridColumn = uGrid22_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid22_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CLTDT";
            oUltraGridColumn.Header.Caption = "채수일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid22_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REGMNGR";
            oUltraGridColumn.Header.Caption = "담당자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid22_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMVOL";
            oUltraGridColumn.Header.Caption = "항목수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid22_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMGRP";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid22_1);

            #endregion

            #region 수질관리/월간수질시험관리

            oUltraGridColumn = uGrid23_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid23_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CLTDT";
            oUltraGridColumn.Header.Caption = "채수일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid23_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REGMNGR";
            oUltraGridColumn.Header.Caption = "담당자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid23_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMVOL";
            oUltraGridColumn.Header.Caption = "항목수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid23_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMGRP";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid23_1);

            #endregion

            #region 수질관리/분기수질시험관리

            oUltraGridColumn = uGrid24_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid24_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CLTDT";
            oUltraGridColumn.Header.Caption = "채수일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid24_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REGMNGR";
            oUltraGridColumn.Header.Caption = "담당자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid24_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMVOL";
            oUltraGridColumn.Header.Caption = "항목수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid24_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMGRP";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid24_1);

            #endregion

            #region 수질관리/반기수질시험관리

            oUltraGridColumn = uGrid25_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid25_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CLTDT";
            oUltraGridColumn.Header.Caption = "채수일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid25_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REGMNGR";
            oUltraGridColumn.Header.Caption = "담당자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid25_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMVOL";
            oUltraGridColumn.Header.Caption = "항목수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid25_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMGRP";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid25_1);

            #endregion

            #endregion

            #region Grid Set - AutoResizeColumes

            FormManager.SetGridStyle_PerformAutoResize(this.uGrid21_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid22_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid23_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid24_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid25_1);

            #endregion

            #region Grid Set - AffterRowInsert, AfterCellUpdate

            this.uGrid21_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid21_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid22_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid22_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid23_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid23_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid24_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid24_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid25_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid25_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);

            #endregion

        }

        /// <summary>
        /// frmWQMain에 종속된 Control중 UltraGrid를 제외한 다른 Control에 대해 기본 정보를 Setting한다.
        /// </summary>
        private void InitializeControlSetting()
        {
            WQ_Function.SetCombo_Establishment(this.cbEstablishment2, 0);
            WQ_Function.SetCombo_Establishment(this.cbEstablishment3, 0);
            WQ_Function.SetCombo_Establishment(this.cbEstablishment4, 0);
            WQ_Function.SetCombo_Establishment(this.cbEstablishment5, 0);
            WQ_Function.SetCombo_Establishment(this.cbEstablishment6, 0);
        }

        #region Button Events

        #region 공통

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.CloseForm();
        }

        private void btnDetailData_Click(object sender, EventArgs e)
        {
            this.OpenToTestPopup(1);
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ExportToExcelFromUltraGrid(this.uTabMenuS2.SelectedTab.Index);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 다수의 btnNew# 들이 사용하는 이벤트 프로시져
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewTest_Click(object sender, EventArgs e)
        {
            this.OpenToTestPopup(0);
        }

        #endregion

        #region - 수질관리/일일수질시험관리

        private void btnQuery4_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetDailyWQTestData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnDelete3_Click(object sender, EventArgs e)
        {
            if (this.uGrid21_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    this.DeleteDailyWQTestData();
                    this.GetDailyWQTestData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #region - 수질관리/주간수질시험관리

        private void btnQuery5_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetWeeklyWQTestData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnDelete4_Click(object sender, EventArgs e)
        {
            if (this.uGrid22_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    this.DeleteWeeklyWQTestData();
                    this.GetWeeklyWQTestData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #region - 수질관리/월간수질시험관리

        private void btnQuery6_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetMonthlyWQTestData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnDelete5_Click(object sender, EventArgs e)
        {
            if (this.uGrid23_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    //this.DeleteTestItemData();
                    this.GetMonthlyWQTestData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #region - 수질관리/분기수질시험관리

        private void btnQuery7_Click(object sender, EventArgs e)
        {
            this.GetQuarterlyWQTestData();
        }

        private void btnDelete6_Click(object sender, EventArgs e)
        {
            if (this.uGrid24_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    //this.DeleteTestItemData();
                    this.GetQuarterlyWQTestData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #region - 수질관리/반기수질시험관리

        private void btnQuery8_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetHalfYearlyWQTestData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnDelete7_Click(object sender, EventArgs e)
        {
            if (this.uGrid25_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    this.DeleteHalfYearlyWQTestData();
                    this.GetHalfYearlyWQTestData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #endregion

        #region Control Events

        #region - 수질관리/일일,주간,월간,분기,반기수질시험관리

        private void uGrid2N_1_DoubleClick(object sender, EventArgs e)
        {
            this.OpenToTestPopup(1);
        }

        private void uGrid_Click(object sender, EventArgs e)
        {
            UltraGrid oUGrid = null;

            string strPUR_NAM = string.Empty;
            int iSTabIndex = this.uTabMenuS2.SelectedTab.Index;
            
            if (_Map == null) return;
            
            switch (iSTabIndex)
            {
                case 0: // 수질관리/일일수질시험관리
                    oUGrid = this.uGrid21_1;
                    break;

                case 1: // 수질관리/주간수질시험관리
                    oUGrid = this.uGrid22_1;
                    break;

                case 2: // 수질관리/월간수질시험관리
                    oUGrid = this.uGrid23_1;
                    break;

                case 3: // 수질관리/분기수질시험관리
                    oUGrid = this.uGrid24_1;
                    break;

                case 4: // 수질관리/반기수질시험관리
                    oUGrid = this.uGrid25_1;
                    break;
            }

            if (oUGrid.Rows.Count <= 0) return;

            strPUR_NAM = WQ_Function.SplitToCodeName(oUGrid.ActiveRow.Cells[0].Text);

            ILayer pLayer = WaterAOCore.ArcManager.GetMapLayer(_Map, "정수장");

            IFeature pFeature = ArcManager.GetFeature(pLayer, "PUR_NAM='" + strPUR_NAM + "'");

            if (pFeature != null)
            {
                //_Map.Extent = pFeature.Shape.Envelope;
                if (ArcManager.GetMapScale(_Map) > (double)1000)
                {
                    ArcManager.SetMapScale(_Map, (double)1000);
                }

                ArcManager.MoveCenterAt(_Map, pFeature);

                ArcManager.FlashShape(_Map.ActiveView, (IGeometry)pFeature.Shape, 10);

                ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();
            }
        }

        #endregion
        
        #region User Function

        #region 공통
        
        /// <summary>
        /// Open to New Test Popup (수질 또는 수도꼭지시험관리 : FormPopup.frmWQPTestItem)
        /// </summary>
        /// <param name="iNDType">수질시험관리 : 0 - 신규, 1 - 상세 구분, 수도꼭지시험관리 : 0 - 신규</param>
        private void OpenToTestPopup(int iNDType)
        {
            try
            {
                string strNDTitle = string.Empty;
                int iSTabIndex = 0;

                if (iNDType == 0)
                {
                    strNDTitle = "신규";
                }
                else
                {
                    strNDTitle = "상세";
                }

                WaterNet.WQ_GumsaData.FormPopup.frmWQPTestItem oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQPTestItem();

                iSTabIndex = this.uTabMenuS2.SelectedTab.Index;
                oForm.Title = this.uTabMenuS2.SelectedTab.Text.ToString() + strNDTitle;

                switch (iSTabIndex)
                {
                    case 0: // 수질관리/일일수질시험관리
                        if (iNDType == 1 && this.uGrid21_1.Rows.Count == 0) return;

                        oForm.Establishment = this.cbEstablishment2.Text.ToString();
                        if (iNDType != 0)
                        {
                            oForm.WaterCollectDate = this.uGrid21_1.ActiveRow.Cells[1].Text;
                        }
                        else
                        {
                            oForm.WaterCollectDate = DateTime.Now.ToString();
                        }
                        oForm.MeasurePeriod = "1";
                        oForm.ShowDialog();
                        this.cbEstablishment2.Text = oForm.Establishment;
                        this.udtWaterCollectDateS1.Value = Convert.ToDateTime(oForm.WaterCollectDate);
                        this.GetDailyWQTestData();
                        break;

                    case 1: // 수질관리/주간수질시험관리
                        if (iNDType == 1 && this.uGrid22_1.Rows.Count == 0) return;

                        oForm.Establishment = this.cbEstablishment3.Text.ToString();
                        if (iNDType != 0)
                        {
                            oForm.WaterCollectDate = this.uGrid22_1.ActiveRow.Cells[1].Text;
                        }
                        else
                        {
                            oForm.WaterCollectDate = DateTime.Now.ToString();
                        }
                        oForm.MeasurePeriod = "2";
                        oForm.ShowDialog();
                        this.cbEstablishment3.Text = oForm.Establishment;
                        this.udtWaterCollectDateS2.Value = Convert.ToDateTime(oForm.WaterCollectDate);
                        this.GetWeeklyWQTestData();
                        break;

                    case 2: // 수질관리/월간수질시험관리
                        if (iNDType == 1 && this.uGrid23_1.Rows.Count == 0) return;

                        oForm.Establishment = this.cbEstablishment4.Text.ToString();
                        if (iNDType != 0)
                        {
                            oForm.WaterCollectDate = this.uGrid23_1.ActiveRow.Cells[1].Text;
                        }
                        else
                        {
                            oForm.WaterCollectDate = DateTime.Now.ToString();
                        }
                        oForm.MeasurePeriod = "3";
                        oForm.ShowDialog();
                        this.cbEstablishment4.Text = oForm.Establishment;
                        this.udtWaterCollectDateS3.Value = Convert.ToDateTime(oForm.WaterCollectDate);
                        this.GetMonthlyWQTestData();
                        break;

                    case 3: // 수질관리/분기수질시험관리
                        if (iNDType == 1 && this.uGrid24_1.Rows.Count == 0) return;

                        oForm.Establishment = this.cbEstablishment5.Text.ToString();
                        if (iNDType != 0)
                        {
                            oForm.WaterCollectDate = this.uGrid24_1.ActiveRow.Cells[1].Text;
                        }
                        else
                        {
                            oForm.WaterCollectDate = DateTime.Now.ToString();
                        }
                        oForm.MeasurePeriod = "4";
                        oForm.ShowDialog();
                        this.cbEstablishment5.Text = oForm.Establishment;
                        this.udtWaterCollectDateS4.Value = Convert.ToDateTime(oForm.WaterCollectDate);
                        this.GetQuarterlyWQTestData();
                        break;

                    case 4: // 수질관리/반기수질시험관리
                        if (iNDType == 1 && this.uGrid25_1.Rows.Count == 0) return;

                        oForm.Establishment = this.cbEstablishment6.Text.ToString();
                        if (iNDType != 0)
                        {
                            oForm.WaterCollectDate = this.uGrid25_1.ActiveRow.Cells[1].Text;
                        }
                        else
                        {
                            oForm.WaterCollectDate = DateTime.Now.ToString();
                        }
                        oForm.MeasurePeriod = "5";
                        oForm.ShowDialog();
                        this.cbEstablishment6.Text = oForm.Establishment;
                        this.udtWaterCollectDateS5.Value = Convert.ToDateTime(oForm.WaterCollectDate);
                        this.GetHalfYearlyWQTestData();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "OpenToTestPopup()", ex.Message.ToString(), "");
            }
        }

        /// <summary>
        /// 현재 창을 닫는다.
        /// </summary>
        private void CloseForm()
        {
            WQ_AppStatic.IS_SHOW_FORM_TEST_RESULT = false;
            if (m_oDBManager != null)
            {
                m_oDBManager.Close();
            }
            this.Dispose();
            this.Close();
        }

        /// <summary>
        /// UltraGrid에서 Excel로 Export 한다.
        /// </summary>
        /// <param name="iMTabIndex">uTabMenuM의 SelectedTab.Index</param>
        private void ExportToExcelFromUltraGrid(int iMTabIndex)
        {
            try
            {
                string strTitle = string.Empty;

                UltraGrid oUGrid = new UltraGrid();
                UltraGrid oUGrid2 = new UltraGrid();
                strTitle = this.uTabMenuS2.SelectedTab.Text.ToString();

                switch (iMTabIndex)
                {
                    case 0: // 수질관리/일일수질시험관리
                        oUGrid = this.uGrid21_1;
                        break;

                    case 1: // 수질관리/주간수질시험관리
                        oUGrid = this.uGrid22_1;
                        break;

                    case 2: // 수질관리/월간수질시험관리
                        oUGrid = this.uGrid23_1;
                        break;

                    case 3: // 수질관리/분기수질시험관리
                        oUGrid = this.uGrid24_1;
                        break;

                    case 4: // 수질관리/반기수질시험관리
                        oUGrid = this.uGrid25_1;
                        break;
                }

                if (oUGrid.Rows.Count <= 0) return;
                FormManager.ExportToExcelFromUltraGrid(oUGrid, oUGrid2, strTitle, 1);
            }
            catch (Exception ex)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExportToExcelFromUltraGrid()", ex.Message.ToString(), "");
            }
        }
        
        #endregion

        #region - 수질관리/일일수질시험관리

        /// <summary>
        /// 일일수질시험관리 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetDailyWQTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strSITE = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment2.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strSDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateS1.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateE1.DateTime);

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT BETWEEN '" + strSDate + "' AND '" + strEDate + "'";
            oStringBuilder.AppendLine("SELECT   '" + strSITE + "' AS SITECD, TO_DATE(CLTDT) AS CLTDT, REGMNGR, (SELECT COUNT(DISTINCT(TITCD)) FROM WQCPPRTIT WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SRPRID = '1') AS ITEMVOL, DECODE(REQNUM, '', REQNUM, 'LIMS 연계 자료') AS ITEMGRP");
            oStringBuilder.AppendLine("FROM     WQCDWQSMR");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY CLTDT");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCDWQSMR");

            this.uGrid21_1.DataSource = pDS.Tables["WQCDWQSMR"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid21_1);

            //첫번째 Row에 Activate 부여
            if (this.uGrid21_1.Rows.Count > 0) this.uGrid21_1.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteDailyWQTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid21_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid21_1.ActiveRow.Cells[0].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCLTDT = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGrid21_1.ActiveRow.Cells[1].Value));

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCDWQSMR");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPDWQRL");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        #region - 수질관리/주간수질시험관리

        /// <summary>
        /// 주간수질시험관리 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetWeeklyWQTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strSITE = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment3.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strSDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateS2.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateE2.DateTime);

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SPRPID = '2' AND CLTDT BETWEEN '" + strSDate + "' AND '" + strEDate + "'";
            oStringBuilder.AppendLine("SELECT   '" + strSITE + "' AS SITECD, TO_DATE(CLTDT) AS CLTDT, REGMNGR, (SELECT COUNT(DISTINCT(TITCD)) FROM WQCPPRTIT WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SRPRID = '2') AS ITEMVOL, DECODE(REQNUM, '', REQNUM, 'LIMS 연계 자료') AS ITEMGRP");
            oStringBuilder.AppendLine("FROM     WQCPWQSMR");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY CLTDT");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCPWQSMR");

            this.uGrid22_1.DataSource = pDS.Tables["WQCPWQSMR"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid22_1);

            //첫번째 Row에 Activate 부여
            if (this.uGrid22_1.Rows.Count > 0) this.uGrid22_1.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteWeeklyWQTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid22_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid22_1.ActiveRow.Cells[0].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCLTDT = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGrid22_1.ActiveRow.Cells[1].Value));

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND SPRPID = '2'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQSMR");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQRL");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        #region - 수질관리/월간수질시험관리

        /// <summary>
        /// 월간수질시험관리 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetMonthlyWQTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strSITE = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment4.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strSDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateS3.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateE3.DateTime);

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SPRPID = '3' AND CLTDT BETWEEN '" + strSDate + "' AND '" + strEDate + "'";
            oStringBuilder.AppendLine("SELECT   '" + strSITE + "' AS SITECD, TO_DATE(CLTDT) AS CLTDT, REGMNGR, (SELECT COUNT(DISTINCT(TITCD)) FROM WQCPPRTIT WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SRPRID = '3') AS ITEMVOL, DECODE(REQNUM, '', REQNUM, 'LIMS 연계 자료') AS ITEMGRP");
            oStringBuilder.AppendLine("FROM     WQCPWQSMR");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY CLTDT");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCPWQSMR");

            this.uGrid23_1.DataSource = pDS.Tables["WQCPWQSMR"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid23_1);

            //첫번째 Row에 Activate 부여
            if (this.uGrid23_1.Rows.Count > 0) this.uGrid23_1.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteMonthlyWQTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid23_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid23_1.ActiveRow.Cells[0].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCLTDT = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGrid23_1.ActiveRow.Cells[1].Value));

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND SPRPID = '3'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQSMR");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQRL");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        #region - 수질관리/분기수질시험관리

        /// <summary>
        /// 분기수질시험관리 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetQuarterlyWQTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strSITE = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment5.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strSDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateS4.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateE4.DateTime);

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SPRPID = '4' AND CLTDT BETWEEN '" + strSDate + "' AND '" + strEDate + "'";
            oStringBuilder.AppendLine("SELECT   '" + strSITE + "' AS SITECD, TO_DATE(CLTDT) AS CLTDT, REGMNGR, (SELECT COUNT(DISTINCT(TITCD)) FROM WQCPPRTIT WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SRPRID = '4') AS ITEMVOL, DECODE(REQNUM, '', REQNUM, 'LIMS 연계 자료') AS ITEMGRP");
            oStringBuilder.AppendLine("FROM     WQCPWQSMR");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY CLTDT");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCPWQSMR");

            this.uGrid24_1.DataSource = pDS.Tables["WQCPWQSMR"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid24_1);

            //첫번째 Row에 Activate 부여
            if (this.uGrid24_1.Rows.Count > 0) this.uGrid24_1.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteQuarterlyWQTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid24_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid24_1.ActiveRow.Cells[0].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCLTDT = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGrid24_1.ActiveRow.Cells[1].Value));

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND SPRPID = '4'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQSMR");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQRL");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        #region - 수질관리/반기수질시험관리

        /// <summary>
        /// 반기수질시험관리 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetHalfYearlyWQTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strSITE = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment6.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strSDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateS5.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateE5.DateTime);

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SPRPID = '5' AND CLTDT BETWEEN '" + strSDate + "' AND '" + strEDate + "'";
            oStringBuilder.AppendLine("SELECT   '" + strSITE + "' AS SITECD, TO_DATE(CLTDT) AS CLTDT, REGMNGR, (SELECT COUNT(DISTINCT(TITCD)) FROM WQCPPRTIT WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SRPRID = '5') AS ITEMVOL, DECODE(REQNUM, '', REQNUM, 'LIMS 연계 자료') AS ITEMGRP");
            oStringBuilder.AppendLine("FROM     WQCPWQSMR");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY CLTDT");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCPWQSMR");

            this.uGrid25_1.DataSource = pDS.Tables["WQCPWQSMR"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid25_1);

            //첫번째 Row에 Activate 부여
            if (this.uGrid25_1.Rows.Count > 0) this.uGrid25_1.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteHalfYearlyWQTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid25_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid25_1.ActiveRow.Cells[0].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCLTDT = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGrid25_1.ActiveRow.Cells[1].Value));

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND SPRPID = '5'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQSMR");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPWQRL");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        private void btnQuery7_Click_1(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

    }
}
