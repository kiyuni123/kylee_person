﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

#endregion

namespace WaterNet.WQ_GumsaData.FormPopup
{
    public partial class frmWQCode : Form
    {
        private OracleDBManager m_oDBManager = null;

        public frmWQCode()
        {
            WQ_AppStatic.IS_SHOW_FORM_CODE = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                WQ_AppStatic.IS_SHOW_FORM_CODE = false;
                this.Close();
                return;
            }

            InitializeComponent();

            InitializeSetting();
        }

        private void frmWQCode_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================수질관리(코드관리)

            object o = EMFrame.statics.AppStatic.USER_MENU["수질검사자료관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                //사업장별 공정관리
                this.btnSave1.Enabled = false;
                this.btnAppend1.Enabled = false;
                this.btnDelete1.Enabled = false;
                this.btnSave2.Enabled = false;

                //시험항목 관리
                this.btnSave3.Enabled = false;
                this.btnAppend2.Enabled = false;
                this.btnDelete2.Enabled = false;

                //사업장공 수질항목관리
                this.btnSave4.Enabled = false;

            }

            //===========================================================================

            this.GetEstablishmentData();
            this.GetEstablishmentWQData();
            this.GetTestItemData();
        }

        private void frmWQCode_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_CODE = false;
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            // TODO
            InitializeGridSetting();
            InitializeControlSetting();
        }

        /// <summary>
        /// frmWQMain의 UltraTab Control에 종속된 UltraGrid의 Header 등 기본 정보를 Setting한다.
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            #region 코드관리 Grid Set

            #region 코드관리/사업장별공정관리/사업장관리

            oUltraGridColumn = uGrid11_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SGCCD";
            oUltraGridColumn.Header.Caption = "지자체";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid11_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid11_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITENM";
            oUltraGridColumn.Header.Caption = "사업장명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid11_1);

            #endregion

            #region 코드관리/사업장별공정관리/사업장공정관리

            oUltraGridColumn = uGrid11_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USEYN";
            oUltraGridColumn.Header.Caption = "사용여부";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid11_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CD";
            oUltraGridColumn.Header.Caption = "공정코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid11_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CDNM";
            oUltraGridColumn.Header.Caption = "공정명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid11_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PRCSEQ";
            oUltraGridColumn.Header.Caption = "공정순번";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid11_2);

            #endregion

            #region 코드관리/시험항목관리

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITCD";
            oUltraGridColumn.Header.Caption = "코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITMNM";
            oUltraGridColumn.Header.Caption = "표기명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PRESET";
            oUltraGridColumn.Header.Caption = "입력형식";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VLDFGR";
            oUltraGridColumn.Header.Caption = "유효자리";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HMNTSVAL";
            oUltraGridColumn.Header.Caption = "상한기준치";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LMNTSVAL";
            oUltraGridColumn.Header.Caption = "하한기준치";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LTRSVAL";
            oUltraGridColumn.Header.Caption = "문자기준치";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITGRP";
            oUltraGridColumn.Header.Caption = "항목분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITKOR";
            oUltraGridColumn.Header.Caption = "국문명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITENG";
            oUltraGridColumn.Header.Caption = "영문명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid12_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITUNT";
            oUltraGridColumn.Header.Caption = "단위";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid12_1);

            #endregion

            #region 코드관리/사업장공정수질항목관리/사업장별공정

            oUltraGridColumn = uGrid13_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SGCCD";
            oUltraGridColumn.Header.Caption = "지자체";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid13_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid13_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CWPRCCD";
            oUltraGridColumn.Header.Caption = "공정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid13_1);

            #endregion

            #region 코드관리/사업장공정수질항목관리/시험항목관리

            oUltraGridColumn = uGrid13_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USEYN";
            oUltraGridColumn.Header.Caption = "사용여부";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid13_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITCD";
            oUltraGridColumn.Header.Caption = "시험항목";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid13_2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LINESEQ";
            oUltraGridColumn.Header.Caption = "정렬순서";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid13_2);

            #endregion

            #endregion

            #region Grid Set - AutoResizeColumes

            FormManager.SetGridStyle_PerformAutoResize(this.uGrid11_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid11_2);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid12_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid13_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid13_2);

            #endregion

            #region Grid Set - AffterRowInsert, AfterCellUpdate

            this.uGrid11_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid11_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid11_2.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid11_2.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid12_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid12_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid13_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid13_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid13_2.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid13_2.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);

            #endregion
        }

        /// <summary>
        /// frmWQMain에 종속된 Control중 UltraGrid를 제외한 다른 Control에 대해 기본 정보를 Setting한다.
        /// </summary>
        private void InitializeControlSetting()
        {
            WQ_Function.SetCombo_MeasurePeriod(this.cbMeasurePeriod);
            WQ_Function.SetCombo_Establishment(this.cbEstablishment1, 0);
            WQ_Function.SetCombo_TestItemCategory(this.cbCategoryName);
        }


        #region Button Events

        private void btnQuery1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetEstablishmentData();
                string strSITECD = string.Empty;
                if (this.uGrid11_1.Rows.Count > 0)
                {
                    strSITECD = this.uGrid11_1.ActiveRow.Cells[1].Text.ToString();
                    this.GetEstablishmentProcessData(EMFrame.statics.AppStatic.USER_SGCCD, strSITECD);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnQuery2_Click(object sender, EventArgs e)
        {
            this.GetTestItemData();
        }

        private void btnQuery3_Click(object sender, EventArgs e)
        {
            this.GetEstablishmentWQData();
            string strSITECD = string.Empty;
            string strCWPRCCD = string.Empty;
            string strTITCD = string.Empty;
            string strSRPRID = string.Empty;

            if (this.uGrid13_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid13_1.Rows[0].Cells[1].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCWPRCCD = this.uGrid13_1.ActiveRow.Cells[2].Text.ToString();
                strCWPRCCD = WQ_Function.SplitToCode(strCWPRCCD);
                strSRPRID = this.cbMeasurePeriod.Text.Trim();
                strSRPRID = WQ_Function.SplitToCode(strSRPRID);
                this.GetEstablishmentWQItemData(EMFrame.statics.AppStatic.USER_SGCCD, strSITECD, strCWPRCCD, strSRPRID);
            }
        }

        private void btnExcel1_Click(object sender, EventArgs e)
        {
            if (this.uGrid11_1.Rows.Count <= 0) return;
            FormManager.ExportToExcelFromUltraGrid(this.uGrid11_1, this.uGrid11_2, this.uTabMenuS1.SelectedTab.Text, 1);
        }

        private void btnExcel2_Click(object sender, EventArgs e)
        {
            if (this.uGrid12_1.Rows.Count <= 0) return;
            FormManager.ExportToExcelFromUltraGrid(this.uGrid12_1, this.uTabMenuS1.SelectedTab.Text);
        }

        private void btnExcel3_Click(object sender, EventArgs e)
        {
            if (this.uGrid13_1.Rows.Count <= 0) return;
            FormManager.ExportToExcelFromUltraGrid(this.uGrid13_1, this.uGrid13_2, this.uTabMenuS1.SelectedTab.Text, 1);
        }

        private void btnAppend1_Click(object sender, EventArgs e)
        {
            WaterNetCore.FormManager.SetGridStyle_Append(this.uGrid11_1);
            FormManager.SetGridStyle_ColumeNoEdit(this.uGrid11_1, "SGCCD");
        }

        private void btnSave1_Click(object sender, EventArgs e)
        {
            if (this.uGrid11_1.Rows.Count > 0)
            {
                this.SaveEstablishmentData();
                this.GetEstablishmentData();
                WQ_Function.SetCombo_Establishment(this.cbEstablishment1, 0);
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDelete1_Click(object sender, EventArgs e)
        {
            if (this.uGrid11_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    this.DeleteEstablishmentData();
                    this.GetEstablishmentData();
                    WQ_Function.SetCombo_Establishment(this.cbEstablishment1, 0);
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnSave2_Click(object sender, EventArgs e)
        {
            if (this.uGrid11_2.Rows.Count > 0)
            {
                this.SaveEstablishmentProcessData();
                this.GetEstablishmentData();
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAppend2_Click(object sender, EventArgs e)
        {
            WaterNetCore.FormManager.SetGridStyle_Append(this.uGrid12_1);
            //FormManager.SetGridStyle_ColumeNoEdit(this.uGrid12_1, "SGCCD");
        }

        private void btnSave3_Click(object sender, EventArgs e)
        {
            if (this.uGrid12_1.Rows.Count > 0)
            {
                this.SaveTestItemData();
                this.GetTestItemData();
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDelete2_Click(object sender, EventArgs e)
        {
            if (this.uGrid12_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    this.DeleteTestItemData();
                    this.GetTestItemData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnSave4_Click(object sender, EventArgs e)
        {
            if (this.uGrid13_1.Rows.Count > 0)
            {
                this.SaveEstablishmentWQItemData();
                this.GetEstablishmentWQData();
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.CloseForm();
        }

        #endregion


        #region Control Events

        private void uGrid11_1_Click(object sender, EventArgs e)
        {
            string strSITECD = string.Empty;
            if (this.uGrid11_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid11_1.ActiveRow.Cells[1].Text.ToString();
                this.GetEstablishmentProcessData(EMFrame.statics.AppStatic.USER_SGCCD, strSITECD);
            }
        }

        private void uGrid11_2_Click(object sender, EventArgs e)
        {
            if (this.uGrid11_2.Rows.Count <= 0) return;
            if (this.uGrid11_2.ActiveRow.Cells[0].Value.ToString().ToUpper() == false.ToString().ToUpper())
            {
                this.uGrid11_2.ActiveRow.Cells[3].Value = this.GetEstablishmentProcessMaxSequence();
                this.uGrid11_2.ActiveRow.Cells[0].Value = true;
            }
            else
            {
                int iNow = Convert.ToInt32(this.uGrid11_2.ActiveRow.Cells[3].Text);
                this.uGrid11_2.ActiveRow.Cells[3].Value = "";
                this.SetAutoEstablishmentProcessSequence(iNow);
                this.uGrid11_2.ActiveRow.Cells[0].Value = false;
            }
        }
               
        private void uGrid13_1_Click(object sender, EventArgs e)
        {
            string strSITECD = string.Empty;
            string strCWPRCCD = string.Empty;
            string strTITCD = string.Empty;
            string strSRPRID = string.Empty;

            if (this.uGrid13_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid13_1.Rows[0].Cells[1].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCWPRCCD = this.uGrid13_1.ActiveRow.Cells[2].Text.ToString();
                strCWPRCCD = WQ_Function.SplitToCode(strCWPRCCD);
                strSRPRID = this.cbMeasurePeriod.Text.Trim();
                strSRPRID = WQ_Function.SplitToCode(strSRPRID);
                this.GetEstablishmentWQItemData(EMFrame.statics.AppStatic.USER_SGCCD, strSITECD, strCWPRCCD, strSRPRID);
            }
        }

        private void uGrid13_2_Click(object sender, EventArgs e)
        {
            if (this.uGrid13_2.Rows.Count <= 0) return;
            if (this.uGrid13_2.ActiveRow.Cells[0].Value.ToString().ToUpper() == false.ToString().ToUpper())
            {
                this.uGrid13_2.ActiveRow.Cells[2].Value = this.GetEstablishmentWQItemMaxSequence();
                this.uGrid13_2.ActiveRow.Cells[0].Value = true;
            }
            else
            {
                int iNow = Convert.ToInt32(this.uGrid13_2.ActiveRow.Cells[2].Text);
                this.uGrid13_2.ActiveRow.Cells[2].Value = "";
                this.SetAutoEstablishmentWQItemSequence(iNow);
                this.uGrid13_2.ActiveRow.Cells[0].Value = false;
            }
        }

        #endregion


        #region User Function

        #region - 코드관리/사업장별공정관리

        /// <summary>
        /// 사업장별공정관리 순번의 현재 최대 값을 계산해 반환한다.
        /// </summary>
        /// <param name="iNow"></param>
        private string GetEstablishmentProcessMaxSequence()
        {
            int iMax = 1;
            for (int i = 0; i < this.uGrid11_2.Rows.Count; i++)
            {
                if (this.uGrid11_2.Rows[i].Cells[3].Text.Trim() != "")
                {
                    iMax = iMax + 1;
                }

            }
            return iMax.ToString();
        }

        /// <summary>
        /// 사업장별공정관리의 순번을 자동으로 채번한다.
        /// </summary>
        /// <param name="iNow"></param>
        private void SetAutoEstablishmentProcessSequence(int iNow)
        {
            int iCur = 1;
            for (int i = 0; i < this.uGrid11_2.Rows.Count; i++)
            {
                if (this.uGrid11_2.Rows[i].Cells[3].Text.Trim() != "")
                {
                    iCur = Convert.ToInt32(this.uGrid11_2.Rows[i].Cells[3].Text);
                    if (iCur > iNow)
                    {
                        iCur = iCur - 1;
                        this.uGrid11_2.Rows[i].Cells[3].Value = iCur.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// 사업장 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetEstablishmentData()
        {

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;

            DataSet pDS = new DataSet();

            //사업장
            oStringBuilder.AppendLine("SELECT   '" + EMFrame.statics.AppStatic.USER_SGCNM + "' AS SGCCD, SITECD, SITENM");
            oStringBuilder.AppendLine("FROM     WQSITE");
            oStringBuilder.AppendLine("ORDER BY SGCCD, SITECD");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQSITE");

            FormManager.SetGridStyle(uGrid11_1);
            this.uGrid11_1.DataSource = pDS.Tables["WQSITE"].DefaultView;

            if (this.uGrid11_1.Rows.Count > 0)
            {
                //첫번째 Row에 Activate 부여
                this.uGrid11_1.Rows[0].Activated = true;
                strSITECD = this.uGrid11_1.ActiveRow.Cells[1].Text.ToString();
                this.GetEstablishmentProcessData(EMFrame.statics.AppStatic.USER_SGCCD, strSITECD);
            }
            FormManager.SetGridStyle_ColumeNoEdit(this.uGrid11_1, "SGCCD");
            FormManager.SetGridStyle_SetDefaultCellValue(this.uGrid11_1, "SGCCD", EMFrame.statics.AppStatic.USER_SGCNM + " (" + EMFrame.statics.AppStatic.USER_SGCCD + ")");
            FormManager.SetGridStyle_UnAppend(this.uGrid11_1);

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid11_1);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 사업장 공정 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        /// <param name="strSGCCD">지자체 코드</param>
        /// <param name="strSITECD">사업장 코드</param>
        private void GetEstablishmentProcessData(string strSGCCD, string strSITECD)
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;

            DataSet pDS = new DataSet();
            //사업장공정                            
            if (this.uGrid11_1.Rows.Count > 0)
            {
                strParam = "SGCCD = '" + strSGCCD + "' AND SITECD = '" + strSITECD + "'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   DECODE(UPPER(PRCSEQ), 99,'false', 'true') as USEYN,  CD, CDNM, DECODE(PRCSEQ, 99, NULL, PRCSEQ) AS PRCSEQ");
                oStringBuilder.AppendLine("FROM");
                oStringBuilder.AppendLine("        (SELECT  CWPRCCD AS CD, CWPRCNM AS CDNM, PRCSEQ");
                oStringBuilder.AppendLine("         FROM    WQCCWPPRC");
                oStringBuilder.AppendLine("         WHERE   " + strParam);
                oStringBuilder.AppendLine("         UNION");
                oStringBuilder.AppendLine("         SELECT  CD, CDNM, 99 as PRCSEQ");
                oStringBuilder.AppendLine("         FROM");
                oStringBuilder.AppendLine("                (SELECT  CD, CDNM");
                oStringBuilder.AppendLine("                 FROM    WQCODE");
                oStringBuilder.AppendLine("                 WHERE   TITLE = '공정'");
                oStringBuilder.AppendLine("                 MINUS");
                oStringBuilder.AppendLine("                 SELECT  CWPRCCD AS CD, CWPRCNM AS CDNM");
                oStringBuilder.AppendLine("                 FROM    WQCCWPPRC");
                oStringBuilder.AppendLine("                 WHERE   " + strParam + "))");
                oStringBuilder.AppendLine("ORDER BY TO_NUMBER(PRCSEQ), CD");

                pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCCWPPRC");

                this.uGrid11_2.DataSource = pDS.Tables["WQCCWPPRC"].DefaultView;

                //AutoResizeColumes
                FormManager.SetGridStyle_PerformAutoResize(this.uGrid11_2);

                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// UltraGrid에서 추가된 사업장 Data를 Database에 Insert한다.
        /// </summary>
        private void SaveEstablishmentData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strSITECD = string.Empty;
            string strSITENM = string.Empty;

            DataSet pDS = new DataSet();
            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid11_1.ActiveRow.Tag != null && this.uGrid11_1.ActiveRow.Tag.ToString() == "I")
            {
                strSITECD = this.uGrid11_1.ActiveRow.Cells[1].Text.ToString();
                strSITENM = this.uGrid11_1.ActiveRow.Cells[2].Text.ToString();
                strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + strSITENM + "', ";
                strParam += "'" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE)";
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT   INTO    WQSITE");
                oStringBuilder.AppendLine("(SGCCD, SITECD, SITENM, REGMNGR, REGMNGRIP, REGDT)");
                oStringBuilder.AppendLine("VALUES");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        /// <summary>
        /// UltraGrid에서 선택된 사업장공정 Data를 Database에 Insert한다.
        /// </summary>
        private void SaveEstablishmentProcessData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strSITECD = string.Empty;
            string strCWPRCCD = string.Empty;
            string strCWPRCNM = string.Empty;
            string strPRCSEQ = string.Empty;

            DataSet pDS = new DataSet();
            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            for (int i = 0; i < this.uGrid11_2.Rows.Count; i++)
            {
                if (this.uGrid11_2.Rows[i].Cells[0].Text.ToUpper() == "TRUE")
                {
                    strSITECD = this.uGrid11_1.ActiveRow.Cells[1].Text.ToString();
                    strCWPRCCD = this.uGrid11_2.Rows[i].Cells[1].Text.ToString();
                    strCWPRCNM = this.uGrid11_2.Rows[i].Cells[2].Text.ToString();
                    strPRCSEQ = this.uGrid11_2.Rows[i].Cells[3].Text.ToString();
                    strParam = "WHERE       SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CWPRCCD = '" + strCWPRCCD + "'";

                    //사업장 공정을 수정하므로 WQCCWPPRC에서 삭제후 INSERT.
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE");
                    oStringBuilder.AppendLine("FROM     WQCCWPPRC");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + strCWPRCCD + "', '" + strCWPRCNM + "', '" + strPRCSEQ + "', ";
                    strParam += "'" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE)";
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("INSERT   INTO    WQCCWPPRC");
                    oStringBuilder.AppendLine("(SGCCD, SITECD, CWPRCCD, CWPRCNM, PRCSEQ, REGMNGR, REGMNGRIP, REGDT)");
                    oStringBuilder.AppendLine("VALUES");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                }
                else
                {
                    strSITECD = this.uGrid11_1.ActiveRow.Cells[1].Text.ToString();
                    strCWPRCCD = this.uGrid11_2.Rows[i].Cells[1].Text.ToString();
                    strParam = "WHERE       SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CWPRCCD = '" + strCWPRCCD + "'";

                    //사업장 공정을 삭제
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE");
                    oStringBuilder.AppendLine("FROM     WQCCWPPRC");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);


                    strParam = "WHERE       SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND ";
                    strParam += "CWPRCCD = '" + strCWPRCCD + "'";

                    //사업장 공정을 삭제하므로 WQCPPRTIT에서도 시험 ITEM이 삭제되야 한다.
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE");
                    oStringBuilder.AppendLine("FROM     WQCPPRTIT");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                }
            }

            FormManager.SetGridStyle_UnAppend(this.uGrid11_1);
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteEstablishmentData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strSITECD = string.Empty;

            DataSet pDS = new DataSet();
            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid11_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid11_1.ActiveRow.Cells[1].Text.ToString();
                strParam = "WHERE       SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "'";

                //사업장 삭제
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQSITE ");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                //사업장공정 삭제
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCCWPPRC ");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                //공정수질시험항목 삭제
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPPRTIT ");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        #region - 코드관리/시험항목관리

        /// <summary>
        /// 시험항목관리를 Select해서 Grid에 Set한다.
        /// </summary>
        private void GetTestItemData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;

            DataSet pDS = new DataSet();

            strParam = " AND TITGRP = '" + this.cbCategoryName.Text.ToString() + "' ";

            if (this.txtItemName.Text.Trim() != "")
            {
                strParam += "AND UPPER(TITMNM) LIKE '%" + this.txtItemName.Text.ToUpper().Trim() + "%'";
            }
            oStringBuilder.AppendLine("SELECT   TITCD, TITMNM, PRESET, VLDFGR, HMNTSVAL, LMNTSVAL, LTRSVAL, TITGRP, TITKOR, TITENG, TITUNT");
            oStringBuilder.AppendLine("FROM     WQCTIT");
            oStringBuilder.AppendLine("WHERE    UPPER(USEYN) = '1'" + strParam);
            oStringBuilder.AppendLine("ORDER BY TITCD");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            this.uGrid12_1.DataSource = pDS.Tables["WQCTIT"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid12_1);

            FormManager.SetGridStyle_ColumeNoEdit(this.uGrid12_1, "TITGRP");
            FormManager.SetGridStyle_SetDefaultCellValue(this.uGrid12_1, "TITGRP", this.cbCategoryName.Text);
            FormManager.SetGridStyle_UnAppend(this.uGrid12_1);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// UltraGrid에서 추가된 사업장 Data를 Database에 Insert한다.
        /// </summary>
        private void SaveTestItemData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid12_1.ActiveRow.Tag != null && this.uGrid12_1.ActiveRow.Tag.ToString() == "I")
            {
                strParam = "('" + this.uGrid12_1.ActiveRow.Cells[0].Text.ToString() + "', '" + this.uGrid12_1.ActiveRow.Cells[1].Text.ToString() + "', '" + this.uGrid12_1.ActiveRow.Cells[2].Text.ToString() + "', ";
                strParam += "'" + this.uGrid12_1.ActiveRow.Cells[3].Text.ToString() + "', '" + this.uGrid12_1.ActiveRow.Cells[4].Text.ToString() + "', '" + this.uGrid12_1.ActiveRow.Cells[5].Text.ToString() + "', ";
                strParam += "'" + this.uGrid12_1.ActiveRow.Cells[6].Text.ToString() + "', '" + this.uGrid12_1.ActiveRow.Cells[7].Text.ToString() + "', '" + this.uGrid12_1.ActiveRow.Cells[8].Text.ToString() + "', ";
                strParam += "'" + this.uGrid12_1.ActiveRow.Cells[9].Text.ToString() + "', '" + this.uGrid12_1.ActiveRow.Cells[10].Text.ToString() + "', ";
                strParam += "'1', TO_CHAR(SYSDATE, 'RRRRMMDD'), '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE, 0.00, 0, '')";
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT   INTO    WQCTIT");
                oStringBuilder.AppendLine("(TITCD, TITMNM, PRESET, VLDFGR, HMNTSVAL, LMNTSVAL, LTRSVAL, TITGRP, TITKOR, TITENG, TITUNT, USEYN, CDT, REGMNGR, REGMNGRIP, REGDT, IFE, LINESEQ, LINENUM)");
                oStringBuilder.AppendLine("VALUES");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteTestItemData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid12_1.Rows.Count > 0)
            {
                //시험항목 삭제
                strParam = "WHERE       TITCD = '" + this.uGrid12_1.ActiveRow.Cells[0].Text.ToString() + "'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("UPDATE   WQCTIT");
                oStringBuilder.AppendLine("SET      USEYN = '0'");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                //시험항목을 삭제하므로 WQCPPRTIT에서도 시험 ITEM이 삭제되야 한다.
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCPPRTIT ");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        #region - 코드관리/사업장공정수질항목관리

        /// <summary>
        /// 사업장공정수질항목관리 순번의 현재 최대 값을 계산해 반환한다.
        /// </summary>
        /// <param name="iNow"></param>
        private string GetEstablishmentWQItemMaxSequence()
        {
            int iMax = 1;
            for (int i = 0; i < this.uGrid13_2.Rows.Count; i++)
            {
                if (this.uGrid13_2.Rows[i].Cells[2].Text.Trim() != "")
                {
                    iMax = iMax + 1;
                }

            }
            return iMax.ToString();
        }

        /// <summary>
        /// 사업장공정수질항목관리 순번을 자동으로 채번한다.
        /// </summary>
        /// <param name="iNow"></param>
        private void SetAutoEstablishmentWQItemSequence(int iNow)
        {
            int iCur = 1;
            for (int i = 0; i < this.uGrid13_2.Rows.Count; i++)
            {
                if (this.uGrid13_2.Rows[i].Cells[2].Text.Trim() != "")
                {
                    iCur = Convert.ToInt32(this.uGrid13_2.Rows[i].Cells[2].Text);
                    if (iCur > iNow)
                    {
                        iCur = iCur - 1;
                        this.uGrid13_2.Rows[i].Cells[2].Value = iCur.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// 사업장공정 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        private void GetEstablishmentWQData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strCWPRCCD = string.Empty;
            string strSRPRID = string.Empty;

            DataSet pDS = new DataSet();

            //사업장공정
            strSITECD = WQ_Function.SplitToCode(this.cbEstablishment1.Text);

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "'";
            oStringBuilder.AppendLine("SELECT   '" + EMFrame.statics.AppStatic.USER_SGCNM + "' AS SGCCD, '" + this.cbEstablishment1.Text + "' AS SITECD, CWPRCNM || ' (' || CWPRCCD || ')' AS CWPRCCD");
            oStringBuilder.AppendLine("FROM     WQCCWPPRC");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY PRCSEQ");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCCWPPRC");

            FormManager.SetGridStyle(uGrid13_1);
            this.uGrid13_1.DataSource = pDS.Tables["WQCCWPPRC"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid13_1);

            if (this.uGrid13_1.Rows.Count > 0)
            {
                //첫번째 Row에 Activate 부여
                this.uGrid13_1.Rows[0].Activated = true;
                strSITECD = this.uGrid13_1.Rows[0].Cells[1].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCWPRCCD = this.uGrid13_1.ActiveRow.Cells[2].Text.ToString();
                strCWPRCCD = WQ_Function.SplitToCode(strCWPRCCD);
                strSRPRID = this.cbMeasurePeriod.Text.Trim();
                strSRPRID = WQ_Function.SplitToCode(strSRPRID);
                this.GetEstablishmentWQItemData(EMFrame.statics.AppStatic.USER_SGCCD, strSITECD, strCWPRCCD, strSRPRID);
            }

            FormManager.SetGridStyle_UnAppend(this.uGrid13_1);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 사업장공정수질항목 Data를 Select 해서 Grid에 Set한다.
        /// </summary>
        /// <param name="strSGCCD">지자체 코드</param>
        /// <param name="strSITECD">사업장 코드</param>
        /// <param name="strSITECD">공정 코드</param>
        /// <param name="strSRPRID">측정주기</param>
        private void GetEstablishmentWQItemData(string strSGCCD, string strSITECD, string strCWPRCCD, string strSRPRID)
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;

            DataSet pDS = new DataSet();
            //사업장공정                            
            if (this.uGrid13_1.Rows.Count > 0)
            {
                //첫번째 Row에 Activate 부여
                strParam = "SGCCD = '" + strSGCCD + "' AND SITECD = '" + strSITECD + "' AND CWPRCCD = '" + strCWPRCCD + "' AND SRPRID = '" + strSRPRID + "'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   DECODE(UPPER(A.USEYN), '1','true', 'false') as USEYN,  (SELECT TITMNM || ' (' || TITCD || ')' FROM WQCTIT WHERE TITCD = A.TITCD) AS TITCD, DECODE(A.LINESEQ, 9999999999, NULL, A.LINESEQ) AS LINESEQ");
                oStringBuilder.AppendLine("FROM");
                oStringBuilder.AppendLine("        (SELECT  '1' AS USEYN, TITCD, LINESEQ");
                oStringBuilder.AppendLine("         FROM    WQCPPRTIT");
                oStringBuilder.AppendLine("         WHERE   " + strParam);
                oStringBuilder.AppendLine("         UNION");
                oStringBuilder.AppendLine("         SELECT  '0' AS USEYN, TITCD, 9999999999 AS LINESEQ");
                oStringBuilder.AppendLine("         FROM");
                oStringBuilder.AppendLine("                (SELECT  TITCD");
                oStringBuilder.AppendLine("                 FROM    WQCTIT");
                oStringBuilder.AppendLine("                 WHERE   USEYN = '1'");
                oStringBuilder.AppendLine("                 MINUS");
                oStringBuilder.AppendLine("                 SELECT  TITCD");
                oStringBuilder.AppendLine("                 FROM    WQCPPRTIT");
                oStringBuilder.AppendLine("                 WHERE   " + strParam + ")) A");
                oStringBuilder.AppendLine("ORDER BY USEYN DESC, A.LINESEQ, A.TITCD");

                pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCPPRTIT");

                this.uGrid13_2.DataSource = pDS.Tables["WQCPPRTIT"].DefaultView;

                //AutoResizeColumes
                FormManager.SetGridStyle_PerformAutoResize(this.uGrid13_2);

                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// UltraGrid에서 추가된 사업장 Data를 Database에 Insert한다.
        /// </summary>
        private void SaveEstablishmentWQItemData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strCWPRCCD = string.Empty;
            string strTITCD = string.Empty;
            string strSRPRID = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            for (int i = 0; i < this.uGrid13_2.Rows.Count; i++)
            {
                strSITECD = this.uGrid13_1.Rows[0].Cells[1].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCWPRCCD = this.uGrid13_1.ActiveRow.Cells[2].Text.ToString();
                strCWPRCCD = WQ_Function.SplitToCode(strCWPRCCD);
                strTITCD = this.uGrid13_2.Rows[i].Cells[1].Text.ToString();
                strTITCD = WQ_Function.SplitToCode(strTITCD);
                strSRPRID = this.cbMeasurePeriod.Text.Trim();
                strSRPRID = WQ_Function.SplitToCode(strSRPRID);
                //Check된 항목은 추가
                if (this.uGrid13_2.Rows[i].Cells[0].Text.ToUpper() == "TRUE")
                {
                    strParam = "WHERE       SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND ";
                    strParam += "CWPRCCD = '" + strCWPRCCD + "' AND TITCD = '" + strTITCD + "' AND SRPRID = '" + strSRPRID + "'";

                    //시험항목을 삭제하므로 WQCPPRTIT에서도 시험 ITEM이 삭제되야 한다.
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE");
                    oStringBuilder.AppendLine("FROM     WQCPPRTIT");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', ";
                    strParam += "'" + strCWPRCCD + "', '" + strTITCD + "', ";
                    strParam += "'" + strSRPRID + "', '" + this.uGrid13_2.Rows[i].Cells[2].Text.ToString() + "', ";
                    strParam += "'" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE, '')";
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("INSERT   INTO    WQCPPRTIT");
                    oStringBuilder.AppendLine("(SGCCD, SITECD, CWPRCCD, TITCD, SRPRID, LINESEQ, REGMNGR, REGMNGRIP, REGDT, PTUSEYN)");
                    oStringBuilder.AppendLine("VALUES");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                }
                else //Check되지 않은 항목은 Delete
                {
                    strParam = "WHERE       SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND ";
                    strParam += "CWPRCCD = '" + strCWPRCCD + "' AND TITCD = '" + strTITCD + "' AND SRPRID = '" + strSRPRID + "'";

                    //시험항목을 삭제하므로 WQCPPRTIT에서도 시험 ITEM이 삭제되야 한다.
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE");
                    oStringBuilder.AppendLine("FROM     WQCPPRTIT");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                }
            }
        }

        #endregion

        /// <summary>
        /// 현재 창을 닫는다.
        /// </summary>
        private void CloseForm()
        {
            WQ_AppStatic.IS_SHOW_FORM_CODE = false;
            if (m_oDBManager != null)
            {
                m_oDBManager.Close();
            }
            this.Dispose();
            this.Close();
        }
        
        #endregion
    }
}
