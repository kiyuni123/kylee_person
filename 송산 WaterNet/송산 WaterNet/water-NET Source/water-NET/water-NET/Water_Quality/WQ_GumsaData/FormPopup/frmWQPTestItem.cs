﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

#endregion

namespace WaterNet.WQ_GumsaData.FormPopup
{
    public partial class frmWQPTestItem : Form
    {
        /// <summary>
        /// Project ID : WN_WQ_A01
        /// Project Explain : 수질검사자료관리
        /// Project Developer : 오두석
        /// Project Create Date : 2010.09.17
        /// Form Explain : 수질시험 신규/상세 Popup Form
        /// </summary>
        private OracleDBManager m_oDBManager = null;

        string _Title = string.Empty;
        string _Establishment = string.Empty;
        string _WaterCollectDate = string.Empty;
        string _MeasurePeriod = string.Empty;

        /// <summary>
        /// 수질관리 Sub Tab들의 Title
        /// </summary>
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                _Title = value;
            }
        }

        /// <summary>
        /// 사업장
        /// </summary>
        public string Establishment
        {
            get
            {
                return _Establishment;
            }
            set
            {
                _Establishment = value;
            }
        }

        /// <summary>
        /// 채수일자
        /// </summary>
        public string WaterCollectDate
        {
            get
            {
                return _WaterCollectDate;
            }
            set
            {
                _WaterCollectDate = value;
            }
        }

        /// <summary>
        /// 측정주기
        /// </summary>
        public string MeasurePeriod
        {
            get
            {
                return _MeasurePeriod;
            }
            set
            {
                _MeasurePeriod = value;
            }
        }

        public frmWQPTestItem()
        {
            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region - 그리드 설정
            
            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMNM";
            oUltraGridColumn.Header.Caption = "항목명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TLIMIT";
            oUltraGridColumn.Header.Caption = "기준치";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            oUltraGridColumn.CellButtonAppearance.Image = WQ_GumsaData.Properties.Resources.Find;
            oUltraGridColumn.CellButtonAppearance.ImageHAlign = HAlign.Center;
            oUltraGridColumn.CellButtonAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE1";
            oUltraGridColumn.Header.Caption = "상수원수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE2";
            oUltraGridColumn.Header.Caption = "원수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE3";
            oUltraGridColumn.Header.Caption = "혼화수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE4";
            oUltraGridColumn.Header.Caption = "침전수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE5";
            oUltraGridColumn.Header.Caption = "여과수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE6";
            oUltraGridColumn.Header.Caption = "오존처리수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE7";
            oUltraGridColumn.Header.Caption = "활성탄처리수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE8";
            oUltraGridColumn.Header.Caption = "정수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE9";
            oUltraGridColumn.Header.Caption = "수도꼭지";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE10";
            oUltraGridColumn.Header.Caption = "급수과정시설";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE11";
            oUltraGridColumn.Header.Caption = "민원수질";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SVALUE12";
            oUltraGridColumn.Header.Caption = "수돗물품질확인";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPopup);

            uGridPopup.DisplayLayout.Bands[0].Columns[1].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
                     
            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);
            FormManager.SetGridStyle_ColumeAllowEdit(this.uGridPopup, 2, uGridPopup.DisplayLayout.Bands[0].Columns.Count - 1);

            //ActiionKeyMapping();
            #endregion          
        }

        private void frmWQPTestItem_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================수질관리(신규)

            object o = EMFrame.statics.AppStatic.USER_MENU["수질검사자료관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }

            //===========================================================================

            // frmWQMain 에서 받은 값을을 Set
            this.Text = _Title;
            this.cbEstablishment.Text = _Establishment;
            this.udtWaterCollectDate.Text = _WaterCollectDate;
            this.cbEstablishment.Enabled = false;

            if (this.Text.Substring(this.Text.Length - 2) == "신규")
            {
                this.udtWaterCollectDate.Enabled = true;
                this.GetWQNewTestData();
            }
            else
            {
                this.udtWaterCollectDate.Enabled = false;
                this.GetWQDetailTestData();
            }
            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.Text.Substring(this.Text.Length - 2) == "신규")
                {
                    this.GetWQNewTestData();
                }
                else
                {
                    this.GetWQDetailTestData();
                }
                FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Text.Substring(this.Text.Length - 2) == "신규")
            {
                this.SaveWQNewTestData();
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.ClosePopup();
            }
            else
            {
                this.SaveWQDetailTestData();
                this.GetWQDetailTestData();
                FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string strTitle = string.Empty;

                UltraGrid oUGrid = new UltraGrid();

                strTitle = _Title;
                oUGrid = this.uGridPopup;

                FormManager.ExportToExcelFromUltraGrid(oUGrid, strTitle);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void uGridPopup_ClickCellButton(object sender, CellEventArgs e)
        {
            this.OpenToTIBasicValuePopup();
        }

        #endregion


        #region User Function

        /// <summary>
        /// 신규 : 수질시험항목,공정,수치 Data를 취득해서 그리드에 Set한다
        /// </summary>
        private void GetWQNewTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strSITECD = string.Empty;
            string strTITCD = string.Empty;
            int iCWPRCCD = 0;

            int i, j, k = 0;

            DataSet pDS = new DataSet();
            DataSet pDSTmp = new DataSet();

            strSITECD = WQ_Function.SplitToCode(this.cbEstablishment.Text.ToString());

            //수질시험항목 Select
            oStringBuilder.AppendLine("SELECT   DISTINCT(A.TITMNM || ' (' || A.TITCD || ')') AS ITEMNM, '' AS TLIMIT");
            oStringBuilder.AppendLine("FROM     WQCTIT A ");
            oStringBuilder.AppendLine("         RIGHT OUTER JOIN WQCPPRTIT B ON B.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND B.SITECD = '" + strSITECD + "' AND B.SRPRID = '" + _MeasurePeriod + "'");
            oStringBuilder.AppendLine("WHERE    UPPER(A.USEYN) = '1'");
            oStringBuilder.AppendLine("ORDER BY ITEMNM");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            for (int x = 0; x < pDS.Tables["WQCTIT"].Rows.Count; x++)
            {
                Array arrTemp = pDS.Tables["WQCTIT"].Rows[x].ItemArray;

                if (arrTemp.GetValue(0).ToString() == " ()")
                {
                    pDS.Tables["WQCTIT"].Rows[x].Delete();
                }
            }

            this.uGridPopup.DataSource = pDS;

            //시험항목이 허용된 공정을 찾아 Loop를 돌면서 허용되지 않은 공정의 Cell을 Disable시킨다.
            for (i = 0; i < this.uGridPopup.Rows.Count; i++)
            {
                strTITCD = WQ_Function.SplitToCode(this.uGridPopup.Rows[i].Cells[0].Text.Trim());

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   CWPRCCD");
                oStringBuilder.AppendLine("FROM     WQCPPRTIT");
                oStringBuilder.AppendLine("WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND SRPRID = '" + _MeasurePeriod + "' AND TITCD = '" + strTITCD + "'");
                oStringBuilder.AppendLine("ORDER BY CWPRCCD");

                pDSTmp = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCPPRTIT");

                if ((pDSTmp.Tables.Count > 0) && (pDSTmp.Tables[0].Rows.Count > 0))
                {
                    foreach (DataRow oDRow in pDSTmp.Tables[0].Rows)
                    {
                        iCWPRCCD = Convert.ToInt32(oDRow["CWPRCCD"].ToString());

                        for (j = 2; j < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            k = j - 1;
                            if (iCWPRCCD == k)
                            {
                                this.uGridPopup.Rows[i].Cells[j].Value = "";
                                FormManager.SetGridStyle_CellAllowEdit(this.uGridPopup, i, j);
                            }
                            else
                            {
                                if (this.uGridPopup.Rows[i].Cells[j].Value == null)
                                {
                                    this.uGridPopup.Rows[i].Cells[j].Value = "X";
                                    FormManager.SetGridStyle_CellDisable(this.uGridPopup, i, j);
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int x = 2; x < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; x++)
                    {
                        this.uGridPopup.Rows[i].Cells[x].Value = "X";
                        FormManager.SetGridStyle_CellDisable(this.uGridPopup, i, x);
                    }
                }
            }

            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 상세 : 수질시험항목,공정,수치 Data를 취득해서 그리드에 Set한다
        /// </summary>
        private void GetWQDetailTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strSITECD = string.Empty;
            string strTITCD = string.Empty;
            string strCLTDT = string.Empty;
            string strTABLE = string.Empty;

            int iCWPRCCD = 0;

            int i, j, k = 0;

            DataSet pDS = new DataSet();
            DataSet pDSTmp = new DataSet();

            strSITECD = WQ_Function.SplitToCode(this.cbEstablishment.Text.ToString());
            strCLTDT = WQ_Function.StringToDateTime(this.udtWaterCollectDate.DateTime);

            //수질시험항목 Select
            oStringBuilder.AppendLine("SELECT   DISTINCT(A.TITMNM || ' (' || A.TITCD || ')') AS ITEMNM, '' AS TLIMIT");
            oStringBuilder.AppendLine("FROM     WQCTIT A ");
            oStringBuilder.AppendLine("         RIGHT OUTER JOIN WQCPPRTIT B ON B.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND B.SITECD = '" + strSITECD + "' AND SRPRID = '" + _MeasurePeriod + "'");
            oStringBuilder.AppendLine("ORDER BY ITEMNM");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            for (int x = 0; x < pDS.Tables["WQCTIT"].Rows.Count; x++)
            {
                Array arrTemp = pDS.Tables["WQCTIT"].Rows[x].ItemArray;

                if (arrTemp.GetValue(0).ToString() == " ()")
                {
                    pDS.Tables["WQCTIT"].Rows[x].Delete();
                }
            }
            
            this.uGridPopup.DataSource = pDS;

            if (_MeasurePeriod == "1")
            {
                strTABLE = "WQCPDWQRL";
            }
            else
            {
                strTABLE = "WQCPWQRL";
            }
            //시험항목이 허용된 공정을 찾아 Loop를 돌면서 허용되지 않은 공정의 Cell을 Disable시킨다.
            for (i = 0; i < this.uGridPopup.Rows.Count; i++)
            {
                strTITCD = WQ_Function.SplitToCode(this.uGridPopup.Rows[i].Cells[0].Text.ToString());

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   SVAL, CWPRCCD");
                oStringBuilder.AppendLine("FROM     " + strTABLE);
                oStringBuilder.AppendLine("WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND TITCD = '" + strTITCD + "' AND CLTDT = '" + strCLTDT + "'");
                oStringBuilder.AppendLine("ORDER BY CWPRCCD");

                pDSTmp = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCPDWQRL");

                if ((pDSTmp.Tables.Count > 0) && (pDSTmp.Tables[0].Rows.Count > 0))
                {
                    foreach (DataRow oDRow in pDSTmp.Tables[0].Rows)
                    {
                        iCWPRCCD = Convert.ToInt32(oDRow["CWPRCCD"].ToString());

                        for (j = 2; j < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            k = j - 1;
                            if (iCWPRCCD == k)
                            {
                                this.uGridPopup.Rows[i].Cells[j].Value = oDRow["SVAL"].ToString();
                                FormManager.SetGridStyle_CellAllowEdit(this.uGridPopup, i, j);
                            }
                            else
                            {
                                if (this.uGridPopup.Rows[i].Cells[j].Value == null)
                                {
                                    this.uGridPopup.Rows[i].Cells[j].Value = "X";
                                    FormManager.SetGridStyle_CellDisable(this.uGridPopup, i, j);
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int x = 2; x < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; x++)
                    {
                        this.uGridPopup.Rows[i].Cells[x].Value = "X";
                        FormManager.SetGridStyle_CellDisable(this.uGridPopup, i, x);
                    }
                }
            }

            this.txtRemark.Text = WQ_Function.GetRemark(_MeasurePeriod, "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "'");

            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 신규 수질 Test Data를 Database에 Insert 한다.
        /// </summary>
        private void SaveWQNewTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;
            string strCWPRCCD = string.Empty;
            string strTITCD = string.Empty;
            string strSVAL = string.Empty;
            string strSERNO = string.Empty;

            DataSet pDS = new DataSet();

            strSITECD = WQ_Function.SplitToCode(_Establishment);
            strCLTDT = WQ_Function.StringToDateTime(this.udtWaterCollectDate.DateTime);

            switch (_MeasurePeriod)
            {
                case "1": //일일
                    
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE   WQCDWQSMR");
                    oStringBuilder.AppendLine("WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "'");

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE   WQCPDWQRL");
                    oStringBuilder.AppendLine("WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "'");

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + strCLTDT + "', '', '" + this.txtRemark.Text.Trim() + "', '', '', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE, '', '')";
                    
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("INSERT INTO WQCDWQSMR");
                    oStringBuilder.AppendLine("(SGCCD, SITECD, CLTDT, WTH, RMK, GYUL, REQNUM, REGMNGR, REGMNGRIP, REGDT, LAB, HDPGSUID)");
                    oStringBuilder.AppendLine("VALUES");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    for (int i = 0; i < this.uGridPopup.Rows.Count; i++)
                    {
                        for (int j = 2; j < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            strCWPRCCD = WQ_Function.FillZero(Convert.ToString(j - 1), 2, 1);
                            if (this.uGridPopup.Rows[i].Cells[j].Activation == Activation.AllowEdit)
                            {
                                strTITCD = WQ_Function.SplitToCode(this.uGridPopup.Rows[i].Cells[0].Text.ToString());
                                strSVAL = this.uGridPopup.Rows[i].Cells[j].Text.ToString();
                                strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + strCLTDT + "', '" + strCWPRCCD + "', '" + strTITCD + "', '" + strSVAL + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE)";

                                oStringBuilder.Remove(0, oStringBuilder.Length);
                                oStringBuilder.AppendLine("INSERT INTO WQCPDWQRL");
                                oStringBuilder.AppendLine("(SGCCD, SITECD, CLTDT, CWPRCCD, TITCD, SVAL, REGMNGR, REGMNGRIP, REGDT)");
                                oStringBuilder.AppendLine("VALUES");
                                oStringBuilder.AppendLine(strParam);

                                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                            }
                        }
                    }

                    break;

                case "2": //주간
                case "3": //월간
                case "4": //분기
                case "5": //반기
                    strSERNO = WQ_Function.GenWQGumsaSequenceNo(strCLTDT, 1);

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE   WQCPWQSMR");
                    oStringBuilder.AppendLine("WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND SPRPID = '" + _MeasurePeriod + "'");

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("DELETE   WQCPWQRL");
                    oStringBuilder.AppendLine("WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND SPRPID = '" + _MeasurePeriod + "'");

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + _MeasurePeriod + "', '" + strCLTDT + "', '" + strSERNO + "', '" + this.txtRemark.Text.Trim() + "', '', '', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE, '', '')";

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("INSERT INTO WQCPWQSMR");
                    oStringBuilder.AppendLine("(SGCCD, SITECD, SPRPID, CLTDT, SERNO, RMK, GYUL, REQNUM, REGMNGR, REGMNGRIP, REGDT, LAB, HDPGSUID)");
                    oStringBuilder.AppendLine("VALUES");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    for (int i = 0; i < this.uGridPopup.Rows.Count; i++)
                    {
                        for (int j = 2; j < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            strCWPRCCD = WQ_Function.FillZero(Convert.ToString(j - 1), 2, 1);
                            if (this.uGridPopup.Rows[i].Cells[j].Activation == Activation.AllowEdit)
                            {
                                strTITCD = WQ_Function.SplitToCode(this.uGridPopup.Rows[i].Cells[0].Text.ToString());
                                strSVAL = this.uGridPopup.Rows[i].Cells[j].Text.ToString();
                                strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + _MeasurePeriod + "', '" + strCLTDT + "', '" + strCWPRCCD + "', '" + strTITCD + "', '" + strSERNO + "', '" + strSVAL + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE)";
                                
                                oStringBuilder.Remove(0, oStringBuilder.Length);
                                oStringBuilder.AppendLine("INSERT INTO WQCPWQRL");
                                oStringBuilder.AppendLine("(SGCCD, SITECD, SPRPID, CLTDT, CWPRCCD, TITCD, SERNO, SVAL, REGMNGR, REGMNGRIP, REGDT)");
                                oStringBuilder.AppendLine("VALUES");
                                oStringBuilder.AppendLine(strParam);

                                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                            }
                        }
                    }
                    
                    break;

            }

            this.GetWQNewTestData();
        }

        /// <summary>
        /// 상세 수질 Test Data를 Database에 Update 한다.
        /// </summary>
        private void SaveWQDetailTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;
            string strCWPRCCD = string.Empty;
            string strTITCD = string.Empty;
            string strSVAL = string.Empty;
            string strSERNO = string.Empty;

            DataSet pDS = new DataSet();

            strSITECD = WQ_Function.SplitToCode(_Establishment);
            strCLTDT = WQ_Function.StringToDateTime(this.udtWaterCollectDate.DateTime);

            switch (_MeasurePeriod)
            {
                case "1": //일일

                    strParam = "WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "'";

                    oStringBuilder.AppendLine("UPDATE   WQCDWQSMR");
                    oStringBuilder.AppendLine("SET      RMK = '" + this.txtRemark.Text.Trim() + "', REGMNGR = '" + EMFrame.statics.AppStatic.USER_ID + "', REGMNGRIP = '" + EMFrame.statics.AppStatic.USER_IP + "', REGDT = SYSDATE");
                    oStringBuilder.AppendLine(strParam);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    for (int i = 0; i < this.uGridPopup.Rows.Count; i++)
                    {
                        for (int j = 2; j < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            strCWPRCCD = WQ_Function.FillZero(Convert.ToString(j - 1), 2, 1);
                            if (this.uGridPopup.Rows[i].Cells[j].Activation == Activation.AllowEdit)
                            {
                                strTITCD = WQ_Function.SplitToCode(this.uGridPopup.Rows[i].Cells[0].Text.ToString());
                                strSVAL = this.uGridPopup.Rows[i].Cells[j].Text.ToString();

                                strParam = "WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND CWPRCCD = '" + strCWPRCCD + "' AND TITCD = '" + strTITCD + "'";
                                oStringBuilder.Remove(0, oStringBuilder.Length);
                                oStringBuilder.AppendLine("DELETE   WQCPDWQRL");
                                oStringBuilder.AppendLine(strParam);
                                
                                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                                strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + strCLTDT + "', '" + strCWPRCCD + "', '" + strTITCD + "', '" + strSVAL + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE)";
                                oStringBuilder.Remove(0, oStringBuilder.Length);
                                oStringBuilder.AppendLine("INSERT INTO WQCPDWQRL");
                                oStringBuilder.AppendLine("(SGCCD, SITECD, CLTDT, CWPRCCD, TITCD, SVAL, REGMNGR, REGMNGRIP, REGDT)");
                                oStringBuilder.AppendLine("VALUES");
                                oStringBuilder.AppendLine(strParam);

                                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                            }
                        }
                    }

                    break;

                case "2": //주간
                case "3": //월간
                case "4": //분기
                case "5": //반기
                    strParam = "WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND SPRPID = '" + _MeasurePeriod + "'";

                    oStringBuilder.AppendLine("UPDATE   WQCPWQSMR");
                    oStringBuilder.AppendLine("SET      RMK = '" + this.txtRemark.Text.Trim() + "', REGMNGR = '" + EMFrame.statics.AppStatic.USER_ID + "', REGMNGRIP = '" + EMFrame.statics.AppStatic.USER_IP + "', REGDT = SYSDATE");
                    oStringBuilder.AppendLine(strParam);

                    strSERNO = WQ_Function.GetWQGumsaSequenceNo(strParam, 1);

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                    for (int i = 0; i < this.uGridPopup.Rows.Count; i++)
                    {
                        for (int j = 2; j < this.uGridPopup.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            strCWPRCCD = WQ_Function.FillZero(Convert.ToString(j - 1), 2, 1);
                            if (this.uGridPopup.Rows[i].Cells[j].Activation == Activation.AllowEdit)
                            {
                                strTITCD = WQ_Function.SplitToCode(this.uGridPopup.Rows[i].Cells[0].Text.ToString());
                                strSVAL = this.uGridPopup.Rows[i].Cells[j].Text.ToString();

                                strParam = "WHERE SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND CWPRCCD = '" + strCWPRCCD + "' AND TITCD = '" + strTITCD + "' AND SERNO = '" + strSERNO + "' AND SPRPID = '" + _MeasurePeriod + "'";
                                

                                oStringBuilder.Remove(0, oStringBuilder.Length);
                                oStringBuilder.AppendLine("DELETE   WQCPWQRL");
                                oStringBuilder.AppendLine(strParam);

                                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                                strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strSITECD + "', '" + _MeasurePeriod + "', '" + strCLTDT + "', '" + strCWPRCCD + "', '" + strTITCD + "', '" + strSERNO + "', '" + strSVAL + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', SYSDATE)";

                                oStringBuilder.Remove(0, oStringBuilder.Length);
                                oStringBuilder.AppendLine("INSERT INTO WQCPWQRL");
                                oStringBuilder.AppendLine("(SGCCD, SITECD, SPRPID, CLTDT, CWPRCCD, TITCD, SERNO, SVAL, REGMNGR, REGMNGRIP, REGDT)");
                                oStringBuilder.AppendLine("VALUES");
                                oStringBuilder.AppendLine(strParam);

                                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                            }
                        }
                    }
                    break;
            }

            this.GetWQDetailTestData();
        }

        /// <summary>
        /// 특정 값을 부모 Form으로 Retun할 수 있게 하고, 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            _Establishment = this.cbEstablishment.Text.ToString();
            _WaterCollectDate = this.udtWaterCollectDate.Text.ToString();

            this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        /// <summary>
        /// Open to 시험항목기준치 Popup
        /// </summary>
        private void OpenToTIBasicValuePopup()
        {
            WaterNet.WQ_GumsaData.FormPopup.frmWQPTIBasicValue oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQPTIBasicValue();
            oForm.TIName = this.uGridPopup.ActiveRow.Cells[0].Text.ToString();
            oForm.ShowDialog();
        }

        #endregion
    }
}
