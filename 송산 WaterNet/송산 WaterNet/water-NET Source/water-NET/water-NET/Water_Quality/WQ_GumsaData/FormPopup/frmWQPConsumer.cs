﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

#endregion

namespace WaterNet.WQ_GumsaData.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A01
    /// Project Explain : 수질검사자료관리
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.15
    /// Form Explain : 수용가조회 Popup Form
    /// </summary>
    public partial class frmWQPConsumer : Form
    {
        private OracleDBManager m_oDBManager = null;

        string _Consumer = string.Empty;

        /// <summary>
        /// 수용가명
        /// </summary>
        public string Consumer
        {
            get
            {
                return _Consumer;
            }
            set
            {
                _Consumer = value;
            }
        }

        public frmWQPConsumer()
        {
            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region - 그리드 설정

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNM";
            oUltraGridColumn.Header.Caption = "수용가명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TRADENM";
            oUltraGridColumn.Header.Caption = "상호명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HYDRNTNO";
            oUltraGridColumn.Header.Caption = "수전번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WSREPBIZKNDCD";
            oUltraGridColumn.Header.Caption = "상수도업종";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            //oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "NRDADDR";
            //oUltraGridColumn.Header.Caption = "새길주소";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Width = 120;
            //oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMTEL";
            oUltraGridColumn.Header.Caption = "전화번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HYDRNTSTAT";
            oUltraGridColumn.Header.Caption = "상수도 급수상태";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WSPIPESZCD";
            oUltraGridColumn.Header.Caption = "상수도 구경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPopup);
            //특정 Colume을 Edit 가능하게 셋팅
            //이유 : 선택된 Cell의 데이터를 Copy해서 frmWQMain에 Paste할 수 있게 하기 위함.
            //       단, Control + c 허용
            WaterNetCore.FormManager.SetGridStyle_ColumeAllowEdit(uGridPopup, 0);
            WaterNetCore.FormManager.SetGridStyle_ColumeAllowEdit(uGridPopup, 1);
            WaterNetCore.FormManager.SetGridStyle_ColumeAllowEdit(uGridPopup, 5);
            WaterNetCore.FormManager.SetGridStyle_ColumeAllowEdit(uGridPopup, 6);

            //ActiionKeyMapping();
            #endregion          
        }

        private void frmWQPConsumer_Load(object sender, EventArgs e)
        {
            // frmWQMain 에서 받은 수용가명을 Set
            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            WQ_Function.SetCombo_SupplyStatus(this.cbSupplyStatus);
            this.GetQueryData();
        }


        #region Button Events
        
        private void btnInit_Click(object sender, EventArgs e)
        {
            this.InitControl();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetQueryData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.txtConsumerName.Text = "";
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void uGridPopup_AfterRowActivate(object sender, EventArgs e)
        {
            this.txtConsumerName.Text = this.uGridPopup.ActiveRow.Cells[1].Text;
            this.txtConsumerNo.Text = this.uGridPopup.ActiveRow.Cells[0].Text;
            this.txtPaddyField.Text = this.uGridPopup.ActiveRow.Cells[3].Text;

            WQ_Function.SetCombo_SelectFindData(this.cbSupplyStatus, this.uGridPopup.ActiveRow.Cells[8].Text);

            this.txtAddress.Text = this.uGridPopup.ActiveRow.Cells[5].Text;
        }

        private void uGridPopup_DoubleClick(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        /// <summary>
        /// Control + C만 허용
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridPopup_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Control + C만 허용
            if ((e.KeyChar != 3))
            {
                e.Handled = true;
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetQueryData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            //oStringBuilder.AppendLine("SELECT   A.DMNO, A.DMNM, A.TRADENM, A.HYDRNTNO, B.WSREPBIZKNDCD, A.DMADDR, A.NRDADDR, A.DMTEL, B.HYDRNTSTAT, B.WSPIPESZCD");
            oStringBuilder.AppendLine("SELECT   A.DMNO, A.DMNM, A.TRADENM, A.HYDRNTNO, B.WSREPBIZKNDCD, A.DMADDR, '' AS DMTEL, B.HYDRNTSTAT, B.WSPIPESZCD");
            oStringBuilder.AppendLine("FROM     WI_DMINFO A, WI_DMWSRSRC B");
            oStringBuilder.AppendLine("WHERE    B.DMNO = A.DMNO AND A.DMNM LIKE '%" + WQ_Function.SplitToCodeName(this.txtConsumerName.Text) + "%'");
            oStringBuilder.AppendLine("         AND A.DMNO LIKE '%" + this.txtConsumerNo.Text.ToString().Trim() + "%' AND A.HYDRNTNO LIKE '%" + this.txtPaddyField.Text.Trim() + "%'");
            oStringBuilder.AppendLine("         AND A.DMADDR LIKE '%" + this.txtAddress.Text.Trim() + "%' AND B.HYDRNTSTAT = '" + this.cbSupplyStatus.Text.ToString() + "'");
            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_DMINFO");

            uGridPopup.DataSource = pDS.Tables["WI_DMINFO"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            if (this.uGridPopup.Rows.Count > 0) this.uGridPopup.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 특정 값을 부모 Form으로 Retun할 수 있게 하고, 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            _Consumer = this.txtConsumerNo.Text.Trim();
            this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        /// <summary>
        /// 검색조건 컨트롤들을 모두 클리어
        /// </summary>
        private void InitControl()
        {
            this.txtConsumerName.Text = string.Empty;
            this.txtAddress.Text = string.Empty;
            this.txtPaddyField.Text = string.Empty;
            this.txtConsumerNo.Text = string.Empty;
        }

        #endregion
    }
}
