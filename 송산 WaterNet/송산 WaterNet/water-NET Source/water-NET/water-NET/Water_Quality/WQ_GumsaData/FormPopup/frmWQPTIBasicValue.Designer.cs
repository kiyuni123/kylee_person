﻿namespace WaterNet.WQ_GumsaData.FormPopup
{
    partial class frmWQPTIBasicValue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTITMNM = new System.Windows.Forms.TextBox();
            this.txtHMNTSVAL = new System.Windows.Forms.TextBox();
            this.txtLMNTSVAL = new System.Windows.Forms.TextBox();
            this.txtLTRSVAL = new System.Windows.Forms.TextBox();
            this.txtVLDFGR = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(30, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "항목명 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(4, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "상한기준값 :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(4, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "하한기준값 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(4, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "문자기준값 :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(17, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "유효자리 :";
            // 
            // txtTITMNM
            // 
            this.txtTITMNM.BackColor = System.Drawing.SystemColors.Control;
            this.txtTITMNM.Location = new System.Drawing.Point(88, 2);
            this.txtTITMNM.Name = "txtTITMNM";
            this.txtTITMNM.ReadOnly = true;
            this.txtTITMNM.Size = new System.Drawing.Size(150, 21);
            this.txtTITMNM.TabIndex = 5;
            this.txtTITMNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtHMNTSVAL
            // 
            this.txtHMNTSVAL.BackColor = System.Drawing.SystemColors.Control;
            this.txtHMNTSVAL.Location = new System.Drawing.Point(88, 25);
            this.txtHMNTSVAL.Name = "txtHMNTSVAL";
            this.txtHMNTSVAL.ReadOnly = true;
            this.txtHMNTSVAL.Size = new System.Drawing.Size(150, 21);
            this.txtHMNTSVAL.TabIndex = 6;
            this.txtHMNTSVAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLMNTSVAL
            // 
            this.txtLMNTSVAL.BackColor = System.Drawing.SystemColors.Control;
            this.txtLMNTSVAL.Location = new System.Drawing.Point(88, 48);
            this.txtLMNTSVAL.Name = "txtLMNTSVAL";
            this.txtLMNTSVAL.ReadOnly = true;
            this.txtLMNTSVAL.Size = new System.Drawing.Size(150, 21);
            this.txtLMNTSVAL.TabIndex = 7;
            this.txtLMNTSVAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLTRSVAL
            // 
            this.txtLTRSVAL.BackColor = System.Drawing.SystemColors.Control;
            this.txtLTRSVAL.Location = new System.Drawing.Point(88, 71);
            this.txtLTRSVAL.Name = "txtLTRSVAL";
            this.txtLTRSVAL.ReadOnly = true;
            this.txtLTRSVAL.Size = new System.Drawing.Size(150, 21);
            this.txtLTRSVAL.TabIndex = 8;
            this.txtLTRSVAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVLDFGR
            // 
            this.txtVLDFGR.BackColor = System.Drawing.SystemColors.Control;
            this.txtVLDFGR.Location = new System.Drawing.Point(88, 94);
            this.txtVLDFGR.Name = "txtVLDFGR";
            this.txtVLDFGR.ReadOnly = true;
            this.txtVLDFGR.Size = new System.Drawing.Size(150, 21);
            this.txtVLDFGR.TabIndex = 9;
            this.txtVLDFGR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtVLDFGR);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtLTRSVAL);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtLMNTSVAL);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtHMNTSVAL);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtTITMNM);
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(243, 119);
            this.panel1.TabIndex = 10;
            // 
            // frmWQPTIBasicValue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(247, 123);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWQPTIBasicValue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "수질 시험항목 기준치";
            this.Load += new System.EventHandler(this.frmWQPTIBasicValue_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTITMNM;
        private System.Windows.Forms.TextBox txtHMNTSVAL;
        private System.Windows.Forms.TextBox txtLMNTSVAL;
        private System.Windows.Forms.TextBox txtLTRSVAL;
        private System.Windows.Forms.TextBox txtVLDFGR;
        private System.Windows.Forms.Panel panel1;
    }
}