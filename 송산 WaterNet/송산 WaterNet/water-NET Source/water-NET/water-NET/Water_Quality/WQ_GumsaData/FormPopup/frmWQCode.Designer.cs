﻿namespace WaterNet.WQ_GumsaData.FormPopup
{
    partial class frmWQCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab11 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab12 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.uTabPageM1S1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel4 = new System.Windows.Forms.Panel();
            this.uGrid11_2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnExcel1 = new System.Windows.Forms.Button();
            this.btnSave2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.picFrCleftS11 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.uGrid11_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAppend1 = new System.Windows.Forms.Button();
            this.btnSave1 = new System.Windows.Forms.Button();
            this.btnDelete1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnQuery1 = new System.Windows.Forms.Button();
            this.btnClose1 = new System.Windows.Forms.Button();
            this.picFrRightS11 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS11 = new System.Windows.Forms.PictureBox();
            this.picFrBottomS11 = new System.Windows.Forms.PictureBox();
            this.picFrTopS11 = new System.Windows.Forms.PictureBox();
            this.uTabPageM1S2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.uGrid12_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnAppend2 = new System.Windows.Forms.Button();
            this.btnExcel2 = new System.Windows.Forms.Button();
            this.btnSave3 = new System.Windows.Forms.Button();
            this.btnDelete2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnQuery2 = new System.Windows.Forms.Button();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnClose2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cbCategoryName = new System.Windows.Forms.ComboBox();
            this.picFrRightS12 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS12 = new System.Windows.Forms.PictureBox();
            this.picFrBottomS12 = new System.Windows.Forms.PictureBox();
            this.picFrTopS12 = new System.Windows.Forms.PictureBox();
            this.uTabPageM1S3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel9 = new System.Windows.Forms.Panel();
            this.uGrid13_2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnExcel3 = new System.Windows.Forms.Button();
            this.btnSave4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.uGrid13_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.cbMeasurePeriod = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbEstablishment1 = new System.Windows.Forms.ComboBox();
            this.btnQuery3 = new System.Windows.Forms.Button();
            this.btnClose3 = new System.Windows.Forms.Button();
            this.picFrRightS13 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS13 = new System.Windows.Forms.PictureBox();
            this.picFrBottomS13 = new System.Windows.Forms.PictureBox();
            this.picFrTopS13 = new System.Windows.Forms.PictureBox();
            this.picFrRightM1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.picFrBottomM1 = new System.Windows.Forms.PictureBox();
            this.picFrTopM1 = new System.Windows.Forms.PictureBox();
            this.uTabMenuS1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTabPageM1S1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrCleftS11)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).BeginInit();
            this.uTabPageM1S2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS12)).BeginInit();
            this.uTabPageM1S3.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid13_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid13_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenuS1)).BeginInit();
            this.uTabMenuS1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uTabPageM1S1
            // 
            this.uTabPageM1S1.Controls.Add(this.panel4);
            this.uTabPageM1S1.Controls.Add(this.picFrCleftS11);
            this.uTabPageM1S1.Controls.Add(this.panel3);
            this.uTabPageM1S1.Controls.Add(this.panel1);
            this.uTabPageM1S1.Controls.Add(this.picFrRightS11);
            this.uTabPageM1S1.Controls.Add(this.picFrLeftS11);
            this.uTabPageM1S1.Controls.Add(this.picFrBottomS11);
            this.uTabPageM1S1.Controls.Add(this.picFrTopS11);
            this.uTabPageM1S1.Location = new System.Drawing.Point(1, 23);
            this.uTabPageM1S1.Name = "uTabPageM1S1";
            this.uTabPageM1S1.Size = new System.Drawing.Size(841, 479);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.uGrid11_2);
            this.panel4.Controls.Add(this.pictureBox33);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.pictureBox31);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(368, 32);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(469, 443);
            this.panel4.TabIndex = 108;
            // 
            // uGrid11_2
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid11_2.DisplayLayout.Appearance = appearance37;
            this.uGrid11_2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid11_2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid11_2.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid11_2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.uGrid11_2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid11_2.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.uGrid11_2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid11_2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid11_2.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid11_2.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.uGrid11_2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid11_2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid11_2.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid11_2.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGrid11_2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid11_2.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid11_2.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.uGrid11_2.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.uGrid11_2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid11_2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.uGrid11_2.DisplayLayout.Override.RowAppearance = appearance47;
            this.uGrid11_2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid11_2.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.uGrid11_2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid11_2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid11_2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid11_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid11_2.Location = new System.Drawing.Point(0, 36);
            this.uGrid11_2.Name = "uGrid11_2";
            this.uGrid11_2.Size = new System.Drawing.Size(469, 407);
            this.uGrid11_2.TabIndex = 4;
            this.uGrid11_2.Text = "ultraGrid1";
            this.uGrid11_2.Click += new System.EventHandler(this.uGrid11_2_Click);
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.Color.Gold;
            this.pictureBox33.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox33.Location = new System.Drawing.Point(0, 32);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(469, 4);
            this.pictureBox33.TabIndex = 104;
            this.pictureBox33.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnExcel1);
            this.panel6.Controls.Add(this.btnSave2);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(469, 28);
            this.panel6.TabIndex = 1;
            // 
            // btnExcel1
            // 
            this.btnExcel1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcel1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel1.Location = new System.Drawing.Point(327, 0);
            this.btnExcel1.Name = "btnExcel1";
            this.btnExcel1.Size = new System.Drawing.Size(70, 26);
            this.btnExcel1.TabIndex = 8;
            this.btnExcel1.Text = "엑셀";
            this.btnExcel1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel1.UseVisualStyleBackColor = true;
            this.btnExcel1.Click += new System.EventHandler(this.btnExcel1_Click);
            // 
            // btnSave2
            // 
            this.btnSave2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSave2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Save;
            this.btnSave2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave2.Location = new System.Drawing.Point(397, 0);
            this.btnSave2.Name = "btnSave2";
            this.btnSave2.Size = new System.Drawing.Size(70, 26);
            this.btnSave2.TabIndex = 7;
            this.btnSave2.Text = "저장";
            this.btnSave2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave2.UseVisualStyleBackColor = true;
            this.btnSave2.Click += new System.EventHandler(this.btnSave2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(6, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "사업장공정관리";
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.Gold;
            this.pictureBox31.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox31.Location = new System.Drawing.Point(0, 0);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(469, 4);
            this.pictureBox31.TabIndex = 103;
            this.pictureBox31.TabStop = false;
            // 
            // picFrCleftS11
            // 
            this.picFrCleftS11.BackColor = System.Drawing.Color.Gold;
            this.picFrCleftS11.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrCleftS11.Location = new System.Drawing.Point(364, 32);
            this.picFrCleftS11.Name = "picFrCleftS11";
            this.picFrCleftS11.Size = new System.Drawing.Size(4, 443);
            this.picFrCleftS11.TabIndex = 109;
            this.picFrCleftS11.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.uGrid11_1);
            this.panel3.Controls.Add(this.pictureBox32);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.pictureBox30);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(4, 32);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(360, 443);
            this.panel3.TabIndex = 107;
            // 
            // uGrid11_1
            // 
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid11_1.DisplayLayout.Appearance = appearance49;
            this.uGrid11_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid11_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid11_1.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid11_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.uGrid11_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid11_1.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.uGrid11_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid11_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid11_1.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid11_1.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.uGrid11_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid11_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid11_1.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid11_1.DisplayLayout.Override.CellAppearance = appearance56;
            this.uGrid11_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid11_1.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid11_1.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.uGrid11_1.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.uGrid11_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid11_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.uGrid11_1.DisplayLayout.Override.RowAppearance = appearance59;
            this.uGrid11_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid11_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.uGrid11_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid11_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid11_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid11_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid11_1.Location = new System.Drawing.Point(0, 36);
            this.uGrid11_1.Name = "uGrid11_1";
            this.uGrid11_1.Size = new System.Drawing.Size(360, 407);
            this.uGrid11_1.TabIndex = 2;
            this.uGrid11_1.Text = "ultraGrid1";
            this.uGrid11_1.Click += new System.EventHandler(this.uGrid11_1_Click);
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.Color.Gold;
            this.pictureBox32.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox32.Location = new System.Drawing.Point(0, 32);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(360, 4);
            this.pictureBox32.TabIndex = 104;
            this.pictureBox32.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnAppend1);
            this.panel5.Controls.Add(this.btnSave1);
            this.panel5.Controls.Add(this.btnDelete1);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(360, 28);
            this.panel5.TabIndex = 1;
            // 
            // btnAppend1
            // 
            this.btnAppend1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAppend1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAppend1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.AddRow;
            this.btnAppend1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAppend1.Location = new System.Drawing.Point(148, 0);
            this.btnAppend1.Name = "btnAppend1";
            this.btnAppend1.Size = new System.Drawing.Size(70, 26);
            this.btnAppend1.TabIndex = 6;
            this.btnAppend1.Text = "추가";
            this.btnAppend1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAppend1.UseVisualStyleBackColor = true;
            this.btnAppend1.Click += new System.EventHandler(this.btnAppend1_Click);
            // 
            // btnSave1
            // 
            this.btnSave1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Save;
            this.btnSave1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave1.Location = new System.Drawing.Point(218, 0);
            this.btnSave1.Name = "btnSave1";
            this.btnSave1.Size = new System.Drawing.Size(70, 26);
            this.btnSave1.TabIndex = 5;
            this.btnSave1.Text = "저장";
            this.btnSave1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave1.UseVisualStyleBackColor = true;
            this.btnSave1.Click += new System.EventHandler(this.btnSave1_Click);
            // 
            // btnDelete1
            // 
            this.btnDelete1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDelete1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete1.Location = new System.Drawing.Point(288, 0);
            this.btnDelete1.Name = "btnDelete1";
            this.btnDelete1.Size = new System.Drawing.Size(70, 26);
            this.btnDelete1.TabIndex = 4;
            this.btnDelete1.Text = "삭제";
            this.btnDelete1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete1.UseVisualStyleBackColor = true;
            this.btnDelete1.Click += new System.EventHandler(this.btnDelete1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(6, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "사업장관리";
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.Gold;
            this.pictureBox30.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox30.Location = new System.Drawing.Point(0, 0);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(360, 4);
            this.pictureBox30.TabIndex = 103;
            this.pictureBox30.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnQuery1);
            this.panel1.Controls.Add(this.btnClose1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(833, 28);
            this.panel1.TabIndex = 106;
            // 
            // btnQuery1
            // 
            this.btnQuery1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery1.Location = new System.Drawing.Point(691, 0);
            this.btnQuery1.Name = "btnQuery1";
            this.btnQuery1.Size = new System.Drawing.Size(70, 26);
            this.btnQuery1.TabIndex = 5;
            this.btnQuery1.Text = "조회";
            this.btnQuery1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery1.UseVisualStyleBackColor = true;
            this.btnQuery1.Click += new System.EventHandler(this.btnQuery1_Click);
            // 
            // btnClose1
            // 
            this.btnClose1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose1.Location = new System.Drawing.Point(761, 0);
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.Size = new System.Drawing.Size(70, 26);
            this.btnClose1.TabIndex = 3;
            this.btnClose1.Text = "닫기";
            this.btnClose1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose1.UseVisualStyleBackColor = true;
            this.btnClose1.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // picFrRightS11
            // 
            this.picFrRightS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightS11.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightS11.Location = new System.Drawing.Point(837, 4);
            this.picFrRightS11.Name = "picFrRightS11";
            this.picFrRightS11.Size = new System.Drawing.Size(4, 471);
            this.picFrRightS11.TabIndex = 105;
            this.picFrRightS11.TabStop = false;
            // 
            // picFrLeftS11
            // 
            this.picFrLeftS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS11.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS11.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftS11.Name = "picFrLeftS11";
            this.picFrLeftS11.Size = new System.Drawing.Size(4, 471);
            this.picFrLeftS11.TabIndex = 104;
            this.picFrLeftS11.TabStop = false;
            // 
            // picFrBottomS11
            // 
            this.picFrBottomS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS11.Location = new System.Drawing.Point(0, 475);
            this.picFrBottomS11.Name = "picFrBottomS11";
            this.picFrBottomS11.Size = new System.Drawing.Size(841, 4);
            this.picFrBottomS11.TabIndex = 103;
            this.picFrBottomS11.TabStop = false;
            // 
            // picFrTopS11
            // 
            this.picFrTopS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS11.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS11.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS11.Name = "picFrTopS11";
            this.picFrTopS11.Size = new System.Drawing.Size(841, 4);
            this.picFrTopS11.TabIndex = 102;
            this.picFrTopS11.TabStop = false;
            // 
            // uTabPageM1S2
            // 
            this.uTabPageM1S2.Controls.Add(this.panel2);
            this.uTabPageM1S2.Controls.Add(this.panel8);
            this.uTabPageM1S2.Controls.Add(this.picFrRightS12);
            this.uTabPageM1S2.Controls.Add(this.picFrLeftS12);
            this.uTabPageM1S2.Controls.Add(this.picFrBottomS12);
            this.uTabPageM1S2.Controls.Add(this.picFrTopS12);
            this.uTabPageM1S2.Location = new System.Drawing.Point(-10000, -10000);
            this.uTabPageM1S2.Name = "uTabPageM1S2";
            this.uTabPageM1S2.Size = new System.Drawing.Size(841, 397);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.uGrid12_1);
            this.panel2.Controls.Add(this.pictureBox35);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.pictureBox34);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(833, 361);
            this.panel2.TabIndex = 111;
            // 
            // uGrid12_1
            // 
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid12_1.DisplayLayout.Appearance = appearance61;
            this.uGrid12_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid12_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid12_1.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid12_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.uGrid12_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid12_1.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.uGrid12_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid12_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid12_1.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid12_1.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.uGrid12_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid12_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid12_1.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid12_1.DisplayLayout.Override.CellAppearance = appearance68;
            this.uGrid12_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid12_1.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid12_1.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.uGrid12_1.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.uGrid12_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid12_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.uGrid12_1.DisplayLayout.Override.RowAppearance = appearance71;
            this.uGrid12_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid12_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.uGrid12_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid12_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid12_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid12_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid12_1.Location = new System.Drawing.Point(0, 36);
            this.uGrid12_1.Name = "uGrid12_1";
            this.uGrid12_1.Size = new System.Drawing.Size(833, 325);
            this.uGrid12_1.TabIndex = 4;
            this.uGrid12_1.Text = "ultraGrid1";
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackColor = System.Drawing.Color.Gold;
            this.pictureBox35.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox35.Location = new System.Drawing.Point(0, 32);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(833, 4);
            this.pictureBox35.TabIndex = 108;
            this.pictureBox35.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnAppend2);
            this.panel7.Controls.Add(this.btnExcel2);
            this.panel7.Controls.Add(this.btnSave3);
            this.panel7.Controls.Add(this.btnDelete2);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(833, 28);
            this.panel7.TabIndex = 1;
            // 
            // btnAppend2
            // 
            this.btnAppend2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAppend2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.AddRow;
            this.btnAppend2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAppend2.Location = new System.Drawing.Point(551, 0);
            this.btnAppend2.Name = "btnAppend2";
            this.btnAppend2.Size = new System.Drawing.Size(70, 26);
            this.btnAppend2.TabIndex = 7;
            this.btnAppend2.Text = "추가";
            this.btnAppend2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAppend2.UseVisualStyleBackColor = true;
            this.btnAppend2.Click += new System.EventHandler(this.btnAppend2_Click);
            // 
            // btnExcel2
            // 
            this.btnExcel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExcel2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel2.Location = new System.Drawing.Point(621, 0);
            this.btnExcel2.Name = "btnExcel2";
            this.btnExcel2.Size = new System.Drawing.Size(70, 26);
            this.btnExcel2.TabIndex = 14;
            this.btnExcel2.Text = "엑셀";
            this.btnExcel2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel2.UseVisualStyleBackColor = true;
            this.btnExcel2.Click += new System.EventHandler(this.btnExcel2_Click);
            // 
            // btnSave3
            // 
            this.btnSave3.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave3.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Save;
            this.btnSave3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave3.Location = new System.Drawing.Point(691, 0);
            this.btnSave3.Name = "btnSave3";
            this.btnSave3.Size = new System.Drawing.Size(70, 26);
            this.btnSave3.TabIndex = 13;
            this.btnSave3.Text = "저장";
            this.btnSave3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave3.UseVisualStyleBackColor = true;
            this.btnSave3.Click += new System.EventHandler(this.btnSave3_Click);
            // 
            // btnDelete2
            // 
            this.btnDelete2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDelete2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete2.Location = new System.Drawing.Point(761, 0);
            this.btnDelete2.Name = "btnDelete2";
            this.btnDelete2.Size = new System.Drawing.Size(70, 26);
            this.btnDelete2.TabIndex = 12;
            this.btnDelete2.Text = "삭제";
            this.btnDelete2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete2.UseVisualStyleBackColor = true;
            this.btnDelete2.Click += new System.EventHandler(this.btnDelete2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(6, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "시험항목관리";
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackColor = System.Drawing.Color.Gold;
            this.pictureBox34.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox34.Location = new System.Drawing.Point(0, 0);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(833, 4);
            this.pictureBox34.TabIndex = 107;
            this.pictureBox34.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnQuery2);
            this.panel8.Controls.Add(this.txtItemName);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Controls.Add(this.btnClose2);
            this.panel8.Controls.Add(this.label5);
            this.panel8.Controls.Add(this.cbCategoryName);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(4, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(833, 28);
            this.panel8.TabIndex = 110;
            // 
            // btnQuery2
            // 
            this.btnQuery2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnQuery2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery2.Location = new System.Drawing.Point(691, 0);
            this.btnQuery2.Name = "btnQuery2";
            this.btnQuery2.Size = new System.Drawing.Size(70, 26);
            this.btnQuery2.TabIndex = 12;
            this.btnQuery2.Text = "조회";
            this.btnQuery2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery2.UseVisualStyleBackColor = true;
            this.btnQuery2.Click += new System.EventHandler(this.btnQuery2_Click);
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(385, 3);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(178, 21);
            this.txtItemName.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(325, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "표기명 :";
            // 
            // btnClose2
            // 
            this.btnClose2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose2.Location = new System.Drawing.Point(761, 0);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.Size = new System.Drawing.Size(70, 26);
            this.btnClose2.TabIndex = 3;
            this.btnClose2.Text = "닫기";
            this.btnClose2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose2.UseVisualStyleBackColor = true;
            this.btnClose2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(6, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "분류명 :";
            // 
            // cbCategoryName
            // 
            this.cbCategoryName.FormattingEnabled = true;
            this.cbCategoryName.Location = new System.Drawing.Point(66, 4);
            this.cbCategoryName.Name = "cbCategoryName";
            this.cbCategoryName.Size = new System.Drawing.Size(253, 20);
            this.cbCategoryName.TabIndex = 1;
            // 
            // picFrRightS12
            // 
            this.picFrRightS12.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightS12.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightS12.Location = new System.Drawing.Point(837, 4);
            this.picFrRightS12.Name = "picFrRightS12";
            this.picFrRightS12.Size = new System.Drawing.Size(4, 389);
            this.picFrRightS12.TabIndex = 109;
            this.picFrRightS12.TabStop = false;
            // 
            // picFrLeftS12
            // 
            this.picFrLeftS12.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS12.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS12.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftS12.Name = "picFrLeftS12";
            this.picFrLeftS12.Size = new System.Drawing.Size(4, 389);
            this.picFrLeftS12.TabIndex = 108;
            this.picFrLeftS12.TabStop = false;
            // 
            // picFrBottomS12
            // 
            this.picFrBottomS12.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS12.Location = new System.Drawing.Point(0, 393);
            this.picFrBottomS12.Name = "picFrBottomS12";
            this.picFrBottomS12.Size = new System.Drawing.Size(841, 4);
            this.picFrBottomS12.TabIndex = 107;
            this.picFrBottomS12.TabStop = false;
            // 
            // picFrTopS12
            // 
            this.picFrTopS12.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS12.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS12.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS12.Name = "picFrTopS12";
            this.picFrTopS12.Size = new System.Drawing.Size(841, 4);
            this.picFrTopS12.TabIndex = 106;
            this.picFrTopS12.TabStop = false;
            // 
            // uTabPageM1S3
            // 
            this.uTabPageM1S3.Controls.Add(this.panel9);
            this.uTabPageM1S3.Controls.Add(this.pictureBox1);
            this.uTabPageM1S3.Controls.Add(this.panel11);
            this.uTabPageM1S3.Controls.Add(this.panel13);
            this.uTabPageM1S3.Controls.Add(this.picFrRightS13);
            this.uTabPageM1S3.Controls.Add(this.picFrLeftS13);
            this.uTabPageM1S3.Controls.Add(this.picFrBottomS13);
            this.uTabPageM1S3.Controls.Add(this.picFrTopS13);
            this.uTabPageM1S3.Location = new System.Drawing.Point(-10000, -10000);
            this.uTabPageM1S3.Name = "uTabPageM1S3";
            this.uTabPageM1S3.Size = new System.Drawing.Size(841, 397);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Transparent;
            this.panel9.Controls.Add(this.uGrid13_2);
            this.panel9.Controls.Add(this.pictureBox49);
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.pictureBox37);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(440, 32);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(397, 361);
            this.panel9.TabIndex = 112;
            // 
            // uGrid13_2
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid13_2.DisplayLayout.Appearance = appearance25;
            this.uGrid13_2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid13_2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid13_2.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid13_2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGrid13_2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid13_2.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGrid13_2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid13_2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid13_2.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid13_2.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.uGrid13_2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid13_2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid13_2.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid13_2.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGrid13_2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid13_2.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid13_2.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.uGrid13_2.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.uGrid13_2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid13_2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.uGrid13_2.DisplayLayout.Override.RowAppearance = appearance35;
            this.uGrid13_2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid13_2.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.uGrid13_2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid13_2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid13_2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid13_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid13_2.Location = new System.Drawing.Point(0, 36);
            this.uGrid13_2.Name = "uGrid13_2";
            this.uGrid13_2.Size = new System.Drawing.Size(397, 325);
            this.uGrid13_2.TabIndex = 4;
            this.uGrid13_2.Text = "ultraGrid1";
            this.uGrid13_2.Click += new System.EventHandler(this.uGrid13_2_Click);
            // 
            // pictureBox49
            // 
            this.pictureBox49.BackColor = System.Drawing.Color.Gold;
            this.pictureBox49.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox49.Location = new System.Drawing.Point(0, 32);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(397, 4);
            this.pictureBox49.TabIndex = 108;
            this.pictureBox49.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.Control;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btnExcel3);
            this.panel10.Controls.Add(this.btnSave4);
            this.panel10.Controls.Add(this.label7);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(397, 28);
            this.panel10.TabIndex = 1;
            // 
            // btnExcel3
            // 
            this.btnExcel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExcel3.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel3.Location = new System.Drawing.Point(255, 0);
            this.btnExcel3.Name = "btnExcel3";
            this.btnExcel3.Size = new System.Drawing.Size(70, 26);
            this.btnExcel3.TabIndex = 9;
            this.btnExcel3.Text = "엑셀";
            this.btnExcel3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel3.UseVisualStyleBackColor = true;
            this.btnExcel3.Click += new System.EventHandler(this.btnExcel3_Click);
            // 
            // btnSave4
            // 
            this.btnSave4.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Save;
            this.btnSave4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave4.Location = new System.Drawing.Point(325, 0);
            this.btnSave4.Name = "btnSave4";
            this.btnSave4.Size = new System.Drawing.Size(70, 26);
            this.btnSave4.TabIndex = 8;
            this.btnSave4.Text = "저장";
            this.btnSave4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave4.UseVisualStyleBackColor = true;
            this.btnSave4.Click += new System.EventHandler(this.btnSave4_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(6, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "시험항목관리";
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackColor = System.Drawing.Color.Gold;
            this.pictureBox37.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox37.Location = new System.Drawing.Point(0, 0);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(397, 4);
            this.pictureBox37.TabIndex = 107;
            this.pictureBox37.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Location = new System.Drawing.Point(436, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(4, 361);
            this.pictureBox1.TabIndex = 113;
            this.pictureBox1.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.Controls.Add(this.uGrid13_1);
            this.panel11.Controls.Add(this.pictureBox48);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.pictureBox36);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(4, 32);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(432, 361);
            this.panel11.TabIndex = 111;
            // 
            // uGrid13_1
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid13_1.DisplayLayout.Appearance = appearance13;
            this.uGrid13_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid13_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid13_1.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid13_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGrid13_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid13_1.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGrid13_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid13_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid13_1.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid13_1.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGrid13_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid13_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid13_1.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid13_1.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGrid13_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid13_1.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid13_1.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGrid13_1.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGrid13_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid13_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGrid13_1.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGrid13_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid13_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGrid13_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid13_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid13_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid13_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid13_1.Location = new System.Drawing.Point(0, 36);
            this.uGrid13_1.Name = "uGrid13_1";
            this.uGrid13_1.Size = new System.Drawing.Size(432, 325);
            this.uGrid13_1.TabIndex = 2;
            this.uGrid13_1.Text = "ultraGrid1";
            this.uGrid13_1.Click += new System.EventHandler(this.uGrid13_1_Click);
            // 
            // pictureBox48
            // 
            this.pictureBox48.BackColor = System.Drawing.Color.Gold;
            this.pictureBox48.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox48.Location = new System.Drawing.Point(0, 32);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(432, 4);
            this.pictureBox48.TabIndex = 108;
            this.pictureBox48.TabStop = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Control;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.label8);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(432, 28);
            this.panel12.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(6, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 12);
            this.label8.TabIndex = 3;
            this.label8.Text = "사업장별 공정";
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackColor = System.Drawing.Color.Gold;
            this.pictureBox36.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox36.Location = new System.Drawing.Point(0, 0);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(432, 4);
            this.pictureBox36.TabIndex = 107;
            this.pictureBox36.TabStop = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.Control;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.label11);
            this.panel13.Controls.Add(this.cbMeasurePeriod);
            this.panel13.Controls.Add(this.label10);
            this.panel13.Controls.Add(this.cbEstablishment1);
            this.panel13.Controls.Add(this.btnQuery3);
            this.panel13.Controls.Add(this.btnClose3);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(4, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(833, 28);
            this.panel13.TabIndex = 110;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(274, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 12);
            this.label11.TabIndex = 107;
            this.label11.Text = "측정주기 :";
            // 
            // cbMeasurePeriod
            // 
            this.cbMeasurePeriod.FormattingEnabled = true;
            this.cbMeasurePeriod.Location = new System.Drawing.Point(347, 3);
            this.cbMeasurePeriod.Name = "cbMeasurePeriod";
            this.cbMeasurePeriod.Size = new System.Drawing.Size(63, 20);
            this.cbMeasurePeriod.TabIndex = 106;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(6, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 12);
            this.label10.TabIndex = 8;
            this.label10.Text = "사업장 :";
            // 
            // cbEstablishment1
            // 
            this.cbEstablishment1.FormattingEnabled = true;
            this.cbEstablishment1.Location = new System.Drawing.Point(66, 3);
            this.cbEstablishment1.Name = "cbEstablishment1";
            this.cbEstablishment1.Size = new System.Drawing.Size(202, 20);
            this.cbEstablishment1.TabIndex = 7;
            // 
            // btnQuery3
            // 
            this.btnQuery3.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnQuery3.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery3.Location = new System.Drawing.Point(691, 0);
            this.btnQuery3.Name = "btnQuery3";
            this.btnQuery3.Size = new System.Drawing.Size(70, 26);
            this.btnQuery3.TabIndex = 6;
            this.btnQuery3.Text = "조회";
            this.btnQuery3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery3.UseVisualStyleBackColor = true;
            this.btnQuery3.Click += new System.EventHandler(this.btnQuery3_Click);
            // 
            // btnClose3
            // 
            this.btnClose3.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose3.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose3.Location = new System.Drawing.Point(761, 0);
            this.btnClose3.Name = "btnClose3";
            this.btnClose3.Size = new System.Drawing.Size(70, 26);
            this.btnClose3.TabIndex = 3;
            this.btnClose3.Text = "닫기";
            this.btnClose3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose3.UseVisualStyleBackColor = true;
            this.btnClose3.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // picFrRightS13
            // 
            this.picFrRightS13.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightS13.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightS13.Location = new System.Drawing.Point(837, 4);
            this.picFrRightS13.Name = "picFrRightS13";
            this.picFrRightS13.Size = new System.Drawing.Size(4, 389);
            this.picFrRightS13.TabIndex = 109;
            this.picFrRightS13.TabStop = false;
            // 
            // picFrLeftS13
            // 
            this.picFrLeftS13.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS13.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS13.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftS13.Name = "picFrLeftS13";
            this.picFrLeftS13.Size = new System.Drawing.Size(4, 389);
            this.picFrLeftS13.TabIndex = 108;
            this.picFrLeftS13.TabStop = false;
            // 
            // picFrBottomS13
            // 
            this.picFrBottomS13.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS13.Location = new System.Drawing.Point(0, 393);
            this.picFrBottomS13.Name = "picFrBottomS13";
            this.picFrBottomS13.Size = new System.Drawing.Size(841, 4);
            this.picFrBottomS13.TabIndex = 107;
            this.picFrBottomS13.TabStop = false;
            // 
            // picFrTopS13
            // 
            this.picFrTopS13.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS13.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS13.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS13.Name = "picFrTopS13";
            this.picFrTopS13.Size = new System.Drawing.Size(841, 4);
            this.picFrTopS13.TabIndex = 106;
            this.picFrTopS13.TabStop = false;
            // 
            // picFrRightM1
            // 
            this.picFrRightM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightM1.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightM1.Location = new System.Drawing.Point(849, 4);
            this.picFrRightM1.Name = "picFrRightM1";
            this.picFrRightM1.Size = new System.Drawing.Size(4, 505);
            this.picFrRightM1.TabIndex = 108;
            this.picFrRightM1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(4, 505);
            this.picFrLeftM1.TabIndex = 107;
            this.picFrLeftM1.TabStop = false;
            // 
            // picFrBottomM1
            // 
            this.picFrBottomM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomM1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomM1.Location = new System.Drawing.Point(0, 509);
            this.picFrBottomM1.Name = "picFrBottomM1";
            this.picFrBottomM1.Size = new System.Drawing.Size(853, 4);
            this.picFrBottomM1.TabIndex = 106;
            this.picFrBottomM1.TabStop = false;
            // 
            // picFrTopM1
            // 
            this.picFrTopM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopM1.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopM1.Location = new System.Drawing.Point(0, 0);
            this.picFrTopM1.Name = "picFrTopM1";
            this.picFrTopM1.Size = new System.Drawing.Size(853, 4);
            this.picFrTopM1.TabIndex = 105;
            this.picFrTopM1.TabStop = false;
            // 
            // uTabMenuS1
            // 
            appearance145.BackColor = System.Drawing.SystemColors.Control;
            this.uTabMenuS1.Appearance = appearance145;
            this.uTabMenuS1.Controls.Add(this.ultraTabSharedControlsPage2);
            this.uTabMenuS1.Controls.Add(this.uTabPageM1S1);
            this.uTabMenuS1.Controls.Add(this.uTabPageM1S2);
            this.uTabMenuS1.Controls.Add(this.uTabPageM1S3);
            this.uTabMenuS1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabMenuS1.Location = new System.Drawing.Point(4, 4);
            this.uTabMenuS1.Name = "uTabMenuS1";
            this.uTabMenuS1.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.uTabMenuS1.Size = new System.Drawing.Size(845, 505);
            this.uTabMenuS1.TabIndex = 109;
            ultraTab10.TabPage = this.uTabPageM1S1;
            ultraTab10.Text = "사업장별공정관리";
            ultraTab11.TabPage = this.uTabPageM1S2;
            ultraTab11.Text = "시험항목관리";
            ultraTab12.TabPage = this.uTabPageM1S3;
            ultraTab12.Text = "사업장공정수질항목관리";
            this.uTabMenuS1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab10,
            ultraTab11,
            ultraTab12});
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(841, 479);
            // 
            // frmWQCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 513);
            this.Controls.Add(this.uTabMenuS1);
            this.Controls.Add(this.picFrRightM1);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.picFrBottomM1);
            this.Controls.Add(this.picFrTopM1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmWQCode";
            this.Text = "코드관리";
            this.Load += new System.EventHandler(this.frmWQCode_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQCode_FormClosing);
            this.uTabPageM1S1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrCleftS11)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).EndInit();
            this.uTabPageM1S2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS12)).EndInit();
            this.uTabPageM1S3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid13_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid13_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenuS1)).EndInit();
            this.uTabMenuS1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrRightM1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox picFrBottomM1;
        private System.Windows.Forms.PictureBox picFrTopM1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenuS1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM1S1;
        private System.Windows.Forms.Panel panel4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid11_2;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnExcel1;
        private System.Windows.Forms.Button btnSave2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox picFrCleftS11;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid11_1;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnAppend1;
        private System.Windows.Forms.Button btnSave1;
        private System.Windows.Forms.Button btnDelete1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnQuery1;
        private System.Windows.Forms.Button btnClose1;
        private System.Windows.Forms.PictureBox picFrRightS11;
        private System.Windows.Forms.PictureBox picFrLeftS11;
        private System.Windows.Forms.PictureBox picFrBottomS11;
        private System.Windows.Forms.PictureBox picFrTopS11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM1S2;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid12_1;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnAppend2;
        private System.Windows.Forms.Button btnExcel2;
        private System.Windows.Forms.Button btnSave3;
        private System.Windows.Forms.Button btnDelete2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnQuery2;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnClose2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbCategoryName;
        private System.Windows.Forms.PictureBox picFrRightS12;
        private System.Windows.Forms.PictureBox picFrLeftS12;
        private System.Windows.Forms.PictureBox picFrBottomS12;
        private System.Windows.Forms.PictureBox picFrTopS12;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM1S3;
        private System.Windows.Forms.Panel panel9;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid13_2;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnExcel3;
        private System.Windows.Forms.Button btnSave4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel11;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid13_1;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbMeasurePeriod;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbEstablishment1;
        private System.Windows.Forms.Button btnQuery3;
        private System.Windows.Forms.Button btnClose3;
        private System.Windows.Forms.PictureBox picFrRightS13;
        private System.Windows.Forms.PictureBox picFrLeftS13;
        private System.Windows.Forms.PictureBox picFrBottomS13;
        private System.Windows.Forms.PictureBox picFrTopS13;
    }
}