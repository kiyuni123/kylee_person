﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;

namespace WaterNet.WQ_GumsaData.FormPopup
{
    public partial class frmWQPTIBasicValue : Form
    {
        /// <summary>
        /// Project ID : WN_WQ_A01
        /// Project Explain : 수질검사자료관리
        /// Project Developer : 오두석
        /// Project Create Date : 2010.09.30
        /// Form Explain : 수질시험 시험항목 기준치 Popup Form
        /// </summary>

        private string _TIName = string.Empty;
        /// <summary>
        /// 수질시험항목명
        /// </summary>
        public string TIName
        {
            get
            {
                return _TIName;
            }
            set
            {
                _TIName = value;
            }
        }

        public frmWQPTIBasicValue()
        {
            InitializeComponent();
        }

        private void frmWQPTIBasicValue_Load(object sender, EventArgs e)
        {
            this.GetTIBasicValue();
        }

        /// <summary>
        /// 시험항목의 기준치를 Select해 Control에 Set
        /// </summary>
        public void GetTIBasicValue()
        {

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   TITMNM, HMNTSVAL, LMNTSVAL, LTRSVAL, VLDFGR");
            oStringBuilder.AppendLine("FROM     WQCTIT");
            oStringBuilder.AppendLine("WHERE    TITCD = '" + WQ_Function.SplitToCode(_TIName) + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.txtTITMNM.Text = oDRow["TITMNM"].ToString();
                    this.txtHMNTSVAL.Text = oDRow["HMNTSVAL"].ToString();
                    this.txtLMNTSVAL.Text = oDRow["LMNTSVAL"].ToString();
                    this.txtLTRSVAL.Text = oDRow["LTRSVAL"].ToString();
                    this.txtVLDFGR.Text = oDRow["VLDFGR"].ToString();

                    i++;
                }
            }
            pDS.Dispose();
            oDBManager.Close();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

    }
}
