﻿namespace WaterNet.WQ_GumsaData.FormPopup
{
    partial class frmWQTestResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab14 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.uTabPageM2S1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel14 = new System.Windows.Forms.Panel();
            this.uGrid21_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnDetailData1 = new System.Windows.Forms.Button();
            this.btnExcel4 = new System.Windows.Forms.Button();
            this.btnNewTest1 = new System.Windows.Forms.Button();
            this.btnDelete3 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.udtWaterCollectDateE1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbEstablishment2 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.udtWaterCollectDateS1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnQuery4 = new System.Windows.Forms.Button();
            this.btnClose4 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.uTabPageM2S2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel17 = new System.Windows.Forms.Panel();
            this.uGrid22_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnDetailData2 = new System.Windows.Forms.Button();
            this.btnExcel5 = new System.Windows.Forms.Button();
            this.btnNewTest2 = new System.Windows.Forms.Button();
            this.btnDelete4 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.udtWaterCollectDateE2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cbEstablishment3 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.udtWaterCollectDateS2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnQuery5 = new System.Windows.Forms.Button();
            this.btnClose5 = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.uTabPageM2S3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel20 = new System.Windows.Forms.Panel();
            this.uGrid23_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btnDetailData3 = new System.Windows.Forms.Button();
            this.btnExcel6 = new System.Windows.Forms.Button();
            this.btnNewTest3 = new System.Windows.Forms.Button();
            this.btnDelete5 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.udtWaterCollectDateE3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cbEstablishment4 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.udtWaterCollectDateS3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnQuery6 = new System.Windows.Forms.Button();
            this.btnClose6 = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.uTabPageM2S4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel23 = new System.Windows.Forms.Panel();
            this.uGrid24_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.btnDetailData4 = new System.Windows.Forms.Button();
            this.btnExcel7 = new System.Windows.Forms.Button();
            this.btnNewTest4 = new System.Windows.Forms.Button();
            this.btnDelete6 = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.udtWaterCollectDateE4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.cbEstablishment5 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.udtWaterCollectDateS4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnQuery7 = new System.Windows.Forms.Button();
            this.btnClose7 = new System.Windows.Forms.Button();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.uTabPageM2S5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel26 = new System.Windows.Forms.Panel();
            this.uGrid25_1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.btnDetailData5 = new System.Windows.Forms.Button();
            this.btnExcel8 = new System.Windows.Forms.Button();
            this.btnNewTest5 = new System.Windows.Forms.Button();
            this.btnDelete7 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.udtWaterCollectDateE5 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.cbEstablishment6 = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.udtWaterCollectDateS5 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnQuery8 = new System.Windows.Forms.Button();
            this.btnClose8 = new System.Windows.Forms.Button();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.picFrRightM2 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM2 = new System.Windows.Forms.PictureBox();
            this.picFrTopM2 = new System.Windows.Forms.PictureBox();
            this.uTabMenuS2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage3 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTabPageM2S1.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid21_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.uTabPageM2S2.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid22_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.uTabPageM2S3.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid23_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.uTabPageM2S4.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid24_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.uTabPageM2S5.SuspendLayout();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid25_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenuS2)).BeginInit();
            this.uTabMenuS2.SuspendLayout();
            this.SuspendLayout();
            // 
            // uTabPageM2S1
            // 
            this.uTabPageM2S1.Controls.Add(this.panel14);
            this.uTabPageM2S1.Controls.Add(this.panel16);
            this.uTabPageM2S1.Controls.Add(this.pictureBox2);
            this.uTabPageM2S1.Controls.Add(this.pictureBox3);
            this.uTabPageM2S1.Controls.Add(this.pictureBox1);
            this.uTabPageM2S1.Controls.Add(this.pictureBox5);
            this.uTabPageM2S1.Location = new System.Drawing.Point(1, 23);
            this.uTabPageM2S1.Name = "uTabPageM2S1";
            this.uTabPageM2S1.Size = new System.Drawing.Size(674, 399);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.Controls.Add(this.uGrid21_1);
            this.panel14.Controls.Add(this.pictureBox38);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Controls.Add(this.pictureBox39);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(4, 32);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(666, 363);
            this.panel14.TabIndex = 117;
            // 
            // uGrid21_1
            // 
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid21_1.DisplayLayout.Appearance = appearance97;
            this.uGrid21_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid21_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance98.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance98.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance98.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance98.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid21_1.DisplayLayout.GroupByBox.Appearance = appearance98;
            appearance99.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid21_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance99;
            this.uGrid21_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance100.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance100.BackColor2 = System.Drawing.SystemColors.Control;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid21_1.DisplayLayout.GroupByBox.PromptAppearance = appearance100;
            this.uGrid21_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid21_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance101.BackColor = System.Drawing.SystemColors.Window;
            appearance101.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid21_1.DisplayLayout.Override.ActiveCellAppearance = appearance101;
            appearance102.BackColor = System.Drawing.SystemColors.Highlight;
            appearance102.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid21_1.DisplayLayout.Override.ActiveRowAppearance = appearance102;
            this.uGrid21_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid21_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance103.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid21_1.DisplayLayout.Override.CardAreaAppearance = appearance103;
            appearance104.BorderColor = System.Drawing.Color.Silver;
            appearance104.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid21_1.DisplayLayout.Override.CellAppearance = appearance104;
            this.uGrid21_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid21_1.DisplayLayout.Override.CellPadding = 0;
            appearance105.BackColor = System.Drawing.SystemColors.Control;
            appearance105.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance105.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance105.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance105.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid21_1.DisplayLayout.Override.GroupByRowAppearance = appearance105;
            appearance106.TextHAlignAsString = "Left";
            this.uGrid21_1.DisplayLayout.Override.HeaderAppearance = appearance106;
            this.uGrid21_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid21_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance107.BackColor = System.Drawing.SystemColors.Window;
            appearance107.BorderColor = System.Drawing.Color.Silver;
            this.uGrid21_1.DisplayLayout.Override.RowAppearance = appearance107;
            this.uGrid21_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance108.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid21_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance108;
            this.uGrid21_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid21_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid21_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid21_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid21_1.Location = new System.Drawing.Point(0, 36);
            this.uGrid21_1.Name = "uGrid21_1";
            this.uGrid21_1.Size = new System.Drawing.Size(666, 327);
            this.uGrid21_1.TabIndex = 4;
            this.uGrid21_1.Text = "ultraGrid1";
            this.uGrid21_1.Click += new System.EventHandler(this.uGrid_Click);
            this.uGrid21_1.DoubleClick += new System.EventHandler(this.uGrid2N_1_DoubleClick);
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackColor = System.Drawing.Color.Gold;
            this.pictureBox38.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox38.Location = new System.Drawing.Point(0, 32);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(666, 4);
            this.pictureBox38.TabIndex = 114;
            this.pictureBox38.TabStop = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.Control;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnDetailData1);
            this.panel15.Controls.Add(this.btnExcel4);
            this.panel15.Controls.Add(this.btnNewTest1);
            this.panel15.Controls.Add(this.btnDelete3);
            this.panel15.Controls.Add(this.label12);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(666, 28);
            this.panel15.TabIndex = 1;
            // 
            // btnDetailData1
            // 
            this.btnDetailData1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDetailData1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Infor;
            this.btnDetailData1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetailData1.Location = new System.Drawing.Point(383, 0);
            this.btnDetailData1.Name = "btnDetailData1";
            this.btnDetailData1.Size = new System.Drawing.Size(70, 26);
            this.btnDetailData1.TabIndex = 13;
            this.btnDetailData1.Text = "상세";
            this.btnDetailData1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetailData1.UseVisualStyleBackColor = true;
            this.btnDetailData1.Click += new System.EventHandler(this.btnDetailData_Click);
            // 
            // btnExcel4
            // 
            this.btnExcel4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcel4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel4.Location = new System.Drawing.Point(453, 0);
            this.btnExcel4.Name = "btnExcel4";
            this.btnExcel4.Size = new System.Drawing.Size(70, 26);
            this.btnExcel4.TabIndex = 12;
            this.btnExcel4.Text = "엑셀";
            this.btnExcel4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel4.UseVisualStyleBackColor = true;
            this.btnExcel4.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnNewTest1
            // 
            this.btnNewTest1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNewTest1.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.NewPopup;
            this.btnNewTest1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNewTest1.Location = new System.Drawing.Point(523, 0);
            this.btnNewTest1.Name = "btnNewTest1";
            this.btnNewTest1.Size = new System.Drawing.Size(70, 26);
            this.btnNewTest1.TabIndex = 7;
            this.btnNewTest1.Text = "신규";
            this.btnNewTest1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewTest1.UseVisualStyleBackColor = true;
            this.btnNewTest1.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnDelete3
            // 
            this.btnDelete3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete3.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete3.Location = new System.Drawing.Point(593, 0);
            this.btnDelete3.Name = "btnDelete3";
            this.btnDelete3.Size = new System.Drawing.Size(70, 26);
            this.btnDelete3.TabIndex = 8;
            this.btnDelete3.Text = "삭제";
            this.btnDelete3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete3.UseVisualStyleBackColor = true;
            this.btnDelete3.Click += new System.EventHandler(this.btnDelete3_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(6, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 12);
            this.label12.TabIndex = 5;
            this.label12.Text = "일일수질시험관리";
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackColor = System.Drawing.Color.Gold;
            this.pictureBox39.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox39.Location = new System.Drawing.Point(0, 0);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(666, 4);
            this.pictureBox39.TabIndex = 113;
            this.pictureBox39.TabStop = false;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.SystemColors.Control;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.udtWaterCollectDateE1);
            this.panel16.Controls.Add(this.label16);
            this.panel16.Controls.Add(this.label13);
            this.panel16.Controls.Add(this.cbEstablishment2);
            this.panel16.Controls.Add(this.label15);
            this.panel16.Controls.Add(this.udtWaterCollectDateS1);
            this.panel16.Controls.Add(this.btnQuery4);
            this.panel16.Controls.Add(this.btnClose4);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(4, 4);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(666, 28);
            this.panel16.TabIndex = 116;
            // 
            // udtWaterCollectDateE1
            // 
            this.udtWaterCollectDateE1.Location = new System.Drawing.Point(409, 2);
            this.udtWaterCollectDateE1.Name = "udtWaterCollectDateE1";
            this.udtWaterCollectDateE1.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateE1.TabIndex = 20;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(395, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 12);
            this.label16.TabIndex = 24;
            this.label16.Text = "~";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(6, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 12);
            this.label13.TabIndex = 22;
            this.label13.Text = "사업장 :";
            // 
            // cbEstablishment2
            // 
            this.cbEstablishment2.FormattingEnabled = true;
            this.cbEstablishment2.Location = new System.Drawing.Point(66, 3);
            this.cbEstablishment2.Name = "cbEstablishment2";
            this.cbEstablishment2.Size = new System.Drawing.Size(150, 20);
            this.cbEstablishment2.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(222, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 12);
            this.label15.TabIndex = 18;
            this.label15.Text = "채수일자 :";
            // 
            // udtWaterCollectDateS1
            // 
            this.udtWaterCollectDateS1.Location = new System.Drawing.Point(295, 2);
            this.udtWaterCollectDateS1.Name = "udtWaterCollectDateS1";
            this.udtWaterCollectDateS1.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateS1.TabIndex = 17;
            // 
            // btnQuery4
            // 
            this.btnQuery4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery4.Location = new System.Drawing.Point(523, 0);
            this.btnQuery4.Name = "btnQuery4";
            this.btnQuery4.Size = new System.Drawing.Size(70, 26);
            this.btnQuery4.TabIndex = 12;
            this.btnQuery4.Text = "조회";
            this.btnQuery4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery4.UseVisualStyleBackColor = true;
            this.btnQuery4.Click += new System.EventHandler(this.btnQuery4_Click);
            // 
            // btnClose4
            // 
            this.btnClose4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose4.Location = new System.Drawing.Point(593, 0);
            this.btnClose4.Name = "btnClose4";
            this.btnClose4.Size = new System.Drawing.Size(70, 26);
            this.btnClose4.TabIndex = 3;
            this.btnClose4.Text = "닫기";
            this.btnClose4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose4.UseVisualStyleBackColor = true;
            this.btnClose4.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Location = new System.Drawing.Point(670, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(4, 391);
            this.pictureBox2.TabIndex = 115;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(4, 391);
            this.pictureBox3.TabIndex = 114;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 395);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(674, 4);
            this.pictureBox1.TabIndex = 113;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(674, 4);
            this.pictureBox5.TabIndex = 112;
            this.pictureBox5.TabStop = false;
            // 
            // uTabPageM2S2
            // 
            this.uTabPageM2S2.Controls.Add(this.panel17);
            this.uTabPageM2S2.Controls.Add(this.pictureBox41);
            this.uTabPageM2S2.Controls.Add(this.panel19);
            this.uTabPageM2S2.Controls.Add(this.pictureBox6);
            this.uTabPageM2S2.Controls.Add(this.pictureBox7);
            this.uTabPageM2S2.Controls.Add(this.pictureBox8);
            this.uTabPageM2S2.Controls.Add(this.pictureBox9);
            this.uTabPageM2S2.Location = new System.Drawing.Point(-10000, -10000);
            this.uTabPageM2S2.Name = "uTabPageM2S2";
            this.uTabPageM2S2.Size = new System.Drawing.Size(811, 472);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.Controls.Add(this.uGrid22_1);
            this.panel17.Controls.Add(this.pictureBox40);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(4, 36);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(803, 432);
            this.panel17.TabIndex = 123;
            // 
            // uGrid22_1
            // 
            appearance109.BackColor = System.Drawing.SystemColors.Window;
            appearance109.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid22_1.DisplayLayout.Appearance = appearance109;
            this.uGrid22_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid22_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance110.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance110.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance110.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance110.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid22_1.DisplayLayout.GroupByBox.Appearance = appearance110;
            appearance111.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid22_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance111;
            this.uGrid22_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance112.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance112.BackColor2 = System.Drawing.SystemColors.Control;
            appearance112.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance112.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid22_1.DisplayLayout.GroupByBox.PromptAppearance = appearance112;
            this.uGrid22_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid22_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance113.BackColor = System.Drawing.SystemColors.Window;
            appearance113.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid22_1.DisplayLayout.Override.ActiveCellAppearance = appearance113;
            appearance114.BackColor = System.Drawing.SystemColors.Highlight;
            appearance114.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid22_1.DisplayLayout.Override.ActiveRowAppearance = appearance114;
            this.uGrid22_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid22_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance115.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid22_1.DisplayLayout.Override.CardAreaAppearance = appearance115;
            appearance116.BorderColor = System.Drawing.Color.Silver;
            appearance116.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid22_1.DisplayLayout.Override.CellAppearance = appearance116;
            this.uGrid22_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid22_1.DisplayLayout.Override.CellPadding = 0;
            appearance117.BackColor = System.Drawing.SystemColors.Control;
            appearance117.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance117.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance117.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance117.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid22_1.DisplayLayout.Override.GroupByRowAppearance = appearance117;
            appearance118.TextHAlignAsString = "Left";
            this.uGrid22_1.DisplayLayout.Override.HeaderAppearance = appearance118;
            this.uGrid22_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid22_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance119.BackColor = System.Drawing.SystemColors.Window;
            appearance119.BorderColor = System.Drawing.Color.Silver;
            this.uGrid22_1.DisplayLayout.Override.RowAppearance = appearance119;
            this.uGrid22_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance120.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid22_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance120;
            this.uGrid22_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid22_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid22_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid22_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid22_1.Location = new System.Drawing.Point(0, 32);
            this.uGrid22_1.Name = "uGrid22_1";
            this.uGrid22_1.Size = new System.Drawing.Size(803, 400);
            this.uGrid22_1.TabIndex = 4;
            this.uGrid22_1.Text = "ultraGrid1";
            this.uGrid22_1.Click += new System.EventHandler(this.uGrid_Click);
            this.uGrid22_1.DoubleClick += new System.EventHandler(this.uGrid2N_1_DoubleClick);
            // 
            // pictureBox40
            // 
            this.pictureBox40.BackColor = System.Drawing.Color.Gold;
            this.pictureBox40.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox40.Location = new System.Drawing.Point(0, 28);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(803, 4);
            this.pictureBox40.TabIndex = 114;
            this.pictureBox40.TabStop = false;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.SystemColors.Control;
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.btnDetailData2);
            this.panel18.Controls.Add(this.btnExcel5);
            this.panel18.Controls.Add(this.btnNewTest2);
            this.panel18.Controls.Add(this.btnDelete4);
            this.panel18.Controls.Add(this.label17);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(803, 28);
            this.panel18.TabIndex = 1;
            // 
            // btnDetailData2
            // 
            this.btnDetailData2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDetailData2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Infor;
            this.btnDetailData2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetailData2.Location = new System.Drawing.Point(383, 0);
            this.btnDetailData2.Name = "btnDetailData2";
            this.btnDetailData2.Size = new System.Drawing.Size(70, 26);
            this.btnDetailData2.TabIndex = 13;
            this.btnDetailData2.Text = "상세";
            this.btnDetailData2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetailData2.UseVisualStyleBackColor = true;
            this.btnDetailData2.Click += new System.EventHandler(this.btnDetailData_Click);
            // 
            // btnExcel5
            // 
            this.btnExcel5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcel5.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel5.Location = new System.Drawing.Point(453, 0);
            this.btnExcel5.Name = "btnExcel5";
            this.btnExcel5.Size = new System.Drawing.Size(70, 26);
            this.btnExcel5.TabIndex = 12;
            this.btnExcel5.Text = "엑셀";
            this.btnExcel5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel5.UseVisualStyleBackColor = true;
            this.btnExcel5.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnNewTest2
            // 
            this.btnNewTest2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNewTest2.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.NewPopup;
            this.btnNewTest2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNewTest2.Location = new System.Drawing.Point(523, 0);
            this.btnNewTest2.Name = "btnNewTest2";
            this.btnNewTest2.Size = new System.Drawing.Size(70, 26);
            this.btnNewTest2.TabIndex = 7;
            this.btnNewTest2.Text = "신규";
            this.btnNewTest2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewTest2.UseVisualStyleBackColor = true;
            this.btnNewTest2.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnDelete4
            // 
            this.btnDelete4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete4.Location = new System.Drawing.Point(593, 0);
            this.btnDelete4.Name = "btnDelete4";
            this.btnDelete4.Size = new System.Drawing.Size(70, 26);
            this.btnDelete4.TabIndex = 8;
            this.btnDelete4.Text = "삭제";
            this.btnDelete4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete4.UseVisualStyleBackColor = true;
            this.btnDelete4.Click += new System.EventHandler(this.btnDelete4_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(6, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(109, 12);
            this.label17.TabIndex = 5;
            this.label17.Text = "주간수질시험관리";
            // 
            // pictureBox41
            // 
            this.pictureBox41.BackColor = System.Drawing.Color.Gold;
            this.pictureBox41.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox41.Location = new System.Drawing.Point(4, 32);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(803, 4);
            this.pictureBox41.TabIndex = 124;
            this.pictureBox41.TabStop = false;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.Control;
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.udtWaterCollectDateE2);
            this.panel19.Controls.Add(this.label18);
            this.panel19.Controls.Add(this.label19);
            this.panel19.Controls.Add(this.cbEstablishment3);
            this.panel19.Controls.Add(this.label20);
            this.panel19.Controls.Add(this.udtWaterCollectDateS2);
            this.panel19.Controls.Add(this.btnQuery5);
            this.panel19.Controls.Add(this.btnClose5);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(4, 4);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(803, 28);
            this.panel19.TabIndex = 122;
            // 
            // udtWaterCollectDateE2
            // 
            this.udtWaterCollectDateE2.Location = new System.Drawing.Point(409, 2);
            this.udtWaterCollectDateE2.Name = "udtWaterCollectDateE2";
            this.udtWaterCollectDateE2.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateE2.TabIndex = 20;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(395, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 12);
            this.label18.TabIndex = 24;
            this.label18.Text = "~";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(6, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 12);
            this.label19.TabIndex = 22;
            this.label19.Text = "사업장 :";
            // 
            // cbEstablishment3
            // 
            this.cbEstablishment3.FormattingEnabled = true;
            this.cbEstablishment3.Location = new System.Drawing.Point(66, 3);
            this.cbEstablishment3.Name = "cbEstablishment3";
            this.cbEstablishment3.Size = new System.Drawing.Size(150, 20);
            this.cbEstablishment3.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(222, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 12);
            this.label20.TabIndex = 18;
            this.label20.Text = "채수일자 :";
            // 
            // udtWaterCollectDateS2
            // 
            this.udtWaterCollectDateS2.Location = new System.Drawing.Point(295, 2);
            this.udtWaterCollectDateS2.Name = "udtWaterCollectDateS2";
            this.udtWaterCollectDateS2.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateS2.TabIndex = 17;
            // 
            // btnQuery5
            // 
            this.btnQuery5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery5.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery5.Location = new System.Drawing.Point(523, 0);
            this.btnQuery5.Name = "btnQuery5";
            this.btnQuery5.Size = new System.Drawing.Size(70, 26);
            this.btnQuery5.TabIndex = 12;
            this.btnQuery5.Text = "조회";
            this.btnQuery5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery5.UseVisualStyleBackColor = true;
            this.btnQuery5.Click += new System.EventHandler(this.btnQuery5_Click);
            // 
            // btnClose5
            // 
            this.btnClose5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose5.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose5.Location = new System.Drawing.Point(593, 0);
            this.btnClose5.Name = "btnClose5";
            this.btnClose5.Size = new System.Drawing.Size(70, 26);
            this.btnClose5.TabIndex = 3;
            this.btnClose5.Text = "닫기";
            this.btnClose5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose5.UseVisualStyleBackColor = true;
            this.btnClose5.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox6.Location = new System.Drawing.Point(807, 4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(4, 464);
            this.pictureBox6.TabIndex = 121;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(0, 4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(4, 464);
            this.pictureBox7.TabIndex = 120;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox8.Location = new System.Drawing.Point(0, 468);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(811, 4);
            this.pictureBox8.TabIndex = 119;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(0, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(811, 4);
            this.pictureBox9.TabIndex = 118;
            this.pictureBox9.TabStop = false;
            // 
            // uTabPageM2S3
            // 
            this.uTabPageM2S3.Controls.Add(this.panel20);
            this.uTabPageM2S3.Controls.Add(this.pictureBox43);
            this.uTabPageM2S3.Controls.Add(this.panel22);
            this.uTabPageM2S3.Controls.Add(this.pictureBox10);
            this.uTabPageM2S3.Controls.Add(this.pictureBox11);
            this.uTabPageM2S3.Controls.Add(this.pictureBox12);
            this.uTabPageM2S3.Controls.Add(this.pictureBox13);
            this.uTabPageM2S3.Location = new System.Drawing.Point(-10000, -10000);
            this.uTabPageM2S3.Name = "uTabPageM2S3";
            this.uTabPageM2S3.Size = new System.Drawing.Size(811, 472);
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Transparent;
            this.panel20.Controls.Add(this.uGrid23_1);
            this.panel20.Controls.Add(this.pictureBox42);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(4, 36);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(803, 432);
            this.panel20.TabIndex = 123;
            // 
            // uGrid23_1
            // 
            appearance121.BackColor = System.Drawing.SystemColors.Window;
            appearance121.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid23_1.DisplayLayout.Appearance = appearance121;
            this.uGrid23_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid23_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance122.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance122.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance122.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance122.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid23_1.DisplayLayout.GroupByBox.Appearance = appearance122;
            appearance123.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid23_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance123;
            this.uGrid23_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance124.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance124.BackColor2 = System.Drawing.SystemColors.Control;
            appearance124.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance124.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid23_1.DisplayLayout.GroupByBox.PromptAppearance = appearance124;
            this.uGrid23_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid23_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance125.BackColor = System.Drawing.SystemColors.Window;
            appearance125.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid23_1.DisplayLayout.Override.ActiveCellAppearance = appearance125;
            appearance126.BackColor = System.Drawing.SystemColors.Highlight;
            appearance126.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid23_1.DisplayLayout.Override.ActiveRowAppearance = appearance126;
            this.uGrid23_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid23_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance127.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid23_1.DisplayLayout.Override.CardAreaAppearance = appearance127;
            appearance128.BorderColor = System.Drawing.Color.Silver;
            appearance128.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid23_1.DisplayLayout.Override.CellAppearance = appearance128;
            this.uGrid23_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid23_1.DisplayLayout.Override.CellPadding = 0;
            appearance129.BackColor = System.Drawing.SystemColors.Control;
            appearance129.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance129.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance129.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance129.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid23_1.DisplayLayout.Override.GroupByRowAppearance = appearance129;
            appearance130.TextHAlignAsString = "Left";
            this.uGrid23_1.DisplayLayout.Override.HeaderAppearance = appearance130;
            this.uGrid23_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid23_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance131.BackColor = System.Drawing.SystemColors.Window;
            appearance131.BorderColor = System.Drawing.Color.Silver;
            this.uGrid23_1.DisplayLayout.Override.RowAppearance = appearance131;
            this.uGrid23_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance132.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid23_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance132;
            this.uGrid23_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid23_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid23_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid23_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid23_1.Location = new System.Drawing.Point(0, 32);
            this.uGrid23_1.Name = "uGrid23_1";
            this.uGrid23_1.Size = new System.Drawing.Size(803, 400);
            this.uGrid23_1.TabIndex = 4;
            this.uGrid23_1.Text = "ultraGrid1";
            this.uGrid23_1.Click += new System.EventHandler(this.uGrid_Click);
            this.uGrid23_1.DoubleClick += new System.EventHandler(this.uGrid2N_1_DoubleClick);
            // 
            // pictureBox42
            // 
            this.pictureBox42.BackColor = System.Drawing.Color.Gold;
            this.pictureBox42.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox42.Location = new System.Drawing.Point(0, 28);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(803, 4);
            this.pictureBox42.TabIndex = 125;
            this.pictureBox42.TabStop = false;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.SystemColors.Control;
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.btnDetailData3);
            this.panel21.Controls.Add(this.btnExcel6);
            this.panel21.Controls.Add(this.btnNewTest3);
            this.panel21.Controls.Add(this.btnDelete5);
            this.panel21.Controls.Add(this.label22);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(803, 28);
            this.panel21.TabIndex = 1;
            // 
            // btnDetailData3
            // 
            this.btnDetailData3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDetailData3.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Infor;
            this.btnDetailData3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetailData3.Location = new System.Drawing.Point(383, 0);
            this.btnDetailData3.Name = "btnDetailData3";
            this.btnDetailData3.Size = new System.Drawing.Size(70, 26);
            this.btnDetailData3.TabIndex = 13;
            this.btnDetailData3.Text = "상세";
            this.btnDetailData3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetailData3.UseVisualStyleBackColor = true;
            this.btnDetailData3.Click += new System.EventHandler(this.btnDetailData_Click);
            // 
            // btnExcel6
            // 
            this.btnExcel6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcel6.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel6.Location = new System.Drawing.Point(453, 0);
            this.btnExcel6.Name = "btnExcel6";
            this.btnExcel6.Size = new System.Drawing.Size(70, 26);
            this.btnExcel6.TabIndex = 12;
            this.btnExcel6.Text = "엑셀";
            this.btnExcel6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel6.UseVisualStyleBackColor = true;
            this.btnExcel6.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnNewTest3
            // 
            this.btnNewTest3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNewTest3.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.NewPopup;
            this.btnNewTest3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNewTest3.Location = new System.Drawing.Point(523, 0);
            this.btnNewTest3.Name = "btnNewTest3";
            this.btnNewTest3.Size = new System.Drawing.Size(70, 26);
            this.btnNewTest3.TabIndex = 7;
            this.btnNewTest3.Text = "신규";
            this.btnNewTest3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewTest3.UseVisualStyleBackColor = true;
            this.btnNewTest3.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnDelete5
            // 
            this.btnDelete5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete5.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete5.Location = new System.Drawing.Point(593, 0);
            this.btnDelete5.Name = "btnDelete5";
            this.btnDelete5.Size = new System.Drawing.Size(70, 26);
            this.btnDelete5.TabIndex = 8;
            this.btnDelete5.Text = "삭제";
            this.btnDelete5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete5.UseVisualStyleBackColor = true;
            this.btnDelete5.Click += new System.EventHandler(this.btnDelete5_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(6, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(109, 12);
            this.label22.TabIndex = 5;
            this.label22.Text = "월간수질시험관리";
            // 
            // pictureBox43
            // 
            this.pictureBox43.BackColor = System.Drawing.Color.Gold;
            this.pictureBox43.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox43.Location = new System.Drawing.Point(4, 32);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(803, 4);
            this.pictureBox43.TabIndex = 125;
            this.pictureBox43.TabStop = false;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.SystemColors.Control;
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.udtWaterCollectDateE3);
            this.panel22.Controls.Add(this.label23);
            this.panel22.Controls.Add(this.label24);
            this.panel22.Controls.Add(this.cbEstablishment4);
            this.panel22.Controls.Add(this.label25);
            this.panel22.Controls.Add(this.udtWaterCollectDateS3);
            this.panel22.Controls.Add(this.btnQuery6);
            this.panel22.Controls.Add(this.btnClose6);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(4, 4);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(803, 28);
            this.panel22.TabIndex = 122;
            // 
            // udtWaterCollectDateE3
            // 
            this.udtWaterCollectDateE3.Location = new System.Drawing.Point(409, 2);
            this.udtWaterCollectDateE3.Name = "udtWaterCollectDateE3";
            this.udtWaterCollectDateE3.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateE3.TabIndex = 20;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(395, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(15, 12);
            this.label23.TabIndex = 24;
            this.label23.Text = "~";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(6, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 12);
            this.label24.TabIndex = 22;
            this.label24.Text = "사업장 :";
            // 
            // cbEstablishment4
            // 
            this.cbEstablishment4.FormattingEnabled = true;
            this.cbEstablishment4.Location = new System.Drawing.Point(66, 3);
            this.cbEstablishment4.Name = "cbEstablishment4";
            this.cbEstablishment4.Size = new System.Drawing.Size(150, 20);
            this.cbEstablishment4.TabIndex = 21;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(222, 7);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 12);
            this.label25.TabIndex = 18;
            this.label25.Text = "채수일자 :";
            // 
            // udtWaterCollectDateS3
            // 
            this.udtWaterCollectDateS3.Location = new System.Drawing.Point(295, 2);
            this.udtWaterCollectDateS3.Name = "udtWaterCollectDateS3";
            this.udtWaterCollectDateS3.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateS3.TabIndex = 17;
            // 
            // btnQuery6
            // 
            this.btnQuery6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery6.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery6.Location = new System.Drawing.Point(523, 0);
            this.btnQuery6.Name = "btnQuery6";
            this.btnQuery6.Size = new System.Drawing.Size(70, 26);
            this.btnQuery6.TabIndex = 12;
            this.btnQuery6.Text = "조회";
            this.btnQuery6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery6.UseVisualStyleBackColor = true;
            this.btnQuery6.Click += new System.EventHandler(this.btnQuery6_Click);
            // 
            // btnClose6
            // 
            this.btnClose6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose6.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose6.Location = new System.Drawing.Point(593, 0);
            this.btnClose6.Name = "btnClose6";
            this.btnClose6.Size = new System.Drawing.Size(70, 26);
            this.btnClose6.TabIndex = 3;
            this.btnClose6.Text = "닫기";
            this.btnClose6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose6.UseVisualStyleBackColor = true;
            this.btnClose6.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox10.Location = new System.Drawing.Point(807, 4);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(4, 464);
            this.pictureBox10.TabIndex = 121;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox11.Location = new System.Drawing.Point(0, 4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(4, 464);
            this.pictureBox11.TabIndex = 120;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox12.Location = new System.Drawing.Point(0, 468);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(811, 4);
            this.pictureBox12.TabIndex = 119;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox13.Location = new System.Drawing.Point(0, 0);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(811, 4);
            this.pictureBox13.TabIndex = 118;
            this.pictureBox13.TabStop = false;
            // 
            // uTabPageM2S4
            // 
            this.uTabPageM2S4.Controls.Add(this.panel23);
            this.uTabPageM2S4.Controls.Add(this.pictureBox44);
            this.uTabPageM2S4.Controls.Add(this.panel25);
            this.uTabPageM2S4.Controls.Add(this.pictureBox14);
            this.uTabPageM2S4.Controls.Add(this.pictureBox15);
            this.uTabPageM2S4.Controls.Add(this.pictureBox16);
            this.uTabPageM2S4.Controls.Add(this.pictureBox17);
            this.uTabPageM2S4.Location = new System.Drawing.Point(-10000, -10000);
            this.uTabPageM2S4.Name = "uTabPageM2S4";
            this.uTabPageM2S4.Size = new System.Drawing.Size(811, 472);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Transparent;
            this.panel23.Controls.Add(this.uGrid24_1);
            this.panel23.Controls.Add(this.pictureBox45);
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(4, 36);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(803, 432);
            this.panel23.TabIndex = 123;
            // 
            // uGrid24_1
            // 
            appearance133.BackColor = System.Drawing.SystemColors.Window;
            appearance133.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid24_1.DisplayLayout.Appearance = appearance133;
            this.uGrid24_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid24_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance134.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance134.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance134.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance134.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid24_1.DisplayLayout.GroupByBox.Appearance = appearance134;
            appearance135.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid24_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance135;
            this.uGrid24_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance136.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance136.BackColor2 = System.Drawing.SystemColors.Control;
            appearance136.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance136.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid24_1.DisplayLayout.GroupByBox.PromptAppearance = appearance136;
            this.uGrid24_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid24_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance137.BackColor = System.Drawing.SystemColors.Window;
            appearance137.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid24_1.DisplayLayout.Override.ActiveCellAppearance = appearance137;
            appearance138.BackColor = System.Drawing.SystemColors.Highlight;
            appearance138.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid24_1.DisplayLayout.Override.ActiveRowAppearance = appearance138;
            this.uGrid24_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid24_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance139.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid24_1.DisplayLayout.Override.CardAreaAppearance = appearance139;
            appearance140.BorderColor = System.Drawing.Color.Silver;
            appearance140.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid24_1.DisplayLayout.Override.CellAppearance = appearance140;
            this.uGrid24_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid24_1.DisplayLayout.Override.CellPadding = 0;
            appearance141.BackColor = System.Drawing.SystemColors.Control;
            appearance141.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance141.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance141.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance141.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid24_1.DisplayLayout.Override.GroupByRowAppearance = appearance141;
            appearance142.TextHAlignAsString = "Left";
            this.uGrid24_1.DisplayLayout.Override.HeaderAppearance = appearance142;
            this.uGrid24_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid24_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance143.BackColor = System.Drawing.SystemColors.Window;
            appearance143.BorderColor = System.Drawing.Color.Silver;
            this.uGrid24_1.DisplayLayout.Override.RowAppearance = appearance143;
            this.uGrid24_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance144.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid24_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance144;
            this.uGrid24_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid24_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid24_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid24_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid24_1.Location = new System.Drawing.Point(0, 32);
            this.uGrid24_1.Name = "uGrid24_1";
            this.uGrid24_1.Size = new System.Drawing.Size(803, 400);
            this.uGrid24_1.TabIndex = 4;
            this.uGrid24_1.Text = "ultraGrid1";
            this.uGrid24_1.Click += new System.EventHandler(this.uGrid_Click);
            this.uGrid24_1.DoubleClick += new System.EventHandler(this.uGrid2N_1_DoubleClick);
            // 
            // pictureBox45
            // 
            this.pictureBox45.BackColor = System.Drawing.Color.Gold;
            this.pictureBox45.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox45.Location = new System.Drawing.Point(0, 28);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(803, 4);
            this.pictureBox45.TabIndex = 125;
            this.pictureBox45.TabStop = false;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.SystemColors.Control;
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.btnDetailData4);
            this.panel24.Controls.Add(this.btnExcel7);
            this.panel24.Controls.Add(this.btnNewTest4);
            this.panel24.Controls.Add(this.btnDelete6);
            this.panel24.Controls.Add(this.label27);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(803, 28);
            this.panel24.TabIndex = 1;
            // 
            // btnDetailData4
            // 
            this.btnDetailData4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDetailData4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Infor;
            this.btnDetailData4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetailData4.Location = new System.Drawing.Point(383, 0);
            this.btnDetailData4.Name = "btnDetailData4";
            this.btnDetailData4.Size = new System.Drawing.Size(70, 26);
            this.btnDetailData4.TabIndex = 13;
            this.btnDetailData4.Text = "상세";
            this.btnDetailData4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetailData4.UseVisualStyleBackColor = true;
            this.btnDetailData4.Click += new System.EventHandler(this.btnDetailData_Click);
            // 
            // btnExcel7
            // 
            this.btnExcel7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcel7.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel7.Location = new System.Drawing.Point(453, 0);
            this.btnExcel7.Name = "btnExcel7";
            this.btnExcel7.Size = new System.Drawing.Size(70, 26);
            this.btnExcel7.TabIndex = 12;
            this.btnExcel7.Text = "엑셀";
            this.btnExcel7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel7.UseVisualStyleBackColor = true;
            this.btnExcel7.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnNewTest4
            // 
            this.btnNewTest4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNewTest4.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.NewPopup;
            this.btnNewTest4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNewTest4.Location = new System.Drawing.Point(523, 0);
            this.btnNewTest4.Name = "btnNewTest4";
            this.btnNewTest4.Size = new System.Drawing.Size(70, 26);
            this.btnNewTest4.TabIndex = 7;
            this.btnNewTest4.Text = "신규";
            this.btnNewTest4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewTest4.UseVisualStyleBackColor = true;
            this.btnNewTest4.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnDelete6
            // 
            this.btnDelete6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete6.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete6.Location = new System.Drawing.Point(593, 0);
            this.btnDelete6.Name = "btnDelete6";
            this.btnDelete6.Size = new System.Drawing.Size(70, 26);
            this.btnDelete6.TabIndex = 8;
            this.btnDelete6.Text = "삭제";
            this.btnDelete6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete6.UseVisualStyleBackColor = true;
            this.btnDelete6.Click += new System.EventHandler(this.btnDelete6_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(6, 7);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(109, 12);
            this.label27.TabIndex = 5;
            this.label27.Text = "분기수질시험관리";
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackColor = System.Drawing.Color.Gold;
            this.pictureBox44.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox44.Location = new System.Drawing.Point(4, 32);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(803, 4);
            this.pictureBox44.TabIndex = 125;
            this.pictureBox44.TabStop = false;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.Control;
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.udtWaterCollectDateE4);
            this.panel25.Controls.Add(this.label28);
            this.panel25.Controls.Add(this.label29);
            this.panel25.Controls.Add(this.cbEstablishment5);
            this.panel25.Controls.Add(this.label30);
            this.panel25.Controls.Add(this.udtWaterCollectDateS4);
            this.panel25.Controls.Add(this.btnQuery7);
            this.panel25.Controls.Add(this.btnClose7);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(4, 4);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(803, 28);
            this.panel25.TabIndex = 122;
            // 
            // udtWaterCollectDateE4
            // 
            this.udtWaterCollectDateE4.Location = new System.Drawing.Point(409, 2);
            this.udtWaterCollectDateE4.Name = "udtWaterCollectDateE4";
            this.udtWaterCollectDateE4.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateE4.TabIndex = 20;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(395, 7);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 12);
            this.label28.TabIndex = 24;
            this.label28.Text = "~";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(6, 7);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 12);
            this.label29.TabIndex = 22;
            this.label29.Text = "사업장 :";
            // 
            // cbEstablishment5
            // 
            this.cbEstablishment5.FormattingEnabled = true;
            this.cbEstablishment5.Location = new System.Drawing.Point(66, 3);
            this.cbEstablishment5.Name = "cbEstablishment5";
            this.cbEstablishment5.Size = new System.Drawing.Size(150, 20);
            this.cbEstablishment5.TabIndex = 21;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(222, 7);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(67, 12);
            this.label30.TabIndex = 18;
            this.label30.Text = "채수일자 :";
            // 
            // udtWaterCollectDateS4
            // 
            this.udtWaterCollectDateS4.Location = new System.Drawing.Point(295, 2);
            this.udtWaterCollectDateS4.Name = "udtWaterCollectDateS4";
            this.udtWaterCollectDateS4.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateS4.TabIndex = 17;
            // 
            // btnQuery7
            // 
            this.btnQuery7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery7.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery7.Location = new System.Drawing.Point(523, 0);
            this.btnQuery7.Name = "btnQuery7";
            this.btnQuery7.Size = new System.Drawing.Size(70, 26);
            this.btnQuery7.TabIndex = 12;
            this.btnQuery7.Text = "조회";
            this.btnQuery7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery7.UseVisualStyleBackColor = true;
            this.btnQuery7.Click += new System.EventHandler(this.btnQuery7_Click_1);
            // 
            // btnClose7
            // 
            this.btnClose7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose7.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose7.Location = new System.Drawing.Point(593, 0);
            this.btnClose7.Name = "btnClose7";
            this.btnClose7.Size = new System.Drawing.Size(70, 26);
            this.btnClose7.TabIndex = 3;
            this.btnClose7.Text = "닫기";
            this.btnClose7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose7.UseVisualStyleBackColor = true;
            this.btnClose7.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox14.Location = new System.Drawing.Point(807, 4);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(4, 464);
            this.pictureBox14.TabIndex = 121;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox15.Location = new System.Drawing.Point(0, 4);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(4, 464);
            this.pictureBox15.TabIndex = 120;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox16.Location = new System.Drawing.Point(0, 468);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(811, 4);
            this.pictureBox16.TabIndex = 119;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox17.Location = new System.Drawing.Point(0, 0);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(811, 4);
            this.pictureBox17.TabIndex = 118;
            this.pictureBox17.TabStop = false;
            // 
            // uTabPageM2S5
            // 
            this.uTabPageM2S5.Controls.Add(this.panel26);
            this.uTabPageM2S5.Controls.Add(this.pictureBox46);
            this.uTabPageM2S5.Controls.Add(this.panel28);
            this.uTabPageM2S5.Controls.Add(this.pictureBox18);
            this.uTabPageM2S5.Controls.Add(this.pictureBox19);
            this.uTabPageM2S5.Controls.Add(this.pictureBox20);
            this.uTabPageM2S5.Controls.Add(this.pictureBox21);
            this.uTabPageM2S5.Location = new System.Drawing.Point(-10000, -10000);
            this.uTabPageM2S5.Name = "uTabPageM2S5";
            this.uTabPageM2S5.Size = new System.Drawing.Size(811, 472);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Transparent;
            this.panel26.Controls.Add(this.uGrid25_1);
            this.panel26.Controls.Add(this.pictureBox47);
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(4, 36);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(803, 432);
            this.panel26.TabIndex = 123;
            // 
            // uGrid25_1
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid25_1.DisplayLayout.Appearance = appearance1;
            this.uGrid25_1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid25_1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid25_1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid25_1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGrid25_1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid25_1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGrid25_1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid25_1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid25_1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid25_1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGrid25_1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid25_1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid25_1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid25_1.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGrid25_1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid25_1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid25_1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGrid25_1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGrid25_1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid25_1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGrid25_1.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGrid25_1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid25_1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGrid25_1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid25_1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid25_1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid25_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid25_1.Location = new System.Drawing.Point(0, 32);
            this.uGrid25_1.Name = "uGrid25_1";
            this.uGrid25_1.Size = new System.Drawing.Size(803, 400);
            this.uGrid25_1.TabIndex = 4;
            this.uGrid25_1.Text = "ultraGrid1";
            this.uGrid25_1.Click += new System.EventHandler(this.uGrid_Click);
            this.uGrid25_1.DoubleClick += new System.EventHandler(this.uGrid2N_1_DoubleClick);
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackColor = System.Drawing.Color.Gold;
            this.pictureBox47.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox47.Location = new System.Drawing.Point(0, 28);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(803, 4);
            this.pictureBox47.TabIndex = 125;
            this.pictureBox47.TabStop = false;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.SystemColors.Control;
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.btnDetailData5);
            this.panel27.Controls.Add(this.btnExcel8);
            this.panel27.Controls.Add(this.btnNewTest5);
            this.panel27.Controls.Add(this.btnDelete7);
            this.panel27.Controls.Add(this.label32);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(803, 28);
            this.panel27.TabIndex = 1;
            // 
            // btnDetailData5
            // 
            this.btnDetailData5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDetailData5.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Infor;
            this.btnDetailData5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetailData5.Location = new System.Drawing.Point(383, 0);
            this.btnDetailData5.Name = "btnDetailData5";
            this.btnDetailData5.Size = new System.Drawing.Size(70, 26);
            this.btnDetailData5.TabIndex = 13;
            this.btnDetailData5.Text = "상세";
            this.btnDetailData5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetailData5.UseVisualStyleBackColor = true;
            this.btnDetailData5.Click += new System.EventHandler(this.btnDetailData_Click);
            // 
            // btnExcel8
            // 
            this.btnExcel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcel8.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Excel;
            this.btnExcel8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcel8.Location = new System.Drawing.Point(453, 0);
            this.btnExcel8.Name = "btnExcel8";
            this.btnExcel8.Size = new System.Drawing.Size(70, 26);
            this.btnExcel8.TabIndex = 12;
            this.btnExcel8.Text = "엑셀";
            this.btnExcel8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel8.UseVisualStyleBackColor = true;
            this.btnExcel8.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnNewTest5
            // 
            this.btnNewTest5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNewTest5.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.NewPopup;
            this.btnNewTest5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNewTest5.Location = new System.Drawing.Point(523, 0);
            this.btnNewTest5.Name = "btnNewTest5";
            this.btnNewTest5.Size = new System.Drawing.Size(70, 26);
            this.btnNewTest5.TabIndex = 7;
            this.btnNewTest5.Text = "신규";
            this.btnNewTest5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewTest5.UseVisualStyleBackColor = true;
            this.btnNewTest5.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnDelete7
            // 
            this.btnDelete7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete7.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Delete;
            this.btnDelete7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete7.Location = new System.Drawing.Point(593, 0);
            this.btnDelete7.Name = "btnDelete7";
            this.btnDelete7.Size = new System.Drawing.Size(70, 26);
            this.btnDelete7.TabIndex = 8;
            this.btnDelete7.Text = "삭제";
            this.btnDelete7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete7.UseVisualStyleBackColor = true;
            this.btnDelete7.Click += new System.EventHandler(this.btnDelete7_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.Location = new System.Drawing.Point(6, 7);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(109, 12);
            this.label32.TabIndex = 5;
            this.label32.Text = "반기수질시험관리";
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackColor = System.Drawing.Color.Gold;
            this.pictureBox46.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox46.Location = new System.Drawing.Point(4, 32);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(803, 4);
            this.pictureBox46.TabIndex = 125;
            this.pictureBox46.TabStop = false;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.SystemColors.Control;
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.udtWaterCollectDateE5);
            this.panel28.Controls.Add(this.label33);
            this.panel28.Controls.Add(this.label34);
            this.panel28.Controls.Add(this.cbEstablishment6);
            this.panel28.Controls.Add(this.label35);
            this.panel28.Controls.Add(this.udtWaterCollectDateS5);
            this.panel28.Controls.Add(this.btnQuery8);
            this.panel28.Controls.Add(this.btnClose8);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(4, 4);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(803, 28);
            this.panel28.TabIndex = 122;
            // 
            // udtWaterCollectDateE5
            // 
            this.udtWaterCollectDateE5.Location = new System.Drawing.Point(409, 2);
            this.udtWaterCollectDateE5.Name = "udtWaterCollectDateE5";
            this.udtWaterCollectDateE5.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateE5.TabIndex = 20;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.Location = new System.Drawing.Point(395, 7);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(15, 12);
            this.label33.TabIndex = 24;
            this.label33.Text = "~";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.Location = new System.Drawing.Point(6, 7);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 12);
            this.label34.TabIndex = 22;
            this.label34.Text = "사업장 :";
            // 
            // cbEstablishment6
            // 
            this.cbEstablishment6.FormattingEnabled = true;
            this.cbEstablishment6.Location = new System.Drawing.Point(66, 3);
            this.cbEstablishment6.Name = "cbEstablishment6";
            this.cbEstablishment6.Size = new System.Drawing.Size(150, 20);
            this.cbEstablishment6.TabIndex = 21;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.Location = new System.Drawing.Point(222, 7);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(67, 12);
            this.label35.TabIndex = 18;
            this.label35.Text = "채수일자 :";
            // 
            // udtWaterCollectDateS5
            // 
            this.udtWaterCollectDateS5.Location = new System.Drawing.Point(295, 2);
            this.udtWaterCollectDateS5.Name = "udtWaterCollectDateS5";
            this.udtWaterCollectDateS5.Size = new System.Drawing.Size(100, 21);
            this.udtWaterCollectDateS5.TabIndex = 17;
            // 
            // btnQuery8
            // 
            this.btnQuery8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery8.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Query;
            this.btnQuery8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery8.Location = new System.Drawing.Point(523, 0);
            this.btnQuery8.Name = "btnQuery8";
            this.btnQuery8.Size = new System.Drawing.Size(70, 26);
            this.btnQuery8.TabIndex = 12;
            this.btnQuery8.Text = "조회";
            this.btnQuery8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery8.UseVisualStyleBackColor = true;
            this.btnQuery8.Click += new System.EventHandler(this.btnQuery8_Click);
            // 
            // btnClose8
            // 
            this.btnClose8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose8.Image = global::WaterNet.WQ_GumsaData.Properties.Resources.Close2;
            this.btnClose8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose8.Location = new System.Drawing.Point(593, 0);
            this.btnClose8.Name = "btnClose8";
            this.btnClose8.Size = new System.Drawing.Size(70, 26);
            this.btnClose8.TabIndex = 3;
            this.btnClose8.Text = "닫기";
            this.btnClose8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose8.UseVisualStyleBackColor = true;
            this.btnClose8.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox18.Location = new System.Drawing.Point(807, 4);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(4, 464);
            this.pictureBox18.TabIndex = 121;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox19.Location = new System.Drawing.Point(0, 4);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(4, 464);
            this.pictureBox19.TabIndex = 120;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox20.Location = new System.Drawing.Point(0, 468);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(811, 4);
            this.pictureBox20.TabIndex = 119;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox21.Location = new System.Drawing.Point(0, 0);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(811, 4);
            this.pictureBox21.TabIndex = 118;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(4, 429);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(678, 4);
            this.pictureBox4.TabIndex = 117;
            this.pictureBox4.TabStop = false;
            // 
            // picFrRightM2
            // 
            this.picFrRightM2.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightM2.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightM2.Location = new System.Drawing.Point(682, 4);
            this.picFrRightM2.Name = "picFrRightM2";
            this.picFrRightM2.Size = new System.Drawing.Size(4, 429);
            this.picFrRightM2.TabIndex = 116;
            this.picFrRightM2.TabStop = false;
            // 
            // picFrLeftM2
            // 
            this.picFrLeftM2.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM2.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM2.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftM2.Name = "picFrLeftM2";
            this.picFrLeftM2.Size = new System.Drawing.Size(4, 429);
            this.picFrLeftM2.TabIndex = 115;
            this.picFrLeftM2.TabStop = false;
            // 
            // picFrTopM2
            // 
            this.picFrTopM2.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopM2.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopM2.Location = new System.Drawing.Point(0, 0);
            this.picFrTopM2.Name = "picFrTopM2";
            this.picFrTopM2.Size = new System.Drawing.Size(686, 4);
            this.picFrTopM2.TabIndex = 114;
            this.picFrTopM2.TabStop = false;
            // 
            // uTabMenuS2
            // 
            this.uTabMenuS2.Controls.Add(this.ultraTabSharedControlsPage3);
            this.uTabMenuS2.Controls.Add(this.uTabPageM2S1);
            this.uTabMenuS2.Controls.Add(this.uTabPageM2S2);
            this.uTabMenuS2.Controls.Add(this.uTabPageM2S3);
            this.uTabMenuS2.Controls.Add(this.uTabPageM2S4);
            this.uTabMenuS2.Controls.Add(this.uTabPageM2S5);
            this.uTabMenuS2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabMenuS2.Location = new System.Drawing.Point(4, 4);
            this.uTabMenuS2.Name = "uTabMenuS2";
            this.uTabMenuS2.SharedControlsPage = this.ultraTabSharedControlsPage3;
            this.uTabMenuS2.Size = new System.Drawing.Size(678, 425);
            this.uTabMenuS2.TabIndex = 118;
            ultraTab7.TabPage = this.uTabPageM2S1;
            ultraTab7.Text = "일일수질시험관리";
            ultraTab8.TabPage = this.uTabPageM2S2;
            ultraTab8.Text = "주간수질시험관리";
            ultraTab9.TabPage = this.uTabPageM2S3;
            ultraTab9.Text = "월간수질시험관리";
            ultraTab13.TabPage = this.uTabPageM2S4;
            ultraTab13.Text = "분기수질시험관리";
            ultraTab14.TabPage = this.uTabPageM2S5;
            ultraTab14.Text = "반기수질시험관리";
            this.uTabMenuS2.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab7,
            ultraTab8,
            ultraTab9,
            ultraTab13,
            ultraTab14});
            this.uTabMenuS2.Click += new System.EventHandler(this.btnQuery7_Click);
            // 
            // ultraTabSharedControlsPage3
            // 
            this.ultraTabSharedControlsPage3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage3.Name = "ultraTabSharedControlsPage3";
            this.ultraTabSharedControlsPage3.Size = new System.Drawing.Size(674, 399);
            // 
            // frmWQTestResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 433);
            this.Controls.Add(this.uTabMenuS2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.picFrRightM2);
            this.Controls.Add(this.picFrLeftM2);
            this.Controls.Add(this.picFrTopM2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmWQTestResult";
            this.Text = "수질시험결과관리";
            this.Load += new System.EventHandler(this.frmWQTestResult_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQTestResult_FormClosing);
            this.uTabPageM2S1.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid21_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.uTabPageM2S2.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid22_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.uTabPageM2S3.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid23_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.uTabPageM2S4.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid24_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.uTabPageM2S5.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid25_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateE5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtWaterCollectDateS5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenuS2)).EndInit();
            this.uTabMenuS2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox picFrRightM2;
        private System.Windows.Forms.PictureBox picFrLeftM2;
        private System.Windows.Forms.PictureBox picFrTopM2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenuS2;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM2S1;
        private System.Windows.Forms.Panel panel14;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid21_1;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnDetailData1;
        private System.Windows.Forms.Button btnExcel4;
        private System.Windows.Forms.Button btnNewTest1;
        private System.Windows.Forms.Button btnDelete3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.Panel panel16;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateE1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbEstablishment2;
        private System.Windows.Forms.Label label15;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateS1;
        private System.Windows.Forms.Button btnQuery4;
        private System.Windows.Forms.Button btnClose4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM2S2;
        private System.Windows.Forms.Panel panel17;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid22_1;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnDetailData2;
        private System.Windows.Forms.Button btnExcel5;
        private System.Windows.Forms.Button btnNewTest2;
        private System.Windows.Forms.Button btnDelete4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.Panel panel19;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateE2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbEstablishment3;
        private System.Windows.Forms.Label label20;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateS2;
        private System.Windows.Forms.Button btnQuery5;
        private System.Windows.Forms.Button btnClose5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM2S3;
        private System.Windows.Forms.Panel panel20;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid23_1;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnDetailData3;
        private System.Windows.Forms.Button btnExcel6;
        private System.Windows.Forms.Button btnNewTest3;
        private System.Windows.Forms.Button btnDelete5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.Panel panel22;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateE3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbEstablishment4;
        private System.Windows.Forms.Label label25;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateS3;
        private System.Windows.Forms.Button btnQuery6;
        private System.Windows.Forms.Button btnClose6;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM2S4;
        private System.Windows.Forms.Panel panel23;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid24_1;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Button btnDetailData4;
        private System.Windows.Forms.Button btnExcel7;
        private System.Windows.Forms.Button btnNewTest4;
        private System.Windows.Forms.Button btnDelete6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.Panel panel25;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateE4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cbEstablishment5;
        private System.Windows.Forms.Label label30;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateS4;
        private System.Windows.Forms.Button btnQuery7;
        private System.Windows.Forms.Button btnClose7;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl uTabPageM2S5;
        private System.Windows.Forms.Panel panel26;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid25_1;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Button btnDetailData5;
        private System.Windows.Forms.Button btnExcel8;
        private System.Windows.Forms.Button btnNewTest5;
        private System.Windows.Forms.Button btnDelete7;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.Panel panel28;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateE5;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbEstablishment6;
        private System.Windows.Forms.Label label35;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtWaterCollectDateS5;
        private System.Windows.Forms.Button btnQuery8;
        private System.Windows.Forms.Button btnClose8;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
    }
}