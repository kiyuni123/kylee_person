﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_GumsaData.FormPopup
{
    public partial class frmWQFaucet : Form
    {
        private OracleDBManager m_oDBManager = null;

        #region 맵 프로퍼티

        private IMapControl3 _Map;

        public IMapControl3 IMAP
        {
            get
            {
                return _Map;
            }
            set
            {
                _Map = value;
            }
        }

        #endregion

        public frmWQFaucet()
        {
            WQ_AppStatic.IS_SHOW_FORM_FAUCET = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                WQ_AppStatic.IS_SHOW_FORM_FAUCET = false;
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            // TODO
            InitializeGridSetting();
            InitializeControlSetting();
        }

        /// <summary>
        /// frmWQMain의 UltraTab Control에 종속된 UltraGrid의 Header 등 기본 정보를 Setting한다.
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            #region 수도꼭지관리 Grid Set

            #region 수도꼭지관리/수도꼭지지점관리

            oUltraGridColumn = uGrid31_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid31_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid31_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMSEARCH";
            oUltraGridColumn.Header.Caption = "찾기";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            oUltraGridColumn.CellButtonAppearance.Image = WQ_GumsaData.Properties.Resources.Find;
            oUltraGridColumn.CellButtonAppearance.ImageHAlign = HAlign.Center;
            oUltraGridColumn.CellButtonAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid31_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNM";
            oUltraGridColumn.Header.Caption = "수용가명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid31_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid31_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TLNO";
            oUltraGridColumn.Header.Caption = "전화번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid31_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REGDT";
            oUltraGridColumn.Header.Caption = "등록일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid31_1);

            uGrid31_1.DisplayLayout.Bands[0].Columns[2].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;

            #endregion

            #region 수도꼭지관리/수도꼭지시험관리

            oUltraGridColumn = uGrid32_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SITECD";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid32_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid32_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNM";
            oUltraGridColumn.Header.Caption = "수용가명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = uGrid32_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CLTDT";
            oUltraGridColumn.Header.Caption = "채수일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid32_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REGMNGR";
            oUltraGridColumn.Header.Caption = "채수자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGrid32_1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ITEMGRP";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGrid32_1);

            #endregion

            #endregion

            #region Grid Set -AutoResizeColumes

            FormManager.SetGridStyle_PerformAutoResize(this.uGrid31_1);
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid32_1);

            #endregion

            #region Grid Set - AffterRowInsert, AfterCellUpdate

            this.uGrid31_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid31_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.uGrid32_1.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.uGrid32_1.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);

            #endregion

        }

        /// <summary>
        /// frmWQMain에 종속된 Control중 UltraGrid를 제외한 다른 Control에 대해 기본 정보를 Setting한다.
        /// </summary>
        private void InitializeControlSetting()
        {
            WQ_Function.SetCombo_Establishment(this.cbEstablishment7, 0);
            WQ_Function.SetCombo_Establishment(this.cbEstablishment8, 0);
            WQ_Function.SetCombo_Division(this.cbDivisionName, EMFrame.statics.AppStatic.USER_SGCNM + " (" + EMFrame.statics.AppStatic.USER_SGCCD + ")");

            WQ_Function.SetUDateTime_MaskInput(this.udtWaterCollectDateS6, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtWaterCollectDateS6, -30);
        }
        
        private void frmWQFaucet_Load(object sender, EventArgs e)
        {
            //수도꼭지 DATA 조회
            this.GetFaucetTestData();
            this.GetFaucetPostionData();

            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================수질관리(수도꼭지시험결과관리)

            object o = EMFrame.statics.AppStatic.USER_MENU["수질검사자료관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
               //수도꼭지 지점관리 탭
                this.btnAppend4.Enabled = false;
                this.btnSave5.Enabled = false;
                this.btnDelete8.Enabled = false;

                //수도꼭지 시험관리 탭
                this.btnDelete9.Enabled = false;

            }

            //===========================================================================

        }

        private void frmWQFaucet_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_FAUCET = false;
        }


        #region Button Events

        /// <summary>
        /// 다수의 btnSearch# 들이 사용하는 이벤트 프로시져
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.OpenToSearchPopup(this.uTabMenuS3.SelectedTab.Index);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.CloseForm();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            this.ExportToExcelFromUltraGrid(this.uTabMenuS3.SelectedTab.Index);
        }

        #region - 수도꼭지관리/수도꼭지지점관리

        private void btnAppend4_Click(object sender, EventArgs e)
        {
            WaterNetCore.FormManager.SetGridStyle_Append(this.uGrid31_1);
            FormManager.SetGridStyle_ColumeNoEdit(this.uGrid31_1, "SITECD");
        }

        private void btnQuery9_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetFaucetPostionData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnSave5_Click(object sender, EventArgs e)
        {
            if (this.uGrid31_1.Rows.Count > 0)
            {
                this.SaveFaucetPostionData();
                this.GetFaucetPostionData();
                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDelete8_Click(object sender, EventArgs e)
        {
            if (this.uGrid31_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    this.DeleteFaucetPostionData();
                    this.GetFaucetPostionData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #region - 수도꼭지관리/수도꼭지시험관리

        private void btnDetailData_Click(object sender, EventArgs e)
        {
            this.OpenToTestPopup(1);
        }

        /// <summary>
        /// 다수의 btnNew# 들이 사용하는 이벤트 프로시져
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewTest_Click(object sender, EventArgs e)
        {
            this.OpenToTestPopup(0);
        }

        private void btnQuery10_Click(object sender, EventArgs e)
        {
            this.GetFaucetTestData();
        }

        private void btnDelete9_Click(object sender, EventArgs e)
        {
            if (this.uGrid32_1.Rows.Count > 0)
            {
                DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oDRtn == DialogResult.Yes)
                {
                    this.DeleteFaucetTestData();
                    this.GetFaucetTestData();
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        
        #endregion
        
        #endregion

        #region Control Events

        #region - 수도꼭지관리/수도꼭지지점관리

        private void uGrid31_1_ClickCellButton(object sender, CellEventArgs e)
        {
            this.OpenToSearchPopup(this.uTabMenuS3.SelectedTab.Index);
        }

        #endregion

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid oUGrid = null;

            string strDMNO = string.Empty;
            int iSTabIndex = this.uTabMenuS3.SelectedTab.Index;

            if (_Map == null) return;

            switch (iSTabIndex)
            {
                case 0: // 수질관리/일일수질시험관리
                    oUGrid = this.uGrid31_1;
                    break;

                case 1: // 수질관리/주간수질시험관리
                    oUGrid = this.uGrid32_1;
                    break;
            }

            if (oUGrid.Rows.Count <= 0) return;

            strDMNO = oUGrid.ActiveRow.Cells[1].Text;

            ILayer pLayer = WaterAOCore.ArcManager.GetMapLayer(_Map, "수도계량기");

            IFeature pFeature = ArcManager.GetFeature(pLayer, "DMNO='" + strDMNO + "'");

            if (pFeature != null)
            {
                if (ArcManager.GetMapScale(_Map) > (double)500)
                {
                    ArcManager.SetMapScale(_Map, (double)500);
                }

                ArcManager.MoveCenterAt(_Map, pFeature);

                //_Map.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);

                ArcManager.FlashShape(_Map.ActiveView, (IGeometry)pFeature.Shape, 10);

                ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();
            }
        }

        #endregion

        #region User Function

        /// <summary>
        /// 현재 창을 닫는다.
        /// </summary>
        private void CloseForm()
        {
            WQ_AppStatic.IS_SHOW_FORM_FAUCET = false;
            if (m_oDBManager != null)
            {
                m_oDBManager.Close();
            }
            this.Dispose();
            this.Close();
        }

        /// <summary>
        /// Open to Search Popup (수용가 검색 : FormPopup.frmWQPopup2)
        /// </summary>
        /// <param name="iSTabIndex">uTabMenuS3의 SelectedTab.Index</param>
        private void OpenToSearchPopup(int iSTabIndex)
        {
            WaterNet.WQ_GumsaData.FormPopup.frmWQPConsumer oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQPConsumer();

            switch (iSTabIndex)
            {
                case 0: // 수도꼭지관리/수도꼭지지점관리
                    oForm.StartPosition = FormStartPosition.Manual;
                    //Ugrid @ 버튼 아래 위치하게 위치 고정
                    oForm.Top = 280;
                    oForm.Left = 298;
                    oForm.Show();
                    this.txtConsumer1.Text = oForm.Consumer;
                    break;

                case 1: // 수도꼭지관리/수도꼭지시험관리
                    oForm.StartPosition = FormStartPosition.CenterScreen;
                    oForm.ShowDialog();
                    this.txtConsumer2.Text = oForm.Consumer;
                    break;
            }
        }

        /// <summary>
        /// Open to New Test Popup (수질 또는 수도꼭지시험관리 : FormPopup.frmWQPopup1 or FormPopup.frmWQPopup3)
        /// </summary>
         /// <param name="iNDType">수질시험관리 : 0 - 신규, 1 - 상세 구분, 수도꼭지시험관리 : 0 - 신규</param>
        private void OpenToTestPopup(int iNDType)
        {
            try
            {
                string strNDTitle = string.Empty;

                if (iNDType == 0)
                {
                    strNDTitle = "신규";
                }
                else
                {
                    strNDTitle = "상세";
                }
                                
                if (iNDType == 1 && this.uGrid32_1.Rows.Count == 0) return;

                WaterNet.WQ_GumsaData.FormPopup.frmWQPFaucetTestItem oForm = new WaterNet.WQ_GumsaData.FormPopup.frmWQPFaucetTestItem();

                oForm.Title = this.uTabMenuS3.SelectedTab.Text.ToString() + strNDTitle;

                // 수도꼭지관리/수도꼭지시험관리

                oForm.Establishment = this.cbEstablishment8.Text.ToString();

                if (iNDType != 0)
                {
                    oForm.Consumer = this.uGrid32_1.ActiveRow.Cells[1].Text.ToString();
                    oForm.WaterCollectDate = this.uGrid32_1.ActiveRow.Cells[3].Text;
                }
                else
                {
                    oForm.Consumer = this.txtConsumer2.Text.ToString();
                    oForm.WaterCollectDate = DateTime.Now.ToString();
                }

                oForm.ShowDialog();
            }
            catch (Exception ex)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "OpenToTestPopup()", ex.Message.ToString(), "");
            }
        }
        
        /// <summary>
        /// UltraGrid에서 Excel로 Export 한다.
        /// </summary>
        /// <param name="iMTabIndex">uTabMenuM의 SelectedTab.Index</param>
        private void ExportToExcelFromUltraGrid(int iMTabIndex)
        {
            try
            {
                string strTitle = string.Empty;

                UltraGrid oUGrid = new UltraGrid();
                UltraGrid oUGrid2 = new UltraGrid();
                strTitle = this.uTabMenuS3.SelectedTab.Text.ToString();

                switch (iMTabIndex)
                {
                    case 0: // 수질관리/일일수질시험관리
                        oUGrid = this.uGrid31_1;
                        break;

                    case 1: // 수질관리/주간수질시험관리
                        oUGrid = this.uGrid32_1;
                        break;
                }

                if (oUGrid.Rows.Count <= 0) return;
                FormManager.ExportToExcelFromUltraGrid(oUGrid, strTitle);
            }
            catch (Exception ex)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExportToExcelFromUltraGrid()", ex.Message.ToString(), "");
            }
        }

        #region - 수도꼭지관리/수도꼭지지점관리

        /// <summary>
        /// 수도꼭지지점 Data를 Select해서 Grid에 Set한다.
        /// </summary>
        private void GetFaucetPostionData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITE = string.Empty;
            string strSITECD = string.Empty;
            string strDPTCD = string.Empty;
            string strDMNO = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment7.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strDPTCD = this.cbDivisionName.Text.ToString();
            strDPTCD = WQ_Function.SplitToCode(strDPTCD);
            strDMNO = this.txtConsumer1.Text.ToString();

            strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND DPTCD = '" + strDPTCD + "' AND DMNO LIKE '%" + strDMNO + "%'";
            oStringBuilder.AppendLine("SELECT   '" + strSITE + "' AS SITECD, DMNO, '' AS DMSEARCH, DMNM, ADDR1 AS ADDR, TLNO, REGDT");
            oStringBuilder.AppendLine("FROM     WQCTP");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY SITECD, DMNO");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTP");

            this.uGrid31_1.DataSource = pDS.Tables["WQCTP"].DefaultView;

            //첫번째 Row에 Activate 부여
            if (this.uGrid31_1.Rows.Count > 0) this.uGrid31_1.Rows[0].Activated = true;

            FormManager.SetGridStyle_ColumeNoEdit(this.uGrid31_1, "SITECD");
            FormManager.SetGridStyle_SetDefaultCellValue(this.uGrid31_1, "SITECD", this.cbEstablishment7.Text);
            FormManager.SetGridStyle_SetDefaultCellValue(this.uGrid31_1, "DMSEARCH", "@");
            FormManager.SetGridStyle_SetDefaultCellValue(this.uGrid31_1, "REGDT", DateTime.Today.ToString());
            FormManager.SetGridStyle_UnAppend(this.uGrid31_1);

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid31_1);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// Grid에 Append된 수도꼭지지점 Data를 Insert
        /// </summary>
        private void SaveFaucetPostionData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strSITECD = string.Empty;
            string strDPTCD = string.Empty;
            string strDPTNM = string.Empty;
            string strDMNO = string.Empty;
            string strDMNM = string.Empty;
            string strADDR = string.Empty;
            string strTLNO = string.Empty;
            string strSPOT = string.Empty;

            DataSet pDS = new DataSet();

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid31_1.ActiveRow.Tag != null && this.uGrid31_1.ActiveRow.Tag.ToString() == "I")
            {
                strSITECD = this.cbEstablishment7.Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strDPTCD = this.cbDivisionName.Text.ToString();
                strDPTNM = WQ_Function.SplitToCodeName(strDPTCD);
                strDPTCD = WQ_Function.SplitToCode(strDPTCD);
                strDMNO = this.uGrid31_1.ActiveRow.Cells[1].Text.ToString();
                strDMNM = this.uGrid31_1.ActiveRow.Cells[3].Text.ToString();
                strADDR = this.uGrid31_1.ActiveRow.Cells[4].Text.ToString();
                strTLNO = this.uGrid31_1.ActiveRow.Cells[5].Text.ToString();
                strSPOT = "1";

                strParam = "('" + EMFrame.statics.AppStatic.USER_SGCCD + "', '" + strDMNO + "', '" + strSPOT + "', '" + strDMNM + "', '', '', '', '', '" + strADDR + "', '', '" + strTLNO + "', SYSDATE, '', 'Y', '" + strSITECD + "', '" + strDPTCD + "', '" + strDPTNM + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "')";
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT   INTO    WQCTP");
                oStringBuilder.AppendLine("(SGCCD, DMNO, SPOT, DMNM, FREQUENCY, OBJECT, REGTYPE, PSTNO, ADDR1, ADDR2, TLNO, REGDT, WPPESTS, USEYN, SITECD, DPTCD, DPTNM, REGMNGR, REGMNGRIP)");
                oStringBuilder.AppendLine("VALUES");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }

            FormManager.SetGridStyle_UnAppend(this.uGrid31_1);
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteFaucetPostionData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strSITECD = string.Empty;
            string strDPTCD = string.Empty;
            string strDMNO = string.Empty;

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid31_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid31_1.ActiveRow.Cells[0].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strDPTCD = this.cbDivisionName.Text.Trim();
                strDPTCD = WQ_Function.SplitToCode(strDPTCD);
                strDMNO = this.uGrid31_1.ActiveRow.Cells[1].Text.ToString();
                //strDMNO = WQ_Function.GetConsumerNo(strDMNO);

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND DPTCD = '" + strDPTCD + "' AND DMNO = '" + strDMNO + "'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCTP");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion

        #region - 수도꼭지관리/수도꼭지시험관리

        /// <summary>
        /// 수도꼭지지점 Data를 Select해서 Grid에 Set한다.
        /// </summary>
        private void GetFaucetTestData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;
            string strSITE = string.Empty;
            string strSITECD = string.Empty;
            string strDPTCD = string.Empty;
            string strDMNO = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;

            DataSet pDS = new DataSet();

            strSITE = this.cbEstablishment7.Text.ToString();
            strSITECD = WQ_Function.SplitToCode(strSITE);
            strDPTCD = this.cbDivisionName.Text.ToString();
            strDPTCD = WQ_Function.SplitToCode(strDPTCD);
            strDMNO = this.txtConsumer2.Text.ToString();

            strSDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateS6.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtWaterCollectDateE6.DateTime);

            strParam = "WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND A.SITECD = '" + strSITECD + "' AND A.CLTDT BETWEEN '" + strSDate + "' AND '" + strEDate + "' AND A.DMNO LIKE '%" + strDMNO + "%'";

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   '" + strSITE + "' AS SITECD, A.DMNO, (SELECT DMNM FROM WI_DMINFO WHERE DMNO = A.DMNO) AS DMNM, TO_DATE(A.CLTDT) AS CLTDT, A.REGMNGR, DECODE(A.WPLINK, '', A.WPLINK, 'LIMS 연계 자료') AS ITEMGRP");
            oStringBuilder.AppendLine("FROM     WQCTPWQIPS A");
            oStringBuilder.AppendLine(strParam);
            oStringBuilder.AppendLine("ORDER BY A.SITECD, CLTDT, A.DMNO");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTPWQIPS");

            this.uGrid32_1.DataSource = pDS.Tables["WQCTPWQIPS"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGrid32_1);

            //첫번째 Row에 Activate 부여
            if (this.uGrid32_1.Rows.Count > 0) this.uGrid32_1.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// UltraGrid에서 선택된 Data를 Database에서 Delete한다.
        /// </summary>
        private void DeleteFaucetTestData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            string strSITECD = string.Empty;
            string strCLTDT = string.Empty;
            string strDMNO = string.Empty;

            string strParam = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.uGrid32_1.Rows.Count > 0)
            {
                strSITECD = this.uGrid32_1.ActiveRow.Cells[0].Text.ToString();
                strSITECD = WQ_Function.SplitToCode(strSITECD);
                strCLTDT = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGrid32_1.ActiveRow.Cells[1].Value));
                strDMNO = WQ_Function.SplitToCode(this.uGrid32_1.ActiveRow.Cells[2].Value.ToString());

                strParam = "WHERE    SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND SITECD = '" + strSITECD + "' AND CLTDT = '" + strCLTDT + "' AND DMNO = '" + strDMNO + "'";

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCTPWQIPS");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("DELETE");
                oStringBuilder.AppendLine("FROM     WQCTPWQIPR");
                oStringBuilder.AppendLine(strParam);

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        #endregion


        #endregion
    }
}
