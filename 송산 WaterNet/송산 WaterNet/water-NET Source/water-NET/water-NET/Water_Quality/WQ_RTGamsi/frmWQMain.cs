﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_RTGamsi
{
    /// <summary>
    /// Project ID : WN_WQ_A02
    /// Project Explain : 실시간감시, 기본적으로 WaterAOCore.frmMap을 사용
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.27
    /// Form Explain : 실시간감시 Main Form
    /// </summary>
    public partial class frmWQMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        double m_MapX; //GIS의 Map X 좌표 값
        double m_MapY; //GIS의 Map Y 좌표 값
        int m_X; //GIS의 윈도우 X 좌표
        int m_Y; //GIS의 윈도우 Y 좌표

        ArrayList m_ArrayContents = new ArrayList(); //감시지점 간략 폼배열
        ILayer m_Layer = null; //감시지점 레이어

        FormPopup.frmRTMonitorPoint m_Form_1 = new FormPopup.frmRTMonitorPoint();
        FormPopup.frmRTData m_Form_2 = new FormPopup.frmRTData();
        FormPopup.frmRTAlertHistory m_Form_3 = new FormPopup.frmRTAlertHistory();

        public frmWQMain()
        {
            WQ_AppStatic.IS_SHOW_FORM_ALERT_HIS = false;
            WQ_AppStatic.IS_SHOW_FORM_DETAIL = false;
            WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = false;
            WQ_AppStatic.IS_SHOW_FORM_RT_DATA = false;

            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버 (메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.)

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();

            m_Layer = WaterAOCore.ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_Pipegraphic, "감시지점");
            axMap.AddLayer(m_Layer);
            WaterAOCore.ArcManager.SetDefaultRenderer(axMap.ActiveView.FocusMap, (IFeatureLayer)m_Layer);
            m_Layer.Visible = true;

            this.SetLayerFromDB();

            if (WQ_AppStatic.IS_SHOW_FORM_RT_DATA == false)
            {
                m_Form_2 = null;
                m_Form_2 = new FormPopup.frmRTData();
                m_Form_2.IMAP = (IMapControl3)axMap.Object;
                m_Form_2.StartPosition = FormStartPosition.Manual;
                m_Form_2.TopLevel = false;
                m_Form_2.Parent = this;
                m_Form_2.Tag = "RTData";
                m_Form_2.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, m_Form_2.Width, m_Form_2.Height);
                m_Form_2.BringToFront();
                m_Form_2.Show();
            }
        }
        
        private void frmWQMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_ArrayContents.Clear();
        }


        #region Button Events

        private void btnRTMonitorPnt_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT == false)
                {
                    m_Form_1 = null;
                    m_Form_1 = new FormPopup.frmRTMonitorPoint();
                    m_Form_1.IMAP = (IMapControl3)axMap.Object;
                    m_Form_1.TopLevel = false;
                    m_Form_1.Parent = this;
                    m_Form_1.StartPosition = FormStartPosition.Manual;
                    m_Form_1.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, m_Form_1.Width, m_Form_1.Height);
                    m_Form_1.BringToFront();
                    m_Form_1.Show();
                }
                else
                {
                    m_Form_1.BringToFront();
                    m_Form_1.Activate();
                    m_Form_1.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnRTMonitor_Click(object sender, EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_RT_DATA == false)
            {
                m_Form_2 = null;
                m_Form_2 = new FormPopup.frmRTData();
                m_Form_2.IMAP = (IMapControl3)axMap.Object;
                m_Form_2.StartPosition = FormStartPosition.Manual;
                m_Form_2.TopLevel = false;
                m_Form_2.Parent = this;
                m_Form_2.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, m_Form_2.Width, m_Form_2.Height);
                m_Form_2.BringToFront();
                m_Form_2.Show();
            }
            else
            {
                m_Form_2.BringToFront();
                m_Form_2.Activate();
                m_Form_2.Show();
            }
        }

        private void btnRTAlertHis_Click(object sender, System.EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_ALERT_HIS == false)
            {
                m_Form_3 = null;
                m_Form_3 = new FormPopup.frmRTAlertHistory();
                m_Form_3.IMAP = (IMapControl3)axMap.Object;
                m_Form_3.TopLevel = false;
                m_Form_3.Parent = this;
                m_Form_3.StartPosition = FormStartPosition.Manual;
                m_Form_3.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, m_Form_3.Width, m_Form_3.Height);
                m_Form_3.BringToFront();
                m_Form_3.Show();
            }
            else
            {
                m_Form_3.BringToFront();
                m_Form_3.Activate();
                m_Form_3.Show();
            }
        }

        #endregion


        #region Control Events

        /// <summary>
        /// Map에서 오른쪽 마우스 버튼 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            ///*****************************************************************************
            ///GIS MAP에서 Mouse 다운이 일어난 위치
            ///----------------------------------------------------------------------------S
            ///GIS 상 좌표
            m_MapX = e.mapX;
            m_MapY = e.mapY;
            ///Windows 상 좌표
            m_X = e.x;
            m_Y = e.y;
            ///----------------------------------------------------------------------------E

            WQ_AppStatic.CURRENT_IGEOMETRY = null;

            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;


            #region 위치선택 - 지도에 사고지점을 선택하고, 별표 표시

            if (WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT == true)
            {
                ///기존 객체 및 환경 초기화
                ArcManager.ClearSelection(axMap.ActiveView.FocusMap);
                IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
                pGraphicsContainer.DeleteAllElements();

                IActiveView activeView = axMap.ActiveView;
                IScreenDisplay screenDisplay = activeView.ScreenDisplay;
                IColor pColor = ArcManager.GetColor(255, 0, 0);

                ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
                IRubberBand pRubberBand = new RubberPointClass();
                IPoint pSearchPoint = (IPoint)pRubberBand.TrackNew(screenDisplay, pSymbol);

                if (pSearchPoint == null | pSearchPoint.IsEmpty) return;

                IElement pElement = new MarkerElementClass();
                pElement.Geometry = pSearchPoint as IGeometry;
                IMarkerElement pMarkerElement = pElement as IMarkerElement;
                pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;

                pGraphicsContainer.AddElement(pElement, 0);
                ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
                Application.DoEvents();
                ArcManager.FlashShape(axMap.ActiveView, pSearchPoint as IPoint, 3);
            }

            #endregion 지도에 사고지점을 선택하고, 별표 표시 끝

            ///*****************************************************************************
            ///GIS 상 좌표로 포인트를 만들고 포인트로 지오메트리를 생성
            ///----------------------------------------------------------------------------S
            ILayer pLayerRT = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "감시지점");

            if (pLayerRT == null) return;

            IPoint pPointRT = new PointClass();
            pPointRT.X = m_MapX;
            pPointRT.Y = m_MapY;
            pPointRT.Z = 0;

            IGeometry pGeomRT = (IGeometry)pPointRT;

            WQ_AppStatic.CURRENT_IGEOMETRY = pGeomRT;

            ///----------------------------------------------------------------------------E

            ///*****************************************************************************
            ///ArcManager.GetSpatialCursor()로 Map에 마우스 클릭한 Map 상 좌표에서 반경 50m 
            ///지점의 특정 값을 가져와 FeatureCursor로 만들고 FeatureCursor로 Feature를
            ///만든 다음, ArcManager.GetValue()로 Layer 상의 특정 Field값을 Object로 받는다.
            ///----------------------------------------------------------------------------S

            double dblContains = 0;

            if (ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)100000)
            {
                dblContains = 300;
            }
            else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)100000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)50000)
            {
                dblContains = 200;
            }
            else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)50000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)15000)
            {
                dblContains = 100;
            }
            else
            {
                dblContains = 10;
            }

            IFeatureCursor pIFCursorRT = ArcManager.GetSpatialCursor(pLayerRT, pGeomRT, dblContains, esriSpatialRelEnum.esriSpatialRelContains, string.Empty);

            IFeature pFeatureRT = pIFCursorRT.NextFeature();

            if (pFeatureRT != null)
            {
                object oNO = WaterAOCore.ArcManager.GetValue(pFeatureRT, "NOS");

                if (oNO != null)
                {
                    foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
                    this.MakeControlsOnlySelectedPoint(oNO.ToString());
                    IEnvelope pEnvelope = axMap.Extent;
                    this.ExtentUpdatedEvent(pEnvelope);
                }
                else
                {
                    foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
                }
            }
            else
            {
                foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
            }
            
            switch (e.button)
            {
                case 1:    //왼쪽버튼
                    break;

                case 2:    //오른쪽버튼
                    //ContextMenuStripPopup.Show(axMap, m_X, m_Y);
                    break;
            }
        }

        private void axMap_OnExtentUpdated(object sender, IMapControlEvents2_OnExtentUpdatedEvent e)
        {
            //Map의 축척이 15000 이하일 경우에만 간략 폼을 뷰한다.
            //if (ArcManager.GetMapScale((IMapControl3)axMap.Object) < (double)15000)
            //{
            //    this.MakeControls();
            //    this.ExtentUpdatedEvent(e.newEnvelope as IEnvelope);
            //}
            //else
            //{
            //    foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
            //}
        }

        #endregion


        #region User Function

        /// <summary>
        /// Array에 저장된 실시간감시 간략화면을 가져온다
        /// </summary>
        /// <param name="nOID">유량계 OID</param>
        /// <returns></returns>
        private Form GetControl(string nOID)
        {
            foreach (var item in m_ArrayContents)
            {
                if (((Form)item).Tag.ToString() == nOID)
                {
                    return (Form)item;
                }
            }
            return null;
        }

        /// <summary>
        /// 현재 Extent에 Intersect되는 감시지점의 실시간감시 간략화면 표시한다.
        /// </summary>
        /// <param name="pEnvelope"></param>
        private void ExtentUpdatedEvent(IEnvelope pEnvelope)
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;

            if (!pFeatureLayer.Visible) return;
            if (pFeatureLayer.MaximumScale > axMap.MapScale && pFeatureLayer.MaximumScale != 0) return;
            if (pFeatureLayer.MinimumScale < axMap.MapScale && pFeatureLayer.MinimumScale != 0) return;

            //먼저 콤포넌트를 보이지 않게 한다.
            foreach (var item in m_ArrayContents) ((Form)item).Visible = false;

            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pEnvelope;
            pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;
            pSpatialFilter.WhereClause = string.Empty;

            IFeatureCursor pFCursor = pFeatureClass.Search(pSpatialFilter, true);
            IFeature pFeature = pFCursor.NextFeature();
            try
            {
                while (pFeature != null)
                {
                    IGeometry pGeom = pFeature.Shape;
                    if (pGeom is IPoint)
                    {
                        IPoint pPoint = pGeom as IPoint;
                        int x; int y;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.Units = esriUnits.esriMeters;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.FromMapPoint(pPoint, out x, out y);

                        Form oControl = GetControl(Convert.ToString(pFeature.OID));
                        if (oControl != null)
                        {
                            oControl.SetBounds(x + 11, y + 25, 403, 158);
                            oControl.BringToFront();
                            oControl.Visible = true;
                            Application.DoEvents();
                        }
                    }
                    pFeature = pFCursor.NextFeature();
                }
            }
            catch (Exception oExec)
            {
                //Console.WriteLine(oExec.Message);
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pFCursor);

            }
        }

        /// <summary>
        /// MakeControl - 실시간감시 감시 데이터 화면 생성하여 Array에 저장
        /// </summary>
        private void MakeControls()
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ICursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, string.Empty);
            IRow pRow = pCursor.NextRow();
            try
            {
                //이미 등록된 Control(Form)을 클로즈 한다.
                foreach (var item in m_ArrayContents) ((Form)item).Close();
                m_ArrayContents.Clear();
                while (pRow != null)
                {
                    object o = WaterAOCore.ArcManager.GetValue(pRow, "FID");
                    object oNAME = WaterAOCore.ArcManager.GetValue(pRow, "NAMES");
                    object oNO = WaterAOCore.ArcManager.GetValue(pRow, "NOS");
                    if (oNO != null)
                    {
                        FormPopup.frmRTBrief oControl = new FormPopup.frmRTBrief();
                        oControl.TopLevel = false;
                        oControl.Parent = this;
                        oControl.Tag = Convert.ToString(o);
                        oControl.NO = oNO.ToString();
                        oControl.NAME = oNAME.ToString();
                        m_ArrayContents.Add(oControl);
                    }
                    pRow = pCursor.NextRow();
                }
            }
            catch (Exception)
            {
                //Console.WriteLine("MakeControl");
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
            }
        }

        /// <summary>
        /// MakeControl - 실시간감시 감시 데이터 화면 생성하여 Array에 저장
        /// </summary>
        private void MakeControlsOnlySelectedPoint(string strNO)
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "NOS='" + strNO + "'") as IFeatureCursor;
            IFeature pFeature = pCursor.NextFeature();
            
            try
            {
                foreach (var item in m_ArrayContents) ((Form)item).Close();
                m_ArrayContents.Clear();

                if (pFeature != null)
                {
                    FormPopup.frmRTBrief oControl = new FormPopup.frmRTBrief();
                    oControl.TopLevel = false;
                    oControl.Parent = this;
                    oControl.Tag = WaterAOCore.ArcManager.GetValue(pFeature, "FID").ToString();
                    oControl.NO = strNO;
                    oControl.NAME = WaterAOCore.ArcManager.GetValue(pFeature, "NAMES").ToString();

                    m_ArrayContents.Add(oControl);
                }            
            }
            catch (Exception)
            {
                //Console.WriteLine("MakeControl");
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
            }
        }

        /// <summary>
        /// 지점정보를 취득해 레이어에 정보를 SET한다.
        /// </summary>
        private void SetLayerFromDB()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            ArcManager.DeleteAllFeatures(m_Layer);

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.SBLOCK_CODE, A.MONPNT_GBN, A.MONITOR_NO, A.MONPNT, A.MAP_X, A.MAP_Y");
            oStringBuilder.AppendLine("FROM      WQ_RT_MONITOR_POINT A");
            oStringBuilder.AppendLine("WHERE     A.MONPNT_GBN = '0'");
            oStringBuilder.AppendLine("ORDER BY  A.SBLOCK_CODE, A.MONPNT_GBN, A.MONITOR_NO");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "SET_LAYER");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {

                IWorkspaceEdit pWorkspaceEdit = ArcManager.getWorkspaceEdit(m_Layer as IFeatureLayer);
                if (pWorkspaceEdit == null) return;

                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureBuffer pFeatureBuffer = ((IFeatureLayer)m_Layer).FeatureClass.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = ((IFeatureLayer)m_Layer).FeatureClass.Insert(true);
                    IFeature pFeaturebuffer = pFeatureBuffer as IFeature;

                    comReleaser.ManageLifetime(pFeatureCursor);

                    IPoint pPoint = new PointClass();

                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        //레이어의 지점 좌표
                        pPoint.PutCoords(Convert.ToDouble(oDRow["MAP_X"].ToString()), Convert.ToDouble(oDRow["MAP_Y"].ToString()));

                        //레이어의 지점 데이터
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("SBLOCK"), (object)oDRow["SBLOCK_CODE"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("GBN"), (object)oDRow["MONPNT_GBN"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("NOS"), (object)oDRow["MONITOR_NO"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("NAMES"), (object)oDRow["MONPNT"].ToString());

                        //레이어의 지점 좌표로 IGeometry
                        pFeaturebuffer.Shape = pPoint as IGeometry;

                        pFeatureCursor.InsertFeature(pFeatureBuffer);
                    }
                    pFeatureCursor.Flush();
                }

                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
            }

            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();

            pDS.Dispose();
        }

        #endregion

    }
}
