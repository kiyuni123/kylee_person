﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

using ChartFX.WinForms;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;
using WaterNet.WV_Common.manager;

namespace WaterNet.WQ_RTGamsi.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A02
    /// Project Explain : 실시간수질감시조회
    /// Project Developer : 오두석
    /// Project Create Date : 2011.03.24
    /// Form Explain : 실시간수질감시조회 Popup Form
    /// </summary>
    public partial class frmRTData : Form
    {
        #region 프로퍼티

        private IMapControl3 _Map;

        public IMapControl3 IMAP
        {
            get
            {
                return _Map;
            }
            set
            {
                _Map = value;
            }
        }

        #endregion

        private OracleDBManager m_oDBManager = null;
        private ExcelManager excelManager = new ExcelManager();

        string m_RT_ITEM_CD = string.Empty;
        string m_RT_ITEM_NM = string.Empty;

        public frmRTData()
        {
            WQ_AppStatic.IS_SHOW_FORM_RT_DATA = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.FormClose();
                return;
            }

            InitializeComponent();

            this.InitializeGridSetting();

            this.InitializeChartSetting();

            DateTime now = DateTime.Now;
            int iMinutes = -30;
            uDTAlertS.Value = now.AddMinutes(iMinutes).Year + "-" + now.AddMinutes(iMinutes).Month + "-" + now.AddMinutes(iMinutes).Day
                + " " + now.AddMinutes(iMinutes).Hour + ":" + now.AddMinutes(iMinutes).Minute;
            uDTAlertE.Value = now;
        }

        private void frmRTData_Load(object sender, EventArgs e)
        {
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);

            this.GetRTMonitorData();  ///cdkim 2017.05.31 기간 검색 추가
            //this.GetRTMonitorData();
            this.rdoCL.Checked = true;
            m_RT_ITEM_CD = "CL";
            m_RT_ITEM_NM = "잔류염소";

            if (this.ugRTData.Rows.Count <= 0) return;

            this.SetChartByRTDataItem();
        }

        private void frmRTData_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.FormClose();
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetRTMonitorData();  ///cdkim 2017.05.31 기간 검색 추가
                //this.GetRTMonitorData();

                this.InitializeChartSetting();

                if (this.ugRTData.Rows.Count <= 0) return;

                this.SetChartByRTDataItem();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.FormClose();
        }

        #endregion


        #region Control Events

        private void ugRTData_Click(object sender, EventArgs e)
        {
            this.InitializeChartSetting();

            if (this.ugRTData.Rows.Count <= 0) return;

            this.SetChartByRTDataItem();
        }

        private void ugRTData_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            string strMONITOR_NO = WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[1].Text);
            this.Viewmap_SelectedMonitorPoint(strMONITOR_NO);
        }

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLOC_CODE = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLOC_CODE, 1);
        }

        private void rdoCL_Click(object sender, EventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "CL";
            m_RT_ITEM_NM = "잔류염소";

            this.SetChartByRTDataItem();
        }

        private void rdoTB_Click(object sender, EventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "TB";
            m_RT_ITEM_NM = "탁도";

            this.SetChartByRTDataItem();
        }

        private void rdoPH_Click(object sender, EventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "PH";
            m_RT_ITEM_NM = "수소이온농도";

            this.SetChartByRTDataItem();
        }

        private void rdoTE_Click(object sender, EventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "TE";
            m_RT_ITEM_NM = "온도";

            this.SetChartByRTDataItem();
        }

        private void rdoCU_Click(object sender, EventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "CU";
            m_RT_ITEM_NM = "전기전도도";

            this.SetChartByRTDataItem();
        }

        #endregion

        
        #region User Function

        #region - Initialize Control

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            #region 실시간감시지점에 대한 실시간데이터

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MON_PNT";
            oUltraGridColumn.Header.Caption = "감시지점";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DT";
            oUltraGridColumn.Header.Caption = "측정일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CL_VAL";
            oUltraGridColumn.Header.Caption = "잔류염소(CL)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TB_VAL";
            oUltraGridColumn.Header.Caption = "탁도(TB)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PH_VAL";
            oUltraGridColumn.Header.Caption = "수소이온농도(pH)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TE_VAL";
            oUltraGridColumn.Header.Caption = "온도(TE)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();

            oUltraGridColumn.Key = "CU_VAL";
            oUltraGridColumn.Header.Caption = "전기전도도(CU)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.ugRTData);

            //ugRTData.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            #endregion
        }

        /// <summary>
        /// Chart 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
            this.cFXRT.Data.Clear();
            this.cFXRT.AxisX.Staggered = true;
            this.cFXRT.AxisY.Staggered = true;

            this.cFXRT.LegendBox.Dock = DockArea.Top;
        }

        #endregion

        #region - RT Monitor Function

        /// <summary>
        /// 조회 조건에 맞는 실시간 감압밸브 감시지점의 실시간계측값을 그리드에 SET
        /// cdkim 2017.05.31 기간 검색 인자 추가
        /// </summary>
        private void GetRTMonitorData()
        {
            string startTime = string.Format("{0:yyyyMMddHHmm}", uDTAlertS.Value);  ///cdkim 2017.05.31 기간 검색 추가
            string endTime = string.Format("{0:yyyyMMddHHmm}", uDTAlertE.Value);    ///cdkim 2017.05.31 기간 검색 추가

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilderTemp = new StringBuilder();
            StringBuilder oStringBuilder = new StringBuilder();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            //string strSDate = string.Empty;
            //string strEDate = string.Empty;

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "AND SBLOCK_CODE IN (" + strBLOCK_CD_S + ", '" + strBLOCK_CD_M + "')";
                }
                else
                {
                    strBLOCK_SQL = "AND SBLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            //strSDate = WQ_Function.StringToDateTime(this.udtSDate.DateTime);
            //strEDate = WQ_Function.StringToDateTime(this.udtEDate.DateTime);

            string strCUR_TIME = WQ_Function.ChangeDateTimeToDateTime(DateTime.Now);
            string strHH24MI = strCUR_TIME.Substring(strCUR_TIME.Length - 6, 4);

            DataSet pDS = new DataSet();
            DataSet pDSTemp = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilderTemp.Remove(0, oStringBuilderTemp.Length);
            oStringBuilderTemp.AppendLine("SELECT MONITOR_NO FROM WQ_RT_MONITOR_POINT WHERE MONPNT_GBN = '0' " + strBLOCK_SQL + " ORDER BY MONITOR_NO");

            pDSTemp = m_oDBManager.ExecuteScriptDataSet(oStringBuilderTemp.ToString(), null, "MONITOR_POINT");

            if ((pDSTemp.Tables.Count > 0) && (pDSTemp.Tables[0].Rows.Count > 0))
            {
                int i = 0;
                oStringBuilder.Remove(0, oStringBuilder.Length);
                foreach (DataRow oDRowTemp in pDSTemp.Tables[0].Rows)
                {
                    if (i != 0)
                    {
                        oStringBuilder.AppendLine("UNION ALL");
                    }
                    //아래 //처리된 SQL은 SDATE, EDATE를 적용한 경우지만
                    //frmRTBriefDetail의 조회 SQL과 같아지기 때문에 이 화면에서는 30분 데이터만 보기로 한다.
                    //oStringBuilder.AppendLine("SELECT   (SELECT LOC_NAME || ' (' || LOC_CODE || ')' FROM CM_LOCATION WHERE LOC_CODE = PNT.SBLOCK_CODE) AS BLOCK,");
                    //oStringBuilder.AppendLine("         PNT.MONPNT || ' (' || PNT.MONITOR_NO || ')' AS MON_PNT,");
                    //oStringBuilder.AppendLine("         TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS DT,");
                    //oStringBuilder.AppendLine("         DECODE(CL.CUR_VALUE, '', 0, CL.CUR_VALUE) AS CL_VAL,");
                    //oStringBuilder.AppendLine("         DECODE(PH.CUR_VALUE, '', 0, PH.CUR_VALUE) AS PH_VAL,");
                    //oStringBuilder.AppendLine("         DECODE(TB.CUR_VALUE, '', 0, TB.CUR_VALUE) AS TB_VAL,");
                    //oStringBuilder.AppendLine("         DECODE(TE.CUR_VALUE, '', 0, TE.CUR_VALUE) AS TE_VAL,");
                    //oStringBuilder.AppendLine("         DECODE(CU.CUR_VALUE, '', 0, CU.CUR_VALUE) AS CU_VAL");
                    //oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CL FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + strSDate + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strEDate + strHH24MI + "00', 'RRRRMMDDHH24MISS')) CL,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_PH FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + strSDate + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strEDate + strHH24MI + "00', 'RRRRMMDDHH24MISS')) PH,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TB FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + strSDate + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strEDate + strHH24MI + "00', 'RRRRMMDDHH24MISS')) TB,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + strSDate + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strEDate + strHH24MI + "00', 'RRRRMMDDHH24MISS')) TE,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CU FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + strSDate + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strEDate + strHH24MI + "00', 'RRRRMMDDHH24MISS')) CU,");
                    //oStringBuilder.AppendLine("         (SELECT MONITOR_NO, MONPNT, SBLOCK_CODE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "' AND MONPNT_GBN = '0') PNT,");
                    //oStringBuilder.AppendLine("         (SELECT TO_DATE('" + strEDate + strHH24MI + "00', 'RRRRMMDDHH24MISS') - ((1/24/60) * ROWNUM) AS DT FROM DUAL CONNECT BY ROWNUM <= ((TO_DATE('" + strEDate + strHH24MI + "00', 'RRRRMMDDHH24MISS') - TO_DATE('" + strSDate + "000000', 'RRRRMMDDHH24MISS')) * 24 * 60)) TMP");
                    //oStringBuilder.AppendLine("WHERE    CL.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    //oStringBuilder.AppendLine("         AND PH.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    //oStringBuilder.AppendLine("         AND TB.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    //oStringBuilder.AppendLine("         AND TE.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    //oStringBuilder.AppendLine("         AND CU.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");

                    oStringBuilder.AppendLine("SELECT   (SELECT LOC_NAME || ' (' || LOC_CODE || ')' FROM CM_LOCATION WHERE LOC_CODE = PNT.SBLOCK_CODE) AS BLOCK,");
                    oStringBuilder.AppendLine("         PNT.MONPNT || ' (' || PNT.MONITOR_NO || ')' AS MON_PNT,");
                    oStringBuilder.AppendLine("         TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS DT,");
                    oStringBuilder.AppendLine("         DECODE(CL.CUR_VALUE, '', 0, CL.CUR_VALUE) AS CL_VAL,");                    
                    oStringBuilder.AppendLine("         DECODE(TB.CUR_VALUE, '', 0, TB.CUR_VALUE) AS TB_VAL,");
                    oStringBuilder.AppendLine("         DECODE(PH.CUR_VALUE, '', 0, PH.CUR_VALUE) AS PH_VAL,");
                    oStringBuilder.AppendLine("         DECODE(TE.CUR_VALUE, '', 0, TE.CUR_VALUE) AS TE_VAL,");
                    oStringBuilder.AppendLine("         DECODE(CU.CUR_VALUE, '', 0, CU.CUR_VALUE) AS CU_VAL");
                    oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CL FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + startTime + "','YYYYMMDDHH24MI') AND TO_DATE('" + endTime + "','YYYYMMDDHH24MI')) CL,");   ///cdkim 2017.05.31 기간 검색 인자 추가
                    oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TB FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + startTime + "','YYYYMMDDHH24MI') AND TO_DATE('" + endTime + "','YYYYMMDDHH24MI')) TB,");   ///cdkim 2017.05.31 기간 검색 인자 추가
                    oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_PH FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + startTime + "','YYYYMMDDHH24MI') AND TO_DATE('" + endTime + "','YYYYMMDDHH24MI')) PH,");   ///cdkim 2017.05.31 기간 검색 인자 추가
                    oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + startTime + "','YYYYMMDDHH24MI') AND TO_DATE('" + endTime + "','YYYYMMDDHH24MI')) TE,");   ///cdkim 2017.05.31 기간 검색 인자 추가
                    oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CU FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN TO_DATE('" + startTime + "','YYYYMMDDHH24MI') AND TO_DATE('" + endTime + "','YYYYMMDDHH24MI')) CU,");   ///cdkim 2017.05.31 기간 검색 인자 추가
                    //oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CL FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (41/(24*60)) AND SYSDATE - (10/(24*60))) CL,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TB FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (41/(24*60)) AND SYSDATE - (10/(24*60))) TB,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_PH FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (41/(24*60)) AND SYSDATE - (10/(24*60))) PH,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (41/(24*60)) AND SYSDATE - (10/(24*60))) TE,");
                    //oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CU FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (41/(24*60)) AND SYSDATE - (10/(24*60))) CU,");
                    oStringBuilder.AppendLine("         (SELECT MONITOR_NO, MONPNT, SBLOCK_CODE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowTemp["MONITOR_NO"].ToString().Trim() + "') PNT,");
                    //oStringBuilder.AppendLine("         (SELECT ((SYSDATE - (9/(24*60))) - (1/24/60) * (ROWNUM)) AS DT FROM DUAL CONNECT BY ROWNUM <= (SYSDATE + ( 1 / 24 )) - (SYSDATE - (4320/(24*60)) * 24 * 60)) TMP");
                    oStringBuilder.AppendLine("         (SELECT (TO_DATE('" + endTime + "', 'RRRRMMDDHH24MI') - (1/24/60) * (ROWNUM)) AS DT FROM DUAL CONNECT BY ROWNUM <= (TO_DATE('" + endTime + "', 'RRRRMMDDHH24MI') - TO_DATE('" + startTime + "', 'RRRRMMDDHH24MI')) * 60 * 24) TMP");   ///cdkim 2017.05.31 기간 검색 인자 추가
                    oStringBuilder.AppendLine("WHERE    CL.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    oStringBuilder.AppendLine("         AND PH.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    oStringBuilder.AppendLine("         AND TB.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    oStringBuilder.AppendLine("         AND TE.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                    oStringBuilder.AppendLine("         AND CU.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");

                    i++;
                }

                oStringBuilder.AppendLine("ORDER BY DT DESC, BLOCK, MON_PNT");

                pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RTDATA_GRID");

                this.ugRTData.DataSource = pDS.Tables["RTDATA_GRID"];
            }
            else
            {
                this.ugRTData.DataSource = null;
            }

            if (this.ugRTData.Rows.Count > 0) this.ugRTData.Rows[0].Activated = true;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.ugRTData);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region - Chart Function

        /// <summary>
        /// Chart FX를 Setting 해서 Data를 연결한다.
        /// 항목별 분석 그리드에 대한
        /// </summary>
        private void SetChartByRTDataItem()
        {
            string startTime = string.Format("{0:yyyyMMddHHmm}", uDTAlertS.Value);  ///cdkim 2017.05.31 기간 검색 추가
            string endTime = string.Format("{0:yyyyMMddHHmm}", uDTAlertE.Value);    ///cdkim 2017.05.31 기간 검색 추가

            string strCOLUMN = string.Empty;
            string strMONITOR_NO = string.Empty;
            string strMONITOR_NM = string.Empty;
            string strAlertOption = string.Empty; //1 : 기준치로 경보, 2 : 패턴으로 경보

            strMONITOR_NO = WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[1].Text);
            strMONITOR_NM = WQ_Function.SplitToCodeName(this.ugRTData.ActiveRow.Cells[1].Text);

            //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
            this.cFXRT.Data.Clear();
            this.cFXRT.Series.Clear();
            this.cFXRT.DataSourceSettings.Fields.Clear();

            //Chart 타이틀 생성
            TitleDockable td = new TitleDockable();
            td.Font = new Font("굴림", 10, FontStyle.Bold);
            td.TextColor = Color.DarkBlue;
            td.Text = strMONITOR_NM;
            this.cFXRT.Titles.Clear();
            this.cFXRT.Titles.Add(td);

            #region -- 항목별 Y축 최대, 최소, 스탭 설정

            switch (m_RT_ITEM_CD)
            {
                case "CL":
                    strCOLUMN = "TAG_ID_CL";
                    this.cFXRT.Panes[0].AxisY.Max = 1.5;
                    this.cFXRT.Panes[0].AxisY.Min = 0.1;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.0";
                    break;
                case "TB":
                    strCOLUMN = "TAG_ID_TB";
                    this.cFXRT.Panes[0].AxisY.Max = 2.0;
                    this.cFXRT.Panes[0].AxisY.Min = 0.01;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.00";
                    break;
                case "PH":
                    strCOLUMN = "TAG_ID_PH";
                    this.cFXRT.Panes[0].AxisY.Max = 14.0;
                    this.cFXRT.Panes[0].AxisY.Min = 1.0;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.0";
                    break;
                case "TE":
                    strCOLUMN = "TAG_ID_TE";
                    this.cFXRT.Panes[0].AxisY.Max = 30;
                    this.cFXRT.Panes[0].AxisY.Min = -10;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.0";
                    break;
                case "CU":
                    strCOLUMN = "TAG_ID_CU";
                    this.cFXRT.Panes[0].AxisY.Max = 200;
                    this.cFXRT.Panes[0].AxisY.Min = 80;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0";
                    break;
            }

            this.cFXRT.Panes[0].AxisY.Step = this.cFXRT.Panes[0].AxisY.Max / 20;

            #endregion

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            //oStringBuilder.Remove(0, oStringBuilder.Length);
            //oStringBuilder.AppendLine("SELECT   TO_CHAR(RT.TIMESTAMP, 'RRRR-MM-DD HH24:MI') AS DT,");
            //oStringBuilder.AppendLine("         RT.CUR_VALUE AS CUR_VALUE,");
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_TOP_LIMIT), 3) AS T_ALERT1,");
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_LOW_LIMIT), 3) AS L_ALERT1,");
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.TOP_LIMIT) + (TO_NUMBER(PTN.TOP_LIMIT) * (TO_NUMBER(MAP.N2_TOP_LIMIT) / 100)), 3) AS T_ALERT2,");
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.LOWER_LIMIT) - (TO_NUMBER(PTN.LOWER_LIMIT) * (TO_NUMBER(MAP.N2_LOW_LIMIT) / 100)), 3) AS L_ALERT2");
            //oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT " + strCOLUMN + " FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS')) RT,");
            //oStringBuilder.AppendLine("         (SELECT N1_TOP_LIMIT, N1_LOW_LIMIT, N2_TOP_LIMIT, N2_LOW_LIMIT FROM WQ_MONPNT_MAPPING WHERE MONITOR_NO = '" + _MONITOR_NO + "' AND WQ_ITEM_CODE = '" + strITEM_CD + "') MAP,");
            //oStringBuilder.AppendLine("         (SELECT TIME AS DT, TOP_LIMIT, LOWER_LIMIT FROM WQ_PATTERN WHERE MONITOR_NO = '" + _MONITOR_NO + "' AND WQ_ITEM_CODE = '" + strITEM_CD + "') PTN,");
            //oStringBuilder.AppendLine("         (SELECT TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS') - ((1/24/60) * " + strFilterTime + " * ROWNUM) AS DT FROM DUAL CONNECT BY ROWNUM <= ((TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS') - TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS')) * 24 * 60) / " + strFilterTime + ") TMP");
            //oStringBuilder.AppendLine("WHERE    RT.TIMESTAMP = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            //oStringBuilder.AppendLine("         AND PTN.DT = TO_CHAR(RT.TIMESTAMP, 'HH24:MI')");
            //oStringBuilder.AppendLine("ORDER BY DT");

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS DT,");
            oStringBuilder.AppendLine("         DECODE(RT.CUR_VALUE, '', 0, RT.CUR_VALUE) AS CUR_VALUE,");
            oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_TOP_LIMIT), 3) AS T_ALERT1,");   //기준상한
            oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_LOW_LIMIT), 3) AS L_ALERT1");   //기준하한
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.TOP_LIMIT) + (TO_NUMBER(PTN.TOP_LIMIT) * (TO_NUMBER(MAP.N2_TOP_LIMIT) / 100)), 3) AS T_ALERT2,");       //패턴상한    ///cdkim 2017.05.31 WQ_PATTERN 데이터 부재로 인한 주석 처리
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.LOWER_LIMIT) - (TO_NUMBER(PTN.LOWER_LIMIT) * (TO_NUMBER(MAP.N2_LOW_LIMIT) / 100)), 3) AS L_ALERT2");    //패턴하한    ///cdkim 2017.05.31 WQ_PATTERN 데이터 부재로 인한 주석 처리
            oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT " + strCOLUMN + " FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + strMONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + startTime + "', 'YYYYMMDDHH24MI') AND TO_DATE('" + endTime + "', 'YYYYMMDDHH24MI')) RT,"); ///cdkim 2017.05.31 기간 검색 추가
            oStringBuilder.AppendLine("         (SELECT N1_TOP_LIMIT, N1_LOW_LIMIT, N2_TOP_LIMIT, N2_LOW_LIMIT FROM WQ_MONPNT_MAPPING WHERE MONITOR_NO = '" + strMONITOR_NO + "' AND WQ_ITEM_CODE = '" + m_RT_ITEM_CD + "') MAP,");
            //oStringBuilder.AppendLine("         (SELECT TIME AS DT, TOP_LIMIT, LOWER_LIMIT FROM WQ_PATTERN WHERE MONITOR_NO = '" + strMONITOR_NO + "' AND WQ_ITEM_CODE = '" + m_RT_ITEM_CD + "') PTN,");  ///cdkim 2017.05.31 기간 검색 추가
            oStringBuilder.AppendLine("         (SELECT (TO_DATE('" + endTime + "', 'RRRRMMDDHH24MI') - (1/24/60) * (ROWNUM)) AS DT FROM DUAL CONNECT BY ROWNUM <= (TO_DATE('" + endTime + "', 'RRRRMMDDHH24MI') - TO_DATE('" + startTime + "', 'RRRRMMDDHH24MI')) * 60 * 24) TMP");    ///cdkim 2017.05.31 기간 검색 추가
            oStringBuilder.AppendLine("WHERE    RT.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            //oStringBuilder.AppendLine("         AND PTN.DT = TO_CHAR(TMP.DT, 'HH24:MI')");    ///cdkim 2017.05.31 WQ_PATTERN 데이터 부재로 인한 주석 처리
            oStringBuilder.AppendLine("ORDER BY DT");

            //oStringBuilder.Remove(0, oStringBuilder.Length);
            //oStringBuilder.AppendLine("SELECT   TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS DT,");
            //oStringBuilder.AppendLine("         DECODE(RT.CUR_VALUE, '', 0, RT.CUR_VALUE) AS CUR_VALUE,");
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_TOP_LIMIT), 3) AS T_ALERT1,");   //기준상한
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_LOW_LIMIT), 3) AS L_ALERT1,");   //기준하한
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.TOP_LIMIT) + (TO_NUMBER(PTN.TOP_LIMIT) * (TO_NUMBER(MAP.N2_TOP_LIMIT) / 100)), 3) AS T_ALERT2,");       //패턴상한
            //oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.LOWER_LIMIT) - (TO_NUMBER(PTN.LOWER_LIMIT) * (TO_NUMBER(MAP.N2_LOW_LIMIT) / 100)), 3) AS L_ALERT2");    //패턴하한
            //oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT " + strCOLUMN + " FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + strMONITOR_NO + "') AND TIMESTAMP BETWEEN SYSDATE - (41/(24*60)) AND SYSDATE - (10/(24*60))) RT,");
            //oStringBuilder.AppendLine("         (SELECT N1_TOP_LIMIT, N1_LOW_LIMIT, N2_TOP_LIMIT, N2_LOW_LIMIT FROM WQ_MONPNT_MAPPING WHERE MONITOR_NO = '" + strMONITOR_NO + "' AND WQ_ITEM_CODE = '" + m_RT_ITEM_CD + "') MAP,");
            //oStringBuilder.AppendLine("         (SELECT TIME AS DT, TOP_LIMIT, LOWER_LIMIT FROM WQ_PATTERN WHERE MONITOR_NO = '" + strMONITOR_NO + "' AND WQ_ITEM_CODE = '" + m_RT_ITEM_CD + "') PTN,");
            //oStringBuilder.AppendLine("         (SELECT ((TO_DATE('201407011402', 'YYYYMMDDHH24MI') - (9/(24*60))) - (1/24/60) * (ROWNUM)) AS DT FROM DUAL CONNECT BY ROWNUM <= (SYSDATE + ( 1 / 24 )) - (SYSDATE - (30/(24*60)) * 24 * 60)) TMP");
            //oStringBuilder.AppendLine("WHERE    RT.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            //oStringBuilder.AppendLine("         AND PTN.DT = TO_CHAR(TMP.DT, 'HH24:MI')");
            //oStringBuilder.AppendLine("ORDER BY DT");
            
            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RTDATA_CHART");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                strAlertOption = this.GetAlertOptionByMonitorPoint(strMONITOR_NO);

                DataTable oDT = pDS.Tables["RTDATA_CHART"];

                //Series = 감시지점
                this.cFXRT.Data.Series = 3;

                #region -- 차트 시리즈 스타일 설정

                this.cFXRT.Series[0].Gallery = Gallery.Curve;
                this.cFXRT.Series[0].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[0].Text = m_RT_ITEM_NM;
                this.cFXRT.Series[0].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[0].Line.Style = System.Drawing.Drawing2D.DashStyle.Solid;

                switch (strAlertOption)
                {
                    case "1":   //기준치
                        this.cFXRT.Series[1].Text = "기준 상한치";
                        this.cFXRT.Series[2].Text = "기준 하한치";
                        break;
                    case "2":   //패턴
                        this.cFXRT.Series[1].Text = "패턴 상한치";
                        this.cFXRT.Series[2].Text = "패턴 하한치";
                        break;
                }

                this.cFXRT.Series[1].Gallery = Gallery.Lines;
                this.cFXRT.Series[1].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[1].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[1].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;

                this.cFXRT.Series[2].Gallery = Gallery.Lines;
                this.cFXRT.Series[2].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[2].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[2].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;

                #endregion

                int i = 0;

                foreach (DataRow oDRow in oDT.Rows)
                {
                    this.cFXRT.AxisX.Labels[i] = oDRow["DT"].ToString().Substring(11);
                    this.cFXRT.Data[0, i] = Convert.ToDouble(oDRow["CUR_VALUE"]);

                    switch (strAlertOption)
                    {
                        case "1":   //기준치
                            this.cFXRT.Data[1, i] = Convert.ToDouble(oDRow["T_ALERT1"].ToString());
                            this.cFXRT.Data[2, i] = Convert.ToDouble(oDRow["L_ALERT1"].ToString());
                            break;
                        case "2":   //패턴
                            this.cFXRT.Data[1, i] = Convert.ToDouble(oDRow["T_ALERT2"].ToString());
                            this.cFXRT.Data[2, i] = Convert.ToDouble(oDRow["L_ALERT2"].ToString());
                            break;
                    }

                    i++;
                }
            }
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 감시지점의 감시 옵션을 취득해 반환한다.
        /// </summary>
        /// <param name="strMONITOR"></param>
        /// <returns></returns>
        private string GetAlertOptionByMonitorPoint(string strMONITOR)
        {
            string strRTN = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "2";
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   OPTIONS AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQ_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR + "'");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_MONITOR_OPTION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["RTNDATA"].ToString().Trim();
                    break;
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            return strRTN;
        }

        #endregion

        #region - Map 보기

        /// <summary>
        /// Grid에서 선택한 TAG의 블록을 찾아 Map을 표시하고 색을 변경한다.
        /// </summary>
        private void Viewmap_SelectedMonitorPoint(string strMONITOR_NO)
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            if (_Map != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(_Map, "감시지점");

                IFeature pFeature = ArcManager.GetFeature(pLayer, "NOS = '" + strMONITOR_NO + "'");

                if (pFeature == null) return;

                ArcManager.FlashShape(_Map.ActiveView, (IGeometry)pFeature.Shape, 10);

                if (ArcManager.GetMapScale(_Map) > (double)5000)
                {
                    ArcManager.SetMapScale(_Map, (double)5000);
                }

                ArcManager.MoveCenterAt(_Map, pFeature);

                ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();

                this.BringToFront();
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// Form을 Close하며 정리
        /// </summary>
        private void FormClose()
        {
            WQ_AppStatic.IS_SHOW_FORM_RT_DATA = false;
            if (m_oDBManager != null) this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }
        #endregion

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.cFXRT, "실시간수질감시", 0, 0, 9, 20);
                this.excelManager.AddWorksheet(this.ugRTData, "실시간수질감시", 10, 0);
                this.excelManager.Save("실시간수질감시", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
