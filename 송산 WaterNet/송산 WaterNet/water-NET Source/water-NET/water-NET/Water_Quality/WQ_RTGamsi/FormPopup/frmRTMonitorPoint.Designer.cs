﻿namespace WaterNet.WQ_RTGamsi.FormPopup
{
    partial class frmRTMonitorPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.uGridPoint = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rdoOPTIONS2 = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.rdoOPTIONS1 = new System.Windows.Forms.RadioButton();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtPLL_CU = new System.Windows.Forms.TextBox();
            this.txtPTL_CU = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtALL_CU = new System.Windows.Forms.TextBox();
            this.txtATL_CU = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtPLL_TE = new System.Windows.Forms.TextBox();
            this.txtPTL_TE = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtALL_TE = new System.Windows.Forms.TextBox();
            this.txtATL_TE = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtPLL_PH = new System.Windows.Forms.TextBox();
            this.txtPTL_PH = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtALL_PH = new System.Windows.Forms.TextBox();
            this.txtATL_PH = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPLL_TB = new System.Windows.Forms.TextBox();
            this.txtPTL_TB = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtALL_TB = new System.Windows.Forms.TextBox();
            this.txtATL_TB = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPLL_CL = new System.Windows.Forms.TextBox();
            this.txtPTL_CL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtALL_CL = new System.Windows.Forms.TextBox();
            this.txtATL_CL = new System.Windows.Forms.TextBox();
            this.cboTAGCU = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cboTAGTB = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cboTAGTE = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cboTAGPH = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboTAGCL = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.chkGItem4 = new System.Windows.Forms.CheckBox();
            this.chkGItem3 = new System.Windows.Forms.CheckBox();
            this.chkGItem2 = new System.Windows.Forms.CheckBox();
            this.chkGItem1 = new System.Windows.Forms.CheckBox();
            this.txtPOINT_NO = new System.Windows.Forms.TextBox();
            this.txtPOINT_NM = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cboBLOCK = new System.Windows.Forms.ComboBox();
            this.chkGItem5 = new System.Windows.Forms.CheckBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cboSBlock);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cboMBlock);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cboLBlock);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(810, 28);
            this.panel2.TabIndex = 112;
            // 
            // btnQuery
            // 
            this.btnQuery.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Query;
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(628, 0);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(90, 26);
            this.btnQuery.TabIndex = 153;
            this.btnQuery.Text = "지점 조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnClose.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Close2;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(718, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 26);
            this.btnClose.TabIndex = 154;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(417, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 148;
            this.label9.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(477, 3);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 20);
            this.cboSBlock.TabIndex = 147;
            this.cboSBlock.SelectedIndexChanged += new System.EventHandler(this.cboSBlock_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(211, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 146;
            this.label8.Text = "중블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(271, 3);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 20);
            this.cboMBlock.TabIndex = 145;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(7, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 144;
            this.label7.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(65, 3);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 20);
            this.cboLBlock.TabIndex = 143;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 560);
            this.picFrLeft.TabIndex = 110;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(814, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 560);
            this.PicFrRight.TabIndex = 111;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 564);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(818, 4);
            this.picFrBottom.TabIndex = 109;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(818, 4);
            this.picFrTop.TabIndex = 108;
            this.picFrTop.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(4, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(810, 4);
            this.pictureBox2.TabIndex = 147;
            this.pictureBox2.TabStop = false;
            // 
            // uGridPoint
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPoint.DisplayLayout.Appearance = appearance1;
            this.uGridPoint.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPoint.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPoint.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPoint.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPoint.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPoint.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPoint.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPoint.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPoint.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPoint.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPoint.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPoint.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPoint.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPoint.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPoint.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPoint.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPoint.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPoint.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPoint.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPoint.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPoint.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPoint.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPoint.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPoint.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPoint.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridPoint.Location = new System.Drawing.Point(4, 36);
            this.uGridPoint.Name = "uGridPoint";
            this.uGridPoint.Size = new System.Drawing.Size(810, 261);
            this.uGridPoint.TabIndex = 148;
            this.uGridPoint.Text = "ultraGrid1";
            this.uGridPoint.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPoint_DoubleClickRow);
            this.uGridPoint.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGridPoint_ClickCell);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(4, 297);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(810, 4);
            this.pictureBox1.TabIndex = 149;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnNew);
            this.panel6.Controls.Add(this.btnSave);
            this.panel6.Controls.Add(this.btnUpdate);
            this.panel6.Controls.Add(this.label38);
            this.panel6.Controls.Add(this.btnDelete);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(4, 301);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(810, 28);
            this.panel6.TabIndex = 150;
            // 
            // btnNew
            // 
            this.btnNew.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNew.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Clear;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNew.Location = new System.Drawing.Point(448, 0);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 26);
            this.btnNew.TabIndex = 159;
            this.btnNew.Text = "초기화";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSave.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(538, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 26);
            this.btnSave.TabIndex = 158;
            this.btnSave.Text = "지점 등록";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnUpdate.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Save;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.Location = new System.Drawing.Point(628, 0);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(90, 26);
            this.btnUpdate.TabIndex = 168;
            this.btnUpdate.Text = "정보 수정";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.Location = new System.Drawing.Point(7, 7);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(279, 12);
            this.label38.TabIndex = 167;
            this.label38.Text = "실시간 수질 감시 지점 정보 및 경보 옵션 관리";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(718, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 26);
            this.btnDelete.TabIndex = 157;
            this.btnDelete.Text = "지점 삭제";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(481, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "경보 방법 : ";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.rdoOPTIONS2);
            this.panel7.Controls.Add(this.label33);
            this.panel7.Controls.Add(this.rdoOPTIONS1);
            this.panel7.Controls.Add(this.label34);
            this.panel7.Controls.Add(this.label35);
            this.panel7.Controls.Add(this.txtPLL_CU);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.txtPTL_CU);
            this.panel7.Controls.Add(this.label36);
            this.panel7.Controls.Add(this.label37);
            this.panel7.Controls.Add(this.txtALL_CU);
            this.panel7.Controls.Add(this.txtATL_CU);
            this.panel7.Controls.Add(this.label20);
            this.panel7.Controls.Add(this.label21);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Controls.Add(this.txtPLL_TE);
            this.panel7.Controls.Add(this.txtPTL_TE);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.txtALL_TE);
            this.panel7.Controls.Add(this.txtATL_TE);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.txtPLL_PH);
            this.panel7.Controls.Add(this.txtPTL_PH);
            this.panel7.Controls.Add(this.label28);
            this.panel7.Controls.Add(this.label32);
            this.panel7.Controls.Add(this.txtALL_PH);
            this.panel7.Controls.Add(this.txtATL_PH);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.txtPLL_TB);
            this.panel7.Controls.Add(this.txtPTL_TB);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.txtALL_TB);
            this.panel7.Controls.Add(this.txtATL_TB);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.txtPLL_CL);
            this.panel7.Controls.Add(this.txtPTL_CL);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.txtALL_CL);
            this.panel7.Controls.Add(this.txtATL_CL);
            this.panel7.Controls.Add(this.cboTAGCU);
            this.panel7.Controls.Add(this.label31);
            this.panel7.Controls.Add(this.cboTAGTB);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.cboTAGTE);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.cboTAGPH);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.cboTAGCL);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.pictureBox3);
            this.panel7.Controls.Add(this.chkGItem4);
            this.panel7.Controls.Add(this.chkGItem3);
            this.panel7.Controls.Add(this.chkGItem2);
            this.panel7.Controls.Add(this.chkGItem1);
            this.panel7.Controls.Add(this.txtPOINT_NO);
            this.panel7.Controls.Add(this.txtPOINT_NM);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.cboBLOCK);
            this.panel7.Controls.Add(this.chkGItem5);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(4, 329);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(810, 235);
            this.panel7.TabIndex = 151;
            // 
            // rdoOPTIONS2
            // 
            this.rdoOPTIONS2.AutoSize = true;
            this.rdoOPTIONS2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoOPTIONS2.Location = new System.Drawing.Point(559, 203);
            this.rdoOPTIONS2.Name = "rdoOPTIONS2";
            this.rdoOPTIONS2.Size = new System.Drawing.Size(183, 16);
            this.rdoOPTIONS2.TabIndex = 194;
            this.rdoOPTIONS2.Text = "패턴 상/하한치로 경보발생";
            this.rdoOPTIONS2.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.Location = new System.Drawing.Point(384, 215);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 12);
            this.label33.TabIndex = 236;
            this.label33.Text = "(%)";
            // 
            // rdoOPTIONS1
            // 
            this.rdoOPTIONS1.AutoSize = true;
            this.rdoOPTIONS1.Checked = true;
            this.rdoOPTIONS1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoOPTIONS1.Location = new System.Drawing.Point(559, 181);
            this.rdoOPTIONS1.Name = "rdoOPTIONS1";
            this.rdoOPTIONS1.Size = new System.Drawing.Size(183, 16);
            this.rdoOPTIONS1.TabIndex = 193;
            this.rdoOPTIONS1.TabStop = true;
            this.rdoOPTIONS1.Text = "기준 상/하한치로 경보발생";
            this.rdoOPTIONS1.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.Location = new System.Drawing.Point(269, 215);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(12, 12);
            this.label34.TabIndex = 235;
            this.label34.Text = "/";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.Location = new System.Drawing.Point(55, 214);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(105, 12);
            this.label35.TabIndex = 234;
            this.label35.Text = "패턴 상/하한치 :";
            // 
            // txtPLL_CU
            // 
            this.txtPLL_CU.Location = new System.Drawing.Point(284, 211);
            this.txtPLL_CU.Name = "txtPLL_CU";
            this.txtPLL_CU.Size = new System.Drawing.Size(100, 21);
            this.txtPLL_CU.TabIndex = 20;
            this.txtPLL_CU.Text = "5";
            this.txtPLL_CU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPTL_CU
            // 
            this.txtPTL_CU.Location = new System.Drawing.Point(166, 211);
            this.txtPTL_CU.Name = "txtPTL_CU";
            this.txtPTL_CU.Size = new System.Drawing.Size(100, 21);
            this.txtPTL_CU.TabIndex = 19;
            this.txtPTL_CU.Text = "5";
            this.txtPTL_CU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.Location = new System.Drawing.Point(269, 192);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(12, 12);
            this.label36.TabIndex = 231;
            this.label36.Text = "/";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.Location = new System.Drawing.Point(55, 191);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(105, 12);
            this.label37.TabIndex = 230;
            this.label37.Text = "기준 상/하한치 :";
            // 
            // txtALL_CU
            // 
            this.txtALL_CU.Location = new System.Drawing.Point(284, 188);
            this.txtALL_CU.Name = "txtALL_CU";
            this.txtALL_CU.Size = new System.Drawing.Size(100, 21);
            this.txtALL_CU.TabIndex = 18;
            this.txtALL_CU.Text = "0";
            this.txtALL_CU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtATL_CU
            // 
            this.txtATL_CU.Location = new System.Drawing.Point(166, 188);
            this.txtATL_CU.Name = "txtATL_CU";
            this.txtATL_CU.Size = new System.Drawing.Size(100, 21);
            this.txtATL_CU.TabIndex = 17;
            this.txtATL_CU.Text = "0";
            this.txtATL_CU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(777, 147);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 12);
            this.label20.TabIndex = 227;
            this.label20.Text = "(%)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(662, 147);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(12, 12);
            this.label21.TabIndex = 226;
            this.label21.Text = "/";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(448, 146);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 12);
            this.label22.TabIndex = 225;
            this.label22.Text = "패턴 상/하한치 :";
            // 
            // txtPLL_TE
            // 
            this.txtPLL_TE.Location = new System.Drawing.Point(677, 143);
            this.txtPLL_TE.Name = "txtPLL_TE";
            this.txtPLL_TE.Size = new System.Drawing.Size(100, 21);
            this.txtPLL_TE.TabIndex = 16;
            this.txtPLL_TE.Text = "5";
            this.txtPLL_TE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPTL_TE
            // 
            this.txtPTL_TE.Location = new System.Drawing.Point(559, 143);
            this.txtPTL_TE.Name = "txtPTL_TE";
            this.txtPTL_TE.Size = new System.Drawing.Size(100, 21);
            this.txtPTL_TE.TabIndex = 15;
            this.txtPTL_TE.Text = "5";
            this.txtPTL_TE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(662, 124);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(12, 12);
            this.label23.TabIndex = 222;
            this.label23.Text = "/";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(448, 123);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 12);
            this.label24.TabIndex = 221;
            this.label24.Text = "기준 상/하한치 :";
            // 
            // txtALL_TE
            // 
            this.txtALL_TE.Location = new System.Drawing.Point(677, 120);
            this.txtALL_TE.Name = "txtALL_TE";
            this.txtALL_TE.Size = new System.Drawing.Size(100, 21);
            this.txtALL_TE.TabIndex = 14;
            this.txtALL_TE.Text = "0.0";
            this.txtALL_TE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtATL_TE
            // 
            this.txtATL_TE.Location = new System.Drawing.Point(559, 120);
            this.txtATL_TE.Name = "txtATL_TE";
            this.txtATL_TE.Size = new System.Drawing.Size(100, 21);
            this.txtATL_TE.TabIndex = 13;
            this.txtATL_TE.Text = "0.0";
            this.txtATL_TE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(384, 147);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(28, 12);
            this.label25.TabIndex = 218;
            this.label25.Text = "(%)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(269, 147);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(12, 12);
            this.label26.TabIndex = 217;
            this.label26.Text = "/";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(55, 146);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 12);
            this.label27.TabIndex = 216;
            this.label27.Text = "패턴 상/하한치 :";
            // 
            // txtPLL_PH
            // 
            this.txtPLL_PH.Location = new System.Drawing.Point(284, 143);
            this.txtPLL_PH.Name = "txtPLL_PH";
            this.txtPLL_PH.Size = new System.Drawing.Size(100, 21);
            this.txtPLL_PH.TabIndex = 12;
            this.txtPLL_PH.Text = "5";
            this.txtPLL_PH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPTL_PH
            // 
            this.txtPTL_PH.Location = new System.Drawing.Point(166, 143);
            this.txtPTL_PH.Name = "txtPTL_PH";
            this.txtPTL_PH.Size = new System.Drawing.Size(100, 21);
            this.txtPTL_PH.TabIndex = 11;
            this.txtPTL_PH.Text = "5";
            this.txtPTL_PH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(269, 124);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 12);
            this.label28.TabIndex = 213;
            this.label28.Text = "/";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.Location = new System.Drawing.Point(55, 123);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(105, 12);
            this.label32.TabIndex = 212;
            this.label32.Text = "기준 상/하한치 :";
            // 
            // txtALL_PH
            // 
            this.txtALL_PH.Location = new System.Drawing.Point(284, 120);
            this.txtALL_PH.Name = "txtALL_PH";
            this.txtALL_PH.Size = new System.Drawing.Size(100, 21);
            this.txtALL_PH.TabIndex = 10;
            this.txtALL_PH.Text = "5.0";
            this.txtALL_PH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtATL_PH
            // 
            this.txtATL_PH.Location = new System.Drawing.Point(166, 120);
            this.txtATL_PH.Name = "txtATL_PH";
            this.txtATL_PH.Size = new System.Drawing.Size(100, 21);
            this.txtATL_PH.TabIndex = 9;
            this.txtATL_PH.Text = "9.0";
            this.txtATL_PH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(777, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 12);
            this.label12.TabIndex = 209;
            this.label12.Text = "(%)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(662, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(12, 12);
            this.label15.TabIndex = 208;
            this.label15.Text = "/";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(448, 78);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 12);
            this.label16.TabIndex = 207;
            this.label16.Text = "패턴 상/하한치 :";
            // 
            // txtPLL_TB
            // 
            this.txtPLL_TB.Location = new System.Drawing.Point(677, 75);
            this.txtPLL_TB.Name = "txtPLL_TB";
            this.txtPLL_TB.Size = new System.Drawing.Size(100, 21);
            this.txtPLL_TB.TabIndex = 8;
            this.txtPLL_TB.Text = "5";
            this.txtPLL_TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPTL_TB
            // 
            this.txtPTL_TB.Location = new System.Drawing.Point(559, 75);
            this.txtPTL_TB.Name = "txtPTL_TB";
            this.txtPTL_TB.Size = new System.Drawing.Size(100, 21);
            this.txtPTL_TB.TabIndex = 7;
            this.txtPTL_TB.Text = "5";
            this.txtPTL_TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(662, 56);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 12);
            this.label18.TabIndex = 204;
            this.label18.Text = "/";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(448, 55);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(105, 12);
            this.label19.TabIndex = 203;
            this.label19.Text = "기준 상/하한치 :";
            // 
            // txtALL_TB
            // 
            this.txtALL_TB.Location = new System.Drawing.Point(677, 52);
            this.txtALL_TB.Name = "txtALL_TB";
            this.txtALL_TB.Size = new System.Drawing.Size(100, 21);
            this.txtALL_TB.TabIndex = 6;
            this.txtALL_TB.Text = "0.00";
            this.txtALL_TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtATL_TB
            // 
            this.txtATL_TB.Location = new System.Drawing.Point(559, 52);
            this.txtATL_TB.Name = "txtATL_TB";
            this.txtATL_TB.Size = new System.Drawing.Size(100, 21);
            this.txtATL_TB.TabIndex = 5;
            this.txtATL_TB.Text = "1.00";
            this.txtATL_TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(384, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 12);
            this.label11.TabIndex = 200;
            this.label11.Text = "(%)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(269, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 12);
            this.label4.TabIndex = 199;
            this.label4.Text = "/";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(55, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 12);
            this.label10.TabIndex = 198;
            this.label10.Text = "패턴 상/하한치 :";
            // 
            // txtPLL_CL
            // 
            this.txtPLL_CL.Location = new System.Drawing.Point(284, 75);
            this.txtPLL_CL.Name = "txtPLL_CL";
            this.txtPLL_CL.Size = new System.Drawing.Size(100, 21);
            this.txtPLL_CL.TabIndex = 4;
            this.txtPLL_CL.Text = "5";
            this.txtPLL_CL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPTL_CL
            // 
            this.txtPTL_CL.Location = new System.Drawing.Point(166, 75);
            this.txtPTL_CL.Name = "txtPTL_CL";
            this.txtPTL_CL.Size = new System.Drawing.Size(100, 21);
            this.txtPTL_CL.TabIndex = 3;
            this.txtPTL_CL.Text = "5";
            this.txtPTL_CL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(269, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 12);
            this.label2.TabIndex = 195;
            this.label2.Text = "/";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(55, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 12);
            this.label1.TabIndex = 194;
            this.label1.Text = "기준 상/하한치 :";
            // 
            // txtALL_CL
            // 
            this.txtALL_CL.Location = new System.Drawing.Point(284, 52);
            this.txtALL_CL.Name = "txtALL_CL";
            this.txtALL_CL.Size = new System.Drawing.Size(100, 21);
            this.txtALL_CL.TabIndex = 2;
            this.txtALL_CL.Text = "0.2";
            this.txtALL_CL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtATL_CL
            // 
            this.txtATL_CL.Location = new System.Drawing.Point(166, 52);
            this.txtATL_CL.Name = "txtATL_CL";
            this.txtATL_CL.Size = new System.Drawing.Size(100, 21);
            this.txtATL_CL.TabIndex = 1;
            this.txtATL_CL.Text = "1.0";
            this.txtATL_CL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboTAGCU
            // 
            this.cboTAGCU.FormattingEnabled = true;
            this.cboTAGCU.Location = new System.Drawing.Point(166, 166);
            this.cboTAGCU.Name = "cboTAGCU";
            this.cboTAGCU.Size = new System.Drawing.Size(218, 20);
            this.cboTAGCU.TabIndex = 191;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.Location = new System.Drawing.Point(119, 171);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 12);
            this.label31.TabIndex = 190;
            this.label31.Text = "태그 :";
            // 
            // cboTAGTB
            // 
            this.cboTAGTB.FormattingEnabled = true;
            this.cboTAGTB.Location = new System.Drawing.Point(559, 30);
            this.cboTAGTB.Name = "cboTAGTB";
            this.cboTAGTB.Size = new System.Drawing.Size(218, 20);
            this.cboTAGTB.TabIndex = 189;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(512, 33);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 12);
            this.label30.TabIndex = 188;
            this.label30.Text = "태그 :";
            // 
            // cboTAGTE
            // 
            this.cboTAGTE.FormattingEnabled = true;
            this.cboTAGTE.Location = new System.Drawing.Point(559, 98);
            this.cboTAGTE.Name = "cboTAGTE";
            this.cboTAGTE.Size = new System.Drawing.Size(218, 20);
            this.cboTAGTE.TabIndex = 187;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(512, 102);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 12);
            this.label29.TabIndex = 186;
            this.label29.Text = "태그 :";
            // 
            // cboTAGPH
            // 
            this.cboTAGPH.FormattingEnabled = true;
            this.cboTAGPH.Location = new System.Drawing.Point(166, 98);
            this.cboTAGPH.Name = "cboTAGPH";
            this.cboTAGPH.Size = new System.Drawing.Size(218, 20);
            this.cboTAGPH.TabIndex = 185;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(119, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 184;
            this.label5.Text = "태그 :";
            // 
            // cboTAGCL
            // 
            this.cboTAGCL.FormattingEnabled = true;
            this.cboTAGCL.Location = new System.Drawing.Point(166, 30);
            this.cboTAGCL.Name = "cboTAGCL";
            this.cboTAGCL.Size = new System.Drawing.Size(218, 20);
            this.cboTAGCL.TabIndex = 183;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(119, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "태그 :";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(810, 4);
            this.pictureBox3.TabIndex = 180;
            this.pictureBox3.TabStop = false;
            // 
            // chkGItem4
            // 
            this.chkGItem4.AutoSize = true;
            this.chkGItem4.Checked = true;
            this.chkGItem4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGItem4.Enabled = false;
            this.chkGItem4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem4.Location = new System.Drawing.Point(12, 32);
            this.chkGItem4.Name = "chkGItem4";
            this.chkGItem4.Size = new System.Drawing.Size(76, 16);
            this.chkGItem4.TabIndex = 177;
            this.chkGItem4.TabStop = false;
            this.chkGItem4.Text = "잔류염소";
            this.chkGItem4.UseVisualStyleBackColor = true;
            // 
            // chkGItem3
            // 
            this.chkGItem3.AutoSize = true;
            this.chkGItem3.Checked = true;
            this.chkGItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGItem3.Enabled = false;
            this.chkGItem3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem3.Location = new System.Drawing.Point(411, 32);
            this.chkGItem3.Name = "chkGItem3";
            this.chkGItem3.Size = new System.Drawing.Size(50, 16);
            this.chkGItem3.TabIndex = 176;
            this.chkGItem3.TabStop = false;
            this.chkGItem3.Text = "탁도";
            this.chkGItem3.UseVisualStyleBackColor = true;
            // 
            // chkGItem2
            // 
            this.chkGItem2.AutoSize = true;
            this.chkGItem2.Checked = true;
            this.chkGItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGItem2.Enabled = false;
            this.chkGItem2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem2.Location = new System.Drawing.Point(12, 101);
            this.chkGItem2.Name = "chkGItem2";
            this.chkGItem2.Size = new System.Drawing.Size(102, 16);
            this.chkGItem2.TabIndex = 175;
            this.chkGItem2.TabStop = false;
            this.chkGItem2.Text = "수소이온농도";
            this.chkGItem2.UseVisualStyleBackColor = true;
            // 
            // chkGItem1
            // 
            this.chkGItem1.AutoSize = true;
            this.chkGItem1.Checked = true;
            this.chkGItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGItem1.Enabled = false;
            this.chkGItem1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem1.Location = new System.Drawing.Point(411, 101);
            this.chkGItem1.Name = "chkGItem1";
            this.chkGItem1.Size = new System.Drawing.Size(50, 16);
            this.chkGItem1.TabIndex = 174;
            this.chkGItem1.TabStop = false;
            this.chkGItem1.Text = "온도";
            this.chkGItem1.UseVisualStyleBackColor = true;
            // 
            // txtPOINT_NO
            // 
            this.txtPOINT_NO.Location = new System.Drawing.Point(612, 7);
            this.txtPOINT_NO.Name = "txtPOINT_NO";
            this.txtPOINT_NO.ReadOnly = true;
            this.txtPOINT_NO.Size = new System.Drawing.Size(187, 21);
            this.txtPOINT_NO.TabIndex = 172;
            this.txtPOINT_NO.TabStop = false;
            // 
            // txtPOINT_NM
            // 
            this.txtPOINT_NM.Location = new System.Drawing.Point(287, 7);
            this.txtPOINT_NM.Name = "txtPOINT_NM";
            this.txtPOINT_NM.Size = new System.Drawing.Size(193, 21);
            this.txtPOINT_NM.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(487, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 12);
            this.label13.TabIndex = 167;
            this.label13.Text = "감시지점일련번호 :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(201, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 12);
            this.label14.TabIndex = 166;
            this.label14.Text = "감시지점명 :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(8, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 163;
            this.label17.Text = "블록 :";
            // 
            // cboBLOCK
            // 
            this.cboBLOCK.FormattingEnabled = true;
            this.cboBLOCK.Location = new System.Drawing.Point(55, 7);
            this.cboBLOCK.Name = "cboBLOCK";
            this.cboBLOCK.Size = new System.Drawing.Size(140, 20);
            this.cboBLOCK.TabIndex = 162;
            this.cboBLOCK.SelectedIndexChanged += new System.EventHandler(this.cboBLOCK_SelectedIndexChanged);
            // 
            // chkGItem5
            // 
            this.chkGItem5.AutoSize = true;
            this.chkGItem5.Checked = true;
            this.chkGItem5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGItem5.Enabled = false;
            this.chkGItem5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGItem5.Location = new System.Drawing.Point(12, 170);
            this.chkGItem5.Name = "chkGItem5";
            this.chkGItem5.Size = new System.Drawing.Size(89, 16);
            this.chkGItem5.TabIndex = 178;
            this.chkGItem5.TabStop = false;
            this.chkGItem5.Text = "전기전도도";
            this.chkGItem5.UseVisualStyleBackColor = true;
            // 
            // frmRTMonitorPoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 568);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.uGridPoint);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmRTMonitorPoint";
            this.Text = "실시간 수질 감시 지점 등록";
            this.Load += new System.EventHandler(this.frmRTMonitorPoint_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRTMonitorPoint_FormClosing);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPoint;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox cboTAGCU;
        private System.Windows.Forms.ComboBox cboTAGTB;
        private System.Windows.Forms.ComboBox cboTAGTE;
        private System.Windows.Forms.ComboBox cboTAGPH;
        private System.Windows.Forms.ComboBox cboTAGCL;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.CheckBox chkGItem4;
        private System.Windows.Forms.CheckBox chkGItem3;
        private System.Windows.Forms.CheckBox chkGItem2;
        private System.Windows.Forms.CheckBox chkGItem1;
        private System.Windows.Forms.TextBox txtPOINT_NO;
        private System.Windows.Forms.TextBox txtPOINT_NM;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboBLOCK;
        private System.Windows.Forms.CheckBox chkGItem5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rdoOPTIONS2;
        private System.Windows.Forms.RadioButton rdoOPTIONS1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtALL_CL;
        private System.Windows.Forms.TextBox txtATL_CL;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPLL_TB;
        private System.Windows.Forms.TextBox txtPTL_TB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtALL_TB;
        private System.Windows.Forms.TextBox txtATL_TB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPLL_CL;
        private System.Windows.Forms.TextBox txtPTL_CL;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtPLL_CU;
        private System.Windows.Forms.TextBox txtPTL_CU;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtALL_CU;
        private System.Windows.Forms.TextBox txtATL_CU;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtPLL_TE;
        private System.Windows.Forms.TextBox txtPTL_TE;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtALL_TE;
        private System.Windows.Forms.TextBox txtATL_TE;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtPLL_PH;
        private System.Windows.Forms.TextBox txtPTL_PH;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtALL_PH;
        private System.Windows.Forms.TextBox txtATL_PH;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button btnUpdate;
    }
}