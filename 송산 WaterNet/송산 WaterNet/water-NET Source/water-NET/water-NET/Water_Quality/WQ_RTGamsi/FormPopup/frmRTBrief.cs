﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;

namespace WaterNet.WQ_RTGamsi.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A02
    /// Project Explain : 실시간감시
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.28
    /// Form Explain : 감시지점에 대한 실시간 간략 정보 Popup Form
    /// </summary>
    public partial class frmRTBrief : Form
    {
        private OracleDBManager m_oDBManager = null;

        private string _NO;
        private string _NAME;

        public string NO
        {
            get
            {
                return _NO;
            }
            set
            {
                _NO = value;
            }
        }

        public string NAME
        {
            get
            {
                return _NAME;
            }
            set
            {
                _NAME = value;
            }
        }

        public frmRTBrief()
        {
            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            InitializeComponent();
        }

        private void frmRTBrief_Load(object sender, EventArgs e)
        {
            this.tmrGetData.Stop();
            this.tmrGetData.Interval = 100;
            this.tmrGetData.Start();
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_DETAIL == false)
            {
                FormPopup.frmRTBriefDetail oForm = new FormPopup.frmRTBriefDetail();
                oForm.MONITOR_NO = _NO;
                oForm.MONITOR_NM = _NAME;
                oForm.StartPosition = FormStartPosition.WindowsDefaultBounds;
                oForm.TopLevel = false;
                oForm.Parent = this.ParentForm;
                oForm.Tag = "RTDetail";
                //oForm.SetBounds(this.Width - this.axToolbar.Width - 4, this.axToolbar.Height + 2, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        private void tmrGetData_Tick(object sender, EventArgs e)
        {
            this.tmrGetData.Stop();
            this.GetRTBriefData();
            this.tmrGetData.Interval = 60000;
            this.tmrGetData.Start();
        }

        private void GetRTBriefData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   PNT.MONPNT || ' (' || PNT.MONITOR_NO || ')' AS MON_PNT,");
            oStringBuilder.AppendLine("         TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS DT,");
            oStringBuilder.AppendLine("         DECODE(CL.CUR_VALUE, '', 0, CL.CUR_VALUE) AS CL_VAL,");
            oStringBuilder.AppendLine("         DECODE(TB.CUR_VALUE, '', 0, TB.CUR_VALUE) AS TB_VAL,");
            oStringBuilder.AppendLine("         DECODE(PH.CUR_VALUE, '', 0, PH.CUR_VALUE) AS PH_VAL,");
            oStringBuilder.AppendLine("         DECODE(TE.CUR_VALUE, '', 0, TE.CUR_VALUE) AS TE_VAL,");
            oStringBuilder.AppendLine("         DECODE(CU.CUR_VALUE, '', 0, CU.CUR_VALUE) AS CU_VAL");
            oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CL FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _NO + "') AND TO_CHAR(TIMESTAMP, 'RRRRMMDDHH24MI') = TO_CHAR(SYSDATE - (10/(24*60)), 'RRRRMMDDHH24MI')) CL,");
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TB FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _NO + "') AND TO_CHAR(TIMESTAMP, 'RRRRMMDDHH24MI') = TO_CHAR(SYSDATE - (10/(24*60)), 'RRRRMMDDHH24MI')) TB,");
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_PH FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _NO + "') AND TO_CHAR(TIMESTAMP, 'RRRRMMDDHH24MI') = TO_CHAR(SYSDATE - (10/(24*60)), 'RRRRMMDDHH24MI')) PH,");
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _NO + "') AND TO_CHAR(TIMESTAMP, 'RRRRMMDDHH24MI') = TO_CHAR(SYSDATE - (10/(24*60)), 'RRRRMMDDHH24MI')) TE,");
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CU FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _NO + "') AND TO_CHAR(TIMESTAMP, 'RRRRMMDDHH24MI') = TO_CHAR(SYSDATE - (10/(24*60)), 'RRRRMMDDHH24MI')) CU,");
            oStringBuilder.AppendLine("         (SELECT MONITOR_NO, MONPNT, SBLOCK_CODE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _NO + "') PNT,");
            oStringBuilder.AppendLine("         (SELECT ((SYSDATE - (9/(24*60))) - (1/24/60) * (ROWNUM)) AS DT FROM DUAL CONNECT BY ROWNUM <= 1) TMP");
            oStringBuilder.AppendLine("WHERE    CL.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND PH.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND TB.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND TE.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND CU.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            
            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_BRIEF");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.lblRTPntNm.Text = oDRow["MON_PNT"].ToString();
                    this.lblRTDT.Text = oDRow["DT"].ToString();
                    this.txtCL.Text = oDRow["CL_VAL"].ToString();
                    this.txtPH.Text = oDRow["PH_VAL"].ToString();
                    this.txtTB.Text = oDRow["TB_VAL"].ToString();
                    this.txtTE.Text = oDRow["TE_VAL"].ToString();
                    this.txtCU.Text = oDRow["CU_VAL"].ToString();

                    break;
                }
            }
        }
    }
}
