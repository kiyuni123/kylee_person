﻿namespace WaterNet.WQ_RTGamsi.FormPopup
{
    partial class frmRTBriefDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.panel32 = new System.Windows.Forms.Panel();
            this.cFXRT = new ChartFX.WinForms.Chart();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.uGridPopup = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.rdoCU = new System.Windows.Forms.RadioButton();
            this.rdoTE = new System.Windows.Forms.RadioButton();
            this.rdoPH = new System.Windows.Forms.RadioButton();
            this.rdoTB = new System.Windows.Forms.RadioButton();
            this.rdoCL = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.uDTE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDTS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboBaseTime = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXRT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDTS)).BeginInit();
            this.SuspendLayout();
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 555);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(676, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 555);
            this.PicFrRight.TabIndex = 107;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 559);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(680, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(680, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.SystemColors.Control;
            this.panel32.Controls.Add(this.cFXRT);
            this.panel32.Controls.Add(this.pictureBox6);
            this.panel32.Controls.Add(this.uGridPopup);
            this.panel32.Controls.Add(this.pictureBox1);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(4, 52);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(672, 507);
            this.panel32.TabIndex = 137;
            // 
            // cFXRT
            // 
            this.cFXRT.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.cFXRT.AxisX.Staggered = true;
            this.cFXRT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cFXRT.Location = new System.Drawing.Point(0, 213);
            this.cFXRT.Name = "cFXRT";
            this.cFXRT.Size = new System.Drawing.Size(672, 294);
            this.cFXRT.TabIndex = 140;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Gold;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(0, 209);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(672, 4);
            this.pictureBox6.TabIndex = 139;
            this.pictureBox6.TabStop = false;
            // 
            // uGridPopup
            // 
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPopup.DisplayLayout.Appearance = appearance73;
            this.uGridPopup.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPopup.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance74.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.GroupByBox.Appearance = appearance74;
            appearance75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.BandLabelAppearance = appearance75;
            this.uGridPopup.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance76.BackColor2 = System.Drawing.SystemColors.Control;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.PromptAppearance = appearance76;
            this.uGridPopup.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPopup.DisplayLayout.MaxRowScrollRegions = 1;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPopup.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            appearance78.BackColor = System.Drawing.SystemColors.Highlight;
            appearance78.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPopup.DisplayLayout.Override.ActiveRowAppearance = appearance78;
            this.uGridPopup.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPopup.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.CardAreaAppearance = appearance79;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            appearance80.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPopup.DisplayLayout.Override.CellAppearance = appearance80;
            this.uGridPopup.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPopup.DisplayLayout.Override.CellPadding = 0;
            appearance81.BackColor = System.Drawing.SystemColors.Control;
            appearance81.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance81.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance81.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.GroupByRowAppearance = appearance81;
            appearance82.TextHAlignAsString = "Left";
            this.uGridPopup.DisplayLayout.Override.HeaderAppearance = appearance82;
            this.uGridPopup.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPopup.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.Color.Silver;
            this.uGridPopup.DisplayLayout.Override.RowAppearance = appearance83;
            this.uGridPopup.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPopup.DisplayLayout.Override.TemplateAddRowAppearance = appearance84;
            this.uGridPopup.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPopup.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPopup.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPopup.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridPopup.Location = new System.Drawing.Point(0, 4);
            this.uGridPopup.Name = "uGridPopup";
            this.uGridPopup.Size = new System.Drawing.Size(672, 205);
            this.uGridPopup.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(672, 4);
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.SystemColors.Control;
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.rdoCU);
            this.panel34.Controls.Add(this.rdoTE);
            this.panel34.Controls.Add(this.rdoPH);
            this.panel34.Controls.Add(this.rdoTB);
            this.panel34.Controls.Add(this.rdoCL);
            this.panel34.Controls.Add(this.label4);
            this.panel34.Controls.Add(this.uDTE);
            this.panel34.Controls.Add(this.uDTS);
            this.panel34.Controls.Add(this.label6);
            this.panel34.Controls.Add(this.label7);
            this.panel34.Controls.Add(this.cboBaseTime);
            this.panel34.Controls.Add(this.label1);
            this.panel34.Controls.Add(this.btnQuery);
            this.panel34.Controls.Add(this.btnClose);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel34.Location = new System.Drawing.Point(4, 4);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(672, 48);
            this.panel34.TabIndex = 136;
            // 
            // rdoCU
            // 
            this.rdoCU.AutoSize = true;
            this.rdoCU.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoCU.Location = new System.Drawing.Point(400, 27);
            this.rdoCU.Name = "rdoCU";
            this.rdoCU.Size = new System.Drawing.Size(88, 16);
            this.rdoCU.TabIndex = 181;
            this.rdoCU.TabStop = true;
            this.rdoCU.Text = "전기전도도";
            this.rdoCU.UseVisualStyleBackColor = true;
            this.rdoCU.Click += new System.EventHandler(this.rdoCU_Click);
            // 
            // rdoTE
            // 
            this.rdoTE.AutoSize = true;
            this.rdoTE.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoTE.Location = new System.Drawing.Point(345, 27);
            this.rdoTE.Name = "rdoTE";
            this.rdoTE.Size = new System.Drawing.Size(49, 16);
            this.rdoTE.TabIndex = 180;
            this.rdoTE.TabStop = true;
            this.rdoTE.Text = "온도";
            this.rdoTE.UseVisualStyleBackColor = true;
            this.rdoTE.Click += new System.EventHandler(this.rdoTE_Click);
            // 
            // rdoPH
            // 
            this.rdoPH.AutoSize = true;
            this.rdoPH.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoPH.Location = new System.Drawing.Point(238, 27);
            this.rdoPH.Name = "rdoPH";
            this.rdoPH.Size = new System.Drawing.Size(101, 16);
            this.rdoPH.TabIndex = 179;
            this.rdoPH.TabStop = true;
            this.rdoPH.Text = "수소이온농도";
            this.rdoPH.UseVisualStyleBackColor = true;
            this.rdoPH.Click += new System.EventHandler(this.rdoPH_Click);
            // 
            // rdoTB
            // 
            this.rdoTB.AutoSize = true;
            this.rdoTB.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoTB.Location = new System.Drawing.Point(183, 27);
            this.rdoTB.Name = "rdoTB";
            this.rdoTB.Size = new System.Drawing.Size(49, 16);
            this.rdoTB.TabIndex = 178;
            this.rdoTB.TabStop = true;
            this.rdoTB.Text = "탁도";
            this.rdoTB.UseVisualStyleBackColor = true;
            this.rdoTB.Click += new System.EventHandler(this.rdoTB_Click);
            // 
            // rdoCL
            // 
            this.rdoCL.AutoSize = true;
            this.rdoCL.Checked = true;
            this.rdoCL.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoCL.Location = new System.Drawing.Point(102, 27);
            this.rdoCL.Name = "rdoCL";
            this.rdoCL.Size = new System.Drawing.Size(75, 16);
            this.rdoCL.TabIndex = 177;
            this.rdoCL.TabStop = true;
            this.rdoCL.Text = "잔류염소";
            this.rdoCL.UseVisualStyleBackColor = true;
            this.rdoCL.Click += new System.EventHandler(this.rdoCL_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(5, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 12);
            this.label4.TabIndex = 176;
            this.label4.Text = "차트표시항목 :";
            // 
            // uDTE
            // 
            this.uDTE.Location = new System.Drawing.Point(192, 3);
            this.uDTE.Name = "uDTE";
            this.uDTE.Size = new System.Drawing.Size(100, 20);
            this.uDTE.TabIndex = 165;
            // 
            // uDTS
            // 
            this.uDTS.Location = new System.Drawing.Point(77, 3);
            this.uDTS.Name = "uDTS";
            this.uDTS.Size = new System.Drawing.Size(100, 20);
            this.uDTS.TabIndex = 164;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(178, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 12);
            this.label6.TabIndex = 163;
            this.label6.Text = "~";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(5, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 12);
            this.label7.TabIndex = 162;
            this.label7.Text = "조회일자 :";
            // 
            // cboBaseTime
            // 
            this.cboBaseTime.FormattingEnabled = true;
            this.cboBaseTime.Location = new System.Drawing.Point(421, 3);
            this.cboBaseTime.Name = "cboBaseTime";
            this.cboBaseTime.Size = new System.Drawing.Size(80, 19);
            this.cboBaseTime.TabIndex = 43;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(298, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 12);
            this.label1.TabIndex = 40;
            this.label1.Text = "조회필터(분)단위 : ";
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Query;
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(527, 10);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(70, 26);
            this.btnQuery.TabIndex = 38;
            this.btnQuery.Text = "조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Close2;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(596, 10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 26);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmRTBriefDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 563);
            this.Controls.Add(this.panel32);
            this.Controls.Add(this.panel34);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.MinimizeBox = false;
            this.Name = "frmRTBriefDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "실시간 수질 감시 지점의 상세 정보";
            this.Load += new System.EventHandler(this.frmRTBriefDetail_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRTBriefDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXRT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDTS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel32;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPopup;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboBaseTime;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDTE;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDTS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private ChartFX.WinForms.Chart cFXRT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rdoCU;
        private System.Windows.Forms.RadioButton rdoTE;
        private System.Windows.Forms.RadioButton rdoPH;
        private System.Windows.Forms.RadioButton rdoTB;
        private System.Windows.Forms.RadioButton rdoCL;
    }
}