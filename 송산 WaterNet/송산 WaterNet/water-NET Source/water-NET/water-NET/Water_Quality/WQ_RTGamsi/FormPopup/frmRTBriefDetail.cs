﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

#endregion

using ChartFX.WinForms;
using EMFrame.log;

namespace WaterNet.WQ_RTGamsi.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A02
    /// Project Explain : 실시간감시
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.27
    /// Form Explain : 실시간감시 감시지점 상세정보 Popup Form
    /// </summary>
    public partial class frmRTBriefDetail : Form
    {
        private OracleDBManager m_oDBManager = null;

        #region 프로퍼티 

        string _MONITOR_NO = string.Empty;

        /// <summary>
        /// 감시지점일련번호
        /// </summary>
        public string MONITOR_NO
        {
            get
            {
                return _MONITOR_NO;
            }
            set
            {
                _MONITOR_NO = value;
            }
        }

        string _MONITOR_NM = string.Empty;

        /// <summary>
        /// 감시지점명
        /// </summary>
        public string MONITOR_NM
        {
            get
            {
                return _MONITOR_NM;
            }
            set
            {
                _MONITOR_NM = value;
            }
        }

        #endregion

        string m_RT_ITEM_CD = string.Empty;
        string m_RT_ITEM_NM = string.Empty;

        public frmRTBriefDetail()
        {
            WQ_AppStatic.IS_SHOW_FORM_DETAIL = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.ClosePopup();
                return;
            }

            InitializeComponent();

            this.InitializeSetting();

            this.InitializeChartSetting();
        }

        private void frmRTBriefDetail_Load(object sender, EventArgs e)
        {
            WQ_Function.SetUDateTime_MaskInput(this.uDTS, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.uDTS, -1);
            WQ_Function.SetUDateTime_MaskInput(this.uDTE, 0, 0);

            this.Text = "실시간 수질 감시 지점(" + _MONITOR_NM + ")의 상세 정보 ";

            WQ_Function.SetCombo_BaseDisplayTime(this.cboBaseTime);

            this.GetMonitorPosDetailData();

            this.rdoCL.Checked = true;
            m_RT_ITEM_CD = "CL";
            m_RT_ITEM_NM = "잔류염소";

            if (this.uGridPopup.Rows.Count <= 0) return;

            this.SetChartByDetailData();
        }

        private void frmRTBriefDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.ClosePopup();
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetMonitorPosDetailData();

                this.InitializeChartSetting();

                if (this.uGridPopup.Rows.Count <= 0) return;

                this.SetChartByDetailData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void rdoCL_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "CL";
            m_RT_ITEM_NM = "잔류염소";

            this.SetChartByDetailData();
        }

        private void rdoTB_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "TB";
            m_RT_ITEM_NM = "탁도";

            this.SetChartByDetailData();
        }

        private void rdoPH_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "PH";
            m_RT_ITEM_NM = "수소이온농도";

            this.SetChartByDetailData();
        }

        private void rdoTE_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "TE";
            m_RT_ITEM_NM = "온도";

            this.SetChartByDetailData();
        }

        private void rdoCU_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            m_RT_ITEM_CD = "CU";
            m_RT_ITEM_NM = "전기전도도";

            this.SetChartByDetailData();
        }

        #endregion


        #region User Function

        #region - Initialize Control

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region 실시간감시지점에 대한 실시간데이터

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MON_PNT";
            oUltraGridColumn.Header.Caption = "감시지점";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DT";
            oUltraGridColumn.Header.Caption = "측정일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CL_VAL";
            oUltraGridColumn.Header.Caption = "잔류염소(CL)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TB_VAL";
            oUltraGridColumn.Header.Caption = "탁도(TB)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PH_VAL";
            oUltraGridColumn.Header.Caption = "수소이온농도(pH)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TE_VAL";
            oUltraGridColumn.Header.Caption = "온도(TE)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();

            oUltraGridColumn.Key = "CU_VAL";
            oUltraGridColumn.Header.Caption = "전기전도도(CU)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridPopup);

            //uGridPopup.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            #endregion
        }

        /// <summary>
        /// Chart 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
            this.cFXRT.Data.Clear();
            this.cFXRT.AxisX.Staggered = true;
            this.cFXRT.AxisY.Staggered = true;
            this.cFXRT.LegendBox.Dock = DockArea.Top;
        }

        #endregion

        #region SQL & Chart Function

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetMonitorPosDetailData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            string strFilterTime = string.Empty;
            string strFilterSQL = string.Empty;

            DataSet pDS = new DataSet();

            strFilterTime = this.cboBaseTime.Text;
            strFilterTime = WQ_Function.SplitToCodeName(strFilterTime);
            string strDTS = WQ_Function.StringToDateTime(this.uDTS.DateTime);
            string strDTE = WQ_Function.StringToDateTime(this.uDTE.DateTime);
            string strNOW_TIME = WQ_Function.ChangeDateTimeToDateTime(DateTime.Now.AddMinutes(-9));
            strNOW_TIME = strNOW_TIME.Substring(strNOW_TIME.Length - 6, 4);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   (SELECT LOC_NAME || ' (' || LOC_CODE || ')' FROM CM_LOCATION WHERE LOC_CODE = PNT.SBLOCK_CODE) AS BLOCK,");
            oStringBuilder.AppendLine("         PNT.MONPNT || ' (' || PNT.MONITOR_NO || ')' AS MON_PNT,");
            oStringBuilder.AppendLine("         TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS DT,");
            oStringBuilder.AppendLine("         DECODE(CL.CUR_VALUE, '', 0, CL.CUR_VALUE) AS CL_VAL,");
            oStringBuilder.AppendLine("         DECODE(TB.CUR_VALUE, '', 0, TB.CUR_VALUE) AS TB_VAL,");
            oStringBuilder.AppendLine("         DECODE(PH.CUR_VALUE, '', 0, PH.CUR_VALUE) AS PH_VAL,");            
            oStringBuilder.AppendLine("         DECODE(TE.CUR_VALUE, '', 0, TE.CUR_VALUE) AS TE_VAL,");
            oStringBuilder.AppendLine("         DECODE(CU.CUR_VALUE, '', 0, CU.CUR_VALUE) AS CU_VAL");
            oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CL FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS')) CL,");            
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TB FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS')) TB,");
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_PH FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS')) PH,");
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS')) TE,");
            oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CU FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS')) CU,");
            oStringBuilder.AppendLine("         (SELECT MONITOR_NO, MONPNT, SBLOCK_CODE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') PNT,");
            oStringBuilder.AppendLine("         (SELECT TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS') - ((1/24/60) * " + strFilterTime + " * ROWNUM) AS DT FROM DUAL CONNECT BY ROWNUM <= ((TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS') - TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS')) * 24 * 60) / " + strFilterTime + ") TMP");
            oStringBuilder.AppendLine("WHERE    CL.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND PH.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND TB.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND TE.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND CU.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("ORDER BY DT DESC, BLOCK, MON_PNT");
            
            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_RT_DETAIL");

            this.uGridPopup.DataSource = pDS.Tables["WQ_RT_DETAIL"];

            if (this.uGridPopup.Rows.Count > 0) this.uGridPopup.Rows[0].Activated = true;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// Chart FX를 Setting 해서 Data를 연결한다.
        /// 항목별 분석 그리드에 대한
        /// </summary>
        private void SetChartByDetailData()
        {
            string strCOLUMN = string.Empty;
            string strAlertOption = string.Empty; //1 : 기준치로 경보, 2 : 패턴으로 경보

            //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
            this.cFXRT.Data.Clear();
            this.cFXRT.Series.Clear();
            this.cFXRT.DataSourceSettings.Fields.Clear();

            //Chart 타이틀 생성
            TitleDockable td = new TitleDockable();
            td.Font = new Font("굴림", 10, FontStyle.Bold);
            td.TextColor = Color.DarkBlue;
            td.Text = _MONITOR_NM;
            this.cFXRT.Titles.Clear();
            this.cFXRT.Titles.Add(td);

            #region -- 항목별 Y축 최대, 최소, 스탭 설정

            switch (m_RT_ITEM_CD)
            {
                case "CL":
                    strCOLUMN = "TAG_ID_CL";
                    this.cFXRT.Panes[0].AxisY.Max = 1.5;
                    this.cFXRT.Panes[0].AxisY.Min = 0.1;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.0";
                    break;
                case "TB":
                    strCOLUMN = "TAG_ID_TB";
                    this.cFXRT.Panes[0].AxisY.Max = 2.0;
                    this.cFXRT.Panes[0].AxisY.Min = 0.01;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.00";
                    break;
                case "PH":
                    strCOLUMN = "TAG_ID_PH";
                    this.cFXRT.Panes[0].AxisY.Max = 14.0;
                    this.cFXRT.Panes[0].AxisY.Min = 1.0;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.0";
                    break;
                case "TE":
                    strCOLUMN = "TAG_ID_TE";
                    this.cFXRT.Panes[0].AxisY.Max = 30;
                    this.cFXRT.Panes[0].AxisY.Min = -10;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.0";
                    break;
                case "CU":
                    strCOLUMN = "TAG_ID_CU";
                    this.cFXRT.Panes[0].AxisY.Max = 200;
                    this.cFXRT.Panes[0].AxisY.Min = 80;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0";
                    break;
            }

            this.cFXRT.Panes[0].AxisY.Step = this.cFXRT.Panes[0].AxisY.Max / 20;

            #endregion

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            string strFilterTime = string.Empty;
            string strFilterSQL = string.Empty;

            DataSet pDS = new DataSet();

            strFilterTime = this.cboBaseTime.Text;
            strFilterTime = WQ_Function.SplitToCodeName(strFilterTime);
            string strDTS = WQ_Function.StringToDateTime(this.uDTS.DateTime);
            string strDTE = WQ_Function.StringToDateTime(this.uDTE.DateTime);
            string strNOW_TIME = WQ_Function.ChangeDateTimeToDateTime(DateTime.Now.AddMinutes(-9));
            strNOW_TIME = strNOW_TIME.Substring(strNOW_TIME.Length - 6, 4);

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   TO_CHAR(RT.TIMESTAMP, 'RRRR-MM-DD HH24:MI') AS DT,");
            oStringBuilder.AppendLine("         RT.CUR_VALUE AS CUR_VALUE,");
            oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_TOP_LIMIT), 3) AS T_ALERT1,");   //기준상한
            oStringBuilder.AppendLine("         ROUND(TO_NUMBER(MAP.N1_LOW_LIMIT), 3) AS L_ALERT1,");   //기준하한
            oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.TOP_LIMIT) + (TO_NUMBER(PTN.TOP_LIMIT) * (TO_NUMBER(MAP.N2_TOP_LIMIT) / 100)), 3) AS T_ALERT2,");       //패턴상한
            oStringBuilder.AppendLine("         ROUND(TO_NUMBER(PTN.LOWER_LIMIT) - (TO_NUMBER(PTN.LOWER_LIMIT) * (TO_NUMBER(MAP.N2_LOW_LIMIT) / 100)), 3) AS L_ALERT2");    //패턴하한
            oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT " + strCOLUMN + " FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + _MONITOR_NO + "') AND TIMESTAMP BETWEEN TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS') AND TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS')) RT,");
            oStringBuilder.AppendLine("         (SELECT N1_TOP_LIMIT, N1_LOW_LIMIT, N2_TOP_LIMIT, N2_LOW_LIMIT FROM WQ_MONPNT_MAPPING WHERE MONITOR_NO = '" + _MONITOR_NO + "' AND WQ_ITEM_CODE = '" + m_RT_ITEM_CD + "') MAP,");
            oStringBuilder.AppendLine("         (SELECT TIME AS DT, TOP_LIMIT, LOWER_LIMIT FROM WQ_PATTERN WHERE MONITOR_NO = '" + _MONITOR_NO + "' AND WQ_ITEM_CODE = '" + m_RT_ITEM_CD + "') PTN,");
            oStringBuilder.AppendLine("         (SELECT TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS') - ((1/24/60) * " + strFilterTime + " * ROWNUM) AS DT FROM DUAL CONNECT BY ROWNUM <= ((TO_DATE('" + strDTE + strNOW_TIME + "00', 'RRRRMMDDHH24MISS') - TO_DATE('" + strDTS + "000000', 'RRRRMMDDHH24MISS')) * 24 * 60) / " + strFilterTime + ") TMP");
            oStringBuilder.AppendLine("WHERE    RT.TIMESTAMP = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND PTN.DT = TO_CHAR(RT.TIMESTAMP, 'HH24:MI')");
            oStringBuilder.AppendLine("ORDER BY DT");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RTDATA_CHART");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                strAlertOption = this.GetAlertOptionByMonitorPoint(_MONITOR_NO);

                DataTable oDT = pDS.Tables["RTDATA_CHART"];

                //Series = 감시지점
                this.cFXRT.Data.Series = 3;

                #region -- 차트 시리즈 스타일 설정

                this.cFXRT.Series[0].Gallery = Gallery.Curve;
                this.cFXRT.Series[0].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[0].Text = m_RT_ITEM_NM;
                this.cFXRT.Series[0].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[0].Line.Style = System.Drawing.Drawing2D.DashStyle.Solid;

                switch (strAlertOption)
                {
                    case "1":   //기준치
                        this.cFXRT.Series[1].Text = "기준 상한치";
                        this.cFXRT.Series[2].Text = "기준 하한치";
                        break;
                    case "2":   //패턴
                        this.cFXRT.Series[1].Text = "패턴 상한치";
                        this.cFXRT.Series[2].Text = "패턴 하한치";
                        break;
                }

                this.cFXRT.Series[1].Gallery = Gallery.Lines;
                this.cFXRT.Series[1].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[1].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[1].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;

                this.cFXRT.Series[2].Gallery = Gallery.Lines;
                this.cFXRT.Series[2].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[2].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[2].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;

                #endregion

                int i = 0;

                foreach (DataRow oDRow in oDT.Rows)
                {
                    this.cFXRT.AxisX.Labels[i] = oDRow["DT"].ToString().Substring(11);
                    this.cFXRT.Data[0, i] = Convert.ToDouble(oDRow["CUR_VALUE"]);

                    switch (strAlertOption)
                    {
                        case "1":   //기준치
                            this.cFXRT.Data[1, i] = Convert.ToDouble(oDRow["T_ALERT1"].ToString());
                            this.cFXRT.Data[2, i] = Convert.ToDouble(oDRow["L_ALERT1"].ToString());
                            break;
                        case "2":   //패턴
                            this.cFXRT.Data[1, i] = Convert.ToDouble(oDRow["T_ALERT2"].ToString());
                            this.cFXRT.Data[2, i] = Convert.ToDouble(oDRow["L_ALERT2"].ToString());
                            break;
                    }

                    i++;
                }
            }
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 감시지점의 감시 옵션을 취득해 반환한다.
        /// </summary>
        /// <param name="strMONITOR"></param>
        /// <returns></returns>
        private string GetAlertOptionByMonitorPoint(string strMONITOR)
        {
            string strRTN = string.Empty;

            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "2";
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   OPTIONS AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQ_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR + "'");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_MONITOR_OPTION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["RTNDATA"].ToString().Trim();
                    break;
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            return strRTN;
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_DETAIL = false;
            if (this.m_oDBManager != null) this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        #endregion

        #endregion

    }
}
