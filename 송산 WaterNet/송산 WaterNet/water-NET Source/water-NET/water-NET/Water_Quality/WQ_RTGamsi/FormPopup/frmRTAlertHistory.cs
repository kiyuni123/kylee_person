﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_RTGamsi.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A02
    /// Project Explain : 실시간감시
    /// Project Developer : 오두석
    /// Project Create Date : 2010.09.28
    /// Form Explain : 수질경보이력 Popup Form
    /// </summary>
    public partial class frmRTAlertHistory : Form
    {
        private OracleDBManager m_oDBManager = null;

        private IMapControl3 _Map;

        public IMapControl3 IMAP
        {
            get
            {
                return _Map;
            }
            set
            {
                _Map = value;
            }
        }

        public frmRTAlertHistory()
        {
            WQ_AppStatic.IS_SHOW_FORM_ALERT_HIS = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                WQ_AppStatic.IS_SHOW_FORM_ALERT_HIS = false;
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region Grid Set

            #region uGridPopup : 수질경보이력 Grid

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SBLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MONITOR_NO";
            oUltraGridColumn.Header.Caption = "감시지점";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_DATE";
            oUltraGridColumn.Header.Caption = "발생일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_TYPE";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_LEVEL";
            oUltraGridColumn.Header.Caption = "계측값";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridHistory);

            #endregion

            //ActiionKeyMapping();

            #endregion          
        }

        private void frmRTAlertHistory_Load(object sender, EventArgs e)
        {
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);
        }

        private void frmRTAlertHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_ALERT_HIS = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetAlertHistoryData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);
        }

        private void uGridHistory_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridHistory.Rows.Count <= 0) return;

            if (_Map != null)
            {
                string strNO = this.uGridHistory.ActiveRow.Cells[1].Text;
                strNO = WQ_Function.SplitToCode(strNO);

                ILayer pLayer = ArcManager.GetMapLayer(_Map, "감시지점");

                IFeature pFeature = ArcManager.GetFeature(pLayer, "NOS = '" + strNO + "'");

                if (pFeature == null) return;

                ArcManager.FlashShape(_Map.ActiveView, (IGeometry)pFeature.Shape, 10);

                if (ArcManager.GetMapScale(_Map) > (double)5000)
                {
                    ArcManager.SetMapScale(_Map, (double)5000);
                }

                ArcManager.MoveCenterAt(_Map, pFeature);

                ArcManager.PartialRefresh(_Map.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();

                this.BringToFront();
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetAlertHistoryData()
        {
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            DataSet pDS = new DataSet();

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "AND B.SBLOCK_CODE IN (" + strBLOCK_CD_S + ", '" + strBLOCK_CD_M + "')";
                }
                else
                {
                    strBLOCK_SQL = "AND B.SBLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            strSDate = WQ_Function.StringToDateTime(this.uDTAlertS.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.uDTAlertE.DateTime);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   (SELECT LOC_NAME || ' (' || LOC_CODE || ')' FROM CM_LOCATION WHERE LOC_CODE = B.SBLOCK_CODE) AS SBLOCK,");
            oStringBuilder.AppendLine("         B.MONPNT || ' (' || B.MONITOR_NO || ')' AS MONITOR_NO,");
            oStringBuilder.AppendLine("         TO_CHAR(TO_DATE(SUBSTR(A.DATES, 0, 8) || ' ' || SUBSTR(A.DATES, 9, 6), 'RRRR-MM-DD HH24:MI:SS'), 'RRRR-MM-DD HH24:MI:SS') AS ALERT_DATE,");
            oStringBuilder.AppendLine("         C.CODE_NAME || ' (' || C.CODE || ')' || '이상' AS ALERT_TYPE,");
            oStringBuilder.AppendLine("         A.ALARM_LEVEL AS ALERT_LEVEL");
            oStringBuilder.AppendLine("FROM     WQ_ALARM_HIST A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN WQ_RT_MONITOR_POINT B ON B.MONITOR_NO = A.MONITOR_NO " + strBLOCK_SQL);
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE C ON C.PCODE = '7004' AND C.CODE = A.WQ_ITEM_CODE");
            oStringBuilder.AppendLine("WHERE    A.MONITOR_NO = B.MONITOR_NO AND SUBSTR(A.DATES, 0, 8) BETWEEN '" + strSDate + "' AND '" + strEDate + "'");
            oStringBuilder.AppendLine("ORDER BY ALERT_DATE DESC, A.MONITOR_NO");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_ALARM_HIST");

            this.uGridHistory.DataSource = pDS.Tables["WQ_ALARM_HIST"].DefaultView;

            if (this.uGridHistory.Rows.Count > 0) this.uGridHistory.Rows[0].Activated = true;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGridHistory);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_ALERT_HIS = false;
            this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        #endregion

    }
}
