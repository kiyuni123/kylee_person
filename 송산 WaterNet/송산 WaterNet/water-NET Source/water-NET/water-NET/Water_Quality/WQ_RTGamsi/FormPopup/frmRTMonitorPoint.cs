﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using EMFrame.log;

namespace WaterNet.WQ_RTGamsi.FormPopup
{
    public partial class frmRTMonitorPoint : Form
    {
        #region 프로퍼티

        private IMapControl3 _IMAP;

        public IMapControl3 IMAP
        {
            get
            {
                return _IMAP;
            }
            set
            {
                _IMAP = value;
            }
        }

        #endregion

        private OracleDBManager m_oDBManager = null;

        public frmRTMonitorPoint()
        {
            WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = true;

            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            m_oDBManager.Open();
            if (m_oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.FormClose();
                return;
            }

            InitializeComponent();

            this.InitializeGridSetting();

            this.SetCombo_AllMediumAndSmallBlock(this.cboBLOCK, EMFrame.statics.AppStatic.USER_SGCCD);
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);

            this.InitializeControl();

            WQ_Function.SetCombo_TAGID2(this.cboTAGCL, "CL");
            WQ_Function.SetCombo_TAGID2(this.cboTAGTB, "TB");
            WQ_Function.SetCombo_TAGID2(this.cboTAGPH, "PH");
            WQ_Function.SetCombo_TAGID2(this.cboTAGTE, "TE");
            WQ_Function.SetCombo_TAGID2(this.cboTAGCU, "CU");
        }


        #region Form Events

        private void frmRTMonitorPoint_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================실시간감시관리(실시간 수질 감시 지점 등록)

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간감시ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnUpdate.Enabled = false;
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            //===========================================================================

            this.GetRTMonitorPoint();
        }

        private void frmRTMonitorPoint_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.FormClose();
        }

        #endregion


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //this.InitializeControl();

                this.GetRTMonitorPoint();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try ///cdkim 2017.05.30 try catch문 추가
            {
                this.FormClose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try ///cdkim 2017.05.30 try catch문 추가
            {
                this.InitializeControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try ///cdkim 2017.05.30 try catch문 추가
            {
                if (this.txtPOINT_NO.Text.Trim() == "")
                {
                    this.txtPOINT_NO.Text = WQ_Function.GenerationWQSerialNumber("RP");
                }
                else
                {
                    MessageBox.Show("신규 실시간 수질 감시 지점 등록은 초기화 버튼을 누른 후 시작해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (this.IsValidation() == false)
                {
                    this.txtPOINT_NO.Text = "";
                    return;
                }

                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                WaterNet.WQ_RTGamsi.FormPopup.frmRTSaveProgress oForm = new WaterNet.WQ_RTGamsi.FormPopup.frmRTSaveProgress();
                oForm.TopLevel = true;
                oForm.StartPosition = FormStartPosition.CenterScreen;
                oForm.BringToFront();
                oForm.Show();

                bool msgFlag = true;    ///cdkim 2017.05.30 등록 여부 메시지 출력을 위한 Flag

                if (_IMAP != null && WQ_AppStatic.CURRENT_IGEOMETRY != null && CheckMonitorPointData()) ///cdkim 2017.05.30 ComboBox 감시목록 설정 체크 부분 추가
                {
                    this.DeleteAllDataByMonitorPoint(this.txtPOINT_NO.Text);

                    this.Deletemap_MonitorPoint(this.txtPOINT_NO.Text);

                    this.SaveMonitorPointData();

                    this.SaveMonitorOptionData();

                    this.SaveMonitorMappingData();

                    //if (this.rdoOPTIONS2.Checked == true)
                    //{
                    this.SaveMonitorPatternData();
                    //}

                    this.Storemap_MonitorPoint(WQ_Function.SplitToCode(this.cboBLOCK.Text), this.txtPOINT_NO.Text.Trim(), this.txtPOINT_NO.Text.Trim(), "0");
                }
                else
                    msgFlag = false;    ///cdkim 2017.05.30

                //this.InitializeControl();

                oForm.Dispose();
                oForm.Close();

                this.Cursor = System.Windows.Forms.Cursors.Default;

                this.GetRTMonitorPoint();

                this.SelectRowRegisteredPoint();

                if (msgFlag) ///cdkim 2017.05.30
                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    if(WQ_AppStatic.CURRENT_IGEOMETRY == null)
                        MessageBox.Show("지도에서 감시지점 선택 후 등록이 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);    ///cdkim 2017.05.30 등록 실패 메시지 추가
                    else if(!CheckMonitorPointData())
                        MessageBox.Show("감시 태그 선택 후 등록이 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);    ///cdkim 2017.05.30 등록 실패 메시지 추가
                    else
                        MessageBox.Show("등록이 실패 하였습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);    ///cdkim 2017.05.30 등록 실패 메시지 추가
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try ///cdkim 2017.05.30 try catch문 추가
            {
                if (this.uGridPoint.Rows.Count <= 0) return;

                if (this.IsValidation() == false) return;
                if (CheckMonitorPointData())    ///cdkim 2017.05.30 ComboBox 감시목록 설정 체크 부분 추가
                {
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                    WaterNet.WQ_RTGamsi.FormPopup.frmRTSaveProgress oForm = new WaterNet.WQ_RTGamsi.FormPopup.frmRTSaveProgress();
                    oForm.TopLevel = true;
                    oForm.StartPosition = FormStartPosition.CenterScreen;
                    oForm.BringToFront();
                    oForm.Show();


                    this.DeleteAllDataByMonitorPoint(this.txtPOINT_NO.Text);

                    this.Deletemap_MonitorPoint(this.txtPOINT_NO.Text);

                    this.SaveMonitorPointData();

                    this.SaveMonitorOptionData();

                    this.SaveMonitorMappingData();

                    //if (this.rdoOPTIONS2.Checked == true)
                    //{
                    this.SaveMonitorPatternData();
                    //}

                    //this.InitializeControl();

                    oForm.Dispose();
                    oForm.Close();

                    this.Cursor = System.Windows.Forms.Cursors.Default;

                    this.GetRTMonitorPoint();

                    this.SelectRowRegisteredPoint();

                    MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (!CheckMonitorPointData())
                        MessageBox.Show("감시 태그 선택 후 수정이 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);    ///cdkim 2017.05.30 등록 실패 메시지 추가
                    else
                        MessageBox.Show("수정이 실패 하였습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try ///cdkim 2017.05.30 try catch문 추가
            {
                if (this.uGridPoint.Rows.Count <= 0) return;

                if (this.txtPOINT_NO.Text.Trim() == "")
                {
                    MessageBox.Show("삭제할 실시간 수질 감시 지점을 선택하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                this.DeleteAllDataByMonitorPoint(this.txtPOINT_NO.Text);

                this.Deletemap_MonitorPoint(this.txtPOINT_NO.Text);

                this.InitializeControl();

                this.Cursor = System.Windows.Forms.Cursors.Default;

                this.GetRTMonitorPoint();

                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        #endregion


        #region Combo Events

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.InitializeControl();
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);

            //strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);

            //WQ_Function.SetCombo_TAGID(this.cboTAGCL, "CL", "", strMFTRIDN, "");
            //WQ_Function.SetCombo_TAGID(this.cboTAGTB, "TB", "", strMFTRIDN, "");
            //WQ_Function.SetCombo_TAGID(this.cboTAGPH, "PH", "", strMFTRIDN, "");
            //WQ_Function.SetCombo_TAGID(this.cboTAGTE, "TE", "", strMFTRIDN, "");
            //WQ_Function.SetCombo_TAGID(this.cboTAGCU, "CU", "", strMFTRIDN, "");
        }

        private void cboSBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cboSBlock.Text == "전체")
            {
                this.cboBLOCK.SelectedIndex = 0;
            }
            else
            {
                WQ_Function.SetCombo_SelectFindData(this.cboBLOCK, this.cboSBlock.Text);
            }
        }

        private void cboBLOCK_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string strSFTRIDN = string.Empty;
            //string strMFTRIDN = string.Empty;
            //string strReservoir = string.Empty;

            //strSFTRIDN = WQ_Function.SplitToCode(this.cboBLOCK.Text);
            //strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            //strReservoir = WQ_Function.GetReservoirFromMediumBlockCode(strMFTRIDN);

            //WQ_Function.SetCombo_TAGID2(this.cboTAGCL, "CL");
            //WQ_Function.SetCombo_TAGID2(this.cboTAGTB, "TB");
            //WQ_Function.SetCombo_TAGID2(this.cboTAGPH, "PH");
            //WQ_Function.SetCombo_TAGID2(this.cboTAGTE, "TE");
            //WQ_Function.SetCombo_TAGID2(this.cboTAGCU, "CU");

            //WQ_Function.SetCombo_TAGID(this.cboTAGCL, "CL", strSFTRIDN, strMFTRIDN, strReservoir);
            //WQ_Function.SetCombo_TAGID(this.cboTAGTB, "TB", strSFTRIDN, strMFTRIDN, strReservoir);
            //WQ_Function.SetCombo_TAGID(this.cboTAGPH, "PH", strSFTRIDN, strMFTRIDN, strReservoir);
            //WQ_Function.SetCombo_TAGID(this.cboTAGTE, "TE", strSFTRIDN, strMFTRIDN, strReservoir);
            //WQ_Function.SetCombo_TAGID(this.cboTAGCU, "CU", strSFTRIDN, strMFTRIDN, strReservoir);
        }

        #endregion


        #region UltraGrid Events
        
        private void uGridPoint_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (this.uGridPoint.Rows.Count <= 0) return;

            this.InitializeControl();
            this.SetMonitorPointDataOnGrid(this.uGridPoint.ActiveRow.Index);
        }

        private void uGridPoint_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridPoint.Rows.Count <= 0) return;

            this.Viewmap_MonitorPoint("감시지점", this.txtPOINT_NO.Text, "NOS");
        }

        #endregion


        #region Initialize Function

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitializeGridSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region uGridPoint : 감시지점 Grid

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 200;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MONPNT";
            oUltraGridColumn.Header.Caption = "감시지점명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 200;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MONITOR_NO";
            oUltraGridColumn.Header.Caption = "감시지점일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 160;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SELECT_DATE";
            oUltraGridColumn.Header.Caption = "선정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTIONS";
            oUltraGridColumn.Header.Caption = "경보옵션";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_TE";
            oUltraGridColumn.Header.Caption = "TAG_ID_TE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_PH";
            oUltraGridColumn.Header.Caption = "TAG_ID_PH";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_TB";
            oUltraGridColumn.Header.Caption = "TAG_ID_TB";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_CU";
            oUltraGridColumn.Header.Caption = "TAG_ID_CU";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_ID_CL";
            oUltraGridColumn.Header.Caption = "TAG_ID_CL";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPoint);

            #endregion

        }

        /// <summary>
        /// 실시간 수질 감시 지점 및 감시 정보를 Clear한다.
        /// </summary>
        private void InitializeControl()
        {            
            this.cboBLOCK.SelectedIndex = 0;
            this.txtPOINT_NM.Text = "";
            this.txtPOINT_NO.Text = "";
            this.rdoOPTIONS1.Checked = true;
            this.rdoOPTIONS2.Checked = false;

            //this.cboTAGCL.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cboTAGCL.SelectedValue = "";  ///cdkim 2017.05.30 완전 초기화를 위한 처리
            //this.cboTAGCL.SelectedValue = this.uGridPoint.ActiveRow != null ? this.uGridPoint.ActiveRow.Cells["TAG_ID_CL"].Value : null;  ///cdkim 2017.05.30 
            this.txtATL_CL.Text = "1.0";   //기준 상한치 설정값
            this.txtALL_CL.Text = "0.2";   //기준 하한치 설정값
            this.txtPTL_CL.Text = "5";  //패턴 상한치 설정값(기본 +5%)
            this.txtPLL_CL.Text = "5";  //패턴 하한치 설정값(기본 -5%)

            //this.cboTAGTB.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cboTAGTB.SelectedValue = "";   ///cdkim 2017.05.30 완전 초기화를 위한 처리
            //this.cboTAGTB.SelectedValue = this.uGridPoint.ActiveRow != null ? this.uGridPoint.ActiveRow.Cells["TAG_ID_TB"].Value : null;  ///cdkim 2017.05.30
            this.txtATL_TB.Text = "1.00";
            this.txtALL_TB.Text = "0.00";
            this.txtPTL_TB.Text = "5";
            this.txtPLL_TB.Text = "5";

            //this.cboTAGPH.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cboTAGPH.SelectedValue = "";   ///cdkim 2017.05.30 완전 초기화를 위한 처리
            //this.cboTAGPH.SelectedValue = this.uGridPoint.ActiveRow != null ? this.uGridPoint.ActiveRow.Cells["TAG_ID_PH"].Value : null;  ///cdkim 2017.05.30
            this.txtATL_PH.Text = "9.0";
            this.txtALL_PH.Text = "5.0";
            this.txtPTL_PH.Text = "5";
            this.txtPLL_PH.Text = "5";

            //this.cboTAGTE.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cboTAGTE.SelectedValue = "";   ///cdkim 2017.05.30 완전 초기화를 위한 처리
            //this.cboTAGTE.SelectedValue = this.uGridPoint.ActiveRow != null ? this.uGridPoint.ActiveRow.Cells["TAG_ID_TE"].Value : null;  ///cdkim 2017.05.30
            this.txtATL_TE.Text = "0.0";
            this.txtALL_TE.Text = "0.0";
            this.txtPTL_TE.Text = "5";
            this.txtPLL_TE.Text = "5";

            //this.cboTAGCU.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cboTAGCU.SelectedValue = "";   ///cdkim 2017.05.30 완전 초기화를 위한 처리
            //this.cboTAGCU.SelectedValue = this.uGridPoint.ActiveRow != null ? this.uGridPoint.ActiveRow.Cells["TAG_ID_CU"].Value : null;  ///cdkim 2017.05.30
            this.txtATL_CU.Text = "0";
            this.txtALL_CU.Text = "0";
            this.txtPTL_CU.Text = "5";
            this.txtPLL_CU.Text = "5";
        }

        #endregion


        #region SQL Function

        /// <summary>
        /// 실시간 수질 감시 지점의 기본 정보를 SELECT한다.
        /// </summary>
        private void GetRTMonitorPoint()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = " AND A.SBLOCK_CODE IN (" + strBLOCK_CD_S + ", '" + strBLOCK_CD_M + "')";
                }
                else
                {
                    strBLOCK_SQL = " AND A.SBLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   C.LOC_NAME || ' (' || C.LOC_CODE || ')' AS BLOCK,");
            oStringBuilder.AppendLine("         A.MONPNT,A.TAG_ID_TE, A.TAG_ID_PH, A.TAG_ID_TB, A.TAG_ID_CU, A.TAG_ID_CL,");
            oStringBuilder.AppendLine("         A.MONITOR_NO,");
            oStringBuilder.AppendLine("         TO_DATE(A.SELECT_DATE, 'RRRR-MM-DD') AS SELECT_DATE,");
            oStringBuilder.AppendLine("         B.OPTIONS");
            oStringBuilder.AppendLine("FROM     WQ_RT_MONITOR_POINT A,");
            oStringBuilder.AppendLine("         WQ_MONITOR_OPTION B,");
            oStringBuilder.AppendLine("         CM_LOCATION C");
            oStringBuilder.AppendLine("WHERE    A.MONPNT_GBN = '0'");
            oStringBuilder.AppendLine("         " + strBLOCK_SQL);
            oStringBuilder.AppendLine("         AND B.MONITOR_NO = A.MONITOR_NO");
            oStringBuilder.AppendLine("         AND C.LOC_CODE = A.SBLOCK_CODE");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RTMONPOINT");

            this.uGridPoint.DataSource = pDS.Tables["RTMONPOINT"].DefaultView;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 실시간 수질 감시 지점 및 감시 정보를 Set한다.
        /// </summary>
        /// <param name="iRow">그리드의 Row 인덱스</param>
        private void SetMonitorPointDataOnGrid(int iRow)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            //하단 블록 Set
            WQ_Function.SetCombo_SelectFindData(this.cboBLOCK, this.uGridPoint.Rows[iRow].Cells[0].Text);

            this.txtPOINT_NM.Text = this.uGridPoint.Rows[iRow].Cells[1].Value.ToString();
            this.txtPOINT_NO.Text = this.uGridPoint.Rows[iRow].Cells[7].Value.ToString();   //cdkim 2017.05.30
            switch (this.uGridPoint.Rows[iRow].Cells[4].Text)
            {
                case "1": //기준 상/하한치로 경보발생
                    this.rdoOPTIONS1.Checked = true;
                    this.rdoOPTIONS2.Checked = false;
                    break;

                case "2": //패턴 상/하한치로 경보발생
                    this.rdoOPTIONS1.Checked = false;
                    this.rdoOPTIONS2.Checked = true;
                    break;
            }

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   PNT.TAG_ID_CL,");
            oStringBuilder.AppendLine("         PNT.TAG_ID_TB,"); 
            oStringBuilder.AppendLine("         PNT.TAG_ID_PH,");
            oStringBuilder.AppendLine("         PNT.TAG_ID_TE,");
            oStringBuilder.AppendLine("         PNT.TAG_ID_CU,");
            oStringBuilder.AppendLine("         MAP.WQ_ITEM_CODE,");
            oStringBuilder.AppendLine("         MAP.N1_TOP_LIMIT,");
            oStringBuilder.AppendLine("         MAP.N1_LOW_LIMIT,");
            oStringBuilder.AppendLine("         MAP.N2_TOP_LIMIT,");
            oStringBuilder.AppendLine("         MAP.N2_LOW_LIMIT");
            oStringBuilder.AppendLine("FROM     WQ_RT_MONITOR_POINT PNT, ");
            oStringBuilder.AppendLine("         WQ_MONPNT_MAPPING MAP");
            oStringBuilder.AppendLine("WHERE    PNT.MONITOR_NO = '" + this.txtPOINT_NO.Text + "'");
            oStringBuilder.AppendLine("         AND MAP.MONITOR_NO = PNT.MONITOR_NO");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RTPOINT_DATA");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    switch (oDRow["WQ_ITEM_CODE"].ToString().Trim())
                    {
                        case "CL":
                            //this.cboTAGCL.Items.Clear();
                            //this.cboTAGCL.Items.Add(oDRow["TAG_ID_CL"].ToString().Trim()); this.cboTAGCL.SelectedIndex = 0;
                            this.cboTAGCL.SelectedValue = oDRow["TAG_ID_CL"] != DBNull.Value ? oDRow["TAG_ID_CL"].ToString() : "";  //cdkim 2017.05.30 Count 보유 시 null ArgumentException 발생 부분 "" 으로 변환
                            //this.cboTAGCL.SelectedValue = oDRow["TAG_ID_CL"] != DBNull.Value ? oDRow["TAG_ID_CL"].ToString() : null;  //cdkim 2017.05.30
                            this.txtATL_CL.Text = oDRow["N1_TOP_LIMIT"].ToString().Trim();  //기준 상한치 설정값
                            this.txtALL_CL.Text = oDRow["N1_LOW_LIMIT"].ToString().Trim();  //기준 하한치 설정값
                            this.txtPTL_CL.Text = oDRow["N2_TOP_LIMIT"].ToString().Trim();  //패턴 상한치 설정값
                            this.txtPLL_CL.Text = oDRow["N2_LOW_LIMIT"].ToString().Trim();  //패턴 하한치 설정값                            
                            break;

                        case "TB":
                            //this.cboTAGTB.Items.Clear();
                            //this.cboTAGTB.Items.Add(oDRow["TAG_ID_TB"].ToString().Trim()); this.cboTAGTB.SelectedIndex = 0;
                            this.cboTAGTB.SelectedValue = oDRow["TAG_ID_TB"] != DBNull.Value ? oDRow["TAG_ID_TB"].ToString() : "";  //cdkim 2017.05.30 Count 보유 시 null ArgumentException 발생 부분 "" 으로 변환
                            //this.cboTAGTB.SelectedValue = oDRow["TAG_ID_TB"] != DBNull.Value ? oDRow["TAG_ID_TB"].ToString() : null;  //cdkim 2017.05.30
                            this.txtATL_TB.Text = oDRow["N1_TOP_LIMIT"].ToString().Trim();
                            this.txtALL_TB.Text = oDRow["N1_LOW_LIMIT"].ToString().Trim();
                            this.txtPTL_TB.Text = oDRow["N2_TOP_LIMIT"].ToString().Trim();
                            this.txtPLL_TB.Text = oDRow["N2_LOW_LIMIT"].ToString().Trim();
                            break;

                        case "PH":
                            //this.cboTAGPH.Items.Clear();
                            //this.cboTAGPH.Items.Add(oDRow["TAG_ID_PH"].ToString().Trim()); this.cboTAGPH.SelectedIndex = 0;
                            this.cboTAGPH.SelectedValue = oDRow["TAG_ID_PH"] != DBNull.Value ? oDRow["TAG_ID_PH"].ToString() : "";  //cdkim 2017.05.30 Count 보유 시 null ArgumentException 발생 부분 "" 으로 변환
                            //this.cboTAGPH.SelectedValue = oDRow["TAG_ID_PH"] != DBNull.Value ? oDRow["TAG_ID_PH"].ToString() : null;  //cdkim 2017.05.30
                            this.txtATL_PH.Text = oDRow["N1_TOP_LIMIT"].ToString().Trim();
                            this.txtALL_PH.Text = oDRow["N1_LOW_LIMIT"].ToString().Trim();
                            this.txtPTL_PH.Text = oDRow["N2_TOP_LIMIT"].ToString().Trim();
                            this.txtPLL_PH.Text = oDRow["N2_LOW_LIMIT"].ToString().Trim();
                            break;

                        case "TE":
                            //this.cboTAGTE.Items.Clear();
                            //this.cboTAGTE.Items.Add(oDRow["TAG_ID_TE"].ToString().Trim()); this.cboTAGTE.SelectedIndex = 0;
                            this.cboTAGTE.SelectedValue = oDRow["TAG_ID_TE"] != DBNull.Value ? oDRow["TAG_ID_TE"].ToString() : "";  //cdkim 2017.05.30 Count 보유 시 null ArgumentException 발생 부분 "" 으로 변환
                            //this.cboTAGTE.SelectedValue = oDRow["TAG_ID_TE"] != DBNull.Value ? oDRow["TAG_ID_TE"].ToString() : null;  //cdkim 2017.05.30
                            this.txtATL_TE.Text = oDRow["N1_TOP_LIMIT"].ToString().Trim();
                            this.txtALL_TE.Text = oDRow["N1_LOW_LIMIT"].ToString().Trim();
                            this.txtPTL_TE.Text = oDRow["N2_TOP_LIMIT"].ToString().Trim();
                            this.txtPLL_TE.Text = oDRow["N2_LOW_LIMIT"].ToString().Trim();
                            break;

                        case "CU":
                            //this.cboTAGCU.Items.Clear();
                            //this.cboTAGCU.Items.Add(oDRow["TAG_ID_CU"].ToString().Trim()); this.cboTAGCU.SelectedIndex = 0;
                            this.cboTAGCU.SelectedValue = oDRow["TAG_ID_CU"] != DBNull.Value ? oDRow["TAG_ID_CU"].ToString() : "";  //cdkim 2017.05.30 Count 보유 시 null ArgumentException 발생 부분 "" 으로 변환
                            //this.cboTAGCU.SelectedValue = oDRow["TAG_ID_CU"] != DBNull.Value ? oDRow["TAG_ID_CU"].ToString() : null;  //cdkim 2017.05.30
                            this.txtATL_CU.Text = oDRow["N1_TOP_LIMIT"].ToString().Trim();
                            this.txtALL_CU.Text = oDRow["N1_LOW_LIMIT"].ToString().Trim();
                            this.txtPTL_CU.Text = oDRow["N2_TOP_LIMIT"].ToString().Trim();
                            this.txtPLL_CU.Text = oDRow["N2_LOW_LIMIT"].ToString().Trim();
                            break;
                    }
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 감시지점 체크
        /// </summary>
        /// <returns></returns>
        private bool CheckMonitorPointData()
        {
            if(this.cboTAGCL.SelectedValue != null && this.cboTAGCL.SelectedValue.ToString() != "")
                return true;
            if (this.cboTAGTB.SelectedValue != null && this.cboTAGTB.SelectedValue.ToString() != "")
                return true;
            if (this.cboTAGPH.SelectedValue != null && this.cboTAGPH.SelectedValue.ToString() != "")
                return true;
            if (this.cboTAGTE.SelectedValue != null && this.cboTAGTE.SelectedValue.ToString() != "")
                return true;
            if (this.cboTAGCU.SelectedValue != null && this.cboTAGCU.SelectedValue.ToString() != "")
                return true;
            return false;
        }
        /// <summary>
        /// 감시지점의 지점정보를 저장 한다.
        /// </summary>
        private void SaveMonitorPointData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strBLOCK = string.Empty;
            string strMONITOR_NO = string.Empty;            
            string strMONPNT = string.Empty;
            string strMONPNT_GBN = string.Empty;

            DataSet pDS = new DataSet();

            //if (this.cboBLOCK.Text == "")
            //{
            //    strBLOCK = WQ_Function.SplitToCode(this.cboMBlock.Text);
            //}
            //else
            //{
                strBLOCK = WQ_Function.SplitToCode(this.cboBLOCK.Text);
            //}
            strMONITOR_NO = this.txtPOINT_NO.Text;
            strMONPNT = this.txtPOINT_NM.Text.Trim();
            strMONPNT_GBN = "0";

            //Layer에서 선정된 지점의 Layer 좌표
            IPoint pPoint = new PointClass();
            pPoint = (IPoint)WQ_AppStatic.CURRENT_IGEOMETRY;

            #region - INSERT SQL

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("(");
            oStringBuilder.AppendLine("     MONITOR_NO,");
            oStringBuilder.AppendLine("     MONPNT_GBN,");
            oStringBuilder.AppendLine("     SBLOCK_CODE,");
            oStringBuilder.AppendLine("     SELECT_DATE,");
            oStringBuilder.AppendLine("     MONPNT,");
            oStringBuilder.AppendLine("     TAG_ID_CL,");
            oStringBuilder.AppendLine("     TAG_ID_TB,");
            oStringBuilder.AppendLine("     TAG_ID_PH,");
            oStringBuilder.AppendLine("     TAG_ID_TE,");
            oStringBuilder.AppendLine("     TAG_ID_CU,");
            oStringBuilder.AppendLine("     NODE_ID,");
            oStringBuilder.AppendLine("     MAP_X,");
            oStringBuilder.AppendLine("     MAP_Y");
            oStringBuilder.AppendLine(")");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("(");
            oStringBuilder.AppendLine("     '" + strMONITOR_NO + "',");
            oStringBuilder.AppendLine("     '" + strMONPNT_GBN + "',");
            oStringBuilder.AppendLine("     '" + strBLOCK + "',");
            oStringBuilder.AppendLine("     TO_CHAR(SYSDATE, 'RRRRMMDD'),");
            oStringBuilder.AppendLine("     '" + strMONPNT + "',");
            oStringBuilder.AppendLine("     '" + this.cboTAGCL.SelectedValue + "',");
            oStringBuilder.AppendLine("     '" + this.cboTAGTB.SelectedValue + "',");
            oStringBuilder.AppendLine("     '" + this.cboTAGPH.SelectedValue + "',");
            oStringBuilder.AppendLine("     '" + this.cboTAGTE.SelectedValue + "',");
            oStringBuilder.AppendLine("     '" + this.cboTAGCU.SelectedValue + "',");
            oStringBuilder.AppendLine("     '',");
            oStringBuilder.AppendLine("     '" + pPoint.X.ToString() + "',");
            oStringBuilder.AppendLine("     '" + pPoint.Y.ToString() + "'");
            oStringBuilder.AppendLine(")");

            #endregion

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감시지점의 경보방법을 저장 한다.
        /// </summary>
        private void SaveMonitorOptionData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            string strMONITOR_NO = this.txtPOINT_NO.Text.Trim();
            string strOPTIONS = string.Empty;

            if (this.rdoOPTIONS1.Checked == true)
            {
                strOPTIONS = "1";   //기준 상/하한치로 경보발생
            }
            else
            {
                strOPTIONS = "2";   //패턴 상/하한치로 경보발생
            }   

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WQ_MONITOR_OPTION");
            oStringBuilder.AppendLine("(");
            oStringBuilder.AppendLine("     MONITOR_NO,");
            oStringBuilder.AppendLine("     OPTIONS");
            oStringBuilder.AppendLine(")");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("(");
            oStringBuilder.AppendLine("     '" + strMONITOR_NO + "',");
            oStringBuilder.AppendLine("     '" + strOPTIONS + "'");
            oStringBuilder.AppendLine(")");

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감시항목의 경보설정치를 저장 한다.
        /// </summary>
        private void SaveMonitorMappingData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strWQ_ITEM_CODE = string.Empty;
            string strMONITOR_NO = string.Empty;
            string strN1_TOP_LIMIT = string.Empty;
            string strN2_TOP_LIMIT = string.Empty;
            string strN1_LOW_LIMIT = string.Empty;
            string strN2_LOW_LIMIT = string.Empty;

            DataSet pDS = new DataSet();

            strMONITOR_NO = this.txtPOINT_NO.Text.Trim();

            for (int i = 0; i < 5; i++)
            {
                #region - 수질항목 SET

                switch (i)
                {
                    case 0:
                        strWQ_ITEM_CODE = "CL";
                        strN1_TOP_LIMIT = this.txtATL_CL.Text.Trim();
                        strN1_LOW_LIMIT = this.txtALL_CL.Text.Trim();
                        strN2_TOP_LIMIT = this.txtPTL_CL.Text.Trim();
                        strN2_LOW_LIMIT = this.txtPLL_CL.Text.Trim();
                        break;

                    case 1:
                        strWQ_ITEM_CODE = "TB";
                        strN1_TOP_LIMIT = this.txtATL_TB.Text.Trim();
                        strN1_LOW_LIMIT = this.txtALL_TB.Text.Trim();
                        strN2_TOP_LIMIT = this.txtPTL_TB.Text.Trim();
                        strN2_LOW_LIMIT = this.txtPLL_TB.Text.Trim();
                        break;

                    case 2:
                        strWQ_ITEM_CODE = "PH";
                        strN1_TOP_LIMIT = this.txtATL_PH.Text.Trim();
                        strN1_LOW_LIMIT = this.txtALL_PH.Text.Trim();
                        strN2_TOP_LIMIT = this.txtPTL_PH.Text.Trim();
                        strN2_LOW_LIMIT = this.txtPLL_PH.Text.Trim();
                        break;

                    case 3:
                        strWQ_ITEM_CODE = "TE";
                        strN1_TOP_LIMIT = this.txtATL_TE.Text.Trim();
                        strN1_LOW_LIMIT = this.txtALL_TE.Text.Trim();
                        strN2_TOP_LIMIT = this.txtPTL_TE.Text.Trim();
                        strN2_LOW_LIMIT = this.txtPLL_TE.Text.Trim();
                        break;

                    case 4:
                        strWQ_ITEM_CODE = "CU";
                        strN1_TOP_LIMIT = this.txtATL_CU.Text.Trim();
                        strN1_LOW_LIMIT = this.txtALL_CU.Text.Trim();
                        strN2_TOP_LIMIT = this.txtPTL_CU.Text.Trim();
                        strN2_LOW_LIMIT = this.txtPLL_CU.Text.Trim();
                        break;
                }

                #endregion

                #region - INSERT SQL

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT   INTO    WQ_MONPNT_MAPPING");
                oStringBuilder.AppendLine("(");
                oStringBuilder.AppendLine("     MONITOR_NO,");
                oStringBuilder.AppendLine("     WQ_ITEM_CODE,");
                oStringBuilder.AppendLine("     N1_TOP_LIMIT,");
                oStringBuilder.AppendLine("     N1_LOW_LIMIT,");
                oStringBuilder.AppendLine("     N2_TOP_LIMIT,");
                oStringBuilder.AppendLine("     N2_LOW_LIMIT,");
                oStringBuilder.AppendLine("     N3_TOP_LIMIT,");
                oStringBuilder.AppendLine("     N3_LOW_LIMIT,");
                oStringBuilder.AppendLine("     ALARM_YN");
                oStringBuilder.AppendLine(")");
                oStringBuilder.AppendLine("VALUES");
                oStringBuilder.AppendLine("(");
                oStringBuilder.AppendLine("     '" + strMONITOR_NO + "',");
                oStringBuilder.AppendLine("     '" + strWQ_ITEM_CODE + "',");
                oStringBuilder.AppendLine("     '" + strN1_TOP_LIMIT + "',");
                oStringBuilder.AppendLine("     '" + strN1_LOW_LIMIT + "',");
                oStringBuilder.AppendLine("     '" + strN2_TOP_LIMIT + "',");
                oStringBuilder.AppendLine("     '" + strN2_LOW_LIMIT + "',");
                oStringBuilder.AppendLine("     '',");
                oStringBuilder.AppendLine("     '',");
                oStringBuilder.AppendLine("     'Y'");
                oStringBuilder.AppendLine(")");

                #endregion

                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            }
        }

        /// <summary>
        /// 감시항목의 패턴 Data를 저장 한다.
        /// </summary>
        private void SaveMonitorPatternData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strMONITOR_NO = string.Empty;
            string strWQ_ITEM_CODE = string.Empty;
            string strCOLUMN = string.Empty;

            DataSet pDS = new DataSet();

            strMONITOR_NO = this.txtPOINT_NO.Text;

            for (int i = 0; i < 5; i++)
            {
                #region - 수질항목 SET

                switch (i)
                {
                    case 0:
                        strWQ_ITEM_CODE = "CL";
                        strCOLUMN = "TAG_ID_CL";
                        break;

                    case 1:
                        strWQ_ITEM_CODE = "TB";
                        strCOLUMN = "TAG_ID_TB";
                        break;

                    case 2:
                        strWQ_ITEM_CODE = "PH";
                        strCOLUMN = "TAG_ID_PH";
                        break;

                    case 3:
                        strWQ_ITEM_CODE = "TE";
                        strCOLUMN = "TAG_ID_TE";
                        break;

                    case 4:
                        strWQ_ITEM_CODE = "CU";
                        strCOLUMN = "TAG_ID_CU";
                        break;
                }

                #endregion

                #region - 패턴 SELECT SQL

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   TO_CHAR(RT.DT, 'HH24:MI') AS PTNTIME,");
                oStringBuilder.AppendLine("         '" + strWQ_ITEM_CODE + "' AS PTNITEM,");
                oStringBuilder.AppendLine("         ROUND(MAX(RT.PTN_VALUE), 3) AS MAXVAL,");
                oStringBuilder.AppendLine("         ROUND(MIN(RT.PTN_VALUE), 3) AS MINVAL,");
                oStringBuilder.AppendLine("         ROUND(AVG(RT.PTN_VALUE), 3) AS AVGVAL");
                oStringBuilder.AppendLine("FROM     (SELECT    A.TIMESTAMP AS DT, TO_NUMBER(A.VALUE) AS PTN_VALUE ");
                oStringBuilder.AppendLine("          FROM      IF_GATHER_REALTIME A, WQ_RT_MONITOR_POINT B");
                oStringBuilder.AppendLine("          WHERE     B.MONITOR_NO = '" + strMONITOR_NO + "' AND A.TAGNAME = B." + strCOLUMN);
                //패턴을 만들때 30일간 순시 분 데이터를 수집해서 만드는 것은 시간이 너무 오래 걸려 15일로 수정
                oStringBuilder.AppendLine("                    AND TO_CHAR(A.TIMESTAMP, 'RRRRMMDD') BETWEEN TO_CHAR(SYSDATE - 16, 'RRRRMMDD') AND TO_CHAR(SYSDATE - 1, 'RRRRMMDD')");
                //oStringBuilder.AppendLine("                    AND TO_CHAR(A.TIMESTAMP, 'RRRRMMDD') BETWEEN TO_CHAR(SYSDATE - 31, 'RRRRMMDD') AND TO_CHAR(SYSDATE - 1, 'RRRRMMDD')");
                oStringBuilder.AppendLine("                    AND TO_NUMBER(A.VALUE) > 0");
                oStringBuilder.AppendLine("         ) RT");
                oStringBuilder.AppendLine("GROUP BY TO_CHAR(RT.DT, 'HH24:MI')");
                oStringBuilder.AppendLine("ORDER BY PTNTIME");

                #endregion

                pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PATTERN_DATA");

                if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
                {
                    #region - BULK INSERT를 위한 필드 선언

                    List<string> MONITOR_NO = new List<string>();
                    List<string> WQ_ITEM_CODE = new List<string>();
                    List<string> TIME = new List<string>();
                    List<string> TOP_LIMIT = new List<string>();
                    List<string> LOWER_LIMIT = new List<string>();
                    List<string> AVG = new List<string>();
                    List<string> APPLY_YN = new List<string>();
                    List<string> WEEK_CODE = new List<string>();
                    List<string> PATTERN_ID = new List<string>();

                    #endregion

                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        #region - 패턴 일반 INSERT SQL

                        //oStringBuilder.Remove(0, oStringBuilder.Length);
                        //oStringBuilder.AppendLine("INSERT   INTO    WQ_PATTERN");
                        //oStringBuilder.AppendLine("(");
                        //oStringBuilder.AppendLine("     MONITOR_NO,");
                        //oStringBuilder.AppendLine("     WQ_ITEM_CODE,");
                        //oStringBuilder.AppendLine("     TIME,");
                        //oStringBuilder.AppendLine("     TOP_LIMIT,");
                        //oStringBuilder.AppendLine("     LOWER_LIMIT,");
                        //oStringBuilder.AppendLine("     AVG,");
                        //oStringBuilder.AppendLine("     APPLY_YN,");
                        //oStringBuilder.AppendLine("     WEEK_CODE,");
                        //oStringBuilder.AppendLine("     PATTERN_ID");
                        //oStringBuilder.AppendLine(")");
                        //oStringBuilder.AppendLine("VALUES");
                        //oStringBuilder.AppendLine("(");
                        //oStringBuilder.AppendLine("     '" + strMONITOR_NO + "',");
                        //oStringBuilder.AppendLine("     '" + strWQ_ITEM_CODE + "',");
                        //oStringBuilder.AppendLine("     '" + oDRow["PTNTIME"].ToString().Trim() + "',");
                        //oStringBuilder.AppendLine("     '" + oDRow["MAXVAL"].ToString().Trim() + "',");
                        //oStringBuilder.AppendLine("     '" + oDRow["MINVAL"].ToString().Trim() + "',");
                        //oStringBuilder.AppendLine("     '" + oDRow["AVGVAL"].ToString().Trim() + "',");
                        //oStringBuilder.AppendLine("     'Y',");
                        //oStringBuilder.AppendLine("     ' ',");
                        //oStringBuilder.AppendLine("     ' '");
                        //oStringBuilder.AppendLine(")");

                        //m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                        #endregion

                        #region - 패턴 BULK INSERT DATA SET
                        
                        MONITOR_NO.Add(strMONITOR_NO);
                        WQ_ITEM_CODE.Add(strWQ_ITEM_CODE);
                        TIME.Add(oDRow["PTNTIME"].ToString().Trim());
                        TOP_LIMIT.Add(oDRow["MAXVAL"].ToString().Trim());
                        LOWER_LIMIT.Add(oDRow["MINVAL"].ToString().Trim());
                        AVG.Add(oDRow["AVGVAL"].ToString().Trim());
                        APPLY_YN.Add("Y");
                        WEEK_CODE.Add(" ");
                        PATTERN_ID.Add(" ");
                        
                        #endregion
                    }

                    #region - 패턴 BULK INSERT DATA의 리스트를 해쉬테이블에 SET

                    Hashtable oHT = new Hashtable();

                    oHT.Add("MONITOR_NO", MONITOR_NO);
                    oHT.Add("WQ_ITEM_CODE", WQ_ITEM_CODE);
                    oHT.Add("TIME", TIME);
                    oHT.Add("TOP_LIMIT", TOP_LIMIT);
                    oHT.Add("LOWER_LIMIT", LOWER_LIMIT);
                    oHT.Add("AVG", AVG);
                    oHT.Add("APPLY_YN", APPLY_YN);
                    oHT.Add("WEEK_CODE", WEEK_CODE);
                    oHT.Add("PATTERN_ID", PATTERN_ID);

                    #endregion

                    #region - 패턴 BULK INSERT

                    this.BulkInsertPatternData(oHT);
                    
                    #endregion

                    //Console.WriteLine(strWQ_ITEM_CODE);

                    //System.Threading.Thread.Sleep(2000);
                }
            }
        }

        /// <summary>
        /// 패턴 데이터를 벌크 타입으로 Insert 한다.
        /// </summary>
        /// <param name="pHT"></param>
        private void BulkInsertPatternData(Hashtable pHT)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WQ_PATTERN");
            oStringBuilder.AppendLine("(");
            oStringBuilder.AppendLine("     MONITOR_NO,");
            oStringBuilder.AppendLine("     WQ_ITEM_CODE,");
            oStringBuilder.AppendLine("     TIME,");
            oStringBuilder.AppendLine("     TOP_LIMIT,");
            oStringBuilder.AppendLine("     LOWER_LIMIT,");
            oStringBuilder.AppendLine("     AVG,");
            oStringBuilder.AppendLine("     APPLY_YN,");
            oStringBuilder.AppendLine("     WEEK_CODE,");
            oStringBuilder.AppendLine("     PATTERN_ID");
            oStringBuilder.AppendLine(")");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("(");
            oStringBuilder.AppendLine("     :1,");
            oStringBuilder.AppendLine("     :2,");
            oStringBuilder.AppendLine("     :3,");
            oStringBuilder.AppendLine("     :4,");
            oStringBuilder.AppendLine("     :5,");
            oStringBuilder.AppendLine("     :6,");
            oStringBuilder.AppendLine("     :7,");
            oStringBuilder.AppendLine("     :8,");
            oStringBuilder.AppendLine("     :9");
            oStringBuilder.AppendLine(")");

            OracleCommand oOracleCMD = new OracleCommand();
            oOracleCMD.CommandText = oStringBuilder.ToString();
            oOracleCMD.Connection = m_oDBManager.Connection;
            oOracleCMD.ArrayBindCount = ((List<string>)pHT["MONITOR_NO"]).Count;

            OracleParameter oOP_MONITOR_NO = new OracleParameter("1", OracleDbType.Varchar2);
            oOP_MONITOR_NO.Direction = ParameterDirection.Input;
            oOP_MONITOR_NO.Value = ((List<string>)pHT["MONITOR_NO"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_MONITOR_NO);

            OracleParameter oOP_WQ_ITEM_CODE = new OracleParameter("2", OracleDbType.Varchar2);
            oOP_WQ_ITEM_CODE.Direction = ParameterDirection.Input;
            oOP_WQ_ITEM_CODE.Value = ((List<string>)pHT["WQ_ITEM_CODE"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_WQ_ITEM_CODE);

            OracleParameter oOP_TIME = new OracleParameter("3", OracleDbType.Varchar2);
            oOP_TIME.Direction = ParameterDirection.Input;
            oOP_TIME.Value = ((List<string>)pHT["TIME"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_TIME);

            OracleParameter oOP_TOP_LIMIT = new OracleParameter("4", OracleDbType.Varchar2);
            oOP_TOP_LIMIT.Direction = ParameterDirection.Input;
            oOP_TOP_LIMIT.Value = ((List<string>)pHT["TOP_LIMIT"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_TOP_LIMIT);

            OracleParameter oOP_LOWER_LIMIT = new OracleParameter("5", OracleDbType.Varchar2);
            oOP_LOWER_LIMIT.Direction = ParameterDirection.Input;
            oOP_LOWER_LIMIT.Value = ((List<string>)pHT["LOWER_LIMIT"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_LOWER_LIMIT);

            OracleParameter oOP_AVG = new OracleParameter("6", OracleDbType.Varchar2);
            oOP_AVG.Direction = ParameterDirection.Input;
            oOP_AVG.Value = ((List<string>)pHT["AVG"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_AVG);

            OracleParameter oOP_APPLY_YN = new OracleParameter("7", OracleDbType.Varchar2);
            oOP_APPLY_YN.Direction = ParameterDirection.Input;
            oOP_APPLY_YN.Value = ((List<string>)pHT["APPLY_YN"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_APPLY_YN);

            OracleParameter oOP_WEEK_CODE = new OracleParameter("8", OracleDbType.Varchar2);
            oOP_WEEK_CODE.Direction = ParameterDirection.Input;
            oOP_WEEK_CODE.Value = ((List<string>)pHT["WEEK_CODE"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_WEEK_CODE);

            OracleParameter oOP_PATTERN_ID = new OracleParameter("9", OracleDbType.Varchar2);
            oOP_PATTERN_ID.Direction = ParameterDirection.Input;
            oOP_PATTERN_ID.Value = ((List<string>)pHT["PATTERN_ID"]).ToArray();
            oOracleCMD.Parameters.Add(oOP_PATTERN_ID);

            oOracleCMD.ExecuteNonQuery();
        }

        /// <summary>
        /// 특정 수질 감시 지점의 모든 DATA를 삭제
        /// </summary>
        /// <param name="strMONITOR_NO"></param>
        private void DeleteAllDataByMonitorPoint(string strMONITOR_NO)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR_NO + "'");

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_MONPNT_MAPPING");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR_NO + "'");

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR_NO + "'");

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WQ_PATTERN");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR_NO + "'");

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        }

        #endregion


        #region Map Function

        /// <summary>
        /// 감시지점 그리드 클릭시 맵 이동 및 축척 변경
        /// </summary>
        /// <param name="strShape"></param>
        private void Viewmap_MonitorPoint(string strShape, string strKey, string strKeyName)
        {
            if (_IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(_IMAP, strShape);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strKeyName + "='" + strKey + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    IGeometry oIGeometry = pFeature.Shape;

                    WQ_AppStatic.CURRENT_IGEOMETRY = oIGeometry;

                    ArcManager.FlashShape(_IMAP.ActiveView, oIGeometry, 10);

                    if (ArcManager.GetMapScale(_IMAP) > (double)1000)
                    {
                        ArcManager.SetMapScale(_IMAP, (double)1000);
                    }

                    ArcManager.MoveCenterAt(_IMAP, pFeature);

                    ArcManager.PartialRefresh(_IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                    Application.DoEvents();
                }
                else
                {
                    WQ_AppStatic.CURRENT_IGEOMETRY = null;
                }
                this.BringToFront();
            }
            else
            {
                WQ_AppStatic.CURRENT_IGEOMETRY = null;
            }
        }

        /// <summary>
        /// Layer에 Monitor Point를 Save한다.
        /// </summary>
        private void Storemap_MonitorPoint(string strBLOCK, string strPOINT_NO, string strPOINT_NM, string strPOINT_GBN)
        {
            ILayer pLayer = ArcManager.GetMapLayer(_IMAP, "감시지점");

            IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
            IFeature pFeature = pFeatureClass.CreateFeature();

            //Shpae에 지점 등록
            pFeature.Shape = WQ_AppStatic.CURRENT_IGEOMETRY;

            //등록된 지점에 밸류 등록
            ArcManager.SetValue(pFeature, "SBLOCK", (object)strBLOCK);
            ArcManager.SetValue(pFeature, "GBN", (object)strPOINT_GBN);
            ArcManager.SetValue(pFeature, "NOS", (object)strPOINT_NO);
            ArcManager.SetValue(pFeature, "NAMES", (object)strPOINT_NM);

            //Shape 저장
            pFeature.Store();

            //GIS Map Refresh
            _IMAP.ActiveView.ContentsChanged();
            ArcManager.PartialRefresh(_IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();

            WQ_AppStatic.CURRENT_IGEOMETRY = null;
        }

        /// <summary>
        /// Layer에서 Monitor Point를 Delete한다.
        /// </summary>
        private void Deletemap_MonitorPoint(string strPOINT_NO)
        {
            if (_IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(_IMAP, "감시지점");
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "NOS='" + strPOINT_NO + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    WQ_AppStatic.CURRENT_IGEOMETRY = pFeature.Shape;
                    pFeature.Delete();
                }
            }
            _IMAP.ActiveView.ContentsChanged();
            ArcManager.PartialRefresh(_IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();
        }

        #endregion


        #region ETC Function

        /// <summary>
        /// 저장하는 Data를 검증한다.
        /// </summary>
        /// <returns>True / False</returns>
        private bool IsValidation()
        {
            bool blRtn = true;

            if (this.cboBLOCK.Text == "") blRtn = false;
            if (this.txtPOINT_NM.Text.Trim() == "") blRtn = false;
            if (this.txtPOINT_NO.Text.Trim() == "") blRtn = false;

            if (this.txtATL_CL.Text.Trim() == "") blRtn = false;
            if (this.txtALL_CL.Text.Trim() == "") blRtn = false;
            if (this.txtPTL_CL.Text.Trim() == "") blRtn = false;
            if (this.txtPLL_CL.Text.Trim() == "") blRtn = false;

            if (this.txtATL_TB.Text.Trim() == "") blRtn = false;
            if (this.txtALL_TB.Text.Trim() == "") blRtn = false;
            if (this.txtPTL_TB.Text.Trim() == "") blRtn = false;
            if (this.txtPLL_TB.Text.Trim() == "") blRtn = false;

            if (this.txtATL_PH.Text.Trim() == "") blRtn = false;
            if (this.txtALL_PH.Text.Trim() == "") blRtn = false;
            if (this.txtPTL_PH.Text.Trim() == "") blRtn = false;
            if (this.txtPLL_PH.Text.Trim() == "") blRtn = false;

            if (this.txtATL_TE.Text.Trim() == "") blRtn = false;
            if (this.txtALL_TE.Text.Trim() == "") blRtn = false;
            if (this.txtPTL_TE.Text.Trim() == "") blRtn = false;
            if (this.txtPLL_TE.Text.Trim() == "") blRtn = false;

            if (this.txtATL_CU.Text.Trim() == "") blRtn = false;
            if (this.txtALL_CU.Text.Trim() == "") blRtn = false;
            if (this.txtPTL_CU.Text.Trim() == "") blRtn = false;
            if (this.txtPLL_CU.Text.Trim() == "") blRtn = false;

            if (blRtn == false)
            {
                MessageBox.Show("실시간 수질 감시 지점 정보를 입력(또는 선택)해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return blRtn;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 소블록
        /// 대상 Table : CM_LOCATION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSRCCD">지자체 코드</param>
        private void SetCombo_AllMediumAndSmallBlock(ComboBox oCombo, string strSRCCD)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    FTR_CODE IN ('BZ003', 'BZ002') AND SGCCD = '" + strSRCCD + "'");
            oStringBuilder.AppendLine("ORDER BY FTR_CODE, RTNDATA");

            pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            oCombo.Items.Clear();

            oCombo.Items.Add("");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;

            pDS.Dispose();
        }

        /// <summary>
        /// 지점등록, 지점수정 후 새롭게 등록 또는 수정된 감시지점으로 Select 한다.
        /// </summary>
        private void SelectRowRegisteredPoint()
        {
            if (this.uGridPoint.Rows.Count <= 0) return;

            if (this.uGridPoint.ActiveRow != null) this.uGridPoint.ActiveRow.Selected = false;
            if (this.uGridPoint.ActiveRow != null) this.uGridPoint.ActiveRow.Activated = false;

            for (int i = 0; i < this.uGridPoint.Rows.Count; i++)
            {
                if (this.txtPOINT_NO.Text.Trim() == this.uGridPoint.Rows[i].Cells[2].Text.Trim())
                {
                    this.uGridPoint.Rows[i].Selected = true;
                    this.uGridPoint.Rows[i].Activated = true;
                    break;
                }
            }
        }

        /// <summary>
        /// Form을 Close하며 정리
        /// </summary>
        private void FormClose()
        {
            WQ_AppStatic.IS_SHOW_FORM_MONITOR_POINT = false;
            if (m_oDBManager != null) this.m_oDBManager.Close();
            this.Dispose();
            this.Close();
        }

        #endregion

    }
}
