﻿namespace WaterNet.WQ_RTGamsi.FormPopup
{
    partial class frmRTData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.ugRTData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uDTAlertE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.rdoCU = new System.Windows.Forms.RadioButton();
            this.uDTAlertS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.rdoTE = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.rdoPH = new System.Windows.Forms.RadioButton();
            this.rdoTB = new System.Windows.Forms.RadioButton();
            this.rdoCL = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.cFXRT = new ChartFX.WinForms.Chart();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ugRTData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTAlertE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDTAlertS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cFXRT)).BeginInit();
            this.SuspendLayout();
            // 
            // ugRTData
            // 
            this.ugRTData.Dock = System.Windows.Forms.DockStyle.Top;
            this.ugRTData.Location = new System.Drawing.Point(4, 88);
            this.ugRTData.Name = "ugRTData";
            this.ugRTData.Size = new System.Drawing.Size(729, 160);
            this.ugRTData.TabIndex = 0;
            this.ugRTData.Text = "실시간 수질 감시 데이터";
            this.ugRTData.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ugRTData_DoubleClickRow);
            this.ugRTData.Click += new System.EventHandler(this.ugRTData_Click);
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(737, 4);
            this.picFrTop.TabIndex = 112;
            this.picFrTop.TabStop = false;
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(71, 4);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 20);
            this.cboLBlock.TabIndex = 155;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(11, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 156;
            this.label7.Text = "대블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(277, 4);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 20);
            this.cboMBlock.TabIndex = 157;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(217, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 158;
            this.label8.Text = "중블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(483, 4);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 20);
            this.cboSBlock.TabIndex = 159;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(423, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 160;
            this.label9.Text = "소블록 :";
            // 
            // btnQuery
            // 
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(646, 5);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(38, 26);
            this.btnQuery.TabIndex = 161;
            this.btnQuery.Text = "조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnClose
            // 
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(686, 33);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 26);
            this.btnClose.TabIndex = 162;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.uDTAlertE);
            this.panel1.Controls.Add(this.rdoCU);
            this.panel1.Controls.Add(this.uDTAlertS);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.rdoTE);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.rdoPH);
            this.panel1.Controls.Add(this.rdoTB);
            this.panel1.Controls.Add(this.rdoCL);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.cboSBlock);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cboMBlock);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cboLBlock);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(729, 80);
            this.panel1.TabIndex = 8;
            // 
            // uDTAlertE
            // 
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.uDTAlertE.Appearance = appearance3;
            this.uDTAlertE.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Standard;
            this.uDTAlertE.Location = new System.Drawing.Point(243, 48);
            this.uDTAlertE.Margin = new System.Windows.Forms.Padding(0);
            this.uDTAlertE.MaskInput = "yyyy-mm-dd hh:mm";
            this.uDTAlertE.Name = "uDTAlertE";
            this.uDTAlertE.Size = new System.Drawing.Size(140, 21);
            this.uDTAlertE.TabIndex = 196;
            // 
            // rdoCU
            // 
            this.rdoCU.AutoSize = true;
            this.rdoCU.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoCU.Location = new System.Drawing.Point(408, 29);
            this.rdoCU.Name = "rdoCU";
            this.rdoCU.Size = new System.Drawing.Size(88, 16);
            this.rdoCU.TabIndex = 199;
            this.rdoCU.TabStop = true;
            this.rdoCU.Text = "전기전도도";
            this.rdoCU.UseVisualStyleBackColor = true;
            this.rdoCU.Click += new System.EventHandler(this.rdoCU_Click);
            // 
            // uDTAlertS
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.uDTAlertS.Appearance = appearance2;
            this.uDTAlertS.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Standard;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.uDTAlertS.DropDownAppearance = appearance1;
            this.uDTAlertS.Location = new System.Drawing.Point(82, 48);
            this.uDTAlertS.Margin = new System.Windows.Forms.Padding(0);
            this.uDTAlertS.MaskInput = "yyyy-mm-dd hh:mm";
            this.uDTAlertS.Name = "uDTAlertS";
            this.uDTAlertS.Size = new System.Drawing.Size(140, 21);
            this.uDTAlertS.TabIndex = 195;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(225, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 12);
            this.label2.TabIndex = 194;
            this.label2.Text = "~";
            // 
            // rdoTE
            // 
            this.rdoTE.AutoSize = true;
            this.rdoTE.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoTE.Location = new System.Drawing.Point(353, 29);
            this.rdoTE.Name = "rdoTE";
            this.rdoTE.Size = new System.Drawing.Size(49, 16);
            this.rdoTE.TabIndex = 198;
            this.rdoTE.TabStop = true;
            this.rdoTE.Text = "온도";
            this.rdoTE.UseVisualStyleBackColor = true;
            this.rdoTE.Click += new System.EventHandler(this.rdoTE_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(12, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 12);
            this.label3.TabIndex = 193;
            this.label3.Text = "발생일자 :";
            // 
            // rdoPH
            // 
            this.rdoPH.AutoSize = true;
            this.rdoPH.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoPH.Location = new System.Drawing.Point(246, 29);
            this.rdoPH.Name = "rdoPH";
            this.rdoPH.Size = new System.Drawing.Size(101, 16);
            this.rdoPH.TabIndex = 197;
            this.rdoPH.TabStop = true;
            this.rdoPH.Text = "수소이온농도";
            this.rdoPH.UseVisualStyleBackColor = true;
            this.rdoPH.Click += new System.EventHandler(this.rdoPH_Click);
            // 
            // rdoTB
            // 
            this.rdoTB.AutoSize = true;
            this.rdoTB.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoTB.Location = new System.Drawing.Point(191, 29);
            this.rdoTB.Name = "rdoTB";
            this.rdoTB.Size = new System.Drawing.Size(49, 16);
            this.rdoTB.TabIndex = 196;
            this.rdoTB.TabStop = true;
            this.rdoTB.Text = "탁도";
            this.rdoTB.UseVisualStyleBackColor = true;
            this.rdoTB.Click += new System.EventHandler(this.rdoTB_Click);
            // 
            // rdoCL
            // 
            this.rdoCL.AutoSize = true;
            this.rdoCL.Checked = true;
            this.rdoCL.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoCL.Location = new System.Drawing.Point(110, 29);
            this.rdoCL.Name = "rdoCL";
            this.rdoCL.Size = new System.Drawing.Size(75, 16);
            this.rdoCL.TabIndex = 195;
            this.rdoCL.TabStop = true;
            this.rdoCL.Text = "잔류염소";
            this.rdoCL.UseVisualStyleBackColor = true;
            this.rdoCL.Click += new System.EventHandler(this.rdoCL_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(11, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 12);
            this.label4.TabIndex = 194;
            this.label4.Text = "차트표시항목 :";
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 597);
            this.picFrLeft.TabIndex = 113;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(733, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 597);
            this.PicFrRight.TabIndex = 114;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(4, 597);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(729, 4);
            this.picFrBottom.TabIndex = 115;
            this.picFrBottom.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(4, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(729, 4);
            this.pictureBox1.TabIndex = 116;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(4, 248);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(729, 4);
            this.pictureBox2.TabIndex = 147;
            this.pictureBox2.TabStop = false;
            // 
            // cFXRT
            // 
            this.cFXRT.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.cFXRT.AxisX.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cFXRT.AxisX.Staggered = true;
            this.cFXRT.AxisY.Staggered = true;
            this.cFXRT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cFXRT.Location = new System.Drawing.Point(4, 252);
            this.cFXRT.Name = "cFXRT";
            this.cFXRT.Size = new System.Drawing.Size(729, 345);
            this.cFXRT.TabIndex = 148;
            // 
            // button1
            // 
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(686, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 26);
            this.button1.TabIndex = 200;
            this.button1.Text = "엑셀";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmRTData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 601);
            this.Controls.Add(this.cFXRT);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.ugRTData);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.picFrTop);
            this.MinimizeBox = false;
            this.Name = "frmRTData";
            this.Text = "실시간 수질 감시 조회";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRTData_FormClosing);
            this.Load += new System.EventHandler(this.frmRTData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ugRTData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDTAlertE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDTAlertS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cFXRT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid ugRTData;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private ChartFX.WinForms.Chart cFXRT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rdoCU;
        private System.Windows.Forms.RadioButton rdoTE;
        private System.Windows.Forms.RadioButton rdoPH;
        private System.Windows.Forms.RadioButton rdoTB;
        private System.Windows.Forms.RadioButton rdoCL;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDTAlertE;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDTAlertS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
    }
}