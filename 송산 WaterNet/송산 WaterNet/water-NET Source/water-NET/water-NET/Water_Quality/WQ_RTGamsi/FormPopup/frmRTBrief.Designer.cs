﻿namespace WaterNet.WQ_RTGamsi.FormPopup
{
    partial class frmRTBrief
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrGetData = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRTDT = new System.Windows.Forms.Label();
            this.lblRTPntNm = new System.Windows.Forms.Label();
            this.txtCU = new System.Windows.Forms.TextBox();
            this.txtTB = new System.Windows.Forms.TextBox();
            this.txtPH = new System.Windows.Forms.TextBox();
            this.txtTE = new System.Windows.Forms.TextBox();
            this.txtCL = new System.Windows.Forms.TextBox();
            this.btnDetail = new System.Windows.Forms.Button();
            this.lblCU = new System.Windows.Forms.Label();
            this.lblCL = new System.Windows.Forms.Label();
            this.lblTB = new System.Windows.Forms.Label();
            this.lblPH = new System.Windows.Forms.Label();
            this.lblTE = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrGetData
            // 
            this.tmrGetData.Tick += new System.EventHandler(this.tmrGetData_Tick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblRTDT);
            this.panel1.Controls.Add(this.lblRTPntNm);
            this.panel1.Controls.Add(this.txtCU);
            this.panel1.Controls.Add(this.txtTB);
            this.panel1.Controls.Add(this.txtPH);
            this.panel1.Controls.Add(this.txtTE);
            this.panel1.Controls.Add(this.txtCL);
            this.panel1.Controls.Add(this.btnDetail);
            this.panel1.Controls.Add(this.lblCU);
            this.panel1.Controls.Add(this.lblCL);
            this.panel1.Controls.Add(this.lblTB);
            this.panel1.Controls.Add(this.lblPH);
            this.panel1.Controls.Add(this.lblTE);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(391, 128);
            this.panel1.TabIndex = 8;
            // 
            // lblRTDT
            // 
            this.lblRTDT.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblRTDT.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRTDT.Location = new System.Drawing.Point(3, 25);
            this.lblRTDT.Name = "lblRTDT";
            this.lblRTDT.Size = new System.Drawing.Size(383, 23);
            this.lblRTDT.TabIndex = 124;
            this.lblRTDT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRTPntNm
            // 
            this.lblRTPntNm.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblRTPntNm.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRTPntNm.Location = new System.Drawing.Point(3, 3);
            this.lblRTPntNm.Name = "lblRTPntNm";
            this.lblRTPntNm.Size = new System.Drawing.Size(384, 22);
            this.lblRTPntNm.TabIndex = 123;
            this.lblRTPntNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCU
            // 
            this.txtCU.Location = new System.Drawing.Point(116, 100);
            this.txtCU.Name = "txtCU";
            this.txtCU.ReadOnly = true;
            this.txtCU.Size = new System.Drawing.Size(70, 21);
            this.txtCU.TabIndex = 122;
            // 
            // txtTB
            // 
            this.txtTB.Location = new System.Drawing.Point(116, 77);
            this.txtTB.Name = "txtTB";
            this.txtTB.ReadOnly = true;
            this.txtTB.Size = new System.Drawing.Size(70, 21);
            this.txtTB.TabIndex = 121;
            // 
            // txtPH
            // 
            this.txtPH.Location = new System.Drawing.Point(315, 54);
            this.txtPH.Name = "txtPH";
            this.txtPH.ReadOnly = true;
            this.txtPH.Size = new System.Drawing.Size(70, 21);
            this.txtPH.TabIndex = 120;
            // 
            // txtTE
            // 
            this.txtTE.Location = new System.Drawing.Point(315, 77);
            this.txtTE.Name = "txtTE";
            this.txtTE.ReadOnly = true;
            this.txtTE.Size = new System.Drawing.Size(70, 21);
            this.txtTE.TabIndex = 119;
            // 
            // txtCL
            // 
            this.txtCL.Location = new System.Drawing.Point(116, 54);
            this.txtCL.Name = "txtCL";
            this.txtCL.ReadOnly = true;
            this.txtCL.Size = new System.Drawing.Size(70, 21);
            this.txtCL.TabIndex = 118;
            // 
            // btnDetail
            // 
            this.btnDetail.Image = global::WaterNet.WQ_RTGamsi.Properties.Resources.Infor;
            this.btnDetail.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetail.Location = new System.Drawing.Point(314, 99);
            this.btnDetail.Name = "btnDetail";
            this.btnDetail.Size = new System.Drawing.Size(72, 25);
            this.btnDetail.TabIndex = 13;
            this.btnDetail.Text = "상세";
            this.btnDetail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetail.UseVisualStyleBackColor = true;
            this.btnDetail.Click += new System.EventHandler(this.btnDetail_Click);
            // 
            // lblCU
            // 
            this.lblCU.AutoSize = true;
            this.lblCU.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCU.Location = new System.Drawing.Point(1, 104);
            this.lblCU.Name = "lblCU";
            this.lblCU.Size = new System.Drawing.Size(111, 12);
            this.lblCU.TabIndex = 12;
            this.lblCU.Text = "전기전도도(CU) :";
            this.lblCU.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCL
            // 
            this.lblCL.AutoSize = true;
            this.lblCL.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCL.Location = new System.Drawing.Point(15, 58);
            this.lblCL.Name = "lblCL";
            this.lblCL.Size = new System.Drawing.Size(97, 12);
            this.lblCL.TabIndex = 11;
            this.lblCL.Text = "잔류염소(CL) :";
            this.lblCL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTB
            // 
            this.lblTB.AutoSize = true;
            this.lblTB.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTB.Location = new System.Drawing.Point(41, 81);
            this.lblTB.Name = "lblTB";
            this.lblTB.Size = new System.Drawing.Size(71, 12);
            this.lblTB.TabIndex = 10;
            this.lblTB.Text = "탁도(TB) :";
            this.lblTB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPH
            // 
            this.lblPH.AutoSize = true;
            this.lblPH.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPH.Location = new System.Drawing.Point(189, 58);
            this.lblPH.Name = "lblPH";
            this.lblPH.Size = new System.Drawing.Size(122, 12);
            this.lblPH.TabIndex = 9;
            this.lblPH.Text = "수소이온농도(pH) :";
            this.lblPH.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTE
            // 
            this.lblTE.AutoSize = true;
            this.lblTE.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTE.Location = new System.Drawing.Point(240, 81);
            this.lblTE.Name = "lblTE";
            this.lblTE.Size = new System.Drawing.Size(71, 12);
            this.lblTE.TabIndex = 8;
            this.lblTE.Text = "온도(TE) :";
            this.lblTE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmRTBrief
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(397, 134);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmRTBrief";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = " 실시간 수질 감시 지점 정보";
            this.Load += new System.EventHandler(this.frmRTBrief_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer tmrGetData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDetail;
        private System.Windows.Forms.Label lblCU;
        private System.Windows.Forms.Label lblCL;
        private System.Windows.Forms.Label lblTB;
        private System.Windows.Forms.Label lblPH;
        private System.Windows.Forms.Label lblTE;
        private System.Windows.Forms.Label lblRTPntNm;
        private System.Windows.Forms.TextBox txtCU;
        private System.Windows.Forms.TextBox txtTB;
        private System.Windows.Forms.TextBox txtPH;
        private System.Windows.Forms.TextBox txtTE;
        private System.Windows.Forms.TextBox txtCL;
        private System.Windows.Forms.Label lblRTDT;
    }
}