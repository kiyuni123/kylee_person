﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WQ_RTGamsi
{
    class WQ_AppStatic
    {
        #region 팝업 폼 오픈여부 선언부

        /// <summary>
        /// frmRTData 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_RT_DATA = false;
        /// <summary>
        /// frmRTAlertHistory 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_ALERT_HIS = false;
        /// <summary>
        /// frmRTBriefDetail 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_DETAIL = false;
        /// <summary>
        /// frmRTMonitorPoint 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_MONITOR_POINT = false;

        #endregion

        public static double MAP_X = 0; //GIS에서 Mouse Down시 구한 Map X 좌표
        public static double MAP_Y = 0; //GIS에서 Mouse Down시 구한 Map Y 좌표

        /// <summary>
        /// 그리드에서 선택한 지점의 IGeometry
        /// </summary>
        public static IGeometry CURRENT_IGEOMETRY;
    }
}
