﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

using WaterNet.WaterNetCore;

namespace WaterNet.WQ_Common
{
    /// <summary>
    /// Project ID : WN_WQ_A00
    /// Project Explain : 수질공통
    /// Project Developer : 오두석
    /// Project Create Date : 2010.10.13
    /// Class Explain : 수질의 모든 프로젝트에서 사용되는 공통함수 Class
    /// </summary>
    public class WQ_Function
    {
        #region Control Setting 함수

        #region - Combo Setting

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 민원의 분류
        /// 대상 Table : OCSGCCD
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_MinwonType(ComboBox oCombo)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   CODE_NAME || ' (' || CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_CODE");
            oStringBuilder.AppendLine("WHERE    PCODE = '9004' AND SUBSTR(CODE, 0, 2) = '20' AND CODE NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("ORDER BY CODE");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_CODE");

            oCombo.Items.Clear();

            oCombo.Items.Add("전체");
            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 지자체
        /// 대상 Table : OCSGCCD
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_Municipality(ComboBox oCombo)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   SGCNM || ' (' || SGCCD || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     OCSGCCD");
            oStringBuilder.AppendLine("ORDER BY SGCCD");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "OCSGCCD");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 사업장
        /// 대상 Table : WQSITE
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="iAddAll"> 0 : '전체' 항목 Add 안함, 1 : '전체' 항목 Add 함</param>
        public static void SetCombo_Establishment(ComboBox oCombo, int iAddAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   SITENM || ' (' || SITECD || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQSITE");
            oStringBuilder.AppendLine("ORDER BY SITECD");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQSITE");

            oCombo.Items.Clear();

            if (iAddAll == 1) oCombo.Items.Add("전체");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : Center
        /// 대상 Table : OCSGCCD
        /// 비고 : 센터는 코드는 지자체 코드를 쓰고 센터명만 따로 쓴다.
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_Center(ComboBox oCombo, string strSGCCD)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   CTRNM || ' (' || SGCCD || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     OCSGCCD");
            oStringBuilder.AppendLine("WHERE    SGCCD = '" + strSGCCD + "'");
            oStringBuilder.AppendLine("ORDER BY SGCCD");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "OCSGCCD");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 정수장
        /// 대상 Table : CM_LOCATION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSRCCD">지자체 코드</param>
        public static void SetCombo_FiltrationPlant(ComboBox oCombo, string strSRCCD)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   TO_NUMBER(LOC_CODE) AS CD, LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    LOC_GBN = '정수장' AND SGCCD = '" + strSRCCD + "'");
            oStringBuilder.AppendLine("ORDER BY CD");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 배수지
        /// 대상 Table : CM_LOCATION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSRCCD">지자체 코드</param>
        public static void SetCombo_WaterReservoir(ComboBox oCombo, string strSRCCD)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   TO_NUMBER(LOC_CODE) AS CD, LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    LOC_GBN = '배수지' AND SGCCD = '" + strSRCCD + "'");
            oStringBuilder.AppendLine("ORDER BY CD");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 대블록
        /// 대상 Table : CM_LOCATION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSRCCD">지자체 코드</param>
        public static void SetCombo_LargeBlock(ComboBox oCombo, string strSRCCD)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    LOC_GBN = '대블록' AND SGCCD = '" + strSRCCD + "'");
            oStringBuilder.AppendLine("ORDER BY LOC_NAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 중블록
        /// 대상 Table : CM_LOCATION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSRCCD">지자체 코드</param>
        /// <param name="strLFTRIDN">대블록 코드</param>
        /// <param name="iIsUseAll">0 : 전체 포함 X, 1 : 전체 포함</param>
        public static void SetCombo_MediumBlock(ComboBox oCombo, string strSRCCD, string strLFTRIDN, int iIsUseAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    LOC_GBN = '중블록' AND SGCCD = '" + strSRCCD + "' AND PLOC_CODE = '" + strLFTRIDN  + "'");
            oStringBuilder.AppendLine("ORDER BY LOC_NAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            oCombo.Items.Clear();

            if (iIsUseAll == 1)
            {
                oCombo.Items.Add("전체");
            }

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 중블록
        /// 대상 Table : CM_LOCATION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSRCCD">지자체 코드</param>
        /// <param name="strLFTRIDN">대블록 코드</param>
        public static void SetCombo_MediumBlock(ComboBox oCombo, string strSRCCD, string strLFTRIDN)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    LOC_GBN = '중블록' AND SGCCD = '" + strSRCCD + "' AND PLOC_CODE = '" + strLFTRIDN + "'");
            oStringBuilder.AppendLine("ORDER BY LOC_NAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 소블록
        /// 대상 Table : CM_LOCATION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSRCCD">지자체 코드</param>
        /// <param name="strMFTRIDN">중블록 코드</param>
        /// <param name="iIsUseAll">0 : 전체 포함 X, 1 : 전체 포함</param>
        public static void SetCombo_SmallBlock(ComboBox oCombo, string strSRCCD, string strMFTRIDN, int iIsUseAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    LOC_GBN = '소블록' AND SGCCD = '" + strSRCCD + "' AND PLOC_CODE = '" + strMFTRIDN + "'");
            oStringBuilder.AppendLine("ORDER BY LOC_NAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            oCombo.Items.Clear();

            if (iIsUseAll == 1)
            {
                oCombo.Items.Add("전체");
            }
            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 수질감시옵션 여부 - 옵션 1 ~ 옵션 3
        /// 대상 Table : 없음
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_MonitorOption(ComboBox oCombo)
        {
            oCombo.Items.Add("옵션 1");
            oCombo.Items.Add("옵션 2");
            oCombo.Items.Add("옵션 3");

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 관망해석실행 여부 - 실행 (Y), 실행안함 (N)
        /// 대상 Table : 없음
        /// PN : Piping Network
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_PNAnalysisExecuteFlag(ComboBox oCombo)
        {
            oCombo.Items.Add("실행 (Y)");
            oCombo.Items.Add("실행안함 (N)");
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 관망해석 재실행 주기 - 10 ~ 60 (10분 단위) - 추후 변경 가능
        /// 대상 Table : 없음
        /// PN : Piping Network
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_PNAnalysisReExecutePeriod(ComboBox oCombo)
        {
            oCombo.Items.Add("10 (분)");
            oCombo.Items.Add("20 (분)");
            oCombo.Items.Add("30 (분)");
            oCombo.Items.Add("40 (분)");
            oCombo.Items.Add("50 (분)");
            oCombo.Items.Add("60 (분)");

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 감시지점상세정보 표시할 필터링시간 1, 10, 30, 60분
        /// 대상 Table : 없음
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_BaseDisplayTime(ComboBox oCombo)
        {
            oCombo.Items.Add("1 (분)");
            oCombo.Items.Add("10 (분)");
            oCombo.Items.Add("30 (분)");
            oCombo.Items.Add("60 (분)");

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 중요시설
        /// 대상 Table : WQ_IMPORTANT_FACILITIES
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSFTRIDN">소블록 코드. Empty 허용</param>
        /// <param name="strIF_GBN">중요시설구분. Empty 허용</param>
        /// <param name="IsUseAll">Combo에 전체 Item 등록 여부. True : 전체등록,  False : 전체등록안함</param>
        public static void SetCombo_FacilitiesPosition(ComboBox oCombo, string strSFTRIDN, string strIF_GBN, bool IsUseAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   NO AS NO, FACILITIES_NAME || ' (' || NO || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQ_IMPORTANT_FACILITIES");
            oStringBuilder.AppendLine("WHERE    SBLOCK_CODE LIKE '%" + strSFTRIDN + "%' AND FACILITIES_GBN LIKE '%" + strIF_GBN + "%'");
            oStringBuilder.AppendLine("ORDER BY NO");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_IMPORTANT_FACILITIES");

            oCombo.Items.Clear();

            if (IsUseAll == true)
            {
                oCombo.Items.Add("전체");
            }

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 중요시설구분
        /// 대상 Table : CM_CODE
        /// </summary>
        /// <param name="iAddAll"> 0 : '전체' 항목 Add 안함, 1 : '전체' 항목 Add 함</param>
        public static void SetCombo_FacilitiesPartition(ComboBox oCombo, int iAddAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   CODE_NAME || ' (' || CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_CODE");
            oStringBuilder.AppendLine("WHERE    PCODE = '3001'");
            oStringBuilder.AppendLine("ORDER BY CODE");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "FACILITIES");

            oCombo.Items.Clear();

            if (iAddAll == 1) oCombo.Items.Add("전체");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 감시지점
        /// 대상 Table : WQ_RT_MONITOR_POINT
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSFTRIDN">소블록 코드. Empty 허용</param>
        /// <param name="strMONPNT_GBN">감시지점구분. Empty 허용</param>
        /// <param name="IsUseAll">Combo에 전체 Item 등록 여부. True : 전체등록,  False : 전체등록안함</param>
        public static void SetCombo_MonitorPosition(ComboBox oCombo, string strSFTRIDN, string strMONPNT_GBN, bool IsUseAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   MONITOR_NO AS MONITOR_NO, MONPNT || ' (' || MONITOR_NO || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("WHERE    SBLOCK_CODE LIKE '%" + strSFTRIDN + "%' AND MONPNT_GBN LIKE '%" + strMONPNT_GBN + "%'");
            oStringBuilder.AppendLine("ORDER BY MONITOR_NO");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_RT_MONITOR_POINT");

            oCombo.Items.Clear();

            if (IsUseAll == true)
            {
                oCombo.Items.Add("전체");
            }

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 감시지점
        /// 대상 Table : WQ_RT_MONITOR_POINT
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strMFTRIDN">중블록 코드 </param>
        /// <param name="strMONPNT_GBN">감시지점구분. Empty 허용</param>
        /// <param name="IsUseAll">Combo에 전체 Item 등록 여부. True : 전체등록,  False : 전체등록안함</param>
        public static void SetCombo_MonitorPositionByMediumBlock(ComboBox oCombo, string strMFTRIDN, string strMONPNT_GBN, bool IsUseAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
            string strBLOCK_SQL = WQ_Function.GetAllSmallBlockInMediumBlock(strMFTRIDN);

            oStringBuilder.AppendLine("SELECT   MONITOR_NO AS MONITOR_NO, MONPNT || ' (' || MONITOR_NO || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("WHERE    SBLOCK_CODE IN (" + strBLOCK_SQL + ") AND MONPNT_GBN LIKE '%" + strMONPNT_GBN + "%'");
            oStringBuilder.AppendLine("ORDER BY MONITOR_NO");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_RT_MONITOR_POINT");

            oCombo.Items.Clear();

            if (IsUseAll == true)
            {
                oCombo.Items.Add("전체");
            }

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 감시지점구분
        /// 대상 Table : CM_CODE
        /// </summary>
        /// <param name="iAddAll"> 0 : '전체' 항목 Add 안함, 1 : '전체' 항목 Add 함</param>
        public static void SetCombo_MonitorPartition(ComboBox oCombo, int iAddAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   CODE_NAME || ' (' || CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_CODE");
            oStringBuilder.AppendLine("WHERE    PCODE = '3002'");
            oStringBuilder.AppendLine("ORDER BY CODE");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "FACILITIES");

            oCombo.Items.Clear();

            if (iAddAll == 1) oCombo.Items.Add("전체");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 관세척지점
        /// 대상 Table : WQ_PCLEAN_SECTION
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSFTRIDN">소블록 코드. Empty 허용</param>
        /// <param name="IsUseAll">Combo에 전체 Item 등록 여부. True : 전체등록,  False : 전체등록안함</param>
        public static void SetCombo_PipeCleanPosition(ComboBox oCombo, string strSFTRIDN, bool IsUseAll)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   PCLEAN_NO AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQ_PCLEAN_SECTION");
            oStringBuilder.AppendLine("WHERE    BLOCK_CODE = '" + strSFTRIDN + "'");
            oStringBuilder.AppendLine("ORDER BY PCLEAN_NO");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_PCLEAN_SECTION");

            oCombo.Items.Clear();

            if (IsUseAll == true)
            {
                oCombo.Items.Add("전체");
            }

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 측정주기
        /// 대상 Table : WQCODE
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_MeasurePeriod(ComboBox oCombo)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   CDNM || ' (' || CD || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQCODE");
            oStringBuilder.AppendLine("WHERE    TITLE = '측정주기'");
            oStringBuilder.AppendLine("ORDER BY CD");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCODE");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 시험항목분류
        /// 대상 Table : WQCTIT
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_TestItemCategory(ComboBox oCombo)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   TITGRP AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQCTIT");
            oStringBuilder.AppendLine("GROUP BY TITGRP");
            oStringBuilder.AppendLine("ORDER BY TITGRP");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 부서
        /// 대상 Table : WQDPT
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strSGCCD">지자체명 (코드) - Combo값 그대로 받아 스플릿해서 사용</param>
        public static void SetCombo_Division(ComboBox oCombo, string strSGCCD)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            strSGCCD = SplitToCode(strSGCCD);
            oStringBuilder.AppendLine("SELECT   DPTNM || ' (' || DPTCD || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WQDPT");
            oStringBuilder.AppendLine("WHERE    SGCCD = '" + strSGCCD + "'");
            oStringBuilder.AppendLine("ORDER BY DPTCD");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQDPT");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 수용가 급수상태
        /// 대상 Table : WI_DMWSRSRC
        /// </summary>
        /// <param name="oCombo"></param>
        public static void SetCombo_SupplyStatus(ComboBox oCombo)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   DISTINCT(HYDRNTSTAT) AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WI_DMWSRSRC");
            oStringBuilder.AppendLine("ORDER BY HYDRNTSTAT");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_DMWSRSRC");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        public static void SetCombo_TAGID2(ComboBox oCombo, string strBR_CODE)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   A.TAGNAME, a.tagname || '(' || a.description || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     IF_IHTAGS A, IF_TAG_GBN B");
            oStringBuilder.AppendLine("WHERE    A.TAGNAME = B.TAGNAME AND B.TAG_GBN = '" + strBR_CODE + "I'" + " AND A.USE_YN = 'Y'");
            oStringBuilder.AppendLine("ORDER BY A.TAGNAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");
            //oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                List<KeyValuePair<string, string>> loFormat = new List<KeyValuePair<string, string>>();
                loFormat.Add(new KeyValuePair<string, string>(string.Empty, string.Empty));
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    loFormat.Add(new KeyValuePair<string, string>(oDRow["TAGNAME"].ToString().Trim(), oDRow["RTNDATA"].ToString().Trim()));

                    //oCombo.Items.Add(new KeyValuePair<string, string>(oDRow["TAGNAME"].ToString().Trim(), oDRow["RTNDATA"].ToString().Trim()));
                    //oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                    //i++;
                }

                oCombo.DataSource = loFormat.Select(s => new KeyValuePair<string, string>(s.Key, s.Value)).ToList();
                oCombo.ValueMember = "Key";
                oCombo.DisplayMember = "Value";

                //foreach (DataRow oDRow in pDS.Tables[0].Rows)
                //{
                //    oCombo.Items.Add(new KeyValuePair<string, string>(oDRow["TAGNAME"].ToString().Trim(), oDRow["RTNDATA"].ToString().Trim()));
                //    //oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                //    i++;
                //}
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : TAG ID를 SELECT 해서 set한다.
        /// 대상 Table : IF_IHTAGS
        /// </summary>
        public static void SetCombo_TAGID(ComboBox oCombo, string strBR_CODE, string strSFTRIDN, string strMFTRIDN, string strReservoir)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   A.TAGNAME AS RTNDATA");
            oStringBuilder.AppendLine("FROM     IF_IHTAGS A, IF_TAG_GBN B");
            oStringBuilder.AppendLine("WHERE    A.TAGNAME = B.TAGNAME AND B.TAG_GBN = '" + strBR_CODE + "I'" + " AND A.USE_YN = 'Y'");
            oStringBuilder.AppendLine("         AND (A.LOC_CODE = '" + strSFTRIDN + "' OR A.LOC_CODE = '" + strMFTRIDN + "' OR A.LOC_CODE = '" + strReservoir + "')");
            oStringBuilder.AppendLine("ORDER BY A.TAGNAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// ComboBox에서 특정 Data를 찾아 해당 Select 시킨다.
        /// </summary>
        /// <param name="oCombo">ComboBox</param>
        /// <param name="strFindData">Combo에서 찾을 Data</param>
        public static void SetCombo_SelectFindData(ComboBox oCombo, string strFindData)
        {
            int iIndex = 0;

            if (oCombo.Items.Count <= 0)
            {
                oCombo.Items.Add(strFindData);
                return;
            } 

            iIndex = oCombo.FindString(strFindData);
            if (iIndex > 0)
            {
                oCombo.SelectedIndex = iIndex;
            }
            else
            {
                oCombo.SelectedIndex = 0;
            }
        }

        #endregion

        #region - UltraDateTime Setting
        
        /// <summary>
        /// UltraDateTime Control의 Mask Format을 변환한다.
        /// </summary>
        /// <param name="oUDT">대상 UltraDateTimeEditor</param>
        /// <param name="iType">Mask Format Type. 
        /// 0 : 'RRRR-MM-dd',
        /// 1 : 'dd-mm-yyyy',
        /// 2 : 'RRRR-MM',
        /// 3 : 'mm-yyyy',
        /// n : 'RRRR-MM-dd'</param>
        /// <param name="iSeparator">Mask Format Separator
        /// 0 : '-',
        /// 1 : '/',
        /// n : '-'</param>
        public static void SetUDateTime_MaskInput(UltraDateTimeEditor oUDT, int iType, int iSeparator)
        {
            string strSeparator = string.Empty;

            switch (iSeparator)
            {
                case 0:
                    strSeparator = "-";
                    break;
                case 1:
                    strSeparator = "/";
                    break;
                default:
                    strSeparator = "-";
                    break;
            }

            switch (iType)
            {
                case 0:
                    oUDT.MaskInput = "yyyy" + strSeparator + "mm" + strSeparator +"dd";
                    break;
                case 1:
                    oUDT.MaskInput = "dd" + strSeparator + "mm" + strSeparator + "yyyy";
                    break;
                case 2:
                    oUDT.MaskInput = "yyyy" + strSeparator + "mm";
                    break;
                case 3:
                    oUDT.MaskInput = "mm" + strSeparator + "yyyy";
                    break;
                default:
                    oUDT.MaskInput = "dd" + strSeparator + "mm" + strSeparator + "yyyy";
                    break;
            }
        }

        /// <summary>
        /// UltraDateTimeEditor의 현재 Date에 +, - 월을 계산해서 Set한다.
        /// </summary>
        /// <param name="oUDT">대상 UltraDateTimeEditor</param>
        /// <param name="iMonth">+, - Month
        /// ex) -10 : 현재보다 10개월 전
        ///      10 : 현재보다 10개월 후</param>
        public static void SetUDateTime_ModifyMonth(UltraDateTimeEditor oUDT, int iMonth)
        {
            string strSeparator = string.Empty;
            DateTime oDT = DateTime.Now;

            strSeparator = "-";

            oUDT.Value = oDT.AddMonths(iMonth).Year + strSeparator + oDT.AddMonths(iMonth).Month + strSeparator + oDT.Day;
        }

        /// <summary>
        /// UltraDateTimeEditor의 현재 Date에 +, - 일을 계산해서 Set한다.
        /// </summary>
        /// <param name="oUDT">대상 UltraDateTimeEditor</param>
        /// <param name="iMonth">+, - Day
        /// ex) -10 : 현재보다 10일 전
        ///      10 : 현재보다 10일 후</param>
        public static void SetUDateTime_ModifyDay(UltraDateTimeEditor oUDT, int iDay)
        {
            string strSeparator = string.Empty;
            DateTime oDT = DateTime.Now;

            strSeparator = "-";

            oUDT.Value = oDT.AddDays(iDay).Year + strSeparator + oDT.AddDays(iDay).Month + strSeparator + oDT.AddDays(iDay).Day;
        }

        #endregion

        #endregion


        #region Database에서 특정 값 Select해서 반환

        /// <summary>
        /// Oracle Database Server의 System Date를 취득해 RRRRMMDD Format으로 반환한다.
        /// </summary>
        /// <returns>string RRRRMMDD</returns>
        public static string GetOracleSystemDate()
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();
            string strRtn = string.Empty;

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.AppendLine("SELECT   TO_CHAR(SYSDATE, 'RRRRMMDD') AS RTNDATA");
            oStringBuilder.AppendLine("FROM     DUAL");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQCTIT");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();

                    i++;
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// 수용가 마스터 테이블(DMINFO) 에서 수용가 명으로 수용가 No를 취득해 반환한다.
        /// </summary>
        /// <param name="strDMNM">수용가 명</param>
        /// <returns>수용가 코드</returns>
        public static string GetConsumerNo(string strDMNM)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();
            string strRtn = string.Empty;

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.AppendLine("SELECT   DMNO AS RTNDATA");
            oStringBuilder.AppendLine("FROM     DMINFO");
            oStringBuilder.AppendLine("WHERE    DMNM = '" + strDMNM + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "DMINFO");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();

                    i++;
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// 일일수질측정개요(WQCDWQSMR), 주간월간수질측정개요(WQCPWQSMR) Table에서 비고(RMK) Select해서 반환한다.
        /// </summary>
        /// <param name="strSRPRID">주기</param>
        /// <param name="strParam">Where 조건</param>
        /// <returns></returns>
        public static string GetRemark(string strSRPRID, string strParam)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();
            string strTable = string.Empty;
            string strRtn = string.Empty;

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            if (strSRPRID == "1")
            {
                strTable = "WQCDWQSMR";
            }
            else
            {
                strTable = "WQCPWQSMR";
            }
            oStringBuilder.AppendLine("SELECT   RMK AS RTNDATA");
            oStringBuilder.AppendLine("FROM     " + strTable);
            oStringBuilder.AppendLine(strParam);

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, strTable);

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();

                    i++;
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// 관로번호 취득 함수 : WH_RPT_LINKS에서 LINK_ID를 취득후 조건으로 WH_TAGS에서 PIPE_NUMBER 취득해 반환
        /// POSITION_INFO 필드 : 여러개 항목이 파이프로 구분
        ///             첫번째 - 필드명			데이터				설명
        ///                      FTR_CDE        SA001				상수관로로 Layer 찾을때 사용
        ///                      FTR_CDE        SA002				급수관로로 Layer 찾을때 사용
        ///             세번째 - 필드명			데이터				설명
        ///                      FTR_IDN        1					Layer에서 FTR_IDN 이므로 레이어에서 지점 찾을 때 사용 하면 될 듯
        ///             네번째 - 필드명			데이터				설명
        ///                      FTR_IDN        1					소블록 코드로 CM_LOCATION의  FTR_IDN임 CM_LOCATION에서 FTR_CODE = BZ003 AND FTR_IDN = 1 으로 소블록 코드를 찾을 수 있음.
        ///                      위 조건대로면 블록명은 고부이고 LOC_CODE는 000037 이 됨.
        /// </summary>
        /// <param name="strRPT_NUMBER"></param>
        /// <param name="strINP_NUMBER"></param>
        /// <returns></returns>
        public static string GetPipeNumber(string strINP_NUMBER, string strLINK_ID)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();
            string strRtn = string.Empty;

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   POSITION_INFO AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WH_TAGS");
            oStringBuilder.AppendLine("WHERE    ID = '" + strLINK_ID + "' AND INP_NUMBER = '" + strINP_NUMBER + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WH_TAGS");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    string[] arrList = oDRow["RTNDATA"].ToString().Trim().Split('|');
                    strRtn = arrList[2].ToString();

                    i++;
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// 수도꼭지수질검사 순번 채번(채수일자를 기준으로)
        /// </summary>
        /// <param name="strCLTDT">순번을 채번할 채수일자</param>
        /// <param name="iTBLType">순번을 채번할 Table.
        /// 0 : WQCTPWQIPS, 1 : WQCPWQSMR</param>
        /// <returns></returns>
        public static string GenWQGumsaSequenceNo(string strCLTDT, int iTBLType)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();
            string strRtn = string.Empty;
            string strTable = string.Empty;

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            switch (iTBLType)
            {
                case 0:
                    strTable = "WQCTPWQIPS";
                    break;
                case 1:
                    strTable = "WQCPWQSMR";
                    break;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   COUNT(CLTDT) + 1 AS RTNDATA");
            oStringBuilder.AppendLine("FROM     " + strTable);
            oStringBuilder.AppendLine("WHERE    CLTDT = '" + strCLTDT + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, strTable);

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();

                    i++;
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// 수도꼭지수질검사 순번 취득
        /// </summary>
        /// <param name="strParam">순번을 가져올 조건</param>
        /// <param name="iTBLType">순번을 채번할 Table.
        /// 0 : WQCTPWQIPS, 1 : WQCPWQSMR, 2 : WQCPWQRL</param>
        /// <returns></returns>
        public static string GetWQGumsaSequenceNo(string strParam, int iTBLType)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();
            string strRtn = string.Empty;
            string strTable = string.Empty;

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            switch (iTBLType)
            {
                case 0:
                    strTable = "WQCTPWQIPS";
                    break;
                case 1:
                    strTable = "WQCPWQSMR";
                    break;
                case 2:
                    strTable = "WQCPWQRL";
                    break;
            }
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   SERNO AS RTNDATA");
            oStringBuilder.AppendLine("FROM     " + strTable);
            oStringBuilder.AppendLine("WHERE    " + strParam );

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, strTable);

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();

                    i++;
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// FTR_IDN으로 블록명 (코드)를 찾아 반환한다.
        /// </summary>
        /// <param name="strFTR_IDN"></param>
        /// <returns></returns>
        public static string GetBlockNameCodeByFTR_IDN(string strFTR_IDN)
        {
            string strRtn = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   LOC_NAME || ' (' || LOC_CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    FTR_IDN = '" + strFTR_IDN + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// strLOC_CODE으로 소블록의 FTR_IDN을 찾아 반환한다.
        /// </summary>
        /// <param name="strLOC_CODE"></param>
        /// <returns>CM_LOCATION의 FTR_IDN</returns>
        public static string GetSmallBlockFTR_IDN(string strLOC_CODE)
        {
            string strRtn = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   FTR_IDN AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    FTR_CODE = 'BZ003' AND LOC_CODE = '" + strLOC_CODE + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// 중블록 LOC_CODE로 중블록에 포함된 모든 소블록 LOC_CODE를 찾아 반환한다.
        /// </summary>
        /// <param name="strLOC_CODE">중블록 코드</param>
        /// <returns>'LOC_CODE', 'LOC_CODE', 'n'</returns>
        public static string GetAllSmallBlockInMediumBlock(string strLOC_CODE)
        {
            string strRtn = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   LOC_CODE AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    FTR_CODE = 'BZ003' AND PLOC_CODE = '" + strLOC_CODE + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn += "'" + oDRow["RTNDATA"].ToString().Trim() + "', ";
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn.Substring(0, strRtn.Length - 2);
        }

        /// <summary>
        /// 중블록 LOC_CODE로 중블록에 포함된 모든 소블록 FTR_IDN를 찾아 반환한다.
        /// </summary>
        /// <param name="strLOC_CODE">중블록 코드</param>
        /// <returns>'FTR_IDN', 'FTR_IDN', 'n'</returns>
        public static string GetAllSmallBlockFTRIDNInMediumBlock(string strLOC_CODE)
        {
            string strRtn = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   FTR_IDN AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    FTR_CODE = 'BZ003' AND PLOC_CODE = '" + strLOC_CODE + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn += "'" + oDRow["RTNDATA"].ToString().Trim() + "', ";
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn.Substring(0, strRtn.Length - 2);
        }

        /// <summary>
        /// 중블록코드로 중블록에 속한 모든 소블록 코드를 찾아 반환
        /// 소블록이 없으면 중블록 코드
        /// </summary>
        /// <param name="strDMNM">수용가 명</param>
        /// <returns>수용가 코드</returns>
        public static string GetReservoirFromMediumBlockCode(string strMBlock)
        {
            string strRtn = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   B.LOC_CODE AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_LOCATION B ON B.LOC_NAME = A.REL_LOC_NAME");
            oStringBuilder.AppendLine("WHERE    A.FTR_CODE = 'BZ002' AND A.LOC_CODE = '" + strMBlock + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// 소블록 코드로 중블록 코드를 찾아 반환
        /// </summary>
        /// <param name="strLOC_CODE"></param>
        /// <returns>CM_LOCATION의 PFTR_IDN</returns>
        public static string GetMediumBlockBySmallBlockCode(string strLOC_CODE)
        {
            string strRtn = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   PLOC_CODE AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    FTR_CODE = 'BZ003' AND LOC_CODE = '" + strLOC_CODE + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        /// <summary>
        /// strLOC_CODE으로 중블록의 FTR_IDN을 찾아 반환한다.
        /// </summary>
        /// <param name="strLOC_CODE"></param>
        /// <returns>CM_LOCATION의 FTR_IDN</returns>
        public static string GetMediumBlockFTR_IDN(string strLOC_CODE)
        {
            string strRtn = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return strRtn;
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   FTR_IDN AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    FTR_CODE = 'BZ002' AND LOC_CODE = '" + strLOC_CODE + "'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_LOCATION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRtn = oDRow["RTNDATA"].ToString().Trim();
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            return strRtn;
        }

        #endregion


        #region Data 변환

        /// <summary>
        /// DateTime을 받아 RRRRMMDD Format의 String로 변환해 반환한다.
        /// </summary>
        /// <param name="oDateTime">DateTime Object</param>
        /// <returns>string RRRRMMDD</returns>
        public static string StringToDateTime(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;
        }

        /// <summary>
        /// DateTime을 받아 RRRRMMDDHH24MISS Format의 String로 변환해 반환한다.
        /// </summary>
        /// <param name="oDateTime">DateTime Object</param>
        /// <returns>string RRRRMMDDHH24MISS</returns>
        public static string ChangeDateTimeToDateTime(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0') + Convert.ToString(oDateTime.Hour).PadLeft(2, '0') + Convert.ToString(oDateTime.Minute).PadLeft(2, '0') + Convert.ToString(oDateTime.Second).PadLeft(2, '0');

            return strRtn;
        }

        /// <summary>
        /// 현재 PC 시간에 +, - 시간을 구해서 string 으로 반환
        /// </summary>
        /// <param name="dblCalHour">현재 시간에 +, - 할 시간</param>
        /// <param name="iSeparator"></param>
        /// <returns></returns>
        public static string GetDateTimeFormatStringToCalculateHour(double dblCalHour, int iSeparator)
        {
            string strRtn = string.Empty;

            string strSeparator = string.Empty;

            DateTime oDateTime = DateTime.Now;

            switch (iSeparator)
            {
                case 0:
                    strSeparator = "-";
                    break;
                case 1:
                    strSeparator = "/";
                    break;
                default:
                    strSeparator = "-";
                    break;
            }

            strRtn = Convert.ToString(oDateTime.AddHours(dblCalHour).Year) + strSeparator + Convert.ToString(oDateTime.AddHours(dblCalHour).Month).PadLeft(2, '0') + strSeparator + Convert.ToString(oDateTime.AddHours(dblCalHour).Day).PadLeft(2, '0');
            strRtn += " " + Convert.ToString(oDateTime.AddHours(dblCalHour).Hour).PadLeft(2, '0') + ":" + Convert.ToString(oDateTime.Minute).PadLeft(2, '0') + ":" + Convert.ToString(oDateTime.Second).PadLeft(2, '0');

            return strRtn;
        }

        /// <summary>
        /// 현재 PC 시간에 +, - 분을 구해서 string 으로 반환
        /// </summary>
        /// <param name="dblCalMinute">현재 시간에 +, - 할 분</param>
        /// <param name="iSeparator"></param>
        /// <returns></returns>
        public static string GetDateTimeFormatStringToCalculateMinute(double dblCalMinute, int iSeparator)
        {
            string strRtn = string.Empty;

            string strSeparator = string.Empty;

            DateTime oDateTime = DateTime.Now;

            switch (iSeparator)
            {
                case 0:
                    strSeparator = "-";
                    break;
                case 1:
                    strSeparator = "/";
                    break;
                default:
                    strSeparator = "-";
                    break;
            }

            strRtn = Convert.ToString(oDateTime.AddMinutes(dblCalMinute).Year) + strSeparator + Convert.ToString(oDateTime.AddMinutes(dblCalMinute).Month).PadLeft(2, '0') + strSeparator + Convert.ToString(oDateTime.AddMinutes(dblCalMinute).Day).PadLeft(2, '0');
            strRtn += " " + Convert.ToString(oDateTime.AddMinutes(dblCalMinute).Hour).PadLeft(2, '0') + ":" + Convert.ToString(oDateTime.AddMinutes(dblCalMinute).Minute).PadLeft(2, '0') + ":" + Convert.ToString(oDateTime.Second).PadLeft(2, '0');

            return strRtn;
        }

        /// <summary>
        /// Time Format의 String을 일반 String으로 변환해서 리턴
        /// </summary>
        /// <param name="strTime">HH:MM:SS Type의 Time String</param>
        /// <returns>HHMMSS</returns>
        public static string ChangeStringToTimeFormat(string strTime, int iType)
        {
            string strRTN = string.Empty;

            if (iType == 0) //HHMMSS
            {
                strRTN = strTime.Substring(0, 2) + strTime.Substring(3, 2) + strTime.Substring(6, 2);
            }
            else if (iType == 1) //HHMM
            {
                strRTN = strTime.Substring(0, 2) + strTime.Substring(3, 2);
            }
            else //HH
            {
                strRTN = strTime.Substring(0, 2);
            }

            return strRTN;
        }

        /// <summary>
        /// RRRRMMDDHHMMSS Format의 String을 RRRR-MM-DD HH:MM:SS String으로 변환해서 리턴
        /// </summary>
        /// <param name="strTime">RRRRMMDDHHMMSS Type의 DATE TIME String</param>
        /// <param name="iSeparator">Mask Format Separator
        /// 0 : '-',
        /// 1 : '/',
        /// n : '-'</param>/// 
        /// <returns>RRRR-MM-DD HH:MM:SS</returns>
        public static string ChangeStringFormatToDateTimeString(string strDT, int iSeparator)
        {
            string strSeparator = string.Empty;

            switch (iSeparator)
            {
                case 0:
                    strSeparator = "-";
                    break;
                case 1:
                    strSeparator = "/";
                    break;
                default:
                    strSeparator = "-";
                    break;
            }

            string strRTN = string.Empty;

            strRTN = strDT.Substring(0, 4) + strSeparator + strDT.Substring(4, 2) + strSeparator + strDT.Substring(6, 2);
            strRTN += " " + strDT.Substring(8, 2) + ":" + strDT.Substring(10, 2) + ":" + strDT.Substring(12, 2);
            return strRTN;
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 001을 반환</param>
        /// <returns>string Code</returns>
        public static string SplitToCode(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[strArr.Length - 1].Replace(")","");//.Substring(0, strArr[1].Length - 1);
            }
            else
            {
                if (strArr[0].Trim() == "전체")
                {
                    strCode = " ";
                }
                else
                {
                    strCode = strArr[0].Trim();
                }
            }

            return strCode;
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드명 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 산정정수장을 반환</param>
        /// <returns>string Code Name</returns>
        public static string SplitToCodeName(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[0].Substring(0, strArr[0].Length - 1);
            }
            else
            {
                strCode = strArr[0].Trim();
            }

            return strCode;
        }

        /// <summary>
        /// 해당 문자열이 숫자인지를 확인하는 정규식
        /// Char.IsNumber 메서드 (Char)로 변환 해주세요
        /// </summary>
        /// <param name="strData">문자열</param>
        /// <returns></returns>
        public static bool IsNumeric(string strData)
        {
            Regex isNumber = new Regex(@"^\d+$");
            Match m = isNumber.Match(strData);

            return m.Success;
        }

        /// <summary>
        /// intLen보다 strOrgString의 Length가 작으면, intLen 만큼 Space를 채운다.
        /// </summary>
        /// <param name="strOrgData"> 원본 Data </param>        
        /// <param name="intLen"> Space를 채울 기준 Length </param>
        /// <param name="intLRType"> 0 : strOrgData 의 뒤로 Space를 채움, 1 : strOrgData의 앞으로 Space를 채움 </param>
        /// <returns> string FillSpace</returns>
        public static string FillSpace(string strOrgData, int intLen, int intLRType)
        {
            string strTmp = string.Empty;
            int intOrgLen = 0;

            intOrgLen = strOrgData.Trim().Length;

            if (intLen >= intOrgLen)
            {
                switch (intLRType)
                {
                    case 0:
                        strTmp = strOrgData.PadRight(intLen);
                        break;
                    case 1:
                        strTmp = strOrgData.PadLeft(intLen);
                        break;
                }
            }

            return strTmp;
        }

        /// <summary>
        /// intLen보다 strOrgString의 Length가 작으면, intLen 만큼 숫자 0을 채운다.
        /// </summary>
        /// <param name="strOrgData"> 원본 Data </param>        
        /// <param name="intLen"> Space를 채울 기준 Length </param>
        /// <param name="intLRType"> 0 : strOrgData 의 뒤로 숫자 0을 채움, 1 : strOrgData의 앞으로 숫자 0을 채움 </param>
        /// <returns> string FillZero</returns>
        public static string FillZero(string strOrgData, int intLen, int intLRType)
        {
            string strTmp = string.Empty;
            int intOrgLen = 0;

            intOrgLen = strOrgData.Trim().Length;

            if (intLen >= intOrgLen)
            {
                switch (intLRType)
                {
                    case 0:
                        strTmp = strOrgData.PadRight(intLen, '0');
                        break;

                    case 1:
                        strTmp = strOrgData.PadLeft(intLen, '0');
                        break;
                }
            }

            return strTmp;
        }

        #endregion


        #region 일련번호 생성 및 반환

        /// <summary>
        /// 수질용 일련번호 생성 함수
        /// 기본 길이 15Byte = Key 2Byte + RRRRMMDD 8Byte + 초 2Byte + 밀리초 3Byte
        /// 전체 길이 16Byte = 기본길이 14Byte + 랜덤숫자 1Byte
        /// </summary>
        /// <param name="strKey">일련번호 앞에 들어갈 대표문자 2Byte (영문)</param>
        /// <returns></returns>
        public static String GenerationWQSerialNumber(string strKey)
        {
            Random pRandom = new Random();

            string strRtn = string.Empty;
            string strWQSN = string.Empty;

            strWQSN = StringToDateTime(DateTime.Now)
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0');
            strWQSN += (pRandom.Next(1, 9).ToString()).PadLeft(1, '0');

            strRtn = strKey + strWQSN;

            return strRtn;
        }

        #endregion
    }
}
