﻿namespace WaterNet.WE_EnergyPump
{
    partial class frmPumpRealTimeEnergy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinScrollBar.ScrollBarLook scrollBarLook3 = new Infragistics.Win.UltraWinScrollBar.ScrollBarLook();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.ultraTilePanel1 = new Infragistics.Win.Misc.UltraTilePanel();
            this.cboModel = new System.Windows.Forms.ComboBox();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTilePanel1)).BeginInit();
            this.ultraTilePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.cboModel);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(1122, 42);
            this.panelCommand.TabIndex = 4;
            // 
            // ultraTilePanel1
            // 
            this.ultraTilePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTilePanel1.Location = new System.Drawing.Point(0, 42);
            this.ultraTilePanel1.Name = "ultraTilePanel1";
            this.ultraTilePanel1.NormalModeDimensions = new System.Drawing.Size(0, 0);
            scrollBarLook3.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.Standard;
            this.ultraTilePanel1.ScrollBarLook = scrollBarLook3;
            this.ultraTilePanel1.Size = new System.Drawing.Size(1122, 723);
            this.ultraTilePanel1.TabIndex = 7;
            this.ultraTilePanel1.TileSettings.ShowTileShadow = Infragistics.Win.DefaultableBoolean.True;
            // 
            // cboModel
            // 
            this.cboModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboModel.FormattingEnabled = true;
            this.cboModel.Location = new System.Drawing.Point(12, 12);
            this.cboModel.Name = "cboModel";
            this.cboModel.Size = new System.Drawing.Size(280, 20);
            this.cboModel.TabIndex = 38;
            this.cboModel.Tag = "PP_CAT";
            this.cboModel.SelectedIndexChanged += new System.EventHandler(this.cboModel_SelectedIndexChanged);
            // 
            // frmPumpRealTimeEnergy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 765);
            this.Controls.Add(this.ultraTilePanel1);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmPumpRealTimeEnergy";
            this.Text = "frmPumpRealTimeEnergy";
            this.Load += new System.EventHandler(this.frmPumpRealTimeEnergy_Load);
            this.panelCommand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTilePanel1)).EndInit();
            this.ultraTilePanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private Infragistics.Win.Misc.UltraTilePanel ultraTilePanel1;
        private System.Windows.Forms.ComboBox cboModel;

    }
}