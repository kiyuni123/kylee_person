﻿namespace WaterNet.WE_EnergyPump
{
    partial class frmPumpObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel_Chart = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSel = new System.Windows.Forms.Button();
            this.comboBox_gubun = new System.Windows.Forms.ComboBox();
            this.label_BIZ_PLA_NM = new System.Windows.Forms.Label();
            this.ultraDateTimeEditor_endDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraDateTimeEditor_startDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label_PP_HOGI = new System.Windows.Forms.Label();
            this.panel_PP_Form = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.panel_Chart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_endDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_startDay)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel_Chart);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(516, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(521, 441);
            this.panel2.TabIndex = 1;
            // 
            // panel_Chart
            // 
            this.panel_Chart.Controls.Add(this.chart1);
            this.panel_Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Chart.Location = new System.Drawing.Point(0, 39);
            this.panel_Chart.Name = "panel_Chart";
            this.panel_Chart.Size = new System.Drawing.Size(521, 402);
            this.panel_Chart.TabIndex = 1;
            // 
            // chart1
            // 
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(521, 402);
            this.chart1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnSel);
            this.panel3.Controls.Add(this.comboBox_gubun);
            this.panel3.Controls.Add(this.label_BIZ_PLA_NM);
            this.panel3.Controls.Add(this.ultraDateTimeEditor_endDay);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.ultraDateTimeEditor_startDay);
            this.panel3.Controls.Add(this.label_PP_HOGI);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(521, 39);
            this.panel3.TabIndex = 0;
            // 
            // btnSel
            // 
            this.btnSel.Location = new System.Drawing.Point(294, 7);
            this.btnSel.Name = "btnSel";
            this.btnSel.Size = new System.Drawing.Size(50, 26);
            this.btnSel.TabIndex = 23;
            this.btnSel.Text = "조회";
            this.btnSel.UseVisualStyleBackColor = true;
            this.btnSel.Click += new System.EventHandler(this.btnSel_Click);
            // 
            // comboBox_gubun
            // 
            this.comboBox_gubun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_gubun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_gubun.FormattingEnabled = true;
            this.comboBox_gubun.Location = new System.Drawing.Point(428, 10);
            this.comboBox_gubun.Name = "comboBox_gubun";
            this.comboBox_gubun.Size = new System.Drawing.Size(85, 20);
            this.comboBox_gubun.TabIndex = 22;
            this.comboBox_gubun.SelectedIndexChanged += new System.EventHandler(this.comboBox_gubun_SelectedIndexChanged);
            // 
            // label_BIZ_PLA_NM
            // 
            this.label_BIZ_PLA_NM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_BIZ_PLA_NM.AutoSize = true;
            this.label_BIZ_PLA_NM.Location = new System.Drawing.Point(371, 14);
            this.label_BIZ_PLA_NM.Name = "label_BIZ_PLA_NM";
            this.label_BIZ_PLA_NM.Size = new System.Drawing.Size(53, 12);
            this.label_BIZ_PLA_NM.TabIndex = 21;
            this.label_BIZ_PLA_NM.Text = "성능곡선";
            // 
            // ultraDateTimeEditor_endDay
            // 
            this.ultraDateTimeEditor_endDay.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_endDay.AutoSize = false;
            this.ultraDateTimeEditor_endDay.Location = new System.Drawing.Point(189, 9);
            this.ultraDateTimeEditor_endDay.Name = "ultraDateTimeEditor_endDay";
            this.ultraDateTimeEditor_endDay.Size = new System.Drawing.Size(102, 21);
            this.ultraDateTimeEditor_endDay.TabIndex = 20;
            this.ultraDateTimeEditor_endDay.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor_endDay_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(170, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "~";
            // 
            // ultraDateTimeEditor_startDay
            // 
            this.ultraDateTimeEditor_startDay.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_startDay.AutoSize = false;
            this.ultraDateTimeEditor_startDay.Location = new System.Drawing.Point(63, 9);
            this.ultraDateTimeEditor_startDay.Name = "ultraDateTimeEditor_startDay";
            this.ultraDateTimeEditor_startDay.Size = new System.Drawing.Size(102, 21);
            this.ultraDateTimeEditor_startDay.TabIndex = 18;
            this.ultraDateTimeEditor_startDay.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor_startDay_ValueChanged);
            // 
            // label_PP_HOGI
            // 
            this.label_PP_HOGI.AutoSize = true;
            this.label_PP_HOGI.Location = new System.Drawing.Point(5, 14);
            this.label_PP_HOGI.Name = "label_PP_HOGI";
            this.label_PP_HOGI.Size = new System.Drawing.Size(53, 12);
            this.label_PP_HOGI.TabIndex = 17;
            this.label_PP_HOGI.Text = "조회기간";
            // 
            // panel_PP_Form
            // 
            this.panel_PP_Form.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_PP_Form.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_PP_Form.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_PP_Form.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_PP_Form.Location = new System.Drawing.Point(0, 0);
            this.panel_PP_Form.Name = "panel_PP_Form";
            this.panel_PP_Form.Size = new System.Drawing.Size(516, 441);
            this.panel_PP_Form.TabIndex = 0;
            // 
            // frmPumpObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1037, 441);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel_PP_Form);
            this.MaximizeBox = false;
            this.Name = "frmPumpObject";
            this.ShowInTaskbar = false;
            this.Text = "펌프 효율곡선 조회";
            this.Load += new System.EventHandler(this.frmPumpObject_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPumpObject_FormClosed);
            this.Resize += new System.EventHandler(this.frmPumpObject_Resize);
            this.panel2.ResumeLayout(false);
            this.panel_Chart.ResumeLayout(false);
            this.panel_Chart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_endDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_startDay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Chart;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_endDay;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_startDay;
        private System.Windows.Forms.Label label_PP_HOGI;
        private System.Windows.Forms.ComboBox comboBox_gubun;
        private System.Windows.Forms.Label label_BIZ_PLA_NM;
        private System.Windows.Forms.Button btnSel;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Panel panel_PP_Form;
        private ChartFX.WinForms.Chart chart1;

    }
}