﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

using Infragistics.Win.UltraWinTabControl;
using Infragistics.Win.Misc;

namespace WaterNet.WE_EnergyPump
{
    public partial class frmPumpRealTimeEnergy : Form, WaterNet.WaterNetCore.IForminterface
    {
        public frmPumpRealTimeEnergy()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "실시간 펌프 현황"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            #region ComboBox 설정
            oStringBuilder.AppendLine("SELECT INP_NUMBER AS CD, TITLE || '(' || INP_NUMBER || ')' AS CDNM FROM WH_TITLE ");
            oStringBuilder.AppendLine(" WHERE USE_GBN = 'EN'                                                            ");
            oStringBuilder.AppendLine(" ORDER BY INS_DATE DESC                                                          ");
            WaterNetCore.FormManager.SetComboBoxEX(cboModel, oStringBuilder.ToString(), false);

            #endregion
        }

        public void Open()
        {
            cboModel_SelectedIndexChanged(this, new EventArgs());
        }

        private void frmPumpRealTimeEnergy_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// 에너지 모델 번호 선택
        /// 해당 모델의 펌프정보 화면 ADD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT A.BIZ_NAME, B.PUMP_ID, B.PUMP_NAME    ");
                oStringBuilder.AppendLine("  FROM  WE_BIZPLA A                          ");
                oStringBuilder.AppendLine("       ,WE_PUMPINFO B                        ");
                oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = B.INP_NUMBER           ");
                oStringBuilder.AppendLine("   AND A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
                oStringBuilder.AppendLine("   AND A.MODEL_ID = B.MODEL_ID               ");
                oStringBuilder.AppendLine(" ORDER BY A.ORDERS ASC                       ");

                DataTable pDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                this.ultraTilePanel1.Tiles.Clear();
                this.ultraTilePanel1.BeginUpdate();

                foreach (DataRow item in pDataTable.Rows)
                {
                    frmPumpImageObject oform = new frmPumpImageObject();
                    oform.MODEL_ID = Convert.ToString(cboModel.SelectedValue);
                    oform.BIZ_NAME = Convert.ToString(item["BIZ_NAME"]);
                    oform.PUMP_ID = Convert.ToString(item["PUMP_ID"]);
                    oform.PUMP_NAME = Convert.ToString(item["PUMP_NAME"]);

                    //frmPumpImageObject oform = new frmPumpImageObject(Convert.ToString(item["PUMP_ID"]), Convert.ToString(item["PUMP_NAME"]));
                    //oform.BIZ_PLASEQ = Convert.ToString(comboBox_BIZ_PLA_NM.SelectedValue);
                    UltraTile pUltraTile = new UltraTile();
                    pUltraTile.Name = Convert.ToString(item["PUMP_ID"]);

                    oform.FormBorderStyle = FormBorderStyle.None;
                    oform.TopLevel = false;
                    oform.Dock = DockStyle.Fill;
                    oform.Parent = pUltraTile;
                    oform.Show();
                    pUltraTile.Control = oform;
                    pUltraTile.State = TileState.Normal;
                    pUltraTile.AllowMoving = false;
                    this.ultraTilePanel1.Tiles.Add(pUltraTile);
                }
                this.ultraTilePanel1.EndUpdate();
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }
    }
}
