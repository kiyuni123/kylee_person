﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChartFX.WinForms;

using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_EnergyPump
{
    public partial class frmPumpObject : Form
    {
        //private string m_RunStatusTAG = string.Empty;   //펌프가동 TAG
        private string m_InHeadTAG = string.Empty;      //흡입수두 TAG
        private string m_OutHeadTAG = string.Empty;     //유출수두 TAG
        private string m_FlowTAG = string.Empty;        //유량순시 TAG
        private string m_EnergeTAG = string.Empty;      //전력량 TAG

        private DataTable m_Data = null;

        private WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();

        public frmPumpObject()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region Public Property

        public string InHeadTAG
        {
            set { m_InHeadTAG = value; }
        }

        public string OutHeadTAG
        {
            set { m_OutHeadTAG = value; }
        }

        public string FlowTAG
        {
            set { m_FlowTAG = value; }
        }

        public string EnergeTAG
        {
            set { m_EnergeTAG = value; }
        }

        #endregion Public Property

        #region 초기화설정
        private void InitializeSetting()
        {
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            oDBManager.Open();

            object[,] aoSource = {
                {"0","Q-H"},
                {"1","H-원단위"},
                {"2","H-전력량"},
                {"3","H-효율"}
            };
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_gubun, aoSource, false);

            ultraDateTimeEditor_startDay.Value = DateTime.Today;
            ultraDateTimeEditor_endDay.Value = DateTime.Today.AddDays(1);

            //챠트초기화
            InitializeChart();
        }
        #endregion

        private void InitializeChart()
        {
            this.chart1.LegendBox.MarginY = 1;
            this.chart1.LegendBox.AutoSize = true;
            this.chart1.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chart1.AxisX.DataFormat.CustomFormat = "yyyy-mm-dd hh:MM";
            this.chart1.AxisX.Staggered = true;
            this.chart1.LegendBox.Visible = true;
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
        }

        /// <summary>
        /// 챠트 그리기
        /// </summary>
        private void DrawChartData(string flag)
        {
            this.chart1.Data.Series = 2;
            this.chart1.Data.Points = m_Data.Rows.Count;

            this.chart1.Series[0].Gallery = Gallery.Curve;
            this.chart1.Series[1].Gallery = Gallery.Curve;

            //-------------------------------------------------

            this.chart1.Series[0].AxisY = chart1.AxisY;
            this.chart1.Series[1].AxisY = chart1.AxisY2;

            this.chart1.Series[0].AxisY.Title.Alignment = StringAlignment.Far;
            this.chart1.Series[1].AxisY.Title.Alignment = StringAlignment.Far;

            switch (flag)
            {
                case "0" :  //Q-H
                    this.chart1.Series[0].AxisY.Title.Text = "유량(㎥/h)";
                    this.chart1.Series[1].AxisY.Title.Text = "양정(m)";
                    this.chart1.Series[0].Text = "유량";
                    this.chart1.Series[1].Text = "양정";

                    this.chart1.Series[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[0].AxisY.LabelsFormat.CustomFormat = "###,##0.##";
                    this.chart1.Series[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[1].AxisY.LabelsFormat.CustomFormat = "###,##0.##";

                    this.chart1.Series[1].AxisY.Step = 10;
                    foreach (DataRow row in m_Data.Rows)
                    {
                        int idx = m_Data.Rows.IndexOf(row);
                        DateTime datetime = Convert.ToDateTime(row["TIMESTAMP"]);
                        string strRtn = Convert.ToString(datetime.Year) + "-" + Convert.ToString(datetime.Month).PadLeft(2, '0') + "-" + Convert.ToString(datetime.Day).PadLeft(2, '0');
                        strRtn += " " + Convert.ToString(datetime.Hour).PadLeft(2, '0') + ":" + Convert.ToString(datetime.Minute).PadLeft(2, '0');

                        this.chart1.AxisX.Labels[idx] = strRtn; 

                        this.chart1.Data[0, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_FLOW"]);
                        this.chart1.Data[1, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_H"]);
                    }
                    break;
                case "1":   //H-원단위
                    this.chart1.Series[0].AxisY.Title.Text = "양정(m)";
                    this.chart1.Series[0].Text = "양정";
                    this.chart1.Series[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[1].AxisY.LabelsFormat.CustomFormat = "###,##0.##";

                    this.chart1.Series[1].AxisY.Title.Text = "원단위";
                    this.chart1.Series[1].Text = "원단위";
                    this.chart1.Series[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[1].AxisY.LabelsFormat.CustomFormat = "#,##0.####";
                    this.chart1.Series[1].AxisY.Step = 1;
                    foreach (DataRow row in m_Data.Rows)
                    {
                        int idx = m_Data.Rows.IndexOf(row);
                        DateTime datetime = Convert.ToDateTime(row["TIMESTAMP"]);
                        string strRtn = Convert.ToString(datetime.Year) + "-" + Convert.ToString(datetime.Month).PadLeft(2, '0') + "-" + Convert.ToString(datetime.Day).PadLeft(2, '0');
                        strRtn += " " + Convert.ToString(datetime.Hour).PadLeft(2, '0') + ":" + Convert.ToString(datetime.Minute).PadLeft(2, '0');

                        this.chart1.AxisX.Labels[idx] = strRtn;

                        this.chart1.Data[0, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_H"]);
                        this.chart1.Data[1, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_W"]);
                    }
                    break;
                case "2":   //H-전력량
                    this.chart1.Series[0].AxisY.Title.Text = "양정(m)";
                    this.chart1.Series[1].AxisY.Title.Text = "전력량(kWh)";
                    this.chart1.Series[0].Text = "양정";
                    this.chart1.Series[1].Text = "전력량";
                    this.chart1.Series[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[0].AxisY.LabelsFormat.CustomFormat = "###,##0.##";
                    this.chart1.Series[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[1].AxisY.LabelsFormat.CustomFormat = "#,###,##0";

                    this.chart1.Series[1].AxisY.Step = 10000;
                    foreach (DataRow row in m_Data.Rows)
                    {
                        int idx = m_Data.Rows.IndexOf(row);
                        DateTime datetime = Convert.ToDateTime(row["TIMESTAMP"]);
                        string strRtn = Convert.ToString(datetime.Year) + "-" + Convert.ToString(datetime.Month).PadLeft(2, '0') + "-" + Convert.ToString(datetime.Day).PadLeft(2, '0');
                        strRtn += " " + Convert.ToString(datetime.Hour).PadLeft(2, '0') + ":" + Convert.ToString(datetime.Minute).PadLeft(2, '0');

                        this.chart1.AxisX.Labels[idx] = strRtn;

                        this.chart1.Data[0, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_H"]);
                        this.chart1.Data[1, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_ENERGY"]);
                    }
                    break;
                case "3":   //H-효율
                    this.chart1.Series[0].AxisY.Title.Text = "양정(m)";
                    this.chart1.Series[1].AxisY.Title.Text = "효율(%)";
                    this.chart1.Series[0].Text = "양정";
                    this.chart1.Series[1].Text = "효율";
                    this.chart1.Series[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[0].AxisY.LabelsFormat.CustomFormat = "###,##0.##";
                    this.chart1.Series[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
                    this.chart1.Series[1].AxisY.LabelsFormat.CustomFormat = "##0.##";

                    this.chart1.Series[1].AxisY.Step = 100;
                    foreach (DataRow row in m_Data.Rows)
                    {
                        int idx = m_Data.Rows.IndexOf(row);
                        DateTime datetime = Convert.ToDateTime(row["TIMESTAMP"]);
                        string strRtn = Convert.ToString(datetime.Year) + "-" + Convert.ToString(datetime.Month).PadLeft(2, '0') + "-" + Convert.ToString(datetime.Day).PadLeft(2, '0');
                        strRtn += " " + Convert.ToString(datetime.Hour).PadLeft(2, '0') + ":" + Convert.ToString(datetime.Minute).PadLeft(2, '0');

                        this.chart1.AxisX.Labels[idx] = strRtn;

                        this.chart1.Data[0, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_H"]);
                        this.chart1.Data[1, idx] = WE_Common.WE_FunctionManager.convertToDouble(row["F_E"]);
                    }
                    break;
            }

            this.chart1.Update();

        }

        private void getQueryData()
        {
            string startDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_startDay.Value);
            string endDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_endDay.Value);
            startDay += "000000";
            endDay += "235959";

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT  TIMESTAMP, F_IN, F_OUT, F_FLOW, F_ENERGY                                                   ");
            oStringBuilder.AppendLine("       ,ROUND(F_OUT-F_IN, 2) AS F_H                                                                ");
            oStringBuilder.AppendLine("       ,CASE WHEN F_ENERGY = 0 THEN 0                                                              ");
            oStringBuilder.AppendLine("             ELSE ROUND((9800 * F_FLOW * (F_OUT-F_IN)) / F_ENERGY,2) END AS F_E --효율           ");
            oStringBuilder.AppendLine("       ,CASE WHEN F_ENERGY = 0 THEN 0                                                              ");
            oStringBuilder.AppendLine("             WHEN F_OUT-F_IN = 0 THEN 0                                                       ");
            oStringBuilder.AppendLine("             ELSE ROUND((0.002722 * (((9800 * F_FLOW * (F_OUT-F_IN)) / F_ENERGY) / (F_OUT-F_IN))),3) END AS F_W --원단위  ");
            oStringBuilder.AppendLine("FROM                                                                                               ");
            oStringBuilder.AppendLine("      (SELECT TIMESTAMP                                                                            ");
            oStringBuilder.AppendLine("            ,SUM(DECODE(GBN,'유입압력',V)) AS F_IN                                                 ");
            oStringBuilder.AppendLine("            ,SUM(DECODE(GBN,'유출압력',V)) AS F_OUT                                                ");
            oStringBuilder.AppendLine("            ,SUM(DECODE(GBN,'유량',V)) AS F_FLOW                                                   ");
            oStringBuilder.AppendLine("            ,SUM(DECODE(GBN,'전력량',V)) AS F_ENERGY                                               ");
            oStringBuilder.AppendLine("      FROM                                                                                         ");
            oStringBuilder.AppendLine("       (SELECT TIMESTAMP, TO_NUMBER(VALUE) * 10 AS V, '유입압력' AS GBN                            ");
            oStringBuilder.AppendLine("          FROM IF_GATHER_REALTIME                                                                  ");
            oStringBuilder.AppendLine("         WHERE TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS')                    ");
            oStringBuilder.AppendLine("                             AND TO_DATE('" + endDay + "','YYYYMMDDHH24MISS')                      ");
            oStringBuilder.AppendLine("           AND TAGNAME = '" + m_InHeadTAG + "'"                                                     );
            oStringBuilder.AppendLine("       UNION ALL                                                                                   ");
            oStringBuilder.AppendLine("       SELECT TIMESTAMP, TO_NUMBER(VALUE) * 10 AS V, '유출압력' AS GBN                             ");
            oStringBuilder.AppendLine("         FROM IF_GATHER_REALTIME                                                                   ");
            oStringBuilder.AppendLine("         WHERE TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS')                    ");
            oStringBuilder.AppendLine("                             AND TO_DATE('" + endDay + "','YYYYMMDDHH24MISS')                      ");
            oStringBuilder.AppendLine("           AND TAGNAME = '" + m_OutHeadTAG + "'");
            oStringBuilder.AppendLine("      UNION ALL                                                                                    ");
            oStringBuilder.AppendLine("      SELECT TIMESTAMP, TO_NUMBER(VALUE) AS V, '유량' AS GBN                                       ");
            oStringBuilder.AppendLine("        FROM IF_GATHER_REALTIME                                                                    ");
            oStringBuilder.AppendLine("         WHERE TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS')                    ");
            oStringBuilder.AppendLine("                             AND TO_DATE('" + endDay + "','YYYYMMDDHH24MISS')                      ");
            oStringBuilder.AppendLine("           AND TAGNAME = '" + m_FlowTAG + "'");
            oStringBuilder.AppendLine("      UNION ALL                                                                                    ");
            oStringBuilder.AppendLine("      SELECT TIMESTAMP, TO_NUMBER(VALUE) AS V, '전력량' AS GBN                                     ");
            oStringBuilder.AppendLine("        FROM IF_GATHER_REALTIME                                                                    ");
            oStringBuilder.AppendLine("         WHERE TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS')                    ");
            oStringBuilder.AppendLine("                             AND TO_DATE('" + endDay + "','YYYYMMDDHH24MISS')                      ");
            oStringBuilder.AppendLine("           AND TAGNAME = '" + m_EnergeTAG + "')");
            oStringBuilder.AppendLine("      GROUP BY TIMESTAMP                                                                           ");
            oStringBuilder.AppendLine("      ORDER BY TIMESTAMP ASC                                                                       ");
            oStringBuilder.AppendLine(")                                                                                                  ");

            m_Data = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

        }
        private void btnSel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (((DateTime)ultraDateTimeEditor_endDay.Value) < ((DateTime)ultraDateTimeEditor_startDay.Value))
                {
                    MessageManager.ShowInformationMessage("종료일이 시작일보다 작을수는 없습니다.");
                    ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(1);
                    return;
                }

                if (((DateTime)ultraDateTimeEditor_endDay.Value) > ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(7))
                {
                    MessageManager.ShowInformationMessage("최대 검색기간은 7일 이내입니다.");
                    ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(1);
                    return;
                }

                getQueryData();

                comboBox_gubun_SelectedIndexChanged(this, new EventArgs());
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// 폼 로드 : Addin된 펌프이미지 폼의 사업장 및 시설물(펌프)관리번호를 멤버변수에 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPumpObject_Load(object sender, EventArgs e)
        {
            foreach (Control item in panel_PP_Form.Controls)
            {
                if (item is Form)
                {
                    Form oform = (Form)item;
                    if (oform.Name.Equals("frmPumpImageObject"))
                    {
                        btnSel_Click(this, new EventArgs());
                        return;
                    }
                }
            }
        }

        private void ultraDateTimeEditor_startDay_ValueChanged(object sender, EventArgs e)
        {
            ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(2);
        }

        private void ultraDateTimeEditor_endDay_ValueChanged(object sender, EventArgs e)
        {
            if (((DateTime)ultraDateTimeEditor_endDay.Value) < ((DateTime)ultraDateTimeEditor_startDay.Value))
            {
                MessageManager.ShowInformationMessage("종료일이 시작일보다 작을수는 없습니다.");
                ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(1);
                return;
            }

            if (((DateTime)ultraDateTimeEditor_endDay.Value) > ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(7))
            {
                MessageManager.ShowInformationMessage("최대 검색기간은 7일 이내입니다.");
                ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(1);
                return;
            }
        }

        private void frmPumpObject_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (oDBManager != null) oDBManager.Close();

        }

        private void frmPumpObject_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Button minForm = new Button();
                minForm.Size = new System.Drawing.Size(80, 10);
                minForm.Text = this.Text;
                minForm.Tag = this;
                minForm.Location = new Point(800,800);
                minForm.Visible = true;
                //this.Visible = false;
                
            }
        }

        private void comboBox_gubun_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_gubun.SelectedIndex == -1) return;
            if (m_Data == null) return;
            if (m_Data.Rows.Count == 0) return;
            DrawChartData(Convert.ToString(comboBox_gubun.SelectedValue));
        }
    }
}
