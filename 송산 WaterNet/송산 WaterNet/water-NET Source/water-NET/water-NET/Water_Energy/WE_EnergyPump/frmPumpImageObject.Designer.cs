﻿namespace WaterNet.WE_EnergyPump
{
    partial class frmPumpImageObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPumpImageObject));
            this.panel_Pump = new System.Windows.Forms.Panel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblBIZ_NAME = new System.Windows.Forms.Label();
            this.lbl_Eff = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblPUMP_NAME = new System.Windows.Forms.Label();
            this.lbl_Head = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbl_Out_Head = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_Flow = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_In_Head = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_Time = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_Won = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_Energy = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel_Pump.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Pump
            // 
            this.panel_Pump.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel_Pump.BackgroundImage")));
            this.panel_Pump.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_Pump.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Pump.Controls.Add(this.lblStatus);
            this.panel_Pump.Controls.Add(this.lblBIZ_NAME);
            this.panel_Pump.Controls.Add(this.lbl_Eff);
            this.panel_Pump.Controls.Add(this.label15);
            this.panel_Pump.Controls.Add(this.lblPUMP_NAME);
            this.panel_Pump.Controls.Add(this.lbl_Head);
            this.panel_Pump.Controls.Add(this.label14);
            this.panel_Pump.Controls.Add(this.lbl_Out_Head);
            this.panel_Pump.Controls.Add(this.label12);
            this.panel_Pump.Controls.Add(this.lbl_Flow);
            this.panel_Pump.Controls.Add(this.label10);
            this.panel_Pump.Controls.Add(this.lbl_In_Head);
            this.panel_Pump.Controls.Add(this.label8);
            this.panel_Pump.Controls.Add(this.lbl_Time);
            this.panel_Pump.Controls.Add(this.label6);
            this.panel_Pump.Controls.Add(this.lbl_Won);
            this.panel_Pump.Controls.Add(this.label4);
            this.panel_Pump.Controls.Add(this.lbl_Energy);
            this.panel_Pump.Controls.Add(this.label1);
            this.panel_Pump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Pump.Location = new System.Drawing.Point(0, 0);
            this.panel_Pump.Name = "panel_Pump";
            this.panel_Pump.Size = new System.Drawing.Size(575, 422);
            this.panel_Pump.TabIndex = 1;
            this.panel_Pump.DoubleClick += new System.EventHandler(this.panel1_DoubleClick);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblStatus.AutoEllipsis = true;
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Location = new System.Drawing.Point(245, 7);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(69, 16);
            this.lblStatus.TabIndex = 30;
            this.lblStatus.Text = "펌프 #1";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBIZ_NAME
            // 
            this.lblBIZ_NAME.AutoEllipsis = true;
            this.lblBIZ_NAME.AutoSize = true;
            this.lblBIZ_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblBIZ_NAME.Font = new System.Drawing.Font("굴림", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBIZ_NAME.ForeColor = System.Drawing.Color.Black;
            this.lblBIZ_NAME.Location = new System.Drawing.Point(4, 7);
            this.lblBIZ_NAME.Name = "lblBIZ_NAME";
            this.lblBIZ_NAME.Size = new System.Drawing.Size(63, 15);
            this.lblBIZ_NAME.TabIndex = 29;
            this.lblBIZ_NAME.Text = "펌프 #1";
            this.lblBIZ_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Eff
            // 
            this.lbl_Eff.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_Eff.AutoEllipsis = true;
            this.lbl_Eff.BackColor = System.Drawing.Color.White;
            this.lbl_Eff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Eff.ForeColor = System.Drawing.Color.Black;
            this.lbl_Eff.Location = new System.Drawing.Point(170, 373);
            this.lbl_Eff.Name = "lbl_Eff";
            this.lbl_Eff.Size = new System.Drawing.Size(73, 22);
            this.lbl_Eff.TabIndex = 28;
            this.lbl_Eff.Tag = "PP_E";
            this.lbl_Eff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label15.AutoEllipsis = true;
            this.label15.BackColor = System.Drawing.Color.LightGray;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(170, 356);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 18);
            this.label15.TabIndex = 27;
            this.label15.Text = "효율";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPUMP_NAME
            // 
            this.lblPUMP_NAME.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPUMP_NAME.AutoEllipsis = true;
            this.lblPUMP_NAME.AutoSize = true;
            this.lblPUMP_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblPUMP_NAME.Font = new System.Drawing.Font("굴림", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPUMP_NAME.ForeColor = System.Drawing.Color.Black;
            this.lblPUMP_NAME.Location = new System.Drawing.Point(424, 7);
            this.lblPUMP_NAME.Name = "lblPUMP_NAME";
            this.lblPUMP_NAME.Size = new System.Drawing.Size(63, 15);
            this.lblPUMP_NAME.TabIndex = 22;
            this.lblPUMP_NAME.Text = "펌프 #1";
            this.lblPUMP_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_Head
            // 
            this.lbl_Head.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_Head.AutoEllipsis = true;
            this.lbl_Head.BackColor = System.Drawing.Color.White;
            this.lbl_Head.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Head.ForeColor = System.Drawing.Color.Black;
            this.lbl_Head.Location = new System.Drawing.Point(258, 187);
            this.lbl_Head.Name = "lbl_Head";
            this.lbl_Head.Size = new System.Drawing.Size(73, 22);
            this.lbl_Head.TabIndex = 13;
            this.lbl_Head.Tag = "PP_H";
            this.lbl_Head.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoEllipsis = true;
            this.label14.BackColor = System.Drawing.Color.LightGray;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(258, 170);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 18);
            this.label14.TabIndex = 12;
            this.label14.Text = "전양정";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Out_Head
            // 
            this.lbl_Out_Head.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Out_Head.AutoEllipsis = true;
            this.lbl_Out_Head.BackColor = System.Drawing.Color.White;
            this.lbl_Out_Head.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Out_Head.ForeColor = System.Drawing.Color.Black;
            this.lbl_Out_Head.Location = new System.Drawing.Point(490, 324);
            this.lbl_Out_Head.Name = "lbl_Out_Head";
            this.lbl_Out_Head.Size = new System.Drawing.Size(73, 22);
            this.lbl_Out_Head.TabIndex = 11;
            this.lbl_Out_Head.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoEllipsis = true;
            this.label12.BackColor = System.Drawing.Color.LightGray;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(490, 307);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 18);
            this.label12.TabIndex = 10;
            this.label12.Text = "유출수두";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Flow
            // 
            this.lbl_Flow.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_Flow.AutoEllipsis = true;
            this.lbl_Flow.BackColor = System.Drawing.Color.White;
            this.lbl_Flow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Flow.ForeColor = System.Drawing.Color.Black;
            this.lbl_Flow.Location = new System.Drawing.Point(249, 324);
            this.lbl_Flow.Name = "lbl_Flow";
            this.lbl_Flow.Size = new System.Drawing.Size(73, 22);
            this.lbl_Flow.TabIndex = 9;
            this.lbl_Flow.Tag = "PP_Q";
            this.lbl_Flow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label10.AutoEllipsis = true;
            this.label10.BackColor = System.Drawing.Color.LightGray;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(249, 307);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 18);
            this.label10.TabIndex = 8;
            this.label10.Text = "유량";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_In_Head
            // 
            this.lbl_In_Head.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_In_Head.AutoEllipsis = true;
            this.lbl_In_Head.BackColor = System.Drawing.Color.White;
            this.lbl_In_Head.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_In_Head.ForeColor = System.Drawing.Color.Black;
            this.lbl_In_Head.Location = new System.Drawing.Point(7, 324);
            this.lbl_In_Head.Name = "lbl_In_Head";
            this.lbl_In_Head.Size = new System.Drawing.Size(73, 22);
            this.lbl_In_Head.TabIndex = 7;
            this.lbl_In_Head.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.LightGray;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(7, 307);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 18);
            this.label8.TabIndex = 6;
            this.label8.Text = "흡입수두";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Time
            // 
            this.lbl_Time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Time.AutoEllipsis = true;
            this.lbl_Time.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Time.ForeColor = System.Drawing.Color.Black;
            this.lbl_Time.Location = new System.Drawing.Point(494, 395);
            this.lbl_Time.Name = "lbl_Time";
            this.lbl_Time.Size = new System.Drawing.Size(73, 22);
            this.lbl_Time.TabIndex = 5;
            this.lbl_Time.Tag = "HM";
            this.lbl_Time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoEllipsis = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(494, 378);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 18);
            this.label6.TabIndex = 4;
            this.label6.Text = "Update";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Won
            // 
            this.lbl_Won.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_Won.AutoEllipsis = true;
            this.lbl_Won.BackColor = System.Drawing.Color.White;
            this.lbl_Won.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Won.ForeColor = System.Drawing.Color.Black;
            this.lbl_Won.Location = new System.Drawing.Point(249, 373);
            this.lbl_Won.Name = "lbl_Won";
            this.lbl_Won.Size = new System.Drawing.Size(73, 22);
            this.lbl_Won.TabIndex = 3;
            this.lbl_Won.Tag = "PP_U";
            this.lbl_Won.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.LightGray;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(249, 356);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "원단위";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Energy
            // 
            this.lbl_Energy.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_Energy.AutoEllipsis = true;
            this.lbl_Energy.BackColor = System.Drawing.Color.White;
            this.lbl_Energy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Energy.ForeColor = System.Drawing.Color.Black;
            this.lbl_Energy.Location = new System.Drawing.Point(327, 373);
            this.lbl_Energy.Name = "lbl_Energy";
            this.lbl_Energy.Size = new System.Drawing.Size(73, 22);
            this.lbl_Energy.TabIndex = 1;
            this.lbl_Energy.Tag = "PP_W";
            this.lbl_Energy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.LightGray;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(327, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "전력량";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmPumpImageObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 422);
            this.Controls.Add(this.panel_Pump);
            this.Name = "frmPumpImageObject";
            this.Text = "frmPumpImageObject";
            this.Load += new System.EventHandler(this.frmPumpImageObject_Load);
            this.panel_Pump.ResumeLayout(false);
            this.panel_Pump.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Pump;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Energy;
        private System.Windows.Forms.Label lbl_Time;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_Won;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_Head;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbl_Out_Head;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_Flow;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_In_Head;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPUMP_NAME;
        private System.Windows.Forms.Label lbl_Eff;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblBIZ_NAME;
        private System.Windows.Forms.Label lblStatus;
    }
}