﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

namespace WaterNet.WE_EnergyPump
{
    public partial class frmPumpImageObject : Form
    {
        
        ///////////////////////////////////////////////////////////////////
        private string m_MODEL_ID = string.Empty;       //에너지 모델 번호
        private string m_PUMP_ID = string.Empty;        //펌프 ID
        private string m_PUMP_NAME = string.Empty;      //펌프 명
        private string m_BIZ_NAME = string.Empty;       //사업장 명

        private string m_RunStatusTAG = string.Empty;   //펌프가동 TAG
        private string m_InHeadTAG = string.Empty;      //흡입수두 TAG
        private string m_OutHeadTAG = string.Empty;     //유출수두 TAG
        private string m_FlowTAG = string.Empty;        //유량순시 TAG
        private string m_EnergeTAG = string.Empty;      //전력량 TAG

        public frmPumpImageObject()
        {
            InitializeComponent();
        }

        #region Public Property

        public string MODEL_ID
        {
            set { m_MODEL_ID = value; }
        }

        public string PUMP_ID
        {
            set { m_PUMP_ID = value; }
        }

        public string PUMP_NAME
        {
            set { m_PUMP_NAME = value; }
        }

        public string BIZ_NAME
        {
            set { m_BIZ_NAME = value; }
        }
        
        #endregion Public Property

        private void InitializeSetting()
        {
            lblPUMP_NAME.Text = m_PUMP_NAME;
            lblBIZ_NAME.Text = m_BIZ_NAME;

            if (EMFrame.statics.AppStatic.USER_SGCCD.Equals("45180")) ///정읍
            {
                switch (m_PUMP_ID.ToUpper())
                {
                    case "SD1":   //상동 1호기
                        m_RunStatusTAG = "JEUPSC.401-501-PMB-8103.F_CV";
                        m_InHeadTAG = "JEUPSC.401-501-PRI-8001.F_CV";
                        m_OutHeadTAG = "JEUPSC.401-501-PRI-8002.F_CV";
                        m_FlowTAG = "JEUPSC.401-501-FRI-8002.F_CV";
                        m_EnergeTAG = "JEUPSC.401-501-PWI-8006.F_CV";
                        break;
                    case "SD2":    //상동 2호기
                        m_RunStatusTAG = "JEUPSC.401-501-PMB-8107.F_CV";
                        m_InHeadTAG = "JEUPSC.401-501-PRI-8001.F_CV";
                        m_OutHeadTAG = "JEUPSC.401-501-PRI-8002.F_CV";
                        m_FlowTAG = "JEUPSC.401-501-FRI-8002.F_CV";
                        m_EnergeTAG = "JEUPSC.401-501-PWI-8006.F_CV";
                        break;
                    case "SD3":    //상동 3호기
                        m_RunStatusTAG = "JEUPSC.401-501-PMB-8111.F_CV";
                        m_InHeadTAG = "JEUPSC.401-501-PRI-8001.F_CV";
                        m_OutHeadTAG = "JEUPSC.401-501-PRI-8002.F_CV";
                        m_FlowTAG = "JEUPSC.401-501-FRI-8002.F_CV";
                        m_EnergeTAG = "JEUPSC.401-501-PWI-8006.F_CV";
                        break;
                    case "HR1":    //회룡 1호기
                        m_RunStatusTAG = "JEUPSC.401-501-PMB-8103.F_CV";
                        m_InHeadTAG = "JEUPSC.401-502-PRI-8001.F_CV";
                        m_OutHeadTAG = "JEUPSC.401-502-PRI-8002.F_CV";
                        m_FlowTAG = "JEUPSC.401-502-FRI-8002.F_CV";
                        m_EnergeTAG = "JEUPSC.401-502-PWI-8006.F_CV";
                        break;
                    case "HR2":    //회룡 2호기
                        m_RunStatusTAG = "JEUPSC.401-501-PMB-8107.F_CV";
                        m_InHeadTAG = "JEUPSC.401-502-PRI-8003.F_CV";
                        m_OutHeadTAG = "JEUPSC.401-502-PRI-8004.F_CV";
                        m_FlowTAG = "JEUPSC.401-502-FRI-8002.F_CV";
                        m_EnergeTAG = "JEUPSC.401-502-PWI-8006.F_CV";
                        break;
                }                
            }
            else if (EMFrame.statics.AppStatic.USER_SGCCD.Equals("48310")) ///거제
            {

            }
        }

        /// <summary>
        /// 펌프 데이터 쿼리
        /// </summary>
        private void Execute_QuerySelect()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            //////////////////////////////펌프 관련 데이터 값 가져오기
            oStringBuilder.AppendLine("SELECT  TO_CHAR(A.TIMESTAMP , 'HH24:MI:SS') AS TIMESTAMP                ");
            oStringBuilder.AppendLine("       ,A.TAGNAME                                                             ");
            oStringBuilder.AppendLine("       ,A.VALUE                                                               ");
            oStringBuilder.AppendLine("  FROM IF_GATHER_REALTIME A                                                   ");
            oStringBuilder.AppendLine("       ,(SELECT /*+ INDEX_DESC(IF_GATHER_REALTIME IF_GATHER_REALTIME_PK) */   ");
            oStringBuilder.AppendLine("               TIMESTAMP                                                      ");
            oStringBuilder.AppendLine("          FROM IF_GATHER_REALTIME                                             ");
            oStringBuilder.AppendLine("         WHERE TAGNAME = '" + m_RunStatusTAG + "'"); //JEUPSC.401-501-PMB-8103.F_CV'  --가동
            oStringBuilder.AppendLine("           AND ROWNUM = 1) STATUS                                             ");
            oStringBuilder.AppendLine("WHERE A.TAGNAME IN ('" + m_RunStatusTAG + "'");      //--가동                     ");
            oStringBuilder.AppendLine("                  ,'" + m_EnergeTAG + "'");          //JEUPSC.401-501-PWI-8006.F_CV'  --무효전력량               ");
            oStringBuilder.AppendLine("                  ,'" + m_InHeadTAG + "'");          //JEUPSC.401-501-PRI-8001.F_CV'  --흡입압력                 ");
            oStringBuilder.AppendLine("                  ,'" + m_FlowTAG + "'");            //JEUPSC.401-501-FRI-8002.F_CV'  --유출유량 순시            ");
            oStringBuilder.AppendLine("                  ,'" + m_OutHeadTAG + "')");        //JEUPSC.401-501-PRI-8002.F_CV') --토출압력                 ");
            oStringBuilder.AppendLine("   AND A.TIMESTAMP = STATUS.TIMESTAMP                                         ");

#if DEBUG
    Console.WriteLine(m_PUMP_ID + " : " + oStringBuilder.ToString());            
#endif
            

            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            try
            {
                oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                oDBManager.Open();

                DataTable datatable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                ///가동여부 체크
                DataRow[] rows = datatable.Select("TAGNAME = '" + m_RunStatusTAG + "'");
                if (rows.Length == 0)
                {
                    //MessageBox.Show("계측(펌프가동) 데이터 없음");
                    return;
                }

                Boolean RunStatus = false;
                RunStatus = Convert.ToInt32(rows[0]["VALUE"]) == 1 ? true : false;

                ///계측 시간
                lbl_Time.Text = Convert.ToString(datatable.Rows[0]["TIMESTAMP"]);

                if (!RunStatus) //미가동
                {
                    //panel_Pump.BackColor = Color.FromArgb(224, 230, 243);
                    lblStatus.ForeColor = Color.Gray; lblStatus.Text = "정지";
                    lblStatus.BackColor = Color.Transparent;
                    lbl_Time.BackColor = Color.FromArgb(220, 220, 220);

                    lbl_Energy.Text = "0";
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (Convert.ToString(row["TAGNAME"]).Equals(m_InHeadTAG))  //흡입압력
                        {
                            lbl_In_Head.Text = Convert.ToString(Convert.ToDouble(row["VALUE"]) * 10);
                        }
                        else if (Convert.ToString(row["TAGNAME"]).Equals(m_OutHeadTAG))  //토출압력
                        {
                            lbl_Out_Head.Text = Convert.ToString(Convert.ToDouble(row["VALUE"]) * 10);
                        }
                    }
                    lbl_Flow.Text = "0";
                    lbl_Head.Text = "0";
                    lbl_Eff.Text = "0";
                    lbl_Won.Text = "0";
                }
                else
                {
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (Convert.ToString(row["TAGNAME"]).Equals(m_RunStatusTAG))  //가동
                        {
                            //panel_Pump.BackColor = Color.FromArgb(74, 168, 213);
                            lblStatus.ForeColor = Color.Red; lblStatus.Text = "가동중";
                            lblStatus.BackColor = Color.Transparent;
                            lbl_Time.BackColor = Color.FromArgb(255, 165, 0);
                        }
                        else if (Convert.ToString(row["TAGNAME"]).Equals(m_EnergeTAG))  //전력량(에너지)
                        {
                            lbl_Energy.Text = Convert.ToString(row["VALUE"]);
                        }
                        else if (Convert.ToString(row["TAGNAME"]).Equals(m_InHeadTAG))  //흡입압력
                        {
                            lbl_In_Head.Text = Convert.ToString(Convert.ToDouble(row["VALUE"]) * 10);
                        }
                        else if (Convert.ToString(row["TAGNAME"]).Equals(m_OutHeadTAG))  //토출압력
                        {
                            lbl_Out_Head.Text = Convert.ToString(Convert.ToDouble(row["VALUE"]) * 10);
                        }
                        else if (Convert.ToString(row["TAGNAME"]).Equals(m_FlowTAG))    //순시유량
                        {
                            lbl_Flow.Text = Convert.ToString(row["VALUE"]);
                        }
                    }

                    ///계산에 의한 데이터 설정
                    double temp1 = 0.0; double temp2 = 0.0; double temp3 = 0.0; double temp4 = 0.0;
                    /// 전양정 = 유출수두 - 흡입수두
                    temp1 = string.IsNullOrEmpty(lbl_In_Head.Text) ? 0 : Convert.ToDouble(lbl_In_Head.Text);   //흡입수두
                    temp2 = string.IsNullOrEmpty(lbl_Out_Head.Text) ? 0 : Convert.ToDouble(lbl_Out_Head.Text); //유출수두
                    lbl_Head.Text = Convert.ToString(temp2 - temp1);  //전양정

                    /// 효율 = (9800 * 유량 * (전양정)) / 전력량
                    temp1 = string.IsNullOrEmpty(lbl_Flow.Text) ? 0 : Convert.ToDouble(lbl_Flow.Text); //유량
                    temp2 = string.IsNullOrEmpty(lbl_Head.Text) ? 0 : Convert.ToDouble(lbl_Head.Text); //전양정
                    temp3 = string.IsNullOrEmpty(lbl_Energy.Text) ? 0 : Convert.ToDouble(lbl_Energy.Text); //전력량

                    lbl_Eff.Text = Convert.ToString((9800 * temp1 * temp2 * temp3) == 0 ? 0 : (9800 * temp1 * temp2) / temp3);  //효율

                    /// 원단위 = (0.002722 * (효율 / 전양정))
                    temp1 = string.IsNullOrEmpty(lbl_Eff.Text) ? 0 : Convert.ToDouble(lbl_Eff.Text); //효율
                    temp2 = string.IsNullOrEmpty(lbl_Head.Text) ? 0 : Convert.ToDouble(lbl_Head.Text); //전양정

                    lbl_Won.Text = Convert.ToString((temp1 * temp2) == 0 ? 0 : (0.002722 * (temp1 / temp2)));  //원단위
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
          
        }

        /// <summary>
        /// 상세화면 보기 : 이미지 및 챠트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel1_DoubleClick(object sender, EventArgs e)
        {
            //모달리스에서 실행시 return
            if (!(this.ParentForm is IForminterface)) return;
            
            frmPumpObject oform = new frmPumpObject();
            oform.WindowState = FormWindowState.Normal;
            oform.StartPosition = FormStartPosition.CenterParent;
            oform.InHeadTAG = m_InHeadTAG;
            oform.OutHeadTAG = m_OutHeadTAG;
            oform.FlowTAG = m_FlowTAG;
            oform.EnergeTAG = m_EnergeTAG;

            //Child Form Control
            Form objectControl = new frmPumpImageObject();
            ((frmPumpImageObject)objectControl).m_MODEL_ID = m_MODEL_ID;
            ((frmPumpImageObject)objectControl).m_PUMP_ID = m_PUMP_ID;
            ((frmPumpImageObject)objectControl).m_PUMP_NAME = m_PUMP_NAME;
            ((frmPumpImageObject)objectControl).m_BIZ_NAME = m_BIZ_NAME;

            objectControl.FormBorderStyle = FormBorderStyle.None;
            objectControl.TopLevel = false;
            objectControl.Dock = DockStyle.Fill;
            objectControl.Parent = ((frmPumpObject)oform).panel_PP_Form;
            objectControl.Show();
            ((frmPumpObject)oform).panel_PP_Form.Controls.Add(objectControl);

            oform.ShowDialog();
        }

        private void frmPumpImageObject_Load(object sender, EventArgs e)
        {
            InitializeSetting();

            lbl_Head.TextChanged += new EventHandler(LabelInsertComma);
            lbl_In_Head.TextChanged += new EventHandler(LabelInsertComma);
            lbl_Out_Head.TextChanged += new EventHandler(LabelInsertComma);
            lbl_Flow.TextChanged += new EventHandler(LabelInsertComma);
            lbl_Eff.TextChanged += new EventHandler(LabelInsertComma);
            lbl_Won.TextChanged += new EventHandler(LabelInsertComma);
            lbl_Energy.TextChanged += new EventHandler(LabelInsertComma);

            timer1.Start();
            Execute_QuerySelect();
        }

        #region Timer
        private void timer1_Tick(object sender, EventArgs e)
        {
            Execute_QuerySelect();
        }
        #endregion

        /// <summary>
        /// 텍스트박스에 ',' 추가 하기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelInsertComma(object sender, EventArgs e)
        {
            Label tbTemp = (Label)sender;
            string strTemp = tbTemp.Text;
            if (strTemp.Length <= 0)
                return;
            strTemp = strTemp.Replace(",", "");
            double dTemp = 0;
            try
            {
                dTemp = double.Parse(strTemp);
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            strTemp = string.Format("{0:#,0.##}", dTemp);
            tbTemp .Text = strTemp;

        }

    }
}
