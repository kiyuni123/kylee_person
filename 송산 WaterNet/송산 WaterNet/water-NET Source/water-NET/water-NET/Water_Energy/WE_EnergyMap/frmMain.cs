﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

namespace WE_EnergyMap
{
    public partial class frmMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        private IMapControl3 imapControl3 = null;

        private string[] m_shapes = { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE" };

        public frmMain()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "에너지관리"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        /// <summary>
        /// Open
        /// </summary>
        public override void Open()
        {
            base.Open();
            this.panelCommand.Visible = false;
            //추가적인 구현은 여기에....
            InitializeMap();

            Open_ModelLayer();

            this.imapControl3 = (IMapControl3)axMap.Object;
        }

        /// <summary>
        /// 지도상에 레이어를 표시하지 않도록 설정
        /// </summary>
        private void InitializeMap()
        {
            IEnumLayer pEnumLayer = this.axMap.ActiveView.FocusMap.get_Layers(null, false);
            pEnumLayer.Reset();

            ILayer pLayer = (ILayer)pEnumLayer.Next();

            do
            {
                if (pLayer is IFeatureLayer)
                {
                    switch (pLayer.Name)
                    {
                        case "재염소처리지점":
                        case "중요시설":
                        case "감시지점":
                        case "누수지점":
                        case "민원지점":
                        case "관세척구간":
                        //case "밸브":
                        //case "유량계":
                        case "수압계":
                        case "소방시설":
                        case "스탠드파이프":
                        case "수도계량기":
                        case "표고점":
                        //case "급수관로":
                        //case "상수관로":
                        //case "정수장":
                        //case "가압장":
                        //case "취수장":
                        //case "수원지":
                        //case "배수지":
                        case "밸브실":
                        //case "철도":
                        case "도로경계선":
                        case "등고선":
                        case "담장":
                        case "법정읍면동":
                        //case "행정읍면동":
                        case "건물":
                        case "하천":
                        case "지방도":
                        case "국도":
                        case "고속도로":
                        case "지형지번":
                            //case "소블록":
                            //case "중블록":
                            //case "대블록":
                            pLayer.Visible = false;
                            break;
                        default:
                            break;
                    }
                }
                pLayer = pEnumLayer.Next();
            }
            while (pLayer != null);

        }


        /// <summary>
        /// 기존의 INP 레이어를 지도화면에서 삭제한다.
        /// </summary>
        private void Remove_INP_Layer()
        {
            for (int i = 0; i < this.m_shapes.Length; i++)
            {
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, (string)this.m_shapes.GetValue(i));
            }
        }

        /// <summary>
        /// INP Shape을 지도화면에 로드한다.
        /// </summary>
        private void Load_INP_Layer(IWorkspace pWorkspace)
        {
            if (pWorkspace == null) return;

            ILayer pLayer = null; IColor pColor = null; ISymbol pLineSymbol = null;
            ISymbol pMarkSymbol = null;
            for (int i = 0; i < this.m_shapes.Length; i++)
            {
                pLayer = ArcManager.GetShapeLayer(pWorkspace, (string)this.m_shapes.GetValue(i), (string)this.m_shapes.GetValue(i));
                if (pLayer == null) continue;

                ArcManager.SetDefaultRenderer2(axMap.ActiveView.FocusMap, (IFeatureLayer)pLayer);
                axMap.AddLayer(pLayer);
            }
        }

        /// <summary>
        /// 해석모델 선택버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_ModelLayer()
        {
            WaterNet.WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            oDBManager.Open();
            object o = oDBManager.ExecuteScriptScalar("SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y'", new IDataParameter[] {});
            if (o == null) return;

            //해석모델 선택화면
            try
            {
                //기존의 Shape을 Map에서 Unload
                Remove_INP_Layer();

                //해당 모델의 Workspace가 없을경우
                IWorkspace pWorkspace = WaterNet.WaterAOCore.ArcManager.getShapeWorkspace(WaterNet.WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + (string)o);
                if (pWorkspace == null)
                {
                    //해당 모델의 Shape를 생성
                    CreateINPLayerManager oInpLayerManager = new CreateINPLayerManager((string)o);
                    try
                    {
                        pWorkspace = oInpLayerManager.Worksapce;
                        oInpLayerManager.StartEditor();
                        oInpLayerManager.CreateINP_Shape();
                        oInpLayerManager.StopEditor(true);
                    }
                    catch (Exception)
                    {
                        oInpLayerManager.AbortEditor();
                        oInpLayerManager.StopEditor(false);
                    }
                }

                //해석모델((INP_NUMBER)번호에 해당하는 Shape로드
                Load_INP_Layer(pWorkspace);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewBackground);
            }
        }

        /// <summary>
        /// 레이어에서 특정 퓨처를 찾아 포커스를 이동시켜 준다. 
        /// </summary>
        /// <param name="layerName">레이어명</param>
        /// <param name="whereCase">조건</param>
        public void MoveFocusAt(string layerName, string whereCase)
        {
            this.MoveFocusAt(layerName, whereCase, 10000);
        }

        /// <summary>
        /// 레이어에서 특정 퓨처를 찾아 포커스를 이동시켜 준다. 
        /// </summary>
        /// <param name="layerName">레이어명</param>
        /// <param name="whereCase">조건</param>
        public void MoveFocusAt(string layerName, string whereCase, int scale)
        {
            ILayer layer = ArcManager.GetMapLayer(imapControl3, layerName);

            if (layer != null)
            {
                ESRI.ArcGIS.Geodatabase.IFeature feature = ArcManager.GetFeature(layer, whereCase);

                if (feature != null)
                {
                    IRelationalOperator pRelOp = feature.Shape as IRelationalOperator;
                    if (!pRelOp.Within(this.imapControl3.ActiveView.Extent.Envelope as IGeometry))
                    {
                        ArcManager.MoveCenterAt(this.imapControl3, feature);
                        Application.DoEvents();
                    }

                    ArcManager.FlashShape(this.imapControl3.ActiveView, (IGeometry)feature.Shape, 8, 100);
                }
                if (feature == null)
                {
                    MessageBox.Show("GIS에 선택한 객체가 존재하지 않습니다.");
                }
            }
        }
    }
}
