﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.control;
using WaterNet.WE_PumpMasterManage.work;
using WaterNet.WE_PumpPerformanceCurveManage.work;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_PumpMasterManage.form
{
    public partial class frmMain : Form
    {
        //MainMap 을 제어하기 위한 인스턴스 변수
        //private IMapControl mainMap = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        //public IMapControl MainMap
        //{
        //    set
        //    {
        //        this.mainMap = value;
        //    }
        //}

        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();
            DataTable station = new DataTable();
            station.Columns.Add("CODE");
            station.Columns.Add("CODE_NAME");
            station.Rows.Add(null, "전체");

            foreach (DataRow row in table.Rows)
            {
                station.Rows.Add(row["CODE"], row["CODE_NAME"].ToString());
            }

            this.bzs.ValueMember = "CODE";
            this.bzs.DisplayMember = "CODE_NAME";
            this.bzs.DataSource = station;
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            //그리드1
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SELECTED";
            column.Header.Caption = "";
            column.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            column.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.None;
            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;

            column.SortIndicator = SortIndicator.Disabled;
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 30;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "펌프설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 130;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_GBN";
            column.Header.Caption = "펌프구분";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_RPM";
            column.Header.Caption = "회전수(rpm)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SIM_GBN";
            column.Header.Caption = "전력계약종별";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "성능진단일자";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Format = "yyyy-MM-dd";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.Default;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MODEL_YN";
            column.Header.Caption = "모델반영여부";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.Default;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = true;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DIFF";
            column.Header.Caption = "운영효율범위(%)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 110;
            column.Hidden = false;

            //그리드2
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "E";
            column.Header.Caption = "효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "POINT_YN";
            column.Header.Caption = "정격점";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.CellAppearance.FontData.Bold = DefaultableBoolean.True;
            column.CellAppearance.ForeColor = Color.Red;

            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //사업장
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMPSTATION_ID"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMPSTATION_ID");

                DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();

                foreach (DataRow row in table.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMPSTATION_ID"];
            }

            //펌프구분
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMP_GBN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMP_GBN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "PKP");
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMP_GBN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMP_GBN"];
            }

            //전력계약종별
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SIM_GBN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SIM_GBN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "6201");
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SIM_GBN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["SIM_GBN"];
            }

            //모델반영여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MODEL_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MODEL_YN");
                valueList.ValueListItems.Add("Y", "예");
                valueList.ValueListItems.Add("N", "아니요");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["MODEL_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["MODEL_YN"];
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);

            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterHeaderCheckStateChanged += new AfterHeaderCheckStateChangedEventHandler(ultraGrid1_AfterHeaderCheckStateChanged);

            this.ultraGrid1.ClickCellButton += new CellEventHandler(ultraGrid1_ClickCellButton);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid1.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid1_DoubleClickRow);
        }

        //성능곡선 변경시에 모델에 성능곡선값을 변경해준다.
        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "CURVE_ID")
            {
                DialogResult qe = MessageBox.Show("모델의 성능곡선을 수정하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    Hashtable parameter = new Hashtable();
                    parameter["PUMP_FTR_IDN"] = e.Cell.Row.Cells["PUMP_FTR_IDN"].Value;
                    parameter["CURVE_ID"] = e.Cell.Row.Cells["CURVE_ID"].Value;

                    PumpMasterWork.GetInstance().UpdateModelCurve(parameter);
                    e.Cell.Row.Cells["MODEL_YN"].Value = "Y";
                    PumpMasterWork.GetInstance().UpdatePumpMaster_MODEL_YN(parameter);
                }
                else
                {
                    Hashtable parameter = new Hashtable();
                    parameter["PUMP_FTR_IDN"] = e.Cell.Row.Cells["PUMP_FTR_IDN"].Value;
                    e.Cell.Row.Cells["MODEL_YN"].Value = "N";
                    PumpMasterWork.GetInstance().UpdatePumpMaster_MODEL_YN(parameter);
                }
            }
        }

        private void ultraGrid1_ClickCellButton(object sender, CellEventArgs e)
        {
            //클릭하면 성능 선택....
            //해당 FTR_IDN으로 펌프 조회.....
            frmCurve curve = new frmCurve(e.Cell);
            curve.Owner = this;
            curve.Show(this);
        }

        private Hashtable parameter = null;

        //검색버튼 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = Utils.ConverToHashtable(this.groupBox1);
                this.parameter["PUMPSTATION_ID"] = this.parameter["BZS"];
                this.SelectPumpMaster();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectPumpMaster()
        {
            this.Cursor = Cursors.WaitCursor;
            //this.ultraGrid1.DataSource =
            //    Utils.ColumnMatch
            //    (
            //        this.ultraGrid1,
            //        PumpMasterWork.GetInstance().SelectPumpMaster(this.mainMap, this.parameter)
            //    );

            this.gridManager.DefaultColumnsMapping2(this.ultraGrid2);
            this.Cursor = Cursors.Default;
        }

        //저장버튼 이벤트 핸들러
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult qe = MessageBox.Show("선택항목을 저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.UpdatePumpMaster();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void UpdatePumpMaster()
        {
            this.Cursor = Cursors.WaitCursor;
            PumpMasterWork.GetInstance().UpdatePumpMaster(this.ultraGrid1);
            this.SelectPumpMaster();
            this.Cursor = Cursors.Default;
        }

        //엑셀버튼 이벤트 핸들러
        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid2, this.Text);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            this.SelectPumpMasterDetail();
        }

        private void SelectPumpMasterDetail()
        {
            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid2.DataSource =
                PumpPerformanceCurveManageWork.GetInstance().SelectPumpPerformanceCurveManageDetail(Utils.ConverToHashtable(this.ultraGrid1.ActiveRow));
            this.Cursor = Cursors.Default;
        }

        //체크박스 전체 선택 / 전체 해제
        private void ultraGrid1_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;

            bool result = false;

            if (e.Column.GetHeaderCheckedState(e.Rows) == CheckState.Checked)
            {
                result = true;
            }

            foreach (UltraGridRow row in ultraGrid.Rows)
            {
                if (!row.Hidden)
                {
                    row.Cells["SELECTED"].Value = result;
                }
            }
            ultraGrid.UpdateData();
        }

        //그리드 초기화할때 전체선택을 해제한다.
        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["SELECtED"].SetHeaderCheckedState(e.Layout.Rows, false);
        }

        private void ultraGrid1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //string whereCase = "FTR_IDN = '" + e.Row.Cells["PUMP_FTR_IDN"].Value.ToString() + "'";
            //string layerName = "펌프모터";
            //this.mainMap.MoveFocusAt(layerName, whereCase);
        }
    }
}
