﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_Common.manager;
using Infragistics.Win;
using WaterNet.WV_Common.util;
using WaterNet.WE_PumpMasterManage.work;

namespace WaterNet.WE_PumpMasterManage.form
{
    public partial class frmCurve : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        public frmCurve()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        private UltraGridCell cell = null;
        public frmCurve(UltraGridCell cell)
        {
            this.cell = cell;
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
            this.selectBtn.PerformClick();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.SelectPumpPerformanceCurveManage();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            //그리드1
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.Width = 120;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "성능진단일자";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "설명";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 350;
            column.Hidden = false;

            //그리드2
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량(㎥/h)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "###,###";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정(m)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "E";
            column.Header.Caption = "효율(%)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "POINT_YN";
            column.Header.Caption = "정격점";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.ForeColor = Color.Red;
            column.Width = 120;
            column.Hidden = false;
            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.unselectedBtn.Click += new EventHandler(unselectedBtn_Click);
            this.selectBtn.Click += new EventHandler(selectBtn_Click);
            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
        }

        private void unselectedBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("연결항목을 해제 하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                this.cell.Row.Cells["CURVE_ID"].Value = DBNull.Value;
                this.cell.Row.Cells["TIMESTAMP"].Value = DBNull.Value;
                this.Close();
            }
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                MessageBox.Show("성능진단자료를 선택해주세요.");
                return;
            }

            this.cell.Row.Cells["CURVE_ID"].Value = this.ultraGrid1.ActiveRow.Cells["CURVE_ID"].Value;
            this.cell.Row.Cells["TIMESTAMP"].Value = this.ultraGrid1.ActiveRow.Cells["TIMESTAMP"].Value;
            this.Close();
        }

        private Hashtable parameter = null;
        private void SelectPumpPerformanceCurveManage()
        {
            if (this.parameter == null)
            {
                this.parameter = Utils.ConverToHashtable(this.cell.Row);
            }

            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid1.DataSource =
                PumpMasterWork.GetInstance().SelectPumpPerformanceCurveManage(this.parameter);

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (gridRow.Cells["CURVE_ID"].Value.ToString() == this.cell.Value.ToString())
                {
                    gridRow.Activated = true;
                    break;
                }
            }
            this.Cursor = Cursors.Default;
        }

        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            this.SelectPumpPerformanceCurveManageDetail();
        }

        private void SelectPumpPerformanceCurveManageDetail()
        {
            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid2.DataSource =
                PumpMasterWork.GetInstance().SelectPumpPerformanceCurveManageDetail(Utils.ConverToHashtable(this.ultraGrid1.ActiveRow));
            this.Cursor = Cursors.Default;
        }
    }
}
