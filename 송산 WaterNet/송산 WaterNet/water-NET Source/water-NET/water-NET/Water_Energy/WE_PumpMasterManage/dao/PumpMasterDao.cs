﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WE_PumpMasterManage.dao
{
    public class PumpMasterDao
    {
        private static PumpMasterDao dao = null;
        private PumpMasterDao() { }

        public static PumpMasterDao GetInstance()
        {
            if (dao == null)
            {
                dao = new PumpMasterDao();
            }
            return dao;
        }

        public void InsertCurveModel(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into wh_curves (                                           ");
            query.AppendLine("select                                                            ");
            query.AppendLine("(select max(inp_number) from wh_title a where a.use_gbn = 'WH'),  ");
            query.AppendLine(":ID,                                                              ");
            query.AppendLine(":IDX,                                                             ");
            query.AppendLine(":X,                                                               ");
            query.AppendLine(":Y,                                                               ");
            query.AppendLine(":CURCD,                                                           ");
            query.AppendLine("''                                                                ");
            query.AppendLine("  from dual                                                       ");
            query.AppendLine(")                                                                 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("ID", OracleDbType.Varchar2),
                     new OracleParameter("IDX", OracleDbType.Varchar2),
                     new OracleParameter("X", OracleDbType.Varchar2),
                     new OracleParameter("Y", OracleDbType.Varchar2),
                     new OracleParameter("CURCD", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["ID"];
            parameters[1].Value = parameter["IDX"];
            parameters[2].Value = parameter["X"];
            parameters[3].Value = parameter["Y"];
            parameters[4].Value = parameter["CURCD"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeleteCurveModel(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" delete from wh_curves                                                     ");
            query.AppendLine("  where id in (:C_ID, :E_ID)                                               ");
            query.AppendLine("    and inp_number = (select inp_number from wh_title where use_gbn = 'WH')");

            IDataParameter[] parameters =  {
                     new OracleParameter("C_ID", OracleDbType.Varchar2),
                     new OracleParameter("E_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["C_ID"];
            parameters[1].Value = parameter["E_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public object SelectCurveModelCurveID(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" select replace(regexp_replace(b.properties, '[^,]*[hH][eE][aA][dD]', ''),' ', '')");
            query.AppendLine("   from wh_title a                                                                ");
            query.AppendLine("       ,wh_pumps b                                                                ");
            query.AppendLine("       ,wh_tags c                                                                 ");
            query.AppendLine("  where a.use_gbn = 'WH'                                                          ");
            query.AppendLine("    and b.inp_number = a.inp_number                                               ");
            query.AppendLine("    and c.inp_number = b.inp_number                                               ");
            query.AppendLine("    and c.id = b.id                                                               ");
            query.AppendLine("    and c.value1 = :PUMP_FTR_IDN                                                  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectCurveModelEnergyID(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select replace(regexp_replace(b.energy_statement,                                  ");
            query.AppendLine("                '[^,]*[eE][fF][fF][iI][cC][iI][eE][nN][cC][yY]', ''),' ', '')      ");
            query.AppendLine("  from (                                                                           ");
            query.AppendLine("       select a.inp_number                                                         ");
            query.AppendLine("             ,'PUMP'||c.id||'EFFICIENCY' id                                        ");
            query.AppendLine("         from wh_title a                                                           ");
            query.AppendLine("             ,wh_pumps b                                                           ");
            query.AppendLine("             ,wh_tags c                                                            ");
            query.AppendLine("        where a.use_gbn = 'WH'                                                     ");
            query.AppendLine("          and b.inp_number = a.inp_number                                          ");
            query.AppendLine("          and c.inp_number = b.inp_number                                          ");
            query.AppendLine("          and c.id = b.id                                                          ");
            query.AppendLine("          and c.value1 = :PUMP_FTR_IDN                                             ");
            query.AppendLine("      ) a                                                                          ");
            query.AppendLine("      ,wh_energy b                                                                 ");
            query.AppendLine(" where b.inp_number = a.inp_number                                                 ");
            query.AppendLine("   and replace(upper(b.energy_statement), ' ', '') like '%'||a.id||'%'             ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public DataSet SelectPumpMasterLink(OracleDBManager manager, string PUMP_FTR_IDN)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c.pumpstation_id                                                                ");
            query.AppendLine("     ,a.pump_ftr_idn                                                                   ");
            query.AppendLine("     ,b.remark remark                                                                  ");
            query.AppendLine("     ,b.pump_gbn                                                                       ");
            query.AppendLine("     ,to_number(b.pump_rpm) pump_rpm                                                   ");
            query.AppendLine("     ,b.sim_gbn                                                                        ");
            query.AppendLine("     ,decode(b.curve_id, null, null,                                                   ");
            query.AppendLine("       (select to_char(timestamp,'yyyy-mm-dd') from we_curve_master where curve_id = b.curve_id)) timestamp  ");
            query.AppendLine("     ,b.curve_id curve_id                                                              ");
            query.AppendLine("     ,to_number(nvl(b.diff,10)) diff                                                   ");
            query.AppendLine("     ,nvl(b.model_yn, 'N') model_yn                                                    ");
            query.AppendLine("  from (                                                                               ");
            query.AppendLine("     select c.value1 pump_ftr_idn                                                      ");
            query.AppendLine("           ,null remark                                                            ");
            query.AppendLine("       from wh_title a                                                                 ");
            query.AppendLine("           ,wh_pumps b                                                                 ");
            query.AppendLine("           ,wh_tags c                                                                  ");
            query.AppendLine("      where a.use_gbn = 'WH'                                                           ");
            query.AppendLine("        and b.inp_number = a.inp_number                                                ");
            query.AppendLine("        and c.inp_number = b.inp_number                                                ");
            query.AppendLine("        and c.id = b.id                                                                ");
            query.AppendLine("        and c.value1 = :PUMP_FTR_IDN                                                   ");
            query.AppendLine("     ) a                                                                               ");
            query.AppendLine("    ,we_pump b                                                                         ");
            query.AppendLine("    ,we_pump_station c                                                                 ");
            query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn                                              ");
            query.AppendLine("   and c.pumpstation_id(+) = b.pumpstation_id                                          ");
            query.AppendLine("order by to_number(c.pumpstation_id), to_number(a.pump_ftr_idn)                        ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = PUMP_FTR_IDN;

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectPumpMaster(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c.pumpstation_id                                                                ");
            query.AppendLine("     ,a.pump_ftr_idn                                                                   ");
            query.AppendLine("     ,nvl(b.remark, a.remark) remark                                                   ");
            query.AppendLine("     ,b.pump_gbn                                                                       ");
            query.AppendLine("     ,to_number(b.pump_rpm) pump_rpm                                                   ");
            query.AppendLine("     ,b.sim_gbn                                                                        ");
            query.AppendLine("     ,decode(b.curve_id, null, null,                                                   ");
            query.AppendLine("       (select to_char(timestamp,'yyyy-mm-dd') from we_curve_master where curve_id = b.curve_id)) timestamp  ");
            query.AppendLine("     ,b.curve_id curve_id                                                              ");
            query.AppendLine("     ,to_number(nvl(b.diff,10)) diff                                                   ");
            query.AppendLine("     ,nvl(b.model_yn, 'N') model_yn                                                    ");
            query.AppendLine("  from (                                                                               ");
            query.AppendLine("     select c.value1 pump_ftr_idn                                                      ");
            query.AppendLine("           ,b.remark remark                                                            ");
            query.AppendLine("       from wh_title a                                                                 ");
            query.AppendLine("           ,wh_pumps b                                                                 ");
            query.AppendLine("           ,wh_tags c                                                                  ");
            query.AppendLine("      where a.use_gbn = 'WH'                                                           ");
            query.AppendLine("        and b.inp_number = a.inp_number                                                ");
            query.AppendLine("        and c.inp_number = b.inp_number                                                ");
            query.AppendLine("        and c.id = b.id                                                                ");
            query.AppendLine("     ) a                                                                               ");
            query.AppendLine("    ,we_pump b                                                                         ");
            query.AppendLine("    ,we_pump_station c                                                                 ");
            query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn                                              ");
            query.AppendLine("   and c.pumpstation_id(+) = b.pumpstation_id                                          ");
            query.AppendLine("   and b.pumpstation_id = nvl(:BZS, b.pumpstation_id)                                  ");
            query.AppendLine("order by to_number(c.pumpstation_id), to_number(a.pump_ftr_idn)                        ");

            IDataParameter[] parameters =  {
                     new OracleParameter("BZS", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["BZS"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public void UpdatePumpMaster(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into we_pump a                                                                   ");
            query.AppendLine("using (                                                                                ");
            query.AppendLine("       select :PUMP_FTR_IDN pump_ftr_idn                                               ");
            query.AppendLine("             ,:REMARK remark                                                           ");
            query.AppendLine("             ,:PUMP_GBN pump_gbn                                                       ");
            query.AppendLine("             ,:PUMP_RPM pump_rpm                                                       ");
            query.AppendLine("             ,:SIM_GBN sim_gbn                                                         ");
            query.AppendLine("             ,:PUMPSTATION_ID pumpstation_id                                           ");
            query.AppendLine("             ,:DIFF diff                                                               ");
            query.AppendLine("             ,:CURVE_ID curve_id                                                       ");
            query.AppendLine("             ,:MODEL_YN model_yn                                                       ");
            query.AppendLine("         from dual                                                                     ");
            query.AppendLine("      ) b                                                                              ");
            query.AppendLine("   on (                                                                                ");
            query.AppendLine("       a.pump_ftr_idn = b.pump_ftr_idn                                                 ");
            query.AppendLine("      )                                                                                ");
            query.AppendLine(" when matched then                                                                     ");
            query.AppendLine("      update set a.remark = b.remark                                                   ");
            query.AppendLine("                ,a.pump_gbn = b.pump_gbn                                               ");
            query.AppendLine("                ,a.pump_rpm = b.pump_rpm                                               ");
            query.AppendLine("                ,a.sim_gbn = b.sim_gbn                                                 ");
            query.AppendLine("                ,a.pumpstation_id = b.pumpstation_id                                   ");
            query.AppendLine("                ,a.diff = b.diff                                                       ");
            query.AppendLine("                ,a.curve_id = b.curve_id                                               ");
            query.AppendLine("                ,a.model_yn = b.model_yn                                               ");
            query.AppendLine(" when not matched then                                                                 ");
            query.AppendLine("      insert (                                                                         ");
            query.AppendLine("              a.pump_ftr_idn                                                           ");
            query.AppendLine("             ,a.remark                                                                 ");
            query.AppendLine("             ,a.pump_gbn                                                               ");
            query.AppendLine("             ,a.pump_rpm                                                               ");
            query.AppendLine("             ,a.sim_gbn                                                                ");
            query.AppendLine("             ,a.pumpstation_id                                                         ");
            query.AppendLine("             ,a.diff                                                                   ");
            query.AppendLine("             ,a.curve_id                                                               ");
            query.AppendLine("             ,a.model_yn                                                               ");
            query.AppendLine("             )                                                                         ");
            query.AppendLine("      values (                                                                         ");
            query.AppendLine("              b.pump_ftr_idn                                                           ");
            query.AppendLine("             ,b.remark                                                                 ");
            query.AppendLine("             ,b.pump_gbn                                                               ");
            query.AppendLine("             ,b.pump_rpm                                                               ");
            query.AppendLine("             ,b.sim_gbn                                                                ");
            query.AppendLine("             ,b.pumpstation_id                                                         ");
            query.AppendLine("             ,b.diff                                                                   ");
            query.AppendLine("             ,b.curve_id                                                               ");
            query.AppendLine("             ,b.model_yn                                                               ");
            query.AppendLine("             )                                                                         ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("REMARK", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_GBN", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_RPM", OracleDbType.Varchar2),
                     new OracleParameter("SIM_GBN", OracleDbType.Varchar2),
                     new OracleParameter("PUMPSTATION_ID", OracleDbType.Varchar2),
                     new OracleParameter("DIFF", OracleDbType.Varchar2),
                     new OracleParameter("CURVE_ID", OracleDbType.Varchar2),
                     new OracleParameter("MODEL_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];
            parameters[1].Value = parameter["REMARK"];
            parameters[2].Value = parameter["PUMP_GBN"];
            parameters[3].Value = parameter["PUMP_RPM"];
            parameters[4].Value = parameter["SIM_GBN"];
            parameters[5].Value = parameter["PUMPSTATION_ID"];
            parameters[6].Value = parameter["DIFF"];
            parameters[7].Value = parameter["CURVE_ID"];
            parameters[8].Value = parameter["MODEL_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void UpdatePumpMaster_MODEL_YN(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into we_pump a                                                                   ");
            query.AppendLine("using (                                                                                ");
            query.AppendLine("       select :PUMP_FTR_IDN pump_ftr_idn                                               ");
            query.AppendLine("             ,:MODEL_YN model_yn                                                       ");
            query.AppendLine("         from dual                                                                     ");
            query.AppendLine("      ) b                                                                              ");
            query.AppendLine("   on (                                                                                ");
            query.AppendLine("       a.pump_ftr_idn = b.pump_ftr_idn                                                 ");
            query.AppendLine("      )                                                                                ");
            query.AppendLine(" when matched then                                                                     ");
            query.AppendLine("      update set a.model_yn = b.model_yn                                               ");
            query.AppendLine(" when not matched then                                                                 ");
            query.AppendLine("      insert (                                                                         ");
            query.AppendLine("              a.pump_ftr_idn                                                           ");
            query.AppendLine("             ,a.model_yn                                                               ");
            query.AppendLine("             )                                                                         ");
            query.AppendLine("      values (                                                                         ");
            query.AppendLine("              b.pump_ftr_idn                                                           ");
            query.AppendLine("             ,b.model_yn                                                               ");
            query.AppendLine("             )                                                                         ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("MODEL_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];
            parameters[1].Value = parameter["MODEL_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public DataSet SelectPumpPerformanceCurveManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select curve_id                                                              ");
            query.AppendLine("      ,pump_ftr_idn                                                          ");
            query.AppendLine("      ,to_char(timestamp,'yyyy-mm-dd') timestamp                             ");
            query.AppendLine("      ,remark                                                                ");
            query.AppendLine("  from we_curve_master                                                       ");
            query.AppendLine(" where 1 = 1                                                                 ");
            query.AppendLine("   and pump_ftr_idn = :PUMP_FTR_IDN                                          ");
            query.AppendLine(" order by curve_id, timestamp                                                ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectPumpPerformanceCurveManageDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select curve_id                                                              ");
            query.AppendLine("      ,to_number(flow) flow                                                  ");
            query.AppendLine("      ,to_number(h) h                                                        ");
            query.AppendLine("      ,to_number(e) e                                                        ");
            query.AppendLine("      ,point_yn                                                              ");
            query.AppendLine("  from we_curve_data                                                         ");
            query.AppendLine(" where curve_id = :CURVE_ID                                                  ");
            query.AppendLine(" order by to_number(flow)                                                    ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["CURVE_ID"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }
    }
}
