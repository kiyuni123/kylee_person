﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WE_PumpMasterManage.dao;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.work;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.interface1;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WaterAOCore;
using System.Windows.Forms;

namespace WaterNet.WE_PumpMasterManage.work
{
    public class PumpMasterWork : BaseWork
    {
        private static PumpMasterWork work = null;
        private PumpMasterDao dao = null;

        private PumpMasterWork()
        {
            dao = PumpMasterDao.GetInstance();
        }

        public static PumpMasterWork GetInstance()
        {
            if (work == null)
            {
                work = new PumpMasterWork();
            }
            return work;
        }

        //모델의 성능곡선을 변경한다.
        public void UpdateModelCurve(Hashtable parameter)
        {
            if (!parameter.ContainsKey("CURVE_ID") || parameter["CURVE_ID"] == DBNull.Value)
            {
                return;
            }

            try
            {
                ConnectionOpen();
                BeginTransaction();

                parameter["C_ID"] = dao.SelectCurveModelCurveID(base.DataBaseManager, parameter);
                parameter["E_ID"] = dao.SelectCurveModelEnergyID(base.DataBaseManager, parameter);

                dao.DeleteCurveModel(base.DataBaseManager, parameter);

                DataSet set = dao.SelectPumpPerformanceCurveManageDetail(base.DataBaseManager, parameter);

                if (set == null || set.Tables.Count == 0)
                {
                    return;
                }

                int h_index = -1;
                int e_index = -1;

                foreach (DataRow row in set.Tables[0].Rows)
                {
                    if (row["H"] != DBNull.Value)
                    {
                        h_index++;
                        if (parameter["C_ID"] != null)
                        {
                            Hashtable h_parameter = new Hashtable();
                            h_parameter["ID"] = parameter["C_ID"];
                            h_parameter["IDX"] = h_index;
                            h_parameter["X"] = row["FLOW"];
                            h_parameter["Y"] = row["H"];
                            h_parameter["CURCD"] = "000001";
                            dao.InsertCurveModel(base.DataBaseManager, h_parameter);
                        }
                    }

                    if (row["E"] != DBNull.Value)
                    {
                        e_index++;
                        if (parameter["E_ID"] != null)
                        {
                            Hashtable e_parameter = new Hashtable();
                            e_parameter["ID"] = parameter["E_ID"];
                            e_parameter["IDX"] = h_index;
                            e_parameter["X"] = row["FLOW"];
                            e_parameter["Y"] = row["E"];
                            e_parameter["CURCD"] = "000002";
                            dao.InsertCurveModel(base.DataBaseManager, e_parameter);
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public DataTable SelectPumpMaster(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectPumpMaster(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];

                        result.Columns.Add("SELECTED", typeof(bool));

                        foreach (DataRow row in result.Rows)
                        {
                            row["SELECTED"] = false;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void UpdatePumpMaster(UltraGrid gridRow)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow row in gridRow.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["SELECTED"].Value))
                    {
                        dao.UpdatePumpMaster(base.DataBaseManager, Utils.ConverToHashtable(row));
                    }
                }

                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void UpdatePumpMaster_MODEL_YN(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.UpdatePumpMaster_MODEL_YN(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public DataTable SelectPumpPerformanceCurveManage(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectPumpPerformanceCurveManage(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectPumpPerformanceCurveManageDetail(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectPumpPerformanceCurveManageDetail(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
