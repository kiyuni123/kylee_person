﻿namespace WaterNet.WE_EnergyAnalysis
{
    partial class frmBizPlace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.cboModel = new System.Windows.Forms.ComboBox();
            this.splitContainerContents = new System.Windows.Forms.SplitContainer();
            this.ultraGrid_Biz = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelBiz = new System.Windows.Forms.Panel();
            this.btnBizSave = new System.Windows.Forms.Button();
            this.btnBizRefresh = new System.Windows.Forms.Button();
            this.btnBizDelete = new System.Windows.Forms.Button();
            this.lblBiz = new System.Windows.Forms.Label();
            this.ultraGrid_Tag = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelTag = new System.Windows.Forms.Panel();
            this.btnTagSave = new System.Windows.Forms.Button();
            this.btnTagRefresh = new System.Windows.Forms.Button();
            this.btnTagDelete = new System.Windows.Forms.Button();
            this.lblTag = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ultraGrid_Pump = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelPump = new System.Windows.Forms.Panel();
            this.btnPumpSave = new System.Windows.Forms.Button();
            this.btnPumpRefresh = new System.Windows.Forms.Button();
            this.btnPumpDelete = new System.Windows.Forms.Button();
            this.lblPump = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ultraGrid_Junction = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnJunctionRefresh = new System.Windows.Forms.Button();
            this.btnJunctionSave = new System.Windows.Forms.Button();
            this.btnJunctionDelete = new System.Windows.Forms.Button();
            this.lblJunction = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.splitContainerContents.Panel1.SuspendLayout();
            this.splitContainerContents.Panel2.SuspendLayout();
            this.splitContainerContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Biz)).BeginInit();
            this.panelBiz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Tag)).BeginInit();
            this.panelTag.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pump)).BeginInit();
            this.panelPump.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Junction)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.cboModel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(515, 42);
            this.panel1.TabIndex = 28;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(435, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 30);
            this.btnClose.TabIndex = 39;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cboModel
            // 
            this.cboModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboModel.FormattingEnabled = true;
            this.cboModel.Location = new System.Drawing.Point(12, 12);
            this.cboModel.Name = "cboModel";
            this.cboModel.Size = new System.Drawing.Size(280, 20);
            this.cboModel.TabIndex = 37;
            this.cboModel.Tag = "PP_CAT";
            this.cboModel.SelectedIndexChanged += new System.EventHandler(this.cboModel_SelectedIndexChanged);
            // 
            // splitContainerContents
            // 
            this.splitContainerContents.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerContents.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainerContents.Location = new System.Drawing.Point(0, 42);
            this.splitContainerContents.Name = "splitContainerContents";
            this.splitContainerContents.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerContents.Panel1
            // 
            this.splitContainerContents.Panel1.Controls.Add(this.ultraGrid_Biz);
            this.splitContainerContents.Panel1.Controls.Add(this.panelBiz);
            this.splitContainerContents.Panel1.Controls.Add(this.lblBiz);
            // 
            // splitContainerContents.Panel2
            // 
            this.splitContainerContents.Panel2.Controls.Add(this.ultraGrid_Tag);
            this.splitContainerContents.Panel2.Controls.Add(this.panelTag);
            this.splitContainerContents.Panel2.Controls.Add(this.lblTag);
            this.splitContainerContents.Size = new System.Drawing.Size(515, 262);
            this.splitContainerContents.SplitterDistance = 131;
            this.splitContainerContents.TabIndex = 31;
            // 
            // ultraGrid_Biz
            // 
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Biz.DisplayLayout.Appearance = appearance15;
            this.ultraGrid_Biz.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Biz.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_Biz.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Biz.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_Biz.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Biz.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Biz.DisplayLayout.Override.CardAreaAppearance = appearance16;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Biz.DisplayLayout.Override.CellAppearance = appearance17;
            this.ultraGrid_Biz.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Biz.DisplayLayout.Override.CellPadding = 0;
            appearance18.BackColor = System.Drawing.SystemColors.Control;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Biz.DisplayLayout.Override.GroupByRowAppearance = appearance18;
            appearance19.TextHAlignAsString = "Left";
            this.ultraGrid_Biz.DisplayLayout.Override.HeaderAppearance = appearance19;
            this.ultraGrid_Biz.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Biz.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Biz.DisplayLayout.Override.RowAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Biz.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.ultraGrid_Biz.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Biz.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Biz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Biz.Location = new System.Drawing.Point(26, 22);
            this.ultraGrid_Biz.Name = "ultraGrid_Biz";
            this.ultraGrid_Biz.Size = new System.Drawing.Size(487, 107);
            this.ultraGrid_Biz.TabIndex = 45;
            this.ultraGrid_Biz.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Biz_AfterRowInsert);
            this.ultraGrid_Biz.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.ultraGrid_Biz_AfterSelectChange);
            // 
            // panelBiz
            // 
            this.panelBiz.Controls.Add(this.btnBizSave);
            this.panelBiz.Controls.Add(this.btnBizRefresh);
            this.panelBiz.Controls.Add(this.btnBizDelete);
            this.panelBiz.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBiz.Location = new System.Drawing.Point(26, 0);
            this.panelBiz.Name = "panelBiz";
            this.panelBiz.Size = new System.Drawing.Size(487, 22);
            this.panelBiz.TabIndex = 44;
            // 
            // btnBizSave
            // 
            this.btnBizSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBizSave.Location = new System.Drawing.Point(366, 1);
            this.btnBizSave.Name = "btnBizSave";
            this.btnBizSave.Size = new System.Drawing.Size(54, 20);
            this.btnBizSave.TabIndex = 8;
            this.btnBizSave.Text = "저장";
            this.btnBizSave.UseVisualStyleBackColor = true;
            this.btnBizSave.Click += new System.EventHandler(this.btnBizSave_Click);
            // 
            // btnBizRefresh
            // 
            this.btnBizRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBizRefresh.Location = new System.Drawing.Point(290, 1);
            this.btnBizRefresh.Name = "btnBizRefresh";
            this.btnBizRefresh.Size = new System.Drawing.Size(71, 20);
            this.btnBizRefresh.TabIndex = 7;
            this.btnBizRefresh.Text = "새로고침";
            this.btnBizRefresh.UseVisualStyleBackColor = true;
            this.btnBizRefresh.Click += new System.EventHandler(this.btnBizRefresh_Click);
            // 
            // btnBizDelete
            // 
            this.btnBizDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBizDelete.Location = new System.Drawing.Point(426, 1);
            this.btnBizDelete.Name = "btnBizDelete";
            this.btnBizDelete.Size = new System.Drawing.Size(54, 20);
            this.btnBizDelete.TabIndex = 6;
            this.btnBizDelete.Text = "삭제";
            this.btnBizDelete.UseVisualStyleBackColor = true;
            this.btnBizDelete.Click += new System.EventHandler(this.btnBizDelete_Click);
            // 
            // lblBiz
            // 
            this.lblBiz.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblBiz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBiz.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblBiz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBiz.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBiz.Location = new System.Drawing.Point(0, 0);
            this.lblBiz.Name = "lblBiz";
            this.lblBiz.Size = new System.Drawing.Size(26, 129);
            this.lblBiz.TabIndex = 42;
            this.lblBiz.Text = "사업장";
            this.lblBiz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ultraGrid_Tag
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Tag.DisplayLayout.Appearance = appearance1;
            this.ultraGrid_Tag.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Tag.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_Tag.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Tag.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_Tag.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Tag.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Tag.DisplayLayout.Override.CardAreaAppearance = appearance2;
            appearance3.BorderColor = System.Drawing.Color.Silver;
            appearance3.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Tag.DisplayLayout.Override.CellAppearance = appearance3;
            this.ultraGrid_Tag.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Tag.DisplayLayout.Override.CellPadding = 0;
            appearance4.BackColor = System.Drawing.SystemColors.Control;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Tag.DisplayLayout.Override.GroupByRowAppearance = appearance4;
            appearance5.TextHAlignAsString = "Left";
            this.ultraGrid_Tag.DisplayLayout.Override.HeaderAppearance = appearance5;
            this.ultraGrid_Tag.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Tag.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Tag.DisplayLayout.Override.RowAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Tag.DisplayLayout.Override.TemplateAddRowAppearance = appearance7;
            this.ultraGrid_Tag.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Tag.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Tag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Tag.Location = new System.Drawing.Point(26, 22);
            this.ultraGrid_Tag.Name = "ultraGrid_Tag";
            this.ultraGrid_Tag.Size = new System.Drawing.Size(487, 103);
            this.ultraGrid_Tag.TabIndex = 46;
            this.ultraGrid_Tag.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Tag_AfterRowInsert);
            // 
            // panelTag
            // 
            this.panelTag.Controls.Add(this.btnTagSave);
            this.panelTag.Controls.Add(this.btnTagRefresh);
            this.panelTag.Controls.Add(this.btnTagDelete);
            this.panelTag.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTag.Location = new System.Drawing.Point(26, 0);
            this.panelTag.Name = "panelTag";
            this.panelTag.Size = new System.Drawing.Size(487, 22);
            this.panelTag.TabIndex = 45;
            // 
            // btnTagSave
            // 
            this.btnTagSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTagSave.Location = new System.Drawing.Point(366, 1);
            this.btnTagSave.Name = "btnTagSave";
            this.btnTagSave.Size = new System.Drawing.Size(54, 20);
            this.btnTagSave.TabIndex = 8;
            this.btnTagSave.Text = "저장";
            this.btnTagSave.UseVisualStyleBackColor = true;
            this.btnTagSave.Click += new System.EventHandler(this.btnTagSave_Click);
            // 
            // btnTagRefresh
            // 
            this.btnTagRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTagRefresh.Location = new System.Drawing.Point(290, 1);
            this.btnTagRefresh.Name = "btnTagRefresh";
            this.btnTagRefresh.Size = new System.Drawing.Size(71, 20);
            this.btnTagRefresh.TabIndex = 7;
            this.btnTagRefresh.Text = "새로고침";
            this.btnTagRefresh.UseVisualStyleBackColor = true;
            this.btnTagRefresh.Click += new System.EventHandler(this.btnTagRefresh_Click);
            // 
            // btnTagDelete
            // 
            this.btnTagDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTagDelete.Location = new System.Drawing.Point(426, 1);
            this.btnTagDelete.Name = "btnTagDelete";
            this.btnTagDelete.Size = new System.Drawing.Size(54, 20);
            this.btnTagDelete.TabIndex = 6;
            this.btnTagDelete.Text = "삭제";
            this.btnTagDelete.UseVisualStyleBackColor = true;
            this.btnTagDelete.Click += new System.EventHandler(this.btnTagDelete_Click);
            // 
            // lblTag
            // 
            this.lblTag.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblTag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTag.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTag.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTag.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTag.Location = new System.Drawing.Point(0, 0);
            this.lblTag.Name = "lblTag";
            this.lblTag.Size = new System.Drawing.Size(26, 125);
            this.lblTag.TabIndex = 43;
            this.lblTag.Text = "T A G 설정";
            this.lblTag.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 304);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(515, 266);
            this.splitContainer1.SplitterDistance = 123;
            this.splitContainer1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ultraGrid_Pump);
            this.panel2.Controls.Add(this.panelPump);
            this.panel2.Controls.Add(this.lblPump);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(513, 121);
            this.panel2.TabIndex = 31;
            // 
            // ultraGrid_Pump
            // 
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Pump.DisplayLayout.Appearance = appearance22;
            this.ultraGrid_Pump.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Pump.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_Pump.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Pump.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_Pump.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Pump.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pump.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Pump.DisplayLayout.Override.CellAppearance = appearance24;
            this.ultraGrid_Pump.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Pump.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pump.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Pump.DisplayLayout.Override.RowAppearance = appearance27;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Pump.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.ultraGrid_Pump.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Pump.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Pump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Pump.Location = new System.Drawing.Point(26, 22);
            this.ultraGrid_Pump.Name = "ultraGrid_Pump";
            this.ultraGrid_Pump.Size = new System.Drawing.Size(487, 99);
            this.ultraGrid_Pump.TabIndex = 47;
            this.ultraGrid_Pump.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Pump_AfterRowUpdate);
            this.ultraGrid_Pump.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Pump_AfterRowInsert);
            // 
            // panelPump
            // 
            this.panelPump.Controls.Add(this.btnPumpSave);
            this.panelPump.Controls.Add(this.btnPumpRefresh);
            this.panelPump.Controls.Add(this.btnPumpDelete);
            this.panelPump.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPump.Location = new System.Drawing.Point(26, 0);
            this.panelPump.Name = "panelPump";
            this.panelPump.Size = new System.Drawing.Size(487, 22);
            this.panelPump.TabIndex = 46;
            // 
            // btnPumpSave
            // 
            this.btnPumpSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPumpSave.Location = new System.Drawing.Point(366, 1);
            this.btnPumpSave.Name = "btnPumpSave";
            this.btnPumpSave.Size = new System.Drawing.Size(54, 20);
            this.btnPumpSave.TabIndex = 10;
            this.btnPumpSave.Text = "저장";
            this.btnPumpSave.UseVisualStyleBackColor = true;
            this.btnPumpSave.Click += new System.EventHandler(this.btnPumpSave_Click);
            // 
            // btnPumpRefresh
            // 
            this.btnPumpRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPumpRefresh.Location = new System.Drawing.Point(290, 1);
            this.btnPumpRefresh.Name = "btnPumpRefresh";
            this.btnPumpRefresh.Size = new System.Drawing.Size(71, 20);
            this.btnPumpRefresh.TabIndex = 9;
            this.btnPumpRefresh.Text = "새로고침";
            this.btnPumpRefresh.UseVisualStyleBackColor = true;
            this.btnPumpRefresh.Click += new System.EventHandler(this.btnPumpRefresh_Click);
            // 
            // btnPumpDelete
            // 
            this.btnPumpDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPumpDelete.Location = new System.Drawing.Point(426, 1);
            this.btnPumpDelete.Name = "btnPumpDelete";
            this.btnPumpDelete.Size = new System.Drawing.Size(54, 20);
            this.btnPumpDelete.TabIndex = 6;
            this.btnPumpDelete.Text = "삭제";
            this.btnPumpDelete.UseVisualStyleBackColor = true;
            this.btnPumpDelete.Click += new System.EventHandler(this.btnPumpDelete_Click);
            // 
            // lblPump
            // 
            this.lblPump.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPump.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPump.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPump.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblPump.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPump.Location = new System.Drawing.Point(0, 0);
            this.lblPump.Name = "lblPump";
            this.lblPump.Size = new System.Drawing.Size(26, 121);
            this.lblPump.TabIndex = 19;
            this.lblPump.Text = "펌프";
            this.lblPump.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ultraGrid_Junction);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.lblJunction);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(513, 137);
            this.panel3.TabIndex = 32;
            // 
            // ultraGrid_Junction
            // 
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Junction.DisplayLayout.Appearance = appearance8;
            this.ultraGrid_Junction.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Junction.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_Junction.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Junction.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_Junction.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Junction.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Junction.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Junction.DisplayLayout.Override.CellAppearance = appearance10;
            this.ultraGrid_Junction.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Junction.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Junction.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.ultraGrid_Junction.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.ultraGrid_Junction.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Junction.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Junction.DisplayLayout.Override.RowAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Junction.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.ultraGrid_Junction.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Junction.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Junction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Junction.Location = new System.Drawing.Point(26, 22);
            this.ultraGrid_Junction.Name = "ultraGrid_Junction";
            this.ultraGrid_Junction.Size = new System.Drawing.Size(487, 115);
            this.ultraGrid_Junction.TabIndex = 47;
            this.ultraGrid_Junction.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Junction_AfterRowInsert);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnJunctionRefresh);
            this.panel4.Controls.Add(this.btnJunctionSave);
            this.panel4.Controls.Add(this.btnJunctionDelete);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(26, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(487, 22);
            this.panel4.TabIndex = 46;
            // 
            // btnJunctionRefresh
            // 
            this.btnJunctionRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJunctionRefresh.Location = new System.Drawing.Point(290, 1);
            this.btnJunctionRefresh.Name = "btnJunctionRefresh";
            this.btnJunctionRefresh.Size = new System.Drawing.Size(71, 20);
            this.btnJunctionRefresh.TabIndex = 11;
            this.btnJunctionRefresh.Text = "새로고침";
            this.btnJunctionRefresh.UseVisualStyleBackColor = true;
            this.btnJunctionRefresh.Click += new System.EventHandler(this.btnJunctionRefresh_Click);
            // 
            // btnJunctionSave
            // 
            this.btnJunctionSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJunctionSave.Location = new System.Drawing.Point(366, 1);
            this.btnJunctionSave.Name = "btnJunctionSave";
            this.btnJunctionSave.Size = new System.Drawing.Size(54, 20);
            this.btnJunctionSave.TabIndex = 10;
            this.btnJunctionSave.Text = "저장";
            this.btnJunctionSave.UseVisualStyleBackColor = true;
            this.btnJunctionSave.Click += new System.EventHandler(this.btnJunctionSave_Click);
            // 
            // btnJunctionDelete
            // 
            this.btnJunctionDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJunctionDelete.Location = new System.Drawing.Point(426, 1);
            this.btnJunctionDelete.Name = "btnJunctionDelete";
            this.btnJunctionDelete.Size = new System.Drawing.Size(54, 20);
            this.btnJunctionDelete.TabIndex = 6;
            this.btnJunctionDelete.Text = "삭제";
            this.btnJunctionDelete.UseVisualStyleBackColor = true;
            this.btnJunctionDelete.Click += new System.EventHandler(this.btnJunctionDelete_Click);
            // 
            // lblJunction
            // 
            this.lblJunction.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblJunction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblJunction.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblJunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblJunction.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblJunction.Location = new System.Drawing.Point(0, 0);
            this.lblJunction.Name = "lblJunction";
            this.lblJunction.Size = new System.Drawing.Size(26, 137);
            this.lblJunction.TabIndex = 19;
            this.lblJunction.Text = "수압감시지점";
            this.lblJunction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmBizPlace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 570);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.splitContainerContents);
            this.Controls.Add(this.panel1);
            this.Name = "frmBizPlace";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "기본정보";
            this.Load += new System.EventHandler(this.frmBizPlace_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBizPlace_FormClosed);
            this.panel1.ResumeLayout(false);
            this.splitContainerContents.Panel1.ResumeLayout(false);
            this.splitContainerContents.Panel2.ResumeLayout(false);
            this.splitContainerContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Biz)).EndInit();
            this.panelBiz.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Tag)).EndInit();
            this.panelTag.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pump)).EndInit();
            this.panelPump.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Junction)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cboModel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.SplitContainer splitContainerContents;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Biz;
        private System.Windows.Forms.Panel panelBiz;
        private System.Windows.Forms.Button btnBizSave;
        private System.Windows.Forms.Button btnBizRefresh;
        private System.Windows.Forms.Button btnBizDelete;
        private System.Windows.Forms.Label lblBiz;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Tag;
        private System.Windows.Forms.Panel panelTag;
        private System.Windows.Forms.Button btnTagSave;
        private System.Windows.Forms.Button btnTagRefresh;
        private System.Windows.Forms.Button btnTagDelete;
        private System.Windows.Forms.Label lblTag;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Pump;
        private System.Windows.Forms.Panel panelPump;
        private System.Windows.Forms.Button btnPumpSave;
        private System.Windows.Forms.Button btnPumpRefresh;
        private System.Windows.Forms.Button btnPumpDelete;
        private System.Windows.Forms.Label lblPump;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Junction;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnJunctionSave;
        private System.Windows.Forms.Button btnJunctionDelete;
        private System.Windows.Forms.Label lblJunction;
        private System.Windows.Forms.Button btnJunctionRefresh;
    }
}