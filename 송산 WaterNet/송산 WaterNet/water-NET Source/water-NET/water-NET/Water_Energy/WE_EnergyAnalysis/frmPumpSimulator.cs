﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChartFX.WinForms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
using WaterNet.WE_Common;

using WaterNet.WH_PipingNetworkAnalysis.epanet;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
#endregion UltraGrid를 사용=>namespace선언


namespace WaterNet.WE_EnergyAnalysis
{
    public partial class frmPumpSimulator : Form, WaterNet.WaterNetCore.IForminterface
    {
        #region Private Field ------------------------------------------------------------------------------
        private GridManager m_GridSimuManager = null;    //시뮬레이션 그리드
        private GridManager m_GridHeadManager = null;    //실시간 가동시간 그리드
        private GridManager m_GridRealFrequence = null;  //실시간 주파수 그리드

        private ChartManager m_ChartManager = null;            //챠트
        private ChartManager m_ChartStatusManager = null;      //펌프 가동챠트
        #endregion Private Field ------------------------------------------------------------------------------

        #region 생성자 및 환경설정 ----------------------------------------------------------------------
        public frmPumpSimulator()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "에너지 모의"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        #region 초기화설정
        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            #region 시뮬레이션 유량 그리드
            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHECKED";
            oUltraGridColumn.Header.Caption = "선택";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.Edit;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_NAME";
            oUltraGridColumn.Header.Caption = "펌프#";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TOT";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.CellAppearance.BackColor = Color.WhiteSmoke;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Formula = "([T00] + [T01] + [T02] + [T03] + [T04] + [T05] + [T06] + [T07] + [T08] + [T09] + [T10] + [T11] + [T12] + [T13] + [T14] + [T15] + [T16] + [T17] + [T18] + [T19] + [T20] + [T21] + [T22] + [T23])";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;
            
            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T00";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T01";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T02";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T03";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T04";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T05";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T06";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T07";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T08";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T09";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T10";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T11";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T12";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T13";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T14";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T15";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T16";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T17";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T18";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T19";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T20";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T21";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T22";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T23";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            WaterNetCore.FormManager.SetGridStyle(ultraGrid_SimuFlow);
            ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns["CHECKED"].CellActivation = Activation.AllowEdit;
            ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns["CHECKED"].CellClickAction = CellClickAction.Edit;
            #endregion 시뮬레이션 유량 그리드

            #region 시뮬레이션 수위 그리드
            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHECKED";
            oUltraGridColumn.Header.Caption = "선택";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_NAME";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AVG";
            oUltraGridColumn.Header.Caption = "평균";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.CellAppearance.BackColor = Color.WhiteSmoke;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Formula = "([T00] + [T01] + [T02] + [T03] + [T04] + [T05] + [T06] + [T07] + [T08] + [T09] + [T10] + [T11] + [T12] + [T13] + [T14] + [T15] + [T16] + [T17] + [T18] + [T19] + [T20] + [T21] + [T22] + [T23]) / 24";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T00";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T01";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T02";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T03";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T04";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T05";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T06";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T07";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T08";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T09";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T10";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T11";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T12";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T13";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T14";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T15";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T16";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T17";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T18";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T19";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T20";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T21";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T22";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T23";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            WaterNetCore.FormManager.SetGridStyle(ultraGrid_SimuHead);
            ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns["CHECKED"].CellActivation = Activation.AllowEdit;
            ultraGrid_SimuHead.DisplayLayout.Bands[0].Columns["CHECKED"].CellClickAction = CellClickAction.Edit;
            #endregion 시뮬레이션 수위 그리드

            #region 시뮬레이션 에너지 그리드
            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHECKED";
            oUltraGridColumn.Header.Caption = "선택";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_NAME";
            oUltraGridColumn.Header.Caption = "펌프#";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TOT";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.CellAppearance.BackColor = Color.WhiteSmoke;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Formula = "[T00] + [T01] + [T02] + [T03] + [T04] + [T05] + [T06] + [T07] + [T08] + [T09] + [T10] + [T11] + [T12] + [T13] + [T14] + [T15] + [T16] + [T17] + [T18] + [T19] + [T20] + [T21] + [T22] + [T23]";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T00";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T01";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T02";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T03";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T04";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T05";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T06";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T07";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T08";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T09";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T10";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T11";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T12";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T13";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T14";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T15";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T16";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T17";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T18";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T19";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T20";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T21";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T22";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T23";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            WaterNetCore.FormManager.SetGridStyle(ultraGrid_SimuEnergy);
            ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns["CHECKED"].CellActivation = Activation.AllowEdit;
            ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns["CHECKED"].CellClickAction = CellClickAction.Edit;
            #endregion 시뮬레이션 에너지 그리드

            #region 시뮬레이션 가동 그리드
            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            //oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "CHECKED";
            //oUltraGridColumn.Header.Caption = "선택";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Width = 50;
            //oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_NAME";
            oUltraGridColumn.Header.Caption = "펌프#";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TOT";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.CellAppearance.BackColor = Color.WhiteSmoke;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0";
            oUltraGridColumn.Formula = "[T00] + [T01] + [T02] + [T03] + [T04] + [T05] + [T06] + [T07] + [T08] + [T09] + [T10] + [T11] + [T12] + [T13] + [T14] + [T15] + [T16] + [T17] + [T18] + [T19] + [T20] + [T21] + [T22] + [T23]";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T00";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T01";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T02";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T03";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T04";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T05";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T06";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T07";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T08";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T09";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T10";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T11";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T12";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T13";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T14";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T15";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T16";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T17";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T18";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T19";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T20";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T21";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T22";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T23";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            WaterNetCore.FormManager.SetGridStyle(ultraGrid_SimuStatus);
            //ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns["CHECKED"].CellActivation = Activation.AllowEdit;
            //ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns["CHECKED"].CellClickAction = CellClickAction.Edit;
            #endregion 시뮬레이션 가동 그리드

            #region 시뮬레이션 Junction 그리드
            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHECKED";
            oUltraGridColumn.Header.Caption = "선택";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AVG";
            oUltraGridColumn.Header.Caption = "평균";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.CellAppearance.BackColor = Color.WhiteSmoke;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Formula = "([T00] + [T01] + [T02] + [T03] + [T04] + [T05] + [T06] + [T07] + [T08] + [T09] + [T10] + [T11] + [T12] + [T13] + [T14] + [T15] + [T16] + [T17] + [T18] + [T19] + [T20] + [T21] + [T22] + [T23]) / 24";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T00";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T01";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T02";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T03";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T04";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T05";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T06";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T07";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T08";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T09";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T10";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T11";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T12";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T13";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T14";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T15";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T16";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T17";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T18";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T19";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T20";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T21";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T22";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T23";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            WaterNetCore.FormManager.SetGridStyle(ultraGrid_SimuPress);
            ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns["CHECKED"].CellActivation = Activation.AllowEdit;
            ultraGrid_SimuPress.DisplayLayout.Bands[0].Columns["CHECKED"].CellClickAction = CellClickAction.Edit;
            #endregion 시뮬레이션 Junction 그리드

            #region 흡입수두 그리드
            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_NAME";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T00";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T01";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T02";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T03";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50; oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T04";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50; 
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T05";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T06";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T07";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T08";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T09";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T10";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T11";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T12";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T13";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T14";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T15";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T16";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T17";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T18";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T19";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T20";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T21";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T22";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_ReservoirsHead.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "T23";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "##0.##";
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            m_GridHeadManager = new GridManager(ultraGrid_ReservoirsHead);
            m_GridHeadManager.SetGridStyle_Update();
            m_GridHeadManager.ColumeNoEdit("BIZ_NAME");
            //m_GridHeadManager.PerformAutoResize();
            #endregion 흡입수두 그리드

            #endregion 그리드 설정

            DockManagerCommand.Visible = false;

            if (m_ChartManager == null) m_ChartManager = new ChartManager(chart1);
            m_ChartManager.ChartInit();

            if (m_ChartStatusManager == null) m_ChartStatusManager = new ChartManager(chart2);
            m_ChartStatusManager.ChartInit();

        }

        #endregion 초기화설정

        /// <summary>
        /// 펌프 가동상태 챠트 생성
        /// </summary>
        public void InitializePumpStatusChart()
        {
            try
            {
                ///상동(가) 펌프가동 상태 그래프
                m_ChartStatusManager.ChartInit();
                m_ChartStatusManager.chart.Panes[0].Visible = false;  //0번 Pane는 Default로 생성되며, 지울수가 없으므로, 표시안함.

                Pane pane = new Pane();
                if (EMFrame.statics.AppStatic.USER_SGCCD.Equals("45180"))
                {
                    pane.Title.Text = "상동가압장";    
                }
                
                pane.Visible = true;
                pane.Axes[0].DataFormat.Format = AxisFormat.Number;
                pane.Axes[0].DataFormat.CustomFormat = "#";
                m_ChartStatusManager.AddPane(pane);

                Pane pane2 = new Pane();
                if (EMFrame.statics.AppStatic.USER_SGCCD.Equals("45180"))
                {
                    pane2.Title.Text = "회룡가압장";
                }
                
                pane2.Visible = true;
                pane2.Axes[0].DataFormat.Format = AxisFormat.Number;
                pane2.Axes[0].DataFormat.CustomFormat = "#";
                m_ChartStatusManager.AddPane(pane2);

                m_ChartStatusManager.chart.LegendBox.Visible = true;
                m_ChartStatusManager.chart.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
                m_ChartStatusManager.chart.LegendBox.MarginY = -1;
                m_ChartStatusManager.chart.AxisX.Staggered = false;
                m_ChartStatusManager.chart.PlotAreaMargin.Top = -1;
                m_ChartStatusManager.chart.PlotAreaMargin.Bottom = -1;
                                
                for (int i = 0; i < 24; i++)  m_ChartStatusManager.chart.AxisX.Labels[i] = (i + 1).ToString() + "h";

                m_ChartStatusManager.DataPoint = 24;

                ///이유를 알지 못하지만, 
                ///첫번째 로우의 데이터의 이상현상
                ///위 현상에 대한 편법으로 시리즈를 생성하고 항상 표시하지 않음.
                SeriesAttributes series1 = m_ChartStatusManager.AddSeries();
                series1.Visible = false;
                ////////////////////////////////////////////////////////////////////////

                ///펌프 가동 챠트
                ///상동가압장
                ArrayList SDStatuslists = GetPumpDataValue(ultraGrid_SimuStatus, "SD");

                SeriesAttributes SDStatus_series = m_ChartStatusManager.AddSeries();
                SDStatus_series.Gallery = ChartFX.WinForms.Gallery.Step;
                SDStatus_series.Text = Convert.ToString("상동가압장");
                SDStatus_series.Visible = true;

                SDStatus_series.AxisY = m_ChartStatusManager.chart.Panes[1].AxisY;
                //SDStatus_series.AxisY.Title.Text = "가동";
                SDStatus_series.AxisY.Title.Alignment = StringAlignment.Far;
                SDStatus_series.AxisY.LabelsFormat.Format = AxisFormat.Number;
                SDStatus_series.AxisY.LabelsFormat.CustomFormat = "#0";
                SDStatus_series.AxisY.Step = 1;

                int SDStatus_index = m_ChartStatusManager.GetSeriesIndex(SDStatus_series.Text);

                if (SDStatus_index > -1)
                {
                    for (int i = 0; i < SDStatuslists.Count; i++)
                    {
                        m_ChartStatusManager.chart.Data.Y[SDStatus_index, i] = Convert.ToDouble(SDStatuslists[i]);
                    }
                }

                ///회룡가압장
                ArrayList HRStatuslists = GetPumpDataValue(ultraGrid_SimuStatus, "HR");

                SeriesAttributes HRStatus_series = m_ChartStatusManager.AddSeries();
                HRStatus_series.Gallery = ChartFX.WinForms.Gallery.Step;
                HRStatus_series.Text = Convert.ToString("회룡가압장");
                HRStatus_series.Visible = true;

                HRStatus_series.AxisY = m_ChartStatusManager.chart.Panes[2].AxisY;
                //HRStatus_series.AxisY.Title.Text = "가동";
                HRStatus_series.AxisY.Title.Alignment = StringAlignment.Far;
                HRStatus_series.AxisY.LabelsFormat.Format = AxisFormat.Number;
                HRStatus_series.AxisY.LabelsFormat.CustomFormat = "#0";
                HRStatus_series.AxisY.Step = 1;

                int HRStatus_index = m_ChartStatusManager.GetSeriesIndex(HRStatus_series.Text);

                if (HRStatus_index > -1)
                {
                    for (int i = 0; i < HRStatuslists.Count; i++)
                    {
                        m_ChartStatusManager.chart.Data.Y[HRStatus_index, i] = Convert.ToDouble(HRStatuslists[i]);
                    }
                }

                //foreach (UltraGridRow item in ultraGrid_SimuStatus.Rows)
                //{
                //    SeriesAttributes series = m_ChartStatusManager.AddSeries();
                //    series.Gallery = ChartFX.WinForms.Gallery.Step;
                //    series.Text = Convert.ToString(item.Cells["PUMP_NAME"].Value);
                //    series.Visible = true;

                //    series.AxisY.Title.Text = "가동";
                //    series.AxisY.Title.Alignment = StringAlignment.Far;
                //    series.AxisY.LabelsFormat.Format = AxisFormat.Number;
                //    series.AxisY.LabelsFormat.CustomFormat = "0";

                //    if (Convert.ToString(item.Cells["PUMP_ID"].Value).IndexOf("SD") > -1) //상동가압장 펌프가 아님으로 skip.
                //    {
                //        series.AxisY = m_ChartStatusManager.chart.Panes[1].AxisY;
                //    }
                //    else
                //    {
                //        series.AxisY = m_ChartStatusManager.chart.Panes[2].AxisY;
                //    }



                //    int index = m_ChartStatusManager.GetSeriesIndex(series.Text);

                //    if (index > -1)
                //    {
                //        for (int i = 0; i < 24; i++)
                //        {
                //            m_ChartStatusManager.chart.Data.Y[index, i] = Convert.ToDouble(item.Cells[i + 3].Value);
                //        }
                //    }

                //}

                m_ChartStatusManager.chart.Update();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
           
        }

        /// <summary>
        /// 유량 및 에너지, 수압, 수위 데이터 챠트 생성
        /// </summary>
        public void InitializeChart()
        {
            checkBox_Head.Checked = false;
            checkBox_Junction.Checked = false;

            m_ChartManager.ChartInit();

            m_ChartManager.chart.LegendBox.Visible = true;
            m_ChartManager.chart.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            m_ChartManager.chart.LegendBox.MarginY = 1;
            m_ChartManager.chart.AxisX.Staggered = false;

            for (int i = 0; i < 24; i++)
                m_ChartManager.chart.AxisX.Labels[i] = (i + 1).ToString() + "h";

            m_ChartManager.DataPoint = 24;


            AxisY AddlAxisY = new AxisY();
            AddlAxisY.Visible = true;
            AddlAxisY.Position = AxisPosition.Far;
            AddlAxisY.ForceZero = false;
            AddlAxisY.Title.Text = "수위(m)";
            AddlAxisY.Title.Alignment = StringAlignment.Far;
            m_ChartManager.chart.AxesY.Add(AddlAxisY);

            AxisY AddlAxisY2 = new AxisY();
            AddlAxisY2.Visible = true;
            AddlAxisY2.Position = AxisPosition.Far;
            AddlAxisY2.ForceZero = false;
            AddlAxisY2.Title.Text = "에너지(kWh)";
            AddlAxisY2.Title.Alignment = StringAlignment.Far;
            m_ChartManager.chart.AxesY.Add(AddlAxisY2);

            AxisY AddlAxisY3 = new AxisY();
            AddlAxisY3.Visible = true;
            AddlAxisY3.Position = AxisPosition.Far;
            AddlAxisY3.ForceZero = false;
            AddlAxisY3.Title.Text = "수압(m)";
            AddlAxisY3.Title.Alignment = StringAlignment.Far;
            m_ChartManager.chart.AxesY.Add(AddlAxisY3);

            ///이유를 알지 못하지만, 
            ///첫번째 로우의 데이터의 이상현상
            ///위 현상에 대한 편법으로 시리즈를 생성하고 항상 표시하지 않음.
            SeriesAttributes series1 = m_ChartManager.AddSeries();
            series1.Visible = false;
            ////////////////////////////////////////////////////////////////////////
           
            foreach (UltraGridRow item in ultraGrid_SimuFlow.Rows)
            {
                SeriesAttributes series = m_ChartManager.AddSeries();
                series.Gallery = ChartFX.WinForms.Gallery.Bar;
                series.Text = Convert.ToString(item.Cells["PUMP_NAME"].Value) + "(유량)";
                series.Visible = false;

                series.AxisY.Title.Text = "유량(㎥/h)";
                series.AxisY.Title.Alignment = StringAlignment.Far;
                series.AxisY.LabelsFormat.Format = AxisFormat.Number;
                series.AxisY.LabelsFormat.CustomFormat = "#,###,##0.#";

                int index = m_ChartManager.GetSeriesIndex(series.Text);

                //Console.WriteLine(series.Text + "," + (m_ChartManager.chart.Series.Count - 1).ToString() + " , " + index.ToString());
                if (index > -1)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        m_ChartManager.chart.Data.Y[index, i] = Convert.ToDouble(item.Cells[i + 3].Value);
                    }
                }

            }

            foreach (UltraGridRow item in ultraGrid_SimuHead.Rows)
            {
                SeriesAttributes series = m_ChartManager.AddSeries();
                series.Gallery = ChartFX.WinForms.Gallery.Curve;
                series.Text = Convert.ToString(item.Cells["BIZ_NAME"].Value) + "(수위)";
                series.Visible = false;

                series.AxisY = AddlAxisY;
                series.AxisY.ForceZero = true;
                series.AxisY.LabelsFormat.Format = AxisFormat.Number;
                series.AxisY.LabelsFormat.CustomFormat = "#0.#";

                int index = m_ChartManager.GetSeriesIndex(series.Text);

                //Console.WriteLine(series.Text + "," + (m_ChartManager.chart.Series.Count - 1).ToString() + " , " + index.ToString());
                if (index > -1)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        m_ChartManager.chart.Data.Y[index, i] = Convert.ToDouble(item.Cells[i + 3].Value);
                    }
                }
            }

            foreach (UltraGridRow item in ultraGrid_SimuEnergy.Rows)
            {
                SeriesAttributes series = m_ChartManager.AddSeries();
                series.Gallery = ChartFX.WinForms.Gallery.Bar;
                series.Text = Convert.ToString(item.Cells["PUMP_NAME"].Value) + "(에너지)";
                series.Visible = false;

                series.AxisY = AddlAxisY2;
                series.AxisY.LabelsFormat.Format = AxisFormat.Number;
                series.AxisY.LabelsFormat.CustomFormat = "#,###,##0.#";

                int index = m_ChartManager.GetSeriesIndex(series.Text);

                //Console.WriteLine(series.Text + "," + (m_ChartManager.chart.Series.Count - 1).ToString() + " , " + index.ToString());
                if (index > -1)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        m_ChartManager.chart.Data.Y[index, i] = Convert.ToDouble(item.Cells[i + 3].Value);
                    }
                }

            }

            foreach (UltraGridRow item in ultraGrid_SimuPress.Rows)
            {
                SeriesAttributes series = m_ChartManager.AddSeries();
                series.Gallery = ChartFX.WinForms.Gallery.Curve;
                series.Text = Convert.ToString(item.Cells["ID"].Value) + "(수압)";
                series.Visible = false;

                series.AxisY = AddlAxisY3;
                series.AxisY.LabelsFormat.Format = AxisFormat.Number;
                series.AxisY.LabelsFormat.CustomFormat = "##,##0.#";

                int index = m_ChartManager.GetSeriesIndex(series.Text);

                //Console.WriteLine(series.Text + "," + (m_ChartManager.chart.Series.Count - 1).ToString() + " , " + index.ToString());
                if (index > -1)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        m_ChartManager.chart.Data.Y[index, i] = Convert.ToDouble(item.Cells[i + 3].Value);
                    }
                }

            }



            ///유량 소계 챠트
            ArrayList SDFlowlists = GetPumpDataValue(ultraGrid_SimuFlow, "SD");

            SeriesAttributes SDFlow_series = m_ChartManager.AddSeries();
            SDFlow_series.Gallery = ChartFX.WinForms.Gallery.Bar;
            SDFlow_series.Text = Convert.ToString("상동(가):(유량)");
            SDFlow_series.Visible = false;

            SDFlow_series.AxisY.Title.Text = "유량(㎥/h)";
            SDFlow_series.AxisY.Title.Alignment = StringAlignment.Far;
            SDFlow_series.AxisY.LabelsFormat.Format = AxisFormat.Number;
            SDFlow_series.AxisY.LabelsFormat.CustomFormat = "#,###,##0.#";

            int SDFlow_index = m_ChartManager.GetSeriesIndex(SDFlow_series.Text);

            if (SDFlow_index > -1)
            {
                for (int i = 0; i < SDFlowlists.Count; i++)
                {
                    m_ChartManager.chart.Data.Y[SDFlow_index, i] = Convert.ToDouble(SDFlowlists[i]);
                }
            }

            ArrayList HRFlowlists = GetPumpDataValue(ultraGrid_SimuFlow, "HR");

            SeriesAttributes HRFlow_series = m_ChartManager.AddSeries();
            HRFlow_series.Gallery = ChartFX.WinForms.Gallery.Bar;
            HRFlow_series.Text = Convert.ToString("회룡(가):(유량)");
            HRFlow_series.Visible = false;

            HRFlow_series.AxisY.Title.Text = "유량(㎥/h)";
            HRFlow_series.AxisY.Title.Alignment = StringAlignment.Far;
            HRFlow_series.AxisY.LabelsFormat.Format = AxisFormat.Number;
            HRFlow_series.AxisY.LabelsFormat.CustomFormat = "#,###,##0.#";

            int HRFlow_index = m_ChartManager.GetSeriesIndex(HRFlow_series.Text);

            if (HRFlow_index > -1)
            {
                for (int i = 0; i < SDFlowlists.Count; i++)
                {
                    m_ChartManager.chart.Data.Y[HRFlow_index, i] = Convert.ToDouble(HRFlowlists[i]);
                }
            }

            ///에너지 소계 챠트
            ArrayList SDEnergylists = GetPumpDataValue(ultraGrid_SimuEnergy, "SD");

            SeriesAttributes SDEnergy_series = m_ChartManager.AddSeries();
            SDEnergy_series.Gallery = ChartFX.WinForms.Gallery.Bar;
            SDEnergy_series.Text = Convert.ToString("상동(가):(에너지)");
            SDEnergy_series.Visible = false;

            SDEnergy_series.AxisY = AddlAxisY2;
            SDEnergy_series.AxisY.Title.Text = "에너지(kWh)";
            SDEnergy_series.AxisY.Title.Alignment = StringAlignment.Far;
            SDEnergy_series.AxisY.LabelsFormat.Format = AxisFormat.Number;
            SDEnergy_series.AxisY.LabelsFormat.CustomFormat = "#,###,##0.#";

            int SDEnergy_index = m_ChartManager.GetSeriesIndex(SDEnergy_series.Text);

            if (SDEnergy_index > -1)
            {
                for (int i = 0; i < SDEnergylists.Count; i++)
                {
                    m_ChartManager.chart.Data.Y[SDEnergy_index, i] = Convert.ToDouble(SDEnergylists[i]);
                }
            }

            ArrayList HREnergylists = GetPumpDataValue(ultraGrid_SimuEnergy, "HR");

            SeriesAttributes HREnergy_series = m_ChartManager.AddSeries();
            HREnergy_series.Gallery = ChartFX.WinForms.Gallery.Bar;
            HREnergy_series.Text = Convert.ToString("회룡(가):(에너지)");
            HREnergy_series.Visible = false;

            HREnergy_series.AxisY = AddlAxisY2;
            HREnergy_series.AxisY.Title.Text = "에너지(kWh)";
            HREnergy_series.AxisY.Title.Alignment = StringAlignment.Far;
            HREnergy_series.AxisY.LabelsFormat.Format = AxisFormat.Number;
            HREnergy_series.AxisY.LabelsFormat.CustomFormat = "#,###,##0.#";

            int HREnergy_index = m_ChartManager.GetSeriesIndex(HREnergy_series.Text);

            if (HREnergy_index > -1)
            {
                for (int i = 0; i < SDEnergylists.Count; i++)
                {
                    m_ChartManager.chart.Data.Y[HREnergy_index, i] = Convert.ToDouble(HREnergylists[i]);
                }
            }


            checkBox_Energy_CheckedChanged(this, new EventArgs());
            checkBox_Flow_CheckedChanged(this, new EventArgs());

            m_ChartManager.chart.Update();
        }
        #endregion 생성자 및 환경설정 ----------------------------------------------------------------------

        private ArrayList GetPumpDataValue(UltraGrid uGrid, string gubun)
        {
            ArrayList lists = new ArrayList();

            double dT00 = 0.0;
            double dT01 = 0.0;
            double dT02 = 0.0;
            double dT03 = 0.0;
            double dT04 = 0.0;
            double dT05 = 0.0;
            double dT06 = 0.0;
            double dT07 = 0.0;
            double dT08 = 0.0;
            double dT09 = 0.0;
            double dT10 = 0.0;
            double dT11 = 0.0;
            double dT12 = 0.0;
            double dT13 = 0.0;
            double dT14 = 0.0;
            double dT15 = 0.0;
            double dT16 = 0.0;
            double dT17 = 0.0;
            double dT18 = 0.0;
            double dT19 = 0.0;
            double dT20 = 0.0;
            double dT21 = 0.0;
            double dT22 = 0.0;
            double dT23 = 0.0;

            foreach (UltraGridRow row in uGrid.Rows)
            {
                if (Convert.ToString(row.Cells["PUMP_ID"].Value).IndexOf(gubun) > -1)
                {
                    foreach (UltraGridCell cell in row.Cells)
                    {
                        switch (cell.Column.Key)
	                    {
                            case "T00":
                                dT00 += Convert.ToDouble(cell.Value);
                                break;
                            case "T01":
                                dT01 += Convert.ToDouble(cell.Value);
                                break;
                            case "T02":
                                dT02 += Convert.ToDouble(cell.Value);
                                break;
                            case "T03":
                                dT03 += Convert.ToDouble(cell.Value);
                                break;
                            case "T04":
                                dT04 += Convert.ToDouble(cell.Value);
                                break;
                            case "T05":
                                dT05 += Convert.ToDouble(cell.Value);
                                break;
                            case "T06":
                                dT06 += Convert.ToDouble(cell.Value);
                                break;
                            case "T07":
                                dT07 += Convert.ToDouble(cell.Value);
                                break;
                            case "T08":
                                dT08 += Convert.ToDouble(cell.Value);
                                break;
                            case "T09":
                                dT09 += Convert.ToDouble(cell.Value);
                                break;
                            case "T10":
                                dT10 += Convert.ToDouble(cell.Value);
                                break;
                            case "T11":
                                dT11 += Convert.ToDouble(cell.Value);
                                break;
                            case "T12":
                                dT12 += Convert.ToDouble(cell.Value);
                                break;
                            case "T13":
                                dT13 += Convert.ToDouble(cell.Value);
                                break;
                            case "T14":
                                dT14 += Convert.ToDouble(cell.Value);
                                break;
                            case "T15":
                                dT15 += Convert.ToDouble(cell.Value);
                                break;
                            case "T16":
                                dT16 += Convert.ToDouble(cell.Value);
                                break;
                            case "T17":
                                dT17 += Convert.ToDouble(cell.Value);
                                break;
                            case "T18":
                                dT18 += Convert.ToDouble(cell.Value);
                                break;
                            case "T19":
                                dT19 += Convert.ToDouble(cell.Value);
                                break;
                            case "T20":
                                dT20 += Convert.ToDouble(cell.Value);
                                break;
                            case "T21":
                                dT21 += Convert.ToDouble(cell.Value);
                                break;
                            case "T22":
                                dT22 += Convert.ToDouble(cell.Value);
                                break;
                            case "T23":
                                dT23 += Convert.ToDouble(cell.Value);
                                break;
	                    }
                    }
                }
            }

            lists.Add(dT00);
            lists.Add(dT01);
            lists.Add(dT02);
            lists.Add(dT03);
            lists.Add(dT04);
            lists.Add(dT05);
            lists.Add(dT06);
            lists.Add(dT07);
            lists.Add(dT08);
            lists.Add(dT09);
            lists.Add(dT10);
            lists.Add(dT11);
            lists.Add(dT12);
            lists.Add(dT13);
            lists.Add(dT14);
            lists.Add(dT15);
            lists.Add(dT16);
            lists.Add(dT17);
            lists.Add(dT18);
            lists.Add(dT19);
            lists.Add(dT20);
            lists.Add(dT21);
            lists.Add(dT22);
            lists.Add(dT23);
            
            return lists;
        }
        /// <summary>
        /// 차트 데이터 시리즈 표시 여부
        /// 그리드에서 CHECKED 체크박스 선택시
        /// </summary>
        private void InitializeChartSetting(object sender, CellEventArgs e)
        {
            if (!(e.Cell.Column.Key.Equals("CHECKED"))) return;
            SeriesAttributes series = null;
            switch (((UltraGrid)sender).Name)
	        {
                case "ultraGrid_SimuFlow":
                    series = m_ChartManager.GetSeries(Convert.ToString(e.Cell.Row.Cells["PUMP_NAME"].Value) + "(유량)");
                    break;
                case "ultraGrid_SimuHead":
                    series = m_ChartManager.GetSeries(Convert.ToString(e.Cell.Row.Cells["BIZ_NAME"].Value) + "(수위)");
                    break;
                case "ultraGrid_SimuEnergy":
                    series = m_ChartManager.GetSeries(Convert.ToString(e.Cell.Row.Cells["PUMP_NAME"].Value) + "(에너지)");
                    break;
                case "ultraGrid_SimuPress":
                    series = m_ChartManager.GetSeries(Convert.ToString(e.Cell.Row.Cells["ID"].Value) + "(수압)");
                    break;
                //case "ultraGrid_SimuStatus":
                //    series = m_ChartStatusManager.GetSeries(Convert.ToString(e.Cell.Row.Cells["PUMP_NAME"].Value));
                //    break;
	        }
            if (series == null) return;
            if (e.Cell.Text.Equals("True"))
                series.Visible = true;
            else
                series.Visible = false;
            
        }

        public void Open()
        {
            ///모달리스 폼
            frmPumpSimuConditionSet oform = new frmPumpSimuConditionSet();
            oform.Owner = this;
            oform.mainform = this;
            oform.Open();
        }

        private void btnCurve_Click(object sender, EventArgs e)
        {
            ///모달 폼(OpenDialog)
            frmPumpCurve oform = new frmPumpCurve();
            oform.Owner = this;
            oform.StartPosition = FormStartPosition.CenterParent;
            oform.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ///모달리스 폼
            frmPumpSimuConditionSet oform = new frmPumpSimuConditionSet();
            oform.Owner = this;
            oform.mainform = this;
            oform.Open();
        }

        /// <summary>
        /// 기본 정보 화면 보기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInfo_Click(object sender, EventArgs e)
        {
            ///모달 폼(OpenDialog)
            frmBizPlace oform = new frmBizPlace();
            oform.Owner = this;
            oform.StartPosition = FormStartPosition.CenterParent;
            oform.Open();
        }

        private void btnGraph_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1Collapsed = false;
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;
        }

        private void btnTable_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = false;
            splitContainer1.Panel1Collapsed = !splitContainer1.Panel1Collapsed;
        }

        private void frmPumpSimulator_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================에너지관리(펌프에너지 시뮬레이션)

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuPumpSchedule"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                
            }

            //===========================================================================

            ultraGrid_SimuFlow.CellChange += new CellEventHandler(InitializeChartSetting);
            ultraGrid_SimuHead.CellChange += new CellEventHandler(InitializeChartSetting);
            ultraGrid_SimuEnergy.CellChange += new CellEventHandler(InitializeChartSetting);
            ultraGrid_SimuPress.CellChange += new CellEventHandler(InitializeChartSetting);

            //ultraGrid_SimuStatus.CellChange += new CellEventHandler(InitializeChartSetting);
        }

        private void ultraGrid_SimuStatus_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            foreach (UltraGridCell cell in e.Row.Cells)
            {
                switch (cell.Column.Key)
                {
                    case "PUMP_ID":
                    case "PUMP_NAME":
                    //case "CHECKED":
                    case "TOT":
                        break;
                    default:
                        if (Convert.ToString(cell.Value).Equals("1")) cell.Appearance.BackColor = Color.Yellow;
                        else cell.Appearance.BackColor = Color.Empty;
                        break;
                }
            }
        }

        private void ultraGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {

        }

        public void SetSummaryRows()
        {
            this.ultraGrid_SimuStatus.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Summaries.Clear();

            foreach (UltraGridColumn column in ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;
                if (column.Key.Equals("PUMP_NAME"))
                {
                    summary = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Summaries.Add("합계", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "전체합계";

                    summary = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Summaries.Add("상동가압장", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "상동가압장";

                    summary = ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Summaries.Add("회룡가압장", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "회룡가압장";                    
                }


                if (column.Style != Infragistics.Win.UltraWinGrid.ColumnStyle.Double) continue;

                summary = this.ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, column, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,##0.00}";
                summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_SUM";

                summary = this.ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Sum_SD_FilterCalculator(), column, SummaryPosition.UseSummaryPositionColumn, column);
                summary.DisplayFormat = "{0:###,##0.00}";
                summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_SD_SUB";

                summary = this.ultraGrid_SimuEnergy.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Sum_HR_FilterCalculator(), column, SummaryPosition.UseSummaryPositionColumn, column);
                summary.DisplayFormat = "{0:###,##0.00}";
                summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_HR_SUB";


            }


            this.ultraGrid_SimuFlow.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (UltraGridColumn column in ultraGrid_SimuFlow.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;
                if (column.Key.Equals("PUMP_NAME"))
                {
                    summary = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Summaries.Add("합계", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "전체합계";

                    summary = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Summaries.Add("상동가압장", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "상동가압장";

                    summary = ultraGrid_SimuFlow.DisplayLayout.Bands[0].Summaries.Add("회룡가압장", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "회룡가압장";
                }

                if (column.Style != Infragistics.Win.UltraWinGrid.ColumnStyle.Double) continue;

                summary = this.ultraGrid_SimuFlow.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, column, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,##0.00}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_SUM";

                summary = this.ultraGrid_SimuFlow.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Sum_SD_FilterCalculator(), column, SummaryPosition.UseSummaryPositionColumn, column);
                summary.DisplayFormat = "{0:###,##0.00}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_SD_SUB";

                summary = this.ultraGrid_SimuFlow.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Sum_HR_FilterCalculator(), column, SummaryPosition.UseSummaryPositionColumn, column);
                summary.DisplayFormat = "{0:###,##0.00}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_HR_SUB";

            }
            
            this.ultraGrid_SimuStatus.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (UltraGridColumn column in ultraGrid_SimuStatus.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;
                if (column.Key.Equals("PUMP_NAME"))
                {
                    summary = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Summaries.Add("합계", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "전체합계";

                    summary = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Summaries.Add("상동가압장", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "상동가압장";

                    summary = ultraGrid_SimuStatus.DisplayLayout.Bands[0].Summaries.Add("회룡가압장", SummaryType.Formula, column);
                    summary.SummaryDisplayArea = SummaryDisplayAreas.Bottom | SummaryDisplayAreas.GroupByRowsFooter;
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.Appearance.TextHAlign = HAlign.Right;
                    summary.DisplayFormat = "회룡가압장";
                }

                if (column.Style != Infragistics.Win.UltraWinGrid.ColumnStyle.Double) continue;

                summary = this.ultraGrid_SimuStatus.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, column, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:#0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.True;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_SUM";

                summary = this.ultraGrid_SimuStatus.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Sum_SD_FilterCalculator(), column, SummaryPosition.UseSummaryPositionColumn, column);
                summary.DisplayFormat = "{0:#0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_SD_SUB";

                summary = this.ultraGrid_SimuStatus.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Sum_HR_FilterCalculator(), column, SummaryPosition.UseSummaryPositionColumn, column);
                summary.DisplayFormat = "{0:#0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = column.Key + "_HR_SUB";
            }
        }

        private void checkBox_Flow_CheckedChanged(object sender, EventArgs e)
        {
            SeriesAttributes series = null;
            series = m_ChartManager.GetSeries(Convert.ToString("상동(가):(유량)"));
            if (series != null) series.Visible = checkBox_Flow.Checked;
            series = m_ChartManager.GetSeries(Convert.ToString("회룡(가):(유량)"));
            if (series != null) series.Visible = checkBox_Flow.Checked;
        }

        private void checkBox_Energy_CheckedChanged(object sender, EventArgs e)
        {
            SeriesAttributes series = null;
            series = m_ChartManager.GetSeries(Convert.ToString("상동(가):(에너지)"));
            if (series != null) series.Visible = checkBox_Energy.Checked;
            series = m_ChartManager.GetSeries(Convert.ToString("회룡(가):(에너지)"));
            if (series != null) series.Visible = checkBox_Energy.Checked;
        }

        private void checkBox_Head_CheckedChanged(object sender, EventArgs e)
        {
            foreach (UltraGridRow row in ultraGrid_SimuHead.Rows)
            {
                row.Cells["CHECKED"].Value = checkBox_Head.Checked ? "True" : "False";
                InitializeChartSetting(ultraGrid_SimuHead, new CellEventArgs(row.Cells["CHECKED"]));
            }
        }

        private void checkBox_Junction_CheckedChanged(object sender, EventArgs e)
        {
            foreach (UltraGridRow row in ultraGrid_SimuPress.Rows)
            {
                row.Cells["CHECKED"].Value = checkBox_Junction.Checked ? "True" : "False";
                InitializeChartSetting(ultraGrid_SimuPress, new CellEventArgs(row.Cells["CHECKED"]));
            }
        }

    }
}
