﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
using WaterNet.WE_Common;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion UltraGrid를 사용=>namespace선언

namespace WaterNet.WE_EnergyAnalysis
{
    public partial class frmPumpCurve : Form
    {
        /// <summary>
        /// 폼을 최소화하기 위한 멤버선언
        /// </summary>
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();

        private GridManager m_GridPumpCurveManager = null;  //펌프커브 그리드
        private GridManager m_GridEffCurveManager = null;  //효율커브 그리드

        #region 생성자 및 환경설정 ----------------------------------------------------------------------
        public frmPumpCurve()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;
            #region 펌프커브 그리드
            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "GUBUN";
            oUltraGridColumn.Header.Caption = "GUBUN";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "X";
            oUltraGridColumn.Header.Caption = "유량(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "#,##0.###";
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Y";
            oUltraGridColumn.Header.Caption = "양정(m)";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "#,##0.###";
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "IDX";
            oUltraGridColumn.Header.Caption = "순서";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            m_GridPumpCurveManager = new GridManager(ultraGrid_Pump);
            m_GridPumpCurveManager.SetGridStyle_Append();
            //m_GridPumpCurveManager.PerformAutoResize();
            #endregion 펌프커브 그리드

            #region 효율커브 그리드
            oUltraGridColumn = ultraGrid_Eff.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "GUBUN";
            oUltraGridColumn.Header.Caption = "GUBUN";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Eff.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Eff.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "X";
            oUltraGridColumn.Header.Caption = "유량(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "#,##0.###";
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Eff.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Y";
            oUltraGridColumn.Header.Caption = "효율";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "#,##0.###";
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Eff.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "IDX";
            oUltraGridColumn.Header.Caption = "순서";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            m_GridEffCurveManager = new GridManager(ultraGrid_Eff);
            m_GridEffCurveManager.SetGridStyle_Append();
            //m_GridEffCurveManager.PerformAutoResize();
            #endregion 효율커브 그리드

            #endregion 그리드 설정

            SetQueryCurveID();
        }

        #endregion 초기화설정
        #endregion 생성자 및 환경설정 ----------------------------------------------------------------------

        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    int deskHeight = Screen.PrimaryScreen.Bounds.Height;
                    int deskWidth = Screen.PrimaryScreen.Bounds.Width;

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                    {
                        if (this.Owner != null)
                            this.Location = new System.Drawing.Point(this.Owner.Width - 170, this.Owner.Height - 40);
                        else
                            this.Location = new System.Drawing.Point(0, 0);
                    }
                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                //else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                //{
                //    return;
                //}
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        /// <summary>
        /// 폼 로드 : 그리드의 이벤트설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPumpCurve_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuPumpSchedule"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
                this.btnPumpDelete.Enabled = false;
                this.btnEffDelete.Enabled = false;
            }

            //===========================================================================

            this.ultraGrid_Pump.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.ultraGrid_Pump.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.ultraGrid_Eff.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.ultraGrid_Eff.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);

            this.cboEffID.SelectedValueChanged += new EventHandler(cboEffID_TextUpdate);
            this.cboCurveID.SelectedValueChanged += new EventHandler(cboCurveID_TextUpdate);
        }

        /// <summary>
        /// 커브ID, 효율ID 콤보박스 설정
        /// </summary>
        private void SetQueryCurveID()
        {
            Oracle.DataAccess.Client.OracleDataReader oReader1 = null;
            Oracle.DataAccess.Client.OracleDataReader oReader2 = null;
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();
                ///펌프 커브
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT DISTINCT ID             ");
                oStringBuilder.AppendLine("  FROM WE_CURVEDATA            ");
                oStringBuilder.AppendLine(" WHERE GUBUN = '1'             ");
                oStringBuilder.AppendLine(" ORDER BY ID                   ");
                oReader1 = oDBManager.ExecuteScriptDataReader(oStringBuilder.ToString(), null);
                while (oReader1.Read())
                {
                    cboCurveID.Items.Add(oReader1["ID"]);
                }
                if (cboCurveID.Items.Count > 0)
                {
                    cboCurveID.SelectedIndex = 0;
                    cboCurveID_TextUpdate(this, new EventArgs());
                }

                //효율 커브
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT DISTINCT ID             ");
                oStringBuilder.AppendLine("  FROM WE_CURVEDATA            ");
                oStringBuilder.AppendLine(" WHERE GUBUN = '2'             ");
                oStringBuilder.AppendLine(" ORDER BY ID                   ");
                oReader2 = oDBManager.ExecuteScriptDataReader(oStringBuilder.ToString(), null);
                while (oReader2.Read())
                {
                    cboEffID.Items.Add(oReader2["ID"]);
                }
                if (cboEffID.Items.Count > 0)
                {
                    cboEffID.SelectedIndex = 0;

                    cboEffID_TextUpdate(this, new EventArgs());
                }
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oReader1 != null) oReader1.Close();
                if (oReader2 != null) oReader2.Close();
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 화면 불러오기
        /// </summary>
        public void Open()
        {
            
            this.ShowDialog();

        }

        #region 그리드 Validation 이벤트
        private void ultraGrid_Pump_AfterRowUpdate(object sender, RowEventArgs e)
        {
            bool b = false;
            b = (Convert.IsDBNull(e.Row.Cells["ID"].Value) ? true : false);
            b = (Convert.IsDBNull(e.Row.Cells["X"].Value) ? true : false);
            b = (Convert.IsDBNull(e.Row.Cells["Y"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["IDX"].Value) ? true : false);

            if (b)   e.Row.Delete(false);
        }

        private void ultraGrid_Eff_AfterRowUpdate(object sender, RowEventArgs e)
        {
            bool b = false;
            b = (Convert.IsDBNull(e.Row.Cells["ID"].Value) ? true : false);
            b = (Convert.IsDBNull(e.Row.Cells["X"].Value) ? true : false);
            b = (Convert.IsDBNull(e.Row.Cells["Y"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["IDX"].Value) ? true : false);

            if (b) e.Row.Delete(false);
        }


        private void ultraGrid_Pump_AfterRowInsert(object sender, RowEventArgs e)
        {
            /// 1 : 펌프 커브
            e.Row.Cells["GUBUN"].Value = "1";
            e.Row.Cells["ID"].Value = cboCurveID.Text;
        }

        private void ultraGrid_Eff_AfterRowInsert(object sender, RowEventArgs e)
        {
            /// 2 : 효율 커브
            e.Row.Cells["GUBUN"].Value = "2";
            e.Row.Cells["ID"].Value = cboEffID.Text;
        }


        /// <summary>
        /// 펌프 커브 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPumpDelete_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid_Pump.Selected.Rows.Count == 0) return;
            UltraGridRow row = this.ultraGrid_Pump.Selected.Rows[0];
            if (row == null) return;
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("DELETE FROM WE_CURVEDATA ");
                oStringBuilder.AppendLine(" WHERE GUBUN = '1'");
                oStringBuilder.AppendLine("   AND IDX = '" + row.Cells["IDX"].Value + "'");
                oStringBuilder.AppendLine("   AND ID  = '" + row.Cells["ID"].Value + "'");

                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                row.Delete(false);
                ultraGrid_Eff.UpdateData();

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }  
        }

        /// <summary>
        /// 효율 커브 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEffDelete_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid_Eff.Selected.Rows.Count == 0) return;
            UltraGridRow row = this.ultraGrid_Eff.Selected.Rows[0];
            if (row == null) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("DELETE FROM WE_CURVEDATA ");
                oStringBuilder.AppendLine(" WHERE GUBUN = '2'"       );
                oStringBuilder.AppendLine("   AND IDX = '" + row.Cells["IDX"].Value + "'");
                oStringBuilder.AppendLine("   AND ID  = '" + row.Cells["ID"].Value + "'");

                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                row.Delete(false);
                ultraGrid_Eff.UpdateData();

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }  
        }

        #endregion 그리드 Validation 이벤트

        /// <summary>
        /// 화면 닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 저장 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
                if (eDialogResult != DialogResult.Yes) return;

                OracleDBManager oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

                try
                {
                    oDBManager.Open();
                    oDBManager.BeginTransaction();

                    foreach (UltraGridRow item in ultraGrid_Pump.Rows)
                    {
                        if (item.Tag == null) continue;
                        if (item.Tag.Equals('I'))
                        {
                            InsertProcess(ref oDBManager, item);
                        }
                        else if (item.Tag.Equals('U'))
                        {
                            UpdateProcess(ref oDBManager, item);
                        }
                    }

                    foreach (UltraGridRow item in ultraGrid_Eff.Rows)
                    {
                        if (item.Tag == null) continue;
                        if (item.Tag.Equals('I'))
                        {
                            InsertProcess(ref oDBManager, item);
                        }
                        else if (item.Tag.Equals('U'))
                        {
                            UpdateProcess(ref oDBManager, item);
                        }
                    }

                    oDBManager.CommitTransaction();
                    MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);

                }
                catch (Exception oException)
                {
                    oDBManager.RollbackTransaction();
                    MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                    //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
                }
                finally
                {
                    if (oDBManager != null) oDBManager.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        ////////////////데이터베이스 Query////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Insert Process
        /// </summary>
        /// <returns></returns>
        private void InsertProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("INSERT INTO WE_CURVEDATA (");
            oStringBuilder.AppendLine("GUBUN  ,");
            oStringBuilder.AppendLine("ID     ,");
            oStringBuilder.AppendLine("IDX    ,");
            oStringBuilder.AppendLine("X_VALUE,");
            oStringBuilder.AppendLine("Y_VALUE)");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine(" :GUBUN  ,");
            oStringBuilder.AppendLine(" :ID     ,");
            oStringBuilder.AppendLine(" :IDX    ,");
            oStringBuilder.AppendLine(" :X_VALUE,");
            oStringBuilder.AppendLine(" :Y_VALUE)");

            IDataParameter[] oParams = {
                new OracleParameter(":GUBUN",OracleDbType.Varchar2,1),
                new OracleParameter(":ID",OracleDbType.Varchar2,10),
                new OracleParameter(":IDX",OracleDbType.Int32,10),
                new OracleParameter(":X_VALUE",OracleDbType.Double,10),
                new OracleParameter(":Y_VALUE",OracleDbType.Double,10)
            };

            oParams[0].Value = oRow.Cells["GUBUN"].Value;  
            oParams[1].Value = oRow.Cells["ID"].Value;     
            oParams[2].Value = Convert.ToString(oRow.Cells["IDX"].Value);    
            oParams[3].Value = oRow.Cells["X"].Value;
            oParams[4].Value = oRow.Cells["Y"].Value;


            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }

        /// <summary>
        /// UpdateProcess
        /// </summary>
        /// <returns></returns>
        private void UpdateProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE WE_CURVEDATA SET ");
            oStringBuilder.AppendLine("       IDX   = :IDX ,");
            oStringBuilder.AppendLine("       X_VALUE = :X_VALUE,");
            oStringBuilder.AppendLine("       Y_VALUE = :Y_VALUE ");
            oStringBuilder.AppendLine("WHERE  GUBUN = :GUBUN ");
            oStringBuilder.AppendLine("  AND  ID =    :ID    ");
            oStringBuilder.AppendLine("  AND  IDX =   :IDX2    ");


            IDataParameter[] oParams = {
                new OracleParameter(":IDX",OracleDbType.Int32,10),
                new OracleParameter(":X_VALUE",OracleDbType.Double,10),
                new OracleParameter(":Y_VALUE",OracleDbType.Double,10),
                new OracleParameter(":GUBUN",OracleDbType.Varchar2,1),
                new OracleParameter(":ID",OracleDbType.Varchar2,10),
                new OracleParameter(":IDX2",OracleDbType.Int32,10)
            };


            oParams[0].Value = Convert.ToString(oRow.Cells["IDX"].Value);
            oParams[1].Value = oRow.Cells["X"].Value;
            oParams[2].Value = oRow.Cells["Y"].Value;
            oParams[3].Value = oRow.Cells["GUBUN"].Value;
            oParams[4].Value = oRow.Cells["ID"].Value;
            oParams[5].Value = Convert.ToString(oRow.Cells["IDX"].OriginalValue);

            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }

        /// <summary>
        /// 펌프 커브 ID : 콤보박스 값이 변경시 해당 데이터 질의
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboCurveID_TextUpdate(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Convert.ToString(cboCurveID.Text))) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();
                ///펌프 커브
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT                         ");
                oStringBuilder.AppendLine("       GUBUN                   ");
                oStringBuilder.AppendLine("      ,ID                      ");
                oStringBuilder.AppendLine("      ,X_VALUE AS X            ");
                oStringBuilder.AppendLine("      ,Y_VALUE AS Y            ");
                oStringBuilder.AppendLine("      ,IDX                     ");
                oStringBuilder.AppendLine("  FROM WE_CURVEDATA            ");
                oStringBuilder.AppendLine(" WHERE GUBUN = '1'             ");
                oStringBuilder.AppendLine("   AND ID = '" + cboCurveID.Text + "'");
                oStringBuilder.AppendLine(" ORDER BY GUBUN, ID, TO_NUMBER(X_VALUE) ASC   ");
                DataTable oDatatable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                m_GridPumpCurveManager.SetDataSource(oDatatable);

                Calc((UltraGrid)ultraGrid_Pump);
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 효율커브 ID : 콤보박스 값이 변경시 해당 데이터 질의
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboEffID_TextUpdate(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Convert.ToString(cboEffID.Text))) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                //효율 커브
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT                         ");
                oStringBuilder.AppendLine("       GUBUN                   ");
                oStringBuilder.AppendLine("      ,ID                      ");
                oStringBuilder.AppendLine("      ,X_VALUE AS X            ");
                oStringBuilder.AppendLine("      ,Y_VALUE AS Y            ");
                oStringBuilder.AppendLine("      ,IDX                     ");
                oStringBuilder.AppendLine("  FROM WE_CURVEDATA            ");
                oStringBuilder.AppendLine(" WHERE GUBUN = '2'             ");
                oStringBuilder.AppendLine("   AND ID = '" + cboEffID.Text + "'");
                oStringBuilder.AppendLine(" ORDER BY GUBUN, ID, TO_NUMBER(X_VALUE) ASC   ");
                DataTable oDatatable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                m_GridEffCurveManager.SetDataSource(oDatatable);

                Calc((UltraGrid)ultraGrid_Eff);
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        private void Calc2(UltraGrid ugGrid)
        {

        }

        private void Calc(UltraGrid ugGrid)
        {
            double xsum = 0.0;
            double ysum = 0.0;
            double x2sum = 0.0;
            double y2sum = 0.0;
            double xysum = 0.0;
            StringBuilder o = new StringBuilder();
            foreach (UltraGridRow item in ugGrid.Rows)
            {
                xsum += Convert.ToDouble(item.Cells["X"].Value);
                ysum += Convert.ToDouble(item.Cells["Y"].Value);
                x2sum += Convert.ToDouble(item.Cells["X"].Value) * Convert.ToDouble(item.Cells["X"].Value);
                y2sum += Math.Log(Convert.ToDouble(item.Cells["Y"].Value)) * Math.Log(Convert.ToDouble(item.Cells["Y"].Value));
                xysum += Convert.ToDouble(item.Cells["X"].Value) * Math.Log(Convert.ToDouble(item.Cells["Y"].Value));

                o.AppendLine(item.Cells["X"].Value + "\t" + item.Cells["Y"].Value + "\t" + xsum.ToString() + "\t" + ysum.ToString() + "\t" + x2sum.ToString() + "\t" + y2sum.ToString() + "\t" + xysum.ToString());
            }

            Console.WriteLine(o.ToString());

        }

    }
}
