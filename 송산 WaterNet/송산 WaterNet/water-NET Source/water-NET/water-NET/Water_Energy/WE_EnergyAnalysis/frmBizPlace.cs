﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
using WaterNet.WE_Common;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
#endregion UltraGrid를 사용=>namespace선언

namespace WaterNet.WE_EnergyAnalysis
{
    public partial class frmBizPlace : Form
    {
        /// <summary>
        /// 폼을 최소화하기 위한 멤버선언
        /// </summary>
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();

        private GridManager m_GridBizManager = null;  //사업장 그리드
        private GridManager m_GridTagManager = null;  //TAG 그리드
        private GridManager m_GridPumpManager = null;  //펌프 그리드
        private GridManager m_GridJunctionManager = null;  //수압감시지점 그리드

        private OracleDBManager m_oDBManager = new OracleDBManager();  ///데이터베이스

        #region 생성자 및 환경설정 ----------------------------------------------------------------------
        public frmBizPlace()
        {
            InitializeComponent();
        
            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            try
            {
                m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                m_oDBManager.Open();
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;
            #region 사업장 그리드
            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_ID";
            oUltraGridColumn.Header.Caption = "항목ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_NAME";
            oUltraGridColumn.Header.Caption = "사업장명";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INT_LEVEL";
            oUltraGridColumn.Header.Caption = "초기수위";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Format = "##.#";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MIN_LEVEL";
            oUltraGridColumn.Header.Caption = "최소수위";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Format = "##.#";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MAX_LEVEL";
            oUltraGridColumn.Header.Caption = "최대수위";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Format = "##.#";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ELEV";
            oUltraGridColumn.Header.Caption = "관저고";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Format = "###.##";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Biz.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORDERS";
            oUltraGridColumn.Header.Caption = "순서";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "#";
            oUltraGridColumn.Hidden = false;

            m_GridBizManager = new GridManager(ultraGrid_Biz);
            m_GridBizManager.SetGridStyle_Append();
            m_GridBizManager.ColumeNoEdit("INT_LEVEL");
            m_GridBizManager.ColumeNoEdit("MIN_LEVEL");
            m_GridBizManager.ColumeNoEdit("MAX_LEVEL");
            #endregion 사업장 그리드

            #region TAG 그리드
            oUltraGridColumn = ultraGrid_Tag.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Tag.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_ID";
            oUltraGridColumn.Header.Caption = "항목ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Tag.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "계측기TAG";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 200;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Tag.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BR_CODE";
            oUltraGridColumn.Header.Caption = "기호";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Tag.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DESCRIPTION";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;

            m_GridTagManager = new GridManager(ultraGrid_Tag);
            m_GridTagManager.SetGridStyle_Append();
            m_GridTagManager.ColumeNoEdit("BR_CODE");
            m_GridTagManager.ColumeNoEdit("DESCRIPTION");
            #endregion TAG 그리드

            #region 펌프 그리드
            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_ID";
            oUltraGridColumn.Header.Caption = "항목ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_ID";
            oUltraGridColumn.Header.Caption = "항목ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_NAME";
            oUltraGridColumn.Header.Caption = "펌프번호";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPCURVE_ID";
            oUltraGridColumn.Header.Caption = "펌프커브ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;
            SetValuelist_PumpCurve();

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EFFCURVE_ID";
            oUltraGridColumn.Header.Caption = "효율커브ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;
            SetValuelist_EffCurve();

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FREQ";
            oUltraGridColumn.Header.Caption = "주파수";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;
            SetValueList_Freq();

            m_GridPumpManager = new GridManager(ultraGrid_Pump);
            m_GridPumpManager.SetGridStyle_Append();
            #endregion 펌프 그리드

            #region 수압감시지점 그리드
            oUltraGridColumn = ultraGrid_Junction.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Junction.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Junction.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ELEV";
            oUltraGridColumn.Header.Caption = "ELEV";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Junction.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DEMAND";
            oUltraGridColumn.Header.Caption = "DEMAND";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Hidden = false;

            m_GridJunctionManager = new GridManager(ultraGrid_Junction);
            m_GridJunctionManager.SetGridStyle_Append();
            m_GridJunctionManager.ColumeNoEdit("ELEV");
            m_GridJunctionManager.ColumeNoEdit("DEMAND");
            #endregion 펌프 그리드

            #endregion 그리드 설정

            StringBuilder oStringBuilder = new StringBuilder();
            #region ComboBox 설정
            oStringBuilder.AppendLine("SELECT INP_NUMBER AS CD, TITLE || '(' || INP_NUMBER || ')' AS CDNM FROM WH_TITLE ");
            oStringBuilder.AppendLine(" WHERE USE_GBN = 'EN'                                                            ");
            oStringBuilder.AppendLine(" ORDER BY INS_DATE DESC                                                          ");
            WaterNetCore.FormManager.SetComboBoxEX(cboModel, oStringBuilder.ToString(), false);
            #endregion
        }

        #endregion 초기화설정
        #endregion 생성자 및 환경설정 ----------------------------------------------------------------------

        //========================================================================
        //
        //                    동진 수정_2012.6.07
        //                      권한박탈(조회만 가능)       
        //========================================================================

        private void frmBizPlace_Load(object sender, EventArgs e)
        {

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuPumpSchedule"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnBizSave.Enabled = false;
                this.btnBizDelete.Enabled = false;

                this.btnTagSave.Enabled = false;
                this.btnTagDelete.Enabled = false;

                this.btnPumpSave.Enabled = false;
                this.btnPumpDelete.Enabled = false;

                this.btnJunctionSave.Enabled = false;
                this.btnJunctionDelete.Enabled = false;
            }

            //===========================================================================
        }
        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    int deskHeight = Screen.PrimaryScreen.Bounds.Height;
                    int deskWidth = Screen.PrimaryScreen.Bounds.Width;

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                    {
                        if (this.Owner != null)
                            this.Location = new System.Drawing.Point(this.Owner.Width - 170, this.Owner.Height - 40);
                        else
                            this.Location = new System.Drawing.Point(0, 0);
                    }
                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                //else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                //{
                //    return;
                //}
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        private void SetValuelist_PumpCurve()
        {

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT DISTINCT ID AS CODE, ID AS VALUE ");
            oStringBuilder.AppendLine("  FROM   WE_CURVEDATA                   ");
            oStringBuilder.AppendLine(" WHERE GUBUN = '1'");
            oStringBuilder.AppendLine(" ORDER BY ID ASC  ");

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(oStringBuilder.ToString());
            this.ultraGrid_Pump.DisplayLayout.Bands[0].Columns["PUMPCURVE_ID"].ValueList = objValue;

        }

        private void SetValuelist_EffCurve()
        {
             StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT DISTINCT ID AS CODE, ID AS VALUE ");
            oStringBuilder.AppendLine("  FROM   WE_CURVEDATA                   ");
            oStringBuilder.AppendLine(" WHERE GUBUN = '2'");
            oStringBuilder.AppendLine(" ORDER BY ID ASC  ");

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(oStringBuilder.ToString());
            this.ultraGrid_Pump.DisplayLayout.Bands[0].Columns["EFFCURVE_ID"].ValueList = objValue;

        }

        /// <summary>
        /// 주파수 Valuelist 설정 : 펌프특성 그리드 콤보박스
        /// </summary>
        private void SetValueList_Freq()
        {
            object[,] aoSource = {
                {"30",		"30"},
                {"31",		"31"},
                {"32",		"32"},
                {"33",		"33"},
                {"34",		"34"},
                {"35",		"35"},
                {"36",		"36"},
                {"37",		"37"},
                {"38",		"38"},
                {"39",		"39"},
                {"40",		"40"},
                {"41",		"41"},
                {"42",		"42"},
                {"43",		"43"},
                {"44",		"44"},
                {"45",		"45"},
                {"46",		"46"},
                {"47",		"47"},
                {"48",		"48"},
                {"49",		"49"},
                {"50",		"50"},
                {"51",		"51"},
                {"52",		"52"},
                {"53",		"53"},
                {"54",		"54"},
                {"55",		"55"},
                {"56",		"56"},
                {"57",		"57"},
                {"58",		"58"},
                {"59",		"59"},
                {"60",		"60"}
            };

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(aoSource);
            this.ultraGrid_Pump.DisplayLayout.Bands[0].Columns["FREQ"].ValueList = objValue;
        }

        #region 버튼 클릭 이벤트
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 사업장 삭제 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBizDelete_Click(object sender, EventArgs e)
        {
            UltraGridRow row = this.ultraGrid_Biz.ActiveRow;
            if (row == null) return;

            DialogResult eDialogResult = MessageManager.ShowYesNoMessage("펌프정보와 함께 지워집니다. 그래도 " + MessageManager.MSG_QUERY_DELETE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();

                oDBManager.BeginTransaction();
                StringBuilder oStringBuilder = new StringBuilder();

                oStringBuilder.AppendLine("DELETE FROM WE_BIZPLA    ");
                oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + row.Cells["INP_NUMBER"].Value + "'");
                oStringBuilder.AppendLine("   AND MODEL_ID = '" + row.Cells["MODEL_ID"].Value + "'");
                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                oDBManager.CommitTransaction();

                row.Delete(false);
                ultraGrid_Biz.UpdateData();

                m_GridPumpManager.InitializeGrid();
                m_GridTagManager.InitializeGrid();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);
            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 사업장 저장 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBizSave_Click(object sender, EventArgs e)
        {
            DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();

                oDBManager.BeginTransaction();

                foreach (UltraGridRow item in ultraGrid_Biz.Rows)
                {
                    if (item.Tag == null) continue;
                    if (item.Tag.Equals('I'))
                    {
                        BIZ_InsertProcess(ref oDBManager, item);
                    }
                    else if (item.Tag.Equals('U'))
                    {
                        BIZ_UpdateProcess(ref oDBManager, item);
                    }
                }

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }

        }


        /// <summary>
        /// TAG 저장 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTagSave_Click(object sender, EventArgs e)
        {
            DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();

                oDBManager.BeginTransaction();

                foreach (UltraGridRow item in ultraGrid_Tag.Rows)
                {
                    if (item.Tag == null) continue;
                    if (item.Tag.Equals('I'))
                    {
                        TAG_InsertProcess(ref oDBManager, item);
                    }
                    else if (item.Tag.Equals('U'))
                    {
                        TAG_UpdateProcess(ref oDBManager, item);
                    }
                }

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }

        }

        /// <summary>
        /// TAG 삭제 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTagDelete_Click(object sender, EventArgs e)
        {
            UltraGridRow row = this.ultraGrid_Tag.ActiveRow;
            if (row == null) return;

            DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_DELETE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("DELETE FROM WE_ENERGY_TAGS ");
                oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + row.Cells["INP_NUMBER"].Value + "'");
                oStringBuilder.AppendLine("   AND MODEL_ID = '" + row.Cells["MODEL_ID"].Value + "'");
                oStringBuilder.AppendLine("   AND TAGNAME  = '" + row.Cells["TAGNAME"].Value + "'");

                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                row.Delete(false);

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// TAG 새로 고침 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTagRefresh_Click(object sender, EventArgs e)
        {
            ultraGrid_Biz_AfterSelectChange(this, new AfterSelectChangeEventArgs(null));
        }

        /// <summary>
        /// 사업장 새로 고침 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBizRefresh_Click(object sender, EventArgs e)
        {
            cboModel_SelectedIndexChanged(this, new EventArgs());
        }


        /// <summary>
        /// 펌프 새로고침 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPumpRefresh_Click(object sender, EventArgs e)
        {
            ultraGrid_Biz_AfterSelectChange(this, new AfterSelectChangeEventArgs(null));
        }

        /// <summary>
        /// 펌프 삭제 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPumpDelete_Click(object sender, EventArgs e)
        {
            UltraGridRow row = this.ultraGrid_Pump.ActiveRow;
            if (row == null) return;

            DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_DELETE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("DELETE FROM WE_PUMPINFO ");
                oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + row.Cells["INP_NUMBER"].Value +  "'");
                oStringBuilder.AppendLine("   AND MODEL_ID = '" + row.Cells["MODEL_ID"].Value + "'");
                oStringBuilder.AppendLine("   AND PUMP_ID  = '" + row.Cells["PUMP_ID"].Value + "'");

                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                row.Delete(false);
                ultraGrid_Pump.UpdateData();

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 펌프 저장 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPumpSave_Click(object sender, EventArgs e)
        {
            DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();

                foreach (UltraGridRow item in ultraGrid_Pump.Rows)
                {
                    if (item.Tag == null) continue;
                    if (item.Tag.Equals('I'))
                    {
                        PUMP_InsertProcess(ref oDBManager, item);
                    }
                    else if (item.Tag.Equals('U'))
                    {
                        PUMP_UpdateProcess(ref oDBManager, item);
                    }
                }


                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }

        }

        /// <summary>
        /// 수압감시지점 새로고침 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnJunctionRefresh_Click(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.INP_NUMBER, A.ID, B.ELEV, B.DEMAND ");
            oStringBuilder.AppendLine("  FROM  WE_PRES_MONITOR A                   ");
            oStringBuilder.AppendLine("       ,WH_JUNCTIONS B                      ");
            oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = B.INP_NUMBER          ");
            oStringBuilder.AppendLine("   AND A.ID = B.ID                          ");
            oStringBuilder.AppendLine("   AND A.INP_NUMBER = '" + cboModel.SelectedValue + "'");

            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            m_GridJunctionManager.SetDataSource(oDatatable);
        }

        /// <summary>
        /// 수압감시지점 저장 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnJunctionSave_Click(object sender, EventArgs e)
        {
            DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();


                foreach (UltraGridRow oRow in ultraGrid_Junction.Rows)
                {
                    if (oRow.Tag == null) continue;

                    StringBuilder oStringBuilder = new StringBuilder();
                    if (oRow.Tag.Equals('I'))
                    {
                        oStringBuilder.AppendLine("INSERT INTO WE_PRES_MONITOR (");
                        oStringBuilder.AppendLine("  INP_NUMBER,          ");
                        oStringBuilder.AppendLine("  ID     )             ");
                        oStringBuilder.AppendLine("VALUES (");
                        oStringBuilder.AppendLine("  :INP_NUMBER,          ");
                        oStringBuilder.AppendLine("  :ID     )             ");

                        IDataParameter[] oParams = {
                            new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2,48),
                            new OracleParameter(":ID",OracleDbType.Varchar2,50)
                        };

                        oParams[0].Value = oRow.Cells["INP_NUMBER"].Value;
                        oParams[1].Value = oRow.Cells["ID"].Value;

                        oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                    }
                    else if (oRow.Tag.Equals('U'))
                    {
                        oStringBuilder.AppendLine("UPDATE WE_PRES_MONITOR SET           ");
                        oStringBuilder.AppendLine("        ID   = :ID                   ");
                        oStringBuilder.AppendLine("WHERE  INP_NUMBER = :INP_NUMBER   ");
                        oStringBuilder.AppendLine("  AND  ID =   :ID2    ");

                        IDataParameter[] oParams = {
                            new OracleParameter(":ID",OracleDbType.Varchar2, 50),
                            new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2, 48),
                            new OracleParameter(":ID2",OracleDbType.Varchar2, 50)
                        };


                        oParams[0].Value = oRow.Cells["ID"].Value;
                        oParams[1].Value = oRow.Cells["INP_NUMBER"].Value;
                        oParams[2].Value = oRow.Cells["ID"].OriginalValue;

                        oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                    }
                }


                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 수압감시지점 삭제 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnJunctionDelete_Click(object sender, EventArgs e)
        {
            UltraGridRow row = this.ultraGrid_Junction.ActiveRow;
            if (row == null) return;

            DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_DELETE);
            if (eDialogResult != DialogResult.Yes) return;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("DELETE FROM WE_PRES_MONITOR ");
                oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + row.Cells["INP_NUMBER"].Value + "'");
                oStringBuilder.AppendLine("   AND ID = '" + row.Cells["ID"].Value + "'");

                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                row.Delete(false);
                ultraGrid_Junction.UpdateData();

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }
        #endregion 버튼 클릭 이벤트

        ////////////////데이터베이스 Query////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Insert Process
        /// </summary>
        /// <returns></returns>
        private void TAG_InsertProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("INSERT INTO WE_ENERGY_TAGS (");
            oStringBuilder.AppendLine("  INP_NUMBER,          ");
            oStringBuilder.AppendLine("  MODEL_ID,            ");
            oStringBuilder.AppendLine("  TAGNAME)             ");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine("  :INP_NUMBER,          ");
            oStringBuilder.AppendLine("  :MODEL_ID,            ");
            oStringBuilder.AppendLine("  :TAGNAME)             ");

            IDataParameter[] oParams = {
                new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2,48),
                new OracleParameter(":MODEL_ID",OracleDbType.Varchar2,50),
                new OracleParameter(":TAGNAME",OracleDbType.Varchar2,50)
            };

            oParams[0].Value = oRow.Cells["INP_NUMBER"].Value;
            oParams[1].Value = oRow.Cells["MODEL_ID"].Value;
            oParams[2].Value = oRow.Cells["TAGNAME"].Value;

            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }

        /// <summary>
        /// UpdateProcess
        /// </summary>
        /// <returns></returns>
        private void TAG_UpdateProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            
            oStringBuilder.AppendLine("UPDATE WE_ENERGY_TAGS SET           ");
            oStringBuilder.AppendLine("  MODEL_ID   = :MODEL_ID,           ");
            oStringBuilder.AppendLine("  TAGNAME    = :TAGNAME             ");
            oStringBuilder.AppendLine("WHERE  INP_NUMBER = :INP_NUMBER   ");
            oStringBuilder.AppendLine("  AND  MODEL_ID =   :MODEL_ID2    ");
            oStringBuilder.AppendLine("  AND  TAGNAME =    :TAGNAME2     ");


            IDataParameter[] oParams = {
                new OracleParameter(":MODEL_ID",OracleDbType.Varchar2, 50),
                new OracleParameter(":TAGNAME",OracleDbType.Varchar2, 50),
                new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2, 48),
                new OracleParameter(":MODEL_ID2",OracleDbType.Varchar2, 50),
                new OracleParameter(":TAGNAME2",OracleDbType.Varchar2, 50)
            };


            oParams[0].Value = oRow.Cells["MODEL_ID"].Value;
            oParams[1].Value = oRow.Cells["TAGNAME"].Value;
            oParams[2].Value = oRow.Cells["INP_NUMBER"].Value;
            oParams[3].Value = oRow.Cells["MODEL_ID"].OriginalValue;
            oParams[4].Value = oRow.Cells["TAGNAME"].OriginalValue;

            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }

        /// <summary>
        /// Insert Process
        /// </summary>
        /// <returns></returns>
        private void BIZ_InsertProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("INSERT INTO WE_BIZPLA (");
            oStringBuilder.AppendLine("  INP_NUMBER,          ");
            oStringBuilder.AppendLine("  MODEL_ID,            ");
            oStringBuilder.AppendLine("  BIZ_NAME,            ");
            oStringBuilder.AppendLine("  ELEV,                ");
            oStringBuilder.AppendLine("  ORDERS)              ");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine("  :INP_NUMBER,          ");
            oStringBuilder.AppendLine("  :MODEL_ID,            ");
            oStringBuilder.AppendLine("  :BIZ_NAME,            ");
            oStringBuilder.AppendLine("  :ELEV,                ");
            oStringBuilder.AppendLine("  :ORDERS)              ");

            IDataParameter[] oParams = {
                new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2,48),
                new OracleParameter(":MODEL_ID",OracleDbType.Varchar2,50),
                new OracleParameter(":TAGNAME",OracleDbType.Varchar2,50),
                new OracleParameter(":ELEV",OracleDbType.Double),
                new OracleParameter(":ORDERS",OracleDbType.Int32)
            };

            oParams[0].Value = oRow.Cells["INP_NUMBER"].Value;
            oParams[1].Value = oRow.Cells["MODEL_ID"].Value;
            oParams[2].Value = oRow.Cells["BIZ_NAME"].Value;
            oParams[3].Value = oRow.Cells["ELEV"].Value;
            oParams[4].Value = oRow.Cells["ORDERS"].Value;

            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }

        /// <summary>
        /// UpdateProcess
        /// </summary>
        /// <returns></returns>
        private void BIZ_UpdateProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE WE_BIZPLA SET                ");
            oStringBuilder.AppendLine("  MODEL_ID   = :MODEL_ID,           ");
            oStringBuilder.AppendLine("  BIZ_NAME   = :BIZ_NAME,           ");
            oStringBuilder.AppendLine("  ELEV       = :ELEV,               ");
            oStringBuilder.AppendLine("  ORDERS     = :ORDERS              ");
            oStringBuilder.AppendLine("WHERE  INP_NUMBER = :INP_NUMBER   ");
            oStringBuilder.AppendLine("  AND  MODEL_ID =   :MODEL_ID2    ");


            IDataParameter[] oParams = {
                new OracleParameter(":MODEL_ID",OracleDbType.Varchar2, 50),
                new OracleParameter(":BIZ_NAME",OracleDbType.Varchar2, 50),
                new OracleParameter(":ELEV",OracleDbType.Double),
                new OracleParameter(":ORDERS",OracleDbType.Int32),
                new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2, 48),
                new OracleParameter(":MODEL_ID2",OracleDbType.Varchar2, 50)
            };


            oParams[0].Value = oRow.Cells["MODEL_ID"].Value;
            oParams[1].Value = oRow.Cells["BIZ_NAME"].Value;
            oParams[2].Value = oRow.Cells["ELEV"].Value;
            oParams[3].Value = oRow.Cells["ORDERS"].Value;
            oParams[4].Value = oRow.Cells["INP_NUMBER"].Value;
            oParams[5].Value = oRow.Cells["MODEL_ID"].OriginalValue;

            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);

        }

        /// <summary>
        /// Insert Process
        /// </summary>
        /// <returns></returns>
        private void PUMP_InsertProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("INSERT INTO WE_PUMPINFO (");
            oStringBuilder.AppendLine("  INP_NUMBER,          ");
            oStringBuilder.AppendLine("  MODEL_ID,            ");
            oStringBuilder.AppendLine("  PUMP_ID,             ");
            oStringBuilder.AppendLine("  PUMP_NAME,           ");
            oStringBuilder.AppendLine("  PUMPCURVE_ID,        ");
            oStringBuilder.AppendLine("  EFFCURVE_ID,         ");
            oStringBuilder.AppendLine("  FREQ       )         ");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine("  :INP_NUMBER,          ");
            oStringBuilder.AppendLine("  :MODEL_ID,            ");
            oStringBuilder.AppendLine("  :PUMP_ID,             ");
            oStringBuilder.AppendLine("  :PUMP_NAME,           ");
            oStringBuilder.AppendLine("  :PUMPCURVE_ID,        ");
            oStringBuilder.AppendLine("  :EFFCURVE_ID,         ");
            oStringBuilder.AppendLine("  :FREQ       )         ");

            IDataParameter[] oParams = {
                new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2,48),
                new OracleParameter(":MODEL_ID",OracleDbType.Varchar2,50),
                new OracleParameter(":PUMP_ID",OracleDbType.Varchar2,50),
                new OracleParameter(":PUMP_NAME",OracleDbType.Varchar2,50),
                new OracleParameter(":PUMPCURVE_ID",OracleDbType.Int32),
                new OracleParameter(":EFFCURVE_ID",OracleDbType.Int32),
                new OracleParameter(":FREQ",OracleDbType.Varchar2, 10)
            };

            oParams[0].Value = oRow.Cells["INP_NUMBER"].Value;
            oParams[1].Value = oRow.Cells["MODEL_ID"].Value;
            oParams[2].Value = oRow.Cells["PUMP_ID"].Value;
            oParams[3].Value = oRow.Cells["PUMP_NAME"].Value;
            oParams[4].Value = oRow.Cells["PUMPCURVE_ID"].Value;
            oParams[5].Value = oRow.Cells["EFFCURVE_ID"].Value;
            oParams[6].Value = oRow.Cells["FREQ"].Value;


            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }

        /// <summary>
        /// UpdateProcess
        /// </summary>
        /// <returns></returns>
        private void PUMP_UpdateProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE WE_PUMPINFO SET                ");
            oStringBuilder.AppendLine("  PUMP_ID   = :PUMP_ID,           ");
            oStringBuilder.AppendLine("  PUMP_NAME   = :PUMP_NAME,           ");
            oStringBuilder.AppendLine("  PUMPCURVE_ID  = :PUMPCURVE_ID,          ");
            oStringBuilder.AppendLine("  EFFCURVE_ID  = :EFFCURVE_ID,           ");
            oStringBuilder.AppendLine("  FREQ  = :FREQ           ");
            oStringBuilder.AppendLine("WHERE  INP_NUMBER = :INP_NUMBER   ");
            oStringBuilder.AppendLine("  AND  MODEL_ID =   :MODEL_ID     ");
            oStringBuilder.AppendLine("  AND  PUMP_ID =    :PUMP_ID2     ");


            IDataParameter[] oParams = {
                new OracleParameter(":PUMP_ID",OracleDbType.Varchar2, 50),
                new OracleParameter(":PUMP_NAME",OracleDbType.Varchar2, 50),
                new OracleParameter(":PUMPCURVE_ID",OracleDbType.Varchar2, 10),
                new OracleParameter(":EFFCURVE_ID",OracleDbType.Varchar2, 10),
                new OracleParameter(":FREQ",OracleDbType.Varchar2, 10),
                new OracleParameter(":INP_NUMBER",OracleDbType.Varchar2, 48),
                new OracleParameter(":MODEL_ID",OracleDbType.Varchar2, 50),
                new OracleParameter(":PUMP_ID2",OracleDbType.Varchar2, 50)
            };


            oParams[0].Value = oRow.Cells["PUMP_ID"].Value;
            oParams[1].Value = oRow.Cells["PUMP_NAME"].Value;
            oParams[2].Value = oRow.Cells["PUMPCURVE_ID"].Value;
            oParams[3].Value = oRow.Cells["EFFCURVE_ID"].Value;
            oParams[4].Value = oRow.Cells["FREQ"].Value;
            oParams[5].Value = oRow.Cells["INP_NUMBER"].Value;
            oParams[6].Value = oRow.Cells["MODEL_ID"].Value;
            oParams[7].Value = oRow.Cells["PUMP_ID"].OriginalValue;

            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }
 

        private void cboModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboModel.SelectedIndex == -1) return;

            try
            {
                ///사업장정보
                StringBuilder oStringBuilder = new StringBuilder();
                
                oStringBuilder.AppendLine("SELECT A.INP_NUMBER, A.MODEL_ID, A.BIZ_NAME                                 ");
                oStringBuilder.AppendLine("      ,B.INITLVL AS INT_LEVEL, B.MINLVL AS MIN_LEVEL, B.MAXLVL AS MAX_LEVEL ");
                oStringBuilder.AppendLine("      ,A.ELEV, A.ORDERS                                                     ");
                oStringBuilder.AppendLine("  FROM WE_BIZPLA A,                                                         ");
                oStringBuilder.AppendLine("       (SELECT ID, INITLVL, MINLVL, MAXLVL                                  ");
                oStringBuilder.AppendLine("          FROM WH_TANK                                                      ");
                oStringBuilder.AppendLine("         WHERE INP_NUMBER = '" + cboModel.SelectedValue + "'"                );
                oStringBuilder.AppendLine("         UNION ALL                                                          ");
                oStringBuilder.AppendLine("        SELECT ID, NULL, NULL, NULL                                         ");
                oStringBuilder.AppendLine("          FROM WH_RESERVOIRS                                                ");
                oStringBuilder.AppendLine("         WHERE INP_NUMBER = '" + cboModel.SelectedValue + "') B");
                oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'"                      );
                oStringBuilder.AppendLine("   AND A.MODEL_ID = B.ID                                                    ");
                oStringBuilder.AppendLine(" ORDER BY A.ORDERS ASC                                                      ");

                DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                m_GridBizManager.SetDataSource(oDatatable);

                btnJunctionRefresh_Click(this, new EventArgs());

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
        }

        /// <summary>
        /// 화면 불러오기
        /// </summary>
        public void Open()
        {
            this.ultraGrid_Pump.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.ultraGrid_Pump.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.ultraGrid_Biz.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.ultraGrid_Biz.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.ultraGrid_Tag.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.ultraGrid_Tag.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
            this.ultraGrid_Junction.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            this.ultraGrid_Junction.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);

            cboModel_SelectedIndexChanged(this, new EventArgs());

            this.ShowDialog();
        }

        private void frmBizPlace_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_oDBManager != null) m_oDBManager.Close();
        }

        private void ultraGrid_Biz_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            UltraGridRow row = ultraGrid_Biz.ActiveRow;
            if (row == null)    return;

            try
            {
                ///TAG정보
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT A.INP_NUMBER, A.MODEL_ID, A.TAGNAME ");
                oStringBuilder.AppendLine("      ,CASE WHEN B.BR_CODE = 'TB' THEN '탁도' ");
                oStringBuilder.AppendLine("            WHEN B.BR_CODE = 'CL' THEN '잔류염소'");
                oStringBuilder.AppendLine("            WHEN B.BR_CODE = 'PH' THEN '수소이온농도'");
                oStringBuilder.AppendLine("            WHEN B.BR_CODE = 'CU' THEN '전기전도도'");
                oStringBuilder.AppendLine("            WHEN B.BR_CODE = 'TE' THEN '온도'");
                oStringBuilder.AppendLine("            WHEN B.BR_CODE = 'FR' THEN '유량'");
                oStringBuilder.AppendLine("            WHEN B.BR_CODE = 'LE' THEN '수위'");
                oStringBuilder.AppendLine("            WHEN B.BR_CODE = 'PR' THEN '압력'");
                oStringBuilder.AppendLine("       END  AS BR_CODE");
                oStringBuilder.AppendLine("      ,B.DESCRIPTION");
                oStringBuilder.AppendLine("  FROM WE_ENERGY_TAGS A                                              ");
                oStringBuilder.AppendLine("      ,IF_IHTAGS B                                                   ");
                oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + row.Cells["INP_NUMBER"].Value +         "'");
                oStringBuilder.AppendLine("   AND A.MODEL_ID = '" + row.Cells["MODEL_ID"].Value +             "'");
                oStringBuilder.AppendLine("   AND A.TAGNAME = B.TAGNAME                                         ");
                oStringBuilder.AppendLine(" ORDER BY A.MODEL_ID, A.TAGNAME ASC                                  ");

                DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                m_GridTagManager.SetDataSource(oDatatable);

                ///펌프정보
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT INP_NUMBER, MODEL_ID, PUMP_ID, PUMP_NAME, PUMPCURVE_ID, EFFCURVE_ID, FREQ ");
                oStringBuilder.AppendLine("  FROM WE_PUMPINFO                                                     ");
                oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + row.Cells["INP_NUMBER"].Value + "'");
                oStringBuilder.AppendLine("   AND MODEL_ID = '" + row.Cells["MODEL_ID"].Value + "'");
                oStringBuilder.AppendLine(" ORDER BY PUMP_ID ASC                                               ");

                DataTable oDatatable2 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                m_GridPumpManager.SetDataSource(oDatatable2);

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
        }

        private void ultraGrid_Biz_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //bool b = false;
            //b = (Convert.IsDBNull(e.Row.Cells["ID"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["X"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["Y"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["IDX"].Value) ? true : false);

            //if (b) e.Row.Delete(false);
        }

        private void ultraGrid_Pump_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //bool b = false;
            //b = (Convert.IsDBNull(e.Row.Cells["ID"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["X"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["Y"].Value) ? true : false);
            //b = (Convert.IsDBNull(e.Row.Cells["IDX"].Value) ? true : false);

            //if (b) e.Row.Delete(false);
        }

        private void ultraGrid_Biz_AfterRowInsert(object sender, RowEventArgs e)
        {
            e.Row.Cells["INP_NUMBER"].Value = cboModel.SelectedValue;
        }

        private void ultraGrid_Pump_AfterRowInsert(object sender, RowEventArgs e)
        {
            e.Row.Cells["INP_NUMBER"].Value = ultraGrid_Biz.ActiveRow.Cells["INP_NUMBER"].Value;
            e.Row.Cells["MODEL_ID"].Value = ultraGrid_Biz.ActiveRow.Cells["MODEL_ID"].Value;
        }

        private void ultraGrid_Tag_AfterRowInsert(object sender, RowEventArgs e)
        {
            e.Row.Cells["INP_NUMBER"].Value = ultraGrid_Biz.ActiveRow.Cells["INP_NUMBER"].Value;
            e.Row.Cells["MODEL_ID"].Value = ultraGrid_Biz.ActiveRow.Cells["MODEL_ID"].Value;
        }

        private void ultraGrid_Junction_AfterRowInsert(object sender, RowEventArgs e)
        {
            e.Row.Cells["INP_NUMBER"].Value = cboModel.SelectedValue;
        }





    



    }
}
