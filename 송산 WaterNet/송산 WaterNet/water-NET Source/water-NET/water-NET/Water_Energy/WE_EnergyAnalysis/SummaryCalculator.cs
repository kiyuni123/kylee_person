﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Infragistics.Win.UltraWinGrid;

namespace WaterNet.WE_EnergyAnalysis
{
    class SummaryCalculator
    {

    }


    public class Sum_SD_FilterCalculator : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            switch (summarySettings.SourceColumn.Key)
            {
                case "T00":
                case "T01":
                case "T02":
                case "T03":
                case "T04":
                case "T05":
                case "T06":
                case "T07":
                case "T08":
                case "T09":
                case "T10":
                case "T11":
                case "T12":
                case "T13":
                case "T14":
                case "T15":
                case "T16":
                case "T17":
                case "T18":
                case "T19":
                case "T20":
                case "T21":
                case "T22":
                case "T23":
                case "TOT":
                    break;
                default:
                    return this.result;
            }
            //if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            //{
            //    return this.result;
            //}

            foreach (UltraGridRow row in rows)
            {
                if (Convert.ToString(row.Cells["PUMP_ID"].Value).IndexOf("SD") > -1) //상동가압장 펌프가 아님으로 skip.
                {
                    double value = Convert.ToDouble(row.Cells[summarySettings.SourceColumn].Value);
                    this.result += value;
                }
            }

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }
        #endregion
    }

    public class Sum_HR_FilterCalculator : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            switch (summarySettings.SourceColumn.Key)
	        {
                case "T00":
                case "T01":
                case "T02":
                case "T03":
                case "T04":
                case "T05":
                case "T06":
                case "T07":
                case "T08":
                case "T09":
                case "T10":
                case "T11":
                case "T12":
                case "T13":
                case "T14":
                case "T15":
                case "T16":
                case "T17":
                case "T18":
                case "T19":
                case "T20":
                case "T21":
                case "T22":
                case "T23":
                case "TOT":
                    break;
                default :
                    return this.result;
	        }
            
            //if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            //{
            //    return this.result;
            //}

            foreach (UltraGridRow row in rows)
            {
                if (Convert.ToString(row.Cells["PUMP_ID"].Value).IndexOf("HR") > -1) //상동가압장 펌프가 아님으로 skip.
                {
                    double value = Convert.ToDouble(row.Cells[summarySettings.SourceColumn].Value);
                    this.result += value;
                }
            }

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }
        #endregion
    }

    public class Week4Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 28; i--)
            {
                double value = Convert.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week3Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 21; i--)
            {
                double value = Convert.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week2Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 14; i--)
            {
                double value = Convert.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week1Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 7; i--)
            {
                double value = Convert.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }
}
