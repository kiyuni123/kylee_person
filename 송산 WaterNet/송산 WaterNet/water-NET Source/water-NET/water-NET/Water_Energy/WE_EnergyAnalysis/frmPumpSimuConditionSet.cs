﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
using WaterNet.WE_Common;
using WaterNet.WH_PipingNetworkAnalysis.epanet;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion UltraGrid를 사용=>namespace선언

namespace WaterNet.WE_EnergyAnalysis
{
    public partial class frmPumpSimuConditionSet : Form
    {
        /// <summary>
        /// 폼을 최소화하기 위한 멤버선언
        /// </summary>
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();                                //최종 윈도우 파라메터

        private Form m_OwnerForm = null;

        private GridManager m_GridWaterLevelManager = null;  //수위조건 그리드
        private GridManager m_GridPumpProptyManager = null;  //펌프특성 그리드
        private GridManager m_GridDemandManager = null;  //Demand 그리드
        private GridManager m_GridRuleManager = null;  //Rules 그리드

        private OracleDBManager m_oDBManager = new OracleDBManager();  ///데이터베이스

        //해석결과를 담을 데이터 테이블
        private DataTable m_linkFlowTable = new DataTable("FLOW");       //펌프의 유량
        private DataTable m_linkEnergyTable = new DataTable("ENERGY");   //펌프의 에너지
        private DataTable m_linkStatusTable = new DataTable("STATUS");   //펌프의 가동상태

        private DataTable m_nodeHeadTable = new DataTable("HEAD");   //사업장(tank,Resvr)의 Head
        private DataTable m_nodePressTable = new DataTable("PRESSURE");  //감시지점(JUNCTION의 수압)
        //-----------------------------------------------------------------------------------------------
        private DataTable m_nodeTable = new DataTable("NODE");
        private DataTable m_linkTable = new DataTable("LINK");

        public frmPumpSimuConditionSet()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            try
            {
                m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                m_oDBManager.Open();
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;
            #region 수위조건 그리드
            //------------------------------
            oUltraGridColumn = ultraGrid_WaterLevel.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WaterLevel.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_NAME";
            oUltraGridColumn.Header.Caption = "사업장명";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WaterLevel.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INT_LEVEL";
            oUltraGridColumn.Header.Caption = "초기수위";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Format = "#0.#";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WaterLevel.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MIN_LEVEL";
            oUltraGridColumn.Header.Caption = "최소수위";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Format = "#0.#";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WaterLevel.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MAX_LEVEL";
            oUltraGridColumn.Header.Caption = "최대수위";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Format = "#0.#";
            oUltraGridColumn.Hidden = false;
            //------------------------------
            m_GridWaterLevelManager = new GridManager(ultraGrid_WaterLevel);
            m_GridWaterLevelManager.SetGridStyle_Default();
            m_GridWaterLevelManager.SetGridStyle_Update();
            m_GridWaterLevelManager.ColumeNoEdit(0,2);
            ultraGrid_WaterLevel.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;

            #endregion 수위조건 그리드

            #region 펌프특성 그리드
            oUltraGridColumn = ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_ID";
            oUltraGridColumn.Header.Caption = "MODEL_ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_NAME";
            oUltraGridColumn.Header.Caption = "펌프명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPCURVE_ID";
            oUltraGridColumn.Header.Caption = "펌프커브ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;
            SetValuelist_PumpCurve();

            oUltraGridColumn = ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EFFCURVE_ID";
            oUltraGridColumn.Header.Caption = "효율커브ID";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Hidden = false;
            SetValuelist_EffCurve();

            oUltraGridColumn = ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FREQ";
            oUltraGridColumn.Header.Caption = "주파수";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Hidden = false;
            SetValueList_Freq();

            oUltraGridColumn = ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED";
            oUltraGridColumn.Header.Caption = "SPEED";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.Formula = "([FREQ] / 60)";
            oUltraGridColumn.Hidden = true;

            m_GridPumpProptyManager = new GridManager(ultraGrid_PumpPropty);
            m_GridPumpProptyManager.SetGridStyle_Default();
            m_GridPumpProptyManager.SetGridStyle_Update();
            m_GridPumpProptyManager.ColumeNoEdit(1, 3);
            ultraGrid_PumpPropty.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;

            #endregion 펌프특성 그리드

            #region 룰 그리드
            oUltraGridColumn = ultraGrid_Rule.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "STATEMENT";
            oUltraGridColumn.Header.Caption = "STATEMENT";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 300;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Rule.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H_LEVEL";
            oUltraGridColumn.Header.Caption = "수위 레벨";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;
            SetValuelist_Rule();

            oUltraGridColumn = ultraGrid_Rule.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "IDX";
            oUltraGridColumn.Header.Caption = "순서";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 40;
            oUltraGridColumn.Hidden = true;

            m_GridRuleManager = new GridManager(ultraGrid_Rule);
            m_GridRuleManager.SetGridStyle_Default();
            m_GridRuleManager.SetGridStyle_Update();
            //m_GridRuleManager.ColumeNoEdit(0, 3);
            ultraGrid_Rule.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
            
            #endregion 룰 그리드
            #endregion 그리드 설정

            StringBuilder oStringBuilder = new StringBuilder();
            #region ComboBox 설정
            oStringBuilder.AppendLine("SELECT INP_NUMBER AS CD, TITLE || '(' || INP_NUMBER || ')' AS CDNM FROM WH_TITLE ");
            oStringBuilder.AppendLine(" WHERE USE_GBN = 'EN'                                                            ");
            oStringBuilder.AppendLine(" ORDER BY INS_DATE DESC                                                          ");
            WaterNetCore.FormManager.SetComboBoxEX(cboModel, oStringBuilder.ToString(), false);

            #endregion

            CreateAnalisysTable();
        }

        #endregion 초기화설정

        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    int deskHeight = Screen.PrimaryScreen.Bounds.Height;
                    int deskWidth = Screen.PrimaryScreen.Bounds.Width;        

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                    {
                        if (this.Owner != null)
                            this.Location = new System.Drawing.Point(this.Owner.Width - 170, this.Owner.Height - 40);
                        else
                            this.Location = new System.Drawing.Point(0, 0);
                    }
                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                {
                    return;
                }
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        public Form mainform
        {
            set
            {
                m_OwnerForm = value;
            }
        }

        /// <summary>
        /// 해석결과를 저장할 데이터테이블 및 필드생성
        /// nodeTable, linkTable
        /// </summary>
        private void CreateAnalisysTable()
        {
            ////해석결과를 담는 객체 초기화
            m_nodeTable.Columns.Add("TIME", typeof(System.String));
            m_nodeTable.Columns.Add("ID", typeof(System.String));
            m_nodeTable.Columns.Add("ELEVATION", typeof(System.Double));
            m_nodeTable.Columns.Add("BASEDEMAND", typeof(System.Double));
            m_nodeTable.Columns.Add("PATTERN", typeof(System.Double));
            m_nodeTable.Columns.Add("EMITTER", typeof(System.Double));
            m_nodeTable.Columns.Add("INITQUAL", typeof(System.Double));
            m_nodeTable.Columns.Add("SOURCEQUAL", typeof(System.Double));
            m_nodeTable.Columns.Add("SOURCEPAT", typeof(System.Double));
            m_nodeTable.Columns.Add("SOURCETYPE", typeof(System.Double));
            m_nodeTable.Columns.Add("TANKLEVEL", typeof(System.Double));
            m_nodeTable.Columns.Add("DEMAND", typeof(System.Double));
            m_nodeTable.Columns.Add("HEAD", typeof(System.Double));
            m_nodeTable.Columns.Add("PRESSURE", typeof(System.Double));
            m_nodeTable.Columns.Add("QUALITY", typeof(System.Double));
            m_nodeTable.Columns.Add("SOURCEMASS", typeof(System.Double));
            m_nodeTable.Columns.Add("INITVOLUMN", typeof(System.Double));
            m_nodeTable.Columns.Add("MIXMODEL", typeof(System.Double));
            m_nodeTable.Columns.Add("MIXZONEVOL", typeof(System.Double));
            m_nodeTable.Columns.Add("TANKDIAM", typeof(System.Double));
            m_nodeTable.Columns.Add("MINVOLUMN", typeof(System.Double));
            m_nodeTable.Columns.Add("VOLCURVE", typeof(System.Double));
            m_nodeTable.Columns.Add("MINLEVEL", typeof(System.Double));
            m_nodeTable.Columns.Add("MAXLEVEL", typeof(System.Double));
            m_nodeTable.Columns.Add("MIXFRACTION", typeof(System.Double));
            m_nodeTable.Columns.Add("TANK_KBULK", typeof(System.Double));

            m_linkTable.Columns.Add("TIME", typeof(System.String));
            m_linkTable.Columns.Add("ID", typeof(System.String));
            m_linkTable.Columns.Add("DIAMETER", typeof(System.Double));
            m_linkTable.Columns.Add("LENGTH", typeof(System.Double));
            m_linkTable.Columns.Add("ROUGHNESS", typeof(System.Double));
            m_linkTable.Columns.Add("MINORLOSS", typeof(System.Double));
            m_linkTable.Columns.Add("INITSTATUS", typeof(System.Double));
            m_linkTable.Columns.Add("INITSETTING", typeof(System.Double));
            m_linkTable.Columns.Add("KBULK", typeof(System.Double));
            m_linkTable.Columns.Add("KWALL", typeof(System.Double));
            m_linkTable.Columns.Add("FLOW", typeof(System.Double));
            m_linkTable.Columns.Add("VELOCITY", typeof(System.Double));
            m_linkTable.Columns.Add("HEADLOSS", typeof(System.Double));
            m_linkTable.Columns.Add("STATUS", typeof(System.Double));
            m_linkTable.Columns.Add("SETTING", typeof(System.Double));
            m_linkTable.Columns.Add("ENERGY", typeof(System.Double));

            //(유량:FLOW)----------------------------------------------------------------------------------------
            m_linkFlowTable.Columns.Add("PUMP_ID", typeof(System.String));
            m_linkFlowTable.Columns.Add("PUMP_NAME", typeof(System.String));
            m_linkFlowTable.Columns.Add("CHECKED", typeof(System.String));
            m_linkFlowTable.Columns.Add("T00", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T01", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T02", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T03", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T04", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T05", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T06", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T07", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T08", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T09", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T10", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T11", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T12", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T13", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T14", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T15", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T16", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T17", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T18", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T19", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T20", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T21", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T22", typeof(System.Double));
            m_linkFlowTable.Columns.Add("T23", typeof(System.Double));
            //(에너지:ENERGY)----------------------------------------------------------------------------------------
            m_linkEnergyTable.Columns.Add("PUMP_ID", typeof(System.String));
            m_linkEnergyTable.Columns.Add("PUMP_NAME", typeof(System.String));
            m_linkEnergyTable.Columns.Add("CHECKED", typeof(System.String));
            m_linkEnergyTable.Columns.Add("T00", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T01", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T02", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T03", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T04", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T05", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T06", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T07", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T08", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T09", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T10", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T11", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T12", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T13", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T14", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T15", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T16", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T17", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T18", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T19", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T20", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T21", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T22", typeof(System.Double));
            m_linkEnergyTable.Columns.Add("T23", typeof(System.Double));
            //(가동상태:STATUS)----------------------------------------------------------------------------------------
            m_linkStatusTable.Columns.Add("PUMP_ID", typeof(System.String));
            m_linkStatusTable.Columns.Add("PUMP_NAME", typeof(System.String));
            //m_linkStatusTable.Columns.Add("CHECKED", typeof(System.String));
            m_linkStatusTable.Columns.Add("T00", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T01", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T02", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T03", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T04", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T05", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T06", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T07", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T08", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T09", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T10", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T11", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T12", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T13", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T14", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T15", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T16", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T17", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T18", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T19", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T20", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T21", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T22", typeof(System.Double));
            m_linkStatusTable.Columns.Add("T23", typeof(System.Double));
            //(사업장:Demand(수위))----------------------------------------------------------------------------------------
            m_nodeHeadTable.Columns.Add("MODEL_ID", typeof(System.String));
            m_nodeHeadTable.Columns.Add("BIZ_NAME", typeof(System.String));
            m_nodeHeadTable.Columns.Add("CHECKED", typeof(System.String));
            m_nodeHeadTable.Columns.Add("T00", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T01", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T02", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T03", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T04", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T05", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T06", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T07", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T08", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T09", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T10", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T11", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T12", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T13", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T14", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T15", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T16", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T17", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T18", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T19", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T20", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T21", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T22", typeof(System.Double));
            m_nodeHeadTable.Columns.Add("T23", typeof(System.Double));
            //(감시지점:(JUNCTION의 수압))----------------------------------------------------------------------------------------
            m_nodePressTable.Columns.Add("CHECKED", typeof(System.String));
            m_nodePressTable.Columns.Add("ID", typeof(System.String));
            m_nodePressTable.Columns.Add("T00", typeof(System.Double));
            m_nodePressTable.Columns.Add("T01", typeof(System.Double));
            m_nodePressTable.Columns.Add("T02", typeof(System.Double));
            m_nodePressTable.Columns.Add("T03", typeof(System.Double));
            m_nodePressTable.Columns.Add("T04", typeof(System.Double));
            m_nodePressTable.Columns.Add("T05", typeof(System.Double));
            m_nodePressTable.Columns.Add("T06", typeof(System.Double));
            m_nodePressTable.Columns.Add("T07", typeof(System.Double));
            m_nodePressTable.Columns.Add("T08", typeof(System.Double));
            m_nodePressTable.Columns.Add("T09", typeof(System.Double));
            m_nodePressTable.Columns.Add("T10", typeof(System.Double));
            m_nodePressTable.Columns.Add("T11", typeof(System.Double));
            m_nodePressTable.Columns.Add("T12", typeof(System.Double));
            m_nodePressTable.Columns.Add("T13", typeof(System.Double));
            m_nodePressTable.Columns.Add("T14", typeof(System.Double));
            m_nodePressTable.Columns.Add("T15", typeof(System.Double));
            m_nodePressTable.Columns.Add("T16", typeof(System.Double));
            m_nodePressTable.Columns.Add("T17", typeof(System.Double));
            m_nodePressTable.Columns.Add("T18", typeof(System.Double));
            m_nodePressTable.Columns.Add("T19", typeof(System.Double));
            m_nodePressTable.Columns.Add("T20", typeof(System.Double));
            m_nodePressTable.Columns.Add("T21", typeof(System.Double));
            m_nodePressTable.Columns.Add("T22", typeof(System.Double));
            m_nodePressTable.Columns.Add("T23", typeof(System.Double)); 
        
        }

        private void SetValuelist_Rule()
        {
            object[,] aoSource = {
                {"HRT MIN",   "HRT MIN LEVEL"},
                {"HRT MAX",   "HRT MAX LEVEL"},
                {"NST MIN",   "NST MIN LEVEL"},
                {"NST MAX",   "NST MAX LEVEL"}
            };
            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(aoSource);
            this.ultraGrid_Rule.DisplayLayout.Bands[0].Columns["H_LEVEL"].ValueList = objValue;

        }

        /// <summary>
        /// 펌프 커브 ValueList 설정 : 그리드 콤보박스
        /// </summary>
        private void SetValuelist_PumpCurve()
        {

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT DISTINCT ID AS CODE, ID AS VALUE ");
            oStringBuilder.AppendLine("  FROM   WE_CURVEDATA                   ");
            oStringBuilder.AppendLine(" WHERE GUBUN = '1'");
            oStringBuilder.AppendLine(" ORDER BY ID ASC  ");

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(oStringBuilder.ToString());
            this.ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns["PUMPCURVE_ID"].ValueList = objValue;

        }

        /// <summary>
        /// 효율 커브 Valuelist 설정 : 그리드 콤보박스
        /// </summary>
        private void SetValuelist_EffCurve()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT DISTINCT ID AS CODE, ID AS VALUE ");
            oStringBuilder.AppendLine("  FROM   WE_CURVEDATA                   ");
            oStringBuilder.AppendLine(" WHERE GUBUN = '2'");
            oStringBuilder.AppendLine(" ORDER BY ID ASC  ");

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(oStringBuilder.ToString());
            this.ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns["EFFCURVE_ID"].ValueList = objValue;

        }

        /// <summary>
        /// 주파수 Valuelist 설정 : 펌프특성 그리드 콤보박스
        /// </summary>
        private void SetValueList_Freq()
        {
            object[,] aoSource = {
                {"30",		"30"},
                {"31",		"31"},
                {"32",		"32"},
                {"33",		"33"},
                {"34",		"34"},
                {"35",		"35"},
                {"36",		"36"},
                {"37",		"37"},
                {"38",		"38"},
                {"39",		"39"},
                {"40",		"40"},
                {"41",		"41"},
                {"42",		"42"},
                {"43",		"43"},
                {"44",		"44"},
                {"45",		"45"},
                {"46",		"46"},
                {"47",		"47"},
                {"48",		"48"},
                {"49",		"49"},
                {"50",		"50"},
                {"51",		"51"},
                {"52",		"52"},
                {"53",		"53"},
                {"54",		"54"},
                {"55",		"55"},
                {"56",		"56"},
                {"57",		"57"},
                {"58",		"58"},
                {"59",		"59"},
                {"60",		"60"}
            };

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(aoSource);
            this.ultraGrid_PumpPropty.DisplayLayout.Bands[0].Columns["FREQ"].ValueList = objValue;
        }
        
        /// <summary>
        /// 화면 오픈 : 화면위치
        /// </summary>
        public void Open()
        {
            this.TopLevel = false;
            this.Parent = this.Owner;
            this.Location = new System.Drawing.Point(Convert.ToInt32((this.ParentForm.Width - this.Width) / 2), Convert.ToInt32((this.ParentForm.Height - this.Height) / 2));
            this.BringToFront();
            this.Show();

        }

        /// <summary>
        /// 화면오픈 :  모델 Change Event 호출, 주파수 콤보박스 Change Event호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPumpSimuConditionSet_Load(object sender, EventArgs e)
        {
            cboModel_SelectedIndexChanged(this, new EventArgs());
        }

        /// <summary>
        /// 펌프 특성조건 : 새로고침 버튼 클릭 : 펌프커브, 효율커브 새로고침
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            SetValuelist_PumpCurve();
            SetValuelist_EffCurve();
        }

        #region 시뮬레이션 조건 설정
        /// <summary>
        /// 수위조건 질의문
        /// </summary>
        private void QuerySelectWaterLevel()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.MODEL_ID, A.BIZ_NAME, B.INITLVL AS INT_LEVEL, B.MINLVL AS MIN_LEVEL, B.MAXLVL AS MAX_LEVEL ");
            oStringBuilder.AppendLine("  FROM WE_BIZPLA A, WH_TANK B                              ");
            oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("   AND A.INP_NUMBER = B.INP_NUMBER AND A.MODEL_ID = B.ID ");
            oStringBuilder.AppendLine(" ORDER BY A.ORDERS ASC                                               ");

            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            m_GridWaterLevelManager.SetDataSource(oDatatable);
        }

        /// <summary>
        /// 펌프특성조건 질의문
        /// </summary>
        private void QuerySelectPumpPropty()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.MODEL_ID, A.PUMP_ID, A.PUMP_NAME, A.PUMPCURVE_ID, A.EFFCURVE_ID, A.FREQ ");
            oStringBuilder.AppendLine("  FROM WE_PUMPINFO A, WE_BIZPLA B                                    ");
            oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("   AND A.INP_NUMBER = B.INP_NUMBER                                   ");
            oStringBuilder.AppendLine("   AND A.MODEL_ID = B.MODEL_ID                                       ");
            oStringBuilder.AppendLine(" ORDER BY B.ORDERS, A.PUMP_ID ASC                                    ");

            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            m_GridPumpProptyManager.SetDataSource(oDatatable);
        }

        /// <summary>
        /// Demand 설정 조건 질의문
        /// </summary>
        private void QuerySelectDemand()
        {
            DateTime dt = (DateTime)ultraDateTimeEditor_Day.Value;
            string startDay = dt.Year.ToString() + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0') + "000000";
            string endDay = dt.Year.ToString() + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0') + "235959";

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/");
            oStringBuilder.AppendLine("        A.MODEL_ID, A.BIZ_NAME, A.ORDERS, NVL(ROUND(AVG(VALUE),2),0) AS DEMAND  --A.MODEL_ID, A.BIZ_NAME, A.ORDERS, B.TAGNAME  ");
            oStringBuilder.AppendLine("  FROM  WE_BIZPLA A                                                                                ");
            oStringBuilder.AppendLine("       ,WE_ENERGY_TAGS B                                                                           ");
            oStringBuilder.AppendLine("       ,IF_IHTAGS C                                                                                ");
            oStringBuilder.AppendLine("       ,IF_GATHER_REALTIME REALTIME                                                                ");
            oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("   AND A.INP_NUMBER = B.INP_NUMBER                                                                 ");
            oStringBuilder.AppendLine("   AND A.MODEL_ID = B.MODEL_ID                                                                     ");
            oStringBuilder.AppendLine("   AND A.MODEL_ID IN (SELECT ID FROM WH_RESERVOIRS WHERE INP_NUMBER = '" + cboModel.SelectedValue + "')");
            oStringBuilder.AppendLine("   AND B.TAGNAME = C.TAGNAME                                                                       ");
            oStringBuilder.AppendLine("   AND C.BR_CODE = 'FR'                                                                            ");
            oStringBuilder.AppendLine("   AND B.TAGNAME = REALTIME.TAGNAME                                                                ");
            //oStringBuilder.AppendLine("   AND A.ORDERS = 1                                                                                ");
            oStringBuilder.AppendLine("   AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine("                          AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine(" GROUP BY A.MODEL_ID, A.BIZ_NAME,A.ORDERS                                                          ");
            oStringBuilder.AppendLine(" ORDER BY A.ORDERS ASC                                                                             ");
            ///굳이 union을 할 필요가 없을것 같음.
            ///
            //oStringBuilder.AppendLine("SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/");
            //oStringBuilder.AppendLine("        A.MODEL_ID, A.BIZ_NAME, A.ORDERS, NVL(ROUND(AVG(VALUE),2),0) AS DEMAND  --A.MODEL_ID, A.BIZ_NAME, A.ORDERS, B.TAGNAME  ");
            //oStringBuilder.AppendLine("  FROM  WE_BIZPLA A                                                                                ");
            //oStringBuilder.AppendLine("       ,WE_ENERGY_TAGS B                                                                           ");
            //oStringBuilder.AppendLine("       ,IF_IHTAGS C                                                                                ");
            //oStringBuilder.AppendLine("       ,IF_GATHER_REALTIME REALTIME                                                                ");
            //oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            //oStringBuilder.AppendLine("   AND A.INP_NUMBER = B.INP_NUMBER                                                                 ");
            //oStringBuilder.AppendLine("   AND A.MODEL_ID = B.MODEL_ID                                                                     ");
            //oStringBuilder.AppendLine("   AND B.TAGNAME = C.TAGNAME                                                                       ");
            //oStringBuilder.AppendLine("   AND C.BR_CODE = 'FR'                                                                            ");
            //oStringBuilder.AppendLine("   AND A.ORDERS = 1                                                                                ");
            //oStringBuilder.AppendLine("   AND B.TAGNAME = REALTIME.TAGNAME                                                                ");
            //oStringBuilder.AppendLine("   AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            //oStringBuilder.AppendLine("                          AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            //oStringBuilder.AppendLine(" GROUP BY A.MODEL_ID, A.BIZ_NAME,A.ORDERS                                                                               ");
            //oStringBuilder.AppendLine("UNION ALL                                                                                          ");
            //oStringBuilder.AppendLine("SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/");
            //oStringBuilder.AppendLine("        A.MODEL_ID, A.BIZ_NAME, A.ORDERS, NVL(ROUND(AVG(VALUE),2),0) AS DEMAND  --A.MODEL_ID, A.BIZ_NAME, A.ORDERS, B.TAGNAME  ");
            //oStringBuilder.AppendLine("  FROM  WE_BIZPLA A                                                                                ");
            //oStringBuilder.AppendLine("       ,WE_ENERGY_TAGS B                                                                           ");
            //oStringBuilder.AppendLine("       ,IF_IHTAGS C                                                                                ");
            //oStringBuilder.AppendLine("       ,IF_GATHER_REALTIME REALTIME                                                                ");
            //oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            //oStringBuilder.AppendLine("   AND A.INP_NUMBER = B.INP_NUMBER                                                                 ");
            //oStringBuilder.AppendLine("   AND A.MODEL_ID = B.MODEL_ID                                                                     ");
            //oStringBuilder.AppendLine("   AND B.TAGNAME = C.TAGNAME                                                                       ");
            //oStringBuilder.AppendLine("   AND C.BR_CODE = 'FR'                                                                            ");
            //oStringBuilder.AppendLine("   AND A.ORDERS = 2                                                                                ");
            //oStringBuilder.AppendLine("   AND B.TAGNAME = REALTIME.TAGNAME                                                                ");
            //oStringBuilder.AppendLine("   AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            //oStringBuilder.AppendLine("                          AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            //oStringBuilder.AppendLine(" GROUP BY A.MODEL_ID, A.BIZ_NAME,A.ORDERS                                                                              ");
            //oStringBuilder.AppendLine("UNION ALL                                                                                                ");
            //oStringBuilder.AppendLine("SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/");
            //oStringBuilder.AppendLine("        A.MODEL_ID,  A.BIZ_NAME, A.ORDERS, ROUND(AVG(VALUE),2) AS DEMAND  --A.MODEL_ID, A.BIZ_NAME, A.ORDERS, B.TAGNAME         ");
            //oStringBuilder.AppendLine("  FROM  WE_BIZPLA A                                                                                      ");
            //oStringBuilder.AppendLine("       ,WE_ENERGY_TAGS B                                                                                 ");
            //oStringBuilder.AppendLine("       ,IF_IHTAGS C                                                                                      ");
            //oStringBuilder.AppendLine("       ,IF_GATHER_REALTIME REALTIME                                                                      ");
            //oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            //oStringBuilder.AppendLine("   AND A.INP_NUMBER = B.INP_NUMBER                                                                       ");
            //oStringBuilder.AppendLine("   AND A.MODEL_ID = B.MODEL_ID                                                                           ");
            //oStringBuilder.AppendLine("   AND B.TAGNAME = C.TAGNAME                                                                             ");
            //oStringBuilder.AppendLine("   AND C.BR_CODE = 'FR'                                                                                  ");
            //oStringBuilder.AppendLine("   AND A.ORDERS = 3                                                                                      ");
            //oStringBuilder.AppendLine("   AND B.TAGNAME = REALTIME.TAGNAME                                                                      ");
            //oStringBuilder.AppendLine("   AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            //oStringBuilder.AppendLine("                          AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            //oStringBuilder.AppendLine(" GROUP BY A.MODEL_ID, A.BIZ_NAME, A.ORDERS                                                                           ");

            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            m_GridDemandManager.SetDataSource(oDatatable);
        }

        /// <summary>
        /// 흡입수두 설정 조건 질의문
        /// </summary>
        private void QuerySelectReservoirsHead()
        {
            if (m_OwnerForm == null) return;

            DateTime dt = (DateTime)ultraDateTimeEditor_Day.Value;
            string startDay = dt.Year.ToString() + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0') + "000000";
            string endDay = dt.Year.ToString() + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0') + "235959";

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT *                                                                               ");
            oStringBuilder.AppendLine("  FROM                                                                                 ");
            oStringBuilder.AppendLine("      (SELECT A.MODEL_ID, A.BIZ_NAME                                                   ");
            oStringBuilder.AppendLine("             ,TO_CHAR(TIMESTAMP,'HH24') AS HH                                          ");
            oStringBuilder.AppendLine("             ,ROUND((NVL(AVG(VALUE),0) * 10) + NVL(A.ELEV,0),2) AS HEAD                ");
            oStringBuilder.AppendLine("         FROM  WE_BIZPLA A                                                             ");
            oStringBuilder.AppendLine("             ,WE_ENERGY_TAGS B                                                         ");
            oStringBuilder.AppendLine("             ,IF_IHTAGS C                                                              ");
            oStringBuilder.AppendLine("             ,IF_GATHER_REALTIME REALTIME                                              ");
            oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("         AND A.INP_NUMBER = B.INP_NUMBER                                               ");
            oStringBuilder.AppendLine("         AND A.MODEL_ID = B.MODEL_ID                                                   ");
            oStringBuilder.AppendLine("         AND A.MODEL_ID = (SELECT ID FROM WH_RESERVOIRS WHERE INP_NUMBER = '" + cboModel.SelectedValue + "')");
            oStringBuilder.AppendLine("         AND B.TAGNAME = C.TAGNAME                                                     ");
            oStringBuilder.AppendLine("         AND C.BR_CODE = 'PR'                                                          ");
            oStringBuilder.AppendLine("         AND B.TAGNAME = REALTIME.TAGNAME                                              ");
            oStringBuilder.AppendLine("         AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine("                                AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine("       GROUP BY A.MODEL_ID, A.BIZ_NAME, TO_CHAR(TIMESTAMP,'HH24'), A.ELEV              ");
            oStringBuilder.AppendLine("       ORDER BY A.MODEL_ID, A.BIZ_NAME, TO_CHAR(TIMESTAMP,'HH24') ASC)                 ");
            oStringBuilder.AppendLine(" PIVOT (                                                                               ");
            oStringBuilder.AppendLine("   AVG(HEAD)                                                                           ");
            oStringBuilder.AppendLine("   FOR HH                                                                              ");
            oStringBuilder.AppendLine("   IN ('00' AS T00,'01' AS T01,'02' AS T02,'03' AS T03,'04' AS T04,'05' AS T05,'06' AS T06");
            oStringBuilder.AppendLine("      ,'07' AS T07,'08' AS T08,'09' AS T09,'10' AS T10,'11' AS T11,'12' AS T12,'13' AS T13");
            oStringBuilder.AppendLine("      ,'14' AS T14,'15' AS T15,'16' AS T16,'17' AS T17,'18' AS T18,'19' AS T19            ");
            oStringBuilder.AppendLine("      ,'20' AS T20,'21' AS T21,'22' AS T22,'23' AS T23)                                  ");
            oStringBuilder.AppendLine(" )                                                                                     ");

            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            ((frmPumpSimulator)m_OwnerForm).ultraGrid_ReservoirsHead.DataSource = oDatatable;
        }


        /// <summary>
        /// 룰 조건 질의문
        /// </summary>
        private void QuerySelectRuleBase()
        {
            txt_Rule.Text = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT RULES_STATEMENT AS STATEMENT, '' AS H_LEVEL, IDX ");
            oStringBuilder.AppendLine("  FROM WH_RULES                                                      ");
            oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine(" ORDER BY TO_NUMBER(IDX) ASC                                     ");

            DataTable datatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            foreach (DataRow row in datatable.Rows)
            {
                string statement = Convert.ToString(row["STATEMENT"]);
                string[] parsestring = statement.Trim().Split(' ');
                if (parsestring.Length > 0)
                {
                    if (!parsestring[0].ToUpper().Equals("RULE"))
                    {
                        if (IsNumeric(parsestring[parsestring.Length - 1]))
                        {
                            string strtemp = string.Empty;
                            for (int i = 0; i < parsestring.Length - 1; i++)
		                    {
                			    strtemp += parsestring[i] + " ";
		                    }
                            row["STATEMENT"] = strtemp;
                            double dtemp = Convert.ToDouble(parsestring[parsestring.Length - 1]);

                            foreach (UltraGridRow GridRow in ultraGrid_WaterLevel.Rows)
                            {
                                if (strtemp.IndexOf(Convert.ToString(GridRow.Cells["MODEL_ID"].Value)) > -1)
                                {
                                    Console.WriteLine(strtemp + " : " + Convert.ToString(GridRow.Cells["MODEL_ID"].Value));
                                    Console.WriteLine(dtemp.ToString() + " : " + Convert.ToString(GridRow.Cells["MIN_LEVEL"].Value) + " : " + Convert.ToString(GridRow.Cells["MAX_LEVEL"].Value));
                                    if (dtemp == Convert.ToDouble(GridRow.Cells["MIN_LEVEL"].Value))
                                    {
                                        row["H_LEVEL"] = Convert.ToString(GridRow.Cells["MODEL_ID"].Value) + " MIN LEVEL";
                                        continue;
                                    }
                                    else if (dtemp == Convert.ToDouble(GridRow.Cells["MAX_LEVEL"].Value))
                                    {
                                        row["H_LEVEL"] = Convert.ToString(GridRow.Cells["MODEL_ID"].Value) + " MAX LEVEL";
                                        continue;
                                    }
                                    else row["H_LEVEL"] = (int)dtemp;// parsestring[parsestring.Length - 1];
                                }
                            }
                        }
                    }
                }
            }
            datatable.AcceptChanges();

            ultraGrid_Rule.DataSource = datatable.DefaultView;
        }
        #endregion 시뮬레이션 조건 설정

        /// <summary>
        /// Demand 설정조건 : 날짜 변경 이벤트
        /// Demand 질의 , 흡입수두 질의
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraDateTimeEditor_Day_ValueChanged(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            //QuerySelectDemand();
            QuerySelectReservoirsHead();
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 모델 콤보박스 Change 이벤트 : 시뮬레이션 조건 재설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            QuerySelectWaterLevel();
            QuerySelectPumpPropty();
            //QuerySelectDemand();
            QuerySelectReservoirsHead();
            QuerySelectRuleBase();
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #region 해석결과 임시 저장 테이블
        private void UpdateAnalysisData_INP_DataTable()
        {
            //수위
            m_nodeHeadTable.Rows.Clear();
            foreach (UltraGridRow gridrow in this.ultraGrid_WaterLevel.Rows)
            {
                string filter = "ID = '" + gridrow.Cells["MODEL_ID"].Value + "'";

                /////////수위
                DataRow[] rows = m_nodeTable.Select(filter, "TIME ASC");
                if (rows.Length > 0)
                {
                    DataRow newRow = m_nodeHeadTable.NewRow();
                    newRow["BIZ_NAME"] = gridrow.Cells["BIZ_NAME"].Value;
                    newRow["MODEL_ID"] = gridrow.Cells["MODEL_ID"].Value;
                    newRow["CHECKED"] = "false";
                    foreach (DataRow row in rows)
                    {
                        string timefield = "T" + Convert.ToString(row["TIME"]).Substring(0, 2);
                        if (timefield.Equals("T24")) continue;
                        newRow[timefield] = row["PRESSURE"];
                    }
                    m_nodeHeadTable.Rows.Add(newRow);
                }
            }
            m_nodeHeadTable.AcceptChanges();

            //유량
            m_linkFlowTable.Rows.Clear();
            foreach (UltraGridRow gridrow in this.ultraGrid_PumpPropty.Rows)
            {
                string filter = "ID = '" + gridrow.Cells["PUMP_ID"].Value + "'";

                /////////수위
                DataRow[] rows = m_linkTable.Select(filter, "TIME ASC");
                if (rows.Length > 0)
                {
                    DataRow newRow = m_linkFlowTable.NewRow();
                    newRow["PUMP_NAME"] = gridrow.Cells["PUMP_NAME"].Value;
                    newRow["PUMP_ID"] = gridrow.Cells["PUMP_ID"].Value;
                    newRow["CHECKED"] = "false";
                    foreach (DataRow row in rows)
                    {
                        string timefield = "T" + Convert.ToString(row["TIME"]).Substring(0, 2);
                        if (timefield.Equals("T24")) continue;
                        newRow[timefield] = Convert.ToDouble(row["FLOW"]);
                        //Console.WriteLine(Convert.ToString(row["TIME"]) + " , " + Convert.ToString(row["FLOW"]));
                        //Console.WriteLine(Convert.ToString(gridrow.Cells["PUMP_ID"].Value) + " , " + Convert.ToString(row["FLOW"]));
                    }
                    m_linkFlowTable.Rows.Add(newRow);
                }
            }
            m_linkFlowTable.AcceptChanges();

            //에너지
            m_linkEnergyTable.Rows.Clear();
            foreach (UltraGridRow gridrow in this.ultraGrid_PumpPropty.Rows)
            {
                string filter = "ID = '" + gridrow.Cells["PUMP_ID"].Value + "'";

                /////////수위
                DataRow[] rows = m_linkTable.Select(filter, "TIME ASC");
                if (rows.Length > 0)
                {
                    DataRow newRow = m_linkEnergyTable.NewRow();
                    newRow["PUMP_NAME"] = gridrow.Cells["PUMP_NAME"].Value;
                    newRow["PUMP_ID"] = gridrow.Cells["PUMP_ID"].Value;
                    newRow["CHECKED"] = "false";
                    foreach (DataRow row in rows)
                    {
                        string timefield = "T" + Convert.ToString(row["TIME"]).Substring(0, 2);
                        if (timefield.Equals("T24")) continue;
                        newRow[timefield] = row["ENERGY"];
                    }
                    m_linkEnergyTable.Rows.Add(newRow);
                }
            }
            m_linkEnergyTable.AcceptChanges();

            //펌프가동
            m_linkStatusTable.Rows.Clear();
            foreach (UltraGridRow gridrow in this.ultraGrid_PumpPropty.Rows)
            {
                string filter = "ID = '" + gridrow.Cells["PUMP_ID"].Value + "'";

                /////////수위
                DataRow[] rows = m_linkTable.Select(filter, "TIME ASC");
                if (rows.Length > 0)
                {
                    DataRow newRow = m_linkStatusTable.NewRow();
                    newRow["PUMP_NAME"] = gridrow.Cells["PUMP_NAME"].Value;
                    newRow["PUMP_ID"] = gridrow.Cells["PUMP_ID"].Value;
                    //newRow["CHECKED"] = "True";
                    foreach (DataRow row in rows)
                    {
                        string timefield = "T" + Convert.ToString(row["TIME"]).Substring(0, 2);
                        if (timefield.Equals("T24")) continue;
                        newRow[timefield] = row["STATUS"];
                    }
                    m_linkStatusTable.Rows.Add(newRow);
                }
            }
            m_linkStatusTable.AcceptChanges();

            //수압감시 지점 : Junction
            m_nodePressTable.Rows.Clear();
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.ID                                           ");
            oStringBuilder.AppendLine("  FROM WE_PRES_MONITOR A                            ");
            oStringBuilder.AppendLine("       ,WH_JUNCTIONS B                              ");
            oStringBuilder.AppendLine(" WHERE A.ID = B.ID                                  ");
            oStringBuilder.AppendLine("   AND A.INP_NUMBER = B.INP_NUMBER                  ");
            oStringBuilder.AppendLine("   AND A.INP_NUMBER = '" + cboModel.SelectedValue + "'");

            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            foreach (DataRow id in oDatatable.Rows)
            {
                string filter = "ID = '" + id["ID"] + "'";

                /////////수위
                DataRow[] rows = m_nodeTable.Select(filter, "TIME ASC");
                if (rows.Length > 0)
                {
                    DataRow newRow = m_nodePressTable.NewRow();
                    newRow["ID"] = id["ID"];
                    newRow["CHECKED"] = "false";
                    foreach (DataRow row in rows)
                    {
                        string timefield = "T" + Convert.ToString(row["TIME"]).Substring(0, 2);
                        if (timefield.Equals("T24")) continue;
                        newRow[timefield] = Convert.IsDBNull(row["PRESSURE"]) ? 0.0 : Convert.ToDouble(row["PRESSURE"]) < 0 ? 0.0 : row["PRESSURE"];
                    }
                    m_nodePressTable.Rows.Add(newRow);
                }
            }
            m_nodePressTable.AcceptChanges();

        }

        /// <summary>
        /// 수리모델 해석결과를 NODE, LINK 구분별로 분리하여
        /// DataTable에 저장한다.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="analysisData"></param>
        private void DivideAnalysisData_INP(Hashtable result)
        {
            m_linkTable.Rows.Clear();
            m_nodeTable.Rows.Clear();

            DataRow row = null;
            ////Node, Link별 해석결과 저장
            foreach (string time in result.Keys)
            {
                Hashtable resultByTime = (Hashtable)result[time];
                foreach (string type in resultByTime.Keys)
                {
                    //type은 node,link
                    Hashtable resultByType = (Hashtable)resultByTime[type];
                    foreach (string subtype in resultByType.Keys)
                    {
                        //subtype은 junction,reservior,tank,cvPipe,pipe,pump,prv,psv,pbv,fcv,tcv,gpv
                        ArrayList resultList = (ArrayList)resultByType[subtype];
                        for (int i = 0; i < resultList.Count; i++)
                        {
                            //ID별 해석결과 리스트
                            Hashtable resultData = (Hashtable)resultList[i];
                            switch (type.ToUpper())
                            {
                                case "NODE":
                                    row = m_nodeTable.NewRow();
                                    row["TIME"] = time;
                                    row["ID"] = Convert.ToString(resultData["NODE_ID"]);

                                    row["ELEVATION"] = Math.Round(Convert.ToDouble(resultData["EN_ELEVATION"]), 2);
                                    row["BASEDEMAND"] = Math.Round(Convert.ToDouble(resultData["EN_BASEDEMAND"]), 2);
                                    row["PATTERN"] = Math.Round(Convert.ToDouble(resultData["EN_PATTERN"]), 2);
                                    row["EMITTER"] = Math.Round(Convert.ToDouble(resultData["EN_EMITTER"]), 2);
                                    row["INITQUAL"] = Math.Round(Convert.ToDouble(resultData["EN_INITQUAL"]), 2);
                                    row["SOURCEQUAL"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEQUAL"]), 2);
                                    row["SOURCEPAT"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEPAT"]), 2);
                                    row["SOURCETYPE"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCETYPE"]), 2);
                                    row["TANKLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_TANKLEVEL"]), 2);
                                    row["DEMAND"] = Math.Round(Convert.ToDouble(resultData["EN_DEMAND"]), 2);
                                    row["HEAD"] = Math.Round(Convert.ToDouble(resultData["EN_HEAD"]), 2);
                                    row["PRESSURE"] = Math.Round(Convert.ToDouble(resultData["EN_PRESSURE"]), 1);
                                    row["QUALITY"] = Math.Round(Convert.ToDouble(resultData["EN_QUALITY"]), 2);
                                    row["SOURCEMASS"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEMASS"]), 2);
                                    row["INITVOLUMN"] = Math.Round(Convert.ToDouble(resultData["EN_INITVOLUMN"]), 2);
                                    row["MIXMODEL"] = Math.Round(Convert.ToDouble(resultData["EN_MIXMODEL"]), 2);
                                    row["MIXZONEVOL"] = Math.Round(Convert.ToDouble(resultData["EN_MIXZONEVOL"]), 2);
                                    row["TANKDIAM"] = Math.Round(Convert.ToDouble(resultData["EN_TANKDIAM"]), 2);
                                    row["MINVOLUMN"] = Math.Round(Convert.ToDouble(resultData["EN_MINVOLUMN"]), 2);
                                    row["VOLCURVE"] = Math.Round(Convert.ToDouble(resultData["EN_VOLCURVE"]), 2);
                                    row["MINLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_MINLEVEL"]), 2);
                                    row["MAXLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_MAXLEVEL"]), 2);
                                    row["MIXFRACTION"] = Math.Round(Convert.ToDouble(resultData["EN_MIXFRACTION"]), 2);
                                    row["TANK_KBULK"] = Math.Round(Convert.ToDouble(resultData["EN_TANK_KBULK"]), 2);

                                    m_nodeTable.Rows.Add(row);

                                    break;
                                case "LINK":
                                    row = m_linkTable.NewRow();
                                    row["TIME"] = time;
                                    row["ID"] = Convert.ToString(resultData["LINK_ID"]);
                                    row["DIAMETER"] = Math.Round(Convert.ToDouble(resultData["EN_DIAMETER"]), 2);
                                    row["LENGTH"] = Math.Round(Convert.ToDouble(resultData["EN_LENGTH"]), 2);
                                    row["ROUGHNESS"] = Math.Round(Convert.ToDouble(resultData["EN_ROUGHNESS"]), 2);
                                    row["MINORLOSS"] = Math.Round(Convert.ToDouble(resultData["EN_MINORLOSS"]), 2);
                                    row["INITSTATUS"] = Math.Round(Convert.ToDouble(resultData["EN_INITSTATUS"]), 2);
                                    row["INITSETTING"] = Math.Round(Convert.ToDouble(resultData["EN_INITSETTING"]), 2);
                                    row["KBULK"] = Math.Round(Convert.ToDouble(resultData["EN_KBULK"]), 2);
                                    row["KWALL"] = Math.Round(Convert.ToDouble(resultData["EN_KWALL"]), 2);
                                    row["FLOW"] = Math.Round(Convert.ToDouble(resultData["EN_FLOW"]), 2);
                                    row["VELOCITY"] = Math.Round(Convert.ToDouble(resultData["EN_VELOCITY"]), 2);
                                    row["HEADLOSS"] = Math.Round(Convert.ToDouble(resultData["EN_HEADLOSS"]), 2);
                                    row["STATUS"] = Math.Round(Convert.ToDouble(resultData["EN_STATUS"]), 2);
                                    row["SETTING"] = Math.Round(Convert.ToDouble(resultData["EN_SETTING"]), 2);
                                    row["ENERGY"] = Math.Round(Convert.ToDouble(resultData["EN_ENERGY"]), 2);
                                    m_linkTable.Rows.Add(row);
                                    break;
                            }
                        }
                    }
                }
            }
            m_linkTable.AcceptChanges();
            m_nodeTable.AcceptChanges();
        }
        #endregion 해석결과 임시 저장 테이블

        /// <summary>
        /// 시뮬레이션 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRun_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable resetValue = new Hashtable();
                try
                {
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                    if (!ExecuteAnalysis()) return;

                    ((frmPumpSimulator)m_OwnerForm).ultraGrid_SimuFlow.DataSource = m_linkFlowTable.DefaultView;
                    ((frmPumpSimulator)m_OwnerForm).ultraGrid_SimuHead.DataSource = m_nodeHeadTable.DefaultView;
                    ((frmPumpSimulator)m_OwnerForm).ultraGrid_SimuEnergy.DataSource = m_linkEnergyTable.DefaultView;

                    ((frmPumpSimulator)m_OwnerForm).ultraGrid_SimuPress.DataSource = m_nodePressTable.DefaultView;
                    ((frmPumpSimulator)m_OwnerForm).ultraGrid_SimuStatus.DataSource = m_linkStatusTable.DefaultView;

                    ((frmPumpSimulator)m_OwnerForm).SetSummaryRows();

                    ((frmPumpSimulator)m_OwnerForm).InitializeChart();
                    ((frmPumpSimulator)m_OwnerForm).InitializePumpStatusChart();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    this.Cursor = System.Windows.Forms.Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #region 모델 해석을 위한 INP 데이터 생성
        /// <summary>
        /// 패턴 Section 생성
        /// 압력(수두) 패턴, 유량 패턴을 생성한다.
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreatePatternsSection(ref Hashtable resetValue)
        {
            Hashtable PatternData = new Hashtable();     //Pattern 패턴

            ///수위패턴
            foreach (UltraGridRow item in ((frmPumpSimulator)m_OwnerForm).ultraGrid_ReservoirsHead.Rows)
            {
                ArrayList PatternArray = new ArrayList();
                PatternArray.Add(Convert.IsDBNull(item.Cells["T00"].Value) ? 0 : item.Cells["T00"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T01"].Value) ? 0 : item.Cells["T01"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T02"].Value) ? 0 : item.Cells["T02"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T03"].Value) ? 0 : item.Cells["T03"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T04"].Value) ? 0 : item.Cells["T04"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T05"].Value) ? 0 : item.Cells["T05"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T06"].Value) ? 0 : item.Cells["T06"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T07"].Value) ? 0 : item.Cells["T07"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T08"].Value) ? 0 : item.Cells["T08"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T09"].Value) ? 0 : item.Cells["T09"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T10"].Value) ? 0 : item.Cells["T10"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T11"].Value) ? 0 : item.Cells["T11"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T12"].Value) ? 0 : item.Cells["T12"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T13"].Value) ? 0 : item.Cells["T13"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T14"].Value) ? 0 : item.Cells["T14"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T15"].Value) ? 0 : item.Cells["T15"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T16"].Value) ? 0 : item.Cells["T16"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T17"].Value) ? 0 : item.Cells["T17"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T18"].Value) ? 0 : item.Cells["T18"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T19"].Value) ? 0 : item.Cells["T19"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T20"].Value) ? 0 : item.Cells["T20"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T21"].Value) ? 0 : item.Cells["T21"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T22"].Value) ? 0 : item.Cells["T22"].Value);
                PatternArray.Add(Convert.IsDBNull(item.Cells["T23"].Value) ? 0 : item.Cells["T23"].Value);

                PatternData.Add("H-" + Convert.ToString(item.Cells["MODEL_ID"].Value), PatternArray);
            }


            ///////----------------------------------------------------------------------
            ///////유량 패턴
            ///////순서 : 전체사업장 시간 평균유량 질의
            ///////        상동(가)의 유출유량 - 회룡흡수정의 유입유량= > '1번패턴으로 정의함: 모델에서 Fixed.
            StringBuilder oStringBuilder = new StringBuilder();
            //////junction : Demand의 합계
            //oStringBuilder.AppendLine("SELECT SUM(DEMAND) FROM WH_JUNCTIONS");
            //oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + cboModel.SelectedValue + "'");
            //object o = m_oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null);
            //double sumDemand = 0.0;
            //if (o != null) sumDemand = Convert.ToDouble(o);
            #region 상동(가)의 유출유량 - 회룡흡수정의 유입유량 : 수시로 변경됨으로 인하여 주석이 많음
            ///시간별 실시간 평균유량
            DateTime dt = (DateTime)ultraDateTimeEditor_Day.Value;
            string startDay = dt.Year.ToString() + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0') + "000000";
            string endDay = dt.Year.ToString() + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0') + "235959";

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT *                                                                                        ");
            oStringBuilder.AppendLine("  FROM                                                                                          ");
            oStringBuilder.AppendLine("     (SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/                                      ");
            oStringBuilder.AppendLine("              A.MODEL_ID, A.BIZ_NAME, A.ORDERS                                                  ");
            oStringBuilder.AppendLine("             ,TO_CHAR(TIMESTAMP,'HH24') AS HH                                                   ");
            oStringBuilder.AppendLine("             ,NVL(ROUND(AVG(VALUE) ,2),0) AS DEMAND                                        ");
            oStringBuilder.AppendLine("        FROM  WE_BIZPLA A                                                                       ");
            oStringBuilder.AppendLine("             ,WE_ENERGY_TAGS B                                                                  ");
            oStringBuilder.AppendLine("             ,IF_IHTAGS C                                                                       ");
            oStringBuilder.AppendLine("             ,IF_GATHER_REALTIME REALTIME                                                       ");
            oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("         AND A.INP_NUMBER = B.INP_NUMBER                                                        ");
            oStringBuilder.AppendLine("         AND A.MODEL_ID = B.MODEL_ID                                                            ");
            oStringBuilder.AppendLine("         AND B.TAGNAME = C.TAGNAME                                                              ");
            oStringBuilder.AppendLine("         AND C.BR_CODE = 'FR'                                                                   ");
            oStringBuilder.AppendLine("         AND A.ORDERS IN ('1','2')                                                              ");
            oStringBuilder.AppendLine("         AND B.TAGNAME = REALTIME.TAGNAME                                                       ");
            oStringBuilder.AppendLine("         AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine("                                AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine("       GROUP BY A.MODEL_ID, A.BIZ_NAME, A.ORDERS, TO_CHAR(TIMESTAMP,'HH24') )                   ");
            oStringBuilder.AppendLine(" PIVOT (                                                                                        ");
            oStringBuilder.AppendLine("   AVG(DEMAND)                                                                                  ");
            oStringBuilder.AppendLine("   FOR HH                                                                                       ");
            oStringBuilder.AppendLine("   IN ('00' AS T00,'01' AS T01,'02' AS T02,'03' AS T03,'04' AS T04,'05' AS T05,'06' AS T06      ");
            oStringBuilder.AppendLine("      ,'07' AS T07,'08' AS T08,'09' AS T09,'10' AS T10,'11' AS T11,'12' AS T12,'13' AS T13      ");
            oStringBuilder.AppendLine("      ,'14' AS T14,'15' AS T15,'16' AS T16,'17' AS T17,'18' AS T18,'19' AS T19                  ");
            oStringBuilder.AppendLine("      ,'20' AS T20,'21' AS T21,'22' AS T22,'23' AS T23)                                         ");
            oStringBuilder.AppendLine(" )                                                                                              ");
            oStringBuilder.AppendLine(" ORDER BY ORDERS ASC                                                                            ");

            ///////굳이 union을 안써도 될 것같음
            //////oStringBuilder.AppendLine("SELECT *                                                                                        ");
            //////oStringBuilder.AppendLine("  FROM                                                                                          ");
            //////oStringBuilder.AppendLine("     (SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/                                      ");
            //////oStringBuilder.AppendLine("              A.MODEL_ID, A.BIZ_NAME, A.ORDERS                                                  ");
            //////oStringBuilder.AppendLine("             ,TO_CHAR(TIMESTAMP,'HH24') AS HH                                                   ");
            //////oStringBuilder.AppendLine("             ,NVL(ROUND(AVG(VALUE),2),0) AS DEMAND                                              ");
            //////oStringBuilder.AppendLine("        FROM  WE_BIZPLA A                                                                       ");
            //////oStringBuilder.AppendLine("             ,WE_ENERGY_TAGS B                                                                  ");
            //////oStringBuilder.AppendLine("             ,IF_IHTAGS C                                                                       ");
            //////oStringBuilder.AppendLine("             ,IF_GATHER_REALTIME REALTIME                                                       ");
            //////oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            //////oStringBuilder.AppendLine("         AND A.INP_NUMBER = B.INP_NUMBER                                                        ");
            //////oStringBuilder.AppendLine("         AND A.MODEL_ID = B.MODEL_ID                                                            ");
            //////oStringBuilder.AppendLine("         AND B.TAGNAME = C.TAGNAME                                                              ");
            //////oStringBuilder.AppendLine("         AND C.BR_CODE = 'FR'                                                                   ");
            //////oStringBuilder.AppendLine("         AND A.ORDERS = 1                                                                       ");
            //////oStringBuilder.AppendLine("         AND B.TAGNAME = REALTIME.TAGNAME                                                       ");
            //////oStringBuilder.AppendLine("         AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            //////oStringBuilder.AppendLine("                                AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            //////oStringBuilder.AppendLine("       GROUP BY A.MODEL_ID, A.BIZ_NAME, A.ORDERS, TO_CHAR(TIMESTAMP,'HH24')                     ");
            //////oStringBuilder.AppendLine("      UNION ALL                                                                                 ");
            //////oStringBuilder.AppendLine("      SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/                                      ");
            //////oStringBuilder.AppendLine("              A.MODEL_ID, A.BIZ_NAME, A.ORDERS                                                  ");
            //////oStringBuilder.AppendLine("             ,TO_CHAR(TIMESTAMP,'HH24') AS HH                                                   ");
            //////oStringBuilder.AppendLine("             ,NVL(ROUND(AVG(VALUE),2),0) AS DEMAND                                              ");
            //////oStringBuilder.AppendLine("        FROM  WE_BIZPLA A                                                                       ");
            //////oStringBuilder.AppendLine("             ,WE_ENERGY_TAGS B                                                                  ");
            //////oStringBuilder.AppendLine("             ,IF_IHTAGS C                                                                       ");
            //////oStringBuilder.AppendLine("             ,IF_GATHER_REALTIME REALTIME                                                       ");
            //////oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            //////oStringBuilder.AppendLine("         AND A.INP_NUMBER = B.INP_NUMBER                                                        ");
            //////oStringBuilder.AppendLine("         AND A.MODEL_ID = B.MODEL_ID                                                            ");
            //////oStringBuilder.AppendLine("         AND B.TAGNAME = C.TAGNAME                                                              ");
            //////oStringBuilder.AppendLine("         AND C.BR_CODE = 'FR'                                                                   ");
            //////oStringBuilder.AppendLine("         AND A.ORDERS = 2                                                                       ");
            //////oStringBuilder.AppendLine("         AND B.TAGNAME = REALTIME.TAGNAME                                                       ");
            //////oStringBuilder.AppendLine("         AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            //////oStringBuilder.AppendLine("                                AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            //////oStringBuilder.AppendLine("       GROUP BY A.MODEL_ID, A.BIZ_NAME, A.ORDERS, TO_CHAR(TIMESTAMP,'HH24')                     ");
            //////oStringBuilder.AppendLine("      UNION ALL                                                                                 ");
            //////oStringBuilder.AppendLine("      SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/                                      ");
            //////oStringBuilder.AppendLine("                    A.MODEL_ID, A.BIZ_NAME, A.ORDERS                                            ");
            //////oStringBuilder.AppendLine("                   ,TO_CHAR(TIMESTAMP,'HH24') AS HH                                             ");
            //////oStringBuilder.AppendLine("                   ,NVL(ROUND(AVG(VALUE),2),0) AS DEMAND                                        ");
            //////oStringBuilder.AppendLine("        FROM  WE_BIZPLA A                                                                       ");
            //////oStringBuilder.AppendLine("             ,WE_ENERGY_TAGS B                                                                  ");
            //////oStringBuilder.AppendLine("             ,IF_IHTAGS C                                                                       ");
            //////oStringBuilder.AppendLine("             ,IF_GATHER_REALTIME REALTIME                                                       ");
            //////oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            //////oStringBuilder.AppendLine("         AND A.INP_NUMBER = B.INP_NUMBER                                                        ");
            //////oStringBuilder.AppendLine("         AND A.MODEL_ID = B.MODEL_ID                                                            ");
            //////oStringBuilder.AppendLine("         AND B.TAGNAME = C.TAGNAME                                                              ");
            //////oStringBuilder.AppendLine("         AND C.BR_CODE = 'FR'                                                                   ");
            //////oStringBuilder.AppendLine("         AND A.ORDERS = 3                                                                       ");
            //////oStringBuilder.AppendLine("         AND B.TAGNAME = REALTIME.TAGNAME                                                       ");
            //////oStringBuilder.AppendLine("         AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            //////oStringBuilder.AppendLine("                                AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            //////oStringBuilder.AppendLine("             GROUP BY A.MODEL_ID, A.BIZ_NAME, A.ORDERS, TO_CHAR(TIMESTAMP,'HH24'))              ");
            //////oStringBuilder.AppendLine(" PIVOT (                                                                                        ");
            //////oStringBuilder.AppendLine("   AVG(DEMAND)                                                                                  ");
            //////oStringBuilder.AppendLine("   FOR HH                                                                                       ");
            //////oStringBuilder.AppendLine("   IN ('00' AS T00,'01' AS T01,'02' AS T02,'03' AS T03,'04' AS T04,'05' AS T05,'06' AS T06      ");
            //////oStringBuilder.AppendLine("      ,'07' AS T07,'08' AS T08,'09' AS T09,'10' AS T10,'11' AS T11,'12' AS T12,'13' AS T13      ");
            //////oStringBuilder.AppendLine("      ,'14' AS T14,'15' AS T15,'16' AS T16,'17' AS T17,'18' AS T18,'19' AS T19                  ");
            //////oStringBuilder.AppendLine("      ,'20' AS T20,'21' AS T21,'22' AS T22,'23' AS T23)                                         ");
            //////oStringBuilder.AppendLine(" )                                                                                              ");
            //////oStringBuilder.AppendLine(" ORDER BY ORDERS ASC                                                                            ");

            DataTable oDatatable2 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            if (oDatatable2.Rows.Count != 2) return;

            ///        상동(가) 유량에서 - 회룡(가) 유량을 뺀다.
            if (Convert.ToString(oDatatable2.Rows[1]["ORDERS"]).Equals("2"))
            {
                oDatatable2.Rows[0]["T00"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T00"]) ? 0 : oDatatable2.Rows[0]["T00"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T00"]) ? 0 : oDatatable2.Rows[1]["T00"]);
                oDatatable2.Rows[0]["T01"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T01"]) ? 0 : oDatatable2.Rows[0]["T01"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T01"]) ? 0 : oDatatable2.Rows[1]["T01"]);
                oDatatable2.Rows[0]["T02"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T02"]) ? 0 : oDatatable2.Rows[0]["T02"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T02"]) ? 0 : oDatatable2.Rows[1]["T02"]);
                oDatatable2.Rows[0]["T03"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T03"]) ? 0 : oDatatable2.Rows[0]["T03"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T03"]) ? 0 : oDatatable2.Rows[1]["T03"]);
                oDatatable2.Rows[0]["T04"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T04"]) ? 0 : oDatatable2.Rows[0]["T04"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T04"]) ? 0 : oDatatable2.Rows[1]["T04"]);
                oDatatable2.Rows[0]["T05"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T05"]) ? 0 : oDatatable2.Rows[0]["T05"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T05"]) ? 0 : oDatatable2.Rows[1]["T05"]);
                oDatatable2.Rows[0]["T06"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T06"]) ? 0 : oDatatable2.Rows[0]["T06"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T06"]) ? 0 : oDatatable2.Rows[1]["T06"]);
                oDatatable2.Rows[0]["T07"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T07"]) ? 0 : oDatatable2.Rows[0]["T07"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T07"]) ? 0 : oDatatable2.Rows[1]["T07"]);
                oDatatable2.Rows[0]["T08"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T08"]) ? 0 : oDatatable2.Rows[0]["T08"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T08"]) ? 0 : oDatatable2.Rows[1]["T08"]);
                oDatatable2.Rows[0]["T09"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T09"]) ? 0 : oDatatable2.Rows[0]["T09"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T09"]) ? 0 : oDatatable2.Rows[1]["T09"]);
                oDatatable2.Rows[0]["T10"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T10"]) ? 0 : oDatatable2.Rows[0]["T10"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T10"]) ? 0 : oDatatable2.Rows[1]["T10"]);
                oDatatable2.Rows[0]["T11"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T11"]) ? 0 : oDatatable2.Rows[0]["T11"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T11"]) ? 0 : oDatatable2.Rows[1]["T11"]);
                oDatatable2.Rows[0]["T12"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T12"]) ? 0 : oDatatable2.Rows[0]["T12"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T12"]) ? 0 : oDatatable2.Rows[1]["T12"]);
                oDatatable2.Rows[0]["T13"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T13"]) ? 0 : oDatatable2.Rows[0]["T13"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T13"]) ? 0 : oDatatable2.Rows[1]["T13"]);
                oDatatable2.Rows[0]["T14"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T14"]) ? 0 : oDatatable2.Rows[0]["T14"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T14"]) ? 0 : oDatatable2.Rows[1]["T14"]);
                oDatatable2.Rows[0]["T15"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T15"]) ? 0 : oDatatable2.Rows[0]["T15"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T15"]) ? 0 : oDatatable2.Rows[1]["T15"]);
                oDatatable2.Rows[0]["T16"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T16"]) ? 0 : oDatatable2.Rows[0]["T16"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T16"]) ? 0 : oDatatable2.Rows[1]["T16"]);
                oDatatable2.Rows[0]["T17"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T17"]) ? 0 : oDatatable2.Rows[0]["T17"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T17"]) ? 0 : oDatatable2.Rows[1]["T17"]);
                oDatatable2.Rows[0]["T18"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T18"]) ? 0 : oDatatable2.Rows[0]["T18"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T18"]) ? 0 : oDatatable2.Rows[1]["T18"]);
                oDatatable2.Rows[0]["T19"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T19"]) ? 0 : oDatatable2.Rows[0]["T19"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T19"]) ? 0 : oDatatable2.Rows[1]["T19"]);
                oDatatable2.Rows[0]["T20"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T20"]) ? 0 : oDatatable2.Rows[0]["T20"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T20"]) ? 0 : oDatatable2.Rows[1]["T20"]);
                oDatatable2.Rows[0]["T21"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T21"]) ? 0 : oDatatable2.Rows[0]["T21"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T21"]) ? 0 : oDatatable2.Rows[1]["T21"]);
                oDatatable2.Rows[0]["T22"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T22"]) ? 0 : oDatatable2.Rows[0]["T22"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T22"]) ? 0 : oDatatable2.Rows[1]["T22"]);
                oDatatable2.Rows[0]["T23"] = Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[0]["T23"]) ? 0 : oDatatable2.Rows[0]["T23"]) - Convert.ToDouble(Convert.IsDBNull(oDatatable2.Rows[1]["T23"]) ? 0 : oDatatable2.Rows[1]["T23"]);

                oDatatable2.Rows[1].Delete();
            }
            oDatatable2.AcceptChanges();
            if (oDatatable2.Rows.Count == 0) return;

            ///// 사업장의 유량값을 화면에서 설정된 유량으로 나눈다.
            foreach (DataRow row in oDatatable2.Rows)
            {
                ArrayList PatternArray = new ArrayList();
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T00"]) ? 0 : row["T00"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T01"]) ? 0 : row["T01"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T02"]) ? 0 : row["T02"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T03"]) ? 0 : row["T03"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T04"]) ? 0 : row["T04"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T05"]) ? 0 : row["T05"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T06"]) ? 0 : row["T06"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T07"]) ? 0 : row["T07"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T08"]) ? 0 : row["T08"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T09"]) ? 0 : row["T09"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T10"]) ? 0 : row["T10"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T11"]) ? 0 : row["T11"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T12"]) ? 0 : row["T12"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T13"]) ? 0 : row["T13"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T14"]) ? 0 : row["T14"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T15"]) ? 0 : row["T15"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T16"]) ? 0 : row["T16"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T17"]) ? 0 : row["T17"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T18"]) ? 0 : row["T18"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T19"]) ? 0 : row["T19"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T20"]) ? 0 : row["T20"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T21"]) ? 0 : row["T21"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T22"]) ? 0 : row["T22"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));
                //PatternArray.Add(Math.Round((Convert.ToDouble(Convert.IsDBNull(row["T23"]) ? 0 : row["T23"]) / Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)), 2));

                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T00"]) ? 0 : row["T00"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T01"]) ? 0 : row["T01"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T02"]) ? 0 : row["T02"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T03"]) ? 0 : row["T03"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T04"]) ? 0 : row["T04"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T05"]) ? 0 : row["T05"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T06"]) ? 0 : row["T06"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T07"]) ? 0 : row["T07"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T08"]) ? 0 : row["T08"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T09"]) ? 0 : row["T09"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T10"]) ? 0 : row["T10"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T11"]) ? 0 : row["T11"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T12"]) ? 0 : row["T12"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T13"]) ? 0 : row["T13"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T14"]) ? 0 : row["T14"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T15"]) ? 0 : row["T15"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T16"]) ? 0 : row["T16"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T17"]) ? 0 : row["T17"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T18"]) ? 0 : row["T18"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T19"]) ? 0 : row["T19"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T20"]) ? 0 : row["T20"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T21"]) ? 0 : row["T21"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T22"]) ? 0 : row["T22"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T23"]) ? 0 : row["T23"]));


                PatternData.Add("1", PatternArray);

            }
            #endregion 상동(가)의 유출유량 - 회룡흡수정의 유입유량

            #region 내장배수지의 유출 유량 : 패턴ID = 2로 Fixed됨
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT *                                                                                        ");
            oStringBuilder.AppendLine("  FROM                                                                                          ");
            oStringBuilder.AppendLine("     (SELECT  /*IF_GATHER_REALTIME IF_GATHER_REALTIME_PK*/                                      ");
            oStringBuilder.AppendLine("              A.MODEL_ID, A.BIZ_NAME, A.ORDERS                                                  ");
            oStringBuilder.AppendLine("             ,TO_CHAR(TIMESTAMP,'HH24') AS HH                                                   ");
            oStringBuilder.AppendLine("             ,NVL(ROUND(AVG(VALUE) ,2),0) AS DEMAND                                        ");
            oStringBuilder.AppendLine("        FROM  WE_BIZPLA A                                                                       ");
            oStringBuilder.AppendLine("             ,WE_ENERGY_TAGS B                                                                  ");
            oStringBuilder.AppendLine("             ,IF_IHTAGS C                                                                       ");
            oStringBuilder.AppendLine("             ,IF_GATHER_REALTIME REALTIME                                                       ");
            oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("         AND A.INP_NUMBER = B.INP_NUMBER                                                        ");
            oStringBuilder.AppendLine("         AND A.MODEL_ID = B.MODEL_ID                                                            ");
            oStringBuilder.AppendLine("         AND B.TAGNAME = C.TAGNAME                                                              ");
            oStringBuilder.AppendLine("         AND C.BR_CODE = 'FR'                                                                   ");
            oStringBuilder.AppendLine("         AND A.ORDERS IN ('3')                                                                  ");
            oStringBuilder.AppendLine("         AND B.TAGNAME = REALTIME.TAGNAME                                                       ");
            oStringBuilder.AppendLine("         AND REALTIME.TIMESTAMP BETWEEN TO_DATE('" + startDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine("                                AND     TO_DATE('" + endDay + "','YYYYMMDDHH24MISS') ");
            oStringBuilder.AppendLine("       GROUP BY A.MODEL_ID, A.BIZ_NAME, A.ORDERS, TO_CHAR(TIMESTAMP,'HH24') )                   ");
            oStringBuilder.AppendLine(" PIVOT (                                                                                        ");
            oStringBuilder.AppendLine("   AVG(DEMAND)                                                                                  ");
            oStringBuilder.AppendLine("   FOR HH                                                                                       ");
            oStringBuilder.AppendLine("   IN ('00' AS T00,'01' AS T01,'02' AS T02,'03' AS T03,'04' AS T04,'05' AS T05,'06' AS T06      ");
            oStringBuilder.AppendLine("      ,'07' AS T07,'08' AS T08,'09' AS T09,'10' AS T10,'11' AS T11,'12' AS T12,'13' AS T13      ");
            oStringBuilder.AppendLine("      ,'14' AS T14,'15' AS T15,'16' AS T16,'17' AS T17,'18' AS T18,'19' AS T19                  ");
            oStringBuilder.AppendLine("      ,'20' AS T20,'21' AS T21,'22' AS T22,'23' AS T23)                                         ");
            oStringBuilder.AppendLine(" )                                                                                              ");
            oStringBuilder.AppendLine(" ORDER BY ORDERS ASC                                                                            ");

            DataTable oDatatable3 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            foreach (DataRow row in oDatatable3.Rows)
            {
                ArrayList PatternArray = new ArrayList();
                
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T00"]) ? 0 : row["T00"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T01"]) ? 0 : row["T01"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T02"]) ? 0 : row["T02"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T03"]) ? 0 : row["T03"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T04"]) ? 0 : row["T04"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T05"]) ? 0 : row["T05"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T06"]) ? 0 : row["T06"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T07"]) ? 0 : row["T07"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T08"]) ? 0 : row["T08"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T09"]) ? 0 : row["T09"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T10"]) ? 0 : row["T10"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T11"]) ? 0 : row["T11"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T12"]) ? 0 : row["T12"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T13"]) ? 0 : row["T13"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T14"]) ? 0 : row["T14"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T15"]) ? 0 : row["T15"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T16"]) ? 0 : row["T16"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T17"]) ? 0 : row["T17"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T18"]) ? 0 : row["T18"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T19"]) ? 0 : row["T19"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T20"]) ? 0 : row["T20"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T21"]) ? 0 : row["T21"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T22"]) ? 0 : row["T22"]));
                PatternArray.Add(Convert.ToString(Convert.IsDBNull(row["T23"]) ? 0 : row["T23"]));


                PatternData.Add("2", PatternArray);

            }
            #endregion 내장배수지의 유출 유량 : 패턴ID = 2로 Fixed됨

            #region 기타 유량 패턴 : 패턴ID = 3로 Fixed됨
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT PATTERN_ID, MULTIPLIER FROM WH_PATTERNS");
            oStringBuilder.AppendLine(" WHERE INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("   AND PATTERN_ID NOT IN ('1','2')");
            oStringBuilder.AppendLine(" ORDER BY TO_NUMBER(IDX) ASC");
            DataTable oDatatable4 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            DataTable IDTable = oDatatable4.DefaultView.ToTable(true, "PATTERN_ID");
            foreach (DataRow IDRow in IDTable.Rows)
            {
                ArrayList PatternArray = new ArrayList();
                foreach (DataRow row in oDatatable4.Rows)
                {
                    PatternArray.Add(Convert.ToString(row["MULTIPLIER"]));
                }
                PatternData.Add(IDRow["PATTERN_ID"], PatternArray);
            }            

            #endregion
 

            

            resetValue.Add("pattern", PatternData);

        }

        /// <summary>
        /// 커브 Section 생성
        /// 펌프특성조건 : 설정된 커브ID에 해당하는 커브정보를
        /// WC_CURVEDATA 테이블에서 질의하여 펌프커브, 효율커브의
        /// 커브패턴을 생성한다.
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreateCurvesSection(ref Hashtable resetValue)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            ArrayList CurveArray = new ArrayList();
            foreach (UltraGridRow CurveRow in this.ultraGrid_PumpPropty.Rows)
            {
                ///펌프커브 패턴
                oStringBuilder.Remove(0, oStringBuilder.Length);

                oStringBuilder.AppendLine("SELECT A.PUMP_ID, B.X_VALUE, B.Y_VALUE");
                oStringBuilder.AppendLine("  FROM WE_PUMPINFO A");
                oStringBuilder.AppendLine("      ,WE_CURVEDATA B");
                oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
                oStringBuilder.AppendLine("   AND A.PUMPCURVE_ID = B.ID AND GUBUN = '1'");
                oStringBuilder.AppendLine("   AND A.PUMP_ID = '" + CurveRow.Cells["PUMP_ID"].Value + "'");
                oStringBuilder.AppendLine("   AND A.PUMPCURVE_ID = '" + CurveRow.Cells["PUMPCURVE_ID"].Value + "'");
                oStringBuilder.AppendLine(" ORDER BY A.PUMP_ID, X_VALUE ASC");

                DataTable oDatatable1 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                if (oDatatable1.Rows.Count > 0)
                {
                    foreach (DataRow item in oDatatable1.Rows)
                    {
                        Hashtable CurveData = new Hashtable();     //Curve 패턴
                        CurveData.Add("ID", "P-" + Convert.ToString(CurveRow.Cells["PUMP_ID"].Value));
                        CurveData.Add("X", item["X_VALUE"]);
                        CurveData.Add("Y", item["Y_VALUE"]);

                        CurveArray.Add(CurveData);
                        //CurveArray.Add(item["X_VALUE"]);
                        //CurveArray.Add(item["Y_VALUE"]);
                    }
                    //CurveData.Add("P-" + Convert.ToString(CurveRow.Cells["PUMP_ID"].Value), CurveArray);
                }

                ///효율커브 패턴
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT A.PUMP_ID, B.X_VALUE, B.Y_VALUE");
                oStringBuilder.AppendLine("  FROM WE_PUMPINFO A");
                oStringBuilder.AppendLine("      ,WE_CURVEDATA B");
                oStringBuilder.AppendLine(" WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
                oStringBuilder.AppendLine("   AND A.EFFCURVE_ID = B.ID AND GUBUN = '2'");
                oStringBuilder.AppendLine("   AND A.PUMP_ID = '" + CurveRow.Cells["PUMP_ID"].Value + "'");
                oStringBuilder.AppendLine("   AND A.EFFCURVE_ID = '" + CurveRow.Cells["EFFCURVE_ID"].Value + "'");
                oStringBuilder.AppendLine(" ORDER BY A.PUMP_ID, X_VALUE ASC");

                DataTable oDatatable2 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                if (oDatatable2.Rows.Count > 0)
                {
                    foreach (DataRow item in oDatatable2.Rows)
                    {
                        Hashtable CurveData = new Hashtable();     //Curve 패턴
                        CurveData.Add("ID", "E-" + Convert.ToString(CurveRow.Cells["PUMP_ID"].Value));
                        CurveData.Add("X", item["X_VALUE"]);
                        CurveData.Add("Y", item["Y_VALUE"]);

                        CurveArray.Add(CurveData);
                        //CurveArray.Add(item["X_VALUE"]);
                        //CurveArray.Add(item["Y_VALUE"]);
                    }
                    //CurveData.Add("E-" + Convert.ToString(CurveRow.Cells["PUMP_ID"].Value), CurveArray);
                }                
            }

            resetValue.Add("curve", CurveArray);
        }

        //private void CreateJunctionDemandSection(ref Hashtable resetValue)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("SELECT A.ID, A.ELEV, ROUND(A.DEMAND / B.SUMD, 6) AS DEMAND      ");
        //    oStringBuilder.AppendLine("  FROM WH_JUNCTIONS A                                           ");
        //    oStringBuilder.AppendLine("     ,(SELECT INP_NUMBER, SUM(DEMAND) AS SUMD FROM WH_JUNCTIONS ");
        //    oStringBuilder.AppendLine("        WHERE INP_NUMBER = '" + cboModel.SelectedValue + "'");
        //    oStringBuilder.AppendLine("        GROUP BY INP_NUMBER) B                                  ");
        //    oStringBuilder.AppendLine("WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
        //    oStringBuilder.AppendLine("  AND A.INP_NUMBER = B.INP_NUMBER                               ");

        //    DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

        //    Hashtable JunctionData = new Hashtable();     //Junction 
        //    foreach (DataRow item in oDatatable.Rows)
        //    {
        //        JunctionData.Add(item["ID"], Convert.ToString(Convert.IsDBNull(item["DEMAND"]) ? 0 : Convert.ToDouble(item["DEMAND"]) * Convert.ToDouble(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value)));

        //        //Console.WriteLine(item["ID"].ToString() + " , " + Convert.ToString(item["DEMAND"]) + " , " + Convert.ToString(ultraGrid_Demand.Rows[0].Cells["DEMAND"].Value));
        //    }

        //    resetValue.Add("demand", JunctionData);
        //}
        ///// <summary>
        ///// Junction Pattern Section 생성
        ///// </summary>
        ///// <param name="resetValue"></param>
        //private void CreateJunctionPatternSection(ref Hashtable resetValue)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("SELECT A.ID, A.ELEV, A.DEMAND                                  ");
        //    oStringBuilder.AppendLine("  FROM WH_JUNCTIONS A                                           ");
        //    oStringBuilder.AppendLine("WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");

        //    DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

        //    Hashtable JunctionData = new Hashtable();     //Junction 패턴
        //    foreach (DataRow item in oDatatable.Rows)
        //    {
        //        JunctionData.Add(item["ID"], "F-1");
        //    }

        //    resetValue.Add("junctionPattern", JunctionData);

        //    //foreach (UltraGridRow CurveRow in this.ultraGrid_PumpPropty.Rows)
        //    //{
        //    //    ///펌프커브 패턴
        //    //    oStringBuilder.Remove(0, oStringBuilder.Length);
        //    //    oStringBuilder.AppendLine("      SELECT  A.PUMP_ID, B.X_VALUE, B.Y_VALUE  ");
        //    //    oStringBuilder.AppendLine("        FROM  WE_PUMPINFO A                                              ");
        //    //    oStringBuilder.AppendLine("             ,WATERNET.WE_CURVEDATA B                                    ");
        //    //    oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
        //    //    oStringBuilder.AppendLine("         AND A.PUMPCURVE_ID = B.ID AND GUBUN = '1'                       ");
        //    //    oStringBuilder.AppendLine("         AND A.PUMP_ID = '" + CurveRow.Cells["PUMP_ID"].Value + "'");
        //    //    oStringBuilder.AppendLine("         AND A.PUMPCURVE_ID = '" + CurveRow.Cells["PUMPCURVE_ID"].Value + "'");
        //    //    oStringBuilder.AppendLine("       ORDER BY A.PUMP_ID, X_VALUE ASC                                    ");

        //    //    DataTable oDatatable1 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
        //    //    if (oDatatable1.Rows.Count > 0)
        //    //    {
        //    //        ArrayList CurveArray = new ArrayList();
        //    //        foreach (DataRow item in oDatatable1.Rows)
        //    //        {
        //    //            CurveArray.Add(item["X_VALUE"]);
        //    //            CurveArray.Add(item["Y_VALUE"]);
        //    //        }
        //    //        CurveData.Add("P-" + Convert.ToString(CurveRow.Cells["PUMP_ID"].Value), CurveArray);
        //    //    }

        //    //    ///효율커브 패턴
        //    //    oStringBuilder.Remove(0, oStringBuilder.Length);
        //    //    oStringBuilder.AppendLine("      SELECT  A.PUMP_ID, B.X_VALUE, B.Y_VALUE  ");
        //    //    oStringBuilder.AppendLine("        FROM  WE_PUMPINFO A                                              ");
        //    //    oStringBuilder.AppendLine("             ,WATERNET.WE_CURVEDATA B                                    ");
        //    //    oStringBuilder.AppendLine("       WHERE A.INP_NUMBER = '" + cboModel.SelectedValue + "'");
        //    //    oStringBuilder.AppendLine("         AND A.EFFCURVE_ID = B.ID AND GUBUN = '2'                       ");
        //    //    oStringBuilder.AppendLine("         AND A.PUMP_ID = '" + CurveRow.Cells["PUMP_ID"].Value + "'");
        //    //    oStringBuilder.AppendLine("         AND A.EFFCURVE_ID = '" + CurveRow.Cells["EFFCURVE_ID"].Value + "'");
        //    //    oStringBuilder.AppendLine("       ORDER BY A.PUMP_ID, X_VALUE ASC                                    ");

        //    //    DataTable oDatatable2 = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
        //    //    if (oDatatable2.Rows.Count > 0)
        //    //    {
        //    //        ArrayList CurveArray = new ArrayList();
        //    //        foreach (DataRow item in oDatatable2.Rows)
        //    //        {
        //    //            CurveArray.Add(item["X_VALUE"]);
        //    //            CurveArray.Add(item["Y_VALUE"]);
        //    //        }
        //    //        CurveData.Add("E-" + Convert.ToString(CurveRow.Cells["PUMP_ID"].Value), CurveArray);
        //    //    }
        //    //}

            
        //}

        /// <summary>
        /// Pump Section 생성
        /// ID , Parameters
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreatePumpSection(ref Hashtable resetValue)
        {
            Hashtable PumpData = new Hashtable();     //Pump Data
            foreach (UltraGridRow gridrow in this.ultraGrid_PumpPropty.Rows)
            {
                if (Convert.IsDBNull(gridrow.Cells["PUMP_ID"].Value)) continue;
                string param = string.Empty;
                param = "HEAD" + "\t" + "P-" + gridrow.Cells["PUMP_ID"].Value + "\t";
                param += "SPEED" + "\t" + Convert.ToString(Math.Round(Convert.ToDouble(gridrow.Cells["SPEED"].Value),2)) + "\t";
                //param += "PATTERN" + "\t" + "F-" + gridrow.Cells["MODEL_ID"].Value;
                PumpData.Add(gridrow.Cells["PUMP_ID"].Value, param);
            }

            resetValue.Add("pump", PumpData);

            ArrayList EnergyArray = new ArrayList();    //Energy Data
            StringBuilder oStringBuilder = new StringBuilder();
            ///Energy Section에서 Pump 효율커브 ID를 제외한 statement를 사용하기 위해 추가
            oStringBuilder.AppendLine("SELECT  ENERGY_STATEMENT ");
            oStringBuilder.AppendLine("  FROM  WH_ENERGY                                    ");
            oStringBuilder.AppendLine(" WHERE  INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("MINUS ");
            oStringBuilder.AppendLine("SELECT  ENERGY_STATEMENT ");
            oStringBuilder.AppendLine("  FROM  WH_ENERGY                                    ");
            oStringBuilder.AppendLine(" WHERE  INP_NUMBER = '" + cboModel.SelectedValue + "'");
            oStringBuilder.AppendLine("   AND UPPER(ENERGY_STATEMENT) LIKE '%PUMP%' ");
            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            foreach (DataRow row in oDatatable.Rows)
            {
                if (Convert.IsDBNull(row["ENERGY_STATEMENT"])) continue;
                EnergyArray.Add(row["ENERGY_STATEMENT"]);
            }

            foreach (UltraGridRow gridrow in this.ultraGrid_PumpPropty.Rows)
            {
                if (Convert.IsDBNull(gridrow.Cells["PUMP_ID"].Value)) continue;

                EnergyArray.Add("Pump" + "\t" + gridrow.Cells["PUMP_ID"].Value + "\t" + "Efficiency" + "\t" + "E-" + gridrow.Cells["PUMP_ID"].Value);
            }
            resetValue.Add("energy", EnergyArray);
        }

        private void CreateRuleSection(ref Hashtable resetValue)
        {
            Hashtable RuleData = new Hashtable();     //Rule 패턴
            ArrayList RuleArray = new ArrayList();

            foreach (UltraGridRow row in ultraGrid_Rule.Rows)
            {
                if (string.IsNullOrEmpty(row.Cells["H_LEVEL"].Text))
                {
                    RuleArray.Add(row.Cells["STATEMENT"].Text);
                }
                else
                {
                    string strtemp = row.Cells["STATEMENT"].Text;
                    switch (row.Cells["H_LEVEL"].Text)
                    {
                        case "HRT MIN LEVEL" :
                            foreach (UltraGridRow item in ultraGrid_WaterLevel.Rows)
                            {
                                if (Convert.ToString(item.Cells["MODEL_ID"].Value).Equals("HRT"))
                                {
                                    strtemp += " " + item.Cells["MIN_LEVEL"].Value;
                                    break;
                                }   
                            }
                            break;
                        case "HRT MAX LEVEL" :
                            foreach (UltraGridRow item in ultraGrid_WaterLevel.Rows)
                            {
                                if (Convert.ToString(item.Cells["MODEL_ID"].Value).Equals("HRT"))
                                {
                                    strtemp += " " + item.Cells["MAX_LEVEL"].Value;
                                    break;
                                }
                            }
                            break;
                        case "NST MIN LEVEL" :
                            foreach (UltraGridRow item in ultraGrid_WaterLevel.Rows)
                            {
                                if (Convert.ToString(item.Cells["MODEL_ID"].Value).Equals("NST"))
                                {
                                    strtemp += " " + item.Cells["MIN_LEVEL"].Value;
                                    break;
                                }
                            }
                            break;
                        case "NST MAX LEVEL" :
                            foreach (UltraGridRow item in ultraGrid_WaterLevel.Rows)
                            {
                                if (Convert.ToString(item.Cells["MODEL_ID"].Value).Equals("NST"))
                                {
                                    strtemp += " " + item.Cells["MAX_LEVEL"].Value;
                                    break;
                                }
                            }
                            break;
                        default:
                            strtemp += " " + row.Cells["H_LEVEL"].Text;
                            break;
                    }
                    
                    RuleArray.Add(strtemp);
                }
            }
            //foreach (string item in txt_Rule.Lines)
            //{
            //    if (item.Trim().Length == 0) continue;
            //    RuleArray.Add(item);
            //}

            resetValue.Add("rule", RuleArray);

        }

        /// <summary>
        /// 레저버 섹션 생성
        /// 패턴 섹션의 압력패턴ID를 [Pattern 필드]에 추가
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreateReservoirSection(ref Hashtable resetValue)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT  ID, HEAD ");
            oStringBuilder.AppendLine("  FROM  WH_reservoirs                                                             ");
            oStringBuilder.AppendLine(" WHERE  INP_NUMBER = '" + cboModel.SelectedValue + "'");
            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            Hashtable ReservoirskData = new Hashtable();     //Reservoirs INP value

            //foreach (UltraGridRow gridrow in this.ultraGrid_WaterLevel.Rows)
            //{
                foreach (DataRow datarow in oDatatable.Rows)
                {
                    //if (Convert.IsDBNull(gridrow.Cells["MODEL_ID"].Value)) continue;

                    ReservoirskData.Add(datarow["ID"], "H-" + Convert.ToString(datarow["ID"]));

                    //if (Convert.ToString(gridrow.Cells["MODEL_ID"].Value).Equals(Convert.ToString(datarow["ID"])))
                    //{
                    //    ReservoirskData.Add(gridrow.Cells["MODEL_ID"].Value, "H-" + Convert.ToString(gridrow.Cells["MODEL_ID"].Value));
                    //    continue;
                    //}
                }
            //}

            resetValue.Add("reservoirPattern", ReservoirskData);
        }

        /// <summary>
        /// Tanks Section 생성
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreateTankSection(ref Hashtable resetValue)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT  ID, ELEV, INITLVL, MINLVL, MAXLVL, DIAM, MINVOL, VOLCURVE_ID ");
            oStringBuilder.AppendLine("  FROM  WH_TANK                                                             ");
            oStringBuilder.AppendLine(" WHERE  INP_NUMBER = '" + cboModel.SelectedValue + "'");
            DataTable oDatatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            ArrayList tankArray = new ArrayList();
            foreach (UltraGridRow gridrow in this.ultraGrid_WaterLevel.Rows)
            {
                foreach (DataRow datarow in oDatatable.Rows)
                {
                    if (Convert.IsDBNull(gridrow.Cells["MODEL_ID"].Value)) continue;

                    if (Convert.ToString(gridrow.Cells["MODEL_ID"].Value).Equals(Convert.ToString(datarow["ID"])))
                    {
                        Hashtable TankData = new Hashtable();     //Tank INP value
                        TankData.Add("ID", Convert.ToString(gridrow.Cells["MODEL_ID"].Value));
                        TankData.Add("ELEV", Convert.ToString(datarow["ELEV"]));
                        TankData.Add("INITLVL", Convert.ToString(gridrow.Cells["INT_LEVEL"].Value));
                        TankData.Add("MINLVL", Convert.ToString(gridrow.Cells["MIN_LEVEL"].Value));
                        TankData.Add("MAXLVL", Convert.ToString(gridrow.Cells["MAX_LEVEL"].Value));
                        TankData.Add("DIAM", Convert.ToString(datarow["DIAM"]));
                        TankData.Add("MINVOL", Convert.ToString(datarow["MINVOL"]));
                        TankData.Add("VOLCURVE_ID", Convert.ToString(datarow["VOLCURVE_ID"]));

                        tankArray.Add(TankData);
                        continue;
                    }
                }
            }

            resetValue.Add("tank", tankArray);
                        
        }

        private void CreateOptionsSection(ref Hashtable resetValue)
        {
            Hashtable options = new Hashtable();   //INP Options Section
            //options.Add("Quality", "None mg/L");
            options.Add("Units", "CMH");
            //options.Add("Headloss", "H-W");
            //options.Add("Specific Gravity", "1");
            //options.Add("Viscosity", "1");
            //options.Add("Trials", "40");
            //options.Add("Accuracy", "0.001");
            //options.Add("Unbalanced Continue", "10");
            //options.Add("Pattern", "1");
            //options.Add("Demand Multiplier", "1");
            //options.Add("Emitter Exponent", "0.5");
            //options.Add("Diffusivity", "1");
            //options.Add("Tolerance", "0.01");

            resetValue.Add("option", options);
        }

        private void CreateTimesSection(ref Hashtable resetValue)
        {
            Hashtable times = new Hashtable(); //INP Times Section
            times.Add("Report Timestep", "1:00");
            times.Add("Duration", "24:00");
            //times.Add("Pattern Timestep", "1:00");
            //times.Add("Hydraulic Timestep", "1:00");
            ////times.Add("Quality Timestep", "1:00");
            //times.Add("Pattern Start", "00:00");
            //times.Add("Report Start", "00:00");
            //times.Add("Start ClockTime", "12:00 AM");
            //times.Add("Statistic", "None");
            resetValue.Add("time", times);
        }
        #endregion 모델 해석을 위한 INP 데이터 생성

        /// <summary>
        /// 수리해석 모델 실행 , 에너지 분석
        /// </summary>
        /// <param name="oList"></param>
        /// <returns></returns>
        private bool ExecuteAnalysis()
        {
            /////수리 모델 해석실행
            AnalysisEngine engine = new AnalysisEngine();
            try
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", cboModel.SelectedValue); //INP_NUMBER
                conditions.Add("AUTO_MANUAL", "M");
                conditions.Add("analysisFlag", "H");
                conditions.Add("analysisType", 0);
                conditions.Add("resetValues", CreateResetValues());   //null : DB에 저장된 모델, HashTable
                conditions.Add("saveReport", false);
                conditions.Add("TARGET_DATE", WE_Common.WE_FunctionManager.GetNowDateTimeToString(DateTime.Now));

                Hashtable result = engine.Execute(conditions);
                if (result == null)
                {
                    WaterNetCore.MessageManager.ShowErrorMessage("에너지해석 수행중 오류가 발생했습니다.");
                    return false;
                }
                //////에너지 모델 해석결과를 Shape에 저장
                DivideAnalysisData_INP(result);
                UpdateAnalysisData_INP_DataTable();



                return true;
            }
            catch (Exception ex)
            {
                WaterNetCore.MessageManager.ShowErrorMessage("에너지해석 수행중 오류가 발생했습니다.");
                return false;
                throw ex;
            }
        }

        private Hashtable CreateResetValues()
        {
            Hashtable resetValue = new Hashtable();

            CreatePatternsSection(ref resetValue);
            CreateCurvesSection(ref resetValue);
            CreateTankSection(ref resetValue);
            CreateReservoirSection(ref resetValue);
            CreatePumpSection(ref resetValue);
            CreateRuleSection(ref resetValue);
            //CreateJunctionDemandSection(ref resetValue);
            //CreateJunctionPatternSection(ref resetValue);
            CreateOptionsSection(ref resetValue);
            CreateTimesSection(ref resetValue);

            return resetValue;
        }
        ///// <summary>
        ///// 수리모델 해석결과를 INP Shape에 저장한다.
        ///// </summary>
        //public void UpdateAnalysisData_INP_Shape()
        //{
        //    //ITable junctionTable = ArcManager.GetMapLayer(m_map, "JUNCTION") as ITable;
        //    //ITable tankTable = ArcManager.GetMapLayer(m_map, "TANK") as ITable;
        //    //ITable reservoirTable = ArcManager.GetMapLayer(m_map, "RESERVOIR") as ITable;
        //    //ITable pipeTable = ArcManager.GetMapLayer(m_map, "PIPE") as ITable;
        //    //ITable pumpTable = ArcManager.GetMapLayer(m_map, "PUMP") as ITable;
        //    //ITable valveTable = ArcManager.GetMapLayer(m_map, "VALVE") as ITable;

        //    //IWorkspaceEdit pWorkspaceEdit = ArcManager.getWorkspaceEdit(junctionTable as IFeatureLayer);
        //    //if (pWorkspaceEdit == null) return;

        //    //try
        //    //{
        //    //    pWorkspaceEdit.StartEditing(true);
        //    //    pWorkspaceEdit.StartEditOperation();

        //    //    UpdateNodeData(junctionTable as ITable);

        //    //    UpdateLinkData(pipeTable as ITable);

        //    //    //if (m_struct_Junction_Node.Count > 0)
        //    //    //    UpdateNodeData(InpJunctionLayer as ITable, m_struct_Junction_Node);    

        //    //    ////속도를 위해서 junction만 Update함.
        //    //    //if (m_struct_Tank_Node.Count > 0)
        //    //    //    UpdateNodeData(tankTable, m_struct_Tank_Node);

        //    //    //if (m_struct_Reservoir_Node.Count > 0)
        //    //    //    UpdateNodeData(reservoirTable, m_struct_Reservoir_Node);

        //    //    //if (m_struct_Pipe_Link.Count > 0)
        //    //    //    UpdateLinkData(pipeTable, m_struct_Pipe_Link);

        //    //    //if (m_struct_Pump_Link.Count > 0)
        //    //    //    UpdateLinkData(pumpTable, m_struct_Pump_Link);

        //    //    //if (m_struct_Valve_Link.Count > 0)
        //    //    //    UpdateLinkData(valveTable, m_struct_Valve_Link);

        //    //    pWorkspaceEdit.StopEditOperation();
        //    //    pWorkspaceEdit.StopEditing(true);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    pWorkspaceEdit.AbortEditOperation();
        //    //    pWorkspaceEdit.StopEditing(false);
        //    //    throw ex;
        //    //}
        //}

        ///// <summary>
        ///// 해석결과 NODE 데이터를 shape에 저장한다.
        ///// </summary>
        ///// <param name="oTable"></param>
        ///// <param name="oStructList"></param>
        //private void UpdateNodeData(ITable oTable)
        //{
        //    if (oTable == null) return;
        //    if (m_nodeTable.Rows.Count == 0) return;

        //    try
        //    {
        //        using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
        //        {
        //            ICursor pCusror = oTable.Update(null, true);
        //            comReleaser.ManageLifetime(pCusror);

        //            IRow oRow = null;
        //            DataRow row = null;

        //            while ((oRow = pCusror.NextRow()) != null)
        //            {
        //                //row = GetResultDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), m_nodeTable);
        //                if (row != null)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ELEVATI"), row["ELEVATION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_BASEDEM"), row["BASEDEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PATTERN"), row["PATTERN"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_EMITTER"), row["EMITTER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITQUA"), row["INITQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEQ"), row["SOURCEQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEP"), row["SOURCEPAT"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCET"), row["SOURCETYPE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKLEV"), row["TANKLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DEMAND"), row["DEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEAD"), row["HEAD"]);
        //                    if (Convert.ToDouble(row["PRESSURE"]) <= 0)
        //                        oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), 0);
        //                    else oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), row["PRESSURE"]);

        //                    oRow.set_Value(oRow.Fields.FindField("EN_QUALITY"), row["QUALITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEM"), row["SOURCEMASS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITVOL"), row["INITVOLUMN"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXMODE"), row["MIXMODEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXZONE"), row["MIXZONEVOL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKDIA"), row["TANKDIAM"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINVOLU"), row["MINVOLUMN"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VOLCURV"), row["VOLCURVE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINLEVE"), row["MINLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MAXLEVE"), row["MAXLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXFRAC"), row["MIXFRACTION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANK_KB"), row["TANK_KBULK"]);

        //                    pCusror.UpdateRow(oRow);
        //                }
        //            }
        //            pCusror.Flush();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }

        //}

        ///// <summary>
        ///// 해석결과 LINK 데이터를 shape에 저장한다.
        ///// </summary>
        ///// <param name="oTable"></param>
        ///// <param name="oStructList"></param>
        //private void UpdateLinkData(ITable oTable)
        //{
        //    if (oTable == null) return;
        //    if (m_linkTable.Rows.Count == 0) return;

        //    try
        //    {
        //        using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
        //        {
        //            ICursor pCusror = oTable.Update(null, true);
        //            comReleaser.ManageLifetime(pCusror);

        //            IRow oRow = null;
        //            DataRow row = null;

        //            while ((oRow = pCusror.NextRow()) != null)
        //            {
        //                //row = GetResultDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), m_linkTable);
        //                if (row != null)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DIAMETE"), row["DIAMETER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_LENGTH"), row["LENGTH"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ROUGHNE"), row["ROUGHNESS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINORLO"), row["MINORLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSTA"), row["INITSTATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSET"), row["INITSETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KBULK"), row["KBULK"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KWALL"), row["KWALL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_FLOW"), row["FLOW"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VELOCIT"), row["VELOCITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEADLOS"), row["HEADLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_STATUS"), row["STATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SETTING"), row["SETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ENERGY"), row["ENERGY"]);

        //                    pCusror.UpdateRow(oRow);
        //                }
        //            }
        //            pCusror.Flush();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }


        //}


        /// <summary>
        /// 해당 문자열이 숫자인지를 확인하는 정규식
        /// Char.IsNumber 메서드 (Char)로 변환 해주세요
        /// </summary>
        /// <param name="strData">문자열</param>
        /// <returns></returns>
        private bool IsNumeric(string strData)
        {
            System.Text.RegularExpressions.Regex isNumber = new System.Text.RegularExpressions.Regex(@"^[+-]?\d*(\.?\d*)$");
            
            System.Text.RegularExpressions.Match m = isNumber.Match(strData);

            return m.Success;
        }

        private void btnBIZRefresh_Click(object sender, EventArgs e)
        {
            QuerySelectWaterLevel();
        }

        private void btnCurveInfo_Click(object sender, EventArgs e)
        {
            ///모달 폼(OpenDialog)
            frmPumpCurve oform = new frmPumpCurve();
            oform.Owner = this;
            oform.StartPosition = FormStartPosition.CenterParent;
            oform.Open();
        }



    }
}
