﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChartFX.WinForms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
using WaterNet.WE_Common;

using WaterNet.WH_PipingNetworkAnalysis.epanet;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
#endregion UltraGrid를 사용=>namespace선언

namespace WaterNet.WE_EnergyAnalysis
{
    public partial class frmPumpSchedule : Form, WaterNet.WaterNetCore.IForminterface
    {
        #region Private Field ------------------------------------------------------------------------------
        private DataTable m_MasterTable;
        private GridManager m_gridManager1 = default(GridManager);  //모의분석 그리드
        private DataSet m_DataSet = new DataSet();

        /// <summary>
        /// 실시간 에너지 해석모델번호
        /// </summary>
        private string m_INP_NUMBER;

        /// <summary>
        /// 펌프별 해석항목ID
        /// </summary>
        private string m_strAN_OPR_MDLID_1;
        private string m_strAN_OPR_MDLID_2;
        private string m_strAN_OPR_MDLID_3;
        private string m_strAN_OPR_MDLID_4;
        private string m_strAN_OPR_MDLID_5;
        private string m_strAN_OPR_MDLID_6;

        /// <summary>
        /// 펌프별 패턴항목 ID
        /// </summary>
        private string m_strAN_PTN_MDLID_1;
        private string m_strAN_PTN_MDLID_2;
        private string m_strAN_PTN_MDLID_3;
        private string m_strAN_PTN_MDLID_4;
        private string m_strAN_PTN_MDLID_5;
        private string m_strAN_PTN_MDLID_6;

        /// <summary>
        /// 펌프 장치일련번호
        /// </summary>
        private string m_strFAC_SEQ_1;
        private string m_strFAC_SEQ_2;
        private string m_strFAC_SEQ_3;
        private string m_strFAC_SEQ_4;
        private string m_strFAC_SEQ_5;
        private string m_strFAC_SEQ_6;

        /// <summary>
        /// 펌프의 전단, 후단의 Junction 해석번호
        /// </summary>
        private string m_strAN_IN_MDLID_1;
        private string m_strAN_OUT_MDLID_1;
        private string m_strAN_IN_MDLID_2;
        private string m_strAN_OUT_MDLID_2;
        private string m_strAN_IN_MDLID_3;
        private string m_strAN_OUT_MDLID_3;
        private string m_strAN_IN_MDLID_4;
        private string m_strAN_OUT_MDLID_4;
        private string m_strAN_IN_MDLID_5;
        private string m_strAN_OUT_MDLID_5;
        private string m_strAN_IN_MDLID_6;
        private string m_strAN_OUT_MDLID_6;

        #endregion Private Field ------------------------------------------------------------------------------

        #region 생성자 및 환경설정 ----------------------------------------------------------------------
        /// <summary>
        /// 생성자
        /// </summary>
        public frmPumpSchedule()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "펌프스케쥴모의"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        #region 초기화설정
        private void InitializeSetting()
        {
            splitContainer1.Panel1Collapsed = true;     //챠트부분 Visible = fasle
            
            StringBuilder oStringBuilder = new StringBuilder();

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DD";
            oUltraGridColumn.Header.Caption = "가동일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HM";
            oUltraGridColumn.Header.Caption = "가동시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 가동 VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            ////////////////////펌프 해석결과 전력량(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Formula = "if(([ORG_W1] + [ORG_W2] + [ORG_W3] + [ORG_W4] + [ORG_W5] + [ORG_W6])=0, 0, [ORG_W1] + [ORG_W2] + [ORG_W3] + [ORG_W4] + [ORG_W5] + [ORG_W6])";

            ////////////////////펌프 해석결과 전력량(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([SIMU_W1] + [SIMU_W2] + [SIMU_W3] + [SIMU_W4] + [SIMU_W5] + [SIMU_W6]) = 0, 0, [SIMU_W1] + [SIMU_W2] + [SIMU_W3] + [SIMU_W4] + [SIMU_W5] + [SIMU_W6])";

            ////////////////////펌프 해석결과 유량(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([ORG_Q1] + [ORG_Q2] + [ORG_Q3] + [ORG_Q4] + [ORG_Q5] + [ORG_Q6]) = 0, 0, [ORG_Q1] + [ORG_Q2] + [ORG_Q3] + [ORG_Q4] + [ORG_Q5] + [ORG_Q6])";

            ////////////////////펌프 해석결과 유량(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([SIMU_Q1] + [SIMU_Q2] + [SIMU_Q3] + [SIMU_Q4] + [SIMU_Q5] + [SIMU_Q6]) = 0, 0, [SIMU_Q1] + [SIMU_Q2] + [SIMU_Q3] + [SIMU_Q4] + [SIMU_Q5] + [SIMU_Q6])";

            ////////////////////펌프 해석결과 양정(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([ORG_H1] + [ORG_H2] + [ORG_H3] + [ORG_H4] + [ORG_H5] + [ORG_H6]) = 0, 0, [ORG_H1] + [ORG_H2] + [ORG_H3] + [ORG_H4] + [ORG_H5] + [ORG_H6])";

            ////////////////////펌프 해석결과 양정(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([SIMU_H1] + [SIMU_H2] + [SIMU_H3] + [SIMU_H4] + [SIMU_H5] + [SIMU_H6]) = 0, 0, [SIMU_H1] + [SIMU_H2] + [SIMU_H3] + [SIMU_H4] + [SIMU_H5] + [SIMU_H6])";

            ////////////////////펌프 해석결과 효율(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";
            oUltraGridColumn.Formula = "if([ORG_W1] = 0, 0, ([ORG_Q1] * [ORG_H1]) / [ORG_W1])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";
            oUltraGridColumn.Formula = "if([ORG_W2] = 0, 0, ([ORG_Q2] * [ORG_H2]) / [ORG_W2])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";
            oUltraGridColumn.Formula = "if([ORG_W3] = 0, 0, ([ORG_Q3] * [ORG_H3]) / [ORG_W3])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";
            oUltraGridColumn.Formula = "if([ORG_W4] = 0, 0, ([ORG_Q4] * [ORG_H4]) / [ORG_W4])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";
            oUltraGridColumn.Formula = "if([ORG_W5] = 0, 0, ([ORG_Q5] * [ORG_H5]) / [ORG_W5])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";
            oUltraGridColumn.Formula = "if([ORG_W6] = 0, 0, ([ORG_Q6] * [ORG_H6]) / [ORG_W6])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([ORG_E1] + [ORG_E2] + [ORG_E3] + [ORG_E4] + [ORG_E5] + [ORG_E6]) = 0, 0, [ORG_E1] + [ORG_E2] + [ORG_E3] + [ORG_E4] + [ORG_E5] + [ORG_E6])";

            ////////////////////펌프 해석결과 효율(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";
            oUltraGridColumn.Formula = "if([SIMU_W1] = 0, 0, ([SIMU_Q1] * [SIMU_H1]) / [SIMU_W1])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if([SIMU_W2] = 0, 0, ([SIMU_Q2] * [SIMU_H2]) / [SIMU_W2])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";
            oUltraGridColumn.Formula = "if([SIMU_W3] = 0, 0, ([SIMU_Q3] * [SIMU_H3]) / [SIMU_W3])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";
            oUltraGridColumn.Formula = "if([SIMU_W4] = 0, 0, ([SIMU_Q4] * [SIMU_H4]) / [SIMU_W4])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";
            oUltraGridColumn.Formula = "if([SIMU_W5] = 0, 0, ([SIMU_Q5] * [SIMU_H5]) / [SIMU_W5])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";
            oUltraGridColumn.Formula = "if([SIMU_W6] = 0, 0, ([SIMU_Q6] * [SIMU_H6]) / [SIMU_W6])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([SIMU_E1] + [SIMU_E2] + [SIMU_E3] + [SIMU_E4] + [SIMU_E5] + [SIMU_E6]) = 0, 0, [SIMU_E1] + [SIMU_E2] + [SIMU_E3] + [SIMU_E4] + [SIMU_E5] + [SIMU_E6])";

            ////////////////////펌프 해석결과 원단위(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";
            oUltraGridColumn.Formula = "if([ORG_Q1] = 0, 0, [ORG_W1] / [ORG_Q1])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";
            oUltraGridColumn.Formula = "if([ORG_Q2] = 0, 0, [ORG_W2] / [ORG_Q2])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";
            oUltraGridColumn.Formula = "if([ORG_Q3] = 0, 0, [ORG_W3] / [ORG_Q3])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";
            oUltraGridColumn.Formula = "if([ORG_Q4] = 0, 0, [ORG_W4] / [ORG_Q4])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";
            oUltraGridColumn.Formula = "if([ORG_Q5] = 0, 0, [ORG_W5] / [ORG_Q5])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";
            oUltraGridColumn.Formula = "if([ORG_Q6] = 0, 0, [ORG_W6] / [ORG_Q6])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([ORG_U1] + [ORG_U2] + [ORG_U3] + [ORG_U4] + [ORG_U5] + [ORG_U6]) = 0, 0, [ORG_U1] + [ORG_U2] + [ORG_U3] + [ORG_U4] + [ORG_U5] + [ORG_U6])";

            ////////////////////펌프 해석결과 원단위(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#1";
            oUltraGridColumn.Formula = "if([SIMU_Q1] = 0, 0, [SIMU_W1] / [SIMU_Q1])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#2";
            oUltraGridColumn.Formula = "if([SIMU_Q2] = 0, 0, [SIMU_W2] / [SIMU_Q2])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#3";
            oUltraGridColumn.Formula = "if([SIMU_Q3] = 0, 0, [SIMU_W3] / [SIMU_Q3])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#4";
            oUltraGridColumn.Formula = "if([SIMU_Q4] = 0, 0, [SIMU_W4] / [SIMU_Q4])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#5";
            oUltraGridColumn.Formula = "if([SIMU_Q5] = 0, 0, [SIMU_W5] / [SIMU_Q5])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Tag = "#6";
            oUltraGridColumn.Formula = "if([SIMU_Q6] = 0, 0, [SIMU_W6] / [SIMU_Q6])";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Formula = "if(([SIMU_U1] + [SIMU_U2] + [SIMU_U3] + [SIMU_U4] + [SIMU_U5] + [SIMU_U6]) = 0, 0, [SIMU_U1] + [SIMU_U2] + [SIMU_U3] + [SIMU_U4] + [SIMU_U5] + [SIMU_U6])";

            //그리드매니저 - 울트라그리드맵핑
            m_gridManager1 = new GridManager(ultraGrid_WE_PP_MODEL);

            ArrayList keyValues = new ArrayList();
            keyValues.Add("DD");
            keyValues.Add("HM");
            m_gridManager1.AddColumnGroup("모의날짜", keyValues);

            keyValues.Clear();
            keyValues.Add("#1");
            keyValues.Add("#2");
            keyValues.Add("#3");
            keyValues.Add("#4");
            keyValues.Add("#5");
            keyValues.Add("#6");
            m_gridManager1.AddColumnGroup("OPR_VALUE", "펌프가동", keyValues);

            keyValues.Clear();
            keyValues.Add("ORG_W1");
            keyValues.Add("ORG_W2");
            keyValues.Add("ORG_W3");
            keyValues.Add("ORG_W4");
            keyValues.Add("ORG_W5");
            keyValues.Add("ORG_W6");
            keyValues.Add("ORG_W_SUM");
            m_gridManager1.AddColumnGroup("ORG_W", "기존전력", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("ORG_W", true);

            keyValues.Clear();
            keyValues.Add("SIMU_W1");
            keyValues.Add("SIMU_W2");
            keyValues.Add("SIMU_W3");
            keyValues.Add("SIMU_W4");
            keyValues.Add("SIMU_W5");
            keyValues.Add("SIMU_W6");
            keyValues.Add("SIMU_W_SUM");
            m_gridManager1.AddColumnGroup("SIMU_W", "변경전력", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("SIMU_W", true);

            keyValues.Clear();
            keyValues.Add("ORG_Q1");
            keyValues.Add("ORG_Q2");
            keyValues.Add("ORG_Q3");
            keyValues.Add("ORG_Q4");
            keyValues.Add("ORG_Q5");
            keyValues.Add("ORG_Q6");
            keyValues.Add("ORG_Q_SUM");
            m_gridManager1.AddColumnGroup("ORG_Q", "기존유량", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("ORG_Q", true);

            keyValues.Clear();
            keyValues.Add("SIMU_Q1");
            keyValues.Add("SIMU_Q2");
            keyValues.Add("SIMU_Q3");
            keyValues.Add("SIMU_Q4");
            keyValues.Add("SIMU_Q5");
            keyValues.Add("SIMU_Q6");
            keyValues.Add("SIMU_Q_SUM");
            m_gridManager1.AddColumnGroup("SIMU_Q", "변경유량", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("SIMU_Q", true);

            keyValues.Clear();
            keyValues.Add("ORG_H1");
            keyValues.Add("ORG_H2");
            keyValues.Add("ORG_H3");
            keyValues.Add("ORG_H4");
            keyValues.Add("ORG_H5");
            keyValues.Add("ORG_H6");
            keyValues.Add("ORG_H_SUM");
            m_gridManager1.AddColumnGroup("ORG_H", "기존양정", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("ORG_H", true);

            keyValues.Clear();
            keyValues.Add("SIMU_H1");
            keyValues.Add("SIMU_H2");
            keyValues.Add("SIMU_H3");
            keyValues.Add("SIMU_H4");
            keyValues.Add("SIMU_H5");
            keyValues.Add("SIMU_H6");
            keyValues.Add("SIMU_H_SUM");
            m_gridManager1.AddColumnGroup("SIMU_H", "변경양정", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("SIMU_H", true);

            keyValues.Clear();
            keyValues.Add("ORG_E1");
            keyValues.Add("ORG_E2");
            keyValues.Add("ORG_E3");
            keyValues.Add("ORG_E4");
            keyValues.Add("ORG_E5");
            keyValues.Add("ORG_E6");
            keyValues.Add("ORG_E_SUM");
            m_gridManager1.AddColumnGroup("ORG_E", "기존효율", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("ORG_E", true);

            keyValues.Clear();
            keyValues.Add("SIMU_E1");
            keyValues.Add("SIMU_E2");
            keyValues.Add("SIMU_E3");
            keyValues.Add("SIMU_E4");
            keyValues.Add("SIMU_E5");
            keyValues.Add("SIMU_E6");
            keyValues.Add("SIMU_E_SUM");
            m_gridManager1.AddColumnGroup("SIMU_E", "변경효율", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("SIMU_E", true);

            keyValues.Clear();
            keyValues.Add("ORG_U1");
            keyValues.Add("ORG_U2");
            keyValues.Add("ORG_U3");
            keyValues.Add("ORG_U4");
            keyValues.Add("ORG_U5");
            keyValues.Add("ORG_U6");
            keyValues.Add("ORG_U_SUM");
            m_gridManager1.AddColumnGroup("ORG_U", "기존원단위", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("ORG_U", true);

            keyValues.Clear();
            keyValues.Add("SIMU_U1");
            keyValues.Add("SIMU_U2");
            keyValues.Add("SIMU_U3");
            keyValues.Add("SIMU_U4");
            keyValues.Add("SIMU_U5");
            keyValues.Add("SIMU_U6");
            keyValues.Add("SIMU_U_SUM");
            m_gridManager1.AddColumnGroup("SIMU_U", "변경원단위", keyValues, "UnLock");
            m_gridManager1.VisibleGridGroup("SIMU_U", true);

            m_gridManager1.SetGridStyle_Select();
            m_gridManager1.ColumeAllowEdit(2, 8);  //펌프 가동 : AllowEdit
            m_gridManager1.SetColumnWidth(70);

            //-----------------------------------------------------------------------------------------------------------------

            #region ComboBox 설정

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT BIZ_PLA_SEQ AS CD, BIZ_PLA_NM AS CDNM FROM WE_BIZ_PLA ORDER BY BIZ_PLA_NM ASC");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_BIZ_PLA_NM, oStringBuilder.ToString(), false);

            object[,] aoSource = {
                {"1호기",  "1호기"},
                {"2호기",  "2호기"},
                {"3호기",  "3호기"},
                {"4호기",  "4호기"},
                {"5호기",  "5호기"},
                {"6호기",  "6호기"}
            };
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_ORG, aoSource, false);
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_SIMU, aoSource, false);
            #endregion

            #region 멤버변수 설정
            //그리드의 설정된 필드에 따라 빈 테이블을 생성한다.
            //다음의 테이블을 병합하기 위한 마스터데이블
            m_MasterTable = m_gridManager1.GetGridToDataTable();
            DataColumn oDD = getDataColumn(m_MasterTable, "DD");
            DataColumn oHM = getDataColumn(m_MasterTable, "HM");
            m_MasterTable.PrimaryKey = new DataColumn[] { oDD, oHM };

            //해석테이블 생성
            CreateDataTable();  
            #endregion

            #region  //차트 초기화
            InitializeChart();
            #endregion

            ultraDateTimeEditor_Day.Value = DateTime.Today.AddDays(-1);

        }
        #endregion

        //private void InitializeGrid(UltraGrid oUltraGrid)
        //{
        //    oUltraGrid.DisplayLayout.Bands[0].Columns.ClearUnbound();

        //    #region 그리드 설정
        //    UltraGridColumn oUltraGridColumn;

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "DD";
        //    oUltraGridColumn.Header.Caption = "가동일자";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "HM";
        //    oUltraGridColumn.Header.Caption = "가동시간";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;

        //    ////////////////////펌프 가동 VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "#1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.AllowEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Tag = "#1";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "#2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.AllowEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Tag = "#2";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "#3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.AllowEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Tag = "#3";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "#4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.AllowEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Tag = "#4";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "#5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.AllowEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Tag = "#5";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "#6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.AllowEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Tag = "#6";

        //    ////////////////////펌프 해석결과 전력량(기존) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_W1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_W2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_W3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_W4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_W5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_W6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_W_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Formula = "if(([ORG_W1] + [ORG_W2] + [ORG_W3] + [ORG_W4] + [ORG_W5] + [ORG_W6])=0, 0, [ORG_W1] + [ORG_W2] + [ORG_W3] + [ORG_W4] + [ORG_W5] + [ORG_W6])";

        //    ////////////////////펌프 해석결과 전력량(변경) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_W1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_W2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_W3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_W4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_W5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_W6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_W_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([SIMU_W1] + [SIMU_W2] + [SIMU_W3] + [SIMU_W4] + [SIMU_W5] + [SIMU_W6]) = 0, 0, [SIMU_W1] + [SIMU_W2] + [SIMU_W3] + [SIMU_W4] + [SIMU_W5] + [SIMU_W6])";

        //    ////////////////////펌프 해석결과 유량(기존) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_Q1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_Q2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_Q3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_Q4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_Q5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_Q6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_Q_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([ORG_Q1] + [ORG_Q2] + [ORG_Q3] + [ORG_Q4] + [ORG_Q5] + [ORG_Q6]) = 0, 0, [ORG_Q1] + [ORG_Q2] + [ORG_Q3] + [ORG_Q4] + [ORG_Q5] + [ORG_Q6])";

        //    ////////////////////펌프 해석결과 유량(변경) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_Q1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_Q2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_Q3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_Q4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_Q5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_Q6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_Q_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([SIMU_Q1] + [SIMU_Q2] + [SIMU_Q3] + [SIMU_Q4] + [SIMU_Q5] + [SIMU_Q6]) = 0, 0, [SIMU_Q1] + [SIMU_Q2] + [SIMU_Q3] + [SIMU_Q4] + [SIMU_Q5] + [SIMU_Q6])";

        //    ////////////////////펌프 해석결과 양정(기존) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_H1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_H2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_H3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_H4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_H5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_H6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_H_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([ORG_H1] + [ORG_H2] + [ORG_H3] + [ORG_H4] + [ORG_H5] + [ORG_H6]) = 0, 0, [ORG_H1] + [ORG_H2] + [ORG_H3] + [ORG_H4] + [ORG_H5] + [ORG_H6])";

        //    ////////////////////펌프 해석결과 양정(변경) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_H1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_H2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_H3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_H4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_H5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_H6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_H_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([SIMU_H1] + [SIMU_H2] + [SIMU_H3] + [SIMU_H4] + [SIMU_H5] + [SIMU_H6]) = 0, 0, [SIMU_H1] + [SIMU_H2] + [SIMU_H3] + [SIMU_H4] + [SIMU_H5] + [SIMU_H6])";

        //    ////////////////////펌프 해석결과 효율(기존) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_E1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";
        //    oUltraGridColumn.Formula = "if([ORG_W1] = 0, 0, ([ORG_Q1] * [ORG_H1]) / [ORG_W1])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_E2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";
        //    oUltraGridColumn.Formula = "if([ORG_W2] = 0, 0, ([ORG_Q2] * [ORG_H2]) / [ORG_W2])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_E3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";
        //    oUltraGridColumn.Formula = "if([ORG_W3] = 0, 0, ([ORG_Q3] * [ORG_H3]) / [ORG_W3])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_E4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";
        //    oUltraGridColumn.Formula = "if([ORG_W4] = 0, 0, ([ORG_Q4] * [ORG_H4]) / [ORG_W4])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_E5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";
        //    oUltraGridColumn.Formula = "if([ORG_W5] = 0, 0, ([ORG_Q5] * [ORG_H5]) / [ORG_W5])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_E6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";
        //    oUltraGridColumn.Formula = "if([ORG_W6] = 0, 0, ([ORG_Q6] * [ORG_H6]) / [ORG_W6])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_E_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([ORG_E1] + [ORG_E2] + [ORG_E3] + [ORG_E4] + [ORG_E5] + [ORG_E6]) = 0, 0, [ORG_E1] + [ORG_E2] + [ORG_E3] + [ORG_E4] + [ORG_E5] + [ORG_E6])";

        //    ////////////////////펌프 해석결과 효율(변경) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_E1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";
        //    oUltraGridColumn.Formula = "if([SIMU_W1] = 0, 0, ([SIMU_Q1] * [SIMU_H1]) / [SIMU_W1])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_E2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Tag = "#2";
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if([SIMU_W2] = 0, 0, ([SIMU_Q2] * [SIMU_H2]) / [SIMU_W2])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_E3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";
        //    oUltraGridColumn.Formula = "if([SIMU_W3] = 0, 0, ([SIMU_Q3] * [SIMU_H3]) / [SIMU_W3])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_E4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";
        //    oUltraGridColumn.Formula = "if([SIMU_W4] = 0, 0, ([SIMU_Q4] * [SIMU_H4]) / [SIMU_W4])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_E5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";
        //    oUltraGridColumn.Formula = "if([SIMU_W5] = 0, 0, ([SIMU_Q5] * [SIMU_H5]) / [SIMU_W5])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_E6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";
        //    oUltraGridColumn.Formula = "if([SIMU_W6] = 0, 0, ([SIMU_Q6] * [SIMU_H6]) / [SIMU_W6])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_E_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([SIMU_E1] + [SIMU_E2] + [SIMU_E3] + [SIMU_E4] + [SIMU_E5] + [SIMU_E6]) = 0, 0, [SIMU_E1] + [SIMU_E2] + [SIMU_E3] + [SIMU_E4] + [SIMU_E5] + [SIMU_E6])";

        //    ////////////////////펌프 해석결과 원단위(기존) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_U1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";
        //    oUltraGridColumn.Formula = "if([ORG_Q1] = 0, 0, [ORG_W1] / [ORG_Q1])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_U2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";
        //    oUltraGridColumn.Formula = "if([ORG_Q2] = 0, 0, [ORG_W2] / [ORG_Q2])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_U3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";
        //    oUltraGridColumn.Formula = "if([ORG_Q3] = 0, 0, [ORG_W3] / [ORG_Q3])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_U4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";
        //    oUltraGridColumn.Formula = "if([ORG_Q4] = 0, 0, [ORG_W4] / [ORG_Q4])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_U5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";
        //    oUltraGridColumn.Formula = "if([ORG_Q5] = 0, 0, [ORG_W5] / [ORG_Q5])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_U6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";
        //    oUltraGridColumn.Formula = "if([ORG_Q6] = 0, 0, [ORG_W6] / [ORG_Q6])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "ORG_U_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([ORG_U1] + [ORG_U2] + [ORG_U3] + [ORG_U4] + [ORG_U5] + [ORG_U6]) = 0, 0, [ORG_U1] + [ORG_U2] + [ORG_U3] + [ORG_U4] + [ORG_U5] + [ORG_U6])";

        //    ////////////////////펌프 해석결과 원단위(변경) VALUE
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_U1";
        //    oUltraGridColumn.Header.Caption = "#1";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#1";
        //    oUltraGridColumn.Formula = "if([SIMU_Q1] = 0, 0, [SIMU_W1] / [SIMU_Q1])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_U2";
        //    oUltraGridColumn.Header.Caption = "#2";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#2";
        //    oUltraGridColumn.Formula = "if([SIMU_Q2] = 0, 0, [SIMU_W2] / [SIMU_Q2])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_U3";
        //    oUltraGridColumn.Header.Caption = "#3";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#3";
        //    oUltraGridColumn.Formula = "if([SIMU_Q3] = 0, 0, [SIMU_W3] / [SIMU_Q3])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_U4";
        //    oUltraGridColumn.Header.Caption = "#4";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#4";
        //    oUltraGridColumn.Formula = "if([SIMU_Q4] = 0, 0, [SIMU_W4] / [SIMU_Q4])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_U5";
        //    oUltraGridColumn.Header.Caption = "#5";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#5";
        //    oUltraGridColumn.Formula = "if([SIMU_Q5] = 0, 0, [SIMU_W5] / [SIMU_Q5])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_U6";
        //    oUltraGridColumn.Header.Caption = "#6";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Tag = "#6";
        //    oUltraGridColumn.Formula = "if([SIMU_Q6] = 0, 0, [SIMU_W6] / [SIMU_Q6])";

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "SIMU_U_SUM";
        //    oUltraGridColumn.Header.Caption = "합계";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;
        //    oUltraGridColumn.Format = "###,##0.00";
        //    oUltraGridColumn.Formula = "if(([SIMU_U1] + [SIMU_U2] + [SIMU_U3] + [SIMU_U4] + [SIMU_U5] + [SIMU_U6]) = 0, 0, [SIMU_U1] + [SIMU_U2] + [SIMU_U3] + [SIMU_U4] + [SIMU_U5] + [SIMU_U6])";

        //    //그리드매니저 - 울트라그리드맵핑
        //    m_gridManager1 = new GridManager(ultraGrid_WE_PP_MODEL);

        //    ArrayList keyValues = new ArrayList();
        //    keyValues.Add("DD");
        //    keyValues.Add("HM");
        //    m_gridManager1.AddColumnGroup("모의날짜", keyValues);

        //    keyValues.Clear();
        //    keyValues.Add("#1");
        //    keyValues.Add("#2");
        //    keyValues.Add("#3");
        //    keyValues.Add("#4");
        //    keyValues.Add("#5");
        //    keyValues.Add("#6");
        //    m_gridManager1.AddColumnGroup("OPR_VALUE", "펌프가동", keyValues);

        //    keyValues.Clear();
        //    keyValues.Add("ORG_W1");
        //    keyValues.Add("ORG_W2");
        //    keyValues.Add("ORG_W3");
        //    keyValues.Add("ORG_W4");
        //    keyValues.Add("ORG_W5");
        //    keyValues.Add("ORG_W6");
        //    keyValues.Add("ORG_W_SUM");
        //    m_gridManager1.AddColumnGroup("ORG_W", "기존전력", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("ORG_W", true);

        //    keyValues.Clear();
        //    keyValues.Add("SIMU_W1");
        //    keyValues.Add("SIMU_W2");
        //    keyValues.Add("SIMU_W3");
        //    keyValues.Add("SIMU_W4");
        //    keyValues.Add("SIMU_W5");
        //    keyValues.Add("SIMU_W6");
        //    keyValues.Add("SIMU_W_SUM");
        //    m_gridManager1.AddColumnGroup("SIMU_W", "변경전력", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("SIMU_W", true);

        //    keyValues.Clear();
        //    keyValues.Add("ORG_Q1");
        //    keyValues.Add("ORG_Q2");
        //    keyValues.Add("ORG_Q3");
        //    keyValues.Add("ORG_Q4");
        //    keyValues.Add("ORG_Q5");
        //    keyValues.Add("ORG_Q6");
        //    keyValues.Add("ORG_Q_SUM");
        //    m_gridManager1.AddColumnGroup("ORG_Q", "기존유량", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("ORG_Q", true);

        //    keyValues.Clear();
        //    keyValues.Add("SIMU_Q1");
        //    keyValues.Add("SIMU_Q2");
        //    keyValues.Add("SIMU_Q3");
        //    keyValues.Add("SIMU_Q4");
        //    keyValues.Add("SIMU_Q5");
        //    keyValues.Add("SIMU_Q6");
        //    keyValues.Add("SIMU_Q_SUM");
        //    m_gridManager1.AddColumnGroup("SIMU_Q", "변경유량", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("SIMU_Q", true);

        //    keyValues.Clear();
        //    keyValues.Add("ORG_H1");
        //    keyValues.Add("ORG_H2");
        //    keyValues.Add("ORG_H3");
        //    keyValues.Add("ORG_H4");
        //    keyValues.Add("ORG_H5");
        //    keyValues.Add("ORG_H6");
        //    keyValues.Add("ORG_H_SUM");
        //    m_gridManager1.AddColumnGroup("ORG_H", "기존양정", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("ORG_H", true);

        //    keyValues.Clear();
        //    keyValues.Add("SIMU_H1");
        //    keyValues.Add("SIMU_H2");
        //    keyValues.Add("SIMU_H3");
        //    keyValues.Add("SIMU_H4");
        //    keyValues.Add("SIMU_H5");
        //    keyValues.Add("SIMU_H6");
        //    keyValues.Add("SIMU_H_SUM");
        //    m_gridManager1.AddColumnGroup("SIMU_H", "변경양정", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("SIMU_H", true);

        //    keyValues.Clear();
        //    keyValues.Add("ORG_E1");
        //    keyValues.Add("ORG_E2");
        //    keyValues.Add("ORG_E3");
        //    keyValues.Add("ORG_E4");
        //    keyValues.Add("ORG_E5");
        //    keyValues.Add("ORG_E6");
        //    keyValues.Add("ORG_E_SUM");
        //    m_gridManager1.AddColumnGroup("ORG_E", "기존효율", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("ORG_E", true);

        //    keyValues.Clear();
        //    keyValues.Add("SIMU_E1");
        //    keyValues.Add("SIMU_E2");
        //    keyValues.Add("SIMU_E3");
        //    keyValues.Add("SIMU_E4");
        //    keyValues.Add("SIMU_E5");
        //    keyValues.Add("SIMU_E6");
        //    keyValues.Add("SIMU_E_SUM");
        //    m_gridManager1.AddColumnGroup("SIMU_E", "변경효율", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("SIMU_E", true);

        //    keyValues.Clear();
        //    keyValues.Add("ORG_U1");
        //    keyValues.Add("ORG_U2");
        //    keyValues.Add("ORG_U3");
        //    keyValues.Add("ORG_U4");
        //    keyValues.Add("ORG_U5");
        //    keyValues.Add("ORG_U6");
        //    keyValues.Add("ORG_U_SUM");
        //    m_gridManager1.AddColumnGroup("ORG_U", "기존원단위", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("ORG_U", true);

        //    keyValues.Clear();
        //    keyValues.Add("SIMU_U1");
        //    keyValues.Add("SIMU_U2");
        //    keyValues.Add("SIMU_U3");
        //    keyValues.Add("SIMU_U4");
        //    keyValues.Add("SIMU_U5");
        //    keyValues.Add("SIMU_U6");
        //    keyValues.Add("SIMU_U_SUM");
        //    m_gridManager1.AddColumnGroup("SIMU_U", "변경원단위", keyValues, "UnLock");
        //    m_gridManager1.VisibleGridGroup("SIMU_U", true);

        //    m_gridManager1.SetGridStyle_Select();
        //    m_gridManager1.ColumeAllowEdit(2, 8);  //펌프 가동 : AllowEdit
        //    m_gridManager1.SetColumnWidth(70);

        //    //-----------------------------------------------------------------------------------------------------------------

        //    #endregion
        //}

        private void InitializeChart()
        {
            this.chart_ORG.LegendBox.MarginY = 1;
            this.chart_ORG.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chart_ORG.AxisX.DataFormat.CustomFormat = "hh:MM";
            this.chart_ORG.AxisX.Staggered = true;

            this.chart_SIMU.LegendBox.MarginY = 1;
            this.chart_SIMU.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chart_SIMU.AxisX.DataFormat.CustomFormat = "hh:MM";
            this.chart_SIMU.AxisX.Staggered = true;


        }

        private void frmPumpSchedule_Load(object sender, EventArgs e)
        {
            //그리드의 Insert, Update구분 Event
            ultraGrid_WE_PP_MODEL.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            ultraGrid_WE_PP_MODEL.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);

            checkBox_W.CheckedChanged += new EventHandler(VisibleColumnGroup);
            checkBox_E.CheckedChanged += new EventHandler(VisibleColumnGroup);
            checkBox_H.CheckedChanged += new EventHandler(VisibleColumnGroup);
            checkBox_Q.CheckedChanged += new EventHandler(VisibleColumnGroup);
            checkBox_U.CheckedChanged += new EventHandler(VisibleColumnGroup);

            comboBox_ORG.SelectedIndexChanged += new EventHandler(ChartDrawVisible);
            comboBox_SIMU.SelectedIndexChanged += new EventHandler(ChartDrawVisible);
            this.UpdateMenuEnabledStates();
        }

        public void Open()
        {

        }
        #endregion

        #endregion 생성자 및 환경설정 ----------------------------------------------------------------------

        #region Private Function
        /// <summary>
        /// 에너지 해석결과를 담기위한 데이터테이블 생성
        /// </summary>
        /// <param name="tableName"></param>
        private void CreateDataTable()
        {
            DataTable oDataTable = new DataTable("WH_ANALISYS");

            //날짜 = pk
            DataColumn oColumn1 = new DataColumn("DD");
            oDataTable.Columns.Add(oColumn1);
            DataColumn oColumn2 = new DataColumn("HM");
            oDataTable.Columns.Add(oColumn2);
            oDataTable.PrimaryKey = new DataColumn[] { oColumn1, oColumn2 };

            //전력량
            oDataTable.Columns.Add("SIMU_W1");
            oDataTable.Columns.Add("SIMU_W2");
            oDataTable.Columns.Add("SIMU_W3");
            oDataTable.Columns.Add("SIMU_W4");
            oDataTable.Columns.Add("SIMU_W5");
            oDataTable.Columns.Add("SIMU_W6");
            oDataTable.Columns.Add("SIMU_W_SUM");
            //양정(압력)
            oDataTable.Columns.Add("SIMU_H1");
            oDataTable.Columns.Add("SIMU_H2");
            oDataTable.Columns.Add("SIMU_H3");
            oDataTable.Columns.Add("SIMU_H4");
            oDataTable.Columns.Add("SIMU_H5");
            oDataTable.Columns.Add("SIMU_H6");
            oDataTable.Columns.Add("SIMU_H_SUM");

            //유량
            oDataTable.Columns.Add("SIMU_Q1");
            oDataTable.Columns.Add("SIMU_Q2");
            oDataTable.Columns.Add("SIMU_Q3");
            oDataTable.Columns.Add("SIMU_Q4");
            oDataTable.Columns.Add("SIMU_Q5");
            oDataTable.Columns.Add("SIMU_Q6");
            oDataTable.Columns.Add("SIMU_Q_SUM");

            if (m_DataSet.Tables["WH_ANALISYS"] != null)
            {
                m_DataSet.Tables.Remove("WH_ANALISYS");
            }
            m_DataSet.Tables.Add(oDataTable);

        }

        ///// <summary>
        ///// 에너지 해석을 재실행하기 위해 데이터내용만 지운다
        ///// </summary>
        ///// <param name="tableName"></param>
        //private void ClearDataTable(string tableName)
        //{
        //    DataTable oDataTable = m_DataSet.Tables[tableName];
        //    if (oDataTable != null)
        //    {
        //        oDataTable.Rows.Clear();
        //    }
        //}

        /// <summary>
        /// 멤버변수 설정
        /// </summary>
        private void setMemberVariable()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            OracleDataReader oReader = null;
            try
            {
                oDBManager.Open();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT * ");
                oStringBuilder.AppendLine("FROM WE_BIZ_PLA ");
                oStringBuilder.AppendLine("WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");

                oReader = oDBManager.ExecuteScriptDataReader(oStringBuilder.ToString(), null);
                if (oReader.HasRows)
                {
                    m_INP_NUMBER = Convert.ToString(oReader["RL_AN_MDL_NO"]);

                    m_strAN_OPR_MDLID_1 = Convert.ToString(oReader["AN_OPR_MDLID_1"]);
                    m_strAN_OPR_MDLID_2 = Convert.ToString(oReader["AN_OPR_MDLID_2"]);
                    m_strAN_OPR_MDLID_3 = Convert.ToString(oReader["AN_OPR_MDLID_3"]);
                    m_strAN_OPR_MDLID_4 = Convert.ToString(oReader["AN_OPR_MDLID_4"]);
                    m_strAN_OPR_MDLID_5 = Convert.ToString(oReader["AN_OPR_MDLID_5"]);
                    m_strAN_OPR_MDLID_6 = Convert.ToString(oReader["AN_OPR_MDLID_6"]);

                    m_strAN_PTN_MDLID_1 = Convert.ToString(oReader["AN_PTN_MDLID_1"]);
                    m_strAN_PTN_MDLID_2 = Convert.ToString(oReader["AN_PTN_MDLID_2"]);
                    m_strAN_PTN_MDLID_3 = Convert.ToString(oReader["AN_PTN_MDLID_3"]);
                    m_strAN_PTN_MDLID_4 = Convert.ToString(oReader["AN_PTN_MDLID_4"]);
                    m_strAN_PTN_MDLID_5 = Convert.ToString(oReader["AN_PTN_MDLID_5"]);
                    m_strAN_PTN_MDLID_6 = Convert.ToString(oReader["AN_PTN_MDLID_6"]);
                }
                oReader.Close();

                oStringBuilder.Remove(0, oStringBuilder.Length);
                //oStringBuilder.AppendLine("SELECT DECODE(PP_HOGI, '#1', FAC_SEQ) AS P1");
                //oStringBuilder.AppendLine("      ,DECODE(PP_HOGI, '#2', FAC_SEQ) AS P2");
                //oStringBuilder.AppendLine("      ,DECODE(PP_HOGI, '#3', FAC_SEQ) AS P3");
                //oStringBuilder.AppendLine("      ,DECODE(PP_HOGI, '#4', FAC_SEQ) AS P4");
                //oStringBuilder.AppendLine("      ,DECODE(PP_HOGI, '#5', FAC_SEQ) AS P5");
                //oStringBuilder.AppendLine("      ,DECODE(PP_HOGI, '#6', FAC_SEQ) AS P6");
                //oStringBuilder.AppendLine("FROM WATERNET.WE_PP_INFO                   ");
                //oStringBuilder.AppendLine("WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                //oStringBuilder.AppendLine("ORDER BY PP_HOGI ASC");


                oStringBuilder.AppendLine("SELECT FAC_SEQ, PP_HOGI, AN_IN_MDLID, AN_OUT_MDLID ");
                oStringBuilder.AppendLine("FROM WATERNET.WE_PP_INFO                           ");
                oStringBuilder.AppendLine("WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("ORDER BY PP_HOGI ASC                              ");

                oReader = oDBManager.ExecuteScriptDataReader(oStringBuilder.ToString(), null);
                if (oReader.HasRows)
                {
                    while (oReader.Read())
                    {
                        if ("#1".Equals(Convert.ToString(oReader["PP_HOGI"])))
                        {
                            m_strFAC_SEQ_1 = Convert.ToString(oReader["FAC_SEQ"]);
                            m_strAN_IN_MDLID_1 = Convert.ToString(oReader["AN_IN_MDLID"]);
                            m_strAN_OUT_MDLID_1 = Convert.ToString(oReader["AN_OUT_MDLID"]);
                            continue;
                        }
                        if ("#2".Equals(Convert.ToString(oReader["PP_HOGI"])))
                        {
                            m_strFAC_SEQ_2 = Convert.ToString(oReader["FAC_SEQ"]);
                            m_strAN_IN_MDLID_2 = Convert.ToString(oReader["AN_IN_MDLID"]);
                            m_strAN_OUT_MDLID_2 = Convert.ToString(oReader["AN_OUT_MDLID"]);
                            continue;
                        }
                        if ("#3".Equals(Convert.ToString(oReader["PP_HOGI"])))
                        {
                            m_strFAC_SEQ_3 = Convert.ToString(oReader["FAC_SEQ"]);
                            m_strAN_IN_MDLID_3 = Convert.ToString(oReader["AN_IN_MDLID"]);
                            m_strAN_OUT_MDLID_3 = Convert.ToString(oReader["AN_OUT_MDLID"]);
                            continue;
                        }
                        if ("#4".Equals(Convert.ToString(oReader["PP_HOGI"])))
                        {
                            m_strFAC_SEQ_4 = Convert.ToString(oReader["FAC_SEQ"]);
                            m_strAN_IN_MDLID_4 = Convert.ToString(oReader["AN_IN_MDLID"]);
                            m_strAN_OUT_MDLID_4 = Convert.ToString(oReader["AN_OUT_MDLID"]);
                            continue;
                        }
                        if ("#5".Equals(Convert.ToString(oReader["PP_HOGI"])))
                        {
                            m_strFAC_SEQ_5 = Convert.ToString(oReader["FAC_SEQ"]);
                            m_strAN_IN_MDLID_5 = Convert.ToString(oReader["AN_IN_MDLID"]);
                            m_strAN_OUT_MDLID_5 = Convert.ToString(oReader["AN_OUT_MDLID"]);
                            continue;
                        }
                        if ("#6".Equals(Convert.ToString(oReader["PP_HOGI"])))
                        {
                            m_strFAC_SEQ_6 = Convert.ToString(oReader["FAC_SEQ"]);
                            m_strAN_IN_MDLID_6 = Convert.ToString(oReader["AN_IN_MDLID"]);
                            m_strAN_OUT_MDLID_6 = Convert.ToString(oReader["AN_OUT_MDLID"]);
                            continue;
                        }
                    }
                }

            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oReader != null) oReader.Close();
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 실시간 해석결과 Node 데이터
        /// WH_RPT_NODES
        /// </summary>
        private void getRpt_Nodes()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                string oDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_Day.Value);

                oStringBuilder.AppendLine("SELECT                                                                                                   ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD hh24:MI:SS'), 'YYYY-MM-DD') AS DD                        ");
                oStringBuilder.AppendLine("      , TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS'), 'HH24:MI') AS HM                          ");
                oStringBuilder.AppendLine("      , NVL(P1.HEAD,0) AS ORG_H1                                                                         ");
                oStringBuilder.AppendLine("      , NVL(P2.HEAD,0) AS ORG_H2                                                                         ");
                oStringBuilder.AppendLine("      , NVL(P3.HEAD,0) AS ORG_H3                                                                         ");
                oStringBuilder.AppendLine("      , NVL(P4.HEAD,0) AS ORG_H4                                                                         ");
                oStringBuilder.AppendLine("      , NVL(P5.HEAD,0) AS ORG_H5                                                                         ");
                oStringBuilder.AppendLine("      , NVL(P6.HEAD,0) AS ORG_H6                                                                         ");
                oStringBuilder.AppendLine("      , NULL           AS ORG_H_SUM                                                                      ");
                oStringBuilder.AppendLine("FROM                                                                                                     ");
                oStringBuilder.AppendLine(" (SELECT DISTINCT TO_CHAR(TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP   ");
                oStringBuilder.AppendLine("  FROM IF_GATHER_REALTIME_E                                                                              ");
                oStringBuilder.AppendLine("  WHERE TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "') A");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.BHEAD, C.CHEAD, C.HEAD                                                                      ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (    ------전양정                                                              ");
                oStringBuilder.AppendLine("       SELECT C.HEAD - B.HEAD AS HEAD, B.HEAD AS BHEAD, C.HEAD AS CHEAD, B.RPT_NUMBER, B.INP_NUMBER      ");
                oStringBuilder.AppendLine("       FROM WE_PP_INFO A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES B ON A.AN_IN_MDLID = B.NODE_ID AND A.PP_HOGI = '#1'                      ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES C ON A.AN_OUT_MDLID = C.NODE_ID AND B.RPT_NUMBER = C.RPT_NUMBER          ");
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')           ");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P1 ON A.TIMESTAMP = P1.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.BHEAD, C.CHEAD, C.HEAD                                                                      ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (    ------전양정                                                              ");
                oStringBuilder.AppendLine("       SELECT C.HEAD - B.HEAD AS HEAD, B.HEAD AS BHEAD, C.HEAD AS CHEAD, B.RPT_NUMBER, B.INP_NUMBER      ");
                oStringBuilder.AppendLine("       FROM WE_PP_INFO A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES B ON A.AN_IN_MDLID = B.NODE_ID AND A.PP_HOGI = '#2'                      ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES C ON A.AN_OUT_MDLID = C.NODE_ID AND B.RPT_NUMBER = C.RPT_NUMBER          ");
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')           ");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P2 ON A.TIMESTAMP = P2.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.BHEAD, C.CHEAD, C.HEAD                                                                      ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (    ------전양정                                                              ");
                oStringBuilder.AppendLine("       SELECT C.HEAD - B.HEAD AS HEAD, B.HEAD AS BHEAD, C.HEAD AS CHEAD, B.RPT_NUMBER, B.INP_NUMBER      ");
                oStringBuilder.AppendLine("       FROM WE_PP_INFO A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES B ON A.AN_IN_MDLID = B.NODE_ID AND A.PP_HOGI = '#3'                      ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES C ON A.AN_OUT_MDLID = C.NODE_ID AND B.RPT_NUMBER = C.RPT_NUMBER          ");
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')           ");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P3 ON A.TIMESTAMP = P3.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.BHEAD, C.CHEAD, C.HEAD                                                                      ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (    ------전양정                                                              ");
                oStringBuilder.AppendLine("       SELECT C.HEAD - B.HEAD AS HEAD, B.HEAD AS BHEAD, C.HEAD AS CHEAD, B.RPT_NUMBER, B.INP_NUMBER      ");
                oStringBuilder.AppendLine("       FROM WE_PP_INFO A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES B ON A.AN_IN_MDLID = B.NODE_ID AND A.PP_HOGI = '#4'                      ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES C ON A.AN_OUT_MDLID = C.NODE_ID AND B.RPT_NUMBER = C.RPT_NUMBER          ");
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')           ");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P4 ON A.TIMESTAMP = P4.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.BHEAD, C.CHEAD, C.HEAD                                                                      ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (    ------전양정                                                              ");
                oStringBuilder.AppendLine("       SELECT C.HEAD - B.HEAD AS HEAD, B.HEAD AS BHEAD, C.HEAD AS CHEAD, B.RPT_NUMBER, B.INP_NUMBER      ");
                oStringBuilder.AppendLine("       FROM WE_PP_INFO A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES B ON A.AN_IN_MDLID = B.NODE_ID AND A.PP_HOGI = '#5'                      ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES C ON A.AN_OUT_MDLID = C.NODE_ID AND B.RPT_NUMBER = C.RPT_NUMBER          ");
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')           ");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P5 ON A.TIMESTAMP = P5.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.BHEAD, C.CHEAD, C.HEAD                                                                      ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (    ------전양정                                                              ");
                oStringBuilder.AppendLine("       SELECT C.HEAD - B.HEAD AS HEAD, B.HEAD AS BHEAD, C.HEAD AS CHEAD, B.RPT_NUMBER, B.INP_NUMBER      ");
                oStringBuilder.AppendLine("       FROM WE_PP_INFO A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES B ON A.AN_IN_MDLID = B.NODE_ID AND A.PP_HOGI = '#6'                      ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES C ON A.AN_OUT_MDLID = C.NODE_ID AND B.RPT_NUMBER = C.RPT_NUMBER          ");
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')           ");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P6 ON A.TIMESTAMP = P6.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" ORDER BY DD, HM ASC                                                                                     ");

                //Clipboard.SetText(oStringBuilder.ToString());
                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "WH_RPT_NODES");
                DataColumn oDD = getDataColumn(oDataTable, "DD");
                DataColumn oHM = getDataColumn(oDataTable, "HM");

                oDataTable.PrimaryKey = new DataColumn[] { oDD, oHM };
                if (m_DataSet.Tables["WH_RPT_NODES"] != null)
                {
                    m_DataSet.Tables.Remove("WH_RPT_NODES");
                }
                m_DataSet.Tables.Add(oDataTable);

            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 실시간 해석결과 Link 데이터
        /// WH_RPT_LINKS
        /// </summary>
        private void getRpt_Links()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                string oDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_Day.Value);

                oStringBuilder.AppendLine("SELECT                                                                                                   ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD hh24:MI:SS'), 'YYYY-MM-DD') AS DD,                       ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS'), 'HH24:MI') AS HM                           ");
                oStringBuilder.AppendLine("      ,P1.LINK_ID, NVL(P1.FLOW,0) AS ORG_Q1, NVL(P1.ENERGY,0) AS ORG_W1");
                oStringBuilder.AppendLine("      ,P2.LINK_ID, NVL(P2.FLOW,0) AS ORG_Q2, NVL(P2.ENERGY,0) AS ORG_W2");
                oStringBuilder.AppendLine("      ,P3.LINK_ID, NVL(P3.FLOW,0) AS ORG_Q3, NVL(P3.ENERGY,0) AS ORG_W3");
                oStringBuilder.AppendLine("      ,P4.LINK_ID, NVL(P4.FLOW,0) AS ORG_Q4, NVL(P4.ENERGY,0) AS ORG_W4");
                oStringBuilder.AppendLine("      ,P5.LINK_ID, NVL(P5.FLOW,0) AS ORG_Q5, NVL(P5.ENERGY,0) AS ORG_W5");
                oStringBuilder.AppendLine("      ,P6.LINK_ID, NVL(P6.FLOW,0) AS ORG_Q6, NVL(P6.ENERGY,0) AS ORG_W6");
                oStringBuilder.AppendLine("      , NULL  AS ORG_Q_SUM, NULL AS ORG_W_SUM                                                            ");
                oStringBuilder.AppendLine("FROM                                                                                                     ");
                oStringBuilder.AppendLine(" (SELECT DISTINCT TO_CHAR(TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP   ");
                oStringBuilder.AppendLine("  FROM IF_GATHER_REALTIME_E                                                                              ");
                oStringBuilder.AppendLine("  WHERE TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "') A");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , B.LINK_ID, B.FLOW, B.ENERGY        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN WH_RPT_LINKS B ON A.RPT_NUMBER = B.RPT_NUMBER ");
                oStringBuilder.AppendLine("           AND B.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     INNER JOIN WE_BIZ_PLA C ON B.LINK_ID = C.AN_OPR_MDLID_1");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P1 ON A.TIMESTAMP = P1.TIMESTAMP                                                                   ");

                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , B.LINK_ID, B.FLOW, B.ENERGY        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN WH_RPT_LINKS B ON A.RPT_NUMBER = B.RPT_NUMBER ");
                oStringBuilder.AppendLine("           AND B.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     INNER JOIN WE_BIZ_PLA C ON B.LINK_ID = C.AN_OPR_MDLID_2");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P2 ON A.TIMESTAMP = P2.TIMESTAMP                                                                   ");

                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , B.LINK_ID, B.FLOW, B.ENERGY        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN WH_RPT_LINKS B ON A.RPT_NUMBER = B.RPT_NUMBER ");
                oStringBuilder.AppendLine("           AND B.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     INNER JOIN WE_BIZ_PLA C ON B.LINK_ID = C.AN_OPR_MDLID_3");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P3 ON A.TIMESTAMP = P3.TIMESTAMP                                                                   ");

                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , B.LINK_ID, B.FLOW, B.ENERGY        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN WH_RPT_LINKS B ON A.RPT_NUMBER = B.RPT_NUMBER ");
                oStringBuilder.AppendLine("           AND B.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     INNER JOIN WE_BIZ_PLA C ON B.LINK_ID = C.AN_OPR_MDLID_4");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P4 ON A.TIMESTAMP = P4.TIMESTAMP                                                                   ");

                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , B.LINK_ID, B.FLOW, B.ENERGY        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN WH_RPT_LINKS B ON A.RPT_NUMBER = B.RPT_NUMBER ");
                oStringBuilder.AppendLine("           AND B.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     INNER JOIN WE_BIZ_PLA C ON B.LINK_ID = C.AN_OPR_MDLID_5");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P5 ON A.TIMESTAMP = P5.TIMESTAMP                                                                   ");

                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , B.LINK_ID, B.FLOW, B.ENERGY        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN WH_RPT_LINKS B ON A.RPT_NUMBER = B.RPT_NUMBER ");
                oStringBuilder.AppendLine("           AND B.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     INNER JOIN WE_BIZ_PLA C ON B.LINK_ID = C.AN_OPR_MDLID_6");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P6 ON A.TIMESTAMP = P6.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" ORDER BY DD, HM ASC                                                                                     ");

                //Clipboard.SetText(oStringBuilder.ToString());
                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "WH_RPT_LINKS");
                DataColumn oDD = getDataColumn(oDataTable, "DD");
                DataColumn oHM = getDataColumn(oDataTable, "HM");

                oDataTable.PrimaryKey = new DataColumn[] { oDD, oHM };
                if (m_DataSet.Tables["WH_RPT_LINKS"] != null)
                {
                    m_DataSet.Tables.Remove("WH_RPT_LINKS");
                }
                m_DataSet.Tables.Add(oDataTable);
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }
        /// <summary>
        /// 날짜 및 시간 + 펌프별 가동여부 실시간 데이터
        /// IF_GATHER_REALTIME_E
        /// </summary>
        private void getGather_Realtime()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                string oDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_Day.Value);

                oStringBuilder.AppendLine("SELECT /*+ index_desc(if_gather_realtime_e XPKIF_GATHER_REALTIME_E) */                                   ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD hh24:MI:SS'), 'YYYY-MM-DD') AS DD,                       ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS'), 'HH24:MI') AS HM,                          ");
                oStringBuilder.AppendLine("       P1.AN_OPR_MDLID_1, P1.AN_PTN_MDLID_1, P1.OPR_TAGID_1,  ");
                oStringBuilder.AppendLine("       P2.AN_OPR_MDLID_2, P2.AN_PTN_MDLID_2, P2.OPR_TAGID_2,  ");
                oStringBuilder.AppendLine("       P3.AN_OPR_MDLID_3, P3.AN_PTN_MDLID_3, P3.OPR_TAGID_3,  ");
                oStringBuilder.AppendLine("       P4.AN_OPR_MDLID_4, P4.AN_PTN_MDLID_4, P4.OPR_TAGID_4,  ");
                oStringBuilder.AppendLine("       P5.AN_OPR_MDLID_5, P5.AN_PTN_MDLID_5, P5.OPR_TAGID_5,  ");
                oStringBuilder.AppendLine("       P6.AN_OPR_MDLID_6, P6.AN_PTN_MDLID_6, P6.OPR_TAGID_6,  ");
                oStringBuilder.AppendLine("       NVL(P1.VALUE,0) AS \"#1\", NVL(P2.VALUE,0) AS \"#2\" , NVL(P3.VALUE,0) AS \"#3\",                 ");
                oStringBuilder.AppendLine("       NVL(P4.VALUE,0) AS \"#4\", NVL(P5.VALUE,0) AS \"#5\" , NVL(P6.VALUE,0) AS \"#6\"                  ");
                oStringBuilder.AppendLine("FROM                                                                                                     ");
                oStringBuilder.AppendLine(" (SELECT DISTINCT TO_CHAR(TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP   ");
                oStringBuilder.AppendLine("  FROM IF_GATHER_REALTIME_E                                                                              ");
                oStringBuilder.AppendLine("  WHERE TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "') A");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT A.AN_OPR_MDLID_1, A.AN_PTN_MDLID_1, A.OPR_TAGID_1                                             ");
                oStringBuilder.AppendLine("         , TO_CHAR(B.TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP        ");
                oStringBuilder.AppendLine("         , B.VALUE                                                                     ");
                oStringBuilder.AppendLine("    FROM WE_BIZ_PLA A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_1 = TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("          AND TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P1 ON A.TIMESTAMP = P1.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT A.AN_OPR_MDLID_2, A.AN_PTN_MDLID_2, A.OPR_TAGID_2                                             ");
                oStringBuilder.AppendLine("    , TO_CHAR(B.TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP             ");
                oStringBuilder.AppendLine("    , B.VALUE                                                                          ");
                oStringBuilder.AppendLine("    FROM WE_BIZ_PLA A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_2 = TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("          AND TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P2 ON A.TIMESTAMP = P2.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT A.AN_OPR_MDLID_3, A.AN_PTN_MDLID_3, A.OPR_TAGID_3                                             ");
                oStringBuilder.AppendLine("    , TO_CHAR(B.TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP             ");
                oStringBuilder.AppendLine("    , B.VALUE                                                                          ");
                oStringBuilder.AppendLine("    FROM WE_BIZ_PLA A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_3 = TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("          AND TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P3 ON A.TIMESTAMP = P3.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT A.AN_OPR_MDLID_4, A.AN_PTN_MDLID_4, A.OPR_TAGID_4                                             ");
                oStringBuilder.AppendLine("    , TO_CHAR(B.TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP             ");
                oStringBuilder.AppendLine("    , B.VALUE                                                                         ");
                oStringBuilder.AppendLine("    FROM WE_BIZ_PLA A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_4 = TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("          AND TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P4 ON A.TIMESTAMP = P4.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT A.AN_OPR_MDLID_5, A.AN_PTN_MDLID_5, A.OPR_TAGID_5                                             ");
                oStringBuilder.AppendLine("    , TO_CHAR(B.TIMESTAMP,'YYYYMMDDHH24MISS') AS TIMESTAMP             ");
                oStringBuilder.AppendLine("    , B.VALUE                                                                          ");
                oStringBuilder.AppendLine("    FROM WE_BIZ_PLA A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_5 = TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("          AND TO_CHAR(TIMESTAMP, 'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P5 ON A.TIMESTAMP = P5.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT A.AN_OPR_MDLID_6, A.AN_PTN_MDLID_6, A.OPR_TAGID_6                                             ");
                oStringBuilder.AppendLine("    , TO_CHAR(B.TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP             ");
                oStringBuilder.AppendLine("    , B.VALUE                                                                         ");
                oStringBuilder.AppendLine("    FROM WE_BIZ_PLA A                                                                                    ");
                oStringBuilder.AppendLine("     INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_6 = TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("          AND TO_CHAR(TIMESTAMP,  'YYYYMMDD') = '" + oDay + "'");
                oStringBuilder.AppendLine("    ) P6 ON A.TIMESTAMP = P6.TIMESTAMP                                                                   ");
                oStringBuilder.AppendLine(" ORDER BY DD, HM ASC                                                                                     ");

                //Clipboard.SetText(oStringBuilder.ToString());
                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "IF_GATHER_REALTIME");
                DataColumn oDD = getDataColumn(oDataTable, "DD");
                DataColumn oHM = getDataColumn(oDataTable, "HM");

                oDataTable.PrimaryKey = new DataColumn[] { oDD, oHM };

                if (m_DataSet.Tables["IF_GATHER_REALTIME"] != null)
                {
                    m_DataSet.Tables.Remove("IF_GATHER_REALTIME");
                }
                m_DataSet.Tables.Add(oDataTable);
                
                
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        #endregion

        //private void CloneDataTable(DataTable oDataTable)
        //{
        //    DataRow row = null;
        //    foreach (DataRow oRow in oDataTable.Rows)
        //    {
        //        row = m_DataSet.Tables[oDataTable.TableName].NewRow();
        //        foreach (DataColumn oCol in oDataTable.Columns)
        //        {
        //            row[oCol.ColumnName] = oCol;
        //        }
        //        m_DataSet.Tables[oDataTable.TableName].Rows.Add(row);
        //    }
            
        //}

        /// <summary>
        /// 해당 테이블에 칼럼을 리턴한다.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private DataColumn getDataColumn(DataTable table, string key)
        {
            DataColumn oDataColumn = null;

            //DataTable oDataTable = GetDataSource();
            if (table != null)
            {
                foreach (DataColumn item in table.Columns)
                {
                    if (item.ColumnName == key)
                    {
                        return item;
                    }
                }
            }

            return oDataColumn;
        }


        #region 버튼(기능) 실행
        /// <summary>
        /// 불러오기 버튼 클릭 - 펌프 가동데이터 불러오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoad_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1Collapsed = true;     //챠트부분 Visible = true
            //기존 테이블 초기화
            if (m_DataSet.Tables["IF_GATHER_REALTIME"] != null) m_DataSet.Tables["IF_GATHER_REALTIME"].Clear();    
            if (m_DataSet.Tables["WH_RPT_LINKS"] != null) m_DataSet.Tables["WH_RPT_LINKS"].Clear();
            if (m_DataSet.Tables["WH_RPT_NODES"] != null) m_DataSet.Tables["WH_RPT_NODES"].Clear();
            if (m_DataSet.Tables["WH_ANALISYS"] != null)  m_DataSet.Tables["WH_ANALISYS"].Clear();

            Application.DoEvents();

            getGather_Realtime();
            getRpt_Links();
            getRpt_Nodes();

            SetMergedDataTable();

            VisibleColumnGroup(this, new EventArgs());
        }

        /// <summary>
        /// 챠트 그리기
        /// </summary>
        private void DrawChartData()
        {
            this.chart_ORG.Data.Series = 30;
            this.chart_ORG.Data.Points = m_gridManager1.Grid.Rows.Count;
            this.chart_SIMU.Data.Series = 30;
            this.chart_SIMU.Data.Points = m_gridManager1.Grid.Rows.Count;

            this.chart_ORG.Series[0].Text = "1호기-전력량";
            this.chart_ORG.Series[1].Text = "1호기-유량";
            this.chart_ORG.Series[2].Text = "1호기-양정";
            this.chart_ORG.Series[3].Text = "1호기-효율";
            this.chart_ORG.Series[4].Text = "1호기-원단위";
            this.chart_ORG.Series[5].Text = "2호기-전력량";
            this.chart_ORG.Series[6].Text = "2호기-유량";
            this.chart_ORG.Series[7].Text = "2호기-양정";
            this.chart_ORG.Series[8].Text = "2호기-효율";
            this.chart_ORG.Series[9].Text = "2호기-원단위";
            this.chart_ORG.Series[10].Text = "3호기-전력량";
            this.chart_ORG.Series[11].Text = "3호기-유량";
            this.chart_ORG.Series[12].Text = "3호기-양정";
            this.chart_ORG.Series[13].Text = "3호기-효율";
            this.chart_ORG.Series[14].Text = "3호기-원단위";
            this.chart_ORG.Series[15].Text = "4호기-전력량";
            this.chart_ORG.Series[16].Text = "4호기-유량";
            this.chart_ORG.Series[17].Text = "4호기-양정";
            this.chart_ORG.Series[18].Text = "4호기-효율";
            this.chart_ORG.Series[19].Text = "4호기-원단위";
            this.chart_ORG.Series[20].Text = "5호기-전력량";
            this.chart_ORG.Series[21].Text = "5호기-유량";
            this.chart_ORG.Series[22].Text = "5호기-양정";
            this.chart_ORG.Series[23].Text = "5호기-효율";
            this.chart_ORG.Series[24].Text = "5호기-원단위";
            this.chart_ORG.Series[25].Text = "6호기-전력량";
            this.chart_ORG.Series[26].Text = "6호기-유량";
            this.chart_ORG.Series[27].Text = "6호기-양정";
            this.chart_ORG.Series[28].Text = "6호기-효율";
            this.chart_ORG.Series[29].Text = "6호기-원단위";

            //-----------------------------------------------
            this.chart_SIMU.Series[0].Text = "1호기-전력량";
            this.chart_SIMU.Series[1].Text = "1호기-유량";
            this.chart_SIMU.Series[2].Text = "1호기-양정";
            this.chart_SIMU.Series[3].Text = "1호기-효율";
            this.chart_SIMU.Series[4].Text = "1호기-원단위";
            this.chart_SIMU.Series[5].Text = "2호기-전력량";
            this.chart_SIMU.Series[6].Text = "2호기-유량";
            this.chart_SIMU.Series[7].Text = "2호기-양정";
            this.chart_SIMU.Series[8].Text = "2호기-효율";
            this.chart_SIMU.Series[9].Text = "2호기-원단위";
            this.chart_SIMU.Series[10].Text = "3호기-전력량";
            this.chart_SIMU.Series[11].Text = "3호기-유량";
            this.chart_SIMU.Series[12].Text = "3호기-양정";
            this.chart_SIMU.Series[13].Text = "3호기-효율";
            this.chart_SIMU.Series[14].Text = "3호기-원단위";
            this.chart_SIMU.Series[15].Text = "4호기-전력량";
            this.chart_SIMU.Series[16].Text = "4호기-유량";
            this.chart_SIMU.Series[17].Text = "4호기-양정";
            this.chart_SIMU.Series[18].Text = "4호기-효율";
            this.chart_SIMU.Series[19].Text = "4호기-원단위";
            this.chart_SIMU.Series[20].Text = "5호기-전력량";
            this.chart_SIMU.Series[21].Text = "5호기-유량";
            this.chart_SIMU.Series[22].Text = "5호기-양정";
            this.chart_SIMU.Series[23].Text = "5호기-효율";
            this.chart_SIMU.Series[24].Text = "5호기-원단위";
            this.chart_SIMU.Series[25].Text = "6호기-전력량";
            this.chart_SIMU.Series[26].Text = "6호기-유량";
            this.chart_SIMU.Series[27].Text = "6호기-양정";
            this.chart_SIMU.Series[28].Text = "6호기-효율";
            this.chart_SIMU.Series[29].Text = "6호기-원단위";
            //-------------------------------------------------------



            this.chart_ORG.Series[0].Gallery = Gallery.Curve;
            this.chart_ORG.Series[1].Gallery = Gallery.Curve;
            this.chart_ORG.Series[2].Gallery = Gallery.Curve;
            this.chart_ORG.Series[3].Gallery = Gallery.Curve;
            this.chart_ORG.Series[4].Gallery = Gallery.Curve;
            this.chart_ORG.Series[5].Gallery = Gallery.Curve;
            this.chart_ORG.Series[6].Gallery = Gallery.Curve;
            this.chart_ORG.Series[7].Gallery = Gallery.Curve;
            this.chart_ORG.Series[8].Gallery = Gallery.Curve;
            this.chart_ORG.Series[9].Gallery = Gallery.Curve;
            this.chart_ORG.Series[10].Gallery = Gallery.Curve;
            this.chart_ORG.Series[11].Gallery = Gallery.Curve;
            this.chart_ORG.Series[12].Gallery = Gallery.Curve;
            this.chart_ORG.Series[13].Gallery = Gallery.Curve;
            this.chart_ORG.Series[14].Gallery = Gallery.Curve;
            this.chart_ORG.Series[15].Gallery = Gallery.Curve;
            this.chart_ORG.Series[16].Gallery = Gallery.Curve;
            this.chart_ORG.Series[17].Gallery = Gallery.Curve;
            this.chart_ORG.Series[18].Gallery = Gallery.Curve;
            this.chart_ORG.Series[19].Gallery = Gallery.Curve;
            this.chart_ORG.Series[20].Gallery = Gallery.Curve;
            this.chart_ORG.Series[21].Gallery = Gallery.Curve;
            this.chart_ORG.Series[22].Gallery = Gallery.Curve;
            this.chart_ORG.Series[23].Gallery = Gallery.Curve;
            this.chart_ORG.Series[24].Gallery = Gallery.Curve;
            this.chart_ORG.Series[25].Gallery = Gallery.Curve;
            this.chart_ORG.Series[26].Gallery = Gallery.Curve;
            this.chart_ORG.Series[27].Gallery = Gallery.Curve;
            this.chart_ORG.Series[28].Gallery = Gallery.Curve;
            this.chart_ORG.Series[29].Gallery = Gallery.Curve;

            //-----------------------------------------------
            this.chart_SIMU.Series[0].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[1].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[2].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[3].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[4].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[5].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[6].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[7].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[8].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[9].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[10].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[11].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[12].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[13].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[14].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[15].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[16].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[17].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[18].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[19].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[20].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[21].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[22].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[23].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[24].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[25].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[26].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[27].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[28].Gallery = Gallery.Curve;
            this.chart_SIMU.Series[29].Gallery = Gallery.Curve;

            //-------------------------------------------------

            this.chart_ORG.Series[0].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[1].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[2].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[3].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[4].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[5].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[6].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[7].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[8].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[9].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[10].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[11].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[12].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[13].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[14].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[15].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[16].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[17].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[18].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[19].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[20].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[21].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[22].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[23].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[24].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[25].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[26].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[27].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[28].AxisY = chart_ORG.AxisY;
            this.chart_ORG.Series[29].AxisY = chart_ORG.AxisY;

            //-----------------------------------------------
            this.chart_SIMU.Series[0].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[1].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[2].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[3].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[4].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[5].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[6].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[7].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[8].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[9].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[10].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[11].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[12].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[13].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[14].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[15].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[16].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[17].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[18].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[19].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[20].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[21].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[22].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[23].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[24].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[25].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[26].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[27].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[28].AxisY = chart_SIMU.AxisY;
            this.chart_SIMU.Series[29].AxisY = chart_SIMU.AxisY;
            
            foreach (UltraGridRow row in m_gridManager1.Grid.Rows)
            {
                int idx = m_gridManager1.Grid.Rows.IndexOf(row);
                this.chart_ORG.AxisX.Labels[idx] = Convert.ToString(row.Cells["HM"].Value);
                this.chart_SIMU.AxisX.Labels[idx] = Convert.ToString(row.Cells["HM"].Value);

                this.chart_ORG.Data[0, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_W1"].Value);
                this.chart_ORG.Data[1, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_Q1"].Value);
                this.chart_ORG.Data[2, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_H1"].Value);
                this.chart_ORG.Data[3, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_E1"].Value);
                this.chart_ORG.Data[4, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_U1"].Value);

                this.chart_ORG.Data[5, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_W2"].Value);
                this.chart_ORG.Data[6, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_Q2"].Value);
                this.chart_ORG.Data[7, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_H2"].Value);
                this.chart_ORG.Data[8, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_E2"].Value);
                this.chart_ORG.Data[9, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_U2"].Value);

                this.chart_ORG.Data[10, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_W3"].Value);
                this.chart_ORG.Data[11, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_Q3"].Value);
                this.chart_ORG.Data[12, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_H3"].Value);
                this.chart_ORG.Data[13, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_E3"].Value);
                this.chart_ORG.Data[14, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_U3"].Value);

                this.chart_ORG.Data[15, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_W4"].Value);
                this.chart_ORG.Data[16, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_Q4"].Value);
                this.chart_ORG.Data[17, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_H4"].Value);
                this.chart_ORG.Data[18, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_E4"].Value);
                this.chart_ORG.Data[19, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_U4"].Value);

                this.chart_ORG.Data[20, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_W5"].Value);
                this.chart_ORG.Data[21, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_Q5"].Value);
                this.chart_ORG.Data[22, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_H5"].Value);
                this.chart_ORG.Data[23, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_E5"].Value);
                this.chart_ORG.Data[24, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_U5"].Value);

                this.chart_ORG.Data[25, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_W6"].Value);
                this.chart_ORG.Data[26, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_Q6"].Value);
                this.chart_ORG.Data[27, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_H6"].Value);
                this.chart_ORG.Data[28, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_E6"].Value);
                this.chart_ORG.Data[29, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["ORG_U6"].Value);  

                //------------------------------------------------------------------------
                this.chart_SIMU.Data[0, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_W1"].Value);
                this.chart_SIMU.Data[1, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_Q1"].Value);
                this.chart_SIMU.Data[2, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_H1"].Value);
                this.chart_SIMU.Data[3, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_E1"].Value);
                this.chart_SIMU.Data[4, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_U1"].Value);

                this.chart_SIMU.Data[5, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_W2"].Value);
                this.chart_SIMU.Data[6, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_Q2"].Value);
                this.chart_SIMU.Data[7, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_H2"].Value);
                this.chart_SIMU.Data[8, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_E2"].Value);
                this.chart_SIMU.Data[9, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_U2"].Value);

                this.chart_SIMU.Data[10, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_W3"].Value);
                this.chart_SIMU.Data[11, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_Q3"].Value);
                this.chart_SIMU.Data[12, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_H3"].Value);
                this.chart_SIMU.Data[13, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_E3"].Value);
                this.chart_SIMU.Data[14, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_U3"].Value);

                this.chart_SIMU.Data[15, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_W4"].Value);
                this.chart_SIMU.Data[16, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_Q4"].Value);
                this.chart_SIMU.Data[17, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_H4"].Value);
                this.chart_SIMU.Data[18, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_E4"].Value);
                this.chart_SIMU.Data[19, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_U4"].Value);

                this.chart_SIMU.Data[20, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_W5"].Value);
                this.chart_SIMU.Data[21, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_Q5"].Value);
                this.chart_SIMU.Data[22, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_H5"].Value);
                this.chart_SIMU.Data[23, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_E5"].Value);
                this.chart_SIMU.Data[24, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_U5"].Value);

                this.chart_SIMU.Data[25, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_W6"].Value);
                this.chart_SIMU.Data[26, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_Q6"].Value);
                this.chart_SIMU.Data[27, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_H6"].Value);
                this.chart_SIMU.Data[28, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_E6"].Value);
                this.chart_SIMU.Data[29, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["SIMU_U6"].Value);     
                //--------------------------------------------------------------------------                         
            }
         
            this.chart_ORG.Update();
            this.chart_SIMU.Update();

            comboBox_ORG_SelectedValueChanged(this, new EventArgs());
        }

        /// <summary>
        /// 데이터 테이블 Merge
        /// 그리드에 맵핑한다.
        /// </summary>
        private void SetMergedDataTable()
        {
            m_MasterTable.Rows.Clear();

            m_MasterTable.Merge(m_DataSet.Tables["IF_GATHER_REALTIME"], false, MissingSchemaAction.Ignore);
            m_MasterTable.Merge(m_DataSet.Tables["WH_RPT_LINKS"], false, MissingSchemaAction.Ignore);
            m_MasterTable.Merge(m_DataSet.Tables["WH_RPT_NODES"], false, MissingSchemaAction.Ignore);
            m_MasterTable.Merge(m_DataSet.Tables["WH_ANALISYS"], false, MissingSchemaAction.Ignore);

            m_gridManager1.SetDataSource(m_MasterTable);

            m_gridManager1.SetGridSort("HM");

            DrawChartData();
        }




        private void btnExcel_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region 그리드 팝업메뉴
        private void ultraGrid_WE_PP_MODEL_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                contextMenuStripUltraGrid.Show((UltraGrid)sender, e.X, e.Y);
        }

        /// <summary>
        /// 그리드 팝업 : 선택 Cell ClipBoard로 복사
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuCopy_Click(object sender, EventArgs e)
        {
            this.ultraGrid_WE_PP_MODEL.PerformAction(UltraGridAction.Copy);
        }

        /// <summary>
        /// 그리드 팝업 : ClipBoard 붙이기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuPaste_Click(object sender, EventArgs e)
        {
            this.ultraGrid_WE_PP_MODEL.PerformAction(UltraGridAction.Paste);
        }

        /// <summary>
        /// 그리드 팝업 : 되돌리기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuUndo_Click(object sender, EventArgs e)
        {
            this.ultraGrid_WE_PP_MODEL.PerformAction(UltraGridAction.Undo);
        }

        private void ToolStripMenuRedo_Click(object sender, EventArgs e)
        {
            this.ultraGrid_WE_PP_MODEL.PerformAction(UltraGridAction.Redo);
        }

        /// <summary>
        /// 그리드 팝업 : Column 값 치환 : 2개 column선택
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuColumn_Click(object sender, EventArgs e)
        {
           
                         
        }

        /// <summary>
        /// 그리드 팝업 : 선택 Cell 일괄변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuCell_Click(object sender, EventArgs e)
        {
            m_gridManager1.SetGridStyle_Select();
        }

        #endregion

        #region UltraGrid Action
        /// <summary>
        /// CanPerformAction
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        private bool CanPerformAction(UltraGridAction action)
        {
            UltraGridState s = this.ultraGrid_WE_PP_MODEL.CurrentState;
            
            return ultraGrid_WE_PP_MODEL.KeyActionMappings.IsActionAllowed(action, (long)this.ultraGrid_WE_PP_MODEL.CurrentState);
        }

        #region ultraGrid_WE_TM_MES_DTL_AfterSelectChange
        private void ultraGrid_WE_PP_MODEL_AfterSelectChange(object sender, Infragistics.Win.UltraWinGrid.AfterSelectChangeEventArgs e)
        {
            UltraGridState s = this.ultraGrid_WE_PP_MODEL.CurrentState;
            this.UpdateMenuEnabledStates();
        }
        #endregion ultraGrid_WE_TM_MES_DTL_AfterSelectChange

        #region ultraGrid_WE_TM_MES_DTL_AfterPerformAction
        private void ultraGrid_WE_PP_MODEL_AfterPerformAction(object sender, Infragistics.Win.UltraWinGrid.AfterUltraGridPerformActionEventArgs e)
        {
            this.UpdateMenuEnabledStates();
        }
        #endregion ultraGrid_WE_TM_MES_DTL_AfterPerformAction

        private void ultraGrid_WE_PP_MODEL_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.MultiCellOperation)
            {
                e.Cancel = true;

                System.Text.StringBuilder SB = new System.Text.StringBuilder();

                SB.AppendFormat("An error has occurred during the {0} operation", e.MultiCellOperationErrorInfo.Operation.ToString());
                SB.Append(Environment.NewLine);
                SB.Append("The error was as follows:");
                SB.Append(Environment.NewLine);
                SB.Append(Environment.NewLine);
                SB.AppendFormat("\"{0}\"", e.ErrorText);

                // Get the cell on which the error occurred
                UltraGridCell errorCell = e.MultiCellOperationErrorInfo.ErrorCell;

                // The ErrorCell will be null if the error was an error in the whole
                // operation as oppossed to an error no a single cell.
                if (errorCell != null)
                {
                    int rowIndex = errorCell.Row.Index;
                    string columnCaption = errorCell.Column.Header.Caption;

                    SB.Append(Environment.NewLine);
                    SB.AppendFormat("The error occurred on Row {0} in Column {1}", rowIndex, columnCaption);
                }

                MessageBox.Show(this, SB.ToString());
            }
        }

        private void UpdateMenuEnabledStates()
        {
            this.toolStripMenuCopy.Enabled = this.CanPerformAction(UltraGridAction.Copy);
            this.toolStripMenuPaste.Enabled = this.CanPerformAction(UltraGridAction.Paste);

            this.toolStripMenuUndo.Enabled = this.CanPerformAction(UltraGridAction.Undo);
            this.ToolStripMenuRedo.Enabled = this.CanPerformAction(UltraGridAction.Redo);

        }

        #endregion 

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReUseControls();
        }

        private void ReUseControls()
        {

        }

        /// <summary>
        /// 그리드 다시 구성 : 펌프의 호기갯수가 다르므로, 조회후 다시구성한다.
        /// </summary>
        /// <param name="oReader"></param>
        //private void ReInitializeGrid(DataTable oTable)
        //{
        //    m_gridManager1.RemoveAllColumns();

        //    UltraGridColumn oUltraGridColumn;
        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "DD";
        //    oUltraGridColumn.Header.Caption = "가동일자";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;

        //    oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
        //    oUltraGridColumn.Key = "HM";
        //    oUltraGridColumn.Header.Caption = "가동시간";
        //    oUltraGridColumn.CellActivation = Activation.NoEdit;
        //    oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
        //    oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
        //    oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
        //    oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
        //    oUltraGridColumn.Hidden = false;

        //    if (oTable.Rows.Count == 0) return;

        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        m_gridManager1.AddColumn(Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.AllowEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "펌프");
        //    }

        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        m_gridManager1.AddColumn("OW" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, true, 100, "전력량");
        //        m_gridManager1.AddColumn("NW" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "전력량");
        //    }
        //    m_gridManager1.AddColumn("W_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "전력량");

        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        m_gridManager1.AddColumn("OQ" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, true, 100, "유량");
        //        m_gridManager1.AddColumn("NQ" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "유량");
        //    }
        //    m_gridManager1.AddColumn("Q_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "유량");

        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        m_gridManager1.AddColumn("OH" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, true, 100, "양정");
        //        m_gridManager1.AddColumn("NH" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "양정");
        //    }
        //    m_gridManager1.AddColumn("H_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "양정");

        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        m_gridManager1.AddColumn("OU" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, true, 100, "원단위");
        //        m_gridManager1.AddColumn("NU" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "원단위");
        //    }
        //    m_gridManager1.AddColumn("U_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "원단위");

        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        m_gridManager1.AddColumn("OE" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, true, 100, "효율");
        //        m_gridManager1.AddColumn("NE" + Convert.ToString(item["PP_HOGI"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "효율");
        //    }
        //    m_gridManager1.AddColumn("E_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "효율");



        //    ArrayList keyValues = new ArrayList();
        //    keyValues.Add("DD");
        //    keyValues.Add("HM");
        //    keyValues.Add("AN_MDLID");
        //    m_gridManager1.AddColumnGroup("모의날짜", keyValues);

        //    keyValues.Clear();
        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        keyValues.Add(Convert.ToString(item["PP_HOGI"]));
        //    }
        //    m_gridManager1.AddColumnGroup("펌프모터 가동", "펌프모터 가동", keyValues, "Lock");

        //    keyValues.Clear();
        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        keyValues.Add("OW" + Convert.ToString(item["PP_HOGI"]));
        //        keyValues.Add("NW" + Convert.ToString(item["PP_HOGI"]));
        //    }
        //    keyValues.Add("W_SUM");
        //    m_gridManager1.AddColumnGroup("전력량", "전력량", keyValues, "Unlock");

        //    keyValues.Clear();
        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        keyValues.Add("OQ" + Convert.ToString(item["PP_HOGI"]));
        //        keyValues.Add("NQ" + Convert.ToString(item["PP_HOGI"]));
        //    }
        //    keyValues.Add("Q_SUM");
        //    m_gridManager1.AddColumnGroup("유량", "유량", keyValues, "Unlock");

        //    keyValues.Clear();
        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        keyValues.Add("OH" + Convert.ToString(item["PP_HOGI"]));
        //        keyValues.Add("NH" + Convert.ToString(item["PP_HOGI"]));
        //    }
        //    keyValues.Add("H_SUM");
        //    m_gridManager1.AddColumnGroup("양정", "양정", keyValues, "Unlock");

        //    keyValues.Clear();
        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        keyValues.Add("OU" + Convert.ToString(item["PP_HOGI"]));
        //        keyValues.Add("NU" + Convert.ToString(item["PP_HOGI"]));
        //    }
        //    keyValues.Add("U_SUM");
        //    m_gridManager1.AddColumnGroup("원단위", "원단위", keyValues, "Unlock");

        //    keyValues.Clear();
        //    foreach (DataRow item in oTable.Rows)
        //    {
        //        keyValues.Add("OE" + Convert.ToString(item["PP_HOGI"]));
        //        keyValues.Add("NE" + Convert.ToString(item["PP_HOGI"]));
        //    }
        //    keyValues.Add("E_SUM");
        //    m_gridManager1.AddColumnGroup("효율", "효율", keyValues, "Unlock");

        //    m_gridManager1.Grid.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
        //}

          /// <summary>
        /// 사업장 콤보박스 SelectValue Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_BIZ_PLA_NM_SelectedValueChanged(object sender, EventArgs e)
        {

            setMemberVariable();
            //OracleDBManager oDBManager = new OracleDBManager();
            //oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            //OracleDataReader oReader = null;
            //try
            //{
            //    oDBManager.Open();

            //    StringBuilder oStringBuilder = new StringBuilder();
            //    oStringBuilder.AppendLine("SELECT A.FAC_SEQ, A.PP_HOGI");
            //    oStringBuilder.AppendLine("FROM WE_PP_INFO A");
            //    oStringBuilder.AppendLine("INNER JOIN WE_BIZ_PLA  B ON A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND B.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
            //    oStringBuilder.AppendLine("ORDER BY PP_HOGI ASC");

            //    oReader = oDBManager.ExecuteScriptDataReader(oStringBuilder.ToString(), null);
            //    while (oReader.Read())
            //    {
                    
            //    }
            //    //oStringBuilder.AppendLine("SELECT OPR_TAGID_1, OPR_TAGID_2, OPR_TAGID_3, OPR_TAGID_4, OPR_TAGID_5, OPR_TAGID_6");
            //    //oStringBuilder.AppendLine("FROM WE_BIZ_PLA A");

            //    //DataSet pDataset = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null);
            //    //DataTable pDataTable = pDataset.Tables[0];

            //    //ReInitializeGrid(pDataTable);

            //    //comboBox_gubun_SelectedValueChanged(this.comboBox_gubun, new EventArgs());
            //}
            //catch (Exception oException)
            //{
            //    Console.WriteLine(oException.Message);
            //    //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            //}
            //finally
            //{
            //    if (oReader != null) oReader.Close();
            //    if (oDBManager != null) oDBManager.Close();
            //}
        }

        //private void comboBox_gubun_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ArrayList oArray = new ArrayList();
        //    switch (Convert.ToString(((ComboBox)sender).SelectedValue))
        //    {
        //        case "전력량":
        //            oArray.Add("ORG_W");
        //            oArray.Add("SIMU_W");
        //            m_gridManager1.VisibleGridGroup("ORG_W", false);
        //            m_gridManager1.VisibleGridGroup("SIMU_W", false);

        //            label_ORG.Text = "실시간 해석 - 전력량";
        //            label_SIMU.Text = "모의 해석 - 전력량";
        //            break;
        //        case "유량":
        //            oArray.Add("ORG_Q");
        //            oArray.Add("SIMU_Q");
        //            m_gridManager1.VisibleGridGroup("ORG_Q", false);
        //            m_gridManager1.VisibleGridGroup("SIMU_Q", false);

        //            label_ORG.Text = "실시간 해석 - 유량";
        //            label_SIMU.Text = "모의 해석 - 유량";
        //            break;
        //        case "양정":
        //            oArray.Add("ORG_H");
        //            oArray.Add("SIMU_H");
        //            m_gridManager1.VisibleGridGroup("ORG_H", false);
        //            m_gridManager1.VisibleGridGroup("SIMU_H", false);

        //            label_ORG.Text = "실시간 해석 - 양정";
        //            label_SIMU.Text = "모의 해석 - 양정";
        //            break;
        //        case "효율":
        //            oArray.Add("ORG_E");
        //            oArray.Add("SIMU_E");
        //            m_gridManager1.VisibleGridGroup("ORG_E", false);
        //            m_gridManager1.VisibleGridGroup("SIMU_E", false);

        //            label_ORG.Text = "실시간 해석 - 효율";
        //            label_SIMU.Text = "모의 해석 - 효율";
        //            break;
        //        case "원단위":
        //            oArray.Add("ORG_U");
        //            oArray.Add("SIMU_U");
        //            m_gridManager1.VisibleGridGroup("ORG_U", false);
        //            m_gridManager1.VisibleGridGroup("SIMU_U", false);

        //            label_ORG.Text = "실시간 해석 - 원단위";
        //            label_SIMU.Text = "모의 해석 - 원단위";
        //            break;
        //    }
        //    //DrawChartData();

        //}

        private string gettimetostring(string stime)
        {
            string hm = string.Empty;
            if (stime.Length == 5 )
            {
                int hh = Convert.ToInt16(stime.Substring(0, 2));
                //int mm = Convert.ToInt16(stime.Substring(3, 2));
                hm = Convert.ToString(hh) + ":" + Convert.ToString(stime.Substring(3, 2));
            }
            return hm;
        }

        private void btnModelEdit_Click(object sender, EventArgs e)
        {
            if (ultraGrid_WE_PP_MODEL.Rows.Count < 1) return;
            splitContainer1.Panel1Collapsed = true;     //챠트부분 Visible = false
            this.Cursor = Cursors.WaitCursor;
            Application.DoEvents();

            //날짜, 시간순서가 중요함으로 소트하여 배열
            m_gridManager1.SetGridSort("HM");

            ArrayList oPATTERNSarray = new ArrayList();
            
            Hashtable resetValue = new Hashtable();  //최상위 hashtable
            Hashtable oPATTERNS = new Hashtable(); //서브 펌프 패턴
            Hashtable oPUMPS = new Hashtable(); //서브 펌프 파라메터

            //Console.WriteLine("P1 => " + m_strAN_OPR_MDLID_1 + " : " + " HEAD " + m_strAN_PTN_MDLID_1 + " PATTERN " + m_strAN_PTN_MDLID_1);
            //Console.WriteLine("P2 => " + m_strAN_OPR_MDLID_2 + " : " + " HEAD " + m_strAN_PTN_MDLID_2 + " PATTERN " + m_strAN_PTN_MDLID_2);
            //Console.WriteLine("P3 => " + m_strAN_OPR_MDLID_3 + " : " + " HEAD " + m_strAN_PTN_MDLID_3 + " PATTERN " + m_strAN_PTN_MDLID_3);
            //Console.WriteLine("P4 => " + m_strAN_OPR_MDLID_4 + " : " + " HEAD " + m_strAN_PTN_MDLID_4 + " PATTERN " + m_strAN_PTN_MDLID_4);
            //Console.WriteLine("P5 => " + m_strAN_OPR_MDLID_5 + " : " + " HEAD " + m_strAN_PTN_MDLID_5 + " PATTERN " + m_strAN_PTN_MDLID_5);
            //Console.WriteLine("P6 => " + m_strAN_OPR_MDLID_6 + " : " + " HEAD " + m_strAN_PTN_MDLID_6 + " PATTERN " + m_strAN_PTN_MDLID_6);

            oPUMPS.Add(m_strAN_OPR_MDLID_1, " HEAD " + m_strAN_PTN_MDLID_1 + " PATTERN " + m_strAN_PTN_MDLID_1);
            oPUMPS.Add(m_strAN_OPR_MDLID_2, " HEAD " + m_strAN_PTN_MDLID_2 + " PATTERN " + m_strAN_PTN_MDLID_2);
            oPUMPS.Add(m_strAN_OPR_MDLID_3, " HEAD " + m_strAN_PTN_MDLID_3 + " PATTERN " + m_strAN_PTN_MDLID_3);
            oPUMPS.Add(m_strAN_OPR_MDLID_4, " HEAD " + m_strAN_PTN_MDLID_4 + " PATTERN " + m_strAN_PTN_MDLID_4);
            oPUMPS.Add(m_strAN_OPR_MDLID_5, " HEAD " + m_strAN_PTN_MDLID_5 + " PATTERN " + m_strAN_PTN_MDLID_5);
            oPUMPS.Add(m_strAN_OPR_MDLID_6, " HEAD " + m_strAN_PTN_MDLID_6 + " PATTERN " + m_strAN_PTN_MDLID_6);
            resetValue.Add("pump", oPUMPS);
            foreach (UltraGridRow oRow in ultraGrid_WE_PP_MODEL.Rows)
            {
                //oPATTERNSarray.Add(Convert.IsDBNull(oRow.Cells["#1"].Value) ? "0" : Convert.ToString(oRow.Cells["#1"].Value));
                

                if (Convert.IsDBNull(oRow.Cells["#1"].Value) | Convert.ToString(oRow.Cells["#1"].Value) == "0")
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_1 +  " CLOSED AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " CLOSE AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }
                else
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_1 + " OPEN AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " OPEN AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }


                if (Convert.IsDBNull(oRow.Cells["#2"].Value) | Convert.ToString(oRow.Cells["#2"].Value) == "0")
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_2 + " CLOSED AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " CLOSE AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }
                else
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_2 + " OPEN AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " OPEN AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }

                if (Convert.IsDBNull(oRow.Cells["#3"].Value) | Convert.ToString(oRow.Cells["#3"].Value) == "0")
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_3 + " CLOSED AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_3 + " CLOSE AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }
                else
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_3 + " OPEN AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " OPEN AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }

                if (Convert.IsDBNull(oRow.Cells["#4"].Value) | Convert.ToString(oRow.Cells["#4"].Value) == "0")
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_4 + " CLOSED AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " CLOSE AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }
                else
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_4 + " OPEN AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " OPEN AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }

                if (Convert.IsDBNull(oRow.Cells["#5"].Value) | Convert.ToString(oRow.Cells["#5"].Value) == "0")
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_5 + " CLOSED AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " CLOSE AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }
                else
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_5 + " OPEN AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " OPEN AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }

                if (Convert.IsDBNull(oRow.Cells["#6"].Value) | Convert.ToString(oRow.Cells["#6"].Value) == "0")
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_6 + " CLOSED AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " CLOSE AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }
                else
                {
                    oPATTERNSarray.Add("LINK " + m_strAN_OPR_MDLID_6 + " OPEN AT TIME " + gettimetostring(Convert.ToString(oRow.Cells["HM"].Value)));
                    //Console.WriteLine("LINK " + m_strAN_OPR_MDLID_1 + " OPEN AT TIME " + Convert.ToString(oRow.Cells["HM"].Value));
                }

                
            }
            

            resetValue.Add("control", oPATTERNSarray);

            try
            {
                ///에너지 모델 해석실행
                AnalysisEngine engine = new AnalysisEngine();

                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", m_INP_NUMBER);
                conditions.Add("analysisType", 0);
                conditions.Add("resetValues", resetValue);  //null : DB에 저장된 모델, HashTable
                conditions.Add("saveReport", false);

                Hashtable result = engine.Execute(conditions);
                //----------------------------------------------------

                Hashtable resultByTime = new Hashtable();
                Hashtable resultByType = new Hashtable();
                ArrayList resultList = new ArrayList();
                Hashtable resultData = new Hashtable();

                //해석결과를 담기위해
                DataTable oDataTable = m_DataSet.Tables["WH_ANALISYS"];
                oDataTable.Rows.Clear();

                DataRow oDataRow = null;

                string oDay = FunctionManager.DateTimeToDashString((DateTime)ultraDateTimeEditor_Day.Value);

                foreach (string time in result.Keys)    ///해석시간
                {
                    oDataRow = oDataTable.NewRow();
                    oDataRow["DD"] = oDay;
                    oDataRow["HM"] = time;

                    resultByTime = result[time] as Hashtable;
                    foreach (string type in resultByTime.Keys)  //노드 or 링크
                    {
                        resultByType = resultByTime[type] as Hashtable;
                        foreach (string subtype in resultByType.Keys)  //junction, reservior...
                        {
                            resultList = resultByType[subtype] as ArrayList;  //해석항목ID(복수)에 해당하는 해석값
                            switch (subtype)
                            {
                                case "junction":    //펌프의 전단과 후단의 HEAD를 비교
                                    //for (int i = 0; i < resultList.Count; i++)
                                    //{
                                    //    resultData = resultList[i] as Hashtable;  //개개의 해석항목ID 해석값
                                    double h_value = 0.0;

                                    h_value = getJuncionData(resultList, m_strAN_IN_MDLID_1, m_strAN_OUT_MDLID_1);
                                    if (h_value != 0)
                                    {
                                        oDataRow["SIMU_H1"] = FunctionManager.getRound(h_value, 2);
                                    }
                                    h_value = getJuncionData(resultList, m_strAN_IN_MDLID_2, m_strAN_OUT_MDLID_2);
                                    if (h_value != 0)
                                    {
                                        oDataRow["SIMU_H2"] = FunctionManager.getRound(h_value, 2);
                                    }
                                    h_value = getJuncionData(resultList, m_strAN_IN_MDLID_3, m_strAN_OUT_MDLID_3);
                                    if (h_value != 0)
                                    {
                                        oDataRow["SIMU_H3"] = FunctionManager.getRound(h_value, 2);
                                    }
                                    h_value = getJuncionData(resultList, m_strAN_IN_MDLID_4, m_strAN_OUT_MDLID_4);
                                    if (h_value != 0)
                                    {
                                        oDataRow["SIMU_H4"] = FunctionManager.getRound(h_value, 2);
                                    }
                                    h_value = getJuncionData(resultList, m_strAN_IN_MDLID_5, m_strAN_OUT_MDLID_5);
                                    if (h_value != 0)
                                    {
                                        oDataRow["SIMU_H5"] = FunctionManager.getRound(h_value, 2);
                                    }
                                    h_value = getJuncionData(resultList, m_strAN_IN_MDLID_6, m_strAN_OUT_MDLID_6);
                                    if (h_value != 0)
                                    {
                                        oDataRow["SIMU_H6"] = FunctionManager.getRound(h_value, 2);
                                    }
                                    //}
                                    break;
                                //case "reservior":
                                //    for (int i = 0; i < resultList.Count; i++)
                                //    {
                                //        resultData = resultList[i] as Hashtable;  //개개의 해석항목ID 해석값

                                //        if (Convert.ToString(resultData["NODE_ID"]) == m_strAN_OPR_MDLID_1)
                                //        {
                                //            oDataRow["SIMU_H1"] = FunctionManager.getRound(resultData["EN_HEAD"],2);
                                //        }
                                //        else if (Convert.ToString(resultData["NODE_ID"]) == m_strAN_OPR_MDLID_2)
                                //        {
                                //            oDataRow["SIMU_H2"] = FunctionManager.getRound(resultData["EN_HEAD"],2);
                                //        }
                                //        else if (Convert.ToString(resultData["NODE_ID"]) == m_strAN_OPR_MDLID_3)
                                //        {
                                //            oDataRow["SIMU_H3"] = FunctionManager.getRound(resultData["EN_HEAD"],2);
                                //        }
                                //        else if (Convert.ToString(resultData["NODE_ID"]) == m_strAN_OPR_MDLID_4)
                                //        {
                                //            oDataRow["SIMU_H4"] = FunctionManager.getRound(resultData["EN_HEAD"],2);
                                //        }
                                //        else if (Convert.ToString(resultData["NODE_ID"]) == m_strAN_OPR_MDLID_5)
                                //        {
                                //            oDataRow["SIMU_H5"] = FunctionManager.getRound(resultData["EN_HEAD"],2);
                                //        }
                                //        else if (Convert.ToString(resultData["NODE_ID"]) == m_strAN_OPR_MDLID_6)
                                //        {
                                //            oDataRow["SIMU_H6"] = FunctionManager.getRound(resultData["EN_HEAD"],2);
                                //        }

                                //        //Console.WriteLine(time + " : " + type + " : " + subtype + " : " + i.ToString() + " : " + resultData["NODE_ID"]
                                //        //                  + " : " + resultData["ANALYSIS_TIME"] + " : " + resultData["EN_HEAD"]);
                                //    }
                                //    break;
                                case "pump":
                                    for (int i = 0; i < resultList.Count; i++)
                                    {
                                        resultData = resultList[i] as Hashtable;
                                        if (Convert.ToString(resultData["LINK_ID"]) == m_strAN_OPR_MDLID_1)
                                        {
                                            oDataRow["SIMU_W1"] = FunctionManager.getRound(resultData["EN_ENERGY"], 2);
                                            oDataRow["SIMU_Q1"] = FunctionManager.getRound(resultData["EN_FLOW"], 2);
                                        }
                                        else if (Convert.ToString(resultData["LINK_ID"]) == m_strAN_OPR_MDLID_2)
                                        {
                                            oDataRow["SIMU_W2"] = FunctionManager.getRound(resultData["EN_ENERGY"], 2);
                                            oDataRow["SIMU_Q2"] = FunctionManager.getRound(resultData["EN_FLOW"], 2);
                                        }
                                        else if (Convert.ToString(resultData["LINK_ID"]) == m_strAN_OPR_MDLID_3)
                                        {
                                            oDataRow["SIMU_W3"] = FunctionManager.getRound(resultData["EN_ENERGY"], 2);
                                            oDataRow["SIMU_Q3"] = FunctionManager.getRound(resultData["EN_FLOW"], 2);
                                        }
                                        else if (Convert.ToString(resultData["LINK_ID"]) == m_strAN_OPR_MDLID_4)
                                        {
                                            oDataRow["SIMU_W4"] = FunctionManager.getRound(resultData["EN_ENERGY"], 2);
                                            oDataRow["SIMU_Q4"] = FunctionManager.getRound(resultData["EN_FLOW"], 2);
                                        }
                                        else if (Convert.ToString(resultData["LINK_ID"]) == m_strAN_OPR_MDLID_5)
                                        {
                                            oDataRow["SIMU_W5"] = FunctionManager.getRound(resultData["EN_ENERGY"], 2);
                                            oDataRow["SIMU_Q5"] = FunctionManager.getRound(resultData["EN_FLOW"], 2);
                                        }
                                        else if (m_strAN_OPR_MDLID_6.Equals(Convert.ToString(resultData["LINK_ID"])))
                                        {
                                            oDataRow["SIMU_W6"] = FunctionManager.getRound(resultData["EN_ENERGY"], 2);
                                            oDataRow["SIMU_Q6"] = FunctionManager.getRound(resultData["EN_FLOW"], 2);
                                        }
                                        //Console.WriteLine(time + " : " + type + " : " + subtype + " : " + i.ToString() + " : " + resultData["LINK_ID"]
                                        //                  + " : " + resultData["EN_ENERGY"] + " : " + resultData["EN_FLOW"]);
                                    }
                                    break;
                            }
                        }
                    }

                    oDataTable.Rows.Add(oDataRow);
                }

                oDataTable.AcceptChanges();
                SetMergedDataTable();
                VisibleColumnGroup(this, new EventArgs());
                splitContainer1.Panel1Collapsed = false;     //챠트부분 Visible = true
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageManager.ShowInformationMessage("에너지 모델해석 실행중 오류가 발생했습니다.");
                //throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private double getJuncionData(ArrayList resultList, string strIN_MDLID, string strOUT_MDLID)
        {
            Hashtable resultData = new Hashtable();
            double InValue = 0.0;
            double OutValue = 0.0;

            for (int i = 0; i < resultList.Count; i++)
            {
                resultData = resultList[i] as Hashtable;  //개개의 해석항목ID 해석값
                if (Convert.ToString(resultData["NODE_ID"]).Equals(strIN_MDLID))
                {
                    InValue = Convert.ToDouble(resultData["EN_HEAD"]);
                    continue;
                }
                if (Convert.ToString(resultData["NODE_ID"]).Equals(strOUT_MDLID))
                {
                    OutValue = Convert.ToDouble(resultData["EN_HEAD"]);
                    continue;
                }    
            }

            //Console.WriteLine(Convert.ToString(InValue - OutValue));
            return (OutValue - InValue);

        }
        /// <summary>
        /// Save 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //기존 데이터 삭제하고, 신규저장
            InsertProcess();
        }

        /// <summary>
        /// 모의해석 결과 저장
        /// </summary>
        /// <returns></returns>
        private void InsertProcess()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                oDBManager.BeginTransaction();

                #region 기존 모의해석결과 삭제
                oStringBuilder.AppendLine("DELETE FROM WE_PP_OPR_SIMU");
                oStringBuilder.AppendLine("WHERE PP_OPR_SIMU_TIME = '" + FunctionManager.StringToDateTime((DateTime)ultraDateTimeEditor_Day.Value) + "'");

                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                #endregion

                #region 모의해석결과 - Insert
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT INTO WE_PP_OPR_SIMU (");
                oStringBuilder.AppendLine("PP_OPR_SIMU_TIME,");
                oStringBuilder.AppendLine("FAC_SEQ,");
                oStringBuilder.AppendLine("PP_OPR_SIMU_DUR,");
                oStringBuilder.AppendLine("PP_ORG_Q,");
                oStringBuilder.AppendLine("PP_ORG_H,");
                oStringBuilder.AppendLine("PP_ORG_E,");
                oStringBuilder.AppendLine("PP_ORG_W,");
                oStringBuilder.AppendLine("PP_ORG_U,");
                oStringBuilder.AppendLine("PP_SIMU_Q,");
                oStringBuilder.AppendLine("PP_SIMU_H,");
                oStringBuilder.AppendLine("PP_SIMU_E,");
                oStringBuilder.AppendLine("PP_SIMU_W,");
                oStringBuilder.AppendLine("PP_SIMU_U,");
                oStringBuilder.AppendLine("USERID,");
                oStringBuilder.AppendLine("ENTDT)");
                oStringBuilder.AppendLine("VALUES (");
                oStringBuilder.AppendLine("  :PP_OPR_SIMU_TIME,");
                oStringBuilder.AppendLine("  :FAC_SEQ,");
                oStringBuilder.AppendLine("  :PP_OPR_SIMU_DUR,");
                oStringBuilder.AppendLine("  :PP_ORG_Q,");
                oStringBuilder.AppendLine("  :PP_ORG_H,");
                oStringBuilder.AppendLine("  :PP_ORG_E,");
                oStringBuilder.AppendLine("  :PP_ORG_W,");
                oStringBuilder.AppendLine("  :PP_ORG_U,");
                oStringBuilder.AppendLine("  :PP_SIMU_Q,");
                oStringBuilder.AppendLine("  :PP_SIMU_H,");
                oStringBuilder.AppendLine("  :PP_SIMU_E,");
                oStringBuilder.AppendLine("  :PP_SIMU_W,");
                oStringBuilder.AppendLine("  :PP_SIMU_U,");
                oStringBuilder.AppendLine("  :USERID,");
                oStringBuilder.AppendLine("  :ENTDT)");

                foreach (UltraGridColumn oColumn in m_gridManager1.Grid.DisplayLayout.Bands[0].Columns)
                {
                    if (!(oColumn.Key == "#1" | oColumn.Key == "#2" | oColumn.Key == "#3" | oColumn.Key == "#4" | oColumn.Key == "#5" | oColumn.Key == "#6")) continue;
                    //MessageBox.Show(m_strFAC_SEQ_1 + " : " + m_strFAC_SEQ_2 + " : " + m_strFAC_SEQ_3 + " : " + m_strFAC_SEQ_4 + " : " + m_strFAC_SEQ_5 + " : " + m_strFAC_SEQ_6);
                    foreach (UltraGridRow item in m_gridManager1.Grid.Rows)
                    {
                        IDataParameter[] oParams = {
                        new OracleParameter(":PP_OPR_SIMU_TIME", OracleDbType.Varchar2,16),
                        new OracleParameter(":FAC_SEQ",          OracleDbType.Varchar2,16),
                        new OracleParameter(":PP_OPR_SIMU_DUR",  OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_ORG_Q",         OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_ORG_H",         OracleDbType.Varchar2,10),        
                        new OracleParameter(":PP_ORG_E",         OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_ORG_W",         OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_ORG_U",         OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_SIMU_Q",        OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_SIMU_H",        OracleDbType.Varchar2,10),  
                        new OracleParameter(":PP_SIMU_E",        OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_SIMU_W",        OracleDbType.Varchar2,10),
                        new OracleParameter(":PP_SIMU_U",        OracleDbType.Varchar2,10),
                        new OracleParameter(":USERID",           OracleDbType.Varchar2,50),
                        new OracleParameter(":ENTDT",            OracleDbType.Varchar2,16)
                        };
                        
                        if (oColumn.Key == "#1")
                        {
                            oParams[0].Value = FunctionManager.getRemovedChar(Convert.ToString(item.Cells["DD"].Value), '-') + FunctionManager.getRemovedChar(Convert.ToString(item.Cells["HM"].Value), ':') + "00";
                            if (string.IsNullOrEmpty(m_strFAC_SEQ_1)) continue;
                            oParams[1].Value = m_strFAC_SEQ_1;
                            oParams[2].Value = Convert.IsDBNull(item.Cells["#1"].Value) ? "0" : Convert.ToString(item.Cells["#1"].Value);
                            oParams[3].Value = Convert.IsDBNull(item.Cells["ORG_Q1"].Value) ? "0" : Convert.ToString(item.Cells["ORG_Q1"].Value);
                            oParams[4].Value = Convert.IsDBNull(item.Cells["ORG_H1"].Value) ? "0" : Convert.ToString(item.Cells["ORG_H1"].Value);
                            oParams[5].Value = Convert.IsDBNull(item.Cells["ORG_E1"].Value) ? "0" : Convert.ToString(item.Cells["ORG_E1"].Value);
                            oParams[6].Value = Convert.IsDBNull(item.Cells["ORG_W1"].Value) ? "0" : Convert.ToString(item.Cells["ORG_W1"].Value);
                            oParams[7].Value = Convert.IsDBNull(item.Cells["ORG_U1"].Value) ? "0" : Convert.ToString(item.Cells["ORG_U1"].Value);
                            oParams[8].Value = Convert.IsDBNull(item.Cells["SIMU_Q1"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_Q1"].Value);
                            oParams[9].Value = Convert.IsDBNull(item.Cells["SIMU_H1"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_H1"].Value);
                            oParams[10].Value = Convert.IsDBNull(item.Cells["SIMU_E1"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_E1"].Value);
                            oParams[11].Value = Convert.IsDBNull(item.Cells["SIMU_W1"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_W1"].Value);
                            oParams[12].Value = Convert.IsDBNull(item.Cells["SIMU_U1"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_U1"].Value);
                            oParams[13].Value = FunctionManager.GetUserID();    // "USERID";       //등록자
                            oParams[14].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                        }
                        else if (oColumn.Key == "#2")
                        {
                            oParams[0].Value = FunctionManager.getRemovedChar(Convert.ToString(item.Cells["DD"].Value), '-') + FunctionManager.getRemovedChar(Convert.ToString(item.Cells["HM"].Value), ':') + "00";
                            if (string.IsNullOrEmpty(m_strFAC_SEQ_2)) continue;
                            oParams[1].Value = m_strFAC_SEQ_2;
                            oParams[2].Value = Convert.IsDBNull(item.Cells["#2"].Value) ? "0" : Convert.ToString(item.Cells["#2"].Value);
                            oParams[3].Value = Convert.IsDBNull(item.Cells["ORG_Q2"].Value) ? "0" : Convert.ToString(item.Cells["ORG_Q2"].Value);
                            oParams[4].Value = Convert.IsDBNull(item.Cells["ORG_H2"].Value) ? "0" : Convert.ToString(item.Cells["ORG_H2"].Value);
                            oParams[5].Value = Convert.IsDBNull(item.Cells["ORG_E2"].Value) ? "0" : Convert.ToString(item.Cells["ORG_E2"].Value);
                            oParams[6].Value = Convert.IsDBNull(item.Cells["ORG_W2"].Value) ? "0" : Convert.ToString(item.Cells["ORG_W2"].Value);
                            oParams[7].Value = Convert.IsDBNull(item.Cells["ORG_U2"].Value) ? "0" : Convert.ToString(item.Cells["ORG_U2"].Value);
                            oParams[8].Value = Convert.IsDBNull(item.Cells["SIMU_Q2"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_Q2"].Value);
                            oParams[9].Value = Convert.IsDBNull(item.Cells["SIMU_H2"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_H2"].Value);
                            oParams[10].Value = Convert.IsDBNull(item.Cells["SIMU_E2"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_E2"].Value);
                            oParams[11].Value = Convert.IsDBNull(item.Cells["SIMU_W2"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_W2"].Value);
                            oParams[12].Value = Convert.IsDBNull(item.Cells["SIMU_U2"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_U2"].Value);
                            oParams[13].Value = FunctionManager.GetUserID();    // "USERID";       //등록자
                            oParams[14].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                        }
                        else if (oColumn.Key == "#3")
                        {
                            oParams[0].Value = FunctionManager.getRemovedChar(Convert.ToString(item.Cells["DD"].Value), '-') + FunctionManager.getRemovedChar(Convert.ToString(item.Cells["HM"].Value), ':') + "00";
                            if (string.IsNullOrEmpty(m_strFAC_SEQ_3)) continue;
                            oParams[1].Value = m_strFAC_SEQ_3;
                            oParams[2].Value = Convert.IsDBNull(item.Cells["#3"].Value) ? "0" : Convert.ToString(item.Cells["#3"].Value);
                            oParams[3].Value = Convert.IsDBNull(item.Cells["ORG_Q3"].Value) ? "0" : Convert.ToString(item.Cells["ORG_Q3"].Value);
                            oParams[4].Value = Convert.IsDBNull(item.Cells["ORG_H3"].Value) ? "0" : Convert.ToString(item.Cells["ORG_H3"].Value);
                            oParams[5].Value = Convert.IsDBNull(item.Cells["ORG_E3"].Value) ? "0" : Convert.ToString(item.Cells["ORG_E3"].Value);
                            oParams[6].Value = Convert.IsDBNull(item.Cells["ORG_W3"].Value) ? "0" : Convert.ToString(item.Cells["ORG_W3"].Value);
                            oParams[7].Value = Convert.IsDBNull(item.Cells["ORG_U3"].Value) ? "0" : Convert.ToString(item.Cells["ORG_U3"].Value);
                            oParams[8].Value = Convert.IsDBNull(item.Cells["SIMU_Q3"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_Q3"].Value);
                            oParams[9].Value = Convert.IsDBNull(item.Cells["SIMU_H3"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_H3"].Value);
                            oParams[10].Value = Convert.IsDBNull(item.Cells["SIMU_E3"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_E3"].Value);
                            oParams[11].Value = Convert.IsDBNull(item.Cells["SIMU_W3"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_W3"].Value);
                            oParams[12].Value = Convert.IsDBNull(item.Cells["SIMU_U3"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_U3"].Value);
                            oParams[13].Value = FunctionManager.GetUserID();    // "USERID";       //등록자
                            oParams[14].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                        }
                        else if (oColumn.Key == "#4")
                        {
                            oParams[0].Value = FunctionManager.getRemovedChar(Convert.ToString(item.Cells["DD"].Value), '-') + FunctionManager.getRemovedChar(Convert.ToString(item.Cells["HM"].Value), ':') + "00";
                            if (string.IsNullOrEmpty(m_strFAC_SEQ_4)) continue;
                            oParams[1].Value = m_strFAC_SEQ_4;
                            oParams[2].Value = Convert.IsDBNull(item.Cells["#4"].Value) ? "0" : Convert.ToString(item.Cells["#4"].Value);
                            oParams[3].Value = Convert.IsDBNull(item.Cells["ORG_Q4"].Value) ? "0" : Convert.ToString(item.Cells["ORG_Q4"].Value);
                            oParams[4].Value = Convert.IsDBNull(item.Cells["ORG_H4"].Value) ? "0" : Convert.ToString(item.Cells["ORG_H4"].Value);
                            oParams[5].Value = Convert.IsDBNull(item.Cells["ORG_E4"].Value) ? "0" : Convert.ToString(item.Cells["ORG_E4"].Value);
                            oParams[6].Value = Convert.IsDBNull(item.Cells["ORG_W4"].Value) ? "0" : Convert.ToString(item.Cells["ORG_W4"].Value);
                            oParams[7].Value = Convert.IsDBNull(item.Cells["ORG_U4"].Value) ? "0" : Convert.ToString(item.Cells["ORG_U4"].Value);
                            oParams[8].Value = Convert.IsDBNull(item.Cells["SIMU_Q4"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_Q4"].Value);
                            oParams[9].Value = Convert.IsDBNull(item.Cells["SIMU_H4"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_H4"].Value);
                            oParams[10].Value = Convert.IsDBNull(item.Cells["SIMU_E4"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_E4"].Value);
                            oParams[11].Value = Convert.IsDBNull(item.Cells["SIMU_W4"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_W4"].Value);
                            oParams[12].Value = Convert.IsDBNull(item.Cells["SIMU_U4"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_U4"].Value);
                            oParams[13].Value = FunctionManager.GetUserID();    // "USERID";       //등록자
                            oParams[14].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                        }
                        else if (oColumn.Key == "#5")
                        {
                            oParams[0].Value = FunctionManager.getRemovedChar(Convert.ToString(item.Cells["DD"].Value), '-') + FunctionManager.getRemovedChar(Convert.ToString(item.Cells["HM"].Value), ':') + "00";
                            if (string.IsNullOrEmpty(m_strFAC_SEQ_5)) continue;
                            oParams[1].Value = m_strFAC_SEQ_5;
                            oParams[2].Value = Convert.IsDBNull(item.Cells["#5"].Value) ? "0" : Convert.ToString(item.Cells["#5"].Value);
                            oParams[3].Value = Convert.IsDBNull(item.Cells["ORG_Q5"].Value) ? "0" : Convert.ToString(item.Cells["ORG_Q5"].Value);
                            oParams[4].Value = Convert.IsDBNull(item.Cells["ORG_H5"].Value) ? "0" : Convert.ToString(item.Cells["ORG_H5"].Value);
                            oParams[5].Value = Convert.IsDBNull(item.Cells["ORG_E5"].Value) ? "0" : Convert.ToString(item.Cells["ORG_E5"].Value);
                            oParams[6].Value = Convert.IsDBNull(item.Cells["ORG_W5"].Value) ? "0" : Convert.ToString(item.Cells["ORG_W5"].Value);
                            oParams[7].Value = Convert.IsDBNull(item.Cells["ORG_U5"].Value) ? "0" : Convert.ToString(item.Cells["ORG_U5"].Value);
                            oParams[8].Value = Convert.IsDBNull(item.Cells["SIMU_Q5"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_Q5"].Value);
                            oParams[9].Value = Convert.IsDBNull(item.Cells["SIMU_H5"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_H5"].Value);
                            oParams[10].Value = Convert.IsDBNull(item.Cells["SIMU_E5"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_E5"].Value);
                            oParams[11].Value = Convert.IsDBNull(item.Cells["SIMU_W5"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_W5"].Value);
                            oParams[12].Value = Convert.IsDBNull(item.Cells["SIMU_U5"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_U5"].Value);
                            oParams[13].Value = FunctionManager.GetUserID();    // "USERID";       //등록자
                            oParams[14].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                        }
                        else if (oColumn.Key == "#6")
                        {
                            oParams[0].Value = FunctionManager.getRemovedChar(Convert.ToString(item.Cells["DD"].Value), '-') + FunctionManager.getRemovedChar(Convert.ToString(item.Cells["HM"].Value), ':') + "00";
                            if (string.IsNullOrEmpty(m_strFAC_SEQ_6)) continue;
                            oParams[1].Value = m_strFAC_SEQ_6;
                            oParams[2].Value = Convert.IsDBNull(item.Cells["#6"].Value) ? "0" : Convert.ToString(item.Cells["#6"].Value);
                            oParams[3].Value = Convert.IsDBNull(item.Cells["ORG_Q6"].Value) ? "0" : Convert.ToString(item.Cells["ORG_Q6"].Value);
                            oParams[4].Value = Convert.IsDBNull(item.Cells["ORG_H6"].Value) ? "0" : Convert.ToString(item.Cells["ORG_H6"].Value);
                            oParams[5].Value = Convert.IsDBNull(item.Cells["ORG_E6"].Value) ? "0" : Convert.ToString(item.Cells["ORG_E6"].Value);
                            oParams[6].Value = Convert.IsDBNull(item.Cells["ORG_W6"].Value) ? "0" : Convert.ToString(item.Cells["ORG_W6"].Value);
                            oParams[7].Value = Convert.IsDBNull(item.Cells["ORG_U6"].Value) ? "0" : Convert.ToString(item.Cells["ORG_U6"].Value);
                            oParams[8].Value = Convert.IsDBNull(item.Cells["SIMU_Q6"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_Q6"].Value);
                            oParams[9].Value = Convert.IsDBNull(item.Cells["SIMU_H6"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_H6"].Value);
                            oParams[10].Value = Convert.IsDBNull(item.Cells["SIMU_E6"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_E6"].Value);
                            oParams[11].Value = Convert.IsDBNull(item.Cells["SIMU_W6"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_W6"].Value);
                            oParams[12].Value = Convert.IsDBNull(item.Cells["SIMU_U6"].Value) ? "0" : Convert.ToString(item.Cells["SIMU_U6"].Value);
                            oParams[13].Value = FunctionManager.GetUserID();    // "USERID";       //등록자
                            oParams[14].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                        }
                        else continue;

                    }
                }
                #endregion

                oDBManager.CommitTransaction();

                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                throw oException;
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
            
        }
        private void ultraGrid_WE_PP_MODEL_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //이걸 안하면, Copy & Paste 안되욤
            e.Layout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
        }

        private void comboBox_BIZ_PLA_NM_SelectedIndexChanged(object sender, EventArgs e)
        {
            //setMemberVariable();
        }

        
        #region Formula 계산
        private void SetFormula()
        {

        }

        #endregion Formula 계산

        /// <summary>
        /// 항목보기 체크박스 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VisibleColumnGroup(object sender, EventArgs e)
        {
            m_gridManager1.VisibleGridGroup("ORG_W", !checkBox_W.Checked);
            m_gridManager1.VisibleGridGroup("SIMU_W", !checkBox_W.Checked);
            m_gridManager1.VisibleGridGroup("ORG_Q", !checkBox_Q.Checked);
            m_gridManager1.VisibleGridGroup("SIMU_Q", !checkBox_Q.Checked);
            m_gridManager1.VisibleGridGroup("ORG_H", !checkBox_H.Checked);
            m_gridManager1.VisibleGridGroup("SIMU_H", !checkBox_H.Checked);
            m_gridManager1.VisibleGridGroup("ORG_E", !checkBox_E.Checked);
            m_gridManager1.VisibleGridGroup("SIMU_E", !checkBox_E.Checked);
            m_gridManager1.VisibleGridGroup("ORG_U", !checkBox_U.Checked);
            m_gridManager1.VisibleGridGroup("SIMU_U", !checkBox_U.Checked);
        }

        /// <summary>
        /// 설정된 펌프에 대해서 챠트를 보여준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChartDrawVisible(object sender, EventArgs e)
        {
            switch (Convert.ToString(comboBox_ORG.SelectedValue))
            {
                case "1호기":
                    for (int i = 0; i < chart_ORG.Series.Count; i++)
                    {
                        if (i >= 0 && i < 5)
                        {
                            chart_ORG.Series[i].Visible = true;
                            chart_SIMU.Series[i].Visible = true;
                        }
                        else
                        {
                            chart_ORG.Series[i].Visible = false;
                            chart_SIMU.Series[i].Visible = false;
                        }
                    }
                    break;
                case "2호기":
                    for (int i = 0; i < chart_ORG.Series.Count; i++)
                    {
                        if (i >= 5 && i < 10) {
                            chart_ORG.Series[i].Visible = true;
                            chart_SIMU.Series[i].Visible = true;
                        }
                        else
                        {
                            chart_ORG.Series[i].Visible = false;
                            chart_SIMU.Series[i].Visible = false;
                        }
                    }
                    break;
                case "3호기":
                    for (int i = 0; i < chart_ORG.Series.Count; i++)
                    {
                        if (i >= 10 && i < 15)
                        {
                            chart_ORG.Series[i].Visible = true;
                            chart_SIMU.Series[i].Visible = true;
                        }
                        else
                        {
                            chart_ORG.Series[i].Visible = false;
                            chart_SIMU.Series[i].Visible = false;
                        }
                    }
                    break;
                case "4호기":
                    for (int i = 0; i < chart_ORG.Series.Count; i++)
                    {
                        if (i >= 15 && i < 20)
                        {
                            chart_ORG.Series[i].Visible = true;
                            chart_SIMU.Series[i].Visible = true;
                        }
                        else
                        {
                            chart_ORG.Series[i].Visible = false;
                            chart_SIMU.Series[i].Visible = false;
                        }
                    }
                    break;
                case "5호기":
                    for (int i = 0; i < chart_ORG.Series.Count; i++)
                    {
                        if (i >= 20 && i < 25)
                        {
                            chart_ORG.Series[i].Visible = true;
                            chart_SIMU.Series[i].Visible = true;
                        }
                        else
                        {
                            chart_ORG.Series[i].Visible = false;
                            chart_SIMU.Series[i].Visible = false;
                        }
                    }
                    break;
                case "6호기":
                    for (int i = 0; i < chart_ORG.Series.Count; i++)
                    {
                        if (i >= 25 && i < 30)
                        {
                            chart_ORG.Series[i].Visible = true;
                            chart_SIMU.Series[i].Visible = true;
                        }
                        else
                        {
                            chart_ORG.Series[i].Visible = false;
                            chart_SIMU.Series[i].Visible = false;
                        }
                    }
                    break;
            }

            this.chart_ORG.Update();
            this.chart_SIMU.Update();

        }


        private void checkBox_W_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void comboBox_ORG_SelectedValueChanged(object sender, EventArgs e)
        {

        }


    }
}
