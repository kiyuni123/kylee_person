﻿namespace WaterNet.WE_EnergyAnalysis
{
    partial class frmPumpSimuConditionSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.cboModel = new System.Windows.Forms.ComboBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.panelWaterLevel = new System.Windows.Forms.Panel();
            this.ultraDateTimeEditor_Day = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnBIZRefresh = new System.Windows.Forms.Button();
            this.ultraGrid_WaterLevel = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraCalcManager1 = new Infragistics.Win.UltraWinCalcManager.UltraCalcManager(this.components);
            this.lblWaterLevel = new System.Windows.Forms.Label();
            this.panelPumpPropty = new System.Windows.Forms.Panel();
            this.btnCurveInfo = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.ultraGrid_PumpPropty = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lblPumpPropty = new System.Windows.Forms.Label();
            this.panelFreq = new System.Windows.Forms.Panel();
            this.ultraGrid_Rule = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txt_Rule = new System.Windows.Forms.RichTextBox();
            this.lblFreq = new System.Windows.Forms.Label();
            this.panelCommand.SuspendLayout();
            this.panelWaterLevel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_Day)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WaterLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).BeginInit();
            this.panelPumpPropty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_PumpPropty)).BeginInit();
            this.panelFreq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Rule)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.cboModel);
            this.panelCommand.Controls.Add(this.btnRun);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(471, 47);
            this.panelCommand.TabIndex = 17;
            // 
            // cboModel
            // 
            this.cboModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboModel.FormattingEnabled = true;
            this.cboModel.Location = new System.Drawing.Point(9, 15);
            this.cboModel.Name = "cboModel";
            this.cboModel.Size = new System.Drawing.Size(286, 20);
            this.cboModel.TabIndex = 38;
            this.cboModel.Tag = "";
            this.cboModel.SelectedIndexChanged += new System.EventHandler(this.cboModel_SelectedIndexChanged);
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Location = new System.Drawing.Point(384, 3);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(81, 40);
            this.btnRun.TabIndex = 5;
            this.btnRun.Text = "시뮬레이션";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // panelWaterLevel
            // 
            this.panelWaterLevel.Controls.Add(this.ultraDateTimeEditor_Day);
            this.panelWaterLevel.Controls.Add(this.btnBIZRefresh);
            this.panelWaterLevel.Controls.Add(this.ultraGrid_WaterLevel);
            this.panelWaterLevel.Controls.Add(this.lblWaterLevel);
            this.panelWaterLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelWaterLevel.Location = new System.Drawing.Point(0, 47);
            this.panelWaterLevel.Name = "panelWaterLevel";
            this.panelWaterLevel.Size = new System.Drawing.Size(471, 105);
            this.panelWaterLevel.TabIndex = 18;
            // 
            // ultraDateTimeEditor_Day
            // 
            this.ultraDateTimeEditor_Day.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraDateTimeEditor_Day.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_Day.AutoSize = false;
            this.ultraDateTimeEditor_Day.Location = new System.Drawing.Point(289, 2);
            this.ultraDateTimeEditor_Day.Name = "ultraDateTimeEditor_Day";
            this.ultraDateTimeEditor_Day.Size = new System.Drawing.Size(102, 21);
            this.ultraDateTimeEditor_Day.TabIndex = 47;
            this.ultraDateTimeEditor_Day.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor_Day_ValueChanged);
            // 
            // btnBIZRefresh
            // 
            this.btnBIZRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBIZRefresh.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnBIZRefresh.Location = new System.Drawing.Point(397, 3);
            this.btnBIZRefresh.Name = "btnBIZRefresh";
            this.btnBIZRefresh.Size = new System.Drawing.Size(71, 20);
            this.btnBIZRefresh.TabIndex = 48;
            this.btnBIZRefresh.Text = "초기화";
            this.btnBIZRefresh.UseVisualStyleBackColor = true;
            this.btnBIZRefresh.Click += new System.EventHandler(this.btnBIZRefresh_Click);
            // 
            // ultraGrid_WaterLevel
            // 
            this.ultraGrid_WaterLevel.CalcManager = this.ultraCalcManager1;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_WaterLevel.DisplayLayout.Appearance = appearance15;
            this.ultraGrid_WaterLevel.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_WaterLevel.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_WaterLevel.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_WaterLevel.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.CardAreaAppearance = appearance16;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.CellAppearance = appearance17;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.CellPadding = 0;
            appearance18.BackColor = System.Drawing.SystemColors.Control;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.GroupByRowAppearance = appearance18;
            appearance19.TextHAlignAsString = "Left";
            this.ultraGrid_WaterLevel.DisplayLayout.Override.HeaderAppearance = appearance19;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.RowAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_WaterLevel.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.ultraGrid_WaterLevel.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_WaterLevel.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_WaterLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_WaterLevel.Location = new System.Drawing.Point(0, 25);
            this.ultraGrid_WaterLevel.Name = "ultraGrid_WaterLevel";
            this.ultraGrid_WaterLevel.Size = new System.Drawing.Size(471, 80);
            this.ultraGrid_WaterLevel.TabIndex = 46;
            // 
            // ultraCalcManager1
            // 
            this.ultraCalcManager1.ContainingControl = this;
            // 
            // lblWaterLevel
            // 
            this.lblWaterLevel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblWaterLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblWaterLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblWaterLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblWaterLevel.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblWaterLevel.Location = new System.Drawing.Point(0, 0);
            this.lblWaterLevel.Name = "lblWaterLevel";
            this.lblWaterLevel.Size = new System.Drawing.Size(471, 25);
            this.lblWaterLevel.TabIndex = 43;
            this.lblWaterLevel.Text = "■ 수위조건";
            this.lblWaterLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelPumpPropty
            // 
            this.panelPumpPropty.Controls.Add(this.btnCurveInfo);
            this.panelPumpPropty.Controls.Add(this.btnRefresh);
            this.panelPumpPropty.Controls.Add(this.ultraGrid_PumpPropty);
            this.panelPumpPropty.Controls.Add(this.lblPumpPropty);
            this.panelPumpPropty.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPumpPropty.Location = new System.Drawing.Point(0, 152);
            this.panelPumpPropty.Name = "panelPumpPropty";
            this.panelPumpPropty.Size = new System.Drawing.Size(471, 160);
            this.panelPumpPropty.TabIndex = 19;
            // 
            // btnCurveInfo
            // 
            this.btnCurveInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCurveInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCurveInfo.Location = new System.Drawing.Point(320, 3);
            this.btnCurveInfo.Name = "btnCurveInfo";
            this.btnCurveInfo.Size = new System.Drawing.Size(71, 20);
            this.btnCurveInfo.TabIndex = 48;
            this.btnCurveInfo.Text = "커브정보";
            this.btnCurveInfo.UseVisualStyleBackColor = true;
            this.btnCurveInfo.Click += new System.EventHandler(this.btnCurveInfo_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRefresh.Location = new System.Drawing.Point(397, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(71, 20);
            this.btnRefresh.TabIndex = 47;
            this.btnRefresh.Text = "초기화";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // ultraGrid_PumpPropty
            // 
            this.ultraGrid_PumpPropty.CalcManager = this.ultraCalcManager1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_PumpPropty.DisplayLayout.Appearance = appearance8;
            this.ultraGrid_PumpPropty.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_PumpPropty.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_PumpPropty.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_PumpPropty.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.CellAppearance = appearance10;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.ultraGrid_PumpPropty.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.RowAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_PumpPropty.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.ultraGrid_PumpPropty.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_PumpPropty.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_PumpPropty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_PumpPropty.Location = new System.Drawing.Point(0, 25);
            this.ultraGrid_PumpPropty.Name = "ultraGrid_PumpPropty";
            this.ultraGrid_PumpPropty.Size = new System.Drawing.Size(471, 135);
            this.ultraGrid_PumpPropty.TabIndex = 46;
            // 
            // lblPumpPropty
            // 
            this.lblPumpPropty.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPumpPropty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPumpPropty.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPumpPropty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblPumpPropty.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPumpPropty.Location = new System.Drawing.Point(0, 0);
            this.lblPumpPropty.Name = "lblPumpPropty";
            this.lblPumpPropty.Size = new System.Drawing.Size(471, 25);
            this.lblPumpPropty.TabIndex = 44;
            this.lblPumpPropty.Text = "■ 펌프특성조건";
            this.lblPumpPropty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelFreq
            // 
            this.panelFreq.Controls.Add(this.ultraGrid_Rule);
            this.panelFreq.Controls.Add(this.txt_Rule);
            this.panelFreq.Controls.Add(this.lblFreq);
            this.panelFreq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFreq.Location = new System.Drawing.Point(0, 312);
            this.panelFreq.Name = "panelFreq";
            this.panelFreq.Size = new System.Drawing.Size(471, 240);
            this.panelFreq.TabIndex = 23;
            // 
            // ultraGrid_Rule
            // 
            this.ultraGrid_Rule.CalcManager = this.ultraCalcManager1;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Rule.DisplayLayout.Appearance = appearance1;
            this.ultraGrid_Rule.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Rule.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_Rule.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Rule.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_Rule.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Rule.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Rule.DisplayLayout.Override.CardAreaAppearance = appearance2;
            appearance3.BorderColor = System.Drawing.Color.Silver;
            appearance3.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Rule.DisplayLayout.Override.CellAppearance = appearance3;
            this.ultraGrid_Rule.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Rule.DisplayLayout.Override.CellPadding = 0;
            appearance4.BackColor = System.Drawing.SystemColors.Control;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Rule.DisplayLayout.Override.GroupByRowAppearance = appearance4;
            appearance5.TextHAlignAsString = "Left";
            this.ultraGrid_Rule.DisplayLayout.Override.HeaderAppearance = appearance5;
            this.ultraGrid_Rule.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
            this.ultraGrid_Rule.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Rule.DisplayLayout.Override.RowAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Rule.DisplayLayout.Override.TemplateAddRowAppearance = appearance7;
            this.ultraGrid_Rule.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Rule.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Rule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Rule.Location = new System.Drawing.Point(0, 25);
            this.ultraGrid_Rule.Name = "ultraGrid_Rule";
            this.ultraGrid_Rule.Size = new System.Drawing.Size(471, 215);
            this.ultraGrid_Rule.TabIndex = 47;
            // 
            // txt_Rule
            // 
            this.txt_Rule.Location = new System.Drawing.Point(0, 25);
            this.txt_Rule.Name = "txt_Rule";
            this.txt_Rule.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txt_Rule.Size = new System.Drawing.Size(150, 62);
            this.txt_Rule.TabIndex = 44;
            this.txt_Rule.Text = "";
            // 
            // lblFreq
            // 
            this.lblFreq.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFreq.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblFreq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFreq.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFreq.Location = new System.Drawing.Point(0, 0);
            this.lblFreq.Name = "lblFreq";
            this.lblFreq.Size = new System.Drawing.Size(471, 25);
            this.lblFreq.TabIndex = 43;
            this.lblFreq.Text = "■ Rule 설정";
            this.lblFreq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmPumpSimuConditionSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 552);
            this.Controls.Add(this.panelFreq);
            this.Controls.Add(this.panelPumpPropty);
            this.Controls.Add(this.panelWaterLevel);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmPumpSimuConditionSet";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "에너지모의 조건설정";
            this.Load += new System.EventHandler(this.frmPumpSimuConditionSet_Load);
            this.panelCommand.ResumeLayout(false);
            this.panelWaterLevel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_Day)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WaterLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).EndInit();
            this.panelPumpPropty.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_PumpPropty)).EndInit();
            this.panelFreq.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Rule)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Panel panelWaterLevel;
        private System.Windows.Forms.Panel panelPumpPropty;
        private System.Windows.Forms.Label lblWaterLevel;
        private System.Windows.Forms.Label lblPumpPropty;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_WaterLevel;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_PumpPropty;
        private System.Windows.Forms.ComboBox cboModel;
        private System.Windows.Forms.Button btnRefresh;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_Day;
        private Infragistics.Win.UltraWinCalcManager.UltraCalcManager ultraCalcManager1;
        private System.Windows.Forms.Panel panelFreq;
        private System.Windows.Forms.RichTextBox txt_Rule;
        private System.Windows.Forms.Label lblFreq;
        private System.Windows.Forms.Button btnBIZRefresh;
        private System.Windows.Forms.Button btnCurveInfo;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Rule;
    }
}