﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WH_PipingNetworkAnalysis.epanet;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WE_EnergyAnalysis
{
    public partial class frmPumpSimulation2 : Form, WaterNet.WaterNetCore.IForminterface
    {
        public frmPumpSimulation2()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "펌프스케쥴"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        /// <summary>
        /// 지도 및 툴바 초기설정
        /// </summary>
        private void InitializeSetting()
        {
            if (ArcManager.CheckOutLicenses(ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine) != ESRI.ArcGIS.esriSystem.esriLicenseStatus.esriLicenseAvailable)
            {
                WaterNetCore.MessageManager.ShowExclamationMessage("ArcEngine Runtime License가 없습니다.");
                return;
            }

            axToolbar1.CustomProperty = this;
            //Add map navigation commands
            axToolbar1.AddItem("esriControls.ControlsMapZoomInTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsMapZoomOutTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsMapPanTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsMapFullExtentCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsMapZoomToLastExtentBackCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsMapZoomToLastExtentForwardCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsLayerListToolControl", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsSelectTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsSelectFeaturesTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsSelectAllCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsClearSelectionCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsMapZoomToolControl", 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar1.AddItem("esriControls.ControlsMapIdentifyTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsMapFindCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar1.AddItem("esriControls.ControlsMapMeasureTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar1.SetBuddyControl(axMap1);
            axMap1.SpatialReference = ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

            axToolbar2.CustomProperty = this;
            //Add map navigation commands
            axToolbar2.AddItem("esriControls.ControlsMapZoomInTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsMapZoomOutTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsMapPanTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsMapFullExtentCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsMapZoomToLastExtentBackCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsMapZoomToLastExtentForwardCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsLayerListToolControl", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsSelectTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsSelectFeaturesTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsSelectAllCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsClearSelectionCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsMapZoomToolControl", 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar2.AddItem("esriControls.ControlsMapIdentifyTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsMapFindCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar2.AddItem("esriControls.ControlsMapMeasureTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar2.SetBuddyControl(axMap2);
            axMap2.SpatialReference = axMap1.SpatialReference;

        }

        /// <summary>
        /// Open
        /// </summary>
        public void Open()
        {
            IMapCache pMapCache = axMap1.ActiveView.FocusMap as IMapCache;
            pMapCache.AutoCacheActive = true;
            pMapCache.MaxScale = 3000;
            pMapCache.ScaleLimit = true;
            pMapCache.RefreshAutoCache();

            IMapCache pMapCache2 = axMap2.ActiveView.FocusMap as IMapCache;
            pMapCache2.AutoCacheActive = true;
            pMapCache2.MaxScale = 3000;
            pMapCache2.ScaleLimit = true;
            pMapCache2.RefreshAutoCache();
        }

        private void btnSelModel_Click(object sender, EventArgs e)
        {
            //해석모델 선택화면
            WE_Common.frmINP_Title oform = new WE_Common.frmINP_Title("EN");
            oform.Open();
            if (oform.ShowDialog() == DialogResult.Cancel) return;

            //해석모델(INP_NUMBER)번호 멤버변수 저장
            WE_Common.WE_VariableManager.m_INP_NUMBER = oform.m_INP_NUMBER;

            //해당 모델의 Workspace가 없을경우
            IWorkspace pWorkspace = WaterAOCore.ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + WE_Common.WE_VariableManager.m_INP_NUMBER);
            if (pWorkspace == null)
            {
                //해당 모델의 Shape를 생성
                CreateINPLayerManager oInpLayerManager = new CreateINPLayerManager(WE_Common.WE_VariableManager.m_INP_NUMBER);
                try
                {
                    pWorkspace = oInpLayerManager.Worksapce;
                    oInpLayerManager.StartEditor();
                    oInpLayerManager.CreateINP_Shape();
                    oInpLayerManager.StopEditor(true);
                }
                catch (Exception)
                {
                    oInpLayerManager.AbortEditor();
                    oInpLayerManager.StopEditor(false);
                }
            }

            //기존의 Shape을 Map에서 Unload
            Remove_INP_Layer(axMap1);

            //해석모델((INP_NUMBER)번호에 해당하는 Shape로드
            Load_INP_Layer(pWorkspace, axMap1);

            Remove_INP_Layer(axMap2);

            IWorkspace memWorkspace = WaterAOCore.ArcManager.CreateInMemoryWorkspace("MemWorkspace");

            for (int i = 0; i < axMap1.LayerCount; i++)
			{
			    ILayer pLayer = axMap1.get_Layer(i);

                ILayer pLayer2 = WE_Common.WE_FunctionManager.Copy(memWorkspace, pLayer);
			}

            Load_INP_Layer(memWorkspace, axMap2);

        }

        /// <summary>
        /// 기존의 INP 레이어를 지도화면에서 삭제한다.
        /// </summary>
        private void Remove_INP_Layer(AxMapControl map)
        {
            for (int i = 0; i < WE_Common.WE_VariableManager.m_shapes.Length; i++)
            {
                ArcManager.RemoveMapLayer((IMapControl3)map.Object, (string)WE_Common.WE_VariableManager.m_shapes.GetValue(i));
            }
        }

        /// <summary>
        /// INP Shape을 지도화면에 로드한다.
        /// </summary>
        private void Load_INP_Layer(IWorkspace pWorkspace, AxMapControl map)
        {
            if (pWorkspace == null)
            {
                WaterNetCore.MessageManager.ShowExclamationMessage(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + WE_Common.WE_VariableManager.m_INP_NUMBER + " 경로에 Workspace가 없습니다.");
                return;
            }
          
            ILayer pLayer = null; IColor pLabelColor = null; IColor pOutColor = null; IColor pColor = null;
            ISymbol pLineSymbol = null; ISymbol pFillSymbol = null; ISymbol pMarkSymbol = null;

            for (int i = 0; i < WE_Common.WE_VariableManager.m_shapes.Length; i++)
            {
                pLayer = ArcManager.GetShapeLayer(pWorkspace, (string)WE_Common.WE_VariableManager.m_shapes.GetValue(i), (string)WE_Common.WE_VariableManager.m_shapes.GetValue(i));
                if (pLayer == null) continue;

                switch (pLayer.Name)
                {
                    case "TANK":
                        WE_Common.WE_VariableManager.m_INP_TankLayer = pLayer as IFeatureLayer;
                        break;
                    case "RESERVOIR":
                        WE_Common.WE_VariableManager.m_INP_ReservoirLayer = pLayer as IFeatureLayer;
                        pColor = WaterNet.WaterAOCore.ArcManager.GetColor(0, 0, 100);
                        pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(8, esriSimpleMarkerStyle.esriSMSCircle, pColor);
                        WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pMarkSymbol as ISymbol);
                        break;
                    case "JUNCTION":
                        WE_Common.WE_VariableManager.m_INP_JunctionLayer = pLayer as IFeatureLayer;
                        //SetPressureLabelProperty((IFeatureLayer)pLayer);
                        break;
                    case "PIPE":
                        ///차단밸브를 찾기위해 PIPE레이어를 멤버변수에 저장
                        WE_Common.WE_VariableManager.m_INP_PipeLayer = pLayer as IFeatureLayer;
                        break;
                    case "PUMP":
                        break;
                    case "VALVE":
                        WE_Common.WE_VariableManager.m_INP_ValveLayer = pLayer as IFeatureLayer;
                        break;
                }

                map.AddLayer(pLayer);
            }
        }

        private void axMap2_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            {
                axMap1.Extent = axMap2.Extent;
                axMap1.Refresh(esriViewDrawPhase.esriViewForeground, null, null);
            }
        }

        private void axMap1_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            //esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            //if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            //{
            //    axMap2.Refresh(esriViewDrawPhase.esriViewForeground, null, null);
            //}
        }

    }
}
