﻿namespace WaterNet.WE_EnergyAnalysis
{
    partial class frmPumpScheduleSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.SeriesAttributes seriesAttributes1 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes2 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes3 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes4 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes5 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes6 = new ChartFX.WinForms.SeriesAttributes();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.ultraDateTimeEditor_Day = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.comboBox_BIZ_PLA_NM = new System.Windows.Forms.ComboBox();
            this.label_BIZ_PLA_NM = new System.Windows.Forms.Label();
            this.label_PP_HOGI = new System.Windows.Forms.Label();
            this.btnSel = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_PP_Schedule = new System.Windows.Forms.TabPage();
            this.panel_Content1 = new System.Windows.Forms.Panel();
            this.ultraGrid_WE_PP_MODEL = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tabPage_Schedule_View = new System.Windows.Forms.TabPage();
            this.panel_Content2 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2_1 = new System.Windows.Forms.SplitContainer();
            this.chart_ORG = new ChartFX.WinForms.Chart();
            this.label_ORG = new System.Windows.Forms.Label();
            this.chart_SIMU = new ChartFX.WinForms.Chart();
            this.label_SIMU = new System.Windows.Forms.Label();
            this.ultraGrid_WE_PP_OPR_SIMU = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_Day)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage_PP_Schedule.SuspendLayout();
            this.panel_Content1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_MODEL)).BeginInit();
            this.tabPage_Schedule_View.SuspendLayout();
            this.panel_Content2.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer2_1.Panel1.SuspendLayout();
            this.splitContainer2_1.Panel2.SuspendLayout();
            this.splitContainer2_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_ORG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SIMU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_OPR_SIMU)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.ultraDateTimeEditor_Day);
            this.panelCommand.Controls.Add(this.comboBox_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_PP_HOGI);
            this.panelCommand.Controls.Add(this.btnSel);
            this.panelCommand.Controls.Add(this.btnExcel);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(1069, 55);
            this.panelCommand.TabIndex = 5;
            // 
            // ultraDateTimeEditor_Day
            // 
            this.ultraDateTimeEditor_Day.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_Day.AutoSize = false;
            this.ultraDateTimeEditor_Day.DateTime = new System.DateTime(2010, 10, 27, 0, 0, 0, 0);
            this.ultraDateTimeEditor_Day.Location = new System.Drawing.Point(246, 19);
            this.ultraDateTimeEditor_Day.Name = "ultraDateTimeEditor_Day";
            this.ultraDateTimeEditor_Day.Size = new System.Drawing.Size(104, 21);
            this.ultraDateTimeEditor_Day.TabIndex = 13;
            this.ultraDateTimeEditor_Day.Value = new System.DateTime(2010, 10, 27, 0, 0, 0, 0);
            // 
            // comboBox_BIZ_PLA_NM
            // 
            this.comboBox_BIZ_PLA_NM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BIZ_PLA_NM.FormattingEnabled = true;
            this.comboBox_BIZ_PLA_NM.Location = new System.Drawing.Point(60, 18);
            this.comboBox_BIZ_PLA_NM.Name = "comboBox_BIZ_PLA_NM";
            this.comboBox_BIZ_PLA_NM.Size = new System.Drawing.Size(111, 20);
            this.comboBox_BIZ_PLA_NM.TabIndex = 12;
            this.comboBox_BIZ_PLA_NM.SelectedValueChanged += new System.EventHandler(this.comboBox_BIZ_PLA_NM_SelectedValueChanged);
            // 
            // label_BIZ_PLA_NM
            // 
            this.label_BIZ_PLA_NM.AutoSize = true;
            this.label_BIZ_PLA_NM.Location = new System.Drawing.Point(9, 22);
            this.label_BIZ_PLA_NM.Name = "label_BIZ_PLA_NM";
            this.label_BIZ_PLA_NM.Size = new System.Drawing.Size(41, 12);
            this.label_BIZ_PLA_NM.TabIndex = 11;
            this.label_BIZ_PLA_NM.Text = "사업장";
            // 
            // label_PP_HOGI
            // 
            this.label_PP_HOGI.AutoSize = true;
            this.label_PP_HOGI.Location = new System.Drawing.Point(188, 24);
            this.label_PP_HOGI.Name = "label_PP_HOGI";
            this.label_PP_HOGI.Size = new System.Drawing.Size(53, 12);
            this.label_PP_HOGI.TabIndex = 7;
            this.label_PP_HOGI.Text = "분석일자";
            // 
            // btnSel
            // 
            this.btnSel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSel.Location = new System.Drawing.Point(901, 12);
            this.btnSel.Name = "btnSel";
            this.btnSel.Size = new System.Drawing.Size(75, 30);
            this.btnSel.TabIndex = 5;
            this.btnSel.Text = "조회";
            this.btnSel.UseVisualStyleBackColor = true;
            this.btnSel.Click += new System.EventHandler(this.btnSel_Click);
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Location = new System.Drawing.Point(982, 12);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExcel.TabIndex = 2;
            this.btnExcel.Text = "엑셀";
            this.btnExcel.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage_PP_Schedule);
            this.tabControl1.Controls.Add(this.tabPage_Schedule_View);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 55);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1069, 456);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage_PP_Schedule
            // 
            this.tabPage_PP_Schedule.Controls.Add(this.panel_Content1);
            this.tabPage_PP_Schedule.Location = new System.Drawing.Point(4, 22);
            this.tabPage_PP_Schedule.Name = "tabPage_PP_Schedule";
            this.tabPage_PP_Schedule.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_PP_Schedule.Size = new System.Drawing.Size(1061, 430);
            this.tabPage_PP_Schedule.TabIndex = 0;
            this.tabPage_PP_Schedule.Text = "펌프별";
            this.tabPage_PP_Schedule.UseVisualStyleBackColor = true;
            // 
            // panel_Content1
            // 
            this.panel_Content1.Controls.Add(this.ultraGrid_WE_PP_MODEL);
            this.panel_Content1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Content1.Location = new System.Drawing.Point(3, 3);
            this.panel_Content1.Name = "panel_Content1";
            this.panel_Content1.Size = new System.Drawing.Size(1055, 424);
            this.panel_Content1.TabIndex = 6;
            // 
            // ultraGrid_WE_PP_MODEL
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Appearance = appearance1;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CellAppearance = appearance8;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.RowAppearance = appearance11;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_WE_PP_MODEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_WE_PP_MODEL.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_WE_PP_MODEL.Name = "ultraGrid_WE_PP_MODEL";
            this.ultraGrid_WE_PP_MODEL.Size = new System.Drawing.Size(1055, 424);
            this.ultraGrid_WE_PP_MODEL.TabIndex = 3;
            // 
            // tabPage_Schedule_View
            // 
            this.tabPage_Schedule_View.Controls.Add(this.panel_Content2);
            this.tabPage_Schedule_View.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Schedule_View.Name = "tabPage_Schedule_View";
            this.tabPage_Schedule_View.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Schedule_View.Size = new System.Drawing.Size(1061, 430);
            this.tabPage_Schedule_View.TabIndex = 1;
            this.tabPage_Schedule_View.Text = "항목별";
            this.tabPage_Schedule_View.UseVisualStyleBackColor = true;
            // 
            // panel_Content2
            // 
            this.panel_Content2.Controls.Add(this.splitContainer2);
            this.panel_Content2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Content2.Location = new System.Drawing.Point(3, 3);
            this.panel_Content2.Name = "panel_Content2";
            this.panel_Content2.Size = new System.Drawing.Size(1055, 424);
            this.panel_Content2.TabIndex = 7;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer2_1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.ultraGrid_WE_PP_OPR_SIMU);
            this.splitContainer2.Size = new System.Drawing.Size(1055, 424);
            this.splitContainer2.SplitterDistance = 225;
            this.splitContainer2.TabIndex = 4;
            // 
            // splitContainer2_1
            // 
            this.splitContainer2_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2_1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2_1.Name = "splitContainer2_1";
            // 
            // splitContainer2_1.Panel1
            // 
            this.splitContainer2_1.Panel1.Controls.Add(this.chart_ORG);
            this.splitContainer2_1.Panel1.Controls.Add(this.label_ORG);
            this.splitContainer2_1.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer2_1.Panel2
            // 
            this.splitContainer2_1.Panel2.Controls.Add(this.chart_SIMU);
            this.splitContainer2_1.Panel2.Controls.Add(this.label_SIMU);
            this.splitContainer2_1.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.splitContainer2_1.Size = new System.Drawing.Size(1055, 225);
            this.splitContainer2_1.SplitterDistance = 527;
            this.splitContainer2_1.TabIndex = 1;
            // 
            // chart_ORG
            // 
            this.chart_ORG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart_ORG.Location = new System.Drawing.Point(3, 23);
            this.chart_ORG.Name = "chart_ORG";
            this.chart_ORG.RandomData.Series = 6;
            seriesAttributes4.Text = "1호기";
            this.chart_ORG.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes1,
            seriesAttributes2,
            seriesAttributes3,
            seriesAttributes4,
            seriesAttributes5,
            seriesAttributes6});
            this.chart_ORG.Size = new System.Drawing.Size(521, 199);
            this.chart_ORG.TabIndex = 2;
            // 
            // label_ORG
            // 
            this.label_ORG.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label_ORG.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_ORG.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_ORG.Location = new System.Drawing.Point(3, 3);
            this.label_ORG.Name = "label_ORG";
            this.label_ORG.Size = new System.Drawing.Size(521, 20);
            this.label_ORG.TabIndex = 1;
            this.label_ORG.Text = "실시간 해석";
            this.label_ORG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chart_SIMU
            // 
            this.chart_SIMU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart_SIMU.Location = new System.Drawing.Point(3, 23);
            this.chart_SIMU.Name = "chart_SIMU";
            this.chart_SIMU.RandomData.Series = 6;
            this.chart_SIMU.Size = new System.Drawing.Size(518, 199);
            this.chart_SIMU.TabIndex = 3;
            // 
            // label_SIMU
            // 
            this.label_SIMU.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label_SIMU.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_SIMU.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_SIMU.Location = new System.Drawing.Point(3, 3);
            this.label_SIMU.Name = "label_SIMU";
            this.label_SIMU.Size = new System.Drawing.Size(518, 20);
            this.label_SIMU.TabIndex = 2;
            this.label_SIMU.Text = "모의 해석";
            this.label_SIMU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ultraGrid_WE_PP_OPR_SIMU
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Appearance = appearance13;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.CellAppearance = appearance20;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.RowAppearance = appearance23;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_WE_PP_OPR_SIMU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_WE_PP_OPR_SIMU.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_WE_PP_OPR_SIMU.Name = "ultraGrid_WE_PP_OPR_SIMU";
            this.ultraGrid_WE_PP_OPR_SIMU.Size = new System.Drawing.Size(1055, 195);
            this.ultraGrid_WE_PP_OPR_SIMU.TabIndex = 4;
            // 
            // frmPumpScheduleSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 511);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmPumpScheduleSearch";
            this.Text = "frmPumpScheduleSearch";
            this.panelCommand.ResumeLayout(false);
            this.panelCommand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_Day)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage_PP_Schedule.ResumeLayout(false);
            this.panel_Content1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_MODEL)).EndInit();
            this.tabPage_Schedule_View.ResumeLayout(false);
            this.panel_Content2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer2_1.Panel1.ResumeLayout(false);
            this.splitContainer2_1.Panel1.PerformLayout();
            this.splitContainer2_1.Panel2.ResumeLayout(false);
            this.splitContainer2_1.Panel2.PerformLayout();
            this.splitContainer2_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart_ORG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SIMU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_OPR_SIMU)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_Day;
        private System.Windows.Forms.ComboBox comboBox_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_PP_HOGI;
        private System.Windows.Forms.Button btnSel;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_PP_Schedule;
        private System.Windows.Forms.Panel panel_Content1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_WE_PP_MODEL;
        private System.Windows.Forms.TabPage tabPage_Schedule_View;
        private System.Windows.Forms.Panel panel_Content2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_WE_PP_OPR_SIMU;
        private System.Windows.Forms.SplitContainer splitContainer2_1;
        private ChartFX.WinForms.Chart chart_ORG;
        private System.Windows.Forms.Label label_ORG;
        private ChartFX.WinForms.Chart chart_SIMU;
        private System.Windows.Forms.Label label_SIMU;
    }
}