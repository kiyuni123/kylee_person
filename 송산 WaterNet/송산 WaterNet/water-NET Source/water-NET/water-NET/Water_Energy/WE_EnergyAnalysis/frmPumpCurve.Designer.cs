﻿namespace WaterNet.WE_EnergyAnalysis
{
    partial class frmPumpCurve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.splitContainerContents = new System.Windows.Forms.SplitContainer();
            this.ultraGrid_Pump = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboCurveID = new System.Windows.Forms.ComboBox();
            this.btnPumpDelete = new System.Windows.Forms.Button();
            this.lblPump = new System.Windows.Forms.Label();
            this.ultraGrid_Eff = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboEffID = new System.Windows.Forms.ComboBox();
            this.btnEffDelete = new System.Windows.Forms.Button();
            this.lblEff = new System.Windows.Forms.Label();
            this.panelCommand.SuspendLayout();
            this.splitContainerContents.Panel1.SuspendLayout();
            this.splitContainerContents.Panel2.SuspendLayout();
            this.splitContainerContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pump)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Eff)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Controls.Add(this.btnSave);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(343, 35);
            this.panelCommand.TabIndex = 16;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(260, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 24);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(179, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 24);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // splitContainerContents
            // 
            this.splitContainerContents.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerContents.Location = new System.Drawing.Point(0, 35);
            this.splitContainerContents.Name = "splitContainerContents";
            this.splitContainerContents.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerContents.Panel1
            // 
            this.splitContainerContents.Panel1.Controls.Add(this.ultraGrid_Pump);
            this.splitContainerContents.Panel1.Controls.Add(this.panel1);
            this.splitContainerContents.Panel1.Controls.Add(this.lblPump);
            // 
            // splitContainerContents.Panel2
            // 
            this.splitContainerContents.Panel2.Controls.Add(this.ultraGrid_Eff);
            this.splitContainerContents.Panel2.Controls.Add(this.panel2);
            this.splitContainerContents.Panel2.Controls.Add(this.lblEff);
            this.splitContainerContents.Size = new System.Drawing.Size(343, 359);
            this.splitContainerContents.SplitterDistance = 177;
            this.splitContainerContents.TabIndex = 17;
            // 
            // ultraGrid_Pump
            // 
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Pump.DisplayLayout.Appearance = appearance22;
            this.ultraGrid_Pump.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Pump.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_Pump.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Pump.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_Pump.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Pump.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pump.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Pump.DisplayLayout.Override.CellAppearance = appearance24;
            this.ultraGrid_Pump.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Pump.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pump.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Pump.DisplayLayout.Override.RowAppearance = appearance27;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Pump.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.ultraGrid_Pump.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Pump.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Pump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Pump.Location = new System.Drawing.Point(26, 22);
            this.ultraGrid_Pump.Name = "ultraGrid_Pump";
            this.ultraGrid_Pump.Size = new System.Drawing.Size(315, 153);
            this.ultraGrid_Pump.TabIndex = 45;
            this.ultraGrid_Pump.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Pump_AfterRowUpdate);
            this.ultraGrid_Pump.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Pump_AfterRowInsert);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cboCurveID);
            this.panel1.Controls.Add(this.btnPumpDelete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(26, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(315, 22);
            this.panel1.TabIndex = 44;
            // 
            // cboCurveID
            // 
            this.cboCurveID.FormattingEnabled = true;
            this.cboCurveID.Location = new System.Drawing.Point(5, 1);
            this.cboCurveID.Name = "cboCurveID";
            this.cboCurveID.Size = new System.Drawing.Size(73, 20);
            this.cboCurveID.TabIndex = 39;
            this.cboCurveID.Tag = "";
            this.cboCurveID.TextUpdate += new System.EventHandler(this.cboCurveID_TextUpdate);
            // 
            // btnPumpDelete
            // 
            this.btnPumpDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPumpDelete.Location = new System.Drawing.Point(254, 1);
            this.btnPumpDelete.Name = "btnPumpDelete";
            this.btnPumpDelete.Size = new System.Drawing.Size(54, 20);
            this.btnPumpDelete.TabIndex = 6;
            this.btnPumpDelete.Text = "삭제";
            this.btnPumpDelete.UseVisualStyleBackColor = true;
            this.btnPumpDelete.Click += new System.EventHandler(this.btnPumpDelete_Click);
            // 
            // lblPump
            // 
            this.lblPump.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPump.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPump.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPump.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblPump.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPump.Location = new System.Drawing.Point(0, 0);
            this.lblPump.Name = "lblPump";
            this.lblPump.Size = new System.Drawing.Size(26, 175);
            this.lblPump.TabIndex = 42;
            this.lblPump.Text = "펌프커브";
            this.lblPump.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ultraGrid_Eff
            // 
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Eff.DisplayLayout.Appearance = appearance8;
            this.ultraGrid_Eff.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Eff.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_Eff.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Eff.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_Eff.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Eff.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Eff.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Eff.DisplayLayout.Override.CellAppearance = appearance10;
            this.ultraGrid_Eff.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Eff.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Eff.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.ultraGrid_Eff.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.ultraGrid_Eff.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Eff.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Eff.DisplayLayout.Override.RowAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Eff.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.ultraGrid_Eff.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Eff.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Eff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Eff.Location = new System.Drawing.Point(26, 22);
            this.ultraGrid_Eff.Name = "ultraGrid_Eff";
            this.ultraGrid_Eff.Size = new System.Drawing.Size(315, 154);
            this.ultraGrid_Eff.TabIndex = 46;
            this.ultraGrid_Eff.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Eff_AfterRowUpdate);
            this.ultraGrid_Eff.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ultraGrid_Eff_AfterRowInsert);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cboEffID);
            this.panel2.Controls.Add(this.btnEffDelete);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(26, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(315, 22);
            this.panel2.TabIndex = 45;
            // 
            // cboEffID
            // 
            this.cboEffID.FormattingEnabled = true;
            this.cboEffID.Location = new System.Drawing.Point(5, 1);
            this.cboEffID.Name = "cboEffID";
            this.cboEffID.Size = new System.Drawing.Size(73, 20);
            this.cboEffID.TabIndex = 40;
            this.cboEffID.Tag = "";
            this.cboEffID.TextUpdate += new System.EventHandler(this.cboEffID_TextUpdate);
            // 
            // btnEffDelete
            // 
            this.btnEffDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEffDelete.Location = new System.Drawing.Point(254, 1);
            this.btnEffDelete.Name = "btnEffDelete";
            this.btnEffDelete.Size = new System.Drawing.Size(54, 20);
            this.btnEffDelete.TabIndex = 6;
            this.btnEffDelete.Text = "삭제";
            this.btnEffDelete.UseVisualStyleBackColor = true;
            this.btnEffDelete.Click += new System.EventHandler(this.btnEffDelete_Click);
            // 
            // lblEff
            // 
            this.lblEff.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblEff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEff.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblEff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblEff.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEff.Location = new System.Drawing.Point(0, 0);
            this.lblEff.Name = "lblEff";
            this.lblEff.Size = new System.Drawing.Size(26, 176);
            this.lblEff.TabIndex = 18;
            this.lblEff.Text = "효율커브";
            this.lblEff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmPumpCurve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 394);
            this.Controls.Add(this.splitContainerContents);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmPumpCurve";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "펌프Curve정보";
            this.Load += new System.EventHandler(this.frmPumpCurve_Load);
            this.panelCommand.ResumeLayout(false);
            this.splitContainerContents.Panel1.ResumeLayout(false);
            this.splitContainerContents.Panel2.ResumeLayout(false);
            this.splitContainerContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pump)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Eff)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SplitContainer splitContainerContents;
        private System.Windows.Forms.Label lblPump;
        private System.Windows.Forms.Label lblEff;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Pump;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Eff;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnPumpDelete;
        private System.Windows.Forms.Button btnEffDelete;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cboCurveID;
        private System.Windows.Forms.ComboBox cboEffID;
    }
}