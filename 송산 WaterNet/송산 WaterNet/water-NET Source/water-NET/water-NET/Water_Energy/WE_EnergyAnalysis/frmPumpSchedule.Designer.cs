﻿namespace WaterNet.WE_EnergyAnalysis
{
    partial class frmPumpSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            ChartFX.WinForms.SeriesAttributes seriesAttributes25 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes26 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes27 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes28 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes29 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes30 = new ChartFX.WinForms.SeriesAttributes();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnModelEdit = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.ultraDateTimeEditor_Day = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.comboBox_BIZ_PLA_NM = new System.Windows.Forms.ComboBox();
            this.label_BIZ_PLA_NM = new System.Windows.Forms.Label();
            this.label_PP_HOGI = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.contextMenuStripUltraGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuColumn = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuCell = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chart_ORG = new ChartFX.WinForms.Chart();
            this.label_ORG = new System.Windows.Forms.Label();
            this.chart_SIMU = new ChartFX.WinForms.Chart();
            this.label_SIMU = new System.Windows.Forms.Label();
            this.panel_Content1 = new System.Windows.Forms.Panel();
            this.ultraGrid_WE_PP_MODEL = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraCalcManager1 = new Infragistics.Win.UltraWinCalcManager.UltraCalcManager(this.components);
            this.checkBox_W = new System.Windows.Forms.CheckBox();
            this.checkBox_Q = new System.Windows.Forms.CheckBox();
            this.checkBox_H = new System.Windows.Forms.CheckBox();
            this.checkBox_E = new System.Windows.Forms.CheckBox();
            this.checkBox_U = new System.Windows.Forms.CheckBox();
            this.comboBox_ORG = new System.Windows.Forms.ComboBox();
            this.comboBox_SIMU = new System.Windows.Forms.ComboBox();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_Day)).BeginInit();
            this.contextMenuStripUltraGrid.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_ORG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SIMU)).BeginInit();
            this.panel_Content1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_MODEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.checkBox_U);
            this.panelCommand.Controls.Add(this.checkBox_E);
            this.panelCommand.Controls.Add(this.checkBox_H);
            this.panelCommand.Controls.Add(this.checkBox_Q);
            this.panelCommand.Controls.Add(this.checkBox_W);
            this.panelCommand.Controls.Add(this.btnModelEdit);
            this.panelCommand.Controls.Add(this.btnLoad);
            this.panelCommand.Controls.Add(this.ultraDateTimeEditor_Day);
            this.panelCommand.Controls.Add(this.comboBox_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_PP_HOGI);
            this.panelCommand.Controls.Add(this.btnSave);
            this.panelCommand.Controls.Add(this.btnExcel);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(1044, 54);
            this.panelCommand.TabIndex = 4;
            // 
            // btnModelEdit
            // 
            this.btnModelEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModelEdit.Location = new System.Drawing.Point(799, 13);
            this.btnModelEdit.Name = "btnModelEdit";
            this.btnModelEdit.Size = new System.Drawing.Size(75, 30);
            this.btnModelEdit.TabIndex = 15;
            this.btnModelEdit.Text = "모의실행";
            this.btnModelEdit.UseVisualStyleBackColor = true;
            this.btnModelEdit.Click += new System.EventHandler(this.btnModelEdit_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.Location = new System.Drawing.Point(720, 13);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 30);
            this.btnLoad.TabIndex = 14;
            this.btnLoad.Text = "불러오기";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // ultraDateTimeEditor_Day
            // 
            this.ultraDateTimeEditor_Day.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_Day.AutoSize = false;
            this.ultraDateTimeEditor_Day.DateTime = new System.DateTime(2010, 11, 12, 0, 0, 0, 0);
            this.ultraDateTimeEditor_Day.Location = new System.Drawing.Point(246, 19);
            this.ultraDateTimeEditor_Day.Name = "ultraDateTimeEditor_Day";
            this.ultraDateTimeEditor_Day.Size = new System.Drawing.Size(104, 21);
            this.ultraDateTimeEditor_Day.TabIndex = 13;
            this.ultraDateTimeEditor_Day.Value = new System.DateTime(2010, 11, 12, 0, 0, 0, 0);
            // 
            // comboBox_BIZ_PLA_NM
            // 
            this.comboBox_BIZ_PLA_NM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BIZ_PLA_NM.FormattingEnabled = true;
            this.comboBox_BIZ_PLA_NM.Location = new System.Drawing.Point(60, 18);
            this.comboBox_BIZ_PLA_NM.Name = "comboBox_BIZ_PLA_NM";
            this.comboBox_BIZ_PLA_NM.Size = new System.Drawing.Size(111, 20);
            this.comboBox_BIZ_PLA_NM.TabIndex = 12;
            this.comboBox_BIZ_PLA_NM.SelectedIndexChanged += new System.EventHandler(this.comboBox_BIZ_PLA_NM_SelectedIndexChanged);
            this.comboBox_BIZ_PLA_NM.SelectedValueChanged += new System.EventHandler(this.comboBox_BIZ_PLA_NM_SelectedValueChanged);
            // 
            // label_BIZ_PLA_NM
            // 
            this.label_BIZ_PLA_NM.AutoSize = true;
            this.label_BIZ_PLA_NM.Location = new System.Drawing.Point(9, 22);
            this.label_BIZ_PLA_NM.Name = "label_BIZ_PLA_NM";
            this.label_BIZ_PLA_NM.Size = new System.Drawing.Size(41, 12);
            this.label_BIZ_PLA_NM.TabIndex = 11;
            this.label_BIZ_PLA_NM.Text = "사업장";
            // 
            // label_PP_HOGI
            // 
            this.label_PP_HOGI.AutoSize = true;
            this.label_PP_HOGI.Location = new System.Drawing.Point(188, 24);
            this.label_PP_HOGI.Name = "label_PP_HOGI";
            this.label_PP_HOGI.Size = new System.Drawing.Size(53, 12);
            this.label_PP_HOGI.TabIndex = 7;
            this.label_PP_HOGI.Text = "분석일자";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(960, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Location = new System.Drawing.Point(879, 13);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExcel.TabIndex = 2;
            this.btnExcel.Text = "엑셀";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // contextMenuStripUltraGrid
            // 
            this.contextMenuStripUltraGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuCopy,
            this.toolStripMenuPaste,
            this.toolStripMenuUndo,
            this.ToolStripMenuRedo,
            this.toolStripSeparator1,
            this.toolStripMenuColumn,
            this.toolStripMenuCell});
            this.contextMenuStripUltraGrid.Name = "contextMenuStripBreak";
            this.contextMenuStripUltraGrid.Size = new System.Drawing.Size(150, 142);
            // 
            // toolStripMenuCopy
            // 
            this.toolStripMenuCopy.Name = "toolStripMenuCopy";
            this.toolStripMenuCopy.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuCopy.Text = "Copy (Ctrl+C)";
            this.toolStripMenuCopy.Click += new System.EventHandler(this.toolStripMenuCopy_Click);
            // 
            // toolStripMenuPaste
            // 
            this.toolStripMenuPaste.Name = "toolStripMenuPaste";
            this.toolStripMenuPaste.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuPaste.Text = "Paste (Ctrl+V)";
            this.toolStripMenuPaste.Click += new System.EventHandler(this.toolStripMenuPaste_Click);
            // 
            // toolStripMenuUndo
            // 
            this.toolStripMenuUndo.Name = "toolStripMenuUndo";
            this.toolStripMenuUndo.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuUndo.Text = "Undo (Ctrl+Z)";
            this.toolStripMenuUndo.Click += new System.EventHandler(this.toolStripMenuUndo_Click);
            // 
            // ToolStripMenuRedo
            // 
            this.ToolStripMenuRedo.Name = "ToolStripMenuRedo";
            this.ToolStripMenuRedo.Size = new System.Drawing.Size(149, 22);
            this.ToolStripMenuRedo.Text = "Redo (Ctrl+Y)";
            this.ToolStripMenuRedo.Click += new System.EventHandler(this.ToolStripMenuRedo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // toolStripMenuColumn
            // 
            this.toolStripMenuColumn.Name = "toolStripMenuColumn";
            this.toolStripMenuColumn.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuColumn.Text = "Column 치환";
            this.toolStripMenuColumn.Click += new System.EventHandler(this.toolStripMenuColumn_Click);
            // 
            // toolStripMenuCell
            // 
            this.toolStripMenuCell.Name = "toolStripMenuCell";
            this.toolStripMenuCell.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuCell.Text = "Cell 변경";
            this.toolStripMenuCell.Click += new System.EventHandler(this.toolStripMenuCell_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 54);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel_Content1);
            this.splitContainer1.Size = new System.Drawing.Size(1044, 528);
            this.splitContainer1.SplitterDistance = 248;
            this.splitContainer1.TabIndex = 8;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.comboBox_ORG);
            this.splitContainer2.Panel1.Controls.Add(this.chart_ORG);
            this.splitContainer2.Panel1.Controls.Add(this.label_ORG);
            this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.comboBox_SIMU);
            this.splitContainer2.Panel2.Controls.Add(this.chart_SIMU);
            this.splitContainer2.Panel2.Controls.Add(this.label_SIMU);
            this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.splitContainer2.Size = new System.Drawing.Size(1044, 248);
            this.splitContainer2.SplitterDistance = 522;
            this.splitContainer2.TabIndex = 0;
            // 
            // chart_ORG
            // 
            this.chart_ORG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart_ORG.Location = new System.Drawing.Point(3, 23);
            this.chart_ORG.Name = "chart_ORG";
            this.chart_ORG.RandomData.Series = 6;
            seriesAttributes25.Text = "1호기";
            this.chart_ORG.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes25,
            seriesAttributes26,
            seriesAttributes27,
            seriesAttributes28,
            seriesAttributes29,
            seriesAttributes30});
            this.chart_ORG.Size = new System.Drawing.Size(516, 222);
            this.chart_ORG.TabIndex = 2;
            // 
            // label_ORG
            // 
            this.label_ORG.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label_ORG.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_ORG.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_ORG.Location = new System.Drawing.Point(3, 3);
            this.label_ORG.Name = "label_ORG";
            this.label_ORG.Size = new System.Drawing.Size(516, 20);
            this.label_ORG.TabIndex = 1;
            this.label_ORG.Text = "실시간 해석";
            this.label_ORG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chart_SIMU
            // 
            this.chart_SIMU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart_SIMU.Location = new System.Drawing.Point(3, 23);
            this.chart_SIMU.Name = "chart_SIMU";
            this.chart_SIMU.RandomData.Series = 6;
            this.chart_SIMU.Size = new System.Drawing.Size(512, 222);
            this.chart_SIMU.TabIndex = 3;
            // 
            // label_SIMU
            // 
            this.label_SIMU.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label_SIMU.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_SIMU.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_SIMU.Location = new System.Drawing.Point(3, 3);
            this.label_SIMU.Name = "label_SIMU";
            this.label_SIMU.Size = new System.Drawing.Size(512, 20);
            this.label_SIMU.TabIndex = 2;
            this.label_SIMU.Text = "모의 해석";
            this.label_SIMU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_Content1
            // 
            this.panel_Content1.Controls.Add(this.ultraGrid_WE_PP_MODEL);
            this.panel_Content1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Content1.Location = new System.Drawing.Point(0, 0);
            this.panel_Content1.Name = "panel_Content1";
            this.panel_Content1.Size = new System.Drawing.Size(1044, 276);
            this.panel_Content1.TabIndex = 8;
            // 
            // ultraGrid_WE_PP_MODEL
            // 
            this.ultraGrid_WE_PP_MODEL.CalcManager = this.ultraCalcManager1;
            this.ultraGrid_WE_PP_MODEL.ContextMenuStrip = this.contextMenuStripUltraGrid;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Appearance = appearance1;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CardAreaAppearance = appearance2;
            appearance3.BorderColor = System.Drawing.Color.Silver;
            appearance3.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CellAppearance = appearance3;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.CellPadding = 0;
            appearance4.BackColor = System.Drawing.SystemColors.Control;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.GroupByRowAppearance = appearance4;
            appearance5.TextHAlignAsString = "Left";
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.HeaderAppearance = appearance5;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.RowAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.Override.TemplateAddRowAppearance = appearance7;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_WE_PP_MODEL.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_WE_PP_MODEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_WE_PP_MODEL.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_WE_PP_MODEL.Name = "ultraGrid_WE_PP_MODEL";
            this.ultraGrid_WE_PP_MODEL.Size = new System.Drawing.Size(1044, 276);
            this.ultraGrid_WE_PP_MODEL.TabIndex = 40;
            this.ultraGrid_WE_PP_MODEL.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ultraGrid_WE_PP_MODEL_Error);
            this.ultraGrid_WE_PP_MODEL.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ultraGrid_WE_PP_MODEL_MouseDown);
            this.ultraGrid_WE_PP_MODEL.AfterPerformAction += new Infragistics.Win.UltraWinGrid.AfterUltraGridPerformActionEventHandler(this.ultraGrid_WE_PP_MODEL_AfterPerformAction);
            this.ultraGrid_WE_PP_MODEL.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ultraGrid_WE_PP_MODEL_InitializeLayout);
            this.ultraGrid_WE_PP_MODEL.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.ultraGrid_WE_PP_MODEL_AfterSelectChange);
            // 
            // ultraCalcManager1
            // 
            this.ultraCalcManager1.ContainingControl = this;
            // 
            // checkBox_W
            // 
            this.checkBox_W.AutoSize = true;
            this.checkBox_W.Location = new System.Drawing.Point(369, 23);
            this.checkBox_W.Name = "checkBox_W";
            this.checkBox_W.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_W.Size = new System.Drawing.Size(60, 16);
            this.checkBox_W.TabIndex = 16;
            this.checkBox_W.Text = "전력량";
            this.checkBox_W.UseVisualStyleBackColor = true;
            this.checkBox_W.CheckedChanged += new System.EventHandler(this.checkBox_W_CheckedChanged);
            // 
            // checkBox_Q
            // 
            this.checkBox_Q.AutoSize = true;
            this.checkBox_Q.Location = new System.Drawing.Point(437, 23);
            this.checkBox_Q.Name = "checkBox_Q";
            this.checkBox_Q.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_Q.Size = new System.Drawing.Size(48, 16);
            this.checkBox_Q.TabIndex = 17;
            this.checkBox_Q.Text = "유량";
            this.checkBox_Q.UseVisualStyleBackColor = true;
            // 
            // checkBox_H
            // 
            this.checkBox_H.AutoSize = true;
            this.checkBox_H.Location = new System.Drawing.Point(496, 23);
            this.checkBox_H.Name = "checkBox_H";
            this.checkBox_H.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_H.Size = new System.Drawing.Size(48, 16);
            this.checkBox_H.TabIndex = 18;
            this.checkBox_H.Text = "양정";
            this.checkBox_H.UseVisualStyleBackColor = true;
            // 
            // checkBox_E
            // 
            this.checkBox_E.AutoSize = true;
            this.checkBox_E.Location = new System.Drawing.Point(559, 23);
            this.checkBox_E.Name = "checkBox_E";
            this.checkBox_E.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_E.Size = new System.Drawing.Size(48, 16);
            this.checkBox_E.TabIndex = 19;
            this.checkBox_E.Text = "효율";
            this.checkBox_E.UseVisualStyleBackColor = true;
            // 
            // checkBox_U
            // 
            this.checkBox_U.AutoSize = true;
            this.checkBox_U.Location = new System.Drawing.Point(613, 23);
            this.checkBox_U.Name = "checkBox_U";
            this.checkBox_U.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_U.Size = new System.Drawing.Size(60, 16);
            this.checkBox_U.TabIndex = 20;
            this.checkBox_U.Text = "원단위";
            this.checkBox_U.UseVisualStyleBackColor = true;
            // 
            // comboBox_ORG
            // 
            this.comboBox_ORG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_ORG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ORG.FormattingEnabled = true;
            this.comboBox_ORG.Location = new System.Drawing.Point(408, 3);
            this.comboBox_ORG.Name = "comboBox_ORG";
            this.comboBox_ORG.Size = new System.Drawing.Size(111, 20);
            this.comboBox_ORG.TabIndex = 13;
            this.comboBox_ORG.SelectedValueChanged += new System.EventHandler(this.comboBox_ORG_SelectedValueChanged);
            // 
            // comboBox_SIMU
            // 
            this.comboBox_SIMU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_SIMU.FormattingEnabled = true;
            this.comboBox_SIMU.Location = new System.Drawing.Point(3, 3);
            this.comboBox_SIMU.Name = "comboBox_SIMU";
            this.comboBox_SIMU.Size = new System.Drawing.Size(111, 20);
            this.comboBox_SIMU.TabIndex = 13;
            this.comboBox_SIMU.Visible = false;
            // 
            // frmPumpSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 582);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmPumpSchedule";
            this.Text = "frmPumpSchedule";
            this.Load += new System.EventHandler(this.frmPumpSchedule_Load);
            this.panelCommand.ResumeLayout(false);
            this.panelCommand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_Day)).EndInit();
            this.contextMenuStripUltraGrid.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart_ORG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_SIMU)).EndInit();
            this.panel_Content1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_MODEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_Day;
        private System.Windows.Forms.ComboBox comboBox_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_PP_HOGI;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripUltraGrid;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuCopy;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuPaste;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuColumn;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuCell;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuRedo;
        private System.Windows.Forms.Button btnModelEdit;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel_Content1;
        private System.Windows.Forms.Label label_ORG;
        private System.Windows.Forms.Label label_SIMU;
        private ChartFX.WinForms.Chart chart_ORG;
        private ChartFX.WinForms.Chart chart_SIMU;
        private Infragistics.Win.UltraWinCalcManager.UltraCalcManager ultraCalcManager1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_WE_PP_MODEL;
        private System.Windows.Forms.CheckBox checkBox_U;
        private System.Windows.Forms.CheckBox checkBox_E;
        private System.Windows.Forms.CheckBox checkBox_H;
        private System.Windows.Forms.CheckBox checkBox_Q;
        private System.Windows.Forms.CheckBox checkBox_W;
        private System.Windows.Forms.ComboBox comboBox_ORG;
        private System.Windows.Forms.ComboBox comboBox_SIMU;
    }
}