﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
using WaterNet.WE_Common;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WE_EnergyAnalysis
{
    public partial class frmPumpScheduleSearch : Form, WaterNet.WaterNetCore.IForminterface
    {
        #region Private Field ------------------------------------------------------------------------------
        GridManager m_gridManager1 = default(GridManager);  //모의분석 그리드
        GridManager m_gridManager2 = default(GridManager);  //분석결과 그리드
        #endregion

        #region 생성자 및 환경설정 ----------------------------------------------------------------------
        /// <summary>
        /// 생성자
        /// </summary>
        public frmPumpScheduleSearch()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "에너지관리-모의분석 조회"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        #region 초기화설정
        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;
            //----------------------------------------------------------------------------------------------------------------
            #region 그리드 설정
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DD";
            oUltraGridColumn.Header.Caption = "가동일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HM";
            oUltraGridColumn.Header.Caption = "가동시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            //////////////////////펌프 가동 TAG-ID
            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "OPR_TAGID_1";
            //oUltraGridColumn.Header.Caption = "#1 TAG";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#1";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "OPR_TAGID_2";
            //oUltraGridColumn.Header.Caption = "#2 TAG";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#2";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "OPR_TAGID_3";
            //oUltraGridColumn.Header.Caption = "#3 TAG";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#3";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "OPR_TAGID_4";
            //oUltraGridColumn.Header.Caption = "#4 TAG";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#4";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "OPR_TAGID_5";
            //oUltraGridColumn.Header.Caption = "#5 TAG";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#5";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "OPR_TAGID_6";
            //oUltraGridColumn.Header.Caption = "#6 TAG";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#6";

            //////////////////////펌프 가동 해석모델 항목ID
            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_OPR_MDLID_1";
            //oUltraGridColumn.Header.Caption = "#1 OPRID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#1";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_OPR_MDLID_2";
            //oUltraGridColumn.Header.Caption = "#2 OPRID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#2";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_OPR_MDLID_3";
            //oUltraGridColumn.Header.Caption = "#3 OPRID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#3";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_OPR_MDLID_4";
            //oUltraGridColumn.Header.Caption = "#4 OPRID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#4";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_OPR_MDLID_5";
            //oUltraGridColumn.Header.Caption = "#5 OPRID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#5";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_OPR_MDLID_6";
            //oUltraGridColumn.Header.Caption = "#6 OPRID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#6";

            //////////////////////펌프 가동 해석 패턴 항목ID
            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_PTN_MDLID_1";
            //oUltraGridColumn.Header.Caption = "#1 PTNID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#1";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_PTN_MDLID_2";
            //oUltraGridColumn.Header.Caption = "#2 PTNID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#2";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_PTN_MDLID_3";
            //oUltraGridColumn.Header.Caption = "#3 PTNID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#3";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_PTN_MDLID_4";
            //oUltraGridColumn.Header.Caption = "#4 PTNID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#4";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_PTN_MDLID_5";
            //oUltraGridColumn.Header.Caption = "#5 PTNID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#5";

            //oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "AN_PTN_MDLID_6";
            //oUltraGridColumn.Header.Caption = "#6 PTNID";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Tag = "#6";

            ////////////////////펌프 가동 VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            ////////////////////펌프 해석결과 전력량(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_W_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 전력량(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_W_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 유량(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_Q_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 유량(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_Q_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 양정(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_H_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 양정(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_H_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 효율(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_E_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 효율(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_E_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 원단위(기존) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ORG_U_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 해석결과 원단위(변경) VALUE
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIMU_U_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            m_gridManager1 = new GridManager(ultraGrid_WE_PP_MODEL);
            m_gridManager1.SetGridStyle_Select();
            m_gridManager1.ColumeAllowEdit(20, 25);  //펌프 가동 : AllowEdit

            ArrayList keyValues = new ArrayList();
            keyValues.Add("DD");
            keyValues.Add("HM");
            m_gridManager1.AddColumnGroup("모의날짜", keyValues);

            //keyValues.Clear();
            //keyValues.Add("OPR_TAGID_1");
            //keyValues.Add("OPR_TAGID_2");
            //keyValues.Add("OPR_TAGID_3");
            //keyValues.Add("OPR_TAGID_4");
            //keyValues.Add("OPR_TAGID_5");
            //keyValues.Add("OPR_TAGID_6");
            //m_gridManager1.AddColumnGroup("OPR_TAGID", "가동 TAG-ID", keyValues);
            ////m_gridManager1.VisibleGridGroup("OPR_TAGID", false);

            //keyValues.Clear();
            //keyValues.Add("AN_OPR_MDLID_1");
            //keyValues.Add("AN_OPR_MDLID_2");
            //keyValues.Add("AN_OPR_MDLID_3");
            //keyValues.Add("AN_OPR_MDLID_4");
            //keyValues.Add("AN_OPR_MDLID_5");
            //keyValues.Add("AN_OPR_MDLID_6");
            //m_gridManager1.AddColumnGroup("AN_OPR_MDLID", "가동 모델항목ID", keyValues);
            ////m_gridManager1.VisibleGridGroup("AN_OPR_MDLID", false);

            //keyValues.Clear();
            //keyValues.Add("AN_PTN_MDLID_1");
            //keyValues.Add("AN_PTN_MDLID_2");
            //keyValues.Add("AN_PTN_MDLID_3");
            //keyValues.Add("AN_PTN_MDLID_4");
            //keyValues.Add("AN_PTN_MDLID_5");
            //keyValues.Add("AN_PTN_MDLID_6");
            //m_gridManager1.AddColumnGroup("AN_PTN_MDLID", "가동 모델패턴ID", keyValues);
            ////m_gridManager1.VisibleGridGroup("AN_PTN_MDLID", false);

            keyValues.Clear();
            keyValues.Add("#1");
            keyValues.Add("#2");
            keyValues.Add("#3");
            keyValues.Add("#4");
            keyValues.Add("#5");
            keyValues.Add("#6");
            m_gridManager1.AddColumnGroup("OPR_VALUE", "펌프가동", keyValues);

            keyValues.Clear();
            keyValues.Add("ORG_W1");
            keyValues.Add("ORG_W2");
            keyValues.Add("ORG_W3");
            keyValues.Add("ORG_W4");
            keyValues.Add("ORG_W5");
            keyValues.Add("ORG_W6");
            keyValues.Add("ORG_W_SUM");
            m_gridManager1.AddColumnGroup("ORG_W", "기존전력", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("ORG_W", false);

            keyValues.Clear();
            keyValues.Add("SIMU_W1");
            keyValues.Add("SIMU_W2");
            keyValues.Add("SIMU_W3");
            keyValues.Add("SIMU_W4");
            keyValues.Add("SIMU_W5");
            keyValues.Add("SIMU_W6");
            keyValues.Add("SIMU_W_SUM");
            m_gridManager1.AddColumnGroup("SIMU_W", "변경전력", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("SIMU_W", false);

            keyValues.Clear();
            keyValues.Add("ORG_Q1");
            keyValues.Add("ORG_Q2");
            keyValues.Add("ORG_Q3");
            keyValues.Add("ORG_Q4");
            keyValues.Add("ORG_Q5");
            keyValues.Add("ORG_Q6");
            keyValues.Add("ORG_Q_SUM");
            m_gridManager1.AddColumnGroup("ORG_Q", "기존유량", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("ORG_Q", false);

            keyValues.Clear();
            keyValues.Add("SIMU_Q1");
            keyValues.Add("SIMU_Q2");
            keyValues.Add("SIMU_Q3");
            keyValues.Add("SIMU_Q4");
            keyValues.Add("SIMU_Q5");
            keyValues.Add("SIMU_Q6");
            keyValues.Add("SIMU_Q_SUM");
            m_gridManager1.AddColumnGroup("SIMU_Q", "변경유량", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("SIMU_Q", false);

            keyValues.Clear();
            keyValues.Add("ORG_H1");
            keyValues.Add("ORG_H2");
            keyValues.Add("ORG_H3");
            keyValues.Add("ORG_H4");
            keyValues.Add("ORG_H5");
            keyValues.Add("ORG_H6");
            keyValues.Add("ORG_H_SUM");
            m_gridManager1.AddColumnGroup("ORG_H", "기존양정", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("ORG_H", false);

            keyValues.Clear();
            keyValues.Add("SIMU_H1");
            keyValues.Add("SIMU_H2");
            keyValues.Add("SIMU_H3");
            keyValues.Add("SIMU_H4");
            keyValues.Add("SIMU_H5");
            keyValues.Add("SIMU_H6");
            keyValues.Add("SIMU_H_SUM");
            m_gridManager1.AddColumnGroup("SIMU_H", "변경양정", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("SIMU_H", false);

            keyValues.Clear();
            keyValues.Add("ORG_E1");
            keyValues.Add("ORG_E2");
            keyValues.Add("ORG_E3");
            keyValues.Add("ORG_E4");
            keyValues.Add("ORG_E5");
            keyValues.Add("ORG_E6");
            keyValues.Add("ORG_E_SUM");
            m_gridManager1.AddColumnGroup("ORG_E", "기존효율", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("ORG_E", false);

            keyValues.Clear();
            keyValues.Add("SIMU_E1");
            keyValues.Add("SIMU_E2");
            keyValues.Add("SIMU_E3");
            keyValues.Add("SIMU_E4");
            keyValues.Add("SIMU_E5");
            keyValues.Add("SIMU_E6");
            keyValues.Add("SIMU_E_SUM");
            m_gridManager1.AddColumnGroup("SIMU_E", "변경효율", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("SIMU_E", false);

            keyValues.Clear();
            keyValues.Add("ORG_U1");
            keyValues.Add("ORG_U2");
            keyValues.Add("ORG_U3");
            keyValues.Add("ORG_U4");
            keyValues.Add("ORG_U5");
            keyValues.Add("ORG_U6");
            keyValues.Add("ORG_U_SUM");
            m_gridManager1.AddColumnGroup("ORG_U", "기존원단위", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("ORG_U", false);

            keyValues.Clear();
            keyValues.Add("SIMU_U1");
            keyValues.Add("SIMU_U2");
            keyValues.Add("SIMU_U3");
            keyValues.Add("SIMU_U4");
            keyValues.Add("SIMU_U5");
            keyValues.Add("SIMU_U6");
            keyValues.Add("SIMU_U_SUM");
            m_gridManager1.AddColumnGroup("SIMU_U", "변경원단위", keyValues, "UnLock");
            //m_gridManager1.VisibleGridGroup("SIMU_U", false);

            m_gridManager1.Grid.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            #endregion
            //-----------------------------------------------------------------------------------------------------------------


            //-----------------------------------------------------------------------------------------------------------------
            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DD";
            oUltraGridColumn.Header.Caption = "가동일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HM";
            oUltraGridColumn.Header.Caption = "가동시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_OPR_SIMU_DUR";
            oUltraGridColumn.Header.Caption = "모의가동시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_ORG_Q";
            oUltraGridColumn.Header.Caption = "유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_ORG_H";
            oUltraGridColumn.Header.Caption = "양정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_ORG_E";
            oUltraGridColumn.Header.Caption = "효율";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_ORG_W";
            oUltraGridColumn.Header.Caption = "전력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_ORG_U";
            oUltraGridColumn.Header.Caption = "원단위";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_SIMU_Q";
            oUltraGridColumn.Header.Caption = "유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_SIMU_H";
            oUltraGridColumn.Header.Caption = "양정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_SIMU_E";
            oUltraGridColumn.Header.Caption = "효율";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_SIMU_W";
            oUltraGridColumn.Header.Caption = "전력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_SIMU_U";
            oUltraGridColumn.Header.Caption = "원단위";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SAVE_WON";
            oUltraGridColumn.Header.Caption = "절감액";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,###,##0.00";
            oUltraGridColumn.Hidden = false;

            //oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "USERID";
            //oUltraGridColumn.Header.Caption = "등록자";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;

            //oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "ENTDT";
            //oUltraGridColumn.Header.Caption = "등록일자";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;

            //oUltraGridColumn = ultraGrid_WE_PP_OPR_SIMU.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "CHGDT";
            //oUltraGridColumn.Header.Caption = "수정일자";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;

            m_gridManager2 = new GridManager(ultraGrid_WE_PP_OPR_SIMU);
            m_gridManager2.SetGridStyle_Select();


            keyValues.Clear();
            keyValues.Add("DD");
            keyValues.Add("HM");
            m_gridManager2.AddColumnGroup("모의날짜", keyValues);

            keyValues.Clear();
            keyValues.Add("PP_ORG_Q");
            keyValues.Add("PP_ORG_H");
            keyValues.Add("PP_ORG_E");
            keyValues.Add("PP_ORG_W");
            keyValues.Add("PP_ORG_U");
            m_gridManager2.AddColumnGroup("현행", keyValues);

            keyValues.Clear();
            keyValues.Add("PP_SIMU_Q");
            keyValues.Add("PP_SIMU_H");
            keyValues.Add("PP_SIMU_E");
            keyValues.Add("PP_SIMU_W");
            keyValues.Add("PP_SIMU_U");
            m_gridManager2.AddColumnGroup("개선", keyValues);

            keyValues.Clear();
            keyValues.Add("SAVE_WON");
            m_gridManager2.AddColumnGroup("절감액", "절감액", keyValues);

            #endregion
            m_gridManager1.SetColumnWidth(60);
            m_gridManager2.SetColumnWidth(60);

            #region ComboBox 설정

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT BIZ_PLA_SEQ AS CD, BIZ_PLA_NM AS CDNM FROM WE_BIZ_PLA ORDER BY BIZ_PLA_NM ASC");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_BIZ_PLA_NM, oStringBuilder.ToString(), false);

            #endregion

            ultraDateTimeEditor_Day.Value = DateTime.Today.AddDays(-1);

            splitContainer2.Panel1Collapsed = true;
        }
        #endregion
        #endregion

        public void Open()
        {

        }

        // Private function
        /// <summary>
        /// 그리드 다시 구성 : 펌프의 호기갯수가 다르므로, 조회후 다시구성한다.
        /// </summary>
        /// <param name="oReader"></param>
        private void ReInitializeGrid(DataTable oTable)
        {

            m_gridManager1.RemoveAllColumns();

            UltraGridColumn oUltraGridColumn;
            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DD";
            oUltraGridColumn.Header.Caption = "가동일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_MODEL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HM";
            oUltraGridColumn.Header.Caption = "가동시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;


            if (oTable.Rows.Count == 0) return;

            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("N" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.AllowEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100);
            }

            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("OW" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "전력량");
            }
            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("NW" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "전력량");
            }
            m_gridManager1.AddColumn("W_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "전력량");

            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("OQ" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "유량");
            }
            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("NQ" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "유량");
            }
            m_gridManager1.AddColumn("Q_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "유량");

            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("OH" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "양정");
            }
            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("NH" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "양정");
            }
            m_gridManager1.AddColumn("H_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "양정");

            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("OU" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "원단위");
            }
            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("NU" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "원단위");
            }
            m_gridManager1.AddColumn("U_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "원단위");

            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("OE" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "효율");
            }
            foreach (DataRow item in oTable.Rows)
            {
                m_gridManager1.AddColumn("NE" + Convert.ToString(item["FAC_SEQ"]), Convert.ToString(item["PP_HOGI"]), Activation.NoEdit, CellClickAction.RowSelect, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "효율");
            }
            m_gridManager1.AddColumn("E_SUM", "합계", Activation.NoEdit, CellClickAction.Default, Infragistics.Win.UltraWinGrid.ColumnStyle.Default, false, 100, "효율");


            ArrayList keyValues = new ArrayList();
            keyValues.Add("DD");
            keyValues.Add("HM");
            m_gridManager1.AddColumnGroup("모의날짜", keyValues);

            keyValues.Clear();
            foreach (DataRow item in oTable.Rows)
            {
                keyValues.Add("N" + Convert.ToString(item["FAC_SEQ"]));
            }
            m_gridManager1.AddColumnGroup("펌프모터 가동", "펌프모터 가동", keyValues, "Lock");

            keyValues.Clear();
            foreach (DataRow item in oTable.Rows)
            {
                keyValues.Add("OW" + Convert.ToString(item["FAC_SEQ"]));
                keyValues.Add("NW" + Convert.ToString(item["FAC_SEQ"]));
            }
            keyValues.Add("W_SUM");
            m_gridManager1.AddColumnGroup("전력량", "전력량", keyValues, "Unlock");

            keyValues.Clear();
            foreach (DataRow item in oTable.Rows)
            {
                keyValues.Add("OQ" + Convert.ToString(item["FAC_SEQ"]));
                keyValues.Add("NQ" + Convert.ToString(item["FAC_SEQ"]));
            }
            keyValues.Add("Q_SUM");
            m_gridManager1.AddColumnGroup("유량", "유량", keyValues, "Unlock");

            keyValues.Clear();
            foreach (DataRow item in oTable.Rows)
            {
                keyValues.Add("OH" + Convert.ToString(item["FAC_SEQ"]));
                keyValues.Add("NH" + Convert.ToString(item["FAC_SEQ"]));
            }
            keyValues.Add("H_SUM");
            m_gridManager1.AddColumnGroup("양정", "양정", keyValues, "Unlock");

            keyValues.Clear();
            foreach (DataRow item in oTable.Rows)
            {
                keyValues.Add("OU" + Convert.ToString(item["FAC_SEQ"]));
                keyValues.Add("NU" + Convert.ToString(item["FAC_SEQ"]));
            }
            keyValues.Add("U_SUM");
            m_gridManager1.AddColumnGroup("원단위", "원단위", keyValues, "Unlock");

            keyValues.Clear();
            foreach (DataRow item in oTable.Rows)
            {
                keyValues.Add("OE" + Convert.ToString(item["FAC_SEQ"]));
                keyValues.Add("NE" + Convert.ToString(item["FAC_SEQ"]));
            }
            keyValues.Add("E_SUM");
            m_gridManager1.AddColumnGroup("효율", "효율", keyValues, "Unlock");

        }

        /// <summary>
        /// 조회버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSel_Click(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            OracleDataReader oReader = null;
            try
            {
                oDBManager.Open();

                #region Tab1
                oStringBuilder.AppendLine("SELECT ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(SUBSTR(A.PP_OPR_SIMU_TIME, 1, 8), 'YYYYMMDD'), 'YYYY-MM-DD') AS DD,");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(SUBSTR(A.PP_OPR_SIMU_TIME, 9, 4), 'HH24:MI'), 'HH24:MI') AS HM,");
                oStringBuilder.AppendLine("			 P1.PP_OPR_SIMU_DUR AS \"#1\", P1.PP_ORG_Q AS ORG_Q1, P1.PP_ORG_H AS ORG_H1, P1.PP_ORG_E AS ORG_E1, ");
                oStringBuilder.AppendLine("			 P1.PP_ORG_W AS ORG_W1, P1.PP_ORG_U AS ORG_U1, P1.PP_SIMU_Q AS SIMU_Q1, P1.PP_SIMU_H AS SIMU_H1, ");
                oStringBuilder.AppendLine("			 P1.PP_SIMU_E AS SIMU_E1, P1.PP_SIMU_W AS SIMU_W1, P1.PP_SIMU_U AS SIMU_U1,");
                oStringBuilder.AppendLine("			 P2.PP_OPR_SIMU_DUR AS \"#2\", P2.PP_ORG_Q AS ORG_Q2, P2.PP_ORG_H AS ORG_H2, P2.PP_ORG_E AS ORG_E2, ");
                oStringBuilder.AppendLine("			 P2.PP_ORG_W AS ORG_W2, P2.PP_ORG_U AS ORG_U2, P2.PP_SIMU_Q AS SIMU_Q2, P2.PP_SIMU_H AS SIMU_H2, ");
                oStringBuilder.AppendLine("			 P2.PP_SIMU_E AS SIMU_E2, P2.PP_SIMU_W AS SIMU_W2, P2.PP_SIMU_U AS SIMU_U2,			 ");
                oStringBuilder.AppendLine("			 P3.PP_OPR_SIMU_DUR AS \"#3\", P3.PP_ORG_Q AS ORG_Q3, P3.PP_ORG_H AS ORG_H3, P3.PP_ORG_E AS ORG_E3, ");
                oStringBuilder.AppendLine("			 P3.PP_ORG_W AS ORG_W3, P3.PP_ORG_U AS ORG_U3, P3.PP_SIMU_Q AS SIMU_Q3, P3.PP_SIMU_H AS SIMU_H3, ");
                oStringBuilder.AppendLine("			 P3.PP_SIMU_E AS SIMU_E3, P3.PP_SIMU_W AS SIMU_W3, P3.PP_SIMU_U AS SIMU_U3,		");
                oStringBuilder.AppendLine("			 P4.PP_OPR_SIMU_DUR AS \"#4\", P4.PP_ORG_Q AS ORG_Q4, P4.PP_ORG_H AS ORG_H4, P4.PP_ORG_E AS ORG_E4, ");
                oStringBuilder.AppendLine("			 P4.PP_ORG_W AS ORG_W4, P4.PP_ORG_U AS ORG_U4, P4.PP_SIMU_Q AS SIMU_Q4, P4.PP_SIMU_H AS SIMU_H4, ");
                oStringBuilder.AppendLine("			 P4.PP_SIMU_E AS SIMU_E4, P4.PP_SIMU_W AS SIMU_W4, P4.PP_SIMU_U AS SIMU_U4,			");
                oStringBuilder.AppendLine("			 P5.PP_OPR_SIMU_DUR AS \"#5\", P5.PP_ORG_Q AS ORG_Q5, P5.PP_ORG_H AS ORG_H5, P5.PP_ORG_E AS ORG_E5, ");
                oStringBuilder.AppendLine("			 P5.PP_ORG_W AS ORG_W5, P5.PP_ORG_U AS ORG_U5, P5.PP_SIMU_Q AS SIMU_Q5, P5.PP_SIMU_H AS SIMU_H5, ");
                oStringBuilder.AppendLine("			 P5.PP_SIMU_E AS SIMU_E5, P5.PP_SIMU_W AS SIMU_W5, P5.PP_SIMU_U AS SIMU_U5,		");
                oStringBuilder.AppendLine("			 P6.PP_OPR_SIMU_DUR AS \"#6\", P6.PP_ORG_Q AS ORG_Q6, P6.PP_ORG_H AS ORG_H6, P6.PP_ORG_E AS ORG_E6, ");
                oStringBuilder.AppendLine("			 P6.PP_ORG_W AS ORG_W6, P6.PP_ORG_U AS ORG_U6, P6.PP_SIMU_Q AS SIMU_Q6, P6.PP_SIMU_H AS SIMU_H6, ");
                oStringBuilder.AppendLine("			 P6.PP_SIMU_E AS SIMU_E6, P6.PP_SIMU_W AS SIMU_W6, P6.PP_SIMU_U AS SIMU_U6		 		 	 		 ");
                oStringBuilder.AppendLine("FROM");
                oStringBuilder.AppendLine(" (SELECT DISTINCT B.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine("  FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("   INNER JOIN WE_PP_OPR_SIMU B ON A.FAC_SEQ = B.FAC_SEQ AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'"); 
                oStringBuilder.AppendLine("   AND TO_DATE(SUBSTR(B.PP_OPR_SIMU_TIME ,1,8),'yyyymmdd') = '" + FunctionManager.StringToDateTime((DateTime)ultraDateTimeEditor_Day.Value) + "') A");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (");
                oStringBuilder.AppendLine("  SELECT B.PP_OPR_SIMU_TIME, B.PP_OPR_SIMU_DUR, B.PP_ORG_Q, B.PP_ORG_H, B.PP_ORG_E, B.PP_ORG_W, B.PP_ORG_U,");
                oStringBuilder.AppendLine("       B.PP_SIMU_Q, B.PP_SIMU_H, B.PP_SIMU_E, B.PP_SIMU_W, B.PP_SIMU_U");
                oStringBuilder.AppendLine("  FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("   INNER JOIN WE_PP_OPR_SIMU B ON A.FAC_SEQ = B.FAC_SEQ AND A.BIZ_PLA_SEQ = '1' AND A.PP_HOGI = '#1') P1 ON A.PP_OPR_SIMU_TIME = P1.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (");
                oStringBuilder.AppendLine("  SELECT B.PP_OPR_SIMU_TIME, B.PP_OPR_SIMU_DUR, B.PP_ORG_Q, B.PP_ORG_H, B.PP_ORG_E, B.PP_ORG_W, B.PP_ORG_U,");
                oStringBuilder.AppendLine("       B.PP_SIMU_Q, B.PP_SIMU_H, B.PP_SIMU_E, B.PP_SIMU_W, B.PP_SIMU_U");
                oStringBuilder.AppendLine("  FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("   INNER JOIN WE_PP_OPR_SIMU B ON A.FAC_SEQ = B.FAC_SEQ AND A.BIZ_PLA_SEQ = '1' AND A.PP_HOGI = '#2') P2 ON A.PP_OPR_SIMU_TIME = P2.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (");
                oStringBuilder.AppendLine("  SELECT B.PP_OPR_SIMU_TIME, B.PP_OPR_SIMU_DUR, B.PP_ORG_Q, B.PP_ORG_H, B.PP_ORG_E, B.PP_ORG_W, B.PP_ORG_U,");
                oStringBuilder.AppendLine("       B.PP_SIMU_Q, B.PP_SIMU_H, B.PP_SIMU_E, B.PP_SIMU_W, B.PP_SIMU_U");
                oStringBuilder.AppendLine("  FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("   INNER JOIN WE_PP_OPR_SIMU B ON A.FAC_SEQ = B.FAC_SEQ AND A.BIZ_PLA_SEQ = '1' AND A.PP_HOGI = '#3') P3 ON A.PP_OPR_SIMU_TIME = P3.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (");
                oStringBuilder.AppendLine("  SELECT B.PP_OPR_SIMU_TIME, B.PP_OPR_SIMU_DUR, B.PP_ORG_Q, B.PP_ORG_H, B.PP_ORG_E, B.PP_ORG_W, B.PP_ORG_U,");
                oStringBuilder.AppendLine("       B.PP_SIMU_Q, B.PP_SIMU_H, B.PP_SIMU_E, B.PP_SIMU_W, B.PP_SIMU_U");
                oStringBuilder.AppendLine("  FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("   INNER JOIN WE_PP_OPR_SIMU B ON A.FAC_SEQ = B.FAC_SEQ AND A.BIZ_PLA_SEQ = '1' AND A.PP_HOGI = '#4') P4 ON A.PP_OPR_SIMU_TIME = P4.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (");
                oStringBuilder.AppendLine("  SELECT B.PP_OPR_SIMU_TIME, B.PP_OPR_SIMU_DUR, B.PP_ORG_Q, B.PP_ORG_H, B.PP_ORG_E, B.PP_ORG_W, B.PP_ORG_U,");
                oStringBuilder.AppendLine("       B.PP_SIMU_Q, B.PP_SIMU_H, B.PP_SIMU_E, B.PP_SIMU_W, B.PP_SIMU_U");
                oStringBuilder.AppendLine("  FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("   INNER JOIN WE_PP_OPR_SIMU B ON A.FAC_SEQ = B.FAC_SEQ AND A.BIZ_PLA_SEQ = '1' AND A.PP_HOGI = '#5') P5 ON A.PP_OPR_SIMU_TIME = P5.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (");
                oStringBuilder.AppendLine("  SELECT B.PP_OPR_SIMU_TIME, B.PP_OPR_SIMU_DUR, B.PP_ORG_Q, B.PP_ORG_H, B.PP_ORG_E, B.PP_ORG_W, B.PP_ORG_U,");
                oStringBuilder.AppendLine("       B.PP_SIMU_Q, B.PP_SIMU_H, B.PP_SIMU_E, B.PP_SIMU_W, B.PP_SIMU_U");
                oStringBuilder.AppendLine("  FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("   INNER JOIN WE_PP_OPR_SIMU B ON A.FAC_SEQ = B.FAC_SEQ AND A.BIZ_PLA_SEQ = '1' AND A.PP_HOGI = '#6') P6 ON A.PP_OPR_SIMU_TIME = P6.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine(" ORDER BY DD, HM ASC");

                Clipboard.SetText(oStringBuilder.ToString());
                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                m_gridManager1.SetDataSource(oDataTable);
                #endregion

                #region Tab2
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT ");
                oStringBuilder.AppendLine("TO_CHAR(TO_DATE(SUBSTR(B.PP_OPR_SIMU_TIME, 1, 8), 'YYYYMMDD'), 'YYYY-MM-DD') AS DD,");
                oStringBuilder.AppendLine("TO_CHAR(TO_DATE(SUBSTR(B.PP_OPR_SIMU_TIME, 9, 4), 'HH24:MI'), 'HH24:MI') AS HM,   ");
                oStringBuilder.AppendLine("SUM(B.PP_OPR_SIMU_DUR) AS PP_OPR_SIMU_DUR, NVL(SUM(B.PP_ORG_Q),0) AS PP_ORG_Q, NVL(SUM(B.PP_ORG_H),0) AS PP_ORG_H, ");
                oStringBuilder.AppendLine("NVL(SUM(B.PP_ORG_E),0) AS PP_ORG_E, NVL(SUM(B.PP_ORG_W),0) AS PP_ORG_W, NVL(SUM(B.PP_ORG_U),0) AS PP_ORG_U,");
                oStringBuilder.AppendLine("NVL(SUM(B.PP_SIMU_Q),0) AS PP_SIMU_Q, NVL(SUM(B.PP_SIMU_H),0) AS PP_SIMU_H, NVL(SUM(B.PP_SIMU_E),0) AS PP_SIMU_E, ");
                oStringBuilder.AppendLine("NVL(SUM(B.PP_SIMU_W),0) AS PP_SIMU_W, NVL(SUM(B.PP_SIMU_U),0) AS PP_SIMU_U, ");
                oStringBuilder.AppendLine("(NVL(SUM(B.PP_SIMU_W),0) - NVL(SUM(B.PP_ORG_W),0)) * (SELECT BIZ_ENGWON FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')" + " AS SAVE_WON");
                oStringBuilder.AppendLine("FROM WE_PP_INFO A");
                oStringBuilder.AppendLine("  INNER JOIN WE_PP_OPR_SIMU B ON B.FAC_SEQ = A.FAC_SEQ ");
                oStringBuilder.AppendLine("     AND TO_DATE(SUBSTR(B.PP_OPR_SIMU_TIME,1,8),'YYYYMMDD') =  '" + FunctionManager.StringToDateTime((DateTime)ultraDateTimeEditor_Day.Value) + "'");
                oStringBuilder.AppendLine("     AND A.BIZ_PLA_SEQ =  '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("GROUP BY B.PP_OPR_SIMU_TIME");
                oStringBuilder.AppendLine("ORDER BY DD, HM ASC");

                Clipboard.SetText(oStringBuilder.ToString());
                DataTable oDataTable2 = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
                m_gridManager2.SetDataSource(oDataTable2);

                #endregion

            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oReader != null) oReader.Close();
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 사업장 콤보박스 SelectValue Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_BIZ_PLA_NM_SelectedValueChanged(object sender, EventArgs e)
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                //StringBuilder oStringBuilder = new StringBuilder();
                //oStringBuilder.AppendLine("SELECT A.FAC_SEQ, A.PP_HOGI");
                //oStringBuilder.AppendLine("FROM WE_PP_INFO A");
                //oStringBuilder.AppendLine("INNER JOIN WE_BIZ_PLA  B ON A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND B.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                //oStringBuilder.AppendLine("ORDER BY PP_HOGI ASC");

                //DataSet pDataset = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null);
                //DataTable pDataTable = pDataset.Tables[0];

                //ReInitializeGrid(pDataTable);
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }
    }
}
