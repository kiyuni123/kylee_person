﻿namespace WaterNet.WE_EnergyAnalysis
{
    partial class frmPumpSimulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            ChartFX.WinForms.Pane pane1 = new ChartFX.WinForms.Pane();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane1 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedLeft, new System.Guid("ef8f8ca6-772f-4fbe-afff-62759f9a5437"));
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane1 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("9ff75420-fa5e-4953-a039-faadcef54994"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("ef8f8ca6-772f-4fbe-afff-62759f9a5437"), -1);
            this.panelCommand = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCurve = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel_Chart = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.chart2 = new ChartFX.WinForms.Chart();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.splitContainerContents = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ultraGrid_SimuStatus = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraCalcManager1 = new Infragistics.Win.UltraWinCalcManager.UltraCalcManager(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ultraGrid_SimuEnergy = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ultraGrid_SimuFlow = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ultraGrid_SimuHead = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.ultraGrid_SimuPress = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.labelSimu = new System.Windows.Forms.Label();
            this.ultraGrid_HourFlow = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label_Flow = new System.Windows.Forms.Label();
            this.ultraGrid_ReservoirsHead = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lblReservoirsHead = new System.Windows.Forms.Label();
            this.DockManagerCommand = new Infragistics.Win.UltraWinDock.UltraDockManager(this.components);
            this._frmPumpSimulatorUnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulatorUnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulatorUnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulatorUnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulatorAutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
            this.dockableWindow1 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.windowDockingArea1 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnInfo = new System.Windows.Forms.Button();
            this.checkBox_Junction = new System.Windows.Forms.CheckBox();
            this.checkBox_Head = new System.Windows.Forms.CheckBox();
            this.checkBox_Energy = new System.Windows.Forms.CheckBox();
            this.checkBox_Flow = new System.Windows.Forms.CheckBox();
            this.btnGraph = new System.Windows.Forms.Button();
            this.btnTable = new System.Windows.Forms.Button();
            this.panelCommand.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel_Chart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.panelGrid.SuspendLayout();
            this.splitContainerContents.Panel1.SuspendLayout();
            this.splitContainerContents.Panel2.SuspendLayout();
            this.splitContainerContents.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuEnergy)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuFlow)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuHead)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuPress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_HourFlow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_ReservoirsHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmPumpSimulatorAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.button1);
            this.panelCommand.Controls.Add(this.btnCurve);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 18);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(66, 970);
            this.panelCommand.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(2, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 54);
            this.button1.TabIndex = 9;
            this.button1.Text = "커브정보";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCurve
            // 
            this.btnCurve.Location = new System.Drawing.Point(2, 60);
            this.btnCurve.Name = "btnCurve";
            this.btnCurve.Size = new System.Drawing.Size(62, 54);
            this.btnCurve.TabIndex = 6;
            this.btnCurve.Text = "커브정보";
            this.btnCurve.UseVisualStyleBackColor = true;
            this.btnCurve.Click += new System.EventHandler(this.btnCurve_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(21, 33);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel_Chart);
            this.splitContainer1.Panel1.Controls.Add(this.chart2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panelGrid);
            this.splitContainer1.Size = new System.Drawing.Size(840, 955);
            this.splitContainer1.SplitterDistance = 623;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 20;
            // 
            // panel_Chart
            // 
            this.panel_Chart.Controls.Add(this.chart1);
            this.panel_Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Chart.Location = new System.Drawing.Point(0, 0);
            this.panel_Chart.Name = "panel_Chart";
            this.panel_Chart.Size = new System.Drawing.Size(840, 449);
            this.panel_Chart.TabIndex = 30;
            // 
            // chart1
            // 
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.RandomData.Points = 24;
            this.chart1.RandomData.Series = 1;
            this.chart1.Size = new System.Drawing.Size(840, 449);
            this.chart1.TabIndex = 3;
            // 
            // chart2
            // 
            this.chart2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.chart2.Location = new System.Drawing.Point(0, 449);
            this.chart2.MainPane.Title.Text = "상동가압장";
            this.chart2.Name = "chart2";
            pane1.Title.Text = "회룡가압장";
            this.chart2.Panes.AddRange(new ChartFX.WinForms.Pane[] {
            this.chart2.MainPane,
            pane1});
            this.chart2.PlotAreaMargin.Top = -10;
            this.chart2.Size = new System.Drawing.Size(840, 174);
            this.chart2.TabIndex = 29;
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.splitContainerContents);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 0);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(840, 330);
            this.panelGrid.TabIndex = 19;
            // 
            // splitContainerContents
            // 
            this.splitContainerContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerContents.Location = new System.Drawing.Point(0, 0);
            this.splitContainerContents.Name = "splitContainerContents";
            this.splitContainerContents.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerContents.Panel1
            // 
            this.splitContainerContents.Panel1.Controls.Add(this.tabControl1);
            this.splitContainerContents.Panel1.Controls.Add(this.labelSimu);
            // 
            // splitContainerContents.Panel2
            // 
            this.splitContainerContents.Panel2.Controls.Add(this.ultraGrid_HourFlow);
            this.splitContainerContents.Panel2.Controls.Add(this.label_Flow);
            this.splitContainerContents.Panel2.Controls.Add(this.ultraGrid_ReservoirsHead);
            this.splitContainerContents.Panel2.Controls.Add(this.lblReservoirsHead);
            this.splitContainerContents.Size = new System.Drawing.Size(840, 330);
            this.splitContainerContents.SplitterDistance = 245;
            this.splitContainerContents.SplitterWidth = 1;
            this.splitContainerContents.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 20);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(840, 225);
            this.tabControl1.TabIndex = 46;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ultraGrid_SimuStatus);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(832, 199);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "펌프가동";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_SimuStatus
            // 
            this.ultraGrid_SimuStatus.CalcManager = this.ultraCalcManager1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_SimuStatus.DisplayLayout.Appearance = appearance22;
            this.ultraGrid_SimuStatus.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_SimuStatus.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_SimuStatus.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_SimuStatus.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.CellAppearance = appearance24;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.ultraGrid_SimuStatus.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.RowAppearance = appearance27;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_SimuStatus.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.ultraGrid_SimuStatus.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_SimuStatus.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_SimuStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_SimuStatus.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_SimuStatus.Name = "ultraGrid_SimuStatus";
            this.ultraGrid_SimuStatus.Size = new System.Drawing.Size(832, 199);
            this.ultraGrid_SimuStatus.TabIndex = 47;
            this.ultraGrid_SimuStatus.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ultraGrid_SimuStatus_InitializeRow);
            // 
            // ultraCalcManager1
            // 
            this.ultraCalcManager1.CalcFrequency = Infragistics.Win.UltraWinCalcManager.CalcFrequency.Synchronous;
            this.ultraCalcManager1.ContainingControl = this;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ultraGrid_SimuEnergy);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(832, 199);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "에너지(kWh)";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_SimuEnergy
            // 
            this.ultraGrid_SimuEnergy.CalcManager = this.ultraCalcManager1;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_SimuEnergy.DisplayLayout.Appearance = appearance15;
            this.ultraGrid_SimuEnergy.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_SimuEnergy.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_SimuEnergy.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_SimuEnergy.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.CardAreaAppearance = appearance16;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.CellAppearance = appearance17;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.CellPadding = 0;
            appearance18.BackColor = System.Drawing.SystemColors.Control;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.GroupByRowAppearance = appearance18;
            appearance19.TextHAlignAsString = "Left";
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.HeaderAppearance = appearance19;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.RowAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_SimuEnergy.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.ultraGrid_SimuEnergy.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_SimuEnergy.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_SimuEnergy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_SimuEnergy.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_SimuEnergy.Name = "ultraGrid_SimuEnergy";
            this.ultraGrid_SimuEnergy.Size = new System.Drawing.Size(832, 199);
            this.ultraGrid_SimuEnergy.TabIndex = 46;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ultraGrid_SimuFlow);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(832, 199);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "유량(㎥/h)";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_SimuFlow
            // 
            this.ultraGrid_SimuFlow.CalcManager = this.ultraCalcManager1;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_SimuFlow.DisplayLayout.Appearance = appearance1;
            this.ultraGrid_SimuFlow.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_SimuFlow.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_SimuFlow.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_SimuFlow.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.CardAreaAppearance = appearance2;
            appearance3.BorderColor = System.Drawing.Color.Silver;
            appearance3.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.CellAppearance = appearance3;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.CellPadding = 0;
            appearance4.BackColor = System.Drawing.SystemColors.Control;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.GroupByRowAppearance = appearance4;
            appearance5.TextHAlignAsString = "Left";
            this.ultraGrid_SimuFlow.DisplayLayout.Override.HeaderAppearance = appearance5;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.RowAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_SimuFlow.DisplayLayout.Override.TemplateAddRowAppearance = appearance7;
            this.ultraGrid_SimuFlow.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_SimuFlow.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_SimuFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_SimuFlow.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid_SimuFlow.Name = "ultraGrid_SimuFlow";
            this.ultraGrid_SimuFlow.Size = new System.Drawing.Size(826, 193);
            this.ultraGrid_SimuFlow.TabIndex = 44;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ultraGrid_SimuHead);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(832, 199);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "수위(m)";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_SimuHead
            // 
            this.ultraGrid_SimuHead.CalcManager = this.ultraCalcManager1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_SimuHead.DisplayLayout.Appearance = appearance8;
            this.ultraGrid_SimuHead.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_SimuHead.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_SimuHead.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_SimuHead.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_SimuHead.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_SimuHead.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuHead.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_SimuHead.DisplayLayout.Override.CellAppearance = appearance10;
            this.ultraGrid_SimuHead.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_SimuHead.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuHead.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.ultraGrid_SimuHead.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.ultraGrid_SimuHead.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_SimuHead.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_SimuHead.DisplayLayout.Override.RowAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_SimuHead.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.ultraGrid_SimuHead.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_SimuHead.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_SimuHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_SimuHead.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid_SimuHead.Name = "ultraGrid_SimuHead";
            this.ultraGrid_SimuHead.Size = new System.Drawing.Size(826, 193);
            this.ultraGrid_SimuHead.TabIndex = 45;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.ultraGrid_SimuPress);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(832, 199);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "수압(m)";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_SimuPress
            // 
            this.ultraGrid_SimuPress.CalcManager = this.ultraCalcManager1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_SimuPress.DisplayLayout.Appearance = appearance29;
            this.ultraGrid_SimuPress.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_SimuPress.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_SimuPress.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_SimuPress.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_SimuPress.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_SimuPress.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuPress.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_SimuPress.DisplayLayout.Override.CellAppearance = appearance31;
            this.ultraGrid_SimuPress.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_SimuPress.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_SimuPress.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.ultraGrid_SimuPress.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.ultraGrid_SimuPress.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_SimuPress.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_SimuPress.DisplayLayout.Override.RowAppearance = appearance34;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_SimuPress.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.ultraGrid_SimuPress.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_SimuPress.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_SimuPress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_SimuPress.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_SimuPress.Name = "ultraGrid_SimuPress";
            this.ultraGrid_SimuPress.Size = new System.Drawing.Size(832, 199);
            this.ultraGrid_SimuPress.TabIndex = 47;
            // 
            // labelSimu
            // 
            this.labelSimu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelSimu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSimu.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSimu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelSimu.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelSimu.Location = new System.Drawing.Point(0, 0);
            this.labelSimu.Name = "labelSimu";
            this.labelSimu.Size = new System.Drawing.Size(840, 20);
            this.labelSimu.TabIndex = 42;
            this.labelSimu.Text = "■ 모의해석 결과";
            this.labelSimu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ultraGrid_HourFlow
            // 
            this.ultraGrid_HourFlow.CalcManager = this.ultraCalcManager1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_HourFlow.DisplayLayout.Appearance = appearance36;
            this.ultraGrid_HourFlow.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_HourFlow.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_HourFlow.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_HourFlow.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_HourFlow.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_HourFlow.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_HourFlow.DisplayLayout.Override.CardAreaAppearance = appearance37;
            appearance38.BorderColor = System.Drawing.Color.Silver;
            appearance38.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_HourFlow.DisplayLayout.Override.CellAppearance = appearance38;
            this.ultraGrid_HourFlow.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_HourFlow.DisplayLayout.Override.CellPadding = 0;
            appearance39.BackColor = System.Drawing.SystemColors.Control;
            appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance39.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_HourFlow.DisplayLayout.Override.GroupByRowAppearance = appearance39;
            appearance40.TextHAlignAsString = "Left";
            this.ultraGrid_HourFlow.DisplayLayout.Override.HeaderAppearance = appearance40;
            this.ultraGrid_HourFlow.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_HourFlow.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_HourFlow.DisplayLayout.Override.RowAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_HourFlow.DisplayLayout.Override.TemplateAddRowAppearance = appearance42;
            this.ultraGrid_HourFlow.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_HourFlow.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_HourFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_HourFlow.Location = new System.Drawing.Point(0, 38);
            this.ultraGrid_HourFlow.Name = "ultraGrid_HourFlow";
            this.ultraGrid_HourFlow.Size = new System.Drawing.Size(840, 46);
            this.ultraGrid_HourFlow.TabIndex = 46;
            this.ultraGrid_HourFlow.Visible = false;
            // 
            // label_Flow
            // 
            this.label_Flow.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label_Flow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_Flow.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_Flow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label_Flow.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_Flow.Location = new System.Drawing.Point(0, 19);
            this.label_Flow.Name = "label_Flow";
            this.label_Flow.Size = new System.Drawing.Size(840, 19);
            this.label_Flow.TabIndex = 45;
            this.label_Flow.Text = "■ 순시유량((㎥/h))";
            this.label_Flow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label_Flow.Visible = false;
            // 
            // ultraGrid_ReservoirsHead
            // 
            this.ultraGrid_ReservoirsHead.CalcManager = this.ultraCalcManager1;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Appearance = appearance43;
            this.ultraGrid_ReservoirsHead.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_ReservoirsHead.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid_ReservoirsHead.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_ReservoirsHead.DisplayLayout.MaxRowScrollRegions = 1;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.CardAreaAppearance = appearance44;
            appearance45.BorderColor = System.Drawing.Color.Silver;
            appearance45.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.CellAppearance = appearance45;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.CellPadding = 0;
            appearance46.BackColor = System.Drawing.SystemColors.Control;
            appearance46.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance46.BackGradientAlignment = Infragistics.Win.GradientAlignment.Client;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance46.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.GroupByRowAppearance = appearance46;
            appearance47.TextHAlignAsString = "Left";
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.HeaderAppearance = appearance47;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            appearance48.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.RowAppearance = appearance48;
            appearance49.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_ReservoirsHead.DisplayLayout.Override.TemplateAddRowAppearance = appearance49;
            this.ultraGrid_ReservoirsHead.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_ReservoirsHead.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_ReservoirsHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_ReservoirsHead.Location = new System.Drawing.Point(0, 19);
            this.ultraGrid_ReservoirsHead.Name = "ultraGrid_ReservoirsHead";
            this.ultraGrid_ReservoirsHead.Size = new System.Drawing.Size(840, 65);
            this.ultraGrid_ReservoirsHead.TabIndex = 44;
            // 
            // lblReservoirsHead
            // 
            this.lblReservoirsHead.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblReservoirsHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReservoirsHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblReservoirsHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblReservoirsHead.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblReservoirsHead.Location = new System.Drawing.Point(0, 0);
            this.lblReservoirsHead.Name = "lblReservoirsHead";
            this.lblReservoirsHead.Size = new System.Drawing.Size(840, 19);
            this.lblReservoirsHead.TabIndex = 18;
            this.lblReservoirsHead.Text = "■ 흡입수두(m)";
            this.lblReservoirsHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.AutoHideDelay = 200;
            this.DockManagerCommand.CompressUnpinnedTabs = false;
            dockableControlPane1.Control = this.panelCommand;
            dockableControlPane1.FlyoutSize = new System.Drawing.Size(66, -1);
            dockableControlPane1.OriginalControlBounds = new System.Drawing.Rectangle(0, 0, 67, 583);
            dockableControlPane1.Pinned = false;
            dockableControlPane1.Size = new System.Drawing.Size(100, 100);
            dockableControlPane1.Text = "기능 펼치기";
            dockAreaPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane1});
            dockAreaPane1.Size = new System.Drawing.Size(95, 583);
            this.DockManagerCommand.DockAreas.AddRange(new Infragistics.Win.UltraWinDock.DockAreaPane[] {
            dockAreaPane1});
            this.DockManagerCommand.DragWindowStyle = Infragistics.Win.UltraWinDock.DragWindowStyle.LayeredWindow;
            this.DockManagerCommand.HostControl = this;
            this.DockManagerCommand.ShowCloseButton = false;
            this.DockManagerCommand.ShowDisabledButtons = false;
            this.DockManagerCommand.SplitterBarWidth = 2;
            // 
            // _frmPumpSimulatorUnpinnedTabAreaLeft
            // 
            this._frmPumpSimulatorUnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this._frmPumpSimulatorUnpinnedTabAreaLeft.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulatorUnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 0);
            this._frmPumpSimulatorUnpinnedTabAreaLeft.Name = "_frmPumpSimulatorUnpinnedTabAreaLeft";
            this._frmPumpSimulatorUnpinnedTabAreaLeft.Owner = this.DockManagerCommand;
            this._frmPumpSimulatorUnpinnedTabAreaLeft.Size = new System.Drawing.Size(21, 988);
            this._frmPumpSimulatorUnpinnedTabAreaLeft.TabIndex = 21;
            // 
            // _frmPumpSimulatorUnpinnedTabAreaRight
            // 
            this._frmPumpSimulatorUnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
            this._frmPumpSimulatorUnpinnedTabAreaRight.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulatorUnpinnedTabAreaRight.Location = new System.Drawing.Point(861, 0);
            this._frmPumpSimulatorUnpinnedTabAreaRight.Name = "_frmPumpSimulatorUnpinnedTabAreaRight";
            this._frmPumpSimulatorUnpinnedTabAreaRight.Owner = this.DockManagerCommand;
            this._frmPumpSimulatorUnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 988);
            this._frmPumpSimulatorUnpinnedTabAreaRight.TabIndex = 22;
            // 
            // _frmPumpSimulatorUnpinnedTabAreaTop
            // 
            this._frmPumpSimulatorUnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
            this._frmPumpSimulatorUnpinnedTabAreaTop.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulatorUnpinnedTabAreaTop.Location = new System.Drawing.Point(21, 0);
            this._frmPumpSimulatorUnpinnedTabAreaTop.Name = "_frmPumpSimulatorUnpinnedTabAreaTop";
            this._frmPumpSimulatorUnpinnedTabAreaTop.Owner = this.DockManagerCommand;
            this._frmPumpSimulatorUnpinnedTabAreaTop.Size = new System.Drawing.Size(840, 0);
            this._frmPumpSimulatorUnpinnedTabAreaTop.TabIndex = 23;
            // 
            // _frmPumpSimulatorUnpinnedTabAreaBottom
            // 
            this._frmPumpSimulatorUnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._frmPumpSimulatorUnpinnedTabAreaBottom.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulatorUnpinnedTabAreaBottom.Location = new System.Drawing.Point(21, 988);
            this._frmPumpSimulatorUnpinnedTabAreaBottom.Name = "_frmPumpSimulatorUnpinnedTabAreaBottom";
            this._frmPumpSimulatorUnpinnedTabAreaBottom.Owner = this.DockManagerCommand;
            this._frmPumpSimulatorUnpinnedTabAreaBottom.Size = new System.Drawing.Size(840, 0);
            this._frmPumpSimulatorUnpinnedTabAreaBottom.TabIndex = 24;
            // 
            // _frmPumpSimulatorAutoHideControl
            // 
            this._frmPumpSimulatorAutoHideControl.Controls.Add(this.dockableWindow1);
            this._frmPumpSimulatorAutoHideControl.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulatorAutoHideControl.Location = new System.Drawing.Point(21, 0);
            this._frmPumpSimulatorAutoHideControl.Name = "_frmPumpSimulatorAutoHideControl";
            this._frmPumpSimulatorAutoHideControl.Owner = this.DockManagerCommand;
            this._frmPumpSimulatorAutoHideControl.Size = new System.Drawing.Size(18, 988);
            this._frmPumpSimulatorAutoHideControl.TabIndex = 25;
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.Controls.Add(this.panelCommand);
            this.dockableWindow1.Location = new System.Drawing.Point(0, 0);
            this.dockableWindow1.Name = "dockableWindow1";
            this.dockableWindow1.Owner = this.DockManagerCommand;
            this.dockableWindow1.Size = new System.Drawing.Size(66, 988);
            this.dockableWindow1.TabIndex = 28;
            // 
            // windowDockingArea1
            // 
            this.windowDockingArea1.Dock = System.Windows.Forms.DockStyle.Left;
            this.windowDockingArea1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.windowDockingArea1.Location = new System.Drawing.Point(0, 0);
            this.windowDockingArea1.Name = "windowDockingArea1";
            this.windowDockingArea1.Owner = this.DockManagerCommand;
            this.windowDockingArea1.Size = new System.Drawing.Size(97, 583);
            this.windowDockingArea1.TabIndex = 26;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnInfo);
            this.panel1.Controls.Add(this.checkBox_Junction);
            this.panel1.Controls.Add(this.checkBox_Head);
            this.panel1.Controls.Add(this.checkBox_Energy);
            this.panel1.Controls.Add(this.checkBox_Flow);
            this.panel1.Controls.Add(this.btnGraph);
            this.panel1.Controls.Add(this.btnTable);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(21, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(840, 33);
            this.panel1.TabIndex = 27;
            // 
            // btnInfo
            // 
            this.btnInfo.Location = new System.Drawing.Point(4, 6);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(61, 24);
            this.btnInfo.TabIndex = 58;
            this.btnInfo.Text = "기본정보";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // checkBox_Junction
            // 
            this.checkBox_Junction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Junction.AutoSize = true;
            this.checkBox_Junction.Location = new System.Drawing.Point(633, 11);
            this.checkBox_Junction.Name = "checkBox_Junction";
            this.checkBox_Junction.Size = new System.Drawing.Size(48, 16);
            this.checkBox_Junction.TabIndex = 57;
            this.checkBox_Junction.Text = "수압";
            this.checkBox_Junction.UseVisualStyleBackColor = true;
            this.checkBox_Junction.CheckedChanged += new System.EventHandler(this.checkBox_Junction_CheckedChanged);
            // 
            // checkBox_Head
            // 
            this.checkBox_Head.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Head.AutoSize = true;
            this.checkBox_Head.Location = new System.Drawing.Point(577, 11);
            this.checkBox_Head.Name = "checkBox_Head";
            this.checkBox_Head.Size = new System.Drawing.Size(48, 16);
            this.checkBox_Head.TabIndex = 56;
            this.checkBox_Head.Text = "수위";
            this.checkBox_Head.UseVisualStyleBackColor = true;
            this.checkBox_Head.CheckedChanged += new System.EventHandler(this.checkBox_Head_CheckedChanged);
            // 
            // checkBox_Energy
            // 
            this.checkBox_Energy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Energy.AutoSize = true;
            this.checkBox_Energy.Checked = true;
            this.checkBox_Energy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_Energy.Location = new System.Drawing.Point(447, 11);
            this.checkBox_Energy.Name = "checkBox_Energy";
            this.checkBox_Energy.Size = new System.Drawing.Size(60, 16);
            this.checkBox_Energy.TabIndex = 55;
            this.checkBox_Energy.Text = "에너지";
            this.checkBox_Energy.UseVisualStyleBackColor = true;
            this.checkBox_Energy.CheckedChanged += new System.EventHandler(this.checkBox_Energy_CheckedChanged);
            // 
            // checkBox_Flow
            // 
            this.checkBox_Flow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Flow.AutoSize = true;
            this.checkBox_Flow.Checked = true;
            this.checkBox_Flow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_Flow.Location = new System.Drawing.Point(518, 11);
            this.checkBox_Flow.Name = "checkBox_Flow";
            this.checkBox_Flow.Size = new System.Drawing.Size(48, 16);
            this.checkBox_Flow.TabIndex = 54;
            this.checkBox_Flow.Text = "유량";
            this.checkBox_Flow.UseVisualStyleBackColor = true;
            this.checkBox_Flow.CheckedChanged += new System.EventHandler(this.checkBox_Flow_CheckedChanged);
            // 
            // btnGraph
            // 
            this.btnGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGraph.Location = new System.Drawing.Point(706, 3);
            this.btnGraph.Name = "btnGraph";
            this.btnGraph.Size = new System.Drawing.Size(59, 25);
            this.btnGraph.TabIndex = 53;
            this.btnGraph.Text = "그래프";
            this.btnGraph.UseVisualStyleBackColor = true;
            this.btnGraph.Click += new System.EventHandler(this.btnGraph_Click);
            // 
            // btnTable
            // 
            this.btnTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTable.Location = new System.Drawing.Point(769, 3);
            this.btnTable.Name = "btnTable";
            this.btnTable.Size = new System.Drawing.Size(59, 25);
            this.btnTable.TabIndex = 52;
            this.btnTable.Text = "테이블";
            this.btnTable.UseVisualStyleBackColor = true;
            this.btnTable.Click += new System.EventHandler(this.btnTable_Click);
            // 
            // frmPumpSimulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 988);
            this.Controls.Add(this._frmPumpSimulatorAutoHideControl);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.windowDockingArea1);
            this.Controls.Add(this._frmPumpSimulatorUnpinnedTabAreaTop);
            this.Controls.Add(this._frmPumpSimulatorUnpinnedTabAreaBottom);
            this.Controls.Add(this._frmPumpSimulatorUnpinnedTabAreaRight);
            this.Controls.Add(this._frmPumpSimulatorUnpinnedTabAreaLeft);
            this.Name = "frmPumpSimulator";
            this.Text = "펌프 에너지 시뮬레이터";
            this.Load += new System.EventHandler(this.frmPumpSimulator_Load);
            this.panelCommand.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel_Chart.ResumeLayout(false);
            this.panel_Chart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.panelGrid.ResumeLayout(false);
            this.splitContainerContents.Panel1.ResumeLayout(false);
            this.splitContainerContents.Panel2.ResumeLayout(false);
            this.splitContainerContents.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuEnergy)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuFlow)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuHead)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_SimuPress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_HourFlow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_ReservoirsHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmPumpSimulatorAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panelGrid;
        private System.Windows.Forms.SplitContainer splitContainerContents;
        private System.Windows.Forms.Label labelSimu;
        private System.Windows.Forms.Label lblReservoirsHead;
        private System.Windows.Forms.Button btnCurve;
        protected Infragistics.Win.UltraWinDock.UltraDockManager DockManagerCommand;
        private Infragistics.Win.UltraWinDock.AutoHideControl _frmPumpSimulatorAutoHideControl;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow1;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulatorUnpinnedTabAreaTop;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulatorUnpinnedTabAreaBottom;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulatorUnpinnedTabAreaLeft;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulatorUnpinnedTabAreaRight;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        public Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_ReservoirsHead;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage5;
        public Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_SimuFlow;
        public Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_SimuHead;
        public Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_SimuEnergy;
        public Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_SimuStatus;
        public Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_SimuPress;
        private Infragistics.Win.UltraWinCalcManager.UltraCalcManager ultraCalcManager1;
        private System.Windows.Forms.Button btnGraph;
        private System.Windows.Forms.Button btnTable;
        private System.Windows.Forms.Panel panel_Chart;
        public ChartFX.WinForms.Chart chart1;
        private ChartFX.WinForms.Chart chart2;
        public Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_HourFlow;
        private System.Windows.Forms.Label label_Flow;
        private System.Windows.Forms.CheckBox checkBox_Junction;
        private System.Windows.Forms.CheckBox checkBox_Head;
        private System.Windows.Forms.CheckBox checkBox_Energy;
        private System.Windows.Forms.CheckBox checkBox_Flow;
        private System.Windows.Forms.Button btnInfo;
    }
}