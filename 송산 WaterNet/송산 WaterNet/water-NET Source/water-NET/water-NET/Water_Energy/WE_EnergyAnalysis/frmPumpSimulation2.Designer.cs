﻿namespace WaterNet.WE_EnergyAnalysis
{
    partial class frmPumpSimulation2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane1 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedLeft, new System.Guid("ef8f8ca6-772f-4fbe-afff-62759f9a5437"));
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane1 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("9ff75420-fa5e-4953-a039-faadcef54994"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("ef8f8ca6-772f-4fbe-afff-62759f9a5437"), -1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPumpSimulation2));
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnSelModel = new System.Windows.Forms.Button();
            this.DockManagerCommand = new Infragistics.Win.UltraWinDock.UltraDockManager(this.components);
            this._frmPumpSimulationUnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulationUnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulationUnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulationUnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._frmPumpSimulationAutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
            this.dockableWindow1 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.windowDockingArea1 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.axMap1 = new ESRI.ArcGIS.Controls.AxMapControl();
            this.axToolbar1 = new ESRI.ArcGIS.Controls.AxToolbarControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.axMap2 = new ESRI.ArcGIS.Controls.AxMapControl();
            this.axToolbar2 = new ESRI.ArcGIS.Controls.AxToolbarControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmPumpSimulationAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axMap1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.panelCommand.Controls.Add(this.btnSelModel);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelCommand.Location = new System.Drawing.Point(0, 18);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(66, 472);
            this.panelCommand.TabIndex = 6;
            // 
            // btnSelModel
            // 
            this.btnSelModel.Location = new System.Drawing.Point(2, 7);
            this.btnSelModel.Name = "btnSelModel";
            this.btnSelModel.Size = new System.Drawing.Size(62, 54);
            this.btnSelModel.TabIndex = 6;
            this.btnSelModel.Text = "해석모델선택";
            this.btnSelModel.UseVisualStyleBackColor = true;
            this.btnSelModel.Click += new System.EventHandler(this.btnSelModel_Click);
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.AutoHideDelay = 300;
            this.DockManagerCommand.CompressUnpinnedTabs = false;
            dockableControlPane1.Control = this.panelCommand;
            dockableControlPane1.FlyoutSize = new System.Drawing.Size(66, -1);
            dockableControlPane1.OriginalControlBounds = new System.Drawing.Rectangle(0, 0, 67, 583);
            dockableControlPane1.Pinned = false;
            dockableControlPane1.Size = new System.Drawing.Size(100, 100);
            dockableControlPane1.Text = "기능 펼치기";
            dockAreaPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane1});
            dockAreaPane1.Size = new System.Drawing.Size(95, 583);
            this.DockManagerCommand.DockAreas.AddRange(new Infragistics.Win.UltraWinDock.DockAreaPane[] {
            dockAreaPane1});
            this.DockManagerCommand.DragWindowStyle = Infragistics.Win.UltraWinDock.DragWindowStyle.LayeredWindow;
            this.DockManagerCommand.HostControl = this;
            this.DockManagerCommand.LayoutStyle = Infragistics.Win.UltraWinDock.DockAreaLayoutStyle.FillContainer;
            this.DockManagerCommand.ShowCloseButton = false;
            this.DockManagerCommand.ShowDisabledButtons = false;
            this.DockManagerCommand.UnpinnedTabStyle = Infragistics.Win.UltraWinTabs.TabStyle.PropertyPageSelected;
            // 
            // _frmPumpSimulationUnpinnedTabAreaLeft
            // 
            this._frmPumpSimulationUnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this._frmPumpSimulationUnpinnedTabAreaLeft.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulationUnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 0);
            this._frmPumpSimulationUnpinnedTabAreaLeft.Name = "_frmPumpSimulationUnpinnedTabAreaLeft";
            this._frmPumpSimulationUnpinnedTabAreaLeft.Owner = this.DockManagerCommand;
            this._frmPumpSimulationUnpinnedTabAreaLeft.Size = new System.Drawing.Size(24, 490);
            this._frmPumpSimulationUnpinnedTabAreaLeft.TabIndex = 7;
            // 
            // _frmPumpSimulationUnpinnedTabAreaRight
            // 
            this._frmPumpSimulationUnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
            this._frmPumpSimulationUnpinnedTabAreaRight.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulationUnpinnedTabAreaRight.Location = new System.Drawing.Point(1065, 0);
            this._frmPumpSimulationUnpinnedTabAreaRight.Name = "_frmPumpSimulationUnpinnedTabAreaRight";
            this._frmPumpSimulationUnpinnedTabAreaRight.Owner = this.DockManagerCommand;
            this._frmPumpSimulationUnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 490);
            this._frmPumpSimulationUnpinnedTabAreaRight.TabIndex = 8;
            // 
            // _frmPumpSimulationUnpinnedTabAreaTop
            // 
            this._frmPumpSimulationUnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
            this._frmPumpSimulationUnpinnedTabAreaTop.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulationUnpinnedTabAreaTop.Location = new System.Drawing.Point(24, 0);
            this._frmPumpSimulationUnpinnedTabAreaTop.Name = "_frmPumpSimulationUnpinnedTabAreaTop";
            this._frmPumpSimulationUnpinnedTabAreaTop.Owner = this.DockManagerCommand;
            this._frmPumpSimulationUnpinnedTabAreaTop.Size = new System.Drawing.Size(1041, 0);
            this._frmPumpSimulationUnpinnedTabAreaTop.TabIndex = 9;
            // 
            // _frmPumpSimulationUnpinnedTabAreaBottom
            // 
            this._frmPumpSimulationUnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._frmPumpSimulationUnpinnedTabAreaBottom.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulationUnpinnedTabAreaBottom.Location = new System.Drawing.Point(24, 490);
            this._frmPumpSimulationUnpinnedTabAreaBottom.Name = "_frmPumpSimulationUnpinnedTabAreaBottom";
            this._frmPumpSimulationUnpinnedTabAreaBottom.Owner = this.DockManagerCommand;
            this._frmPumpSimulationUnpinnedTabAreaBottom.Size = new System.Drawing.Size(1041, 0);
            this._frmPumpSimulationUnpinnedTabAreaBottom.TabIndex = 10;
            // 
            // _frmPumpSimulationAutoHideControl
            // 
            this._frmPumpSimulationAutoHideControl.Controls.Add(this.dockableWindow1);
            this._frmPumpSimulationAutoHideControl.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this._frmPumpSimulationAutoHideControl.Location = new System.Drawing.Point(24, 0);
            this._frmPumpSimulationAutoHideControl.Name = "_frmPumpSimulationAutoHideControl";
            this._frmPumpSimulationAutoHideControl.Owner = this.DockManagerCommand;
            this._frmPumpSimulationAutoHideControl.Size = new System.Drawing.Size(71, 490);
            this._frmPumpSimulationAutoHideControl.TabIndex = 11;
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.Controls.Add(this.panelCommand);
            this.dockableWindow1.Location = new System.Drawing.Point(0, 0);
            this.dockableWindow1.Name = "dockableWindow1";
            this.dockableWindow1.Owner = this.DockManagerCommand;
            this.dockableWindow1.Size = new System.Drawing.Size(66, 490);
            this.dockableWindow1.TabIndex = 15;
            // 
            // windowDockingArea1
            // 
            this.windowDockingArea1.Dock = System.Windows.Forms.DockStyle.Left;
            this.windowDockingArea1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.windowDockingArea1.Location = new System.Drawing.Point(0, 0);
            this.windowDockingArea1.Name = "windowDockingArea1";
            this.windowDockingArea1.Owner = this.DockManagerCommand;
            this.windowDockingArea1.Size = new System.Drawing.Size(100, 583);
            this.windowDockingArea1.TabIndex = 12;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(24, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.axMap1);
            this.splitContainer1.Panel1.Controls.Add(this.axToolbar1);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.axMap2);
            this.splitContainer1.Panel2.Controls.Add(this.axToolbar2);
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Size = new System.Drawing.Size(1041, 490);
            this.splitContainer1.SplitterDistance = 521;
            this.splitContainer1.TabIndex = 14;
            // 
            // axMap1
            // 
            this.axMap1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axMap1.Location = new System.Drawing.Point(0, 55);
            this.axMap1.Name = "axMap1";
            this.axMap1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap1.OcxState")));
            this.axMap1.Size = new System.Drawing.Size(521, 435);
            this.axMap1.TabIndex = 15;
            this.axMap1.OnAfterDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterDrawEventHandler(this.axMap1_OnAfterDraw);
            // 
            // axToolbar1
            // 
            this.axToolbar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.axToolbar1.Location = new System.Drawing.Point(0, 27);
            this.axToolbar1.Name = "axToolbar1";
            this.axToolbar1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar1.OcxState")));
            this.axToolbar1.Size = new System.Drawing.Size(521, 28);
            this.axToolbar1.TabIndex = 14;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(521, 27);
            this.panel1.TabIndex = 0;
            // 
            // axMap2
            // 
            this.axMap2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axMap2.Location = new System.Drawing.Point(0, 55);
            this.axMap2.Name = "axMap2";
            this.axMap2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap2.OcxState")));
            this.axMap2.Size = new System.Drawing.Size(516, 435);
            this.axMap2.TabIndex = 15;
            this.axMap2.OnAfterDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterDrawEventHandler(this.axMap2_OnAfterDraw);
            // 
            // axToolbar2
            // 
            this.axToolbar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.axToolbar2.Location = new System.Drawing.Point(0, 27);
            this.axToolbar2.Name = "axToolbar2";
            this.axToolbar2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar2.OcxState")));
            this.axToolbar2.Size = new System.Drawing.Size(516, 28);
            this.axToolbar2.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(516, 27);
            this.panel2.TabIndex = 1;
            // 
            // frmPumpSimulation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 490);
            this.Controls.Add(this._frmPumpSimulationAutoHideControl);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.windowDockingArea1);
            this.Controls.Add(this._frmPumpSimulationUnpinnedTabAreaTop);
            this.Controls.Add(this._frmPumpSimulationUnpinnedTabAreaBottom);
            this.Controls.Add(this._frmPumpSimulationUnpinnedTabAreaLeft);
            this.Controls.Add(this._frmPumpSimulationUnpinnedTabAreaRight);
            this.Name = "frmPumpSimulation";
            this.Text = "frmPumpSimulation";
            this.panelCommand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmPumpSimulationAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axMap1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panelCommand;
        protected Infragistics.Win.UltraWinDock.UltraDockManager DockManagerCommand;
        private Infragistics.Win.UltraWinDock.AutoHideControl _frmPumpSimulationAutoHideControl;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow1;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulationUnpinnedTabAreaTop;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulationUnpinnedTabAreaBottom;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulationUnpinnedTabAreaLeft;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _frmPumpSimulationUnpinnedTabAreaRight;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea1;
        private System.Windows.Forms.Button btnSelModel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private ESRI.ArcGIS.Controls.AxMapControl axMap1;
        private ESRI.ArcGIS.Controls.AxToolbarControl axToolbar1;
        private ESRI.ArcGIS.Controls.AxMapControl axMap2;
        private ESRI.ArcGIS.Controls.AxToolbarControl axToolbar2;
    }
}