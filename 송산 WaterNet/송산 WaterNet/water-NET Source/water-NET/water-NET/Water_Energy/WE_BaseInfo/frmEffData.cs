﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Windows.Forms;

using ChartFX.WinForms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion

namespace WaterNet.WE_BaseInfo
{
    public partial class frmEffData : Form, WaterNet.WaterNetCore.IForminterface
    {
        #region Private Field ------------------------------------------------------------------------------
        private FormManager.SubFormEventType m_FormeventType = FormManager.SubFormEventType.isViewer;
        private DataSet m_DataSet = new DataSet();
        private string m_strQuerySelect = string.Empty;
        private string m_strQueryInsert = string.Empty;
        private string m_strQueryUpdate = string.Empty;

        #endregion

        #region public Field  -----------------------------------------------------------------------------------------

        #endregion

        #region 생성자 및 환경설정 ---------------------------------------------------------------------- 
        public frmEffData()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            splitContainer1.Panel1Collapsed = true;

            StringBuilder oStringBuilder = new StringBuilder();

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;
            
            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EFF_DATA_SEQ";
            oUltraGridColumn.Header.Caption = "측정자료일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_PLA_NM";
            oUltraGridColumn.Header.Caption = "사업장명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT BIZ_PLA_SEQ AS CODE, BIZ_PLA_NM AS VALUE FROM WE_BIZ_PLA");
            objValue = FormManager.SetValueList(oStringBuilder.ToString());
            oUltraGridColumn.ValueList = objValue;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_HOGI";
            oUltraGridColumn.Header.Caption = "호기번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            //objValue = new Infragistics.Win.ValueList();
            //oStringBuilder.Remove(0, oStringBuilder.Length);
            //oStringBuilder.AppendLine("SELECT DISTINCT FAC_SEQ AS CODE, PP_HOGI AS VALUE FROM WE_PP_INFO");
            //objValue = localFunctionManager.SetValueList(oStringBuilder.ToString());
            //oUltraGridColumn.ValueList = objValue;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FAC_SEQ";
            oUltraGridColumn.Header.Caption = "시설물장치일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EFF_YEAR";
            oUltraGridColumn.Header.Caption = "효율측정년도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EFF_CHK_DT";
            oUltraGridColumn.Header.Caption = "측정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EFFCHK_TM";
            oUltraGridColumn.Header.Caption = "측정시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TDH";
            oUltraGridColumn.Header.Caption = "양정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EIP";
            oUltraGridColumn.Header.Caption = "전력량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HE";
            oUltraGridColumn.Header.Caption = "효율";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VFR";
            oUltraGridColumn.Header.Caption = "유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ST";
            oUltraGridColumn.Header.Caption = "흡입관온도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DT";
            oUltraGridColumn.Header.Caption = "토출관온도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SSH";
            oUltraGridColumn.Header.Caption = "흡입양정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DSH";
            oUltraGridColumn.Header.Caption = "토출양정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DW";
            oUltraGridColumn.Header.Caption = "구동효율";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RS";
            oUltraGridColumn.Header.Caption = "회전속도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WU";
            oUltraGridColumn.Header.Caption = "원단위";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RSPD";
            oUltraGridColumn.Header.Caption = "비속도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NOTES";
            oUltraGridColumn.Header.Caption = "참고사항";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USERID";
            oUltraGridColumn.Header.Caption = "등록자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENTDT";
            oUltraGridColumn.Header.Caption = "등록일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGridWE_PP_EFF_DATA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHGDT";
            oUltraGridColumn.Header.Caption = "수정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            WaterNetCore.FormManager.SetGridStyle(ultraGridWE_PP_EFF_DATA);
            #endregion

            #region ComboBox 설정
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT BIZ_PLA_SEQ AS CD, BIZ_PLA_NM AS CDNM FROM WE_BIZ_PLA ORDER BY BIZ_PLA_NM ASC");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_BIZ_PLA_NM, oStringBuilder.ToString(), false);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT EFF_YEAR AS CD, EFF_YEAR AS CDNM FROM WE_PP_EFF_DATA ORDER BY EFF_YEAR ASC");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_EFF_YEAR, oStringBuilder.ToString(), true);
            #endregion

            #region Query문 설정
            //Select 문
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT");
            oStringBuilder.AppendLine("    D.BIZ_PLA_NM AS BIZ_PLA_NM, -- 측정자료일련번호");
            oStringBuilder.AppendLine("    C.PP_HOGI AS PP_HOGI, -- 측정자료일련번호");
            oStringBuilder.AppendLine("    A.EFF_DATA_SEQ AS EFF_DATA_SEQ, -- 측정자료일련번호");
            oStringBuilder.AppendLine("    A.FAC_SEQ AS FAC_SEQ, -- 시설물장치일련번호");
            oStringBuilder.AppendLine("    A.EFF_YEAR AS EFF_YEAR, -- 효율측정년도");
            oStringBuilder.AppendLine("    A.EFF_CHK_DT AS EFF_CHK_DT, -- 측정일자");
            oStringBuilder.AppendLine("    A.EFFCHK_TM AS EFFCHK_TM, -- 측정시간");
            oStringBuilder.AppendLine("    A.TDH AS TDH, -- 양정");
            oStringBuilder.AppendLine("    A.EIP AS EIP, -- 전력량");
            oStringBuilder.AppendLine("    A.HE AS HE, -- 효율");
            oStringBuilder.AppendLine("    A.VFR AS VFR, -- 유량");
            oStringBuilder.AppendLine("    A.ST AS ST, -- 흡입관온도");
            oStringBuilder.AppendLine("    A.DT AS DT, -- 토출관온도");
            oStringBuilder.AppendLine("    A.SSH AS SSH, -- 흡입양정");
            oStringBuilder.AppendLine("    A.DSH AS DSH, -- 토출양정");
            oStringBuilder.AppendLine("    A.DW AS DW, -- 구동효율");
            oStringBuilder.AppendLine("    A.RS AS RS, -- 회전속도");
            oStringBuilder.AppendLine("    A.WU AS WU, -- 원단위");
            oStringBuilder.AppendLine("    A.RSPD AS RSPD, -- 비속도");
            oStringBuilder.AppendLine("    A.NOTES AS NOTES, -- 참고사항");
            oStringBuilder.AppendLine("    A.USERID AS USERID, -- 등록자");
            oStringBuilder.AppendLine("    A.ENTDT AS ENTDT, -- 등록일자");
            oStringBuilder.AppendLine("    A.CHGDT AS CHGDT -- 수정일자");
            oStringBuilder.AppendLine("FROM WE_PP_EFF_DATA A -- ");
            oStringBuilder.AppendLine(" INNER JOIN WE_PP_INFO   C ON A.FAC_SEQ =      C.FAC_SEQ");
            oStringBuilder.AppendLine(" INNER JOIN WE_BIZ_PLA   D ON C.BIZ_PLA_SEQ = D.BIZ_PLA_SEQ");
            m_strQuerySelect = oStringBuilder.ToString();

            //Insert 문
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO WE_PP_EFF_DATA (");
            oStringBuilder.AppendLine("EFF_DATA_SEQ,");
            oStringBuilder.AppendLine("FAC_SEQ,");
            oStringBuilder.AppendLine("EFF_YEAR,");
            oStringBuilder.AppendLine("EFF_CHK_DT,");
            oStringBuilder.AppendLine("EFFCHK_TM,");
            oStringBuilder.AppendLine("TDH,");
            oStringBuilder.AppendLine("EIP,");
            oStringBuilder.AppendLine("HE,");
            oStringBuilder.AppendLine("VFR,");
            oStringBuilder.AppendLine("ST,");
            oStringBuilder.AppendLine("DT,");
            oStringBuilder.AppendLine("SSH,");
            oStringBuilder.AppendLine("DSH,");
            oStringBuilder.AppendLine("DW,");
            oStringBuilder.AppendLine("RS,");
            oStringBuilder.AppendLine("WU,");
            oStringBuilder.AppendLine("RSPD,");
            oStringBuilder.AppendLine("NOTES,");
            oStringBuilder.AppendLine("USERID,");
            oStringBuilder.AppendLine("ENTDT)");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine(" :EFF_DATA_SEQ,");
            oStringBuilder.AppendLine(" :FAC_SEQ,");
            oStringBuilder.AppendLine(" :EFF_YEAR,");
            oStringBuilder.AppendLine(" :EFF_CHK_DT,");
            oStringBuilder.AppendLine(" :EFFCHK_TM,");
            oStringBuilder.AppendLine(" :TDH,");
            oStringBuilder.AppendLine(" :EIP,");
            oStringBuilder.AppendLine(" :HE,");
            oStringBuilder.AppendLine(" :VFR,");
            oStringBuilder.AppendLine(" :ST,");
            oStringBuilder.AppendLine(" :DT,");
            oStringBuilder.AppendLine(" :SSH,");
            oStringBuilder.AppendLine(" :DSH,");
            oStringBuilder.AppendLine(" :DW,");
            oStringBuilder.AppendLine(" :RS,");
            oStringBuilder.AppendLine(" :WU,");
            oStringBuilder.AppendLine(" :RSPD,");
            oStringBuilder.AppendLine(" :NOTES,");
            oStringBuilder.AppendLine(" :USERID,");
            oStringBuilder.AppendLine(" :ENTDT)");

            m_strQueryInsert = oStringBuilder.ToString();

            //----------------------------------------------------------------------------------------------------
            //Update 문
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("UPDATE WE_PP_EFF_DATA SET ");
            oStringBuilder.AppendLine("FAC_SEQ = :FAC_SEQ,");
            oStringBuilder.AppendLine("EFF_YEAR = :EFF_YEAR,");
            oStringBuilder.AppendLine("EFF_CHK_DT = :EFF_CHK_DT,");
            oStringBuilder.AppendLine("EFFCHK_TM = :EFFCHK_TM,");
            oStringBuilder.AppendLine("TDH = :TDH,");
            oStringBuilder.AppendLine("EIP = :EIP,");
            oStringBuilder.AppendLine("HE = :HE,");
            oStringBuilder.AppendLine("VFR = :VFR,");
            oStringBuilder.AppendLine("ST = :ST,");
            oStringBuilder.AppendLine("DT = :DT,");
            oStringBuilder.AppendLine("SSH = :SSH,");
            oStringBuilder.AppendLine("DSH = :DSH,");
            oStringBuilder.AppendLine("DW = :DW,");
            oStringBuilder.AppendLine("RS = :RS,");
            oStringBuilder.AppendLine("WU = :WU,");
            oStringBuilder.AppendLine("RSPD = :RSPD,");
            oStringBuilder.AppendLine("NOTES = :NOTES,");
            oStringBuilder.AppendLine("USERID = :USERID,");
            oStringBuilder.AppendLine("CHGDT = :CHGDT");
            oStringBuilder.AppendLine("WHERE EFF_DATA_SEQ = :EFF_DATA_SEQ");
            m_strQueryUpdate = oStringBuilder.ToString();
            #endregion
        }
        #endregion

        private void frmEffData_Load(object sender, EventArgs e)
        {
            //그리드의 Insert, Update구분 Event
            ultraGridWE_PP_EFF_DATA.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            ultraGridWE_PP_EFF_DATA.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);

            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================에너지관리

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuEffData"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnAdd.Enabled = false;
                this.btnSave.Enabled = false;
                this.btnEdit.Enabled = false;
                this.btnDel.Enabled = false;
            }

            //===========================================================================

        }

        public void Open()
        {
            //comboBox_BIZ_PLA_NM_SelectedIndexChanged(this, new EventArgs());
            btnSel_Click(this, new EventArgs());
        }

        #endregion

        #region IForminterface 멤버

        public string FormID
        {
            get { return "에너지관리-펌프효율"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        #region Property -------------------------------------------------------------------------------------

        #endregion

        #region Private Method -------------------------------------------------------------------------------------
        ////////////////콤포넌트////////////////////////////////////////////////////////////////////////////////////////////////////
        private void comboBox_BIZ_PLA_NM_SelectedIndexChanged(object sender, EventArgs e)
        {
            //StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("SELECT");
            //oStringBuilder.AppendLine("    B.FAC_SEQ AS CD, -- 시설물장치일련번호");
            //oStringBuilder.AppendLine("    B.PP_HOGI AS CDNM -- 호기");
            //oStringBuilder.AppendLine("FROM WE_FAC_INFO A -- ");
            //oStringBuilder.AppendLine(" INNER JOIN WE_PP_INFO  B ON A.FAC_SEQ =      B.FAC_SEQ");
            //oStringBuilder.AppendLine(" INNER JOIN WE_BIZ_PLA   C ON A.BIZ_PLA_SEQ = C.BIZ_PLA_SEQ");
            //oStringBuilder.AppendLine(" WHERE C.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue.ToString() + "'");
            
            //WaterNetCore.FormManager.SetComboBoxEX(comboBox_PP_HOGI, oStringBuilder.ToString(), true);
        }


        private void comboBox_BIZ_PLA_NM_SelectedValueChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT");
            oStringBuilder.AppendLine("    A.FAC_SEQ AS CD, -- 시설물장치일련번호");
            oStringBuilder.AppendLine("    A.PP_HOGI AS CDNM -- 호기");
            oStringBuilder.AppendLine("FROM WE_PP_INFO A -- ");
            oStringBuilder.AppendLine(" INNER JOIN WE_BIZ_PLA   C ON A.BIZ_PLA_SEQ = C.BIZ_PLA_SEQ");
            oStringBuilder.AppendLine(" WHERE C.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue.ToString() + "'");

            WaterNetCore.FormManager.SetComboBoxEX(comboBox_PP_HOGI, oStringBuilder.ToString(), true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
                if (eDialogResult != DialogResult.Yes) return;

                OracleDBManager oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

                try
                {
                    oDBManager.Open();
                    oDBManager.BeginTransaction();

                    foreach (UltraGridRow item in ultraGridWE_PP_EFF_DATA.Rows)
                    {
                        if (m_FormeventType == FormManager.SubFormEventType.isAppend) //추가모드
                        {
                            InsertProcess(ref oDBManager, item);
                        }
                        else
                        {
                            if (Convert.ToString(item.Tag) == "I")
                            {
                                InsertProcess(ref oDBManager, item);
                            }
                            else if (Convert.ToString(item.Tag) == "U")
                            {
                                UpdateProcess(ref oDBManager, item);
                            }
                        }
                    }

                    m_FormeventType = FormManager.SubFormEventType.isViewer;
                    oDBManager.CommitTransaction();
                    MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);

                }
                catch (Exception oException)
                {
                    oDBManager.RollbackTransaction();
                    MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                    throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
                }
                finally
                {
                    oDBManager.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            UltraGridRow row = ultraGridWE_PP_EFF_DATA.ActiveRow;
            if (row == null)
            {
                MessageManager.ShowInformationMessage("선택된 목록이 없습니다.");
                return;
            }

            if (MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_DELETE) != DialogResult.Yes) return;

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("DELETE FROM WE_PP_EFF_DATA");
            oStringBuilder.AppendLine("WHERE EFF_DATA_SEQ = '" + row.Cells["EFF_DATA_SEQ"].Value + "'");

            try
            {
                int i = FunctionManager.ExecuteScript(oStringBuilder.ToString(), null);
                if (i >= 0) row.Delete(false);
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);
            }
            catch (Exception ex)
            {
#if DEBUG
                MessageBox.Show(ex.Message);
#endif
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormManager.SetGridStyle_Append(ultraGridWE_PP_EFF_DATA);

            //setCommandStatus();
        }


        private void btnEdit_Click(object sender, EventArgs e)
        {
            FormManager.SetGridStyle_Update(ultraGridWE_PP_EFF_DATA);
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog m_ofdExcel = new OpenFileDialog();
            m_ofdExcel.Filter = "Excel Worksheets|*.xls";
            m_ofdExcel.CheckFileExists = false;
            m_ofdExcel.Multiselect = false;
            if (m_ofdExcel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //ExcelManager m_ExcelApp = new ExcelManager();
                //m_ExcelApp.OpenFile(m_ofdExcel.FileName);
                //if(!m_ExcelApp.FindWorkSheet("효율자료"))
                //{
                //    WaterNetCore.MessageManager.ShowInformationMessage("효율자료 Sheet가 없거나, 잘못된 엑셀파일입니다.");
                //    return;
                //}

                System.Data.DataTable oDataTable = m_dataSet.Tables[0];// new System.Data.DataTable();
                oDataTable.Clear();
                //oDataTable.Columns.Add(new DataColumn("EFF_DATA_SEQ", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("FAC_SEQ", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("EFF_YEAR", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("EFF_CHK_DT", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("EFFCHK_TM", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("TDH", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("EIP", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("HE", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("VFR", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("ST", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("DT", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("SSH", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("DSH", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("DW", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("RS", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("WU", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("RSPD", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("NOTES", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("USERID", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("ENTDT", Type.GetType("System.String")));
                //oDataTable.Columns.Add(new DataColumn("CHGDT", Type.GetType("System.String")));

                Cursor = Cursors.WaitCursor;
                //MessageBox.Show(m_ExcelApp.GetUsedRange().ToString());
                //Excel.Range pRange = m_ExcelApp.GetUsedRange();
                //string sRange = m_ExcelApp.ConvertToRange(pRange.Rows.Count, pRange.Columns.Count);
                //Array pData = m_ExcelApp.GetRangeValue("A7:" + sRange);
                //m_ExcelApp.GetDataTable(ref oDataTable);
                OleDbDataReader pReader = null;
                ExcelOleManager oExcelManager = new ExcelOleManager();
                try
                {
                    oExcelManager.ExcelFilename = m_ofdExcel.FileName;
                    oExcelManager.SheetName = "효율자료";
                    oExcelManager.KeepConnectionOpen = false;
                    oExcelManager.Headers = false;
                    oExcelManager.MixedData = true;
                    oExcelManager.SheetRange = "A7:U65536";
                    StringBuilder oStringBuilder = new StringBuilder();

                    oExcelManager.Open();
                    pReader = oExcelManager.ExecuteDataReader();

                    //string connectionString = "Provider=Microsoft.JET.OLEDB.4.0;data source=" + m_ofdExcel.FileName + ";" + "Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";
                    //OleDbConnection oleConn = new OleDbConnection(connectionString);
                    //oleConn.Open();
                    //OleDbCommand oleCmdSelect = new OleDbCommand(
                    //                        @"SELECT * FROM ["
                    //                        + "효율자료"
                    //                        + "$" + "A7:U65500"
                    //                        + "]", oleConn);

                    //OleDbDataReader pReader = oleCmdSelect.ExecuteReader();
                    oDataTable.BeginLoadData();
                    while (pReader.Read())
                    {
                        if (pReader.IsDBNull(0)) continue;

                        oStringBuilder.Remove(0, oStringBuilder.Length);
                        oStringBuilder.AppendLine("SELECT * FROM WE_BIZ_PLA");
                        oStringBuilder.AppendLine("WHERE BIZ_PLA_NM = '" + Convert.ToString(pReader.GetValue(0)) + "'");
                        int i = FunctionManager.ExecuteScriptCount(oStringBuilder.ToString(), null);
                        if (i == 0)
                        {
                            MessageManager.ShowInformationMessage(Convert.ToString(pReader.GetValue(0)) + " 없습니다.");
                            continue;
                        }

                        oStringBuilder.Remove(0, oStringBuilder.Length);
                        oStringBuilder.AppendLine("SELECT A.FAC_SEQ");
                        oStringBuilder.AppendLine("FROM WE_PP_INFO A");
                        oStringBuilder.AppendLine("INNER JOIN WE_BIZ_PLA C ON A.BIZ_PLA_SEQ = C.BIZ_PLA_SEQ");
                        oStringBuilder.AppendLine("WHERE C.BIZ_PLA_NM = '" + Convert.ToString(pReader.GetValue(0)) + "'");
                        oStringBuilder.AppendLine("   AND A.PP_HOGI = '" + Convert.ToString(pReader.GetValue(1)) + "'");

                        object o = FunctionManager.ExecuteScriptScalar(oStringBuilder.ToString(), null);
                        if (o == null)
                        {
                            //MessageManager.ShowInformationMessage(Convert.ToString(pReader.GetValue(0)) + " 없습니다.");
                            continue;
                        }

                        DataRow oRow = oDataTable.NewRow();
                        oRow["BIZ_PLA_NM"] = Convert.ToString(pReader.GetValue(0));
                        oRow["PP_HOGI"] = Convert.ToString(pReader.GetValue(1));
                        oRow["FAC_SEQ"] = Convert.ToString(o);
                        oRow["EFF_YEAR"] = Convert.ToString(pReader.GetValue(2)).Substring(0, 4);
                        oRow["EFF_CHK_DT"] = Convert.ToString(pReader.GetValue(2));
                        oRow["EFFCHK_TM"] = Convert.ToString(pReader.GetValue(3));
                        oRow["TDH"] = Convert.ToString(pReader.GetValue(4));
                        oRow["EIP"] = Convert.ToString(pReader.GetValue(5));
                        oRow["HE"] = Convert.ToString(pReader.GetValue(6));
                        oRow["VFR"] = Convert.ToString(pReader.GetValue(7));
                        oRow["ST"] = Convert.ToString(pReader.GetValue(8));
                        oRow["DT"] = Convert.ToString(pReader.GetValue(9));
                        oRow["SSH"] = Convert.ToString(pReader.GetValue(10));
                        oRow["DSH"] = Convert.ToString(pReader.GetValue(11));
                        oRow["DW"] = Convert.ToString(pReader.GetValue(12));
                        oRow["RS"] = Convert.ToString(pReader.GetValue(15));
                        oRow["WU"] = Convert.ToString(pReader.GetValue(16));
                        oRow["RSPD"] = Convert.ToString(pReader.GetValue(19));
                        oRow["NOTES"] = Convert.ToString(pReader.GetValue(20));

                        oDataTable.Rows.Add(oRow);

                    }
                    oDataTable.EndLoadData();
                    ultraGridWE_PP_EFF_DATA.DataSource = oDataTable;

                    m_FormeventType = FormManager.SubFormEventType.isAppend;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    pReader.Close();
                    pReader.Dispose();
                    oExcelManager.Close();
                    Cursor = Cursors.Default;
                }
            }
        }

        private void btnSel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string strWhere = getStrWhere();
                Select_WE_PP_EFF_DATA(strWhere);

                //DrawChartData();

                m_FormeventType = FormManager.SubFormEventType.isViewer;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawChartData()
        {
            this.chart1.Series.Clear();
            this.chart1.DataSourceSettings.Fields.Clear();

            this.chart1.DataSourceSettings.Fields.Add(new FieldMap("TDH", FieldUsage.Value));
            this.chart1.DataSourceSettings.Fields.Add(new FieldMap("EIP", FieldUsage.Value));
            this.chart1.DataSourceSettings.Fields.Add(new FieldMap("HE", FieldUsage.Value));
            this.chart1.DataSourceSettings.Fields.Add(new FieldMap("VFR", FieldUsage.Value));
            this.chart1.DataSourceSettings.Fields.Add(new FieldMap("WU", FieldUsage.Value));
            this.chart1.DataSourceSettings.Fields.Add(new FieldMap("EFF_CHK_DT", FieldUsage.Label));

            this.chart1.DataSourceSettings.DataSource = ultraGridWE_PP_EFF_DATA.DataSource;
            
            this.chart1.Series[0].Text = "1호기";
            //this.chart1.Series[1].Text = "2호기";
            //this.chart1.Series[2].Text = "3호기";
            //this.chart1.Series[3].Text = "4호기";
            //this.chart1.Series[4].Text = "5호기";
            //this.chart1.Series[5].Text = "6호기";
            this.chart1.Series[0].Visible = true;
            //this.chart1.Series[1].Visible = true;
            //this.chart1.Series[2].Visible = true;
            //this.chart1.Series[3].Visible = true;
            //this.chart1.Series[4].Visible = true;
            //this.chart1.Series[5].Visible = true;
        }

        ////////////////데이터베이스 Query////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Insert Process
        /// </summary>
        /// <returns></returns>
        private void InsertProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
#if DEBUG
            //Console.WriteLine(Convert.ToString(oRow.Cells["FAC_SEQ"].Value) + " : " + Convert.ToString(oRow.Tag));        
#endif
            IDataParameter[] oParams = {
                new OracleParameter(":EFF_DATA_SEQ",OracleDbType.Varchar2,16),
                new OracleParameter(":FAC_SEQ",OracleDbType.Varchar2,16),
                new OracleParameter(":EFF_YEAR",OracleDbType.Varchar2,4),
                new OracleParameter(":EFF_CHK_DT",OracleDbType.Varchar2,10),
                new OracleParameter(":EFFCHK_TM",OracleDbType.Varchar2,5),
                new OracleParameter(":TDH",OracleDbType.Varchar2,16),
                new OracleParameter(":EIP",OracleDbType.Varchar2,16),
                new OracleParameter(":HE",OracleDbType.Varchar2,16),
                new OracleParameter(":VFR",OracleDbType.Varchar2,16),
                new OracleParameter(":ST",OracleDbType.Varchar2,16),
                new OracleParameter(":DT",OracleDbType.Varchar2,16),
                new OracleParameter(":SSH",OracleDbType.Varchar2,16),
                new OracleParameter(":DSH",OracleDbType.Varchar2,16),
                new OracleParameter(":DW",OracleDbType.Varchar2,16),
                new OracleParameter(":RS",OracleDbType.Varchar2,16),
                new OracleParameter(":WU",OracleDbType.Varchar2,16),
                new OracleParameter(":RSPD",OracleDbType.Varchar2,16),
                new OracleParameter(":NOTES",OracleDbType.Varchar2,100),
                new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                new OracleParameter(":ENTDT",OracleDbType.Varchar2,16)
            };

            System.Int32 pSeq = oDBManager.GetSequenceID("WE_PP_EFF_DATA", "EFF_DATA_SEQ");

            oParams[0].Value = Convert.ToString(pSeq); // "EFF_DATA_SEQ";       //측정자료일련번호
            oParams[1].Value = oRow.Cells["FAC_SEQ"].Value;       //시설물장치일련번호
            oParams[2].Value = oRow.Cells["EFF_YEAR"].Value;       //효율측정년도
            oParams[3].Value = oRow.Cells["EFF_CHK_DT"].Value;       //측정일자
            oParams[4].Value = oRow.Cells["EFFCHK_TM"].Value;       //측정시간
            oParams[5].Value = oRow.Cells["TDH"].Value;       //양정
            oParams[6].Value = oRow.Cells["EIP"].Value;       //전력량
            oParams[7].Value = oRow.Cells["HE"].Value;       //효율
            oParams[8].Value = oRow.Cells["VFR"].Value;       //유량
            oParams[9].Value = oRow.Cells["ST"].Value;       //흡입관온도
            oParams[10].Value = oRow.Cells["DT"].Value;       //토출관온도
            oParams[11].Value = oRow.Cells["SSH"].Value;       //흡입양정
            oParams[12].Value = oRow.Cells["DSH"].Value;       //토출양정
            oParams[13].Value = oRow.Cells["DW"].Value;       //구동효율
            oParams[14].Value = oRow.Cells["RS"].Value;       //회전속도
            oParams[15].Value = oRow.Cells["WU"].Value;       //원단위
            oParams[16].Value = oRow.Cells["RSPD"].Value;       //비속도
            oParams[17].Value = oRow.Cells["NOTES"].Value;       //참고사항
            oParams[18].Value = EMFrame.statics.AppStatic.USER_ID;    // "USERID";       //등록자
            oParams[19].Value = FunctionManager.GetDBDate();   //"ENTDT";       //등록일자

            oDBManager.ExecuteScript(m_strQueryInsert, oParams);
        }

        /// <summary>
        /// UpdateProcess
        /// </summary>
        /// <returns></returns>
        private void UpdateProcess(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
#if DEBUG
            //Console.WriteLine(Convert.ToString(oRow.Cells["FAC_SEQ"].Value) + " : " + Convert.ToString(oRow.Tag));
#endif
            IDataParameter[] oParams = {
                new OracleParameter(":FAC_SEQ",OracleDbType.Varchar2,16),
                new OracleParameter(":EFF_YEAR",OracleDbType.Varchar2,4),
                new OracleParameter(":EFF_CHK_DT",OracleDbType.Varchar2,10),
                new OracleParameter(":EFFCHK_TM",OracleDbType.Varchar2,5),
                new OracleParameter(":TDH",OracleDbType.Varchar2,16),
                new OracleParameter(":EIP",OracleDbType.Varchar2,16),
                new OracleParameter(":HE",OracleDbType.Varchar2,16),
                new OracleParameter(":VFR",OracleDbType.Varchar2,16),
                new OracleParameter(":ST",OracleDbType.Varchar2,16),
                new OracleParameter(":DT",OracleDbType.Varchar2,16),
                new OracleParameter(":SSH",OracleDbType.Varchar2,16),
                new OracleParameter(":DSH",OracleDbType.Varchar2,16),
                new OracleParameter(":DW",OracleDbType.Varchar2,16),
                new OracleParameter(":RS",OracleDbType.Varchar2,16),
                new OracleParameter(":WU",OracleDbType.Varchar2,16),
                new OracleParameter(":RSPD",OracleDbType.Varchar2,16),
                new OracleParameter(":NOTES",OracleDbType.Varchar2,100),
                new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                new OracleParameter(":CHGDT",OracleDbType.Varchar2,16),
                new OracleParameter(":EFF_DATA_SEQ",OracleDbType.Varchar2,16)
            };

            oParams[0].Value = oRow.Cells["FAC_SEQ"].Value;       //시설물장치일련번호
            oParams[1].Value = oRow.Cells["EFF_YEAR"].Value;       //효율측정년도
            oParams[2].Value = oRow.Cells["EFF_CHK_DT"].Value;       //측정일자
            oParams[3].Value = oRow.Cells["EFFCHK_TM"].Value;       //측정시간
            oParams[4].Value = oRow.Cells["TDH"].Value;       //양정
            oParams[5].Value = oRow.Cells["EIP"].Value;       //전력량
            oParams[6].Value = oRow.Cells["HE"].Value;       //효율
            oParams[7].Value = oRow.Cells["VFR"].Value;       //유량
            oParams[8].Value = oRow.Cells["ST"].Value;       //흡입관온도
            oParams[9].Value = oRow.Cells["DT"].Value;       //토출관온도
            oParams[10].Value = oRow.Cells["SSH"].Value;       //흡입양정
            oParams[11].Value = oRow.Cells["DSH"].Value;       //토출양정
            oParams[12].Value = oRow.Cells["DW"].Value;       //구동효율
            oParams[13].Value = oRow.Cells["RS"].Value;       //회전속도
            oParams[14].Value = oRow.Cells["WU"].Value;       //원단위
            oParams[15].Value = oRow.Cells["RSPD"].Value;       //비속도
            oParams[16].Value = oRow.Cells["NOTES"].Value;       //참고사항
            oParams[17].Value = EMFrame.statics.AppStatic.USER_ID;    // "USERID";       //등록자
            oParams[18].Value = FunctionManager.GetDBDate();   //"ENTDT";       //등록일자
            oParams[19].Value = oRow.Cells["EFF_DATA_SEQ"].Value; // "EFF_DATA_SEQ";       //측정자료일련번호

            oDBManager.ExecuteScript(m_strQueryUpdate, oParams);
        }

        /// <summary>
        /// 그리드 질의
        /// </summary>
        /// <param name="strWhere"></param>
        private void Select_WE_PP_EFF_DATA(string strWhere)
        {
            string strQuery = m_strQuerySelect;

            if (!(string.IsNullOrEmpty(strWhere)))
            {
                strQuery += " WHERE " + strWhere;
            }
            //Clipboard.SetText(strQuery);
            m_dataSet = FunctionManager.GetDataSet(strQuery, "WE_PP_EFF_DATA");

            ultraGridWE_PP_EFF_DATA.DataSource = m_dataSet.Tables["WE_PP_EFF_DATA"].DefaultView;
        }

        /// <summary>
        /// Query Where 조건절 생성
        /// </summary>
        /// <returns></returns>
        private string getStrWhere()
        {
            string strWhere = string.Empty;
            if (comboBox_BIZ_PLA_NM.SelectedValue != null)
            {
                if (!(string.IsNullOrEmpty(comboBox_BIZ_PLA_NM.SelectedValue.ToString())))
                {
                    if (string.IsNullOrEmpty(strWhere))
                    {
                        strWhere = " D.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue.ToString() + "'";
                    }
                    else
                    {
                        strWhere = " AND D.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue.ToString() + "'";
                    }
                }
            }

            if (comboBox_PP_HOGI.SelectedValue != null)
            {
                if (!(string.IsNullOrEmpty(comboBox_PP_HOGI.SelectedValue.ToString())))
                {
                    if (string.IsNullOrEmpty(strWhere))
                    {
                        strWhere = " C.FAC_SEQ = '" + comboBox_PP_HOGI.SelectedValue.ToString() + "'";
                    }
                    else
                    {
                        strWhere += " AND C.FAC_SEQ = '" + comboBox_PP_HOGI.SelectedValue.ToString() + "'";
                    }
                }
            }

            if (comboBox_EFF_YEAR.SelectedValue != null)
            {
                if (!(string.IsNullOrEmpty(comboBox_EFF_YEAR.SelectedValue.ToString())))
                {
                    if (string.IsNullOrEmpty(strWhere))
                    {
                        strWhere = " A.EFF_YEAR = '" + comboBox_EFF_YEAR.SelectedValue.ToString() + "'";
                    }
                    else
                    {
                        strWhere += " AND A.EFF_YEAR = '" + comboBox_EFF_YEAR.SelectedValue.ToString() + "'";
                    }
                }
            }
            return strWhere;
        }
        #endregion

        #region public Method -------------------------------------------------------------------------------------

        #endregion





        private void setCommandStatus()
        {
            switch (m_FormeventType)
            {
                case FormManager.SubFormEventType.isAppend:
                    btnSave.Enabled = true;
                    //btnFacDel.Enabled = true;
                    //btnFacAdd.Enabled = true;

                    break;
                case FormManager.SubFormEventType.isUpdate:
                    btnSave.Enabled = true;
                    //btnFacDel.Enabled = true;
                    //btnFacAdd.Enabled = true;

                    break;
                case FormManager.SubFormEventType.isViewer:
                    btnSave.Enabled = false;
                    //btnFacDel.Enabled = true;
                    //btnFacAdd.Enabled = true;

                    break;
            }
        }


    }
}
