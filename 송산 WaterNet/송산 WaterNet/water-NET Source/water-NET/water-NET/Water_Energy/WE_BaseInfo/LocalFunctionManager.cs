﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

namespace WaterNet.WE_BaseInfo
{
    public class LocalFunctionManager
    {

        /// <summary>
        /// 문자열에서 해당 Char를 삭제한다.
        /// </summary>
        /// <param name="ostring"></param>
        /// <param name="cDelemeter"></param>
        /// <returns></returns>
        public static string getRemovedChar(string ostring, char cDelemeter)
        {
            string o = string.Empty;
            for (int i = 0; i < ostring.Length; i++)
            {
                if (ostring[i] != cDelemeter)
                {
                    o += ostring[i];
                }
            }
            return o;
        }
    }
}
