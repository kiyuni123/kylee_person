﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Drawing;

using WaterNet.WaterNetCore;

using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinGrid.ExcelExport;
using Infragistics.Excel;

namespace WaterNet.WE_BaseInfo
{
    public class GridManager
    {
        private UltraGrid m_ultraGrid = null;
        private int m_bandIndex = 0;
        private string m_Title;
        private string m_tag = "flat"; //그리드의 삭제가능 필드

        /// <summary>
        /// 기본 생성자
        /// </summary>
        /// <param name="ultraGrid"></param>
        public GridManager(UltraGrid ultraGrid)
        {
            this.m_ultraGrid = ultraGrid;

            SetGridStyle_Default();
            //InitializeGridStyle();
        }

        public GridManager(UltraGrid ultraGrid, string title)
        {
            this.m_ultraGrid = ultraGrid;
            this.m_Title = title;

            SetGridStyle_Default();
            //InitializeGridStyle();
        }

        /// <summary>
        /// Get UltraGrid
        /// </summary>
        public UltraGrid grid
        {
            get
            {
                return m_ultraGrid;
            }
        }

        /// <summary>
        /// 그리드의 Band 번호
        /// </summary>
        public int BandIndex
        {
            get
            {
                return m_bandIndex;
            }
            set
            {
                m_bandIndex = value;
            }
        }

        public string Text
        {
            get
            {
                return this.m_Title;
            }
            set
            {
                this.m_Title = value;
            }
        }

        public string tag
        {
            get
            {
                return this.m_tag;
            }
            set
            {
                this.m_tag = value;
            }
        }

        /// <summary>
        /// 신규 DataSet을 그리드에 적용
        /// </summary>
        /// <param name="dataSource">System.Data.DataSet</param>
        /// <param name="dataMember">DataTable Name</param>
        public void SetDataSource(DataTable oDataSource)
        {
            try
            {
                this.m_ultraGrid.DataSource = oDataSource.DefaultView;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataSource()
        {
            DataTable oDataTable = null;
            //if (this.m_ultraGrid.DataSource is DataTable)
            //{
                oDataTable = this.m_ultraGrid.DataSource as DataTable;
                if (oDataTable == null)
                {
                    oDataTable = CreateEmptyDataSource();
                }
            //}
            return oDataTable;
        }

        private DataTable CreateEmptyDataSource()
        {
            DataTable oDataTable = new DataTable();
            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns)
            {
                DataColumn oColumn = new DataColumn();
                oColumn.ColumnName = item.Key;
                oColumn.Caption = item.Header.Caption;
                oColumn.DataType = item.DataType;

                oDataTable.Columns.Add(oColumn);
            }

            return oDataTable;
        }

        /// <summary>
        /// 신규 DataSet을 그리드에 적용
        /// </summary>
        /// <param name="dataSource">System.Data.DataSet</param>
        /// <param name="dataMember">DataTable Name</param>
        public void SetDataSource(Object oDataSource, string oDataMember)
        {
            try
            {
                this.m_ultraGrid.DataSource = new BindingSource(oDataSource, oDataMember);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 신규 DataSet을 그리드에 적용
        /// </summary>
        /// <param name="dataSource">System.Data.DataSet</param>
        /// <param name="dataMember">DataTable Name</param>
        public void SetDataSource(Object oDataSource, string oDataMember, string unBindDataMember)
        {
            try
            {
                this.m_ultraGrid.DataSource = new BindingSource(oDataSource, oDataMember);
                //SetUnBoundDataSource(dataSource, unBindDataMember);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool CanPerformAction(UltraGridAction action)
        {
            return this.m_ultraGrid.KeyActionMappings.IsActionAllowed(action, (long)this.m_ultraGrid.CurrentState);
        }

        public void SetGridSort(string key)
        {
            SetGridSort(key, false);
        }

        public void SetGridSort(string key, bool desc)
        {
            UltraGridColumn oGridColumn = getUltraGridColumn(key);
            if (oGridColumn != null)
            {
                this.m_ultraGrid.DisplayLayout.Bands[BandIndex].SortedColumns.Add(oGridColumn, desc);    
            }
        }

        public UltraGridColumn getUltraGridColumn(string key)
        {
            UltraGridColumn oGridColumn = null;
            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns)
            {
                if (item.Key == key)
                {
                    oGridColumn = item;
                    continue;
                }
            }
            return oGridColumn;
        }

        public DataColumn[] SetPrimaryKey(string[] keys)
        {
            DataColumn[] oDataColumns = new DataColumn[keys.Length];
            for (int i = 0; i < keys.Length; i++)
            {
                oDataColumns.SetValue(getDataColumn(keys[i]),i);
            }
            return oDataColumns;
        }

        public DataColumn getDataColumn(string key)
        {
            DataColumn oDataColumn = null;

            DataTable oDataTable = GetDataSource();
            if (oDataTable != null)
            {
                foreach (DataColumn item in oDataTable.Columns)
                {
                    if (item.ColumnName == key)
                    {
                        return item;
                    }
                }    
            }
            
            return oDataColumn;
        }

        #region 그리드에 동적 컬럼 추가
        /// <summary>
        /// 그리드에 동적 컬럼을 추가
        /// </summary>
        /// <param name="key">Column Key Property</param>
        public UltraGridColumn AddColumn(string key)
        {
            return AddColumn(key, key);
        }

        public UltraGridColumn AddColumn(string key, string caption, int width)
        {
            UltraGridColumn column = this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Add();
            column.Width = width;
            column.Key = key;
            column.Header.Caption = caption;

            return column;
        }

        public UltraGridColumn AddColumn(string key, string caption)
        {
            return AddColumn(key, caption, Activation.NoEdit);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation)
        {
            return AddColumn(key, caption, activation, CellClickAction.Default);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction)
        {
            return AddColumn(key, caption, activation, cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle.Default);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style)
        {
            return AddColumn(key, caption, activation, cellClickAction, style, true);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible)
        {
            return AddColumn(key, caption, activation, cellClickAction, style, visible, 100);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible, int width)
        {
            return AddColumn(key, caption, activation, cellClickAction, style, visible, width, string.Empty);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible, int width, string tag)
        {
            UltraGridColumn column = this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Add();
            column.Width = width;
            column.Key = key;
            column.Header.Caption = caption;
            column.CellActivation = activation;
            if (activation == Activation.AllowEdit)
            {
                ////현재 선택된 셀만 백칼라 = YellowGreen
                this.m_ultraGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
                this.m_ultraGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;
            }
            column.CellClickAction = cellClickAction;
            column.Style = style;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Hidden = visible;
            column.Tag = tag;

            return column;
        }
        #endregion

        #region 그리드에 동적 컬럼 삽입
        public void InsertColumn(int index, string key, string caption)
        {
            InsertColumn(index, key, caption, Activation.NoEdit);
        }

        public void InsertColumn(int index, string key, string caption, Activation activation)
        {
            InsertColumn(index, key, caption, activation, CellClickAction.Default);
        }

        public void InsertColumn(int index, string key, string caption, Activation activation, CellClickAction cellClickAction)
        {
            InsertColumn(index,key, caption, activation, cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle.Default);
        }

        public void InsertColumn(int index, string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style)
        {
            InsertColumn(index, key, caption, activation, cellClickAction, style, true);
        }

        public void InsertColumn(int index, string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible)
        {
            InsertColumn(index, key, caption, activation, cellClickAction, style, visible, 100);
        }

        public void InsertColumn(int index, string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible, int width)
        {
            InsertColumn(index, key, caption, activation, cellClickAction, style, visible, width, string.Empty);
        }

        public void InsertColumn(int index, string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible, int width, string tag)
        {
            UltraGridColumn column = this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Insert(index, key);
            column.Width = width;
            column.Header.Caption = caption;
            column.CellActivation = activation;
            column.CellClickAction = cellClickAction;
            column.Style = style;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Hidden = visible;
            column.Tag = tag;
        }

        #endregion

        #region 그리드에 동적 컬럼 삭제
        public void RemoveColumn(string key)
        {
            this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Remove(key);
        }

        public void RemoveColumn(int index)
        {
             this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Remove(index);
        }

        public void RemoveColumn(UltraGridColumn oColumn)
        {
            this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Remove(oColumn.Key);
        }

        public void RemoveColumn(UltraGridColumn oColumn, ColumnsCollection oColumns)
        {
            foreach (UltraGridColumn item in oColumns)
            {
                if (item.Equals(oColumn))
                {
                    oColumns.Remove(item.Key);
                }
            }
        }

        #endregion

        /// <summary>
        /// 그리드에 동적변경 컬럼을 모두 삭제
        /// 데이터 검색시에 호출
        /// </summary>
        public void RemoveAllColumns()
        {
            this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.ClearUnbound();
        }

        /// <summary>
        /// taged 그리드 칼럼 visibled
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="visibled"></param>
        public void VisibleTagedColumns(string tag, bool visibled)
        {
            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns)
            {
                if (string.IsNullOrEmpty(Convert.ToString(item.Tag))) continue;
                
                if (Convert.ToString(item.Tag) == tag)                 item.Hidden = !visibled;
                else item.Hidden = visibled;
            }
            m_ultraGrid.Update();
        }

        #region Ultra Group Header
        /// <summary>
        /// 그리드의 그룹 찾기
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public UltraGridGroup getColumnGroup(string key)
        {
            UltraGridGroup gridGroup = null;
            foreach (UltraGridGroup item in m_ultraGrid.DisplayLayout.Bands[m_bandIndex].Groups)
	        {
        		 if (item.Key == key)
	            {
            		 gridGroup = item;
	            }
	        }
            return gridGroup;
        }

        /// <summary>
        /// 해당 그리드 그룹 Visible : true = Hidden, false = 보이기
        /// </summary>
        /// <param name="key"></param>
        /// <param name="visibled"></param>
        public void VisibleGridGroup(string key, bool visibled)
        {
            UltraGridGroup gridGroup = getColumnGroup(key);
            if (gridGroup == null) return;
            foreach (UltraGridColumn item in gridGroup.Columns)
            {
                if (item != null)
                {
                    item.Hidden = visibled;
                }
            }
            
            gridGroup.Hidden = visibled;
        }

        public void VisibleGridGroup(ArrayList key, bool visibled)
        {
            //동적 그리드 그룹을 모두 Hidden = true
            foreach (UltraGridGroup item in m_ultraGrid.DisplayLayout.Bands[m_bandIndex].Groups)
            {
                if (string.IsNullOrEmpty(Convert.ToString(item.Tag)) || Convert.ToString(item.Tag) == "Lock") continue;
                VisibleGridGroup(item.Key, true);
            }

            for (int i = 0; i < key.Count; i++)
            {
                foreach (UltraGridGroup item in m_ultraGrid.DisplayLayout.Bands[m_bandIndex].Groups)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(item.Tag)) || Convert.ToString(item.Tag) == "Lock") continue;
                    if (Convert.ToString(item.Key) == Convert.ToString(key[i]))
                    {
                        VisibleGridGroup(item.Key, visibled);
                        //item.Hidden = !visibled;    
                    }
                    
                }                
            }
        }

        public void VisibleGridGroups(string key, bool visibled)
        {
            foreach (UltraGridGroup item in m_ultraGrid.DisplayLayout.Bands[m_bandIndex].Groups)
            {
                if (string.IsNullOrEmpty(Convert.ToString(item.Tag)) || Convert.ToString(item.Tag) == "Lock") continue;
                if (item == getColumnGroup(key))
                {
                    VisibleGridGroup(item.Key, !visibled);
                }
                else
                {
                    VisibleGridGroup(item.Key, visibled);
                }
            }
        }

        public void VisibleColumnAsKey(string key, bool visibled)
        {
            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns)
            {
                if (Convert.ToString(item.Key) == key)
                {
                    item.Hidden = !visibled;
                    return;
                }
            }
            m_ultraGrid.Update();
        }

        public UltraGridGroup AddColumnGroup(string key, ArrayList oColumns)
        {
            return AddColumnGroup(key, key, oColumns, string.Empty);
        }

        public UltraGridGroup AddColumnGroup(string key, string caption, ArrayList oColumns)
        {
            return AddColumnGroup(key, caption, oColumns, string.Empty);
        }

        public UltraGridGroup AddColumnGroup(string key, string caption, ArrayList oColumns, string tag)
        {
            return AddColumnGroup(key, caption, oColumns, tag, 1);
        }

        public UltraGridGroup AddColumnGroup(string key, string caption, ArrayList oColumns, string tag, int levelCount)
        {
            UltraGridGroup gridGroup = null;
            m_ultraGrid.DisplayLayout.Bands[m_bandIndex].LevelCount = levelCount;
            if (getColumnGroup(key) == null)
	        {
                gridGroup = m_ultraGrid.DisplayLayout.Bands[m_bandIndex].Groups.Add(key);
                gridGroup.Header.Caption = caption;
                gridGroup.Header.Appearance.TextHAlign = HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = VAlign.Middle;
                gridGroup.Header.Appearance.BackGradientStyle = GradientStyle.None;
                gridGroup.Tag = tag;

                ColumnsCollection ColumnCollection = m_ultraGrid.DisplayLayout.Bands[m_bandIndex].Columns;
                foreach (UltraGridColumn Column in ColumnCollection)
                {
                    foreach (string item in oColumns)
                    {
                        if (Column.Key == item )
                        {
                            Column.Group = gridGroup;
                            Column.Level = m_ultraGrid.DisplayLayout.Bands[m_bandIndex].LevelCount - 1;
                            continue;
                        }
                    }
                }
	        }
            return gridGroup;
        }



        #endregion

        #region GridStyle Set
        /// <summary>
        /// 그리드 스타일의 공통(append, update, select Mode)
        /// </summary>
        public void SetGridStyle_Default()
        {
            this.m_ultraGrid.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            this.m_ultraGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            this.m_ultraGrid.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            this.m_ultraGrid.DisplayLayout.GroupByBox.Hidden = true;
            this.m_ultraGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.m_ultraGrid.DisplayLayout.MaxRowScrollRegions = 1;

            this.m_ultraGrid.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;

            this.m_ultraGrid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            this.m_ultraGrid.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;
            this.m_ultraGrid.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;

            this.m_ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.Default;
            this.m_ultraGrid.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            this.m_ultraGrid.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;
            this.m_ultraGrid.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            this.m_ultraGrid.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;

            this.m_ultraGrid.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            this.m_ultraGrid.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;

            this.m_ultraGrid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            this.m_ultraGrid.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            this.m_ultraGrid.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);
        }

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Update 모드로 정의하고, 모든 Cell에 대해 Edit 가능하도록 설정
        /// </summary>
        public void SetGridStyle_Update()
        {
            this.m_ultraGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;

            this.m_ultraGrid.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.LightYellow;
            this.m_ultraGrid.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Blue;
            this.m_ultraGrid.DisplayLayout.Override.RowAppearance.BackColor = Color.White;

            this.m_ultraGrid.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            this.m_ultraGrid.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;

            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.m_ultraGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.m_ultraGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                item.Header.Appearance.TextHAlign = HAlign.Center;
                item.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.CellActivation = Activation.AllowEdit;
                item.CellClickAction = CellClickAction.Edit;
            }
        }
        #endregion

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Append 모드로 정의하고, 모든 Cell에 대해 Edit 못하게 수정
        /// </summary>
        public void SetGridStyle_Append()
        {
            m_ultraGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.FixedAddRowOnTop;
            m_ultraGrid.DisplayLayout.Override.TemplateAddRowPrompt = "신규 데이터는 여기에 입력하세요...";

            m_ultraGrid.DisplayLayout.Override.TemplateAddRowAppearance.BackColor = Color.FromArgb(245, 250, 255);
            m_ultraGrid.DisplayLayout.Override.TemplateAddRowAppearance.ForeColor = SystemColors.GrayText;

            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.m_ultraGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.m_ultraGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                item.Header.Appearance.TextHAlign = HAlign.Center;
                item.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.CellActivation = Activation.AllowEdit;
                item.CellClickAction = CellClickAction.Edit;
            }

        }

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Select 모드로 정의하고, 모든 Cell에 대해 Edit 못하게 수정
        /// </summary>
        public void SetGridStyle_Select()
        {
            m_ultraGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;

            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                item.Header.Appearance.TextHAlign = HAlign.Center;
                item.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.CellActivation = Activation.NoEdit;
                item.CellClickAction = CellClickAction.RowSelect;
            }
        }

        /// <summary>
        /// UltraGrid에 Set된 Data의 Length에 맞게 자동으로 Column Size를 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        public void PerformAutoResize()
        {
            for (int I = 0; I <= m_ultraGrid.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                m_ultraGrid.DisplayLayout.Bands[0].Columns[I].PerformAutoResize();
            }
        }

        public void SetColumnWidth(int width)
        {
            for (int I = 0; I <= m_ultraGrid.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                m_ultraGrid.DisplayLayout.Bands[0].Columns[I].Width = width;
            }
        }

        #region 그리드 헤더 체크박스
        public void HeaderCheckBox(int iCol)
        {
            HeaderCheckBox(iCol, HeaderCheckBoxVisibility.Always);
        }

        public void HeaderCheckBox(int iCol, HeaderCheckBoxVisibility type)
        {
            m_ultraGrid.DisplayLayout.Bands[0].Columns[iCol].Header.CheckBoxVisibility = type;
        }

        public void HeaderCheckBox(int iSCol, int iECol)
        {
            for (int i = iSCol; i < iECol; i++)
            {
                HeaderCheckBox(i);
            }
        }

        public void HeaderCheckBox(int iSCol, int iECol, HeaderCheckBoxVisibility type)
        {
            for (int i = iSCol; i < iECol; i++)
            {
                HeaderCheckBox(i, type);
            }
        }
        #endregion

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iCol">Column Index</param>
        public void ColumeAllowEdit(int iCol)
        {
            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.m_ultraGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.m_ultraGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            m_ultraGrid.DisplayLayout.Bands[0].Columns[iCol].CellActivation = Activation.AllowEdit;
            m_ultraGrid.DisplayLayout.Bands[0].Columns[iCol].CellClickAction = CellClickAction.EditAndSelectText;
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public void ColumeAllowEdit(int iSCol, int iECol)
        {
            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.m_ultraGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.m_ultraGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            for (int i = iSCol; i < iECol; i++)
            {
                m_ultraGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                m_ultraGrid.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }

        /// <summary>
        /// UltraGrid의 특정 Row의 Cell을 Disable 모드로 변경한다.
        /// 단 UltraGrid의 기본 셋팅이 Activation.AllowEdit 이어야 한다.(이건 좀더 확인 필요)
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iRow">Row Index</param>
        /// <param name="iCol">Column Index</param>
        public void CellDisable(int iRow, int iCol)
        {
            m_ultraGrid.Rows[iRow].Cells[iCol].Appearance.BackColorDisabled = Color.LightSalmon;
            m_ultraGrid.Rows[iRow].Cells[iCol].Activation = Activation.Disabled;
        }

        /// <summary>
        /// UltraGrid의 특정 Row의 Cell을 AllowEdit 모드로 변경한다.
        /// 단 UltraGrid의 기본 셋팅이 Activation.NoEdit 이어야 한다.(이건 좀더 확인 필요)
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iRow">Row Index</param>
        /// <param name="iCol">Column Index</param>
        public void CellAllowEdit(int iRow, int iCol)
        {
            m_ultraGrid.Rows[iRow].Cells[iCol].Appearance.BackColor = Color.Lime;
            m_ultraGrid.Rows[iRow].Cells[iCol].Activation = Activation.AllowEdit;
            Application.DoEvents();
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 NoEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="strCol">Column Name. Empty이면 0번 Column<</param>
        public void ColumeNoEdit(string strCol)
        {
            if (string.IsNullOrEmpty(strCol)) return;

            m_ultraGrid.DisplayLayout.Bands[0].Columns[strCol].CellActivation = Activation.NoEdit;
            m_ultraGrid.DisplayLayout.Bands[0].Columns[strCol].CellClickAction = CellClickAction.RowSelect;
            
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 NoEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public void ColumeNoEdit(int iSCol, int iECol)
        {
            for (int i = iSCol; i < iECol; i++)
            {
                m_ultraGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
                m_ultraGrid.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.RowSelect;
            }
        }



        /// <summary>
        /// 특정 Column에 Default Data를 Set한다.
        /// Grid에 Row를 Add 한 다음, Popup Form에서 조회 결과를 리턴받아 Grid에 자동으로 Set하고 싶은 경우 사용하면 된다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="strColumn">UltraGrid의 Column Name. Empty이면 0번 Column</param>
        /// <param name="strDefaultData">Default로 Set할 Data</param>
        public void SetDefaultCellValue(string strColumn, string strDefaultData)
        {
            if (string.IsNullOrEmpty(strColumn)) return;

            m_ultraGrid.DisplayLayout.Bands[0].Columns[strColumn].DefaultCellValue = strDefaultData;
        }

        /// <summary>
        /// UltraGrid에서 RowSelectorImage를 모두 Null로 표시 함.
        /// </summary>
        /// <param name="oUGrid"></param>
        public void NoRowSelectorImages()
        {
            Image[] oImage = new Image[] { null, null, null, null, null };

            m_ultraGrid.DisplayLayout.RowSelectorImages.ActiveRowImage = oImage[0];
            m_ultraGrid.DisplayLayout.RowSelectorImages.ActiveAndDataChangedImage = oImage[1];
            m_ultraGrid.DisplayLayout.RowSelectorImages.ActiveAndAddNewRowImage = oImage[2];
            m_ultraGrid.DisplayLayout.RowSelectorImages.AddNewRowImage = oImage[3];
            m_ultraGrid.DisplayLayout.RowSelectorImages.DataChangedImage = oImage[4];
        }

        /// <summary>
        /// 그리드매니저에 등록된 그리드만 엑셀로 내보내기
        /// </summary>
        /// <param name="gridManager"></param>
        public void ExportExcelToGrid()
        {
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Excel Worksheets|*.xls";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() != DialogResult.OK) return;

            UltraGridExcelExporter ultraGridExcelExporter = new UltraGridExcelExporter();

            try
            {
                if (!(string.IsNullOrEmpty(saveFileDialog1.FileName)))
                {
                    ultraGridExcelExporter.Export(this.m_ultraGrid, saveFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
