﻿namespace WaterNet.WE_BaseInfo
{
    partial class frmBizPlace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel_WE_BIZ_PLA = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraGrid_Pump = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txt_AN_OUT_MDLID = new System.Windows.Forms.TextBox();
            this.label_AN_OUT_MDLID = new System.Windows.Forms.Label();
            this.txt_OUT_TAGID = new System.Windows.Forms.TextBox();
            this.label_OUT_TAGID = new System.Windows.Forms.Label();
            this.txt_LVL_TAGID = new System.Windows.Forms.TextBox();
            this.label_LVL_TAGID = new System.Windows.Forms.Label();
            this.txt_AN_LVL_MDLID = new System.Windows.Forms.TextBox();
            this.label_AN_LVL_MDLID = new System.Windows.Forms.Label();
            this.txt_RL_AN_MDL_NO = new System.Windows.Forms.TextBox();
            this.txt_BIZ_PLA_ETC = new System.Windows.Forms.TextBox();
            this.txt_BIZ_PLA_NM = new System.Windows.Forms.TextBox();
            this.labelRL_AN_MDL_NO = new System.Windows.Forms.Label();
            this.labelBIZ_PLA_ETC = new System.Windows.Forms.Label();
            this.labelBIZ_PLA_NM = new System.Windows.Forms.Label();
            this.textBox_BIZ_ENGWON = new System.Windows.Forms.TextBox();
            this.label_BIZ_ENGWON = new System.Windows.Forms.Label();
            this.panelCommand.SuspendLayout();
            this.panel_WE_BIZ_PLA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pump)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnSave);
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(746, 43);
            this.panelCommand.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(582, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(663, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel_WE_BIZ_PLA
            // 
            this.panel_WE_BIZ_PLA.Controls.Add(this.textBox_BIZ_ENGWON);
            this.panel_WE_BIZ_PLA.Controls.Add(this.label_BIZ_ENGWON);
            this.panel_WE_BIZ_PLA.Controls.Add(this.label1);
            this.panel_WE_BIZ_PLA.Controls.Add(this.ultraGrid_Pump);
            this.panel_WE_BIZ_PLA.Controls.Add(this.txt_AN_OUT_MDLID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.label_AN_OUT_MDLID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.txt_OUT_TAGID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.label_OUT_TAGID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.txt_LVL_TAGID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.label_LVL_TAGID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.txt_AN_LVL_MDLID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.label_AN_LVL_MDLID);
            this.panel_WE_BIZ_PLA.Controls.Add(this.txt_RL_AN_MDL_NO);
            this.panel_WE_BIZ_PLA.Controls.Add(this.txt_BIZ_PLA_ETC);
            this.panel_WE_BIZ_PLA.Controls.Add(this.txt_BIZ_PLA_NM);
            this.panel_WE_BIZ_PLA.Controls.Add(this.labelRL_AN_MDL_NO);
            this.panel_WE_BIZ_PLA.Controls.Add(this.labelBIZ_PLA_ETC);
            this.panel_WE_BIZ_PLA.Controls.Add(this.labelBIZ_PLA_NM);
            this.panel_WE_BIZ_PLA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_WE_BIZ_PLA.Location = new System.Drawing.Point(0, 43);
            this.panel_WE_BIZ_PLA.Name = "panel_WE_BIZ_PLA";
            this.panel_WE_BIZ_PLA.Size = new System.Drawing.Size(746, 360);
            this.panel_WE_BIZ_PLA.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(0, 178);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(746, 19);
            this.label1.TabIndex = 37;
            this.label1.Text = "펌프 해석정보";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ultraGrid_Pump
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Pump.DisplayLayout.Appearance = appearance4;
            this.ultraGrid_Pump.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Pump.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pump.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Pump.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.ultraGrid_Pump.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Pump.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.ultraGrid_Pump.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Pump.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_Pump.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_Pump.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.ultraGrid_Pump.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Pump.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pump.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Pump.DisplayLayout.Override.CellAppearance = appearance5;
            this.ultraGrid_Pump.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Pump.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pump.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Pump.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Pump.DisplayLayout.Override.RowAppearance = appearance10;
            this.ultraGrid_Pump.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Pump.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.ultraGrid_Pump.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Pump.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Pump.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_Pump.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGrid_Pump.Location = new System.Drawing.Point(0, 197);
            this.ultraGrid_Pump.Name = "ultraGrid_Pump";
            this.ultraGrid_Pump.Size = new System.Drawing.Size(746, 163);
            this.ultraGrid_Pump.TabIndex = 36;
            this.ultraGrid_Pump.Text = "ultraGrid1";
            // 
            // txt_AN_OUT_MDLID
            // 
            this.txt_AN_OUT_MDLID.Location = new System.Drawing.Point(505, 95);
            this.txt_AN_OUT_MDLID.Name = "txt_AN_OUT_MDLID";
            this.txt_AN_OUT_MDLID.Size = new System.Drawing.Size(226, 21);
            this.txt_AN_OUT_MDLID.TabIndex = 35;
            this.txt_AN_OUT_MDLID.Tag = "AN_OUT_MDLID";
            // 
            // label_AN_OUT_MDLID
            // 
            this.label_AN_OUT_MDLID.Location = new System.Drawing.Point(373, 100);
            this.label_AN_OUT_MDLID.Name = "label_AN_OUT_MDLID";
            this.label_AN_OUT_MDLID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_AN_OUT_MDLID.Size = new System.Drawing.Size(121, 12);
            this.label_AN_OUT_MDLID.TabIndex = 34;
            this.label_AN_OUT_MDLID.Text = "유출 해석모델항목ID";
            // 
            // txt_OUT_TAGID
            // 
            this.txt_OUT_TAGID.Location = new System.Drawing.Point(505, 124);
            this.txt_OUT_TAGID.Name = "txt_OUT_TAGID";
            this.txt_OUT_TAGID.Size = new System.Drawing.Size(226, 21);
            this.txt_OUT_TAGID.TabIndex = 33;
            this.txt_OUT_TAGID.Tag = "OUT_TAGID";
            // 
            // label_OUT_TAGID
            // 
            this.label_OUT_TAGID.Location = new System.Drawing.Point(373, 127);
            this.label_OUT_TAGID.Name = "label_OUT_TAGID";
            this.label_OUT_TAGID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_OUT_TAGID.Size = new System.Drawing.Size(121, 12);
            this.label_OUT_TAGID.TabIndex = 32;
            this.label_OUT_TAGID.Text = "유출유량계 Tag-ID";
            // 
            // txt_LVL_TAGID
            // 
            this.txt_LVL_TAGID.Location = new System.Drawing.Point(505, 68);
            this.txt_LVL_TAGID.Name = "txt_LVL_TAGID";
            this.txt_LVL_TAGID.Size = new System.Drawing.Size(226, 21);
            this.txt_LVL_TAGID.TabIndex = 31;
            this.txt_LVL_TAGID.Tag = "LVL_TAGID";
            // 
            // label_LVL_TAGID
            // 
            this.label_LVL_TAGID.Location = new System.Drawing.Point(373, 73);
            this.label_LVL_TAGID.Name = "label_LVL_TAGID";
            this.label_LVL_TAGID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_LVL_TAGID.Size = new System.Drawing.Size(121, 12);
            this.label_LVL_TAGID.TabIndex = 30;
            this.label_LVL_TAGID.Text = "수위 Tag-ID";
            // 
            // txt_AN_LVL_MDLID
            // 
            this.txt_AN_LVL_MDLID.Location = new System.Drawing.Point(505, 40);
            this.txt_AN_LVL_MDLID.Name = "txt_AN_LVL_MDLID";
            this.txt_AN_LVL_MDLID.Size = new System.Drawing.Size(226, 21);
            this.txt_AN_LVL_MDLID.TabIndex = 29;
            this.txt_AN_LVL_MDLID.Tag = "AN_LVL_MDLID";
            // 
            // label_AN_LVL_MDLID
            // 
            this.label_AN_LVL_MDLID.Location = new System.Drawing.Point(373, 45);
            this.label_AN_LVL_MDLID.Name = "label_AN_LVL_MDLID";
            this.label_AN_LVL_MDLID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_AN_LVL_MDLID.Size = new System.Drawing.Size(121, 12);
            this.label_AN_LVL_MDLID.TabIndex = 28;
            this.label_AN_LVL_MDLID.Text = "수위 해석모델항목ID";
            // 
            // txt_RL_AN_MDL_NO
            // 
            this.txt_RL_AN_MDL_NO.Location = new System.Drawing.Point(505, 13);
            this.txt_RL_AN_MDL_NO.Name = "txt_RL_AN_MDL_NO";
            this.txt_RL_AN_MDL_NO.Size = new System.Drawing.Size(226, 21);
            this.txt_RL_AN_MDL_NO.TabIndex = 27;
            this.txt_RL_AN_MDL_NO.Tag = "RL_AN_MDL_NO";
            // 
            // txt_BIZ_PLA_ETC
            // 
            this.txt_BIZ_PLA_ETC.Location = new System.Drawing.Point(44, 45);
            this.txt_BIZ_PLA_ETC.Multiline = true;
            this.txt_BIZ_PLA_ETC.Name = "txt_BIZ_PLA_ETC";
            this.txt_BIZ_PLA_ETC.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_BIZ_PLA_ETC.Size = new System.Drawing.Size(286, 121);
            this.txt_BIZ_PLA_ETC.TabIndex = 26;
            this.txt_BIZ_PLA_ETC.Tag = "BIZ_PLA_ETC";
            // 
            // txt_BIZ_PLA_NM
            // 
            this.txt_BIZ_PLA_NM.Location = new System.Drawing.Point(84, 14);
            this.txt_BIZ_PLA_NM.Name = "txt_BIZ_PLA_NM";
            this.txt_BIZ_PLA_NM.Size = new System.Drawing.Size(246, 21);
            this.txt_BIZ_PLA_NM.TabIndex = 25;
            this.txt_BIZ_PLA_NM.Tag = "BIZ_PLA_NM";
            // 
            // labelRL_AN_MDL_NO
            // 
            this.labelRL_AN_MDL_NO.Location = new System.Drawing.Point(373, 18);
            this.labelRL_AN_MDL_NO.Name = "labelRL_AN_MDL_NO";
            this.labelRL_AN_MDL_NO.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelRL_AN_MDL_NO.Size = new System.Drawing.Size(121, 12);
            this.labelRL_AN_MDL_NO.TabIndex = 20;
            this.labelRL_AN_MDL_NO.Text = "실시간해석모델번호";
            // 
            // labelBIZ_PLA_ETC
            // 
            this.labelBIZ_PLA_ETC.Location = new System.Drawing.Point(22, 52);
            this.labelBIZ_PLA_ETC.Name = "labelBIZ_PLA_ETC";
            this.labelBIZ_PLA_ETC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelBIZ_PLA_ETC.Size = new System.Drawing.Size(20, 80);
            this.labelBIZ_PLA_ETC.TabIndex = 19;
            this.labelBIZ_PLA_ETC.Text = "사업장개요";
            this.labelBIZ_PLA_ETC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBIZ_PLA_NM
            // 
            this.labelBIZ_PLA_NM.Location = new System.Drawing.Point(4, 18);
            this.labelBIZ_PLA_NM.Name = "labelBIZ_PLA_NM";
            this.labelBIZ_PLA_NM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelBIZ_PLA_NM.Size = new System.Drawing.Size(74, 11);
            this.labelBIZ_PLA_NM.TabIndex = 18;
            this.labelBIZ_PLA_NM.Text = "사업장명";
            // 
            // textBox_BIZ_ENGWON
            // 
            this.textBox_BIZ_ENGWON.Location = new System.Drawing.Point(505, 151);
            this.textBox_BIZ_ENGWON.Name = "textBox_BIZ_ENGWON";
            this.textBox_BIZ_ENGWON.Size = new System.Drawing.Size(226, 21);
            this.textBox_BIZ_ENGWON.TabIndex = 39;
            this.textBox_BIZ_ENGWON.Tag = "BIZ_ENGWON";
            // 
            // label_BIZ_ENGWON
            // 
            this.label_BIZ_ENGWON.Location = new System.Drawing.Point(373, 154);
            this.label_BIZ_ENGWON.Name = "label_BIZ_ENGWON";
            this.label_BIZ_ENGWON.Size = new System.Drawing.Size(121, 12);
            this.label_BIZ_ENGWON.TabIndex = 38;
            this.label_BIZ_ENGWON.Text = "단위전력요금(원/kw)";
            // 
            // frmBizPlace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 403);
            this.Controls.Add(this.panel_WE_BIZ_PLA);
            this.Controls.Add(this.panelCommand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBizPlace";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "에너지 사업장정보";
            this.panelCommand.ResumeLayout(false);
            this.panel_WE_BIZ_PLA.ResumeLayout(false);
            this.panel_WE_BIZ_PLA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pump)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel_WE_BIZ_PLA;
        private System.Windows.Forms.TextBox txt_RL_AN_MDL_NO;
        private System.Windows.Forms.TextBox txt_BIZ_PLA_ETC;
        private System.Windows.Forms.TextBox txt_BIZ_PLA_NM;
        private System.Windows.Forms.Label labelRL_AN_MDL_NO;
        private System.Windows.Forms.Label labelBIZ_PLA_ETC;
        private System.Windows.Forms.Label labelBIZ_PLA_NM;
        private System.Windows.Forms.TextBox txt_AN_LVL_MDLID;
        private System.Windows.Forms.Label label_AN_LVL_MDLID;
        private System.Windows.Forms.TextBox txt_OUT_TAGID;
        private System.Windows.Forms.Label label_OUT_TAGID;
        private System.Windows.Forms.TextBox txt_LVL_TAGID;
        private System.Windows.Forms.Label label_LVL_TAGID;
        private System.Windows.Forms.TextBox txt_AN_OUT_MDLID;
        private System.Windows.Forms.Label label_AN_OUT_MDLID;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Pump;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_BIZ_ENGWON;
        private System.Windows.Forms.Label label_BIZ_ENGWON;
    }
}