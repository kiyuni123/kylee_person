﻿namespace WaterNet.WE_BaseInfo
{
    partial class frmMeasurSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.comboBox_endTime = new System.Windows.Forms.ComboBox();
            this.ultraDateTimeEditor_endDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_startTime = new System.Windows.Forms.ComboBox();
            this.ultraDateTimeEditor_startDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.comboBox_BIZ_PLA_NM = new System.Windows.Forms.ComboBox();
            this.label_BIZ_PLA_NM = new System.Windows.Forms.Label();
            this.label_PP_HOGI = new System.Windows.Forms.Label();
            this.btnSel = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.panel_Contents = new System.Windows.Forms.Panel();
            this.ultraGrid_WE_TM_MES_DTL = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraCalcManager1 = new Infragistics.Win.UltraWinCalcManager.UltraCalcManager(this.components);
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_endDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_startDay)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel_Contents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_TM_MES_DTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.comboBox_endTime);
            this.panelCommand.Controls.Add(this.ultraDateTimeEditor_endDay);
            this.panelCommand.Controls.Add(this.label1);
            this.panelCommand.Controls.Add(this.comboBox_startTime);
            this.panelCommand.Controls.Add(this.ultraDateTimeEditor_startDay);
            this.panelCommand.Controls.Add(this.comboBox_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_PP_HOGI);
            this.panelCommand.Controls.Add(this.btnSel);
            this.panelCommand.Controls.Add(this.btnExcel);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(949, 55);
            this.panelCommand.TabIndex = 3;
            // 
            // comboBox_endTime
            // 
            this.comboBox_endTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_endTime.FormattingEnabled = true;
            this.comboBox_endTime.Location = new System.Drawing.Point(521, 3);
            this.comboBox_endTime.Name = "comboBox_endTime";
            this.comboBox_endTime.Size = new System.Drawing.Size(44, 20);
            this.comboBox_endTime.TabIndex = 17;
            this.comboBox_endTime.Visible = false;
            // 
            // ultraDateTimeEditor_endDay
            // 
            this.ultraDateTimeEditor_endDay.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_endDay.AutoSize = false;
            this.ultraDateTimeEditor_endDay.Location = new System.Drawing.Point(374, 19);
            this.ultraDateTimeEditor_endDay.Name = "ultraDateTimeEditor_endDay";
            this.ultraDateTimeEditor_endDay.Size = new System.Drawing.Size(104, 21);
            this.ultraDateTimeEditor_endDay.TabIndex = 16;
            this.ultraDateTimeEditor_endDay.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor_endDay_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(355, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "~";
            // 
            // comboBox_startTime
            // 
            this.comboBox_startTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_startTime.FormattingEnabled = true;
            this.comboBox_startTime.Location = new System.Drawing.Point(356, 3);
            this.comboBox_startTime.Name = "comboBox_startTime";
            this.comboBox_startTime.Size = new System.Drawing.Size(44, 20);
            this.comboBox_startTime.TabIndex = 14;
            this.comboBox_startTime.Visible = false;
            // 
            // ultraDateTimeEditor_startDay
            // 
            this.ultraDateTimeEditor_startDay.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_startDay.AutoSize = false;
            this.ultraDateTimeEditor_startDay.Location = new System.Drawing.Point(246, 19);
            this.ultraDateTimeEditor_startDay.Name = "ultraDateTimeEditor_startDay";
            this.ultraDateTimeEditor_startDay.Size = new System.Drawing.Size(104, 21);
            this.ultraDateTimeEditor_startDay.TabIndex = 13;
            this.ultraDateTimeEditor_startDay.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor_startDay_ValueChanged);
            // 
            // comboBox_BIZ_PLA_NM
            // 
            this.comboBox_BIZ_PLA_NM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BIZ_PLA_NM.FormattingEnabled = true;
            this.comboBox_BIZ_PLA_NM.Location = new System.Drawing.Point(60, 18);
            this.comboBox_BIZ_PLA_NM.Name = "comboBox_BIZ_PLA_NM";
            this.comboBox_BIZ_PLA_NM.Size = new System.Drawing.Size(111, 20);
            this.comboBox_BIZ_PLA_NM.TabIndex = 12;
            // 
            // label_BIZ_PLA_NM
            // 
            this.label_BIZ_PLA_NM.AutoSize = true;
            this.label_BIZ_PLA_NM.Location = new System.Drawing.Point(9, 22);
            this.label_BIZ_PLA_NM.Name = "label_BIZ_PLA_NM";
            this.label_BIZ_PLA_NM.Size = new System.Drawing.Size(41, 12);
            this.label_BIZ_PLA_NM.TabIndex = 11;
            this.label_BIZ_PLA_NM.Text = "사업장";
            // 
            // label_PP_HOGI
            // 
            this.label_PP_HOGI.AutoSize = true;
            this.label_PP_HOGI.Location = new System.Drawing.Point(188, 24);
            this.label_PP_HOGI.Name = "label_PP_HOGI";
            this.label_PP_HOGI.Size = new System.Drawing.Size(53, 12);
            this.label_PP_HOGI.TabIndex = 7;
            this.label_PP_HOGI.Text = "가동일시";
            // 
            // btnSel
            // 
            this.btnSel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSel.Location = new System.Drawing.Point(781, 12);
            this.btnSel.Name = "btnSel";
            this.btnSel.Size = new System.Drawing.Size(75, 30);
            this.btnSel.TabIndex = 5;
            this.btnSel.Text = "조회";
            this.btnSel.UseVisualStyleBackColor = true;
            this.btnSel.Click += new System.EventHandler(this.btnSel_Click);
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Location = new System.Drawing.Point(862, 12);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExcel.TabIndex = 2;
            this.btnExcel.Text = "엑셀";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 55);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chart1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel_Contents);
            this.splitContainer1.Size = new System.Drawing.Size(949, 502);
            this.splitContainer1.SplitterDistance = 242;
            this.splitContainer1.TabIndex = 5;
            // 
            // chart1
            // 
            this.chart1.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(949, 242);
            this.chart1.TabIndex = 0;
            // 
            // panel_Contents
            // 
            this.panel_Contents.Controls.Add(this.ultraGrid_WE_TM_MES_DTL);
            this.panel_Contents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Contents.Location = new System.Drawing.Point(0, 0);
            this.panel_Contents.Name = "panel_Contents";
            this.panel_Contents.Size = new System.Drawing.Size(949, 256);
            this.panel_Contents.TabIndex = 5;
            // 
            // ultraGrid_WE_TM_MES_DTL
            // 
            this.ultraGrid_WE_TM_MES_DTL.CalcManager = this.ultraCalcManager1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Appearance = appearance13;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.CellAppearance = appearance20;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.RowAppearance = appearance23;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_WE_TM_MES_DTL.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_WE_TM_MES_DTL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_WE_TM_MES_DTL.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_WE_TM_MES_DTL.Name = "ultraGrid_WE_TM_MES_DTL";
            this.ultraGrid_WE_TM_MES_DTL.Size = new System.Drawing.Size(949, 256);
            this.ultraGrid_WE_TM_MES_DTL.TabIndex = 3;
            // 
            // ultraCalcManager1
            // 
            this.ultraCalcManager1.ContainingControl = this;
            // 
            // frmMeasurSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 557);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmMeasurSearch";
            this.Text = "frmRealTimeAnalysis";
            this.panelCommand.ResumeLayout(false);
            this.panelCommand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_endDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_startDay)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel_Contents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_TM_MES_DTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalcManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.ComboBox comboBox_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_PP_HOGI;
        private System.Windows.Forms.Button btnSel;
        private System.Windows.Forms.Button btnExcel;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_startDay;
        private System.Windows.Forms.ComboBox comboBox_endTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_endDay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_startTime;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel_Contents;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_WE_TM_MES_DTL;
        private ChartFX.WinForms.Chart chart1;
        private Infragistics.Win.UltraWinCalcManager.UltraCalcManager ultraCalcManager1;
    }
}