﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChartFX.WinForms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion

namespace WaterNet.WE_BaseInfo
{
    public partial class frmMeasurSearch : Form, WaterNet.WaterNetCore.IForminterface
    {
        #region Private Field ------------------------------------------------------------------------------
        private DataTable m_MasterTable;
        private WE_Common.GridManager m_gridManager1 = default(WE_Common.GridManager);  //모의분석 그리드
        //private string m_tag = "flat"; //그리드의 삭제가능 필드
        //private int m_startColumn = 2;  //그리드의 펌프호기시작 칼럼

        private DataSet m_DataSet = new DataSet();  //데이터 셋
        #endregion



        #region public Field  -----------------------------------------------------------------------------------------

        #endregion

        #region 생성자 및 환경설정 ---------------------------------------------------------------------- 
        /// <summary>
        /// 생성자
        /// </summary>
        public frmMeasurSearch()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            #region 그리드 설정

            UltraGridColumn oUltraGridColumn;
            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DD";
            oUltraGridColumn.Header.Caption = "가동일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "HM";
            oUltraGridColumn.Header.Caption = "가동시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            ////////////////////펌프 가동 VALUE
            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#1";
            oUltraGridColumn.Header.Caption = "#1";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#1";

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#2";
            oUltraGridColumn.Header.Caption = "#2";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#2";

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#3";
            oUltraGridColumn.Header.Caption = "#3";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#3";

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#4";
            oUltraGridColumn.Header.Caption = "#4";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#4";

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#5";
            oUltraGridColumn.Header.Caption = "#5";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#5";

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "#6";
            oUltraGridColumn.Header.Caption = "#6";
            oUltraGridColumn.CellActivation = Activation.AllowEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.EditAndSelectText;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Tag = "#6";

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OUTVAL";
            oUltraGridColumn.Header.Caption = "실측유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TM_CA_CHARGE";
            oUltraGridColumn.Header.Caption = "해석유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TM_CA_GAB";
            oUltraGridColumn.Header.Caption = "유량오차";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Formula = "([OUTVAL] - [TM_CA_CHARGE])";

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LVLVAL";
            oUltraGridColumn.Header.Caption = "실측압력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TM_CA_PRESS";
            oUltraGridColumn.Header.Caption = "해석압력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TM_RL_ELECT";
            oUltraGridColumn.Header.Caption = "실측전력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_WE_TM_MES_DTL.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TM_CA_ELECT";
            oUltraGridColumn.Header.Caption = "해석전력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "###,##0.00";
            oUltraGridColumn.Hidden = true;
            
            m_gridManager1 = new WE_Common.GridManager(ultraGrid_WE_TM_MES_DTL);
            m_gridManager1.SetGridStyle_Select();
            m_gridManager1.SetColumnWidth(70);
            #endregion

            #region ComboBox 설정

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT BIZ_PLA_SEQ AS CD, BIZ_PLA_NM AS CDNM FROM WE_BIZ_PLA ORDER BY BIZ_PLA_NM ASC");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_BIZ_PLA_NM, oStringBuilder.ToString(), false);

            object[,] aoSource = {
                {"0","00"},
                {"1","01"},
                {"2","02"},
                {"3","03"},
                {"4","04"},
                {"5","05"},
                {"6","06"},
                {"7","07"},
                {"8","08"},
                {"9","09"},
                {"10","10"},
                {"11","11"},
                {"12","12"},
                {"13","13"},
                {"14","14"},
                {"15","15"},
                {"16","16"},
                {"17","17"},
                {"18","18"},
                {"19","19"},
                {"20","20"},
                {"21","21"},
                {"22","22"},
                {"23","23"}
            };
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_startTime, aoSource, false);
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_endTime, aoSource, false);

            #endregion

            //그리드의 설정된 필드에 따라 빈 테이블을 생성한다.
            //다음의 테이블을 병합하기 위한 마스터데이블
            m_MasterTable = m_gridManager1.GetGridToDataTable();
            DataColumn oDD = getDataColumn(m_MasterTable, "DD");
            DataColumn oHM = getDataColumn(m_MasterTable, "HM");
            m_MasterTable.PrimaryKey = new DataColumn[] { oDD, oHM };

            //챠트 설정
            //차트 초기화
            this.chart1.LegendBox.MarginY = 1;
            this.chart1.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chart1.AxisX.DataFormat.CustomFormat = "yyyy-mm-dd hh:MM";
            this.chart1.AxisX.Staggered = true;
            
            //검색일자 초기화
            ultraDateTimeEditor_startDay.Value = DateTime.Today;
            ultraDateTimeEditor_endDay.Value = DateTime.Today.AddDays(1);
        }
        #endregion

        private void frmEffData_Load(object sender, EventArgs e)
        {
            //그리드의 Insert, Update구분 Event
            ultraGrid_WE_TM_MES_DTL.AfterRowInsert += new RowEventHandler(WaterNetCore.FormManager.AfterRowInsert);
            ultraGrid_WE_TM_MES_DTL.AfterCellUpdate += new CellEventHandler(WaterNetCore.FormManager.AfterCellUpdate);
        }

        /// <summary>
        /// 해당 테이블에 칼럼을 리턴한다.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private DataColumn getDataColumn(DataTable table, string key)
        {
            DataColumn oDataColumn = null;

            if (table != null)
            {
                foreach (DataColumn item in table.Columns)
                {
                    if (item.ColumnName == key)
                    {
                        return item;
                    }
                }
            }

            return oDataColumn;
        }

        public void Open()
        {
            
        }

        #endregion

        #region IForminterface 멤버

        public string FormID
        {
            get { return "실시간모델 오차분석"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion


        /// <summary>
        /// 챠트 그리기
        /// </summary>
        private void DrawChartData()
        {
            this.chart1.Data.Series = 30;
            this.chart1.Data.Points = m_gridManager1.Grid.Rows.Count;

            this.chart1.Series[0].Text = "실측유량";
            this.chart1.Series[1].Text = "해석유량";
            this.chart1.Series[2].Text = "실측압력";
            this.chart1.Series[3].Text = "해석압력";

            this.chart1.Series[0].Gallery = Gallery.Curve;
            this.chart1.Series[1].Gallery = Gallery.Curve;
            this.chart1.Series[2].Gallery = Gallery.Curve;
            this.chart1.Series[3].Gallery = Gallery.Curve;

            this.chart1.Series[0].AxisY = chart1.AxisY;
            this.chart1.Series[1].AxisY = chart1.AxisY;
            this.chart1.Series[2].AxisY = chart1.AxisY2;
            this.chart1.Series[3].AxisY = chart1.AxisY2;

            foreach (UltraGridRow row in m_gridManager1.Grid.Rows)
            {
                int idx = m_gridManager1.Grid.Rows.IndexOf(row);
                this.chart1.AxisX.Labels[idx] = Convert.ToString(row.Cells["HM"].Value);
                this.chart1.AxisX.Labels[idx] = Convert.ToString(row.Cells["HM"].Value);

                this.chart1.Data[0, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["OUTVAL"].Value);
                this.chart1.Data[1, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["TM_CA_CHARGE"].Value);
                this.chart1.Data[2, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["LVLVAL"].Value);
                this.chart1.Data[3, idx] = WE_Common.WE_FunctionManager.convertToDouble(row.Cells["TM_CA_PRESS"].Value);
            }

            this.chart1.Series[0].Visible = true;
            this.chart1.Series[1].Visible = true;
            this.chart1.Series[2].Visible = true;
            this.chart1.Series[3].Visible = true;
        }

        /// <summary>
        /// 실시간 해석결과 Node 데이터
        /// WH_RPT_NODES
        /// </summary>
        private void getRpt_Nodes()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();
                string startDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_startDay.Value);
                string endDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_endDay.Value);

                oStringBuilder.AppendLine("SELECT /*+ index_desc(if_gather_realtime_e XPKIF_GATHER_REALTIME_E) */                                    ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD hh24:MI:SS'), 'YYYY-MM-DD') AS DD                        ");
                oStringBuilder.AppendLine("      , TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS'), 'HH24:MI') AS HM                          ");
                oStringBuilder.AppendLine("      , NVL(P.HEAD,0) AS TM_CA_PRESS --해석수위                                                  ");
                oStringBuilder.AppendLine("FROM                                                                                                     ");
                oStringBuilder.AppendLine(" (SELECT DISTINCT TO_CHAR(TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP   ");
                oStringBuilder.AppendLine("  FROM IF_GATHER_REALTIME_E                                                                              ");
                oStringBuilder.AppendLine("  WHERE TO_CHAR(TIMESTAMP, 'YYYYMMDD') BETWEEN '" + startDay + "'");
                oStringBuilder.AppendLine("         AND '" + endDay + "') A");

                oStringBuilder.AppendLine(" INNER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.HEAD                                                                                        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (                                                                                        ");
                oStringBuilder.AppendLine("       SELECT B.RPT_NUMBER, B.INP_NUMBER, B.HEAD                                                         ");
                oStringBuilder.AppendLine("       FROM WE_BIZ_PLA A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_NODES B ON A.AN_LVL_MDLID = B.NODE_ID                                          ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'");
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') BETWEEN '" + startDay + "'");
                oStringBuilder.AppendLine("         AND '" + endDay + "'");
                oStringBuilder.AppendLine("    ) P ON A.TIMESTAMP = P.TIMESTAMP                                                                     ");
                oStringBuilder.AppendLine("ORDER BY DD, HM ASC                                                                                      ");

#if DEBUG
     //Console.WriteLine(oStringBuilder.ToString());
#endif
                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "WH_RPT_NODES");
                DataColumn oDD = FunctionManager.getDataColumn(oDataTable, "DD");
                DataColumn oHM = FunctionManager.getDataColumn(oDataTable, "HM");

                oDataTable.PrimaryKey = new DataColumn[] { oDD, oHM };
                m_DataSet.Tables.Add(oDataTable);

            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 실시간 해석결과 Link 데이터
        /// WH_RPT_LINKS
        /// </summary>
        private void getRpt_Links()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                string startDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_startDay.Value);
                string endDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_endDay.Value);

                oStringBuilder.AppendLine("SELECT /*+ index_desc(if_gather_realtime_e XPKIF_GATHER_REALTIME_E) */                                    ");
                oStringBuilder.AppendLine("       TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD hh24:MI:SS'), 'YYYY-MM-DD') AS DD                        ");
                oStringBuilder.AppendLine("      , TO_CHAR(TO_DATE(A.TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS'), 'HH24:MI') AS HM                          ");
                oStringBuilder.AppendLine("      , NVL(P.FLOW,0) AS TM_CA_CHARGE --해석유량                                                         ");
                oStringBuilder.AppendLine("FROM                                                                                                     ");
                oStringBuilder.AppendLine(" (SELECT DISTINCT TO_CHAR(TIMESTAMP, 'YYYYMMDDHH24MISS') AS TIMESTAMP   ");
                oStringBuilder.AppendLine("  FROM IF_GATHER_REALTIME_E                                                                              ");
                oStringBuilder.AppendLine("  WHERE TO_CHAR(TIMESTAMP, 'YYYYMMDD') BETWEEN '" + startDay + "'");
                oStringBuilder.AppendLine("         AND '" + endDay + "') A");

                oStringBuilder.AppendLine(" LEFT OUTER JOIN (                                                                                       ");
                oStringBuilder.AppendLine("    SELECT TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD HH24:MI:SS'), 'YYYYMMDDHH24MI') || '00' AS TIMESTAMP  ");
                oStringBuilder.AppendLine("         , C.FLOW                                                                                        ");
                oStringBuilder.AppendLine("    FROM WH_RPT_MASTER A                                                                                 ");
                oStringBuilder.AppendLine("     INNER JOIN (                                                                                        ");
                oStringBuilder.AppendLine("       SELECT B.RPT_NUMBER, B.INP_NUMBER, B.FLOW                                                         ");
                oStringBuilder.AppendLine("       FROM WE_BIZ_PLA A                                                                                 ");
                oStringBuilder.AppendLine("        INNER JOIN WH_RPT_LINKS B ON A.AN_OUT_MDLID = B.LINK_ID                                          ");
                oStringBuilder.AppendLine("                      AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "'"                    );
                oStringBuilder.AppendLine("       ) C ON A.RPT_NUMBER = C.RPT_NUMBER AND A.INP_NUMBER = C.INP_NUMBER                                ");
                oStringBuilder.AppendLine("              AND A.INP_NUMBER = (SELECT RL_AN_MDL_NO FROM WE_BIZ_PLA WHERE BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "')");
                oStringBuilder.AppendLine("     WHERE TO_CHAR(TO_DATE(A.RPT_DATE, 'YYYY-MM-DD hh24:MI:SS'), 'YYYYMMDD') BETWEEN '" + startDay + "'");
                oStringBuilder.AppendLine("         AND '" + endDay + "'");
                oStringBuilder.AppendLine("    ) P ON A.TIMESTAMP = P.TIMESTAMP                                                                     ");
                oStringBuilder.AppendLine("ORDER BY DD, HM ASC                                                                                      ");

#if DEBUG
                //Console.WriteLine(oStringBuilder.ToString());
#endif

                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "WH_RPT_LINKS");
                DataColumn oDD = FunctionManager.getDataColumn(oDataTable, "DD");
                DataColumn oHM = FunctionManager.getDataColumn(oDataTable, "HM");

                oDataTable.PrimaryKey = new DataColumn[] { oDD, oHM };

                m_DataSet.Tables.Add(oDataTable);
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 날짜 및 시간 + 펌프별 가동여부 실시간 데이터
        /// IF_GATHER_REALTIME_E
        /// </summary>
        private void getGather_Realtime()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                string startDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_startDay.Value);
                string endDay = FunctionManager.DateTimeToString((DateTime)ultraDateTimeEditor_endDay.Value);

                oStringBuilder.AppendLine("SELECT /*+ index_desc(if_gather_realtime_e XPKIF_GATHER_REALTIME_E) */ ");
                oStringBuilder.AppendLine("       TO_CHAR(A.TIMESTAMP, 'YYYY-MM-DD') AS DD,");
                oStringBuilder.AppendLine("       TO_CHAR(A.TIMESTAMP, 'HH24:MI') AS HM,");
                oStringBuilder.AppendLine("       P1.VALUE AS \"#1\", P2.VALUE AS \"#2\" , P3.VALUE AS \"#3\",");
                oStringBuilder.AppendLine("       P4.VALUE AS \"#4\", P5.VALUE AS \"#5\" , P6.VALUE AS \"#6\", NVL(P7.VALUE,0) AS LVLVAL, NVL(P8.VALUE,0) AS OUTVAL");
                oStringBuilder.AppendLine("FROM");
                oStringBuilder.AppendLine(" (SELECT DISTINCT TIMESTAMP");
                oStringBuilder.AppendLine("  FROM IF_GATHER_REALTIME_E");
                oStringBuilder.AppendLine("  WHERE TO_CHAR(TIMESTAMP, 'YYYYMMDD') BETWEEN '" + startDay + "' AND '" + endDay + "') A");
                oStringBuilder.AppendLine(" LEFT OUTER JOIN (");
                oStringBuilder.AppendLine("  SELECT A.OPR_TAGID_1, TIMESTAMP, VALUE, '#1' AS PP_HOGI");
                oStringBuilder.AppendLine("  FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("   INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_1 = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P1 ON A.TIMESTAMP = P1.TIMESTAMP");
                oStringBuilder.AppendLine(" INNER JOIN (");
                oStringBuilder.AppendLine("   SELECT A.OPR_TAGID_2, TIMESTAMP, VALUE, '#2' AS PP_HOGI");
                oStringBuilder.AppendLine("   FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("   INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_2 = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P2 ON A.TIMESTAMP = P2.TIMESTAMP");
                oStringBuilder.AppendLine(" INNER JOIN(");
                oStringBuilder.AppendLine("   SELECT A.OPR_TAGID_3, TIMESTAMP, VALUE, '#3' AS PP_HOGI");
                oStringBuilder.AppendLine("   FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("    INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_3 = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P3 ON A.TIMESTAMP = P3.TIMESTAMP");
                oStringBuilder.AppendLine(" INNER JOIN(");
                oStringBuilder.AppendLine("   SELECT A.OPR_TAGID_4, TIMESTAMP, VALUE, '#4' AS PP_HOGI");
                oStringBuilder.AppendLine("   FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("    INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_4 = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P4 ON A.TIMESTAMP = P4.TIMESTAMP");
                oStringBuilder.AppendLine(" INNER JOIN(");
                oStringBuilder.AppendLine("   SELECT A.OPR_TAGID_5, TIMESTAMP, VALUE, '#5' AS PP_HOGI");
                oStringBuilder.AppendLine("   FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("    INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_5 = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P5 ON A.TIMESTAMP = P5.TIMESTAMP");
                oStringBuilder.AppendLine(" INNER JOIN(");
                oStringBuilder.AppendLine("   SELECT A.OPR_TAGID_6, TIMESTAMP, VALUE, '#6' AS PP_HOGI");
                oStringBuilder.AppendLine("   FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("    INNER JOIN IF_GATHER_REALTIME_E B ON A.OPR_TAGID_6 = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P6 ON A.TIMESTAMP = P6.TIMESTAMP");
                oStringBuilder.AppendLine(" INNER JOIN(");
                oStringBuilder.AppendLine("   SELECT A.LVL_TAGID, TIMESTAMP, VALUE, '#6' AS PP_HOGI");
                oStringBuilder.AppendLine("   FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("    INNER JOIN IF_GATHER_REALTIME_E B ON A.LVL_TAGID = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P7 ON A.TIMESTAMP = P7.TIMESTAMP    ");
                oStringBuilder.AppendLine(" INNER JOIN(");
                oStringBuilder.AppendLine("   SELECT A.OUT_TAGID, TIMESTAMP, VALUE, '#6' AS PP_HOGI");
                oStringBuilder.AppendLine("   FROM WE_BIZ_PLA A");
                oStringBuilder.AppendLine("    INNER JOIN IF_GATHER_REALTIME_E B ON A.OUT_TAGID = B.TAGNAME AND A.BIZ_PLA_SEQ = '" + comboBox_BIZ_PLA_NM.SelectedValue + "') P8 ON A.TIMESTAMP = P8.TIMESTAMP  ");
                oStringBuilder.AppendLine(" ORDER BY DD, HM ASC");

#if DEBUG
                //Console.WriteLine(oStringBuilder.ToString());
#endif

                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "IF_GATHER_REALTIME");
                DataColumn oDD = FunctionManager.getDataColumn(oDataTable, "DD");
                DataColumn oHM = FunctionManager.getDataColumn(oDataTable, "HM");
                oDataTable.PrimaryKey = new DataColumn[] { oDD, oHM };

                m_DataSet.Tables.Add(oDataTable);
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 조회 버튼 클릭 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                m_DataSet.Tables.Clear();

                getGather_Realtime();

                getRpt_Links();

                getRpt_Nodes();

                SetMergedDataTable();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// 데이터 테이블 Merge
        /// </summary>
        private void SetMergedDataTable()
        {
            m_MasterTable.Rows.Clear();

            m_MasterTable.Merge(m_DataSet.Tables["IF_GATHER_REALTIME"], false, MissingSchemaAction.Ignore);
            m_MasterTable.Merge(m_DataSet.Tables["WH_RPT_LINKS"], false, MissingSchemaAction.Ignore);
            m_MasterTable.Merge(m_DataSet.Tables["WH_RPT_NODES"], false, MissingSchemaAction.Ignore);

            m_gridManager1.SetDataSource(m_MasterTable);

            m_gridManager1.SetGridSort("DD");
            m_gridManager1.SetGridSort("HM");

            DrawChartData();

        }

        private void ultraDateTimeEditor_startDay_ValueChanged(object sender, EventArgs e)
        {
            ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(1);
        }

        private void ultraDateTimeEditor_endDay_ValueChanged(object sender, EventArgs e)
        {
            if (((DateTime)ultraDateTimeEditor_endDay.Value) < ((DateTime)ultraDateTimeEditor_startDay.Value))
            {
                MessageManager.ShowInformationMessage("종료일이 시작일보다 작을수는 없습니다.");
                ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(1);
                return;
            }

            if (((DateTime)ultraDateTimeEditor_endDay.Value) > ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(7))
            {
                MessageManager.ShowInformationMessage("최대 검색기간은 7일 이내입니다.");
                ultraDateTimeEditor_endDay.Value = ((DateTime)ultraDateTimeEditor_startDay.Value).AddDays(1);
                return;
            }
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {

        }
    }
}
