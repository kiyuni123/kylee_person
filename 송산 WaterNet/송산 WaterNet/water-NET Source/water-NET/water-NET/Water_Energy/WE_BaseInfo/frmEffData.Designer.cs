﻿namespace WaterNet.WE_BaseInfo
{
    partial class frmEffData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.comboBox_BIZ_PLA_NM = new System.Windows.Forms.ComboBox();
            this.label_BIZ_PLA_NM = new System.Windows.Forms.Label();
            this.comboBox_EFF_YEAR = new System.Windows.Forms.ComboBox();
            this.comboBox_PP_HOGI = new System.Windows.Forms.ComboBox();
            this.label_EFF_YEAR = new System.Windows.Forms.Label();
            this.label_PP_HOGI = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnSel = new System.Windows.Forms.Button();
            this.btnFile = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.m_dataSet = new System.Data.DataSet();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.panel_Contents = new System.Windows.Forms.Panel();
            this.ultraGridWE_PP_EFF_DATA = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataSet)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel_Contents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridWE_PP_EFF_DATA)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnEdit);
            this.panelCommand.Controls.Add(this.comboBox_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.comboBox_EFF_YEAR);
            this.panelCommand.Controls.Add(this.comboBox_PP_HOGI);
            this.panelCommand.Controls.Add(this.label_EFF_YEAR);
            this.panelCommand.Controls.Add(this.label_PP_HOGI);
            this.panelCommand.Controls.Add(this.btnDel);
            this.panelCommand.Controls.Add(this.btnSel);
            this.panelCommand.Controls.Add(this.btnFile);
            this.panelCommand.Controls.Add(this.btnAdd);
            this.panelCommand.Controls.Add(this.btnSave);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(1109, 55);
            this.panelCommand.TabIndex = 2;
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Location = new System.Drawing.Point(860, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 13;
            this.btnEdit.Text = "수정";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // comboBox_BIZ_PLA_NM
            // 
            this.comboBox_BIZ_PLA_NM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BIZ_PLA_NM.FormattingEnabled = true;
            this.comboBox_BIZ_PLA_NM.Location = new System.Drawing.Point(60, 18);
            this.comboBox_BIZ_PLA_NM.Name = "comboBox_BIZ_PLA_NM";
            this.comboBox_BIZ_PLA_NM.Size = new System.Drawing.Size(111, 20);
            this.comboBox_BIZ_PLA_NM.TabIndex = 12;
            this.comboBox_BIZ_PLA_NM.SelectedIndexChanged += new System.EventHandler(this.comboBox_BIZ_PLA_NM_SelectedIndexChanged);
            this.comboBox_BIZ_PLA_NM.SelectedValueChanged += new System.EventHandler(this.comboBox_BIZ_PLA_NM_SelectedValueChanged);
            // 
            // label_BIZ_PLA_NM
            // 
            this.label_BIZ_PLA_NM.AutoSize = true;
            this.label_BIZ_PLA_NM.Location = new System.Drawing.Point(9, 22);
            this.label_BIZ_PLA_NM.Name = "label_BIZ_PLA_NM";
            this.label_BIZ_PLA_NM.Size = new System.Drawing.Size(41, 12);
            this.label_BIZ_PLA_NM.TabIndex = 11;
            this.label_BIZ_PLA_NM.Text = "사업장";
            // 
            // comboBox_EFF_YEAR
            // 
            this.comboBox_EFF_YEAR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_EFF_YEAR.FormattingEnabled = true;
            this.comboBox_EFF_YEAR.Location = new System.Drawing.Point(414, 18);
            this.comboBox_EFF_YEAR.Name = "comboBox_EFF_YEAR";
            this.comboBox_EFF_YEAR.Size = new System.Drawing.Size(111, 20);
            this.comboBox_EFF_YEAR.TabIndex = 10;
            // 
            // comboBox_PP_HOGI
            // 
            this.comboBox_PP_HOGI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PP_HOGI.FormattingEnabled = true;
            this.comboBox_PP_HOGI.Location = new System.Drawing.Point(252, 18);
            this.comboBox_PP_HOGI.Name = "comboBox_PP_HOGI";
            this.comboBox_PP_HOGI.Size = new System.Drawing.Size(111, 20);
            this.comboBox_PP_HOGI.TabIndex = 9;
            // 
            // label_EFF_YEAR
            // 
            this.label_EFF_YEAR.AutoSize = true;
            this.label_EFF_YEAR.Location = new System.Drawing.Point(379, 21);
            this.label_EFF_YEAR.Name = "label_EFF_YEAR";
            this.label_EFF_YEAR.Size = new System.Drawing.Size(29, 12);
            this.label_EFF_YEAR.TabIndex = 8;
            this.label_EFF_YEAR.Text = "년도";
            // 
            // label_PP_HOGI
            // 
            this.label_PP_HOGI.AutoSize = true;
            this.label_PP_HOGI.Location = new System.Drawing.Point(188, 21);
            this.label_PP_HOGI.Name = "label_PP_HOGI";
            this.label_PP_HOGI.Size = new System.Drawing.Size(53, 12);
            this.label_PP_HOGI.TabIndex = 7;
            this.label_PP_HOGI.Text = "펌프호기";
            // 
            // btnDel
            // 
            this.btnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDel.Location = new System.Drawing.Point(941, 12);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(75, 30);
            this.btnDel.TabIndex = 6;
            this.btnDel.Text = "삭제";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnSel
            // 
            this.btnSel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSel.Location = new System.Drawing.Point(619, 12);
            this.btnSel.Name = "btnSel";
            this.btnSel.Size = new System.Drawing.Size(75, 30);
            this.btnSel.TabIndex = 5;
            this.btnSel.Text = "조회";
            this.btnSel.UseVisualStyleBackColor = true;
            this.btnSel.Click += new System.EventHandler(this.btnSel_Click);
            // 
            // btnFile
            // 
            this.btnFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFile.Location = new System.Drawing.Point(700, 12);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(75, 30);
            this.btnFile.TabIndex = 4;
            this.btnFile.Text = "측정파일";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(779, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "추가";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(1022, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // m_dataSet
            // 
            this.m_dataSet.DataSetName = "NewDataSet";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 55);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chart1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel_Contents);
            this.splitContainer1.Size = new System.Drawing.Size(1109, 539);
            this.splitContainer1.SplitterDistance = 255;
            this.splitContainer1.TabIndex = 4;
            // 
            // chart1
            // 
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(1109, 255);
            this.chart1.TabIndex = 0;
            // 
            // panel_Contents
            // 
            this.panel_Contents.Controls.Add(this.ultraGridWE_PP_EFF_DATA);
            this.panel_Contents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Contents.Location = new System.Drawing.Point(0, 0);
            this.panel_Contents.Name = "panel_Contents";
            this.panel_Contents.Size = new System.Drawing.Size(1109, 280);
            this.panel_Contents.TabIndex = 4;
            // 
            // ultraGridWE_PP_EFF_DATA
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Appearance = appearance13;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.CellAppearance = appearance20;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.RowAppearance = appearance23;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridWE_PP_EFF_DATA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGridWE_PP_EFF_DATA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridWE_PP_EFF_DATA.Location = new System.Drawing.Point(0, 0);
            this.ultraGridWE_PP_EFF_DATA.Name = "ultraGridWE_PP_EFF_DATA";
            this.ultraGridWE_PP_EFF_DATA.Size = new System.Drawing.Size(1109, 280);
            this.ultraGridWE_PP_EFF_DATA.TabIndex = 3;
            // 
            // frmEffData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 594);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmEffData";
            this.Text = "frmEffData";
            this.Load += new System.EventHandler(this.frmEffData_Load);
            this.panelCommand.ResumeLayout(false);
            this.panelCommand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataSet)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel_Contents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridWE_PP_EFF_DATA)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnSel;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label_PP_HOGI;
        private System.Windows.Forms.ComboBox comboBox_EFF_YEAR;
        private System.Windows.Forms.ComboBox comboBox_PP_HOGI;
        private System.Windows.Forms.Label label_EFF_YEAR;
        private System.Data.DataSet m_dataSet;
        private System.Windows.Forms.ComboBox comboBox_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_BIZ_PLA_NM;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel_Contents;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridWE_PP_EFF_DATA;
        private ChartFX.WinForms.Chart chart1;
    }
}