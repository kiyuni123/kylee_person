﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion

namespace WaterNet.WE_BaseInfo
{
    public partial class frmBaseResource : Form, WaterNet.WaterNetCore.IForminterface
    {
        private GridManager m_gridManager_WE_BIZ_PLA = default(GridManager);  //사업장 그리드
        private GridManager m_gridManager_WE_PP_INFO = default(GridManager);  //펌프 그리드

        private FormManager.SubFormEventType m_FormeventType_WE_PP_INFO = FormManager.SubFormEventType.isViewer;
        private string m_strQuerySelect_WE_BIZ_PLA = string.Empty;
        //private string m_strQuerySelect_WE_FAC_INFO = string.Empty;
        private string m_strQuerySelect_WE_PP_INFO = string.Empty;

        public frmBaseResource()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "기초정보"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        ///// <summary>
        ///// WE_FAC_INFO - 시설물기초정보 추가,수정,조회 이벤트
        ///// </summary>
        //private FormManager.SubFormEventType FormeventType_WE_FAC_INFO
        //{
        //    get { return m_FormeventType_WE_PP_INFO; }
        //    set  {
        //        m_FormeventType_WE_PP_INFO = value;
        //        switch (m_FormeventType_WE_PP_INFO)
        //        {
        //            case FormManager.SubFormEventType.isAppend:
        //                FormManager.SetGridStyle_Append(ultraGrid_WE_FAC_INFO);
        //                break;
        //            case FormManager.SubFormEventType.isUpdate:
        //                FormManager.SetGridStyle_Update(ultraGrid_WE_FAC_INFO);
        //                break;
        //            case FormManager.SubFormEventType.isViewer:
        //                FormManager.SetGridStyle_UnAppend(ultraGrid_WE_FAC_INFO);
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //}

        #region 초기화설정
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region - 그리드 설정(사업장)
            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_PLA_SEQ";
            oUltraGridColumn.Header.Caption = "사업장일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_PLA_NM";
            oUltraGridColumn.Header.Caption = "사업장명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_PLA_ETC";
            oUltraGridColumn.Header.Caption = "사업장개요";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RL_AN_MDL_NO";
            oUltraGridColumn.Header.Caption = "모델번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_LVL_MDLID";
            oUltraGridColumn.Header.Caption = "해석모델항목ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LVL_TAGID";
            oUltraGridColumn.Header.Caption = "수위 tag-ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_OUT_MDLID";
            oUltraGridColumn.Header.Caption = "해석모델항목ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_ENGWON";
            oUltraGridColumn.Header.Caption = "단위전력요금";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OUT_TAGID";
            oUltraGridColumn.Header.Caption = "유출유량 tag-ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SG_CD";
            oUltraGridColumn.Header.Caption = "지자체코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USERID";
            oUltraGridColumn.Header.Caption = "등록자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENTDT";
            oUltraGridColumn.Header.Caption = "등록일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_BIZ_PLA.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHGDT";
            oUltraGridColumn.Header.Caption = "수정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            m_gridManager_WE_BIZ_PLA = new GridManager(ultraGrid_BIZ_PLA);
            m_gridManager_WE_BIZ_PLA.SetGridStyle_Select();
            
            WaterNetCore.FormManager.SetGridStyle(ultraGrid_BIZ_PLA);
            
            #endregion

            #region - 그리드 설정(시설물기초정보)
            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BIZ_PLA_SEQ";
            oUltraGridColumn.Header.Caption = "사업장일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FAC_SEQ";
            oUltraGridColumn.Header.Caption = "시설물장치일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_HOGI";
            oUltraGridColumn.Header.Caption = "호기번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_CAT";
            oUltraGridColumn.Header.Caption = "펌프구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_GRP";
            oUltraGridColumn.Header.Caption = "펌프그룹";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_STD_Q";
            oUltraGridColumn.Header.Caption = "정격유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_STD_H";
            oUltraGridColumn.Header.Caption = "정격양정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_IN_DIA";
            oUltraGridColumn.Header.Caption = "흡입구경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_OUT_DIA";
            oUltraGridColumn.Header.Caption = "토출구경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_TP";
            oUltraGridColumn.Header.Caption = "형식";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_MAKE_YR";
            oUltraGridColumn.Header.Caption = "제작년도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_USG_YR";
            oUltraGridColumn.Header.Caption = "내용년수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_EST_YR";
            oUltraGridColumn.Header.Caption = "설치년도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_PW";
            oUltraGridColumn.Header.Caption = "동력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_RPM";
            oUltraGridColumn.Header.Caption = "RPM";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_RATIO";
            oUltraGridColumn.Header.Caption = "설계비속도";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_EFF";
            oUltraGridColumn.Header.Caption = "설계효율";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_GRKSU";
            oUltraGridColumn.Header.Caption = "극수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_FRQ";
            oUltraGridColumn.Header.Caption = "주파수";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_CHK_VL_TP";
            oUltraGridColumn.Header.Caption = "체크밸브유형";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_IN_MDLID";
            oUltraGridColumn.Header.Caption = "유입유량해석항목 ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_OUT_MDLID";
            oUltraGridColumn.Header.Caption = "유출유량해석항목 ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USERID";
            oUltraGridColumn.Header.Caption = "등록자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENTDT";
            oUltraGridColumn.Header.Caption = "등록일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_WE_PP_INFO.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHGDT";
            oUltraGridColumn.Header.Caption = "수정일자";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            m_gridManager_WE_PP_INFO = new GridManager(ultraGrid_WE_PP_INFO);
            m_gridManager_WE_PP_INFO.SetGridStyle_Select();
            //------------------------------------------------------------------------------
            
            #endregion

            #region 컨트롤 초기화
            //콤보박스 데이터 초기화 
        
            object[,] aoSource4 = {
                {"메인펌프",   "메인펌프"},
                {"조절펌프",   "조절펌프"}
            };
            FormManager.SetComboBoxEX(comboBox_PP_CAT, aoSource4, false);

            object[,] aoSource5 = {
                {"A그룹",   "A그룹"},
                {"B그룹",   "B그룹"}
            };
            FormManager.SetComboBoxEX(comboBox_PP_GRP, aoSource5, false);

            object[,] aoSource6 = {
                {"유형1",   "유형1"},
                {"유형2",   "유형2"}
            };
            FormManager.SetComboBoxEX(comboBox_PP_CHK_VL_TP, aoSource5, false);
            

            #endregion

            #region 쿼리문 설정
            StringBuilder oStringBuilder = new StringBuilder();
            ////사업장 정보
            oStringBuilder.AppendLine("SELECT");
            oStringBuilder.AppendLine("    A.BIZ_PLA_SEQ AS BIZ_PLA_SEQ, -- 사업장일련번호");
            oStringBuilder.AppendLine("    A.BIZ_PLA_NM AS BIZ_PLA_NM, -- 사업장명");
            oStringBuilder.AppendLine("    A.BIZ_PLA_ETC AS BIZ_PLA_ETC, -- 사업장개요");
            oStringBuilder.AppendLine("    A.RL_AN_MDL_NO AS RL_AN_MDL_NO, -- 실시간해석모델번호");
            oStringBuilder.AppendLine("    A.AN_LVL_MDLID AS AN_LVL_MDLID, -- 해석모델항목ID");
            oStringBuilder.AppendLine("    A.LVL_TAGID AS LVL_TAGID, -- 수위 Tag");
            oStringBuilder.AppendLine("    A.AN_OUT_MDLID AS AN_OUT_MDLID, -- 해석모델항목ID");
            oStringBuilder.AppendLine("    A.OUT_TAGID AS OUT_TAGID, -- 유출유량Tag");
            oStringBuilder.AppendLine("    A.BIZ_ENGWON AS BIZ_ENGWON, -- 단위전력요금");
            oStringBuilder.AppendLine("    A.SG_CD AS SG_CD, -- 지자체코드");
            oStringBuilder.AppendLine("    A.USERID AS USERID, -- 등록자");
            oStringBuilder.AppendLine("    A.ENTDT AS ENTDT, -- 등록일자");
            oStringBuilder.AppendLine("    A.CHGDT AS CHGDT -- 수정일자");
            oStringBuilder.AppendLine("FROM WE_BIZ_PLA A -- ");
            m_strQuerySelect_WE_BIZ_PLA = oStringBuilder.ToString();

            ////펌프 정보
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT");
            oStringBuilder.AppendLine("    A.BIZ_PLA_SEQ AS BIZ_PLA_SEQ, -- 사업장일련번호");
            oStringBuilder.AppendLine("    A.FAC_SEQ AS FAC_SEQ, -- 시설물장치일련번호");
            oStringBuilder.AppendLine("    A.PP_HOGI AS PP_HOGI, -- 호기번호");
            oStringBuilder.AppendLine("    A.PP_CAT AS PP_CAT, -- 펌프구분");
            oStringBuilder.AppendLine("    A.PP_GRP AS PP_GRP, -- 펌프그룹");
            oStringBuilder.AppendLine("    A.PP_STD_Q AS PP_STD_Q, -- 정격유량");
            oStringBuilder.AppendLine("    A.PP_STD_H AS PP_STD_H, -- 정격양정");
            oStringBuilder.AppendLine("    A.PP_IN_DIA AS PP_IN_DIA, -- 흡입구경");
            oStringBuilder.AppendLine("    A.PP_OUT_DIA AS PP_OUT_DIA, -- 토출구경");
            oStringBuilder.AppendLine("    A.PP_TP AS PP_TP, -- 형식");
            oStringBuilder.AppendLine("    A.PP_MAKE_YR AS PP_MAKE_YR, -- 제작년도");
            oStringBuilder.AppendLine("    A.PP_USG_YR AS PP_USG_YR, -- 내용년수");
            oStringBuilder.AppendLine("    A.PP_EST_YR AS PP_EST_YR, -- 설치년도");
            oStringBuilder.AppendLine("    A.PP_PW AS PP_PW, -- 동력");
            oStringBuilder.AppendLine("    A.PP_RPM AS PP_RPM, -- RPM");
            oStringBuilder.AppendLine("    A.PP_RATIO AS PP_RATIO, -- 설계비속도");
            oStringBuilder.AppendLine("    A.PP_EFF AS PP_EFF, -- 설계효율");
            oStringBuilder.AppendLine("    A.PP_GRKSU AS PP_GRKSU, -- 극수");
            oStringBuilder.AppendLine("    A.PP_FRQ AS PP_FRQ, -- 주파수");
            oStringBuilder.AppendLine("    A.PP_CHK_VL_TP AS PP_CHK_VL_TP, -- 체크밸브유형");
            oStringBuilder.AppendLine("    A.AN_IN_MDLID AS AN_IN_MDLID, -- 유입유량해석항목 ID");
            oStringBuilder.AppendLine("    A.AN_OUT_MDLID AS AN_OUT_MDLID, -- 유출유량해석항목 ID");
            oStringBuilder.AppendLine("    A.USERID AS USERID, -- 등록자");
            oStringBuilder.AppendLine("    A.ENTDT AS ENTDT, -- 등록일자");
            oStringBuilder.AppendLine("    A.CHGDT AS CHGDT -- 수정일자");
            oStringBuilder.AppendLine("FROM WE_PP_INFO A -- WE_PP_INFO");

            m_strQuerySelect_WE_PP_INFO = oStringBuilder.ToString();
            #endregion

        }
        #endregion

        /// <summary>
        /// Form Open
        /// </summary>
        public void Open()
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuBaseInfo"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnBizAdd.Enabled = false;
                this.btnBizDel.Enabled = false;
                this.btnFacAdd.Enabled = false;
                this.btnFacDel.Enabled = false;
                this.btnFacEdit.Enabled = false;
                this.btnFacSave.Enabled = false;
            }

            Select_WE_BIZ_PLA();
        }

        /// <summary>
        /// 환경 초기화
        /// </summary>
        private void InitializeControls()
        {
            //컨트롤 초기화
            FormManager.InitializePanel(panel_WE_PP_INFOS);

            this.m_FormeventType_WE_PP_INFO = FormManager.SubFormEventType.isViewer;
        }

        /// <summary>
        /// 버튼 및 콤포넌트 Enable 설정
        /// </summary>
        private void setCommandStatus()
        {
            switch (this.m_FormeventType_WE_PP_INFO)
            {
                case FormManager.SubFormEventType.isAppend:
                case FormManager.SubFormEventType.isUpdate:
                    btnFacSave.Enabled = true;
                    btnFacDel.Enabled = true;
                    btnFacAdd.Enabled = true;
                    FormManager.SetEnabledToControls(panel_WE_PP_INFOS, true);
                    break;
                case FormManager.SubFormEventType.isViewer:
                    btnFacSave.Enabled = false;
                    btnFacDel.Enabled = true;
                    btnFacAdd.Enabled = true;
                    FormManager.SetEnabledToControls(panel_WE_PP_INFOS, false);
                    break;
            }
        }

        /// <summary>
        /// 사업장정보를 쿼리한다
        /// </summary>
        public void Select_WE_BIZ_PLA()
        {
            string strOrder = " ORDER BY A.BIZ_PLA_NM ASC ";

            DataSet pDS = FunctionManager.GetDataSet(m_strQuerySelect_WE_BIZ_PLA + strOrder, "WE_BIZ_PLA");

            ultraGrid_BIZ_PLA.DataSource = pDS.Tables["WE_BIZ_PLA"].DefaultView;

            ultraGrid_BIZ_PLA.ActiveRow = null;
        }



        /// <summary>
        /// Form Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBaseResource_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================에너지관리 

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuBaseInfo"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnFacAdd.Enabled = false;
                this.btnFacDel.Enabled = false;
                this.btnFacSave.Enabled = false;
            }

            //===========================================================================
        }

        /// <summary>
        /// 사업장 그리드 선택 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid_BIZ_PLA_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            UltraGridRow oRow = ultraGrid_BIZ_PLA.ActiveRow;
            if (oRow == null) return;

            Select_WE_PP_INFO(Convert.ToString(oRow.Cells["BIZ_PLA_SEQ"].Value));

            InitializeControls();
            FormManager.SetEnabledToControls(panel_WE_PP_INFOS, false);
        }

        #region WE_BIZ_PLA 사업장 - 추가, 삭제, 수정
        /// <summary>
        /// 사업장 상세정보 조회 및 수정화면 Open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid_BIZ_PLA_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            frmBizPlace form = new frmBizPlace(FormManager.SubFormEventType.isUpdate);
            form.row = e.Row;
            form.Open();
            if (form.DialogResult == DialogResult.OK)
            {
                this.Select_WE_BIZ_PLA();
            }
        }

        /// <summary>
        /// 사업장 상세정보 추가화면 Open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBizAdd_Click(object sender, EventArgs e)
        {
            frmBizPlace form = new frmBizPlace(FormManager.SubFormEventType.isAppend);
            form.Open();
            if (form.DialogResult == DialogResult.OK)
            {
                this.Select_WE_BIZ_PLA();
            }
        }

        /// <summary>
        /// 사업장 정보 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBizDel_Click(object sender, EventArgs e)
        {
            UltraGridRow row = ultraGrid_BIZ_PLA.ActiveRow;
            if (row == null)
            {
                MessageManager.ShowInformationMessage("선택된 목록이 없습니다.");
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("SELECT * FROM WE_FAC_INFO");
            //oStringBuilder.AppendLine("WHERE BIZ_PLA_SEQ = '" + row.Cells["BIZ_PLA_SEQ"].Value + "'");
            //int i = FunctionManager.ExecuteScriptCount(oStringBuilder.ToString(), null);
            //if (i > 0)
            //{
            //    MessageManager.ShowInformationMessage("[" + row.Cells["BIZ_PLA_NM"].Value + "] 사업장에 시설물 데이터가 있습니다.");
            //    return;
            //}

            if (MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_DELETE) != DialogResult.Yes) return;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE FROM WE_BIZ_PLA");
            oStringBuilder.AppendLine("WHERE BIZ_PLA_SEQ = '" + row.Cells["BIZ_PLA_SEQ"].Value + "'");

            try
            {
                int i = FunctionManager.ExecuteScript(oStringBuilder.ToString(), null);
                if (i >= 0) row.Delete(false);
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);
            }
            catch (Exception ex)
            {
#if DEBUG
                MessageBox.Show(ex.Message);
#endif
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
            }
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------------------------------
        //  시설물 정보
        //-----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 시설물 정보를 쿼리한다.
        /// </summary>
        /// <param name="pSEQ"></param>
        public void Select_WE_PP_INFO(string pSEQ)
        {
            string strWhere = string.Empty;
            string strOrder = string.Empty;

            strWhere = " A.BIZ_PLA_SEQ = '" + pSEQ + "'";
            strOrder = " ORDER BY A.FAC_SEQ ASC";

            Clipboard.SetText(m_strQuerySelect_WE_PP_INFO + " WHERE " + strWhere + strOrder);
            DataSet pDS = FunctionManager.GetDataSet(m_strQuerySelect_WE_PP_INFO + " WHERE " + strWhere + strOrder, "WE_PP_INFO");

            if (pDS != null) ultraGrid_WE_PP_INFO.DataSource = pDS.Tables["WE_PP_INFO"].DefaultView;
        }

        /// <summary>
        /// 시설물기초정보 그리드를 선택한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid_WE_PP_INFO_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            UltraGridRow oRow = ultraGrid_WE_PP_INFO.ActiveRow;
            if (oRow == null) return;

            string strWhere = string.Empty;
            string strOrder = string.Empty;

            strWhere = " A.FAC_SEQ = '" + oRow.Cells["FAC_SEQ"].Value + "' AND A.BIZ_PLA_SEQ ='" + oRow.Cells["BIZ_PLA_SEQ"].Value + "'";
            strOrder = " ORDER BY A.FAC_SEQ ASC";

            Clipboard.SetText(m_strQuerySelect_WE_PP_INFO + " WHERE " + strWhere + strOrder);
            DataSet pDS = FunctionManager.GetDataSet(m_strQuerySelect_WE_PP_INFO + " WHERE " + strWhere + strOrder, "WE_FAC_INFO");

            FormManager.SetValueToControls(panel_WE_PP_INFO, pDS.Tables["WE_FAC_INFO"]);
        }

        #region ExecuteQueryDelete
        // 펌프기본, 계측기정보 삭제
        private void ExecuteQueryDelete_WE_PP_INFO(UltraGridRow oRow)
        {
            if (oRow == null) return;

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE FROM WE_PP_INFO");
            oStringBuilder.AppendLine("WHERE FAC_SEQ = '" + oRow.Cells["FAC_SEQ"].Value + "'");

            FunctionManager.ExecuteScript(oStringBuilder.ToString(), null);

            oRow.Delete(false);
        }
        #endregion

        #region ExecuteQueryInsert
        //펌프시설 기초정보, 펌프상세정보 추가
        private void ExecuteQueryInsert_WE_PP_INFO(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            if (oRow == null) return;
            StringBuilder oStringBuilder = new StringBuilder();

            #region 펌프기본정보
            StringBuilder oStringBuilder2 = new StringBuilder();
            oStringBuilder2.AppendLine("INSERT INTO WE_PP_INFO (");
            oStringBuilder2.AppendLine("FAC_SEQ,");
            oStringBuilder2.AppendLine("PP_HOGI,");
            oStringBuilder2.AppendLine("PP_CAT,");
            oStringBuilder2.AppendLine("PP_GRP,");
            oStringBuilder2.AppendLine("PP_STD_Q,");
            oStringBuilder2.AppendLine("PP_STD_H,");
            oStringBuilder2.AppendLine("PP_IN_DIA,");
            oStringBuilder2.AppendLine("PP_OUT_DIA,");
            oStringBuilder2.AppendLine("PP_TP,");
            oStringBuilder2.AppendLine("PP_MAKE_YR,");
            oStringBuilder2.AppendLine("PP_USG_YR,");
            oStringBuilder2.AppendLine("PP_EST_YR,");
            oStringBuilder2.AppendLine("PP_PW,");
            oStringBuilder2.AppendLine("PP_RPM,");
            oStringBuilder2.AppendLine("PP_RATIO,");
            oStringBuilder2.AppendLine("PP_EFF,");
            oStringBuilder2.AppendLine("PP_GRKSU,");
            oStringBuilder2.AppendLine("PP_FRQ,");
            oStringBuilder2.AppendLine("PP_CHK_VL_TP,");
            oStringBuilder2.AppendLine("USERID,");
            oStringBuilder2.AppendLine("ENTDT)");
            oStringBuilder2.AppendLine("VALUES (");
            oStringBuilder2.AppendLine(" :FAC_SEQ,");
            oStringBuilder2.AppendLine(" :PP_HOGI,");
            oStringBuilder2.AppendLine(" :PP_CAT,");
            oStringBuilder2.AppendLine(" :PP_GRP,");
            oStringBuilder2.AppendLine(" :PP_STD_Q,");
            oStringBuilder2.AppendLine(" :PP_STD_H,");
            oStringBuilder2.AppendLine(" :PP_IN_DIA,");
            oStringBuilder2.AppendLine(" :PP_OUT_DIA,");
            oStringBuilder2.AppendLine(" :PP_TP,");
            oStringBuilder2.AppendLine(" :PP_MAKE_YR,");
            oStringBuilder2.AppendLine(" :PP_USG_YR,");
            oStringBuilder2.AppendLine(" :PP_EST_YR,");
            oStringBuilder2.AppendLine(" :PP_PW,");
            oStringBuilder2.AppendLine(" :PP_RPM,");
            oStringBuilder2.AppendLine(" :PP_RATIO,");
            oStringBuilder2.AppendLine(" :PP_EFF,");
            oStringBuilder2.AppendLine(" :PP_GRKSU,");
            oStringBuilder2.AppendLine(" :PP_FRQ,");
            oStringBuilder2.AppendLine(" :PP_CHK_VL_TP,");
            oStringBuilder2.AppendLine(" :USERID,");
            oStringBuilder2.AppendLine(" :ENTDT)");

            IDataParameter[] oParams2 = {
                new OracleParameter(":FAC_SEQ",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_HOGI",OracleDbType.Varchar2,3),
                new OracleParameter(":PP_CAT",OracleDbType.Varchar2,20),
                new OracleParameter(":PP_GRP",OracleDbType.Varchar2,20),
                new OracleParameter(":PP_STD_Q",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_STD_H",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_IN_DIA",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_OUT_DIA",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_TP",OracleDbType.Varchar2,20),
                new OracleParameter(":PP_MAKE_YR",OracleDbType.Varchar2,4),
                new OracleParameter(":PP_USG_YR",OracleDbType.Varchar2,4),
                new OracleParameter(":PP_EST_YR",OracleDbType.Varchar2,4),
                new OracleParameter(":PP_PW",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_RPM",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_RATIO",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_EFF",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_GRKSU",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_FRQ",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_CHK_VL_TP",OracleDbType.Varchar2,20),
                new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                new OracleParameter(":ENTDT",OracleDbType.Varchar2,16)
            };

            //oParams2[0].Value = Convert.ToString(pSeq); //"FAC_SEQ";       //시설물장치일련번호
            oParams2[1].Value = textBox_PP_HOGI.Text;  // "PP_HOGI";       //호기번호
            oParams2[2].Value = comboBox_PP_CAT.SelectedValue; // "PP_CAT";       //펌프구분
            oParams2[3].Value = comboBox_PP_GRP.SelectedValue; // "PP_GRP";       //펌프그룹
            oParams2[4].Value = textBox_PP_STD_Q.Text; // "PP_STD_Q";       //정격유량
            oParams2[5].Value = textBox_PP_STD_H.Text; // "PP_STD_H";       //정격양정
            oParams2[6].Value = textBox_PP_IN_DIA.Text; // "PP_IN_DIA";       //흡입구경
            oParams2[7].Value = textBox_PP_OUT_DIA.Text; // "PP_OUT_DIA";       //토출구경
            oParams2[8].Value = textBox_PP_TP.Text; // "PP_TP";       //형식
            oParams2[9].Value = textBox_PP_MAKE_YR.Text; // "PP_MAKE_YR";       //제작년도
            oParams2[10].Value = textBox_PP_USG_YR.Text; // "PP_USG_YR";       //내용년수
            oParams2[11].Value = textBox_PP_EST_YR.Text; // "PP_EST_YR";       //설치년도
            oParams2[12].Value = textBox_PP_PW.Text; // "PP_PW";       //동력
            oParams2[13].Value = textBox_PP_RPM.Text; // "PP_RPM";       //RPM
            oParams2[14].Value = textBox_PP_RATIO.Text; // "PP_RATIO";       //설계비속도
            oParams2[15].Value = textBox_PP_EFF.Text; // "PP_EFF";       //설계효율
            oParams2[16].Value = textBox_PP_GRKSU.Text; // "PP_GRKSU";       //극수
            oParams2[17].Value = textBox_PP_FRQ.Text; // "PP_FRQ";       //주파수
            oParams2[18].Value = comboBox_PP_CHK_VL_TP.SelectedValue; // "PP_CHK_VL_TP";       //체크밸브유형
            oParams2[19].Value = EMFrame.statics.AppStatic.USER_ID;    // "USERID";       //등록자
            oParams2[20].Value = FunctionManager.GetDBDate();   //"ENTDT";       //등록일자

            oDBManager.ExecuteScript(oStringBuilder2.ToString(), oParams2);   //펌프기본정보
            #endregion
        }

        #endregion

        #region ExecuteQueryUpdate
        //펌프시설 정보 수정
        private void ExecuteQueryUpdate_WE_FAC_INFO(ref OracleDBManager oDBManager, UltraGridRow oRow)
        {
            if (oRow == null) return;
            StringBuilder oStringBuilder = new StringBuilder();

            #region 펌프기본정보 - WE_PP_INFO
            StringBuilder oStringBuilder2 = new StringBuilder();
            oStringBuilder2.AppendLine("UPDATE WE_PP_INFO SET ");
            oStringBuilder2.AppendLine("PP_CAT = :PP_CAT,");
            oStringBuilder2.AppendLine("PP_GRP = :PP_GRP,");
            oStringBuilder2.AppendLine("PP_STD_Q = :PP_STD_Q,");
            oStringBuilder2.AppendLine("PP_STD_H = :PP_STD_H,");
            oStringBuilder2.AppendLine("PP_IN_DIA = :PP_IN_DIA,");
            oStringBuilder2.AppendLine("PP_OUT_DIA = :PP_OUT_DIA,");
            oStringBuilder2.AppendLine("PP_TP = :PP_TP,");
            oStringBuilder2.AppendLine("PP_MAKE_YR = :PP_MAKE_YR,");
            oStringBuilder2.AppendLine("PP_USG_YR = :PP_USG_YR,");
            oStringBuilder2.AppendLine("PP_EST_YR = :PP_EST_YR,");
            oStringBuilder2.AppendLine("PP_PW = :PP_PW,");
            oStringBuilder2.AppendLine("PP_RPM = :PP_RPM,");
            oStringBuilder2.AppendLine("PP_RATIO = :PP_RATIO,");
            oStringBuilder2.AppendLine("PP_EFF = :PP_EFF,");
            oStringBuilder2.AppendLine("PP_GRKSU = :PP_GRKSU,");
            oStringBuilder2.AppendLine("PP_FRQ = :PP_FRQ,");
            oStringBuilder2.AppendLine("PP_CHK_VL_TP = :PP_CHK_VL_TP,");

            oStringBuilder2.AppendLine("AN_IN_MDLID = :AN_IN_MDLID,");
            oStringBuilder2.AppendLine("AN_OUT_MDLID = :AN_OUT_MDLID,");

            oStringBuilder2.AppendLine("USERID = :USERID,");
            oStringBuilder2.AppendLine("CHGDT = :CHGDT");
            oStringBuilder2.AppendLine("WHERE FAC_SEQ = :FAC_SEQ");

            IDataParameter[] oParams2 = {
                new OracleParameter(":PP_CAT",OracleDbType.Varchar2,20),
                new OracleParameter(":PP_GRP",OracleDbType.Varchar2,20),
                new OracleParameter(":PP_STD_Q",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_STD_H",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_IN_DIA",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_OUT_DIA",OracleDbType.Varchar2,16),
                new OracleParameter(":PP_TP",OracleDbType.Varchar2,20),
                new OracleParameter(":PP_MAKE_YR",OracleDbType.Varchar2,4),
                new OracleParameter(":PP_USG_YR",OracleDbType.Varchar2,4),
                new OracleParameter(":PP_EST_YR",OracleDbType.Varchar2,4),
                new OracleParameter(":PP_PW",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_RPM",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_RATIO",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_EFF",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_GRKSU",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_FRQ",OracleDbType.Varchar2,10),
                new OracleParameter(":PP_CHK_VL_TP",OracleDbType.Varchar2,20),
                new OracleParameter(":AN_IN_MDLID",OracleDbType.Varchar2,50),
                new OracleParameter(":AN_OUT_MDLID",OracleDbType.Varchar2,50),
                new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                new OracleParameter(":CHGDT",OracleDbType.Varchar2,16),
                new OracleParameter(":FAC_SEQ",OracleDbType.Varchar2,16)
            };


            oParams2[0].Value = comboBox_PP_CAT.SelectedValue; // "PP_CAT";       //펌프구분   
            oParams2[1].Value = comboBox_PP_GRP.SelectedValue; // "PP_GRP";       //펌프그룹   
            oParams2[2].Value = textBox_PP_STD_Q.Text; // "PP_STD_Q";       //정격유량        
            oParams2[3].Value = textBox_PP_STD_H.Text; // "PP_STD_H";       //정격양정        
            oParams2[4].Value = textBox_PP_IN_DIA.Text; // "PP_IN_DIA";       //흡입구경      
            oParams2[5].Value = textBox_PP_OUT_DIA.Text; // "PP_OUT_DIA";       //토출구경    
            oParams2[6].Value = textBox_PP_TP.Text; // "PP_TP";       //형식                  
            oParams2[7].Value = textBox_PP_MAKE_YR.Text; // "PP_MAKE_YR";       //제작년도                    
            oParams2[8].Value = textBox_PP_USG_YR.Text; // "PP_USG_YR";       //내용년수                      
            oParams2[9].Value = textBox_PP_EST_YR.Text; // "PP_EST_YR";       //설치년도                      
            oParams2[10].Value = textBox_PP_PW.Text; // "PP_PW";       //동력                                  
            oParams2[11].Value = textBox_PP_RPM.Text; // "PP_RPM";       //RPM                                 
            oParams2[12].Value = textBox_PP_RATIO.Text; // "PP_RATIO";       //설계비속도                      
            oParams2[13].Value = textBox_PP_EFF.Text; // "PP_EFF";       //설계효율                            
            oParams2[14].Value = textBox_PP_GRKSU.Text; // "PP_GRKSU";       //극수                            
            oParams2[15].Value = textBox_PP_FRQ.Text; // "PP_FRQ";       //주파수                              
            oParams2[16].Value = comboBox_PP_CHK_VL_TP.SelectedValue; // "PP_CHK_VL_TP";       //체크밸브유형   
            oParams2[17].Value = textBox_AN_IN_MDLID.Text; // "AN_IN_MDLID";       //체크밸브유형   
            oParams2[18].Value = textBox_AN_OUT_MDLID.Text; // "AN_OUT_MDLID";       //체크밸브유형   
            oParams2[19].Value = EMFrame.statics.AppStatic.USER_ID;    // "USERID";       //등록자  
            oParams2[20].Value = FunctionManager.GetDBDate();   //"CHGDT";       //수정일자   
            oParams2[21].Value = Convert.ToString(oRow.Cells["FAC_SEQ"].Value);      //시설물장치일련번호

            oDBManager.ExecuteScript(oStringBuilder2.ToString(), oParams2);

            #endregion
        }
        #endregion
        /// <summary>
        /// 시설물 신규버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFacAdd_Click(object sender, EventArgs e)
        {
            this.m_FormeventType_WE_PP_INFO = FormManager.SubFormEventType.isAppend;
            FormManager.InitializePanel(panel_WE_PP_INFOS);

            setCommandStatus();
        }

        /// <summary>
        /// 시설물 삭제 버튼 클릭 : FAC_INFO, PP_INFO, TM_INFO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFacDel_Click(object sender, EventArgs e)
        {
            UltraGridRow row = null;
            StringBuilder oStringBuilder = new StringBuilder();
            //int nCount = 0;
            try
            {
                row = ultraGrid_WE_PP_INFO.ActiveRow;
                if (row == null)
                {
                    MessageManager.ShowInformationMessage("선택된 목록이 없습니다.");
                    return;
                }
                ////하위 데이터가 있는지 체크
                //oStringBuilder.AppendLine("SELECT * FROM WE_PP_INFO");
                //oStringBuilder.AppendLine("WHERE FAC_SEQ = '" + row.Cells["FAC_SEQ"].Value + "'");
                //nCount = FunctionManager.ExecuteScriptCount(oStringBuilder.ToString(), null);
                //if (nCount > 0)
                //{
                //    MessageManager.ShowInformationMessage("[" + row.Cells["FAC_SEQ"].Value + "] 의 펌프 데이터가 있습니다.");
                //    return;
                //}
                //---------------------------------------------------------------------------

                if (MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_DELETE) != DialogResult.Yes) return;

                //DELETE FAC-INFO, PP-INFO, TM-INFO
                ExecuteQueryDelete_WE_PP_INFO(row);

                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_COMPLETE);
            }
            catch (Exception ex)
            {
#if DEBUG
                MessageBox.Show(ex.Message);
#endif
                MessageManager.ShowInformationMessage(MessageManager.MSG_DELETE_CANCEL);
            }
        }

        /// <summary>
        /// 시설물 수정버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFacEdit_Click(object sender, EventArgs e)
        {
            this.m_FormeventType_WE_PP_INFO = FormManager.SubFormEventType.isUpdate;

            setCommandStatus();
        }

        /// <summary>
        /// 시설물 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFacSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
                if (eDialogResult != DialogResult.Yes) return;

                UltraGridRow row = ultraGrid_WE_PP_INFO.ActiveRow;
                UltraGridRow Biz_Pla_Row = ultraGrid_BIZ_PLA.ActiveRow;
                StringBuilder oStringBuilder = new StringBuilder();

                OracleDBManager oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

                try
                {
                    oDBManager.Open();
                    oDBManager.BeginTransaction();

                    if (this.m_FormeventType_WE_PP_INFO == FormManager.SubFormEventType.isAppend)
                    {
                        ExecuteQueryInsert_WE_PP_INFO(ref oDBManager, Biz_Pla_Row);
                    }
                    else
                    {
                        ExecuteQueryUpdate_WE_FAC_INFO(ref oDBManager, row);
                    }
                    oDBManager.CommitTransaction();
                    MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
                }
                catch (Exception oException)
                {
                    oDBManager.RollbackTransaction();
                    MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                    throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
                }
                finally
                {
                    oDBManager.Close();
                }

                if (Biz_Pla_Row == null)
                {
                    return;
                }
                //재쿼리
                Select_WE_PP_INFO(Convert.ToString(Biz_Pla_Row.Cells["BIZ_PLA_SEQ"].Value));

                this.m_FormeventType_WE_PP_INFO = FormManager.SubFormEventType.isViewer;
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void panel_WE_FAC_INFO_Paint(object sender, PaintEventArgs e)
        {

        }

    }


}
