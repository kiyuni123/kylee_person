﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
#endregion

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_BaseInfo
{
    public partial class frmBizPlace : Form
    {

        #region Private Field ------------------------------------------------------------------------------
        private GridManager m_gridManager1 = default(GridManager);  //펌프 그리드
        //폼의 추가, 수정, 보기 설정변수
        private FormManager.SubFormEventType m_FormeventType = FormManager.SubFormEventType.isViewer;
        private string m_strQuerySelect = string.Empty;
        private string m_strQueryInsert = string.Empty;
        private string m_strQueryUpdate = string.Empty;
        private UltraGridRow m_row;
        #endregion

        public frmBizPlace()
        {
            InitializeComponent();
            InitializeSetting();
            Load += new EventHandler(frmBizPlace_Load); //동진 수정_2012.6.08
        }
        private void frmBizPlace_Load(object sender, EventArgs e)
        {

            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================에너지관리 

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuBaseInfo"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }

            //===========================================================================
        }
        public frmBizPlace(FormManager.SubFormEventType pType)
        {
            m_FormeventType = pType;

            InitializeComponent();
            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;
            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FAC_SEQ";
            oUltraGridColumn.Header.Caption = "FAC_SEQ";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PP_HOGI";
            oUltraGridColumn.Header.Caption = "펌프호기";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_OPR_MDLID";
            oUltraGridColumn.Header.Caption = "해석모델항목ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_PTN_MDLID";
            oUltraGridColumn.Header.Caption = "해석모델패턴ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPR_TAGID";
            oUltraGridColumn.Header.Caption = "가동TAG-ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_IN_MDLID";
            oUltraGridColumn.Header.Caption = "유입해석모델ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            oUltraGridColumn = ultraGrid_Pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "AN_OUT_MDLID";
            oUltraGridColumn.Header.Caption = "유출해석모델ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;

            m_gridManager1 = new GridManager(ultraGrid_Pump);
            m_gridManager1.SetGridStyle_Update();


            #endregion

        }
        #endregion

        public void Open()
        {
            switch (m_FormeventType)
            {
                case FormManager.SubFormEventType.isAppend:
                    ///펌프 그리드 6개로 초기화
                    DataTable oDataTable = m_gridManager1.GetDataSource();
                    DataRow oRow = null;
                    for (int i = 0; i < 6; i++)
                    {
                        oRow = oDataTable.NewRow();
                        oRow["PP_HOGI"] = "#" + Convert.ToString(i + 1);

                        oDataTable.Rows.Add(oRow);
                    }
                    m_gridManager1.SetDataSource(oDataTable);
                    break;
                case FormManager.SubFormEventType.isUpdate:
                case FormManager.SubFormEventType.isViewer:
                    if (m_row == null) return;

                    FormManager.SetValueToControls(panel_WE_BIZ_PLA, m_row);

                    SelectProcess(m_row);
                    break;
                default:
                    break;
            }

            this.ShowDialog();
        }

        public UltraGridRow row
        {
            get { return m_row; }
            set { m_row = value; }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (m_FormeventType == FormManager.SubFormEventType.isViewer) return;
                if (!(FormManager.isValidateControl(txt_BIZ_PLA_NM, "사업장명"))) return;

                DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
                if (eDialogResult != DialogResult.Yes) return;

                bool bResult = false;
                try
                {
                    switch (m_FormeventType)
                    {
                        case FormManager.SubFormEventType.isAppend:
                            bResult = InsertProcess();
                            break;
                        case FormManager.SubFormEventType.isUpdate:
                            bResult = UpdateProcess();
                            break;
                        case FormManager.SubFormEventType.isViewer:
                            break;
                    }
                }
                catch (Exception ex)
                {
#if DEBUG
                    MessageBox.Show(ex.Message);
#endif
                    MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                    return;
                }
                if (bResult) this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectProcess(UltraGridRow oRow)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT FAC_SEQ, PP_HOGI, AN_OPR_MDLID, AN_PTN_MDLID, OPR_TAGID, AN_IN_MDLID, AN_OUT_MDLID");
            oStringBuilder.AppendLine("FROM");
            oStringBuilder.AppendLine("(SELECT B.FAC_SEQ,B.PP_HOGI AS PP_HOGI, A.AN_OPR_MDLID_1 AS AN_OPR_MDLID, A.AN_PTN_MDLID_1 AS AN_PTN_MDLID, A.OPR_TAGID_1 AS OPR_TAGID, ");
            oStringBuilder.AppendLine("        B.AN_IN_MDLID AS AN_IN_MDLID, B.AN_OUT_MDLID AS AN_OUT_MDLID, 1 AS RNK");
            oStringBuilder.AppendLine("FROM WE_BIZ_PLA A, WE_PP_INFO B");
            oStringBuilder.AppendLine("WHERE A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND PP_HOGI = '#1'");
            oStringBuilder.AppendLine("AND B.BIZ_PLA_SEQ = '" + oRow.Cells["BIZ_PLA_SEQ"].Value + "'");
            oStringBuilder.AppendLine("UNION");
            oStringBuilder.AppendLine("SELECT B.FAC_SEQ,B.PP_HOGI AS PP_HOGI, AN_OPR_MDLID_2 AS AN_OPR_MDLID, AN_PTN_MDLID_2 AS AN_PTN_MDLID, OPR_TAGID_2 AS OPR_TAGID,");
            oStringBuilder.AppendLine("        B.AN_IN_MDLID AS AN_IN_MDLID, B.AN_OUT_MDLID AS AN_OUT_MDLID, 2 AS RNK");
            oStringBuilder.AppendLine("FROM WE_BIZ_PLA A, WE_PP_INFO B");
            oStringBuilder.AppendLine("WHERE A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND PP_HOGI = '#2'");
            oStringBuilder.AppendLine("AND B.BIZ_PLA_SEQ = '" + oRow.Cells["BIZ_PLA_SEQ"].Value + "'");
            oStringBuilder.AppendLine("UNION");
            oStringBuilder.AppendLine("SELECT B.FAC_SEQ,B.PP_HOGI AS PP_HOGI, AN_OPR_MDLID_3 AS AN_OPR_MDLID, AN_PTN_MDLID_3 AS AN_PTN_MDLID, OPR_TAGID_3 AS OPR_TAGID, ");
            oStringBuilder.AppendLine("        B.AN_IN_MDLID AS AN_IN_MDLID, B.AN_OUT_MDLID AS AN_OUT_MDLID, 3 AS RNK");
            oStringBuilder.AppendLine("FROM WE_BIZ_PLA A, WE_PP_INFO B");
            oStringBuilder.AppendLine("WHERE A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND PP_HOGI = '#3'");
            oStringBuilder.AppendLine("AND B.BIZ_PLA_SEQ = '" + oRow.Cells["BIZ_PLA_SEQ"].Value + "'");
            oStringBuilder.AppendLine("UNION");
            oStringBuilder.AppendLine("SELECT B.FAC_SEQ,B.PP_HOGI AS PP_HOGI, AN_OPR_MDLID_4 AS AN_OPR_MDLID, AN_PTN_MDLID_4 AS AN_PTN_MDLID, OPR_TAGID_4 AS OPR_TAGID, ");
            oStringBuilder.AppendLine("        B.AN_IN_MDLID AS AN_IN_MDLID, B.AN_OUT_MDLID AS AN_OUT_MDLID, 4 AS RNK");
            oStringBuilder.AppendLine("FROM WE_BIZ_PLA A, WE_PP_INFO B");
            oStringBuilder.AppendLine("WHERE A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND PP_HOGI = '#4'");
            oStringBuilder.AppendLine("AND B.BIZ_PLA_SEQ = '" + oRow.Cells["BIZ_PLA_SEQ"].Value + "'");
            oStringBuilder.AppendLine("UNION");
            oStringBuilder.AppendLine("SELECT B.FAC_SEQ,B.PP_HOGI AS PP_HOGI, AN_OPR_MDLID_5 AS AN_OPR_MDLID, AN_PTN_MDLID_5 AS AN_PTN_MDLID, OPR_TAGID_5 AS OPR_TAGID, ");
            oStringBuilder.AppendLine("        B.AN_IN_MDLID AS AN_IN_MDLID, B.AN_OUT_MDLID AS AN_OUT_MDLID, 5 AS RNK");
            oStringBuilder.AppendLine("FROM WE_BIZ_PLA A, WE_PP_INFO B");
            oStringBuilder.AppendLine("WHERE A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND PP_HOGI = '#5'");
            oStringBuilder.AppendLine("AND B.BIZ_PLA_SEQ = '" + oRow.Cells["BIZ_PLA_SEQ"].Value + "'");
            oStringBuilder.AppendLine("UNION");
            oStringBuilder.AppendLine("SELECT B.FAC_SEQ, B.PP_HOGI AS PP_HOGI, AN_OPR_MDLID_6 AS AN_OPR_MDLID, AN_PTN_MDLID_6 AS AN_PTN_MDLID, OPR_TAGID_6 AS OPR_TAGID, ");
            oStringBuilder.AppendLine("        B.AN_IN_MDLID AS AN_IN_MDLID, B.AN_OUT_MDLID AS AN_OUT_MDLID, 6 AS RNK");
            oStringBuilder.AppendLine("FROM WE_BIZ_PLA A, WE_PP_INFO B");
            oStringBuilder.AppendLine("WHERE A.BIZ_PLA_SEQ = B.BIZ_PLA_SEQ AND PP_HOGI = '#6'");
            oStringBuilder.AppendLine("AND B.BIZ_PLA_SEQ = '" + oRow.Cells["BIZ_PLA_SEQ"].Value + "')");
            oStringBuilder.AppendLine("ORDER BY RNK ASC");

            DataSet pDS = WaterNetCore.FunctionManager.GetDataSet(oStringBuilder.ToString(), "table0");
            ultraGrid_Pump.DataSource = pDS.Tables["table0"].DefaultView;
        }

        /// <summary>
        /// 에너지 사업장 및 펌프정보 입력
        /// </summary>
        /// <returns></returns>
        private bool InsertProcess()
        {
            bool bResult = false;
            StringBuilder oStringBuilder = new StringBuilder();
            int intPLA_SEQ = WaterNetCore.FunctionManager.GetMaxID("WE_BIZ_PLA", "BIZ_PLA_SEQ");
            int intFAC_SEQ = WaterNetCore.FunctionManager.GetMaxID("WE_PP_INFO", "FAC_SEQ");

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();
                #region 사업장(WE_BIZ_PLA) -Insert
                oStringBuilder.AppendLine("INSERT INTO WE_BIZ_PLA (");
                oStringBuilder.AppendLine("BIZ_PLA_SEQ,");
                oStringBuilder.AppendLine("BIZ_PLA_NM,");
                oStringBuilder.AppendLine("BIZ_PLA_ETC,");
                oStringBuilder.AppendLine("RL_AN_MDL_NO,");
                oStringBuilder.AppendLine("AN_LVL_MDLID,");
                oStringBuilder.AppendLine("LVL_TAGID,");
                oStringBuilder.AppendLine("AN_OUT_MDLID,");
                oStringBuilder.AppendLine("OUT_TAGID,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_1,");
                oStringBuilder.AppendLine("OPR_TAGID_1,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_1,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_2,");
                oStringBuilder.AppendLine("OPR_TAGID_2,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_2,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_3,");
                oStringBuilder.AppendLine("OPR_TAGID_3,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_3,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_4,");
                oStringBuilder.AppendLine("OPR_TAGID_4,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_4,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_5,");
                oStringBuilder.AppendLine("OPR_TAGID_5,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_5,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_6,");
                oStringBuilder.AppendLine("OPR_TAGID_6,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_6,");
                oStringBuilder.AppendLine("BIZ_ENGWON,");
                oStringBuilder.AppendLine("SG_CD,");
                oStringBuilder.AppendLine("USERID,");
                oStringBuilder.AppendLine("ENTDT)");
                oStringBuilder.AppendLine("VALUES (");
                oStringBuilder.AppendLine(" :BIZ_PLA_SEQ,");
                oStringBuilder.AppendLine(" :BIZ_PLA_NM,");
                oStringBuilder.AppendLine(" :BIZ_PLA_ETC,");
                oStringBuilder.AppendLine(" :RL_AN_MDL_NO,");
                oStringBuilder.AppendLine(" :AN_LVL_MDLID,");
                oStringBuilder.AppendLine(" :LVL_TAGID,");
                oStringBuilder.AppendLine(" :AN_OUT_MDLID,");
                oStringBuilder.AppendLine(" :OUT_TAGID,");
                oStringBuilder.AppendLine(" :AN_OPR_MDLID_1,");
                oStringBuilder.AppendLine(" :OPR_TAGID_1,");
                oStringBuilder.AppendLine(" :AN_PTN_MDLID_1,");
                oStringBuilder.AppendLine(" :AN_OPR_MDLID_2,");
                oStringBuilder.AppendLine(" :OPR_TAGID_2,");
                oStringBuilder.AppendLine(" :AN_PTN_MDLID_2,");
                oStringBuilder.AppendLine(" :AN_OPR_MDLID_3,");
                oStringBuilder.AppendLine(" :OPR_TAGID_3,");
                oStringBuilder.AppendLine(" :AN_PTN_MDLID_3,");
                oStringBuilder.AppendLine(" :AN_OPR_MDLID_4,");
                oStringBuilder.AppendLine(" :OPR_TAGID_4,");
                oStringBuilder.AppendLine(" :AN_PTN_MDLID_4,");
                oStringBuilder.AppendLine(" :AN_OPR_MDLID_5,");
                oStringBuilder.AppendLine(" :OPR_TAGID_5,");
                oStringBuilder.AppendLine(" :AN_PTN_MDLID_5,");
                oStringBuilder.AppendLine(" :AN_OPR_MDLID_6,");
                oStringBuilder.AppendLine(" :OPR_TAGID_6,");
                oStringBuilder.AppendLine(" :AN_PTN_MDLID_6,");
                oStringBuilder.AppendLine(" :BIZ_ENGWON,");
                oStringBuilder.AppendLine(" :SG_CD,");
                oStringBuilder.AppendLine(" :USERID,");
                oStringBuilder.AppendLine(" :ENTDT)");
                IDataParameter[] oParams = {
                    new OracleParameter(":BIZ_PLA_SEQ",OracleDbType.Varchar2,16),
                    new OracleParameter(":BIZ_PLA_NM",OracleDbType.Varchar2,50),
                    new OracleParameter(":BIZ_PLA_ETC",OracleDbType.Varchar2,400),
                    new OracleParameter(":RL_AN_MDL_NO",OracleDbType.Varchar2,48),
                    new OracleParameter(":AN_LVL_MDLID",OracleDbType.Varchar2,50),
                    new OracleParameter(":LVL_TAGID",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_OUT_MDLID",OracleDbType.Varchar2,50),
                    new OracleParameter(":OUT_TAGID",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_OPR_MDLID_1",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_1",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_1",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_2",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_2",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_2",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_3",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_3",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_3",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_4",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_4",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_4",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_5",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_5",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_5",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_6",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_6",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_6",OracleDbType.Varchar2,50),
                    new OracleParameter(":BIZ_ENGWON",OracleDbType.Varchar2,10),
                    new OracleParameter(":SG_CD",OracleDbType.Varchar2,5),
                    new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                    new OracleParameter(":ENTDT",OracleDbType.Varchar2,16)
                };

                oParams[0].Value = Convert.ToString(intPLA_SEQ);       //사업장일련번호
                oParams[1].Value = txt_BIZ_PLA_NM.Text;       //사업장명
                oParams[2].Value = txt_BIZ_PLA_ETC.Text;       //사업장개요
                oParams[3].Value = txt_RL_AN_MDL_NO.Text;       //실시간해석모델번호
                oParams[4].Value = txt_AN_LVL_MDLID.Text;       //수위해석모델 항목ID
                oParams[5].Value = txt_LVL_TAGID.Text;       //수위 TAG_ID
                oParams[6].Value = txt_AN_OUT_MDLID.Text;       //유출유량해석모델 항목ID
                oParams[7].Value = txt_OUT_TAGID.Text;       //유출유량계 TAG_ID

                oParams[8].Value = m_gridManager1.grid.Rows[0].Cells["AN_OPR_MDLID"].Value;       //펌프1 가동해석모델 항목ID
                oParams[9].Value = m_gridManager1.grid.Rows[0].Cells["OPR_TAGID"].Value;       //펌프1 가동 TAG_ID
                oParams[10].Value = m_gridManager1.grid.Rows[0].Cells["AN_PTN_MDLID"].Value;       //펌프1 해석패턴 항목ID
                oParams[11].Value = m_gridManager1.grid.Rows[1].Cells["AN_OPR_MDLID"].Value;       //펌프2 가동해석모델 항목ID
                oParams[12].Value = m_gridManager1.grid.Rows[1].Cells["OPR_TAGID"].Value;       //펌프2 가동 TAG_ID
                oParams[13].Value = m_gridManager1.grid.Rows[1].Cells["AN_PTN_MDLID"].Value;       //펌프2 해석패턴 항목ID
                oParams[14].Value = m_gridManager1.grid.Rows[2].Cells["AN_OPR_MDLID"].Value;        //펌프3 가동해석모델 항목ID
                oParams[15].Value = m_gridManager1.grid.Rows[2].Cells["OPR_TAGID"].Value;        //펌프3 가동 TAG_ID
                oParams[16].Value = m_gridManager1.grid.Rows[2].Cells["AN_PTN_MDLID"].Value;        //펌프3 해석패턴 항목ID
                oParams[17].Value = m_gridManager1.grid.Rows[3].Cells["AN_OPR_MDLID"].Value;       //펌프4 가동해석모델 항목ID
                oParams[18].Value = m_gridManager1.grid.Rows[3].Cells["OPR_TAGID"].Value;       //펌프4 가동 TAG_ID
                oParams[19].Value = m_gridManager1.grid.Rows[3].Cells["AN_PTN_MDLID"].Value;       //펌프4 해석패턴 항목ID
                oParams[20].Value = m_gridManager1.grid.Rows[4].Cells["AN_OPR_MDLID"].Value;       //펌프5 가동해석모델 항목ID
                oParams[21].Value = m_gridManager1.grid.Rows[4].Cells["OPR_TAGID"].Value;       //펌프5 가동 TAG_ID
                oParams[22].Value = m_gridManager1.grid.Rows[4].Cells["AN_PTN_MDLID"].Value;       //펌프5 해석패턴 항목ID
                oParams[23].Value = m_gridManager1.grid.Rows[5].Cells["AN_OPR_MDLID"].Value;       //펌프6 가동해석모델 항목ID
                oParams[24].Value = m_gridManager1.grid.Rows[5].Cells["OPR_TAGID"].Value;       //펌프6 가동 TAG_ID
                oParams[25].Value = m_gridManager1.grid.Rows[5].Cells["AN_PTN_MDLID"].Value;       //펌프6 해석패턴 항목ID
                oParams[26].Value = textBox_BIZ_ENGWON.Text;       //전력단위요금
                oParams[27].Value = EMFrame.statics.AppStatic.USER_SGCCD;    // "SG_CD";       //지자체코드
                oParams[28].Value = EMFrame.statics.AppStatic.USER_ID;   // "USERID";       //등록자
                oParams[29].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                oDBManager.BeginTransaction();

                oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                #endregion

                #region 펌프정보 - Insert
                oStringBuilder.Remove(0, oStringBuilder.Length);

                oStringBuilder.AppendLine("INSERT INTO WE_PP_INFO (");
                oStringBuilder.AppendLine("BIZ_PLA_SEQ,");
                oStringBuilder.AppendLine("FAC_SEQ,");
                oStringBuilder.AppendLine("PP_HOGI,");
                oStringBuilder.AppendLine("AN_IN_MDLID,");
                oStringBuilder.AppendLine("AN_OUT_MDLID,");
                oStringBuilder.AppendLine("USERID,");
                oStringBuilder.AppendLine("ENTDT)");
                oStringBuilder.AppendLine("VALUES (");
                oStringBuilder.AppendLine(" :BIZ_PLA_SEQ,");
                oStringBuilder.AppendLine(" :FAC_SEQ,");
                oStringBuilder.AppendLine(" :PP_HOGI,");
                oStringBuilder.AppendLine(" :AN_IN_MDLID,");
                oStringBuilder.AppendLine(" :AN_OUT_MDLID,");
                oStringBuilder.AppendLine(" :USERID,");
                oStringBuilder.AppendLine(" :ENTDT)");

                foreach (UltraGridRow item in m_gridManager1.grid.Rows)
                {
                    IDataParameter[] oParams2 = {
                        new OracleParameter(":BIZ_PLA_SEQ",OracleDbType.Varchar2,16),
                        new OracleParameter(":FAC_SEQ",OracleDbType.Varchar2,16),
                        new OracleParameter(":PP_HOGI",OracleDbType.Varchar2,3),
                        new OracleParameter(":AN_IN_MDLID",OracleDbType.Varchar2,50),
                        new OracleParameter(":AN_OUT_MDLID",OracleDbType.Varchar2,50),
                        new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                        new OracleParameter(":ENTDT",OracleDbType.Varchar2,16)
                    };

                    oParams2[0].Value = Convert.ToString(intPLA_SEQ);      //사업장일련번호
                    oParams2[1].Value = Convert.ToString(intFAC_SEQ);      //시설물장치일련번호
                    oParams2[2].Value = item.Cells["PP_HOGI"].Value;       //호기번호
                    oParams2[3].Value = item.Cells["AN_IN_MDLID"].Value;       //유입유량해석항목 ID
                    oParams2[4].Value =  item.Cells["AN_OUT_MDLID"].Value;       //유출유량해석항목 ID
                    oParams2[5].Value = EMFrame.statics.AppStatic.USER_ID;    // "USERID";       //등록자
                    oParams2[6].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자

                    oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams2);

                    intFAC_SEQ++;  //펌프관리번호 증가

                }
                #endregion

                

                oDBManager.CommitTransaction();

                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
                bResult = true;
            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
            return bResult;
        }

        private bool UpdateProcess()
        {
            bool bResult = false;

            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();

                #region 사업장 (WE_BIZ_PLA) - Update
                oStringBuilder.AppendLine("UPDATE WE_BIZ_PLA SET ");
                oStringBuilder.AppendLine("BIZ_PLA_NM = :BIZ_PLA_NM,");
                oStringBuilder.AppendLine("BIZ_PLA_ETC = :BIZ_PLA_ETC,");
                oStringBuilder.AppendLine("RL_AN_MDL_NO = :RL_AN_MDL_NO,");
                oStringBuilder.AppendLine("AN_LVL_MDLID = :AN_LVL_MDLID,");
                oStringBuilder.AppendLine("LVL_TAGID = :LVL_TAGID,");
                oStringBuilder.AppendLine("AN_OUT_MDLID = :AN_OUT_MDLID,");
                oStringBuilder.AppendLine("OUT_TAGID = :OUT_TAGID,");

                oStringBuilder.AppendLine("AN_OPR_MDLID_1 =  :AN_OPR_MDLID_1,");
                oStringBuilder.AppendLine("OPR_TAGID_1 = :OPR_TAGID_1,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_1 =  :AN_PTN_MDLID_1,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_2 = :AN_OPR_MDLID_2,");
                oStringBuilder.AppendLine("OPR_TAGID_2 = :OPR_TAGID_2,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_2 =  :AN_PTN_MDLID_2,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_3 = :AN_OPR_MDLID_3,");
                oStringBuilder.AppendLine("OPR_TAGID_3 =  :OPR_TAGID_3,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_3 = :AN_PTN_MDLID_3,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_4 = :AN_OPR_MDLID_4,");
                oStringBuilder.AppendLine("OPR_TAGID_4 = :OPR_TAGID_4,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_4 = :AN_PTN_MDLID_4,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_5 = :AN_OPR_MDLID_5,");
                oStringBuilder.AppendLine("OPR_TAGID_5 = :OPR_TAGID_5,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_5 = :AN_PTN_MDLID_5,");
                oStringBuilder.AppendLine("AN_OPR_MDLID_6 = :AN_OPR_MDLID_6,");
                oStringBuilder.AppendLine("OPR_TAGID_6 =  :OPR_TAGID_6,");
                oStringBuilder.AppendLine("AN_PTN_MDLID_6 = :AN_PTN_MDLID_6,");
                oStringBuilder.AppendLine("BIZ_ENGWON = :BIZ_ENGWON,");

                oStringBuilder.AppendLine("SG_CD = :SG_CD,");
                oStringBuilder.AppendLine("USERID = :USERID,");
                oStringBuilder.AppendLine("CHGDT = :CHGDT");
                oStringBuilder.AppendLine("WHERE BIZ_PLA_SEQ = :BIZ_PLA_SEQ");

                IDataParameter[] oParams = {
                    new OracleParameter(":BIZ_PLA_NM",OracleDbType.Varchar2,50),
                    new OracleParameter(":BIZ_PLA_ETC",OracleDbType.Varchar2,100),
                    new OracleParameter(":RL_AN_MDL_NO",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_LVL_MDLID",OracleDbType.Varchar2,20),
                    new OracleParameter(":LVL_TAGID",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_OUT_MDLID",OracleDbType.Varchar2,20),
                    new OracleParameter(":OUT_TAGID",OracleDbType.Varchar2,100),

                    new OracleParameter(":AN_OPR_MDLID_1",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_1",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_1",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_2",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_2",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_2",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_3",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_3",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_3",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_4",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_4",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_4",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_5",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_5",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_5",OracleDbType.Varchar2,50),
                    new OracleParameter(":AN_OPR_MDLID_6",OracleDbType.Varchar2,50),
                    new OracleParameter(":OPR_TAGID_6",OracleDbType.Varchar2,100),
                    new OracleParameter(":AN_PTN_MDLID_6",OracleDbType.Varchar2,50),
                    new OracleParameter(":BIZ_ENGWON",OracleDbType.Varchar2,10),

                    new OracleParameter(":SG_CD",OracleDbType.Varchar2,5),
                    new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                    new OracleParameter(":CHGDT",OracleDbType.Varchar2,16),
                    new OracleParameter(":BIZ_PLA_SEQ",OracleDbType.Varchar2,16)
                };

                oParams[0].Value = txt_BIZ_PLA_NM.Text;       // "BIZ_PLA_NM";       //사업장명
                oParams[1].Value = txt_BIZ_PLA_ETC.Text;        //"BIZ_PLA_ETC";       //사업장개요
                oParams[2].Value = txt_RL_AN_MDL_NO.Text;    //"RL_AN_MDL_NO";   //실시간해석모델번호

                oParams[3].Value = txt_AN_LVL_MDLID.Text;       // "BIZ_PLA_NM";       //사업장명
                oParams[4].Value = txt_LVL_TAGID.Text;        //"BIZ_PLA_ETC";       //사업장개요
                oParams[5].Value = txt_AN_OUT_MDLID.Text;        //"BIZ_PLA_ETC";       //사업장개요
                oParams[6].Value = txt_OUT_TAGID.Text;    //"RL_AN_MDL_NO";   //실시간해석모델번호


                oParams[7].Value = m_gridManager1.grid.Rows[0].Cells["AN_OPR_MDLID"].Value;       //펌프1 가동해석모델 항목ID
                oParams[8].Value = m_gridManager1.grid.Rows[0].Cells["OPR_TAGID"].Value;       //펌프1 가동 TAG_ID
                oParams[9].Value = m_gridManager1.grid.Rows[0].Cells["AN_PTN_MDLID"].Value;       //펌프1 해석패턴 항목ID
                oParams[10].Value = m_gridManager1.grid.Rows[1].Cells["AN_OPR_MDLID"].Value;       //펌프2 가동해석모델 항목ID
                oParams[11].Value = m_gridManager1.grid.Rows[1].Cells["OPR_TAGID"].Value;       //펌프2 가동 TAG_ID
                oParams[12].Value = m_gridManager1.grid.Rows[1].Cells["AN_PTN_MDLID"].Value;       //펌프2 해석패턴 항목ID
                oParams[13].Value = m_gridManager1.grid.Rows[2].Cells["AN_OPR_MDLID"].Value;        //펌프3 가동해석모델 항목ID
                oParams[14].Value = m_gridManager1.grid.Rows[2].Cells["OPR_TAGID"].Value;        //펌프3 가동 TAG_ID
                oParams[15].Value = m_gridManager1.grid.Rows[2].Cells["AN_PTN_MDLID"].Value;        //펌프3 해석패턴 항목ID
                oParams[16].Value = m_gridManager1.grid.Rows[3].Cells["AN_OPR_MDLID"].Value;       //펌프4 가동해석모델 항목ID
                oParams[17].Value = m_gridManager1.grid.Rows[3].Cells["OPR_TAGID"].Value;       //펌프4 가동 TAG_ID
                oParams[18].Value = m_gridManager1.grid.Rows[3].Cells["AN_PTN_MDLID"].Value;       //펌프4 해석패턴 항목ID
                oParams[19].Value = m_gridManager1.grid.Rows[4].Cells["AN_OPR_MDLID"].Value;       //펌프5 가동해석모델 항목ID
                oParams[20].Value = m_gridManager1.grid.Rows[4].Cells["OPR_TAGID"].Value;       //펌프5 가동 TAG_ID
                oParams[21].Value = m_gridManager1.grid.Rows[4].Cells["AN_PTN_MDLID"].Value;       //펌프5 해석패턴 항목ID
                oParams[22].Value = m_gridManager1.grid.Rows[5].Cells["AN_OPR_MDLID"].Value;       //펌프6 가동해석모델 항목ID
                oParams[23].Value = m_gridManager1.grid.Rows[5].Cells["OPR_TAGID"].Value;       //펌프6 가동 TAG_ID
                oParams[24].Value = m_gridManager1.grid.Rows[5].Cells["AN_PTN_MDLID"].Value;       //펌프6 해석패턴 항목ID
                oParams[25].Value = textBox_BIZ_ENGWON.Text;     // 단위전력요금

                oParams[26].Value = EMFrame.statics.AppStatic.USER_SGCCD;     // "SG_CD";       //지자체코드
                oParams[27].Value = EMFrame.statics.AppStatic.USER_ID;    // "USERID";       //등록자
                oParams[28].Value = FunctionManager.GetDBDate();   //"CHGDT";       //수정일자
                oParams[29].Value = Convert.ToString(m_row.Cells["BIZ_PLA_SEQ"].Value);   // "BIZ_PLA_SEQ";       //사업장일련번호
                
                oDBManager.BeginTransaction();

                oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                #endregion

                #region 펌프정보(WE_PP_INFO) - Update

                oStringBuilder.Remove(0, oStringBuilder.Length);

                oStringBuilder.AppendLine("UPDATE WE_PP_INFO SET");
                oStringBuilder.AppendLine("AN_IN_MDLID = :AN_IN_MDLID,");
                oStringBuilder.AppendLine("AN_OUT_MDLID = :AN_OUT_MDLID,");
                oStringBuilder.AppendLine("USERID = :USERID,");
                oStringBuilder.AppendLine("CHGDT = :CHGDT");
                oStringBuilder.AppendLine("WHERE BIZ_PLA_SEQ = :BIZ_PLA_SEQ AND FAC_SEQ = :FAC_SEQ");

                foreach (UltraGridRow item in m_gridManager1.grid.Rows)
                {
                    IDataParameter[] oParams2 = {
                        new OracleParameter(":AN_IN_MDLID",OracleDbType.Varchar2,50),
                        new OracleParameter(":AN_OUT_MDLID",OracleDbType.Varchar2,50),
                        new OracleParameter(":USERID",OracleDbType.Varchar2,50),
                        new OracleParameter(":CHGDT",OracleDbType.Varchar2,16),
                        new OracleParameter(":BIZ_PLA_SEQ",OracleDbType.Varchar2,16),
                        new OracleParameter(":FAC_SEQ",OracleDbType.Varchar2,16)
                    };

                    oParams2[0].Value = item.Cells["AN_IN_MDLID"].Value;       //유입유량해석항목 ID
                    oParams2[1].Value = item.Cells["AN_OUT_MDLID"].Value;       //유출유량해석항목 ID
                    oParams2[2].Value = EMFrame.statics.AppStatic.USER_ID;    // "USERID";       //등록자
                    oParams2[3].Value = FunctionManager.GetDBDate();   //"CHGDT";       //등록일자
                    oParams2[4].Value = Convert.ToString(m_row.Cells["BIZ_PLA_SEQ"].Value);      //사업장일련번호
                    oParams2[5].Value = item.Cells["FAC_SEQ"].Value;      //시설물장치일련번호

                    oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams2);
                }

                #endregion

                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
                bResult = true;
            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }

            return bResult;

        }
    }
}
