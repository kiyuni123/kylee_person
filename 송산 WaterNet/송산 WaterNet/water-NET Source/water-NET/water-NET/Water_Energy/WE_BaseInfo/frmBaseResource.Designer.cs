﻿namespace WaterNet.WE_BaseInfo
{
    partial class frmBaseResource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnFacEdit = new System.Windows.Forms.Button();
            this.btnFacSave = new System.Windows.Forms.Button();
            this.btnFacAdd = new System.Windows.Forms.Button();
            this.btnBizDel = new System.Windows.Forms.Button();
            this.btnFacDel = new System.Windows.Forms.Button();
            this.btnBizAdd = new System.Windows.Forms.Button();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.ultraGrid_BIZ_PLA = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel_WE_PP_INFOS = new System.Windows.Forms.Panel();
            this.panel_WE_PP_INFO = new System.Windows.Forms.Panel();
            this.textBox_AN_OUT_MDLID = new System.Windows.Forms.TextBox();
            this.textBox_PP_STD_Q = new System.Windows.Forms.TextBox();
            this.label_AN_OUT_MDLID = new System.Windows.Forms.Label();
            this.textBox_AN_IN_MDLID = new System.Windows.Forms.TextBox();
            this.comboBox_PP_CHK_VL_TP = new System.Windows.Forms.ComboBox();
            this.label_AN_IN_MDLID = new System.Windows.Forms.Label();
            this.label_PP_CHK_VL_TP = new System.Windows.Forms.Label();
            this.textBox_PP_FRQ = new System.Windows.Forms.TextBox();
            this.label_PP_FRQ = new System.Windows.Forms.Label();
            this.textBox_PP_GRKSU = new System.Windows.Forms.TextBox();
            this.label_PP_GRKSU = new System.Windows.Forms.Label();
            this.textBox_PP_EFF = new System.Windows.Forms.TextBox();
            this.label_PP_EFF = new System.Windows.Forms.Label();
            this.textBox_PP_RATIO = new System.Windows.Forms.TextBox();
            this.label_PP_RATIO = new System.Windows.Forms.Label();
            this.textBox_PP_RPM = new System.Windows.Forms.TextBox();
            this.label_PP_RPM = new System.Windows.Forms.Label();
            this.textBox_PP_PW = new System.Windows.Forms.TextBox();
            this.label_PP_PW = new System.Windows.Forms.Label();
            this.textBox_PP_EST_YR = new System.Windows.Forms.TextBox();
            this.label_PP_EST_YR = new System.Windows.Forms.Label();
            this.textBox_PP_USG_YR = new System.Windows.Forms.TextBox();
            this.textBox_PP_MAKE_YR = new System.Windows.Forms.TextBox();
            this.textBox_PP_TP = new System.Windows.Forms.TextBox();
            this.textBox_PP_OUT_DIA = new System.Windows.Forms.TextBox();
            this.textBox_PP_IN_DIA = new System.Windows.Forms.TextBox();
            this.textBox_PP_STD_H = new System.Windows.Forms.TextBox();
            this.comboBox_PP_GRP = new System.Windows.Forms.ComboBox();
            this.comboBox_PP_CAT = new System.Windows.Forms.ComboBox();
            this.textBox_PP_HOGI = new System.Windows.Forms.TextBox();
            this.label_PP_TP = new System.Windows.Forms.Label();
            this.label_PP_USG_YR = new System.Windows.Forms.Label();
            this.label_PP_MAKE_YR = new System.Windows.Forms.Label();
            this.label_PP_OUT_DIA = new System.Windows.Forms.Label();
            this.label_PP_IN_DIA = new System.Windows.Forms.Label();
            this.label_PP_STD_H = new System.Windows.Forms.Label();
            this.label_PP_STD_Q = new System.Windows.Forms.Label();
            this.label_PP_GRP = new System.Windows.Forms.Label();
            this.label_PP_CAT = new System.Windows.Forms.Label();
            this.label_PP_HOGI = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panelPPInfogrid = new System.Windows.Forms.Panel();
            this.ultraGrid_WE_PP_INFO = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelCommand.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_BIZ_PLA)).BeginInit();
            this.panel_WE_PP_INFOS.SuspendLayout();
            this.panel_WE_PP_INFO.SuspendLayout();
            this.panelPPInfogrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_INFO)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnFacEdit);
            this.panelCommand.Controls.Add(this.btnFacSave);
            this.panelCommand.Controls.Add(this.btnFacAdd);
            this.panelCommand.Controls.Add(this.btnBizDel);
            this.panelCommand.Controls.Add(this.btnFacDel);
            this.panelCommand.Controls.Add(this.btnBizAdd);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(1242, 44);
            this.panelCommand.TabIndex = 1;
            // 
            // btnFacEdit
            // 
            this.btnFacEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFacEdit.Location = new System.Drawing.Point(1010, 8);
            this.btnFacEdit.Name = "btnFacEdit";
            this.btnFacEdit.Size = new System.Drawing.Size(70, 30);
            this.btnFacEdit.TabIndex = 6;
            this.btnFacEdit.Text = "수정";
            this.btnFacEdit.UseVisualStyleBackColor = true;
            this.btnFacEdit.Click += new System.EventHandler(this.btnFacEdit_Click);
            // 
            // btnFacSave
            // 
            this.btnFacSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFacSave.Location = new System.Drawing.Point(1160, 8);
            this.btnFacSave.Name = "btnFacSave";
            this.btnFacSave.Size = new System.Drawing.Size(70, 30);
            this.btnFacSave.TabIndex = 5;
            this.btnFacSave.Text = "저장";
            this.btnFacSave.UseVisualStyleBackColor = true;
            this.btnFacSave.Click += new System.EventHandler(this.btnFacSave_Click);
            // 
            // btnFacAdd
            // 
            this.btnFacAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFacAdd.Location = new System.Drawing.Point(932, 8);
            this.btnFacAdd.Name = "btnFacAdd";
            this.btnFacAdd.Size = new System.Drawing.Size(70, 30);
            this.btnFacAdd.TabIndex = 4;
            this.btnFacAdd.Text = "신규";
            this.btnFacAdd.UseVisualStyleBackColor = true;
            this.btnFacAdd.Visible = false;
            this.btnFacAdd.Click += new System.EventHandler(this.btnFacAdd_Click);
            // 
            // btnBizDel
            // 
            this.btnBizDel.Location = new System.Drawing.Point(6, 8);
            this.btnBizDel.Name = "btnBizDel";
            this.btnBizDel.Size = new System.Drawing.Size(92, 30);
            this.btnBizDel.TabIndex = 3;
            this.btnBizDel.Text = "삭제(사업장)";
            this.btnBizDel.UseVisualStyleBackColor = true;
            this.btnBizDel.Click += new System.EventHandler(this.btnBizDel_Click);
            // 
            // btnFacDel
            // 
            this.btnFacDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFacDel.Location = new System.Drawing.Point(1086, 8);
            this.btnFacDel.Name = "btnFacDel";
            this.btnFacDel.Size = new System.Drawing.Size(70, 30);
            this.btnFacDel.TabIndex = 2;
            this.btnFacDel.Text = "삭제";
            this.btnFacDel.UseVisualStyleBackColor = true;
            this.btnFacDel.Click += new System.EventHandler(this.btnFacDel_Click);
            // 
            // btnBizAdd
            // 
            this.btnBizAdd.Location = new System.Drawing.Point(104, 8);
            this.btnBizAdd.Name = "btnBizAdd";
            this.btnBizAdd.Size = new System.Drawing.Size(92, 30);
            this.btnBizAdd.TabIndex = 1;
            this.btnBizAdd.Text = "신규(사업장)";
            this.btnBizAdd.UseVisualStyleBackColor = true;
            this.btnBizAdd.Click += new System.EventHandler(this.btnBizAdd_Click);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 44);
            this.splitContainer.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.ultraGrid_BIZ_PLA);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.panel_WE_PP_INFOS);
            this.splitContainer.Panel2.Controls.Add(this.panelPPInfogrid);
            this.splitContainer.Size = new System.Drawing.Size(1242, 700);
            this.splitContainer.SplitterDistance = 334;
            this.splitContainer.SplitterWidth = 3;
            this.splitContainer.TabIndex = 2;
            // 
            // ultraGrid_BIZ_PLA
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Appearance = appearance1;
            this.ultraGrid_BIZ_PLA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_BIZ_PLA.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_BIZ_PLA.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_BIZ_PLA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ultraGrid_BIZ_PLA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_BIZ_PLA.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ultraGrid_BIZ_PLA.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_BIZ_PLA.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.CellAppearance = appearance8;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.RowAppearance = appearance11;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_BIZ_PLA.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ultraGrid_BIZ_PLA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_BIZ_PLA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_BIZ_PLA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_BIZ_PLA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_BIZ_PLA.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_BIZ_PLA.Name = "ultraGrid_BIZ_PLA";
            this.ultraGrid_BIZ_PLA.Size = new System.Drawing.Size(334, 700);
            this.ultraGrid_BIZ_PLA.TabIndex = 0;
            this.ultraGrid_BIZ_PLA.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ultraGrid_BIZ_PLA_DoubleClickRow);
            this.ultraGrid_BIZ_PLA.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.ultraGrid_BIZ_PLA_AfterSelectChange);
            // 
            // panel_WE_PP_INFOS
            // 
            this.panel_WE_PP_INFOS.Controls.Add(this.panel_WE_PP_INFO);
            this.panel_WE_PP_INFOS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_WE_PP_INFOS.Location = new System.Drawing.Point(0, 208);
            this.panel_WE_PP_INFOS.Name = "panel_WE_PP_INFOS";
            this.panel_WE_PP_INFOS.Size = new System.Drawing.Size(905, 492);
            this.panel_WE_PP_INFOS.TabIndex = 8;
            // 
            // panel_WE_PP_INFO
            // 
            this.panel_WE_PP_INFO.AutoScroll = true;
            this.panel_WE_PP_INFO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_AN_OUT_MDLID);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_STD_Q);
            this.panel_WE_PP_INFO.Controls.Add(this.label_AN_OUT_MDLID);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_AN_IN_MDLID);
            this.panel_WE_PP_INFO.Controls.Add(this.comboBox_PP_CHK_VL_TP);
            this.panel_WE_PP_INFO.Controls.Add(this.label_AN_IN_MDLID);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_CHK_VL_TP);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_FRQ);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_FRQ);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_GRKSU);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_GRKSU);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_EFF);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_EFF);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_RATIO);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_RATIO);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_RPM);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_RPM);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_PW);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_PW);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_EST_YR);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_EST_YR);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_USG_YR);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_MAKE_YR);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_TP);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_OUT_DIA);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_IN_DIA);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_STD_H);
            this.panel_WE_PP_INFO.Controls.Add(this.comboBox_PP_GRP);
            this.panel_WE_PP_INFO.Controls.Add(this.comboBox_PP_CAT);
            this.panel_WE_PP_INFO.Controls.Add(this.textBox_PP_HOGI);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_TP);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_USG_YR);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_MAKE_YR);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_OUT_DIA);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_IN_DIA);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_STD_H);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_STD_Q);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_GRP);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_CAT);
            this.panel_WE_PP_INFO.Controls.Add(this.label_PP_HOGI);
            this.panel_WE_PP_INFO.Controls.Add(this.label12);
            this.panel_WE_PP_INFO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_WE_PP_INFO.Location = new System.Drawing.Point(0, 0);
            this.panel_WE_PP_INFO.Name = "panel_WE_PP_INFO";
            this.panel_WE_PP_INFO.Size = new System.Drawing.Size(905, 492);
            this.panel_WE_PP_INFO.TabIndex = 17;
            // 
            // textBox_AN_OUT_MDLID
            // 
            this.textBox_AN_OUT_MDLID.Location = new System.Drawing.Point(487, 270);
            this.textBox_AN_OUT_MDLID.Name = "textBox_AN_OUT_MDLID";
            this.textBox_AN_OUT_MDLID.Size = new System.Drawing.Size(171, 21);
            this.textBox_AN_OUT_MDLID.TabIndex = 53;
            this.textBox_AN_OUT_MDLID.Tag = "AN_OUT_MDLID";
            // 
            // textBox_PP_STD_Q
            // 
            this.textBox_PP_STD_Q.Location = new System.Drawing.Point(167, 93);
            this.textBox_PP_STD_Q.Name = "textBox_PP_STD_Q";
            this.textBox_PP_STD_Q.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_STD_Q.TabIndex = 61;
            this.textBox_PP_STD_Q.Tag = "PP_STD_Q";
            // 
            // label_AN_OUT_MDLID
            // 
            this.label_AN_OUT_MDLID.Location = new System.Drawing.Point(381, 273);
            this.label_AN_OUT_MDLID.Name = "label_AN_OUT_MDLID";
            this.label_AN_OUT_MDLID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_AN_OUT_MDLID.Size = new System.Drawing.Size(94, 12);
            this.label_AN_OUT_MDLID.TabIndex = 52;
            this.label_AN_OUT_MDLID.Tag = "";
            this.label_AN_OUT_MDLID.Text = "유출유량 항목ID";
            // 
            // textBox_AN_IN_MDLID
            // 
            this.textBox_AN_IN_MDLID.Location = new System.Drawing.Point(487, 244);
            this.textBox_AN_IN_MDLID.Name = "textBox_AN_IN_MDLID";
            this.textBox_AN_IN_MDLID.Size = new System.Drawing.Size(171, 21);
            this.textBox_AN_IN_MDLID.TabIndex = 51;
            this.textBox_AN_IN_MDLID.Tag = "AN_IN_MDLID";
            // 
            // comboBox_PP_CHK_VL_TP
            // 
            this.comboBox_PP_CHK_VL_TP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PP_CHK_VL_TP.FormattingEnabled = true;
            this.comboBox_PP_CHK_VL_TP.Location = new System.Drawing.Point(166, 255);
            this.comboBox_PP_CHK_VL_TP.Name = "comboBox_PP_CHK_VL_TP";
            this.comboBox_PP_CHK_VL_TP.Size = new System.Drawing.Size(170, 20);
            this.comboBox_PP_CHK_VL_TP.TabIndex = 60;
            this.comboBox_PP_CHK_VL_TP.Tag = "PP_CHK_VL_TP";
            // 
            // label_AN_IN_MDLID
            // 
            this.label_AN_IN_MDLID.Location = new System.Drawing.Point(381, 247);
            this.label_AN_IN_MDLID.Name = "label_AN_IN_MDLID";
            this.label_AN_IN_MDLID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_AN_IN_MDLID.Size = new System.Drawing.Size(94, 12);
            this.label_AN_IN_MDLID.TabIndex = 50;
            this.label_AN_IN_MDLID.Tag = "";
            this.label_AN_IN_MDLID.Text = "유입유량 항목ID";
            // 
            // label_PP_CHK_VL_TP
            // 
            this.label_PP_CHK_VL_TP.Location = new System.Drawing.Point(75, 258);
            this.label_PP_CHK_VL_TP.Name = "label_PP_CHK_VL_TP";
            this.label_PP_CHK_VL_TP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_CHK_VL_TP.Size = new System.Drawing.Size(85, 12);
            this.label_PP_CHK_VL_TP.TabIndex = 59;
            this.label_PP_CHK_VL_TP.Tag = "PP_CHK_VL_TP";
            this.label_PP_CHK_VL_TP.Text = "체크밸브유형";
            // 
            // textBox_PP_FRQ
            // 
            this.textBox_PP_FRQ.Location = new System.Drawing.Point(166, 228);
            this.textBox_PP_FRQ.Name = "textBox_PP_FRQ";
            this.textBox_PP_FRQ.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_FRQ.TabIndex = 58;
            this.textBox_PP_FRQ.Tag = "PP_FRQ";
            // 
            // label_PP_FRQ
            // 
            this.label_PP_FRQ.Location = new System.Drawing.Point(76, 232);
            this.label_PP_FRQ.Name = "label_PP_FRQ";
            this.label_PP_FRQ.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_FRQ.Size = new System.Drawing.Size(85, 12);
            this.label_PP_FRQ.TabIndex = 57;
            this.label_PP_FRQ.Text = "주파수";
            // 
            // textBox_PP_GRKSU
            // 
            this.textBox_PP_GRKSU.Location = new System.Drawing.Point(166, 201);
            this.textBox_PP_GRKSU.Name = "textBox_PP_GRKSU";
            this.textBox_PP_GRKSU.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_GRKSU.TabIndex = 56;
            this.textBox_PP_GRKSU.Tag = "PP_GRKSU";
            // 
            // label_PP_GRKSU
            // 
            this.label_PP_GRKSU.Location = new System.Drawing.Point(76, 205);
            this.label_PP_GRKSU.Name = "label_PP_GRKSU";
            this.label_PP_GRKSU.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_GRKSU.Size = new System.Drawing.Size(85, 12);
            this.label_PP_GRKSU.TabIndex = 55;
            this.label_PP_GRKSU.Text = "극수";
            // 
            // textBox_PP_EFF
            // 
            this.textBox_PP_EFF.Location = new System.Drawing.Point(166, 173);
            this.textBox_PP_EFF.Name = "textBox_PP_EFF";
            this.textBox_PP_EFF.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_EFF.TabIndex = 54;
            this.textBox_PP_EFF.Tag = "PP_EFF";
            // 
            // label_PP_EFF
            // 
            this.label_PP_EFF.Location = new System.Drawing.Point(76, 177);
            this.label_PP_EFF.Name = "label_PP_EFF";
            this.label_PP_EFF.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_EFF.Size = new System.Drawing.Size(85, 12);
            this.label_PP_EFF.TabIndex = 53;
            this.label_PP_EFF.Text = "설계효율";
            // 
            // textBox_PP_RATIO
            // 
            this.textBox_PP_RATIO.Location = new System.Drawing.Point(166, 148);
            this.textBox_PP_RATIO.Name = "textBox_PP_RATIO";
            this.textBox_PP_RATIO.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_RATIO.TabIndex = 52;
            this.textBox_PP_RATIO.Tag = "PP_RATIO";
            // 
            // label_PP_RATIO
            // 
            this.label_PP_RATIO.Location = new System.Drawing.Point(76, 152);
            this.label_PP_RATIO.Name = "label_PP_RATIO";
            this.label_PP_RATIO.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_RATIO.Size = new System.Drawing.Size(85, 12);
            this.label_PP_RATIO.TabIndex = 51;
            this.label_PP_RATIO.Text = "설계비속도";
            // 
            // textBox_PP_RPM
            // 
            this.textBox_PP_RPM.Location = new System.Drawing.Point(471, 215);
            this.textBox_PP_RPM.Name = "textBox_PP_RPM";
            this.textBox_PP_RPM.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_RPM.TabIndex = 50;
            this.textBox_PP_RPM.Tag = "PP_RPM";
            // 
            // label_PP_RPM
            // 
            this.label_PP_RPM.Location = new System.Drawing.Point(362, 219);
            this.label_PP_RPM.Name = "label_PP_RPM";
            this.label_PP_RPM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_RPM.Size = new System.Drawing.Size(88, 12);
            this.label_PP_RPM.TabIndex = 49;
            this.label_PP_RPM.Text = "RPM";
            // 
            // textBox_PP_PW
            // 
            this.textBox_PP_PW.Location = new System.Drawing.Point(471, 186);
            this.textBox_PP_PW.Name = "textBox_PP_PW";
            this.textBox_PP_PW.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_PW.TabIndex = 48;
            this.textBox_PP_PW.Tag = "PP_PW";
            // 
            // label_PP_PW
            // 
            this.label_PP_PW.Location = new System.Drawing.Point(362, 192);
            this.label_PP_PW.Name = "label_PP_PW";
            this.label_PP_PW.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_PW.Size = new System.Drawing.Size(88, 12);
            this.label_PP_PW.TabIndex = 47;
            this.label_PP_PW.Text = "동력";
            // 
            // textBox_PP_EST_YR
            // 
            this.textBox_PP_EST_YR.Location = new System.Drawing.Point(471, 157);
            this.textBox_PP_EST_YR.Name = "textBox_PP_EST_YR";
            this.textBox_PP_EST_YR.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_EST_YR.TabIndex = 46;
            this.textBox_PP_EST_YR.Tag = "PP_EST_YR";
            // 
            // label_PP_EST_YR
            // 
            this.label_PP_EST_YR.Location = new System.Drawing.Point(362, 163);
            this.label_PP_EST_YR.Name = "label_PP_EST_YR";
            this.label_PP_EST_YR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_EST_YR.Size = new System.Drawing.Size(88, 12);
            this.label_PP_EST_YR.TabIndex = 45;
            this.label_PP_EST_YR.Text = "설치년도";
            // 
            // textBox_PP_USG_YR
            // 
            this.textBox_PP_USG_YR.Location = new System.Drawing.Point(471, 128);
            this.textBox_PP_USG_YR.Name = "textBox_PP_USG_YR";
            this.textBox_PP_USG_YR.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_USG_YR.TabIndex = 44;
            this.textBox_PP_USG_YR.Tag = "PP_USG_YR";
            // 
            // textBox_PP_MAKE_YR
            // 
            this.textBox_PP_MAKE_YR.Location = new System.Drawing.Point(471, 99);
            this.textBox_PP_MAKE_YR.Name = "textBox_PP_MAKE_YR";
            this.textBox_PP_MAKE_YR.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_MAKE_YR.TabIndex = 43;
            this.textBox_PP_MAKE_YR.Tag = "PP_MAKE_YR";
            // 
            // textBox_PP_TP
            // 
            this.textBox_PP_TP.Location = new System.Drawing.Point(471, 70);
            this.textBox_PP_TP.Name = "textBox_PP_TP";
            this.textBox_PP_TP.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_TP.TabIndex = 42;
            this.textBox_PP_TP.Tag = "PP_TP";
            // 
            // textBox_PP_OUT_DIA
            // 
            this.textBox_PP_OUT_DIA.Location = new System.Drawing.Point(471, 41);
            this.textBox_PP_OUT_DIA.Name = "textBox_PP_OUT_DIA";
            this.textBox_PP_OUT_DIA.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_OUT_DIA.TabIndex = 41;
            this.textBox_PP_OUT_DIA.Tag = "PP_OUT_DIA";
            // 
            // textBox_PP_IN_DIA
            // 
            this.textBox_PP_IN_DIA.Location = new System.Drawing.Point(471, 12);
            this.textBox_PP_IN_DIA.Name = "textBox_PP_IN_DIA";
            this.textBox_PP_IN_DIA.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_IN_DIA.TabIndex = 40;
            this.textBox_PP_IN_DIA.Tag = "PP_IN_DIA";
            // 
            // textBox_PP_STD_H
            // 
            this.textBox_PP_STD_H.Location = new System.Drawing.Point(166, 121);
            this.textBox_PP_STD_H.Name = "textBox_PP_STD_H";
            this.textBox_PP_STD_H.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_STD_H.TabIndex = 39;
            this.textBox_PP_STD_H.Tag = "PP_STD_H";
            // 
            // comboBox_PP_GRP
            // 
            this.comboBox_PP_GRP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PP_GRP.FormattingEnabled = true;
            this.comboBox_PP_GRP.Location = new System.Drawing.Point(167, 65);
            this.comboBox_PP_GRP.Name = "comboBox_PP_GRP";
            this.comboBox_PP_GRP.Size = new System.Drawing.Size(170, 20);
            this.comboBox_PP_GRP.TabIndex = 37;
            this.comboBox_PP_GRP.Tag = "PP_GRP";
            // 
            // comboBox_PP_CAT
            // 
            this.comboBox_PP_CAT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PP_CAT.FormattingEnabled = true;
            this.comboBox_PP_CAT.Location = new System.Drawing.Point(167, 38);
            this.comboBox_PP_CAT.Name = "comboBox_PP_CAT";
            this.comboBox_PP_CAT.Size = new System.Drawing.Size(170, 20);
            this.comboBox_PP_CAT.TabIndex = 36;
            this.comboBox_PP_CAT.Tag = "PP_CAT";
            // 
            // textBox_PP_HOGI
            // 
            this.textBox_PP_HOGI.Enabled = false;
            this.textBox_PP_HOGI.Location = new System.Drawing.Point(167, 8);
            this.textBox_PP_HOGI.Name = "textBox_PP_HOGI";
            this.textBox_PP_HOGI.Size = new System.Drawing.Size(171, 21);
            this.textBox_PP_HOGI.TabIndex = 35;
            this.textBox_PP_HOGI.Tag = "PP_HOGI";
            // 
            // label_PP_TP
            // 
            this.label_PP_TP.Location = new System.Drawing.Point(362, 74);
            this.label_PP_TP.Name = "label_PP_TP";
            this.label_PP_TP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_TP.Size = new System.Drawing.Size(88, 12);
            this.label_PP_TP.TabIndex = 34;
            this.label_PP_TP.Text = "형식";
            // 
            // label_PP_USG_YR
            // 
            this.label_PP_USG_YR.Location = new System.Drawing.Point(362, 134);
            this.label_PP_USG_YR.Name = "label_PP_USG_YR";
            this.label_PP_USG_YR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_USG_YR.Size = new System.Drawing.Size(88, 12);
            this.label_PP_USG_YR.TabIndex = 33;
            this.label_PP_USG_YR.Text = "내용연수";
            // 
            // label_PP_MAKE_YR
            // 
            this.label_PP_MAKE_YR.Location = new System.Drawing.Point(362, 106);
            this.label_PP_MAKE_YR.Name = "label_PP_MAKE_YR";
            this.label_PP_MAKE_YR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_MAKE_YR.Size = new System.Drawing.Size(88, 12);
            this.label_PP_MAKE_YR.TabIndex = 31;
            this.label_PP_MAKE_YR.Text = "제작년도";
            // 
            // label_PP_OUT_DIA
            // 
            this.label_PP_OUT_DIA.Location = new System.Drawing.Point(362, 46);
            this.label_PP_OUT_DIA.Name = "label_PP_OUT_DIA";
            this.label_PP_OUT_DIA.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_OUT_DIA.Size = new System.Drawing.Size(88, 12);
            this.label_PP_OUT_DIA.TabIndex = 30;
            this.label_PP_OUT_DIA.Text = "토출구경";
            // 
            // label_PP_IN_DIA
            // 
            this.label_PP_IN_DIA.Location = new System.Drawing.Point(362, 18);
            this.label_PP_IN_DIA.Name = "label_PP_IN_DIA";
            this.label_PP_IN_DIA.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_IN_DIA.Size = new System.Drawing.Size(88, 12);
            this.label_PP_IN_DIA.TabIndex = 29;
            this.label_PP_IN_DIA.Text = "흡입구경";
            // 
            // label_PP_STD_H
            // 
            this.label_PP_STD_H.Location = new System.Drawing.Point(76, 124);
            this.label_PP_STD_H.Name = "label_PP_STD_H";
            this.label_PP_STD_H.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_STD_H.Size = new System.Drawing.Size(85, 12);
            this.label_PP_STD_H.TabIndex = 28;
            this.label_PP_STD_H.Text = "정격양정";
            // 
            // label_PP_STD_Q
            // 
            this.label_PP_STD_Q.Location = new System.Drawing.Point(76, 96);
            this.label_PP_STD_Q.Name = "label_PP_STD_Q";
            this.label_PP_STD_Q.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_STD_Q.Size = new System.Drawing.Size(85, 12);
            this.label_PP_STD_Q.TabIndex = 27;
            this.label_PP_STD_Q.Text = "정격유량";
            // 
            // label_PP_GRP
            // 
            this.label_PP_GRP.Location = new System.Drawing.Point(76, 68);
            this.label_PP_GRP.Name = "label_PP_GRP";
            this.label_PP_GRP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_GRP.Size = new System.Drawing.Size(85, 12);
            this.label_PP_GRP.TabIndex = 26;
            this.label_PP_GRP.Text = "펌프그룹";
            // 
            // label_PP_CAT
            // 
            this.label_PP_CAT.Location = new System.Drawing.Point(76, 40);
            this.label_PP_CAT.Name = "label_PP_CAT";
            this.label_PP_CAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_CAT.Size = new System.Drawing.Size(85, 12);
            this.label_PP_CAT.TabIndex = 25;
            this.label_PP_CAT.Text = "펌프구분";
            // 
            // label_PP_HOGI
            // 
            this.label_PP_HOGI.Location = new System.Drawing.Point(76, 12);
            this.label_PP_HOGI.Name = "label_PP_HOGI";
            this.label_PP_HOGI.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_HOGI.Size = new System.Drawing.Size(85, 12);
            this.label_PP_HOGI.TabIndex = 24;
            this.label_PP_HOGI.Text = "호기번호";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 490);
            this.label12.TabIndex = 17;
            this.label12.Text = "시설물 상세정보";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelPPInfogrid
            // 
            this.panelPPInfogrid.Controls.Add(this.ultraGrid_WE_PP_INFO);
            this.panelPPInfogrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPPInfogrid.Location = new System.Drawing.Point(0, 0);
            this.panelPPInfogrid.Name = "panelPPInfogrid";
            this.panelPPInfogrid.Size = new System.Drawing.Size(905, 208);
            this.panelPPInfogrid.TabIndex = 4;
            // 
            // ultraGrid_WE_PP_INFO
            // 
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Appearance = appearance49;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.CellAppearance = appearance56;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.RowAppearance = appearance59;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_WE_PP_INFO.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_WE_PP_INFO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_WE_PP_INFO.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_WE_PP_INFO.Name = "ultraGrid_WE_PP_INFO";
            this.ultraGrid_WE_PP_INFO.Size = new System.Drawing.Size(905, 208);
            this.ultraGrid_WE_PP_INFO.TabIndex = 4;
            this.ultraGrid_WE_PP_INFO.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.ultraGrid_WE_PP_INFO_AfterSelectChange);
            // 
            // frmBaseResource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 744);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmBaseResource";
            this.Text = "frmBaseResource";
            this.Load += new System.EventHandler(this.frmBaseResource_Load);
            this.panelCommand.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_BIZ_PLA)).EndInit();
            this.panel_WE_PP_INFOS.ResumeLayout(false);
            this.panel_WE_PP_INFO.ResumeLayout(false);
            this.panel_WE_PP_INFO.PerformLayout();
            this.panelPPInfogrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_WE_PP_INFO)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnFacEdit;
        private System.Windows.Forms.Button btnFacSave;
        private System.Windows.Forms.Button btnFacAdd;
        private System.Windows.Forms.Button btnBizDel;
        private System.Windows.Forms.Button btnFacDel;
        private System.Windows.Forms.Button btnBizAdd;
        private System.Windows.Forms.SplitContainer splitContainer;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_BIZ_PLA;
        private System.Windows.Forms.Panel panelPPInfogrid;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_WE_PP_INFO;
        private System.Windows.Forms.Panel panel_WE_PP_INFOS;
        private System.Windows.Forms.Panel panel_WE_PP_INFO;
        private System.Windows.Forms.TextBox textBox_PP_STD_Q;
        private System.Windows.Forms.ComboBox comboBox_PP_CHK_VL_TP;
        private System.Windows.Forms.Label label_PP_CHK_VL_TP;
        private System.Windows.Forms.TextBox textBox_PP_FRQ;
        private System.Windows.Forms.Label label_PP_FRQ;
        private System.Windows.Forms.TextBox textBox_PP_GRKSU;
        private System.Windows.Forms.Label label_PP_GRKSU;
        private System.Windows.Forms.TextBox textBox_PP_EFF;
        private System.Windows.Forms.Label label_PP_EFF;
        private System.Windows.Forms.TextBox textBox_PP_RATIO;
        private System.Windows.Forms.Label label_PP_RATIO;
        private System.Windows.Forms.TextBox textBox_PP_RPM;
        private System.Windows.Forms.Label label_PP_RPM;
        private System.Windows.Forms.TextBox textBox_PP_PW;
        private System.Windows.Forms.Label label_PP_PW;
        private System.Windows.Forms.TextBox textBox_PP_EST_YR;
        private System.Windows.Forms.Label label_PP_EST_YR;
        private System.Windows.Forms.TextBox textBox_PP_USG_YR;
        private System.Windows.Forms.TextBox textBox_PP_MAKE_YR;
        private System.Windows.Forms.TextBox textBox_PP_TP;
        private System.Windows.Forms.TextBox textBox_PP_OUT_DIA;
        private System.Windows.Forms.TextBox textBox_PP_IN_DIA;
        private System.Windows.Forms.TextBox textBox_PP_STD_H;
        private System.Windows.Forms.ComboBox comboBox_PP_GRP;
        private System.Windows.Forms.ComboBox comboBox_PP_CAT;
        private System.Windows.Forms.TextBox textBox_PP_HOGI;
        private System.Windows.Forms.Label label_PP_TP;
        private System.Windows.Forms.Label label_PP_USG_YR;
        private System.Windows.Forms.Label label_PP_MAKE_YR;
        private System.Windows.Forms.Label label_PP_OUT_DIA;
        private System.Windows.Forms.Label label_PP_IN_DIA;
        private System.Windows.Forms.Label label_PP_STD_H;
        private System.Windows.Forms.Label label_PP_STD_Q;
        private System.Windows.Forms.Label label_PP_GRP;
        private System.Windows.Forms.Label label_PP_CAT;
        private System.Windows.Forms.Label label_PP_HOGI;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_AN_OUT_MDLID;
        private System.Windows.Forms.Label label_AN_OUT_MDLID;
        private System.Windows.Forms.TextBox textBox_AN_IN_MDLID;
        private System.Windows.Forms.Label label_AN_IN_MDLID;
    }
}