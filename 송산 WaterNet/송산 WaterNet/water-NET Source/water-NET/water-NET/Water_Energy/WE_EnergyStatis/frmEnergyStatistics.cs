﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WE_EnergyStatis
{
    public partial class frmEnergyStatistics : Form, WaterNet.WaterNetCore.IForminterface
    {
        #region Private Field ------------------------------------------------------------------------------

        #endregion

        #region 생성자 및 환경설정 ----------------------------------------------------------------------
        public frmEnergyStatistics()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return "에너지관리-통계정보"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion


        #region 초기화설정
        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            #region ComboBox 설정
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT BIZ_PLA_SEQ AS CD, BIZ_PLA_NM AS CDNM FROM WE_BIZ_PLA ORDER BY BIZ_PLA_NM ASC");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_BIZ_PLA_NM, oStringBuilder.ToString(), false);

            #endregion

            #region 날짜 설정
            ultraDateTimeEditor_startDay.Value = DateTime.Today;
            ultraDateTimeEditor_endDay.Value = DateTime.Today.AddDays(1);
            #endregion

            #region 버튼 및 option 설정
            optDay.Checked = true;
            optMonth.Checked = false;
            optLength.Checked = false;
            #endregion

            #region 챠트설정
            chart1.AxisX.AutoScale = true;
            chart1.AxisX.Staggered = true;

            #endregion

            #region Query문 설정

            #endregion
        }
        #endregion

        /// <summary>
        /// 챠트 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.chart1.Data.Series = 15;

            this.chart1.AxisX.LabelsFormat.Format = ChartFX.WinForms.AxisFormat.Date;
            this.chart1.AxisX.LabelsFormat.CustomFormat = "yyyy-MM-dd";

            this.chart1.Series[0].Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart1.Series[0].Line.Width = 1;
            this.chart1.Series[0].MarkerShape = ChartFX.WinForms.MarkerShape.None;


        }

        /// <summary>
        /// 화면 보기
        /// </summary>
        public void Open()
        {

        }

        #endregion

        #region Property -------------------------------------------------------------------------------------

        #endregion

        #region Private Method -------------------------------------------------------------------------------------

        #endregion

        #region public Method -------------------------------------------------------------------------------------

        #endregion
    }
}
