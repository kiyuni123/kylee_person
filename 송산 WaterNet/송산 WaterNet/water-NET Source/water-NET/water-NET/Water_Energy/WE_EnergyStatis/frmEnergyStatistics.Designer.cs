﻿namespace WaterNet.WE_EnergyStatis
{
    partial class frmEnergyStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.optLength = new System.Windows.Forms.RadioButton();
            this.optMonth = new System.Windows.Forms.RadioButton();
            this.optDay = new System.Windows.Forms.RadioButton();
            this.ultraDateTimeEditor_endDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraDateTimeEditor_startDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.comboBox_BIZ_PLA_NM = new System.Windows.Forms.ComboBox();
            this.label_BIZ_PLA_NM = new System.Windows.Forms.Label();
            this.btnSel = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.panel_Contents = new System.Windows.Forms.Panel();
            this.ultraGridWE_Statistics = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_endDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_startDay)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel_Contents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridWE_Statistics)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.optLength);
            this.panelCommand.Controls.Add(this.optMonth);
            this.panelCommand.Controls.Add(this.optDay);
            this.panelCommand.Controls.Add(this.ultraDateTimeEditor_endDay);
            this.panelCommand.Controls.Add(this.label1);
            this.panelCommand.Controls.Add(this.ultraDateTimeEditor_startDay);
            this.panelCommand.Controls.Add(this.comboBox_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.label_BIZ_PLA_NM);
            this.panelCommand.Controls.Add(this.btnSel);
            this.panelCommand.Controls.Add(this.btnExcel);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(1054, 55);
            this.panelCommand.TabIndex = 3;
            // 
            // optLength
            // 
            this.optLength.AutoSize = true;
            this.optLength.Location = new System.Drawing.Point(319, 20);
            this.optLength.Name = "optLength";
            this.optLength.Size = new System.Drawing.Size(59, 16);
            this.optLength.TabIndex = 23;
            this.optLength.TabStop = true;
            this.optLength.Text = "기간별";
            this.optLength.UseVisualStyleBackColor = true;
            // 
            // optMonth
            // 
            this.optMonth.AutoSize = true;
            this.optMonth.Location = new System.Drawing.Point(259, 20);
            this.optMonth.Name = "optMonth";
            this.optMonth.Size = new System.Drawing.Size(47, 16);
            this.optMonth.TabIndex = 22;
            this.optMonth.TabStop = true;
            this.optMonth.Text = "월별";
            this.optMonth.UseVisualStyleBackColor = true;
            // 
            // optDay
            // 
            this.optDay.AutoSize = true;
            this.optDay.Location = new System.Drawing.Point(196, 20);
            this.optDay.Name = "optDay";
            this.optDay.Size = new System.Drawing.Size(47, 16);
            this.optDay.TabIndex = 21;
            this.optDay.TabStop = true;
            this.optDay.Text = "일별";
            this.optDay.UseVisualStyleBackColor = true;
            // 
            // ultraDateTimeEditor_endDay
            // 
            this.ultraDateTimeEditor_endDay.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_endDay.AutoSize = false;
            this.ultraDateTimeEditor_endDay.Location = new System.Drawing.Point(526, 17);
            this.ultraDateTimeEditor_endDay.Name = "ultraDateTimeEditor_endDay";
            this.ultraDateTimeEditor_endDay.Size = new System.Drawing.Size(104, 21);
            this.ultraDateTimeEditor_endDay.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(507, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "~";
            // 
            // ultraDateTimeEditor_startDay
            // 
            this.ultraDateTimeEditor_startDay.AutoFillTime = Infragistics.Win.UltraWinMaskedEdit.AutoFillTime.CurrentTime;
            this.ultraDateTimeEditor_startDay.AutoSize = false;
            this.ultraDateTimeEditor_startDay.Location = new System.Drawing.Point(398, 17);
            this.ultraDateTimeEditor_startDay.Name = "ultraDateTimeEditor_startDay";
            this.ultraDateTimeEditor_startDay.Size = new System.Drawing.Size(104, 21);
            this.ultraDateTimeEditor_startDay.TabIndex = 18;
            // 
            // comboBox_BIZ_PLA_NM
            // 
            this.comboBox_BIZ_PLA_NM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BIZ_PLA_NM.FormattingEnabled = true;
            this.comboBox_BIZ_PLA_NM.Location = new System.Drawing.Point(60, 18);
            this.comboBox_BIZ_PLA_NM.Name = "comboBox_BIZ_PLA_NM";
            this.comboBox_BIZ_PLA_NM.Size = new System.Drawing.Size(111, 20);
            this.comboBox_BIZ_PLA_NM.TabIndex = 12;
            // 
            // label_BIZ_PLA_NM
            // 
            this.label_BIZ_PLA_NM.AutoSize = true;
            this.label_BIZ_PLA_NM.Location = new System.Drawing.Point(9, 22);
            this.label_BIZ_PLA_NM.Name = "label_BIZ_PLA_NM";
            this.label_BIZ_PLA_NM.Size = new System.Drawing.Size(41, 12);
            this.label_BIZ_PLA_NM.TabIndex = 11;
            this.label_BIZ_PLA_NM.Text = "사업장";
            // 
            // btnSel
            // 
            this.btnSel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSel.Location = new System.Drawing.Point(886, 13);
            this.btnSel.Name = "btnSel";
            this.btnSel.Size = new System.Drawing.Size(75, 30);
            this.btnSel.TabIndex = 5;
            this.btnSel.Text = "조회";
            this.btnSel.UseVisualStyleBackColor = true;
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Location = new System.Drawing.Point(967, 13);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExcel.TabIndex = 2;
            this.btnExcel.Text = "엑셀";
            this.btnExcel.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 55);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chart1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel_Contents);
            this.splitContainer1.Size = new System.Drawing.Size(1054, 456);
            this.splitContainer1.SplitterDistance = 215;
            this.splitContainer1.TabIndex = 5;
            // 
            // chart1
            // 
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(1054, 215);
            this.chart1.TabIndex = 0;
            // 
            // panel_Contents
            // 
            this.panel_Contents.Controls.Add(this.ultraGridWE_Statistics);
            this.panel_Contents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Contents.Location = new System.Drawing.Point(0, 0);
            this.panel_Contents.Name = "panel_Contents";
            this.panel_Contents.Size = new System.Drawing.Size(1054, 237);
            this.panel_Contents.TabIndex = 4;
            // 
            // ultraGridWE_Statistics
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridWE_Statistics.DisplayLayout.Appearance = appearance13;
            this.ultraGridWE_Statistics.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridWE_Statistics.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridWE_Statistics.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridWE_Statistics.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ultraGridWE_Statistics.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridWE_Statistics.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ultraGridWE_Statistics.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridWE_Statistics.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridWE_Statistics.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridWE_Statistics.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ultraGridWE_Statistics.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridWE_Statistics.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridWE_Statistics.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridWE_Statistics.DisplayLayout.Override.CellAppearance = appearance20;
            this.ultraGridWE_Statistics.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridWE_Statistics.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridWE_Statistics.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ultraGridWE_Statistics.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ultraGridWE_Statistics.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridWE_Statistics.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridWE_Statistics.DisplayLayout.Override.RowAppearance = appearance23;
            this.ultraGridWE_Statistics.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridWE_Statistics.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ultraGridWE_Statistics.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridWE_Statistics.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridWE_Statistics.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGridWE_Statistics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridWE_Statistics.Location = new System.Drawing.Point(0, 0);
            this.ultraGridWE_Statistics.Name = "ultraGridWE_Statistics";
            this.ultraGridWE_Statistics.Size = new System.Drawing.Size(1054, 237);
            this.ultraGridWE_Statistics.TabIndex = 3;
            // 
            // frmEnergyStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 511);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmEnergyStatistics";
            this.Text = "통계정보";
            this.panelCommand.ResumeLayout(false);
            this.panelCommand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_endDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor_startDay)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel_Contents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridWE_Statistics)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.ComboBox comboBox_BIZ_PLA_NM;
        private System.Windows.Forms.Label label_BIZ_PLA_NM;
        private System.Windows.Forms.Button btnSel;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private ChartFX.WinForms.Chart chart1;
        private System.Windows.Forms.Panel panel_Contents;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridWE_Statistics;
        private System.Windows.Forms.RadioButton optDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_endDay;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor_startDay;
        private System.Windows.Forms.RadioButton optLength;
        private System.Windows.Forms.RadioButton optMonth;
    }
}