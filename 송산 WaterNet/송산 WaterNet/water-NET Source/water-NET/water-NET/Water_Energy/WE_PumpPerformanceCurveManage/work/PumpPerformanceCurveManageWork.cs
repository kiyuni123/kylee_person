﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WE_PumpPerformanceCurveManage.dao;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.work;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;
using System.Windows.Forms;

namespace WaterNet.WE_PumpPerformanceCurveManage.work
{
    public class PumpPerformanceCurveManageWork : BaseWork
    {
        private static PumpPerformanceCurveManageWork work = null;
        private PumpPerformanceCurveManageDao dao = null;

        private PumpPerformanceCurveManageWork()
        {
            dao = PumpPerformanceCurveManageDao.GetInstance();
        }

        public static PumpPerformanceCurveManageWork GetInstance()
        {
            if (work == null)
            {
                work = new PumpPerformanceCurveManageWork();
            }
            return work;
        }

        //모델의 성능곡선을 변경한다.
        public void UpdateModelCurve(UltraGrid ultraGrid)
        {
            Hashtable parameter = new Hashtable();

            if (ultraGrid.Rows.Count > 0)
            {
                parameter["C_ID"] = ultraGrid.Rows[0].Cells["C_ID"].Value;
                parameter["E_ID"] = ultraGrid.Rows[0].Cells["E_ID"].Value;
                parameter["CURVE_ID"] = ultraGrid.Rows[0].Cells["CURVE_ID"].Value;
                parameter["PUMP_FTR_IDN"] = ultraGrid.Rows[0].Cells["PUMP_FTR_IDN"].Value;
            }
            
            if (parameter["CURVE_ID"] == null)
            {
                return;
            }

            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.DeleteCurveModel(base.DataBaseManager, parameter);

                int h_index = -1;
                int e_index = -1;

                foreach (UltraGridRow row in ultraGrid.Rows)
                {
                    if (row.Cells["H"].Value != DBNull.Value)
                    {
                        h_index++;
                        if (parameter["C_ID"] != null)
                        {
                            Hashtable h_parameter = new Hashtable();
                            h_parameter["ID"] = parameter["C_ID"];
                            h_parameter["IDX"] = h_index;
                            h_parameter["X"] = row.Cells["FLOW"].Value;
                            h_parameter["Y"] = row.Cells["H"].Value;
                            h_parameter["CURCD"] = "000001";
                            dao.InsertCurveModel(base.DataBaseManager, h_parameter);
                        }
                    }

                    if (row.Cells["E"].Value != DBNull.Value)
                    {
                        e_index++;
                        if (parameter["E_ID"] != null)
                        {
                            Hashtable e_parameter = new Hashtable();
                            e_parameter["ID"] = parameter["E_ID"];
                            e_parameter["IDX"] = h_index;
                            e_parameter["X"] = row.Cells["FLOW"].Value;
                            e_parameter["Y"] = row.Cells["E"].Value;
                            e_parameter["CURCD"] = "000002";
                            dao.InsertCurveModel(base.DataBaseManager, e_parameter);
                        }
                    }
                }

                dao.UpdatePumpModelYn(base.DataBaseManager, parameter);

                CommitTransaction();
                MessageBox.Show("정상적으로 처리 되었습니다.");
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public DataTable SelectCurrentCurve(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectCurrentCurve(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectModelCurve(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectModelCurve(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectPumpInfo(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectPumpInfo(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectPumpMaster(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectPumpMaster(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectPumpPerformanceCurveManage(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectPumpPerformanceCurveManage(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];

                        result.Columns.Add("SELECTED", typeof(bool));

                        foreach (DataRow row in result.Rows)
                        {
                            row["SELECTED"] = false;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectPumpPerformanceCurveManageDetail(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = dao.SelectPumpPerformanceCurveManageDetail(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void InsertPumpPerformanceCurveManage(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                parameter["CURVE_ID"] = dao.SelectPumpPerformanceCurveManageSEQ(base.DataBaseManager, parameter);
                dao.InsertPumpPerformanceCurveManage(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void UpdatePumpPerformanceCurveManage(UltraGrid ultraGrid)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow gridRow in ultraGrid.Rows)
                {
                    if (Convert.ToBoolean(gridRow.Cells["SELECTED"].Value))
                    {
                        Hashtable parameter = Utils.ConverToHashtable(gridRow);
                        dao.UpdatePumpPerformanceCurveManage(base.DataBaseManager, parameter);
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void DeletePumpPerformanceCurveManage(UltraGrid ultraGrid)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow gridRow in ultraGrid.Rows)
                {
                    if (Convert.ToBoolean(gridRow.Cells["SELECTED"].Value))
                    {
                        Hashtable parameter = Utils.ConverToHashtable(gridRow);
                        dao.DeletePumpPerformanceCurveManage(base.DataBaseManager, parameter);
                        dao.DeletePumpPerformanceCurveManageDetail(base.DataBaseManager, parameter);
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void UpdatePumpPerformanceCurveManageDetail(UltraGrid ultraGrid, Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.DeletePumpPerformanceCurveManageDetail(base.DataBaseManager, parameter);

                foreach (UltraGridRow row in ultraGrid.Rows)
                {
                    parameter["FLOW"] = row.Cells["FLOW"].Value;
                    parameter["H"] = row.Cells["H"].Value;
                    parameter["E"] = row.Cells["E"].Value;
                    parameter["POINT_YN"] = row.Cells["POINT_YN"].Value;
                    dao.InsertPumpPerformanceCurveManageDetail(base.DataBaseManager, parameter);
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }
    }
}
