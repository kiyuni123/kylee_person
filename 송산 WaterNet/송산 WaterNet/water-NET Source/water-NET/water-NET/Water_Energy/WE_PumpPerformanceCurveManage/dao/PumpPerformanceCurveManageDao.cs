﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using WaterNet.WaterNetCore;
using System.Collections;

namespace WaterNet.WE_PumpPerformanceCurveManage.dao
{
    public class PumpPerformanceCurveManageDao
    {
        private static PumpPerformanceCurveManageDao dao = null;
        private PumpPerformanceCurveManageDao() { }

        public static PumpPerformanceCurveManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new PumpPerformanceCurveManageDao();
            }

            return dao;
        }

        public void UpdatePumpModelYn(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("UPDATE WE_PUMP ");
            query.AppendLine("   SET CURVE_ID = :CURVE_ID ");
            query.AppendLine("      ,MODEL_YN = 'Y' ");
            query.AppendLine(" WHERE PUMP_ID = :PUMP_FTR_IDN ");

            IDataParameter[] parameters =  {
                     new OracleParameter("CURVE_ID", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["CURVE_ID"];
            parameters[1].Value = parameter["PUMP_FTR_IDN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void InsertCurveModel(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("insert into wh_curves (                                           ");
            //query.AppendLine("select                                                            ");
            //query.AppendLine("(select max(inp_number) from wh_title a where a.energy_gbn = 'Y'),");
            //query.AppendLine(":ID,                                                              ");
            //query.AppendLine(":IDX,                                                             ");
            //query.AppendLine(":X,                                                               ");
            //query.AppendLine(":Y,                                                               ");
            //query.AppendLine(":CURCD,                                                           ");
            //query.AppendLine("''                                                                ");
            //query.AppendLine("  from dual                                                       ");
            //query.AppendLine(")                                                                 ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("ID", OracleDbType.Varchar2),
            //         new OracleParameter("IDX", OracleDbType.Varchar2),
            //         new OracleParameter("X", OracleDbType.Varchar2),
            //         new OracleParameter("Y", OracleDbType.Varchar2),
            //         new OracleParameter("CURCD", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["ID"];
            //parameters[1].Value = parameter["IDX"];
            //parameters[2].Value = parameter["X"];
            //parameters[3].Value = parameter["Y"];
            //parameters[4].Value = parameter["CURCD"];

            query.AppendLine("INSERT INTO ");
            query.AppendLine("WH_CURVES ");
            query.AppendLine("( ");
            query.AppendLine(" SELECT (SELECT MAX(INP_NUMBER) FROM WH_TITLE A WHERE A.ENERGY_GBN = 'Y') ");
            query.AppendLine("       ,:ID ");
            query.AppendLine("	     ,:IDX ");
            query.AppendLine("       ,:X ");
            query.AppendLine("       ,:Y ");
            query.AppendLine("   FROM DUAL ");
            query.AppendLine(") ");

            IDataParameter[] parameters =  {
                     new OracleParameter("ID", OracleDbType.Varchar2),
                     new OracleParameter("IDX", OracleDbType.Varchar2),
                     new OracleParameter("X", OracleDbType.Varchar2),
                     new OracleParameter("Y", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["ID"];
            parameters[1].Value = parameter["IDX"];
            parameters[2].Value = parameter["X"];
            parameters[3].Value = parameter["Y"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeleteCurveModel(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine(" delete from wh_curves                                                     ");
            //query.AppendLine("  where id in (:C_ID, :E_ID)                                               ");
            //query.AppendLine("    and inp_number = (select inp_number from wh_title where energy_gbn = 'Y')");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("C_ID", OracleDbType.Varchar2),
            //         new OracleParameter("E_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["C_ID"];
            //parameters[1].Value = parameter["E_ID"];

            query.AppendLine("DELETE FROM WH_CURVES ");
            query.AppendLine(" WHERE ID IN (:C_ID, :E_ID) ");
            query.AppendLine("   AND INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y') ");

            IDataParameter[] parameters =  {
                     new OracleParameter("C_ID", OracleDbType.Varchar2),
                     new OracleParameter("E_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["C_ID"];
            parameters[1].Value = parameter["E_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public DataSet SelectCurrentCurve(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with tmp as                                                                                     ");
            //query.AppendLine("(                                                                                               ");
            //query.AppendLine("select (select inp_number from wh_title where energy_gbn = 'Y') inp_number                        ");
            //query.AppendLine("      ,:PUMP_FTR_IDN pump_ftr_idn                                                               ");
            //query.AppendLine("      ,c_id                                                                                     ");
            //query.AppendLine("      ,e_id                                                                                     ");
            //query.AppendLine("  from                                                                                          ");
            //query.AppendLine("      (                                                                                         ");
            //query.AppendLine("       select replace(regexp_replace(b.properties, '[^,]*[hH][eE][aA][dD]', ''),' ', '') c_id   ");
            //query.AppendLine("         from wh_title a                                                                        ");
            //query.AppendLine("             ,wh_pumps b                                                                        ");
            //query.AppendLine("             ,wh_tags c                                                                         ");
            //query.AppendLine("        where a.energy_gbn = 'Y'                                                                  ");
            //query.AppendLine("          and b.inp_number = a.inp_number                                                       ");
            //query.AppendLine("          and c.inp_number = b.inp_number                                                       ");
            //query.AppendLine("          and c.id = b.id                                                                       ");
            //query.AppendLine("          and c.value1 = :PUMP_FTR_IDN                                                          ");
            //query.AppendLine("      ),                                                                                        ");
            //query.AppendLine("      (                                                                                         ");
            //query.AppendLine("       select replace(regexp_replace(b.energy_statement,                                        ");
            //query.AppendLine("                    '[^,]*[eE][fF][fF][iI][cC][iI][eE][nN][cC][yY]', ''),' ', '') e_id          ");
            //query.AppendLine("        from (                                                                                  ");
            //query.AppendLine("              select a.inp_number                                                               ");
            //query.AppendLine("                    ,'PUMP'||c.id||'EFFICIENCY' id                                              ");
            //query.AppendLine("                from wh_title a                                                                 ");
            //query.AppendLine("                    ,wh_pumps b                                                                 ");
            //query.AppendLine("                    ,wh_tags c                                                                  ");
            //query.AppendLine("               where a.energy_gbn = 'Y'                                                           ");
            //query.AppendLine("                 and b.inp_number = a.inp_number                                                ");
            //query.AppendLine("                 and c.inp_number = b.inp_number                                                ");
            //query.AppendLine("                 and c.id = b.id                                                                ");
            //query.AppendLine("                 and c.value1 = :PUMP_FTR_IDN                                                   ");
            //query.AppendLine("              ) a                                                                               ");
            //query.AppendLine("             ,wh_energy b                                                                       ");
            //query.AppendLine("        where b.inp_number = a.inp_number                                                       ");
            //query.AppendLine("          and replace(upper(b.energy_statement), ' ', '') like '%'||a.id||'%'                   ");
            //query.AppendLine("      )                                                                                         ");
            //query.AppendLine(")                                                                                               ");
            //query.AppendLine("select a.pump_ftr_idn                                                                           ");
            //query.AppendLine("      ,b.curve_id                                                                               ");
            //query.AppendLine("      ,:MODEL_ID model_id                                                                       ");
            //query.AppendLine("      ,a.c_id                                                                                   ");
            //query.AppendLine("      ,a.e_id                                                                                   ");
            //query.AppendLine("      ,b.flow flow                                                                              ");
            //query.AppendLine("      ,b.h                                                                                      ");
            //query.AppendLine("      ,b.e                                                                                      ");
            //query.AppendLine("      ,b.point_yn                                                                               ");
            //query.AppendLine("  from tmp a                                                                                    ");
            //query.AppendLine("      ,we_curve_data b                                                                          ");
            //query.AppendLine(" where b.curve_id = :CURVE_ID                                                                   ");
            //query.AppendLine(" order by to_number(flow)                                                                       ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("MODEL_ID", OracleDbType.Varchar2),
            //         new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];
            //parameters[1].Value = parameter["PUMP_FTR_IDN"];
            //parameters[2].Value = parameter["PUMP_FTR_IDN"];
            //parameters[3].Value = parameter["MODEL_ID"];
            //parameters[4].Value = parameter["CURVE_ID"];


            //query.AppendLine("WITH TMP AS ");
            //query.AppendLine("( ");
            //query.AppendLine("SELECT (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y') INP_NUMBER ");
            //query.AppendLine("      ,:PUMP_FTR_IDN PUMP_FTR_IDN ");
            //query.AppendLine("      ,C_ID ");
            //query.AppendLine("      ,E_ID ");
            //query.AppendLine("  FROM ");
            //query.AppendLine("      ( ");
            //query.AppendLine("       SELECT REPLACE(REGEXP_REPLACE(B.PROPERTIES, '[^,]*[hH][eE][aA][dD]', ''),' ', '') C_ID ");
            //query.AppendLine("         FROM WH_TITLE A ");
            //query.AppendLine("             ,WH_PUMPS B ");
            //query.AppendLine("             ,WE_PUMP C ");
            //query.AppendLine("        WHERE A.ENERGY_GBN = 'Y' ");
            //query.AppendLine("          AND B.INP_NUMBER = A.INP_NUMBER ");
            //query.AppendLine("          AND C.ID = B.ID ");
            //query.AppendLine("          AND C.PUMP_ID = :PUMP_FTR_IDN ");
            //query.AppendLine("      ), ");
            //query.AppendLine("      ( ");
            //query.AppendLine("       SELECT REPLACE(REGEXP_REPLACE(B.ENERGY_STATEMENT, ");
            //query.AppendLine("                    '[^,]*[eE][fF][fF][iI][cC][iI][eE][nN][cC][yY]', ''),' ', '') E_ID ");
            //query.AppendLine("        FROM ( ");
            //query.AppendLine("              SELECT A.INP_NUMBER ");
            //query.AppendLine("                    ,'PUMP'||C.ID||'EFFICIENCY' ID ");
            //query.AppendLine("                FROM WH_TITLE A ");
            //query.AppendLine("                    ,WH_PUMPS B ");
            //query.AppendLine("                    ,WE_PUMP C ");
            //query.AppendLine("               WHERE A.ENERGY_GBN = 'Y' ");
            //query.AppendLine("                 AND B.INP_NUMBER = A.INP_NUMBER ");
            //query.AppendLine("                 AND C.ID = B.ID ");
            //query.AppendLine("                 AND C.PUMP_ID = :PUMP_FTR_IDN ");
            //query.AppendLine("              ) A ");
            //query.AppendLine("             ,WH_ENERGY B ");
            //query.AppendLine("        WHERE B.INP_NUMBER = A.INP_NUMBER ");
            //query.AppendLine("          AND REPLACE(UPPER(B.ENERGY_STATEMENT), ' ', '') LIKE '%'||A.ID||'%' ");
            //query.AppendLine("      ) ");
            //query.AppendLine(") ");
            //query.AppendLine("SELECT A.PUMP_FTR_IDN ");
            //query.AppendLine("      ,B.CURVE_ID ");
            //query.AppendLine("      ,:MODEL_ID MODEL_ID ");
            //query.AppendLine("      ,A.C_ID ");
            //query.AppendLine("      ,A.E_ID ");
            //query.AppendLine("      ,B.FLOW FLOW ");
            //query.AppendLine("      ,B.H ");
            //query.AppendLine("      ,B.E ");
            //query.AppendLine("      ,B.POINT_YN ");
            //query.AppendLine("  FROM TMP A ");
            //query.AppendLine("      ,WE_CURVE_DATA B ");
            //query.AppendLine(" WHERE B.CURVE_ID = :CURVE_ID ");
            //query.AppendLine(" ORDER BY TO_NUMBER(FLOW) ");

            query.AppendLine("WITH TMP AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y') INP_NUMBER ");
            query.AppendLine("      ,:PUMP_FTR_IDN PUMP_FTR_IDN ");
            query.AppendLine("      ,( ");
            query.AppendLine("       SELECT REPLACE(REGEXP_REPLACE(B.PROPERTIES, '[^,]*[hH][eE][aA][dD]', ''),' ', '') C_ID ");
            query.AppendLine("         FROM WH_TITLE A ");
            query.AppendLine("             ,WH_PUMPS B ");
            query.AppendLine("             ,WE_PUMP C ");
            query.AppendLine("        WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("          AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("          AND C.ID = B.ID ");
            query.AppendLine("          AND C.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("      ) C_ID ");
            query.AppendLine("      ,( ");
            query.AppendLine("       SELECT REPLACE(REGEXP_REPLACE(B.ENERGY_STATEMENT, ");
            query.AppendLine("                    '[^,]*[eE][fF][fF][iI][cC][iI][eE][nN][cC][yY]', ''),' ', '') E_ID ");
            query.AppendLine("        FROM ( ");
            query.AppendLine("              SELECT A.INP_NUMBER ");
            query.AppendLine("                    ,'PUMP'||C.ID||'EFFICIENCY' ID ");
            query.AppendLine("                FROM WH_TITLE A ");
            query.AppendLine("                    ,WH_PUMPS B ");
            query.AppendLine("                    ,WE_PUMP C ");
            query.AppendLine("               WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("                 AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("                 AND C.ID = B.ID ");
            query.AppendLine("                 AND C.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("              ) A ");
            query.AppendLine("             ,WH_ENERGY B ");
            query.AppendLine("        WHERE B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("          AND REPLACE(UPPER(B.ENERGY_STATEMENT), ' ', '') LIKE '%'||A.ID||'%' ");
            query.AppendLine("      ) E_ID");
            query.AppendLine("  FROM DUAL");
            query.AppendLine(") ");
            query.AppendLine("SELECT A.PUMP_FTR_IDN ");
            query.AppendLine("      ,B.CURVE_ID ");
            query.AppendLine("      ,:MODEL_ID MODEL_ID ");
            query.AppendLine("      ,A.C_ID ");
            query.AppendLine("      ,A.E_ID ");
            query.AppendLine("      ,B.FLOW FLOW ");
            query.AppendLine("      ,B.H ");
            query.AppendLine("      ,B.E ");
            query.AppendLine("      ,B.POINT_YN ");
            query.AppendLine("  FROM TMP A ");
            query.AppendLine("      ,WE_CURVE_DATA B ");
            query.AppendLine(" WHERE B.CURVE_ID = :CURVE_ID ");
            query.AppendLine(" ORDER BY TO_NUMBER(FLOW) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("MODEL_ID", OracleDbType.Varchar2),
                     new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];
            parameters[1].Value = parameter["PUMP_FTR_IDN"];
            parameters[2].Value = parameter["PUMP_FTR_IDN"];
            parameters[3].Value = parameter["MODEL_ID"];
            parameters[4].Value = parameter["CURVE_ID"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectModelCurve(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with tmp as                                                                                     ");
            //query.AppendLine("(                                                                                               ");
            //query.AppendLine("select (select inp_number from wh_title where energy_gbn = 'Y') inp_number                        ");
            //query.AppendLine("      ,:PUMP_FTR_IDN pump_ftr_idn                                                               ");
            //query.AppendLine("      ,c_id                                                                                     ");
            //query.AppendLine("      ,e_id                                                                                     ");
            //query.AppendLine("  from                                                                                          ");
            //query.AppendLine("      (                                                                                         ");
            //query.AppendLine("       select replace(regexp_replace(b.properties, '[^,]*[hH][eE][aA][dD]', ''),' ', '') c_id   ");
            //query.AppendLine("         from wh_title a                                                                        ");
            //query.AppendLine("             ,wh_pumps b                                                                        ");
            //query.AppendLine("             ,wh_tags c                                                                         ");
            //query.AppendLine("        where a.energy_gbn = 'Y'                                                                  ");
            //query.AppendLine("          and b.inp_number = a.inp_number                                                       ");
            //query.AppendLine("          and c.inp_number = b.inp_number                                                       ");
            //query.AppendLine("          and c.id = b.id                                                                       ");
            //query.AppendLine("          and c.value1 = :PUMP_FTR_IDN                                                          ");
            //query.AppendLine("      ),                                                                                        ");
            //query.AppendLine("      (                                                                                         ");
            //query.AppendLine("       select replace(regexp_replace(b.energy_statement,                                        ");
            //query.AppendLine("                    '[^,]*[eE][fF][fF][iI][cC][iI][eE][nN][cC][yY]', ''),' ', '') e_id          ");
            //query.AppendLine("        from (                                                                                  ");
            //query.AppendLine("              select a.inp_number                                                               ");
            //query.AppendLine("                    ,'PUMP'||c.id||'EFFICIENCY' id                                              ");
            //query.AppendLine("                from wh_title a                                                                 ");
            //query.AppendLine("                    ,wh_pumps b                                                                 ");
            //query.AppendLine("                    ,wh_tags c                                                                  ");
            //query.AppendLine("               where a.energy_gbn = 'Y'                                                           ");
            //query.AppendLine("                 and b.inp_number = a.inp_number                                                ");
            //query.AppendLine("                 and c.inp_number = b.inp_number                                                ");
            //query.AppendLine("                 and c.id = b.id                                                                ");
            //query.AppendLine("                 and c.value1 = :PUMP_FTR_IDN                                                   ");
            //query.AppendLine("              ) a                                                                               ");
            //query.AppendLine("             ,wh_energy b                                                                       ");
            //query.AppendLine("        where b.inp_number = a.inp_number                                                       ");
            //query.AppendLine("          and replace(upper(b.energy_statement), ' ', '') like '%'||a.id||'%'                   ");
            //query.AppendLine("      )                                                                                         ");
            //query.AppendLine(")                                                                                               ");
            //query.AppendLine("select a.pump_ftr_idn                                                                           ");
            //query.AppendLine("      ,a.c_id                                                                                   ");
            //query.AppendLine("      ,a.e_id                                                                                   ");
            //query.AppendLine("      ,b.x flow                                                                                 ");
            //query.AppendLine("      ,max(decode(b.id, a.c_id, b.y, null)) h                                                   ");
            //query.AppendLine("      ,max(decode(b.id, a.e_id, b.y, null)) e                                                   ");
            //query.AppendLine("      ,:MODEL_ID model_id                                                                       ");
            //query.AppendLine("  from tmp a                                                                                    ");
            //query.AppendLine("      ,wh_curves b                                                                              ");
            //query.AppendLine(" where b.inp_number = a.inp_number                                                              ");
            //query.AppendLine("   and b.id in (a.c_id, a.e_id)                                                                 ");
            //query.AppendLine(" group by a.pump_ftr_idn, a.c_id, a.e_id,  b.x                                                  ");
            //query.AppendLine(" order by to_number(flow)                                                                       ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("MODEL_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];
            //parameters[1].Value = parameter["PUMP_FTR_IDN"];
            //parameters[2].Value = parameter["PUMP_FTR_IDN"];
            //parameters[3].Value = parameter["MODEL_ID"];

            query.AppendLine("WITH TMP AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y') INP_NUMBER ");
            query.AppendLine("      ,:PUMP_FTR_IDN PUMP_FTR_IDN ");
            query.AppendLine("      ,( ");
            query.AppendLine("       SELECT REPLACE(REGEXP_REPLACE(B.PROPERTIES, '[^,]*[hH][eE][aA][dD]', ''),' ', '') C_ID ");
            query.AppendLine("         FROM WH_TITLE A ");
            query.AppendLine("             ,WH_PUMPS B ");
            query.AppendLine("             ,WE_PUMP C ");
            query.AppendLine("        WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("          AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("          AND C.ID = B.ID ");
            query.AppendLine("          AND C.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("      ) C_ID");
            query.AppendLine("      ,( ");
            query.AppendLine("       SELECT REPLACE(REGEXP_REPLACE(B.ENERGY_STATEMENT, ");
            query.AppendLine("                    '[^,]*[eE][fF][fF][iI][cC][iI][eE][nN][cC][yY]', ''),' ', '') E_ID ");
            query.AppendLine("        FROM ( ");
            query.AppendLine("              SELECT A.INP_NUMBER ");
            query.AppendLine("                    ,'PUMP'||C.ID||'EFFICIENCY' ID ");
            query.AppendLine("                FROM WH_TITLE A ");
            query.AppendLine("                    ,WH_PUMPS B ");
            query.AppendLine("                    ,WE_PUMP C ");
            query.AppendLine("               WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("                 AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("                 AND C.ID = B.ID ");
            query.AppendLine("                 AND C.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("              ) A ");
            query.AppendLine("             ,WH_ENERGY B ");
            query.AppendLine("        WHERE B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("          AND REPLACE(UPPER(B.ENERGY_STATEMENT), ' ', '') LIKE '%'||A.ID||'%' ");
            query.AppendLine("      ) E_ID");
            query.AppendLine("  FROM DUAL ");
            query.AppendLine(") ");
            query.AppendLine("SELECT A.PUMP_FTR_IDN ");
            query.AppendLine("      ,A.C_ID ");
            query.AppendLine("      ,A.E_ID ");
            query.AppendLine("      ,B.X FLOW ");
            query.AppendLine("      ,MAX(DECODE(B.ID, A.C_ID, B.Y, NULL)) H ");
            query.AppendLine("      ,MAX(DECODE(B.ID, A.E_ID, B.Y, NULL)) E ");
            query.AppendLine("      ,:MODEL_ID MODEL_ID ");
            query.AppendLine("  FROM TMP A ");
            query.AppendLine("      ,WH_CURVES B ");
            query.AppendLine(" WHERE B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("   AND B.ID IN (A.C_ID, A.E_ID) ");
            query.AppendLine(" GROUP BY ");
            query.AppendLine("       A.PUMP_FTR_IDN ");
            query.AppendLine("	    ,A.C_ID ");
            query.AppendLine("	    ,A.E_ID ");
            query.AppendLine("	    ,B.X ");
            query.AppendLine(" ORDER BY TO_NUMBER(FLOW) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("MODEL_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];
            parameters[1].Value = parameter["PUMP_FTR_IDN"];
            parameters[2].Value = parameter["PUMP_FTR_IDN"];
            parameters[3].Value = parameter["MODEL_ID"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectPumpInfo(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select a.pump_ftr_idn                              ");
            //query.AppendLine("      ,a.id model_id 								 ");
            //query.AppendLine("      ,b.pump_gbn									 ");
            //query.AppendLine("      ,to_number(b.pump_rpm) pump_rpm				 ");
            //query.AppendLine("      ,a.remark remark							 ");
            //query.AppendLine(" from (											 ");
            //query.AppendLine("       select c.value1 pump_ftr_idn				 ");
            //query.AppendLine("             ,null remark						 ");
            //query.AppendLine("             ,b.id								 ");
            //query.AppendLine("        from wh_title a							 ");
            //query.AppendLine("            ,wh_pumps b							 ");
            //query.AppendLine("            ,wh_tags c							 ");
            //query.AppendLine("       where a.energy_gbn = 'Y'						 ");
            //query.AppendLine("         and b.inp_number = a.inp_number			 ");
            //query.AppendLine("         and c.inp_number = b.inp_number			 ");
            //query.AppendLine("         and c.id = b.id							 ");
            //query.AppendLine("     ) a											 ");
            //query.AppendLine("      ,we_pump b									 ");
            //query.AppendLine("      ,we_pump_station c							 ");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn			 ");
            //query.AppendLine("   and c.pumpstation_id(+) = b.pumpstation_id		 ");
            //query.AppendLine("   and b.pump_ftr_idn = :PUMP_FTR_IDN				 ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];

            query.AppendLine("SELECT A.PUMP_ID PUMP_FTR_IDN ");
            query.AppendLine("      ,A.ID MODEL_ID ");
            query.AppendLine("      ,A.PUMP_GBN ");
            query.AppendLine("      ,TO_NUMBER(A.PUMP_RPM) PUMP_RPM ");
            query.AppendLine("      ,A.REMARK REMARK ");
            query.AppendLine("  FROM WE_PUMP A ");
            query.AppendLine("      ,WE_PUMP_STATION B ");
            query.AppendLine(" WHERE B.PUMPSTATION_ID(+) = A.PUMPSTATION_ID ");
            query.AppendLine("   AND A.PUMP_ID = :PUMP_FTR_IDN ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectPumpMaster(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select to_number(c.pumpstation_id) pumpstation_id                                      ");
            //query.AppendLine("     ,a.pump_ftr_idn                                                                   ");
            //query.AppendLine("     ,nvl(b.remark, a.remark) remark                                                   ");
            //query.AppendLine("     ,b.pump_gbn                                                                       ");
            //query.AppendLine("     ,to_number(b.pump_rpm) pump_rpm                                                   ");
            //query.AppendLine("     ,b.sim_gbn                                                                        ");
            //query.AppendLine("     ,decode(b.curve_id, null, null,                                                   ");
            //query.AppendLine("       (select to_char(timestamp,'yyyy-mm-dd') from we_curve_master where curve_id = b.curve_id)) timestamp  ");
            //query.AppendLine("     ,b.curve_id curve_id                                                              ");
            //query.AppendLine("     ,to_number(nvl(b.diff,10)) diff                                                   ");
            //query.AppendLine("     ,nvl(b.model_yn, 'N') model_yn                                                    ");
            //query.AppendLine("  from (                                                                               ");
            //query.AppendLine("     select c.value1 pump_ftr_idn                                                      ");
            //query.AppendLine("           ,null remark                                                            ");
            //query.AppendLine("       from wh_title a                                                                 ");
            //query.AppendLine("           ,wh_pumps b                                                                 ");
            //query.AppendLine("           ,wh_tags c                                                                  ");
            //query.AppendLine("      where a.energy_gbn = 'Y'                                                           ");
            //query.AppendLine("        and b.inp_number = a.inp_number                                                ");
            //query.AppendLine("        and c.inp_number = b.inp_number                                                ");
            //query.AppendLine("        and c.id = b.id                                                                ");
            //query.AppendLine("     ) a                                                                               ");
            //query.AppendLine("    ,we_pump b                                                                         ");
            //query.AppendLine("    ,we_pump_station c                                                                 ");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn                                              ");
            //query.AppendLine("   and c.pumpstation_id(+) = b.pumpstation_id                                          ");
            //query.AppendLine("   and b.pumpstation_id = nvl(:BZS, b.pumpstation_id)                                  ");
            //query.AppendLine("order by to_number(c.pumpstation_id), to_number(a.pump_ftr_idn)                        ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("BZS", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["BZS"];

            query.AppendLine("SELECT TO_NUMBER(B.PUMPSTATION_ID) PUMPSTATION_ID ");
            query.AppendLine("      ,A.PUMP_ID PUMP_FTR_IDN ");
            query.AppendLine("      ,A.REMARK ");
            query.AppendLine("      ,A.PUMP_GBN ");
            query.AppendLine("      ,TO_NUMBER(A.PUMP_RPM) PUMP_RPM ");
            query.AppendLine("      ,A.SIM_GBN ");
            query.AppendLine("      ,DECODE(A.CURVE_ID, NULL, NULL, ");
            query.AppendLine("        (SELECT TO_CHAR(TIMESTAMP,'YYYY-MM-DD') ");
            query.AppendLine("		   FROM WE_CURVE_MASTER  ");
            query.AppendLine("		  WHERE CURVE_ID = A.CURVE_ID) ");
            query.AppendLine("	      ) TIMESTAMP ");
            query.AppendLine("      ,A.CURVE_ID CURVE_ID ");
            query.AppendLine("      ,TO_NUMBER(NVL(A.DIFF,10)) DIFF ");
            query.AppendLine("      ,NVL(A.MODEL_YN, 'N') MODEL_YN ");
            query.AppendLine("  FROM WE_PUMP A ");
            query.AppendLine("      ,WE_PUMP_STATION B ");
            query.AppendLine(" WHERE B.PUMPSTATION_ID(+) = A.PUMPSTATION_ID ");
            query.AppendLine("   AND A.PUMPSTATION_ID = NVL(:BZS, A.PUMPSTATION_ID) ");
            query.AppendLine(" ORDER BY TO_NUMBER(B.PUMPSTATION_ID), TO_NUMBER(A.PUMP_ID) ");

            IDataParameter[] parameters =  {
                     new OracleParameter("BZS", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["BZS"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectPumpPerformanceCurveManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select to_number(curve_id) curve_id                                                     ");
            //query.AppendLine("      ,pump_ftr_idn                                                                     ");
            //query.AppendLine("      ,timestamp                                                                        ");
            //query.AppendLine("      ,remark                                                                           ");
            //query.AppendLine("  from we_curve_master                                                                  ");
            //query.AppendLine(" where 1 = 1                                                                            ");
            //query.AppendLine("   and timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')");
            //query.AppendLine(" order by curve_id, timestamp                                                           ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["STARTDATE"];
            //parameters[1].Value = parameter["ENDDATE"];

            query.AppendLine("SELECT TO_NUMBER(CURVE_ID) CURVE_ID ");
            query.AppendLine("      ,PUMP_FTR_IDN ");
            query.AppendLine("      ,TIMESTAMP ");
            query.AppendLine("      ,REMARK ");
            query.AppendLine("  FROM WE_CURVE_MASTER ");
            query.AppendLine(" WHERE 1 = 1 ");
            query.AppendLine("   AND TIMESTAMP BETWEEN TO_DATE(:STARTDATE,'YYYYMMDD') ");
            query.AppendLine("                     AND TO_DATE(:ENDDATE,'YYYYMMDD') ");
            query.AppendLine(" ORDER BY CURVE_ID ");
            query.AppendLine("      ,TIMESTAMP ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectPumpPerformanceCurveManageDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select curve_id                                                              ");
            //query.AppendLine("      ,to_number(flow) flow                                                  ");
            //query.AppendLine("      ,to_number(h) h                                                        ");
            //query.AppendLine("      ,to_number(e) e                                                        ");
            //query.AppendLine("      ,point_yn                                                              ");
            //query.AppendLine("  from we_curve_data                                                         ");
            //query.AppendLine(" where curve_id = :CURVE_ID                                                  ");
            //query.AppendLine(" order by to_number(flow)                                                    ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["CURVE_ID"];

            query.AppendLine("SELECT CURVE_ID ");
            query.AppendLine("      ,TO_NUMBER(FLOW) FLOW ");
            query.AppendLine("      ,TO_NUMBER(H) H ");
            query.AppendLine("      ,TO_NUMBER(E) E ");
            query.AppendLine("      ,POINT_YN ");
            query.AppendLine("  FROM WE_CURVE_DATA ");
            query.AppendLine(" WHERE CURVE_ID = :CURVE_ID ");
            query.AppendLine(" ORDER BY TO_NUMBER(FLOW) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["CURVE_ID"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public object SelectPumpPerformanceCurveManageSEQ(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select nvl(id,0)+1                                                          ");
            //query.AppendLine("  from (select max(to_number(curve_id)) id from we_curve_master)            ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("GUBUN", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["GUBUN"];

            query.AppendLine("SELECT NVL(ID,0) + 1 ");
            query.AppendLine("  FROM (SELECT MAX(TO_NUMBER(CURVE_ID)) ID FROM WE_CURVE_MASTER) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("GUBUN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["GUBUN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public void InsertPumpPerformanceCurveManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("insert into we_curve_master (curve_id, timestamp)                                    ");
            //query.AppendLine("                  values (:CURVE_ID, to_date(to_char(sysdate,'yyyymmdd'),'yyyymmdd'))");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["CURVE_ID"];

            query.AppendLine("INSERT INTO WE_CURVE_MASTER (CURVE_ID, TIMESTAMP) ");
            query.AppendLine("     VALUES (:CURVE_ID, TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD'),'YYYYMMDD')) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["CURVE_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void InsertPumpPerformanceCurveManageDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("insert into we_curve_data   (curve_id, flow, h, e, point_yn)                         ");
            //query.AppendLine("                  values (:CURVE_ID, :FLOW, :H, :E, :POINT_YN)                       ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("CURVE_ID", OracleDbType.Varchar2),
            //         new OracleParameter("FLOW", OracleDbType.Varchar2),
            //         new OracleParameter("H", OracleDbType.Varchar2),
            //         new OracleParameter("E", OracleDbType.Varchar2),
            //         new OracleParameter("POINT_YN", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["CURVE_ID"];
            //parameters[1].Value = parameter["FLOW"];
            //parameters[2].Value = parameter["H"];
            //parameters[3].Value = parameter["E"];
            //parameters[4].Value = parameter["POINT_YN"];

            query.AppendLine("INSERT INTO WE_CURVE_DATA   (CURVE_ID, FLOW, H, E, POINT_YN) ");
            query.AppendLine("     VALUES (:CURVE_ID, :FLOW, :H, :E, :POINT_YN) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("CURVE_ID", OracleDbType.Varchar2),
                     new OracleParameter("FLOW", OracleDbType.Varchar2),
                     new OracleParameter("H", OracleDbType.Varchar2),
                     new OracleParameter("E", OracleDbType.Varchar2),
                     new OracleParameter("POINT_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["CURVE_ID"];
            parameters[1].Value = parameter["FLOW"];
            parameters[2].Value = parameter["H"];
            parameters[3].Value = parameter["E"];
            parameters[4].Value = parameter["POINT_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void UpdatePumpPerformanceCurveManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("update we_curve_master                                             ");
            //query.AppendLine("                   set pump_ftr_idn = :PUMP_FTR_IDN                ");
            //query.AppendLine("                      ,timestamp = to_date(:TIMESTAMP,'yyyyMMdd')  ");
            //query.AppendLine("                      ,remark = :REMARK                            ");
            //query.AppendLine("                 where curve_id = :CURVE_ID                        ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
            //         new OracleParameter("REMARK", OracleDbType.Varchar2),
            //         new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];
            //parameters[1].Value = parameter["TIMESTAMP"];
            //parameters[2].Value = parameter["REMARK"];
            //parameters[3].Value = parameter["CURVE_ID"];

            query.AppendLine("UPDATE WE_CURVE_MASTER ");
            query.AppendLine("   SET PUMP_FTR_IDN = :PUMP_FTR_IDN ");
            query.AppendLine("      ,TIMESTAMP = TO_DATE(:TIMESTAMP,'YYYYMMDD') ");
            query.AppendLine("      ,REMARK = :REMARK ");
            query.AppendLine(" WHERE CURVE_ID = :CURVE_ID ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
                     new OracleParameter("REMARK", OracleDbType.Varchar2),
                     new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["REMARK"];
            parameters[3].Value = parameter["CURVE_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeletePumpPerformanceCurveManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("delete from we_curve_master                                  ");
            //query.AppendLine("      where curve_id = :CURVE_ID                             ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["CURVE_ID"];

            query.AppendLine("DELETE ");
            query.AppendLine("  FROM WE_CURVE_MASTER ");
            query.AppendLine(" WHERE CURVE_ID = :CURVE_ID ");

            IDataParameter[] parameters =  {
                     new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
                };


            parameters[0].Value = parameter["CURVE_ID"];
            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeletePumpPerformanceCurveManageDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("delete from we_curve_data                                    ");
            //query.AppendLine("      where curve_id = :CURVE_ID                             ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["CURVE_ID"];

            query.AppendLine("DELETE ");
            query.AppendLine("  FROM WE_CURVE_DATA ");
            query.AppendLine(" WHERE CURVE_ID = :CURVE_ID ");

            IDataParameter[] parameters =  {
                     new OracleParameter("CURVE_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["CURVE_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
