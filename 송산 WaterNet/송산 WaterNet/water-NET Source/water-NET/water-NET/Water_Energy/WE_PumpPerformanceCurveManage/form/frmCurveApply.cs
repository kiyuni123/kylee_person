﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using WaterNet.WE_PumpPerformanceCurveManage.work;

namespace WaterNet.WE_PumpPerformanceCurveManage.form
{
    public partial class frmCurveApply : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        public frmCurveApply()
        {
            InitializeComponent();
            Load += new EventHandler(frmCurveApply_Load);
        }
        private Hashtable pumpInfo = null;
        public frmCurveApply(Hashtable pumpInfo)
        {
            this.pumpInfo = pumpInfo;
            InitializeComponent();
            Load += new EventHandler(frmCurveApply_Load);
        }

        private void frmCurveApply_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeForm();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            if (this.pumpInfo != null)
            {
                this.SelectCurveApply();
            }
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);
            this.gridManager.Add(this.ultraGrid3);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid3.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            //그리드1
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MODEL_ID";
            column.Header.Caption = "모델ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_GBN";
            column.Header.Caption = "펌프구분";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_RPM";
            column.Header.Caption = "회전수(rpm)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "펌프설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 350;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            //그리드2
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "C_ID";
            column.Header.Caption = "모델커브곡선ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;
            column.Width = 100;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MODEL_ID";
            column.Header.Caption = "모델ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "E_ID";
            column.Header.Caption = "모델효율ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;
            column.Width = 100;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "###,###,##0";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "E";
            column.Header.Caption = "효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";
            column.SortIndicator = SortIndicator.Disabled;

            //그리드3
            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MODEL_ID";
            column.Header.Caption = "모델ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "C_ID";
            column.Header.Caption = "모델커브곡선ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;
            column.Width = 100;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "E_ID";
            column.Header.Caption = "모델효율ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;
            column.Width = 100;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "###,###,##0";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "E";
            column.Header.Caption = "효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "POINT_YN";
            column.Header.Caption = "정격점";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.ForeColor = Color.Red;
            column.Width = 120;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;
            #endregion

            this.gridManager.DefaultColumnsMapping(this.ultraGrid1);
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //펌프구분
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMP_GBN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMP_GBN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "PKP");
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMP_GBN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMP_GBN"];
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.applyBtn.Click += new EventHandler(applyBtn_Click);
        }

        private void applyBtn_Click(object sender, EventArgs e)
        {
            if (!this.IsValidate())
            {
                MessageBox.Show("현재 성능곡선 자료가 유효하지 않습니다.\n유량은 내림차순으로 양정은 오름차순으로 다른값을 입력해주세요.");
                return;
            }

            this.ultraGrid2.DataSource = 
                Utils.ColumnMatch(this.ultraGrid2, ((DataTable)this.ultraGrid3.DataSource).Copy());

            this.UpdateCurveApply();
        }

        private void UpdateCurveApply()
        {
            PumpPerformanceCurveManageWork.GetInstance().UpdateModelCurve(this.ultraGrid2);
        }

        private bool IsValidate()
        {
            bool resutl = true;

            if (this.ultraGrid3.DataSource == null)
            {
                resutl = false;
                return resutl;
            }

            if (this.ultraGrid3.Rows.Count == 0)
            {
                resutl = false;
                return resutl;
            }

            DataTable table = (DataTable)this.ultraGrid3.DataSource;

            for (int i = 1; i < table.Rows.Count - 1; i++)
            {
                if (Convert.ToDouble(table.Rows[i - 1]["FLOW"]) >= Convert.ToDouble(table.Rows[i]["FLOW"]))
                {
                    resutl = false;
                    break;
                }
                if (Convert.ToDouble(table.Rows[i - 1]["H"]) <= Convert.ToDouble(table.Rows[i]["H"]))
                {
                    resutl = false;
                    break;
                }
            }

            return resutl;
        }

        private void SelectCurveApply()
        {
            this.Cursor = Cursors.WaitCursor;

            //grid1 (펌프기본정보) PUMP_FTR_IDN, MODEL_ID, PUMP_GBN, PUMP_RPM, REMARK
            this.ultraGrid1.DataSource = PumpPerformanceCurveManageWork.GetInstance().SelectPumpInfo(this.pumpInfo);

            //grid2  (모델성능진단자료) PUMP_FTR_IDN, MODEL_ID, C_ID, E_ID, FLOW, H, E
            this.ultraGrid2.DataSource = PumpPerformanceCurveManageWork.GetInstance().SelectModelCurve(this.pumpInfo);

            //grid3 (현재성능진단자료) PUMP_FTR_IDN, CURVE_ID, MODEL_ID, C_ID, E_ID, FLOW, H, E, POINT_YN
            this.ultraGrid3.DataSource = PumpPerformanceCurveManageWork.GetInstance().SelectCurrentCurve(this.pumpInfo);

            this.Cursor = Cursors.Default;
        }
    }
}
