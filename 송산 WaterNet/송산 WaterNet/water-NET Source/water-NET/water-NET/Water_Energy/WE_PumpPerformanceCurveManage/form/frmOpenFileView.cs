﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;

namespace WaterNet.WE_PumpPerformanceCurveManage.form
{
    public partial class frmOpenFileView : Form
    {
        public frmOpenFileView()
        {
            InitializeComponent();
        }

        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        private DataSet dataSet = null;
        private string curve_id = null;

        public frmOpenFileView(DataSet dataSet, string curve_id)
        {
            this.dataSet = dataSet;
            this.curve_id = curve_id;
            InitializeComponent();
            this.Load += new EventHandler(frmOpenFileView_Load);
        }

        private void frmOpenFileView_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeValueList();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.comboBox1.SelectedIndexChanged += new EventHandler(comboBox1_SelectedIndexChanged);
            this.button1.Click += new EventHandler(button1_Click);
        }

        //데이터 검증 및 형식에 맞게 변환하고 결과값 리턴.
        private void button1_Click(object sender, EventArgs e)
        {
            DataTable selectedSheet = this.ultraGrid1.DataSource as DataTable;

            if (selectedSheet == null)
            {
                return;
            }

            DataTable result_table = new DataTable();
            result_table.Columns.Add("CURVE_ID");
            result_table.Columns.Add("FLOW", typeof(double));
            result_table.Columns.Add("H", typeof(double));
            result_table.Columns.Add("E", typeof(double));
            result_table.Columns.Add("POINT_YN");
            
            //선택된 컬럼의 값이 숫자형인지 체크한다.
            foreach (DataRow row in selectedSheet.Rows)
            {
                double flow = 0;
                double h = 0;
                double ei = 0;
                
                bool isNumber = true;

                if (!Double.TryParse(row[this.comboBox2.SelectedValue.ToString()].ToString(), out flow))
                {
                    isNumber = false;
                }
                if (!Double.TryParse(row[this.comboBox3.SelectedValue.ToString()].ToString(), out h))
                {
                    isNumber = false;
                }
                if (!Double.TryParse(row[this.comboBox4.SelectedValue.ToString()].ToString(), out ei))
                {
                    isNumber = false;
                }

                if (!isNumber)
                {
                    MessageBox.Show("숫자형식의 데이터만 선택이 가능합니다.");
                    return;
                }
                else
                {
                    result_table.Rows.Add(curve_id, flow, h, ei, string.Empty);
                }
            }

            frmMain owner = this.Owner as frmMain;
            
            owner.Curves = result_table;
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable selectedSheet = null;

            foreach (DataTable table in this.dataSet.Tables)
            {
                if (this.comboBox1.SelectedItem.ToString() == table.TableName)
                {
                    selectedSheet = table;
                    break;
                }
            }

            if (selectedSheet == null)
            {
                MessageBox.Show("엑셀파일을 읽는중 오류가 발생했습니다.\n파일을 다시 선택해주세요.");
                this.Close();
            }

            DataTable codeList = new DataTable();
            codeList.Columns.Add("CODE");
            codeList.Columns.Add("CODE_NAME");

            foreach (DataColumn column in selectedSheet.Columns)
            {
                codeList.Rows.Add(column.ColumnName.ToString(), column.ColumnName.ToString());
            }

            this.comboBox2.DataSource = codeList.Copy();
            this.comboBox3.DataSource = codeList.Copy();
            this.comboBox4.DataSource = codeList.Copy();

            if (this.comboBox2.Items.Count > 2)
            {
                this.comboBox2.SelectedIndex = 0;
            }
            if (this.comboBox3.Items.Count > 2)
            {
                this.comboBox3.SelectedIndex = 1;
            }
            if (this.comboBox4.Items.Count > 2)
            {
                this.comboBox4.SelectedIndex = 2;
            }

            DataTable result_table = new DataTable();
            result_table.Columns.Add(this.comboBox2.SelectedValue.ToString(), typeof(double));
            result_table.Columns.Add(this.comboBox3.SelectedValue.ToString(), typeof(double));
            result_table.Columns.Add(this.comboBox4.SelectedValue.ToString(), typeof(double));


            this.ultraGrid1.DataSource = selectedSheet;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeValueList()
        {
            this.comboBox2.ValueMember = "CODE";
            this.comboBox2.DisplayMember = "CODE_NAME";

            this.comboBox3.ValueMember = "CODE";
            this.comboBox3.DisplayMember = "CODE_NAME";

            this.comboBox4.ValueMember = "CODE";
            this.comboBox4.DisplayMember = "CODE_NAME";

            foreach (DataTable table in this.dataSet.Tables)
            {
                this.comboBox1.Items.Add(table.TableName);
            }
            this.comboBox1.SelectedIndex = 0;
        }
    }
}
