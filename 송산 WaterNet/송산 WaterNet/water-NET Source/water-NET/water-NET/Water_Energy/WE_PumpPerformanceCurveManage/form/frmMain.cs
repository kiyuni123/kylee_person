﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using WaterNet.WE_PumpPerformanceCurveManage.work;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.interface1;
using System.IO;
using System.Data.OleDb;
using EMFrame.log;

namespace WaterNet.WE_PumpPerformanceCurveManage.form
{
    public partial class frmMain : Form
    {
        //MainMap 을 제어하기 위한 인스턴스 변수
        //private IMapControl mainMap = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        //public IMapControl MainMap
        //{
        //    set
        //    {
        //        this.mainMap = value;
        //    }
        //}

        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        private UltraGridCell cell = null;
        public frmMain(UltraGridCell cell)
        {
            InitializeComponent();
            this.cell = cell;
            this.selectBtn.Visible = true;
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //동진_2012.08.23
            object o = EMFrame.statics.AppStatic.USER_MENU["성능진단자료관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.insertBtn.Enabled = false;
                this.insert2Btn.Enabled = false;

                this.deleteBtn.Enabled = false;
                this.delete2Btn.Enabled = false;

                this.updateBtn.Enabled = false;
                this.update2Btn.Enabled = false;
            }

            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.startDate.Value = DateTime.Now.AddYears(-1).ToString("yyyy-01-01");
            this.endDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            //그리드1
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SELECTED";
            column.Header.Caption = "";
            column.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            column.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.None;
            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;

            column.SortIndicator = SortIndicator.Disabled;
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 30;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.Width = 120;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "성능진단일자";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "설명";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 350;
            column.Hidden = false;

            //그리드2
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량(㎥/h)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "###,###,##0";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정(m)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "E";
            column.Header.Caption = "효율(%)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.Format = "d";
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "POINT_YN";
            column.Header.Caption = "정격점";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.ForeColor = Color.Red;
            column.Width = 120;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;
            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.modelBtn.Click += new EventHandler(modelBtn_Click);
            this.selectBtn.Click += new EventHandler(selectBtn_Click);

            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);

            this.insert2Btn.Click += new EventHandler(insert2Btn_Click);
            this.delete2Btn.Click += new EventHandler(delete2Btn_Click);
            this.update2Btn.Click += new EventHandler(update2Btn_Click);

            this.ultraGrid1.ClickCellButton += new CellEventHandler(ultraGrid1_ClickCellButton);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid2.ClickCell += new ClickCellEventHandler(ultraGrid2_ClickCell);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid1.AfterHeaderCheckStateChanged += new AfterHeaderCheckStateChangedEventHandler(ultraGrid1_AfterHeaderCheckStateChanged);

            this.importBtn.Click += new EventHandler(importBtn_Click);
        }

        private void modelBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                MessageBox.Show("성능진단자료를 선택해주세요.");
                return;
            }

            frmCurveApply form = new frmCurveApply(Utils.ConverToHashtable(this.ultraGrid1.ActiveRow));
            form.Owner = this;
            form.ShowDialog();
        }

        private void ultraGrid1_ClickCellButton(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key != "PUMP_FTR_IDN")
            {
                return;
            }

            frmPump form = new frmPump(e.Cell);
            //form.MainMap = this.mainMap;
            form.Owner = this;
            form.ShowDialog();
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                MessageBox.Show("성능진단목록을 선택해주세요.");
                return;
            }

            this.cell.Value = this.ultraGrid1.ActiveRow.Cells["CURVE_ID"].Value;
            this.Close();
        }

        private Hashtable parameter = null;
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = Utils.ConverToHashtable(this.groupBox1);
                this.SelectPumpPerformanceCurveManage();
                this.gridManager.DefaultColumnsMapping2(this.ultraGrid2);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectPumpPerformanceCurveManage()
        {
            this.Cursor = Cursors.WaitCursor;
            if (this.parameter == null)
            {
                this.parameter = Utils.ConverToHashtable(this.groupBox1);
            }

            this.ultraGrid1.DataSource =
            PumpPerformanceCurveManageWork.GetInstance().SelectPumpPerformanceCurveManage(this.parameter);
            this.Cursor = Cursors.Default;
        }

        private void insertBtn_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Hashtable parameter = new Hashtable();
            PumpPerformanceCurveManageWork.GetInstance().InsertPumpPerformanceCurveManage(parameter);
            this.SelectPumpPerformanceCurveManage();
            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (gridRow.Cells["CURVE_ID"].Value.ToString() == parameter["CURVE_ID"].ToString())
                {
                    gridRow.Activated = true;
                }
            }
            this.Cursor = Cursors.Default;
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult qe = MessageBox.Show("선택항목을 삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    PumpPerformanceCurveManageWork.GetInstance().DeletePumpPerformanceCurveManage(this.ultraGrid1);
                    this.SelectPumpPerformanceCurveManage();
                    this.gridManager.DefaultColumnsMapping2(this.ultraGrid2);
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult qe = MessageBox.Show("선택항목을 저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    PumpPerformanceCurveManageWork.GetInstance().UpdatePumpPerformanceCurveManage(this.ultraGrid1);
                    this.SelectPumpPerformanceCurveManage();
                    this.gridManager.DefaultColumnsMapping2(this.ultraGrid2);
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void insert2Btn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                return;
            } 

            UltraGridRow gridRow = this.ultraGrid2.DisplayLayout.Bands[0].AddNew();
            gridRow.Cells["CURVE_ID"].Value = this.ultraGrid1.ActiveRow.Cells["CURVE_ID"].Value;
        }

        private void delete2Btn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                return;
            }

            DialogResult qe = MessageBox.Show("선택항목을 삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                this.ultraGrid2.ActiveRow.Delete(false);
            }
        }
        private void update2Btn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                this.Cursor = Cursors.WaitCursor;
                Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid1.ActiveRow);
                PumpPerformanceCurveManageWork.GetInstance().UpdatePumpPerformanceCurveManageDetail(this.ultraGrid2, parameter);
                this.Cursor = Cursors.Default;
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
        }

        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            this.SelectPumpPerformanceCurveManageDetail();
        }

        private void SelectPumpPerformanceCurveManageDetail()
        {
            this.ultraGrid2.DataSource =
                PumpPerformanceCurveManageWork.GetInstance().SelectPumpPerformanceCurveManageDetail(Utils.ConverToHashtable(this.ultraGrid1.ActiveRow));
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["SELECTED"].SetHeaderCheckedState(e.Layout.Rows, false);
        }

        //체크박스 전체 선택 / 전체 해제
        private void ultraGrid1_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;

            bool result = false;

            if (e.Column.GetHeaderCheckedState(e.Rows) == CheckState.Checked)
            {
                result = true;
            }

            foreach (UltraGridRow row in ultraGrid.Rows)
            {
                if (!row.Hidden)
                {
                    row.Cells["SELECTED"].Value = result;
                }
            }
            ultraGrid.UpdateData();
        }

        private void ultraGrid2_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (e.Cell.Column.Key != "POINT_YN")
            {
                return;
            }

            foreach (UltraGridRow gridRow in this.ultraGrid2.Rows)
            {
                gridRow.Cells["POINT_YN"].Value = string.Empty;

                if (gridRow.Cells["POINT_YN"].Equals(e.Cell))
                {
                    gridRow.Cells["POINT_YN"].Value = "O";
                }
            }
        }


        public DataTable Curves
        {
            set
            {
                this.ultraGrid2.DataSource = value;
            }
        }

        // 확장명 XLS (Excel 97~2003 용)
        private const string ConnectStrFrm_Excel97_2003 =
            "Provider=Microsoft.Jet.OLEDB.4.0;" +
            "Data Source=\"{0}\";" +
            "Mode=ReadWrite|Share Deny None;" +
            "Extended Properties='Excel 8.0; HDR={1}; IMEX={2}';" +
            "Persist Security Info=False";

        // 확장명 XLSX (Excel 2007 이상용)
        private const string ConnectStrFrm_Excel =
            "Provider=Microsoft.ACE.OLEDB.12.0;" +
            "Data Source=\"{0}\";" +
            "Mode=ReadWrite|Share Deny None;" +
            "Extended Properties='Excel 12.0; HDR={1}; IMEX={2}';" +
            "Persist Security Info=False";

        private void importBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                return;
            }

            string curve_id = this.ultraGrid1.ActiveRow.Cells["CURVE_ID"].ToString();
            int iFileType = -1;

            OpenFileDialog openfile = new OpenFileDialog();

            openfile.Filter = "엑셀 파일|*.xls;*.xlsx";
            openfile.Multiselect = false;
            openfile.FilterIndex = 1;
            openfile.RestoreDirectory = true;

            if (openfile.ShowDialog() == DialogResult.OK)
            {
                iFileType = this.ExcelFileType(openfile.FileName);
                if (iFileType == 0 || iFileType == 1)
                {
                    DataSet dataSet = this.OpenExcel(openfile.FileName, false);

                    if (dataSet != null)
                    {
                        frmOpenFileView viewr = new frmOpenFileView(dataSet, curve_id);
                        viewr.ShowDialog(this);
                    }
                    else
                    {
                        MessageBox.Show("엑셀파일을 읽는중 오류가 발생했습니다.");
                    }
                }
            }
        }

        #region Excel Function

        /// <summary>
        ///    Excel 파일의 형태를 반환한다.
        ///    -2 : Error  
        ///    -1 : 엑셀파일아님
        ///     0 : 97-2003 엑셀 파일 (xls)
        ///     1 : 2007 이상 파일 (xlsx)
        /// </summary>
        /// <param name="XlsFile">
        ///    Excel File 명 전체 경로입니다.
        /// </param>
        private int ExcelFileType(string XlsFile)
        {
            byte[,] ExcelHeader = {
                { 0xD0, 0xCF, 0x11, 0xE0, 0xA1 }, // XLS  File Header
                { 0x50, 0x4B, 0x03, 0x04, 0x14 }  // XLSX File Header
            };

            // result -2=error, -1=not excel , 0=xls , 1=xlsx
            int result = -1;

            FileInfo FI = new FileInfo(XlsFile);
            FileStream FS = FI.Open(FileMode.Open);

            try
            {
                byte[] FH = new byte[5];

                FS.Read(FH, 0, 5);

                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (FH[j] != ExcelHeader[i, j]) break;
                        else if (j == 4) result = i;
                    }
                    if (result >= 0) break;
                }
            }
            catch
            {
                result = (-2);
                //throw e;
            }
            finally
            {
                FS.Close();
            }

            return result;
        }

        /// <summary>
        ///    Excel 파일을 DataSet 으로 변환하여 반환한다.
        /// </summary>
        /// <param name="FileName">
        ///    Excel File 명 PullPath
        /// </param>
        /// <param name="UseHeader">
        ///    첫번째 줄을 Field 명으로 사용할 것이지 여부
        /// </param>
        private DataSet OpenExcel(string FileName, bool UseHeader)
        {
            DataSet DS = null;

            string[] HDROpt = { "NO", "YES" };
            string HDR = "";
            string ConnStr = "";

            if (UseHeader)
                HDR = HDROpt[1];
            else
                HDR = HDROpt[0];

            int ExcelType = ExcelFileType(FileName);

            switch (ExcelType)
            {
                case (-2): throw new Exception(FileName + "의 형식검사중 오류가 발생하였습니다.");
                case (-1): throw new Exception(FileName + "은 엑셀 파일형식이 아닙니다.");
                case (0):
                    ConnStr = string.Format(ConnectStrFrm_Excel97_2003, FileName, HDR, "1");
                    break;
                case (1):
                    ConnStr = string.Format(ConnectStrFrm_Excel, FileName, HDR, "1");
                    break;
            }

            OleDbConnection OleDBConn = null;
            OleDbDataAdapter OleDBAdap = null;
            DataTable Schema;

            try
            {
                OleDBConn = new OleDbConnection(ConnStr);
                OleDBConn.Open();

                Schema = OleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                DS = new DataSet();

                foreach (DataRow DR in Schema.Rows)
                {
                    OleDBAdap = new OleDbDataAdapter(DR["TABLE_NAME"].ToString(), OleDBConn);

                    OleDBAdap.SelectCommand.CommandType = CommandType.TableDirect;
                    OleDBAdap.AcceptChangesDuringFill = false;

                    string TableName = DR["TABLE_NAME"].ToString().Replace("$", String.Empty).Replace("'", String.Empty);

                    if (DR["TABLE_NAME"].ToString().Contains("$")) OleDBAdap.Fill(DS, TableName);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (OleDBConn != null) OleDBConn.Close();
            }
            return DS;
        }

        #endregion
    }
}
