﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.control;
using WaterNet.WV_Common.manager;
using Infragistics.Win;
using WaterNet.WV_Common.enum1;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WE_PumpPerformanceCurveManage.work;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_PumpPerformanceCurveManage.form
{
    public partial class frmPump : Form
    {
        //MainMap 을 제어하기 위한 인스턴스 변수
        //private IMapControl mainMap = null;

        //관리단 사업장 검색조건 제어
        //private EnergySearchStepControl searchStrpControl = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        //public IMapControl MainMap
        //{
        //    set
        //    {
        //        this.mainMap = value;
        //    }
        //}

        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        private UltraGridCell cell = null;

        public frmPump()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmPump_Load);
        }

        public frmPump(UltraGridCell cell)
        {
            this.cell = cell;
            InitializeComponent();
            this.Load += new EventHandler(frmPump_Load);
        }

        private void frmPump_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
            this.searchBtn.PerformClick();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();
            DataTable station = new DataTable();
            station.Columns.Add("CODE");
            station.Columns.Add("CODE_NAME");
            station.Rows.Add(null, "전체");

            foreach (DataRow row in table.Rows)
            {
                station.Rows.Add(row["CODE"], row["CODE_NAME"].ToString());
            }

            this.bzs.ValueMember = "CODE";
            this.bzs.DisplayMember = "CODE_NAME";
            this.bzs.DataSource = station;
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            //그리드1
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "펌프설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 130;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_GBN";
            column.Header.Caption = "펌프구분";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_RPM";
            column.Header.Caption = "회전수(rpm)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SIM_GBN";
            column.Header.Caption = "모의설정";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "성능진단일자";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Format = "yyyy-MM-dd";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.Default;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.NullText = "선택";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MODEL_YN";
            column.Header.Caption = "모델반영여부";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DIFF";
            column.Header.Caption = "운영효율범위(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 110;
            column.Hidden = false;

            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //사업장
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMPSTATION_ID"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMPSTATION_ID");

                DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();

                foreach (DataRow row in table.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMPSTATION_ID"];
            }

            //펌프구분
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMP_GBN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMP_GBN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "PKP");
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMP_GBN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMP_GBN"];
            }

            //모의설정
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SIM_GBN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SIM_GBN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "1016");
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SIM_GBN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["SIM_GBN"];
            }

            //TM여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MODEL_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MODEL_YN");
                valueList.ValueListItems.Add("Y", "예");
                valueList.ValueListItems.Add("N", "아니요");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["MODEL_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["MODEL_YN"];
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.unselectedBtn.Click += new EventHandler(unselectedBtn_Click);
            this.selectedBtn.Click += new EventHandler(selectedBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);

            this.ultraGrid1.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid1_DoubleClickRow);
        }

        private void unselectedBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("연결항목을 해제 하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                this.cell.Value = DBNull.Value;
                this.Close();
            }
        }

        private void selectedBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                MessageBox.Show("펌프를 선택해주세요.");
                return;
            }
            this.cell.Value = this.ultraGrid1.ActiveRow.Cells["PUMP_FTR_IDN"].Value;
            this.Close();
        }

        private Hashtable parameter = null;

        //검색버튼 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            this.parameter = Utils.ConverToHashtable(this.groupBox1);
            this.parameter["PUMPSTATION_ID"] = this.parameter["BZS"];
            this.SelectPumpMaster();
        }

        private void SelectPumpMaster()
        {
            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid1.DataSource = PumpPerformanceCurveManageWork.GetInstance().SelectPumpMaster(this.parameter);
            this.Cursor = Cursors.Default;
        }

        //엑셀버튼 이벤트 핸들러
        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid1, this.Text);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void ultraGrid1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            string whereCase = "FTR_IDN = '" + e.Row.Cells["PUMP_FTR_IDN"].Value.ToString() + "'";
            string layerName = "펌프모터";
            //this.mainMap.MoveFocusAt(layerName, whereCase);
        }
    }
}
