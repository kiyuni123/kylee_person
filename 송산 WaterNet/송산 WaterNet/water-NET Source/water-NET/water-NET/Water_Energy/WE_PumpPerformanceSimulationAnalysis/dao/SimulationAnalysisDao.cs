﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WE_PumpPerformanceSimulationAnalysis.dao
{
    public class SimulationAnalysisDao
    {
        private static SimulationAnalysisDao dao = null;
        private SimulationAnalysisDao() { }

        public static SimulationAnalysisDao GetInstance()
        {
            if (dao == null)
            {
                dao = new SimulationAnalysisDao();
            }
            return dao;
        }

        public DataSet SelectSimulationAnalysisResult(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine(" with pump as                                                                                                                  ");
            //query.AppendLine("(                                                                                                                              ");
            //query.AppendLine("select c.value1 pump_ftr_idn                                                                                                   ");
            //query.AppendLine("      ,b.remark remark                                                                                                         ");
            //query.AppendLine("  from wh_title a                                                                                                              ");
            //query.AppendLine("      ,wh_pumps b                                                                                                              ");
            //query.AppendLine("      ,wh_tags c                                                                                                               ");
            //query.AppendLine("  where a.use_gbn = 'WH'                                                                                                       ");
            //query.AppendLine("    and b.inp_number = a.inp_number                                                                                            ");
            //query.AppendLine("    and c.inp_number = b.inp_number                                                                                            ");
            //query.AppendLine("    and c.id = b.id                                                                                                            ");
            //query.AppendLine(")                                                                                                                              ");
            //query.AppendLine("select b.pumpstation_id                                                                                                        ");
            //query.AppendLine("      ,a.pump_ftr_idn                                                                                                          ");
            //query.AppendLine("      ,nvl(b.remark, a.remark) remark                                                                                          ");
            //query.AppendLine("      ,nvl(c.timestamp, :STARTDATE) timestamp                                                                                                             ");
            //query.AppendLine("      ,c.time                                                                                                                  ");
            //query.AppendLine("      ,c.flow                                                                                                                  ");
            //query.AppendLine("      ,c.h                                                                                                                     ");
            //query.AppendLine("      ,c.energy                                                                                                                ");
            //query.AppendLine("      ,b.diff                                                                                                                  ");
            //query.AppendLine("      ,b.curve_id                                                                                                              ");
            //query.AppendLine("      ,(select to_number(e) from we_curve_data where curve_id = b.curve_id and point_yn = 'O') r_efficiency                    ");
            //query.AppendLine("      ,(select to_number(flow) from we_curve_data where curve_id = b.curve_id and point_yn = 'O') r_flow                       ");
            //query.AppendLine("  from pump a                                                                                                                  ");
            //query.AppendLine("      ,we_pump b                                                                                                               ");
            //query.AppendLine("      ,(                                                                                                                       ");
            //query.AppendLine("        select pump_ftr_idn                                                                                                    ");
            //query.AppendLine("              ,to_date(to_char(timestamp, 'yyyymmdd'),'yyyymmdd') timestamp                                                    ");
            //query.AppendLine("              ,round(sum(d_time),1) time                                                                                       ");
            //query.AppendLine("              ,round(sum(flow)) flow                                                                                           ");
            //query.AppendLine("              ,round(avg(h),1) h                                                                                               ");
            //query.AppendLine("              ,round(sum(energy)) energy                                                                                       ");
            //query.AppendLine("          from (                                                                                                               ");
            //query.AppendLine("               select pump_ftr_idn                                                                                             ");
            //query.AppendLine("                     ,timestamp                                                                                                ");
            //query.AppendLine("                     ,decode(status, 0, 0,                                                                                     ");
            //query.AppendLine("                      round((lead(timestamp, 1)                                                                                ");
            //query.AppendLine("                             over (partition by pump_ftr_idn                                                                   ");
            //query.AppendLine("                                       order by timestamp)-timestamp)*(24/1),10)) d_time                                       ");
            //query.AppendLine("                     ,flow flow                                                                                                ");
            //query.AppendLine("                     ,abs(node2_press - node1_press) h                                                                         ");
            //query.AppendLine("                     ,energy energy                                                                                            ");
            //query.AppendLine("                 from we_rpt                                                                                                   ");
            //query.AppendLine("                where timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi')                                           ");
            //query.AppendLine("                                    and to_date(:STARTDATE||'2359','yyyymmddhh24mi') + 1                                       ");
            //query.AppendLine("               )                                                                                                               ");
            //query.AppendLine("          where timestamp < to_date(:STARTDATE) + 1                                                                            ");

            ////0인 경우 제외
            //query.AppendLine("            and flow >= 1                                                                                                      ");
            
            //query.AppendLine("         group by pump_ftr_idn, to_date(to_char(timestamp, 'yyyymmdd'),'yyyymmdd')                                             ");
            //query.AppendLine("       ) c                                                                                                                     ");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn                                                                                      ");
            //query.AppendLine("   and c.pump_ftr_idn(+) = b.pump_ftr_idn                                                                                      ");
            
            ////커브없는경우제외
            //query.AppendLine("   and b.curve_id is not null                                                                                                  ");

            //if (parameter["BZS"].ToString() != "")
            //{
            //    query.Append(" and b.pumpstation_id = '").Append(parameter["BZS"].ToString()).AppendLine("'");
            //}
            
            //query.AppendLine(" order by pumpstation_id, pump_ftr_idn                                                                                         ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["STARTDATE"];
            //parameters[1].Value = parameter["STARTDATE"];
            //parameters[2].Value = parameter["STARTDATE"];
            //parameters[3].Value = parameter["STARTDATE"];


            query.AppendLine("SELECT B.PUMPSTATION_ID ");
            query.AppendLine("      ,B.PUMP_ID PUMP_FTR_IDN ");
            query.AppendLine("      ,B.REMARK ");
            query.AppendLine("      ,NVL(C.TIMESTAMP, :STARTDATE) TIMESTAMP ");
            query.AppendLine("      ,B.ID ");
            query.AppendLine("      ,C.TIME ");
            query.AppendLine("      ,C.FLOW ");
            query.AppendLine("      ,C.H ");
            query.AppendLine("      ,C.ENERGY ");
            query.AppendLine("      ,B.DIFF ");
            query.AppendLine("      ,B.CURVE_ID ");
            query.AppendLine("      ,(SELECT TO_NUMBER(E) FROM WE_CURVE_DATA WHERE CURVE_ID = B.CURVE_ID AND POINT_YN = 'O') R_EFFICIENCY ");
            query.AppendLine("      ,(SELECT TO_NUMBER(FLOW) FROM WE_CURVE_DATA WHERE CURVE_ID = B.CURVE_ID AND POINT_YN = 'O') R_FLOW ");
            query.AppendLine("  FROM WE_PUMP B ");
            query.AppendLine("      ,( ");
            query.AppendLine("        SELECT ID ID ");
            query.AppendLine("              ,TO_DATE(TO_CHAR(TIMESTAMP, 'YYYYMMDD'),'YYYYMMDD') TIMESTAMP ");
            query.AppendLine("              ,ROUND(SUM(D_TIME),1) TIME ");
            query.AppendLine("              ,ROUND(SUM(FLOW)/6,1) FLOW ");
            query.AppendLine("              ,ROUND(AVG(H),1) H ");
            query.AppendLine("              ,ROUND(SUM(ENERGY)/6,1) ENERGY ");
            query.AppendLine("          FROM ( ");
            query.AppendLine("               SELECT ID ");
            query.AppendLine("                     ,TIMESTAMP ");
            query.AppendLine("                     ,DECODE(STATUS, 0, 0, ");
            query.AppendLine("                      ROUND((LEAD(TIMESTAMP, 1) ");
            query.AppendLine("                             OVER (PARTITION BY ID ");
            query.AppendLine("                                       ORDER BY TIMESTAMP)-TIMESTAMP)*(24/1),10)) D_TIME ");
            query.AppendLine("                     ,FLOW FLOW ");
            query.AppendLine("                     ,ABS(NODE2_PRESS - NODE1_PRESS) H ");
            query.AppendLine("                     ,ENERGY ENERGY ");
            query.AppendLine("                 FROM WE_RPT ");
            query.AppendLine("                WHERE TIMESTAMP BETWEEN TO_DATE(:STARTDATE||'0000','YYYYMMDDHH24MI') ");
            query.AppendLine("                                    AND TO_DATE(:STARTDATE||'2359','YYYYMMDDHH24MI') + 1 ");
            query.AppendLine("                                    AND STATUS ='1' ");
            query.AppendLine("               ) ");
            query.AppendLine("          WHERE TIMESTAMP < TO_DATE(:STARTDATE) + 1 ");
            //query.AppendLine("            AND FLOW >= 1 ");
            query.AppendLine("         GROUP BY ID, TO_DATE(TO_CHAR(TIMESTAMP, 'YYYYMMDD'),'YYYYMMDD') ");
            query.AppendLine("       ) C ");
            query.AppendLine(" WHERE C.ID(+) = B.ID ");
            //query.AppendLine("   AND B.CURVE_ID IS NOT NULL ");

            if (parameter["BZS"].ToString() != "")
            {
                query.Append(" AND B.PUMPSTATION_ID = '").Append(parameter["BZS"].ToString()).AppendLine("'");
            }
            
            query.AppendLine(" ORDER BY PUMPSTATION_ID, PUMP_FTR_IDN ");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectSimulationAnalysisResultDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with pump as                                                                 ");
            //query.AppendLine("(                                                                            ");
            //query.AppendLine("select c.value1 pump_ftr_idn                                                 ");
            //query.AppendLine("      ,b.remark remark                                                       ");
            //query.AppendLine("  from wh_title a                                                            ");
            //query.AppendLine("      ,wh_pumps b                                                            ");
            //query.AppendLine("      ,wh_tags c                                                             ");
            //query.AppendLine("  where a.use_gbn = 'WH'                                                     ");
            //query.AppendLine("    and b.inp_number = a.inp_number                                          ");
            //query.AppendLine("    and c.inp_number = b.inp_number                                          ");
            //query.AppendLine("    and c.id = b.id                                                          ");
            //query.AppendLine("    and c.value1 = :PUMP_FTR_IDN                                             ");
            //query.AppendLine(")                                                                            ");
            //query.AppendLine("select b.pumpstation_id                                                      ");
            //query.AppendLine("      ,b.pump_ftr_idn                                                        ");
            //query.AppendLine("      ,a.remark remark                                                       ");
            //query.AppendLine("      ,c.timestamp time                                                      ");
            //query.AppendLine("      ,c.flow                                                                ");
            //query.AppendLine("      ,c.h                                                                   ");
            //query.AppendLine("      ,c.energy                                                              ");
            //query.AppendLine("      ,b.diff                                                                ");
            //query.AppendLine("      ,(select to_number(e) from we_curve_data where curve_id = b.curve_id and point_yn = 'O') r_efficiency");
            //query.AppendLine("  from pump a                                                                ");
            //query.AppendLine("      ,we_pump b                                                             ");
            //query.AppendLine("      ,(                                                                     ");
            //query.AppendLine("       select pump_ftr_idn                                                   ");
            //query.AppendLine("             ,timestamp timestamp                                            ");
            //query.AppendLine("             ,round(sum(flow)) flow                                          ");
            //query.AppendLine("             ,round(avg(abs(node2_press - node1_press)),1) h                 ");
            //query.AppendLine("             ,round(sum(energy)) energy                                      ");
            //query.AppendLine("         from we_rpt                                                         ");
            //query.AppendLine("        where timestamp between to_date(:STARTDATE,'yyyymmddhh24mi')         ");
            //query.AppendLine("                            and to_date(:ENDDATE,'yyyymmddhh24mi')           ");
            //query.AppendLine("        group by pump_ftr_idn, timestamp                                     ");
            //query.AppendLine("       ) c                                                                   ");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn                                    ");
            //query.AppendLine("   and c.pump_ftr_idn(+) = b.pump_ftr_idn                                    ");
            //query.AppendLine(" order by pumpstation_id, pump_ftr_idn, time                                 ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];
            //parameters[1].Value = parameter["STARTDATE"];
            //parameters[2].Value = parameter["ENDDATE"];

            query.AppendLine("SELECT B.PUMPSTATION_ID ");
            query.AppendLine("      ,B.PUMP_ID PUMP_FTR_IDN ");
            query.AppendLine("      ,B.REMARK REMARK ");
            query.AppendLine("      ,C.TIMESTAMP TIME ");
            query.AppendLine("      ,C.FLOW ");
            query.AppendLine("      ,C.H ");
            query.AppendLine("      ,C.ENERGY ");
            query.AppendLine("      ,B.DIFF ");
            query.AppendLine("      ,(SELECT TO_NUMBER(E) ");
            query.AppendLine("	      FROM WE_CURVE_DATA  ");
            query.AppendLine("		 WHERE CURVE_ID = B.CURVE_ID ");
            query.AppendLine("		   AND POINT_YN = 'O' ");
            query.AppendLine("	   ) R_EFFICIENCY ");
            query.AppendLine("  FROM WE_PUMP B ");
            query.AppendLine("      ,( ");
            query.AppendLine("       SELECT ID ");
            query.AppendLine("             ,TIMESTAMP TIMESTAMP ");
            query.AppendLine("             ,ROUND(SUM(FLOW),2) FLOW ");
            query.AppendLine("             ,ROUND(AVG(ABS(NODE2_PRESS - NODE1_PRESS)),2) H ");
            query.AppendLine("             ,ROUND(SUM(ENERGY),2) ENERGY ");
            query.AppendLine("         FROM WE_RPT ");
            query.AppendLine("        WHERE TIMESTAMP BETWEEN TO_DATE(:STARTDATE,'YYYYMMDDHH24MI') ");
            query.AppendLine("                            AND TO_DATE(:ENDDATE,'YYYYMMDDHH24MI') ");
            query.AppendLine("        GROUP BY ID, TIMESTAMP ");
            query.AppendLine("       ) C ");
            query.AppendLine(" WHERE B.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("   AND C.ID = B.ID ");
            query.AppendLine(" ORDER BY ");
            query.AppendLine("       PUMPSTATION_ID ");
            query.AppendLine("      ,PUMP_FTR_IDN ");
            query.AppendLine("      ,TIME ");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public object SelectSimulationTime(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select timestamp                                                             ");
            //query.AppendLine("  from (select timestamp from we_rpt order by timestamp desc)                ");
            //query.AppendLine(" where rownum = 1                                                            ");

            query.AppendLine("SELECT TIMESTAMP ");
            query.AppendLine("  FROM (SELECT TIMESTAMP ");
            query.AppendLine("          FROM WE_RPT ");
            query.AppendLine("		 ORDER BY TIMESTAMP DESC ");
            query.AppendLine("	   ) ");
            query.AppendLine(" WHERE ROWNUM = 1 ");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        public DataSet SelectRealSimulationAnalysis(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with pump as                                                                 ");
            //query.AppendLine("(																               ");
            //query.AppendLine("select c.value1 pump_ftr_idn									               ");
            //query.AppendLine("        ,b.remark remark										               ");
            //query.AppendLine("      ,to_date(:STARTDATE,'yyyymmddhh24mi') timestamp			               ");
            //query.AppendLine("  from wh_title a												               ");
            //query.AppendLine("        ,wh_pumps b											               ");
            //query.AppendLine("        ,wh_tags c											               ");
            //query.AppendLine(" where a.use_gbn = 'WH'										               ");
            //query.AppendLine("   and b.inp_number = a.inp_number							               ");
            //query.AppendLine("   and c.inp_number = b.inp_number							               ");
            //query.AppendLine("   and c.id = b.id											               ");
            //query.AppendLine(")																               ");
            //query.AppendLine("select b.pumpstation_id										               ");
            //query.AppendLine("      ,a.timestamp timestamp									               ");
            //query.AppendLine("      ,round(sum(c.flow)) flow								               ");
            //query.AppendLine("      ,round(avg(c.h),1) h									               ");
            //query.AppendLine("      ,round(sum(c.energy)) energy							               ");
            //query.AppendLine("      ,round(avg((select to_number(e) 						               ");
            //query.AppendLine("                    from we_curve_data 						               ");
            //query.AppendLine("                   where curve_id = b.curve_id 				               ");
            //query.AppendLine("                     and point_yn = 'O'))						               ");
            //query.AppendLine("            ,1) r_efficiency									               ");
            //query.AppendLine("  from pump a													               ");
            //query.AppendLine("      ,we_pump b												               ");
            //query.AppendLine("      ,(														               ");
            //query.AppendLine("        select pump_ftr_idn									               ");
            //query.AppendLine("              ,timestamp timestamp							               ");
            //query.AppendLine("              ,flow flow										               ");
            //query.AppendLine("              ,abs(node2_press - node1_press) h				               ");
            //query.AppendLine("              ,energy energy									               ");
            //query.AppendLine("              ,status status									               ");
            //query.AppendLine("          from we_rpt											               ");
            //query.AppendLine("         where flow >= 1										               ");
            //query.AppendLine("      ) c														               ");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn						               ");
            //query.AppendLine("   and c.pump_ftr_idn(+) = a.pump_ftr_idn						               ");
            //query.AppendLine("   and c.timestamp(+) = a.timestamp							               ");
            //query.AppendLine("   and b.pumpstation_id = nvl(:BZS, b.pumpstation_id)			               ");
            //query.AppendLine(" group by pumpstation_id, a.timestamp							               ");
            //query.AppendLine(" order by pumpstation_id										               ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("BZS", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["STARTDATE"];
            //parameters[1].Value = parameter["BZS"];

            query.AppendLine("WITH PUMP AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT C.PUMPSTATION_ID ");
            query.AppendLine("      ,C.PUMP_ID PUMP_FTR_IDN ");
            query.AppendLine("      ,C.ID ");
            query.AppendLine("      ,C.REMARK REMARK ");
            query.AppendLine("      ,C.CURVE_ID ");
            query.AppendLine("      ,TO_DATE(:STARTDATE,'YYYYMMDDHH24MI') TIMESTAMP ");
            query.AppendLine("  FROM WH_TITLE A ");
            query.AppendLine("      ,WH_PUMPS B ");
            query.AppendLine("      ,WE_PUMP C ");
            query.AppendLine(" WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("   AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("   AND C.ID = B.ID ");
            query.AppendLine(") ");
            query.AppendLine("SELECT A.PUMPSTATION_ID ");
            query.AppendLine("      ,A.TIMESTAMP TIMESTAMP ");
            query.AppendLine("      ,ROUND(SUM(C.FLOW)) FLOW ");
            query.AppendLine("      ,ROUND(AVG(C.H),1) H ");
            query.AppendLine("      ,ROUND(SUM(C.ENERGY)) ENERGY ");
            query.AppendLine("      ,ROUND(AVG((SELECT TO_NUMBER(E) ");
            query.AppendLine("                    FROM WE_CURVE_DATA ");
            query.AppendLine("                   WHERE CURVE_ID = A.CURVE_ID ");
            query.AppendLine("                     AND POINT_YN = 'O')) ");
            query.AppendLine("            ,1) R_EFFICIENCY ");
            query.AppendLine("  FROM PUMP A ");
            query.AppendLine("      ,( ");
            query.AppendLine("        SELECT ID ");
            query.AppendLine("              ,TIMESTAMP TIMESTAMP ");
            query.AppendLine("              ,FLOW FLOW ");
            query.AppendLine("              ,ABS(NODE2_PRESS - NODE1_PRESS) H ");
            query.AppendLine("              ,ENERGY ENERGY ");
            query.AppendLine("              ,STATUS STATUS ");
            query.AppendLine("          FROM WE_RPT ");
            query.AppendLine("         WHERE FLOW >= 1 ");
            query.AppendLine("      ) C ");
            query.AppendLine(" WHERE A.PUMPSTATION_ID = NVL(:BZS, A.PUMPSTATION_ID) ");
            query.AppendLine("   AND C.ID(+) = A.ID ");
            query.AppendLine("   AND C.TIMESTAMP(+) = A.TIMESTAMP ");
            query.AppendLine(" GROUP BY PUMPSTATION_ID, A.TIMESTAMP ");
            query.AppendLine(" ORDER BY PUMPSTATION_ID ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("BZS", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["BZS"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectRealSimulationAnalysisResult(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with pump as                                                                 ");
            //query.AppendLine("(                                                                            ");
            //query.AppendLine("select c.value1 pump_ftr_idn                                                 ");
            //query.AppendLine("      ,b.remark remark                                                       ");
            //query.AppendLine("      ,to_date(:STARTDATE,'yyyymmddhh24mi') timestamp                        ");
            //query.AppendLine("  from wh_title a                                                            ");
            //query.AppendLine("      ,wh_pumps b                                                            ");
            //query.AppendLine("      ,wh_tags c                                                             ");
            //query.AppendLine(" where a.use_gbn = 'WH'                                                      ");
            //query.AppendLine("   and b.inp_number = a.inp_number                                           ");
            //query.AppendLine("   and c.inp_number = b.inp_number                                           ");
            //query.AppendLine("   and c.id = b.id                                                           ");
            //query.AppendLine(")                                                                            ");
            //query.AppendLine("select b.pumpstation_id                                                      ");
            //query.AppendLine("      ,b.pump_ftr_idn                                                        ");
            //query.AppendLine("      ,a.remark remark                                                       ");
            //query.AppendLine("      ,c.status                                                              ");
            //query.AppendLine("      ,round(c.flow) flow                                                    ");
            //query.AppendLine("      ,round(c.h, 1) h                                                       ");
            //query.AppendLine("      ,round(c.energy) energy                                                ");
            //query.AppendLine("      ,round(b.diff, 1) diff                                                 ");
            //query.AppendLine("      ,b.curve_id                                                            ");
            //query.AppendLine("      ,round((select to_number(e)                                            ");
            //query.AppendLine("                from we_curve_data                                           ");
            //query.AppendLine("               where curve_id = b.curve_id                                   ");
            //query.AppendLine("                 and point_yn = 'O')                                         ");
            //query.AppendLine("             ,1) r_efficiency                                                ");
            //query.AppendLine("  from pump a                                                                ");
            //query.AppendLine("      ,we_pump b                                                             ");
            //query.AppendLine("      ,(                                                                     ");
            //query.AppendLine("       select pump_ftr_idn                                                   ");
            //query.AppendLine("             ,timestamp timestamp                                            ");
            //query.AppendLine("             ,flow flow                                                      ");
            //query.AppendLine("             ,abs(node2_press - node1_press) h                               ");
            //query.AppendLine("             ,energy energy                                                  ");
            //query.AppendLine("             ,status status                                                  ");
            //query.AppendLine("         from we_rpt                                                         ");
            //query.AppendLine("      ) c                                                                    ");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn                                    ");
            //query.AppendLine("   and c.pump_ftr_idn(+) = a.pump_ftr_idn                                    ");
            //query.AppendLine("   and c.timestamp(+) = a.timestamp                                          ");
            //query.AppendLine("   and b.pumpstation_id = nvl(:BZS, b.pumpstation_id)                        ");
            //query.AppendLine(" order by remark, pumpstation_id, pump_ftr_idn                               ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("BZS", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["STARTDATE"];
            //parameters[1].Value = parameter["BZS"];

            query.AppendLine("WITH PUMP AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT C.PUMPSTATION_ID ");
            query.AppendLine("      ,C.PUMP_ID PUMP_FTR_IDN ");
            query.AppendLine("      ,C.ID ");
            query.AppendLine("      ,C.REMARK REMARK ");
            query.AppendLine("      ,C.CURVE_ID ");
            query.AppendLine("      ,C.DIFF ");
            query.AppendLine("      ,TO_DATE(:STARTDATE,'YYYYMMDDHH24MI') TIMESTAMP ");
            query.AppendLine("  FROM WH_TITLE A ");
            query.AppendLine("      ,WH_PUMPS B ");
            query.AppendLine("      ,WE_PUMP C ");
            query.AppendLine(" WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("   AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("   AND C.ID = B.ID ");
            query.AppendLine(") ");
            query.AppendLine("SELECT A.PUMPSTATION_ID ");
            query.AppendLine("      ,A.PUMP_FTR_IDN ");
            query.AppendLine("      ,A.ID ");
            query.AppendLine("      ,A.REMARK REMARK ");
            query.AppendLine("      ,C.STATUS ");
            query.AppendLine("      ,ROUND(C.FLOW) FLOW ");
            query.AppendLine("      ,ROUND(C.H, 1) H ");
            query.AppendLine("      ,ROUND(C.ENERGY) ENERGY ");
            query.AppendLine("      ,ROUND(A.DIFF, 1) DIFF ");
            query.AppendLine("      ,A.CURVE_ID ");
            query.AppendLine("      ,ROUND((SELECT TO_NUMBER(E) ");
            query.AppendLine("                FROM WE_CURVE_DATA ");
            query.AppendLine("               WHERE CURVE_ID = A.CURVE_ID ");
            query.AppendLine("                 AND POINT_YN = 'O') ");
            query.AppendLine("       ,1) R_EFFICIENCY ");
            query.AppendLine("  FROM PUMP A ");
            query.AppendLine("      ,( ");
            query.AppendLine("       SELECT ID ");
            query.AppendLine("             ,TIMESTAMP TIMESTAMP ");
            query.AppendLine("             ,FLOW FLOW ");
            query.AppendLine("             ,ABS(NODE2_PRESS - NODE1_PRESS) H ");
            query.AppendLine("             ,ENERGY ENERGY ");
            query.AppendLine("             ,STATUS STATUS ");
            query.AppendLine("         FROM WE_RPT ");
            query.AppendLine("      ) C ");
            query.AppendLine(" WHERE A.PUMPSTATION_ID = NVL(:BZS, A.PUMPSTATION_ID) ");
            query.AppendLine("   AND C.ID(+) = A.ID ");
            query.AppendLine("   AND C.TIMESTAMP(+) = A.TIMESTAMP ");
            query.AppendLine(" ORDER BY ");
            query.AppendLine("       REMARK ");
            query.AppendLine("      ,PUMPSTATION_ID ");
            query.AppendLine("      ,PUMP_FTR_IDN ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("BZS", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["BZS"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }
    }
}
