﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.control;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WE_PumpPerformanceSimulationAnalysis.work;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_PumpPerformanceSimulationAnalysis.form
{
    public partial class frmRealMain : Form
    {
        private WE_EnergyMap.frmMain mainMap = null;
        public WE_EnergyMap.frmMain MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        public frmRealMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();
            DataTable station = new DataTable();
            station.Columns.Add("CODE");
            station.Columns.Add("CODE_NAME");
            station.Rows.Add(null, "전체");

            foreach (DataRow row in table.Rows)
            {
                station.Rows.Add(row["CODE"], row["CODE_NAME"].ToString());
            }

            this.bzs.ValueMember = "CODE";
            this.bzs.DisplayMember = "CODE_NAME";
            this.bzs.DataSource = station;

            this.startdate.Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.RowsAndCells;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.RowsAndCells;
            this.ultraGrid2.DisplayLayout.Bands[0].ColHeaderLines = 2;

            this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            this.ultraGrid2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "해석일시";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = true;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량(㎥/일)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.Format = "###,###,##0";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 65;
            column.Hidden = false;
            column.Format = "###,###,##0";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ENERGY";
            column.Header.Caption = "전력량(kwh)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.Format = "###,###,##0";

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "W_ENERGY";
            column.Header.Caption = "원단위(kwh/㎥)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Format = "N3";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "O_EFFICIENCY";
            column.Header.Caption = "운영효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_EFFICIENCY";
            column.Header.Caption = "정격효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_O";
            column.Header.Caption = "운영효율차(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "RESULT";
            column.Header.Caption = "평가";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            //////
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ID";
            column.Header.Caption = "ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "펌프설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "STATUS";
            column.Header.Caption = "운전";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량\n(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.Format = "###,###,##0";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정\n(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 65;
            column.Hidden = false;
            column.Format = "###,###,##0";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ENERGY";
            column.Header.Caption = "전력량\n(kwh)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Hidden = false;
            column.Format = "###,###,##0";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "W_ENERGY";
            column.Header.Caption = "원단위\n(kwh/㎥)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Format = "N3";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "O_EFFICIENCY";
            column.Header.Caption = "운영효율\n(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_EFFICIENCY";
            column.Header.Caption = "정격효율\n(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_O";
            column.Header.Caption = "운영효율차\n(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 70;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "RESULT";
            column.Header.Caption = "평가";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DIFF";
            column.Header.Caption = "운전효율범위";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능자료관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = true;

            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //사업장
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMPSTATION_ID"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMPSTATION_ID");

                DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();

                foreach (DataRow row in table.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMPSTATION_ID"];
            }

            //사업장
            if (!this.ultraGrid2.DisplayLayout.ValueLists.Exists("PUMPSTATION_ID"))
            {
                valueList = this.ultraGrid2.DisplayLayout.ValueLists.Add("PUMPSTATION_ID");

                DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();

                foreach (DataRow row in table.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid2.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList =
                    this.ultraGrid2.DisplayLayout.ValueLists["PUMPSTATION_ID"];
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.curveBtn.Click += new EventHandler(curveBtn_Click);

            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid2.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid2_InitializeLayout);
            this.ultraGrid2.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid2_DoubleClickRow);
        }

        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.parameter == null)
            {
                return;
            }

            if (this.ultraGrid1.ActiveRow == null)
            {
                return;
            }

            this.parameter["BZS"] = this.ultraGrid1.ActiveRow.Cells["PUMPSTATION_ID"].Value;
            this.parameter["STARTDATE"] = 
                Convert.ToDateTime(this.ultraGrid1.ActiveRow.Cells["TIMESTAMP"].Value).ToString("yyyyMMddHHmm");
            this.SelectRealSimulationAnalysisResult();
        }

        private Hashtable parameter = null;
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                object time = SimulationAnalysisWork.GetInstance().SelectSimulationTime();

                if (time == null)
                {
                    this.startdate.Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                }
                else
                {
                    this.startdate.Value = Convert.ToDateTime(time).ToString("yyyy-MM-dd HH:mm");
                }

                this.parameter = Utils.ConverToHashtable(this.groupBox1);
                this.parameter["STARTDATE"] = Convert.ToDateTime(this.startdate.Value).ToString("yyyyMMddHHmm");

                this.SelectRealSimulationAnalysis();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            
        }

        private void SelectRealSimulationAnalysis()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable table = SimulationAnalysisWork.GetInstance().SelectRealSimulationAnalysis(this.parameter);
            table.ColumnChanging += new DataColumnChangeEventHandler(this.OnTableCellValueChanging);
            this.ultraGrid1.DataSource = table;
            this.Cursor = Cursors.Default;
        }

        private void SelectRealSimulationAnalysisResult()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable table = SimulationAnalysisWork.GetInstance().SelectRealSimulationAnalysisResult(this.parameter);
            table.ColumnChanging += new DataColumnChangeEventHandler(this.OnTableCellValueChanging);
            this.ultraGrid2.DataSource = table;
            this.Cursor = Cursors.Default;
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid2, this.Text);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void curveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.ultraGrid2.ActiveRow == null)
                {
                    return;
                }
                Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid2.ActiveRow);
                parameter["STARTDATE"]
                    = Convert.ToDateTime(this.startdate.Value).ToString("yyyy-MM-dd HH:mm");
                parameter["ENDDATE"]
                    = Convert.ToDateTime(this.startdate.Value).ToString("yyyy-MM-dd HH:mm");

                WE_PumpCharacteristicCurve.form.frmMain form = new WaterNet.WE_PumpCharacteristicCurve.form.frmMain(parameter);
                form.Owner = this;
                form.Show(this);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            foreach (UltraGridRow row in e.Layout.Rows)
            {
                if (row.Cells["RESULT"].Value.ToString() == "R")
                {
                    row.Cells["RESULT"].SetValue("정격효율확인", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "M")
                {
                    row.Cells["RESULT"].SetValue("운영범위미달", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "O")
                {
                    row.Cells["RESULT"].SetValue("운영범위초과", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "T")
                {
                    row.Cells["RESULT"].SetValue("정상", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "OFF")
                {
                    row.Cells["RESULT"].SetValue("펌프 미가동", false);
                }
            }
            ((UltraGrid)sender).UpdateData();
        }

        private void ultraGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            foreach (UltraGridRow row in e.Layout.Rows)
            {
                if (row.Cells["RESULT"].Value.ToString() == "R")
                {
                    row.Cells["RESULT"].SetValue("정격효율확인", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "M")
                {
                    row.Cells["RESULT"].SetValue("운영범위미달", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "O")
                {
                    row.Cells["RESULT"].SetValue("운영범위초과", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "T")
                {
                    row.Cells["RESULT"].SetValue("정상", false);
                }
                if (row.Cells["RESULT"].Value.ToString() == "OFF")
                {
                    row.Cells["RESULT"].SetValue("펌프 미가동", false);
                }
            }
            ((UltraGrid)sender).UpdateData();
        }

        private void OnTableCellValueChanging(object sender, DataColumnChangeEventArgs e)
        {
            this.ValidateDataRowCell(e.Row, e.Column, e.ProposedValue);
        }

        private void ValidateDataRowCell(DataRow row, DataColumn column, object value)
        {
            if (column.ColumnName == "RESULT")
            {
                row.SetColumnError(column, string.Empty);

                if (row[column.ColumnName].ToString() == "R")
                {
                    row.SetColumnError(column, "정격효율확인");
                }

                if (row[column.ColumnName].ToString() == "M")
                {
                    row.SetColumnError(column, "운영범위미달");
                }

                if (row[column.ColumnName].ToString() == "O")
                {
                    row.SetColumnError(column, "운영범위초과");
                }
            }
        }

        private void ultraGrid2_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.mainMap == null)
            {
                MessageBox.Show("에너지 업무 GIS가 존재하지 않습니다.");
                return;
            }

            string whereCase = "ID = '" + e.Row.Cells["ID"].Value.ToString() + "'";
            string layerName = "PUMP";
            this.mainMap.MoveFocusAt(layerName, whereCase);
        }
    }
}
