﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using System.Collections;
using WaterNet.WV_Common.util;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WV_Common.cal;
using WaterNet.WE_PumpPerformanceSimulationAnalysis.control;
using ChartFX.WinForms;
using ChartFX.WinForms.Annotation;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.enum1;
using WaterNet.WE_PumpPerformanceSimulationAnalysis.work;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_PumpPerformanceSimulationAnalysis.form
{
    public partial class frmSimulationAnalysisResult : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        //차트 매니저(차트 스타일 설정및 기타공통기능설정)
        private ChartManager chartManager = null;

        //테이블// 그래프 레이아웃 
        private TableLayout tableLayout = null;

        private Hashtable pumpInfo = null;

        //MainMap 을 제어하기 위한 인스턴스 변수
        //private IMapControl mainMap = null;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        //public IMapControl MainMap
        //{
        //    set
        //    {
        //        this.mainMap = value;
        //    }
        //}

        public frmSimulationAnalysisResult()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        public frmSimulationAnalysisResult(Hashtable pumpInfo)
        {
            this.pumpInfo = pumpInfo;
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.tableLayout = new TableLayout(this.tableLayoutPanel1, this.chartPanel1, this.tablePanel1, this.chartVisibleBtn, this.tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            //특정 유량계의 표준규격을 찾기위한 호출은 검색버튼을 비활성화 시킨다.
            //검색조건 유량계형식과 유량계구경을 선택한 유량계와 동일하게 설정한다.
            if (this.pumpInfo != null)
            {
                gridManager.DefaultColumnsMapping(this.ultraGrid1, typeof(string));
                this.ultraGrid1.Rows[0].Cells["PUMPSTATION_ID"].Value = this.pumpInfo["PUMPSTATION_ID"].ToString();
                this.ultraGrid1.Rows[0].Cells["PUMP_FTR_IDN"].Value = this.pumpInfo["PUMP_FTR_IDN"].ToString();
                this.ultraGrid1.Rows[0].Cells["REMARK"].Value = this.pumpInfo["REMARK"].ToString();
                this.ultraGrid1.Rows[0].Cells["CURVE_ID"].Value = this.pumpInfo["CURVE_ID"].ToString();
                this.ultraGrid1.Rows[0].Cells["R_EFFICIENCY"].Value = this.pumpInfo["R_EFFICIENCY"].ToString();
                this.ultraGrid1.Rows[0].Cells["DIFF"].Value = this.pumpInfo["DIFF"].ToString();
                this.ultraGrid1.Rows[0].Cells["R_FLOW"].Value = this.pumpInfo["R_FLOW"].ToString();

                this.startDate.Value = DateTime.ParseExact(this.pumpInfo["TIMESTAMP"].ToString(), "yyyyMMdd", null).AddMonths(-1).ToString("yyyy-MM-dd") + " 00:00";
                this.endDate.Value = DateTime.ParseExact(this.pumpInfo["TIMESTAMP"].ToString(), "yyyyMMdd", null).ToString("yyyy-MM-dd") + " 23:59";
            }

            this.searchBtn.PerformClick();
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용

            UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "펌프설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "CURVE_ID";
            column.Header.Caption = "성능진단관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_EFFICIENCY";
            column.Header.Caption = "정격효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_FLOW";
            column.Header.Caption = "정격유량";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DIFF";
            column.Header.Caption = "운영효율범위(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 110;
            column.Hidden = false;




            //패턴오차분석 그리드 컬럼설정
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMP_FTR_IDN";
            column.Header.Caption = "펌프관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIME";
            column.Header.Caption = "일자";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Format = "yyyy-MM-dd HH:mm";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "펌프설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "유량(㎥/h)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Format = "###,###,##0";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "양정(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 90;
            column.Format = "###,###,##0";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ENERGY";
            column.Header.Caption = "전력량(kwh)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 110;
            column.Format = "###,###,##0";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "W_ENERGY";
            column.Header.Caption = "원단위(kwh/㎥)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Format = "N3";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "O_EFFICIENCY";
            column.Header.Caption = "운영효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 110;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DIFF";
            column.Header.Caption = "운영효율범위";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.Format = "N1";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_EFFICIENCY";
            column.Header.Caption = "정격운영효율";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.Format = "N1";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "R_O";
            column.Header.Caption = "정격-운영효율";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = true;
            column.Format = "N1";

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "RESULT";
            column.Header.Caption = "평가분류";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 140;
            column.Hidden = false;

            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //사업장
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMPSTATION_ID"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMPSTATION_ID");

                DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();

                foreach (DataRow row in table.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMPSTATION_ID"];
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.curveBtn.Click += new EventHandler(curveBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.ultraGrid2.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid2_InitializeLayout);

            this.radioButton1.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.radioButton2.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.radioButton3.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.radioButton4.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.radioButton5.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
        }

        private void curveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.ultraGrid1.Rows.Count == 0)
                {
                    return;
                }

                Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid1.Rows[0]);
                parameter["STARTDATE"]
                    = Convert.ToDateTime(this.startDate.Value).ToString("yyyy-MM-dd HH:mm");
                parameter["ENDDATE"]
                    = Convert.ToDateTime(this.endDate.Value).ToString("yyyy-MM-dd HH:mm");

                WE_PumpCharacteristicCurve.form.frmMain form = new WaterNet.WE_PumpCharacteristicCurve.form.frmMain(parameter);
                form.Owner = this;
                form.Show(this);

            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void ultraGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            foreach (UltraGridRow gridRow in e.Layout.Rows)
            {
                if (gridRow.Cells["RESULT"].Value.ToString() == "M")
                {
                    gridRow.Cells["RESULT"].Value = "정격운전범위미달";
                    gridRow.Cells["RESULT"].Appearance.ForeColor = Color.Red;
                }

                if (gridRow.Cells["RESULT"].Value.ToString() == "O")
                {
                    gridRow.Cells["RESULT"].Value = "정격운전범위초과";
                    gridRow.Cells["RESULT"].Appearance.ForeColor = Color.Red;
                }

                if (gridRow.Cells["RESULT"].Value.ToString() == "T")
                {
                    gridRow.Cells["RESULT"].Value = "정격운전범위";
                }

                if (gridRow.Cells["RESULT"].Value.ToString() == "R")
                {
                    gridRow.Cells["RESULT"].Value = "정격효율확인";
                    gridRow.Cells["RESULT"].Appearance.ForeColor = Color.Red;
                }

                if (gridRow.Cells["RESULT"].Value.ToString() == "OFF")
                {
                    gridRow.Cells["RESULT"].Value = "펌프 미가동";
                }
            }
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.pumpInfo["STARTDATE"] = ((DateTime)this.startDate.Value).ToString("yyyyMMddHHmm");
                this.pumpInfo["ENDDATE"] = ((DateTime)this.endDate.Value).ToString("yyyyMMddHHmm");
                this.SelectSimulationAnalysisResultDetail();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private DataTable analysisResult = null;
        private void SelectSimulationAnalysisResultDetail()
        {
            this.Cursor = Cursors.WaitCursor;
            this.analysisResult =
                SimulationAnalysisWork.GetInstance().SelectSimulationAnalysisResultDetail(this.pumpInfo);
            this.ultraGrid2.DataSource = this.analysisResult;
            this.InitializeChartSetting();
            this.Cursor = Cursors.Default;
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid2, this.Text);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!((RadioButton)sender).Checked)
            {
                return;
            }
            this.InitializeChartSeries();
        }

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.chart1.AxisY.Sections.Clear();
            this.chart1.ConditionalAttributes.Clear();
            this.chart1.Extensions.Clear();
            this.chart1.LegendBox.CustomItems.Clear();
            this.chart1.Data.Clear();
            this.chartManager.AllClear(this.chart1);

            this.chart1.Data.Series = 15;
            this.chart1.Data.Points = this.ultraGrid2.Rows.Count;

            this.chart1.Series[0].Text = "정격운전범위미달";
            this.chart1.Series[0].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[0].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[0].Color = Color.White;
            this.chart1.Series[0].Border.Visible = true;
            this.chart1.Series[0].Border.Effect = ChartFX.WinForms.BorderEffect.None;
            this.chart1.Series[0].Border.Color = Color.Red;
            this.chart1.Series[0].MarkerSize = 2;

            this.chart1.Series[1].Text = "정격운전범위";
            this.chart1.Series[1].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[1].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[1].Color = Color.Blue;
            this.chart1.Series[1].Border.Visible = false;
            this.chart1.Series[1].MarkerSize = 2;

            this.chart1.Series[2].Text = "정격운전범위초과";
            this.chart1.Series[2].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[2].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[2].Color = Color.Green;
            this.chart1.Series[2].Border.Visible = false;
            this.chart1.Series[2].MarkerSize = 2;

            this.chart1.Series[3].Text = "정격운전범위미달";
            this.chart1.Series[3].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[3].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[3].Color = Color.White;
            this.chart1.Series[3].Border.Visible = true;
            this.chart1.Series[3].Border.Effect = ChartFX.WinForms.BorderEffect.None;
            this.chart1.Series[3].Border.Color = Color.Red;
            this.chart1.Series[3].MarkerSize = 2;

            this.chart1.Series[4].Text = "정격운전범위";
            this.chart1.Series[4].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[4].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[4].Color = Color.Blue;
            this.chart1.Series[4].Border.Visible = false;
            this.chart1.Series[4].MarkerSize = 2;

            this.chart1.Series[5].Text = "정격운전범위초과";
            this.chart1.Series[5].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[5].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[5].Color = Color.Green;
            this.chart1.Series[5].Border.Visible = false;
            this.chart1.Series[5].MarkerSize = 2;

            this.chart1.Series[6].Text = "정격운전범위미달";
            this.chart1.Series[6].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[6].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[6].Color = Color.White;
            this.chart1.Series[6].Border.Visible = true;
            this.chart1.Series[6].Border.Effect = ChartFX.WinForms.BorderEffect.None;
            this.chart1.Series[6].Border.Color = Color.Red;
            this.chart1.Series[6].MarkerSize = 2;

            this.chart1.Series[7].Text = "정격운전범위";
            this.chart1.Series[7].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[7].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[7].Color = Color.Blue;
            this.chart1.Series[7].Border.Visible = false;
            this.chart1.Series[7].MarkerSize = 2;
            
            this.chart1.Series[8].Text = "정격운전범위초과";
            this.chart1.Series[8].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[8].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[8].Color = Color.Green;
            this.chart1.Series[8].Border.Visible = false;
            this.chart1.Series[8].MarkerSize = 2;

            this.chart1.Series[9].Text = "정격운전범위미달";
            this.chart1.Series[9].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[9].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[9].Color = Color.White;
            this.chart1.Series[9].Border.Visible = true;
            this.chart1.Series[9].Border.Effect = ChartFX.WinForms.BorderEffect.None;
            this.chart1.Series[9].Border.Color = Color.Red;
            this.chart1.Series[9].MarkerSize = 2;

            this.chart1.Series[10].Text = "정격운전범위";
            this.chart1.Series[10].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[10].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[10].Color = Color.Blue;
            this.chart1.Series[10].Border.Visible = false;
            this.chart1.Series[10].MarkerSize = 2;

            this.chart1.Series[11].Text = "정격운전범위초과";
            this.chart1.Series[11].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[11].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[11].Color = Color.Green;
            this.chart1.Series[11].Border.Visible = false;
            this.chart1.Series[11].MarkerSize = 2;

            this.chart1.Series[12].Text = "정격운전범위미달";
            this.chart1.Series[12].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[12].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[12].Color = Color.White;
            this.chart1.Series[12].Border.Visible = true;
            this.chart1.Series[12].Border.Effect = ChartFX.WinForms.BorderEffect.None;
            this.chart1.Series[12].Border.Color = Color.Red;
            this.chart1.Series[12].MarkerSize = 2;

            this.chart1.Series[13].Text = "정격운전범위";
            this.chart1.Series[13].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[13].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[13].Color = Color.Blue;
            this.chart1.Series[13].Border.Visible = false;
            this.chart1.Series[13].MarkerSize = 2;

            this.chart1.Series[14].Text = "정격운전범위초과";
            this.chart1.Series[14].Gallery = ChartFX.WinForms.Gallery.Scatter;
            this.chart1.Series[14].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
            this.chart1.Series[14].Color = Color.Green;
            this.chart1.Series[14].Border.Visible = false;
            this.chart1.Series[14].MarkerSize = 2;

            foreach (UltraGridRow gridRow in this.ultraGrid2.Rows)
            {
                if (gridRow.Cells["TIME"].Value != DBNull.Value)
                {
                    this.chart1.AxisX.Labels[gridRow.Index]
                        = Convert.ToDateTime(gridRow.Cells["TIME"].Value).ToString("yyyy-MM-dd HH:mm");

                    if (gridRow.Cells["RESULT"].Value.ToString() == "정격운전범위미달")
                    {
                        if (Utils.ToDouble(gridRow.Cells["FLOW"].Value) > 0)
                        {
                            this.chart1.Data[0, gridRow.Index] = Utils.ToDouble(gridRow.Cells["FLOW"].Value);
                            this.chart1.Data[3, gridRow.Index] = Utils.ToDouble(gridRow.Cells["H"].Value);
                            this.chart1.Data[6, gridRow.Index] = Utils.ToDouble(gridRow.Cells["ENERGY"].Value);
                            this.chart1.Data[9, gridRow.Index] = Utils.ToDouble(gridRow.Cells["W_ENERGY"].Value);
                            this.chart1.Data[12, gridRow.Index] = Utils.ToDouble(gridRow.Cells["O_EFFICIENCY"].Value);
                        }
                    }
                    if (gridRow.Cells["RESULT"].Value.ToString() == "정격운전범위")
                    {
                        if (Utils.ToDouble(gridRow.Cells["FLOW"].Value) > 0)
                        {
                            this.chart1.Data[1, gridRow.Index] = Utils.ToDouble(gridRow.Cells["FLOW"].Value);
                            this.chart1.Data[4, gridRow.Index] = Utils.ToDouble(gridRow.Cells["H"].Value);
                            this.chart1.Data[7, gridRow.Index] = Utils.ToDouble(gridRow.Cells["ENERGY"].Value);
                            this.chart1.Data[10, gridRow.Index] = Utils.ToDouble(gridRow.Cells["W_ENERGY"].Value);
                            this.chart1.Data[13, gridRow.Index] = Utils.ToDouble(gridRow.Cells["O_EFFICIENCY"].Value);
                        }
                    }

                    if (gridRow.Cells["RESULT"].Value.ToString() == "정격운전범위초과")
                    {
                        if (Utils.ToDouble(gridRow.Cells["FLOW"].Value) > 0)
                        {
                            this.chart1.Data[2, gridRow.Index] = Utils.ToDouble(gridRow.Cells["FLOW"].Value);
                            this.chart1.Data[5, gridRow.Index] = Utils.ToDouble(gridRow.Cells["H"].Value);
                            this.chart1.Data[8, gridRow.Index] = Utils.ToDouble(gridRow.Cells["ENERGY"].Value);
                            this.chart1.Data[11, gridRow.Index] = Utils.ToDouble(gridRow.Cells["W_ENERGY"].Value);
                            this.chart1.Data[14, gridRow.Index] = Utils.ToDouble(gridRow.Cells["O_EFFICIENCY"].Value);


                            Console.WriteLine(Convert.ToDateTime(gridRow.Cells["TIME"].Value).ToString("yyyy-MM-dd HH:mm") + " : " + gridRow.Cells["FLOW"].Value.ToString());

                        }
                    }
                }
            }

            this.chart1.AxesX[0].Title.Text = "일자";
            this.InitializeChartSeries();
        }

        private void InitializeChartSeries()
        {
            if (this.ultraGrid2.Rows.Count == 0)
            {
                return;
            }

            this.chart1.Extensions.Clear();
            this.chart1.LegendBox.CustomItems.Clear();

            foreach (SeriesAttributes series in this.chart1.Series)
            {
                series.Visible = false;
            }

            if (this.radioButton1.Checked)
            {
                this.chart1.Series[0].Visible = true;
                this.chart1.Series[1].Visible = true;
                this.chart1.Series[2].Visible = true;
                this.chart1.AxisY.LabelsFormat.Format = AxisFormat.Number;
                this.chart1.AxisY.LabelsFormat.CustomFormat = "###,###,###";
                this.chart1.AxisY.Title.Text = "유량(㎥/h)";
            }
            if (this.radioButton2.Checked)
            {
                this.chart1.Series[3].Visible = true;
                this.chart1.Series[4].Visible = true;
                this.chart1.Series[5].Visible = true;
                this.chart1.AxisY.LabelsFormat.Format = AxisFormat.Number;
                this.chart1.AxisY.LabelsFormat.CustomFormat = "###,###,###";
                this.chart1.AxisY.Title.Text = "양정(m)";
            }
            if (this.radioButton3.Checked)
            {
                this.chart1.Series[6].Visible = true;
                this.chart1.Series[7].Visible = true;
                this.chart1.Series[8].Visible = true;
                this.chart1.AxisY.LabelsFormat.Format = AxisFormat.Number;
                this.chart1.AxisY.LabelsFormat.CustomFormat = "###,###,###";
                this.chart1.AxisY.Title.Text = "전력량(kwh)";
            }
            if (this.radioButton4.Checked)
            {
                this.chart1.Series[9].Visible = true;
                this.chart1.Series[10].Visible = true;
                this.chart1.Series[11].Visible = true;
                this.chart1.AxisY.LabelsFormat.Format = AxisFormat.Number;
                this.chart1.AxisY.LabelsFormat.CustomFormat = "N3";
                this.chart1.AxisY.Title.Text = "원단위(kwh/㎥)";
            }
            if (this.radioButton5.Checked)
            {
                this.chart1.Series[12].Visible = true;
                this.chart1.Series[13].Visible = true;
                this.chart1.Series[14].Visible = true;
                this.chart1.AxisY.LabelsFormat.Format = AxisFormat.Number;
                this.chart1.AxisY.LabelsFormat.CustomFormat = "###,###,###.0";
                this.chart1.AxisY.Title.Text = "효율(%)";

                this.chart1.Extensions.Clear();
                this.chart1.LegendBox.CustomItems.Clear();


                double min = 0;
                double max = 0;
                double r_efficiency = 0;
                double diff = 0;

                if (this.ultraGrid1.Rows.Count == 0)
                {
                    return;
                }

                r_efficiency = Utils.ToDouble(this.ultraGrid1.Rows[0].Cells["R_EFFICIENCY"].Value);
                diff = Utils.ToDouble(this.ultraGrid1.Rows[0].Cells["DIFF"].Value);

                if (r_efficiency == 0 || diff == 0)
                {
                    return;
                }

                min = r_efficiency - diff;
                max = r_efficiency + diff;
 
                Annotations annots = new Annotations();
                AnnotationArrow low_arrow = new AnnotationArrow();
                AnnotationArrow high_arrow = new AnnotationArrow();

                annots.List.Add(low_arrow);
                annots.List.Add(high_arrow);

                this.chart1.Extensions.Add(annots);

                low_arrow.Border.Color = Color.Red;
                low_arrow.Border.Width = 2;
                low_arrow.Height = 0;
                low_arrow.Width = 10000;
                low_arrow.Anchor = AnchorStyles.None;
                low_arrow.Attach(this.chart1.Data.Points / 2, min);

                high_arrow.Border.Color = Color.Green;
                high_arrow.Border.Width = 2;
                high_arrow.Height = 0;
                high_arrow.Width = 10000;
                high_arrow.Anchor = AnchorStyles.None;
                high_arrow.Attach(this.chart1.Data.Points / 2, max);

                CustomLegendItem c1 = new CustomLegendItem();
                c1.MarkerShape = MarkerShape.None;
                c1.AlternateColor = Color.Red;
                c1.Border.Color = Color.Red;
                c1.Border.Width = 2;
                c1.Color = Color.Red;
                c1.ShowLine = true;
                c1.MarkerShape = MarkerShape.None;
                c1.Text = "최소한계선";

                CustomLegendItem c2 = new CustomLegendItem();
                c2.MarkerShape = MarkerShape.None;
                c2.AlternateColor = Color.Green;
                c2.Border.Color = Color.Green;
                c2.Border.Width = 2;
                c2.Color = Color.Green;
                c2.ShowLine = true;
                c2.Text = "최대한계선";

                this.chart1.LegendBox.CustomItems.Add(c1);
                this.chart1.LegendBox.CustomItems.Add(c2);
            }
        }
    }
}
