﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.work;
using WaterNet.WE_PumpPerformanceSimulationAnalysis.dao;
using System.Collections;
using System.Data;
using WaterNet.WE_Common.data;

namespace WaterNet.WE_PumpPerformanceSimulationAnalysis.work
{
    public class SimulationAnalysisWork : BaseWork
    {
        private static SimulationAnalysisWork work = null;
        private SimulationAnalysisDao dao = null;

        private SimulationAnalysisWork()
        {
            dao = SimulationAnalysisDao.GetInstance();
        }

        public static SimulationAnalysisWork GetInstance()
        {
            if (work == null)
            {
                work = new SimulationAnalysisWork();
            }
            return work;
        }

        public object SelectSimulationTime()
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = dao.SelectSimulationTime(base.DataBaseManager);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectSimulationAnalysisResult(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectSimulationAnalysisResult(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                        result.Columns.Add("W_ENERGY", typeof(double));
                        result.Columns.Add("O_EFFICIENCY", typeof(double));
                        result.Columns.Add("R_O", typeof(double));
                        result.Columns.Add("RESULT");

                        foreach (DataRow row in result.Rows)
                        {
                            SimulationAnalysisData data = new SimulationAnalysisData(row);
                            if (data.H == 0)
                            {
                                row["H"] = DBNull.Value;
                            }
                            else
                            {
                                row["H"] = data.H;
                            }

                            if (data.W_ENERGY == 0)
                            {
                                row["W_ENERGY"] = DBNull.Value;
                            }
                            else
                            {
                                row["W_ENERGY"] = data.W_ENERGY;
                            }

                            if (data.O_EFFICIENCY == 0)
                            {
                                row["O_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["O_EFFICIENCY"] = data.O_EFFICIENCY;
                            }

                            if (data.R_EFFICIENCY == 0)
                            {
                                row["R_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_EFFICIENCY"] = data.R_EFFICIENCY;
                            }

                            if (data.R_O == 0)
                            {
                                row["R_O"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_O"] = data.R_O;
                            }

                            row["RESULT"] = data.RESULT;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectSimulationAnalysisResultDetail(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectSimulationAnalysisResultDetail(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                        result.Columns.Add("W_ENERGY", typeof(double));
                        result.Columns.Add("O_EFFICIENCY", typeof(double));
                        result.Columns.Add("R_O", typeof(double));
                        result.Columns.Add("RESULT");

                        foreach (DataRow row in result.Rows)
                        {
                            SimulationAnalysisData data = new SimulationAnalysisData(row);
                            if (data.H == 0)
                            {
                                row["H"] = DBNull.Value;
                            }
                            else
                            {
                                row["H"] = data.H;
                            }

                            if (data.W_ENERGY == 0)
                            {
                                row["W_ENERGY"] = DBNull.Value;
                            }
                            else
                            {
                                row["W_ENERGY"] = data.W_ENERGY;
                            }

                            if (data.O_EFFICIENCY == 0)
                            {
                                row["O_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["O_EFFICIENCY"] = data.O_EFFICIENCY;
                            }

                            if (data.R_EFFICIENCY == 0)
                            {
                                row["R_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_EFFICIENCY"] = data.R_EFFICIENCY;
                            }

                            if (data.R_O == 0)
                            {
                                row["R_O"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_O"] = data.R_O;
                            }

                            row["RESULT"] = data.RESULT;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectRealSimulationAnalysis(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectRealSimulationAnalysis(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                        result.Columns.Add("W_ENERGY", typeof(double));
                        result.Columns.Add("O_EFFICIENCY", typeof(double));
                        result.Columns.Add("R_O", typeof(double));
                        result.Columns.Add("RESULT");

                        foreach (DataRow row in result.Rows)
                        {
                            SimulationAnalysisData data = new SimulationAnalysisData(row);
                            if (data.H == 0)
                            {
                                row["H"] = DBNull.Value;
                            }
                            else
                            {
                                row["H"] = data.H;
                            }

                            if (data.W_ENERGY == 0)
                            {
                                row["W_ENERGY"] = DBNull.Value;
                            }
                            else
                            {
                                row["W_ENERGY"] = data.W_ENERGY;
                            }

                            if (data.O_EFFICIENCY == 0)
                            {
                                row["O_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["O_EFFICIENCY"] = data.O_EFFICIENCY;
                            }

                            if (data.R_EFFICIENCY == 0)
                            {
                                row["R_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_EFFICIENCY"] = data.R_EFFICIENCY;
                            }

                            if (data.R_O == 0)
                            {
                                row["R_O"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_O"] = data.R_O;
                            }

                            row["RESULT"] = data.RESULT;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectRealSimulationAnalysisResult(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectRealSimulationAnalysisResult(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                        result.Columns.Add("W_ENERGY", typeof(double));
                        result.Columns.Add("O_EFFICIENCY", typeof(double));
                        result.Columns.Add("R_O", typeof(double));
                        result.Columns.Add("RESULT");

                        foreach (DataRow row in result.Rows)
                        {
                            SimulationAnalysisData data = new SimulationAnalysisData(row);
                            if (data.H == 0)
                            {
                                row["H"] = DBNull.Value;
                            }
                            else
                            {
                                row["H"] = data.H;
                            }

                            if (data.W_ENERGY == 0)
                            {
                                row["W_ENERGY"] = DBNull.Value;
                            }
                            else
                            {
                                row["W_ENERGY"] = data.W_ENERGY;
                            }

                            if (data.O_EFFICIENCY == 0)
                            {
                                row["O_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["O_EFFICIENCY"] = data.O_EFFICIENCY;
                            }

                            if (data.R_EFFICIENCY == 0)
                            {
                                row["R_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_EFFICIENCY"] = data.R_EFFICIENCY;
                            }

                            if (data.R_O == 0)
                            {
                                row["R_O"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_O"] = data.R_O;
                            }

                            row["RESULT"] = data.RESULT;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
