﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WaterNet.WE_BaseInfoSet.Dao
{

    public class BaseInfoSetDao
    {
        private static BaseInfoSetDao dao = null;

        private BaseInfoSetDao() { }

        public static BaseInfoSetDao GetInstance()
        {
            if (dao == null)
            {
                dao = new BaseInfoSetDao();
            }
            return dao;
        }

        public DataTable Get_EnergyModelNumber(EMFrame.dm.EMapper mapper, string query, Hashtable param)
        {
            DataTable datatable = mapper.ExecuteScriptDataTable(query.ToString(), param);

            return datatable;
        }

        public DataTable Get_EnergyPumpInfo(EMFrame.dm.EMapper mapper, string query, Hashtable param)
        {
            DataTable datatable = mapper.ExecuteScriptDataTable(query.ToString(), param);

            return datatable;
        }

        public DataTable Get_EnergyCheckPointInfo(EMFrame.dm.EMapper mapper, string query, Hashtable param)
        {
            DataTable datatable = mapper.ExecuteScriptDataTable(query.ToString(), param);

            return datatable;
        }

    }
}
