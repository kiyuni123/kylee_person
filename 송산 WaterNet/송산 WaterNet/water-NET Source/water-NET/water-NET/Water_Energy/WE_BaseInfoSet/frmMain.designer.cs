﻿namespace WaterNet.WE_BaseInfoSet
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "",
            ""}, -1);
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.axToolbar = new ESRI.ArcGIS.Controls.AxToolbarControl();
            this.spcContents = new System.Windows.Forms.SplitContainer();
            this.spcIndexMap = new System.Windows.Forms.SplitContainer();
            this.axIndexMap = new ESRI.ArcGIS.Controls.AxMapControl();
            this.spcTOC = new System.Windows.Forms.SplitContainer();
            this.tabTOC = new System.Windows.Forms.TabControl();
            this.tabPageTOC = new System.Windows.Forms.TabPage();
            this.axTOC = new ESRI.ArcGIS.Controls.AxTOCControl();
            this.tabPageBlock = new System.Windows.Forms.TabPage();
            this.tvBlock = new System.Windows.Forms.TreeView();
            this.tabPageBldg = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listBldg = new System.Windows.Forms.ListView();
            this.columnBLD_NAM = new System.Windows.Forms.ColumnHeader();
            this.columnBJD_CDE = new System.Windows.Forms.ColumnHeader();
            this.columnAddr = new System.Windows.Forms.ColumnHeader();
            this.columnOID = new System.Windows.Forms.ColumnHeader();
            this.panelBldg = new System.Windows.Forms.Panel();
            this.btnSearchBldg = new System.Windows.Forms.Button();
            this.txtBldg = new System.Windows.Forms.TextBox();
            this.tabPageAddr = new System.Windows.Forms.TabPage();
            this.listAddr = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.panelAddr = new System.Windows.Forms.Panel();
            this.lblRi = new System.Windows.Forms.Label();
            this.cboRi = new System.Windows.Forms.ComboBox();
            this.txtBubun = new System.Windows.Forms.TextBox();
            this.checkSan = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDong = new System.Windows.Forms.Label();
            this.cboDong = new System.Windows.Forms.ComboBox();
            this.btnSearchAddr = new System.Windows.Forms.Button();
            this.txtBonbun = new System.Windows.Forms.TextBox();
            this.tabPageCust = new System.Windows.Forms.TabPage();
            this.listCust = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCustName = new System.Windows.Forms.TextBox();
            this.lblNo1 = new System.Windows.Forms.Label();
            this.txtNo1 = new System.Windows.Forms.TextBox();
            this.rdCustNo = new System.Windows.Forms.RadioButton();
            this.rdCustName = new System.Windows.Forms.RadioButton();
            this.txtNo3 = new System.Windows.Forms.TextBox();
            this.lblNo2 = new System.Windows.Forms.Label();
            this.btnSearchCust = new System.Windows.Forms.Button();
            this.txtNo2 = new System.Windows.Forms.TextBox();
            this.axMap = new ESRI.ArcGIS.Controls.AxMapControl();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolActionCommand = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStripTOC = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemScale = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSymbol = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLabel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemTransfy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuItemAllInit = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemSelInit = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ug_pump = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ug_checkpoint = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.rdJunction = new System.Windows.Forms.RadioButton();
            this.rdValve = new System.Windows.Forms.RadioButton();
            this.rdPump = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cboPumpStation = new System.Windows.Forms.ComboBox();
            this.btnStation = new System.Windows.Forms.Button();
            this.tsBasemap = new System.Windows.Forms.ToolStripButton();
            this.tsSatelImage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            this.tabPageBlock.SuspendLayout();
            this.tabPageBldg.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelBldg.SuspendLayout();
            this.tabPageAddr.SuspendLayout();
            this.panelAddr.SuspendLayout();
            this.tabPageCust.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStripTOC.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ug_pump)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ug_checkpoint)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // axToolbar
            // 
            this.axToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.axToolbar.Location = new System.Drawing.Point(1, 3);
            this.axToolbar.Name = "axToolbar";
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            this.axToolbar.Size = new System.Drawing.Size(685, 28);
            this.axToolbar.TabIndex = 10;
            this.axToolbar.OnItemClick += new ESRI.ArcGIS.Controls.IToolbarControlEvents_Ax_OnItemClickEventHandler(this.axToolbar_OnItemClick);
            // 
            // spcContents
            // 
            this.spcContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcContents.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcContents.Location = new System.Drawing.Point(0, 0);
            this.spcContents.Margin = new System.Windows.Forms.Padding(1);
            this.spcContents.Name = "spcContents";
            // 
            // spcContents.Panel1
            // 
            this.spcContents.Panel1.Controls.Add(this.spcIndexMap);
            // 
            // spcContents.Panel2
            // 
            this.spcContents.Panel2.Controls.Add(this.axMap);
            this.spcContents.Panel2.Controls.Add(this.toolStrip1);
            this.spcContents.Panel2.Controls.Add(this.axToolbar);
            this.spcContents.Panel2.Padding = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.spcContents.Size = new System.Drawing.Size(948, 437);
            this.spcContents.SplitterDistance = 257;
            this.spcContents.SplitterWidth = 2;
            this.spcContents.TabIndex = 9;
            // 
            // spcIndexMap
            // 
            this.spcIndexMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcIndexMap.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcIndexMap.Location = new System.Drawing.Point(0, 0);
            this.spcIndexMap.Name = "spcIndexMap";
            this.spcIndexMap.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcIndexMap.Panel1
            // 
            this.spcIndexMap.Panel1.Controls.Add(this.axIndexMap);
            this.spcIndexMap.Panel1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 1);
            // 
            // spcIndexMap.Panel2
            // 
            this.spcIndexMap.Panel2.Controls.Add(this.spcTOC);
            this.spcIndexMap.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.spcIndexMap.Size = new System.Drawing.Size(257, 437);
            this.spcIndexMap.SplitterDistance = 173;
            this.spcIndexMap.SplitterWidth = 2;
            this.spcIndexMap.TabIndex = 0;
            // 
            // axIndexMap
            // 
            this.axIndexMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axIndexMap.Location = new System.Drawing.Point(4, 4);
            this.axIndexMap.Name = "axIndexMap";
            this.axIndexMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axIndexMap.OcxState")));
            this.axIndexMap.Size = new System.Drawing.Size(249, 168);
            this.axIndexMap.TabIndex = 6;
            this.axIndexMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axIndexMap_OnMouseDown);
            this.axIndexMap.OnAfterDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterDrawEventHandler(this.axIndexMap_OnAfterDraw);
            // 
            // spcTOC
            // 
            this.spcTOC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcTOC.Location = new System.Drawing.Point(3, 3);
            this.spcTOC.Name = "spcTOC";
            this.spcTOC.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.spcTOC.Panel1Collapsed = true;
            // 
            // spcTOC.Panel2
            // 
            this.spcTOC.Panel2.Controls.Add(this.tabTOC);
            this.spcTOC.Size = new System.Drawing.Size(251, 256);
            this.spcTOC.SplitterDistance = 114;
            this.spcTOC.TabIndex = 10;
            // 
            // tabTOC
            // 
            this.tabTOC.Controls.Add(this.tabPageTOC);
            this.tabTOC.Controls.Add(this.tabPageBlock);
            this.tabTOC.Controls.Add(this.tabPageBldg);
            this.tabTOC.Controls.Add(this.tabPageAddr);
            this.tabTOC.Controls.Add(this.tabPageCust);
            this.tabTOC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabTOC.Location = new System.Drawing.Point(0, 0);
            this.tabTOC.Name = "tabTOC";
            this.tabTOC.SelectedIndex = 0;
            this.tabTOC.Size = new System.Drawing.Size(251, 256);
            this.tabTOC.TabIndex = 0;
            this.tabTOC.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabTOC_Selected);
            // 
            // tabPageTOC
            // 
            this.tabPageTOC.BackColor = System.Drawing.Color.Transparent;
            this.tabPageTOC.Controls.Add(this.axTOC);
            this.tabPageTOC.Location = new System.Drawing.Point(4, 22);
            this.tabPageTOC.Name = "tabPageTOC";
            this.tabPageTOC.Size = new System.Drawing.Size(243, 230);
            this.tabPageTOC.TabIndex = 0;
            this.tabPageTOC.Text = "레이어";
            this.tabPageTOC.UseVisualStyleBackColor = true;
            // 
            // axTOC
            // 
            this.axTOC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axTOC.Location = new System.Drawing.Point(0, 0);
            this.axTOC.Name = "axTOC";
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            this.axTOC.Padding = new System.Windows.Forms.Padding(4);
            this.axTOC.Size = new System.Drawing.Size(243, 230);
            this.axTOC.TabIndex = 12;
            this.axTOC.OnMouseDown += new ESRI.ArcGIS.Controls.ITOCControlEvents_Ax_OnMouseDownEventHandler(this.axTOC_OnMouseDown);
            // 
            // tabPageBlock
            // 
            this.tabPageBlock.BackColor = System.Drawing.Color.Transparent;
            this.tabPageBlock.Controls.Add(this.tvBlock);
            this.tabPageBlock.Location = new System.Drawing.Point(4, 22);
            this.tabPageBlock.Name = "tabPageBlock";
            this.tabPageBlock.Size = new System.Drawing.Size(243, 230);
            this.tabPageBlock.TabIndex = 2;
            this.tabPageBlock.Text = "블록";
            this.tabPageBlock.UseVisualStyleBackColor = true;
            // 
            // tvBlock
            // 
            this.tvBlock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvBlock.FullRowSelect = true;
            this.tvBlock.Location = new System.Drawing.Point(0, 0);
            this.tvBlock.Name = "tvBlock";
            this.tvBlock.ShowNodeToolTips = true;
            this.tvBlock.Size = new System.Drawing.Size(243, 230);
            this.tvBlock.TabIndex = 0;
            this.tvBlock.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvBlock_AfterSelect);
            // 
            // tabPageBldg
            // 
            this.tabPageBldg.BackColor = System.Drawing.Color.Transparent;
            this.tabPageBldg.Controls.Add(this.panel2);
            this.tabPageBldg.Controls.Add(this.panelBldg);
            this.tabPageBldg.Location = new System.Drawing.Point(4, 22);
            this.tabPageBldg.Name = "tabPageBldg";
            this.tabPageBldg.Size = new System.Drawing.Size(243, 230);
            this.tabPageBldg.TabIndex = 3;
            this.tabPageBldg.Text = "건물";
            this.tabPageBldg.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.listBldg);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(243, 179);
            this.panel2.TabIndex = 3;
            // 
            // listBldg
            // 
            this.listBldg.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnBLD_NAM,
            this.columnBJD_CDE,
            this.columnAddr,
            this.columnOID});
            this.listBldg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBldg.FullRowSelect = true;
            this.listBldg.GridLines = true;
            this.listBldg.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listBldg.Location = new System.Drawing.Point(0, 0);
            this.listBldg.MultiSelect = false;
            this.listBldg.Name = "listBldg";
            this.listBldg.Size = new System.Drawing.Size(243, 179);
            this.listBldg.TabIndex = 0;
            this.listBldg.UseCompatibleStateImageBehavior = false;
            this.listBldg.View = System.Windows.Forms.View.Details;
            this.listBldg.DoubleClick += new System.EventHandler(this.listBldg_DoubleClick);
            // 
            // columnBLD_NAM
            // 
            this.columnBLD_NAM.Text = "이름";
            this.columnBLD_NAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnBLD_NAM.Width = 220;
            // 
            // columnBJD_CDE
            // 
            this.columnBJD_CDE.Text = "동";
            this.columnBJD_CDE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnBJD_CDE.Width = 0;
            // 
            // columnAddr
            // 
            this.columnAddr.Text = "주소";
            this.columnAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnAddr.Width = 0;
            // 
            // columnOID
            // 
            this.columnOID.Text = "OID";
            this.columnOID.Width = 0;
            // 
            // panelBldg
            // 
            this.panelBldg.Controls.Add(this.btnSearchBldg);
            this.panelBldg.Controls.Add(this.txtBldg);
            this.panelBldg.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBldg.Location = new System.Drawing.Point(0, 0);
            this.panelBldg.Name = "panelBldg";
            this.panelBldg.Size = new System.Drawing.Size(243, 51);
            this.panelBldg.TabIndex = 2;
            // 
            // btnSearchBldg
            // 
            this.btnSearchBldg.Location = new System.Drawing.Point(173, 14);
            this.btnSearchBldg.Name = "btnSearchBldg";
            this.btnSearchBldg.Size = new System.Drawing.Size(52, 22);
            this.btnSearchBldg.TabIndex = 3;
            this.btnSearchBldg.Text = "검색";
            this.btnSearchBldg.UseVisualStyleBackColor = true;
            this.btnSearchBldg.Click += new System.EventHandler(this.btnSearchBldg_Click);
            // 
            // txtBldg
            // 
            this.txtBldg.Location = new System.Drawing.Point(12, 14);
            this.txtBldg.Name = "txtBldg";
            this.txtBldg.Size = new System.Drawing.Size(155, 21);
            this.txtBldg.TabIndex = 2;
            // 
            // tabPageAddr
            // 
            this.tabPageAddr.BackColor = System.Drawing.Color.Transparent;
            this.tabPageAddr.Controls.Add(this.listAddr);
            this.tabPageAddr.Controls.Add(this.panelAddr);
            this.tabPageAddr.Location = new System.Drawing.Point(4, 22);
            this.tabPageAddr.Name = "tabPageAddr";
            this.tabPageAddr.Size = new System.Drawing.Size(243, 230);
            this.tabPageAddr.TabIndex = 1;
            this.tabPageAddr.Text = "주소";
            this.tabPageAddr.UseVisualStyleBackColor = true;
            // 
            // listAddr
            // 
            this.listAddr.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listAddr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listAddr.FullRowSelect = true;
            this.listAddr.GridLines = true;
            this.listAddr.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listAddr.Location = new System.Drawing.Point(0, 86);
            this.listAddr.MultiSelect = false;
            this.listAddr.Name = "listAddr";
            this.listAddr.Size = new System.Drawing.Size(243, 144);
            this.listAddr.TabIndex = 4;
            this.listAddr.UseCompatibleStateImageBehavior = false;
            this.listAddr.View = System.Windows.Forms.View.Details;
            this.listAddr.DoubleClick += new System.EventHandler(this.listAddr_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "주소";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 220;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "OID";
            this.columnHeader2.Width = 0;
            // 
            // panelAddr
            // 
            this.panelAddr.Controls.Add(this.lblRi);
            this.panelAddr.Controls.Add(this.cboRi);
            this.panelAddr.Controls.Add(this.txtBubun);
            this.panelAddr.Controls.Add(this.checkSan);
            this.panelAddr.Controls.Add(this.label4);
            this.panelAddr.Controls.Add(this.lblDong);
            this.panelAddr.Controls.Add(this.cboDong);
            this.panelAddr.Controls.Add(this.btnSearchAddr);
            this.panelAddr.Controls.Add(this.txtBonbun);
            this.panelAddr.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAddr.Location = new System.Drawing.Point(0, 0);
            this.panelAddr.Name = "panelAddr";
            this.panelAddr.Size = new System.Drawing.Size(243, 86);
            this.panelAddr.TabIndex = 3;
            // 
            // lblRi
            // 
            this.lblRi.Location = new System.Drawing.Point(11, 37);
            this.lblRi.Name = "lblRi";
            this.lblRi.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblRi.Size = new System.Drawing.Size(56, 14);
            this.lblRi.TabIndex = 13;
            this.lblRi.Text = "리";
            this.lblRi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboRi
            // 
            this.cboRi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRi.FormattingEnabled = true;
            this.cboRi.Location = new System.Drawing.Point(73, 32);
            this.cboRi.Name = "cboRi";
            this.cboRi.Size = new System.Drawing.Size(159, 20);
            this.cboRi.TabIndex = 12;
            // 
            // txtBubun
            // 
            this.txtBubun.Location = new System.Drawing.Point(132, 58);
            this.txtBubun.Name = "txtBubun";
            this.txtBubun.Size = new System.Drawing.Size(38, 21);
            this.txtBubun.TabIndex = 11;
            // 
            // checkSan
            // 
            this.checkSan.AutoSize = true;
            this.checkSan.Location = new System.Drawing.Point(11, 60);
            this.checkSan.Name = "checkSan";
            this.checkSan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkSan.Size = new System.Drawing.Size(36, 16);
            this.checkSan.TabIndex = 10;
            this.checkSan.Text = "산";
            this.checkSan.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(116, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "-";
            // 
            // lblDong
            // 
            this.lblDong.Location = new System.Drawing.Point(11, 11);
            this.lblDong.Name = "lblDong";
            this.lblDong.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDong.Size = new System.Drawing.Size(56, 14);
            this.lblDong.TabIndex = 6;
            this.lblDong.Text = "읍/면/동";
            this.lblDong.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboDong
            // 
            this.cboDong.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDong.FormattingEnabled = true;
            this.cboDong.Location = new System.Drawing.Point(73, 6);
            this.cboDong.Name = "cboDong";
            this.cboDong.Size = new System.Drawing.Size(159, 20);
            this.cboDong.TabIndex = 4;
            this.cboDong.SelectedIndexChanged += new System.EventHandler(this.cboDong_SelectedIndexChanged);
            // 
            // btnSearchAddr
            // 
            this.btnSearchAddr.Location = new System.Drawing.Point(182, 58);
            this.btnSearchAddr.Name = "btnSearchAddr";
            this.btnSearchAddr.Size = new System.Drawing.Size(52, 22);
            this.btnSearchAddr.TabIndex = 3;
            this.btnSearchAddr.Text = "검색";
            this.btnSearchAddr.UseVisualStyleBackColor = true;
            this.btnSearchAddr.Click += new System.EventHandler(this.btnSearchAddr_Click);
            // 
            // txtBonbun
            // 
            this.txtBonbun.Location = new System.Drawing.Point(59, 58);
            this.txtBonbun.Name = "txtBonbun";
            this.txtBonbun.Size = new System.Drawing.Size(53, 21);
            this.txtBonbun.TabIndex = 2;
            // 
            // tabPageCust
            // 
            this.tabPageCust.Controls.Add(this.listCust);
            this.tabPageCust.Controls.Add(this.panel1);
            this.tabPageCust.Location = new System.Drawing.Point(4, 22);
            this.tabPageCust.Name = "tabPageCust";
            this.tabPageCust.Size = new System.Drawing.Size(243, 230);
            this.tabPageCust.TabIndex = 4;
            this.tabPageCust.Text = "수용가";
            this.tabPageCust.UseVisualStyleBackColor = true;
            // 
            // listCust
            // 
            this.listCust.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader5});
            this.listCust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listCust.FullRowSelect = true;
            this.listCust.GridLines = true;
            this.listCust.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listCust.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.listCust.Location = new System.Drawing.Point(0, 65);
            this.listCust.MultiSelect = false;
            this.listCust.Name = "listCust";
            this.listCust.Size = new System.Drawing.Size(243, 165);
            this.listCust.TabIndex = 5;
            this.listCust.UseCompatibleStateImageBehavior = false;
            this.listCust.View = System.Windows.Forms.View.Details;
            this.listCust.DoubleClick += new System.EventHandler(this.listCust_DoubleClick);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "수용가명";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "수용가번호";
            this.columnHeader5.Width = 130;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtCustName);
            this.panel1.Controls.Add(this.lblNo1);
            this.panel1.Controls.Add(this.txtNo1);
            this.panel1.Controls.Add(this.rdCustNo);
            this.panel1.Controls.Add(this.rdCustName);
            this.panel1.Controls.Add(this.txtNo3);
            this.panel1.Controls.Add(this.lblNo2);
            this.panel1.Controls.Add(this.btnSearchCust);
            this.panel1.Controls.Add(this.txtNo2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(243, 65);
            this.panel1.TabIndex = 4;
            // 
            // txtCustName
            // 
            this.txtCustName.Location = new System.Drawing.Point(9, 32);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new System.Drawing.Size(168, 21);
            this.txtCustName.TabIndex = 18;
            // 
            // lblNo1
            // 
            this.lblNo1.AutoSize = true;
            this.lblNo1.Location = new System.Drawing.Point(59, 37);
            this.lblNo1.Name = "lblNo1";
            this.lblNo1.Size = new System.Drawing.Size(11, 12);
            this.lblNo1.TabIndex = 17;
            this.lblNo1.Text = "-";
            this.lblNo1.Visible = false;
            // 
            // txtNo1
            // 
            this.txtNo1.Enabled = false;
            this.txtNo1.Location = new System.Drawing.Point(9, 32);
            this.txtNo1.MaxLength = 5;
            this.txtNo1.Name = "txtNo1";
            this.txtNo1.Size = new System.Drawing.Size(45, 21);
            this.txtNo1.TabIndex = 1;
            this.txtNo1.Text = "45180";
            this.txtNo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNo1.Visible = false;
            // 
            // rdCustNo
            // 
            this.rdCustNo.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdCustNo.Location = new System.Drawing.Point(132, 7);
            this.rdCustNo.Name = "rdCustNo";
            this.rdCustNo.Size = new System.Drawing.Size(92, 18);
            this.rdCustNo.TabIndex = 15;
            this.rdCustNo.Text = "수용가번호";
            this.rdCustNo.UseVisualStyleBackColor = true;
            this.rdCustNo.CheckedChanged += new System.EventHandler(this.rdCustNo_CheckedChanged);
            // 
            // rdCustName
            // 
            this.rdCustName.Checked = true;
            this.rdCustName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdCustName.Location = new System.Drawing.Point(15, 7);
            this.rdCustName.Name = "rdCustName";
            this.rdCustName.Size = new System.Drawing.Size(104, 18);
            this.rdCustName.TabIndex = 14;
            this.rdCustName.TabStop = true;
            this.rdCustName.Text = "수용가명";
            this.rdCustName.UseVisualStyleBackColor = true;
            this.rdCustName.CheckedChanged += new System.EventHandler(this.rdCustName_CheckedChanged);
            // 
            // txtNo3
            // 
            this.txtNo3.Location = new System.Drawing.Point(122, 32);
            this.txtNo3.MaxLength = 5;
            this.txtNo3.Name = "txtNo3";
            this.txtNo3.Size = new System.Drawing.Size(55, 21);
            this.txtNo3.TabIndex = 3;
            this.txtNo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNo3.Visible = false;
            // 
            // lblNo2
            // 
            this.lblNo2.AutoSize = true;
            this.lblNo2.Location = new System.Drawing.Point(105, 38);
            this.lblNo2.Name = "lblNo2";
            this.lblNo2.Size = new System.Drawing.Size(11, 12);
            this.lblNo2.TabIndex = 9;
            this.lblNo2.Text = "-";
            this.lblNo2.Visible = false;
            // 
            // btnSearchCust
            // 
            this.btnSearchCust.Location = new System.Drawing.Point(182, 31);
            this.btnSearchCust.Name = "btnSearchCust";
            this.btnSearchCust.Size = new System.Drawing.Size(52, 22);
            this.btnSearchCust.TabIndex = 3;
            this.btnSearchCust.Text = "검색";
            this.btnSearchCust.UseVisualStyleBackColor = true;
            this.btnSearchCust.Click += new System.EventHandler(this.btnSearchCust_Click);
            // 
            // txtNo2
            // 
            this.txtNo2.Location = new System.Drawing.Point(78, 32);
            this.txtNo2.MaxLength = 2;
            this.txtNo2.Name = "txtNo2";
            this.txtNo2.Size = new System.Drawing.Size(23, 21);
            this.txtNo2.TabIndex = 2;
            this.txtNo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNo2.Visible = false;
            // 
            // axMap
            // 
            this.axMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axMap.Location = new System.Drawing.Point(1, 56);
            this.axMap.Name = "axMap";
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Padding = new System.Windows.Forms.Padding(4);
            this.axMap.Size = new System.Drawing.Size(685, 378);
            this.axMap.TabIndex = 16;
            this.axMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axMap_OnMouseDown);
            this.axMap.OnMouseMove += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseMoveEventHandler(this.axMap_OnMouseMove);
            this.axMap.OnAfterDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterDrawEventHandler(this.axMap_OnAfterDraw);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolActionCommand,
            this.toolStripSeparator2,
            this.tsBasemap,
            this.tsSatelImage});
            this.toolStrip1.Location = new System.Drawing.Point(1, 31);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(685, 25);
            this.toolStrip1.TabIndex = 15;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolActionCommand
            // 
            this.toolActionCommand.CheckOnClick = true;
            this.toolActionCommand.Image = ((System.Drawing.Image)(resources.GetObject("toolActionCommand.Image")));
            this.toolActionCommand.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolActionCommand.Name = "toolActionCommand";
            this.toolActionCommand.Size = new System.Drawing.Size(49, 22);
            this.toolActionCommand.Text = "선택";
            this.toolActionCommand.ToolTipText = "사용자 정의 기능을 수행합니다.";
            this.toolActionCommand.Click += new System.EventHandler(this.toolActionCommand_Click);
            // 
            // contextMenuStripTOC
            // 
            this.contextMenuStripTOC.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemScale,
            this.toolStripMenuItemSymbol,
            this.toolStripMenuItemLabel,
            this.toolStripMenuItemTransfy,
            this.toolStripSeparator1,
            this.ToolStripMenuItemAllInit,
            this.ToolStripMenuItemSelInit});
            this.contextMenuStripTOC.Name = "contextMenuStripTOC";
            this.contextMenuStripTOC.Size = new System.Drawing.Size(177, 142);
            // 
            // toolStripMenuItemScale
            // 
            this.toolStripMenuItemScale.Name = "toolStripMenuItemScale";
            this.toolStripMenuItemScale.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItemScale.Text = "유효축척 설정";
            this.toolStripMenuItemScale.Click += new System.EventHandler(this.toolStripMenuItemScale_Click);
            // 
            // toolStripMenuItemSymbol
            // 
            this.toolStripMenuItemSymbol.Name = "toolStripMenuItemSymbol";
            this.toolStripMenuItemSymbol.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItemSymbol.Text = "심볼 설정";
            this.toolStripMenuItemSymbol.Click += new System.EventHandler(this.toolStripMenuItemSymbol_Click);
            // 
            // toolStripMenuItemLabel
            // 
            this.toolStripMenuItemLabel.Name = "toolStripMenuItemLabel";
            this.toolStripMenuItemLabel.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItemLabel.Text = "주석 설정";
            this.toolStripMenuItemLabel.Click += new System.EventHandler(this.toolStripMenuItemLabel_Click);
            // 
            // toolStripMenuItemTransfy
            // 
            this.toolStripMenuItemTransfy.Name = "toolStripMenuItemTransfy";
            this.toolStripMenuItemTransfy.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItemTransfy.Text = "투명도 설정";
            this.toolStripMenuItemTransfy.Click += new System.EventHandler(this.toolStripMenuItemTransfy_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(173, 6);
            // 
            // ToolStripMenuItemAllInit
            // 
            this.ToolStripMenuItemAllInit.Name = "ToolStripMenuItemAllInit";
            this.ToolStripMenuItemAllInit.Size = new System.Drawing.Size(176, 22);
            this.ToolStripMenuItemAllInit.Text = "레이어초기화(전체)";
            this.ToolStripMenuItemAllInit.Click += new System.EventHandler(this.ToolStripMenuItemAllInit_Click);
            // 
            // ToolStripMenuItemSelInit
            // 
            this.ToolStripMenuItemSelInit.Name = "ToolStripMenuItemSelInit";
            this.ToolStripMenuItemSelInit.Size = new System.Drawing.Size(176, 22);
            this.ToolStripMenuItemSelInit.Text = "레이어초기화(선택)";
            this.ToolStripMenuItemSelInit.Click += new System.EventHandler(this.ToolStripMenuItemSelInit_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.spcContents);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(948, 666);
            this.splitContainer1.SplitterDistance = 437;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(948, 201);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ug_pump);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(940, 175);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "펌프정보";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ug_pump
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ug_pump.DisplayLayout.Appearance = appearance25;
            this.ug_pump.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ug_pump.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ug_pump.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ug_pump.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ug_pump.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ug_pump.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ug_pump.DisplayLayout.MaxColScrollRegions = 1;
            this.ug_pump.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ug_pump.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ug_pump.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ug_pump.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ug_pump.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ug_pump.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ug_pump.DisplayLayout.Override.CellAppearance = appearance32;
            this.ug_pump.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ug_pump.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ug_pump.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ug_pump.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ug_pump.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ug_pump.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ug_pump.DisplayLayout.Override.RowAppearance = appearance35;
            this.ug_pump.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ug_pump.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ug_pump.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ug_pump.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ug_pump.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ug_pump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ug_pump.Location = new System.Drawing.Point(3, 3);
            this.ug_pump.Name = "ug_pump";
            this.ug_pump.Size = new System.Drawing.Size(934, 169);
            this.ug_pump.TabIndex = 1;
            this.ug_pump.Text = "ultraGrid2";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ug_checkpoint);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(940, 175);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "감시지점";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ug_checkpoint
            // 
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ug_checkpoint.DisplayLayout.Appearance = appearance16;
            this.ug_checkpoint.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ug_checkpoint.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.ug_checkpoint.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ug_checkpoint.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.ug_checkpoint.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ug_checkpoint.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.ug_checkpoint.DisplayLayout.MaxColScrollRegions = 1;
            this.ug_checkpoint.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ug_checkpoint.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ug_checkpoint.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.ug_checkpoint.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ug_checkpoint.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.ug_checkpoint.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ug_checkpoint.DisplayLayout.Override.CellAppearance = appearance17;
            this.ug_checkpoint.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ug_checkpoint.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ug_checkpoint.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance23.TextHAlignAsString = "Left";
            this.ug_checkpoint.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.ug_checkpoint.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ug_checkpoint.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.ug_checkpoint.DisplayLayout.Override.RowAppearance = appearance22;
            this.ug_checkpoint.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ug_checkpoint.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.ug_checkpoint.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ug_checkpoint.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ug_checkpoint.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ug_checkpoint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ug_checkpoint.Location = new System.Drawing.Point(3, 3);
            this.ug_checkpoint.Name = "ug_checkpoint";
            this.ug_checkpoint.Size = new System.Drawing.Size(934, 169);
            this.ug_checkpoint.TabIndex = 1;
            this.ug_checkpoint.Text = "ultraGrid3";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnAdd);
            this.panel3.Controls.Add(this.btnDelete);
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Controls.Add(this.rdJunction);
            this.panel3.Controls.Add(this.rdValve);
            this.panel3.Controls.Add(this.rdPump);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.cboPumpStation);
            this.panel3.Controls.Add(this.btnStation);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(948, 26);
            this.panel3.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(697, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(78, 20);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "추가";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(779, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(78, 20);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "삭제";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(860, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(78, 20);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rdJunction
            // 
            this.rdJunction.AutoSize = true;
            this.rdJunction.Location = new System.Drawing.Point(495, 6);
            this.rdJunction.Name = "rdJunction";
            this.rdJunction.Size = new System.Drawing.Size(84, 16);
            this.rdJunction.TabIndex = 7;
            this.rdJunction.Text = "JUNCTION";
            this.rdJunction.UseVisualStyleBackColor = true;
            // 
            // rdValve
            // 
            this.rdValve.AutoSize = true;
            this.rdValve.Location = new System.Drawing.Point(431, 6);
            this.rdValve.Name = "rdValve";
            this.rdValve.Size = new System.Drawing.Size(62, 16);
            this.rdValve.TabIndex = 6;
            this.rdValve.Text = "VALVE";
            this.rdValve.UseVisualStyleBackColor = true;
            // 
            // rdPump
            // 
            this.rdPump.AutoSize = true;
            this.rdPump.Checked = true;
            this.rdPump.Location = new System.Drawing.Point(367, 6);
            this.rdPump.Name = "rdPump";
            this.rdPump.Size = new System.Drawing.Size(58, 16);
            this.rdPump.TabIndex = 5;
            this.rdPump.TabStop = true;
            this.rdPump.Text = "PUMP";
            this.rdPump.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "사업장 :";
            // 
            // cboPumpStation
            // 
            this.cboPumpStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPumpStation.FormattingEnabled = true;
            this.cboPumpStation.Location = new System.Drawing.Point(67, 3);
            this.cboPumpStation.Name = "cboPumpStation";
            this.cboPumpStation.Size = new System.Drawing.Size(191, 20);
            this.cboPumpStation.TabIndex = 1;
            // 
            // btnStation
            // 
            this.btnStation.Location = new System.Drawing.Point(262, 4);
            this.btnStation.Name = "btnStation";
            this.btnStation.Size = new System.Drawing.Size(78, 20);
            this.btnStation.TabIndex = 0;
            this.btnStation.Text = "사업장추가";
            this.btnStation.UseVisualStyleBackColor = true;
            this.btnStation.Click += new System.EventHandler(this.btnStation_Click);
            // 
            // tsBasemap
            // 
            this.tsBasemap.CheckOnClick = true;
            this.tsBasemap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBasemap.Image = ((System.Drawing.Image)(resources.GetObject("tsBasemap.Image")));
            this.tsBasemap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBasemap.Name = "tsBasemap";
            this.tsBasemap.Size = new System.Drawing.Size(45, 22);
            this.tsBasemap.Text = "지형도";
            // 
            // tsSatelImage
            // 
            this.tsSatelImage.CheckOnClick = true;
            this.tsSatelImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsSatelImage.Image = ((System.Drawing.Image)(resources.GetObject("tsSatelImage.Image")));
            this.tsSatelImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSatelImage.Name = "tsSatelImage";
            this.tsSatelImage.Size = new System.Drawing.Size(57, 22);
            this.tsSatelImage.Text = "항공영상";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(948, 666);
            this.Controls.Add(this.splitContainer1);
            this.KeyPreview = true;
            this.Name = "frmMain";
            this.Text = "frmMap";
            this.Load += new System.EventHandler(this.frmMap_Load);
            this.ResizeBegin += new System.EventHandler(this.frmMap_ResizeBegin);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmMap_KeyPress);
            this.ResizeEnd += new System.EventHandler(this.frmMap_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            this.tabPageBlock.ResumeLayout(false);
            this.tabPageBldg.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelBldg.ResumeLayout(false);
            this.panelBldg.PerformLayout();
            this.tabPageAddr.ResumeLayout(false);
            this.panelAddr.ResumeLayout(false);
            this.panelAddr.PerformLayout();
            this.tabPageCust.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStripTOC.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ug_pump)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ug_checkpoint)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.SplitContainer spcContents;
        protected ESRI.ArcGIS.Controls.AxMapControl axIndexMap;
        protected ESRI.ArcGIS.Controls.AxToolbarControl axToolbar;
        private System.Windows.Forms.TabPage tabPageBlock;
        protected System.Windows.Forms.TabControl tabTOC;
        protected System.Windows.Forms.SplitContainer spcTOC;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemScale;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSymbol;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLabel;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemTransfy;
        private System.Windows.Forms.TabPage tabPageBldg;
        protected System.Windows.Forms.ContextMenuStrip contextMenuStripTOC;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelBldg;
        private System.Windows.Forms.Button btnSearchBldg;
        private System.Windows.Forms.TextBox txtBldg;
        private System.Windows.Forms.ListView listBldg;
        private System.Windows.Forms.ColumnHeader columnOID;
        private System.Windows.Forms.ColumnHeader columnBLD_NAM;
        private System.Windows.Forms.ColumnHeader columnBJD_CDE;
        private System.Windows.Forms.ColumnHeader columnAddr;
        protected System.Windows.Forms.TabPage tabPageAddr;
        protected System.Windows.Forms.TreeView tvBlock;
        private System.Windows.Forms.ListView listAddr;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Panel panelAddr;
        private System.Windows.Forms.Button btnSearchAddr;
        private System.Windows.Forms.TextBox txtBonbun;
        private System.Windows.Forms.CheckBox checkSan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblDong;
        private System.Windows.Forms.ComboBox cboDong;
        private System.Windows.Forms.TextBox txtBubun;
        protected System.Windows.Forms.TabPage tabPageTOC;
        protected System.Windows.Forms.SplitContainer spcIndexMap;
        public ESRI.ArcGIS.Controls.AxTOCControl axTOC;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAllInit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSelInit;
        private System.Windows.Forms.Label lblRi;
        private System.Windows.Forms.ComboBox cboRi;
        private System.Windows.Forms.TabPage tabPageCust;
        private System.Windows.Forms.ListView listCust;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtNo3;
        private System.Windows.Forms.Label lblNo2;
        private System.Windows.Forms.Button btnSearchCust;
        private System.Windows.Forms.TextBox txtNo2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.RadioButton rdCustNo;
        private System.Windows.Forms.RadioButton rdCustName;
        private System.Windows.Forms.TextBox txtCustName;
        private System.Windows.Forms.Label lblNo1;
        private System.Windows.Forms.TextBox txtNo1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnStation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboPumpStation;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ug_pump;
        private System.Windows.Forms.TabPage tabPage2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ug_checkpoint;
        public ESRI.ArcGIS.Controls.AxMapControl axMap;
        protected System.Windows.Forms.ToolStrip toolStrip1;
        public System.Windows.Forms.ToolStripButton toolActionCommand;
        private System.Windows.Forms.RadioButton rdJunction;
        private System.Windows.Forms.RadioButton rdValve;
        private System.Windows.Forms.RadioButton rdPump;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsBasemap;
        private System.Windows.Forms.ToolStripButton tsSatelImage;
    }
}