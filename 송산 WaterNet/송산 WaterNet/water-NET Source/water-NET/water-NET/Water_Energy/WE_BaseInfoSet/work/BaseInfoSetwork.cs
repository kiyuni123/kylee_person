﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WE_BaseInfoSet.work
{
    public class BaseInfoSetwork
    {
        /// <summary>
        /// 비지니스로직(Work)클래스의 부모클래스이다.
        /// </summary>
        private EMFrame.dm.EMapper _mapper = null;
        private static BaseInfoSetwork work = null;

        private BaseInfoSetwork()
        {
            _mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
        }

        public static BaseInfoSetwork GetInstance()
        {
            if (work == null)
            {
                work = new BaseInfoSetwork();
            }
            return work;
        }

        /// <summary>
        /// 
        /// </summary>
        protected EMFrame.dm.EMapper mapper
        {
            get
            {
                return _mapper;
            }
        }

        public DataTable Get_EnergyModelNumber(Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT * FROM WH_TITLE WHERE ENERGY_GBN = :ENERGY_GBN ");

            return WE_BaseInfoSet.Dao.BaseInfoSetDao.GetInstance().Get_EnergyModelNumber(_mapper, query.ToString(), param); ;
        }

        public DataTable Get_EnergyPumpInfo(Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT * FROM WE_PUMP WHERE PUMPSTATION_ID = :PUMPSTATION_ID ");

            return WE_BaseInfoSet.Dao.BaseInfoSetDao.GetInstance().Get_EnergyPumpInfo(_mapper, query.ToString(), param); ;
        }

        public DataTable Get_EnergyCheckpointInfo(Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT * FROM WH_CHECKPOINT WHERE PUMPSTATION_ID = :PUMPSTATION_ID ");

            return WE_BaseInfoSet.Dao.BaseInfoSetDao.GetInstance().Get_EnergyCheckPointInfo(_mapper, query.ToString(), param); ;
        }

        public void Del_EnergyPumpInfo(Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM WE_PUMP WHERE PUMP_ID = :PUMP_ID ");

            _mapper.ExecuteScript(query.ToString(), param); ;
        }

        public void Del_EnergyCheckpointInfo(Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM WH_CHECKPOINT WHERE CHECK_ID = :CHECK_ID ");

            _mapper.ExecuteScript(query.ToString(), param); ;
        }

        public void Save_EnergyPumpInfo(List<Hashtable> list)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("merge into we_pump a                                                                   ");
            query.AppendLine("using (                                                                                ");
            query.AppendLine("       select :PUMP_ID pump_id                                               ");
            query.AppendLine("             ,:PUMPSTATION_ID pumpstation_id                                           ");
            query.AppendLine("             ,:TAGNAME tagname                                                       ");
            query.AppendLine("             ,:ID id                                                       ");
            query.AppendLine("             ,:CURVE_ID curve_id                                                       ");
            query.AppendLine("             ,:PUMP_GBN pump_gbn                                                       ");
            query.AppendLine("             ,:PUMP_RPM pump_rpm                                                       ");
            query.AppendLine("             ,:DIFF diff                                                               ");
            query.AppendLine("             ,:SIM_GBN sim_gbn                                                         ");
            query.AppendLine("             ,:MODEL_YN model_yn                                                       ");
            query.AppendLine("             ,:REMARK remark                                                           ");
            query.AppendLine("         from dual                                                                     ");
            query.AppendLine("      ) b                                                                              ");
            query.AppendLine("   on (                                                                                ");
            query.AppendLine("       a.pump_id = b.pump_id                                                 ");
            query.AppendLine("      )                                                                                ");
            query.AppendLine(" when matched then                                                                     ");
            query.AppendLine("      update set a.remark = b.remark                                                   ");
            query.AppendLine("                ,a.pump_gbn = b.pump_gbn                                               ");
            query.AppendLine("                ,a.tagname = b.tagname                                               ");
            query.AppendLine("                ,a.id = b.id                                               ");
            query.AppendLine("                ,a.pump_rpm = b.pump_rpm                                               ");
            query.AppendLine("                ,a.sim_gbn = b.sim_gbn                                                 ");
            query.AppendLine("                ,a.pumpstation_id = b.pumpstation_id                                   ");
            query.AppendLine("                ,a.diff = b.diff                                                       ");
            query.AppendLine("                ,a.curve_id = b.curve_id                                               ");
            query.AppendLine("                ,a.model_yn = b.model_yn                                               ");
            query.AppendLine(" when not matched then                                                                 ");
            query.AppendLine("      insert (                                                                         ");
            query.AppendLine("              a.pump_id                                                           ");
            query.AppendLine("             ,a.remark                                                                 ");
            query.AppendLine("             ,a.pump_gbn                                                               ");
            query.AppendLine("             ,a.pump_rpm                                                               ");
            query.AppendLine("             ,a.tagname                                                               ");
            query.AppendLine("             ,a.id                                                               ");
            query.AppendLine("             ,a.sim_gbn                                                                ");
            query.AppendLine("             ,a.pumpstation_id                                                         ");
            query.AppendLine("             ,a.diff                                                                   ");
            query.AppendLine("             ,a.curve_id                                                               ");
            query.AppendLine("             ,a.model_yn                                                               ");
            query.AppendLine("             )                                                                         ");
            query.AppendLine("      values (                                                                         ");
            query.AppendLine("              b.pump_id                                                           ");
            query.AppendLine("             ,b.remark                                                                 ");
            query.AppendLine("             ,b.pump_gbn                                                               ");
            query.AppendLine("             ,b.pump_rpm                                                               ");
            query.AppendLine("             ,b.tagname                                                               ");
            query.AppendLine("             ,b.id                                                               ");
            query.AppendLine("             ,b.sim_gbn                                                                ");
            query.AppendLine("             ,b.pumpstation_id                                                         ");
            query.AppendLine("             ,b.diff                                                                   ");
            query.AppendLine("             ,b.curve_id                                                               ");
            query.AppendLine("             ,b.model_yn                                                               ");
            query.AppendLine("             )                                                                         ");

            try
            {
                _mapper.Transaction = null;
                _mapper.BeginTransaction();

                foreach (Hashtable param in list)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(param["PUMP_ID"]))) param["PUMP_ID"] = GetMaxID("WE_PUMP", "PUMP_ID");

                    _mapper.ExecuteScript(query.ToString(), param);
                }
                

                _mapper.CommitTransaction();
            }
            catch (Exception)
            {
                _mapper.RollbackTransaction();
                throw;
            }
            

            
        }

        public void Save_EnergyCheckpointInfo(List<Hashtable> list)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wh_checkpoint a                                                                   ");
            query.AppendLine("using (                                                                                ");
            query.AppendLine("       select :CHECK_ID check_id                                               ");
            query.AppendLine("             ,:PUMPSTATION_ID pumpstation_id                                           ");
            //query.AppendLine("             ,:TAGNAME tagname                                                       ");
            query.AppendLine("             ,:ID id                                                       ");
            query.AppendLine("             ,:CHPDVICD chpdvicd                                                       ");
            query.AppendLine("             ,:MIN_PRESS min_press                                                       ");
            query.AppendLine("             ,:USEYN useyn                                                       ");
            query.AppendLine("             ,:REMARK remark                                                           ");
            query.AppendLine("         from dual                                                                     ");
            query.AppendLine("      ) b                                                                              ");
            query.AppendLine("   on (                                                                                ");
            query.AppendLine("       a.check_id = b.check_id                                                 ");
            query.AppendLine("      )                                                                                ");
            query.AppendLine(" when matched then                                                                     ");
            query.AppendLine("      update set a.remark = b.remark                                                   ");
            //query.AppendLine("                ,a.tagname = b.tagname                                               ");
            query.AppendLine("                ,a.id = b.id                                               ");
            query.AppendLine("                ,a.pumpstation_id = b.pumpstation_id                                   ");
            query.AppendLine("                ,a.min_press = b.min_press                                                       ");
            query.AppendLine("                ,a.chpdvicd = b.chpdvicd                                               ");
            query.AppendLine("                ,a.useyn = b.useyn                                               ");
            query.AppendLine(" when not matched then                                                                 ");
            query.AppendLine("      insert (                                                                         ");
            query.AppendLine("              a.check_id                                                           ");
            query.AppendLine("             ,a.remark                                                                 ");
            //query.AppendLine("             ,a.tagname                                                               ");
            query.AppendLine("             ,a.id                                                               ");
            query.AppendLine("             ,a.chpdvicd                                                                ");
            query.AppendLine("             ,a.pumpstation_id                                                         ");
            query.AppendLine("             ,a.min_press                                                                   ");
            query.AppendLine("             ,a.useyn                                                               ");
            query.AppendLine("             )                                                                         ");
            query.AppendLine("      values (                                                                         ");
            query.AppendLine("              b.check_id                                                           ");
            query.AppendLine("             ,b.remark                                                                 ");
            //query.AppendLine("             ,b.tagname                                                               ");
            query.AppendLine("             ,b.id                                                               ");
            query.AppendLine("             ,b.chpdvicd                                                                ");
            query.AppendLine("             ,b.pumpstation_id                                                         ");
            query.AppendLine("             ,b.min_press                                                                   ");
            query.AppendLine("             ,b.useyn                                                               ");
            query.AppendLine("             )                                                                         ");

            try
            {
                _mapper.Transaction = null;
                _mapper.BeginTransaction();

                foreach (Hashtable param in list)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(param["CHECK_ID"]))) param["CHECK_ID"] = GetMaxID("WH_CHECKPOINT", "CHECK_ID");

                    _mapper.ExecuteScript(query.ToString(), param);
                }


                _mapper.CommitTransaction();
            }
            catch (Exception)
            {
                _mapper.RollbackTransaction();
                throw;
            }

        }

        public object GetMaxID(string tablename, string filename)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT NVL(MAX(TO_NUMBER(" + filename + ")),0) + 1 MAXID FROM " + tablename);

            object o = _mapper.ExecuteScriptScalar(query.ToString(), new IDataParameter[] { } );

            return o;
        }
    }
}
