﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.ADF;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;
#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion
using EMFrame;
using QuickGraph;
using EMFrame.log;

namespace WaterNet.WE_BaseInfoSet
{
    public partial class frmMain : Form, WaterNet.WaterNetCore.IForminterface
    {
        //--------------------protected------------------------
        protected DataSet m_dsLayer = new DataSet();
        #region members
        private ESRI.ArcGIS.Carto.IActiveViewEvents_Event m_activeViewEvents;

        private readonly string[] LAYERS = { "PIPE", "JUNCTION", "PUMP", "RESERVOIR", "TANK", "VALVE" };
        #endregion members
        //--------------------private------------------------
        private ILayer m_workLayer = null;
        #region axTOC의 Item을 선택했을때 Private 멤버
        private esriTOCControlItem item = new esriTOCControlItem();
        private ILayer layer = null;
        private IBasicMap map = new MapClass(); 
        private object legendGroup = null; private object index = null;

        private QuickGraph.Geometries.Index.RTree<IFeature> _rtree = new QuickGraph.Geometries.Index.RTree<IFeature>();

        #endregion

        public frmMain()
        {
            InitializeComponent();

            InitializeSetting();

            Load +=new EventHandler(frmMain_Load);
        }
        
        //동진_2012.08.23
        private void frmMain_Load(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["tsEnergyBaseInfoSet"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnStation.Enabled = false;
                this.btnAdd.Enabled = false;
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            this.tsBasemap.Click += new EventHandler(tsBasemap_Click);
            this.tsSatelImage.Click += new EventHandler(tsSatelImage_Click);
        }

        void tsSatelImage_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            try
            {
                EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
                if (mapExplorer.GetLayer("항공영상") != null) return;
                if (VariableManager.global_extLayer.ContainsKey("항공영상"))
                {
                    if (!VariableManager.global_extLayer["항공영상"].Valid) return;

                    ILayer L = VariableManager.global_extLayer["항공영상"];
                    axMap.AddLayer(L, axMap.Map.LayerCount);
                    this.axTOC.Update();
                    //this.m_SatelImageLayerEvents = L as ILayerEvents_Event;
                    //this.m_SatelImageLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_SatelImageLayerEvents_VisibilityChanged);
                    //this.tsSatelImage.Enabled = true; this.tsSatelImage.Checked = true;
                }
                else
                {
                    string file = @Application.StartupPath + "\\" + "Aerial_grs80.lyr";
                    ILayerFile layerfile = new LayerFileClass();

                    try
                    {
                        layerfile.Open(file);
                        if (!layerfile.get_IsPresent(file)) return;
                        if (!layerfile.get_IsLayerFile(file)) return;
                        if (layerfile.Layer != null)
                        {
                            if (!layerfile.Layer.Valid) return;
                            //axMap.AddLayerFromFile(Application.StartupPath + @"\" + "Aerial_grs80.lyr", axMap.Map.LayerCount);
                            ILayer L = layerfile.Layer; // axMap.Map.get_Layer(axMap.Map.LayerCount - 1);
                            L.Visible = true;
                            L.Name = "항공영상";
                            axMap.AddLayer(L, axMap.Map.LayerCount);
                            this.axTOC.Update();
                            //this.m_SatelImageLayerEvents = L as ILayerEvents_Event;
                            //this.m_SatelImageLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_SatelImageLayerEvents_VisibilityChanged);
                            //this.tsSatelImage.Enabled = true; this.tsSatelImage.Checked = true;

                            VariableManager.global_extLayer.Add("항공영상", L);
                        }
                    }
                    catch { }
                }


            }
            catch { }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }

        }

        void tsBasemap_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            try
            {
                EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
                if (mapExplorer.GetLayer("지형도") != null) return;
                if (VariableManager.global_extLayer.ContainsKey("지형도"))
                {
                    if (!VariableManager.global_extLayer["지형도"].Valid) return;

                    ILayer L = VariableManager.global_extLayer["지형도"];
                    L.Name = "지형도";
                    axMap.AddLayer(L, axMap.Map.LayerCount);
                    this.axTOC.Update();
                    //this.m_BasemapLayerEvents = L as ILayerEvents_Event;
                    //this.m_BasemapLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_BasemapLayerEvents_VisibilityChanged);
                    //this.tsBasemap.Enabled = true; this.tsBasemap.Checked = true;
                }
                else
                {
                    string file = @Application.StartupPath + "\\" + "basemap.lyr";
                    ILayerFile layerfile = new LayerFileClass();
                    try
                    {
                        layerfile.Open(file);
                        if (!layerfile.get_IsPresent(file)) return;
                        if (!layerfile.get_IsLayerFile(file)) return;
                        if (layerfile.Layer != null)
                        {
                            if (!layerfile.Layer.Valid) return;
                            ILayer L = layerfile.Layer; // axMap.Map.get_Layer(axMap.Map.LayerCount - 1);
                            L.Visible = true;
                            L.Name = "지형도";
                            axMap.AddLayer(L, axMap.Map.LayerCount);
                            this.axTOC.Update();
                            //this.m_BasemapLayerEvents = L as ILayerEvents_Event;
                            //this.m_BasemapLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_BasemapLayerEvents_VisibilityChanged);
                            //this.tsBasemap.Enabled = true; this.tsBasemap.Checked = true;
                            VariableManager.global_extLayer.Add("지형도", L);

                        }
                    }
                    catch { }
                }

            }
            catch { }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }

        }
        /// <summary>
        /// 지도 및 툴바 초기설정
        /// </summary>
        private void InitializeSetting()
        {
            m_activeViewEvents = (IActiveViewEvents_Event)this.axMap.Map;
            m_activeViewEvents.ItemAdded += new IActiveViewEvents_ItemAddedEventHandler(OnActiveViewEventsItemAdded);
            m_activeViewEvents.ItemDeleted += new IActiveViewEvents_ItemDeletedEventHandler(OnActiveViewEventsItemDeleted);

            if (ArcManager.CheckOutLicenses(ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine) != ESRI.ArcGIS.esriSystem.esriLicenseStatus.esriLicenseAvailable)
            {
                WaterNetCore.MessageManager.ShowExclamationMessage("ArcEngine Runtime License가 없습니다.");
                return;
            }

            #region axToolbar 설정
            axToolbar.CustomProperty = this;

            //Add map navigation commands
            axToolbar.AddItem("esriControls.ControlsMapZoomInTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomOutTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapPanTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar.AddItem(new WaterNet.WaterFullExtent.CmdFullExtent(), -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToLastExtentBackCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToLastExtentForwardCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToolControl", 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar.AddItem("esriControls.ControlsMapIdentifyTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsMapFindCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapMeasureTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapRefreshViewCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            //axToolbar.AddItem("esriControls.ControlsLayerListToolControl", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsSelectTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsSelectFeaturesTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);             
            axToolbar.AddItem("esriControls.ControlsSelectAllCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsClearSelectionCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar.SetBuddyControl(axMap);
            axTOC.SetBuddyControl(axMap);

            toolStrip1.Items.Add(new ToolStripControlHost(axToolbar));

            #endregion axToolbar 설정

            axMap.SpatialReference = ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

            #region 건물찾기
            foreach (TabPage tab in this.tabTOC.TabPages)
            {
                if (tab.Name.Equals("tabPageBldg"))
                {
                    this.tabTOC.TabPages.Remove(tab);
                    continue;
                }
            }
            #endregion 건물찾기
            #region 주소찾기 - 읍면동 콤보박스 설정
            //StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("SELECT DISTINCT DONG_CODE CD, DONG_NAME CDNM");
            //oStringBuilder.AppendLine("  FROM CM_DONGCODE");
            //oStringBuilder.AppendLine(" WHERE GUBUN = '1' AND SI_CODE = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            //oStringBuilder.AppendLine(" ORDER BY DONG_CODE ASC                 ");

            //FormManager.SetComboBoxEX(cboDong, oStringBuilder.ToString(), false);
            foreach (TabPage tab in this.tabTOC.TabPages)
            {
                if (tab.Name.Equals("tabPageAddr"))
                {
                    this.tabTOC.TabPages.Remove(tab);
                    continue;
                }
            }
            #endregion 주소찾기

            txtNo1.Text = EMFrame.statics.AppStatic.USER_SGCCD;
            axMap.MapUnits = esriUnits.esriMeters;

            rdPump.Checked = true;
            rdPump.Enabled = true;
            rdValve.Enabled = false;
            rdJunction.Enabled = false;

            //주승원과장의 요청으로 밸브,정션 미표시
            rdValve.Visible = false;
            rdJunction.Visible = false;
            this.tabControl1.TabPages.Remove(tabPage2);
        }

        private void InitializeGrid()
        {
            UltraGridColumn oUltraGridColumn;
            ///펌프정보
            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_ID";
            oUltraGridColumn.Header.Caption = "PUMP_ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_ID";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "TAG명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_GBN";
            oUltraGridColumn.Header.Caption = "펌프형식";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_RPM";
            oUltraGridColumn.Header.Caption = "회전수(rpm/min)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIM_GBN";
            oUltraGridColumn.Header.Caption = "계약전력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CURVE_ID";
            oUltraGridColumn.Header.Caption = "커브ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DIFF";
            oUltraGridColumn.Header.Caption = "운영효율범위(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_pump.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MODEL_YN";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            ///체크포인트 정보
            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHECK_ID";
            oUltraGridColumn.Header.Caption = "CHECK_ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_ID";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "TAG명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;


            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHPDVICD";
            oUltraGridColumn.Header.Caption = "CHPDVICD";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MIN_PRESS";
            oUltraGridColumn.Header.Caption = "최소한계수압";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ug_checkpoint.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USEYN";
            oUltraGridColumn.Header.Caption = "사용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            FormManager.SetGridStyle(this.ug_pump);
            FormManager.SetGridStyle_ColumeAllowEdit(this.ug_pump, 2, 9);
            FormManager.SetGridStyle(this.ug_checkpoint);
            FormManager.SetGridStyle_ColumeAllowEdit(this.ug_checkpoint,2, 3);
            FormManager.SetGridStyle_ColumeAllowEdit(this.ug_checkpoint,5, 6);
        }

        private void InitializeGridValueList()
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("SELECT PUMPSTATION_ID CODE, PUMPSTATION_NAME VALUE FROM WE_PUMP_STATION");
            this.ug_pump.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList = FormManager.SetValueList(query.ToString());
            this.ug_checkpoint.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList = FormManager.SetValueList(query.ToString());

            query.Remove(0, query.Length);
            query.AppendLine("SELECT CODE CODE, CODE_NAME VALUE FROM CM_CODE WHERE PCODE = 'PKP'");
            this.ug_pump.DisplayLayout.Bands[0].Columns["PUMP_GBN"].ValueList = FormManager.SetValueList(query.ToString());

            query.Remove(0, query.Length);
            query.AppendLine("SELECT CODE CODE, CODE_NAME VALUE FROM CM_CODE WHERE PCODE = '6201'");
            this.ug_pump.DisplayLayout.Bands[0].Columns["SIM_GBN"].ValueList = FormManager.SetValueList(query.ToString());
        }

        private void OnActiveViewEventsItemAdded(object Item)
        {
            // Add your code here
            if (Item is ESRI.ArcGIS.Carto.IFeatureLayer)
            {
                string lname = ((IFeatureLayer)Item).Name;
                if (!(lname.Equals("PUMP") || lname.Equals("VALVE") || lname.Equals("JUNCTION"))) return;

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor cursor = ((IFeatureLayer)Item).Search(null, false);
                    comReleaser.ManageLifetime(cursor);

                    IFeature feature = cursor.NextFeature();
                    while (feature != null)
                    {
                        _rtree.Add(get_Rectangle(feature.ShapeCopy as IGeometry, 0.1 / 4), feature);
                        feature = cursor.NextFeature();
                    }
                }
            }
        }

        // Event handler
        private void OnActiveViewEventsItemDeleted(object Item)
        {
            if (Item is ESRI.ArcGIS.Carto.IFeatureLayer)
            {

            }
        }

        #region IForminterface 멤버
        public string FormID
        {
            get { return "펌프/감시지점 관리"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        public void Open()
        {
            Initialize_LayerInfo();
            if (m_dsLayer.Tables["SI_LAYER"] == null)
            {
                MessageBox.Show("레이어 환경 정보가 생성되지 않았습니다. \n\r관리자에게 문의하십시오", "경고", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Indexmap_Load();

            Viewmap_Load();

            axMap.Map.Name = EMFrame.statics.AppStatic.USER_SGCNM;// "정읍시";
            axTOC.Update();

            InitializeBlock();
        }

        /// <summary>
        /// 레이어 환경
        /// </summary>
        private void Initialize_LayerInfo()
        {
            WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                m_oDBManager.Open();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT * ");
                //oStringBuilder.AppendLine("      A.F_NAME AS F_NAME, A.F_SOURCE AS F_SOURCE, A.F_TYPE AS F_TYPE, ");
                //oStringBuilder.AppendLine("      A.F_ORDER F_ORDER, A.F_ALIAS AS F_ALIAS, ");
                //oStringBuilder.AppendLine("      A.F_VIEW AS F_VIEW, A.F_OBJECT AS F_OBJECT, ");
                //oStringBuilder.AppendLine("      A.F_CATEGORY AS F_CATEGORY ");
                oStringBuilder.AppendLine(" FROM SI_LAYER A ");
                oStringBuilder.AppendLine("WHERE A.F_ORDER IS NOT NULL and a.f_category in ('상수')");
                oStringBuilder.AppendLine(" ORDER BY A.F_ORDER DESC");

                m_dsLayer = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "SI_LAYER");

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                m_oDBManager.Close();
            }
        }

        /// <summary>
        /// 인덱스 맵 로드
        /// </summary>
        private void Indexmap_Load()
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            IWorkspace pWS = null;
            ILayer pLayer = null;

            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Open();
            try
            {
                foreach (DataRow item in pDataTable.Rows)
                {
                    if ((new string[] { "중블록", "소블록" }).Contains(Convert.ToString(item["F_ALIAS"])))
                    {
                        if (item["F_CATEGORY"].ToString() == "지형") pWS = VariableManager.m_Topographic;
                        else if (item["F_CATEGORY"].ToString() == "상수") pWS = VariableManager.m_Pipegraphic;
                        else if (item["F_CATEGORY"].ToString() == "INP") pWS = VariableManager.m_INPgraphic;
                        else continue;
                        if (pWS == null) continue;

                        pLayer = ArcManager.GetShapeLayer(pWS, item["F_NAME"].ToString(), item["F_ALIAS"].ToString());
                        if (pLayer == null) continue;

                        oSplash.Message = "인덱스 맵을 설정중입니다.\n\r" + "[" + pLayer.Name + "]" + " 레이어를 불러오는 중입니다.";
                        Application.DoEvents();

                        LoadRendererStream2IndexMap(pLayer, item);
                        axIndexMap.AddLayer(pLayer);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                oSplash.Close();
            }

            //if (System.IO.File.Exists(Application.StartupPath + @"\" + "basemap.lyr"))
            //{
            //    this.axIndexMap.AddLayerFromFile(Application.StartupPath + @"\" + "basemap.lyr", this.axIndexMap.Map.LayerCount);
            //}
            this.axIndexMap.Extent = ArcManager.SetFullExtent();

        }

        private ESRI.ArcGIS.Carto.ILayerEvents_Event m_SatelImageLayerEvents;
        private ESRI.ArcGIS.Carto.ILayerEvents_Event m_BasemapLayerEvents;

        /// <summary>
        /// Viewmap_Load - 지도화면에 레이어를 표시한다.
        /// </summary>
        protected void Viewmap_Load()
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            ILayer pLayer = null; 
            IWorkspace pWS = null;

            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Open();
            try
            {
                foreach (DataRow item in pDataTable.Rows)
                {
                    #region Workspace
                    if (item["F_CATEGORY"].ToString() == "지형") pWS = VariableManager.m_Topographic;
                    else if (item["F_CATEGORY"].ToString() == "상수") pWS = VariableManager.m_Pipegraphic;
                    else if (item["F_CATEGORY"].ToString() == "INP") pWS = VariableManager.m_INPgraphic;
                    else continue;

                    if (pWS == null) continue;
                    #endregion

                    pLayer = ArcManager.GetShapeLayer(pWS, item["F_NAME"].ToString(), item["F_ALIAS"].ToString());
                    if (pLayer == null) continue;
                    oSplash.Message = "맵을 설정중입니다.\n\r" + "[" + pLayer.Name + "]" + " 레이어를 불러오는 중입니다.";
                    switch (pLayer.Name)
                    {
                        case "재염소처리지점":
                        case "중요시설":
                        case "감시지점":
                        case "누수지점":
                        case "민원지점":
                        case "관세척구간":
                            continue;
                    }
                    LoadRendererStream(pLayer, item);

                    if ((new string[] { "대블록", "중블록" }).Contains(pLayer.Name))
                    {
                        ILayerEffects pLayerEffect = pLayer as ILayerEffects;
                        pLayerEffect.Transparency = (short)80;
                    }
                    else if ((new string[] { "소블록" }).Contains(pLayer.Name))
                    {
                        ILayerEffects pLayerEffect = pLayer as ILayerEffects;
                        pLayerEffect.Transparency = (short)90;
                    }
                    pLayer.Visible = item["F_VIEW"].ToString() == "1" ? true : false;

                    axMap.AddLayer(pLayer);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                oSplash.Close();
            }
            this.axMap.Extent = ArcManager.SetFullExtent();
            this.axMap.ActiveView.ContentsChanged();
        }

        void m_BasemapLayerEvents_VisibilityChanged(bool currentState)
        {
            this.tsBasemap.Checked = currentState;
        }

        void m_SatelImageLayerEvents_VisibilityChanged(bool currentState)
        {
            this.tsSatelImage.Checked = currentState;
        }
        
        /// <summary>
        /// frmMap화면 로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMap_Load(object sender, EventArgs e)
        {

            SetPumpStation();

            InitializeGrid();
            InitializeGridValueList();

            Hashtable param = new Hashtable();
            param.Add("ENERGY_GBN", "Y");

            DataTable table = work.BaseInfoSetwork.GetInstance().Get_EnergyModelNumber(param);
            if (table.Rows.Count > 0)
            {
                string INP_NUMBER = Convert.ToString(table.Rows[0]["INP_NUMBER"]);

                LoadModelLayer(INP_NUMBER);
            }

            this.cboPumpStation.SelectedIndexChanged += new EventHandler(cboPumpStation_SelectedIndexChanged);
            cboPumpStation_SelectedIndexChanged(this, new EventArgs());
            this.ug_pump.DoubleClickRow += new DoubleClickRowEventHandler(ug_grid_DoubleClickRow);
            this.ug_pump.AfterRowInsert += new RowEventHandler(FormManager.AfterRowInsert);
            this.ug_pump.AfterCellUpdate += new CellEventHandler(FormManager.AfterCellUpdate);
            this.ug_pump.ClickCellButton += new CellEventHandler(ug_grid_ClickCellButton);
            this.ug_checkpoint.DoubleClickRow += new DoubleClickRowEventHandler(ug_grid_DoubleClickRow);
            this.ug_checkpoint.AfterRowInsert += new RowEventHandler(FormManager.AfterRowInsert);
            this.ug_checkpoint.AfterCellUpdate += new CellEventHandler(FormManager.AfterCellUpdate);
            this.ug_checkpoint.ClickCellButton += new CellEventHandler(ug_grid_ClickCellButton);
        }

        void ug_grid_ClickCellButton(object sender, CellEventArgs e)
        {
            Form form = null;
            switch (e.Cell.Column.Key)
            {
                case "TAGNAME":
                    form = new frmTagLink();
                    form.ShowDialog();
                    if (!string.IsNullOrEmpty(((frmTagLink)form).TAGNAME))
                    {
                        e.Cell.Value = ((frmTagLink)form).TAGNAME;
                    }
                    break;
                case "CURVE_ID":
                    form = new WE_PumpPerformanceCurveManage.form.frmMain(e.Cell);
                    form.ShowDialog();

                    break;
            }
        }

        private void SetPumpStation()
        {
            string query = "SELECT PUMPSTATION_ID AS CD, PUMPSTATION_NAME AS CDNM FROM WE_PUMP_STATION";
            WaterNetCore.FormManager.SetComboBoxEX(cboPumpStation, query, false);
        }

        void ug_grid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row == null) return;
            UltraGrid Grid = (UltraGrid)sender;
            IFeature pFeature = null;
            switch (Grid.Name.ToUpper())
            {
                case "UG_PUMP":
                    pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer((IMapControl3)this.axMap.Object,"PUMP"), "ID = '" + Convert.ToString(e.Row.Cells["ID"].Value) + "'");
                    break;
                case "UG_CHECKPOINT":
                    if (Convert.ToString(e.Row.Cells["CHPDVICD"].Value).Equals("000001"))
                    {
                        pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer((IMapControl3)this.axMap.Object, "JUNCTION"), "ID = '" + Convert.ToString(e.Row.Cells["ID"].Value) + "'");
                    }
                    else if (Convert.ToString(e.Row.Cells["CHPDVICD"].Value).Equals("000003"))
                    {
                        pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer((IMapControl3)this.axMap.Object, "PUMP"), "ID = '" + Convert.ToString(e.Row.Cells["ID"].Value) + "'");
                    }
                    else if (Convert.ToString(e.Row.Cells["CHPDVICD"].Value).Equals("000004"))
                    {
                        pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer((IMapControl3)this.axMap.Object, "VALVE"), "ID = '" + Convert.ToString(e.Row.Cells["ID"].Value) + "'");
                    }
                    break;
            }
            if (pFeature != null)
            {
                //int nscale = 2000;
                //ArcManager.SetMapScale((IMapControl3)this.axMap.Object, (double)nscale);

                IRelationalOperator pRelOp = pFeature.Shape as IRelationalOperator;
                if (!pRelOp.Within(this.axMap.ActiveView.Extent.Envelope as IGeometry))
                {
                    ArcManager.MoveCenterAt((IMapControl3)this.axMap.Object, pFeature);
                    Application.DoEvents();
                }

                ArcManager.FlashShape(this.axMap.ActiveView, (IGeometry)pFeature.Shape, 8, 100);
            }
        }

        void cboPumpStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void LoadData()
        {
            if (cboPumpStation.SelectedValue == null) return;
            Hashtable param = new Hashtable();
            param.Add("PUMPSTATION_ID", cboPumpStation.SelectedValue.ToString());

            DataTable table1 = work.BaseInfoSetwork.GetInstance().Get_EnergyPumpInfo(param);
            this.ug_pump.DataSource = table1;

            DataTable table2 = work.BaseInfoSetwork.GetInstance().Get_EnergyCheckpointInfo(param);
            this.ug_checkpoint.DataSource = table2;

            FormManager.SetGridStyle_PerformAutoResize(this.ug_pump);
            FormManager.SetGridStyle_PerformAutoResize(this.ug_checkpoint);
        }

        private void LoadModelLayer(string INP_NUMBER)
        {
            //해석모델(INP_NUMBER)번호 멤버변수 저장
            WaterNetCore.frmSplashMsg Splash = new WaterNetCore.frmSplashMsg();  //Splash 폼
            Splash.Open();
            Splash.Message = "해석모델 레이어를 불러오는 중입니다. 잠시만 기다리세요!";
            Application.DoEvents();

            try
            {
                //기존의 Shape을 Map에서 Unload
                Remove_INP_Layer();

                //해당 모델의 Workspace가 없을경우
                IWorkspace pWorkspace = WaterAOCore.ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + INP_NUMBER);
                if (pWorkspace == null)
                {
                    //해당 모델의 Shape를 생성
                    CreateINPLayerManager oInpLayerManager = new CreateINPLayerManager(INP_NUMBER);
                    try
                    {
                        pWorkspace = oInpLayerManager.Worksapce;
                        oInpLayerManager.StartEditor();
                        oInpLayerManager.CreateINP_Shape();
                        oInpLayerManager.StopEditor(true);
                    }
                    catch (Exception)
                    {
                        oInpLayerManager.AbortEditor();
                        oInpLayerManager.StopEditor(false);
                    }
                }

                //해석모델((INP_NUMBER)번호에 해당하는 Shape로드
                Load_INP_Layer(pWorkspace);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Splash.Close();
                ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewBackground);
            }
        }

        /// <summary>
        /// 기존의 INP 레이어를 지도화면에서 삭제한다.
        /// </summary>
        private void Remove_INP_Layer()
        {
            for (int i = 0; i < LAYERS.Length; i++)
            {
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, (string)LAYERS.GetValue(i));
            }
        }

        /// <summary>
        /// INP Shape을 지도화면에 로드한다.
        /// </summary>
        private void Load_INP_Layer(IWorkspace pWorkspace)
        {
            if (pWorkspace == null) return;

            ILayer pLayer = null; IColor pColor = null; ISymbol pLineSymbol = null;
            ISymbol pMarkSymbol = null;
            for (int i = 0; i < LAYERS.Length; i++)
            {
                pLayer = ArcManager.GetShapeLayer(pWorkspace, (string)LAYERS.GetValue(i), (string)LAYERS.GetValue(i));
                if (pLayer == null) continue;

                ArcManager.SetDefaultRenderer2(axMap.ActiveView.FocusMap, (IFeatureLayer)pLayer);
                axMap.AddLayer(pLayer);
            }
        }

        #region IndexMap <-> ViewMap
        /// <summary>
        /// axMap 그리기 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void axMap_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            {
                axIndexMap.Refresh(esriViewDrawPhase.esriViewForeground, null, null);
            }
        }

        /// <summary>
        /// 지도에서 마우스이동 관련 메소드
        /// 현재 지도제어 명령에 따라 마우스포인터 변경
        /// 마우스의 위치를 StatusBar에 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void axMap_OnMouseMove(object sender, IMapControlEvents2_OnMouseMoveEvent e)
        {
            //사용자정의 툴버튼에 따라 마우스포인트 변경
            if ((axMap.CurrentTool == null) & (toolActionCommand.Checked))
            {
                axMap.MousePointer = esriControlsMousePointer.esriPointerCrosshair;
            }
            else if (axMap.CurrentTool == null)
            {
                axMap.MousePointer = esriControlsMousePointer.esriPointerDefault;
            }
            FunctionManager.SetStatusMessage(string.Format("{0}, {1}  {2}", e.mapX.ToString("#######.##"), e.mapY.ToString("#######.##"), axMap.MapUnits.ToString().Substring(4)));
        }

        protected void axIndexMap_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            {
                //Get the IRGBColor interface
                IRgbColor color = new RgbColorClass();
                //Set the color properties
                color.RGB = 255;
                //Get the ILine symbol interface
                ILineSymbol outline = new SimpleLineSymbolClass();
                //Set the line symbol properties
                outline.Width = 1.5;
                outline.Color = color;
                //Get the IFillSymbol interface
                ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
                //Set the fill symbol properties
                simpleFillSymbol.Outline = outline;
                simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSHollow;

                object oLineSymbol = simpleFillSymbol;

                IGeometry geometry = axMap.Extent;

                axIndexMap.DrawShape(geometry, ref oLineSymbol);
            }
        }

        protected void axIndexMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (e.button != 1) return;

            IEnvelope pEnvelope = null;
            IPoint pPoint = axIndexMap.ToMapPoint(e.x, e.y);
            pEnvelope = axIndexMap.TrackRectangle();
            if (!pEnvelope.IsEmpty)
            {
                axMap.Extent = pEnvelope;
            }


        }
        #endregion IndexMap <-> ViewMap

        /// <summary>
        /// 내부 점인지 알아오기
        /// </summary>
        /// <param name="pEnvelpoe"></param>
        /// <param name="pPoint"></param>
        /// <returns></returns>
        private Boolean IsPointIn(IEnvelope pEnvelpoe, IPoint pPoint)
        {
            return (pEnvelpoe.XMin <= pPoint.X && pEnvelpoe.XMax >= pPoint.X) && (pEnvelpoe.YMin <= pPoint.Y && pEnvelpoe.YMax >= pPoint.Y);
        }

        protected void frmMap_ResizeBegin(object sender, EventArgs e)
        {
            axMap.SuppressResizeDrawing(true);
        }

        protected void frmMap_ResizeEnd(object sender, EventArgs e)
        {
            axMap.SuppressResizeDrawing(false);
        }

        #region Util
        /// <summary>
        /// 해당 문자열이 숫자인지를 확인하는 정규식
        /// Char.IsNumber 메서드 (Char)로 변환 해주세요
        /// </summary>
        /// <param name="strData">문자열</param>
        /// <returns></returns>
        private bool IsNumeric(string strData)
        {
            System.Text.RegularExpressions.Regex isNumber = new System.Text.RegularExpressions.Regex(@"^\d+$");
            System.Text.RegularExpressions.Match m = isNumber.Match(strData);

            return m.Success;
        }

        private QuickGraph.Geometries.Rectangle get_Rectangle(IGeometry geometry, double dist)
        {
            IEnvelope envelope = geometry.Envelope;
            envelope.Expand(dist, dist, false);
            QuickGraph.Geometries.Rectangle rect = new QuickGraph.Geometries.Rectangle((float)envelope.XMin, (float)envelope.YMin, (float)envelope.XMax, (float)envelope.YMax, (float)0.0, (float)0.0);
            return rect;
        }

        #endregion Util

        #region TOC 팝업메뉴
        /// <summary>
        /// 축척설정 화면
        /// TOC에서 어떤것이 선택되었는지 상관없음.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemScale_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            Form frmScale = new frmSetScale(m_workLayer);
            if (frmScale.ShowDialog() != DialogResult.OK) return;

            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        /// 주석설정
        /// TOC에서 어떤것이 선택되었는지 상관없음.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemLabel_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;
            if (item != esriTOCControlItem.esriTOCControlItemLayer) return;

            frmSetText frmText = new frmSetText();
            frmText.SelectSymbol(m_workLayer);

            if (frmText.ShowDialog() != DialogResult.OK) return;
            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        /// 심볼설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemSymbol_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            IFeatureLayer featureLayer = m_workLayer as IFeatureLayer;
            if (featureLayer == null) return;

            IGeoFeatureLayer geoFeatureLayer = (IGeoFeatureLayer)featureLayer;
            IFeatureRenderer Renderer = geoFeatureLayer.Renderer;

            ISimpleRenderer pSimpleRenderer = null;
            IUniqueValueRenderer pUniqueRenderer = null;
            IClassBreaksRenderer pClassRenderer = null;

            SymbolAccess scAccess = new SymbolAccess();
            if (Renderer is ISimpleRenderer)
            {
                pSimpleRenderer = Renderer as ISimpleRenderer;
                scAccess.SimpleRenderer(pSimpleRenderer);
            }
            else if (Renderer is IClassBreaksRenderer)
            {
                pClassRenderer = Renderer as IClassBreaksRenderer;
                scAccess.ClassRenderer(pClassRenderer, Convert.ToInt32(index));
            }
            else if (Renderer is IUniqueValueRenderer)
            {
                pUniqueRenderer = Renderer as IUniqueValueRenderer;
                scAccess.UniqueRenderer(pUniqueRenderer, pUniqueRenderer.get_Value((int)index));
            }

            ISymbol pSymbol = scAccess.Selector();
            if (pSymbol != null)
            {
                if (Renderer is ISimpleRenderer)
                {
                    pSimpleRenderer = Renderer as ISimpleRenderer;
                    pSimpleRenderer.Symbol = pSymbol;
                }
                else if (Renderer is IClassBreaksRenderer)
                {
                    pClassRenderer = Renderer as IClassBreaksRenderer;
                    pClassRenderer.set_Symbol(Convert.ToInt32(index), pSymbol);
                }
                else if (Renderer is IUniqueValueRenderer)
                {
                    pUniqueRenderer = Renderer as IUniqueValueRenderer;
                    pUniqueRenderer.set_Symbol(pUniqueRenderer.get_Value((int)index), pSymbol);
                }
                
                //Fire contents changed event that the TOCControl listens to
                axMap.ActiveView.ContentsChanged();
                //Refresh the display
                axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
            }
        }

        /// <summary>
        /// 투명도 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemTransfy_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;
            if (item != esriTOCControlItem.esriTOCControlItemLayer) return;

            frmSetTrans frmTrans = new frmSetTrans(m_workLayer);
            if (frmTrans.ShowDialog() != DialogResult.OK) return;

            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        ///선택레이어 렌더링 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItemSelInit_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            foreach (DataRow item in pDataTable.Rows)
            {
                if (Convert.ToString(item["F_ALIAS"]) == m_workLayer.Name)
                {
                    LoadRendererStream(m_workLayer, item);

                    axMap.ActiveView.ContentsChanged();
                    axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);

                    break;
                }
            }

        }

        /// <summary>
        /// 전체레이어 렌더링 초기화 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItemAllInit_Click(object sender, EventArgs e)
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            ILayer pLayer = null;
            for (int i = 0; i < axMap.LayerCount; i++)
            {
                pLayer = axMap.get_Layer(i);
                if (pLayer != null)
                {
                    foreach (DataRow item in pDataTable.Rows)
                    {
                        if (Convert.ToString(item["F_ALIAS"]) == pLayer.Name)
                        {
                            LoadRendererStream(pLayer, item);

                            break;
                        }
                    }
                }
            }

            axMap.ActiveView.ContentsChanged();
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        #endregion

        #region 블록정보 트리뷰에 적용
        private void InitializeBlock()
        {
            ITable b_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLBG_AS");
            ITable m_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLBM_AS");
            ITable s_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLSM_AS");
            if (b_Table == null) return;
            if (m_Table == null) return;
            if (s_Table == null) return;
            string[] fieldList = { "BLK_NAM" };
            ICursor pCursor = ArcManager.GetSortedCursor(b_Table, string.Empty, fieldList, true);
            if (pCursor == null) return;

            IRow pRow = pCursor.NextRow();
            while (pRow != null)
            {
                //대블록 IRow
                TreeNode tnode = new TreeNode();
                string strNam = Convert.ToString(ArcManager.GetValue(pRow, "BLK_NAM"));
                string strCDE = Convert.ToString(ArcManager.GetValue(pRow, "FTR_CDE"));
                string strIDN = Convert.ToString(ArcManager.GetValue(pRow, "FTR_IDN"));

                tnode.Text = strNam;
                tnode.Tag = strIDN;
                tnode.Name = strCDE;
                tvBlock.Nodes.Add(tnode);
                
                SetTreeNode(tnode, m_Table, "UBL_IDN = '" + strIDN + "'");

                pRow = pCursor.NextRow();
            }

            tvBlock.ExpandAll();

            ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
        }

        private void SetTreeNode(TreeNode pNode, ITable pTable, string sWhereClause)
        {
            if (pTable == null) return;
            string[] fieldList = {"BLK_NAM"};
            ICursor pCursor = ArcManager.GetSortedCursor(pTable, sWhereClause,fieldList,true);
            if (pCursor == null) return;

            IRow pRow = pCursor.NextRow();
            while (pRow != null)
            {
                TreeNode tnode = new TreeNode();
                string strNam = Convert.ToString(ArcManager.GetValue(pRow, "BLK_NAM"));
                string strCDE = Convert.ToString(ArcManager.GetValue(pRow, "FTR_CDE"));
                string strIDN = Convert.ToString(ArcManager.GetValue(pRow, "FTR_IDN"));

                tnode.Text = strNam;
                tnode.Tag = strIDN;
                tnode.Name = strCDE;

                pNode.Nodes.Add(tnode);

                //소블록 
                ITable s_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLSM_AS");
                if (s_Table == null) continue;

                SetTreeNode(tnode, s_Table, "UBL_IDN = '" + strIDN + "'");

                pRow = pCursor.NextRow();
            }

            ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
        }


        protected virtual void tvBlock_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode oNode = e.Node; // tvBlock.SelectedNode;
            if (oNode == null) return;

            IFeatureLayer pFeatureLayer = default(IFeatureLayer);
            switch (oNode.Name)
            {
                case "BZ001":    //대블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "대블록");
                    break;
                case "BZ002":    //중블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
                    break;
                case "BZ003":    //소블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");
                    break;
            }
            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + oNode.Tag.ToString() + "'");
            if (pFeature == null) return;

            axMap.Extent = pFeature.Shape.Envelope;
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);

        }    
        #endregion

        #region TabControl - ActiveTab
        public int ActiveTabIndex
        {
            set {tabTOC.SelectedIndex = value;  }
        }

        public string ActiveTabName
        {
            set 
            {
                foreach (TabPage item in tabTOC.TabPages)
                {
                    if (item.Text == value)
                    {
                        tabTOC.SelectedTab = item;
                        return;
                    }
                }
            }
        }
        #endregion

        public void axTOC_OnMouseDown(object sender, ITOCControlEvents_OnMouseDownEvent e)
        {
            if (2 != e.button) return;
            //do the HitTest
            axTOC.HitTest(e.x, e.y, ref item, ref map, ref layer, ref legendGroup, ref index);

            if (null == layer || !(layer is IFeatureLayer)) return;

            axMap.CustomProperty = layer;

            switch (item)
            {
                case esriTOCControlItem.esriTOCControlItemHeading:        //UniqueValue : 글자선택
                    break;
                case esriTOCControlItem.esriTOCControlItemLayer:            //레이어선택
                    toolStripMenuItemLabel.Enabled = true;
                    toolStripMenuItemScale.Enabled = true;
                    toolStripMenuItemSymbol.Enabled = false;
                    toolStripMenuItemTransfy.Enabled = true;

                    //popup a context menu with a 'Properties' command
                    contextMenuStripTOC.Show(axTOC, e.x, e.y);
                    break;
                case esriTOCControlItem.esriTOCControlItemLegendClass:   //심볼선택
                    toolStripMenuItemLabel.Enabled = false;
                    toolStripMenuItemScale.Enabled = false;
                    toolStripMenuItemSymbol.Enabled = true;
                    toolStripMenuItemTransfy.Enabled = false;

                    //popup a context menu with a 'Properties' command
                    contextMenuStripTOC.Show(axTOC, e.x, e.y);
                    break;
                case esriTOCControlItem.esriTOCControlItemMap:             //TOC 맵 선택
                    break;
                case esriTOCControlItem.esriTOCControlItemNone:
                    break;
                default:
                    break;
            }

            m_workLayer = layer;
        }

        private void tabTOC_Selected(object sender, TabControlEventArgs e)
        {
            switch (e.TabPageIndex)
	        { 
                case 0:    //TOC
                    break;
                case 1:    //블록정보
                    break;
                case 2:
                    if (ArcManager.GetMapLayer((IMapControl3)axMap.Object, "건물") == null)
                    {
                        btnSearchBldg.Enabled = false;
                        MessageBox.Show("건물레이어가 로드되지 않았습니다.");
                    }
                    else
                    {
                        txtBldg.Focus();
                        btnSearchBldg.Enabled = true;
                    }
                    break;
                case 3:
                    if (ArcManager.GetMapLayer((IMapControl3)axMap.Object, "지형지번") == null)
                    {
                        MessageBox.Show("지번레이어가 로드되지 않았습니다.");
                    }
                    break;
		        default:
                    break;
	        }
        }

        #region 건물명 찾기
        private void btnSearchBldg_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBldg.Text))
	        {
                MessageBox.Show("건물명을 입력하세요.", "안내", MessageBoxButtons.OK);
                return;
	        }

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "건물");
            if (pLayer == null)
            {
                MessageBox.Show("건물레이어가 로드되지 않았습니다.");
                return;
            }

            string sWhereClause = "BLD_NAM like '%" + txtBldg.Text + "%'";
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                string[] fieldList = { "BLD_NAM" };
                ICursor pCursor = ArcManager.GetSortedCursor((ITable)pLayer, sWhereClause, fieldList, true);
                comReleaser.ManageLifetime(pCursor);

                IRow pRow = pCursor.NextRow();
                if (pRow == null)
                {
                    MessageBox.Show("검색된 건물이 없습니다.");
                    return;
                }

                listBldg.Items.Clear();
                listBldg.BeginUpdate();
                while (pRow != null)
                {
                    ListViewItem pitem = new ListViewItem();
                    pitem.Text = Convert.ToString(pRow.get_Value(pRow.Fields.FindField("BLD_NAM")));

                    pitem.Tag = pRow.OID;
                    listBldg.Items.Add(pitem);
                    pRow = pCursor.NextRow();
                }
                listBldg.EndUpdate();
            }
           
        }

        /// <summary>
        /// 건물찾기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBldg_DoubleClick(object sender, EventArgs e)
        {
            if (listBldg.SelectedItems.Count != 1) return;

            string sOID = Convert.ToString(listBldg.SelectedItems[0].Tag);
            if (string.IsNullOrEmpty(sOID)) return;

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "건물");
            IFeature pFeature = ArcManager.GetFeature(pLayer, Convert.ToInt32(sOID));
            if (pFeature == null) return;

            ArcManager.MoveCenterAt((IMapControl3)axMap.Object, pFeature.Shape);
            Application.DoEvents();
            ArcManager.FlashShape(axMap.ActiveView, pFeature.Shape, 6, 100);
        }

        private void frmMap_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (tabTOC.SelectedIndex < 2) return;
            
            if ((tabTOC.SelectedIndex == 2) && (e.KeyChar == (char)Keys.Return))
            {
                btnSearchBldg_Click(sender, new EventArgs());
            }
            else if ((tabTOC.SelectedIndex == 3) && (e.KeyChar == (char)Keys.Return))
            {
                btnSearchAddr_Click(sender, new EventArgs());
            }
        }
        #endregion

        #region 주소찾기
        private void btnSearchAddr_Click(object sender, EventArgs e)
        {
            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "지형지번");
            if (pLayer == null)
            {
                MessageBox.Show("지형지번 레이어가 로드되지 않았습니다.");
                return;
            }

            if (String.IsNullOrEmpty(txtBonbun.Text))
            {
                MessageBox.Show("본번 지번을 입력하세요.", "안내", MessageBoxButtons.OK);
                txtBonbun.Focus();
                return;
            }

            if (!IsNumeric(txtBonbun.Text))
            {
                MessageBox.Show("본번 지번에 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                txtBonbun.Focus();
                return;                
            }

            if (!String.IsNullOrEmpty(txtBubun.Text))
            {
                if (!IsNumeric(txtBubun.Text))
                {
                    MessageBox.Show("부번 지번에 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtBubun.Focus();
                    return;
                }
            }

            string sWhereClause = sWhereClause = "BJD_CDE = '" + cboDong.SelectedValue + "'";

            //if (cboRi.SelectedIndex == -1) sWhereClause = "BJD_CDE = '" + cboDong.SelectedValue + "'";
            //else sWhereClause = "BJD_CDE = '" + cboRi.SelectedValue + "'";

            if (checkSan.Checked) sWhereClause += " AND SAN_CDE = '1'";
            else                  sWhereClause += " AND SAN_CDE <> '1'";
            sWhereClause += " AND FAC_NUM = '" + txtBonbun.Text + "'";

            if (txtBubun.Text.Length != 0) sWhereClause += " AND FAD_NUM = '" + txtBubun.Text + "'";

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                string[] fieldList = { "SAN_CDE", "FAC_NUM", "FAD_NUM" };
                ICursor pCursor = ArcManager.GetSortedCursor((ITable)pLayer, sWhereClause, fieldList, true);
                comReleaser.ManageLifetime(pCursor);

                IRow pRow = pCursor.NextRow();
                if (pRow == null)
                {
                    MessageBox.Show("검색된 주소가 없습니다.");
                    return;
                }

                listAddr.Items.Clear();
                listAddr.BeginUpdate();
                while (pRow != null)
                {
                    string pAddress = cboDong.Text;
                    if (Convert.ToString(pRow.get_Value(pRow.Fields.FindField("SAN_CDE"))).Equals("1")) pAddress = "산 ";
                    pAddress += Convert.ToString(pRow.get_Value(pRow.Fields.FindField("FAC_NUM")));
                    if (Convert.ToString(pRow.get_Value(pRow.Fields.FindField("FAD_NUM"))).Trim().Length != 0)
                        pAddress += "-" + Convert.ToString(pRow.get_Value(pRow.Fields.FindField("FAD_NUM")));

                    pAddress += "번지";

                    ListViewItem pitem = new ListViewItem();
                    pitem.SubItems[0].Text = pAddress;
                    pitem.Tag = pRow.OID;
                    listAddr.Items.Add(pitem);
                    pRow = pCursor.NextRow();
                }
                listAddr.EndUpdate();
            }
        }

        /// <summary>
        /// 읍/면/동을 선택하여 해당 리를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboDong_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT DISTINCT RI_CODE CD, RI_NAME CDNM");
            oStringBuilder.AppendLine("  FROM CM_DONGCODE");
            oStringBuilder.AppendLine(" WHERE GUBUN = '1' AND SI_CODE = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("   AND DONG_CODE = '" + cboDong.SelectedValue + "'");
            oStringBuilder.AppendLine(" ORDER BY RI_CODE ASC");

            FormManager.SetComboBoxEX(cboRi, oStringBuilder.ToString(), false);
        }

        /// <summary>
        /// 주소찾기 목록을 선택하여, 지번위치로 지도이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listAddr_DoubleClick(object sender, EventArgs e)
        {
            if (listAddr.SelectedItems.Count != 1) return;

            string sOID = Convert.ToString(listAddr.SelectedItems[0].Tag);
            if (string.IsNullOrEmpty(sOID)) return;

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "지형지번");
            IFeature pFeature = ArcManager.GetFeature(pLayer, Convert.ToInt32(sOID));
            if (pFeature == null) return;

            ArcManager.MoveCenterAt((IMapControl3)axMap.Object, pFeature.Shape);
            Application.DoEvents();
            ArcManager.FlashShape(axMap.ActiveView, pFeature.Shape, 6, 100);
        }

        #endregion 주소찾기

        #region 수용가찾기
        /// <summary>
        /// 수용가 명
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdCustName_CheckedChanged(object sender, EventArgs e)
        {
            txtNo1.Visible = false;
            txtNo2.Visible = false;
            txtNo3.Visible = false;
            lblNo1.Visible = false;
            lblNo2.Visible = false;

            txtCustName.Visible = true;
        }

        /// <summary>
        /// 수용가번호
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdCustNo_CheckedChanged(object sender, EventArgs e)
        {
            txtNo1.Visible = true;
            txtNo2.Visible = true;
            txtNo3.Visible = true;
            lblNo1.Visible = true;
            lblNo2.Visible = true;

            txtCustName.Visible = false;
        }

        /// <summary>
        /// 수용가 검색을 수행한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchCust_Click(object sender, EventArgs e)
        {
            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "수도계량기");
            if (pLayer == null)
            {
                MessageBox.Show("수도계량기 레이어가 로드되지 않았습니다.");
                return;
            }

            #region 검색조건 체크
            if (rdCustName.Checked)
            {
                if (String.IsNullOrEmpty(txtCustName.Text))
                {
                    MessageBox.Show("수용가명 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtCustName.Focus();
                    return;
                }
            }
            else if (rdCustNo.Checked)
            {
                if (String.IsNullOrEmpty(txtNo1.Text))
                {
                    MessageBox.Show("수용가번호를 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtNo1.Focus();
                    return;
                }

                if (!String.IsNullOrEmpty(txtNo1.Text))
                {
                    if (!IsNumeric(txtNo1.Text))
                    {
                        MessageBox.Show("수용가번호에는 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                        txtNo1.Focus();
                        return;
                    }
                }

                if (String.IsNullOrEmpty(txtNo2.Text))
                {
                    MessageBox.Show("수용가번호를 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtNo2.Focus();
                    return;
                }

                if (!String.IsNullOrEmpty(txtNo2.Text))
                {
                    if (!IsNumeric(txtNo2.Text))
                    {
                        MessageBox.Show("수용가번호에는 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                        txtNo2.Focus();
                        return;
                    }
                }

                if (String.IsNullOrEmpty(txtNo3.Text))
                {
                    MessageBox.Show("수용가번호를 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtNo3.Focus();
                    return;
                }

                if (!String.IsNullOrEmpty(txtNo3.Text))
                {
                    if (!IsNumeric(txtNo3.Text))
                    {
                        MessageBox.Show("수용가번호에는 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                        txtNo3.Focus();
                        return;
                    }
                }
            }
            #endregion 검색조건 체크

            string sWhereClause = string.Empty;
            if (rdCustName.Checked)
            {
                sWhereClause = "DMNM LIKE '" + txtCustName.Text + "%'";
            }
            else if (rdCustNo.Checked)
            {
                sWhereClause = "DMNO LIKE '" + txtNo1.Text + txtNo2.Text + txtNo3.Text + "%'";
            }

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                string[] fieldList = { "DMNM", "DMNO" };
                ICursor pCursor = ArcManager.GetSortedCursor((ITable)pLayer, sWhereClause, fieldList, true);
                comReleaser.ManageLifetime(pCursor);

                IRow pRow = pCursor.NextRow();
                if (pRow == null)
                {
                    MessageBox.Show("검색된 수용가정보가 없습니다.");
                    return;
                }

                listCust.Items.Clear();
                listCust.BeginUpdate();
                while (pRow != null)
                {
                    ListViewItem pitem = new ListViewItem();
                    pitem.SubItems.Add("DMNO");

                    pitem.Text = Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNM")));
                    pitem.SubItems[1].Text = Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNO"))).Substring(0,5)
                                           + "-" + Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNO"))).Substring(5, 2)
                                           + "-" + Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNO"))).Substring(7,5);
                    pitem.Tag = pRow.OID;
                    listCust.Items.Add(pitem);
                    pRow = pCursor.NextRow();
                }
                listCust.EndUpdate();
            }
             
        }

        /// <summary>
        /// 수용가목록 클릭하여 위치찾기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listCust_DoubleClick(object sender, EventArgs e)
        {
            if (listCust.SelectedItems.Count != 1) return;

            string sOID = Convert.ToString(listCust.SelectedItems[0].Tag);
            if (string.IsNullOrEmpty(sOID)) return;

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "수도계량기");
            IFeature pFeature = ArcManager.GetFeature(pLayer, Convert.ToInt32(sOID));
            if (pFeature == null) return;

            ArcManager.MoveCenterAt((IMapControl3)axMap.Object, pFeature.Shape);
            Application.DoEvents();
            ArcManager.FlashShape(axMap.ActiveView, pFeature.Shape, 6, 100);
        }

        #endregion 수용가찾기

        #region 지도제어 툴바
        protected virtual void toolActionCommand_Click(object sender, EventArgs e)
        {
            if (toolActionCommand.Checked) axMap.CurrentTool = null;
        }

        private void axToolbar_OnItemClick(object sender, IToolbarControlEvents_OnItemClickEvent e)
        {
            toolActionCommand.Checked = false;
        }

        #endregion 지도제어 툴바

        #region 지도 처리
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (!(e.button == 1 && this.toolActionCommand.Checked && this.axMap.CurrentTool == null)) return;

            IActiveView activeView = this.axMap.ActiveView;
            IRubberBand pRubberBand = new RubberEnvelopeClass();
            IEnvelope envlope = pRubberBand.TrackNew(activeView.ScreenDisplay, null) as IEnvelope;
            if (envlope.IsEmpty) return;
            IGeometry pGeometry = envlope as IGeometry;

            List<IFeature> list = _rtree.Intersects(get_Rectangle(pGeometry, 0.0));
            if (list.Count == 0)
            {
                MessageBox.Show("선택된 객체가 없습니다. 다시 선택하십시오!");
                return;
            }


            //#region 위치선택 - 지도에 사고지점을 선택하고, 별표 표시
            //IColor pColor = ArcManager.GetColor(255, 0, 0);
            //ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
            //IElement pElement = new MarkerElementClass();
            //pElement.Geometry = nearest as IGeometry;
            //IMarkerElement pMarkerElement = pElement as IMarkerElement;
            //pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;

            //IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
            //pGraphicsContainer.AddElement(pElement, 0);
            //ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
            //ArcManager.FlashShape(axMap.ActiveView, nearest as IPoint, 3);
            //#endregion 지도에 사고지점을 선택하고, 별표 표시 끝

            string layername = string.Empty;
            if (tabControl1.SelectedTab.Text.Equals("펌프정보")) layername = "PUMP";
            else 
            {
                if (rdPump.Checked) layername = "PUMP";
                else if (rdValve.Checked) layername = "VALVE";
                else if (rdJunction.Checked) layername = "JUNCTION";
            }

            List<IFeature> features = new List<IFeature>();
            foreach (IFeature feature in list)
                if (feature.Class.AliasName.Equals(layername))
                    features.Add(feature);

            if (features.Count == 0)
            {
                MessageBox.Show("선택된 객체가 없습니다. 다시 선택하십시오!");
                return;
            }

            Console.WriteLine(layername + " : " + features.Count.ToString());

            if (tabControl1.SelectedTab.Text.Equals("펌프정보"))
            {
                ValidatePumps(features);
            }
            else 
            {
                ValidateCheckPoints(features);
            }
        }

        private void ValidatePumps(List<IFeature> list)
        {
            foreach (IFeature feature in list)
            {
                DataRow[] rows = ((DataTable)this.ug_pump.DataSource).Select("ID = '" + feature.get_Value(feature.Fields.FindField("ID")) + "'");
                if (rows.Count() == 0)
                {
                    DataRow row = ((DataTable)this.ug_pump.DataSource).NewRow();
                    row["ID"] = feature.get_Value(feature.Fields.FindField("ID"));
                    row["PUMPSTATION_ID"] = cboPumpStation.SelectedValue;
                    ((DataTable)this.ug_pump.DataSource).Rows.Add(row);
                }
            }
        }

        private void ValidateCheckPoints(List<IFeature> list)
        {
            string chpdvicd = string.Empty;
            if (list[0].Class.AliasName.Equals("PUMP")) chpdvicd = "000004";
            else if (list[0].Class.AliasName.Equals("VALVE")) chpdvicd = "000003";
            else if (list[0].Class.AliasName.Equals("JUNCTION")) chpdvicd = "000001";
            
            foreach (IFeature feature in list)
            {
                DataRow[] rows = ((DataTable)this.ug_checkpoint.DataSource).Select("ID = '" + feature.get_Value(feature.Fields.FindField("ID")) + "'");
                if (rows.Count() == 0)
                {
                    DataRow row = ((DataTable)this.ug_checkpoint.DataSource).NewRow();
                    row["ID"] = feature.get_Value(feature.Fields.FindField("ID"));
                    row["PUMPSTATION_ID"] = cboPumpStation.SelectedValue;
                    row["CHPDVICD"] = chpdvicd;
                    ((DataTable)this.ug_checkpoint.DataSource).Rows.Add(row);
                }
            }
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable param = new Hashtable();
                UltraGridRow row = null;
                if (tabControl1.SelectedTab.Text.Equals("펌프정보"))
                {
                    row = this.ug_pump.ActiveRow;
                    if (row != null)
                    {
                        param.Add("PUMP_ID", row.Cells["PUMP_ID"].Value);
                        work.BaseInfoSetwork.GetInstance().Del_EnergyPumpInfo(param);

                        row.Delete(false);
                    }
                }
                else
                {
                    row = this.ug_checkpoint.ActiveRow;
                    if (row != null)
                    {
                        param.Add("CHECK_ID", row.Cells["PUMP_ID"].Value);
                        work.BaseInfoSetwork.GetInstance().Del_EnergyCheckpointInfo(param);

                        row.Delete(false);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult eDialogResult = MessageManager.ShowYesNoMessage(MessageManager.MSG_QUERY_SAVE);
                if (eDialogResult != DialogResult.Yes) return;

                List<Hashtable> list = new List<Hashtable>();

                foreach (UltraGridRow row in this.ug_pump.Rows)
                {
                    Hashtable param = new Hashtable();
                    param.Add("PUMP_ID", row.Cells["PUMP_ID"].Value);
                    param.Add("PUMPSTATION_ID", row.Cells["PUMPSTATION_ID"].Value);
                    param.Add("TAGNAME", row.Cells["TAGNAME"].Value);
                    param.Add("ID", row.Cells["ID"].Value);
                    param.Add("CURVE_ID", row.Cells["CURVE_ID"].Value);
                    param.Add("PUMP_GBN", row.Cells["PUMP_GBN"].Value);
                    param.Add("PUMP_RPM", row.Cells["PUMP_RPM"].Value);
                    param.Add("DIFF", row.Cells["DIFF"].Value);
                    param.Add("SIM_GBN", row.Cells["SIM_GBN"].Value);
                    param.Add("MODEL_YN", "Y");
                    param.Add("REMARK", row.Cells["REMARK"].Value);
                    list.Add(param);
                }
                work.BaseInfoSetwork.GetInstance().Save_EnergyPumpInfo(list);


                list.Clear();
                foreach (UltraGridRow row in this.ug_checkpoint.Rows)
                {
                    Hashtable param = new Hashtable();
                    param.Add("CHECK_ID", row.Cells["CHECK_ID"].Value);
                    param.Add("PUMPSTATION_ID", row.Cells["PUMPSTATION_ID"].Value);
                    //param.Add("TAGNAME", row.Cells["TAGNAME"].Value);
                    param.Add("ID", row.Cells["ID"].Value);
                    param.Add("CHPDVICD", row.Cells["CHPDVICD"].Value);
                    param.Add("MIN_PRESS", row.Cells["MIN_PRESS"].Value);
                    param.Add("USEYN", "Y");
                    param.Add("REMARK", row.Cells["REMARK"].Value);
                    list.Add(param);
                }
                work.BaseInfoSetwork.GetInstance().Save_EnergyCheckpointInfo(list);

                this.LoadData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion 지도처리

        /// <summary>
        /// SI_LAYER에 저장된 PropertySet을 레이어에 적용한다.
        /// 렌더러, 라벨, activeScale 설정값 => IndexMap에 적용
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pData"></param>
        private void LoadRendererStream2IndexMap(ILayer pLayer, DataRow row)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                if (!Convert.IsDBNull(row["F_RENDERE"]))
                {
                    byte[] pData = row["F_RENDERE"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;

                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IFeatureRenderer"));
                    if (o != null)
                        geoFeatureLayer.Renderer = o as IFeatureRenderer;
                }

                if (pLayer.Name.Equals("중블록") || pLayer.Name.Equals("배수지"))
                {
                    if (!Convert.IsDBNull(row["F_ANNOPROP"]))
                    {
                        byte[] pData = row["F_ANNOPROP"] as byte[];
                        if (pData == null) return;
                        if (pData.Length == 0) return;
                        object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IAnnotateLayerPropertiesCollection"));
                        if (o != null)
                            geoFeatureLayer.AnnotationProperties = o as IAnnotateLayerPropertiesCollection;

                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// SI_LAYER에 저장된 PropertySet을 레이어에 적용한다.
        /// 렌더러, 라벨, activeScale 설정값
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pData"></param>
        private void LoadRendererStream(ILayer pLayer, DataRow row)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                if (!Convert.IsDBNull(row["F_RENDERE"]))
                {
                    byte[] pData = row["F_RENDERE"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;

                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IFeatureRenderer"));
                    if (o != null)
                        geoFeatureLayer.Renderer = o as IFeatureRenderer;
                }
                if (!Convert.IsDBNull(row["F_ANNOPROP"]))
                {
                    byte[] pData = row["F_ANNOPROP"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;
                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IAnnotateLayerPropertiesCollection"));
                    if (o != null)
                        geoFeatureLayer.AnnotationProperties = o as IAnnotateLayerPropertiesCollection;

                }
                if (!Convert.IsDBNull(row["F_MINSCALE"]))
                {
                    geoFeatureLayer.MinimumScale = Convert.ToDouble(row["F_MINSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_MAXSCALE"]))
                {
                    geoFeatureLayer.MaximumScale = Convert.ToDouble(row["F_MAXSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_FIELDDISP"]))
                {
                    geoFeatureLayer.DisplayField = Convert.ToString(row["F_FIELDDISP"]);
                }
                if (!Convert.IsDBNull(row["F_ANNODISP"]))
                {
                    geoFeatureLayer.DisplayAnnotation = Convert.ToBoolean(row["F_ANNODISP"]);
                }
                if (!Convert.IsDBNull(row["F_TIPDISP"]))
                {
                    geoFeatureLayer.ShowTips = Convert.ToBoolean(row["F_TIPDISP"]);
                }
            }
            catch { }
        }

        /// <summary>
        /// 선택레이어 렌더링 초기화(외부 메소드)
        /// </summary>
        /// <param name="pLayer"></param>
        protected void SetDefaultRenderer(ILayer pLayer)
        {
            if (pLayer == null) return;
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            foreach (DataRow item in pDataTable.Rows)
            {
                if (Convert.ToString(item["F_ALIAS"]) == pLayer.Name)
                {
                    LoadRendererStream(pLayer, item);

                    this.axMap.ActiveView.ContentsChanged();
                    this.axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);

                    break;
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                rdPump.Checked = true;
                rdPump.Enabled = true;
                rdValve.Enabled = false;
                rdJunction.Enabled = false;
            }
            else
            {
                rdJunction.Checked = true;
                rdPump.Enabled = false;
                rdValve.Enabled = true;
                rdJunction.Enabled = true;
            }
        }

        /// <summary>
        /// 사업장 추가
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStation_Click(object sender, EventArgs e)
        {
            WE_PumpStationManage.form.frmMain form = new WaterNet.WE_PumpStationManage.form.frmMain();
            form.ShowDialog();

            SetPumpStation();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                DataRow row = ((DataTable)this.ug_pump.DataSource).NewRow();
                row["PUMPSTATION_ID"] = this.cboPumpStation.SelectedValue;
                ((DataTable)this.ug_pump.DataSource).Rows.Add(row);
            }
            else
            {
                DataRow row = ((DataTable)this.ug_checkpoint.DataSource).NewRow();
                row["PUMPSTATION_ID"] = this.cboPumpStation.SelectedValue;
                ((DataTable)this.ug_checkpoint.DataSource).Rows.Add(row);
            }
        }

    }
}
