﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion

namespace WaterNet.WE_BaseInfoSet
{
    public partial class frmTagLink : Form
    {
        private string _tagname = string.Empty;

        public frmTagLink()
        {
            InitializeComponent();
        }

        #region 초기화 설정
        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitializeGridSetting()
        {
            //m_oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            //m_oDBManager.Open();

            UltraGridColumn oUltraGridColumn;
            ///태그 설정 그리드
            oUltraGridColumn = this.ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "TAG명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = this.ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_GBN";
            oUltraGridColumn.Header.Caption = "TAG구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = this.ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAG_DESC";
            oUltraGridColumn.Header.Caption = "TAG설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 300;

            FormManager.SetGridStyle(this.ugData);

        }

        private void InitializeControlSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT DISTINCT B.TAG_GBN CD, B.TAG_GBN CDNM FROM IF_IHTAGS A,IF_TAG_GBN B ");
            oStringBuilder.AppendLine("  WHERE A.TAGNAME = B.TAGNAME ");
            FormManager.SetComboBoxEX(this.cboTagGbn, oStringBuilder.ToString(), true);

        }

        private void InitializeEvnetSetting()
        {
            this.btnSearch.Click += new EventHandler(btnSearch_Click);
            this.btnChoice.Click += new EventHandler(btnChoice_Click);
        }
        #endregion 초기화 설정

        public string TAGNAME
        {
            get { return _tagname; }
        }

        void btnChoice_Click(object sender, EventArgs e)
        {
            if (this.ugData.ActiveRow != null)
            {
                _tagname = Convert.ToString(this.ugData.ActiveRow.Cells["TAGNAME"].Value);
                this.Close();
            }
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
                oDBManager.Open();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT b.tagname, B.TAG_GBN , B.tag_desc FROM IF_IHTAGS A,IF_TAG_GBN B ");
                oStringBuilder.AppendLine("  WHERE A.TAGNAME = B.TAGNAME ");
                oStringBuilder.AppendLine("    AND B.TAG_DESC LIKE '%" + txtDescript.Text + "%'");
                if (this.cboTagGbn.SelectedIndex > 0)
                {
                    oStringBuilder.AppendLine("    AND B.TAG_GBN = '" + cboTagGbn.SelectedValue + "'");
                }

                this.ugData.DataSource = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        private void frmTagLink_Load(object sender, EventArgs e)
        {
            InitializeGridSetting();
            InitializeControlSetting();
            InitializeEvnetSetting();
        }
    }
}
