﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WV_Common.work;
using WaterNet.WE_PumpCharacteristicCurve.dao;
using System.Collections;
using WaterNet.WE_Common.data;

namespace WaterNet.WE_PumpCharacteristicCurve.work
{
    public class CharacteristicCurveWork : BaseWork
    {
        private static CharacteristicCurveWork work = null;
        private CharacteristicCurveDao dao = null;

        private CharacteristicCurveWork()
        {
            dao = CharacteristicCurveDao.GetInstance();
        }

        public static CharacteristicCurveWork GetInstance()
        {
            if (work == null)
            {
                work = new CharacteristicCurveWork();
            }
            return work;
        }

        public DataTable SelectSimulationAnalysisResult(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectSimulationAnalysisResult(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectSimulationAnalysisResultDetail(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectSimulationAnalysisResultDetail(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                        result.Columns.Add("W_ENERGY", typeof(double));
                        result.Columns.Add("O_EFFICIENCY", typeof(double));
                        result.Columns.Add("R_O", typeof(double));
                        result.Columns.Add("RESULT");

                        foreach (DataRow row in result.Rows)
                        {
                            SimulationAnalysisData data = new SimulationAnalysisData(row);
                            if (data.H == 0)
                            {
                                row["H"] = DBNull.Value;
                            }
                            else
                            {
                                row["H"] = data.H;
                            }

                            if (data.W_ENERGY == 0)
                            {
                                row["W_ENERGY"] = DBNull.Value;
                            }
                            else
                            {
                                row["W_ENERGY"] = data.W_ENERGY;
                            }

                            if (data.O_EFFICIENCY == 0)
                            {
                                row["O_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["O_EFFICIENCY"] = data.O_EFFICIENCY;
                            }

                            if (data.R_EFFICIENCY == 0)
                            {
                                row["R_EFFICIENCY"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_EFFICIENCY"] = data.R_EFFICIENCY;
                            }

                            if (data.R_O == 0)
                            {
                                row["R_O"] = DBNull.Value;
                            }
                            else
                            {
                                row["R_O"] = data.R_O;
                            }

                            row["RESULT"] = data.RESULT;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectSimulationAnalysisFlow(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                //펌프의 모델 YN을 조회해서 모델적용인경우 성능곡선 목록을 가져오고
                //모델적용이 아닐경우 모델의 값을 조회한다.
                DataSet dataSet = null;

                //string model_yn = dao.SelectCurveModelYN(base.DataBaseManager, parameter).ToString();

                //if (model_yn == "Y")
                //{
                //    dataSet = dao.SelectSimulationAnalysisFlow(base.DataBaseManager, parameter);
                //}
                //else if (model_yn == "N")
                //{
                //    parameter["C_ID"] = dao.SelectCurveModelCurveID(base.DataBaseManager, parameter);
                //    parameter["E_ID"] = dao.SelectCurveModelEnergyID(base.DataBaseManager, parameter);

                //    dataSet = dao.SelectSimulationAnalysisFlow_MODEL(base.DataBaseManager, parameter);
                //}

                dataSet = dao.SelectSimulationAnalysisFlow(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectSimulationAnalysisCurve(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet = null;

                dataSet = dao.SelectSimulationAnalysisCurve(base.DataBaseManager, parameter);

                //string model_yn = dao.SelectCurveModelYN(base.DataBaseManager, parameter).ToString();

                //if (model_yn == "Y")
                //{
                //    dataSet = dao.SelectSimulationAnalysisCurve(base.DataBaseManager, parameter);
                //}
                //else if (model_yn == "N")
                //{
                //    parameter["C_ID"] = dao.SelectCurveModelCurveID(base.DataBaseManager, parameter);
                //    parameter["E_ID"] = dao.SelectCurveModelEnergyID(base.DataBaseManager, parameter);

                //    dataSet = dao.SelectSimulationAnalysisCurve_MODEL(base.DataBaseManager, parameter);
                //}

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectPumpValueList(Hashtable parameter)
        {
            DataTable data = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();

                data = dao.SelectPumpValueList(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }

            return data;
        }
    }
}
