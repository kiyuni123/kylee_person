﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.control;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using WaterNet.WE_PumpCharacteristicCurve.work;
using WaterNet.WV_Common.util;
using ChartFX.WinForms.Annotation;
using ChartFX.WinForms;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_PumpCharacteristicCurve.form
{
    public partial class frmMain : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        //차트 매니저(차트 스타일 설정및 기타공통기능설정)
        private ChartManager chartManager = null;

        //테이블// 그래프 레이아웃 
        private TableLayout tableLayout = null;

        private Hashtable pumpInfo = null;

        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        public frmMain(Hashtable pumpInfo)
        {
            this.pumpInfo = pumpInfo;
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeChart();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.tableLayout = new TableLayout(this.tableLayoutPanel1, this.chartPanel1, this.tablePanel1, this.chartVisibleBtn, this.tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();
            DataTable station = new DataTable();
            station.Columns.Add("CODE");
            station.Columns.Add("CODE_NAME");
            station.Rows.Add(null, "전체");

            foreach (DataRow row in table.Rows)
            {
                station.Rows.Add(row["CODE"], row["CODE_NAME"].ToString());
            }

            this.bzs.ValueMember = "CODE";
            this.bzs.DisplayMember = "CODE_NAME";
            this.bzs.DataSource = station;

            this.startDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00");
            this.endDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 23:59");

            if (this.pumpInfo != null)
            {
                this.startDate.Value = this.pumpInfo["STARTDATE"].ToString();
                this.endDate.Value = this.pumpInfo["ENDDATE"].ToString();
                this.bzs.SelectedValue = this.pumpInfo["PUMPSTATION_ID"];
                this.pump.SelectedValue = this.pumpInfo["PUMP_FTR_IDN"];
                this.searchBtn.PerformClick();
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.flowLayoutPanel1.SizeChanged += new EventHandler(sizeChanged_EventHandler);
            this.pump.SelectedValueChanged += new EventHandler(pump_SelectedValueChanged);
            this.curve.CheckedChanged += new EventHandler(chart_series_CheckedChanged);
            this.h.CheckedChanged += new EventHandler(chart_series_CheckedChanged);
            this.e.CheckedChanged += new EventHandler(chart_series_CheckedChanged);
            this.bzs.SelectedIndexChanged += new EventHandler(bzs_SelectedIndexChanged);
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid1, this.Text);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void chart_series_CheckedChanged(object sender, EventArgs e)
        {
            this.InitializeChartSeries();
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chart1.Data.Clear();
            this.chart1.Series.Clear();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;
            this.InitializeGridColumn();
            this.gridManager.DefaultColumnsMapping2(this.ultraGrid1);
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "시간";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Format = "yyyy-MM-dd HH:mm";
            column.Hidden = false;
            column.RowLayoutColumnInfo.OriginX = 0;
            column.RowLayoutColumnInfo.OriginY = 0;
            column.RowLayoutColumnInfo.SpanX = 1;
            column.RowLayoutColumnInfo.SpanY = 2;
            #endregion
        }

        private void pump_SelectedValueChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (this.pump.DataSource == null)
            {
                return;
            }

            this.flowLayoutPanel1.Controls.Clear();

            if (this.pump.SelectedIndex == 0)
            {
                foreach (DataRow row in ((DataTable)this.pump.DataSource).Rows)
                {
                    if (row.Table.Rows.IndexOf(row) != 0)
                    {
                        CheckBox chk = new CheckBox();
                        chk.Name = row["CODE"].ToString();
                        chk.Text = row["CODE_NAME"].ToString();
                        chk.Checked = true;
                        chk.CheckedChanged += new EventHandler(chart_series_CheckedChanged);

                        Graphics g = chk.CreateGraphics();
                        chk.Width = (int)g.MeasureString(row["CODE_NAME"].ToString(), chk.Font).Width + 30;
                        g.Dispose();

                        this.flowLayoutPanel1.Controls.Add(chk);
                    }
                }
            }
            else
            {
                foreach (DataRow row in ((DataTable)this.pump.DataSource).Rows)
                {
                    if (this.pump.SelectedValue != null && row["CODE"].ToString() == this.pump.SelectedValue.ToString())
                    {
                        CheckBox chk = new CheckBox();
                        chk.Name = row["CODE"].ToString();
                        chk.Text = row["CODE_NAME"].ToString();
                        chk.Checked = true;
                        chk.CheckedChanged += new EventHandler(chart_series_CheckedChanged);

                        Graphics g = chk.CreateGraphics();
                        chk.Width = (int)g.MeasureString(row["CODE_NAME"].ToString(), chk.Font).Width + 30;
                        g.Dispose();

                        this.flowLayoutPanel1.Controls.Add(chk);
                        break;
                    }
                }
            }

            //this.InitGrid();
            //this.InitChart();
            //this.gridManager.DefaultColumnsMapping2(this.ultraGrid1);
            this.Cursor = Cursors.Default;
        }

        private void bzs_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable parameter = new Hashtable();
            parameter["BZS"] = this.bzs.SelectedValue;

            DataTable table = CharacteristicCurveWork.GetInstance().SelectPumpValueList(parameter);
            DataTable data = new DataTable();
            data.Columns.Add("CODE");
            data.Columns.Add("CODE_NAME");
            data.Rows.Add(null, "전체");

            foreach (DataRow row in table.Rows)
            {
                data.Rows.Add(row["CODE"], row["CODE_NAME"].ToString());
            }

            this.pump.ValueMember = "CODE";
            this.pump.DisplayMember = "CODE_NAME";
            this.pump.DataSource = data;
        }

        /// <summary>
        /// 높이 변환 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sizeChanged_EventHandler(object sender, EventArgs e)
        {
            int oHeight = this.flowLayoutPanel1.Height + this.groupBox2.Margin.Top + this.groupBox2.Margin.Bottom + 15;
            this.groupBox2.Height = oHeight;
            this.setPanel.Height = oHeight;
        }

        private DataTable pumpList = null;
        Hashtable parameter = null;
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.pumpList = new DataTable();
                this.parameter = new Hashtable();

                this.parameter["STARTDATE"] = Convert.ToDateTime(this.startDate.Value).ToString("yyyyMMddHHmm");
                this.parameter["ENDDATE"] = Convert.ToDateTime(this.endDate.Value).ToString("yyyyMMddHHmm");

                this.pumpList.Columns.Add("PUMP_FTR_IDN");
                this.pumpList.Columns.Add("REMARK");
                this.pumpList.Columns.Add("STARTDATE");
                this.pumpList.Columns.Add("ENDDATE");

                foreach (Control control in this.flowLayoutPanel1.Controls)
                {
                    if (control is CheckBox)
                    {
                        this.pumpList.Rows.Add(control.Name, control.Text, this.parameter["STARTDATE"], this.parameter["ENDDATE"]);
                    }
                }

                this.SelectPumpCharacteristicCurve();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectPumpCharacteristicCurve()
        {
            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid1.DataSource = CharacteristicCurveWork.GetInstance().SelectSimulationAnalysisResult(this.parameter);
            this.InitGrid();
            this.SetGridColumns(this.pumpList);

            foreach (DataRow row in this.pumpList.Rows)
            {
                this.SetGridData(CharacteristicCurveWork.GetInstance().SelectSimulationAnalysisResultDetail(Utils.ConverToHashtable(row)));
            }

            this.InitializeChartSetting();
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 그리드내 데이터 컬럼을 설정한다.
        /// </summary>
        private void SetGridData(DataTable table)
        {
            if (table == null)
            {
                return;
            }

            foreach (DataRow dataRow in table.Rows)
            {
                foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
                {
                    if (Convert.ToDateTime(dataRow["TIME"]).ToString("yyyyMMddHHmm") ==
                        Convert.ToDateTime(gridRow.Cells["TIMESTAMP"].Value).ToString("yyyyMMddHHmm"))
                    {
                        gridRow.Cells[dataRow["PUMP_FTR_IDN"].ToString() + "_" + "FLOW"].Value = dataRow["FLOW"];
                        gridRow.Cells[dataRow["PUMP_FTR_IDN"].ToString() + "_" + "H"].Value = dataRow["H"];
                        gridRow.Cells[dataRow["PUMP_FTR_IDN"].ToString() + "_" + "ENERGY"].Value = dataRow["ENERGY"];
                        gridRow.Cells[dataRow["PUMP_FTR_IDN"].ToString() + "_" + "W_ENERGY"].Value = dataRow["W_ENERGY"];
                        gridRow.Cells[dataRow["PUMP_FTR_IDN"].ToString() + "_" + "O_EFFICIENCY"].Value = dataRow["O_EFFICIENCY"];
                    }
                }
            }
        }

        private void InitializeChartSetting()
        {
            this.InitChart();
            this.chart1.LegendBox.Visible = true;

            if (this.ultraGrid1.DataSource == null)
            {
                return;
            }

            int series = -1;

            DataTable flow = new DataTable();
            flow.Columns.Add("FLOW", typeof(double));

            //X축 전체를 가져와야한다.
            foreach (UltraGridGroup group in this.ultraGrid1.DisplayLayout.Bands[0].Groups)
            {
                Hashtable parameter = new Hashtable();
                parameter["PUMP_FTR_IDN"] = group.Key;
                DataTable temp = CharacteristicCurveWork.GetInstance().SelectSimulationAnalysisFlow(parameter);

                if (temp != null)
                {
                    foreach (DataRow row in temp.Rows)
                    {
                        flow.Rows.Add(row["FLOW"]);
                    }
                }
            }

            List<double> step = new List<double>();
            foreach (DataRow row in flow.Rows)
            {
                step.Add(Utils.ToDouble(row["FLOW"]));
            }

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                foreach (UltraGridGroup group in this.ultraGrid1.DisplayLayout.Bands[0].Groups)
                {
                    step.Add(Convert.ToDouble(gridRow.Cells[group.Key + "_FLOW"].Value));
                }
            }

            step = step.Distinct().ToList();
            step.Sort();

            if (step.Count == 1 && step[0] == 0)
            {
                return;
            }

            this.chart1.Data.Series = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Count * 4;

            this.chart1.Data.Points = step.Count;

            for (int i = 0; i < step.Count; i++)
            {
                this.chart1.Data.X[i] = step[i];
            }

            //그룹Key가 펌프관리번호임
            //곡선을 가져와서 보여준다.
            foreach (UltraGridGroup group in this.ultraGrid1.DisplayLayout.Bands[0].Groups)
            {
                Hashtable parameter = new Hashtable();
                parameter["PUMP_FTR_IDN"] = group.Key;

                DataTable table = CharacteristicCurveWork.GetInstance().SelectSimulationAnalysisCurve(parameter);

                //수두곡선과 효율곡선의 데이터를 다시 만든다..............
                //우선 유량값을 추가해준다......
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (i == table.Rows.Count - 1)
                    {
                        break;
                    }

                    double f1 = Utils.ToDouble(table.Rows[i]["FLOW"]);
                    double f2 = Utils.ToDouble(table.Rows[i + 1]["FLOW"]);

                    for (int k = 0; k < step.Count; k++)
                    {
                        double f = step[k];

                        if (f > f1 && f < f2)
                        {
                            DataRow newRow = table.NewRow();
                            newRow["FLOW"] = f;
                            newRow["H"] = DBNull.Value;
                            newRow["E"] = DBNull.Value;
                            table.Rows.InsertAt(newRow, i + 1);
                            break;
                        }
                    }
                }

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (i == table.Rows.Count - 1)
                    {
                        break;
                    }

                    int count = 0;
                    int endIndex = 0;

                    double start_flow = 0;
                    double end_flow = 0;
                    double before_flow = 0;

                    if (table.Rows[i]["H"] == DBNull.Value && table.Rows[i]["E"] == DBNull.Value)
                    {
                        if (i != 0)
                        {
                            before_flow = Utils.ToDouble(table.Rows[i - 1]["FLOW"]);
                        }

                        start_flow = Utils.ToDouble(table.Rows[i]["FLOW"]);

                        for (int j = i; j < table.Rows.Count; j++)
                        {
                            if (j == table.Rows.Count - 1)
                            {
                                break;
                            }
                            count++;
                            if (table.Rows[j + 1]["H"] != DBNull.Value && table.Rows[j + 1]["E"] != DBNull.Value)
                            {
                                end_flow = Utils.ToDouble(table.Rows[j + 1]["FLOW"]);
                                endIndex = j + 1;
                                break;
                            }
                        }
                    }

                    //값수정
                    if (i != 0 && count != 0)
                    {
                        double bh = Utils.ToDouble(table.Rows[i - 1]["H"]);
                        double ah = Utils.ToDouble(table.Rows[endIndex]["H"]);

                        double be = Utils.ToDouble(table.Rows[i - 1]["E"]);
                        double ae = Utils.ToDouble(table.Rows[endIndex]["E"]);

                        table.Rows[i]["H"] = ((ah - bh) / 100) * ((start_flow - before_flow) / ((start_flow - before_flow) + (end_flow - start_flow)) * 100) + Utils.ToDouble(table.Rows[i - 1]["H"]);
                        table.Rows[i]["E"] = ((ae - be) / 100) * ((start_flow - before_flow) / ((start_flow - before_flow) + (end_flow - start_flow)) * 100) + Utils.ToDouble(table.Rows[i - 1]["E"]);
                    }
                }

                if (table != null)
                {
                    series++;
                    foreach (DataRow row in table.Rows)
                    {
                        int point = 0;
                        for (int k = 0; k < step.Count; k++)
                        {
                            if (step[k].ToString() == row["FLOW"].ToString())
                            {
                                point = k;
                            }
                        }

                        this.chart1.Data.Y[series, point] = Utils.ToDouble(row["H"]);
                    }

                    this.chart1.Series[series].AxisY = this.chart1.AxisY;
                    this.chart1.Series[series].Line.Width = 1;
                    this.chart1.Series[series].MarkerSize = 0;
                    this.chart1.Series[series].MarkerShape = MarkerShape.None;
                    this.chart1.Series[series].Text = "수두곡선(" + group.Header.Caption + ")";

                    series++;
                    foreach (DataRow row in table.Rows)
                    {
                        int point = 0;
                        for (int k = 0; k < step.Count; k++)
                        {
                            if (step[k].ToString() == row["FLOW"].ToString())
                            {
                                point = k;
                            }
                        }

                        this.chart1.Data.Y[series, point] = Utils.ToDouble(row["E"]);
                    }

                    this.chart1.Series[series].AxisY = this.chart1.AxisY2;
                    this.chart1.Series[series].Line.Width = 1;
                    this.chart1.Series[series].MarkerSize = 0;
                    this.chart1.Series[series].MarkerShape = MarkerShape.None;
                    this.chart1.Series[series].Text = "효율곡선(" + group.Header.Caption + ")";
                }

                series++;
                this.chart1.Series[series].AxisY = this.chart1.AxisY;
                this.chart1.Series[series].Gallery = ChartFX.WinForms.Gallery.Scatter;
                this.chart1.Series[series].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
                this.chart1.Series[series].Border.Visible = false;
                this.chart1.Series[series].MarkerSize = 2;
                this.chart1.Series[series].Text = "유량-양정(" + group.Header.Caption + ")";

                foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
                {
                    int point = 0;
                    for (int k = 0; k < step.Count; k++)
                    {
                        if (gridRow.Cells[group.Key + "_FLOW"].Value != null && step[k].ToString() == gridRow.Cells[group.Key + "_FLOW"].Value.ToString())
                        {
                            point = k;
                        }
                    }
                    if (gridRow.Cells[group.Key + "_H"].Value != DBNull.Value)
                    {
                        this.chart1.Data[series, point] = Convert.ToDouble(gridRow.Cells[group.Key + "_H"].Value);
                    }
                }

                series++;
                this.chart1.Series[series].AxisY = this.chart1.AxisY2;
                this.chart1.Series[series].Gallery = ChartFX.WinForms.Gallery.Scatter;
                this.chart1.Series[series].MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
                this.chart1.Series[series].Border.Visible = false;
                this.chart1.Series[series].MarkerSize = 2;
                this.chart1.Series[series].Text = "유량-효율(" + group.Header.Caption + ")";

                foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
                {
                    int point = 0;
                    for (int k = 0; k < step.Count; k++)
                    {
                        if (gridRow.Cells[group.Key + "_FLOW"].Value != null && step[k].ToString() == gridRow.Cells[group.Key + "_FLOW"].Value.ToString())
                        {
                            point = k;
                        }
                    }

                    if (gridRow.Cells[group.Key + "_O_EFFICIENCY"].Value != DBNull.Value)
                    {
                        this.chart1.Data[series, point] = Convert.ToDouble(gridRow.Cells[group.Key + "_O_EFFICIENCY"].Value);
                    }
                }
            }

            this.chart1.AxisX.Title.Text = "유량(㎥/h)";
            this.chart1.AxisY.Title.Text = "양정(m)";
            this.chart1.AxisY2.Title.Text = "효율(%)";

            this.InitializeChartSeries();
        }

        private void InitializeChartSeries()
        {
            for (int i = 0; i < this.flowLayoutPanel1.Controls.Count; i++)
            {
                if (this.flowLayoutPanel1.Controls[i] is CheckBox)
                {
                    for (int j = i * 4; j < (i + 1) * 4; j++)
                    {
                        if (((CheckBox)this.flowLayoutPanel1.Controls[i]).Checked)
                        {
                            if (this.chart1.Series.Count != 0)
                            {
                                if (j % 4 == 0 || j % 4 == 1)
                                {
                                    if (this.chart1.Series.Count - 1 >= j)
                                    {
                                        this.chart1.Series[j].Visible = this.curve.Checked;
                                    }
                                }

                                if (j % 4 == 2)
                                {
                                    if (this.chart1.Series.Count - 1 >= j)
                                    {
                                        this.chart1.Series[j].Visible = this.h.Checked;
                                    }
                                }

                                if (j % 4 == 3)
                                {
                                    if (this.chart1.Series.Count - 1 >= j)
                                    {
                                        this.chart1.Series[j].Visible = this.e.Checked;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (this.chart1.Series.Count - 1 >= j)
                            {
                                this.chart1.Series[j].Visible = false;
                            }
                        }
                    }
                }
            }
        }

        //컬럼을 설정한다.
        private void SetGridColumns(DataTable table)
        {
            UltraGridGroup group = null;
            UltraGridColumn column = null;
            bool isFirst = true;

            foreach (DataRow dataRow in table.Rows)
            {
                group = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                group.Key = dataRow["PUMP_FTR_IDN"].ToString();
                group.Header.Caption = dataRow["REMARK"].ToString();
                group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
                group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                group.RowLayoutGroupInfo.LabelSpan = 1;
                group.RowLayoutGroupInfo.SpanX = 1;
                group.RowLayoutGroupInfo.SpanY = 1;

                if (isFirst)
                {
                    group.RowLayoutGroupInfo.OriginX = 1;
                    isFirst = false;
                }

                column = this.GetColumn("###,###,###");
                column.Key = dataRow["PUMP_FTR_IDN"].ToString() + "_" + "FLOW";
                column.Width = 90;
                column.Header.Caption = "유량(㎥/h)";
                column.RowLayoutColumnInfo.ParentGroup = group;

                column = this.GetColumn("N1");
                column.Key = dataRow["PUMP_FTR_IDN"].ToString() + "_" + "H";
                column.Width = 70;
                column.Header.Caption = "양정(m)";
                column.RowLayoutColumnInfo.ParentGroup = group;

                column = this.GetColumn("###,###,###");
                column.Key = dataRow["PUMP_FTR_IDN"].ToString() + "_" + "ENERGY";
                column.Width = 80;
                column.Header.Caption = "전력량(kwh)";
                column.RowLayoutColumnInfo.ParentGroup = group;

                column = this.GetColumn("N3");
                column.Key = dataRow["PUMP_FTR_IDN"].ToString() + "_" + "W_ENERGY";
                column.Width = 100;
                column.Header.Caption = "원단위(kwh/㎥)";
                column.RowLayoutColumnInfo.ParentGroup = group;

                column = this.GetColumn("N1");
                column.Key = dataRow["PUMP_FTR_IDN"].ToString() + "_" + "O_EFFICIENCY";
                column.Width = 80;
                column.Header.Caption = "효율(%)";
                column.RowLayoutColumnInfo.ParentGroup = group;
            }
        }

        /// <summary>
        /// 동적으로 생성한 그룹을 삭제한다.
        /// </summary>
        private void InitGrid()
        {
            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Count; i > 0; i--)
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Groups.Remove(i - 1);
            }

            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count; i > 0; i--)
            {
                if (!this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].IsBound)
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns.Remove(i - 1);
                }
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Clear();
        }

        private void InitChart()
        {
            this.chart1.AxisY.Sections.Clear();
            this.chart1.ConditionalAttributes.Clear();
            this.chart1.Extensions.Clear();
            this.chart1.LegendBox.CustomItems.Clear();
            this.chart1.Series.Clear();
            this.chart1.Data.Clear();
            this.chartManager.AllClear(this.chart1);
        }

        private UltraGridColumn GetColumn(string format)
        {
            UltraGridColumn gridColumn = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            gridColumn.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            gridColumn.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            gridColumn.RowLayoutColumnInfo.LabelPosition = LabelPosition.Default;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            gridColumn.RowLayoutColumnInfo.SpanX = 1;
            gridColumn.RowLayoutColumnInfo.SpanY = 1;
            gridColumn.RowLayoutColumnInfo.LabelSpan = 1;
            gridColumn.SortIndicator = SortIndicator.Disabled;
            gridColumn.DataType = typeof(double);
            gridColumn.Format = format;
            gridColumn.DefaultCellValue = DBNull.Value;
            gridColumn.Hidden = false;
            return gridColumn;
        }
    }
}
