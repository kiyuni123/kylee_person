﻿namespace WaterNet.WE_PumpCharacteristicCurve.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pump = new System.Windows.Forms.ComboBox();
            this.TimeToTime = new System.Windows.Forms.Panel();
            this.endDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.startDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.TimeToTimeIntervalText = new System.Windows.Forms.Label();
            this.LargeBlock = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.bzs = new System.Windows.Forms.ComboBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.chartVisibleBtn = new System.Windows.Forms.Button();
            this.tableVisibleBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel1 = new System.Windows.Forms.Panel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.chartPanel1 = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.panel2 = new System.Windows.Forms.Panel();
            this.setPanel = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.e = new System.Windows.Forms.CheckBox();
            this.h = new System.Windows.Forms.CheckBox();
            this.curve = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.TimeToTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.endDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDate)).BeginInit();
            this.LargeBlock.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.chartPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.setPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 758);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1011, 10);
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(1021, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 758);
            this.pictureBox3.TabIndex = 16;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 758);
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1031, 10);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.TimeToTime);
            this.groupBox1.Controls.Add(this.LargeBlock);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1011, 54);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pump);
            this.panel1.Location = new System.Drawing.Point(585, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 22);
            this.panel1.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "펌프";
            // 
            // pump
            // 
            this.pump.FormattingEnabled = true;
            this.pump.Location = new System.Drawing.Point(38, 0);
            this.pump.Name = "pump";
            this.pump.Size = new System.Drawing.Size(161, 20);
            this.pump.TabIndex = 5;
            // 
            // TimeToTime
            // 
            this.TimeToTime.Controls.Add(this.endDate);
            this.TimeToTime.Controls.Add(this.startDate);
            this.TimeToTime.Controls.Add(this.label4);
            this.TimeToTime.Controls.Add(this.TimeToTimeIntervalText);
            this.TimeToTime.Location = new System.Drawing.Point(6, 20);
            this.TimeToTime.Name = "TimeToTime";
            this.TimeToTime.Size = new System.Drawing.Size(350, 22);
            this.TimeToTime.TabIndex = 19;
            // 
            // endDate
            // 
            this.endDate.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.endDate.Location = new System.Drawing.Point(211, 1);
            this.endDate.MaskInput = "{date} hh:mm";
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(137, 21);
            this.endDate.TabIndex = 15;
            this.endDate.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // startDate
            // 
            this.startDate.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.startDate.Location = new System.Drawing.Point(59, 0);
            this.startDate.MaskInput = "{date} hh:mm";
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(137, 21);
            this.startDate.TabIndex = 14;
            this.startDate.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(198, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "-";
            // 
            // TimeToTimeIntervalText
            // 
            this.TimeToTimeIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeToTimeIntervalText.AutoSize = true;
            this.TimeToTimeIntervalText.Location = new System.Drawing.Point(3, 4);
            this.TimeToTimeIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.TimeToTimeIntervalText.Name = "TimeToTimeIntervalText";
            this.TimeToTimeIntervalText.Size = new System.Drawing.Size(53, 12);
            this.TimeToTimeIntervalText.TabIndex = 1;
            this.TimeToTimeIntervalText.Text = "검색기간";
            // 
            // LargeBlock
            // 
            this.LargeBlock.Controls.Add(this.label10);
            this.LargeBlock.Controls.Add(this.bzs);
            this.LargeBlock.Location = new System.Drawing.Point(362, 20);
            this.LargeBlock.Name = "LargeBlock";
            this.LargeBlock.Size = new System.Drawing.Size(215, 22);
            this.LargeBlock.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "사업장";
            // 
            // bzs
            // 
            this.bzs.FormattingEnabled = true;
            this.bzs.Location = new System.Drawing.Point(51, 0);
            this.bzs.Name = "bzs";
            this.bzs.Size = new System.Drawing.Size(161, 20);
            this.bzs.TabIndex = 5;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.Location = new System.Drawing.Point(971, 3);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 44;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(926, 3);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 41;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // chartVisibleBtn
            // 
            this.chartVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chartVisibleBtn.Location = new System.Drawing.Point(814, 3);
            this.chartVisibleBtn.Name = "chartVisibleBtn";
            this.chartVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.chartVisibleBtn.TabIndex = 40;
            this.chartVisibleBtn.TabStop = false;
            this.chartVisibleBtn.Text = "그래프";
            this.chartVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // tableVisibleBtn
            // 
            this.tableVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableVisibleBtn.Location = new System.Drawing.Point(870, 3);
            this.tableVisibleBtn.Name = "tableVisibleBtn";
            this.tableVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.tableVisibleBtn.TabIndex = 39;
            this.tableVisibleBtn.TabStop = false;
            this.tableVisibleBtn.Text = "테이블";
            this.tableVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tablePanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.chartPanel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 149);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.84848F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.15152F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1011, 609);
            this.tableLayoutPanel1.TabIndex = 46;
            // 
            // tablePanel1
            // 
            this.tablePanel1.BackColor = System.Drawing.Color.White;
            this.tablePanel1.Controls.Add(this.ultraGrid1);
            this.tablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel1.Location = new System.Drawing.Point(0, 247);
            this.tablePanel1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Size = new System.Drawing.Size(1011, 362);
            this.tablePanel1.TabIndex = 1;
            // 
            // ultraGrid1
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance4;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance5;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance10;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1011, 362);
            this.ultraGrid1.TabIndex = 0;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // chartPanel1
            // 
            this.chartPanel1.BackColor = System.Drawing.Color.White;
            this.chartPanel1.Controls.Add(this.chart1);
            this.chartPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel1.Location = new System.Drawing.Point(0, 0);
            this.chartPanel1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.chartPanel1.Name = "chartPanel1";
            this.chartPanel1.Size = new System.Drawing.Size(1011, 237);
            this.chartPanel1.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Spread;
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(1011, 237);
            this.chart1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(10, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1011, 10);
            this.panel2.TabIndex = 47;
            // 
            // setPanel
            // 
            this.setPanel.AutoSize = true;
            this.setPanel.Controls.Add(this.groupBox2);
            this.setPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.setPanel.Location = new System.Drawing.Point(10, 74);
            this.setPanel.Name = "setPanel";
            this.setPanel.Size = new System.Drawing.Size(1011, 45);
            this.setPanel.TabIndex = 48;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flowLayoutPanel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1011, 45);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "펌프선택";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 17);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1005, 0);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.chartVisibleBtn);
            this.panel4.Controls.Add(this.tableVisibleBtn);
            this.panel4.Controls.Add(this.e);
            this.panel4.Controls.Add(this.searchBtn);
            this.panel4.Controls.Add(this.excelBtn);
            this.panel4.Controls.Add(this.h);
            this.panel4.Controls.Add(this.curve);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(10, 119);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1011, 30);
            this.panel4.TabIndex = 49;
            // 
            // e
            // 
            this.e.AutoSize = true;
            this.e.Location = new System.Drawing.Point(192, 8);
            this.e.Name = "e";
            this.e.Size = new System.Drawing.Size(78, 16);
            this.e.TabIndex = 3;
            this.e.Text = "유량-효율";
            this.e.UseVisualStyleBackColor = true;
            // 
            // h
            // 
            this.h.AutoSize = true;
            this.h.Location = new System.Drawing.Point(108, 8);
            this.h.Name = "h";
            this.h.Size = new System.Drawing.Size(78, 16);
            this.h.TabIndex = 1;
            this.h.Text = "유량-양정";
            this.h.UseVisualStyleBackColor = true;
            // 
            // curve
            // 
            this.curve.AutoSize = true;
            this.curve.Checked = true;
            this.curve.CheckState = System.Windows.Forms.CheckState.Checked;
            this.curve.Location = new System.Drawing.Point(6, 8);
            this.curve.Name = "curve";
            this.curve.Size = new System.Drawing.Size(96, 16);
            this.curve.TabIndex = 0;
            this.curve.Text = "펌프성능곡선";
            this.curve.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 768);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.setPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmMain";
            this.Text = "펌프특성곡선분석";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.TimeToTime.ResumeLayout(false);
            this.TimeToTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.endDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDate)).EndInit();
            this.LargeBlock.ResumeLayout(false);
            this.LargeBlock.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.chartPanel1.ResumeLayout(false);
            this.chartPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.setPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel LargeBlock;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox bzs;
        private System.Windows.Forms.Panel TimeToTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TimeToTimeIntervalText;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.Button chartVisibleBtn;
        private System.Windows.Forms.Button tableVisibleBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel tablePanel1;
        private System.Windows.Forms.Panel chartPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel setPanel;
        private System.Windows.Forms.CheckBox curve;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox e;
        private System.Windows.Forms.CheckBox h;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox pump;
        private ChartFX.WinForms.Chart chart1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor endDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor startDate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}