﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Oracle.DataAccess.Client;
using WaterNet.WaterNetCore;

namespace WaterNet.WE_PumpCharacteristicCurve.dao
{
    public class CharacteristicCurveDao
    {
        private static CharacteristicCurveDao dao = null;
        private CharacteristicCurveDao() { }

        public static CharacteristicCurveDao GetInstance()
        {
            if (dao == null)
            {
                dao = new CharacteristicCurveDao();
            }
            return dao;
        }

        public DataSet SelectSimulationAnalysisResult(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TIMESTAMP ");
            query.AppendLine("  FROM WE_RPT ");
            query.AppendLine(" WHERE TIMESTAMP BETWEEN TO_DATE(:STARTDATE,'YYYYMMDDHH24MI') ");
            query.AppendLine("                     AND TO_DATE(:ENDDATE,'YYYYMMDDHH24MI') ");
            query.AppendLine("GROUP BY TIMESTAMP ");
            query.AppendLine("ORDER BY TIMESTAMP ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectSimulationAnalysisResultDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with pump as                                                                 ");
            //query.AppendLine("(                                                                            ");
            //query.AppendLine("select c.value1 pump_ftr_idn                                                 ");
            //query.AppendLine("      ,null remark                                                       ");
            //query.AppendLine("      ,(                                                                     ");
            //query.AppendLine("      select sum(value)                                                      ");
            //query.AppendLine("        from if_tag_mapping b                                                ");
            //query.AppendLine("            ,if_gather_realtime c                                            ");
            //query.AppendLine("       where b.ftr_idn = c.value1                                            ");
            //query.AppendLine("         and b.ftr_gbn = '000004'                                            ");
            //query.AppendLine("         and c.tagname = b.tagname                                           ");
            //query.AppendLine("         and c.timestamp between to_date(:STARTDATE,'yyyymmddhh24mi')        ");
            //query.AppendLine("                             and to_date(:ENDDATE,'yyyymmddhh24mi')          ");
            //query.AppendLine("      ) time                                                                 ");
            //query.AppendLine("  from wh_title a                                                            ");
            //query.AppendLine("      ,wh_pumps b                                                            ");
            //query.AppendLine("      ,wh_tags c                                                             ");
            //query.AppendLine("  where a.energy_gbn = 'Y'                                                     ");
            //query.AppendLine("    and b.inp_number = a.inp_number                                          ");
            //query.AppendLine("    and c.inp_number = b.inp_number                                          ");
            //query.AppendLine("    and c.id = b.id                                                          ");
            //query.AppendLine("    and c.value1 = :PUMP_FTR_IDN                                             ");
            //query.AppendLine(")                                                                            ");
            //query.AppendLine("select b.pumpstation_id                                                      ");
            //query.AppendLine("      ,b.pump_ftr_idn                                                        ");
            //query.AppendLine("      ,nvl(b.remark, a.remark) remark                                        ");
            //query.AppendLine("      ,c.timestamp time                                                      ");
            //query.AppendLine("      ,c.flow                                                                ");
            //query.AppendLine("      ,c.h                                                                   ");
            //query.AppendLine("      ,c.energy                                                              ");
            //query.AppendLine("      ,b.diff                                                                ");
            //query.AppendLine("      ,(select to_number(e) from we_curve_data where curve_id = b.curve_id and point_yn = 'O') r_efficiency ");
            //query.AppendLine("  from pump a                                                                ");
            //query.AppendLine("      ,we_pump b                                                             ");
            //query.AppendLine("      ,(                                                                     ");
            //query.AppendLine("       select pump_ftr_idn                                                   ");
            //query.AppendLine("             ,timestamp timestamp                                            ");
            //query.AppendLine("             ,round(sum(flow)) flow                                          ");
            //query.AppendLine("             ,round(avg(abs(node2_press - node1_press))) h                   ");
            //query.AppendLine("             ,round(sum(energy)) energy                                      ");
            //query.AppendLine("         from we_rpt                                                         ");
            //query.AppendLine("        where timestamp between to_date(:STARTDATE,'yyyymmddhh24mi')         ");
            //query.AppendLine("                            and to_date(:ENDDATE,'yyyymmddhh24mi')           ");
            //query.AppendLine("        group by pump_ftr_idn, timestamp                                     ");
            //query.AppendLine("       ) c                                                                   ");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn                                    ");
            //query.AppendLine("   and c.pump_ftr_idn(+) = b.pump_ftr_idn                                    ");
            //query.AppendLine(" order by pumpstation_id, pump_ftr_idn, time                                 ");


            //IDataParameter[] parameters =  {
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("ENDDATE", OracleDbType.Varchar2),
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["STARTDATE"];
            //parameters[1].Value = parameter["ENDDATE"];
            //parameters[2].Value = parameter["PUMP_FTR_IDN"];
            //parameters[3].Value = parameter["STARTDATE"];
            //parameters[4].Value = parameter["ENDDATE"];

            query.AppendLine("WITH PUMP AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT C.ID ");
            query.AppendLine("  FROM WH_TITLE A ");
            query.AppendLine("      ,WH_PUMPS B ");
            query.AppendLine("      ,WE_PUMP C ");
            query.AppendLine("  WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("    AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("    AND C.ID = B.ID ");
            query.AppendLine("    AND C.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine(") ");
            query.AppendLine("SELECT B.PUMPSTATION_ID ");
            query.AppendLine("      ,B.PUMP_ID PUMP_FTR_IDN ");
            query.AppendLine("      ,B.REMARK REMARK ");
            query.AppendLine("      ,C.TIMESTAMP TIME ");
            query.AppendLine("      ,C.FLOW ");
            query.AppendLine("      ,C.H ");
            query.AppendLine("      ,C.ENERGY ");
            query.AppendLine("      ,B.DIFF ");
            query.AppendLine("      ,(SELECT TO_NUMBER(E) FROM WE_CURVE_DATA WHERE CURVE_ID = B.CURVE_ID AND POINT_YN = 'O') R_EFFICIENCY ");
            query.AppendLine("  FROM PUMP A ");
            query.AppendLine("      ,WE_PUMP B ");
            query.AppendLine("      ,( ");
            query.AppendLine("       SELECT ID  ");
            query.AppendLine("             ,TIMESTAMP TIMESTAMP ");
            query.AppendLine("             ,ROUND(SUM(FLOW)) FLOW ");
            query.AppendLine("             ,ROUND(AVG(ABS(NODE2_PRESS - NODE1_PRESS))) H ");
            query.AppendLine("             ,ROUND(SUM(ENERGY)) ENERGY ");
            query.AppendLine("         FROM WE_RPT ");
            query.AppendLine("        WHERE TIMESTAMP BETWEEN TO_DATE(:STARTDATE,'YYYYMMDDHH24MI') ");
            query.AppendLine("                            AND TO_DATE(:ENDDATE,'YYYYMMDDHH24MI') ");
            query.AppendLine("        GROUP BY ID, TIMESTAMP ");
            query.AppendLine("       ) C ");
            query.AppendLine(" WHERE B.ID(+) = A.ID ");
            query.AppendLine("   AND C.ID(+) = B.ID ");
            query.AppendLine(" ORDER BY  ");
            query.AppendLine("       B.PUMPSTATION_ID ");
            query.AppendLine("      ,B.PUMP_ID ");
            query.AppendLine("      ,C.TIMESTAMP ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public object SelectCurveModelYN(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select nvl((select nvl(model_yn,'N') model_yn                 ");
            //query.AppendLine("  from we_pump a                                              ");
            //query.AppendLine(" where a.pump_ftr_idn = :PUMP_FTR_IDN),'N') from dual         ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];

            query.AppendLine("SELECT NVL((SELECT NVL(MODEL_YN,'N') MODEL_YN ");
            query.AppendLine("  FROM WE_PUMP ");
            query.AppendLine(" WHERE PUMP_ID = :PUMP_FTR_IDN),'N') FROM DUAL ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectCurveModelCurveID(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine(" select rtrim(ltrim(replace(b.properties, 'HEAD', '')))");
            //query.AppendLine("   from wh_title a                                     ");
            //query.AppendLine("     ,wh_pumps b                                       ");
            //query.AppendLine("     ,wh_tags c                                        ");
            //query.AppendLine("  where a.energy_gbn = 'Y'                               ");
            //query.AppendLine("  and b.inp_number = a.inp_number                      ");
            //query.AppendLine("  and c.inp_number = b.inp_number                      ");
            //query.AppendLine("  and c.id = b.id                                      ");
            //query.AppendLine("  and c.value1 = :PUMP_FTR_IDN                         ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];

            query.AppendLine("SELECT RTRIM(LTRIM(REPLACE(B.PROPERTIES, 'HEAD', ''))) ");
            query.AppendLine("  FROM WH_TITLE A ");
            query.AppendLine("      ,WH_PUMPS B ");
            query.AppendLine("      ,WE_PUMP C ");
            query.AppendLine("WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("  AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("  AND C.ID = B.ID ");
            query.AppendLine("  AND C.PUMP_ID = :PUMP_FTR_IDN ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectCurveModelEnergyID(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select ltrim(rtrim(replace(replace(upper(b.energy_statement), ' ', ''), a.id, '')))");
            //query.AppendLine("  from (                                                                           ");
            //query.AppendLine("       select a.inp_number                                                         ");
            //query.AppendLine("             ,'PUMP'||c.id||'EFFICIENCY' id                                        ");
            //query.AppendLine("         from wh_title a                                                           ");
            //query.AppendLine("           ,wh_pumps b                                                             ");
            //query.AppendLine("           ,wh_tags c                                                              ");
            //query.AppendLine("        where a.energy_gbn = 'Y'                                                     ");
            //query.AppendLine("        and b.inp_number = a.inp_number                                            ");
            //query.AppendLine("        and c.inp_number = b.inp_number                                            ");
            //query.AppendLine("        and c.id = b.id                                                            ");
            //query.AppendLine("        and c.value1 = :PUMP_FTR_IDN                                               ");
            //query.AppendLine("      ) a                                                                          ");
            //query.AppendLine("      ,wh_energy b                                                                 ");
            //query.AppendLine(" where b.inp_number = a.inp_number                                                 ");
            //query.AppendLine("   and replace(upper(b.energy_statement), ' ', '') like '%'||a.id||'%'             ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];

            query.AppendLine("SELECT LTRIM(RTRIM(REPLACE(REPLACE(UPPER(B.ENERGY_STATEMENT), ' ', ''), A.ID, ''))) ");
            query.AppendLine("  FROM ( ");
            query.AppendLine("       SELECT A.INP_NUMBER ");
            query.AppendLine("             ,'PUMP'||C.ID||'EFFICIENCY' ID ");
            query.AppendLine("         FROM WH_TITLE A ");
            query.AppendLine("             ,WH_PUMPS B ");
            query.AppendLine("             ,WE_PUMP C ");
            query.AppendLine("        WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("          AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("          AND C.ID = B.ID ");
            query.AppendLine("          AND C.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("      ) A ");
            query.AppendLine("      ,WH_ENERGY B ");
            query.AppendLine(" WHERE B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("   AND REPLACE(UPPER(B.ENERGY_STATEMENT), ' ', '') LIKE '%'||A.ID||'%' ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public DataSet SelectSimulationAnalysisFlow_MODEL(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("  select to_number(b.x) flow                            ");
            //query.AppendLine("    from wh_title a									  ");
            //query.AppendLine("        ,wh_curves b									  ");
            //query.AppendLine("   where a.energy_gbn = 'Y'                               ");
            //query.AppendLine("   	 and b.inp_number = a.inp_number 				  ");
            //query.AppendLine("     and b.id in (:C_ID, :E_ID)						  ");
            //query.AppendLine("   order by to_number(b.x)							  ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("C_ID", OracleDbType.Varchar2),
            //         new OracleParameter("E_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["C_ID"];
            //parameters[1].Value = parameter["E_ID"];

            query.AppendLine("  SELECT TO_NUMBER(B.X) FLOW ");
            query.AppendLine("    FROM WH_TITLE A ");
            query.AppendLine("        ,WH_CURVES B ");
            query.AppendLine("   WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("     AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("     AND B.ID IN (:C_ID, :E_ID) ");
            query.AppendLine("   ORDER BY TO_NUMBER(B.X) ");

            IDataParameter[] parameters =  {
                     new OracleParameter("C_ID", OracleDbType.Varchar2),
                     new OracleParameter("E_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["C_ID"];
            parameters[1].Value = parameter["E_ID"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectSimulationAnalysisFlow(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TO_NUMBER(B.FLOW) FLOW ");
            query.AppendLine("  FROM WE_PUMP A ");
            query.AppendLine("      ,WE_CURVE_DATA B ");
            query.AppendLine(" WHERE A.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("   AND B.CURVE_ID = A.CURVE_ID ");
            query.AppendLine(" ORDER BY TO_NUMBER(FLOW) ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectSimulationAnalysisCurve_MODEL(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("  select to_number(b.x) flow                            ");
            //query.AppendLine("        ,decode(id, :C_ID, b.y) h						  ");
            //query.AppendLine("        ,decode(id, :E_ID, b.y) e						  ");
            //query.AppendLine("    from wh_title a									  ");
            //query.AppendLine("        ,wh_curves b									  ");
            //query.AppendLine("   where a.energy_gbn = 'Y'                               ");
            //query.AppendLine("   	 and b.inp_number = a.inp_number 				  ");
            //query.AppendLine("     and b.id in (:C_ID, :E_ID)						  ");
            //query.AppendLine("   order by to_number(b.x)							  ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("C_ID", OracleDbType.Varchar2),
            //         new OracleParameter("E_ID", OracleDbType.Varchar2),
            //         new OracleParameter("C_ID", OracleDbType.Varchar2),
            //         new OracleParameter("E_ID", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["C_ID"];
            //parameters[1].Value = parameter["E_ID"];
            //parameters[2].Value = parameter["C_ID"];
            //parameters[3].Value = parameter["E_ID"];

            query.AppendLine("SELECT TO_NUMBER(B.X) FLOW ");
            query.AppendLine("      ,DECODE(ID, :C_ID, B.Y) H ");
            query.AppendLine("      ,DECODE(ID, :E_ID, B.Y) E ");
            query.AppendLine("  FROM WH_TITLE A ");
            query.AppendLine("      ,WH_CURVES B ");
            query.AppendLine(" WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("   AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("   AND B.ID IN (:C_ID, :E_ID) ");
            query.AppendLine(" ORDER BY TO_NUMBER(B.X) ");

            IDataParameter[] parameters =  {
                     new OracleParameter("C_ID", OracleDbType.Varchar2),
                     new OracleParameter("E_ID", OracleDbType.Varchar2),
                     new OracleParameter("C_ID", OracleDbType.Varchar2),
                     new OracleParameter("E_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["C_ID"];
            parameters[1].Value = parameter["E_ID"];
            parameters[2].Value = parameter["C_ID"];
            parameters[3].Value = parameter["E_ID"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataSet SelectSimulationAnalysisCurve(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select b.flow                          ");
            //query.AppendLine("      ,b.h							 ");
            //query.AppendLine("      ,b.e							 ");
            //query.AppendLine("  from we_pump a						 ");
            //query.AppendLine("      ,we_curve_data b				 ");
            //query.AppendLine(" where a.pump_ftr_idn = :PUMP_FTR_IDN	 ");
            //query.AppendLine("   and b.curve_id = a.curve_id		 ");
            //query.AppendLine(" order by to_number(flow)				 ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["PUMP_FTR_IDN"];

            query.AppendLine("SELECT B.FLOW ");
            query.AppendLine("      ,B.H ");
            query.AppendLine("      ,B.E ");
            query.AppendLine("  FROM WE_PUMP A ");
            query.AppendLine("      ,WE_CURVE_DATA B ");
            query.AppendLine(" WHERE A.PUMP_ID = :PUMP_FTR_IDN ");
            query.AppendLine("   AND B.CURVE_ID = A.CURVE_ID ");
            query.AppendLine(" ORDER BY TO_NUMBER(FLOW) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("PUMP_FTR_IDN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMP_FTR_IDN"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public DataTable SelectPumpValueList(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT C.PUMP_ID CODE ");
            query.AppendLine("      ,C.REMARK CODE_NAME ");
            query.AppendLine("  FROM WH_TITLE A ");
            query.AppendLine("      ,WH_PUMPS B ");
            query.AppendLine("      ,WE_PUMP C ");
            query.AppendLine(" WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("   AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("   AND C.ID = B.ID ");
            query.AppendLine("   AND C.PUMPSTATION_ID = NVL(:BZS, C.PUMPSTATION_ID) ");
            
            //if (parameter["BZS"] != null)
            //{
            //    query.Append("   AND C.PUMPSTATION_ID = '").Append(parameter["BZS"].ToString()).AppendLine("'"); ;
            //}

            query.AppendLine(" ORDER BY CODE ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("BZS", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["BZS"];

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }
    }
}
