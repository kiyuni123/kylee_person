﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.work;
using WaterNet.WE_PumpStationManage.dao;
using System.Data;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;
using System.Collections;

namespace WaterNet.WE_PumpStationManage.work
{
    public class PumpStationManageWork : BaseWork
    {
        private static PumpStationManageWork work = null;
        private PumpStationManageDao dao = null;

        private PumpStationManageWork()
        {
            dao = PumpStationManageDao.GetInstance();
        }

        public static PumpStationManageWork GetInstance()
        {
            if (work == null)
            {
                work = new PumpStationManageWork();
            }
            return work;
        }

        public DataTable SelectPumpStationManage()
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectPumpStationManage(base.DataBaseManager);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataTable SelectPumpStationManage(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectPumpStationManage(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];

                        result.Columns.Add("SELECTED", typeof(bool));

                        foreach (DataRow row in result.Rows)
                        {
                            row["SELECTED"] = false;
                        }
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void InsertPumpStationManage(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                parameter["PUMPSTATION_ID"] = dao.SelectPumpStationManageSEQ(base.DataBaseManager, parameter);
                dao.InsertPumpStationManage(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void UpdatePumpStationManage(UltraGrid ultraGrid)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow gridRow in ultraGrid.Rows)
                {
                    if (Convert.ToBoolean(gridRow.Cells["SELECTED"].Value))
                    {
                        Hashtable parameter = Utils.ConverToHashtable(gridRow);

                        dao.UpdatePumpStationManage(base.DataBaseManager, parameter);
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void DeletePumpStationManage(UltraGrid ultraGrid)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow gridRow in ultraGrid.Rows)
                {
                    if (Convert.ToBoolean(gridRow.Cells["SELECTED"].Value))
                    {
                        Hashtable parameter = Utils.ConverToHashtable(gridRow);

                        dao.DeletePumpStationManage(base.DataBaseManager, parameter);
                        dao.DeletePumpStationManageDetail(base.DataBaseManager, parameter);
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }
    }
}
