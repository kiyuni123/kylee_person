﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WE_PumpStationManage.work;
using WaterNet.WV_Common.control;
using EMFrame.log;

namespace WaterNet.WE_PumpStationManage.form
{
    public partial class frmMain : Form
    {
        ////MainMap 을 제어하기 위한 인스턴스 변수
        //private IMapControl mainMap = null;

        ////관리단 사업장 검색조건 제어
        //private EnergySearchStepControl searchStrpControl = null;

        ///// <summary>
        ///// MainMap 을 제어하기 위한 인스턴스 변수
        ///// </summary>
        //public IMapControl MainMap
        //{
        //    set
        //    {
        //        this.mainMap = value;
        //    }
        //}

        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            //그리드1
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SELECTED";
            column.Header.Caption = "";
            column.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            column.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.None;
            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;

            column.SortIndicator = SortIndicator.Disabled;
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 30;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장 관리번호";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_NAME";
            column.Header.Caption = "사업장명";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 120;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "REMARK";
            column.Header.Caption = "설명";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 350;
            column.Hidden = false;
            #endregion
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.ultraGrid1.AfterHeaderCheckStateChanged += new AfterHeaderCheckStateChangedEventHandler(ultraGrid1_AfterHeaderCheckStateChanged);
        }

        private Hashtable parameter = null;
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.SelectPumpStationManage();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectPumpStationManage()
        {
            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid1.DataSource =
            PumpStationManageWork.GetInstance().SelectPumpStationManage(this.parameter);
            this.Cursor = Cursors.Default;
        }

        private void insertBtn_Click(object sender, EventArgs e)
        {
            Hashtable parameter = new Hashtable();
            PumpStationManageWork.GetInstance().InsertPumpStationManage(parameter);
            this.SelectPumpStationManage();
            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (gridRow.Cells["PUMPSTATION_ID"].Value.ToString() == parameter["PUMPSTATION_ID"].ToString())
                {
                    gridRow.Activated = true;
                }
            }
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult qe = MessageBox.Show("선택항목을 삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    PumpStationManageWork.GetInstance().DeletePumpStationManage(this.ultraGrid1);
                    this.SelectPumpStationManage();
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult qe = MessageBox.Show("선택항목을 저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    PumpStationManageWork.GetInstance().UpdatePumpStationManage(this.ultraGrid1);
                    this.SelectPumpStationManage();
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //체크박스 전체 선택 / 전체 해제
        private void ultraGrid1_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;

            bool result = false;

            if (e.Column.GetHeaderCheckedState(e.Rows) == CheckState.Checked)
            {
                result = true;
            }

            foreach (UltraGridRow row in ultraGrid.Rows)
            {
                if (!row.Hidden)
                {
                    row.Cells["SELECTED"].Value = result;
                }
            }
            ultraGrid.UpdateData();
        }
    }
}
