﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using WaterNet.WaterNetCore;
using System.Collections;

namespace WaterNet.WE_PumpStationManage.dao
{
    public class PumpStationManageDao
    {
        private static PumpStationManageDao dao = null;
        private PumpStationManageDao() { }

        public static PumpStationManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new PumpStationManageDao();
            }
            return dao;
        }

        public DataSet SelectPumpStationManage(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select pumpstation_id code                                                              ");
            query.AppendLine("      ,pumpstation_name code_name                                                       ");
            query.AppendLine("  from we_pump_station                                                                  ");
            query.AppendLine(" where 1 = 1                                                                            ");
            query.AppendLine("  order by to_number(pumpstation_id)                                                    ");
 
            return manager.ExecuteScriptDataSet(query.ToString(), null);
        }

        public DataSet SelectPumpStationManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select pumpstation_id                                                                   ");
            query.AppendLine("      ,pumpstation_name                                                                 ");
            query.AppendLine("      ,remark                                                                           ");
            query.AppendLine("  from we_pump_station                                                                  ");
            query.AppendLine(" where 1 = 1                                                                            ");
            query.AppendLine("  order by to_number(pumpstation_id)                                                    ");

            return manager.ExecuteScriptDataSet(query.ToString(), null);

            //query.AppendLine("select mg_dvcd                                                                          ");
            //query.AppendLine("      ,rohqcd                                                                           ");
            //query.AppendLine("      ,mgdpcd                                                                           ");
            //query.AppendLine("      ,mg_dvcd||'_'||rohqcd||'_'||mgdpcd department                                     ");
            //query.AppendLine("      ,pumpstation_id                                                                   ");
            //query.AppendLine("      ,pumpstation_name                                                                 ");
            //query.AppendLine("      ,remark                                                                           ");
            //query.AppendLine("  from we_pump_station                                                                  ");
            //query.AppendLine(" where 1 = 1                                                                            ");
            //query.AppendLine("   and mg_dvcd = nvl(:MG_DVCD, mg_dvcd)                                                 ");
            //query.AppendLine("   and rohqcd = nvl(:ROHQCD, rohqcd)                                                    ");
            //query.AppendLine("   and mgdpcd = nvl(:MGDPCD, mgdpcd)                                                    ");
            //query.AppendLine("  order by to_number(pumpstation_id)                                                    ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("MG_DVCD", OracleDbType.Varchar2),
            //         new OracleParameter("ROHQCD", OracleDbType.Varchar2),
            //         new OracleParameter("MGDPCD", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["MG_DVCD"];
            //parameters[1].Value = parameter["ROHQCD"];
            //parameters[2].Value = parameter["MGDPCD"];

            //return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }

        public object SelectPumpStationManageSEQ(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select nvl(id,0)+1                                                          ");
            query.AppendLine("  from (select max(to_number(pumpstation_id)) id from we_pump_station)      ");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        public void InsertPumpStationManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into we_pump_station (pumpstation_id)       ");
            query.AppendLine("                  values (:PUMPSTATION_ID)      ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMPSTATION_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMPSTATION_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void UpdatePumpStationManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update we_pump_station                                             ");
            query.AppendLine("                   set pumpstation_name = :PUMPSTATION_NAME        ");
            query.AppendLine("                      ,remark = :REMARK                            ");
            query.AppendLine("                 where pumpstation_id = :PUMPSTATION_ID            ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMPSTATION_NAME", OracleDbType.Varchar2),
                     new OracleParameter("REMARK", OracleDbType.Varchar2),
                     new OracleParameter("PUMPSTATION_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMPSTATION_NAME"];
            parameters[1].Value = parameter["REMARK"];
            parameters[2].Value = parameter["PUMPSTATION_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeletePumpStationManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from we_pump_station                                  ");
            query.AppendLine("      where pumpstation_id = :PUMPSTATION_ID                 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMPSTATION_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMPSTATION_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeletePumpStationManageDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update we_pump                                    ");
            query.AppendLine("           set pumpstation_id = null              ");
            query.AppendLine("         where pumpstation_id = :PUMPSTATION_ID   ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMPSTATION_ID", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["PUMPSTATION_ID"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
