﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.util;
using System.Collections;
using WaterNet.WV_Common.control;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WE_ResultAnalysis.work;
using ChartFX.WinForms;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WE_ResultAnalysis.form
{
    public partial class frmMain : Form
    {
        //그리드 매니저(그리드 스타일 설정및 기타공통기능설정)
        private UltraGridManager gridManager = null;

        //차트 매니저(차트 스타일 설정및 기타공통기능설정)
        private ChartManager chartManager = null;

        //테이블// 그래프 레이아웃 
        private TableLayout tableLayout = null;

        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 폼 초기화 설정
        /// </summary>
        private void InitializeForm()
        {
            DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();
            DataTable station = new DataTable();
            station.Columns.Add("CODE");
            station.Columns.Add("CODE_NAME");

            foreach (DataRow row in table.Rows)
            {
                station.Rows.Add(row["CODE"], row["CODE_NAME"].ToString());
            }

            this.bzs.ValueMember = "CODE";
            this.bzs.DisplayMember = "CODE_NAME";
            this.bzs.DataSource = station;

            this.tableLayout = new TableLayout(this.tableLayoutPanel1, this.chartPanel1, this.tablePanel1, this.chartVisibleBtn, this.tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            //this.searchStrpControl = new EnergySearchStepControl(this.department, this.bzs, false);

            this.basicCheck = this.DAY_ENERGY;
            this.analysisCheck = this.DAY;

            this.endDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            this.startDate.Value = ((DateTime)this.endDate.Value).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd");

            this.SetSearchInterval();
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.InitializeGridColumn();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region 컬럼추가 내용
            //그리드 컬럼설정
            UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "PUMPSTATION_ID";
            column.Header.Caption = "사업장";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 140;
            column.Hidden = false;
            column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIME";
            column.Header.Caption = "분석일자";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;
            //column.SortIndicator = SortIndicator.Disabled;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "FLOW";
            column.Header.Caption = "공급량(㎥)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 130;
            column.Format = "###,###,##0";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H";
            column.Header.Caption = "평균양정(m)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 130;
            column.Format = "N1";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ENERGY";
            column.Header.Caption = "전력량(kwh)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 130;
            column.Format = "###,###,##0";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "W_ENERGY";
            column.Header.Caption = "원단위(kwh/㎥)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 130;
            column.Format = "N3";
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "O_EFFICIENCY";
            column.Header.Caption = "운영효율(%)";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 130;
            column.Format = "N1";
            column.Hidden = false;
            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //사업장
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PUMPSTATION_ID"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PUMPSTATION_ID");

                DataTable table = WE_PumpStationManage.work.PumpStationManageWork.GetInstance().SelectPumpStationManage();

                foreach (DataRow row in table.Rows)
                {
                    valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
                }

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["PUMPSTATION_ID"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["PUMPSTATION_ID"];
            }
        }

        /// <summary>
        /// 이벤트 초기화 설정
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            this.DAY_ENERGY.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.DAY_W_ENERGY.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.DAY_FLOW.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.DAY_EFFICIENCY.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);

            this.DAY.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.DAY_WEEK.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.WEEK_AVERAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.MONTH_AVERAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);
            this.YEAR_AVERAGE.CheckedChanged += new EventHandler(CheckBoxCheckedChanged_EventHandler);

            this.endDate.ValueChanged += new EventHandler(endDate_ValueChanged);
            this.WEEK_AVERAGE_INTERVAL.SelectedValueChanged += new EventHandler(endDate_ValueChanged);
            this.MONTH_AVERAGE_INTERVAL.SelectedValueChanged += new EventHandler(endDate_ValueChanged);
            this.YEAR_AVERAGE_INTERVAL.SelectedValueChanged += new EventHandler(endDate_ValueChanged);
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid1, this.Text);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        Hashtable parameter = null;

        //검색버튼 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = new Hashtable();
                this.SetSearchDateParameter(this.parameter);
                this.SelectResultAnalysis();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectResultAnalysis()
        {
            //비교조건이 선택안된경우
            if (!this.IsSearchCheckBoxChecked())
            {
                MessageBox.Show("비교조건을 선택해야 합니다.");
                return;
            }

            this.Cursor = Cursors.WaitCursor;
            this.ultraGrid1.DataSource = ResultAnalysisWork.GetInstance().SelectResultAnalysis(this.parameter);

            //차트변경
            this.InitializeChartSetting();
            this.Cursor = Cursors.Default;
        }

        private CheckBox analysisCheck = null;
        private CheckBox basicCheck = null;

        /// <summary>
        /// 체크박스 체크 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxCheckedChanged_EventHandler(object sender, EventArgs e)
        {
            if (sender.Equals(this.DAY_ENERGY) || sender.Equals(this.DAY_W_ENERGY) || sender.Equals(this.DAY_FLOW) || sender.Equals(this.DAY_EFFICIENCY))
            {
                this.InitializeChartSeries();
            }
            else
            {
                if (!sender.Equals(this.analysisCheck))
                {
                    this.analysisCheck.Checked = false;
                    this.analysisCheck = (CheckBox)sender;
                }

                if (((CheckBox)sender).Checked)
                {
                    if (((CheckBox)sender).Name == "DAY" || ((CheckBox)sender).Name == "DAY_WEEK")
                    {
                        this.startDate.Enabled = true;
                        this.startDate.Value = ((DateTime)this.endDate.Value).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd");
                    }

                    if (((CheckBox)sender).Name == "WEEK_AVERAGE")
                    {
                        this.startDate.Enabled = false;
                        this.startDate.Value = 
                            ((DateTime)this.endDate.Value).AddDays(-7 * Convert.ToInt16(this.WEEK_AVERAGE_INTERVAL.SelectedValue)).AddDays(1).ToString("yyyy-MM-dd");
                    }

                    if (((CheckBox)sender).Name == "MONTH_AVERAGE")
                    {
                        this.startDate.Enabled = false;
                        this.startDate.Value = 
                            ((DateTime)this.endDate.Value).AddMonths(-1 * Convert.ToInt16(this.MONTH_AVERAGE_INTERVAL.SelectedValue)).AddDays(1).ToString("yyyy-MM-dd");
                    }

                    if (((CheckBox)sender).Name == "YEAR_AVERAGE")
                    {
                        this.startDate.Enabled = false;
                        this.startDate.Value = 
                            ((DateTime)this.endDate.Value).AddYears(-1 * Convert.ToInt16(this.YEAR_AVERAGE_INTERVAL.SelectedValue)).AddDays(1).ToString("yyyy-MM-dd");
                    }
                }
            }
        }

        private void endDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.WEEK_AVERAGE.Checked)
            {
                this.startDate.Value =
                    ((DateTime)this.endDate.Value).AddDays(-7 * Convert.ToInt16(this.WEEK_AVERAGE_INTERVAL.SelectedValue)).AddDays(1).ToString("yyyy-MM-dd");
            }
            if (this.MONTH_AVERAGE.Checked)
            {
                this.startDate.Value =
                    ((DateTime)this.endDate.Value).AddMonths(-1 * Convert.ToInt16(this.MONTH_AVERAGE_INTERVAL.SelectedValue)).AddDays(1).ToString("yyyy-MM-dd");
            }
            if (this.YEAR_AVERAGE.Checked)
            {
                this.startDate.Value =
                    ((DateTime)this.endDate.Value).AddYears(-1 * Convert.ToInt16(this.YEAR_AVERAGE_INTERVAL.SelectedValue)).AddDays(1).ToString("yyyy-MM-dd");
            }
        }

        /// <summary>
        /// 검색조건의 체크 여부 판단
        /// </summary>
        /// <returns></returns>
        private bool IsSearchCheckBoxChecked()
        {
            bool result = true;

            if (!this.DAY.Checked && !this.DAY_WEEK.Checked &&
                !this.WEEK_AVERAGE.Checked && !this.MONTH_AVERAGE.Checked && !this.YEAR_AVERAGE.Checked)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// 검색조건중 사용자 선택 기간에 따라서 검색기간을 설정한다.
        /// </summary>
        /// <returns></returns>
        private void SetSearchDateParameter(Hashtable parameter)
        {
            if (parameter.ContainsKey("BZS"))
            {
                parameter.Remove("BZS");
            }

            //기존에 존재하는 키를 삭제한다.
            if (parameter.ContainsKey("STARTDATE"))
            {
                parameter.Remove("STARTDATE");
            }

            if (parameter.ContainsKey("ENDDATE"))
            {
                parameter.Remove("ENDDATE");
            }

            parameter["STARTDATE"] = ((DateTime)startDate.Value).ToString("yyyyMMdd");
            parameter["ENDDATE"] = ((DateTime)endDate.Value).ToString("yyyyMMdd");
            parameter["BZS"] = this.bzs.SelectedValue;

            if (this.DAY.Checked)
            {
                parameter["INTERVAL"] = this.DAY.Name;
            }

            if (this.DAY_WEEK.Checked)
            {
                parameter["INTERVAL"] = this.DAY_WEEK.Name;
            }

            if (this.WEEK_AVERAGE.Checked)
            {
                parameter["INTERVAL"] = this.WEEK_AVERAGE.Name;
            }

            if (this.MONTH_AVERAGE.Checked)
            {
                parameter["INTERVAL"] = this.MONTH_AVERAGE.Name;
            }

            if (this.YEAR_AVERAGE.Checked)
            {
                parameter["INTERVAL"] = this.YEAR_AVERAGE.Name;
            }
        }

        /// <summary>
        /// 차트 가변값 설정
        /// </summary>
        private void InitializeChartSetting()
        {
            this.InitChart();
            this.chart1.Data.Series = 4;
            this.chart1.Data.Points = this.ultraGrid1.Rows.Count;

            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                this.chart1.AxisX.Labels[row.Index] = row.Cells["TIME"].Value.ToString();
                this.chart1.Data[0, row.Index] = Utils.ToDouble(row.Cells["FLOW"].Value);
                this.chart1.Data[1, row.Index] = Utils.ToDouble(row.Cells["ENERGY"].Value);
                this.chart1.Data[2, row.Index] = Utils.ToDouble(row.Cells["W_ENERGY"].Value);
                this.chart1.Data[3, row.Index] = Utils.ToDouble(row.Cells["O_EFFICIENCY"].Value);
            }

            this.chart1.AxesY.Add(new ChartFX.WinForms.AxisY());
            this.chart1.AxesY.Add(new ChartFX.WinForms.AxisY());
            this.chart1.AxesY.Add(new ChartFX.WinForms.AxisY());

            this.chart1.Series[0].AxisY = this.chart1.AxesY[0];
            this.chart1.Series[1].AxisY = this.chart1.AxesY[1];
            this.chart1.Series[2].AxisY = this.chart1.AxesY[2];
            this.chart1.Series[3].AxisY = this.chart1.AxesY[3];

            this.chart1.Series[0].AxisY.Position = ChartFX.WinForms.AxisPosition.Near;
            this.chart1.Series[1].AxisY.Position = ChartFX.WinForms.AxisPosition.Near;
            this.chart1.Series[2].AxisY.Position = ChartFX.WinForms.AxisPosition.Far;
            this.chart1.Series[3].AxisY.Position = ChartFX.WinForms.AxisPosition.Far;

            this.chart1.Series[0].AxisY.Title.Text = "공급량(㎥)";
            this.chart1.Series[1].AxisY.Title.Text = "전력량(kwh)";
            this.chart1.Series[2].AxisY.Title.Text = "원단위(kwh/㎥)";
            this.chart1.Series[3].AxisY.Title.Text = "효율(%)";

            this.chart1.Series[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
            this.chart1.Series[0].AxisY.LabelsFormat.CustomFormat = "###,###,###";
            this.chart1.Series[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
            this.chart1.Series[1].AxisY.LabelsFormat.CustomFormat = "###,###,###";
            this.chart1.Series[2].AxisY.LabelsFormat.Format = AxisFormat.Number;
            this.chart1.Series[2].AxisY.LabelsFormat.CustomFormat = "N3";
            this.chart1.Series[3].AxisY.LabelsFormat.Format = AxisFormat.Number;
            this.chart1.Series[3].AxisY.LabelsFormat.CustomFormat = "###,###,###.0";

            this.chart1.Series[0].Gallery = Gallery.Bar;
            this.chart1.Series[1].Gallery = Gallery.Bar;
            this.chart1.Series[2].Gallery = Gallery.Bar;
            this.chart1.Series[3].Gallery = Gallery.Bar;

            this.chart1.Series[0].PointLabels.Visible = true;
            this.chart1.Series[1].PointLabels.Visible = true;
            this.chart1.Series[2].PointLabels.Visible = true;
            this.chart1.Series[3].PointLabels.Visible = true;

            this.chart1.Series[0].Text = "공급량(㎥)";
            this.chart1.Series[1].Text = "전력량(kwh)";
            this.chart1.Series[2].Text = "원단위(kwh/㎥)";
            this.chart1.Series[3].Text = "효율(%)";

            this.InitializeChartSeries();
        }

        private void InitializeChartSeries()
        {
            foreach (SeriesAttributes series in this.chart1.Series)
            {
                series.Visible = false;
            }

            if (this.DAY_FLOW.Checked)
            {
                this.chart1.Series[0].Visible = true;
            }
            if (this.DAY_ENERGY.Checked)
            {
                this.chart1.Series[1].Visible = true;
            }
            if (this.DAY_W_ENERGY.Checked)
            {
                this.chart1.Series[2].Visible = true;
            }
            if (this.DAY_EFFICIENCY.Checked)
            {
                this.chart1.Series[3].Visible = true;
            }
        }

        private void InitChart()
        {
            this.chart1.AxisY.Sections.Clear();
            this.chart1.ConditionalAttributes.Clear();
            this.chart1.Extensions.Clear();
            this.chart1.LegendBox.CustomItems.Clear();
            this.chartManager.AllClear(this.chart1);
        }

        /// <summary>
        /// 검색기간 데이터 설정 
        /// </summary>
        private void SetSearchInterval()
        {
            ComboBoxUtils.AddData(WEEK_AVERAGE_INTERVAL, "CODE", "CODE_NAME", "주", 12);
            ComboBoxUtils.AddData(MONTH_AVERAGE_INTERVAL, "CODE", "CODE_NAME", "개월", 12);
            ComboBoxUtils.AddData(YEAR_AVERAGE_INTERVAL, "CODE", "CODE_NAME", "년", 12);

            WEEK_AVERAGE_INTERVAL.SelectedIndex = 3;
            MONTH_AVERAGE_INTERVAL.SelectedIndex = 2;
            YEAR_AVERAGE_INTERVAL.SelectedIndex = 2;
        }
    }
}
