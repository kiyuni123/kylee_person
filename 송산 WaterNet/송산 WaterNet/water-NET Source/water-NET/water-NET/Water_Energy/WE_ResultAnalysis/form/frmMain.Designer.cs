﻿namespace WaterNet.WE_ResultAnalysis.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LargeBlock = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.bzs = new System.Windows.Forms.ComboBox();
            this.TimeToTime = new System.Windows.Forms.Panel();
            this.endDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.startDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.TimeToTimeIntervalText = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DAY_WEEK = new System.Windows.Forms.CheckBox();
            this.YEAR_AVERAGE_INTERVAL = new System.Windows.Forms.ComboBox();
            this.YEAR_AVERAGE = new System.Windows.Forms.CheckBox();
            this.MONTH_AVERAGE_INTERVAL = new System.Windows.Forms.ComboBox();
            this.MONTH_AVERAGE = new System.Windows.Forms.CheckBox();
            this.WEEK_AVERAGE_INTERVAL = new System.Windows.Forms.ComboBox();
            this.WEEK_AVERAGE = new System.Windows.Forms.CheckBox();
            this.DAY = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DAY_EFFICIENCY = new System.Windows.Forms.CheckBox();
            this.chartVisibleBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.tableVisibleBtn = new System.Windows.Forms.Button();
            this.DAY_ENERGY = new System.Windows.Forms.CheckBox();
            this.DAY_W_ENERGY = new System.Windows.Forms.CheckBox();
            this.DAY_FLOW = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel1 = new System.Windows.Forms.Panel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.chartPanel1 = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.LargeBlock.SuspendLayout();
            this.TimeToTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.endDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.chartPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(971, 10);
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(961, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 565);
            this.pictureBox4.TabIndex = 28;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 565);
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 575);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(971, 10);
            this.pictureBox2.TabIndex = 26;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LargeBlock);
            this.groupBox1.Controls.Add(this.TimeToTime);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(951, 54);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // LargeBlock
            // 
            this.LargeBlock.Controls.Add(this.label10);
            this.LargeBlock.Controls.Add(this.bzs);
            this.LargeBlock.Location = new System.Drawing.Point(287, 20);
            this.LargeBlock.Name = "LargeBlock";
            this.LargeBlock.Size = new System.Drawing.Size(215, 22);
            this.LargeBlock.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "사업장";
            // 
            // bzs
            // 
            this.bzs.FormattingEnabled = true;
            this.bzs.Location = new System.Drawing.Point(51, 0);
            this.bzs.Name = "bzs";
            this.bzs.Size = new System.Drawing.Size(161, 20);
            this.bzs.TabIndex = 5;
            // 
            // TimeToTime
            // 
            this.TimeToTime.Controls.Add(this.endDate);
            this.TimeToTime.Controls.Add(this.label4);
            this.TimeToTime.Controls.Add(this.startDate);
            this.TimeToTime.Controls.Add(this.TimeToTimeIntervalText);
            this.TimeToTime.Location = new System.Drawing.Point(6, 20);
            this.TimeToTime.Name = "TimeToTime";
            this.TimeToTime.Size = new System.Drawing.Size(275, 22);
            this.TimeToTime.TabIndex = 20;
            // 
            // endDate
            // 
            this.endDate.Location = new System.Drawing.Point(173, 0);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(100, 21);
            this.endDate.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "-";
            // 
            // startDate
            // 
            this.startDate.Location = new System.Drawing.Point(59, 0);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(100, 21);
            this.startDate.TabIndex = 2;
            // 
            // TimeToTimeIntervalText
            // 
            this.TimeToTimeIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeToTimeIntervalText.AutoSize = true;
            this.TimeToTimeIntervalText.Location = new System.Drawing.Point(3, 4);
            this.TimeToTimeIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.TimeToTimeIntervalText.Name = "TimeToTimeIntervalText";
            this.TimeToTimeIntervalText.Size = new System.Drawing.Size(53, 12);
            this.TimeToTimeIntervalText.TabIndex = 1;
            this.TimeToTimeIntervalText.Text = "검색일자";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(10, 64);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(951, 10);
            this.pictureBox5.TabIndex = 33;
            this.pictureBox5.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DAY_WEEK);
            this.groupBox2.Controls.Add(this.YEAR_AVERAGE_INTERVAL);
            this.groupBox2.Controls.Add(this.YEAR_AVERAGE);
            this.groupBox2.Controls.Add(this.MONTH_AVERAGE_INTERVAL);
            this.groupBox2.Controls.Add(this.MONTH_AVERAGE);
            this.groupBox2.Controls.Add(this.WEEK_AVERAGE_INTERVAL);
            this.groupBox2.Controls.Add(this.WEEK_AVERAGE);
            this.groupBox2.Controls.Add(this.DAY);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(10, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(951, 58);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "비교조건";
            // 
            // DAY_WEEK
            // 
            this.DAY_WEEK.AutoSize = true;
            this.DAY_WEEK.Location = new System.Drawing.Point(84, 22);
            this.DAY_WEEK.Name = "DAY_WEEK";
            this.DAY_WEEK.Size = new System.Drawing.Size(72, 16);
            this.DAY_WEEK.TabIndex = 22;
            this.DAY_WEEK.Text = "요일비교";
            this.DAY_WEEK.UseVisualStyleBackColor = true;
            // 
            // YEAR_AVERAGE_INTERVAL
            // 
            this.YEAR_AVERAGE_INTERVAL.FormattingEnabled = true;
            this.YEAR_AVERAGE_INTERVAL.Location = new System.Drawing.Point(707, 20);
            this.YEAR_AVERAGE_INTERVAL.Name = "YEAR_AVERAGE_INTERVAL";
            this.YEAR_AVERAGE_INTERVAL.Size = new System.Drawing.Size(86, 20);
            this.YEAR_AVERAGE_INTERVAL.TabIndex = 21;
            // 
            // YEAR_AVERAGE
            // 
            this.YEAR_AVERAGE.AutoSize = true;
            this.YEAR_AVERAGE.Location = new System.Drawing.Point(593, 22);
            this.YEAR_AVERAGE.Name = "YEAR_AVERAGE";
            this.YEAR_AVERAGE.Size = new System.Drawing.Size(108, 16);
            this.YEAR_AVERAGE.TabIndex = 20;
            this.YEAR_AVERAGE.Text = "년간별평균비교";
            this.YEAR_AVERAGE.UseVisualStyleBackColor = true;
            // 
            // MONTH_AVERAGE_INTERVAL
            // 
            this.MONTH_AVERAGE_INTERVAL.FormattingEnabled = true;
            this.MONTH_AVERAGE_INTERVAL.Location = new System.Drawing.Point(492, 20);
            this.MONTH_AVERAGE_INTERVAL.Name = "MONTH_AVERAGE_INTERVAL";
            this.MONTH_AVERAGE_INTERVAL.Size = new System.Drawing.Size(86, 20);
            this.MONTH_AVERAGE_INTERVAL.TabIndex = 19;
            // 
            // MONTH_AVERAGE
            // 
            this.MONTH_AVERAGE.AutoSize = true;
            this.MONTH_AVERAGE.Location = new System.Drawing.Point(378, 22);
            this.MONTH_AVERAGE.Name = "MONTH_AVERAGE";
            this.MONTH_AVERAGE.Size = new System.Drawing.Size(108, 16);
            this.MONTH_AVERAGE.TabIndex = 18;
            this.MONTH_AVERAGE.Text = "월간별평균비교";
            this.MONTH_AVERAGE.UseVisualStyleBackColor = true;
            // 
            // WEEK_AVERAGE_INTERVAL
            // 
            this.WEEK_AVERAGE_INTERVAL.FormattingEnabled = true;
            this.WEEK_AVERAGE_INTERVAL.Location = new System.Drawing.Point(276, 20);
            this.WEEK_AVERAGE_INTERVAL.Name = "WEEK_AVERAGE_INTERVAL";
            this.WEEK_AVERAGE_INTERVAL.Size = new System.Drawing.Size(86, 20);
            this.WEEK_AVERAGE_INTERVAL.TabIndex = 17;
            // 
            // WEEK_AVERAGE
            // 
            this.WEEK_AVERAGE.AutoSize = true;
            this.WEEK_AVERAGE.Location = new System.Drawing.Point(162, 22);
            this.WEEK_AVERAGE.Name = "WEEK_AVERAGE";
            this.WEEK_AVERAGE.Size = new System.Drawing.Size(108, 16);
            this.WEEK_AVERAGE.TabIndex = 16;
            this.WEEK_AVERAGE.Text = "주간별평균비교";
            this.WEEK_AVERAGE.UseVisualStyleBackColor = true;
            // 
            // DAY
            // 
            this.DAY.AutoSize = true;
            this.DAY.Checked = true;
            this.DAY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DAY.Location = new System.Drawing.Point(6, 22);
            this.DAY.Name = "DAY";
            this.DAY.Size = new System.Drawing.Size(72, 16);
            this.DAY.TabIndex = 14;
            this.DAY.Text = "일간비교";
            this.DAY.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DAY_EFFICIENCY);
            this.panel1.Controls.Add(this.chartVisibleBtn);
            this.panel1.Controls.Add(this.searchBtn);
            this.panel1.Controls.Add(this.excelBtn);
            this.panel1.Controls.Add(this.tableVisibleBtn);
            this.panel1.Controls.Add(this.DAY_ENERGY);
            this.panel1.Controls.Add(this.DAY_W_ENERGY);
            this.panel1.Controls.Add(this.DAY_FLOW);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 132);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(951, 30);
            this.panel1.TabIndex = 36;
            // 
            // DAY_EFFICIENCY
            // 
            this.DAY_EFFICIENCY.AutoSize = true;
            this.DAY_EFFICIENCY.Location = new System.Drawing.Point(320, 8);
            this.DAY_EFFICIENCY.Name = "DAY_EFFICIENCY";
            this.DAY_EFFICIENCY.Size = new System.Drawing.Size(68, 16);
            this.DAY_EFFICIENCY.TabIndex = 34;
            this.DAY_EFFICIENCY.Text = "효율(%)";
            this.DAY_EFFICIENCY.UseVisualStyleBackColor = true;
            // 
            // chartVisibleBtn
            // 
            this.chartVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chartVisibleBtn.Location = new System.Drawing.Point(749, 3);
            this.chartVisibleBtn.Name = "chartVisibleBtn";
            this.chartVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.chartVisibleBtn.TabIndex = 33;
            this.chartVisibleBtn.TabStop = false;
            this.chartVisibleBtn.Text = "그래프";
            this.chartVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(911, 3);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 30;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(863, 3);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 31;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // tableVisibleBtn
            // 
            this.tableVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableVisibleBtn.Location = new System.Drawing.Point(805, 3);
            this.tableVisibleBtn.Name = "tableVisibleBtn";
            this.tableVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.tableVisibleBtn.TabIndex = 32;
            this.tableVisibleBtn.TabStop = false;
            this.tableVisibleBtn.Text = "테이블";
            this.tableVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // DAY_ENERGY
            // 
            this.DAY_ENERGY.AutoSize = true;
            this.DAY_ENERGY.Location = new System.Drawing.Point(99, 8);
            this.DAY_ENERGY.Name = "DAY_ENERGY";
            this.DAY_ENERGY.Size = new System.Drawing.Size(93, 16);
            this.DAY_ENERGY.TabIndex = 27;
            this.DAY_ENERGY.Text = "전력량(kWh)";
            this.DAY_ENERGY.UseVisualStyleBackColor = true;
            // 
            // DAY_W_ENERGY
            // 
            this.DAY_W_ENERGY.AutoSize = true;
            this.DAY_W_ENERGY.Location = new System.Drawing.Point(198, 8);
            this.DAY_W_ENERGY.Name = "DAY_W_ENERGY";
            this.DAY_W_ENERGY.Size = new System.Drawing.Size(116, 16);
            this.DAY_W_ENERGY.TabIndex = 29;
            this.DAY_W_ENERGY.Text = "원단위(kwh/㎥)";
            this.DAY_W_ENERGY.UseVisualStyleBackColor = true;
            // 
            // DAY_FLOW
            // 
            this.DAY_FLOW.AutoSize = true;
            this.DAY_FLOW.Checked = true;
            this.DAY_FLOW.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DAY_FLOW.Location = new System.Drawing.Point(6, 8);
            this.DAY_FLOW.Name = "DAY_FLOW";
            this.DAY_FLOW.Size = new System.Drawing.Size(87, 16);
            this.DAY_FLOW.TabIndex = 28;
            this.DAY_FLOW.Text = "공급량(㎥)";
            this.DAY_FLOW.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tablePanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.chartPanel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 162);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.84848F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.15152F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(951, 413);
            this.tableLayoutPanel1.TabIndex = 49;
            // 
            // tablePanel1
            // 
            this.tablePanel1.BackColor = System.Drawing.Color.White;
            this.tablePanel1.Controls.Add(this.ultraGrid1);
            this.tablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel1.Location = new System.Drawing.Point(0, 169);
            this.tablePanel1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Size = new System.Drawing.Size(951, 244);
            this.tablePanel1.TabIndex = 1;
            // 
            // ultraGrid1
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance4;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance5;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance10;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(951, 244);
            this.ultraGrid1.TabIndex = 0;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // chartPanel1
            // 
            this.chartPanel1.BackColor = System.Drawing.Color.White;
            this.chartPanel1.Controls.Add(this.chart1);
            this.chartPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel1.Location = new System.Drawing.Point(0, 0);
            this.chartPanel1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.chartPanel1.Name = "chartPanel1";
            this.chartPanel1.Size = new System.Drawing.Size(951, 159);
            this.chartPanel1.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Spread;
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(951, 159);
            this.chart1.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 585);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmMain";
            this.Text = "전력량/원단위분석조회";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.LargeBlock.ResumeLayout(false);
            this.LargeBlock.PerformLayout();
            this.TimeToTime.ResumeLayout(false);
            this.TimeToTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.endDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.chartPanel1.ResumeLayout(false);
            this.chartPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel LargeBlock;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox bzs;
        private System.Windows.Forms.Panel TimeToTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor startDate;
        private System.Windows.Forms.Label TimeToTimeIntervalText;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox DAY;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox DAY_EFFICIENCY;
        private System.Windows.Forms.Button chartVisibleBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.Button tableVisibleBtn;
        private System.Windows.Forms.CheckBox DAY_ENERGY;
        private System.Windows.Forms.CheckBox DAY_W_ENERGY;
        private System.Windows.Forms.CheckBox DAY_FLOW;
        private System.Windows.Forms.ComboBox MONTH_AVERAGE_INTERVAL;
        private System.Windows.Forms.CheckBox MONTH_AVERAGE;
        private System.Windows.Forms.ComboBox WEEK_AVERAGE_INTERVAL;
        private System.Windows.Forms.CheckBox WEEK_AVERAGE;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel tablePanel1;
        private System.Windows.Forms.Panel chartPanel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor endDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox DAY_WEEK;
        private System.Windows.Forms.ComboBox YEAR_AVERAGE_INTERVAL;
        private System.Windows.Forms.CheckBox YEAR_AVERAGE;
        private ChartFX.WinForms.Chart chart1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
    }
}