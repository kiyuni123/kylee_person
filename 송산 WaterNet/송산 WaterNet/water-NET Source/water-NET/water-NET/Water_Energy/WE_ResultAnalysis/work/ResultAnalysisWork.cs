﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.work;
using WaterNet.WE_ResultAnalysis.dao;
using System.Data;
using System.Collections;
using WaterNet.WE_Common.data;
using WaterNet.WV_Common.util;

namespace WaterNet.WE_ResultAnalysis.work
{
    class ResultAnalysisWork : BaseWork
    {
        private static ResultAnalysisWork work = null;
        private ResultAnalysisDao dao = null;

        private ResultAnalysisWork()
        {
            dao = ResultAnalysisDao.GetInstance();
        }

        public static ResultAnalysisWork GetInstance()
        {
            if (work == null)
            {
                work = new ResultAnalysisWork();
            }
            return work;
        }

        public DataTable SelectResultAnalysis(Hashtable parameter)
        {
            DataTable result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                DataSet dataSet =
                    dao.SelectResultAnalysis(base.DataBaseManager, parameter);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];

                        result.Columns.Add("W_ENERGY", typeof(double));
                        result.Columns.Add("O_EFFICIENCY", typeof(double));

                        foreach (DataRow row in result.Rows)
                        {
                            SimulationAnalysisData data = new SimulationAnalysisData(row);
                            row["W_ENERGY"] = data.W_ENERGY;
                            row["O_EFFICIENCY"] = data.O_EFFICIENCY;
                        }

                        result = this.ConvertResult(result, parameter["INTERVAL"].ToString());
                    }
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                RollbackTransaction();
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        private DataTable ConvertResult(DataTable resultList, string interval)
        {
            DataTable result = new DataTable();
            result.Columns.Add("PUMPSTATION_ID", typeof(string));
            result.Columns.Add("TIME", typeof(string));
            result.Columns.Add("FLOW", typeof(double));
            result.Columns.Add("H", typeof(double));
            result.Columns.Add("ENERGY", typeof(double));
            result.Columns.Add("W_ENERGY", typeof(double));
            result.Columns.Add("O_EFFICIENCY", typeof(double));

            DataRow row = null;

            //주/월/년 평균
            if (interval == "WEEK_AVERAGE" || interval == "MONTH_AVERAGE" || interval == "YEAR_AVERAGE")
            {
                //첫번째 로우에서 한주씩
                DateTime minusDate = new DateTime();
                DateTime firstDate = new DateTime();

                int row_count = 0;
                int interval_count = 1;

                double sum_flow = 0;
                double avg_h = 0;
                double sum_energy = 0;
                double avg_w_energy = 0;
                double avg_o_efficiency = 0;

                foreach (DataRow result_row in resultList.Rows)
                {
                    if (row_count == 0 && interval_count == 1)
                    {
                        firstDate = Convert.ToDateTime(result_row["TIME"]);
                    }

                    if (row_count == 0)
                    {
                        if (interval == "WEEK_AVERAGE")
                        {
                            minusDate = firstDate.AddDays(7 * interval_count).AddDays(-1);
                        }
                        if (interval == "MONTH_AVERAGE")
                        {
                            minusDate = firstDate.AddMonths(interval_count).AddDays(-1);
                        }
                        if (interval == "YEAR_AVERAGE")
                        {
                            minusDate = firstDate.AddYears(interval_count).AddDays(-1);
                        }
                    }

                    sum_flow += Utils.ToDouble(result_row["FLOW"]);
                    avg_h += Utils.ToDouble(result_row["H"]);
                    sum_energy += Utils.ToDouble(result_row["ENERGY"]);
                    avg_w_energy += Utils.ToDouble(result_row["W_ENERGY"]);
                    avg_o_efficiency += Utils.ToDouble(result_row["O_EFFICIENCY"]);

                    row_count++;

                    if (minusDate.ToString("yyyyMMdd") == Convert.ToDateTime(result_row["TIME"]).ToString("yyyyMMdd"))
                    {
                        row = result.NewRow();
                        row["PUMPSTATION_ID"] = result_row["PUMPSTATION_ID"];
                        row["TIME"] = minusDate.ToString("yyyy-MM-dd");
                        row["FLOW"] = sum_flow;
                        row["H"] = avg_h / row_count;
                        row["ENERGY"] = sum_energy;
                        row["W_ENERGY"] = avg_w_energy / row_count;
                        row["O_EFFICIENCY"] = avg_o_efficiency / row_count;
                        result.Rows.Add(row);

                        sum_flow = 0;
                        avg_h = 0;
                        sum_energy = 0;
                        avg_w_energy = 0;
                        avg_o_efficiency = 0;
                        row_count = 0;
                        interval_count++;
                    }
                }
            }
            //요일
            else if (interval == "DAY_WEEK")
            {
                //0번부터 월, 화, 수, 목, 금, 토, 일
                int[] row_count = new int[7];
                double[] sum_flow = new double[7];
                double[] avg_h = new double[7];
                double[] sum_energy = new double[7];
                double[] avg_w_energy = new double[7];
                double[] avg_o_efficiency = new double[7];

                string pumpstation_id = string.Empty;
                string[] day_week = {"월요일", "화요일", "수요일", "목요일", "금요일", "토요일", "일요일" };

                foreach (DataRow result_row in resultList.Rows)
                {
                    if (resultList.Rows.IndexOf(result_row) == 0)
                    {
                        pumpstation_id = result_row["PUMPSTATION_ID"].ToString();
                    }

                    if (Convert.ToDateTime(result_row["TIME"]).DayOfWeek == DayOfWeek.Monday)
                    {
                        row_count[0]++;
                        sum_flow[0] += Utils.ToDouble(result_row["FLOW"]);
                        avg_h[0] += Utils.ToDouble(result_row["H"]);
                        sum_energy[0] += Utils.ToDouble(result_row["ENERGY"]);
                        avg_w_energy[0] += Utils.ToDouble(result_row["W_ENERGY"]);
                        avg_o_efficiency[0] += Utils.ToDouble(result_row["O_EFFICIENCY"]);
                    }

                    if (Convert.ToDateTime(result_row["TIME"]).DayOfWeek == DayOfWeek.Tuesday)
                    {
                        row_count[1]++;
                        sum_flow[1] += Utils.ToDouble(result_row["FLOW"]);
                        avg_h[1] += Utils.ToDouble(result_row["H"]);
                        sum_energy[1] += Utils.ToDouble(result_row["ENERGY"]);
                        avg_w_energy[1] += Utils.ToDouble(result_row["W_ENERGY"]);
                        avg_o_efficiency[1] += Utils.ToDouble(result_row["O_EFFICIENCY"]);
                    }

                    if (Convert.ToDateTime(result_row["TIME"]).DayOfWeek == DayOfWeek.Wednesday)
                    {
                        row_count[2]++;
                        sum_flow[2] += Utils.ToDouble(result_row["FLOW"]);
                        avg_h[2] += Utils.ToDouble(result_row["H"]);
                        sum_energy[2] += Utils.ToDouble(result_row["ENERGY"]);
                        avg_w_energy[2] += Utils.ToDouble(result_row["W_ENERGY"]);
                        avg_o_efficiency[2] += Utils.ToDouble(result_row["O_EFFICIENCY"]);
                    }

                    if (Convert.ToDateTime(result_row["TIME"]).DayOfWeek == DayOfWeek.Thursday)
                    {
                        row_count[3]++;
                        sum_flow[3] += Utils.ToDouble(result_row["FLOW"]);
                        avg_h[3] += Utils.ToDouble(result_row["H"]);
                        sum_energy[3] += Utils.ToDouble(result_row["ENERGY"]);
                        avg_w_energy[3] += Utils.ToDouble(result_row["W_ENERGY"]);
                        avg_o_efficiency[3] += Utils.ToDouble(result_row["O_EFFICIENCY"]);
                    }

                    if (Convert.ToDateTime(result_row["TIME"]).DayOfWeek == DayOfWeek.Friday)
                    {
                        row_count[4]++;
                        sum_flow[4] += Utils.ToDouble(result_row["FLOW"]);
                        avg_h[4] += Utils.ToDouble(result_row["H"]);
                        sum_energy[4] += Utils.ToDouble(result_row["ENERGY"]);
                        avg_w_energy[4] += Utils.ToDouble(result_row["W_ENERGY"]);
                        avg_o_efficiency[4] += Utils.ToDouble(result_row["O_EFFICIENCY"]);
                    }

                    if (Convert.ToDateTime(result_row["TIME"]).DayOfWeek == DayOfWeek.Saturday)
                    {
                        row_count[5]++;
                        sum_flow[5] += Utils.ToDouble(result_row["FLOW"]);
                        avg_h[5] += Utils.ToDouble(result_row["H"]);
                        sum_energy[5] += Utils.ToDouble(result_row["ENERGY"]);
                        avg_w_energy[5] += Utils.ToDouble(result_row["W_ENERGY"]);
                        avg_o_efficiency[5] += Utils.ToDouble(result_row["O_EFFICIENCY"]);
                    }

                    if (Convert.ToDateTime(result_row["TIME"]).DayOfWeek == DayOfWeek.Sunday)
                    {
                        row_count[6]++;
                        sum_flow[6] += Utils.ToDouble(result_row["FLOW"]);
                        avg_h[6] += Utils.ToDouble(result_row["H"]);
                        sum_energy[6] += Utils.ToDouble(result_row["ENERGY"]);
                        avg_w_energy[6] += Utils.ToDouble(result_row["W_ENERGY"]);
                        avg_o_efficiency[6] += Utils.ToDouble(result_row["O_EFFICIENCY"]);
                    }
                }

                for (int i = 0; i < 7; i++)
                {
                    if (!double.IsNaN((avg_o_efficiency[i] / row_count[i])))
                    {
                        row = result.NewRow();
                        row["PUMPSTATION_ID"] = pumpstation_id;
                        row["TIME"] = day_week[i];
                        row["FLOW"] = sum_flow[i];
                        row["H"] = avg_h[i] / row_count[i];
                        row["ENERGY"] = sum_energy[i];
                        row["W_ENERGY"] = avg_w_energy[i] / row_count[i];
                        row["O_EFFICIENCY"] = avg_o_efficiency[i] / row_count[i];
                        result.Rows.Add(row);
                    }
                }
            }
            //디폴트 (일일기준)
            else
            {
                foreach (DataRow result_row in resultList.Rows)
                {
                    row = result.NewRow();
                    row["PUMPSTATION_ID"] = result_row["PUMPSTATION_ID"];
                    row["TIME"] = Convert.ToDateTime(result_row["TIME"]).ToString("yyyy-MM-dd");
                    row["FLOW"] = result_row["FLOW"];
                    row["H"] = result_row["H"];
                    row["ENERGY"] = result_row["ENERGY"];
                    row["W_ENERGY"] = result_row["W_ENERGY"];
                    row["O_EFFICIENCY"] = result_row["O_EFFICIENCY"];
                    result.Rows.Add(row);
                }
            }
            return result;
        }
    }
}
