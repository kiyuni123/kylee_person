﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using Oracle.DataAccess.Client;
using System.Collections;

namespace WaterNet.WE_ResultAnalysis.dao
{
    public class ResultAnalysisDao
    {
        private static ResultAnalysisDao dao = null;
        private ResultAnalysisDao() { }

        public static ResultAnalysisDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ResultAnalysisDao();
            }
            return dao;
        }

        public DataSet SelectResultAnalysis(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine(" with pump as                                                                                 ");
            //query.AppendLine("(																								");
            //query.AppendLine("select c.value1 pump_ftr_idn, d.timestamp														");
            //query.AppendLine("  from wh_title a																				");
            //query.AppendLine("	    ,wh_pumps b																				");
            //query.AppendLine("	    ,wh_tags c																				");
            //query.AppendLine("      ,(select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 timestamp from dual				");
            //query.AppendLine("         connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2");
            //query.AppendLine("       ) d																					");
            //query.AppendLine("  where a.energy_gbn = 'Y'																		");
            //query.AppendLine("    and b.inp_number = a.inp_number															");
            //query.AppendLine("    and c.inp_number = b.inp_number															");
            //query.AppendLine("    and c.id = b.id																			");
            //query.AppendLine(")																								");
            //query.AppendLine("select b.pumpstation_id																		");
            //query.AppendLine("	    ,to_date(to_char(a.timestamp,'yyyymmdd'),'yyyymmdd') time								");
            //query.AppendLine("	    ,round(sum(nvl(c.flow,0))/6) flow															");
            //query.AppendLine("	    ,round(avg(c.h)) h																        ");
            //query.AppendLine("	    ,round(sum(nvl(c.energy,0))/6) energy					  								    ");
            //query.AppendLine("  from pump a																					");
            //query.AppendLine("    	,we_pump b																				");
            //query.AppendLine("	    ,(																						");
            //query.AppendLine("	     select pump_ftr_idn																	");
            //query.AppendLine("		       ,to_date(to_char(a.timestamp,'yyyymmdd'),'yyyymmdd') timestamp					");
            //query.AppendLine("		       ,round(sum(flow)) flow															");
            //query.AppendLine("		       ,round(avg(abs(node2_press - node1_press)),1) h									");
            //query.AppendLine("		       ,round(sum(energy)) energy														");
            //query.AppendLine("	     from we_rpt a																			");
            //query.AppendLine("      where a.flow >= 1                                                                       ");
            //query.AppendLine("	    group by pump_ftr_idn, to_date(to_char(a.timestamp,'yyyymmdd'),'yyyymmdd')				");
            //query.AppendLine("	     ) c																					");
            //query.AppendLine(" where b.pump_ftr_idn(+) = a.pump_ftr_idn														");
            //query.AppendLine("   and c.pump_ftr_idn(+) = a.pump_ftr_idn														");
            
            //if (parameter["BZS"].ToString() != "")
            //{
            //    query.Append(" and b.pumpstation_id = '").Append(parameter["BZS"].ToString()).AppendLine("'");
            //} 

            //query.AppendLine("   and c.timestamp(+) = a.timestamp															");
            //query.AppendLine(" group by pumpstation_id, to_date(to_char(a.timestamp,'yyyymmdd'),'yyyymmdd')					");
            //query.AppendLine(" order by time																				");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2),
            //         new OracleParameter("ENDDATE", OracleDbType.Varchar2),
            //         new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["STARTDATE"];
            //parameters[1].Value = parameter["ENDDATE"];
            //parameters[2].Value = parameter["STARTDATE"];

            query.AppendLine("WITH PUMP AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT C.PUMPSTATION_ID ");
            query.AppendLine("      ,C.ID ");
            query.AppendLine("      ,D.TIMESTAMP ");
            query.AppendLine("  FROM WH_TITLE A ");
            query.AppendLine("	    ,WH_PUMPS B ");
            query.AppendLine("	    ,WE_PUMP C ");
            query.AppendLine("      ,(SELECT TO_DATE(:STARTDATE, 'YYYYMMDD') + ROWNUM -1 TIMESTAMP FROM DUAL ");
            query.AppendLine("         CONNECT BY ROWNUM < TO_DATE(:ENDDATE,'YYYYMMDD') - TO_DATE(:STARTDATE,'YYYYMMDD') + 2 ");
            query.AppendLine("       ) D ");
            query.AppendLine("  WHERE A.ENERGY_GBN = 'Y' ");
            query.AppendLine("    AND B.INP_NUMBER = A.INP_NUMBER ");
            query.AppendLine("    AND C.ID = B.ID ");
            query.AppendLine(") ");
            query.AppendLine("SELECT A.PUMPSTATION_ID ");
            query.AppendLine("	    ,TO_DATE(TO_CHAR(A.TIMESTAMP,'YYYYMMDD'),'YYYYMMDD') TIME ");
            query.AppendLine("	    ,ROUND(SUM(NVL(C.FLOW,0))/6) FLOW ");
            query.AppendLine("	    ,ROUND(AVG(C.H)) H ");
            query.AppendLine("	    ,ROUND(SUM(NVL(C.ENERGY,0))/6) ENERGY ");
            query.AppendLine("  FROM PUMP A ");
            query.AppendLine("	    ,( ");
            query.AppendLine("       SELECT ID ");
            query.AppendLine("		         ,TO_DATE(TO_CHAR(A.TIMESTAMP,'YYYYMMDD'),'YYYYMMDD') TIMESTAMP ");
            query.AppendLine("		         ,ROUND(SUM(FLOW)) FLOW ");
            query.AppendLine("		         ,ROUND(AVG(ABS(NODE2_PRESS - NODE1_PRESS)),1) H ");
            query.AppendLine("		         ,ROUND(SUM(ENERGY)) ENERGY ");
            query.AppendLine("	       FROM WE_RPT A ");
            query.AppendLine("        WHERE A.FLOW >= 1 ");
            query.AppendLine("          AND TIMESTAMP BETWEEN TO_DATE(:STARTDATE, 'YYYYMMDD') ");
            query.AppendLine("                            AND TO_DATE(:ENDDATE, 'YYYYMMDD') + 1 ");
            query.AppendLine("	      GROUP BY ID, TO_DATE(TO_CHAR(A.TIMESTAMP,'YYYYMMDD'),'YYYYMMDD') ");
            query.AppendLine("	     ) C ");
            query.AppendLine(" WHERE C.ID(+) = A.ID ");
            query.AppendLine("   AND C.TIMESTAMP(+) = A.TIMESTAMP ");
           
            if (parameter["BZS"].ToString() != "")
            {
                query.Append(" AND A.PUMPSTATION_ID = '").Append(parameter["BZS"].ToString()).AppendLine("'");
            } 

            query.AppendLine(" GROUP BY  ");
            query.AppendLine("       PUMPSTATION_ID ");
            query.AppendLine("      ,TO_DATE(TO_CHAR(A.TIMESTAMP,'YYYYMMDD'),'YYYYMMDD') ");
            query.AppendLine(" ORDER BY  ");
            query.AppendLine("       TIME ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2),
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2),
	                 new OracleParameter("STARTDATE", OracleDbType.Varchar2),
                     new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];

            return manager.ExecuteScriptDataSet(query.ToString(), parameters);
        }
    }
}
