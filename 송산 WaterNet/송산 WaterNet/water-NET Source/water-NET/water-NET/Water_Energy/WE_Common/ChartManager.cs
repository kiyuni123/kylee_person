﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChartFX.WinForms;

namespace WaterNet.WE_Common
{
    /// <summary>
    /// Project ID : WN_WE_A00
    /// Project Explain : 에너지관리에서 공통으로 사용
    /// Project Developer : 전병록
    /// Project Create Date : 2010.10.13
    /// Class Explain : 에너지관리에서 공통으로 사용하는 챠트관련 Class
    ///                  관리한다
    /// </summary>
    public class ChartManager
    {
        #region Private Field -----------------------------------------------------------------------------
        private ChartFX.WinForms.Chart m_Chart = null;
        #endregion Private Field --------------------------------------------------------------------------

        #region 생성자 -----------------------------------------------------------------------------------
        /// <summary>
        /// 기본 생성자
        /// </summary>
        /// <param name="Chart"></param>
        public ChartManager(Chart chart)
        {
            this.m_Chart = chart;
        }
        #endregion 생성자 --------------------------------------------------------------------------------

        public int DataPoint
        {
            set
            {
                m_Chart.Data.Points = value;
            }
        }

        public Chart chart
        {
            get
            {
                return m_Chart;
            }
        }

        public void ChartInit()
        {
            m_Chart.Reset();
            m_Chart.AxesX.Clear();
            m_Chart.AxesY.Clear();
            m_Chart.LegendBox.Visible = false;
            for (int i = m_Chart.Series.Count - 1; i >= 0; i--)
            {
                m_Chart.Series.Remove(m_Chart.Series[i]);
            }

            for (int i = m_Chart.Panes.Count - 1; i >= 0; i--)
            {
                m_Chart.Panes.Remove(m_Chart.Panes[i]);
            }
            m_Chart.Data.Clear();    // 실제 데이타를 초기화 하여 데모에 따라오는 챠트를 숨김.
            m_Chart.Data.Series = 0;
            m_Chart.Titles.Clear();
        }

        public void AllClear()
        {
            for (int i = m_Chart.Series.Count - 1; i > -1; i--)
            {
                m_Chart.Series.RemoveAt(i);
            }
            m_Chart.Update();
        }

        public void AllHiddenSeries()
        {
            foreach (SeriesAttributes series in m_Chart.Series)
            {
                series.Visible = false;
            }
        }

        public void AllShowSeries()
        {
            foreach (SeriesAttributes series in m_Chart.Series)
            {
                series.Visible = true;
            }
        }

        public SeriesAttributes AddSeries()
        {
            int i = m_Chart.Data.Series++;
            return m_Chart.Series[m_Chart.Series.Count - 1];
        }

        public Pane AddPane(Pane pane)
        {
            m_Chart.Panes.Add(pane);
            return m_Chart.Panes[m_Chart.Panes.Count - 1];
        }

        public void RemoveSeries(SeriesAttributes series)
        {
            //foreach (SeriesAttributes sa in m_Chart.Series)
            //{
            //    if (sa.Equals(series))
            //    {
            //        m_Chart.Series.Remove(series);
            //    }
            //}

            m_Chart.Series.Remove(series);
        }

        public void RemoveSeries(int series)
        {
            RemoveSeries(m_Chart.Series[series]);
        }

        public int GetSeriesIndex(string SeriseName)
        {
            for (int i = 0; i < m_Chart.Series.Count; i++)
            {
                if (m_Chart.Series[i].Text == null) continue;
                if (m_Chart.Series[i].Text.Equals(SeriseName))
                {
                    return i;
                }
            }
            return -1;
        }

        public SeriesAttributes GetSeries(string SeriseName)
        {   
            for (int i = 0; i < m_Chart.Series.Count; i++)
            {
                if (m_Chart.Series[i].Text == null) continue;
                if (m_Chart.Series[i].Text.Equals(SeriseName))
                {
                    return m_Chart.Series[i];
                }
            }
            return null;
        }

        public int GetPaneIndex(string PaneName)
        {
            for (int i = 0; i < m_Chart.Panes.Count; i++)
            {
                if (m_Chart.Panes[i].Title.Text == null) continue;
                if (m_Chart.Panes[i].Title.Text.Equals(PaneName))
                {
                    return i;
                }
            }
            return -1;
        }

        public Pane GetPane(string PaneName)
        {
            for (int i = 0; i < m_Chart.Panes.Count; i++)
            {
                if (m_Chart.Panes[i].Title.Text == null) continue;
                if (m_Chart.Panes[i].Title.Text.Equals(PaneName))
                {
                    return m_Chart.Panes[i];
                }
            }
            return null;
        }

        public Pane GetPane(int Paneindex)
        {
            //if (m_Chart.Panes.Count < Paneindex)
            //{

            //}
            //for (int i = 0; i < m_Chart.Panes.Count; i++)
            //{
            //    if (m_Chart.Panes[i].Title.Text == null) continue;
            //    if (m_Chart.Panes[i].Title.Text.Equals(PaneName))
            //    {
            //        return m_Chart.Panes[i];
            //    }
            //}
            return null;
        }
    }
}
