﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using WaterNet.WaterNetCore;

using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinGrid.ExcelExport;
using Infragistics.Excel;

namespace WaterNet.WE_Common
{
    /// <summary>
    /// Project ID : WN_WE_A00
    /// Project Explain : 에너지관리에서 공통으로 사용
    /// Project Developer : 전병록
    /// Project Create Date : 2010.10.13
    /// Class Explain : 에너지관리에서 공통으로 사용하는 그리드관련 Class
    ///                 그리드 스타일, 이벤트, 데이터바인드등을 관리한다
    /// </summary>
    public class GridManager
    {   
        #region Private Field -----------------------------------------------------------------------------
        private UltraGrid m_ultraGrid = null;
        private int m_bandIndex = 0;
        private string m_Title;
        private string m_tag = "flat"; //그리드의 삭제가능 필드
        #endregion Private Field --------------------------------------------------------------------------

        #region Public Field ------------------------------------------------------------------------------

        #endregion Public Field ---------------------------------------------------------------------------

        #region 생성자 -----------------------------------------------------------------------------------
        /// <summary>
        /// 기본 생성자
        /// </summary>
        /// <param name="ultraGrid"></param>
        public GridManager(UltraGrid ultraGrid)
        {
            this.m_ultraGrid = ultraGrid;

            SetGridStyle_Default();
        }

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="ultraGrid"></param>
        /// <param name="title"></param>
        public GridManager(UltraGrid ultraGrid, string title)
        {
            this.m_ultraGrid = ultraGrid;
            this.m_Title = title;

            SetGridStyle_Default();
        }
        #endregion 생성자 --------------------------------------------------------------------------------

        #region Private Property --------------------------------------------------------------------------

        #endregion Private Property -----------------------------------------------------------------------

        #region Public Property ---------------------------------------------------------------------------


        /// <summary>
        /// Get UltraGrid
        /// </summary>
        public UltraGrid Grid
        {
            get
            {
                return m_ultraGrid;
            }
            set
            {
                m_ultraGrid = value;
                m_ultraGrid.Update();
            }
        }

        /// <summary>
        /// 그리드의 Band 번호
        /// </summary>
        public int BandIndex
        {
            get
            {
                return m_bandIndex;
            }
            set
            {
                m_bandIndex = value;
            }
        }

        /// <summary>
        /// 그리드매니저 클래스에 등록된 그리드의 이름을 설정 또는 반환한다.
        /// 그리드 이름은 그리드를 엑셀로 저장할때 시트이름등으로 사용된다.
        /// </summary>
        public string Title
        {
            get
            {
                return this.m_Title;
            }
            set
            {
                this.m_Title = value;
            }
        }

        /// <summary>
        /// 그리드의 Tag
        /// </summary>
        public string tag
        {
            get
            {
                return this.m_tag;
            }
            set
            {
                this.m_tag = value;
            }
        }

        /// <summary>
        /// 그리드의 컬럼콜렉션
        /// </summary>
        public ColumnsCollection Columns
        {
            get
            {
                return this.Grid.DisplayLayout.Bands[m_bandIndex].Columns;
            }
        }

        #endregion Public Property ------------------------------------------------------------------------

        #region Private Method ----------------------------------------------------------------------------

        #endregion Private Method -------------------------------------------------------------------------

        #region Public Method ----------------------------------------------------------------------------

        #region GridStyle Set
        /// <summary>
        /// 그리드 스타일의 공통(append, update, select Mode)
        /// </summary>
        public void SetGridStyle_Default()
        {
            this.Grid.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            this.Grid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            this.Grid.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            this.Grid.DisplayLayout.GroupByBox.Hidden = true;
            this.Grid.DisplayLayout.MaxColScrollRegions = 1;
            this.Grid.DisplayLayout.MaxRowScrollRegions = 1;

            this.Grid.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;

            this.Grid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            this.Grid.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;
            this.Grid.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;

            this.Grid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.Default;
            this.Grid.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            this.Grid.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;
            this.Grid.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            this.Grid.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;

            this.Grid.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            this.Grid.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;

            this.Grid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            this.Grid.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            this.Grid.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);
        }

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Update 모드로 정의하고, 모든 Cell에 대해 Edit 가능하도록 설정
        /// </summary>
        public void SetGridStyle_Update()
        {
            this.Grid.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;

            this.Grid.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.LightYellow;
            this.Grid.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Blue;
            this.Grid.DisplayLayout.Override.RowAppearance.BackColor = Color.White;

            this.Grid.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            this.Grid.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;

            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.Grid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.Grid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            foreach (UltraGridColumn item in Grid.DisplayLayout.Bands[0].Columns)
            {
                item.Header.Appearance.TextHAlign = HAlign.Center;
                item.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.CellActivation = Activation.AllowEdit;
                item.CellClickAction = CellClickAction.Edit;
            }
        }

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Append 모드로 정의하고, 모든 Cell에 대해 Edit 못하게 수정
        /// </summary>
        public void SetGridStyle_Append()
        {
            Grid.DisplayLayout.Override.AllowAddNew = AllowAddNew.FixedAddRowOnTop;
            Grid.DisplayLayout.Override.TemplateAddRowPrompt = "신규 데이터는 여기에 입력하세요...";

            Grid.DisplayLayout.Override.TemplateAddRowAppearance.BackColor = Color.FromArgb(245, 250, 255);
            Grid.DisplayLayout.Override.TemplateAddRowAppearance.ForeColor = SystemColors.GrayText;

            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.Grid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.Grid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            foreach (UltraGridColumn item in Grid.DisplayLayout.Bands[0].Columns)
            {
                item.Header.Appearance.TextHAlign = HAlign.Center;
                item.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.CellActivation = Activation.AllowEdit;
                item.CellClickAction = CellClickAction.Edit;
            }

        }

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Select 모드로 정의하고, 모든 Cell에 대해 Edit 못하게 수정
        /// </summary>
        public void SetGridStyle_Select()
        {
            Grid.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;

            foreach (UltraGridColumn item in Grid.DisplayLayout.Bands[0].Columns)
            {
                item.Header.Appearance.TextHAlign = HAlign.Center;
                item.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.CellActivation = Activation.NoEdit;
                item.CellClickAction = CellClickAction.RowSelect;
            }
        }
        #endregion

        #region 그리드 컬럼 Width변경
        /// <summary>
        /// UltraGrid에 Set된 Data의 Length에 맞게 자동으로 Column Size를 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        public void PerformAutoResize()
        {
            for (int I = 0; I <= this.Grid.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                this.Grid.DisplayLayout.Bands[0].Columns[I].PerformAutoResize();
            }
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iCol">Column Index</param>
        public void ColumeAllowEdit(int iCol)
        {
            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.Grid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.Grid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            this.Grid.DisplayLayout.Bands[0].Columns[iCol].CellActivation = Activation.AllowEdit;
            this.Grid.DisplayLayout.Bands[0].Columns[iCol].CellClickAction = CellClickAction.EditAndSelectText;
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public void ColumeAllowEdit(int iSCol, int iECol)
        {
            ////현재 선택된 셀만 백칼라 = YellowGreen
            this.Grid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            this.Grid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            for (int i = iSCol; i < iECol; i++)
            {
                Grid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                Grid.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }



        /// <summary>
        /// UltraGrid의 특정 Row의 Cell을 Disable 모드로 변경한다.
        /// 단 UltraGrid의 기본 셋팅이 Activation.AllowEdit 이어야 한다.(이건 좀더 확인 필요)
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iRow">Row Index</param>
        /// <param name="iCol">Column Index</param>
        public void CellDisable(int iRow, int iCol)
        {
            this.Grid.Rows[iRow].Cells[iCol].Appearance.BackColorDisabled = Color.LightSalmon;
            this.Grid.Rows[iRow].Cells[iCol].Activation = Activation.Disabled;
        }

        /// <summary>
        /// UltraGrid의 특정 Row의 Cell을 AllowEdit 모드로 변경한다.
        /// 단 UltraGrid의 기본 셋팅이 Activation.NoEdit 이어야 한다.(이건 좀더 확인 필요)
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iRow">Row Index</param>
        /// <param name="iCol">Column Index</param>
        public void CellAllowEdit(int iRow, int iCol)
        {
            this.Grid.Rows[iRow].Cells[iCol].Appearance.BackColor = Color.Lime;
            this.Grid.Rows[iRow].Cells[iCol].Activation = Activation.AllowEdit;
            Application.DoEvents();
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 NoEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="strCol">Column Name. Empty이면 0번 Column<</param>
        public void ColumeNoEdit(string strCol)
        {
            if (string.IsNullOrEmpty(strCol)) return;

            this.Grid.DisplayLayout.Bands[0].Columns[strCol].CellActivation = Activation.NoEdit;
            this.Grid.DisplayLayout.Bands[0].Columns[strCol].CellClickAction = CellClickAction.RowSelect;

        }

        /// <summary>
        /// UltraGrid의 특정 Column을 NoEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public void ColumeNoEdit(int iSCol, int iECol)
        {
            for (int i = iSCol; i < iECol; i++)
            {
                this.Grid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
                this.Grid.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.RowSelect;
            }
        }
        /// <summary>
        /// 정해진 컬럼 Width변경
        /// </summary>
        /// <param name="width"></param>
        public void SetColumnWidth(int width)
        {
            for (int I = 0; I <= this.Grid.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                this.Grid.DisplayLayout.Bands[0].Columns[I].Width = width;
            }
        }

        public void RemoveAllColumns()
        {
            this.Grid.DisplayLayout.Bands[m_bandIndex].Columns.ClearUnbound();
        }

        #endregion

        #region Ultra Column, GroupColumn Header

        #region 그리드에 동적 컬럼 추가
        /// <summary>
        /// 그리드에 동적 컬럼을 추가
        /// </summary>
        /// <param name="key">Column Key Property</param>
        public UltraGridColumn AddColumn(string key)
        {
            return AddColumn(key, key);
        }

        public UltraGridColumn AddColumn(string key, string caption, int width)
        {
            UltraGridColumn column = this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Add();
            column.Width = width;
            column.Key = key;
            column.Header.Caption = caption;

            return column;
        }

        public UltraGridColumn AddColumn(string key, string caption)
        {
            return AddColumn(key, caption, Activation.NoEdit);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation)
        {
            return AddColumn(key, caption, activation, CellClickAction.Default);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction)
        {
            return AddColumn(key, caption, activation, cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle.Default);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style)
        {
            return AddColumn(key, caption, activation, cellClickAction, style, true);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible)
        {
            return AddColumn(key, caption, activation, cellClickAction, style, visible, 100);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible, int width)
        {
            return AddColumn(key, caption, activation, cellClickAction, style, visible, width, string.Empty);
        }

        public UltraGridColumn AddColumn(string key, string caption, Activation activation, CellClickAction cellClickAction, Infragistics.Win.UltraWinGrid.ColumnStyle style, bool visible, int width, string tag)
        {
            UltraGridColumn column = this.m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns.Add();
            column.Width = width;
            column.Key = key;
            column.Header.Caption = caption;
            column.CellActivation = activation;
            if (activation == Activation.AllowEdit)
            {
                ////현재 선택된 셀만 백칼라 = YellowGreen
                this.m_ultraGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
                this.m_ultraGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;
            }
            column.CellClickAction = cellClickAction;
            column.Style = style;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Hidden = visible;
            column.Tag = tag;

            return column;
        }
        #endregion

        public UltraGridGroup AddColumnGroup(string key, ArrayList oColumns)
        {
            return AddColumnGroup(key, key, oColumns, string.Empty);
        }

        public UltraGridGroup AddColumnGroup(string key, string caption, ArrayList oColumns)
        {
            return AddColumnGroup(key, caption, oColumns, string.Empty);
        }

        public UltraGridGroup AddColumnGroup(string key, string caption, ArrayList oColumns, string tag)
        {
            return AddColumnGroup(key, caption, oColumns, tag, 1);
        }

        public UltraGridGroup AddColumnGroup(string key, string caption, ArrayList oColumns, string tag, int levelCount)
        {
            UltraGridGroup gridGroup = null;
            this.Grid.DisplayLayout.Bands[m_bandIndex].LevelCount = levelCount;
            if (getColumnGroup(key) == null)
            {
                gridGroup = this.Grid.DisplayLayout.Bands[m_bandIndex].Groups.Add(key);
                gridGroup.Header.Caption = caption;
                gridGroup.Header.Appearance.TextHAlign = HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = VAlign.Middle;
                gridGroup.Header.Appearance.BackGradientStyle = GradientStyle.None;
                gridGroup.Tag = tag;

                ColumnsCollection ColumnCollection = this.Grid.DisplayLayout.Bands[m_bandIndex].Columns;
                foreach (UltraGridColumn Column in ColumnCollection)
                {
                    foreach (string item in oColumns)
                    {
                        if (Column.Key == item)
                        {
                            Column.Group = gridGroup;
                            Column.Level = this.Grid.DisplayLayout.Bands[m_bandIndex].LevelCount - 1;
                            continue;
                        }
                    }
                }
            }
            return gridGroup;
        }

        /// <summary>
        /// 그리드의 Header그룹 찾기
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public UltraGridGroup getColumnGroup(string key)
        {
            UltraGridGroup gridGroup = null;
            foreach (UltraGridGroup item in this.Grid.DisplayLayout.Bands[m_bandIndex].Groups)
            {
                if (item.Key == key)
                {
                    gridGroup = item;
                    continue;
                }
            }
            return gridGroup;
        }

        /// <summary>
        /// 해당 그리드 그룹 true = 감추기, false = 보이기
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hidden"></param>
        public void VisibleGridGroup(string key, bool hidden)
        {
            UltraGridGroup gridGroup = getColumnGroup(key);
            if (gridGroup == null) return;
            foreach (UltraGridColumn item in gridGroup.Columns)
            {
                if (item != null)
                {
                    item.Hidden = hidden;
                }
            }

            gridGroup.Hidden = hidden;
        }

        /// <summary>
        /// 그리드의 컬럼 찾기
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public UltraGridColumn getColumn(string key)
        {
            UltraGridColumn column = null;
            foreach (UltraGridColumn item in this.Grid.DisplayLayout.Bands[m_bandIndex].Columns)
            {
                if (Convert.ToString(item.Key) == key)
                {
                    column = item;
                    continue;
                }
            }
            return column;
        }

        /// <summary>
        /// 그리드의 컬럼
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hidden"></param>
        public void VisibleColumn(string key, bool hidden)
        {
            UltraGridColumn column = getColumn(key);
            if (column == null) return;

            column.Hidden = hidden;
        }
        #endregion

        #region DataSource 설정
        /// <summary>
        /// 신규 DataSet을 그리드에 적용
        /// </summary>
        /// <param name="dataSource">System.Data.DataSet</param>
        /// <param name="dataMember">DataTable Name</param>
        public void SetDataSource(DataTable oDataSource)
        {
            try
            {
                this.Grid.DataSource = oDataSource.DefaultView;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 그리드매니저 클래스에 등록된 그리드에 데이터소스를 설정한다.
        /// 데이터소스의 타입은 데이터셋이며 테이블이름을 추가 인자로 갖는다.
        /// </summary>
        /// <param name="dataSource">System.Data.DataSet</param>
        /// <param name="dataMember">DataTable Name</param>
        public void SetDataSource(Object bindDataSource, string bindDataMember)
        {
            try
            {
                this.Grid.DataSource = new BindingSource(bindDataSource, bindDataMember);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// 그리드의 데이터소스를 반환
        /// </summary>
        /// <returns></returns>
        public DataTable GetDataSource()
        {
            DataTable oDataTable = this.Grid.DataSource as DataTable;
            //if (oDataTable == null)
            //{
            //    oDataTable = CreateEmptyDataSource();
            //}
            return oDataTable;
        }

        /// <summary>
        /// 현재 그리드에 설정된 칼럼으로 빈 데이터테이블을 반환한다.
        /// 테이블을 병합하기위해 전체를 가지고 있는 테이블
        /// </summary>
        /// <returns></returns>
        public DataTable GetGridToDataTable()
        {
            DataTable oDataTable = new DataTable();
            foreach (UltraGridColumn item in m_ultraGrid.DisplayLayout.Bands[BandIndex].Columns)
            {
                DataColumn oColumn = new DataColumn();
                oColumn.ColumnName = item.Key;
                oColumn.Caption = item.Header.Caption;
                oColumn.DataType = item.DataType;

                oDataTable.Columns.Add(oColumn);
            }

            return oDataTable;
        }

        /// <summary>
        /// 데이터 소스 삭제
        /// </summary>
        public void ClearDataSource()
        {
            if (this.Grid.DataSource != null)
            {
                if (this.Grid.DataSource is BindingSource)
                {
                    BindingSource source = this.Grid.DataSource as BindingSource;
                    source.Clear();
                }

                if (this.Grid.DataSource is IList)
                {
                    IList source = this.Grid.DataSource as IList;
                    source.Clear();
                }

                if (this.Grid.DataSource is DataTable)
                {
                    DataTable source = this.Grid.DataSource as DataTable;
                    source.Clear();
                }
            }
        }

        public void InitializeGrid()
        {
            foreach (UltraGridRow item in this.Grid.Rows)
            {
                item.Delete(false);
            }
        }
        #endregion DataSource 설정

        /// <summary>
        /// 그리드 소트
        /// 
        /// </summary>
        /// <param name="key"></param>
        public void SetGridSort(string key)
        {
            SetGridSort(key, false);
        }

        /// <summary>
        /// 그리드 소트
        /// true : 내림차순, false : 오름차순
        /// </summary>
        /// <param name="key"></param>
        /// <param name="desc"></param>
        public void SetGridSort(string key, bool desc)
        {
            UltraGridColumn oGridColumn = getColumn(key);
            if (oGridColumn != null)
            {
                this.Grid.DisplayLayout.Bands[m_bandIndex].SortedColumns.Add(oGridColumn, desc);
            }
        }


        public DataColumn[] SetPrimaryKey(string[] keys)
        {
            DataColumn[] oDataColumns = new DataColumn[keys.Length];
            for (int i = 0; i < keys.Length; i++)
            {
                oDataColumns.SetValue(getColumn(keys[i]), i);
            }
            
            return oDataColumns;
        }
        #endregion Public Method -------------------------------------------------------------------------

        #region Public Instance Methods
        /// <summary>
        /// 그리드 매니저에 복수의 그리드 사용가능
        /// 그리드 클래스를 생성하고, 딕셔너리를 사용
        /// 추가, 삭제, 찾기
        /// </summary>
        private Dictionary<string, GridObject> items = new Dictionary<string, GridObject>();
        /// <summary>
        /// 그리드 추가
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        public void Add(string key, UltraGrid item)
        {
            this.Remove(key);

            if (!items.ContainsKey(key))
            {
                this.items.Add(key, new GridObject(item));
            }
        }

        /// <summary>
        /// 그리드 삭제
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            if (this.items.ContainsKey(key))
            {
                this.items.Remove(key);
            }
        }
        /// <summary>
        /// 전체삭제
        /// </summary>
        public void RemoveAll()
        {
            this.items.Clear();
        }
        /// <summary>
        /// 그리드 찾기
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public UltraGrid GetItem(string key)
        {
            if (this.items.ContainsKey(key))
            {
                return this.items[key].Item;
            }
            return null;
        }
        #endregion

    }


    #region 그리드 object Class
    public class GridObject
    {
        private UltraGrid ultraGrid;

        public GridObject(UltraGrid ultraGrid)
        {
            this.ultraGrid = ultraGrid;
        }

        public UltraGrid Item
        {
            get
            {
                return this.ultraGrid;
            }
        }
    }
    #endregion
}
