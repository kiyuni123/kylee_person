﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WV_Common.util;

namespace WaterNet.WE_Common.data
{
    public class SimulationAnalysisData
    {
        private string pumpstation_id = string.Empty;
        private string pump_ftr_idn = string.Empty;
        private string remark = string.Empty;
        private double flow = 0;
        private double h = 0;
        private double energy = 0;
        private double r_efficiency = 0;
        private double diff = 10;
        string result = string.Empty;

        public SimulationAnalysisData(DataRow dataRow)
        {
            if (dataRow.Table.Columns.IndexOf("PUMPSTATION_ID") != -1 && dataRow["PUMPSTATION_ID"] != null && dataRow["PUMPSTATION_ID"] != DBNull.Value)
            {
                this.pumpstation_id = dataRow["PUMPSTATION_ID"].ToString();
            }

            if (dataRow.Table.Columns.IndexOf("PUMP_FTR_IDN") != -1 && dataRow["PUMP_FTR_IDN"] != null && dataRow["PUMP_FTR_IDN"] != DBNull.Value)
            {
                this.pump_ftr_idn = dataRow["PUMP_FTR_IDN"].ToString();
            }

            if (dataRow.Table.Columns.IndexOf("REMARK") != -1 && dataRow["REMARK"] != null && dataRow["REMARK"] != DBNull.Value)
            {
                this.remark = dataRow["REMARK"].ToString();
            }

            if (dataRow.Table.Columns.IndexOf("FLOW") != -1 && dataRow["FLOW"] != null && dataRow["FLOW"] != DBNull.Value)
            {
                this.flow = Utils.ToDouble(dataRow["FLOW"]);
            }

            if (dataRow.Table.Columns.IndexOf("H") != -1 && dataRow["H"] != null && dataRow["H"] != DBNull.Value)
            {
                this.h = Utils.ToDouble(dataRow["H"]);
            }

            if (dataRow.Table.Columns.IndexOf("ENERGY") != -1 && dataRow["ENERGY"] != null && dataRow["ENERGY"] != DBNull.Value)
            {
                this.energy = Utils.ToDouble(dataRow["ENERGY"]);
            }

            if (dataRow.Table.Columns.IndexOf("DIFF") != -1 && dataRow["DIFF"] != null && dataRow["DIFF"] != DBNull.Value)
            {
                //정격효율 운영범위..
                this.diff = Utils.ToDouble(dataRow["DIFF"]);
            }

            if (dataRow.Table.Columns.IndexOf("R_EFFICIENCY") != -1 && dataRow["R_EFFICIENCY"] != null && dataRow["R_EFFICIENCY"] != DBNull.Value)
            {
                //정격효율 운영범위..
                this.r_efficiency = Utils.ToDouble(dataRow["R_EFFICIENCY"]);
            }
        }

        //사업장
        public string PUMPSTATION_ID
        {
            get
            {
                return this.pumpstation_id;
            }
        }

        //펌프아이디
        public string PUMP_FTR_IDN
        {
            get
            {
                return this.pump_ftr_idn;
            }
        }

        //펌프설명
        public string REMARK
        {
            get
            {
                return this.remark;
            }
        }

        //유량
        public double FLOW
        {
            get
            {
                return this.flow;
            }
        }

        //양정
        public double H
        {
            get
            {
                if (this.flow == 0)
                {
                    return 0;
                }

                return this.h;
            }
        }

        //전력량
        public double ENERGY
        {
            get
            {
                if (this.flow == 0)
                {
                    return 0;
                }

                return this.energy;
            }
        }

        //원단위
        public double W_ENERGY
        {
            get
            {
                double result = 0;

                if (this.flow != 0)
                {
                    result = this.energy / this.flow;
                }

                if (this.flow == 0)
                {
                    return 0;
                }

                return result;
            }
        }

        //운영효율
        public double O_EFFICIENCY
        {
            get
            {
                double result = 0;

                if (this.W_ENERGY != 0)
                {
                    result = ((this.h / this.W_ENERGY) * 0.002722) * 100;
                }

                if (this.flow == 0)
                {
                    return 0;
                }

                return result;
            }
        }

        //정격효율
        public double R_EFFICIENCY
        {
            get
            {
                if (this.flow == 0)
                {
                    return 0;
                }

                return this.r_efficiency;
            }
            set
            {
                this.r_efficiency = value;
            }
        }

        //운영효율차
        public double R_O
        {
            get
            {
                if (this.flow == 0)
                {
                    return 0;
                }

                return this.R_EFFICIENCY - this.O_EFFICIENCY;
            }
        }

        //결과
        public string RESULT
        {
            get
            {
                if (this.FLOW == 0)
                {
                    this.result = "OFF";
                    return this.result;
                }

                if (this.r_efficiency == 0)
                {
                    this.result = "R";
                    return this.result;
                }

                double r_o = this.R_O;

                if (r_o > this.diff)
                {
                    this.result = "M";
                }
                else if (r_o < this.diff * -1)
                {
                    this.result = "O";
                }
                else
                {
                    this.result = "T";
                }

                return this.result;
            }
        }
    }
}