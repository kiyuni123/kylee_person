﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WE_Common
{
    public partial class frmINP_Title : Form
    {
        public string m_INP_NUMBER = string.Empty;
        private string m_SelectQuery = string.Empty;

        public frmINP_Title()
        {
            InitializeComponent();

            InitializeSetting();
            m_SelectQuery =  " SELECT INP_NUMBER, USE_GBN, TITLE, ";
            m_SelectQuery += " TO_CHAR(TO_DATE(INS_DATE,'YYYYMMDDHH24MISS'),'YYYY-MM-DD HH24:MI:SS') INS_DATE ";
            m_SelectQuery += " FROM WH_TITLE";

            
        }

        public frmINP_Title(string USE_GBN)
        {
            InitializeComponent();

            InitializeSetting();

            m_SelectQuery =  " SELECT INP_NUMBER, USE_GBN, TITLE, ";
            m_SelectQuery += " TO_CHAR(TO_DATE(INS_DATE,'YYYYMMDDHH24MISS'),'YYYY-MM-DD HH24:MI:SS') INS_DATE ";
            m_SelectQuery += " FROM WH_TITLE";
            m_SelectQuery += " WHERE USE_GBN = '" + USE_GBN + "'";

        }

        #region 초기화설정
        private void InitializeSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = ultraGrid_INP.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "모델번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            oUltraGridColumn = ultraGrid_INP.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USE_GBN";
            oUltraGridColumn.Header.Caption = "구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = ultraGrid_INP.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TITLE";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            oUltraGridColumn = ultraGrid_INP.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INS_DATE";
            oUltraGridColumn.Header.Caption = "작성일";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 160;

            FormManager.SetGridStyle(ultraGrid_INP);
            #endregion
        }
        #endregion

        public void Open()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            try
            {
                oDBManager.Open();

                DataTable oDatatable = oDBManager.ExecuteScriptDataTable(m_SelectQuery, null);

                ultraGrid_INP.DataSource = oDatatable.DefaultView;

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null)
                {
                    oDBManager.Close();
                } 
            }
            toolStripMessage.Text = "실행모델을 더블클릭하거나, 선택버튼을 클릭하세요.";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            m_INP_NUMBER = string.Empty;

            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (ultraGrid_INP.ActiveRow == null)
            {
                MessageManager.ShowInformationMessage("선택된 행이 없습니다.");
                return;
            }

            m_INP_NUMBER = Convert.ToString(ultraGrid_INP.ActiveRow.Cells["INP_NUMBER"].Value);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void ultraGrid_INP_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            btnSelect_Click(this, new EventArgs());
        }
    }
}
