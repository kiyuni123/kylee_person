﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WE_Common
{
    public class WE_VariableManager
    {
        public static string m_INP_NUMBER;  //에너지 해석 모델번호 INP_NUMBER

        public static string[] m_shapes = { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE" };

        #region FeatureLayer 선언
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_ValveLayer;     //INP VALVE 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_JunctionLayer;  //INP JUNCTION 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_ReservoirLayer; //INP RESERVOIR 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_TankLayer;      //INP TANK 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_PipeLayer;      //INP PIPE 레이어
        #endregion FeatureLayer 선언
    }
}
