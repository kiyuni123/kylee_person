﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WE_Common
{
    /// <summary>
    /// Project ID : WN_WE_A00
    /// Project Explain : 에너지관리에서 공통으로 사용
    /// Project Developer : 전병록
    /// Project Create Date : 2010.10.13
    /// Class Explain : 에너지관리에서 공통으로 사용하는 공통함수 Class
    /// </summary>
    public class WE_FunctionManager
    {
        /// <summary>
        /// Double 반환
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double convertToDouble(object value)
        {
            if (Convert.IsDBNull(value)) return 0.0;
            if (value == null) return 0.0;

            return Convert.ToDouble(value);
        }

        /// <summary>
        /// 현재시간을 반환한다.
        /// </summary>
        /// <returns></returns>
        public static string GetNowDateTimeToString()
        {
            return GetNowDateTimeToString(DateTime.Now);
        }

        public static string GetNowDateTimeToString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int year = oDateTime.Year;
            int month = oDateTime.Month;
            int day = oDateTime.Day;
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = year.ToString();
            nowtime += month.ToString().PadLeft(2, '0');
            nowtime += day.ToString().PadLeft(2, '0');
            nowtime += hour.ToString().PadLeft(2, '0');
            nowtime += min.ToString().PadLeft(2, '0');
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowDateTimeToFormatString()
        {
            return GetNowDateTimeToFormatString(DateTime.Now);
        }

        public static string GetNowDateTimeToFormatString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int year = oDateTime.Year;
            int month = oDateTime.Month;
            int day = oDateTime.Day;
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = year.ToString() + "-";
            nowtime += month.ToString().PadLeft(2, '0') + "-";
            nowtime += day.ToString().PadLeft(2, '0') + " ";
            nowtime += hour.ToString().PadLeft(2, '0') + ":";
            nowtime += min.ToString().PadLeft(2, '0') + ":";
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowTimeToString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = hour.ToString().PadLeft(2, '0');
            nowtime += min.ToString().PadLeft(2, '0');
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowTimeToString()
        {
            return GetNowTimeToString(DateTime.Now);
        }

        public static string GetNowTimeToFormatString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = hour.ToString().PadLeft(2, '0') + ":";
            nowtime += min.ToString().PadLeft(2, '0') + ":";
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowTimeToFormatString()
        {
            return GetNowTimeToFormatString(DateTime.Now);
        }

        /// <summary>
        /// 3자리 숫자마다 콤마 삽입
        /// </summary>
        /// <param name="no"></param>
        /// <returns></returns>
        public static string addinComma(string no)
        {
            char[] tmp = no.ToCharArray();
            int cnt = tmp.Length;
            string result = "";
            try
            {
                while (cnt > 0)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        cnt--;
                        result = tmp[cnt] + result;

                    }
                    if (cnt > 0) result = "," + result;
                }

            }
            catch { }
            return result;
        }
        /// 
        public static string addinComma(int no)
        {
            return addinComma(Convert.ToString(no));
        }

        public static string addinComma(double no)
        {
            string str = Convert.ToString(no);
            string[] strtmp = str.Split('.');
            string strint = addinComma(strtmp[0]);

            return strint + "." + strtmp[1];
        }

        /// <summary>
        /// FeatureLayer 복사본 생성하기
        /// </summary>
        /// <param name="pWorkspace"></param>
        /// <param name="pLayer"></param>
        /// <returns></returns>
        public static ILayer Copy(IWorkspace pWorkspace, ILayer pLayer)
        {
            IFeatureLayer srcFeatureLayer = pLayer as IFeatureLayer;

            ///Create Field
            IFields fields = srcFeatureLayer.FeatureClass.Fields;

            ///Create FeatureClass
            IFeatureWorkspace pFeatureWorkspace = pWorkspace as IFeatureWorkspace;
            IFeatureClass pFeatureClass = pFeatureWorkspace.CreateFeatureClass(srcFeatureLayer.Name, fields, srcFeatureLayer.FeatureClass.CLSID,
                                                                               srcFeatureLayer.FeatureClass.EXTCLSID, srcFeatureLayer.FeatureClass.FeatureType,
                                                                               srcFeatureLayer.FeatureClass.ShapeFieldName, "");

            ///Insert Feature
            ///Start an edit session and edit operation.
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)pWorkspace;
            workspaceEdit.StartEditing(true);
            workspaceEdit.StartEditOperation();

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureClass srcFeatureClass = srcFeatureLayer.FeatureClass;
                IFeatureCursor srcFeatureCursor = srcFeatureClass.Search(null, true);
                IFeature srcFeature = null;
                comReleaser.ManageLifetime(srcFeatureCursor);

                IFeatureCursor featureCursor = pFeatureClass.Insert(true);
                comReleaser.ManageLifetime(featureCursor);
                IFeatureBuffer featureBuffer = pFeatureClass.CreateFeatureBuffer();
                comReleaser.ManageLifetime(featureBuffer);
                IFeature pFeature = featureBuffer as IFeature;

                while ((srcFeature = srcFeatureCursor.NextFeature()) != null)
                {
                    for (int i = 0; i < srcFeature.Fields.FieldCount; i++)
                    {
                        switch (srcFeature.Fields.get_Field(i).Type)
                        {
                            case esriFieldType.esriFieldTypeOID:
                                break;
                            case esriFieldType.esriFieldTypeGeometry:
                                pFeature.Shape = srcFeature.Shape;
                                break;
                            default:
                                pFeature.set_Value(srcFeature.Fields.FindField(srcFeature.Fields.get_Field(i).Name), srcFeature.get_Value(i));
                                break;
                        }

                    }
                    featureCursor.InsertFeature(featureBuffer);
                }

                featureCursor.Flush();
            }

            workspaceEdit.StopEditOperation();
            workspaceEdit.StopEditing(true);

            IFeatureLayer CopyLayer = new FeatureLayerClass();
            CopyLayer.FeatureClass = pFeatureClass;
            return CopyLayer as ILayer;
        }
    }
}
