﻿/**************************************************************************
 * 파일명   : ComplexDataAnalysisDao.cs
 * 작성자   : shpark
 * 작성일자 : 2015.10.29
 * 설명     : 복합데이터시계열분석 Dao
 * 변경이력 : 2015.10.29 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;

using WaterNet.WaterNetCore;

namespace WaterNet.WD_ComplexDataAnalysis.dao
{
    class ComplexDataAnalysisDao
    {

        #region 1. 전역변수

        private static ComplexDataAnalysisDao dao = null;

        #endregion 전역변수


        #region 2. 초기화

        private ComplexDataAnalysisDao()
        { 
        }

        public static ComplexDataAnalysisDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ComplexDataAnalysisDao();
            }

            return dao;
        }

        #endregion 초기화


        #region 3. 외부인터페이스

        //관리단 조회
        public DataTable SelectMgdpcd(OracleDBManager dbManager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      MGDPCD                                      ");
            queryString.AppendLine("            ,MGDPNM                                     ");
            queryString.AppendLine("  from      CM_DEPARTMENT                               ");
            queryString.AppendLine(" where      USEYN   =   'Y'                             ");
            queryString.AppendLine(" order by   ORDERBY                                     ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //태그구분 조회
        public DataTable SelectComplexTagTypeList(OracleDBManager dbManager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select distinct code, code_name, orderby from                                                               ");
            queryString.AppendLine("(select     case when code in('TT', 'TD') then 'TD'                                                         ");
            queryString.AppendLine("                 when code in('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_O', 'SPL_I', 'YD', 'YD_R','YD_D') then 'FRQ'         ");
            queryString.AppendLine("                 when code in('FRI', 'RT', 'MNF', 'JU_O') then 'FRI'                                                      ");
            queryString.AppendLine("                 when code in('POI_OU','POI_IN') then 'POI'                                                               ");
            queryString.AppendLine("            else code                                                                                                ");
            queryString.AppendLine("            end  as code,                                                                                               ");
            queryString.AppendLine("            case when code in ('TT', 'TD') then '순시적산'                                                              ");
            queryString.AppendLine("                 when code in ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_O', 'SPL_I', 'YD', 'YD_R','YD_D') then '적산차유량'     ");
            queryString.AppendLine("                 when code in ('FRI', 'RT', 'MNF', 'JU_O') then '순시유량'                                                ");
            queryString.AppendLine("                 when code in('POI_OU','POI_IN') then '밸브개도'                                                          ");
            queryString.AppendLine("                 else code_name                                                                                           ");
            queryString.AppendLine("            end as code_name                                                                                            ");
            queryString.AppendLine("            , orderby                                                                                                   ");
            queryString.AppendLine("from      CM_CODE                                                                                           ");
            queryString.AppendLine("where      PCODE   =   '7010'                                                                               ");
            queryString.AppendLine(")                                                                                                           ");
            queryString.AppendLine("order by to_number(orderby)                                                                                 ");
            

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //태그리스트 조회
        public DataTable SelectComplexTagList(OracleDBManager dbManager, string selectString, Hashtable selectConditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select distinct a.tagname,                                                                                                  ");
            queryString.AppendLine("                a.tag_desc as DESCRIPTION,                                                                                                 ");
            queryString.AppendLine("                b.orderby,                                                                                                  ");
            queryString.AppendLine("                case when a.tag_gbn in ('TT', 'TD') then 'TD'                                                               ");
            queryString.AppendLine("                     when a.tag_gbn in ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_O', 'SPL_I', 'YD', 'YD_R') then 'FRQ'        ");
            queryString.AppendLine("                     when a.tag_gbn in ('FRI', 'RT', 'MNF', 'JU_O') then 'FRI'                                              ");
            queryString.AppendLine("                     when a.tag_gbn in('POI_OU','POI_IN') then 'POI'                                                               ");
            queryString.AppendLine("                    else b.code                                                                                             ");
            queryString.AppendLine("                end as code,                                                                                                ");
            queryString.AppendLine("                case when a.tag_gbn in ('TT', 'TD') then '순시적산'                                                         ");
            queryString.AppendLine("                     when a.tag_gbn in ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_O', 'SPL_I', 'YD', 'YD_R') then '적산차유량'     ");
            queryString.AppendLine("                     when a.tag_gbn in ('FRI', 'RT', 'MNF', 'JU_O') then '순시유량'                                         ");
            queryString.AppendLine("                     when code in('POI_OU','POI_IN') then '밸브개도'                                                          ");
            queryString.AppendLine("                    else b.code_name                                                                                       ");
            queryString.AppendLine("                end as code_name                                                                                            ");
            queryString.AppendLine("from if_tag_gbn a,                                                                                                          ");
            queryString.AppendLine("     cm_code b,                                                                                                             ");
            queryString.AppendLine("     if_ihtags c                                                                                                            ");
            queryString.AppendLine("where a.tag_gbn = b.code                                                                                                    ");
            queryString.AppendLine("and   a.tagname = c.tagname                                                                                                 ");
            queryString.AppendLine("and   b.pcode = '7010'                                                                                                      ");
            queryString.AppendLine("and   c.use_yn = 'Y'                                                                                                        ");
            if (!selectString.Equals(""))
            {
                queryString.AppendLine("   and      a.tag_gbn    in  (" + selectString + ")                                          ");
            }
            if (!selectConditions["TAGNAME"].ToString().Equals(""))
            {
                queryString.AppendLine("   and      a.TAGNAME     like   '" + selectConditions["TAGNAME"].ToString() + "%'");
            }
            if (!selectConditions["DESCRIPTION"].ToString().Equals(""))
            {
                queryString.AppendLine("   and      DESCRIPTION     like   '%" + selectConditions["DESCRIPTION"].ToString() + "%'");
            }
            queryString.AppendLine("order by to_number(b.orderby), a.tagname                                                                                    ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //선택태그 상세 조회
        public DataTable SelectComplexTagListDetail(OracleDBManager dbManager, ArrayList tagList, Hashtable tagConditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("  with TIMEDATA as (                                                                                                                                                                            ");
            queryString.AppendLine("select to_date('" + tagConditions["StartDate"].ToString() + "', 'yyyymmddhh24mi') + ((1/24/60) * (rownum -1)) as TIMESTAMP                                                                       ");
            queryString.AppendLine("  from dual         ");
            queryString.AppendLine("connect by rownum <= ((to_date('" + tagConditions["EndDate"].ToString() + "', 'yyyymmddhh24mi') - to_date('" + tagConditions["StartDate"].ToString() + "', 'yyyymmddhh24mi')) *24*60) + 1)       ");
            queryString.AppendLine(" select    to_char(a.TIMESTAMP, 'yyyy-mm-dd hh24:mi') as TIMESTAMP                                                                                                                              ");

            string tableName = string.Empty;

            foreach (string tagsn in tagList)
            {
                Hashtable tagCondition = tagConditions[tagsn] as Hashtable;

                if (tagCondition["CODE_NAME"].ToString().Equals("적산차유량") || tagCondition["CODE_NAME"].ToString().Equals("전력량"))
                {
                    tableName = "IF_ACCUMULATION_HOUR b";
                }
                else
                {
                    tableName = "IF_GATHER_REALTIME b";
                }

                queryString.AppendLine("           ,(select to_number(b.value) from " + tableName + " where b.TAGNAME = '" + tagsn + "' and b.TIMESTAMP = a.TIMESTAMP)    as \"" + tagsn + "\"  ");
            }    

            queryString.AppendLine("  from TIMEDATA a                                                                                                                                           ");

            if (!tagConditions["SetTime"].ToString().Equals("1"))
            {
                switch (tagConditions["SetTime"].ToString())
                {
                    case "10":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') in ('00','10','20','30','40','50')");
                        break;

                    case "15":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') in ('00','15','30','45')");
                        break;

                    case "30":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') in ('00','30')");
                        break;

                    case "00":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') = '" + tagConditions["SetTime"].ToString() + "'");
                        break;
                }
            }

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        #endregion 외부인터페이스

    }
}
