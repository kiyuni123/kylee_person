﻿/**************************************************************************
 * 파일명   : FavoritesTagDao.cs
 * 작성자   : shpark
 * 작성일자 : 2015.11.26
 * 설명     : 태그즐겨찾기 Dao
 * 변경이력 : 2015.11.26 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

namespace WaterNet.WD_ComplexDataAnalysis.dao
{
    class FavoritesTagDao
    {

        #region 1. 전역변수

        private static FavoritesTagDao dao = null;

        #endregion 전역변수


        #region 2. 초기화

        private FavoritesTagDao()
        { 
        }

        public static FavoritesTagDao GetInstance()
        {
            if (dao == null)
            {
                dao = new FavoritesTagDao();
            }

            return dao;
        }

        #endregion 초기화


        #region 3. 외부인터페이스

        //관리단 조회
        public DataTable SelectMgdpcd(OracleDBManager dbManager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      MGDPCD                                      ");
            queryString.AppendLine("            ,MGDPNM                                     ");
            queryString.AppendLine("  from      CM_DEPARTMENT                               ");
            queryString.AppendLine(" where      USEYN   =   'Y'                             ");
            queryString.AppendLine(" order by   ORDERBY                                     ");

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //즐겨찾기 사용기능 구분 조회
        public DataTable SelectFavoritesCode(OracleDBManager dbManager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      CODE                                        ");
            queryString.AppendLine("            ,CODE_NAME                                  ");
            queryString.AppendLine("  from      CM_CODE                                     ");
            queryString.AppendLine(" where      PCODE   =   'FVR'                           ");
            queryString.AppendLine("   and      USE_YN   =   'Y'                            ");
            queryString.AppendLine(" order by   ORDERBY                                     ");
            
            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        //즐겨찾기 제목 중복 조회
        public DataTable ExistFavorites(OracleDBManager dbManager, Hashtable favoritesData)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      FAVORITES_NAME                              ");
            queryString.AppendLine("  from      CM_FAVORITES_TITLE                          ");
            queryString.AppendLine(" where      FAVORITES_NAME      =   :1                  ");
            
            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = favoritesData["FAVORITES_NAME"].ToString();

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //즐겨찾기 제목 저장
        public void SaveFavoritesTitle(OracleDBManager dbManager, Hashtable favoritesData)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into     CM_FAVORITES_TITLE  (                               ");
            queryString.AppendLine("                                        FAVORITES_ID                ");
            queryString.AppendLine("                                        ,FAVORITES_NAME             ");
            queryString.AppendLine("                                        ,FAVORITES_CODE             ");
            //queryString.AppendLine("                                        ,MGDPCD                     ");
            queryString.AppendLine("                                        ,USER_ID                    ");
            queryString.AppendLine("                                        ,USER_NAME                  ");
            queryString.AppendLine("                                        ,INS_DATE                   ");
            queryString.AppendLine("                                    )                               ");
            queryString.AppendLine("values                                                              ");
            queryString.AppendLine("                                    (                               ");
            queryString.AppendLine("                                        :1                          ");
            queryString.AppendLine("                                        ,:2                         ");
            queryString.AppendLine("                                        ,:3                         ");
            queryString.AppendLine("                                        ,:4                         ");
            queryString.AppendLine("                                        ,:5                         ");            
            queryString.AppendLine("                                        ,sysdate                    ");
            queryString.AppendLine("                                    )                               ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                //,new OracleParameter("6", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)favoritesData["FAVORITES_ID"];
            parameters[1].Value = (string)favoritesData["FAVORITES_NAME"];
            parameters[2].Value = (string)favoritesData["FAVORITES_CODE"];
            //parameters[3].Value = (string)favoritesData["MGDPCD"];
            parameters[3].Value = EMFrame.statics.AppStatic.USER_ID;
            parameters[4].Value = EMFrame.statics.AppStatic.USER_NAME;

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //즐겨찾기 제목 수정
        public void UpdateFavoritesTitle(OracleDBManager dbManager, Hashtable favoritesData)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update      CM_FAVORITES_TITLE                                      ");
            queryString.AppendLine("   set      FAVORITES_NAME      =   :FAVORITES_NAME                 ");
            queryString.AppendLine(" where      FAVORITES_ID        =   :FAVORITES_ID                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)

            };

            parameters[0].Value = (string)favoritesData["FAVORITES_NAME"];
            parameters[1].Value = (string)favoritesData["FAVORITES_ID"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //즐겨찾기 태그리스트 저장
        public void SaveFavoritesTag(OracleDBManager dbManager, Hashtable favoritesData)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into     CM_FAVORITES_TAG    (                               ");
            queryString.AppendLine("                                        FAVORITES_ID                ");
            queryString.AppendLine("                                        ,TAGNAME                    ");
            queryString.AppendLine("                                        ,CHART_TYPE                 ");
            queryString.AppendLine("                                        ,COLOR_NAME                 ");
            queryString.AppendLine("                                        ,ORDERBY                    ");
            queryString.AppendLine("                                    )                               ");
            queryString.AppendLine("values                                                              ");
            queryString.AppendLine("                                    (                               ");
            queryString.AppendLine("                                        :1                          ");
            queryString.AppendLine("                                        ,:2                         ");
            queryString.AppendLine("                                        ,:3                         ");
            queryString.AppendLine("                                        ,:4                         ");
            queryString.AppendLine("                                        ,:5                         ");
            queryString.AppendLine("                                    )                               ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)favoritesData["FAVORITES_ID"];
            parameters[1].Value = (string)favoritesData["TAGNAME"];
            parameters[2].Value = (string)favoritesData["CHART_TYPE"];
            parameters[3].Value = (string)favoritesData["COLOR_NAME"];
            parameters[4].Value = favoritesData["ORDERBY"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //즐겨찾기 태그리스트 수정
        public void UpdateFavoritesTag(OracleDBManager dbManager, Hashtable favoritesData)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update      CM_FAVORITES_TAG                                    ");
            queryString.AppendLine("   set      CHART_TYPE      =   :CHART_TYPE                     ");
            queryString.AppendLine("            ,COLOR_NAME     =   :COLOR_NAME                     ");
            queryString.AppendLine("            ,ORDERBY        =   :ORDERBY                        ");
            queryString.AppendLine(" where      FAVORITES_ID    =   :FAVORITES_ID                   ");
            queryString.AppendLine("   and      TAGNAME         =   :TAGNAME                        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)favoritesData["CHART_TYPE"];
            parameters[1].Value = (string)favoritesData["COLOR_NAME"];
            parameters[2].Value = favoritesData["ORDERBY"];
            parameters[3].Value = (string)favoritesData["FAVORITES_ID"];
            parameters[4].Value = (string)favoritesData["TAGNAME"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //즐겨찾기 타이틀 목록 조회
        public DataTable SelectFavoritesTitle(OracleDBManager dbManager, Hashtable selectConditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.FAVORITES_ID                                                                          ");
            queryString.AppendLine("            ,a.FAVORITES_NAME                                                                       ");
            //queryString.AppendLine("            ,decode(b.MGDPNM, null, '전체', b.MGDPNM) MGDPNM                                        ");
            queryString.AppendLine("            ,c.CODE_NAME                                                                            ");
            queryString.AppendLine("            ,a.USER_ID                                                                              ");
            queryString.AppendLine("            ,a.USER_NAME                                                                            ");
            queryString.AppendLine("            ,a.INS_DATE                                                                             ");
            queryString.AppendLine("  from      CM_FAVORITES_TITLE  a                                                                   ");
            //queryString.AppendLine("            ,CM_DEPARTMENT  b                                                                       ");
            queryString.AppendLine("            ,CM_CODE c                                                                              ");
            queryString.AppendLine(" where      a.FAVORITES_CODE    =   nvl(:FAVORITES_CODE, a.FAVORITES_CODE)                          ");
            queryString.AppendLine("   and      c.CODE    =   a.FAVORITES_CODE                         ");

            //if (!((string)selectConditions["MGDPCD"]).Equals(string.Empty))
            //{
            //    queryString.AppendLine("   and      a.MGDPCD    =   '" + (string)selectConditions["MGDPCD"] +                         "'");
            //}

            if (!((string)selectConditions["FAVORITES_NAME"]).Equals(string.Empty))
            {
                queryString.AppendLine("   and      a.FAVORITES_NAME    like   '%" + (string)selectConditions["FAVORITES_NAME"] +    "%'");
            }

            //queryString.AppendLine("   and      a.MGDPCD   =   b.MGDPCD(+)                                                             ");
            queryString.AppendLine(" order by   c.CODE_NAME asc, a.USER_NAME asc, a.INS_DATE desc                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("FAVORITES_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)selectConditions["FAVORITES_CODE"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //즐겨찾기 태그 목록 조회
        public DataTable SelectFavoritesTag(OracleDBManager dbManager, string selectCondition)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select distinct * from                                                                                                                  ");
            queryString.AppendLine("(select                                                                                                                                 ");
            queryString.AppendLine("                        case when c.code in('TT', 'TD') then 'TD'                                                                       ");
            queryString.AppendLine("                            when c.code in('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_O', 'SPL_I', 'YD', 'YD_R','YD_D') then 'FRQ'          ");
            queryString.AppendLine("                            when c.code in('FRI', 'RT', 'MNF', 'JU_O') then 'FRI'                                                       ");
            queryString.AppendLine("                            when c.code in('POI_OU','POI_IN') then 'POI'                                                                ");
            queryString.AppendLine("                        else code                                                                                                       ");
            queryString.AppendLine("                        end  as code,                                                                                                   ");
            queryString.AppendLine("                        case when code in ('TT', 'TD') then '순시적산'                                                                  ");
            queryString.AppendLine("                             when code in ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_O', 'SPL_I', 'YD', 'YD_R','YD_D') then '적산차'       ");
            queryString.AppendLine("                             when code in ('FRI', 'RT', 'MNF', 'JU_O') then '순시유량'                                                  ");
            queryString.AppendLine("                             when code in('POI_OU','POI_IN') then '밸브개도'                                                            ");
            queryString.AppendLine("                             else code_name                                                                                             ");
            queryString.AppendLine("                        end as code_name                                                                                                ");
            queryString.AppendLine("                        ,b.TAGNAME                                                                                                      ");
            queryString.AppendLine("                        ,b.DESCRIPTION                                                                                                  ");
            queryString.AppendLine("                        ,a.CHART_TYPE                                                                                                   ");
            queryString.AppendLine("                        ,a.COLOR_NAME                                                                                                   ");
            queryString.AppendLine("              from      CM_FAVORITES_TAG a                                                                                              ");
            queryString.AppendLine("                        ,IF_IHTAGS b                                                                                                    ");                                                                   
            queryString.AppendLine("                        ,CM_CODE c                                                                                                      ");
            queryString.AppendLine("                        ,IF_TAG_GBN d                                                                                                   ");
            queryString.AppendLine("             where      a.FAVORITES_ID      =   :1                                                                                      ");
            queryString.AppendLine("               and      a.TAGNAME           =   b.TAGNAME                                                                               ");
            queryString.AppendLine("               and      b.TAGNAME           =   d.TAGNAME                                                                               ");
            queryString.AppendLine("               and      c.PCODE             =   '7010'                                                                                  ");
            queryString.AppendLine("               and      d.TAG_GBN          =   c.CODE                                                                                   ");
            queryString.AppendLine("               order by   a.ORDERBY                                                                                                     ");
            queryString.AppendLine(")                                                                                                                                       ");
            
            //queryString.AppendLine("select      c.CODE                                     ");
            //queryString.AppendLine("            ,c.CODE_NAME                                ");
            //queryString.AppendLine("            ,b.TAGNAME                                  ");
            //queryString.AppendLine("            ,b.DESCRIPTION                              ");
            //queryString.AppendLine("            ,a.CHART_TYPE                               ");
            //queryString.AppendLine("            ,a.COLOR_NAME                               ");
            //queryString.AppendLine("  from      CM_FAVORITES_TAG a                          ");
            //queryString.AppendLine("            ,IF_IHTAGS b                                ");
            //queryString.AppendLine("            ,CM_CODE c                                  ");
            //queryString.AppendLine("            ,IF_TAG_GBN d                               ");            
            //queryString.AppendLine(" where      a.FAVORITES_ID      =   :1                  ");
            //queryString.AppendLine("   and      a.TAGNAME           =   b.TAGNAME           ");
            //queryString.AppendLine("   and      b.TAGNAME           =   d.TAGNAME           ");            
            //queryString.AppendLine("   and      c.PCODE             =   '7010'              ");
            //queryString.AppendLine("   and      d.TAG_GBN          =   c.CODE              ");
            //queryString.AppendLine(" order by   a.ORDERBY                                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = selectCondition;

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //즐겨찾기 태그 타이틀 삭제
        public void DeleteFavoritesTitle(OracleDBManager dbManager, string deleteCondition)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from CM_FAVORITES_TITLE                          ");
            queryString.AppendLine(" where      FAVORITES_ID    =   :1                      ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = deleteCondition;

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //즐겨찾기 태그 목록 삭제
        public void DeleteFavoritesTag(OracleDBManager dbManager, string deleteCondition)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from CM_FAVORITES_TAG                            ");
            queryString.AppendLine(" where      FAVORITES_ID    =   :1                      ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = deleteCondition;

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //즐겨찾기 태그 상세 조회
        public DataTable SelectComplexTagListDetail(OracleDBManager dbManager, ArrayList tagList, Hashtable tagConditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("  with TIMEDATA as (                                                                                                                                                                            ");
            queryString.AppendLine("select to_date('" + tagConditions["StartDate"].ToString() + "', 'yyyymmddhh24mi') + ((1/24/60) * (rownum-1)) as TIMESTAMP                                                                       ");
            queryString.AppendLine("  from dual         ");
            queryString.AppendLine("connect by rownum <= ((to_date('" + tagConditions["EndDate"].ToString() + "', 'yyyymmddhh24mi') - to_date('" + tagConditions["StartDate"].ToString() + "', 'yyyymmddhh24mi')) *24*60) + 1)       ");
            queryString.AppendLine(" select    to_char(a.TIMESTAMP, 'yyyy-mm-dd hh24:mi') as TIMESTAMP                                                                                                                              ");

            string tableName = string.Empty;

            foreach (string tagsn in tagList)
            {
                Hashtable tagCondition = tagConditions[tagsn] as Hashtable;

                if (tagCondition["CODE_NAME"].ToString().Equals("적산차유량") || tagCondition["CODE_NAME"].ToString().Equals("전력량"))
                {
                    tableName = "IF_ACCUMULATION_HOUR b";
                }
                else
                {
                    tableName = "IF_GATHER_REALTIME b";
                }

                queryString.AppendLine("           ,(select to_number(b.value) from " + tableName + " where b.TAGNAME = '" + tagsn + "' and b.TIMESTAMP = a.TIMESTAMP)    as \"" + tagsn + "\"  ");
            }

            queryString.AppendLine("  from TIMEDATA a                                                                                                                                           ");

            if (!tagConditions["SetTime"].ToString().Equals("1"))
            {
                switch (tagConditions["SetTime"].ToString())
                {
                    case "10":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') in ('00','10','20','30','40','50')");
                        break;

                    case "15":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') in ('00','15','30','45')");
                        break;

                    case "30":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') in ('00','30')");
                        break;

                    case "00":
                        queryString.AppendLine("   where      to_char(a.TIMESTAMP, 'mi') = '" + tagConditions["SetTime"].ToString() + "'");
                        break;
                }
            }

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), null);
        }

        #endregion 외부인터페이스
    }
}
