﻿/**************************************************************************
 * 파일명   : FavoritesTagWork.cs
 * 작성자   : shpark
 * 작성일자 : 2015.11.26
 * 설명     : 태그즐겨찾기 work
 * 변경이력 : 2015.11.26 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using WaterNet.WD_Common.utils;
using WaterNet.WD_ComplexDataAnalysis.dao;
using WaterNet.WaterNetCore;

namespace WaterNet.WD_ComplexDataAnalysis.work
{
    class FavoritesTagWork
    {

        #region 1. 전역변수

        private static FavoritesTagWork work = null;
        private FavoritesTagDao dao = null;

        #endregion 전역변수


        #region 2. 초기화

        private FavoritesTagWork()
        {
            dao = FavoritesTagDao.GetInstance();
        }

        public static FavoritesTagWork GetInstance()
        {
            if (work == null)
            {
                work = new FavoritesTagWork();
            }

            return work;
        }

        #endregion 초기화


        #region 3. 외부인터페이스


        //관리단 조회
        public DataTable SelectMgdpcd()
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMgdpcd(dbManager);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //즐겨찾기 사용기능 구분 조회
        public DataTable SelectFavoritesCode()
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectFavoritesCode(dbManager);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //즐겨찾기 저장
        public void SaveFavorites(Hashtable favoritesData)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                if (dao.ExistFavorites(dbManager, favoritesData).Rows.Count != 0)
                {
                    throw new Exception("이미 등록되어 있는 즐겨찾기명 입니다.");
                }

                favoritesData.Add("FAVORITES_ID", CommonUtils.GetSerialNumber("FT"));

                dao.SaveFavoritesTitle(dbManager, favoritesData);

                DataTable tagsDT = favoritesData["tagsDT"] as DataTable;
                favoritesData.Add("TAGNAME", "");
                favoritesData.Add("ORDERBY", "");
                favoritesData.Add("CHART_TYPE", "");
                favoritesData.Add("COLOR_NAME", "");

                for (int i = 0; i < tagsDT.Rows.Count; i++)
                {
                    favoritesData["TAGNAME"] = tagsDT.Rows[i]["TAGNAME"].ToString();
                    favoritesData["CHART_TYPE"] = tagsDT.Rows[i]["CHART_TYPE"].ToString();
                    favoritesData["COLOR_NAME"] = tagsDT.Rows[i]["COLOR_NAME"].ToString();
                    favoritesData["ORDERBY"] = i + 1;

                    dao.SaveFavoritesTag(dbManager, favoritesData);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //즐겨찾기 수정
        public void UpdateFavorites(Hashtable favoritesData)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                //if (dao.ExistFavorites(dbManager, favoritesData).Rows.Count != 0)
                //{
                //    throw new Exception("이미 등록되어 있는 즐겨찾기명 입니다.");
                //}

                dao.UpdateFavoritesTitle(dbManager, favoritesData);

                DataTable tagsDT = favoritesData["tagsDT"] as DataTable;
                favoritesData.Add("TAGNAME", "");
                favoritesData.Add("ORDERBY", "");
                favoritesData.Add("CHART_TYPE", "");
                favoritesData.Add("COLOR_NAME", "");

                for (int i = 0; i < tagsDT.Rows.Count; i++)
                {
                    favoritesData["TAGNAME"] = tagsDT.Rows[i]["TAGNAME"].ToString();
                    favoritesData["CHART_TYPE"] = tagsDT.Rows[i]["CHART_TYPE"].ToString();
                    favoritesData["COLOR_NAME"] = tagsDT.Rows[i]["COLOR_NAME"].ToString();
                    favoritesData["ORDERBY"] = i + 1;

                    dao.UpdateFavoritesTag(dbManager, favoritesData);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //즐겨찾기 타이틀 목록 조회
        public DataTable SelectFavoritesTitle(Hashtable selectConditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectFavoritesTitle(dbManager, selectConditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //즐겨찾기 태그 목록 조회
        public DataTable SelectFavoritesTag(string selectCondition)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectFavoritesTag(dbManager, selectCondition);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //즐겨찾기 삭제
        public void DeleteFavorites(string deleteCondition)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeleteFavoritesTag(dbManager, deleteCondition);
                dao.DeleteFavoritesTitle(dbManager, deleteCondition);
                
                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }
        }

        //선택태그 상세 조회
        public DataTable SelectComplexTagListDetail(ArrayList tagList, Hashtable tagConditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectComplexTagListDetail(dbManager, tagList, tagConditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        #endregion 외부인터페이스

    }
}
