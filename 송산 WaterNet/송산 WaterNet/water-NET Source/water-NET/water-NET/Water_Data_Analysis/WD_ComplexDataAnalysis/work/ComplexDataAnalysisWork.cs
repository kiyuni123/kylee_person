﻿/**************************************************************************
 * 파일명   : ComplexDataAnalysisWork.cs
 * 작성자   : shpark
 * 작성일자 : 2015.10.29
 * 설명     : 복합데이터시계열분석 Work
 * 변경이력 : 2015.10.29 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;

using WaterNet.WaterNetCore;
using WaterNet.WD_ComplexDataAnalysis.dao;

namespace WaterNet.WD_ComplexDataAnalysis.work
{
    class ComplexDataAnalysisWork
    {

        #region 1. 전역변수

        private static ComplexDataAnalysisWork work = null;
        private ComplexDataAnalysisDao dao = null;

        #endregion 전역변수


        #region 2. 초기화

        private ComplexDataAnalysisWork()
        {
            dao = ComplexDataAnalysisDao.GetInstance();
        }

        public static ComplexDataAnalysisWork GetInstance()
        {
            if (work == null)
            {
                work = new ComplexDataAnalysisWork();
            }

            return work;
        }

        #endregion 초기화


        #region 3. 외부인터페이스

        //관리단 조회
        public DataTable SelectMgdpcd()
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMgdpcd(dbManager);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //태그구분 조회
        public DataTable SelectComplexTagTypeList()
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectComplexTagTypeList(dbManager);
                result.Columns.Remove("ORDERBY");                
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //태그리스트 조회
        public DataTable SelectComplexTagList(Hashtable typeConditions, Hashtable selectConditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                string selectTagGbn = string.Empty;   //쿼리의 태그구분 "TAG_GBN in ( )" 에 들어갈 문자열 할당

                if (typeConditions.Count > 0)
                {                    
                    foreach (string key in typeConditions.Keys)
                    {
                        selectTagGbn += "'" + key + "'" + ",";      
                    }

                    selectTagGbn = selectTagGbn.Substring(0, selectTagGbn.LastIndexOf(','));
                }

                result = dao.SelectComplexTagList(dbManager, selectTagGbn, selectConditions);
                result.Columns.Remove("ORDERBY");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //선택태그 상세 조회
        public DataTable SelectComplexTagListDetail(ArrayList tagList, Hashtable tagConditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectComplexTagListDetail(dbManager, tagList, tagConditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        #endregion 외부인터페이스

    }
}
