﻿/**************************************************************************
 * 파일명   : frmFavoritesTagLoad.cs
 * 작성자   : shpark
 * 작성일자 : 2015.11.26
 * 설명     : 태그즐겨찾기 메인 폼
 * 변경이력 : 2015.11.26 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Media;

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using WaterNet.WD_Common.utils;
using WaterNet.WD_ComplexDataAnalysis.work;
using WaterNet.WD_ComplexDataAnalysis.form;
using EMFrame.log;

namespace WaterNet.WD_ComplexDataAnalysis.form
{
    public partial class frmFavoritesTagLoad : Form
    {

        #region 1. 전역변수

        private FavoritesTagWork work = null;
        private Point p;    //컬러 파레트 생성 좌표 설정을 위한 전역변수

        #endregion 전역변수

        
        #region 2. 초기화

        public frmFavoritesTagLoad()
        {
            work = FavoritesTagWork.GetInstance();
            InitializeComponent();
            InitializeComboBox();
            InitializeUltraGrid();
        }

        //콤보박스 초기화
        private void InitializeComboBox()
        {
            ////관리단 콤보박스 초기화
            //comMgdpcd.DataSource = work.SelectMgdpcd();
            //ComboBoxUtils.SetMember(comMgdpcd, "MGDPCD", "MGDPNM");
            //ComboBoxUtils.AddDefaultOption(comMgdpcd, false);

            //사용기능 콤보박스 초기화
            comFavoritesCode.DataSource = work.SelectFavoritesCode();
            ComboBoxUtils.SetMember(comFavoritesCode, "CODE", "CODE_NAME");
            ComboBoxUtils.AddDefaultOption(comFavoritesCode, false);
            comFavoritesCode.SelectedIndex = 0;
        }

        //그리드 초기화
        private void InitializeUltraGrid()
        {
            UltraGridColumn favoritesTitleColumn;        //즐겨찾기 타이틀 그리드

            favoritesTitleColumn = griFavoritesTitle.DisplayLayout.Bands[0].Columns.Add();
            favoritesTitleColumn.Key = "FAVORITES_ID";
            favoritesTitleColumn.Header.Caption = "즐겨찾기 ID";
            favoritesTitleColumn.CellActivation = Activation.NoEdit;
            favoritesTitleColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTitleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTitleColumn.CellAppearance.TextHAlign = HAlign.Left;
            favoritesTitleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTitleColumn.Width = 100;
            favoritesTitleColumn.Hidden = true;

            favoritesTitleColumn = griFavoritesTitle.DisplayLayout.Bands[0].Columns.Add();
            favoritesTitleColumn.Key = "FAVORITES_NAME";
            favoritesTitleColumn.Header.Caption = "즐겨찾기명";
            favoritesTitleColumn.CellActivation = Activation.NoEdit;
            favoritesTitleColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTitleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTitleColumn.CellAppearance.TextHAlign = HAlign.Left;
            favoritesTitleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTitleColumn.Width = 269;

            //favoritesTitleColumn = griFavoritesTitle.DisplayLayout.Bands[0].Columns.Add();
            //favoritesTitleColumn.Key = "MGDPNM";
            //favoritesTitleColumn.Header.Caption = "관리단";
            //favoritesTitleColumn.CellActivation = Activation.NoEdit;
            //favoritesTitleColumn.CellClickAction = CellClickAction.RowSelect;
            //favoritesTitleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //favoritesTitleColumn.CellAppearance.TextHAlign = HAlign.Center;
            //favoritesTitleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //favoritesTitleColumn.Width = 100;

            favoritesTitleColumn = griFavoritesTitle.DisplayLayout.Bands[0].Columns.Add();
            favoritesTitleColumn.Key = "CODE_NAME";
            favoritesTitleColumn.Header.Caption = "사용기능";
            favoritesTitleColumn.CellActivation = Activation.NoEdit;
            favoritesTitleColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTitleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTitleColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTitleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTitleColumn.Width = 120;

            favoritesTitleColumn = griFavoritesTitle.DisplayLayout.Bands[0].Columns.Add();
            favoritesTitleColumn.Key = "USER_ID";
            favoritesTitleColumn.Header.Caption = "등록자";
            favoritesTitleColumn.CellActivation = Activation.NoEdit;
            favoritesTitleColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTitleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTitleColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTitleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTitleColumn.Width = 60;
            favoritesTitleColumn.Hidden = true;

            favoritesTitleColumn = griFavoritesTitle.DisplayLayout.Bands[0].Columns.Add();
            favoritesTitleColumn.Key = "USER_NAME";
            favoritesTitleColumn.Header.Caption = "등록자";
            favoritesTitleColumn.CellActivation = Activation.NoEdit;
            favoritesTitleColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTitleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTitleColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTitleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTitleColumn.Width = 60;

            favoritesTitleColumn = griFavoritesTitle.DisplayLayout.Bands[0].Columns.Add();
            favoritesTitleColumn.Key = "INS_DATE";
            favoritesTitleColumn.Header.Caption = "등록일";
            favoritesTitleColumn.CellActivation = Activation.NoEdit;
            favoritesTitleColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTitleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTitleColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTitleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTitleColumn.Width = 120;
            favoritesTitleColumn.MaskInput = "yyyy-MM-dd HH:mm";
            favoritesTitleColumn.Format = "yyyy-MM-dd HH:mm";

            ValueList seriesList = new ValueList();     //차트 시리즈 타입 선택을 위한 ValueList
            seriesList.ValueListItems.Add("선형");
            seriesList.ValueListItems.Add("영역형");
            seriesList.ValueListItems.Add("막대형");

            UltraGridColumn favoritesTagColumn;        //즐겨찾기 태그 그리드

            //favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            //favoritesTagColumn.Key = "MGDPNM";
            //favoritesTagColumn.Header.Caption = "관리단";
            //favoritesTagColumn.CellActivation = Activation.NoEdit;
            //favoritesTagColumn.CellClickAction = CellClickAction.RowSelect;
            //favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            //favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //favoritesTagColumn.Width = 100;

            favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            favoritesTagColumn.Key = "CODE_NAME";
            favoritesTagColumn.Header.Caption = "구분";
            favoritesTagColumn.CellActivation = Activation.NoEdit;
            favoritesTagColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTagColumn.Width = 90;

            favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            favoritesTagColumn.Key = "CODE";
            favoritesTagColumn.Header.Caption = "코드";
            favoritesTagColumn.CellActivation = Activation.NoEdit;
            favoritesTagColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTagColumn.Width = 80;
            favoritesTagColumn.Hidden = true;

            favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            favoritesTagColumn.Key = "TAGNAME";
            favoritesTagColumn.Header.Caption = "TAGSN";
            favoritesTagColumn.CellActivation = Activation.NoEdit;
            favoritesTagColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTagColumn.Width = 80;

            favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            favoritesTagColumn.Key = "DESCRIPTION";
            favoritesTagColumn.Header.Caption = "TAG DESCRIPTION";
            favoritesTagColumn.CellActivation = Activation.NoEdit;
            favoritesTagColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Left;
            favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTagColumn.Width = 271;

            favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            favoritesTagColumn.Key = "CHART_TYPE";
            favoritesTagColumn.Header.Caption = "차트 모양";
            favoritesTagColumn.CellActivation = Activation.AllowEdit;
            favoritesTagColumn.CellClickAction = CellClickAction.CellSelect;
            favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTagColumn.ValueList = seriesList;
            favoritesTagColumn.Width = 75;

            favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            favoritesTagColumn.Key = "CHART_COLOR";
            favoritesTagColumn.Header.Caption = "차트 색상";
            favoritesTagColumn.CellActivation = Activation.AllowEdit;
            favoritesTagColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTagColumn.Width = 70;

            favoritesTagColumn = griFavoritesTag.DisplayLayout.Bands[0].Columns.Add();
            favoritesTagColumn.Key = "COLOR_NAME";
            favoritesTagColumn.Header.Caption = "색상 이름";
            favoritesTagColumn.CellActivation = Activation.NoEdit;
            favoritesTagColumn.CellClickAction = CellClickAction.RowSelect;
            favoritesTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            favoritesTagColumn.CellAppearance.TextHAlign = HAlign.Left;
            favoritesTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            favoritesTagColumn.Width = 70;
            favoritesTagColumn.Hidden = true;
        }
        
        #endregion 초기화
        

        #region 3. 객체 이벤트

        //폼 로딩 이벤트
        private void frmFavoritesTagLoad_Load(object sender, EventArgs e)
        {
            SelectFavoritesList();      //폼 로딩시 즐겨찾기 조회
        }

        //트렌드보기 버튼 클릭 이벤트
        private void butApplyFavorites_Click(object sender, EventArgs e)
        {
            if (griFavoritesTitle.Rows.Count == 0)
            {
                CommonUtils.ShowInformationMessage("저장된 태그 즐겨찾기가 없습니다.\n즐겨찾기를 등록하세요."); return;
            }

            ArrayList arrTagList = new ArrayList();         //즐겨찾기 순서대로 태그를 조회하기 위한 배열
            Hashtable tagConditions = new Hashtable();      //팝업으로 tag description 을 보여주기위한 해쉬테이블

            foreach (UltraGridRow row in griFavoritesTag.Rows)
            {
                Hashtable tagCondition = new Hashtable();

                arrTagList.Add(row.GetCellValue("TAGNAME").ToString());

                tagCondition.Add("TAGNAME", row.GetCellValue("TAGNAME").ToString());
                tagCondition.Add("DESCRIPTION", row.GetCellValue("DESCRIPTION").ToString());
                tagCondition.Add("CODE_NAME", row.GetCellValue("CODE_NAME").ToString());
                tagCondition.Add("CODE", row.GetCellValue("CODE").ToString());
                tagCondition.Add("CHART_TYPE", row.GetCellValue("CHART_TYPE").ToString());
                tagCondition.Add("CHART_COLOR", row.Cells["CHART_COLOR"].Appearance.BackColor);

                //차트에서 AxisY 텍스트를 표현하기위해 해당하는 값을 할당한다.
                if (row.GetCellValue("CODE").Equals("FRI"))
                {
                    tagCondition.Add("TYPE", "순시유량(㎥/h)");
                }
                else if (row.GetCellValue("CODE").Equals("PRI"))
                {
                    tagCondition.Add("TYPE", "압력(kgf/㎠)");
                }
                else if (row.GetCellValue("CODE").Equals("TD"))
                {
                    tagCondition.Add("TYPE", "적산유량(㎥/h)");
                }
                else if (row.GetCellValue("CODE").Equals("FRQ"))
                {
                    tagCondition.Add("TYPE", "적산차유량(㎥)");
                }
                else if (row.GetCellValue("CODE").Equals("LEI"))
                {
                    tagCondition.Add("TYPE", "수위(m)");
                }
                else if (row.GetCellValue("CODE").Equals("PMB"))
                {
                    tagCondition.Add("TYPE", "펌프가동(On/Off)");
                }
                else if (row.GetCellValue("CODE").Equals("CLI"))
                {
                    tagCondition.Add("TYPE", "잔류염소(CL)");
                }
                else if (row.GetCellValue("CODE").Equals("TBI"))
                {
                    tagCondition.Add("TYPE", "탁도(TB)");
                }
                else if (row.GetCellValue("CODE").Equals("PHI"))
                {
                    tagCondition.Add("TYPE", "PH값(pH)");
                }
                else if (row.GetCellValue("CODE").Equals("TEI"))
                {
                    tagCondition.Add("TYPE", "온도(TE)");
                }
                else if (row.GetCellValue("CODE").Equals("CUI"))
                {
                    tagCondition.Add("TYPE", "전기전도도(CU)");
                }
                else if (row.GetCellValue("CODE").Equals("POI"))
                {
                    tagCondition.Add("TYPE", "밸브개도(%)");
                }              

                tagConditions.Add(row.GetCellValue("TAGNAME").ToString(), tagCondition);
            }

            frmComplexDataAnalysisDetail detailForm = new frmComplexDataAnalysisDetail(arrTagList, tagConditions);
            detailForm.Owner = this;
            detailForm.StartPosition = FormStartPosition.CenterParent;
            detailForm.Show();
        }

        //조회 버튼 클릭 이벤트
        private void butSelectFavorites_Click(object sender, EventArgs e)
        {
            SelectFavoritesList();
        }

        //저장 버튼 클릭 이벤트
        private void butUpdateFavorites_Click(object sender, EventArgs e)
        {
            try
            {
                if (!EMFrame.statics.AppStatic.USER_RIGHT.Equals("3")
                && !EMFrame.statics.AppStatic.USER_ID.Equals(CommonUtils.GetGridCellValue(griFavoritesTitle, "USER_ID")))
                {
                    CommonUtils.ShowInformationMessage("등록자가 달라서 수정할 수 없습니다."); return;
                }

                if (texUpdateTitle.Text.Equals(string.Empty))
                {
                    CommonUtils.ShowInformationMessage("즐겨찾기명을 입력하세요."); return;
                }

                if (CommonUtils.ShowYesNoDialog("변경사항을 저장 하시겠습니까?") == DialogResult.Yes)
                {
                    DataTable tagsDT = CommonUtils.ConvertUltraGridToDataTable(griFavoritesTag);

                    Hashtable favoritesData = new Hashtable();

                    favoritesData.Add("FAVORITES_ID", CommonUtils.GetGridCellValue(griFavoritesTitle, "FAVORITES_ID"));
                    favoritesData.Add("FAVORITES_NAME", griFavoritesTitle.Selected.Rows[0].Cells["FAVORITES_NAME"].Value);
                    favoritesData.Add("tagsDT", tagsDT);

                    work.UpdateFavorites(favoritesData);

                    CommonUtils.ShowInformationMessage("변경사항이 저장되었습니다.");

                    //SelectFavoritesList();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString()); 
            }
        }

        //삭제 버튼 클릭 이벤트
        private void butDeleteFavorites_Click(object sender, EventArgs e)
        {
            try
            {
                if (!EMFrame.statics.AppStatic.USER_RIGHT.Equals("3")
                && !EMFrame.statics.AppStatic.USER_ID.Equals(CommonUtils.GetGridCellValue(griFavoritesTitle, "USER_ID")))
                {
                    CommonUtils.ShowInformationMessage("등록자가 달라서 삭제할 수 없습니다."); return;
                }

                if (CommonUtils.ShowYesNoDialog("선택하신 즐겨찾기를 삭제하시겠습니까?") == DialogResult.Yes)
                {
                    work.DeleteFavorites(CommonUtils.GetGridCellValue(griFavoritesTitle, "FAVORITES_ID"));

                    SelectFavoritesList();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());   
            }
        }

        ////관리단 콤보박스 선택 변경 이벤트
        //private void comMgdpcd_SelectionChangeCommitted(object sender, EventArgs e)
        //{
        //    SelectFavoritesList();
        //}

        //사용기능 콤보박스 선택 변경 이벤트
        private void comFavoritesCode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SelectFavoritesList();
        }

        //즐겨찾기명 텍스트박스 엔터키 입력 이벤트
        private void texFavoritesName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectFavoritesList();
            }
        }

        //즐겨찾기 타이틀 그리드 선택 변경 이벤트
        private void griFavoritesTitle_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                string selectCondition = string.Empty;

                if (griFavoritesTitle.Selected.Rows.Count == 1)
                {
                    selectCondition = (string)griFavoritesTitle.Selected.Rows[0].Cells["FAVORITES_ID"].Value;
                    texUpdateTitle.Text = (string)griFavoritesTitle.Selected.Rows[0].Cells["FAVORITES_NAME"].Value;
                }

                //즐겨찾기 타이틀 조회 후 해당 타이틀에 설정된 즐겨찾기 태그 목록 조회
                griFavoritesTag.DataSource = work.SelectFavoritesTag(selectCondition);
                
                SetChartSeriesType();
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //즐겨찾기 태그 그리드 셀버튼 클릭 이벤트
        private void griFavoritesTag_ClickCellButton(object sender, CellEventArgs e)
        {
            try
            {
                //컬러팔레트에서 선택한 색상을 그리드의 셀과 버튼 컬러로 설정한다.
                ColorPaletteDialog cpd = new ColorPaletteDialog(p.X, p.Y);

                if (cpd.ShowDialog() == DialogResult.OK)
                {
                    e.Cell.Row.Cells["CHART_COLOR"].Appearance.BackColor = cpd.Color;
                    e.Cell.Row.Cells["CHART_COLOR"].ButtonAppearance.BackColor = cpd.Color;
                    e.Cell.Row.Cells["CHART_COLOR"].Value = string.Empty;
                    e.Cell.Row.Cells["COLOR_NAME"].Value = ColorTranslator.ToHtml(cpd.Color);
                }
            }
            catch (Exception)
            {
            }
        }

        //클릭 한 셀의 좌표를 계산하기 위한 이벤트
        private void griFavoritesTag_MouseEnterElement(object sender, UIElementEventArgs e)
        {
            try
            {
                if (e.Element is CellUIElement)
                {
                    CellUIElement cElement = (CellUIElement)e.Element;
                    UltraGridCell cell = cElement.Cell;

                    if (cell.Column.Key.Equals("CHART_COLOR"))
                    {
                        CellUIElement element = (CellUIElement)cell.GetUIElement();
                        Rectangle rect = griFavoritesTag.RectangleToScreen(element.Rect);
                        p = new Point(rect.X, rect.Y);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        //즐겨찾기명 텍스트박스 텍스트 변경 이벤트(저장)
        private void texUpdateTitle_TextChanged(object sender, EventArgs e)
        {
            griFavoritesTitle.Selected.Rows[0].Cells["FAVORITES_NAME"].Value = texUpdateTitle.Text;
            griFavoritesTitle.UpdateData();
        }

        //조회 후 차트 컬러 설정
        private void griFavoritesTag_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (griFavoritesTag.Rows.Count != 0)
            {
                foreach (UltraGridRow row in griFavoritesTag.Rows)
                {
                    if (row.Cells["COLOR_NAME"].Value != DBNull.Value)
                    {
                        row.Cells["CHART_COLOR"].Appearance.BackColor = ColorTranslator.FromHtml(row.Cells["COLOR_NAME"].Value.ToString());
                    }
                    else
                    {
                        row.Cells["CHART_COLOR"].Value = "자동";
                    }
                }
            }
        }

        #region 마우스 드래그로 그리드 로우 이동

        private void griFavoritesTag_SelectionDrag(object sender, CancelEventArgs e)
        {
            griFavoritesTag.DoDragDrop(griFavoritesTag.Selected.Rows, DragDropEffects.Move);
        }

        private void griFavoritesTag_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
            UltraGrid afterGrid = sender as UltraGrid;
            Point pointInGridCoords = afterGrid.PointToClient(new Point(e.X, e.Y));
            if (pointInGridCoords.Y < 10)
            {
                this.griFavoritesTag.ActiveRowScrollRegion.Scroll(RowScrollAction.LineUp);
            }
            else if (pointInGridCoords.Y > afterGrid.Height - 10)
            {
                this.griFavoritesTag.ActiveRowScrollRegion.Scroll(RowScrollAction.PageDown);
            }
        }

        private void griFavoritesTag_DragDrop(object sender, DragEventArgs e)
        {
            int dropRowIndex;

            UIElement uieOver = griFavoritesTag.DisplayLayout.UIElement.ElementFromPoint(griFavoritesTag.PointToClient(new Point(e.X, e.Y)));
            UltraGridRow rowOver = uieOver.GetContext(typeof(UltraGridRow), true) as UltraGridRow;

            if (rowOver != null)
            {
                dropRowIndex = rowOver.Index;

                SelectedRowsCollection selRows = (SelectedRowsCollection)e.Data.GetData(typeof(SelectedRowsCollection)) as SelectedRowsCollection;

                foreach (UltraGridRow row in selRows)
                {
                    griFavoritesTag.Rows.Move(row, dropRowIndex);
                }

                griFavoritesTag.Rows[dropRowIndex].Activate();
                griFavoritesTag.Rows[dropRowIndex].Selected = true;
            }
        }

        #endregion 마우스 드래드로 그리드 로우 이동


        //폼 클로즈 이벤트
        private void frmFavoritesTagLoad_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        #endregion 객체 이벤트


        #region 4. 상속
        #endregion 상속
        
        
        #region 5. 외부사용
        #endregion 외부사용

        
        #region 6. 내부사용

        //즐겨찾기 타이틀 목록 조회
        private void SelectFavoritesList()
        {
            try
            {
                Hashtable selectConditions = new Hashtable();

                selectConditions.Add("FAVORITES_NAME", texFavoritesName.Text);
                selectConditions.Add("FAVORITES_CODE", comFavoritesCode.SelectedValue);
                //selectConditions.Add("MGDPCD", comMgdpcd.SelectedValue);

                griFavoritesTitle.DataSource = work.SelectFavoritesTitle(selectConditions);

                CommonUtils.SelectFirstRow(griFavoritesTitle);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //그리드에서 차트 시리즈의 타입을 설정한다.
        private void SetChartSeriesType()
        {
            foreach (UltraGridRow row in griFavoritesTag.Rows)
            {
                if (row.Cells["CHART_TYPE"].Value == DBNull.Value)
                {
                    if (((string)row.Cells["CODE"].Value).Equals("000004") || ((string)row.Cells["CODE"].Value).Equals("000007"))   //000004 - 적산차유량, 000007 - 전력량
                    {
                        row.Cells["CHART_TYPE"].Value = "막대형";
                    }
                    else
                    {
                        row.Cells["CHART_TYPE"].Value = "선형";
                    }                  
                }
            }
        }

        #endregion 내부사용     

    }
}
