﻿namespace WaterNet.WD_ComplexDataAnalysis.form
{
    partial class frmFavoritesTagLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butDeleteFavorites = new System.Windows.Forms.Button();
            this.comFavoritesCode = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.texFavoritesName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.butApplyFavorites = new System.Windows.Forms.Button();
            this.butSelectFavorites = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.griFavoritesTitle = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.texUpdateTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.butUpdateFavorites = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.griFavoritesTag = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griFavoritesTitle)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griFavoritesTag)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 651);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(694, 10);
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(704, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 651);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 651);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(714, 10);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butDeleteFavorites);
            this.groupBox1.Controls.Add(this.comFavoritesCode);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.texFavoritesName);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.butApplyFavorites);
            this.groupBox1.Controls.Add(this.butSelectFavorites);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(694, 90);
            this.groupBox1.TabIndex = 141;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butDeleteFavorites
            // 
            this.butDeleteFavorites.Font = new System.Drawing.Font("굴림", 9F);
            this.butDeleteFavorites.Location = new System.Drawing.Point(413, 53);
            this.butDeleteFavorites.Name = "butDeleteFavorites";
            this.butDeleteFavorites.Size = new System.Drawing.Size(42, 25);
            this.butDeleteFavorites.TabIndex = 41;
            this.butDeleteFavorites.Text = "삭제";
            this.butDeleteFavorites.UseVisualStyleBackColor = true;
            this.butDeleteFavorites.Click += new System.EventHandler(this.butDeleteFavorites_Click);
            // 
            // comFavoritesCode
            // 
            this.comFavoritesCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comFavoritesCode.FormattingEnabled = true;
            this.comFavoritesCode.Location = new System.Drawing.Point(66, 20);
            this.comFavoritesCode.Name = "comFavoritesCode";
            this.comFavoritesCode.Size = new System.Drawing.Size(154, 20);
            this.comFavoritesCode.TabIndex = 40;
            this.comFavoritesCode.SelectionChangeCommitted += new System.EventHandler(this.comFavoritesCode_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.Location = new System.Drawing.Point(7, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 39;
            this.label2.Text = "사용기능";
            // 
            // texFavoritesName
            // 
            this.texFavoritesName.Font = new System.Drawing.Font("굴림", 9F);
            this.texFavoritesName.Location = new System.Drawing.Point(77, 56);
            this.texFavoritesName.MaxLength = 50;
            this.texFavoritesName.Name = "texFavoritesName";
            this.texFavoritesName.Size = new System.Drawing.Size(220, 21);
            this.texFavoritesName.TabIndex = 35;
            this.texFavoritesName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.texFavoritesName_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F);
            this.label6.Location = new System.Drawing.Point(6, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 34;
            this.label6.Text = "즐겨찾기명";
            // 
            // butApplyFavorites
            // 
            this.butApplyFavorites.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butApplyFavorites.Font = new System.Drawing.Font("굴림", 9F);
            this.butApplyFavorites.Location = new System.Drawing.Point(613, 54);
            this.butApplyFavorites.Name = "butApplyFavorites";
            this.butApplyFavorites.Size = new System.Drawing.Size(75, 25);
            this.butApplyFavorites.TabIndex = 31;
            this.butApplyFavorites.Text = "트렌드보기";
            this.butApplyFavorites.UseVisualStyleBackColor = true;
            this.butApplyFavorites.Click += new System.EventHandler(this.butApplyFavorites_Click);
            // 
            // butSelectFavorites
            // 
            this.butSelectFavorites.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectFavorites.Location = new System.Drawing.Point(365, 54);
            this.butSelectFavorites.Name = "butSelectFavorites";
            this.butSelectFavorites.Size = new System.Drawing.Size(42, 25);
            this.butSelectFavorites.TabIndex = 6;
            this.butSelectFavorites.Text = "조회";
            this.butSelectFavorites.UseVisualStyleBackColor = true;
            this.butSelectFavorites.Click += new System.EventHandler(this.butSelectFavorites_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(10, 100);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(694, 10);
            this.pictureBox6.TabIndex = 143;
            this.pictureBox6.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(10, 110);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Size = new System.Drawing.Size(694, 541);
            this.splitContainer1.SplitterDistance = 270;
            this.splitContainer1.SplitterWidth = 10;
            this.splitContainer1.TabIndex = 144;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.griFavoritesTitle);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(694, 270);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "즐겨찾기";
            // 
            // griFavoritesTitle
            // 
            this.griFavoritesTitle.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griFavoritesTitle.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.griFavoritesTitle.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griFavoritesTitle.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.griFavoritesTitle.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griFavoritesTitle.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.griFavoritesTitle.DisplayLayout.MaxColScrollRegions = 1;
            this.griFavoritesTitle.DisplayLayout.MaxRowScrollRegions = 1;
            this.griFavoritesTitle.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griFavoritesTitle.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griFavoritesTitle.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BorderColor = System.Drawing.Color.Silver;
            this.griFavoritesTitle.DisplayLayout.Override.CellAppearance = appearance4;
            this.griFavoritesTitle.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griFavoritesTitle.DisplayLayout.Override.CellPadding = 0;
            this.griFavoritesTitle.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griFavoritesTitle.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griFavoritesTitle.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griFavoritesTitle.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griFavoritesTitle.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            this.griFavoritesTitle.DisplayLayout.Override.RowAppearance = appearance5;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.griFavoritesTitle.DisplayLayout.Override.RowSelectorAppearance = appearance6;
            this.griFavoritesTitle.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griFavoritesTitle.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griFavoritesTitle.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griFavoritesTitle.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.griFavoritesTitle.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griFavoritesTitle.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griFavoritesTitle.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griFavoritesTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griFavoritesTitle.Font = new System.Drawing.Font("굴림", 9F);
            this.griFavoritesTitle.Location = new System.Drawing.Point(3, 17);
            this.griFavoritesTitle.Name = "griFavoritesTitle";
            this.griFavoritesTitle.Size = new System.Drawing.Size(688, 215);
            this.griFavoritesTitle.TabIndex = 158;
            this.griFavoritesTitle.Text = "ultraGrid1";
            this.griFavoritesTitle.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griFavoritesTitle_AfterSelectChange);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.texUpdateTitle);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.butUpdateFavorites);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 232);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(688, 35);
            this.panel1.TabIndex = 157;
            // 
            // texUpdateTitle
            // 
            this.texUpdateTitle.Font = new System.Drawing.Font("굴림", 9F);
            this.texUpdateTitle.Location = new System.Drawing.Point(74, 7);
            this.texUpdateTitle.MaxLength = 50;
            this.texUpdateTitle.Name = "texUpdateTitle";
            this.texUpdateTitle.Size = new System.Drawing.Size(220, 21);
            this.texUpdateTitle.TabIndex = 45;
            this.texUpdateTitle.TextChanged += new System.EventHandler(this.texUpdateTitle_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 44;
            this.label1.Text = "즐겨찾기명";
            // 
            // butUpdateFavorites
            // 
            this.butUpdateFavorites.Font = new System.Drawing.Font("굴림", 9F);
            this.butUpdateFavorites.Location = new System.Drawing.Point(362, 5);
            this.butUpdateFavorites.Name = "butUpdateFavorites";
            this.butUpdateFavorites.Size = new System.Drawing.Size(42, 25);
            this.butUpdateFavorites.TabIndex = 43;
            this.butUpdateFavorites.Text = "저장";
            this.butUpdateFavorites.UseVisualStyleBackColor = true;
            this.butUpdateFavorites.Click += new System.EventHandler(this.butUpdateFavorites_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.griFavoritesTag);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(694, 261);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "태그목록";
            // 
            // griFavoritesTag
            // 
            this.griFavoritesTag.AllowDrop = true;
            this.griFavoritesTag.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griFavoritesTag.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.griFavoritesTag.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griFavoritesTag.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.griFavoritesTag.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griFavoritesTag.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.griFavoritesTag.DisplayLayout.MaxColScrollRegions = 1;
            this.griFavoritesTag.DisplayLayout.MaxRowScrollRegions = 1;
            this.griFavoritesTag.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griFavoritesTag.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griFavoritesTag.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.griFavoritesTag.DisplayLayout.Override.CellAppearance = appearance10;
            this.griFavoritesTag.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            this.griFavoritesTag.DisplayLayout.Override.CellPadding = 0;
            this.griFavoritesTag.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griFavoritesTag.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griFavoritesTag.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griFavoritesTag.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.griFavoritesTag.DisplayLayout.Override.RowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            this.griFavoritesTag.DisplayLayout.Override.RowSelectorAppearance = appearance12;
            this.griFavoritesTag.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griFavoritesTag.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griFavoritesTag.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griFavoritesTag.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
            this.griFavoritesTag.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griFavoritesTag.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griFavoritesTag.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griFavoritesTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griFavoritesTag.Font = new System.Drawing.Font("굴림", 9F);
            this.griFavoritesTag.Location = new System.Drawing.Point(3, 17);
            this.griFavoritesTag.Name = "griFavoritesTag";
            this.griFavoritesTag.Size = new System.Drawing.Size(688, 241);
            this.griFavoritesTag.TabIndex = 157;
            this.griFavoritesTag.Text = "ultraGrid1";
            this.griFavoritesTag.DragDrop += new System.Windows.Forms.DragEventHandler(this.griFavoritesTag_DragDrop);
            this.griFavoritesTag.DragOver += new System.Windows.Forms.DragEventHandler(this.griFavoritesTag_DragOver);
            this.griFavoritesTag.SelectionDrag += new System.ComponentModel.CancelEventHandler(this.griFavoritesTag_SelectionDrag);
            this.griFavoritesTag.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.griFavoritesTag_ClickCellButton);
            this.griFavoritesTag.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.griFavoritesTag_InitializeLayout);
            this.griFavoritesTag.MouseEnterElement += new Infragistics.Win.UIElementEventHandler(this.griFavoritesTag_MouseEnterElement);
            // 
            // frmFavoritesTagLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 661);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.MinimumSize = new System.Drawing.Size(730, 700);
            this.Name = "frmFavoritesTagLoad";
            this.Text = "태그즐겨찾기";
            this.Load += new System.EventHandler(this.frmFavoritesTagLoad_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFavoritesTagLoad_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griFavoritesTitle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griFavoritesTag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox texFavoritesName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button butApplyFavorites;
        private System.Windows.Forms.Button butSelectFavorites;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid griFavoritesTag;
        private System.Windows.Forms.ComboBox comFavoritesCode;
        private System.Windows.Forms.Button butDeleteFavorites;
        private Infragistics.Win.UltraWinGrid.UltraGrid griFavoritesTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox texUpdateTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butUpdateFavorites;
    }
}