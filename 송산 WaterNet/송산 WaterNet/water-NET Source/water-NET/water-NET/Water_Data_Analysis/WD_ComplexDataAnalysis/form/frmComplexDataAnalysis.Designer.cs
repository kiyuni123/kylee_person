﻿namespace WaterNet.WD_ComplexDataAnalysis.form
{
    partial class frmComplexDataAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butRoadFavorites = new System.Windows.Forms.Button();
            this.butSaveFavorites = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comTagType = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.texTagDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.butSelectTag = new System.Windows.Forms.Button();
            this.texTagsn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.butSelectTagDetail = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.griTagListBeforeSelect = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butMoveRightAll = new System.Windows.Forms.Button();
            this.butMoveRight = new System.Windows.Forms.Button();
            this.butMoveLeft = new System.Windows.Forms.Button();
            this.butMoveLeftAll = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.griTagListAfterSelect = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.griINPList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butMoveFirst = new System.Windows.Forms.Button();
            this.butMoveUp = new System.Windows.Forms.Button();
            this.butMoveDown = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comTagType)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griTagListBeforeSelect)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griTagListAfterSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griINPList)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butRoadFavorites);
            this.groupBox1.Controls.Add(this.butSaveFavorites);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comTagType);
            this.groupBox1.Controls.Add(this.texTagDesc);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.butSelectTag);
            this.groupBox1.Controls.Add(this.texTagsn);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.butSelectTagDetail);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.MinimumSize = new System.Drawing.Size(964, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(964, 90);
            this.groupBox1.TabIndex = 135;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butRoadFavorites
            // 
            this.butRoadFavorites.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butRoadFavorites.Font = new System.Drawing.Font("굴림", 9F);
            this.butRoadFavorites.Location = new System.Drawing.Point(802, 54);
            this.butRoadFavorites.Name = "butRoadFavorites";
            this.butRoadFavorites.Size = new System.Drawing.Size(64, 25);
            this.butRoadFavorites.TabIndex = 42;
            this.butRoadFavorites.Text = "즐겨찾기";
            this.butRoadFavorites.UseVisualStyleBackColor = true;
            this.butRoadFavorites.Click += new System.EventHandler(this.butRoadFavorites_Click);
            // 
            // butSaveFavorites
            // 
            this.butSaveFavorites.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSaveFavorites.Font = new System.Drawing.Font("굴림", 9F);
            this.butSaveFavorites.Location = new System.Drawing.Point(872, 54);
            this.butSaveFavorites.Name = "butSaveFavorites";
            this.butSaveFavorites.Size = new System.Drawing.Size(86, 25);
            this.butSaveFavorites.TabIndex = 41;
            this.butSaveFavorites.Text = "즐겨찾기등록";
            this.butSaveFavorites.UseVisualStyleBackColor = true;
            this.butSaveFavorites.Click += new System.EventHandler(this.butSaveFavorites_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 40;
            this.label1.Text = "태그구분";
            // 
            // comTagType
            // 
            this.comTagType.CheckedListSettings.CheckStateMember = "";
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.comTagType.DisplayLayout.Appearance = appearance4;
            this.comTagType.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.comTagType.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.comTagType.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comTagType.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.comTagType.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comTagType.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.comTagType.DisplayLayout.MaxColScrollRegions = 1;
            this.comTagType.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comTagType.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.comTagType.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.comTagType.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.comTagType.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.comTagType.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.comTagType.DisplayLayout.Override.CellAppearance = appearance5;
            this.comTagType.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.comTagType.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.comTagType.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance29.TextHAlignAsString = "Left";
            this.comTagType.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.comTagType.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.comTagType.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.comTagType.DisplayLayout.Override.RowAppearance = appearance28;
            this.comTagType.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.comTagType.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.comTagType.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.comTagType.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.comTagType.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.comTagType.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.comTagType.Location = new System.Drawing.Point(65, 18);
            this.comTagType.Name = "comTagType";
            this.comTagType.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.comTagType.Size = new System.Drawing.Size(300, 22);
            this.comTagType.TabIndex = 2;
            this.comTagType.AfterCloseUp += new System.EventHandler(this.comTagType_AfterCloseUp);
            // 
            // texTagDesc
            // 
            this.texTagDesc.Font = new System.Drawing.Font("굴림", 9F);
            this.texTagDesc.Location = new System.Drawing.Point(269, 56);
            this.texTagDesc.MaxLength = 50;
            this.texTagDesc.Name = "texTagDesc";
            this.texTagDesc.Size = new System.Drawing.Size(300, 21);
            this.texTagDesc.TabIndex = 4;
            this.texTagDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.texTagDesc_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F);
            this.label4.Location = new System.Drawing.Point(196, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 12);
            this.label4.TabIndex = 37;
            this.label4.Text = "TAG DESC";
            // 
            // butSelectTag
            // 
            this.butSelectTag.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectTag.Location = new System.Drawing.Point(575, 54);
            this.butSelectTag.Name = "butSelectTag";
            this.butSelectTag.Size = new System.Drawing.Size(64, 25);
            this.butSelectTag.TabIndex = 5;
            this.butSelectTag.Text = "태그조회";
            this.butSelectTag.UseVisualStyleBackColor = true;
            this.butSelectTag.Click += new System.EventHandler(this.butSelectTag_Click);
            // 
            // texTagsn
            // 
            this.texTagsn.Font = new System.Drawing.Font("굴림", 9F);
            this.texTagsn.Location = new System.Drawing.Point(59, 56);
            this.texTagsn.MaxLength = 10;
            this.texTagsn.Name = "texTagsn";
            this.texTagsn.Size = new System.Drawing.Size(100, 21);
            this.texTagsn.TabIndex = 3;
            this.texTagsn.TextChanged += new System.EventHandler(this.texTagsn_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F);
            this.label3.Location = new System.Drawing.Point(6, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 34;
            this.label3.Text = "TAGSN";
            // 
            // butSelectTagDetail
            // 
            this.butSelectTagDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectTagDetail.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectTagDetail.Location = new System.Drawing.Point(721, 54);
            this.butSelectTagDetail.Name = "butSelectTagDetail";
            this.butSelectTagDetail.Size = new System.Drawing.Size(75, 25);
            this.butSelectTagDetail.TabIndex = 6;
            this.butSelectTagDetail.Text = "트렌드보기";
            this.butSelectTagDetail.UseVisualStyleBackColor = true;
            this.butSelectTagDetail.Click += new System.EventHandler(this.butSelectTagDetail_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(10, 110);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(964, 441);
            this.splitContainer1.SplitterDistance = 480;
            this.splitContainer1.TabIndex = 137;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.griTagListBeforeSelect);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.MinimumSize = new System.Drawing.Size(430, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(430, 441);
            this.groupBox2.TabIndex = 100;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "태그조회";
            // 
            // griTagListBeforeSelect
            // 
            this.griTagListBeforeSelect.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagListBeforeSelect.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.griTagListBeforeSelect.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griTagListBeforeSelect.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.griTagListBeforeSelect.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griTagListBeforeSelect.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.griTagListBeforeSelect.DisplayLayout.MaxColScrollRegions = 1;
            this.griTagListBeforeSelect.DisplayLayout.MaxRowScrollRegions = 1;
            this.griTagListBeforeSelect.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griTagListBeforeSelect.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagListBeforeSelect.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.griTagListBeforeSelect.DisplayLayout.Override.CellAppearance = appearance16;
            this.griTagListBeforeSelect.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griTagListBeforeSelect.DisplayLayout.Override.CellPadding = 0;
            this.griTagListBeforeSelect.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griTagListBeforeSelect.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griTagListBeforeSelect.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griTagListBeforeSelect.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griTagListBeforeSelect.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.griTagListBeforeSelect.DisplayLayout.Override.RowAppearance = appearance17;
            appearance18.TextHAlignAsString = "Center";
            appearance18.TextVAlignAsString = "Middle";
            this.griTagListBeforeSelect.DisplayLayout.Override.RowSelectorAppearance = appearance18;
            this.griTagListBeforeSelect.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griTagListBeforeSelect.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griTagListBeforeSelect.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griTagListBeforeSelect.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
            this.griTagListBeforeSelect.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griTagListBeforeSelect.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griTagListBeforeSelect.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griTagListBeforeSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griTagListBeforeSelect.Font = new System.Drawing.Font("굴림", 9F);
            this.griTagListBeforeSelect.Location = new System.Drawing.Point(3, 17);
            this.griTagListBeforeSelect.MinimumSize = new System.Drawing.Size(424, 0);
            this.griTagListBeforeSelect.Name = "griTagListBeforeSelect";
            this.griTagListBeforeSelect.Size = new System.Drawing.Size(424, 421);
            this.griTagListBeforeSelect.TabIndex = 154;
            this.griTagListBeforeSelect.Text = "ultraGrid1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.butMoveRightAll);
            this.panel2.Controls.Add(this.butMoveRight);
            this.panel2.Controls.Add(this.butMoveLeft);
            this.panel2.Controls.Add(this.butMoveLeftAll);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(430, 0);
            this.panel2.MinimumSize = new System.Drawing.Size(50, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(50, 441);
            this.panel2.TabIndex = 8;
            // 
            // butMoveRightAll
            // 
            this.butMoveRightAll.Location = new System.Drawing.Point(5, 260);
            this.butMoveRightAll.Name = "butMoveRightAll";
            this.butMoveRightAll.Size = new System.Drawing.Size(41, 23);
            this.butMoveRightAll.TabIndex = 150;
            this.butMoveRightAll.Text = ">>";
            this.butMoveRightAll.UseVisualStyleBackColor = true;
            this.butMoveRightAll.Visible = false;
            this.butMoveRightAll.Click += new System.EventHandler(this.butMoveRightAll_Click);
            // 
            // butMoveRight
            // 
            this.butMoveRight.Location = new System.Drawing.Point(5, 144);
            this.butMoveRight.Name = "butMoveRight";
            this.butMoveRight.Size = new System.Drawing.Size(41, 23);
            this.butMoveRight.TabIndex = 7;
            this.butMoveRight.Text = ">";
            this.butMoveRight.UseVisualStyleBackColor = true;
            this.butMoveRight.Click += new System.EventHandler(this.butMoveRight_Click);
            // 
            // butMoveLeft
            // 
            this.butMoveLeft.Location = new System.Drawing.Point(5, 183);
            this.butMoveLeft.Name = "butMoveLeft";
            this.butMoveLeft.Size = new System.Drawing.Size(41, 23);
            this.butMoveLeft.TabIndex = 8;
            this.butMoveLeft.Text = "<";
            this.butMoveLeft.UseVisualStyleBackColor = true;
            this.butMoveLeft.Click += new System.EventHandler(this.butMoveLeft_Click);
            // 
            // butMoveLeftAll
            // 
            this.butMoveLeftAll.Location = new System.Drawing.Point(5, 222);
            this.butMoveLeftAll.Name = "butMoveLeftAll";
            this.butMoveLeftAll.Size = new System.Drawing.Size(41, 23);
            this.butMoveLeftAll.TabIndex = 9;
            this.butMoveLeftAll.Text = "<<";
            this.butMoveLeftAll.UseVisualStyleBackColor = true;
            this.butMoveLeftAll.Click += new System.EventHandler(this.butMoveLeftAll_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.griTagListAfterSelect);
            this.groupBox3.Controls.Add(this.griINPList);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.MinimumSize = new System.Drawing.Size(430, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(430, 441);
            this.groupBox3.TabIndex = 101;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "태그선택";
            // 
            // griTagListAfterSelect
            // 
            this.griTagListAfterSelect.AllowDrop = true;
            this.griTagListAfterSelect.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagListAfterSelect.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.griTagListAfterSelect.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griTagListAfterSelect.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.griTagListAfterSelect.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griTagListAfterSelect.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.griTagListAfterSelect.DisplayLayout.MaxColScrollRegions = 1;
            this.griTagListAfterSelect.DisplayLayout.MaxRowScrollRegions = 1;
            this.griTagListAfterSelect.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griTagListAfterSelect.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagListAfterSelect.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            this.griTagListAfterSelect.DisplayLayout.Override.CellAppearance = appearance9;
            this.griTagListAfterSelect.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griTagListAfterSelect.DisplayLayout.Override.CellPadding = 0;
            this.griTagListAfterSelect.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griTagListAfterSelect.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griTagListAfterSelect.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griTagListAfterSelect.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griTagListAfterSelect.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.griTagListAfterSelect.DisplayLayout.Override.RowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Center";
            appearance11.TextVAlignAsString = "Middle";
            this.griTagListAfterSelect.DisplayLayout.Override.RowSelectorAppearance = appearance11;
            this.griTagListAfterSelect.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griTagListAfterSelect.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griTagListAfterSelect.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griTagListAfterSelect.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
            this.griTagListAfterSelect.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griTagListAfterSelect.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griTagListAfterSelect.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griTagListAfterSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griTagListAfterSelect.Font = new System.Drawing.Font("굴림", 9F);
            this.griTagListAfterSelect.Location = new System.Drawing.Point(3, 17);
            this.griTagListAfterSelect.MinimumSize = new System.Drawing.Size(424, 0);
            this.griTagListAfterSelect.Name = "griTagListAfterSelect";
            this.griTagListAfterSelect.Size = new System.Drawing.Size(424, 421);
            this.griTagListAfterSelect.TabIndex = 155;
            this.griTagListAfterSelect.Text = "ultraGrid2";
            this.griTagListAfterSelect.DragDrop += new System.Windows.Forms.DragEventHandler(this.griTagListAfterSelect_DragDrop);
            this.griTagListAfterSelect.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.griTagListAfterSelect_InitializeRow);
            this.griTagListAfterSelect.DragOver += new System.Windows.Forms.DragEventHandler(this.griTagListAfterSelect_DragOver);
            this.griTagListAfterSelect.SelectionDrag += new System.ComponentModel.CancelEventHandler(this.griTagListAfterSelect_SelectionDrag);
            this.griTagListAfterSelect.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.griTagListAfterSelect_ClickCellButton);
            this.griTagListAfterSelect.MouseEnterElement += new Infragistics.Win.UIElementEventHandler(this.griTagListAfterSelect_MouseEnterElement);
            // 
            // griINPList
            // 
            this.griINPList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griINPList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.griINPList.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griINPList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.griINPList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griINPList.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.griINPList.DisplayLayout.MaxColScrollRegions = 1;
            this.griINPList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griINPList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griINPList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griINPList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.griINPList.DisplayLayout.Override.CellAppearance = appearance22;
            this.griINPList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griINPList.DisplayLayout.Override.CellPadding = 0;
            this.griINPList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griINPList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griINPList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griINPList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griINPList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.griINPList.DisplayLayout.Override.RowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Center";
            appearance24.TextVAlignAsString = "Middle";
            this.griINPList.DisplayLayout.Override.RowSelectorAppearance = appearance24;
            this.griINPList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griINPList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griINPList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griINPList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griINPList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griINPList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griINPList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griINPList.Font = new System.Drawing.Font("굴림", 9F);
            this.griINPList.Location = new System.Drawing.Point(-431, 117);
            this.griINPList.Name = "griINPList";
            this.griINPList.Size = new System.Drawing.Size(211, 156);
            this.griINPList.TabIndex = 154;
            this.griINPList.Text = "ultraGrid1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butMoveFirst);
            this.panel1.Controls.Add(this.butMoveUp);
            this.panel1.Controls.Add(this.butMoveDown);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(430, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(50, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(50, 441);
            this.panel1.TabIndex = 10;
            // 
            // butMoveFirst
            // 
            this.butMoveFirst.Font = new System.Drawing.Font("굴림", 9F);
            this.butMoveFirst.Location = new System.Drawing.Point(5, 222);
            this.butMoveFirst.Name = "butMoveFirst";
            this.butMoveFirst.Size = new System.Drawing.Size(41, 23);
            this.butMoveFirst.TabIndex = 12;
            this.butMoveFirst.Text = "△";
            this.butMoveFirst.UseVisualStyleBackColor = true;
            this.butMoveFirst.Click += new System.EventHandler(this.butMoveFirst_Click);
            // 
            // butMoveUp
            // 
            this.butMoveUp.Location = new System.Drawing.Point(5, 144);
            this.butMoveUp.Name = "butMoveUp";
            this.butMoveUp.Size = new System.Drawing.Size(41, 23);
            this.butMoveUp.TabIndex = 10;
            this.butMoveUp.Text = "∧";
            this.butMoveUp.UseVisualStyleBackColor = true;
            this.butMoveUp.Click += new System.EventHandler(this.butMoveUp_Click);
            // 
            // butMoveDown
            // 
            this.butMoveDown.Location = new System.Drawing.Point(5, 183);
            this.butMoveDown.Name = "butMoveDown";
            this.butMoveDown.Size = new System.Drawing.Size(41, 23);
            this.butMoveDown.TabIndex = 11;
            this.butMoveDown.Text = "∨";
            this.butMoveDown.UseVisualStyleBackColor = true;
            this.butMoveDown.Click += new System.EventHandler(this.butMoveDown_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 100);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(964, 10);
            this.pictureBox4.TabIndex = 136;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox3.Location = new System.Drawing.Point(10, 551);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(964, 10);
            this.pictureBox3.TabIndex = 127;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(974, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 551);
            this.pictureBox1.TabIndex = 126;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 10);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 551);
            this.picFrLeftM1.TabIndex = 125;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(984, 10);
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            // 
            // frmComplexDataAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "frmComplexDataAnalysis";
            this.Text = "트렌드분석";
            this.Load += new System.EventHandler(this.frmComplexDataAnalysis_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comTagType)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griTagListBeforeSelect)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griTagListAfterSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griINPList)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butSelectTagDetail;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox texTagDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button butSelectTag;
        private System.Windows.Forms.TextBox texTagsn;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button butMoveRightAll;
        private System.Windows.Forms.Button butMoveRight;
        private System.Windows.Forms.Button butMoveLeft;
        private System.Windows.Forms.Button butMoveLeftAll;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butMoveFirst;
        private System.Windows.Forms.Button butMoveUp;
        private System.Windows.Forms.Button butMoveDown;
        private Infragistics.Win.UltraWinGrid.UltraGrid griINPList;
        private Infragistics.Win.UltraWinGrid.UltraGrid griTagListBeforeSelect;
        private Infragistics.Win.UltraWinGrid.UltraGrid griTagListAfterSelect;
        private Infragistics.Win.UltraWinGrid.UltraCombo comTagType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butSaveFavorites;
        private System.Windows.Forms.Button butRoadFavorites;
    }
}