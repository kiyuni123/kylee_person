﻿namespace WaterNet.WD_ComplexDataAnalysis.form
{
    partial class frmComplexDataAnalysisDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butSetTime = new System.Windows.Forms.Button();
            this.comSetTime = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.butSelect = new System.Windows.Forms.Button();
            this.datEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.butExcel = new System.Windows.Forms.Button();
            this.datStart = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chaTagListDetail = new ChartFX.WinForms.Chart();
            this.griTagListDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaTagListDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griTagListDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox3.Location = new System.Drawing.Point(10, 751);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1214, 10);
            this.pictureBox3.TabIndex = 131;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(1224, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 751);
            this.pictureBox1.TabIndex = 130;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 10);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 751);
            this.picFrLeftM1.TabIndex = 129;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1234, 10);
            this.pictureBox2.TabIndex = 128;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butSetTime);
            this.groupBox1.Controls.Add(this.comSetTime);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.butSelect);
            this.groupBox1.Controls.Add(this.datEnd);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.butExcel);
            this.groupBox1.Controls.Add(this.datStart);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1214, 50);
            this.groupBox1.TabIndex = 132;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butSetTime
            // 
            this.butSetTime.Location = new System.Drawing.Point(353, 19);
            this.butSetTime.Name = "butSetTime";
            this.butSetTime.Size = new System.Drawing.Size(86, 23);
            this.butSetTime.TabIndex = 44;
            this.butSetTime.Text = "현재시간적용";
            this.butSetTime.UseVisualStyleBackColor = true;
            this.butSetTime.Click += new System.EventHandler(this.butSetTime_Click);
            // 
            // comSetTime
            // 
            this.comSetTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comSetTime.FormattingEnabled = true;
            this.comSetTime.Location = new System.Drawing.Point(520, 20);
            this.comSetTime.Name = "comSetTime";
            this.comSetTime.Size = new System.Drawing.Size(50, 20);
            this.comSetTime.TabIndex = 41;
            this.comSetTime.SelectionChangeCommitted += new System.EventHandler(this.comSetTime_SelectionChangeCommitted);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F);
            this.label3.Location = new System.Drawing.Point(461, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 40;
            this.label3.Text = "시간설정";
            // 
            // butSelect
            // 
            this.butSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelect.Location = new System.Drawing.Point(1085, 19);
            this.butSelect.Name = "butSelect";
            this.butSelect.Size = new System.Drawing.Size(42, 23);
            this.butSelect.TabIndex = 39;
            this.butSelect.Text = "조회";
            this.butSelect.UseVisualStyleBackColor = true;
            this.butSelect.Click += new System.EventHandler(this.butSelect_Click);
            // 
            // datEnd
            // 
            this.datEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datEnd.Location = new System.Drawing.Point(211, 20);
            this.datEnd.Name = "datEnd";
            this.datEnd.Size = new System.Drawing.Size(120, 21);
            this.datEnd.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(191, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 12);
            this.label1.TabIndex = 36;
            this.label1.Text = "~";
            // 
            // butExcel
            // 
            this.butExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butExcel.Location = new System.Drawing.Point(1133, 19);
            this.butExcel.Name = "butExcel";
            this.butExcel.Size = new System.Drawing.Size(75, 23);
            this.butExcel.TabIndex = 33;
            this.butExcel.Text = "Excel 출력";
            this.butExcel.UseVisualStyleBackColor = true;
            this.butExcel.Click += new System.EventHandler(this.butExcel_Click);
            // 
            // datStart
            // 
            this.datStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datStart.Location = new System.Drawing.Point(65, 20);
            this.datStart.Name = "datStart";
            this.datStart.Size = new System.Drawing.Size(120, 21);
            this.datStart.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 31;
            this.label2.Text = "검색기간";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 60);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1214, 10);
            this.pictureBox4.TabIndex = 133;
            this.pictureBox4.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(10, 70);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chaTagListDetail);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.griTagListDetail);
            this.splitContainer1.Size = new System.Drawing.Size(1214, 681);
            this.splitContainer1.SplitterDistance = 410;
            this.splitContainer1.TabIndex = 134;
            // 
            // chaTagListDetail
            // 
            this.chaTagListDetail.AllSeries.MarkerShape = ChartFX.WinForms.MarkerShape.None;
            this.chaTagListDetail.AllSeries.MarkerSize = ((short)(0));
            this.chaTagListDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaTagListDetail.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chaTagListDetail.LegendBox.Border = ChartFX.WinForms.DockBorder.None;
            this.chaTagListDetail.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chaTagListDetail.Location = new System.Drawing.Point(0, 0);
            this.chaTagListDetail.Name = "chaTagListDetail";
            this.chaTagListDetail.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chaTagListDetail.Size = new System.Drawing.Size(1214, 410);
            this.chaTagListDetail.TabIndex = 151;
            this.chaTagListDetail.MouseMove += new ChartFX.WinForms.HitTestEventHandler(this.chaTagListDetail_MouseMove);
            this.chaTagListDetail.MouseClick += new ChartFX.WinForms.HitTestEventHandler(this.chaTagListDetail_MouseClick);
            // 
            // griTagListDetail
            // 
            this.griTagListDetail.AllowDrop = true;
            this.griTagListDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagListDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.griTagListDetail.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griTagListDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.griTagListDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griTagListDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.griTagListDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.griTagListDetail.DisplayLayout.MaxRowScrollRegions = 1;
            this.griTagListDetail.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griTagListDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagListDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BorderColor = System.Drawing.Color.Silver;
            this.griTagListDetail.DisplayLayout.Override.CellAppearance = appearance4;
            this.griTagListDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            this.griTagListDetail.DisplayLayout.Override.CellPadding = 0;
            this.griTagListDetail.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griTagListDetail.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griTagListDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griTagListDetail.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griTagListDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            this.griTagListDetail.DisplayLayout.Override.RowAppearance = appearance5;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.griTagListDetail.DisplayLayout.Override.RowSelectorAppearance = appearance6;
            this.griTagListDetail.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griTagListDetail.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griTagListDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griTagListDetail.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.griTagListDetail.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.griTagListDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griTagListDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griTagListDetail.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griTagListDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griTagListDetail.Font = new System.Drawing.Font("굴림", 9F);
            this.griTagListDetail.Location = new System.Drawing.Point(0, 0);
            this.griTagListDetail.MinimumSize = new System.Drawing.Size(424, 0);
            this.griTagListDetail.Name = "griTagListDetail";
            this.griTagListDetail.Size = new System.Drawing.Size(1214, 267);
            this.griTagListDetail.TabIndex = 156;
            this.griTagListDetail.Text = "ultraGrid2";
            // 
            // frmComplexDataAnalysisDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 761);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "frmComplexDataAnalysisDetail";
            this.Text = "트렌드 상세분석";
            this.Load += new System.EventHandler(this.frmComplexDataAnalysisDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chaTagListDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griTagListDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker datStart;
        private System.Windows.Forms.Button butExcel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker datEnd;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button butSelect;
        private System.Windows.Forms.ComboBox comSetTime;
        private System.Windows.Forms.Label label3;
        private ChartFX.WinForms.Chart chaTagListDetail;
        private Infragistics.Win.UltraWinGrid.UltraGrid griTagListDetail;
        private System.Windows.Forms.Button butSetTime;
    }
}