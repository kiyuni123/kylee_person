﻿/**************************************************************************
 * 파일명   : frmFavoritesTagSave.cs
 * 작성자   : shpark
 * 작성일자 : 2015.11.26
 * 설명     : 태그즐겨찾기등록 메인 폼
 * 변경이력 : 2015.11.26 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using WaterNet.WD_Common.utils;
using WaterNet.WD_ComplexDataAnalysis.work;
using EMFrame.log;


namespace WaterNet.WD_ComplexDataAnalysis.form
{
    public partial class frmFavoritesTagSave : Form
    {
        #region 1. 전역변수

        private FavoritesTagWork work = null;
        //private ArrayList favoritesTags = null;
        private DataTable tagsDT = null;

        #endregion 전역변수


        #region 2. 초기화

        //public frmFavoritesTagSave(ArrayList favoritesTags)
        public frmFavoritesTagSave(DataTable tagsDT)
        {
            work = FavoritesTagWork.GetInstance();
            InitializeComponent();
            InitializeComboBox();

            this.tagsDT = tagsDT;
        }

        //콤보박스 초기화
        private void InitializeComboBox()
        {
            //관리단 콤보박스 초기화
            //comMgdpcd.DataSource = work.SelectMgdpcd();
            //comMgdpcd.ValueMember = "MGDPCD";
            //comMgdpcd.DisplayMember = "MGDPNM";

            //ComboBoxUtils.AddDefaultOption(comMgdpcd, false);

            //사용기능 콤보박스 초기화
            comFavoritesCode.DataSource = work.SelectFavoritesCode();
            comFavoritesCode.ValueMember = "CODE";
            comFavoritesCode.DisplayMember = "CODE_NAME";
        }

        #endregion 초기화


        #region 3. 객체이벤트

        //등록 버튼 클릭 이벤트
        private void butSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (texFavoritesName.Text.Equals(string.Empty))
                {
                    CommonUtils.ShowInformationMessage("즐겨찾기명을 입력하세요."); return;
                }

                if (CommonUtils.ShowYesNoDialog("선택하신 태그를 즐겨찾기에 등록하시겠습니까?") == DialogResult.Yes)
                {
                    Hashtable favoritesData = new Hashtable();

                    //favoritesData.Add("MGDPCD", comMgdpcd.SelectedValue.ToString());
                    favoritesData.Add("FAVORITES_CODE", comFavoritesCode.SelectedValue.ToString());
                    favoritesData.Add("FAVORITES_NAME", texFavoritesName.Text);
                    favoritesData.Add("tagsDT", this.tagsDT);

                    work.SaveFavorites(favoritesData);

                    CommonUtils.ShowInformationMessage("'" + texFavoritesName.Text + "' 즐겨찾기가 등록되었습니다.");

                    this.Close();
                } 
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        #endregion 객체이벤트


        #region 4. 상속
        #endregion 상속

        #region 5. 외부사용
        #endregion 외부사용

        #region 6. 내부사용
        #endregion 내부사용

    }
}
