﻿/**************************************************************************
 * 파일명   : frmComplexDataAnalysisDetail.cs
 * 작성자   : shpark
 * 작성일자 : 2015.11.1
 * 설명     : 복합데이터시계열상세분석 메인화면
 * 변경이력 : 2015.11.11 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChartFX.WinForms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using WaterNet.WD_Common.utils;
using WaterNet.WD_ComplexDataAnalysis.work;
using EMFrame.log;

namespace WaterNet.WD_ComplexDataAnalysis.form
{
    public partial class frmComplexDataAnalysisDetail : Form
    {
        #region 1. 전역변수

        ComplexDataAnalysisWork work = null;
        private ArrayList arrTagList;           //사용자가 설정한 태그 조회 순서를 할당
        private Hashtable tagConditions;        //tagsn
        private Hashtable tmpYAxis;
        //private ChartManager chartManager = null;   //차트 매니저(차트 스타일 설정및 기타공통기능설정)
        private Color[] seriesColor = null;
        
        #endregion 전역변수


        #region 2. 초기화

        public frmComplexDataAnalysisDetail(ArrayList arrTagList, Hashtable tagConditions)
        {
            work = ComplexDataAnalysisWork.GetInstance();

            this.arrTagList = arrTagList;
            this.tagConditions = tagConditions;

            InitializeComponent();
            initializeDateTimePicker();
            initializeComboBox();
            InitializeUltraGrid();
            InitializeChartSetting();
        }

        //시간설정 콤보박스 초기화
        private void initializeComboBox()
        { 
            var setTimeItems = new BindingList<KeyValuePair<string, string>>();

            setTimeItems.Add(new KeyValuePair<string, string>("1", "1분"));
            setTimeItems.Add(new KeyValuePair<string, string>("10", "10분"));
            setTimeItems.Add(new KeyValuePair<string, string>("15", "15분"));
            setTimeItems.Add(new KeyValuePair<string, string>("30", "30분"));
            setTimeItems.Add(new KeyValuePair<string, string>("00", "60분"));

            comSetTime.DataSource = setTimeItems;
            comSetTime.ValueMember = "Key";
            comSetTime.DisplayMember = "Value";
            comSetTime.SelectedIndex = 0;
        }

        //dateTimePicker 초기화
        private void initializeDateTimePicker()
        {
            datStart.Format = DateTimePickerFormat.Custom;
            datStart.CustomFormat = "yyyy-MM-dd HH:mm";
            datStart.ShowUpDown = true;
            datStart.Value = DateTime.Today.Date.AddDays(-1);

            datEnd.Format = DateTimePickerFormat.Custom;
            datEnd.CustomFormat = "yyyy-MM-dd HH:mm";
            datEnd.ShowUpDown = true;
            datEnd.Value = DateTime.Today.Date.AddMinutes(-1);
        }

        //그리드 초기화
        private void InitializeUltraGrid()
        {
            UltraGridColumn tagListDetailColumn;        //선택전 태그리스트 그리드

            tagListDetailColumn = griTagListDetail.DisplayLayout.Bands[0].Columns.Add();
            tagListDetailColumn.Key = "TIMESTAMP";
            tagListDetailColumn.Header.Caption = "날짜";
            tagListDetailColumn.CellActivation = Activation.NoEdit;
            tagListDetailColumn.CellClickAction = CellClickAction.CellSelect;
            tagListDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListDetailColumn.SortIndicator = SortIndicator.Ascending;
            tagListDetailColumn.Width = 120;
            tagListDetailColumn.MaskInput = "yyyy-MM-dd HH:mm";
            tagListDetailColumn.Format = "yyyy-MM-dd HH:mm";

            foreach (string tagsn in arrTagList)
            {
                tagListDetailColumn = griTagListDetail.DisplayLayout.Bands[0].Columns.Add();
                tagListDetailColumn.Key = tagsn;
                tagListDetailColumn.Header.Caption = tagsn;
                tagListDetailColumn.CellActivation = Activation.NoEdit;
                tagListDetailColumn.CellClickAction = CellClickAction.CellSelect;
                tagListDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                tagListDetailColumn.CellAppearance.TextHAlign = HAlign.Right;
                tagListDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
                tagListDetailColumn.SortIndicator = SortIndicator.Disabled;
                tagListDetailColumn.Width = 100;
                tagListDetailColumn.DataType = typeof(double);
                tagListDetailColumn.Format = "###,###,##0.####";
            }

            griTagListDetail.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Default;
            griTagListDetail.DisplayLayout.Override.SelectTypeCol = SelectType.None;
            griTagListDetail.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;
            griTagListDetail.DisplayLayout.Override.SummaryValueAppearance.BorderColor = Color.Silver;
        }

        //차트 초기화
        private void InitializeChartSetting()
        {
            chaTagListDetail.LegendBox.MarginY = 1;
            chaTagListDetail.AxisX.DataFormat.Format = AxisFormat.DateTime;
            chaTagListDetail.AxisX.DataFormat.CustomFormat = "yyyy-MM-dd HH:mm";
            chaTagListDetail.AxisX.Staggered = true;
            chaTagListDetail.ToolBar.Visible = true;
            chaTagListDetail.Commands[CommandId.PersonalizedOptions].Enabled = false;
            chaTagListDetail.Data.Clear();
            chaTagListDetail.Data.Series = arrTagList.Count;

            chaTagListDetail.DataSourceSettings.Fields.Add(new FieldMap("TIMESTAMP", FieldUsage.Label));        //차트 X축 컬럼 설정

            LegendItemAttributes legendItem = new LegendItemAttributes();
            //legendItem.Visible = false;                                                                       //기본 레전드박스를 비활성화 (커스텀 레전드를 추가하기위해 필요)
            chaTagListDetail.LegendBox.ItemAttributes[chaTagListDetail.Series] = legendItem;

            tmpYAxis = new Hashtable();

            foreach (string tagsn in arrTagList)
            {
                Hashtable tagCondition = new Hashtable();
                tagCondition = tagConditions[tagsn] as Hashtable;

                string typeY = tagCondition["TYPE"].ToString();

                if (tmpYAxis[typeY] == null)
                {
                    AxisY newAxisY = new AxisY();
                    newAxisY.Visible = true;

                    if (tmpYAxis.Count == 0)
                    {
                        newAxisY.Position = AxisPosition.Near;
                    }
                    else
                    {
                        newAxisY.Position = AxisPosition.Far;
                    }
                    
                    newAxisY.AutoScale = true;
                    newAxisY.Title.Text = typeY;
                    newAxisY.LabelsFormat.Decimals = 2;
                    newAxisY.Grids.Major.Visible = false;
                    newAxisY.Grids.Minor.Visible = false;

                    tmpYAxis.Add(typeY, newAxisY);
                    chaTagListDetail.AxesY.Add(newAxisY);
                }

                chaTagListDetail.DataSourceSettings.Fields.Add(new FieldMap(tagsn, FieldUsage.Value));
            }
        }

        #endregion 초기화


        #region 3. 이벤트

        //폼 로드 이벤트
        private void frmComplexDataAnalysisDetail_Load(object sender, EventArgs e)
        {
            //폼 로딩 시 선택태그 조회
            SelectComplexTagListDetail();
        }

        //시간설정 콤보박스 선택 변경 이벤트
        private void comSetTime_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SelectComplexTagListDetail();
        }

        //현재시간적용 버튼 클릭 이벤트
        private void butSetTime_Click(object sender, EventArgs e)
        {
            switch (butSetTime.Text)
            {
                case "현재시간적용":

                    if (CommonUtils.ShowYesNoDialog("검색기간에 현재시간을 적용하시겠습니까?") == DialogResult.Yes)
                    {
                        datStart.Value = DateTime.Now.AddDays(-1);
                        datEnd.Value = DateTime.Now.AddMinutes(-1);

                        butSetTime.Text = "기준시간적용";

                        SelectComplexTagListDetail();
                    }

                    break;

                case "기준시간적용":

                    if (CommonUtils.ShowYesNoDialog("검색기간에 기준시간을 적용하시겠습니까?") == DialogResult.Yes)
                    {
                        datStart.Value = DateTime.Today.Date.AddDays(-1);
                        datEnd.Value = DateTime.Today.Date.AddMinutes(-1);

                        butSetTime.Text = "현재시간적용";

                        SelectComplexTagListDetail();
                    }

                    break;
            }
        }

        //조회 버튼 클릭 이벤트
        private void butSelect_Click(object sender, EventArgs e)
        {
            SelectComplexTagListDetail();
        }

        //Excel 출력 버튼 클릭 이벤트
        private void butExcel_Click(object sender, EventArgs e)
        {
            if (griTagListDetail.Rows.Count == 0)
            {
                CommonUtils.ShowInformationMessage("출력할 수 있는 데이터가 없습니다."); return;
            }

            WaterNetCore.FormManager.ExportToExcelFromUltraGrid(griTagListDetail, "복합데이터 시계열 상세분석");
        }

        //차트 마우스 무브 이벤트
        //차트에서 마우스 커서가 위치하는 곳과 동일한 값을 갖고 있는 그리드의 셀을 선택한다
        private void chaTagListDetail_MouseMove(object sender, HitTestEventArgs e)
        {
            try
            {
                e = chaTagListDetail.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);

                if (e.HitType.ToString().Equals("Point") || e.HitType.ToString().Equals("Between"))
                {
                    int point = e.Point;
                    int series = e.Series;
                    int rowIndex = 0;

                    SortIndicator si = griTagListDetail.DisplayLayout.Bands[0].SortedColumns["TIMESTAMP"].SortIndicator;

                    if (si == SortIndicator.Ascending)
                    {
                        rowIndex = point;
                    }
                    else if (si == SortIndicator.Descending)
                    {
                        rowIndex = griTagListDetail.Rows.Count - point - 1;
                    }

                    if (point != -1 && series != -1)
                    {
                        griTagListDetail.Rows[rowIndex].Cells[series + 1].Activate();
                        griTagListDetail.Rows[rowIndex].Cells[series + 1].Selected = true;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        //차트 마우스 클릭 이벤트
        //레전드박스를 클릭한 경우 해당하는 차트 시리즈를 view/hidden 한다
        private void chaTagListDetail_MouseClick(object sender, HitTestEventArgs e)
        {
            try
            {
                if (e.HitType.ToString().Equals("LegendBox"))
                {
                    e = chaTagListDetail.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);

                    Chart chart = sender as Chart;
                    SeriesAttributes sa = chart.Series[e.Series];

                    if (sa.Color != Color.Transparent)
                    {
                        sa.Color = Color.Transparent;
                        chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = Color.DarkGray;
                    }
                    else
                    {
                        sa.Color = seriesColor[e.Series];
                        chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = Color.Black;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion 이벤트


        #region 6.내부사용

        //선택 태그 상세 조회
        private void SelectComplexTagListDetail()
        {
            if ((datEnd.Value.CompareTo(datStart.Value) != 1))
            {
                CommonUtils.ShowInformationMessage("검색기간을 확인하세요."); return;
            }

            try
            {
                this.Cursor = Cursors.WaitCursor;

                //차트 데이터 삭제
                chaTagListDetail.AxisY.Sections.Clear();
                chaTagListDetail.ConditionalAttributes.Clear();
                chaTagListDetail.Extensions.Clear();
                chaTagListDetail.LegendBox.CustomItems.Clear();

                if (tagConditions["StartDate"] == null)
                {
                    tagConditions.Add("StartDate", CommonUtils.ConvertDateTimePicker(datStart, "DATE_TIME"));
                    tagConditions.Add("EndDate", CommonUtils.ConvertDateTimePicker(datEnd, "DATE_TIME"));
                    tagConditions.Add("SetTime", comSetTime.SelectedValue.ToString());
                }
                else
                {
                    tagConditions["StartDate"] = CommonUtils.ConvertDateTimePicker(datStart, "DATE_TIME");
                    tagConditions["EndDate"] = CommonUtils.ConvertDateTimePicker(datEnd, "DATE_TIME");
                    tagConditions["SetTime"] = comSetTime.SelectedValue.ToString();
                }

                DataTable result = work.SelectComplexTagListDetail(arrTagList, tagConditions);

                griTagListDetail.DataSource = result;
                chaTagListDetail.DataSource = result;

                setGridToolTip();                   //그리드의 컬럼과 셀에 TAG Description을 툴팁 텍스트로 설정
                InitializeChartAfterSelect();       //조회 후 차트 레전드 박스, 시리즈 재설정
                SetSummaryRows();                   //조회 후 그리드 최대, 최소, 합계 그리드 추가

                CommonUtils.SelectFirstRow(griTagListDetail);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //그리드 최소값,최대값,합계 추가
        private void SetSummaryRows()
        {
            griTagListDetail.DisplayLayout.Bands[0].Summaries.Clear();
            griTagListDetail.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            foreach (UltraGridColumn gridColumn in griTagListDetail.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;

                if (gridColumn.Index == 0)
                {
                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Count, gridColumn);
                    summary.DisplayFormat = "합계";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Count, gridColumn);
                    summary.DisplayFormat = "최대값";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Count, gridColumn);
                    summary.DisplayFormat = "최소값";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Count, gridColumn);
                    summary.DisplayFormat = "평균";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                }
                else
                {
                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, gridColumn);
                    summary.DisplayFormat = "{0:###,###,##0.####}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_SUM";
                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_SUM"];

                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Maximum, gridColumn);
                    summary.DisplayFormat = "{0:###,###,##0.####}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_MAX";
                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MAX"];

                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Minimum, gridColumn);
                    summary.DisplayFormat = "{0:###,###,##0.####}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_MIN";
                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MIN"];

                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Average, gridColumn);
                    summary.DisplayFormat = "{0:###,###,##0.####}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_AVG";
                    summary = this.griTagListDetail.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_AVG"];
                }
            }
        }

        //그리드의 컬럼과 셀에 TAG Description을 툴팁 텍스트로 설정한다.
        private void setGridToolTip()
        {
            foreach (string tagsn in tagConditions.Keys)
            {
                Hashtable tagCondition = tagConditions[tagsn] as Hashtable;

                foreach (UltraGridColumn col in griTagListDetail.DisplayLayout.Bands[0].Columns)
                {
                    if (tagsn.Equals(col.Header.Caption))
                    {
                        col.Header.ToolTipText = tagCondition["DESCRIPTION"].ToString();
                    }
                }

                foreach (UltraGridRow row in griTagListDetail.Rows)
                {
                    foreach (UltraGridCell cell in row.Cells)
                    {
                        if (tagsn.Equals(cell.Column.Key))
                        {
                            cell.ToolTipText = tagCondition["DESCRIPTION"].ToString();
                        }
                    }
                }
            }
        }

        //조회 후 차트 재설정
        private void InitializeChartAfterSelect()
        {
            foreach (SeriesAttributes sa in chaTagListDetail.Series)
            {
                if (sa.Text == null) continue;

                Hashtable tagCondition = tagConditions[sa.Text] as Hashtable;

                switch ((string)tagCondition["CHART_TYPE"])
                {
                    case "선형":
                        sa.Gallery = Gallery.Lines; break;

                    case "영역형":
                        sa.Gallery = Gallery.Area; break;

                    case "막대형":
                        sa.Gallery = Gallery.Bar; break;
                }

                if (seriesColor == null && (Color)tagCondition["CHART_COLOR"] != Color.Empty)
                {
                    sa.Color = (Color)tagCondition["CHART_COLOR"];
                }

                sa.AxisY = tmpYAxis[tagCondition["TYPE"].ToString()] as AxisY;

                //if (tagCondition["CODE"].Equals("000004") || tagCondition["CODE"].Equals("000007"))      //code : 000004  - 적산차유량, 000007 - 전력량
                //{
                //    sa.Gallery = Gallery.Bar;                   //적산차유량의 그래프를 bar로 표시한다.
                //}

                chaTagListDetail.Series.IndexOf(sa);
                sa.Text += " " + tagCondition["DESCRIPTION"].ToString();
            }

            if (seriesColor == null)
            {
                seriesColor = new Color[chaTagListDetail.Series.Count];

                for (int i = 0; i < chaTagListDetail.Series.Count; i++)
                {
                    seriesColor[i] = chaTagListDetail.Series[i].Color;
                }
            }

            //레전드박스의 태그네임에 DESCRIPTIPON 추가
            //foreach (string tagsn in arrTagList)
            //{
            //    Hashtable tagCondition = tagConditions[tagsn] as Hashtable;

            //    LegendItemAttributesCollection items = chaTagListDetail.LegendBox.ItemAttributes[chaTagListDetail.Series];
                
            //    //LegendItemAttributes item = chaTagListDetail.LegendBox.ItemAttributes[

            //    LegendItem ci = new CustomLegendItem();
            //    ci.Text = tagsn + " " + tagCondition["DESCRIPTION"].ToString();
            //    ci.MarkerShape = MarkerShape.HorizontalLine;
            //    chaTagListDetail.LegendBox.CustomItems.Add(ci);
            //}

            ////조회 순서에 맞게 레전드박스 설정
            //foreach (string tagsn in arrTagList)
            //{
            //    Hashtable tagCondition = tagConditions[tagsn] as Hashtable;

            //    CustomLegendItem ci = new CustomLegendItem();
            //    ci.Text = tagsn + " " + tagCondition["DESCRIPTION"].ToString();
            //    ci.MarkerShape = MarkerShape.HorizontalLine;
            //    chaTagListDetail.LegendBox.CustomItems.Add(ci);
            //}

            //데이터에 해당되는 시리즈가 어떻게 할당될지 알 수 없기때문에 조회한 후에 다시 설정해준다.
            //foreach (SeriesAttributes sa in chaTagListDetail.Series)
            //{
            //    if (sa.Text == null) continue;

            //    Hashtable tagCondition = tagConditions[sa.Text] as Hashtable;

            //    sa.AxisY = tmpYAxis[tagCondition["TYPE"].ToString()] as AxisY;

            //    if (tagCondition["TYPE"].Equals("적산차(㎥/h)"))
            //    {
            //        sa.Gallery = Gallery.Bar;
            //    }

            //    chaTagListDetail.Series.IndexOf(sa);
            //    chaTagListDetail.LegendBox.CustomItems[chaTagListDetail.Series.IndexOf(sa)].Color = sa.Color;

            //    //chaTagListDetail.Series[0].legen
            //}
        }        

        #endregion 내부사용

    }
}