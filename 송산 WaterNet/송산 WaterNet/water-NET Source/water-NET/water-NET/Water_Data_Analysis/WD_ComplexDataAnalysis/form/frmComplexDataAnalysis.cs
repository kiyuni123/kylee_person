﻿/**************************************************************************
 * 파일명   : frmComplexDataAnalysis.cs
 * 작성자   : shpark
 * 작성일자 : 2015.10.29
 * 설명     : 복합데이터시계열분석 메인화면
 * 변경이력 : 2015.10.29 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using WaterNet.WD_Common.utils;
using WaterNet.WD_ComplexDataAnalysis.work;
using WaterNet.WD_ComplexDataAnalysis.form;
using WaterNet.WaterNetCore;
using EMFrame.log;

namespace WaterNet.WD_ComplexDataAnalysis.form
{
    public partial class frmComplexDataAnalysis : Form, WaterNet.WaterNetCore.IForminterface
    {

        #region 1. 전역변수

        private ComplexDataAnalysisWork work = null;
        
        private frmFavoritesTagLoad loadForm;               //즐겨찾기 폼 전역변수
        private Point p;    //컬러 파레트 생성 좌표 설정을 위한 전역변수

        #endregion 전역변수


        #region 2. 초기화

        //메인폼에서 호출
        public void Open()
        {
        }

        //생성자
        public frmComplexDataAnalysis()
        {
            work = ComplexDataAnalysisWork.GetInstance();
            InitializeComponent();
            InitializeComboBox();
            InitializeUltraGrid();
        }

        //콤보박스 초기화
        private void InitializeComboBox()
        {
            //태그구분 콤보박스
            comTagType.DataSource = work.SelectComplexTagTypeList();   
            UltraGridColumn column = this.comTagType.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "Selected";
            column.Header.Caption = string.Empty;
            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;     //체크박스 전체선택/해제
            column.SortIndicator = SortIndicator.Disabled;
            column.DataType = typeof(bool);
            column.Header.VisiblePosition = 0;

            comTagType.CheckedListSettings.CheckStateMember = "Selected";
            comTagType.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
            comTagType.CheckedListSettings.ListSeparator = " | ";       //태그구분 복수 선택 시 구분자
            comTagType.CheckedListSettings.ItemCheckArea = Infragistics.Win.ItemCheckArea.Item;
            comTagType.DisplayMember = "CODE_NAME";
            comTagType.ValueMember = "CODE";
            comTagType.DisplayLayout.Bands[0].Columns["CODE_NAME"].Header.Caption = "태그구분";
            comTagType.DisplayLayout.Bands[0].Columns["CODE"].Hidden = true;
        }

        //그리드 초기화
        private void InitializeUltraGrid()
        {
            ValueList seriesList = new ValueList();     //차트 시리즈 타입 선택을 위한 ValueList
            seriesList.ValueListItems.Add("선형");
            seriesList.ValueListItems.Add("영역형");
            seriesList.ValueListItems.Add("막대형");

            UltraGridColumn tagListBeforeColumn;        //선택전 태그리스트 그리드

            //tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            //tagListBeforeColumn.Key = "MGDPNM";
            //tagListBeforeColumn.Header.Caption = "관리단";
            //tagListBeforeColumn.CellActivation = Activation.NoEdit;
            //tagListBeforeColumn.CellClickAction = CellClickAction.RowSelect;
            //tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Center;
            //tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tagListBeforeColumn.Width = 100;

            tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListBeforeColumn.Key = "CODE_NAME";
            tagListBeforeColumn.Header.Caption = "구분";
            tagListBeforeColumn.CellActivation = Activation.NoEdit;
            tagListBeforeColumn.CellClickAction = CellClickAction.RowSelect;
            tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListBeforeColumn.Width = 90;

            tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListBeforeColumn.Key = "CODE";
            tagListBeforeColumn.Header.Caption = "코드";
            tagListBeforeColumn.CellActivation = Activation.NoEdit;
            tagListBeforeColumn.CellClickAction = CellClickAction.RowSelect;
            tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListBeforeColumn.Width = 80;
            tagListBeforeColumn.Hidden = true;

            tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListBeforeColumn.Key = "TAGNAME";
            tagListBeforeColumn.Header.Caption = "TAGSN";
            tagListBeforeColumn.CellActivation = Activation.NoEdit;
            tagListBeforeColumn.CellClickAction = CellClickAction.RowSelect;
            tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListBeforeColumn.Width = 80;

            tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListBeforeColumn.Key = "DESCRIPTION";
            tagListBeforeColumn.Header.Caption = "TAG DESCRIPTION";
            tagListBeforeColumn.CellActivation = Activation.NoEdit;
            tagListBeforeColumn.CellClickAction = CellClickAction.RowSelect;
            tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListBeforeColumn.Width = 270;

            tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListBeforeColumn.Key = "CHART_TYPE";
            tagListBeforeColumn.Header.Caption = "차트 모양";
            tagListBeforeColumn.CellActivation = Activation.AllowEdit;
            tagListBeforeColumn.CellClickAction = CellClickAction.CellSelect;
            tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListBeforeColumn.ValueList = seriesList;
            tagListBeforeColumn.Width = 75;
            tagListBeforeColumn.Hidden = true;

            tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListBeforeColumn.Key = "CHART_COLOR";
            tagListBeforeColumn.Header.Caption = "차트 색상";
            tagListBeforeColumn.CellActivation = Activation.AllowEdit;
            tagListBeforeColumn.CellClickAction = CellClickAction.RowSelect;
            tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListBeforeColumn.Width = 70;
            tagListBeforeColumn.Hidden = true;

            tagListBeforeColumn = griTagListBeforeSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListBeforeColumn.Key = "COLOR_NAME";
            tagListBeforeColumn.Header.Caption = "색상 이름";
            tagListBeforeColumn.CellActivation = Activation.AllowEdit;
            tagListBeforeColumn.CellClickAction = CellClickAction.RowSelect;
            tagListBeforeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListBeforeColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListBeforeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListBeforeColumn.Width = 75;
            tagListBeforeColumn.Hidden = true;

            ValueList seriesList1 = new ValueList();     //차트 시리즈 타입 선택을 위한 ValueList
            seriesList1.ValueListItems.Add("선형");
            seriesList1.ValueListItems.Add("영역형");
            seriesList1.ValueListItems.Add("막대형");

            UltraGridColumn tagListAfterColumn;     //선택후 태그리스트 그리드

            //tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            //tagListAfterColumn.Key = "MGDPNM";
            //tagListAfterColumn.Header.Caption = "관리단";
            //tagListAfterColumn.CellActivation = Activation.NoEdit;
            //tagListAfterColumn.CellClickAction = CellClickAction.RowSelect;
            //tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Center;
            //tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tagListAfterColumn.Width = 100;

            tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListAfterColumn.Key = "CODE_NAME";
            tagListAfterColumn.Header.Caption = "구분";
            tagListAfterColumn.CellActivation = Activation.NoEdit;
            tagListAfterColumn.CellClickAction = CellClickAction.RowSelect;
            tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListAfterColumn.Width = 90;

            tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListAfterColumn.Key = "CODE";
            tagListAfterColumn.Header.Caption = "코드";
            tagListAfterColumn.CellActivation = Activation.NoEdit;
            tagListAfterColumn.CellClickAction = CellClickAction.RowSelect;
            tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListAfterColumn.Width = 80;
            tagListAfterColumn.Hidden = true;

            tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListAfterColumn.Key = "TAGNAME";
            tagListAfterColumn.Header.Caption = "TAGSN";
            tagListAfterColumn.CellActivation = Activation.NoEdit;
            tagListAfterColumn.CellClickAction = CellClickAction.RowSelect;
            tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListAfterColumn.Width = 80;

            tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListAfterColumn.Key = "DESCRIPTION";
            tagListAfterColumn.Header.Caption = "TAG DESCRIPTION";
            tagListAfterColumn.CellActivation = Activation.NoEdit;
            tagListAfterColumn.CellClickAction = CellClickAction.RowSelect;
            tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListAfterColumn.Width = 270;

            tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListAfterColumn.Key = "CHART_TYPE";
            tagListAfterColumn.Header.Caption = "차트 모양";
            tagListAfterColumn.CellActivation = Activation.AllowEdit;
            tagListAfterColumn.CellClickAction = CellClickAction.CellSelect;
            tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListAfterColumn.ValueList = seriesList1;
            tagListAfterColumn.Width = 75;

            tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListAfterColumn.Key = "CHART_COLOR";
            tagListAfterColumn.Header.Caption = "차트 색상";
            tagListAfterColumn.CellActivation = Activation.AllowEdit;
            tagListAfterColumn.CellClickAction = CellClickAction.RowSelect;
            tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListAfterColumn.Width = 70;

            tagListAfterColumn = griTagListAfterSelect.DisplayLayout.Bands[0].Columns.Add();
            tagListAfterColumn.Key = "COLOR_NAME";
            tagListAfterColumn.Header.Caption = "색상 이름";
            tagListAfterColumn.CellActivation = Activation.AllowEdit;
            tagListAfterColumn.CellClickAction = CellClickAction.RowSelect;
            tagListAfterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListAfterColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListAfterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListAfterColumn.Width = 70;
            tagListAfterColumn.Hidden = true;

            //선택태그의 그리드 이동을 위해 임시 DataTable 생성 
            DataTable tempDT = new DataTable();
            //tempDT.Columns.Add("MGDPNM", typeof(string));
            tempDT.Columns.Add("TAGNAME", typeof(string));
            tempDT.Columns.Add("DESCRIPTION", typeof(string));
            tempDT.Columns.Add("CODE_NAME", typeof(string));
            tempDT.Columns.Add("CODE", typeof(string));
            tempDT.Columns.Add("CHART_TYPE", typeof(string));
            tempDT.Columns.Add("CHART_COLOR", typeof(string));
            tempDT.Columns.Add("COLOR_NAME", typeof(string));
            griTagListAfterSelect.DataSource = tempDT;
        }

        #endregion 초기화


        #region 3. 객체이벤트

        //폼 로딩 이벤트
        private void frmComplexDataAnalysis_Load(object sender, EventArgs e)
        {
            selectTagList();
        }


        //태그구분 콤보박스 선택변경 이벤트
        private void comTagType_AfterCloseUp(object sender, EventArgs e)
        {
            selectTagList();
        }

        //TAGSN 텍스트박스 키입력 이벤트
        private void texTagsn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) || (e.KeyChar == '.'))
            {
                e.Handled = true;
            }
        }

        //TAGSN 텍스트박스 텍스트변경 이벤트
        private void texTagsn_TextChanged(object sender, EventArgs e)
        {
            selectTagList();     
        }

        //DESCRIPTION 엔터키 입력 이벤트
        private void texTagDesc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                selectTagList();
            }
        }

        //즐겨찾기 버튼 클릭 이벤트
        private void butRoadFavorites_Click(object sender, EventArgs e)
        {
            loadForm = new frmFavoritesTagLoad();
            loadForm.Owner = this;
            loadForm.StartPosition = FormStartPosition.CenterParent;
            loadForm.Show();
        }

        //즐겨찾기등록 버튼 클릭 이벤트
        private void butSaveFavorites_Click(object sender, EventArgs e)
        {
            if (griTagListAfterSelect.Rows.Count == 0)
            {
                CommonUtils.ShowInformationMessage("즐겨찾기에 추가할 태그가 선택되지 않았습니다.\n태그를 선택하세요."); return;
            }

            DataTable tagsDT = CommonUtils.ConvertUltraGridToDataTable(griTagListAfterSelect);

            frmFavoritesTagSave saveForm = new frmFavoritesTagSave(tagsDT);
            saveForm.Owner = this;
            saveForm.StartPosition = FormStartPosition.CenterParent;
            saveForm.ShowDialog();
        }

        //태그조회 버튼 클릭 이벤트
        private void butSelectTag_Click(object sender, EventArgs e)
        {
            selectTagList();
        }

        //트렌드보기 버튼 클릭 이벤트
        private void butSelectTagDetail_Click(object sender, EventArgs e)
        {
            if (griTagListAfterSelect.Rows.Count == 0)
            {
                CommonUtils.ShowInformationMessage("선택된 태그가 없습니다."); return;
            }

            ArrayList arrTagList = new ArrayList();         //선택그리드에서 설정한 순서대로 태그를 조회하기 위한 배열
            Hashtable tagConditions = new Hashtable();      //상세그리드에서 팝업으로 tag description 을 보여주기위한 해쉬테이블

            foreach (UltraGridRow row in griTagListAfterSelect.Rows)
            {
                Hashtable tagCondition = new Hashtable();

                arrTagList.Add(row.GetCellValue("TAGNAME").ToString());

                tagCondition.Add("TAGNAME", row.GetCellValue("TAGNAME").ToString());
                tagCondition.Add("DESCRIPTION", row.GetCellValue("DESCRIPTION").ToString());
                tagCondition.Add("CODE_NAME", row.GetCellValue("CODE_NAME").ToString());
                tagCondition.Add("CODE", row.GetCellValue("CODE").ToString());
                tagCondition.Add("CHART_TYPE", row.GetCellValue("CHART_TYPE").ToString());
                tagCondition.Add("CHART_COLOR", row.Cells["CHART_COLOR"].Appearance.BackColor);

                //차트에서 AxisY 텍스트를 표현하기위해 해당하는 값을 할당한다.
                if (row.GetCellValue("CODE").Equals("FRI"))
                {
                    tagCondition.Add("TYPE", "순시유량(㎥/h)");
                }
                else if (row.GetCellValue("CODE").Equals("PRI"))
                {
                    tagCondition.Add("TYPE", "압력(kgf/㎠)");
                }
                else if (row.GetCellValue("CODE").Equals("TD"))
                {
                    tagCondition.Add("TYPE", "적산유량(㎥/h)");
                }
                else if (row.GetCellValue("CODE").Equals("FRQ"))
                {
                    tagCondition.Add("TYPE", "적산차유량(㎥)");
                }
                else if (row.GetCellValue("CODE").Equals("LEI"))
                {
                    tagCondition.Add("TYPE", "수위(m)");
                }
                else if (row.GetCellValue("CODE").Equals("PMB"))
                {
                    tagCondition.Add("TYPE", "펌프가동(On/Off)");
                }
                else if (row.GetCellValue("CODE").Equals("CLI"))
                {
                    tagCondition.Add("TYPE", "잔류염소(CL)");
                }
                else if (row.GetCellValue("CODE").Equals("TBI"))
                {
                    tagCondition.Add("TYPE", "탁도(TB)");
                }
                else if (row.GetCellValue("CODE").Equals("PHI"))
                {
                    tagCondition.Add("TYPE", "PH값(pH)");
                }
                else if (row.GetCellValue("CODE").Equals("TEI"))
                {
                    tagCondition.Add("TYPE", "온도(TE)");
                }
                else if (row.GetCellValue("CODE").Equals("CUI"))
                {
                    tagCondition.Add("TYPE", "전기전도도(CU)");
                }
                else if (row.GetCellValue("CODE").Equals("POI"))
                {
                    tagCondition.Add("TYPE", "밸브개도(%)");
                }

                tagConditions.Add(row.GetCellValue("TAGNAME").ToString(), tagCondition);
            }

            frmComplexDataAnalysisDetail detailForm = new frmComplexDataAnalysisDetail(arrTagList, tagConditions);
            detailForm.Owner = this;
            detailForm.StartPosition = FormStartPosition.CenterParent;
            detailForm.Show();
        }

        //조회태그 그리드의 선택로우를 '선택태그목록'으로 이동(우로 이동)
        private void butMoveRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (griTagListBeforeSelect.Selected.Rows.Count == 0)
                {
                    CommonUtils.ShowInformationMessage("선택된 태그가 없습니다."); return;
                }

                if ((griTagListBeforeSelect.Selected.Rows.Count + griTagListAfterSelect.Rows.Count) > 20)
                {
                    CommonUtils.ShowInformationMessage("최대 20개의 태그를 선택할 수 있습니다."); return;
                }

                CommonUtils.MoveSelectedRowsData(griTagListBeforeSelect, griTagListAfterSelect, "TAGNAME");
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //선택태그 그리드의 선택로우를 '조회태그목록'으로 이동(좌로 이동)
        private void butMoveLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (griTagListAfterSelect.Selected.Rows.Count == 0)
                {
                    CommonUtils.ShowInformationMessage("선택된 태그가 없습니다."); return;
                }

                CommonUtils.MoveSelectedRowsData(griTagListAfterSelect, griTagListBeforeSelect, "TAGNAME");
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //조회태그 그리드의 전체로우를 '선택태그목록'으로 이동(우로 이동)
        private void butMoveRightAll_Click(object sender, EventArgs e)
        {
            try
            {
                int count = 21 - griTagListAfterSelect.Rows.Count;      //선택태그 그리드의 최대 허용 로우는 20개

                for (int i = 0; i < count; i++)
                {
                    if (griTagListBeforeSelect.Rows.Count == 0) return;

                    butMoveRight.PerformClick();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //선택태그 그리드의 전체로우를 '조회태그목록'으로 이동(좌로 이동)
        private void butMoveLeftAll_Click(object sender, EventArgs e)
        {
            try
            {
                int count = griTagListAfterSelect.Rows.Count;

                griTagListAfterSelect.Selected.Rows.Clear();

                CommonUtils.SelectFirstRow(griTagListAfterSelect);

                for (int i = 0; i < count; i++)
                {
                    butMoveLeft.PerformClick();
                }
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //선택태그 그리드의 선택로우를 한단계 위로 이동
        private void butMoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                CommonUtils.MoveRowUpDown(griTagListAfterSelect, "UP");
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //선택태그 그리드의 선택로우를 한단계 아래로 이동
        private void butMoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                CommonUtils.MoveRowUpDown(griTagListAfterSelect, "DOWN");
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //선택태그 그리드의 선택로우를 그리드의 첫번째 로우로 이동
        private void butMoveFirst_Click(object sender, EventArgs e)
        {
            try
            {
                CommonUtils.MoveRowToFirst(griTagListAfterSelect);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.Message);
                Logger.Error(ex.ToString());
            }
        }

        //선택태그 그리드 차트 모양설정
        private void griTagListAfterSelect_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            SetChartSeriesType(griTagListAfterSelect);
        }

        //선택태그 그리드 컬러 색상 변경
        private void griTagListAfterSelect_ClickCellButton(object sender, CellEventArgs e)
        {
            try
            {
                //컬러팔레트에서 선택한 색상을 그리드의 셀과 버튼 컬러로 설정한다.
                ColorPaletteDialog cpd = new ColorPaletteDialog(p.X, p.Y);

                if (cpd.ShowDialog() == DialogResult.OK)
                {
                    e.Cell.Row.Cells["CHART_COLOR"].Appearance.BackColor = cpd.Color;
                    e.Cell.Row.Cells["CHART_COLOR"].ButtonAppearance.BackColor = cpd.Color;
                    e.Cell.Row.Cells["CHART_COLOR"].Value = string.Empty;

                    e.Cell.Row.Cells["COLOR_NAME"].Value = ColorTranslator.ToHtml(cpd.Color);
                }
            }
            catch (Exception)
            {
            }
        }

        //클릭 한 셀의 좌표를 계산하기 위한 이벤트
        private void griTagListAfterSelect_MouseEnterElement(object sender, UIElementEventArgs e)
        {
            try
            {
                if (e.Element is CellUIElement)
                {
                    CellUIElement cElement = (CellUIElement)e.Element;
                    UltraGridCell cell = cElement.Cell;

                    if (cell.Column.Key.Equals("CHART_COLOR"))
                    {
                        CellUIElement element = (CellUIElement)cell.GetUIElement();
                        Rectangle rect = griTagListAfterSelect.RectangleToScreen(element.Rect);
                        p = new Point(rect.X, rect.Y);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        #region + 선택태그 그리드 마우스 드래그로 로우 이동

        private void griTagListAfterSelect_SelectionDrag(object sender, CancelEventArgs e)
        {
            griTagListAfterSelect.DoDragDrop(griTagListAfterSelect.Selected.Rows, DragDropEffects.Move);
        }

        private void griTagListAfterSelect_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
            UltraGrid afterGrid = sender as UltraGrid;
            Point pointInGridCoords = afterGrid.PointToClient(new Point(e.X, e.Y));
            if (pointInGridCoords.Y < 10)
            {
                this.griTagListAfterSelect.ActiveRowScrollRegion.Scroll(RowScrollAction.LineUp);
            }
            else if (pointInGridCoords.Y > afterGrid.Height - 10)
            {
                this.griTagListAfterSelect.ActiveRowScrollRegion.Scroll(RowScrollAction.PageDown);
            }
        }

        private void griTagListAfterSelect_DragDrop(object sender, DragEventArgs e)
        {
            int dropRowIndex;

            UIElement uieOver = griTagListAfterSelect.DisplayLayout.UIElement.ElementFromPoint(griTagListAfterSelect.PointToClient(new Point(e.X, e.Y)));
            UltraGridRow rowOver = uieOver.GetContext(typeof(UltraGridRow), true) as UltraGridRow;

            if (rowOver != null)
            {
                dropRowIndex = rowOver.Index;

                SelectedRowsCollection selRows = (SelectedRowsCollection)e.Data.GetData(typeof(SelectedRowsCollection)) as SelectedRowsCollection;

                foreach (UltraGridRow row in selRows)
                {
                    griTagListAfterSelect.Rows.Move(row, dropRowIndex);
                }

                griTagListAfterSelect.Rows[dropRowIndex].Activate();
                griTagListAfterSelect.Rows[dropRowIndex].Selected = true;
            }
        }

        #endregion 선택태그 그리드 마우스 드래그로 로우 이동

        #endregion 객체이벤트


        #region 4. 상속

        #region + IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion IForminterface 멤버

        #endregion 상속


        #region 5. 외부호출

        public frmFavoritesTagLoad LoadForm
        {
            set
            {
                this.loadForm = null;
            }
        }

        #endregion 외부호출


        #region 6. 내부사용

        //태그리스트 조회
        private void selectTagList()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable typeConditions = new Hashtable();         //태그구분 콤보박스의 선택 정보를 저장
                Hashtable selectConditions = new Hashtable();       //태그구분 외 검색조건 저장

                if (this.comTagType.CheckedRows.Count != 0)
                {
                    foreach (UltraGridRow row in this.comTagType.CheckedRows)
                    {
                        typeConditions.Add(row.Cells["CODE"].Value.ToString(), row.Cells["CODE_NAME"].Value.ToString());
                        if (row.Cells["CODE"].Value.Equals("FRI"))
                        {
                            typeConditions.Add("RT", "순시유량");
                            typeConditions.Add("MNF", "야간최소유량");
                            typeConditions.Add("JU_O", "정수장유출");
                        }
                        else if (row.Cells["CODE"].Value.Equals("TD"))
                        {
                            typeConditions.Add("TT", "전체적산");                         
                        }
                        else if (row.Cells["CODE"].Value.Equals("FRQ"))
                        {
                            typeConditions.Add("FRQ_I", "배수지유입");
                            typeConditions.Add("FRQ_O", "배수지유출");
                            typeConditions.Add("SPL_D", "분기유입적산");
                            typeConditions.Add("SPL_I", "배수지유입적산");
                            typeConditions.Add("SPL_O", "배수지유출적산");
                            typeConditions.Add("YD", "전일적산");
                            typeConditions.Add("YD_D", "분기점전일적산");
                            typeConditions.Add("YD_R", "배수지유출전일적산");
                        }
                        else if (row.Cells["CODE"].Value.Equals("POI"))
                        {
                            typeConditions.Add("POI_IN", "배수지 유입밸브 개도");
                            typeConditions.Add("POI_OU", "배수지 유출밸브 개도");
                        }
                    }
                }

                selectConditions.Add("TAGNAME", texTagsn.Text);
                selectConditions.Add("DESCRIPTION", texTagDesc.Text);

                DataTable beforeDT = work.SelectComplexTagList(typeConditions, selectConditions);
                DataTable afterDT = griTagListAfterSelect.DataSource as DataTable;

                var compareData = beforeDT.AsEnumerable().Except(afterDT.AsEnumerable(), DataRowComparer.Default);

                if (compareData.Any())
                {
                    griTagListBeforeSelect.DataSource = compareData.CopyToDataTable();
                }
                else
                {
                    griTagListBeforeSelect.DataSource = beforeDT;
                }

                CommonUtils.SelectFirstRow(griTagListBeforeSelect);
            }
            catch (Exception ex)
            {
                CommonUtils.ShowExceptionMessage(ex.ToString());
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //그리드에서 차트 시리즈의 타입을 설정한다.
        private void SetChartSeriesType(UltraGrid grid)
        {
            foreach (UltraGridRow row in grid.Rows)
            {
                if ((row.Cells["CHART_TYPE"].Value == DBNull.Value))
                {
                    if (((string)row.Cells["CODE"].Value).Equals("FRQ") || ((string)row.Cells["CODE"].Value).Equals("FRQ_O") || ((string)row.Cells["CODE"].Value).Equals("FRQ_I"))   //000004 - 적산차유량, 000007 - 전력량
                    {
                        row.Cells["CHART_TYPE"].Value = "막대형";
                    }
                    else
                    {
                        row.Cells["CHART_TYPE"].Value = "선형";
                    }
                }

                if (row.Cells["CHART_COLOR"].Appearance.BackColor == Color.Empty)
                {
                    row.Cells["CHART_COLOR"].Value = "자동";
                }
            }
        }

        #endregion 내부사용

    }
}
