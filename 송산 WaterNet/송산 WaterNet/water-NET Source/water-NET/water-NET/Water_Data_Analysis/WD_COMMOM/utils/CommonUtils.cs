﻿//KH$&0000002
/* 수정사항
 * 2014.10.30 입력값 validation check 추가
 * 2014.10.30 컨트롤 내용 클리어 추가
 * 
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;

namespace WaterNet.WD_Common.utils
{
    public class CommonUtils
    {
        //DataTable을 Hashtable형태로 변경 
        public static Hashtable ConvertDataTableToHashtable(DataTable dt, string keyField, string valueField)
        {
            Hashtable result = new Hashtable();
             
            foreach(DataRow row in dt.Rows)
            {
                result.Add(row[keyField].ToString(), row[valueField].ToString());
            }

            return result;
        }

        //UltraGrid to DataTable
        public static DataTable ConvertUltraGridToDataTable(UltraGrid grid)
        {
            DataTable dt = null;

            if (grid.Rows.Count == 0)
            {
                return dt;
            }

            dt = new DataTable();

            UltraGridBand band = grid.DisplayLayout.Bands[0];

            foreach (UltraGridColumn col in band.Columns)
            {
                dt.Columns.Add(col.Key, col.DataType);
            }

            foreach (UltraGridRow row in grid.Rows)
            {
                List<object> cellValues = new List<object>(row.Cells.Count);

                foreach (UltraGridCell cell in row.Cells)
                {
                    cellValues.Add(cell.Value);
                }

                dt.Rows.Add(cellValues.ToArray());
            }

            return dt;
        }

        //Hashtable의 내용을 Console에 출력
        public static void WriteHashtableToConsole(Hashtable tmpTable)
        {
            Console.WriteLine("== write start ==");

            foreach(string key in tmpTable.Keys)
            {
                Console.WriteLine(key + " : " + tmpTable[key]);
            }

            Console.WriteLine("== write end ==");
            Console.WriteLine("total count : " + tmpTable.Count);
        }

        //ArrayList의 내용을 Console에 출력
        public static void WriteArrayListToConsole(ArrayList tmpList)
        {
            Console.WriteLine("== write start ==");

            for (int i = 0; i < tmpList.Count; i++ )
            {
                Console.WriteLine(tmpList[i]);
            }

            Console.WriteLine("== write end ==");
            Console.WriteLine("total count : " + tmpList.Count);
        }

        //숫자인지 확인
        public static bool IsNumber(string value)
        {
            value = value.Replace("-", "");
            string check = value.Trim();

            bool returnVal = false;
            int dotCount = 0;
            for (int i = 0; i < check.Length; i++)
            {
                if (System.Char.IsNumber(check, i))
                    returnVal = true;
                else
                {
                    if (check.Substring(i, 1) == ".")
                    {
                        returnVal = true;
                        dotCount++;
                    }
                    else
                    {
                        returnVal = false;
                        break;
                    } 
                }
            }

            if (dotCount > 1)
            {
                returnVal = false;
            }

            return returnVal;
        }
    
        //grid에 행추가 : 스크롤 이동 소스 추가 2014.11.19 추가_shpark
        public static void AddGridRow(UltraGrid grid)
        {
            DataTable dataTable = grid.DataSource as DataTable;
            dataTable.Rows.Add(dataTable.NewRow());

            grid.Rows[grid.Rows.Count - 1].Selected = true;
            grid.Rows[grid.Rows.Count - 1].Activate();
            grid.ActiveRowScrollRegion.ScrollRowIntoView(grid.Rows[dataTable.Rows.Count - 1]);
        }

        //grid에 행추가 (초기값 존재)
        public static void AddGridRow(UltraGrid grid, Hashtable initValueSet)
        {
            DataTable dataTable = grid.DataSource as DataTable;
            DataRow newRow = dataTable.NewRow();

            foreach (string key in initValueSet.Keys)
            {
                newRow[key] = initValueSet[key].ToString();
            }

            dataTable.Rows.Add(newRow);

            grid.Rows[grid.Rows.Count - 1].Selected = true;
            grid.Rows[grid.Rows.Count - 1].Activate();
        }

        //grid에 행삭제
        public static Hashtable DeleteGridRow(UltraGrid grid, string message)
        {
            DataTable dt = grid.DataSource as DataTable;
            Hashtable deleteData = null;

            if (grid.Selected.Rows.Count != 0)
            {

                if (!"".Equals(message))
                {
                    if (MessageBox.Show(message, "데이터삭제", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    {
                        return null;
                    }
                }

                int idx = grid.Selected.Rows[0].Index;
                DataRow realDataRow = ((DataRowView)grid.Rows[idx].ListObject).Row;

                if (realDataRow.RowState != DataRowState.Added)
                {
                    deleteData = new Hashtable();

                    foreach (DataColumn column in dt.Columns)
                    {
                        deleteData.Add(column.ToString(), ((DataRowView)grid.Rows[idx].ListObject).Row[column.ToString()].ToString());
                    }
                }

                dt.Rows.RemoveAt(dt.Rows.IndexOf(realDataRow));

                if (dt.Rows.Count > 0)
                {
                    int newIdx = 0;

                    if (idx - 1 < 0)
                    {
                        newIdx = 0;
                    }
                    else
                    {
                        newIdx = idx - 1;
                    }

                    grid.Rows[newIdx].Selected = true;
                    grid.Rows[newIdx].Activate();
                }
            }

            return deleteData;
        }

        //grid의 행 삭제시 행이 수정된 후 DB에 저장된 상태가 아니면 그리드 상에서 행만 삭제한다. : 2014.11.19 추가_shpark
        public static bool CheckDeleteGridRow(UltraGrid grid)
        {
            int idx = grid.Selected.Rows[0].Index;
            DataRow realDataRow = ((DataRowView)grid.Rows[idx].ListObject).Row;
            DataTable dt = (DataTable)grid.DataSource;

            //DataRow의 상태확인
            if (realDataRow.RowState == DataRowState.Added)
            {
                //추가인 경우 그냥 삭제만 한다.
                dt.Rows.RemoveAt(dt.Rows.IndexOf(realDataRow));

                //삭제하고 행이 남았을 경우만 포커싱
                if (grid.Rows.Count > 0)
                {
                    int count = dt.Rows.Count;

                    grid.Rows[count - 1].Selected = true;
                    grid.ActiveRowScrollRegion.ScrollRowIntoView(grid.Rows[count - 1]);
                }

                return false;
            }

            return true;
        }

        //grid에서 선택된 행/컬럼의 값을 발췌 : 2014.11.07 추가_shpark
        public static string GetGridCellValue(UltraGrid grid, string columnName)
        {
            string result = "";

            if (grid.Selected.Rows.Count > 0)
            {
                result = grid.Selected.Rows[0].Cells[columnName].Value.ToString();
            }

            return result;
        }

        //전달받은 grid row 에서 컬럼의 값을 발췌 : 2015.11.18 추가_shpark
        public static string GetRowCellValue(UltraGridRow row, string columnName)
        {
            string result = "";

            result = row.Cells[columnName].Value.ToString();

            return result;
        }

        //Grid 내부에 변경데이터가 있는지 확인 (DataTable 을 전달받는다)
        public static bool HaveChangedData(DataTable data)
        {
            bool result = false;

            foreach (DataRow row in data.Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                {
                    result = true;
                }
            }

            return result;
        }

        //Grid 내부에 변경데이터가 있는지 확인 Overroad (grid 를 전달받는다) : 2014.11.07 추가_shpark
        public static bool CheckChangedGrid(UltraGrid grid)
        {
            bool result = false;

            foreach (DataRow row in ((DataTable)grid.DataSource).Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                {
                    result = true;
                }
            }

            return result;
        }

        //Grid와 컬럼 키를 전달받아 해당 키의 로우에 비어있는 데이터가 있는지 검사한다  : 2014.11.07 추가_shpark
        public static bool CheckNullGrldRow(UltraGrid grid, string columnKey)
        {
            bool result = false;

            foreach (DataRow row in ((DataTable)grid.DataSource).Rows)
            {
                if ("".Equals(row[columnKey]))
                {
                    result = true;
                }
            }

            return result;
        }

        //Grid의 첫행을 select/activation : 2014.11.21 스크롤 포지션 코드 추가_shpark
        public static void SelectFirstRow(UltraGrid grid)
        {
            if (grid.Rows.Count > 0)
            {
                grid.Rows[0].Selected = true;
                grid.Rows[0].Activate();
                grid.ActiveRowScrollRegion.ScrollPosition = 0;
            }
        }

        public static void SelectEndRow(UltraGrid grid)
        {
            if (grid.Rows.Count > 0)
            {
                int index = grid.Rows.Count - 1;
                grid.Rows[index].Selected = true;
                grid.Rows[index].Activate();
            }
        }

        //컨트롤로 전달되는 그리드 조회 완료 시 Grid의 첫행을 select : 2014.10.30 추가_shpark
        public static void SelectFirstRow(Control ctrs)
        {
            foreach (Control ctr in ctrs.Controls)
            {
                if (ctr.Controls.Count > 0)
                {
                    SelectFirstRow(ctr);
                }

                if ("Infragistics.Win.UltraWinGrid.UltraGrid".Equals(ctr.GetType().ToString()))
                {
                    if (((UltraGrid)ctr).Rows.Count > 0)
                    {
                        ((UltraGrid)ctr).Rows[0].Selected = true;
                    }
                }
            }
        }

        //grid의 선택된 로우의 위치를 위 또는 아래로 한칸씩 이동    : 2015-02-04_shpark
        public static void MoveRowUpDown(UltraGrid grid, string upDown)
        {
            if (grid.Selected.Rows.Count == 0) return;

            UltraGridRow row = grid.ActiveRow;
            int rowIndex = grid.Selected.Rows[0].Index;

            if (upDown.Equals("UP"))
            {
                rowIndex = rowIndex == 0 ? 0 : rowIndex - 1;
            }
            else if (upDown.Equals("DOWN"))
            {
                rowIndex = rowIndex == grid.Rows.Count - 1 ? rowIndex : rowIndex + 1;
            }

            grid.Rows.Move(row, rowIndex);

            grid.Rows[rowIndex].Activate();
            grid.Rows[rowIndex].Selected = true;
        }

        //grid의 선택된 로우의 위치를 그리드의 첫번째로 이동    : 2015-11-10_shpark
        public static void MoveRowToFirst(UltraGrid grid)
        {
            if (grid.Selected.Rows.Count == 0) return;

            UltraGridRow row = grid.ActiveRow;

            grid.Rows.Move(row, 0);

            SelectFirstRow(grid);
        }

        //두개의 그리드에서 선택로우의 데이터를 해쉬로 저장하여 이동한다.
        public static void MoveSelectedRowData(UltraGrid souGrid, UltraGrid desGrid, Hashtable rowConditions)
        {
            int activeIndex = souGrid.Selected.Rows[0].Index == 0 ? 0 : souGrid.Selected.Rows[0].Index - 1;

            AddGridRow(desGrid, rowConditions);

            souGrid.Selected.Rows[0].Delete(false);
            (souGrid.DataSource as DataTable).AcceptChanges();

            if (souGrid.Rows.Count != 0)
            {
                souGrid.Rows[activeIndex].Activate();
                souGrid.Rows[activeIndex].Selected = true;
            }
        }

        //두개의 그리드에서 선택로우를 이동한다.    :2015-11-24_shpark
        public static void MoveSelectedRowsData(UltraGrid souGrid, UltraGrid desGrid, string key)
        {
            int selectedIndex = 0;

            foreach (UltraGridRow row in souGrid.Rows)
            {
                if (row.Selected)
                {
                    selectedIndex = row.Index;
                }
            }

            int activeIndex = souGrid.Rows.Count == selectedIndex + 1 ? selectedIndex - souGrid.Selected.Rows.Count : selectedIndex - souGrid.Selected.Rows.Count + 1;

            //int activeIndex = souGrid.Selected.Rows[0].Index == 0 ? 0 : souGrid.Selected.Rows[0].Index - 1;
            //int activeIndex = souGrid.Rows.Count - souGrid.Selected.Rows.Count - 1;

            ArrayList selectKey = new ArrayList();
            DataTable souDT = souGrid.DataSource as DataTable;
            DataTable desDT = desGrid.DataSource as DataTable;

            for (int i = 0; i < souGrid.Rows.Count; i++)
            {
                UltraGridRow row = souGrid.Rows[i];

                if (row.Selected)
                {
                    selectKey.Add(row.Cells[key].Value.ToString());
                    DataRow tempRow = ((DataRowView)souGrid.Rows[i].ListObject).Row;

                    desDT.ImportRow(tempRow);
                    souDT.Rows.Remove(tempRow);

                    i--;
                }
            }

            souGrid.DataSource = souDT;
            desGrid.DataSource = desDT;

            if (souGrid.Rows.Count != 0)
            {
                souGrid.Selected.Rows.Clear();
                souGrid.Rows[activeIndex].Activate();
                souGrid.Rows[activeIndex].Selected = true;
            }

            desGrid.Selected.Rows.Clear();

            foreach (UltraGridRow row in desGrid.Rows)
            {
                foreach (string value in selectKey)
                {
                    if (row.Cells[key].Value.ToString().Equals(value))
                    {
                        row.Activate();
                        row.Selected = true;
                    }
                }
            }
        }

        //텍스트박스, 그리드, 필드 키를 전달받아 텍스트박스의 입력 내용을 선택한 필드의 로우에 반영한다 : 2014.10.30 추가_shpark
        public static void TextBoxToGrid(TextBox tb, UltraGrid grid, string fieldKey)
        {
            if (grid.Selected.Rows.Count != 0)
            {
                if (!(tb.Text).Equals(grid.Selected.Rows[0].Cells[fieldKey].Value.ToString()))
                {
                    grid.Selected.Rows[0].Cells[fieldKey].Value = tb.Text;
                    grid.UpdateData();
                }
            }
        }

        //해당 값을 grid의 값으로 할당
        public static void SetGridCellValue(UltraGrid grid, string columnName, string value)
        {
            if (grid.Selected.Rows.Count > 0)
            {
                if (!value.Equals(grid.Selected.Rows[0].Cells[columnName].Value.ToString()))
                {
                    grid.Selected.Rows[0].Cells[columnName].Value = value;
                    grid.UpdateData();
                }
            }
        }


        //입력값 널 허용, 숫자 입력, 길이 check : 2014.10.30 추가_shpark
        //nullAllow - true : 공백허용, isNumeric - true : 숫자만 허용
        public static bool InputValidation(string text, bool nullAllow, int size, bool isNumeric)
        {

            if (!nullAllow && "".Equals(text))
            {
                MessageBox.Show("값을 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (isNumeric && !IsNumber(text))
            {
                MessageBox.Show("숫자값만 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (System.Text.Encoding.GetEncoding(949).GetByteCount(text) > size)
            {
                Console.WriteLine("ByteCount : " + System.Text.Encoding.GetEncoding(949).GetByteCount(text));
                MessageBox.Show("입력값이 너무 깁니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        //입력값 널 허용, 숫자 입력, 길이 check ovverride - 표출 내용 추가 : 2014.10.30 추가_shpark
        //nullAllow - true : 공백허용, isNumeric - true : 숫자만 허용
        public static bool InputValidation(string text, bool nullAllow, int size, bool isNumeric, string title)
        {

            if (!nullAllow && "".Equals(text))
            {
                MessageBox.Show("'" + title + "' 항목이 입력되지 않았습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (isNumeric && !IsNumber(text))
            {
                MessageBox.Show("'" + title + "'에는 숫자만 입력할 수 있습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (System.Text.Encoding.GetEncoding(949).GetByteCount(text) > size)
            {
                Console.WriteLine("ByteCount : " + System.Text.Encoding.GetEncoding(949).GetByteCount(text));
                MessageBox.Show("'" + title + "' 의 입력값이 너무 깁니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        //grid data 저장 전 상태 확인 : 2014.11.07 추가_shpark
        public static bool SaveGridData(UltraGrid grid, Hashtable validationSet)
        {
            if (grid.Rows.Count == 0)
            {
                return false;
            }

            bool result = false;

            if (!HaveChangedData(grid.DataSource as DataTable))
            {
                MessageBox.Show("변경된 데이터가 없습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //validation
                if (validationSet != null)
                {
                    if (!ValidationGridData(grid, validationSet))
                    {
                        return false;
                    }
                }

                if (MessageBox.Show("저장하시겠습니까?", "데이터저장", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    result = true;
                }
            }

            return result;
        }

        //grid data validation : 2014.11.07 추가_shpark
        public static bool ValidationGridData(UltraGrid grid, Hashtable validationSet)
        {
            DataTable tmpDt = grid.DataSource as DataTable;

            foreach (DataRow row in tmpDt.Rows)
            {
                if (row.RowState == DataRowState.Unchanged)
                {
                    continue;
                }

                foreach (DataColumn column in tmpDt.Columns)
                {
                    if (validationSet[column.ToString()] != null)
                    {
                        string headerCaption = grid.DisplayLayout.Bands[0].Columns[column.ColumnName].Header.Caption;
                        string[] validationData = validationSet[column.ToString()].ToString().Split('|');

                        //사이즈 check
                        if (int.Parse(validationData[0]) < System.Text.Encoding.GetEncoding(949).GetByteCount((row[column.ToString()].ToString())))
                        {
                            MessageBox.Show("입력값이 너무 큽니다. \n" + headerCaption + " : " + row[column], "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }

                        //Null 허용 Check
                        if ("N".Equals(validationData[1].ToUpper()) && "".Equals(row[column].ToString()))
                        {
                            MessageBox.Show("[" + headerCaption + "] 항목에 빈값이 올 수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }

                        //숫자여부 확인
                        if ("Y".Equals(validationData[2].ToUpper()) && !IsNumber(row[column].ToString()))
                        {
                            MessageBox.Show("[" + headerCaption + "] 항목은 숫자만 입력가능합니다. \n" + headerCaption + " : " + row[column], "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        //조회결과 표출 컨트롤 초기화 : 2014.10.30 추가_shpark
        public static void ClearControlData(Control ctrs)
        {
            foreach (Control ctr in ctrs.Controls)
            {
                if (ctr.Controls.Count > 0)
                {
                    ClearControlData(ctr);
                }

                if ("System.Windows.Forms.TextBox".Equals(ctr.GetType().ToString()))
                {
                    //텍스트박스 초기화
                    ((TextBox)ctr).Text = "";
                }
                else if ("System.Windows.Forms.ComboBox".Equals(ctr.GetType().ToString()))
                {
                    //콤보박스 초기화
                    ((ComboBox)ctr).SelectedValue = "";
                }
                else if ("Infragistics.Win.UltraWinGrid.UltraGrid".Equals(ctr.GetType().ToString()))
                {
                    //그리드 초기화
                    if (((UltraGrid)ctr).Rows.Count > 0)
                    {
                        //((UltraGrid)ctr).Rows.Dispose();
                        ((DataTable)((UltraGrid)ctr).DataSource).Clear();
                    }
                }
            }
        }

        //label 에 정보를 표시한다. : 2014.11.07 추가_shpark ###테스트 중
        public static void ShowLabelMassage(Label lab, string info)
        {
            lab.Text = info;
            lab.Refresh();
        }

        //Exception 시 메세지박스 호출
        public static void ShowExceptionMessage(string message)
        {
            MessageBox.Show(message, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //정보성 메세지박스 호출
        public static void ShowInformationMessage(string message)
        {
            MessageBox.Show(message, "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //예/아니오 Dialog box 호출
        public static DialogResult ShowYesNoDialog(string message)
        {
            return MessageBox.Show(message, "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
 
        //시리얼번호 생성
        public static String GetSerialNumber(string workFlag)
        {
            Random random = new Random();

            return workFlag
                    + DateTime.Now.Year.ToString()
                    + (DateTime.Now.Month.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Day.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0')
                    + (random.Next(1, 9999).ToString()).PadLeft(4, '0');
        }

        //dateTimePiker의 날짜 또는 시간을 문자열로 반환한다.   //2015-11-13_shpark
        public static string ConvertDateTimePicker(DateTimePicker dtp, string type)
        {
            string result = string.Empty;

            if (type.Equals("DATE"))
            {
                string month = (dtp.Value.Month.ToString()).Length == 1 ? "0" + dtp.Value.Month.ToString() : dtp.Value.Month.ToString();
                string day = (dtp.Value.Day.ToString()).Length == 1 ? "0" + dtp.Value.Day.ToString() : dtp.Value.Day.ToString();

                result = dtp.Value.Year.ToString() + month + day;
            }
            else if (type.Equals("TIME"))
            {
                string hour = (dtp.Value.Hour.ToString()).Length == 1 ? "0" + dtp.Value.Hour.ToString() : dtp.Value.Hour.ToString();
                string minute = (dtp.Value.Minute.ToString()).Length == 1 ? "0" + dtp.Value.Minute.ToString() : dtp.Value.Minute.ToString();

                result = hour + minute;
            }
            else if (type.Equals("DATE_TIME"))
            {
                string month = (dtp.Value.Month.ToString()).Length == 1 ? "0" + dtp.Value.Month.ToString() : dtp.Value.Month.ToString();
                string day = (dtp.Value.Day.ToString()).Length == 1 ? "0" + dtp.Value.Day.ToString() : dtp.Value.Day.ToString();
                string hour = (dtp.Value.Hour.ToString()).Length == 1 ? "0" + dtp.Value.Hour.ToString() : dtp.Value.Hour.ToString();
                string minute = (dtp.Value.Minute.ToString()).Length == 1 ? "0" + dtp.Value.Minute.ToString() : dtp.Value.Minute.ToString();

                result = dtp.Value.Year.ToString() + month + day + hour + minute;
            }

            return result;
        }
    }
}
