﻿using System;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.IO;
using Infragistics.Win.UltraWinEditors;
using System.Data;
using System.Collections;

namespace WaterNet.WD_Common.utils
{
    /// <summary>
    /// 공통으로 사용되는 유틸모음 클래스이다.
    /// </summary>
    public class Utils
    {
        public static DataTable ColumnMatch(UltraGrid ultraGrid, DataTable dataTable)
        {
            if (dataTable != null)
            {
                for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                {
                    if (ultraGrid.DisplayLayout.Bands[0].Columns.IndexOf(dataTable.Columns[i].ColumnName) == -1)
                    {
                        dataTable.Columns.Remove(dataTable.Columns[i].ColumnName);
                    }
                }
            }
            return dataTable;
        }

        public static bool HashOwnedForm(Form form, Type type)
        {
            bool result = false;
            foreach (Form oForm in form.OwnedForms)
            {
                if (oForm.GetType().Equals(type))
                {
                    oForm.WindowState = FormWindowState.Normal;
                    result = true;
                }
            }
            return result;
        }

        public static Hashtable ConverToHashtable(object target)
        {
            Hashtable parameter = null;

            if (target is UltraGridRow)
            {
                UltraGridRow row = target as UltraGridRow;
                parameter = new Hashtable();

                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.Value is DateTime)
                    {
                        parameter.Add(cell.Column.Key, Convert.ToDateTime(cell.Value).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        if (cell.Column.Key.Equals("SUPY_AREA") && cell.Value.Equals(DBNull.Value))
                            parameter.Add(cell.Column.Key, "");
                        else
                            parameter.Add(cell.Column.Key, cell.Value);
                    }
                }
            }
            else if (target is Control)
            {
                Control control = target as Control;
                parameter = new Hashtable();
                AddHashtableToControls(parameter, control);
            }
            else if (target is DataRow)
            {
                DataRow row = target as DataRow;
                parameter = new Hashtable();

                foreach (DataColumn column in row.Table.Columns)
                {
                    if (row[column] is DateTime)
                    {
                        parameter.Add(column.ColumnName, Convert.ToDateTime(row[column]).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        parameter.Add(column.ColumnName, row[column]);
                    }
                }
            }

            return parameter;
        }

        public static DataTable ConverToDataTable(object target)
        {
            DataTable result = null;

            if (target is Hashtable)
            {
                result = new DataTable();
                Hashtable hash = target as Hashtable;

                foreach (string key in hash.Keys)
                {
                    if (hash.ContainsKey(key) && hash[key] != null)
                    {
                        result.Columns.Add(key, hash[key].GetType());
                    }
                    else
                    {                        
                        result.Columns.Add(key);
                    }
                }

                DataRow dataRow = result.NewRow();

                foreach (string key in hash.Keys)
                {
                    dataRow[key] = hash[key];
                }

                result.Rows.Add(dataRow);
            }
            return result;
        }

        public static void AddHashtableToControls(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    AddHashtableToControls(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();
                    object parameterValue = null;

                    if (control is TextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        parameterValue = combobox.SelectedValue;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        parameterValue = Convert.ToDateTime(date.Value).ToString("yyyyMMdd");

                        string strDate = parameterValue as string;

                        if (strDate == "00010101")
                        {
                            parameterValue = string.Empty;
                        }

                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is RichTextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is CheckBox)
                    {
                        CheckBox cb = control as CheckBox;
                        parameterValue = cb.Checked.ToString();
                        if (parameterValue == null)
                        {
                            parameterValue = "false";
                        }
                        parameter.Add(parameterName, parameterValue);
                    }
                }
            }
        }

        public static void ClearControls(Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    ClearControls(control);
                }
                else
                {
                    if (control is TextBox)
                    {
                        control.Text = string.Empty;
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (combobox.Items.Count > 0)
                        {
                            combobox.SelectedIndex = 0;
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        date.Value = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }

                    if (control is RichTextBox)
                    {
                        control.Text = string.Empty;
                    }
                }
            }
        }

        public static void SetControlDataSetting(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    SetControlDataSetting(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();

                    if (control is TextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (parameter.ContainsKey(parameterName))
                        {
                            combobox.SelectedValue = parameter[parameterName];
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        if (parameter.ContainsKey(parameterName))
                        {
                            date.Value = parameter[parameterName].ToString();
                        }
                    }

                    if (control is RichTextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is CheckBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            CheckBox cb = control as CheckBox;
                            cb.Checked = Convert.ToBoolean(parameter[parameterName]);
                        }
                    }
                }
            }
        }


        //public static void SetValueList(object target, VALUELIST_TYPE type, string pcode)
        //{
        //    //대상에 따라서 객체는 두가지로 나뉘어질수있음
        //    //1. 일반 콤보박스
        //    //2. 그리드의 ValueLit

        //    ValueList valueList = target as ValueList;
        //    ComboBox comboBox = null;

        //    if (valueList == null)
        //    {
        //        comboBox = target as ComboBox;

        //        if (comboBox == null)
        //        {
        //            return;
        //        }

        //        comboBox.ValueMember = "CODE";
        //        comboBox.DisplayMember = "CODE_NAME";

        //        if (type == VALUELIST_TYPE.SINGLE_YEAR)
        //        {
        //            comboBox.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_MONTH)
        //        {
        //            comboBox.DataSource = DateUtils.GetMonth("CODE", "CODE_NAME");
        //        }
        //        else if (type == VALUELIST_TYPE.CODE)
        //        {
        //            if (pcode == null)
        //            {
        //                return;
        //            }
        //            comboBox.DataSource = CodeWork.GetInstance().SelectCodeList(pcode).Tables[0];
        //        }
        //    }

        //    else if (valueList != null)
        //    {
        //        DataTable table = null;

        //        if (type == VALUELIST_TYPE.SINGLE_YEAR)
        //        {
        //            table = DateUtils.GetYear(10, "CODE", "CODE_NAME");

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_MONTH)
        //        {
        //            table = DateUtils.GetMonth("CODE", "CODE_NAME");

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_DAY)
        //        {
        //            table = DateUtils.GetDay("CODE", "CODE_NAME");

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_HOUR)
        //        {
        //            table = DateUtils.GetHours("CODE", "CODE_NAME");

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.CODE)
        //        {
        //            if (pcode == null)
        //            {
        //                return;
        //            }
        //            table = CodeWork.GetInstance().SelectCodeList(pcode).Tables[0];

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.MG_DVCD)
        //        {

        //        }
        //        else if (type == VALUELIST_TYPE.ROHQCD)
        //        {

        //        }
        //        else if (type == VALUELIST_TYPE.MGDPCD)
        //        {

        //        }
        //        else if (type == VALUELIST_TYPE.BZSPCD)
        //        {
        //            table = CodeWork.GetInstance().Select_BZSPCD();

        //            foreach (DataRow row in table.Rows)
        //            {
        //                ValueListItem item = new ValueListItem();
        //                item.DataValue = row["CODE"];
        //                item.DisplayText = row["CODE_NAME"].ToString();
        //                valueList.ValueListItems.Add(item);
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.CNNCD)
        //        {
        //            table = CodeWork.GetInstance().Select_CNNCD();

        //            foreach (DataRow row in table.Rows)
        //            {
        //                ValueListItem item = new ValueListItem();
        //                item.DataValue = row["CODE"];
        //                item.DisplayText = row["CODE_NAME"].ToString();
        //                valueList.ValueListItems.Add(item);
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.SCNCD)
        //        {
        //            table = CodeWork.GetInstance().Select_SCNCD();

        //            foreach (DataRow row in table.Rows)
        //            {
        //                ValueListItem item = new ValueListItem();
        //                item.DataValue = row["CODE"];
        //                item.DisplayText = row["CODE_NAME"].ToString();
        //                valueList.ValueListItems.Add(item);
        //            }
        //        }
        //    }
        //}

        //public static void SetValueList(object target, VALUELIST_TYPE type, string pcode, string firstItemText, string firstItemValue)
        //{
        //    //대상에 따라서 객체는 두가지로 나뉘어질수있음
        //    //1. 일반 콤보박스
        //    //2. 그리드의 ValueLit

        //    ValueList valueList = target as ValueList;
        //    ComboBox comboBox = null;

        //    if (valueList == null)
        //    {
        //        comboBox = target as ComboBox;

        //        if (comboBox == null)
        //        {
        //            return;
        //        }

        //        comboBox.ValueMember = "CODE";
        //        comboBox.DisplayMember = "CODE_NAME";

        //        if (type == VALUELIST_TYPE.SINGLE_YEAR)
        //        {
        //            comboBox.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_MONTH)
        //        {
        //            comboBox.DataSource = DateUtils.GetMonth("CODE", "CODE_NAME");
        //        }
        //        else if (type == VALUELIST_TYPE.CODE)
        //        {
        //            if (pcode == null)
        //            {
        //                return;
        //            }
        //            comboBox.DataSource = CodeWork.GetInstance().SelectCodeList(pcode).Tables[0];
        //            ComboBoxUtils.AddDefaultOption(comboBox, firstItemText, firstItemValue, true);
        //        }
        //    }

        //    else if (valueList != null)
        //    {
        //        DataTable table = null;

        //        if (type == VALUELIST_TYPE.SINGLE_YEAR)
        //        {
        //            table = DateUtils.GetYear(-10, "CODE", "CODE_NAME");

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_MONTH)
        //        {
        //            table = DateUtils.GetMonth("CODE", "CODE_NAME");

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_DAY)
        //        {
        //            table = DateUtils.GetDay("CODE", "CODE_NAME");

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.SINGLE_HOUR)
        //        {
        //            table = DateUtils.GetHours("CODE", "CODE_NAME");


        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }
        //        else if (type == VALUELIST_TYPE.CODE)
        //        {
        //            if (pcode == null)
        //            {
        //                return;
        //            }
        //            table = CodeWork.GetInstance().SelectCodeList(pcode).Tables[0];

        //            foreach (DataRow row in table.Rows)
        //            {
        //                valueList.ValueListItems.Add(row["CODE"], row["CODE_NAME"].ToString());
        //            }
        //        }

        //        valueList.ValueListItems.Insert(0, firstItemValue, firstItemText);
        //    }
        //}


        public static double ToDouble(object value)
        {
            double result = 0;

            try
            {
                if (value != DBNull.Value && !Double.IsInfinity(Convert.ToDouble(value)) && !Double.IsNaN(Convert.ToDouble(value)))
                {
                    result = Convert.ToDouble(value);
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }


        //public static string GetTempFileName(string strTitle, FILE_TYPE type)
        //{
        //    string filename = Path.GetTempFileName();
        //    string exp = string.Empty;

        //    if (type == FILE_TYPE.EXCEL)
        //    {
        //        exp = "xls";
        //    }

        //    if (type == FILE_TYPE.BITMAP)
        //    {
        //        exp = "bmp";
        //    }

        //    filename = filename.Substring(filename.LastIndexOf('\\')+1, filename.LastIndexOf('.') - filename.LastIndexOf('\\'));

        //    return Path.GetTempPath() + strTitle + "_" + filename + exp;
        //}
    }

    public class ComboBoxUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMemeber"></param>
        /// <param name="text"></param>
        /// <param name="interval"></param>
        public static void AddData(ComboBox target, string valueMember, string displayMemeber, string text, int interval)
        {
            SetMember(target, valueMember, displayMemeber);
            target.DataSource = DataUtils.GetRequstData(valueMember, displayMemeber, text, interval);
        }

        public static void AddDefaultOption(ComboBox target, string display, string value, bool isSelected)
        {
            if (target == null)
            {
                return;
            }

            DataTable table = target.DataSource as DataTable;

            if (table == null)
            {
                if (target.DataSource != null)
                {
                    table = ((DataView)target.DataSource).Table;
                }
            }

            if (table == null)
            {
                table = new DataTable();
                table.Columns.Add(target.DisplayMember);
                table.Columns.Add(target.ValueMember);
            }

            DataRow row = table.NewRow();
            row[target.DisplayMember] = display;
            row[target.ValueMember] = value;
            table.Rows.InsertAt(row, 0);

            if (target.DataSource == null)
            {
                target.DataSource = table;
            }

            if (isSelected)
            {
                target.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 콤보박스 기본옵션 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="isSelected">옵션값 선택 / 전체 여부</param>
        public static void AddDefaultOption(ComboBox target, bool isSelected)
        {
            if (target == null)
            {
                return;
            }

            string text = isSelected ? "선택" : "전체";
            string value = isSelected ? "X" : "";

            string displayMember = target.DisplayMember != null ? target.DisplayMember : "CODE_NAME";
            string valueMember = target.ValueMember != null ? target.ValueMember : "CODE";

            SetMember(target, valueMember, displayMember);

            DataTable table = target.DataSource as DataTable;

            if (table == null)
            {
                table = new DataTable();
                table.Columns.Add(displayMember);
                table.Columns.Add(valueMember);
                DataRow row = table.NewRow();
                row[displayMember] = text;
                row[valueMember] = value;
                table.Rows.InsertAt(row, 0);
                target.DataSource = table;
                target.SelectedIndex = 0;
            }
            else
            {
                if (table.Rows.Count != 0)
                {
                    if (table.Rows[0][valueMember].ToString() == "X" || table.Rows[0][valueMember].ToString() == "O")
                    {
                        return;
                    }
                }

                DataRow row = table.NewRow();
                row[displayMember] = text;
                row[valueMember] = value;
                table.Rows.InsertAt(row, 0);
                target.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 콤보박스 기본 멤버 설정 및 기본옵션 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="isSelected">옵션값 선택 / 전체 여부</param>
        /// <param name="valueMember">값 필드멤버명</param>
        /// <param name="displayMember">텍스트 필드멤버명</param>
        public static void AddDefaultOption(ComboBox target, bool isSelected, string valueMember, string displayMember)
        {
            SetMember(target, valueMember, displayMember);
            AddDefaultOption(target, isSelected);
        }

        /// <summary>
        /// 콤보박스 기본 멤버 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="valueMember">값 필드멤버명</param>
        /// <param name="displayMemeber">텍스트 필드멤버명</param>
        public static void SetMember(ComboBox target, string valueMember, string displayMemeber)
        {
            target.ValueMember = valueMember;
            target.DisplayMember = displayMemeber;
        }


        /// <summary>
        /// 콤보박스의 displayValue를 기준으로 같은 아이템을 선택해준다.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="displayValue"></param>
        public static void SelectItemByDisplayMember(ComboBox target, string displayMember)
        {
            DataTable Items = target.DataSource as DataTable;

            if (Items == null)
            {
                Items = ((DataView)target.DataSource).Table;
            }

            int index = 0;

            foreach (DataRow row in Items.Rows)
            {
                if (row[target.DisplayMember].ToString() == displayMember)
                {
                    break;
                }
                index++;
            }
            target.SelectedIndex = index;
        }


        public static void SelectItemByValueMember(ComboBox target, string valueMember)
        {
            DataTable Items = target.DataSource as DataTable;

            if (Items == null)
            {
                Items = ((DataView)target.DataSource).Table;
            }

            int index = 0;

            foreach (DataRow row in Items.Rows)
            {
                if (row[target.ValueMember].ToString() == valueMember)
                {
                    break;
                }
                index++;
            }
            target.SelectedIndex = index;
        }
    }

    public class DateUtils
    {
        /// <summary>
        /// 금년기준 요청 년도 기간으로 연산하여 반환
        /// </summary>
        /// <param name="interval">년도 간격</param>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetYear(int interval, string valueColumn, string textColumn)
        {
            DateTime today = DateTime.Now;

            DataTable years = new DataTable();
            years.Columns.Add(valueColumn);
            years.Columns.Add(textColumn);

            for (int i = today.Year; i >= today.AddYears(interval).Year; i--)
            {
                DataRow year = years.NewRow();
                year[valueColumn] = i.ToString();
                year[textColumn] = i.ToString()+"년";
                years.Rows.Add(year);
            }
            return years;
        }

        /// <summary>
        /// 1월 - 12월을 DataTable로 반환
        /// </summary>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetMonth(string valueColumn, string textColumn)
        {
            DataTable months = new DataTable();
            months.Columns.Add(valueColumn);
            months.Columns.Add(textColumn);
            for (int i = 1; i <= 12; i++)
            {
                DataRow month = months.NewRow();
                month[valueColumn] = i.ToString("00");
                month[textColumn] = i.ToString("00") + "월";
                months.Rows.Add(month);
            }
            return months;
        }
        /// <summary>
        /// 1일 31일 DataTable로 반환
        /// </summary>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetDay(string valueColumn, string textColumn)
        {
            DataTable day = new DataTable();
            day.Columns.Add(valueColumn);
            day.Columns.Add(textColumn);
            for (int i = 1; i <= 31; i++)
            {
                DataRow dataRow = day.NewRow();
                dataRow[valueColumn] = i.ToString();
                dataRow[textColumn] = "매월 "+ i.ToString() + " 일";
                day.Rows.Add(dataRow);
            }
            return day;
        }
        /// <summary>
        /// 1월 - 12월을 DataTable로 반환
        /// </summary>
        /// <param name="valueColumn">값 컬럼명</param>
        /// <param name="textColumn">텍스트 컬럼명</param>
        /// <returns></returns>
        public static DataTable GetHours(string valueColumn, string textColumn)
        {
            DataTable hours = new DataTable();
            hours.Columns.Add(valueColumn);
            hours.Columns.Add(textColumn);
            for (int i = 0; i <= 23; i++)
            {
                DataRow hour = hours.NewRow();
                hour[valueColumn] = i.ToString("00");
                hour[textColumn] = i.ToString("00") + "시";
                hours.Rows.Add(hour);
            }
            return hours;
        }

        /// <summary>
        /// [year] + [division] + [month] + [dibision] + [day] 형식으로 문자열 반환
        /// </summary>
        /// <param name="division">구분값</param>
        /// <param name="add_year">연산 년</param>
        /// <param name="add_month">연산 월</param>
        /// <param name="add_day">연산 일</param>
        /// <returns></returns>
        public static string GetDate(char division, int add_year, int add_month, int add_day)
        {
            DateTime today = DateTime.Now;
            return today.AddYears(add_year).Year.ToString() + division + today.AddMonths(add_month).Month.ToString() + division + today.AddDays(add_day).Day.ToString();
        }

        /// <summary>
        /// [year] + [division] + [month] + [dibision] + [day] + [space] + [hour] + [:] + [minute] 형식으로 문자열 반환
        /// </summary>
        /// <param name="division">구분값</param>
        /// <param name="year">연산 년</param>
        /// <param name="month">연산 월</param>
        /// <param name="day">연산 일</param>
        /// <param name="time">세팅 시간</param>
        /// <returns></returns>
        public static string GetDateTime(char division, int add_year, int add_month, int add_day, int origin_time, int origin_minute)
        {
            DateTime today = DateTime.Now;
            return today.AddYears(add_year).Year.ToString() + division + today.AddMonths(add_month).Month.ToString() + division + today.AddDays(add_day).Day.ToString() + " " + origin_time.ToString("00") + ":" + origin_minute.ToString("00");
        }

        /// <summary>
        /// 포멧형식으로 날자를 반환
        /// </summary>
        /// <param name="target">날자 객체</param>
        /// <param name="format">포멧 (yyyymmdd, yyyymmddhhmi)</param>
        /// <returns></returns>
        public static string DateTimeToString(DateTime target, string format)
        {
            return target.ToString(format);
        }
    }

    public class DataUtils
    {
        public static DataTable DataTableWapperClone(DataTable target)
        {
            DataTable result = new DataTable();
            foreach (DataColumn column in target.Columns)
            {
                result.Columns.Add(column.ColumnName, column.DataType);
            }
            return result;
        }

        public static DataTable GetRequstData(string valueColumn, string textColumn, string text, int interval)
        {
            DataTable result = new DataTable();
            result.Columns.Add(valueColumn);
            result.Columns.Add(textColumn);
            for (int i = 1; i <= interval; i++)
            {
                DataRow line = result.NewRow();
                line[valueColumn] = i.ToString("00");
                line[textColumn] = i.ToString() + text;
                result.Rows.Add(line);
            }
            return result;
        }

        public static Hashtable ConvertDataRowViewToHashtable(DataRowView target)
        {
            DataColumnCollection columns = target.Row.Table.Columns;

            Hashtable result = new Hashtable();

            for (int i = 0; i < columns.Count; i++)
            {
                result[columns[i].ColumnName] = target[columns[i].ColumnName];
            }

            return result;
        }


        public static double NanToZero(double value)
        {
            if (Double.IsNaN(value))
            {
                return 0;
            }

            if (value.Equals(DBNull.Value))
            {
                value = 0;
            }
            return value;
        }


        public static object NullToZero(object value)
        {
            if (value == DBNull.Value)
            {
                return 0;
            }

            return value;
        }

        public static string StringToFormatString(string value, string format)
        {
            string result = string.Empty;

            if (value.Length == 8)
            {
                if (format == "yyyy-MM-dd")
                {
                    result = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);

                }

            }
            return result;
        }
    }

    public class ControlUtils
    {
        public static void SetEnabled(Control control, bool isEnabled)
        {
            if (isEnabled)
            {
                control.Enabled = true;
            }
            else
            {
                control.Enabled = false;
            }
        }

        /// <summary>
        /// TextBox의 값이 Double인지 체크하고 값을 반환한다.
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static double GetDoubleValue(TextBox textBox)
        {
            double result = 0;
            try
            {
                if (textBox.Enabled)
                {
                    result = Convert.ToDouble(textBox.Text);
                }
            }
            catch (Exception e)
            {
                textBox.SelectAll();
                MessageBox.Show(textBox.Name + "] 형식은 숫자형식이 되어야 합니다");
            }
            return result;
        }

        /// <summary>
        /// TextBox의 값이 Double인지 체크하고 값을 반환한다.
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static double GetDoubleValue(ComboBox controls)
        {
            double result = 0;
            try
            {
                result = Convert.ToDouble(controls.SelectedValue);
            }
            catch (Exception e)
            {
                MessageBox.Show(controls.Name + "] 형식은 숫자형식이 되어야 합니다");
            }
            return result;
        }
    }
}
