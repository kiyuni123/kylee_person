﻿using System;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Data;
using System.Timers;
using System.Windows.Forms;
using System.IO;

using Oracle.DataAccess.Client;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

#region ArcObject

#endregion

#region Infragistics
using Infragistics.Win.UltraWinTabControl;
#endregion

#region WaterNet Assembley

using WaterNet.WaterNetCore;
using WaterNet.WH_INPManage.form;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.util;

#endregion

using WaterNet.Water_Net.FormPopup;
using WaterNet.Water_Net.work;
using WaterNet.DF_Common.interface1;

namespace WaterNet.Water_Net
{
    public partial class frmLMain : Form, IApplicationMessage
    {
        private WarningWork work = null;
        private System.Timers.Timer timer = null;
        private delegate void ApplyWarningListCallback(DataTable warningTable);

        //통합서버와 통신을 위해 추가됨-wjs
        private delegate void PrintLogMessage(string value);
        //백그라운드 네트워크 서버 : 클라이언트와 네트워크 통신
        private Net.ClientListener server = null;
        private string logout_gbn = "0";  //로그아웃 구분
        private bool forceClose = false;  //강제종료 : 통합서버에서 종료메세지 받음

        /// <summary>
        /// 생성자
        /// </summary>
        public frmLMain()
        {
            InitializeComponent();

            InitializeSetting();
        }

        /// <summary>
        /// 초기환경 설정
        /// </summary>
        private void InitializeSetting()
        {

            if (WaterAOCore.ArcManager.CheckOutLicenses(ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine) != ESRI.ArcGIS.esriSystem.esriLicenseStatus.esriLicenseAvailable)
            {
                WaterNetCore.MessageManager.ShowExclamationMessage("ArcEngine Runtime License가 없습니다.");
                Application.Exit();
            }

            //this.UpgradeToWaterNETUpdater();
            this.Text = "water-NET " + EMFrame.statics.AppStatic.USER_SGCNM;

            this.Size = new System.Drawing.Size(1024, 869);
            spcContents.Panel2Collapsed = true;
            spcContents.Size = new System.Drawing.Size(1006, 734);
            spcContents.SplitterDistance = 634;
            uTabContents.CloseButtonLocation = Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None;
            

            //WaterAOCore.VariableManager.m_spatialReference = WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

            //하단 경고 그리드 설정
            UltraGridColumn warningColumn;

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WARN_NO";
            warningColumn.Header.Caption = "WARN_NO";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "CREATE_DATE";
            warningColumn.Header.Caption = "경보일자";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "FIXED_DATE";
            warningColumn.Header.Caption = "확인(처리)일자";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WORK_GBN";
            warningColumn.Header.Caption = "작업구분코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "LOC_CODE";
            warningColumn.Header.Caption = "지역코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WAR_CODE";
            warningColumn.Header.Caption = "경보코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "LOC_NAME";
            warningColumn.Header.Caption = "지역";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 100;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WORK_GBN_NAME";
            warningColumn.Header.Caption = "작업구분";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 100;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WAR_CODE_NAME";
            warningColumn.Header.Caption = "경보내용";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Left;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "CHECK_YN";
            warningColumn.Header.Caption = "확인여부";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 80;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "FIXED_YN";
            warningColumn.Header.Caption = "처리여부";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 80;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "SERIAL_NO";
            warningColumn.Header.Caption = "SERIAL_NO";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "REMARK";
            warningColumn.Header.Caption = "비고";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Left;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 400;
            warningColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griWarningList);

            //griWarningList의 스타일 변경
            // Header 스타일 설정
            Infragistics.Win.Appearance appearanceHeader = new Infragistics.Win.Appearance();
            appearanceHeader.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(214)), ((System.Byte)(233)), ((System.Byte)(244)));
            appearanceHeader.BackColor2 = System.Drawing.Color.FromArgb(((System.Byte)(214)), ((System.Byte)(233)), ((System.Byte)(244)));
            appearanceHeader.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(193)), ((System.Byte)(216)), ((System.Byte)(231)));
            appearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(109)), ((System.Byte)(151)), ((System.Byte)(177)));
            appearanceHeader.FontData.Bold = DefaultableBoolean.True;
            //appearanceHeader.FontData.Name = "굴림";
            //appearanceHeader.FontData.SizeInPoints = 10;

            // Row 스타일 설정 (border, font 색)
            Infragistics.Win.Appearance appearanceRow = new Infragistics.Win.Appearance();
            appearanceRow.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(222)), ((System.Byte)(235)), ((System.Byte)(237)));
            appearanceRow.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            // Row 스타일 설정 (border, font 색)
            this.griWarningList.DisplayLayout.Override.RowAppearance = appearanceRow;
            this.griWarningList.DisplayLayout.Override.DefaultRowHeight = 20;
            this.griWarningList.DisplayLayout.Scrollbars = Scrollbars.Vertical;
            // Header 스타일 설정
            this.griWarningList.DisplayLayout.Override.HeaderAppearance = appearanceHeader;
            this.griWarningList.DisplayLayout.BorderStyle = UIElementBorderStyle.None;
            this.griWarningList.UseFlatMode = DefaultableBoolean.False;
            this.griWarningList.DisplayLayout.Override.HeaderStyle = HeaderStyle.WindowsVista;

            /////초기 실행시 경보창 표시하도록 변경
            //toolStripButton7.Checked = true;
            //toolStripButton7_Click(toolStripButton7, new EventArgs());
        }

        /// <summary>
        /// 초기실행시 사용자의 권한에 따른 메뉴설정
        /// </summary>
        private void InitializeMenuSetting()
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
            param.Add("ZONE_GBN", EMFrame.statics.AppStatic.ZONE_GBN);
            System.Data.DataTable datatable = EMFrame.utils.DatabaseUtils.GetInstance().GetUserMenu(param);
            if (datatable == null || datatable.Rows.Count == 0)
            {
                MessageBox.Show(EMFrame.statics.AppStatic.USER_ID + "의 메뉴권한이 설정되지 않았습니다.");
                this.Close();
                Application.Exit();
                return;

            }
            ///사용자 메뉴 권한을 Static 변수에 저장한다.
            EMFrame.statics.AppStatic.USER_MENU.Clear();
            foreach (System.Data.DataRow row in datatable.Rows)
            {
                EMFrame.statics.AppStatic.USER_MENU.Add(row["MU_ID"], row["MENU_AUTH"]);
            }

            foreach (ToolStripMenuItem menu in this.menuStrip1.Items)
            {
                if (menu.HasDropDownItems)
                {
                    IsMenuCheck(menu);
                }
            }
        }

        /// <summary>
        /// 사용자권한에 따른 메뉴 visible설정
        /// </summary>
        /// <param name="menu"></param>
        private void IsMenuCheck(ToolStripMenuItem menu)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU[menu.Name];
            if (o == null)
            {                
                menu.Visible = false;
                return;
            }
            else
            {
                menu.Enabled = Convert.ToString(o).Equals("2") ? false : true;

                //수질 기능 고령지역(41480) 메뉴 설정
                if (EMFrame.statics.AppStatic.USER_SGCCD != "47830")
                {
                    스마트미터관리ToolStripMenuItem.Visible = false;
                }              

                //수질 기능 파주지역(41480) 메뉴 설정
                if (EMFrame.statics.AppStatic.USER_SGCCD == "41480")
                {                    
                    잔류염소예측ToolStripMenuItem.Visible = false;
                    관세척구간관리ToolStripMenuItem.Visible = false;
                    수질검사자료관리ToolStripMenuItem.Visible = false;
                }
                else
                {
                    수질모의ToolStripMenuItem.Visible = false;
                }

                if (!menu.Enabled)
                {
                    switch (menu.Name)
                    {
                        case "MenuCrisisWaterBreak":
                            tsbtnTransfer.Enabled = menu.Enabled;
                            break;
                        case "ToolStripMenuEnergyState":
                            tsbtnEnergyPump.Enabled = menu.Enabled;
                            break;
                        case "실시간관망해석ToolStripMenuItem":
                            toolStripButton01.Enabled = menu.Enabled;
                            break;
                        case "실시간감시ToolStripMenuItem":
                            toolStripButton1.Enabled = menu.Enabled;
                            break;
                        case "수질민원관리ToolStripMenuItem":
                            toolStripButton3.Enabled = menu.Enabled;
                            break;
                        case "누수감시결과ToolStripMenuItem":
                            toolStripButton2.Enabled = menu.Enabled;
                            break;
                        case "유수율현황ToolStripMenuItem":
                            toolStripButton4.Enabled = menu.Enabled;
                            break;
                        case "ToolStripMenuManage":
                            toolStripButton6.Enabled = menu.Enabled;
                            break;
                        case "감압밸브감시ToolStripMenuItem":
                            toolStripButton5.Enabled = menu.Enabled;
                            break;
                        case "ToolStripMenuMapMaker":
                            tsbtnMapMaker.Enabled = menu.Enabled;
                            break;
                        case "ToolStripMenuDrPipe":
                            tsbtnDrPipe.Enabled = menu.Enabled;
                            break;
                    }    
                }
                
                if (menu.HasDropDownItems)
                {
                    for (int i = 0; i < menu.DropDownItems.Count; i++)
                    {
                        ToolStripMenuItem item = menu.DropDownItems[i] as ToolStripMenuItem;
                        if (item != null)
                        {
                            IsMenuCheck(item);
                        }
                    }
                }
            }
        }


        private void frmLMain_Shown(object sender, EventArgs e)
        {
            this.InitializeMenuSetting();  //사용자에 따른 메뉴설정

            #region 통합서버 네트워크 접속 및 로그인 메세지 (주석처리 20190503 lky)
            //WaterNetCore.FunctionManager.ReadXml doc = new WaterNetCore.FunctionManager.ReadXml(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH);
            //string ip = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/ip"));
            ////통합서버에 실행확인
            //server = new Net.ClientListener(this, ip, (int)3000);
            //if (!server.ServerStart())
            //{
            //    MessageBox.Show("통합서버(" + ip + ")가 실행되어 있지 않습니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    Application.Exit();
            //    return;
            //}

            ////통합서버에 로그인 메세지 보내기
            //StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.Append("C_REQ_LOGIN" + ":");
            //oStringBuilder.Append(EMFrame.statics.AppStatic.USER_ID + ",");
            //oStringBuilder.Append(EMFrame.statics.AppStatic.USER_IP + ",");
            //oStringBuilder.Append(EMFrame.statics.AppStatic.LOGIN_DT + ",");
            //oStringBuilder.Append(EMFrame.statics.AppStatic.USER_SGCCD + ",");
            //oStringBuilder.Append(EMFrame.statics.AppStatic.ZONE_GBN);
            //server.SendMessage(oStringBuilder.ToString());

            #endregion 통합서버 네트워크 접속 및 로그인 메세지

            string datadir = EMFrame.statics.AppStatic.DATA_DIR + "\\" + EMFrame.statics.AppStatic.USER_SGCCD;
            WaterAOCore.VariableManager.m_Topographic = WaterAOCore.ArcManager.getShapeWorkspace(datadir + "\\Topographic");
            WaterAOCore.VariableManager.m_Pipegraphic = WaterAOCore.ArcManager.getShapeWorkspace(datadir + "\\Pipegraphic");
            WaterAOCore.VariableManager.m_INPgraphic = WaterAOCore.ArcManager.getShapeWorkspace(datadir + "\\INPgraphic");
            WaterAOCore.VariableManager.m_INPgraphicRootDirectory = datadir + "\\INPgraphic";
            WaterAOCore.VariableManager.m_Netgraphic = datadir + "\\Netgraphic";

            Form oform = new frmMainPanel();
            oform.FormBorderStyle = FormBorderStyle.None;
            oform.TopLevel = false;
            oform.Dock = DockStyle.Fill;
            oform.Parent = spcContents.Panel1;
            oform.BringToFront();
            ((frmMainPanel)oform).Open();

            this.ShowIcon = true;
            this.ShowInTaskbar = true;
        }

        /// <summary>
        /// 화면로드 메소드
        /// Shape Workspace 설정, 로그인화면, 메인패널화면 오픈한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private bool isLogon = true;
        private void frmMain_Load(object sender, EventArgs e)
        {   
            //실시간 경고 설정
            work = WarningWork.GetInstance();

            griWarningList.DataSource = work.SelectWarningList().Tables["WA_WARNING"];

            for (int i = 0; i < griWarningList.Rows.Count; i++)
            {
                if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
                {
                    griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
                }
                else
                {
                    griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
                }
            }

            timer = new System.Timers.Timer();
            timer.Interval = 1000 * 60;
            timer.Elapsed += new ElapsedEventHandler(ApplyWarningListHandler);
            timer.Start();
        }


        /// <summary>
        /// 메인화면 Close()
        /// 독립적으로 실행중인 프로세스 삭제
        /// 상수관망 네트워크구조 생성 프로세스:오랜시간이 소요되므로, 독립기능으로 구성됨.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            tsbtnAllClose_Click(this, new EventArgs());

            System.Diagnostics.Process[] proc = System.Diagnostics.Process.GetProcessesByName("WaterNet.WU_PipeNetwork");
            foreach (System.Diagnostics.Process item in proc)
            {
                if (item != null) item.Kill();
            }
        }

        /// <summary>
        /// 메인화면 종료 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.forceClose)  //강제종료
            {
                StringBuilder oStringBuilder = new StringBuilder();
                /////백그라운드 네트워크 소켓 서버 종료
                ///클라이언트 종료시 종료일시, 종료구분 Update
                oStringBuilder.Append("C_REQ_FORCE_LOGOUT" + ":");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_ID + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_IP + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.LOGIN_DT + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_SGCCD + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.ZONE_GBN + ",");
                switch (this.logOutMessage)
                {
                    case "C_REQ_DUAL_LOGIN": //이중접속
                        oStringBuilder.Append("2");
                        break;
                    case "C_SERVER_OUT": //통합서버 종료
                        oStringBuilder.Append("1"); 
                        break;
                    case "C_REQ_MAXIMUM_OUT": //센터의 라이센스 수 초과
                        oStringBuilder.Append("3");
                        break;
                }
                
                server.SendMessage(oStringBuilder.ToString());
            }

            if (!this.forceClose && this.server != null && this.server.Connected)
            {
                if ((MessageBox.Show("프로그램을 종료하시겠습니까?", "종료", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)) != DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }

                /////클라이언트 종료시 종료일시, 종료구분 Update(정상종료)
                //System.Collections.Hashtable param = new System.Collections.Hashtable();
                //param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                //param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                //param.Add("LOGOUT_GBN", "0");
                //EMFrame.utils.DatabaseUtils.GetInstance().SuccessLogoutInfo(param);

                ///백그라운드 네트워크 소켓 서버 종료
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.Append("C_REQ_LOGOUT" + ":");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_ID + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_IP + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.LOGIN_DT + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_SGCCD + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.ZONE_GBN);
                server.SendMessage(oStringBuilder.ToString());
            }

            Application.DoEvents();

            //(주석처리 20190503 lky)
            //this.server.ServerStop();
            //this.server.Dispose();
        }

        private string logOutMessage = string.Empty;

        /// <summary>
        /// Delegate 종료 메소드
        /// </summary>
        /// <param name="msg"></param>
        public void GetExitMessage(string msg)
        {
            try
            {
                ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
                ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
                ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
                ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
                ///진행바의 해당 속성에 바로 대입한다.
                if (this.InvokeRequired)
                {
                    //delegate 생성
                    PrintLogMessage dele = new PrintLogMessage(GetExitMessage);
                    //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                    this.Invoke(dele, new object[] { msg });
                }
                else
                {
                    string[] token = msg.Split(':', ',');
                    /////클라이언트 종료시 종료일시, 종료구분 Update
                    //System.Collections.Hashtable param = new System.Collections.Hashtable();

                    switch (token[0])
                    {
                        case "C_REQ_DUAL_LOGIN": //이중접속
                            //param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                            //param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                            //param.Add("LOGOUT_GBN", "2");
                            //EMFrame.utils.DatabaseUtils.GetInstance().SuccessLogoutInfo(param);
                            EMFrame.utils.LoggerUtils.logger.Info("이중접속 => " + msg);
                            break;
                        case "C_SERVER_OUT": //통합서버 종료
                            //param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                            //param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                            //param.Add("LOGOUT_GBN", "1");
                            //EMFrame.utils.DatabaseUtils.GetInstance().SuccessLogoutInfo(param);
                            EMFrame.utils.LoggerUtils.logger.Info("통합서버 종료 => " + msg);
                            break;
                        case "C_REQ_MAXIMUM_OUT": //센터의 라이센스 수 초과
                            //param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                            //param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                            //param.Add("LOGOUT_GBN", "3");
                            //EMFrame.utils.DatabaseUtils.GetInstance().SuccessLogoutInfo(param);
                            EMFrame.utils.LoggerUtils.logger.Info("라이센스 초과 => " + msg);
                            break;
                    }

                    MessageBox.Show(msg);

                    this.logOutMessage = token[0];
                    this.forceClose = true;
                    this.Close();
                }
            }
            catch { }

        }

        //실시간 경고 출력 delegate관련 메소드
        private void ApplyWarningListHandler(Object sender, EventArgs eArgs)
        {
            //ApplyWarningList(SelectWarningList());
        }

        //실시간 경고 출력 delegate관련 메소드
        private DataTable SelectWarningList()
        {
            return work.SelectWarningList().Tables["WA_WARNING"];
        }

        //실시간 경고 출력 delegate관련 메소드
        private void ApplyWarningList(DataTable warningTable)
        {
            if (griWarningList.InvokeRequired)
            {
                ApplyWarningListCallback applyWarningListCallback = new ApplyWarningListCallback(ApplyWarningList);
                this.Invoke(applyWarningListCallback, new object[] { warningTable });
            }
            else
            {
                try
                {
                    griWarningList.DataSource = warningTable;

                    for (int i = 0; i < griWarningList.Rows.Count; i++)
                    {
                        if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
                        {
                            griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
                        }
                        else
                        {
                            griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
                        }
                    }
                }
                catch (Exception )
                {
                    Console.WriteLine("Watcher 쓰기 오류...");

                    timer.Stop();
                    timer.Dispose();
                }
            }
        }

        /// <summary>
        /// 서브화면- 기능화면을 메인화면에 표시한다.
        /// </summary>
        /// <param name="oform"></param>
        private void ShowForm(Form oform)
        {
            try
            {
                if (oform is IForminterface)
                {
                    IForminterface obj = (IForminterface)oform;

                    Infragistics.Win.UltraWinTabControl.UltraTab tab = GetForm(oform);
                    if (tab == null)
                    {
                        tab = uTabContents.Tabs.Add(obj.FormKey.ToString() + ":" + obj.GetType().Name.ToString(), obj.FormID);
                        oform.FormBorderStyle = FormBorderStyle.None;
                        oform.TopLevel = false;
                        oform.Dock = DockStyle.Fill;

                        oform.Parent = tab.TabPage;
                        oform.Show();
                    }
                    uTabContents.SelectedTab = tab;
                }
            }
            catch (Exception Exce)
            {
                Console.WriteLine(Exce.Message + " : " + Exce.Source);
            }

            VisibledMainPanel();

        }

        /// <summary>
        /// 메인화면에 표시되어 있는 서브화면을 찾는다
        /// </summary>
        /// <param name="oform"></param>
        /// <returns></returns>
        private Infragistics.Win.UltraWinTabControl.UltraTab GetForm(Form oform)
        {
            if (oform is IForminterface)
            {
                IForminterface obj = (IForminterface)oform;

                foreach (Infragistics.Win.UltraWinTabControl.UltraTab item in uTabContents.Tabs)
                {
                    if (item.Key == obj.FormKey.ToString() + ":" + obj.GetType().Name.ToString())
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 비상상황 그리드를 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuContents_Click(object sender, EventArgs e)
        {
            toolStripButton7.Checked = MenuContents.Checked;
            int BaseHeight = 759; int PanelHeight = 100;
            //int BaseHeight = 800; int PanelHeight = 100;
            if (uTabContents.Tabs.Count == 0)   ///메인화면 상태
            {
                spcContents.Panel2Collapsed = !(MenuContents.Checked);
                if (MenuContents.Checked)       ///Alert그리드 표시됨
                {
                    this.Height = BaseHeight + PanelHeight;
                }
                else                                    ///Alert그리드 표시안됨
                {
                    this.Height = BaseHeight;
                }

            }
            else
                spcContents.Panel2Collapsed = !(MenuContents.Checked);
        }


        #region IApplicationMessage 멤버
        /// <summary>
        /// 상태바에 좌표를 표시하는 Interface 메소드
        /// </summary>
        /// <param name="message"></param>
        public void SetStatusMessage(string message)
        {
            statusBarXY.Text = message;
        }

        #endregion

        /// <summary>
        /// 메인 탭에 화면삭제 팝업메뉴를 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTabContents_MouseDown(object sender, MouseEventArgs e)
        {
            if (uTabContents.Tabs.Count == 0) return;
            if (e.Button == MouseButtons.Right)   contextMenu1.Show(uTabContents, e.Location);
        }

        /// <summary>
        /// 전체화면을 삭제하는 팝업메뉴 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuAllClose_Click(object sender, EventArgs e)
        {
            tsbtnAllClose_Click(this, new EventArgs());
        }

        /// <summary>
        /// 현재화면을 삭제하는 팝업메뉴 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuthisClose_Click(object sender, EventArgs e)
        {
            tsbtnClose_Click(this, new EventArgs());
        }

        /// <summary>
        /// 현재 보여지는 폼을 삭제하는 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbtnClose_Click(object sender, EventArgs e)
        {
            UltraTab tab = uTabContents.SelectedTab;
            if (tab == null) return;

            foreach (object item in tab.TabPage.Controls)
            {
                if (item is IForminterface)
                {
                    ((Form)item).Close();
                    ((Form)item).Dispose();

                    continue;
                }
            }

            ///Contain된 폼을 삭제하는 루틴추가
            tab.Close();
            tab.Dispose();

            VisibledMainPanel();
        }

        /// <summary>
        /// TabPage 삽입된 폼 전체를 삭제하는 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbtnAllClose_Click(object sender, EventArgs e)
        {
            foreach (Infragistics.Win.UltraWinTabControl.UltraTab tab in uTabContents.Tabs)
            {
                foreach (object item in tab.TabPage.Controls)
                {
                    if (item is IForminterface)
                    {
                        ((Form)item).Close();
                        ((Form)item).Dispose();

                        continue;
                    }
                }
            }

            uTabContents.Tabs.Clear();

            VisibledMainPanel();

        }

        /// <summary>
        /// TabPage 삽입된 폼 현재화면을 제외한 전체를 삭제하는 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuNthisClose_Click(object sender, EventArgs e)
        {
            for (int i = uTabContents.Tabs.Count -1; i > -1; i--)
            {
                UltraTab tab = uTabContents.Tabs[i];

                if (tab.Equals(uTabContents.SelectedTab)) continue;

                foreach (object item in tab.TabPage.Controls)
                {
                    if (item is IForminterface)
                    {
                        ((Form)item).Close();
                        ((Form)item).Dispose();

                        continue;
                    }
                }
                tab.Close();
                tab.Dispose();
            }
         }

        #region 툴바
        private void tsbtnTransfer_Click(object sender, EventArgs e)
        {
            MenuCrisisWaterBreak_Click(this, new EventArgs());
        }

        private void tsbtnEnergyPump_Click(object sender, EventArgs e)
        {
            ToolStripMenuEnergyState_Click(this, new EventArgs());
        }

        private void toolStripButton01_Click(object sender, EventArgs e)
        {
            실시간관망해석ToolStripMenuItem_Click(this, new EventArgs());
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            실시간감시ToolStripMenuItem_Click(this, new EventArgs());
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            수질민원관리ToolStripMenuItem_Click(this, new EventArgs());
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            누수감시결과ToolStripMenuItem_Click(this, new EventArgs());
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            유수율현황ToolStripMenuItem_Click(this, new EventArgs());
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            ToolStripMenuManage_Click(this, new EventArgs());
        }

        /// <summary>
        /// 감압밸브 감시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            감압밸브감시ToolStripMenuItem_Click(this, new EventArgs());
        }


        private void toolStripButtonReport_Click(object sender, EventArgs e)
        {
            시단위공급량ToolStripMenuItem_Click(this, new EventArgs());
            일공급량ToolStripMenuItem_Click(this, new EventArgs());
            일야간최소유량ToolStripMenuItem_Click(this, new EventArgs());
            관망운영일보ToolStripMenuItem_Click(this, new EventArgs());
        }

        private void tsbtnMapMaker_Click(object sender, EventArgs e)
        {
            ToolStripMenuMapMaker_Click(this, new EventArgs());
        }

        private void tsbtnDrPipe_Click(object sender, EventArgs e)
        {
            ToolStripMenuDrPipe_Click(this, new EventArgs());
        }
        #endregion 툴바

        /// <summary>
        /// 메인패널폼 보이기/감추기 메소드
        /// </summary>
        private void VisibledMainPanel()
        {
            Form oform = spcContents.Panel1.Controls["frmMainPanel"] as Form;
            if (uTabContents.Tabs.Count == 0)
            {   
                if (oform != null)
                {
                    this.FormBorderStyle = FormBorderStyle.FixedSingle;
                    this.MaximizeBox = false;
                    this.MinimizeBox = true;
                    oform.Visible = true;
                    oform.BringToFront();
                }
            }
            else
            {
                if (oform != null)
                {
                    this.FormBorderStyle = FormBorderStyle.Sizable;
                    this.MaximizeBox = true;
                    this.MinimizeBox = true;

                    oform.Visible = false;
                }
            }
        }

        /// <summary>
        /// 시스템 종료 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>         
        private void menuExitApp_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }



        #region 위기관리 메뉴실행
        /// <summary>
        /// 위기관리-단수발생
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MenuCrisisWaterBreak_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["MenuCrisisWaterBreak"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            this.Cursor = Cursors.WaitCursor;
            Form oform = new WC_CrisisTransfer.frmTransferMain();
            if (GetForm(oform) == null) ((WC_CrisisTransfer.frmTransferMain)oform).Open();                
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 위기관리 - 수질사고
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuWaterTrouble_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuWaterTrouble"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            this.Cursor = Cursors.WaitCursor;
            Form oform = new WC_WaterTrouble.frmTroubleMain();
            if (GetForm(oform) == null) ((WC_WaterTrouble.frmTroubleMain)oform).Open();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }
        #endregion 위기관리 메뉴실행

        #region 에너지관리 메뉴실행
        /// <summary>
        /// 에너지관리 - 기초정보
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuBaseInfo_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuBaseInfo"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WE_BaseInfo.frmBaseResource();
            //if (GetForm(oform) == null) ((WE_BaseInfo.frmBaseResource)oform).Open();

            //ShowForm(oform);
            //this.Cursor = Cursors.Default;
        }

        private void ToolStripMenuEffData_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuEffData"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WE_BaseInfo.frmEffData();
            //if (GetForm(oform) == null) ((WE_BaseInfo.frmEffData)oform).Open();

            //ShowForm(oform);
            //this.Cursor = Cursors.Default;
        }

        private void ToolStripMenuMeaureSearch_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuMeaureSearch"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;


            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WE_BaseInfo.frmMeasurSearch();
            //if (GetForm(oform) == null) ((WE_BaseInfo.frmMeasurSearch)oform).Open();

            //ShowForm(oform);
            //this.Cursor = Cursors.Default;
        }


        private void ToolStripMenuPumpSchedule_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuPumpSchedule"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WE_EnergyAnalysis.frmPumpSimulator();

            //if (GetForm(oform) == null) ((WE_EnergyAnalysis.frmPumpSimulator)oform).Open();

            //ShowForm(oform);
            //this.Cursor = Cursors.Default;            
        }

        /// <summary>
        /// 에너지관리 - 모의분석(스케쥴) 결과 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuScheduleView_Click(object sender, EventArgs e)
        {
            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WE_EnergyAnalysis.frmPumpScheduleSearch();
            //if (GetForm(oform) == null) ((WE_EnergyAnalysis.frmPumpScheduleSearch)oform).Open();

            //ShowForm(oform);
            //this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 에너지 통계 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuEngStatistics_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuEngStatistics"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WE_EnergyStatis.frmEnergyStatistics();
            //if (GetForm(oform) == null) ((WE_EnergyStatis.frmEnergyStatistics)oform).Open();
            //ShowForm(oform);
            //this.Cursor = Cursors.Default;

                
        }

        /// <summary>
        /// 실시간 수송에너지 현황
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuEnergyState_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuEnergyState"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WE_EnergyPump.frmPumpRealTimeEnergy();
            //if (GetForm(oform) == null) ((WE_EnergyPump.frmPumpRealTimeEnergy)oform).Open();
            //ShowForm(oform);
            //this.Cursor = Cursors.Default;
        }
        #endregion 에너지관리 메뉴실행

        #region 수질관리 Menu 실행

        private void 실시간감시ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["실시간감시ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WQ_RTGamsi.frmWQMain();
            if (GetForm(oForm) == null) ((WQ_RTGamsi.frmWQMain)oForm).Open();
            ShowForm(oForm);
        }

        private void 실시간감시관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["실시간감시관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WQ_GamsiPosition.frmWQMain();
            if (GetForm(oForm) == null) ((WQ_GamsiPosition.frmWQMain)oForm).Open();
            ShowForm(oForm);
        }
        
        private void 수질민원관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["수질민원관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WQ_Minwon.frmWQMain();
            if (GetForm(oForm) == null)  ((WQ_Minwon.frmWQMain)oForm).Open();
            ShowForm(oForm);
        }
        
        private void 수질검사자료관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["수질검사자료관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WQ_GumsaData.frmWQMain();
            if (GetForm(oForm) == null)  ((WQ_GumsaData.frmWQMain)oForm).Open();
            ShowForm(oForm);
        }

        private void 잔류염소예측ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["잔류염소예측ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WQ_ReChlorine.frmWQMain();
            if (GetForm(oForm) == null)  ((WQ_ReChlorine.frmWQMain)oForm).Open();
            ShowForm(oForm);
        }

        private void 관세척구간관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["관세척구간관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WQ_PipeCleanSection.frmWQMain();
            if (GetForm(oForm) == null)  ((WQ_PipeCleanSection.frmWQMain)oForm).Open();
            ShowForm(oForm);
        }
        
        #endregion

        #region 수량관리 Menu 실행
        /// <summary>
        /// 실시간 수집 데이터 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuRealTimeData_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuRealTimeData"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            foreach (Form frm in Application.OpenForms)
            {
                if (string.Format("{0}.{1}", frm.GetType().Namespace, frm.Name).IndexOf("WV_RealTimeDataInformation.form.frmRealtime") > -1)
                {
                    frm.Activate();
                    return;
                }
            }
            
            Form oForm = new WV_RealTimeDataInformation.form.frmRealtime();
            oForm.Owner = this;
            ((WV_RealTimeDataInformation.form.frmRealtime)oForm).Show();
        }

        private void 블록운영분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["블록운영분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_OperationManage.form.frmMain)))
            {
                return;
            }

            WV_OperationManage.form.frmMain popupWindow = new WaterNet.WV_OperationManage.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        public void 누수감시결과ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["누수감시ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_LeakageManage.form.frmLeakageResult)))
            {
                return;
            }

            WV_LeakageManage.form.frmLeakageResult popupWindow = new WaterNet.WV_LeakageManage.form.frmLeakageResult();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();

            //WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            //if (GetForm(oForm) == null)
            //{
            //    oForm.Open();
            //    ShowForm(oForm);
            //}
            //else
            //{
            //    oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
            //    GetForm(oForm).TabPage.Tab.Selected = true;
            //}

            //oForm.LeakageMonitering();
        }

        public void CallLeakageMoniteringResult()
        {
            this.누수감시결과ToolStripMenuItem_Click(this, new EventArgs());
        }

        private void 블록별공급량야간최소유량분석toolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["블록별공급량야간최소유량분석toolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_BlockFlowAnalysis.form.BlockOperationAnalysis)))
            {
                return;
            }

            WV_BlockFlowAnalysis.form.BlockOperationAnalysis popupWindow = new WV_BlockFlowAnalysis.form.BlockOperationAnalysis();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 누수량관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["누수량산정결과조회ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_LeakageManage.form.frmMain)))
            {
                return;
            }

            WV_LeakageManage.form.frmMain popupWindow = new WV_LeakageManage.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 사업추진효과분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["사업추진효과분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_BusinessPromotionEffectAnalysis.form.frmMain)))
            {
                return;
            }

            WV_BusinessPromotionEffectAnalysis.form.frmMain popupWindow = new WV_BusinessPromotionEffectAnalysis.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 유량패턴현황ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["유량패턴현황ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_FlowPatternStatus.form.frmMain)))
            {
                return;
            }

            WV_FlowPatternStatus.form.frmMain popupWindow = new WV_FlowPatternStatus.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        public void 유수율현황ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["유수율현황조회ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_RevenueWaterRatioStatus.form.frmMain)))
            {
                return;
            }

            WV_RevenueWaterRatioStatus.form.frmMain popupWindow = new WV_RevenueWaterRatioStatus.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 유수율상호분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["유수율상호분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_RevenueWaterRatioAnalysis.form.frmMain)))
            {
                return;
            }

            WV_RevenueWaterRatioAnalysis.form.frmMain popupWindow = new WV_RevenueWaterRatioAnalysis.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 일반현황관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["일반현황관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_GeneralStatus.form.frmMain)))
            {
                return;
            }

            WV_GeneralStatus.form.frmMain popupWindow = new WV_GeneralStatus.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 계량기교체관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["계량기교체관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_MeterChangeInformation.form.frmMain)))
            {
                return;
            }

            WV_MeterChangeInformation.form.frmMain popupWindow = new WV_MeterChangeInformation.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 야간최소유량필터링ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["야간최소유량산정방식별조회ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_BackgroundMinimumNightFlowFiltering.form.frmMain)))
            {
                return;
            }

            WV_BackgroundMinimumNightFlowFiltering.form.frmMain popupWindow = new WV_BackgroundMinimumNightFlowFiltering.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 누수지점관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["누수지점관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_LeakageRepairManage.form.frmMain)))
            {
                return;
            }

            WV_LeakageRepairManage.form.frmMain popupWindow = new WV_LeakageRepairManage.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 관개대체관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["관개대체관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_PipeChangeManage.form.frmMain)))
            {
                return;
            }

            WV_PipeChangeManage.form.frmMain popupWindow = new WV_PipeChangeManage.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 기타단수작업ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["단수작업관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_ShutOffManage.form.frmMain)))
            {
                return;
            }

            WV_ShutOffManage.form.frmMain popupWindow = new WV_ShutOffManage.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 월유수율분석보고서ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["월유수율분석보고서ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_RevenueWaterRatioReport.form.frmMain)))
            {
                return;
            }

            WV_RevenueWaterRatioReport.form.frmMain popupWindow = new WV_RevenueWaterRatioReport.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 총괄수량수지분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["총괄수량수지분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_MainMap.form.frmMainMap oForm = new WV_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WV_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_WaterBalanceAnalysis.form.frmMain)))
            {
                return;
            }

            WV_WaterBalanceAnalysis.form.frmMain popupWindow = new WV_WaterBalanceAnalysis.form.frmMain();
            popupWindow.MainMap = oForm as IMapControlWV;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 관망운영일보ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["관망운영일보ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frmDailyReport();
            if (GetForm(oForm) == null) ((WV_Report.form.frmDailyReport)oForm).Open();
            ShowForm(oForm);
        }


        private void 용수공급일보ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["용수공급일보ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frmSystemDailyReport();
            if (GetForm(oForm) == null) ((WV_Report.form.frmSystemDailyReport)oForm).Open();
            ShowForm(oForm);
        }

        private void 시단위공급량ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["시단위공급량ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frmTimeSupply();
            if (GetForm(oForm) == null) ((WV_Report.form.frmTimeSupply)oForm).Open();
            ShowForm(oForm);
        }

        private void 시단위공급량2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["시단위공급량2ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frmTimeSupply2();
            if (GetForm(oForm) == null) ((WV_Report.form.frmTimeSupply2)oForm).Open();
            ShowForm(oForm);
        }

        private void 일공급량ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["일공급량ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frm31DaySupply();
            if (GetForm(oForm) == null) ((WV_Report.form.frm31DaySupply)oForm).Open();
            ShowForm(oForm);
        }

        private void 일야간최소유량ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["일야간최소유량ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frm31DayMNF();
            if (GetForm(oForm) == null) ((WV_Report.form.frm31DayMNF)oForm).Open();
            ShowForm(oForm);
        }

        private void 특이사항기록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["특이사항기록ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frmDailyLog();
            if (GetForm(oForm) == null) ((WV_Report.form.frmDailyLog)oForm).Open();
            ShowForm(oForm);
        }

        private void 시단위공급량분기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["시단위공급량분기ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frmSystemTimeSupply();
            if (GetForm(oForm) == null) ((WV_Report.form.frmSystemTimeSupply)oForm).Open();
            ShowForm(oForm);
        }

        private void 공급량수정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["공급량수정ToolStripMenuItem"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WV_Report.form.frmAccumulationHour();
            if (GetForm(oForm) == null) ((WV_Report.form.frmAccumulationHour)oForm).Open();
            ShowForm(oForm);
        }

        private void 블록기본설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["블록별수량관리기본설정ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;
            
            WV_BlockManage.form.frmMain oForm = new WaterNet.WV_BlockManage.form.frmMain();
            oForm.ShowDialog();
        }

        private void 스마트미터관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["스마트미터관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WV_WaterMeterDataInformation.form.frmMain oForm = new WaterNet.WV_WaterMeterDataInformation.form.frmMain();
            oForm.ShowDialog();
        }

        #endregion

        #region 관망해석 Menu 실행

        private void iNP파일관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["iNP파일관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WH_INPManage.form.frmWHINPManage();
            if (GetForm(oForm) == null) ((WH_INPManage.form.frmWHINPManage)oForm).Open();
            ShowForm(oForm);
        }

        private void 실시간작업관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["실시간작업관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WH_AnalysisScheduleManage.form.frmAnalysisScheduleManage();
            if (GetForm(oForm) == null) ((WH_AnalysisScheduleManage.form.frmAnalysisScheduleManage)oForm).Open();
            ShowForm(oForm);
        }

        public void 실시간관망해석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["실시간관망해석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;
            
            Form oForm = new WH_RTAnalysis.form.frmWHMain();
            if (GetForm(oForm) == null) ((WH_RTAnalysis.form.frmWHMain)oForm).Open();
            ShowForm(oForm);
        }

        /// <summary>
        /// 블록 유량현황 보기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuBlockFlowInfo_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["MenuBlockFlowInfo"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            this.Cursor = Cursors.WaitCursor;
            Form oform = new WaterMainMap.frmMainMap();
            if (GetForm(oform) == null) ((WaterMainMap.frmMainMap)oform).Open();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }
        #endregion




        /// <summary>
        /// IWater TAG Mapping
        /// IWater Tag를 Shape의 객체와 맵핑하는 화면
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuTagLinkage_Click(object sender, EventArgs e)
        {
            //object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuTagLinkage"];
            //if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            //this.Cursor = Cursors.WaitCursor;
            //Form oform = new WU_TagLinkage.frmWUMain();
            //if (GetForm(oform) == null) ((WU_TagLinkage.frmWUMain)oform).Open();
            //ShowForm(oform);
            //this.Cursor = Cursors.Default;
        }


        #region 기본설정

        private void DBConnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (AppStatic.USER_RIGHT == "1")
            //{
            //    FormPopup.frmDatabase oForm = new FormPopup.frmDatabase();
            //    oForm.ShowDialog();
            //}
            //else
            //{
            //    MessageBox.Show("데이터베이스 연결 관리는 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void UserManageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (AppStatic.USER_RIGHT == "1")
            //{
            //    FormPopup.frmUserManager oForm = new FormPopup.frmUserManager();
            //    oForm.ShowDialog();
            //}
            //else
            //{
            //    MessageBox.Show("사용자 관리는 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void PasswordManageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPopup.frmChangePWD oForm = new FormPopup.frmChangePWD();
            oForm.ShowDialog();
        }

        private void ENV_TCPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (AppStatic.USER_RIGHT == "1")
            //{
            //    FormPopup.frmFTPConfig oForm = new FormPopup.frmFTPConfig();
            //    oForm.ShowDialog();
            //}
            //else
            //{
            //    MessageBox.Show("FTP 환경관리는 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void VersionManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (AppStatic.USER_RIGHT == "1")
            //{
            //    FormPopup.frmVersionManager oForm = new FormPopup.frmVersionManager();
            //    oForm.ShowDialog();        
            //}
            //else
            //{
            //    MessageBox.Show("버전 관리는 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void waterNET코드관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (EMFrame.statics.AppStatic.USER_RIGHT == "1")
            {
                FormPopup.frmCodeManager oForm = new FormPopup.frmCodeManager();
                oForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("코드 관리는 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void waterNET로컬버전초기화ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (EMFrame.statics.AppStatic.USER_RIGHT == "1")
            //{
            //    if (File.Exists(Application.StartupPath + @"\Config\Version.xml") == true)
            //    {
            //        File.Delete(Application.StartupPath + @"\Config\Version.xml");

            //        MessageBox.Show("Water-NET 프로그램을 다시 실행하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("로컬 버전 초기화는 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}

        }

        private void 수용가핸드폰번호등록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["수용가핸드폰번호등록ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            FormPopup.frmRegDMCELL oForm = new FormPopup.frmRegDMCELL();
            oForm.ShowDialog();        
        }

        #endregion

        #region 수요예측

        private void ToolStripMenuManage_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuManage"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;


            DF_MainMap.form.frmMainMap oForm = new DF_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as DF_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WaterNet.DF_Simulation.form.frmMain)))
            {
                return;
            }

            WaterNet.DF_Simulation.form.frmMain popupWindow = new WaterNet.DF_Simulation.form.frmMain();
            popupWindow.MainMap = oForm as IMapControl;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        public void 수요예측결과조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["수요예측결과조회ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            DF_MainMap.form.frmMainMap oForm = new DF_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as DF_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WaterNet.DF_Management.form.frmMain)))
            {
                return;
            }

            WaterNet.DF_Management.form.frmMain popupWindow = new WaterNet.DF_Management.form.frmMain();
            popupWindow.MainMap = oForm as IMapControl;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }


        private void 배수지운영시뮬레이션ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["배수지운영시뮬레이션ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            DF_MainMap.form.frmMainMap oForm = new DF_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as DF_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WaterNet.WV_ReservoirPredictionManage.form.frmMain)))
            {
                return;
            }

            WaterNet.WV_ReservoirPredictionManage.form.frmMain popupWindow = new WaterNet.WV_ReservoirPredictionManage.form.frmMain();
            popupWindow.MainMap = oForm as IMapControl;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }


        /// <summary>
        /// 수요예측-배수지관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuSupplyArea_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuSupplyArea"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            DF_MainMap.form.frmMainMap oForm = new DF_MainMap.form.frmMainMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as DF_MainMap.form.frmMainMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WaterNet.WV_SupplyAreaManage.form.frmSupplyArea)))
            {
                return;
            }

            WaterNet.WV_SupplyAreaManage.form.frmSupplyArea popupWindow = new WaterNet.WV_SupplyAreaManage.form.frmSupplyArea();
            popupWindow.MainMap = oForm as IMapControl;
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        #endregion

        #region 감압밸브

        private void 감압밸브감시ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["감압밸브감시ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            Form oForm = new WP_RTPRValve.frmWPMain();
            if (GetForm(oForm) == null) ((WP_RTPRValve.frmWPMain)oForm).Open();
            ShowForm(oForm);
        }

        #endregion

        #region 진단관리
        /// <summary>
        /// MapMaker 실행 메뉴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuMapMaker_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuMapMaker"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            if (System.IO.Path.GetFileName("C:\\Dr_Pipe\\MapMaker.exe").Length == 0)
            {
                WaterNetCore.MessageManager.ShowErrorMessage("C:\\Dr_Pipe\\MapMaker.exe" + " 실행파일이 없습니다.");
                return;
            }

            System.Diagnostics.ProcessStartInfo procMapMaker = new System.Diagnostics.ProcessStartInfo();
            procMapMaker.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
            procMapMaker.CreateNoWindow = false;
            procMapMaker.UseShellExecute = false;
            procMapMaker.WorkingDirectory = "C:\\Dr_Pipe";
            procMapMaker.FileName = "C:\\Dr_Pipe\\MapMaker.exe";
            try
            {
                System.Diagnostics.Process.Start(procMapMaker);
            }
            catch (Exception )
            {
                WaterNetCore.MessageManager.ShowErrorMessage("C:\\Dr_Pipe\\MapMaker.exe" + "을 실행할수 없습니다.");
            }       
            
        }

        /// <summary>
        /// Dr.Pipe 실행 메뉴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuDrPipe_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuDrPipe"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            if (System.IO.Path.GetFileName("C:\\Dr_Pipe\\main.exe").Length == 0)
            {
                WaterNetCore.MessageManager.ShowErrorMessage("C:\\Dr_Pipe\\main.exe" + " 실행파일이 없습니다.");
                return;
            }

            System.Diagnostics.ProcessStartInfo procDrPipe = new System.Diagnostics.ProcessStartInfo();
            procDrPipe.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
            procDrPipe.CreateNoWindow = false;
            procDrPipe.UseShellExecute = false;
            procDrPipe.WorkingDirectory = "C:\\Dr_Pipe";
            procDrPipe.FileName = "C:\\Dr_Pipe\\main.exe";
            try
            {
                System.Diagnostics.Process.Start(procDrPipe);
            }
            catch (Exception )
            {
                WaterNetCore.MessageManager.ShowErrorMessage("C:\\Dr_Pipe\\main.exe" + "을 실행할수 없습니다.");
            }     

        }
        #endregion 진단관리

        private void ToolStripMenuSimulation_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuSimulation"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            frmAnalysisJobExport exportInpForm = new frmAnalysisJobExport();
            exportInpForm.ShowDialog();
        }

        //실시간 경고 grid row 더블클릭 시
        private void griWarningList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            Hashtable parameters = new Hashtable();

            parameters.Add("warnNo", griWarningList.Selected.Rows[0].GetCellValue("WARN_NO").ToString());
            parameters.Add("workGbn", griWarningList.Selected.Rows[0].GetCellValue("WORK_GBN").ToString());
            parameters.Add("checkYn", griWarningList.Selected.Rows[0].GetCellValue("CHECK_YN").ToString());
            parameters.Add("warCode", griWarningList.Selected.Rows[0].GetCellValue("WAR_CODE").ToString());
            parameters.Add("serialNo", griWarningList.Selected.Rows[0].GetCellValue("SERIAL_NO").ToString());
            parameters.Add("increaseDate", griWarningList.Selected.Rows[0].GetCellValue("CREATE_DATE").ToString());

            ChangeScreenByWarning(parameters);
        }

        private void 실시간경보확인ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.12
            //			권한박탈(조회만 가능)
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간경보확인ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            //============================================================================

            frmWarning warningForm = new frmWarning();
            warningForm.parentForm = this;
            Form oForm = (Form)warningForm;
            if (GetForm(oForm) == null) ((frmWarning)oForm).Open();
            ShowForm(oForm);
        }

        /// <summary>
        /// 실시간 경보 grid 색상변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void griWarningList_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (Convert.ToString(e.Row.Cells["CHECK_YN"].Value).Equals("N"))
            {
                e.Row.Appearance.BackColor = System.Drawing.Color.FromArgb(255,151,0);   
            }
        }

        //경고정보를 이용해 메인화면을 변경
        public void ChangeScreenByWarning(Hashtable parameters)
        {
            string warnNo = parameters["warnNo"].ToString();
            string workGbn = parameters["workGbn"].ToString();
            string checkYn = parameters["checkYn"].ToString();
            string warCode = parameters["warCode"].ToString();
            string serialNo = parameters["serialNo"].ToString();
            string increaseDate = parameters["increaseDate"].ToString();

            if ("0001".Equals(workGbn))
            {
                int tabIdx = 0;

                if ("000001".Equals(warCode))
                {
                    //수리해석오류
                    tabIdx = 1;
                }
                else if ("000002".Equals(warCode))
                {
                    //압력이상
                    tabIdx = 0;
                }

                //수리해석경고
                WH_RTAnalysis.form.frmWHMain oForm = new WH_RTAnalysis.form.frmWHMain();

                //tsbtnClose_Click(null, null);

                //경고화면 호출용 변수 할당
                oForm.isWarning = true;

                oForm.dummyInpNumber = serialNo;
                oForm.increaseDate = increaseDate;
                oForm.tabIdx = tabIdx;

                Form ConvertedForm = (Form)oForm;

                UltraTab tab = GetForm(ConvertedForm);

                if (tab == null)
                {
                    ((WH_RTAnalysis.form.frmWHMain)ConvertedForm).Open();
                    ShowForm(ConvertedForm);
                }
                else
                {
                    //이미 폼이 열린상태라면 해당 폼의 경보처리 메소드를 호출한다.
                    foreach (object item in tab.TabPage.Controls)
                    {
                        if (item is IForminterface)
                        {
                            WH_RTAnalysis.form.frmWHMain tmpForm = (WH_RTAnalysis.form.frmWHMain)item;
                            tmpForm.dummyInpNumber = serialNo;
                            tmpForm.increaseDate = increaseDate;
                            tmpForm.tabIdx = tabIdx;
                            tmpForm.CallWarning();

                            ShowForm(tmpForm);
                        }
                    }
                }
            }
            else if ("0002".Equals(workGbn))
            {
                //tsbtnClose_Click(null, null);

                //누수경고
                누수감시결과ToolStripMenuItem_Click(null, null);
            }
            else if ("0003".Equals(workGbn))
            {
                //tsbtnClose_Click(null, null);

                //수질경고
                실시간감시ToolStripMenuItem_Click(null, null);
            }
            else if ("0004".Equals(workGbn))
            {
                //tsbtnClose_Click(null, null);

                //감압밸브경고
                감압밸브감시ToolStripMenuItem_Click(null, null);
            }

            //미확인인 경우 확인처리
            if ("N".Equals(checkYn))
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("WARN_NO", warnNo);

                griWarningList.DataSource = work.UpdateWarningList(conditions);
            }

            //재조회
            griWarningList.DataSource = work.SelectWarningList().Tables["WA_WARNING"];

            for (int i = 0; i < griWarningList.Rows.Count; i++)
            {
                if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
                {
                    griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
                }
                else
                {
                    griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
                }
            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            MenuContents.Checked = ((ToolStripButton)sender).Checked;
            MenuContents_Click(this, new EventArgs());
        }

        //#region Water-NET.Updater.exe 을 최신으로 유지

        ///// <summary>
        ///// Water-NET.Updater.exe를 최신으로 유지하기 위한 함수 - wjs
        ///// </summary>
        //private void UpgradeToWaterNETUpdater()
        //{
        //    string strSRC_PATH = @"C:\Water-NET\Download";
        //    string strDST_PATH = @"C:\Water-NET";

        //    StringBuilder oStringBuilder = new StringBuilder();

        //    DataSet pDS = new DataSet();

        //    OracleDBManager oDBManager = new OracleDBManager();

        //    oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

        //    oDBManager.Open();

        //    oStringBuilder.AppendLine("SELECT   FILE_RNAME AS FILENAME, SUBSTR(REG_DATE, 0, 8) AS REGDT");
        //    oStringBuilder.AppendLine("FROM     CM_VERSION");
        //    oStringBuilder.AppendLine("WHERE    FILE_NAME = 'Water-NET.Updater.exe'");

        //    pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_VERSION");

        //    if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
        //    {
        //        foreach (DataRow oDRow in pDS.Tables[0].Rows)
        //        {
        //            strSRC_PATH = strSRC_PATH + @"\" + oDRow["REGDT"].ToString().Trim() + @"\" + oDRow["FILENAME"].ToString().Trim();
        //            strDST_PATH = strDST_PATH + @"\Water-NET.Updater.exe";

        //            if (File.Exists(strSRC_PATH) == true)
        //            {
        //                File.Copy(strSRC_PATH, strDST_PATH, true);
        //            }
        //        }
        //    }

        //    pDS.Dispose();
        //    oDBManager.Close();
        //}
              
        //#endregion



        private void tsMenuInfosImport_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["tsMenuInfosImport"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WU_InfosDataImport.frmImportInfosData oform = new WaterNet.WU_InfosDataImport.frmImportInfosData();
            oform.Show();
        }

        private void waterNET수질모델생성ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["waterNET수질모델생성ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            WQ_MakeQuality.frmWQMakeQuality oform = new WQ_MakeQuality.frmWQMakeQuality();
            oform.Show();
        }

        private void 일별펌프성능모의분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["일별펌프성능모의분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;


            WE_EnergyMap.frmMain oForm = new WE_EnergyMap.frmMain();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WE_EnergyMap.frmMain;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_OperationManage.form.frmMain)))
            {
                return;
            }

            WaterNet.WE_PumpPerformanceSimulationAnalysis.form.frmMain popupWindow = new WaterNet.WE_PumpPerformanceSimulationAnalysis.form.frmMain();
            popupWindow.Owner = oForm;
            popupWindow.MainMap = oForm;
            popupWindow.Show();
        }

        private void 실시간펌프성능모의분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["실시간펌프성능모의분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;


            WE_EnergyMap.frmMain oForm = new WE_EnergyMap.frmMain();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WE_EnergyMap.frmMain;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_OperationManage.form.frmMain)))
            {
                return;
            }

            WaterNet.WE_PumpPerformanceSimulationAnalysis.form.frmRealMain popupWindow = new WaterNet.WE_PumpPerformanceSimulationAnalysis.form.frmRealMain();
            popupWindow.Owner = oForm;
            popupWindow.MainMap = oForm;
            popupWindow.Show();
        }

        private void 펌프장전력량원단위분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["펌프장전력량원단위분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;


            WE_EnergyMap.frmMain oForm = new WE_EnergyMap.frmMain();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WE_EnergyMap.frmMain;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_OperationManage.form.frmMain)))
            {
                return;
            }

            WaterNet.WE_ResultAnalysis.form.frmMain popupWindow = new WaterNet.WE_ResultAnalysis.form.frmMain();
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 펌프특성곡선분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["펌프특성곡선분석ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;


            WE_EnergyMap.frmMain oForm = new WE_EnergyMap.frmMain();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WE_EnergyMap.frmMain;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_OperationManage.form.frmMain)))
            {
                return;
            }

            WaterNet.WE_PumpCharacteristicCurve.form.frmMain popupWindow = new WaterNet.WE_PumpCharacteristicCurve.form.frmMain();
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 성능진단자료관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["성능진단자료관리ToolStripMenuItem"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;


            WE_EnergyMap.frmMain oForm = new WE_EnergyMap.frmMain();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WE_EnergyMap.frmMain;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }

            if (Utils.HashOwnedForm(oForm, typeof(WV_OperationManage.form.frmMain)))
            {
                return;
            }

            WaterNet.WE_PumpPerformanceCurveManage.form.frmMain popupWindow = new WaterNet.WE_PumpPerformanceCurveManage.form.frmMain();
            popupWindow.Owner = oForm;
            popupWindow.Show();
        }

        private void 사업장관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WaterNet.WE_PumpStationManage.form.frmMain popupWindow = new WaterNet.WE_PumpStationManage.form.frmMain();
            popupWindow.Show();
        }

        private void 펌프설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WaterNet.WE_PumpMasterManage.form.frmMain popupWindow = new WaterNet.WE_PumpMasterManage.form.frmMain();
            popupWindow.Show();
        }

        private void tsEnergyBaseInfoSet_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["tsEnergyBaseInfoSet"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            this.Cursor = Cursors.WaitCursor;
            Form oform = new WaterNet.WE_BaseInfoSet.frmMain();
            if (GetForm(oform) == null) ((WaterNet.WE_BaseInfoSet.frmMain)oform).Open();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
            
        }

        private void tsOptimunConstruct_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["tsOptimunConstruct"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            this.Cursor = Cursors.WaitCursor;
            Form oform = new WaterNet.WE_CaseSenarioSimulate.frmCaseSenario();
            if (GetForm(oform) == null) ((WaterNet.WE_CaseSenarioSimulate.frmCaseSenario)oform).Open();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }

        private void tsOptimunSpec_Click(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["tsOptimunSpec"];
            if (o == null || Convert.ToString(o).Equals("2") ? false : true != true) return;

            this.Cursor = Cursors.WaitCursor;
            Form oform = new WaterNet.WE_PumpOptimumSpec.frmOptimumSpecSimulate();
            if (GetForm(oform) == null) ((WaterNet.WE_PumpOptimumSpec.frmOptimumSpecSimulate)oform).Open();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }

        private void MapExtentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (WaterAOCore.VariableManager.m_Pipegraphic == null) return;

            OracleDBManager oDBManager = null;
            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = EMFrame.dm.EMapper.ConnectionString["SVR"];
                oDBManager.Open();

                double xmin; double xmax; double ymin; double ymax;
                WaterAOCore.ArcManager.GetMapFullExtent(out xmin, out xmax, out ymin, out ymax);

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("UPDATE MANAGEMENT_DEPARTMENT SET MIN_X = :MIN_X, MAX_X = :MAX_X, MIN_Y = :MIN_Y, MAX_Y = :MAX_Y ");
                oStringBuilder.AppendLine(" WHERE MGDPCD = :MGDPCD    ");

                IDataParameter[] oParams = {
                        new OracleParameter(":MIN_X",OracleDbType.Double),
                        new OracleParameter(":MAX_X",OracleDbType.Double),
                        new OracleParameter(":MIN_Y",OracleDbType.Double),
                        new OracleParameter(":MAX_Y",OracleDbType.Double),
                        new OracleParameter(":MGDPCD",OracleDbType.Varchar2)
                    };

                oParams[0].Value = xmin;
                oParams[1].Value = xmax;
                oParams[2].Value = ymin;
                oParams[3].Value = ymax;
                oParams[4].Value = EMFrame.statics.AppStatic.USER_SGCCD;

                oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmGMail", "MapExtentToolStripMenuItem", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
        }

        private void WaterNET수질모델생성ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 유수율ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WaterNet.WV_Revenueratio.form.frmMain popupWindow = new WaterNet.WV_Revenueratio.form.frmMain();
            popupWindow.Show();
        }

        private void 수질모의ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WaterNet.WQ_Analysis.form.frmQualityAnalysisMap oForm = new WaterNet.WQ_Analysis.form.frmQualityAnalysisMap();

            if (GetForm(oForm) == null)
            {
                oForm.Open();
                ShowForm(oForm);
            }
            else
            {
                oForm = GetForm(oForm).TabPage.Controls[0] as WaterNet.WQ_Analysis.form.frmQualityAnalysisMap;
                GetForm(oForm).TabPage.Tab.Selected = true;
            }
        }

        private void 트렌드ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form oForm = new WD_ComplexDataAnalysis.form.frmComplexDataAnalysis();
            if (GetForm(oForm) == null) ((WD_ComplexDataAnalysis.form.frmComplexDataAnalysis)oForm).Open();
            ShowForm(oForm);
        }

        private void 태그맵핑관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WaterNet.BlockApp.TagManage.FrmTagManage popupWindow = new BlockApp.TagManage.FrmTagManage();
            popupWindow.Show();
        }
    }
}
