﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace WaterNet.Water_Net.Net
{
    public class ClientListener : IDisposable
    {
        private TcpClient server = null;
        private Client client = null;
        private frmLMain wnd = null;

        private int port = 3000;
        private string serverip = string.Empty;

        public ClientListener(frmLMain wnd, string serverip, int port)
        {
            this.wnd = wnd;
            this.port = port;
            this.serverip = serverip;
        }

        #region IDisposable 멤버
        public void Dispose()
        {
            try
            {
                if (client != null && client.Connect) this.client.Dispose();
                if (server != null && server.Connected)
                {
                    this.server.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        public bool Connected
        {
            get
            {
                return (server != null && server.Connected);
            }
        }

        public bool ServerStart()
        {
            bool result = false;
            try
            {
                IPEndPoint ip = new IPEndPoint(IPAddress.Parse(this.serverip), this.port);
                server = TimeOutSocket.Connect(ip, 1000 * 60);
                if (server != null)
                {
                    //this.wnd.GetReceiveMessage("[서버] :" + ip.Address.ToString() + " : " + ip.Port.ToString());
                    if (server.Connected)  //서버와 접속하면
                    {
                        client = new Client(this.wnd, server); //Client Class 객체 생성
                        client.threadSend();
                    }

                    result = true;
                }
            }
            catch (Exception)
            {
                ;
                //this.wnd.GetReceiveMessage(ex.ToString());
            }

            return result;
        }

        public void ServerStop()
        {
            try
            {
                if (server != null && server.Connected)
                {
                    this.client.Dispose();
                    this.server.Close();
                }
            }
            catch { }
        }

        public void SendMessage(string msg)
        {
            if (this.client != null && this.client.Connect)
            {
                this.client.Send(msg);
            }
        }

        private class TimeOutSocket
        {
            private static bool IsConnectionSuccessful = false;
            private static Exception socketexception;
            private static ManualResetEvent TimeoutObject = new ManualResetEvent(false);

            public static TcpClient Connect(IPEndPoint remoteEndPoint, int timeoutMSec)
            {
                TimeoutObject.Reset();
                socketexception = null;

                string serverip = Convert.ToString(remoteEndPoint.Address);
                int serverport = remoteEndPoint.Port;
                TcpClient tcpclient = new TcpClient();

                tcpclient.BeginConnect(serverip, serverport, new AsyncCallback(CallBackMethod), tcpclient);

                if (TimeoutObject.WaitOne(timeoutMSec, false))
                {
                    if (IsConnectionSuccessful)
                    {
                        return tcpclient;
                    }
                    //else
                    //{
                    //    //throw socketexception;
                    //}
                }
                else
                {
                    tcpclient.Close();
                    //throw new TimeoutException("TimeOut Exception");
                }
                return null;
            }
            private static void CallBackMethod(IAsyncResult asyncresult)
            {
                try
                {
                    IsConnectionSuccessful = false;
                    TcpClient tcpclient = asyncresult.AsyncState as TcpClient;

                    if (tcpclient.Client != null)
                    {
                        tcpclient.EndConnect(asyncresult);
                        IsConnectionSuccessful = true;
                    }
                }
                catch (Exception ex)
                {
                    IsConnectionSuccessful = false;
                    socketexception = ex;
                }
                finally
                {
                    TimeoutObject.Set();
                }
            }
        }
    }

    public class Client : IDisposable
    {
        private TcpClient client = null;
        private string client_ip = string.Empty;
        private Thread th = null;
        private frmLMain wnd = null;

        private NetworkStream stream = null;
        private StreamReader reader = null;
        private StreamWriter writer = null;

        public Client(frmLMain wnd, TcpClient socket)
        {
            this.client = socket;
            this.wnd = wnd;
            IPEndPoint ip = (IPEndPoint)this.client.Client.RemoteEndPoint;
            this.client_ip = ip.Address.ToString();

            this.stream = client.GetStream();
            this.reader = new StreamReader(stream);
            this.writer = new StreamWriter(stream);

            try    //서버가 보내는 데이터 수신할 스레드 생성
            {
                th = new Thread(new ThreadStart(Receive));
                th.IsBackground = true;
                th.Start();
            }
            catch (Exception)
            {
                ;
                //this.wnd.GetReceiveMessage(ex.ToString()); 
            }
        }

        #region IDisposable 멤버
        public void Dispose()
        {
            try
            {
                //this.Send("C_REQ_LOGOUT:" + EMFrame.statics.AppStatic.USER_ID);
                if (th.IsAlive) th.Abort();
                this.client.Close();
                this.wnd = null;
            }
            catch { }

        }
        #endregion

        private System.Threading.Timer timer = null;
        public void threadSend()
        {
            if (timer == null) timer = new System.Threading.Timer(timerCallback, null, 0, 1000 * 60 * 3);
        }

        private void timerCallback(object state)
        {
            if (client != null && client.Connected)
            {
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_ID + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_IP + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.LOGIN_DT + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.USER_SGCCD + ",");
                oStringBuilder.Append(EMFrame.statics.AppStatic.ZONE_GBN);

                this.Send("C_REQ_REFRESH" + ":" + oStringBuilder.ToString());

                //System.Collections.Hashtable param = new System.Collections.Hashtable();
                //param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                //param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                //param.Add("LOGIN_IP", EMFrame.statics.AppStatic.USER_IP);

                //EMFrame.utils.DatabaseUtils.GetInstance().SessionInfo(param);
            }
        }

        public bool Connect
        {
            get
            {
                return this.client.Connected;
            }
        }

        public string Client_IP
        {
            get
            {
                return this.client_ip;
            }
        }

        /// <summary>
        /// 서버가 보내는 메세지 수신하기
        /// </summary>
        public void Receive()
        {
            try
            {
                while (client != null && client.Connected)
                {
                    string msg = reader.ReadLine();

                    EMFrame.utils.LoggerUtils.logger.Info("Receive => " + msg);

                    string[] token = msg.Split(':',',');
                    switch (token[0])
                    {
                        case "C_REQ_DUAL_LOGIN": //이중접속
                            this.wnd.GetExitMessage("[" + token[1] + "](" + token[2] + ")" + token[3] + " 에서 재접속되었습니다. 시스템을 종료합니다.");
                            break;
                        case "C_SERVER_OUT": //통합서버 종료
                            this.wnd.GetExitMessage(msg);
                            break;
                        case "C_REQ_MAXIMUM_OUT": //센터의 라이센스 권한 수 초과
                            this.wnd.GetExitMessage(msg);
                            break;
                        case "C_REQ_REFRESH": //통합서버 에서 리턴됨
                            //activeRefresh(msg);
                            break;
                        case "C_REQ_LOGIN": //통합서버 에서 리턴됨
                            //ProcessLogin(msg);
                            break;
                    }
                }

            }
            catch { }
           
        }

        /// <summary>
        /// 접속한 상대방에 데이터 전송
        /// </summary>
        /// <param name="msg"></param>
        public void Send(string msg)
        {
            try
            {
                if (client != null && client.Connected)  //상대방과 연결되어 있는경우
                {
                    EMFrame.utils.LoggerUtils.logger.Info("Send => " + msg);

                    //this.wnd.GetSendMessage(msg);
                    writer.WriteLine(msg);
                    writer.Flush();
                }
                else
                {
                    EMFrame.utils.LoggerUtils.logger.Info("Send => Not client"); 
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// activeRefresh를 통합서버에서 성공적으로 리턴함
        /// log 테이블의 sesstion_dt에 update
        /// </summary>
        /// <param name="msg"></param>
        private void activeRefresh(string msg)
        {
            try
            {
                System.Collections.Hashtable param = new System.Collections.Hashtable();
                param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                param.Add("LOGIN_IP", EMFrame.statics.AppStatic.USER_IP);

                EMFrame.utils.DatabaseUtils.GetInstance().SessionInfo(param);
            }
            catch { }
        }

        /// <summary>
        /// 서버에 성공적으로 로그인 : 통합서버에서 리턴함.
        /// </summary>
        /// <param name="msg"></param>
        private void ProcessLogin(string msg)
        {
            try
            {
                ///사용자 로그인 정보를 log테이블에 저장
                System.Collections.Hashtable param = new System.Collections.Hashtable();
                param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                param.Add("LOGIN_IP", EMFrame.statics.AppStatic.USER_IP);
                param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                param.Add("SGCCD", EMFrame.statics.AppStatic.USER_SGCCD);
                
                EMFrame.utils.DatabaseUtils.GetInstance().LoginInfo(param);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }
            
        }

    }

}
