﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace WaterNet.Water_Net
{
    static class Program
    {
        private static Water_Net.LicenseInitializer m_AOLicenseInitializer = new Water_Net.LicenseInitializer();

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void BringWindowToTop(IntPtr hWnd);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void SetForegroundWindow(IntPtr hWnd);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string mutexId = string.Format("{0}", "WaterNet.Water_Net.frmLMain");
            using (Mutex mtx = new Mutex(false, mutexId))
            {
                if (!mtx.WaitOne(0, true))
                {
                    MessageBox.Show("water-NET 프로그램이 이미 실행중입니다.", "실행중", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                    return;
                }
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //m_AOLicenseInitializer.InitializeApplication(new ESRI.ArcGIS.esriSystem.esriLicenseProductCode[] 
            //                          { ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine },
            //                                                new ESRI.ArcGIS.esriSystem.esriLicenseExtensionCode[] { });


            //if (WaterAOCore.ArcManager.CheckOutLicenses(ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine) != ESRI.ArcGIS.esriSystem.esriLicenseStatus.esriLicenseAvailable)
            //{
            //    WaterNetCore.MessageManager.ShowExclamationMessage("ArcEngine Runtime License가 없습니다.");
            //    Application.Exit();
            //    return;
            //}

            if (ESRI.ArcGIS.RuntimeManager.ActiveRuntime == null)
            {
                if (!(ESRI.ArcGIS.RuntimeManager.Bind(ESRI.ArcGIS.ProductCode.Engine)))
                {
                    WaterNetCore.MessageManager.ShowExclamationMessage("ArcEngine Runtime License가 없습니다.");
                    Application.Exit();
                    return;
                }
            }

            ESRI.ArcGIS.esriSystem.AoInitialize aoi = new ESRI.ArcGIS.esriSystem.AoInitialize();
            ESRI.ArcGIS.esriSystem.esriLicenseProductCode productcode = ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine;
            if (aoi.IsProductCodeAvailable(productcode) == ESRI.ArcGIS.esriSystem.esriLicenseStatus.esriLicenseAvailable)
            {
                aoi.Initialize(productcode);
            }


            //Database Connection 정보 확인
            if (System.IO.File.Exists(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH) != true)
            {
                MessageBox.Show("Water-NET 데이터베이스 환경설정파일을 찾을 수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                FormPopup.frmDatabase oForm = new FormPopup.frmDatabase();
                oForm.ShowDialog();
                Application.Exit();
                return;
            }

            try
            {
                ///water-NET 지방클라이언트 시스템이므로 변수설정
                EMFrame.statics.AppStatic.ZONE_GBN = "지방";

                //Logging 설정
                EMFrame.config.EConfig.INITIALIZE_LOG();

                //오라클 환경변수 특성을 타서 환경변수 등록함.
                Environment.SetEnvironmentVariable("NLS_LANG", "KOREAN_KOREA.KO16MSWIN949");
                //Environment.SetEnvironmentVariable("172.20.250.86", "komgisi");
                SetHostsData();

                //시스템실행시 초기환경:센터별 DB접속경로, 센터별 라이센스 설정
                SetDatabaseConnectString();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Application.Exit(); 
                return; 
            }

            //try
            //{
            //    //최신버전(System) 체크
            //    using (form.frmFTPDownload download = new form.frmFTPDownload())
            //    {
            //        download.DATATYPE = "SYSTEM";
            //        download.ShowDialog();
            //        if (download.SUCCESS)
            //        {
            //            if (download.REPLACED)
            //            {
            //                MessageBox.Show("실행파일이 변경되었으므로, 다시 실행하십시오.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //                Application.Exit();
            //                return;
            //            }
            //        }
            //        else
            //        {
            //            MessageBox.Show("FTP 서버에 접속할 수 없으므로, 업그레이드 할수 없습니다.\n\r현재 버전으로 실행됩니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        }
            //    }
            //}
            //catch { }

            //Login
            using (form.frmLogOn login = new form.frmLogOn())
            {
                login.ShowDialog();
                if (!((form.frmLogOn)login).IsLogOn)
                {
                    Application.Exit();
                    return;
                }
            }

            //try
            //{
            //    //최신버전(Data) 체크
            //    using (form.frmFTPDownload download = new form.frmFTPDownload())
            //    {
            //        download.DATATYPE = "DATA";
            //        download.ShowDialog();
            //        if (!download.SUCCESS)
            //        {
            //            MessageBox.Show("FTP 서버에 접속할 수 없으므로, 업그레이드 할수 없습니다.\n\r현재 버전으로 실행됩니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        }
            //    }
            //}
            //catch { }

            try
            {
               Application.Run(new frmLMain());
                m_AOLicenseInitializer.ShutdownApplication();
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.ToString());
                Application.Exit();
            }
        }

        private static void SetHostsData()
        {
            try
            {
                System.Security.Principal.WindowsIdentity user = System.Security.Principal.WindowsIdentity.GetCurrent();
                System.Security.Principal.WindowsPrincipal pu = new System.Security.Principal.WindowsPrincipal(user);
                if (pu.IsInRole("Administrators"))
                {
                    var systemPath = Environment.GetFolderPath(Environment.SpecialFolder.System);
                    var path = System.IO.Path.Combine(systemPath, @"drivers\etc\hosts");
                    StreamReader reader = new StreamReader(path, System.Text.Encoding.Default);
                    string data = reader.ReadToEnd();
                    reader.Close();
                    if (data.IndexOf("172.20.250.86") == -1 || data.IndexOf("komgisi") == -1)
                    {
                        data += Environment.NewLine;
                        data += string.Format("{0}\t{1}", "172.20.250.86", "komgisi");
                        StreamWriter writer = new StreamWriter(path, false, System.Text.Encoding.Default);
                        writer.Write(data);
                        writer.Close();
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 연결 문자열 구하기 - 통합서버의 DB경로 설정
        /// 연결환경을 XML file로 변경함
        /// </summary>
        /// <returns>연결 문자열</returns>
        /// <remarks></remarks>
        private static string GetConnectionString()
        {
            WaterNetCore.FunctionManager.ReadXml doc = new WaterNetCore.FunctionManager.ReadXml(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH);

            string svcName = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/servicename"));
            string ip = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/ip"));
            string id = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/id"));
            string passwd = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/password"));
            string port = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/port"));

            //string conStr = "Data Source=(DESCRIPTION="
            //                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
            //                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
            //                + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";

            string conStr = "Data Source=(DESCRIPTION="
                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
                + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";
            return conStr;
        }

        #region 프로그램 실행시 한번만 실행 - static 변수들 설정
        /// <summary>
        /// 데이터베이스 접근경로 등록
        /// </summary>
        private static void SetDatabaseConnectString()
        {
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper();
                string conStr = GetConnectionString();
                EMFrame.dm.EMapper.ConnectionString.Add("SVR", conStr);
                mapper.SetConnectionString("SVR");
                mapper.Open();

                System.Text.StringBuilder oStringBuilder = new System.Text.StringBuilder();
                oStringBuilder.AppendLine("SELECT * FROM MANAGEMENT_DEPARTMENT WHERE ZONE_GBN = '" + EMFrame.statics.AppStatic.ZONE_GBN + "'");

                System.Data.DataTable dsSource = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });
                foreach (System.Data.DataRow row in dsSource.Rows)
                {
                    string svcName = Convert.ToString(row["DB_SID"]);
                    string ip = Convert.ToString(row["DB_ADDRESS"]);
                    string id = Convert.ToString(row["DB_ID"]);
                    string passwd = Convert.ToString(row["DB_PASS"]);
                    string port = Convert.ToString(row["DB_PORT"]);

                    if (string.IsNullOrEmpty(svcName) || string.IsNullOrEmpty(ip) || string.IsNullOrEmpty(id) || string.IsNullOrEmpty(passwd) || string.IsNullOrEmpty(port)) continue;

                    ///센터별 DB접속경로 등록
                    conStr = "Data Source=(DESCRIPTION="
                        + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + Convert.ToString(row["DB_ADDRESS"]) + ")(PORT=" + Convert.ToString(row["DB_PORT"]) + ")))"
                        + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + Convert.ToString(row["DB_SID"]) + ")));"
                        + "User Id=" + Convert.ToString(row["DB_ID"]) + ";Password=" + Convert.ToString(row["DB_PASS"]) + ";Enlist=false";
                    EMFrame.dm.EMapper.ConnectionString.Add(Convert.ToString(row["MGDPCD"]), conStr);
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }
        #endregion 프로그램 실행시 한번만 실행 - static 변수들 설정

    }
}
