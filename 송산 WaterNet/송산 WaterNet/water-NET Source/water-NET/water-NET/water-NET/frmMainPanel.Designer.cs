﻿namespace WaterNet.Water_Net
{
    partial class frmMainPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.DayOfWeekLook dayOfWeekLook3 = new Infragistics.Win.UltraWinSchedule.DayOfWeekLook(System.DayOfWeek.Sunday);
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.DayOfWeekLook dayOfWeekLook4 = new Infragistics.Win.UltraWinSchedule.DayOfWeekLook(System.DayOfWeek.Saturday);
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn4 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Column 0");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn5 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Column 1");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn6 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Column 2");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainPanel));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ultraCalendarLook1 = new Infragistics.Win.UltraWinSchedule.UltraCalendarLook(this.components);
            this.ultraDataSource2 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.ultraDataSource1 = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel_Main = new System.Windows.Forms.Panel();
            this.axShockwaveFlash1 = new AxShockwaveFlashObjects.AxShockwaveFlash();
            this.axMap = new ESRI.ArcGIS.Controls.AxMapControl();
            this.pnlList04 = new System.Windows.Forms.Panel();
            this.pnlMovie = new System.Windows.Forms.Panel();
            this.pnlManual = new System.Windows.Forms.Panel();
            this.lblChart = new System.Windows.Forms.Label();
            this.lblList = new System.Windows.Forms.Label();
            this.pnlChart03 = new System.Windows.Forms.Panel();
            this.pnlChart02 = new System.Windows.Forms.Panel();
            this.pnlChart01 = new System.Windows.Forms.Panel();
            this.pnlList03 = new System.Windows.Forms.Panel();
            this.pnlList02 = new System.Windows.Forms.Panel();
            this.pnlList01 = new System.Windows.Forms.Panel();
            this.pnlChart = new System.Windows.Forms.Panel();
            this.chart03 = new ChartFX.WinForms.Chart();
            this.chart02 = new ChartFX.WinForms.Chart();
            this.chart01 = new ChartFX.WinForms.Chart();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.ugList04 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ugList03 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ugList02 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ugList01 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pnlfuncDF = new System.Windows.Forms.Panel();
            this.pnlfuncWaterCrisis = new System.Windows.Forms.Panel();
            this.pnlfuncWaterRatio = new System.Windows.Forms.Panel();
            this.pnlfuncLeakgeMonitoring = new System.Windows.Forms.Panel();
            this.pnlfuncSimu = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).BeginInit();
            this.panel_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axShockwaveFlash1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            this.pnlChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart01)).BeginInit();
            this.pnlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugList04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugList03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugList02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugList01)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ultraCalendarLook1
            // 
            appearance49.BackColor = System.Drawing.Color.Gray;
            appearance49.FontData.BoldAsString = "True";
            appearance49.ForeColor = System.Drawing.Color.Blue;
            appearance49.TextHAlignAsString = "Right";
            appearance49.TextVAlignAsString = "Middle";
            this.ultraCalendarLook1.ActiveDayAppearance = appearance49;
            appearance50.TextHAlignAsString = "Right";
            appearance50.TextVAlignAsString = "Middle";
            this.ultraCalendarLook1.DayAppearance = appearance50;
            appearance51.ForeColor = System.Drawing.Color.Salmon;
            dayOfWeekLook3.Appearance = appearance51;
            appearance52.ForeColor = System.Drawing.Color.Salmon;
            dayOfWeekLook3.HeaderAppearance = appearance52;
            appearance53.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            dayOfWeekLook4.Appearance = appearance53;
            appearance54.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            dayOfWeekLook4.HeaderAppearance = appearance54;
            this.ultraCalendarLook1.DaysOfWeekLook.Add(dayOfWeekLook3);
            this.ultraCalendarLook1.DaysOfWeekLook.Add(dayOfWeekLook4);
            // 
            // ultraDataSource1
            // 
            this.ultraDataSource1.Band.Columns.AddRange(new object[] {
            ultraDataColumn4,
            ultraDataColumn5,
            ultraDataColumn6});
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Main.BackgroundImage = global::WaterNet.Water_Net.Properties.Resources.con_btn_none2;
            this.panel_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel_Main.Controls.Add(this.axShockwaveFlash1);
            this.panel_Main.Controls.Add(this.axMap);
            this.panel_Main.Controls.Add(this.pnlList04);
            this.panel_Main.Controls.Add(this.pnlMovie);
            this.panel_Main.Controls.Add(this.pnlManual);
            this.panel_Main.Controls.Add(this.lblChart);
            this.panel_Main.Controls.Add(this.lblList);
            this.panel_Main.Controls.Add(this.pnlChart03);
            this.panel_Main.Controls.Add(this.pnlChart02);
            this.panel_Main.Controls.Add(this.pnlChart01);
            this.panel_Main.Controls.Add(this.pnlList03);
            this.panel_Main.Controls.Add(this.pnlList02);
            this.panel_Main.Controls.Add(this.pnlList01);
            this.panel_Main.Controls.Add(this.pnlChart);
            this.panel_Main.Controls.Add(this.pnlGrid);
            this.panel_Main.Controls.Add(this.pnlfuncDF);
            this.panel_Main.Controls.Add(this.pnlfuncWaterCrisis);
            this.panel_Main.Controls.Add(this.pnlfuncWaterRatio);
            this.panel_Main.Controls.Add(this.pnlfuncLeakgeMonitoring);
            this.panel_Main.Controls.Add(this.pnlfuncSimu);
            this.panel_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Main.Location = new System.Drawing.Point(0, 0);
            this.panel_Main.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Size = new System.Drawing.Size(1008, 732);
            this.panel_Main.TabIndex = 18;
            // 
            // axShockwaveFlash1
            // 
            this.axShockwaveFlash1.Enabled = true;
            this.axShockwaveFlash1.Location = new System.Drawing.Point(648, 91);
            this.axShockwaveFlash1.Name = "axShockwaveFlash1";
            this.axShockwaveFlash1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axShockwaveFlash1.OcxState")));
            this.axShockwaveFlash1.Size = new System.Drawing.Size(316, 255);
            this.axShockwaveFlash1.TabIndex = 17;
            // 
            // axMap
            // 
            this.axMap.Location = new System.Drawing.Point(51, 74);
            this.axMap.Name = "axMap";
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Padding = new System.Windows.Forms.Padding(4);
            this.axMap.Size = new System.Drawing.Size(185, 146);
            this.axMap.TabIndex = 16;
            // 
            // pnlList04
            // 
            this.pnlList04.BackColor = System.Drawing.Color.Transparent;
            this.pnlList04.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlList04.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlList04.Location = new System.Drawing.Point(374, 401);
            this.pnlList04.Margin = new System.Windows.Forms.Padding(0);
            this.pnlList04.Name = "pnlList04";
            this.pnlList04.Size = new System.Drawing.Size(40, 35);
            this.pnlList04.TabIndex = 13;
            // 
            // pnlMovie
            // 
            this.pnlMovie.BackColor = System.Drawing.Color.Transparent;
            this.pnlMovie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlMovie.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlMovie.Location = new System.Drawing.Point(805, 19);
            this.pnlMovie.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMovie.Name = "pnlMovie";
            this.pnlMovie.Size = new System.Drawing.Size(68, 17);
            this.pnlMovie.TabIndex = 11;
            // 
            // pnlManual
            // 
            this.pnlManual.BackColor = System.Drawing.Color.Transparent;
            this.pnlManual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlManual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlManual.Location = new System.Drawing.Point(881, 19);
            this.pnlManual.Margin = new System.Windows.Forms.Padding(0);
            this.pnlManual.Name = "pnlManual";
            this.pnlManual.Size = new System.Drawing.Size(83, 17);
            this.pnlManual.TabIndex = 10;
            // 
            // lblChart
            // 
            this.lblChart.BackColor = System.Drawing.Color.White;
            this.lblChart.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblChart.ForeColor = System.Drawing.Color.DimGray;
            this.lblChart.Location = new System.Drawing.Point(647, 443);
            this.lblChart.Name = "lblChart";
            this.lblChart.Size = new System.Drawing.Size(311, 24);
            this.lblChart.TabIndex = 9;
            this.lblChart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblList
            // 
            this.lblList.BackColor = System.Drawing.Color.White;
            this.lblList.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)), true);
            this.lblList.ForeColor = System.Drawing.Color.DimGray;
            this.lblList.Location = new System.Drawing.Point(290, 443);
            this.lblList.Name = "lblList";
            this.lblList.Size = new System.Drawing.Size(280, 24);
            this.lblList.TabIndex = 8;
            this.lblList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlChart03
            // 
            this.pnlChart03.BackColor = System.Drawing.Color.Transparent;
            this.pnlChart03.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlChart03.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlChart03.Location = new System.Drawing.Point(692, 401);
            this.pnlChart03.Margin = new System.Windows.Forms.Padding(0);
            this.pnlChart03.Name = "pnlChart03";
            this.pnlChart03.Size = new System.Drawing.Size(40, 35);
            this.pnlChart03.TabIndex = 7;
            // 
            // pnlChart02
            // 
            this.pnlChart02.BackColor = System.Drawing.Color.Transparent;
            this.pnlChart02.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlChart02.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlChart02.Location = new System.Drawing.Point(653, 401);
            this.pnlChart02.Margin = new System.Windows.Forms.Padding(0);
            this.pnlChart02.Name = "pnlChart02";
            this.pnlChart02.Size = new System.Drawing.Size(40, 35);
            this.pnlChart02.TabIndex = 7;
            // 
            // pnlChart01
            // 
            this.pnlChart01.BackColor = System.Drawing.Color.Transparent;
            this.pnlChart01.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlChart01.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlChart01.Location = new System.Drawing.Point(614, 401);
            this.pnlChart01.Margin = new System.Windows.Forms.Padding(0);
            this.pnlChart01.Name = "pnlChart01";
            this.pnlChart01.Size = new System.Drawing.Size(40, 35);
            this.pnlChart01.TabIndex = 6;
            // 
            // pnlList03
            // 
            this.pnlList03.BackColor = System.Drawing.Color.Transparent;
            this.pnlList03.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlList03.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlList03.Location = new System.Drawing.Point(335, 401);
            this.pnlList03.Margin = new System.Windows.Forms.Padding(0);
            this.pnlList03.Name = "pnlList03";
            this.pnlList03.Size = new System.Drawing.Size(40, 35);
            this.pnlList03.TabIndex = 7;
            // 
            // pnlList02
            // 
            this.pnlList02.BackColor = System.Drawing.Color.Transparent;
            this.pnlList02.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlList02.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlList02.Location = new System.Drawing.Point(296, 401);
            this.pnlList02.Margin = new System.Windows.Forms.Padding(0);
            this.pnlList02.Name = "pnlList02";
            this.pnlList02.Size = new System.Drawing.Size(40, 35);
            this.pnlList02.TabIndex = 6;
            // 
            // pnlList01
            // 
            this.pnlList01.BackColor = System.Drawing.Color.Transparent;
            this.pnlList01.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlList01.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlList01.Location = new System.Drawing.Point(257, 401);
            this.pnlList01.Margin = new System.Windows.Forms.Padding(0);
            this.pnlList01.Name = "pnlList01";
            this.pnlList01.Size = new System.Drawing.Size(40, 35);
            this.pnlList01.TabIndex = 5;
            // 
            // pnlChart
            // 
            this.pnlChart.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlChart.BackColor = System.Drawing.Color.Transparent;
            this.pnlChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlChart.Controls.Add(this.chart03);
            this.pnlChart.Controls.Add(this.chart02);
            this.pnlChart.Controls.Add(this.chart01);
            this.pnlChart.Location = new System.Drawing.Point(623, 470);
            this.pnlChart.Margin = new System.Windows.Forms.Padding(0);
            this.pnlChart.Name = "pnlChart";
            this.pnlChart.Padding = new System.Windows.Forms.Padding(2);
            this.pnlChart.Size = new System.Drawing.Size(350, 257);
            this.pnlChart.TabIndex = 4;
            // 
            // chart03
            // 
            this.chart03.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart03.LegendBox.Visible = false;
            this.chart03.Location = new System.Drawing.Point(205, 50);
            this.chart03.Name = "chart03";
            this.chart03.Size = new System.Drawing.Size(52, 32);
            this.chart03.TabIndex = 2;
            // 
            // chart02
            // 
            this.chart02.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart02.LegendBox.Visible = false;
            this.chart02.Location = new System.Drawing.Point(129, 92);
            this.chart02.Name = "chart02";
            this.chart02.Size = new System.Drawing.Size(52, 32);
            this.chart02.TabIndex = 1;
            // 
            // chart01
            // 
            this.chart01.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart01.LegendBox.Visible = false;
            this.chart01.Location = new System.Drawing.Point(61, 22);
            this.chart01.Name = "chart01";
            this.chart01.Size = new System.Drawing.Size(61, 32);
            this.chart01.TabIndex = 0;
            // 
            // pnlGrid
            // 
            this.pnlGrid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlGrid.BackColor = System.Drawing.Color.Transparent;
            this.pnlGrid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlGrid.Controls.Add(this.ugList04);
            this.pnlGrid.Controls.Add(this.ugList03);
            this.pnlGrid.Controls.Add(this.ugList02);
            this.pnlGrid.Controls.Add(this.ugList01);
            this.pnlGrid.Location = new System.Drawing.Point(264, 470);
            this.pnlGrid.Margin = new System.Windows.Forms.Padding(0);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Padding = new System.Windows.Forms.Padding(2, 2, 2, 0);
            this.pnlGrid.Size = new System.Drawing.Size(328, 257);
            this.pnlGrid.TabIndex = 3;
            // 
            // ugList04
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ugList04.DisplayLayout.Appearance = appearance13;
            this.ugList04.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ugList04.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList04.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList04.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ugList04.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList04.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ugList04.DisplayLayout.MaxColScrollRegions = 1;
            this.ugList04.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ugList04.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ugList04.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ugList04.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ugList04.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ugList04.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ugList04.DisplayLayout.Override.CellAppearance = appearance20;
            this.ugList04.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ugList04.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList04.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ugList04.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ugList04.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugList04.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ugList04.DisplayLayout.Override.RowAppearance = appearance23;
            this.ugList04.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ugList04.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ugList04.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ugList04.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ugList04.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ugList04.Location = new System.Drawing.Point(75, 74);
            this.ugList04.Name = "ugList04";
            this.ugList04.Size = new System.Drawing.Size(135, 66);
            this.ugList04.TabIndex = 3;
            this.ugList04.Text = "ultraGrid1";
            // 
            // ugList03
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ugList03.DisplayLayout.Appearance = appearance1;
            this.ugList03.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ugList03.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList03.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList03.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ugList03.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList03.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ugList03.DisplayLayout.MaxColScrollRegions = 1;
            this.ugList03.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ugList03.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ugList03.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ugList03.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ugList03.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ugList03.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ugList03.DisplayLayout.Override.CellAppearance = appearance8;
            this.ugList03.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ugList03.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList03.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ugList03.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ugList03.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugList03.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ugList03.DisplayLayout.Override.RowAppearance = appearance11;
            this.ugList03.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ugList03.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ugList03.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ugList03.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ugList03.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ugList03.Location = new System.Drawing.Point(54, 42);
            this.ugList03.Name = "ugList03";
            this.ugList03.Size = new System.Drawing.Size(135, 66);
            this.ugList03.TabIndex = 2;
            this.ugList03.Text = "ultraGrid1";
            // 
            // ugList02
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ugList02.DisplayLayout.Appearance = appearance25;
            this.ugList02.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ugList02.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList02.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList02.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ugList02.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList02.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ugList02.DisplayLayout.MaxColScrollRegions = 1;
            this.ugList02.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ugList02.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ugList02.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ugList02.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ugList02.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ugList02.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ugList02.DisplayLayout.Override.CellAppearance = appearance32;
            this.ugList02.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ugList02.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList02.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ugList02.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ugList02.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugList02.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ugList02.DisplayLayout.Override.RowAppearance = appearance35;
            this.ugList02.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ugList02.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ugList02.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ugList02.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ugList02.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ugList02.Location = new System.Drawing.Point(73, 74);
            this.ugList02.Name = "ugList02";
            this.ugList02.Size = new System.Drawing.Size(135, 66);
            this.ugList02.TabIndex = 1;
            this.ugList02.Text = "ultraGrid1";
            // 
            // ugList01
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ugList01.DisplayLayout.Appearance = appearance37;
            this.ugList01.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ugList01.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList01.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList01.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.ugList01.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugList01.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.ugList01.DisplayLayout.MaxColScrollRegions = 1;
            this.ugList01.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ugList01.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ugList01.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.ugList01.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ugList01.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.ugList01.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ugList01.DisplayLayout.Override.CellAppearance = appearance44;
            this.ugList01.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ugList01.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.ugList01.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.ugList01.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.ugList01.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugList01.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.ugList01.DisplayLayout.Override.RowAppearance = appearance47;
            this.ugList01.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ugList01.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.ugList01.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ugList01.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ugList01.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ugList01.Location = new System.Drawing.Point(101, 117);
            this.ugList01.Name = "ugList01";
            this.ugList01.Size = new System.Drawing.Size(135, 66);
            this.ugList01.TabIndex = 0;
            this.ugList01.Text = "ultraGrid1";
            // 
            // pnlfuncDF
            // 
            this.pnlfuncDF.BackColor = System.Drawing.Color.Transparent;
            this.pnlfuncDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlfuncDF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlfuncDF.Location = new System.Drawing.Point(142, 564);
            this.pnlfuncDF.Margin = new System.Windows.Forms.Padding(0);
            this.pnlfuncDF.Name = "pnlfuncDF";
            this.pnlfuncDF.Size = new System.Drawing.Size(45, 42);
            this.pnlfuncDF.TabIndex = 2;
            this.pnlfuncDF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlfuncDF_MouseClick);
            // 
            // pnlfuncWaterCrisis
            // 
            this.pnlfuncWaterCrisis.BackColor = System.Drawing.Color.Transparent;
            this.pnlfuncWaterCrisis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlfuncWaterCrisis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlfuncWaterCrisis.Location = new System.Drawing.Point(157, 495);
            this.pnlfuncWaterCrisis.Margin = new System.Windows.Forms.Padding(0);
            this.pnlfuncWaterCrisis.Name = "pnlfuncWaterCrisis";
            this.pnlfuncWaterCrisis.Size = new System.Drawing.Size(45, 42);
            this.pnlfuncWaterCrisis.TabIndex = 2;
            this.pnlfuncWaterCrisis.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlfuncWaterCrisis_MouseClick);
            // 
            // pnlfuncWaterRatio
            // 
            this.pnlfuncWaterRatio.BackColor = System.Drawing.Color.Transparent;
            this.pnlfuncWaterRatio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlfuncWaterRatio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlfuncWaterRatio.Location = new System.Drawing.Point(96, 461);
            this.pnlfuncWaterRatio.Margin = new System.Windows.Forms.Padding(0);
            this.pnlfuncWaterRatio.Name = "pnlfuncWaterRatio";
            this.pnlfuncWaterRatio.Size = new System.Drawing.Size(45, 42);
            this.pnlfuncWaterRatio.TabIndex = 2;
            this.pnlfuncWaterRatio.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlfuncWaterRatio_MouseClick);
            // 
            // pnlfuncLeakgeMonitoring
            // 
            this.pnlfuncLeakgeMonitoring.BackColor = System.Drawing.Color.Transparent;
            this.pnlfuncLeakgeMonitoring.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlfuncLeakgeMonitoring.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlfuncLeakgeMonitoring.Location = new System.Drawing.Point(20, 551);
            this.pnlfuncLeakgeMonitoring.Margin = new System.Windows.Forms.Padding(0);
            this.pnlfuncLeakgeMonitoring.Name = "pnlfuncLeakgeMonitoring";
            this.pnlfuncLeakgeMonitoring.Size = new System.Drawing.Size(45, 42);
            this.pnlfuncLeakgeMonitoring.TabIndex = 2;
            this.pnlfuncLeakgeMonitoring.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlfuncLeakgeMonitoring_MouseClick);
            // 
            // pnlfuncSimu
            // 
            this.pnlfuncSimu.BackColor = System.Drawing.Color.Transparent;
            this.pnlfuncSimu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlfuncSimu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlfuncSimu.Location = new System.Drawing.Point(26, 479);
            this.pnlfuncSimu.Margin = new System.Windows.Forms.Padding(0);
            this.pnlfuncSimu.Name = "pnlfuncSimu";
            this.pnlfuncSimu.Size = new System.Drawing.Size(45, 42);
            this.pnlfuncSimu.TabIndex = 1;
            this.pnlfuncSimu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlfuncSimu_MouseClick);
            // 
            // frmMainPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 732);
            this.Controls.Add(this.panel_Main);
            this.Name = "frmMainPanel";
            this.Text = "frmMainPanel";
            this.Load += new System.EventHandler(this.frmMainPanel_Load);
            this.SizeChanged += new System.EventHandler(this.frmMainPanel_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.frmMainPanel_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).EndInit();
            this.panel_Main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axShockwaveFlash1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            this.pnlChart.ResumeLayout(false);
            this.pnlChart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart01)).EndInit();
            this.pnlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugList04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugList03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugList02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugList01)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarLook ultraCalendarLook1;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource1;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource2;
        public ESRI.ArcGIS.Controls.AxMapControl axMap;
        private System.Windows.Forms.Panel panel_Main;
        private System.Windows.Forms.Panel pnlList04;
        private System.Windows.Forms.Panel pnlMovie;
        private System.Windows.Forms.Panel pnlManual;
        private System.Windows.Forms.Label lblChart;
        private System.Windows.Forms.Label lblList;
        private System.Windows.Forms.Panel pnlChart03;
        private System.Windows.Forms.Panel pnlChart02;
        private System.Windows.Forms.Panel pnlChart01;
        private System.Windows.Forms.Panel pnlList03;
        private System.Windows.Forms.Panel pnlList02;
        private System.Windows.Forms.Panel pnlList01;
        private System.Windows.Forms.Panel pnlChart;
        private ChartFX.WinForms.Chart chart03;
        private ChartFX.WinForms.Chart chart02;
        private ChartFX.WinForms.Chart chart01;
        private System.Windows.Forms.Panel pnlGrid;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugList04;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugList03;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugList02;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugList01;
        private System.Windows.Forms.Panel pnlfuncDF;
        private System.Windows.Forms.Panel pnlfuncWaterCrisis;
        private System.Windows.Forms.Panel pnlfuncWaterRatio;
        private System.Windows.Forms.Panel pnlfuncLeakgeMonitoring;
        private System.Windows.Forms.Panel pnlfuncSimu;
        private AxShockwaveFlashObjects.AxShockwaveFlash axShockwaveFlash1;
        //private AxShockwaveFlashObjects.AxShockwaveFlash axShockwaveFlash1;
    }
}