﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using ChartFX.WinForms;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WaterAOCore;
using WaterNet.WV_LeakageManage.work;

using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using Microsoft.Win32;

namespace WaterNet.Water_Net
{
    public partial class frmMainPanel : Form
    {
        private Rectangle m_rect = Rectangle.Empty;

        public frmMainPanel()
        {
            InitializeComponent();
            InitializeSetting();
        }

        /// <summary>
        /// 초기 그리드 설정
        /// </summary>
        private void InitializeSetting()
        {
            try
            {
                this.axShockwaveFlash1.LoadMovie(0, Environment.CurrentDirectory + @"\mv_guide.swf");
                this.axShockwaveFlash1.Play();
            }
            catch { }
            

            // Header 스타일 설정
            Infragistics.Win.Appearance appearanceHeader = new Infragistics.Win.Appearance();
            appearanceHeader.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(171)), ((System.Byte)(208)), ((System.Byte)(188)));
            appearanceHeader.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(193)), ((System.Byte)(216)), ((System.Byte)(231)));
            appearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(109)), ((System.Byte)(151)), ((System.Byte)(177)));
            appearanceHeader.FontData.Bold = DefaultableBoolean.True;
            appearanceHeader.FontData.Name = "굴림";
            appearanceHeader.FontData.SizeInPoints = 9;

            // Row 스타일 설정 (border, font 색)
            Infragistics.Win.Appearance appearanceRow = new Infragistics.Win.Appearance();
            appearanceRow.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(222)), ((System.Byte)(235)), ((System.Byte)(237)));
            appearanceRow.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            UltraGridColumn oUltraGridColumn;

            #region 실시간 공급량 그리드 설정

            oUltraGridColumn = this.ugList01.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C1";
            oUltraGridColumn.Header.Caption = "계통";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = this.ugList01.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C2";
            oUltraGridColumn.Header.Caption = "현재공급\n(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            oUltraGridColumn = this.ugList01.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C3";
            oUltraGridColumn.Header.Caption = "금일누적\n(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            oUltraGridColumn = this.ugList01.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C4";
            oUltraGridColumn.Header.Caption = "전일공급\n(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            FormManager.SetGridStyle(ugList01);

            this.ugList01.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            this.ugList01.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;
            this.ugList01.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
            this.ugList01.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            // Header 스타일 설정
            this.ugList01.DisplayLayout.Override.HeaderAppearance = appearanceHeader;
            this.ugList01.DisplayLayout.BorderStyle = UIElementBorderStyle.None;
            this.ugList01.UseFlatMode = DefaultableBoolean.False;
            this.ugList01.DisplayLayout.Override.HeaderStyle = HeaderStyle.WindowsVista;

            // Row 스타일 설정 (border, font 색)
            this.ugList01.DisplayLayout.Override.RowAppearance = appearanceRow;
            this.ugList01.DisplayLayout.Override.DefaultRowHeight = 24;
            this.ugList01.DisplayLayout.Scrollbars = Scrollbars.Automatic;

            //컬럼2줄
            this.ugList01.DisplayLayout.Bands[0].ColHeaderLines = 2;

            //계 추가
            this.ugList01.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ugList01.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.Top;

            //이벤트
            this.ugList01.InitializeLayout += new InitializeLayoutEventHandler(ugList01_InitializeLayout);

            #endregion 실시간 공급량 그리드 설정

            #region 유수율 그리드 설정

            oUltraGridColumn = this.ugList02.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C1";
            oUltraGridColumn.Header.Caption = "년월";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "yyyy-MM";

            oUltraGridColumn = this.ugList02.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C2";
            oUltraGridColumn.Header.Caption = "공급량\n(㎥/월)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            oUltraGridColumn = this.ugList02.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C3";
            oUltraGridColumn.Header.Caption = "유수수량\n(㎥/월)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            oUltraGridColumn = this.ugList02.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C4";
            oUltraGridColumn.Header.Caption = "유수율\n(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N2";

            FormManager.SetGridStyle(ugList02);

            this.ugList02.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            this.ugList02.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;
            this.ugList02.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
            this.ugList02.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            //컬럼2줄
            this.ugList02.DisplayLayout.Bands[0].ColHeaderLines = 2;

            // Header 스타일 설정
            this.ugList02.DisplayLayout.Override.HeaderAppearance = appearanceHeader;
            this.ugList02.DisplayLayout.BorderStyle = UIElementBorderStyle.None;
            this.ugList02.UseFlatMode = DefaultableBoolean.False;
            this.ugList02.DisplayLayout.Override.HeaderStyle = HeaderStyle.WindowsVista;

            // Row 스타일 설정 (border, font 색)
            this.ugList02.DisplayLayout.Override.RowAppearance = appearanceRow;
            this.ugList02.DisplayLayout.Override.DefaultRowHeight = 20;
            this.ugList02.DisplayLayout.Scrollbars = Scrollbars.Automatic;

            //컬럼2줄
            this.ugList02.DisplayLayout.Bands[0].ColHeaderLines = 2;

            #endregion

            #region 수요예측 그리드 설정

            ///수요예측 그리드
            oUltraGridColumn = this.ugList03.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C1";
            oUltraGridColumn.Header.Caption = "계통";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            oUltraGridColumn = this.ugList03.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C2";
            oUltraGridColumn.Header.Caption = "수요예측\n(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            oUltraGridColumn = this.ugList03.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C3";
            oUltraGridColumn.Header.Caption = "금일누적\n(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            oUltraGridColumn = this.ugList03.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "C4";
            oUltraGridColumn.Header.Caption = "잔여공급\n(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            oUltraGridColumn.Format = "N0";

            FormManager.SetGridStyle(ugList03);

            this.ugList03.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            this.ugList03.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;
            this.ugList03.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
            this.ugList03.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            //컬럼2줄
            this.ugList03.DisplayLayout.Bands[0].ColHeaderLines = 2;

            // Header 스타일 설정
            this.ugList03.DisplayLayout.Override.HeaderAppearance = appearanceHeader;
            this.ugList03.DisplayLayout.BorderStyle = UIElementBorderStyle.None;
            this.ugList03.UseFlatMode = DefaultableBoolean.False;
            this.ugList03.DisplayLayout.Override.HeaderStyle = HeaderStyle.WindowsVista;

            // Row 스타일 설정 (border, font 색)
            this.ugList03.DisplayLayout.Override.RowAppearance = appearanceRow;
            this.ugList03.DisplayLayout.Override.DefaultRowHeight = 24;
            this.ugList03.DisplayLayout.Scrollbars = Scrollbars.Automatic;

            //컬럼2줄
            this.ugList03.DisplayLayout.Bands[0].ColHeaderLines = 2;

            //계 추가
            this.ugList03.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ugList03.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.Top;

            //이벤트
            this.ugList03.InitializeLayout += new InitializeLayoutEventHandler(ugList03_InitializeLayout);

            #endregion 수요예측 그리드 설정

            #region 누수감시 그리드 설정

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add("SGCCD");
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add("LOC_CODE");
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "LBLOCK";
            oUltraGridColumn.Header.Caption = "대블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MBLOCK";
            oUltraGridColumn.Header.Caption = "중블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SBLOCK";
            oUltraGridColumn.Header.Caption = "소블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add("LOC_NAME");
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add("FTR_CODE");
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add("FTR_IDN");
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add("DATEE");
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MONITOR_RESULT";
            oUltraGridColumn.Header.Caption = "감시결과";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = this.ugList04.DisplayLayout.Bands[0].Columns.Add("MONITOR_ALARM");
            oUltraGridColumn.Hidden = true;

            FormManager.SetGridStyle(ugList04);
            this.ugList04.Font = new Font("굴림", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 129);

            this.ugList04.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            this.ugList04.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;
            this.ugList04.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
            this.ugList04.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            // Header 스타일 설정
            this.ugList04.DisplayLayout.Override.HeaderAppearance = appearanceHeader;
            this.ugList04.DisplayLayout.BorderStyle = UIElementBorderStyle.None;
            this.ugList04.UseFlatMode = DefaultableBoolean.False;
            this.ugList04.DisplayLayout.Override.HeaderStyle = HeaderStyle.WindowsVista;

            // Row 스타일 설정 (border, font 색)
            this.ugList04.DisplayLayout.Override.RowAppearance = appearanceRow;
            this.ugList04.DisplayLayout.Override.DefaultRowHeight = 20;
            this.ugList04.DisplayLayout.Scrollbars = Scrollbars.Automatic;

            //컬럼2줄
            this.ugList04.DisplayLayout.Bands[0].ColHeaderLines = 2;

            #endregion 누수감시 그리드 설정

            this.ugList01.Dock = DockStyle.Fill;
            this.ugList02.Dock = DockStyle.Fill;
            this.ugList03.Dock = DockStyle.Fill;
            this.ugList04.Dock = DockStyle.Fill;
            this.chart01.Dock = DockStyle.Fill;
            this.chart02.Dock = DockStyle.Fill;
            this.chart03.Dock = DockStyle.Fill;
            this.chart01.Border = null;
            this.chart02.Border = null;
            this.chart03.Border = null;
        }

        /// <summary>
        /// 실시간 공급량 현황 그리드 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ugList01_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            this.ugList01.DisplayLayout.Bands[0].Summaries.Clear();

            SummarySettings summary = null;

            summary = this.ugList01.DisplayLayout.Bands[0].Summaries.Add("C1_SUM", SummaryType.Sum, this.ugList01.DisplayLayout.Bands[0].Columns["C1"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));
            summary.Lines = 2;

            summary = this.ugList01.DisplayLayout.Bands[0].Summaries.Add("C2_SUM", SummaryType.Sum, this.ugList01.DisplayLayout.Bands[0].Columns["C2"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            summary = this.ugList01.DisplayLayout.Bands[0].Summaries.Add("C3_SUM", SummaryType.Sum, this.ugList01.DisplayLayout.Bands[0].Columns["C3"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            summary = this.ugList01.DisplayLayout.Bands[0].Summaries.Add("C4_SUM", SummaryType.Sum, this.ugList01.DisplayLayout.Bands[0].Columns["C4"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            summary = this.ugList01.DisplayLayout.Bands[0].Summaries["C1_SUM"];
            summary.DisplayFormat = "계";
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
        }

        /// <summary>
        /// 수요예측 현황 그리드 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ugList03_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            this.ugList03.DisplayLayout.Bands[0].Summaries.Clear();

            SummarySettings summary = null;

            summary = this.ugList03.DisplayLayout.Bands[0].Summaries.Add("C1_SUM", SummaryType.Sum, this.ugList03.DisplayLayout.Bands[0].Columns["C1"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));
            summary.Lines = 2;

            summary = this.ugList03.DisplayLayout.Bands[0].Summaries.Add("C2_SUM", SummaryType.Sum, this.ugList03.DisplayLayout.Bands[0].Columns["C2"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            summary = this.ugList03.DisplayLayout.Bands[0].Summaries.Add("C3_SUM", SummaryType.Sum, this.ugList03.DisplayLayout.Bands[0].Columns["C3"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            summary = this.ugList03.DisplayLayout.Bands[0].Summaries.Add("C4_SUM", SummaryType.Sum, this.ugList03.DisplayLayout.Bands[0].Columns["C4"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,##0}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Appearance.FontData.SizeInPoints = 9;
            summary.Appearance.FontData.Name = "굴림";
            summary.Appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            summary = this.ugList03.DisplayLayout.Bands[0].Summaries["C1_SUM"];
            summary.DisplayFormat = "계";
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
        }

        /// <summary>
        /// 패널화면 표시 실행
        /// </summary>
        public void Open()
        {   
            this.SelectFlowStatsGrid();

            this.Initialize_LayerInfo();

            this.SetLeakageMonitering_Event();
            this.SetLeakageMonitering_Gis(DateTime.Now.AddDays(-1));
            this.SetLeakageMonitering_Grid(DateTime.Now.AddDays(-1));
            this.SetLeakageMonitering_Setting();

            pnlList01.BackgroundImage = Properties.Resources.List_ov01;
            pnlChart01.BackgroundImage = Properties.Resources.Chart_ov01;

            this.Show();

            panelListMouseClick(pnlList01, new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
            panelChartMouseClick(pnlChart01, new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
        }

        #region 누수감시결과 이벤트 관련

        /// <summary>
        /// 누수감시결과 이벤트 설정
        /// 1.지도와 그리드에서 마우스 우클릭 바로가게 메뉴
        /// 2.그리드 데이터 오류 표시등
        /// </summary>
        /// <param name="dateTime"></param>
        private void SetLeakageMonitering_Event()
        {
            this.axMap.OnMouseDown += new IMapControlEvents2_Ax_OnMouseDownEventHandler(axMap_OnMouseDown);
            this.ugList04.MouseDown += new MouseEventHandler(ultraGrid_WaterMonitor_MouseDown);

            this.ugList04.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid_WaterMonitor_InitializeLayout);
        }


        //
        //선택한 블럭의 FTR_IDN이 스캔되었을 경우만 바로가기 메뉴를 활성화한다.
        //
        private void ultraGrid_WaterMonitor_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.ugList04.DataSource == null || this.ugList04.Rows.Count == 0)
            {
                return;
            }

            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            ContextMenuStrip ContextMenu = new ContextMenuStrip();
            ContextMenu.Items.Add("바로가기", null, menuUtems1_Click);
            ContextMenu.Show(this.ugList04, new System.Drawing.Point(e.X, e.Y));
        }

        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (e.button != 2)
            {
                return;
            }

            ContextMenuStrip ContextMenu = new ContextMenuStrip();
            ContextMenu.Items.Add("바로가기", null, menuUtems1_Click);
            ContextMenu.Show(this.axMap, new System.Drawing.Point(e.x, e.y));
        }

        //누수감시결과를 호출한다.
        private void menuUtems1_Click(object sender, EventArgs e)
        {
            frmLMain oForm = this.Parent.Parent.Parent as frmLMain;
            oForm.CallLeakageMoniteringResult();
        }

        private void ultraGrid_WaterMonitor_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            bool isOrderChange = false;

            foreach (UltraGridRow row in e.Layout.Rows)
            {
                //누수아님
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "F")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("정상", false);

                    //체크박스 수정불가능하게,
                    row.Cells["MONITOR_ALARM"].Activation = Activation.Disabled;
                }

                //누수임
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "T")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("누수", false);
                    isOrderChange = true;
                }

                //확인필요
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "A")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("관심", false);
                    row.Cells["MONITOR_RESULT"].Appearance.ForeColor = Color.YellowGreen;
                    isOrderChange = false;
                }

                //데이터확인
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "N")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("데이터 확인", false);
                    isOrderChange = true;
                }
            }

            if (isOrderChange)
            {
                e.Layout.Bands[0].Columns["MONITOR_RESULT"].SortIndicator = SortIndicator.Ascending;
            }
        }

        private void OnTableCellValueChanging(object sender, DataColumnChangeEventArgs e)
        {
            this.ValidateDataRowCell(e.Row, e.Column, e.ProposedValue);
        }

        private void ValidateDataRowCell(DataRow row, DataColumn column, object value)
        {
            if (column.ColumnName == "MONITOR_RESULT")
            {
                row.SetColumnError(column, string.Empty);

                if (row[column.ColumnName].ToString() == "T")
                {
                    row.SetColumnError(column, "누수");
                }
                else if (row[column.ColumnName].ToString() == "N")
                {
                    row.SetColumnError(column, "데이터 확인");
                }
            }
        }

        #endregion 누수감시결과 이벤트 관련

        #region 누수감시결과 이벤트 관련

        private void SetLeakageMonitering_Setting()
        {
            this.InitializeValueList();
        }

        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ugList04.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ugList04.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
                this.ugList04.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ugList04.DisplayLayout.ValueLists["LBLOCK"];
            }

            if (!this.ugList04.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ugList04.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
                this.ugList04.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ugList04.DisplayLayout.ValueLists["MBLOCK"];
            }

            if (!this.ugList04.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ugList04.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
                this.ugList04.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ugList04.DisplayLayout.ValueLists["SBLOCK"];
            }
        }

        #endregion 누수감시결과 이벤트 관련

        #region 누수감시결과 그리드

        /// <summary>
        /// 누수감시결과 그리드표시
        /// </summary>
        /// <param name="dateTime"></param>
        private void SetLeakageMonitering_Grid(DateTime dateTime)
        {
            Hashtable parameter = new Hashtable();
            parameter["STARTDATE"] = dateTime.ToString("yyyyMMdd");
            parameter["ENDDATE"] = dateTime.ToString("yyyyMMdd");

            DataTable table = LeakageManageWork.GetInstance().SelectLeakageResult(parameter);
            table.ColumnChanging += new DataColumnChangeEventHandler(this.OnTableCellValueChanging);
            this.ugList04.DataSource = table;
        }

        #endregion 누수감시결과 그리드

        #region 누수감시결과 지도

        /// <summary>
        /// 누수감시결과 지도표시
        /// </summary>
        /// <param name="dateTIme"></param>
        private void SetLeakageMonitering_Gis(DateTime dateTIme)
        {
            Hashtable parameter = new Hashtable();
            parameter["STARTDATE"] = dateTIme.ToString("yyyyMMdd");

            DataTable dataTable = LeakageManageWork.GetInstance().SelectLeakageResult(parameter);

            if (dataTable == null || dataTable.Rows.Count == 0)
            {
                return;
            }

            ILayer middleBlock = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
            ILayer smallBlock = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");

            try
            {

                middleBlock.Visible = false;
                smallBlock.MinimumScale = 999999;
                smallBlock.MaximumScale = 1;

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    IColor pColor = null;

                    //데이터오류
                    //if (dataRow["MONITOR_RESULT"].ToString() == "N" && dataRow["MONITOR_ALARM"].ToString() == "True")
                    if (dataRow["MONITOR_RESULT"].ToString() == "N")
                    {
                        pColor = WaterAOCore.ArcManager.GetColor(255, 165, 0);
                    }
                    //누수
                    else if (dataRow["MONITOR_RESULT"].ToString() == "T" && dataRow["MONITOR_ALARM"].ToString() == "True")
                    {
                        pColor = WaterAOCore.ArcManager.GetColor(255, 0, 0);
                    }
                    //확인필요
                    else if (dataRow["MONITOR_RESULT"].ToString() == "A" && dataRow["MONITOR_ALARM"].ToString() == "True")
                    {
                        pColor = WaterAOCore.ArcManager.GetColor(238, 238, 0);
                    }
                    //정상
                    else
                    {
                        pColor = WaterAOCore.ArcManager.GetColor(30, 144, 255);
                    }

                    IColor pOutColor = WaterAOCore.ArcManager.GetColor(100, 100, 200);
                    ISymbol pLineSymbol = null;

                    if (((IGeoFeatureLayer)smallBlock).Renderer is IUniqueValueRenderer)
                    {
                        IUniqueValueRenderer pUniqueRenderer = ((IGeoFeatureLayer)smallBlock).Renderer as IUniqueValueRenderer;

                        pLineSymbol = WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 0.01);
                        ISymbol oSymbol = ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, (ILineSymbol)pLineSymbol, pColor);

                        //pUniqueRenderer.FieldCount = 1;
                        //pUniqueRenderer.set_Field(0, "BLK_NAM");
                        //pUniqueRenderer.DefaultSymbol = oSymbol;
                        //pUniqueRenderer.UseDefaultSymbol = false;

                        pUniqueRenderer.set_Symbol(dataRow["LOC_NAME"].ToString(), oSymbol);
                    }
                    else
                    {
                        IUniqueValueRenderer pUniqueRenderer = new UniqueValueRendererClass();


                        pLineSymbol = WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 0.01);
                        ISymbol oSymbol = ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, (ILineSymbol)pLineSymbol, pColor);

                        pUniqueRenderer.FieldCount = 1;
                        pUniqueRenderer.set_Field(0, "BLK_NAM");
                        pUniqueRenderer.DefaultSymbol = oSymbol;
                        pUniqueRenderer.UseDefaultSymbol = false;
                        //pUniqueRenderer.set_Symbol(dataRow["LOC_NAME"].ToString(), oSymbol);

                        ((IGeoFeatureLayer)smallBlock).Renderer = pUniqueRenderer as IFeatureRenderer;
                    }
                    IColor pLabelColor = WaterAOCore.ArcManager.GetColor(25, 0, 0);
                    WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)smallBlock, "BLK_NAM", "굴림", 8, false, pLabelColor, 0, 0, string.Empty);

                    this.axMap.Refresh();
                }
            }
            catch { }

        }

        #endregion 누수감시결과 지도

        /// <summary>
        /// 레이어 환경
        /// </summary>
        private void Initialize_LayerInfo()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT *");
            //oStringBuilder.AppendLine("      A.F_NAME AS F_NAME, A.F_SOURCE AS F_SOURCE, A.F_TYPE AS F_TYPE, ");
            //oStringBuilder.AppendLine("      A.F_ORDER F_ORDER, A.F_ALIAS AS F_ALIAS, ");
            //oStringBuilder.AppendLine("      A.F_VIEW AS F_VIEW, A.F_OBJECT AS F_OBJECT, ");
            //oStringBuilder.AppendLine("      A.F_CATEGORY AS F_CATEGORY ");
            oStringBuilder.AppendLine(" FROM SI_LAYER A ");
            oStringBuilder.AppendLine("WHERE A.F_ORDER IS NOT NULL");
            oStringBuilder.AppendLine(" ORDER BY A.F_ORDER DESC");

            OracleDBManager oDBManager = new OracleDBManager();

            try
            {
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();


                DataTable dtLayer = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                Viewmap_Load(dtLayer);

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
        }

        private DateTime GetServerDateTime()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT SYSDATE FROM DUAL ");
            OracleDBManager oDBManager = null;
            
            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                object o = oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null);

                return Convert.ToDateTime(o);
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmMainPanel", "SetFlowRealTIme", oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// Viewmap_Load - 지도화면에 레이어를 표시한다.
        /// </summary>
        private void Viewmap_Load(DataTable dtLayer)
        {
            if (dtLayer == null) return;

            ILayer pLayer = null;
            IWorkspace pWS = null;

            try
            {
                foreach (DataRow item in dtLayer.Rows)
                {
                    #region Workspace
                    if (item["F_CATEGORY"].ToString() == "지형") pWS = WaterAOCore.VariableManager.m_Topographic;
                    else if (item["F_CATEGORY"].ToString() == "상수") pWS = WaterAOCore.VariableManager.m_Pipegraphic;
                    else if (item["F_CATEGORY"].ToString() == "INP") pWS = WaterAOCore.VariableManager.m_INPgraphic;
                    else continue;

                    if (pWS == null) continue;
                    #endregion

                    pLayer = WaterAOCore.ArcManager.GetShapeLayer(pWS, item["F_NAME"].ToString(), item["F_ALIAS"].ToString());
                    if (pLayer == null) continue;

                    if ((new string[] {"대블록","중블록","소블록"}).Contains(pLayer.Name))
                    {
                        LoadRendererStream(pLayer, item);

                        axMap.AddLayer(pLayer);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// SI_LAYER에 저장된 PropertySet을 레이어에 적용한다.
        /// 렌더러, 라벨, activeScale 설정값
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pData"></param>
        private void LoadRendererStream(ILayer pLayer, DataRow row)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                if (!Convert.IsDBNull(row["F_RENDERE"]))
                {
                    byte[] pData = row["F_RENDERE"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;

                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IFeatureRenderer"));
                    if (o != null)
                        geoFeatureLayer.Renderer = o as IFeatureRenderer;
                }
                if (!Convert.IsDBNull(row["F_ANNOPROP"]))
                {
                    byte[] pData = row["F_ANNOPROP"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;
                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IAnnotateLayerPropertiesCollection"));
                    if (o != null)
                        geoFeatureLayer.AnnotationProperties = o as IAnnotateLayerPropertiesCollection;

                }
                if (!Convert.IsDBNull(row["F_MINSCALE"]))
                {
                    geoFeatureLayer.MinimumScale = Convert.ToDouble(row["F_MINSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_MAXSCALE"]))
                {
                    geoFeatureLayer.MaximumScale = Convert.ToDouble(row["F_MAXSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_FIELDDISP"]))
                {
                    geoFeatureLayer.DisplayField = Convert.ToString(row["F_FIELDDISP"]);
                }
                if (!Convert.IsDBNull(row["F_ANNODISP"]))
                {
                    geoFeatureLayer.DisplayAnnotation = Convert.ToBoolean(row["F_ANNODISP"]);
                }
                if (!Convert.IsDBNull(row["F_TIPDISP"]))
                {
                    geoFeatureLayer.ShowTips = Convert.ToBoolean(row["F_TIPDISP"]);
                }
            }
            catch { }
        }

        #region 실시간 공급량 데이터 설정 (그리드& 차트)

        /// <summary>
        /// 실시간 공급량 그리드 데이터 쿼리
        /// </summary>
        private void SelectFlowStatsGrid()
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("WITH LOC AS");
            query.AppendLine("(");
            query.AppendLine("SELECT A.LOC_NAME");
            query.AppendLine("      ,A.LOC_CODE");
            query.AppendLine("      ,A.ORDERBY");
            query.AppendLine("      ,ITG.TAGNAME");
            query.AppendLine("      ,ITG.TAG_GBN");
            query.AppendLine("  FROM");
            query.AppendLine("      (");
            query.AppendLine("      SELECT DECODE(A.KT_GBN, '001', A.LOC_NAME,");
            query.AppendLine("                   DECODE(A.KT_GBN, '002', '면지역',");
            query.AppendLine("                         DECODE(A.KT_GBN, '003', (SELECT LOC_NAME FROM CM_LOCATION WHERE LOC_CODE = A.PLOC_CODE)), A.LOC_NAME)) LOC_NAME");
            query.AppendLine("            ,DECODE(A.KT_GBN, '002', (SELECT LOC_CODE FROM CM_LOCATION WHERE PLOC_CODE = A.LOC_CODE), A.LOC_CODE) LOC_CODE");
            query.AppendLine("            ,A.LOC_NAME LOC_NAME2");
            query.AppendLine("            ,DECODE(A.KT_GBN, '003', 0, A.ORDERBY) ORDERBY");
            query.AppendLine("        FROM CM_LOCATION A");
            query.AppendLine("       WHERE (FTR_CODE = 'BZ002' OR (FTR_CODE = 'BZ003'	AND KT_GBN != '003'))");
            query.AppendLine("      ) A");
            query.AppendLine("      ,IF_IHTAGS IIH");
            query.AppendLine("      ,IF_TAG_GBN ITG");
            query.AppendLine(" WHERE IIH.LOC_CODE = A.LOC_CODE");
            query.AppendLine("   AND ITG.TAGNAME = IIH.TAGNAME");
            query.AppendLine("   AND ITG.TAG_GBN IN ('RT', 'TD', 'YD')");
            query.AppendLine(")");
            query.AppendLine("SELECT LOC_NAME C1");
            query.AppendLine("      ,SUPPLY_REAL C2");
            query.AppendLine("      ,SUPPLY_DAY C3");
            query.AppendLine("      ,SUPPLY_YESTERDAY C4");
            query.AppendLine(" FROM (");
            query.AppendLine("      SELECT LOC_NAME");
            query.AppendLine("            ,SUM(R.SUPPLY_REAL) SUPPLY_REAL");
            query.AppendLine("            ,SUM(D.SUPPLY_DAY) SUPPLY_DAY");
            query.AppendLine("            ,SUM(Y.SUPPLY_YESTERDAY) SUPPLY_YESTERDAY");
            query.AppendLine("            ,DECODE(NVL(LOC_NAME,'99999'),'99999', '99999', MAX(ORDERBY)) ORDERBY");
            query.AppendLine("        FROM");
            query.AppendLine("             LOC");
            query.AppendLine("            ,(");
            query.AppendLine("             SELECT LOC.LOC_CODE");
            query.AppendLine("                   ,IGR.TAGNAME");
            query.AppendLine("                   ,IGR.VALUE SUPPLY_REAL");
            query.AppendLine("               FROM LOC");
            query.AppendLine("                   ,IF_GATHER_REALTIME IGR");
            query.AppendLine("              WHERE IGR.TAGNAME = LOC.TAGNAME");
            query.AppendLine("                AND IGR.TAGNAME IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'RT')");
            query.AppendLine("                AND IGR.TIMESTAMP = (SELECT MAX(TIMESTAMP) FROM IF_GATHER_REALTIME)");
            query.AppendLine("             ) R");
            query.AppendLine("            ,(");
            query.AppendLine("             SELECT LOC.LOC_CODE");
            query.AppendLine("                   ,IAT.TAGNAME");
            query.AppendLine("                   ,IAT.VALUE SUPPLY_DAY");
            query.AppendLine("               FROM LOC");
            query.AppendLine("                   ,IF_ACCUMULATION_TODAY IAT");
            query.AppendLine("              WHERE IAT.TAGNAME = LOC.TAGNAME");
            query.AppendLine("                AND IAT.TAGNAME IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'TD')");
            query.AppendLine("                AND IAT.TIMESTAMP = (SELECT MAX(TIMESTAMP) FROM IF_ACCUMULATION_TODAY)");
            query.AppendLine("             ) D");
            query.AppendLine("            ,(");
            query.AppendLine("             SELECT LOC.LOC_CODE");
            query.AppendLine("                   ,IAT.TAGNAME");
            query.AppendLine("                   ,IAT.VALUE SUPPLY_YESTERDAY");
            query.AppendLine("               FROM LOC");
            query.AppendLine("                   ,IF_ACCUMULATION_YESTERDAY IAT");
            query.AppendLine("              WHERE IAT.TAGNAME = LOC.TAGNAME");
            query.AppendLine("                AND IAT.TAGNAME NOT IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'YD_R')");
            query.AppendLine("                AND IAT.TIMESTAMP = (SELECT MAX(TIMESTAMP) FROM IF_ACCUMULATION_YESTERDAY)");
            query.AppendLine("             ) Y");
            query.AppendLine("      WHERE R.LOC_CODE(+) = LOC.LOC_CODE");
            query.AppendLine("        AND R.TAGNAME(+) = LOC.TAGNAME");
            query.AppendLine("        AND D.LOC_CODE(+) = LOC.LOC_CODE");
            query.AppendLine("        AND D.TAGNAME(+) = LOC.TAGNAME");
            query.AppendLine("        AND Y.LOC_CODE(+) = LOC.LOC_CODE");
            query.AppendLine("        AND Y.TAGNAME(+) = LOC.TAGNAME");
            query.AppendLine("      GROUP BY");
            query.AppendLine("            LOC_NAME");
            query.AppendLine("      )");
            query.AppendLine(" ORDER BY");
            query.AppendLine("      TO_NUMBER(ORDERBY)");

            OracleDBManager oDBManager = null;
            DataSet dsSource = null;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                dsSource = oDBManager.ExecuteScriptDataSet(query.ToString(), null, "SUPPLY_RESULT");

            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmMainPanel", "SetFlowRealTIme", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }

            if (dsSource.Tables.Count > 0)
            {
                if (dsSource.Tables.Contains("SUPPLY_RESULT"))
                {
                    this.ugList01.DataSource = dsSource.Tables["SUPPLY_RESULT"];
                    this.ugList01.ActiveRow = null;
                }
            }
        }

        /// <summary>
        /// 유수율 차트 데이터 쿼리
        /// </summary>
        private void SelectFlowStatsChart()
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT YEAR_MON , SUBSTR(YEAR_MON,5,2) AS MONTHS");
            query.AppendLine("      ,ROUND(SUM(NVL(WATER_SUPPLIED,0)),2) AS SUPPLIED");
            query.AppendLine("      ,CASE WHEN SUM(NVL(WATER_SUPPLIED,0)) = 0 THEN 0");
            query.AppendLine("            WHEN SUM(NVL(REVENUE,0)) = 0 THEN 0");
            query.AppendLine("            ELSE ROUND((SUM(REVENUE)/SUM(WATER_SUPPLIED)) * 100,2)");
            query.AppendLine("        END AS RATIO");
            query.AppendLine(" FROM WV_REVENUE_RATIO RAT");
            query.AppendLine("     ,CM_LOCATION LOC");
            query.AppendLine("WHERE LOC.FTR_CODE = 'BZ001'");
            query.AppendLine("  AND LOC.LOC_CODE = RAT.LOC_CODE");
            query.AppendLine("  AND RAT.YEAR_MON BETWEEN TO_CHAR(ADD_MONTHS(SYSDATE,-12),'YYYYMM')");
            query.AppendLine("                       AND TO_CHAR(ADD_MONTHS(SYSDATE,-1), 'YYYYMM')");
            query.AppendLine("GROUP BY YEAR_MON");
            query.AppendLine("ORDER BY YEAR_MON ASC");

            OracleDBManager oDBManager = null;
            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(query.ToString(), null);

                if (oDataTable == null)
                {
                    return;
                }

                this.chart01.Series.Clear();
                this.chart01.AxesX.Clear();
                this.chart01.AxesY.Clear();
                this.chart01.DataSourceSettings.Fields.Clear();
                this.chart01.Data.Clear();

                //this.chart01.AxisY.Max = 1000000;
                //this.chart01.AxisY.Min = 1;
                //this.chart01.AxisY.Step = 20000;

                this.chart01.Data.Series = 2;
                this.chart01.Data.Points = oDataTable.Rows.Count;

                this.chart01.Series[0].Text = "공급량";
                this.chart01.Series[1].Text = "유수율";

                this.chart01.Series[0].Gallery = Gallery.Bar;
                this.chart01.Series[1].Gallery = Gallery.Curve;

                this.chart01.Series[0].FillMode = FillMode.Gradient;
                this.chart01.Series[0].Color = Color.FromArgb(255, 126, 44);
                this.chart01.Series[0].AlternateColor = Color.FromArgb(253, 196, 57);
                this.chart01.Series[0].Border.Visible = false;

                this.chart01.Series[1].Color = Color.FromArgb(95, 227, 16);
                this.chart01.Series[1].MarkerSize = 2;

                this.chart01.Series[0].AxisY = chart01.AxisY;
                this.chart01.Series[1].AxisY = chart01.AxisY2;

                this.chart01.Series[0].AxisY.Title.Text = "공급량(㎥)";
                this.chart01.Series[1].AxisY.Title.Text = "유수율(%)";

                double maxSupply = 0.0;
                double minSupply = 9999999.0;
                double maxRatio = 0.0;
                double minRatio = 999.0;

                foreach (DataRow row in oDataTable.Rows)
                {
                    if (Convert.ToDouble(row["SUPPLIED"]) > maxSupply)
                    {
                        maxSupply = Convert.ToDouble(row["SUPPLIED"]);
                    }
                    
                    if (Convert.ToDouble(row["RATIO"]) > maxRatio)
                    {
                        maxRatio = Convert.ToDouble(row["RATIO"]);
                    }
                    
                    if (Convert.ToDouble(row["SUPPLIED"]) < minSupply)
                    {
                        minSupply = Convert.ToDouble(row["SUPPLIED"]);
                    }
                    
                    if (Convert.ToDouble(row["RATIO"]) < minRatio)
                    {
                        minRatio = Convert.ToDouble(row["RATIO"]);
                    }

                    int idx = oDataTable.Rows.IndexOf(row);
                    this.chart01.AxisX.Labels[idx] = Convert.ToString(Convert.ToInt32(row["MONTHS"]));
                    this.chart01.Data[0, idx] = Convert.ToDouble(FormManager.StringInsertComma(Convert.ToString(row["SUPPLIED"])));
                    this.chart01.Data[1, idx] = Convert.ToDouble(row["RATIO"]);
                }

                this.chart01.Series[0].AxisY.Max = maxSupply * 1.03;
                this.chart01.Series[0].AxisY.Min = minSupply / 1.5;
                //this.chart01.Series[0].AxisY.Step = 100000;
                this.chart01.Series[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
                this.chart01.Series[0].AxisY.LabelsFormat.CustomFormat = "#,###,##0";

                this.chart01.Series[1].AxisY.Max = maxRatio * 1.03;
                this.chart01.Series[1].AxisY.Min = minRatio / 1.1;
                this.chart01.Series[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
                this.chart01.Series[1].AxisY.LabelsFormat.CustomFormat = "##0.#";

                this.chart01.Series[1].BringToFront();

                this.chart01.LegendBox.Visible = true;
                this.chart01.LegendBox.Dock = DockArea.Bottom;

                this.chart01.Update();
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmMainPanel", "SetFlowRealTIme", oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        #endregion 실시간 공급량 데이터 설정 (그리드& 차트)

        #region 유수율 데이터 설정 (그리드)

        /// <summary>
        /// 유수율 그리드 데이터 쿼리
        /// </summary>
        private void SelectWaterRatioGrid()
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" SELECT TO_DATE(YEAR_MON,'yyyymm') AS C1");
            query.AppendLine("	     ,ROUND(SUM(NVL(WATER_SUPPLIED,0)),2) AS C2");
            query.AppendLine("       ,ROUND(SUM(NVL(REVENUE,0)),2) C3");
            query.AppendLine("	     ,CASE WHEN SUM(NVL(WATER_SUPPLIED,0)) = 0 THEN 0");
            query.AppendLine("			       WHEN SUM(NVL(REVENUE,0)) = 0 THEN 0");
            query.AppendLine("            ELSE ROUND((SUM(REVENUE)/SUM(WATER_SUPPLIED)) * 100,2)");
            query.AppendLine("        END AS C4");
            query.AppendLine("  FROM WV_REVENUE_RATIO RAT");
            query.AppendLine("      ,CM_LOCATION LOC");
            query.AppendLine(" WHERE LOC.FTR_CODE = 'BZ001'");
            query.AppendLine("   AND LOC.LOC_CODE = RAT.LOC_CODE");
            query.AppendLine("   AND RAT.YEAR_MON BETWEEN TO_CHAR(ADD_MONTHS(SYSDATE,-12),'YYYYMM')");
            query.AppendLine("						AND TO_CHAR(ADD_MONTHS(SYSDATE,-1), 'YYYYMM')");
            query.AppendLine(" GROUP BY YEAR_MON");
            query.AppendLine(" ORDER BY YEAR_MON DESC");

            OracleDBManager oDBManager = null;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(query.ToString(), null);

                if (oDataTable != null)
                {
                    this.ugList02.DataSource = oDataTable;
                    this.ugList02.ActiveRow = null;
                }
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmMainPanel", "SetFlowRealTIme", oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        #endregion 유수율 데이터 설정 (그리드)

        #region 수요예측 데이터 설정 (그리드& 차트)

        /// <summary>
        /// 수요예측 그리드 데이터 쿼리
        /// </summary>
        private void SelectDemandForecastingGrid()
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("WITH LOC AS");
            query.AppendLine("(");
            query.AppendLine("SELECT A.LOC_NAME");
            query.AppendLine("      ,A.LOC_CODE");
            query.AppendLine("      ,A.ORDERBY");
            query.AppendLine("      ,ITG.TAGNAME");
            query.AppendLine("  FROM");
            query.AppendLine("      (");
            query.AppendLine("      SELECT DECODE(A.KT_GBN, '001', A.LOC_NAME,");
            query.AppendLine("                   DECODE(A.KT_GBN, '002', '면지역',");
            query.AppendLine("                         DECODE(A.KT_GBN, '003', (SELECT LOC_NAME FROM CM_LOCATION WHERE LOC_CODE = A.PLOC_CODE)), A.LOC_NAME)) LOC_NAME");
            query.AppendLine("            ,DECODE(A.KT_GBN, '002', (SELECT LOC_CODE FROM CM_LOCATION WHERE PLOC_CODE = A.LOC_CODE), A.LOC_CODE) LOC_CODE");
            query.AppendLine("            ,A.LOC_NAME LOC_NAME2");
            query.AppendLine("            ,DECODE(A.KT_GBN, '003', 0, A.ORDERBY) ORDERBY");
            query.AppendLine("        FROM CM_LOCATION A");
            query.AppendLine("       WHERE (FTR_CODE = 'BZ002' OR (FTR_CODE = 'BZ003' AND KT_GBN != '003'))");
            query.AppendLine("      ) A");
            query.AppendLine("      ,IF_IHTAGS IIH");
            query.AppendLine("      ,IF_TAG_GBN ITG");
            query.AppendLine(" WHERE IIH.LOC_CODE = A.LOC_CODE");
            query.AppendLine("   AND ITG.TAGNAME = IIH.TAGNAME");
            query.AppendLine("   AND ITG.TAG_GBN IN ('TD')");
            query.AppendLine("   AND ITG.TAGNAME NOT IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'YD_R')");
            query.AppendLine(")");
            query.AppendLine("SELECT LOC_NAME C1");
            query.AppendLine("      ,DF C2");
            query.AppendLine("      ,SUPPLY C3");
            query.AppendLine("      ,DF - SUPPLY C4");
            query.AppendLine(" FROM (");
            query.AppendLine("      SELECT LOC.LOC_NAME");
            query.AppendLine("            ,ROUND(SUM(D.VALUE)) DF");
            query.AppendLine("            ,ROUND(SUM(H.VALUE)) SUPPLY");
            query.AppendLine("            ,DECODE(NVL(LOC_NAME,'99999'),'99999', '99999', MAX(ORDERBY)) ORDERBY");
            query.AppendLine("        FROM");
            query.AppendLine("             LOC");
            query.AppendLine("            ,(");
            query.AppendLine("             SELECT A.LOC_CODE");
            query.AppendLine("                   ,SUM(A.DF_THFQ) VALUE");
            query.AppendLine("               FROM (SELECT LOC_CODE FROM LOC GROUP BY LOC_CODE) LOC");
            query.AppendLine("                   ,DF_RESULT_HOUR A");
            query.AppendLine("              WHERE A.LOC_CODE = LOC.LOC_CODE");
            query.AppendLine("                AND A.DF_RL_DT = TO_DATE(TO_CHAR(SYSDATE, 'YYYYMMDD'), 'YYYYMMDD')");
            query.AppendLine("              GROUP BY A.LOC_CODE, A.DF_RL_DT");
            query.AppendLine("             ) D");
            query.AppendLine("            ,(");
            query.AppendLine("             SELECT LOC.LOC_CODE");
            query.AppendLine("                   ,A.TAGNAME");
            query.AppendLine("                   ,A.VALUE");
            query.AppendLine("               FROM LOC");
            query.AppendLine("                   ,IF_ACCUMULATION_TODAY A");
            query.AppendLine("              WHERE A.TAGNAME = LOC.TAGNAME");
            query.AppendLine("                AND A.TIMESTAMP = (SELECT MAX(TIMESTAMP) FROM IF_ACCUMULATION_TODAY)");
            query.AppendLine("             ) H");
            query.AppendLine("      WHERE D.LOC_CODE(+) = LOC.LOC_CODE");
            query.AppendLine("        AND H.LOC_CODE(+) = LOC.LOC_CODE");
            query.AppendLine("        AND H.TAGNAME(+) = LOC.TAGNAME");
            query.AppendLine("      GROUP BY");
            query.AppendLine("            LOC_NAME");
            query.AppendLine("      )");
            query.AppendLine(" ORDER BY");
            query.AppendLine("      TO_NUMBER(ORDERBY)");
            
            OracleDBManager oDBManager = null;
            DataSet dsSource = null;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                dsSource = oDBManager.ExecuteScriptDataSet(query.ToString(), null, "RESULT");
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmMainPanel", "SetFlowRealTIme", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }

            if (dsSource.Tables.Count > 0)
            {
                if (dsSource.Tables.Contains("RESULT"))
                {
                    this.ugList03.DataSource = dsSource.Tables["RESULT"];
                    this.ugList03.ActiveRow = null;
                }
            }
        }

        /// <summary>
        /// 수요예측 차트 데이터 쿼리
        /// </summary>
        private void SelectDemandForecastingChart()
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("WITH LOC AS");
            query.AppendLine("(");
            query.AppendLine("SELECT A.LOC_NAME");
            query.AppendLine("      ,A.LOC_CODE");
            query.AppendLine("      ,ITG.TAGNAME");
            query.AppendLine("  FROM");
            query.AppendLine("      (");
            query.AppendLine("      SELECT LOC_NAME, LOC_CODE FROM CM_LOCATION WHERE FTR_CODE = 'BZ001'");
            query.AppendLine("      ) A");
            query.AppendLine("      ,IF_IHTAGS IIH");
            query.AppendLine("      ,IF_TAG_GBN ITG");
            query.AppendLine(" WHERE IIH.LOC_CODE = A.LOC_CODE");
            query.AppendLine("   AND ITG.TAGNAME = IIH.TAGNAME");
            query.AppendLine("   AND ITG.TAG_GBN IN ('FRQ')");
            query.AppendLine("   AND ITG.TAGNAME NOT IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'YD_R')");
            query.AppendLine(")");
            query.AppendLine("SELECT TEMP.TIMESTAMP TIME");
            query.AppendLine("      ,SUM(D.VALUE) DF");
            query.AppendLine("      ,SUM(H.VALUE) VALUE");
            query.AppendLine("  FROM (SELECT TO_CHAR(ROWNUM, '00') TIMESTAMP FROM DUAL CONNECT BY ROWNUM <= 24) TEMP");
            query.AppendLine("      ,(");
            query.AppendLine("       SELECT A.DF_THFQ VALUE");
            query.AppendLine("             ,TO_CHAR(A.DF_RL_HOUR+1, '00') TIMESTAMP");
            query.AppendLine("         FROM LOC LOC");
            query.AppendLine("             ,DF_RESULT_HOUR A");
            query.AppendLine("        WHERE A.LOC_CODE = LOC.LOC_CODE");
            query.AppendLine("          AND A.DF_RL_DT = TO_DATE(TO_CHAR(SYSDATE-1/24, 'YYYYMMDD'), 'YYYYMMDD')");
            query.AppendLine("        ) D");
            query.AppendLine("      ,(");
            query.AppendLine("       SELECT A.VALUE");
            query.AppendLine("             ,TO_CHAR(TO_CHAR(A.TIMESTAMP , 'HH24'), '00') TIMESTAMP");
            query.AppendLine("         FROM LOC");
            query.AppendLine("             ,IF_ACCUMULATION_HOUR A");
            query.AppendLine("        WHERE A.TAGNAME = LOC.TAGNAME");
            query.AppendLine("          AND A.TIMESTAMP BETWEEN TO_DATE(TO_CHAR(SYSDATE-1/24, 'YYYYMMDD')||'01', 'YYYYMMDDHH24')");
            query.AppendLine("                              AND TO_DATE(TO_CHAR(SYSDATE-1/24, 'YYYYMMDD'), 'YYYYMMDD') + 1");
            query.AppendLine("       ) H");
            query.AppendLine(" WHERE D.TIMESTAMP(+) = TEMP.TIMESTAMP");
            query.AppendLine("   AND H.TIMESTAMP(+) = TEMP.TIMESTAMP");
            query.AppendLine(" GROUP BY TEMP.TIMESTAMP");
            query.AppendLine(" ORDER BY TEMP.TIMESTAMP");

            OracleDBManager oDBManager = null;
            DataSet dsSource = null;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                dsSource = oDBManager.ExecuteScriptDataSet(query.ToString(), null, "RESULT");
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmMainPanel", "SetFlowRealTIme", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }

            if (dsSource.Tables.Count > 0)
            {
                if (dsSource.Tables.Contains("RESULT"))
                {
                    this.chart02.Series.Clear();
                    this.chart02.AxesX.Clear();
                    this.chart02.AxesY.Clear();
                    this.chart02.DataSourceSettings.Fields.Clear();
                    this.chart02.Data.Clear();

                    this.chart02.AxisY.ForceZero = false;
                    this.chart02.LegendBox.Visible = true;
                    this.chart02.LegendBox.Dock = DockArea.Bottom;

                    DataTable table = dsSource.Tables["RESULT"];

                    this.chart02.Data.Series = 2;
                    this.chart02.Data.Points = table.Rows.Count;

                    foreach (DataRow row in table.Rows)
                    {
                        this.chart02.AxisX.Labels[table.Rows.IndexOf(row)] = row["TIME"].ToString();

                        if (Utils.ToDouble(row["VALUE"]) != 0)
                        {
                            this.chart02.Data[0, table.Rows.IndexOf(row)] = Utils.ToDouble(row["VALUE"]);
                        }
                        if (Utils.ToDouble(row["DF"]) != 0)
                        {
                            this.chart02.Data[1, table.Rows.IndexOf(row)] = Utils.ToDouble(row["DF"]);
                        }
                    }

                    this.chart02.AxesY[0].LabelsFormat.Format = AxisFormat.Number;
                    this.chart02.AxesY[0].LabelsFormat.CustomFormat = "N0";

                    this.chart02.Series[0].Text = "실측량";
                    this.chart02.Series[0].MarkerSize = 1;

                    this.chart02.Series[1].Text = "예측량";
                    this.chart02.Series[1].MarkerSize = 1;

                    this.chart02.AxisY.Title.Text = "유량(㎥/시간)";

                    this.chart02.Series[0].Visible = true;
                    this.chart02.Series[1].Visible = true;
                }
            }
        }

        #endregion 수요예측 데이터 설정 (그리드& 차트)

        #region 민원 차트 설정

        /// <summary>
        /// 민원 통계 데이터 쿼리
        /// 민원챠트 표시
        /// </summary>
        private void SelectMinwonChart()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT SUBSTR(YMD.YYMM,5,2) AS MONTHS                                 ");
            oStringBuilder.AppendLine("      ,NVL(A.CNT1,0) AS CNT1                                          ");
            oStringBuilder.AppendLine("      ,NVL(A.CNT2,0) AS CNT2                                          ");
            oStringBuilder.AppendLine("FROM                                                                  ");
            oStringBuilder.AppendLine("      (SELECT TO_CHAR(CAAPPLDT, 'YYYYMM') AS YYMM  ");
            oStringBuilder.AppendLine("             , SUM(CASE WHEN CALRGCD IN ('2000') THEN 1 ELSE 0 END) AS CNT1 ");
            oStringBuilder.AppendLine("             , SUM(CASE WHEN CALRGCD IN ('1900') THEN 1 ELSE 0 END) AS CNT2 ");
            oStringBuilder.AppendLine("        FROM  WI_CAINFO                                                  ");
            oStringBuilder.AppendLine("        WHERE CAAPPLDT BETWEEN                                        ");
            oStringBuilder.AppendLine("              TO_CHAR(ADD_MONTHS(SYSDATE,-12),'YYYYMM') || '01'       ");
            oStringBuilder.AppendLine("          AND TO_CHAR(LAST_DAY(ADD_MONTHS(SYSDATE, 0)),'YYYYMMDD')    ");
            oStringBuilder.AppendLine("        GROUP BY TO_CHAR(CAAPPLDT, 'YYYYMM')) A                    ");
            oStringBuilder.AppendLine("      ,(SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -11),'YYYYMM') AS YYMM     ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -10),'YYYYMM') AS YYMM     ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -9),'YYYYMM') AS YYMM     ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -8),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -7),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -6),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -5),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -4),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -2),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -1),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL                                                     ");
            oStringBuilder.AppendLine("        UNION ALL                                                     ");
            oStringBuilder.AppendLine("        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, 0),'YYYYMM') AS YYMM      ");
            oStringBuilder.AppendLine("        FROM DUAL) YMD                                                ");
            oStringBuilder.AppendLine("WHERE A.YYMM(+) = YMD.YYMM                                            ");
            oStringBuilder.AppendLine("ORDER BY YMD.YYMM ASC                                                 ");

            OracleDBManager oDBManager = null;
            OracleDataReader oReader = null;
            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                this.chart03.LegendBox.Visible = true;
                this.chart03.LegendBox.Dock = DockArea.Bottom;

                this.chart03.Gallery = Gallery.Bar;
                this.chart03.AllSeries.Stacked = Stacked.Normal;
                ChartFX.WinForms.Galleries.Bar myBar = (ChartFX.WinForms.Galleries.Bar)this.chart03.GalleryAttributes;
                myBar.Overlap = false;

                ////민원통계 쿼리- 챠트에 그리기 추가
                this.chart03.Data.Series = 2;
                this.chart03.Data.Points = oDataTable.Rows.Count;

                this.chart03.Series[0].Text = "수질";
                this.chart03.Series[1].Text = "누수";

                this.chart03.Series[0].Gallery = Gallery.Bar;
                this.chart03.Series[0].Stacked = true;
                this.chart03.Series[0].Color = Color.FromArgb(91, 168, 218);
                this.chart03.Series[1].Gallery = Gallery.Bar;
                this.chart03.Series[1].Stacked = true;
                this.chart03.Series[1].Color = Color.FromArgb(255, 126, 44);

                this.chart03.Series[0].AxisY = this.chart03.AxisY;
                this.chart03.Series[1].AxisY = this.chart03.AxisY;

                foreach (DataRow row in oDataTable.Rows)
                {
                    int idx = oDataTable.Rows.IndexOf(row);
                    this.chart03.AxisX.Labels[idx] = Convert.ToString(Convert.ToInt32(row["MONTHS"]));
                    this.chart03.Data[0, idx] = Convert.ToDouble(row["CNT1"]);
                    this.chart03.Data[1, idx] = Convert.ToDouble(row["CNT2"]);
                }

                this.chart03.Update();
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.Water_Net", "frmMainPanel", "SetFlowRealTIme", oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oReader != null) oReader.Close();
                if (oDBManager != null) oDBManager.Close();
            }
        }

        #endregion 민원 차트 설정
        
        /// <summary>
        /// 주기적으로 실행 : time tick
        /// 주기적으로 실행되어야 할 작업
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.gridIndex == 1)
            {
                this.SelectFlowStatsGrid();
            }
            if (this.gridIndex == 2)
            {
                this.SelectDemandForecastingGrid();
            }

            if (DateTime.Now.Minute.CompareTo(0) == 0)
            {
                //누수감시 GIS결과표시
                this.SetLeakageMonitering_Gis(DateTime.Now.AddDays(-1));

                //누수감시 GRID결과 표시
                this.SetLeakageMonitering_Grid(DateTime.Now.AddDays(-1));
            }
        }

        /// <summary>
        /// 화면이 Visible = timer.start()
        /// 메인화면 frmMain화면의 크기를 재설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMainPanel_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                timer1.Start();
            }
            else
            {
                timer1.Stop();
            }

            if (this.Visible)
            {
                this.ParentForm.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.ParentForm.StartPosition = FormStartPosition.CenterScreen;
                this.ParentForm.WindowState = FormWindowState.Normal;
                this.ParentForm.Size = new System.Drawing.Size(1010, 861);
            }
            else
            {
                this.ParentForm.FormBorderStyle = FormBorderStyle.Sizable;
                this.ParentForm.StartPosition = FormStartPosition.WindowsDefaultLocation;
                this.ParentForm.WindowState = FormWindowState.Maximized;
            }
        }

        /// <summary>
        /// 화면의 크기가 변경될때 화면의 크기를 변수에 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMainPanel_SizeChanged(object sender, EventArgs e)
        {
            if (this.Parent != null)
	        {
                m_rect = this.Parent.Bounds;
	        }
        }

        /// <summary>
        /// 폼로드시 실행 : 상위패널의 Rect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMainPanel_Load(object sender, EventArgs e)
        {
            m_rect = this.Parent.Bounds;

            pnlManual.MouseHover += new EventHandler(panelMouseHover);
            pnlManual.MouseLeave += new EventHandler(panelMouseLeave);
            pnlMovie.MouseHover += new EventHandler(panelMouseHover);
            pnlMovie.MouseLeave += new EventHandler(panelMouseLeave);

            pnlList01.MouseHover += new EventHandler(panelMouseHover);
            pnlList01.MouseLeave += new EventHandler(panelMouseLeave);
            pnlList02.MouseHover += new EventHandler(panelMouseHover);
            pnlList02.MouseLeave += new EventHandler(panelMouseLeave);
            pnlList03.MouseHover += new EventHandler(panelMouseHover);
            pnlList03.MouseLeave += new EventHandler(panelMouseLeave);
            pnlList04.MouseHover += new EventHandler(panelMouseHover);
            pnlList04.MouseLeave += new EventHandler(panelMouseLeave);

            pnlChart01.MouseHover += new EventHandler(panelMouseHover);
            pnlChart01.MouseLeave += new EventHandler(panelMouseLeave);
            pnlChart02.MouseHover += new EventHandler(panelMouseHover);
            pnlChart02.MouseLeave += new EventHandler(panelMouseLeave);
            pnlChart03.MouseHover += new EventHandler(panelMouseHover);
            pnlChart03.MouseLeave += new EventHandler(panelMouseLeave);

            pnlList01.MouseClick += new MouseEventHandler(panelListMouseClick);
            pnlList02.MouseClick += new MouseEventHandler(panelListMouseClick);
            pnlList03.MouseClick += new MouseEventHandler(panelListMouseClick);
            pnlList04.MouseClick += new MouseEventHandler(panelListMouseClick);

            pnlChart01.MouseClick += new MouseEventHandler(panelChartMouseClick);
            pnlChart02.MouseClick += new MouseEventHandler(panelChartMouseClick);
            pnlChart03.MouseClick += new MouseEventHandler(panelChartMouseClick);

            pnlManual.MouseClick += new MouseEventHandler(panelLinkOpenMouseClick);
            pnlMovie.MouseClick += new MouseEventHandler(panelLinkOpenMouseClick);

            this.gridChange();
            this.chartChange();
        }

        /// <summary>
        /// 마우스가 패널에 들어올때, 백그라운드이미지 변경(선택)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelMouseHover(object sender, EventArgs e)
        {
            switch (((Panel)sender).Name)
            {
                case "pnlManual":
                    ((Panel)sender).BackgroundImage = Properties.Resources.Manual;
                    break;
                case "pnlMovie":
                    ((Panel)sender).BackgroundImage = Properties.Resources.Movie;
                    break;

                case "pnlList01":
                    ((Panel)sender).BackgroundImage = Properties.Resources.List_ov01;
                    break;
                case "pnlList02":
                    ((Panel)sender).BackgroundImage = Properties.Resources.List_ov02;
                    break;
                case "pnlList03":
                    ((Panel)sender).BackgroundImage = Properties.Resources.List_ov03;
                    break;
                case "pnlList04":
                    ((Panel)sender).BackgroundImage = Properties.Resources.List_ov04;
                    break;

                case "pnlChart01":
                    ((Panel)sender).BackgroundImage = Properties.Resources.Chart_ov01;
                    break;
                case "pnlChart02":
                    ((Panel)sender).BackgroundImage = Properties.Resources.Chart_ov02;
                    break;
                case "pnlChart03":
                    ((Panel)sender).BackgroundImage = Properties.Resources.Chart_ov03;
                    break;
            }
        }

        /// <summary>
        /// 마우스가 패널에서 나갈때, 백그라운드이미지 변경(해제)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelMouseLeave(object sender, EventArgs e)
        {
            if (((Panel)sender).Tag != null)
            {
                return;
            }

            ((Panel)sender).BackgroundImage = null;
        }

        /// <summary>
        /// 목록 표시 패널 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelListMouseClick(object sender, MouseEventArgs e)
        {
            pnlList01.Tag = null;
            pnlList02.Tag = null;
            pnlList03.Tag = null;
            pnlList04.Tag = null;

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            try
            {
                switch (((Panel)sender).Name)
                {
                    case "pnlList01":
                        this.ugList01.BringToFront();
                        lblList.Text = "공급량 현황";

                        if (this.gridIndex != 1)
                        {
                            this.gridIndex = 1;
                            this.gridChange();
                        }
                        break;

                    case "pnlList02":
                        this.ugList02.BringToFront();
                        lblList.Text = "유수율 현황";

                        if (this.gridIndex != 2)
                        {
                            this.gridIndex = 2;
                            this.gridChange();
                        }
                        break;

                    case "pnlList03":
                        this.ugList03.BringToFront();
                        lblList.Text = "수요예측 결과";

                        if (this.gridIndex != 3)
                        {
                            this.gridIndex = 3;
                            this.gridChange();
                        }
                        break;

                    case "pnlList04":
                        this.ugList04.BringToFront();
                        lblList.Text = "누수감시결과";

                        if (this.gridIndex != 4)
                        {
                            this.gridIndex = 4;
                            this.gridChange();
                        }
                        break;

                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }

            ((Panel)sender).Tag = "ON";

            if (!pnlList01.Name.Equals(((Panel)sender).Name)) pnlList01.BackgroundImage = null;
            if (!pnlList02.Name.Equals(((Panel)sender).Name)) pnlList02.BackgroundImage = null;
            if (!pnlList03.Name.Equals(((Panel)sender).Name)) pnlList03.BackgroundImage = null;
            if (!pnlList04.Name.Equals(((Panel)sender).Name)) pnlList04.BackgroundImage = null;
        }

        /// <summary>
        /// 차트 표시 패널 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelChartMouseClick(object sender, MouseEventArgs e)
        {
            pnlChart01.Tag = null; pnlChart02.Tag = null; pnlChart03.Tag = null;

            switch (((Panel)sender).Name)
            {
                case "pnlChart01":
                    this.chart01.BringToFront();
                    lblChart.Text = "유수율 현황";

                    if (this.chartIndex != 1)
                    {
                        this.chartIndex = 1;
                        this.chartChange();
                    }
                    break;

                case "pnlChart02":
                    this.chart02.BringToFront();
                    lblChart.Text = "수요예측 결과";

                    if (this.chartIndex != 2)
                    {
                        this.chartIndex = 2;
                        this.chartChange();
                    }
                    break;

                case "pnlChart03":
                    this.chart03.BringToFront();
                    lblChart.Text = "민원 현황";
                    if (this.chartIndex != 3)
                    {
                        this.chartIndex = 3;
                        this.chartChange();
                    }
                    break;
            }

            ((Panel)sender).Tag = "ON";

            if (!pnlChart01.Name.Equals(((Panel)sender).Name)) pnlChart01.BackgroundImage = null;
            if (!pnlChart02.Name.Equals(((Panel)sender).Name)) pnlChart02.BackgroundImage = null;
            if (!pnlChart03.Name.Equals(((Panel)sender).Name)) pnlChart03.BackgroundImage = null;
        }

        private void panelLinkOpenMouseClick(object sender, MouseEventArgs e)
        {
            switch (((Panel)sender).Name)
            {
                case "pnlManual":
                    RegistryKey SoftwareKey = Registry.LocalMachine.OpenSubKey("Software", false);
                    if (SoftwareKey == null)
                    {
                        WaterNetCore.MessageManager.ShowErrorMessage("한글 프로그램이 설치되어 있지않습니다.");
                        return;
                    }

                    SoftwareKey = SoftwareKey.OpenSubKey("HNC", false);
                    if (SoftwareKey == null)
                    {
                        WaterNetCore.MessageManager.ShowErrorMessage("한글 프로그램이 설치되어 있지않습니다.");
                        return;
                    }

                    SoftwareKey = SoftwareKey.OpenSubKey("Shared", false);
                    if (SoftwareKey == null)
                    {
                        WaterNetCore.MessageManager.ShowErrorMessage("한글 프로그램이 설치되어 있지않습니다.");
                        return;
                    }

                    string processpath = string.Empty; string processname = string.Empty;
                    try
                    {
                        foreach (string arrStrValueName in SoftwareKey.GetValueNames())
                        {
                            if (arrStrValueName.Contains("Hnc Path"))
                            {
                                processpath = SoftwareKey.GetValue(arrStrValueName) as string;
                                processname = arrStrValueName.Replace("Hnc Path", "Hwp");
                                break;
                            }
                        }

                        System.Diagnostics.ProcessStartInfo procHwp = new System.Diagnostics.ProcessStartInfo();
                        procHwp.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                        procHwp.CreateNoWindow = false;
                        procHwp.UseShellExecute = false;
                        procHwp.WorkingDirectory = Application.StartupPath;
                        procHwp.FileName = processpath + processname + "\\Hwp.EXE";

                        procHwp.Arguments = Application.StartupPath + "\\Manual\\water-NET매뉴얼.hwp";
                        System.Diagnostics.Process.Start(procHwp);
                    }
                    catch (Exception)
                    {
                        WaterNetCore.MessageManager.ShowErrorMessage("한글 프로그램을 실행할수 없습니다.");
                    }
                    break;
                case "pnlMovie":
                    if (!System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Windows Media Player"))
                    {
                        WaterNetCore.MessageManager.ShowErrorMessage(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Windows Media Player" + " 폴더가 없습니다.");
                        return;
                    }
                    if (!System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Windows Media Player\\wmplayer.exe"))
                    {
                        WaterNetCore.MessageManager.ShowErrorMessage("윈도우 미디어 플레이어가 설치되지 않았습니다.\n\r 윈도우 미디어 플레이어를 설치하십시오.");
                        return;
                    }

                    System.Diagnostics.ProcessStartInfo procMovie2 = new System.Diagnostics.ProcessStartInfo();
                    procMovie2.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                    procMovie2.CreateNoWindow = false;
                    procMovie2.UseShellExecute = false;
                    procMovie2.WorkingDirectory = Application.StartupPath;
                    procMovie2.FileName = (Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Windows Media Player\\wmplayer.exe");

                    try
                    {
                        procMovie2.Arguments = Application.StartupPath + "\\Manual\\홍보동영상.wmv";
                        System.Diagnostics.Process.Start(procMovie2);
                    }
                    catch (Exception)
                    {
                        WaterNetCore.MessageManager.ShowErrorMessage("wmplayer.exe" + "을 실행할수 없습니다.");
                    }

                    break;
            }
        }

        //그리드 선택 인덱스
        private int gridIndex = 1;

        //차트 선택 인덱스
        private int chartIndex = 1;

        private void gridChange()
        {
            if (this.gridIndex == 1)
            {
                this.SelectFlowStatsGrid();
            }
            else if (this.gridIndex == 2)
            {
                this.SelectWaterRatioGrid();
            }
            else if (this.gridIndex == 3)
            {
                this.SelectDemandForecastingGrid();
            }
            else if (this.gridIndex == 4)
            {
                this.SetLeakageMonitering_Grid(DateTime.Now.AddDays(-1));
            }
        }

        private void chartChange()
        {
            if (this.chartIndex == 1)
            {
                this.SelectFlowStatsChart();
            }
            else if (this.chartIndex == 2)
            {
                this.SelectDemandForecastingChart();
            }
            else if (this.chartIndex == 3)
            {
                this.SelectMinwonChart();
            }
        }

        /// <summary>
        /// 누수감시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlfuncLeakgeMonitoring_MouseClick(object sender, MouseEventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            ((frmLMain)this.ParentForm).누수감시결과ToolStripMenuItem_Click(sender, new EventArgs());
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 관망해석
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlfuncSimu_MouseClick(object sender, MouseEventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            ((frmLMain)this.ParentForm).실시간관망해석ToolStripMenuItem_Click(sender, new EventArgs());
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 유수율분석
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlfuncWaterRatio_MouseClick(object sender, MouseEventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            ((frmLMain)this.ParentForm).유수율현황ToolStripMenuItem_Click(sender, new EventArgs());
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 단수사고
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlfuncWaterCrisis_MouseClick(object sender, MouseEventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            ((frmLMain)this.ParentForm).MenuCrisisWaterBreak_Click(sender, new EventArgs());
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 수요예측
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlfuncDF_MouseClick(object sender, MouseEventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            ((frmLMain)this.ParentForm).수요예측결과조회ToolStripMenuItem_Click(sender, new EventArgs());
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }
    }
}
