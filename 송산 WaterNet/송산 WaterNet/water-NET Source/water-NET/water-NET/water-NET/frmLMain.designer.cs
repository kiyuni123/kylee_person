﻿namespace WaterNet.Water_Net
{
    partial class frmLMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLMain));
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.관망해석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNP파일관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.실시간관망해석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuBlockFlowInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.수량분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.누수감시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동누수감시결과ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.누수량산정결과조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.집중관리우선순위조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.공급량야간최소유량분석조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.야간최소유량산정방식별조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.블록별공급량수압트랜드조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율현황조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.월유수율분석보고서ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율상호분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.총괄수량수지분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율제고사업추진관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.계량기교체관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.누수지점관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.관개대체관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.단수작업관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.사업추진효과분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.블록별일반현황정보조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.블록별수량관리기본설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.스마트미터관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.감압밸브ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.감압밸브감시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수질ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수질모의ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.실시간감시관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.실시간감시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수질민원관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.잔류염소예측ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.관세척구간관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수질검사자료관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuEnergy = new System.Windows.Forms.ToolStripMenuItem();
            this.일별펌프성능모의분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.실시간펌프성능모의분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.펌프장전력량원단위분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.펌프특성곡선분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuEnergyState = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuPumpSchedule = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsOptimunConstruct = new System.Windows.Forms.ToolStripMenuItem();
            this.tsOptimunSpec = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.성능진단자료관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.사업장관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.펌프설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsEnergyBaseInfoSet = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuCrisis = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuCrisisWaterBreak = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuWaterTrouble = new System.Windows.Forms.ToolStripMenuItem();
            this.수요예측ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배수지운영시뮬레이션ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수요예측결과조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuManage = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuSupplyArea = new System.Windows.Forms.ToolStripMenuItem();
            this.트렌드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.리포트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.관망운영일보ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.시단위공급량ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.시단위공급량2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.일공급량ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.일야간최소유량ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.특이사항기록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.시단위공급량분기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.용수공급일보ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.공급량수정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuRealTimeData = new System.Windows.Forms.ToolStripMenuItem();
            this.실시간경고ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuContents = new System.Windows.Forms.ToolStripMenuItem();
            this.실시간경보확인ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.기본설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuTagLinkage = new System.Windows.Forms.ToolStripMenuItem();
            this.DBConnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UserManageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PasswordManageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ENV_TCPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VersionManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.waterNET코드관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.waterNET로컬버전초기화ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수용가핸드폰번호등록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuInfosImport = new System.Windows.Forms.ToolStripMenuItem();
            this.waterNET수질모델생성ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MapExtentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.진단관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuDrPipe = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuMapMaker = new System.Windows.Forms.ToolStripMenuItem();
            this.태그맵핑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.태그맵핑관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarXY = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbtnAllClose = new System.Windows.Forms.ToolStripButton();
            this.tsbtnClose = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton01 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.tsbtnTransfer = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.tsbtnEnergyPump = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonReport = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuSimulation = new System.Windows.Forms.ToolStripButton();
            this.tsbtnDrPipe = new System.Windows.Forms.ToolStripButton();
            this.tsbtnMapMaker = new System.Windows.Forms.ToolStripButton();
            this.spcContents = new System.Windows.Forms.SplitContainer();
            this.uTabContents = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.griWarningList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenu1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuthisClose = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuAllClose = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuNthisClose = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcContents)).BeginInit();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabContents)).BeginInit();
            this.uTabContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griWarningList)).BeginInit();
            this.contextMenu1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.관망해석ToolStripMenuItem,
            this.수량분석ToolStripMenuItem,
            this.감압밸브ToolStripMenuItem,
            this.수질ToolStripMenuItem,
            this.ToolStripMenuEnergy,
            this.ToolStripMenuCrisis,
            this.수요예측ToolStripMenuItem,
            this.트렌드ToolStripMenuItem,
            this.리포트ToolStripMenuItem,
            this.실시간경고ToolStripMenuItem,
            this.기본설정ToolStripMenuItem,
            this.진단관리ToolStripMenuItem,
            this.태그맵핑ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1006, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 관망해석ToolStripMenuItem
            // 
            this.관망해석ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNP파일관리ToolStripMenuItem,
            this.실시간관망해석ToolStripMenuItem,
            this.MenuBlockFlowInfo});
            this.관망해석ToolStripMenuItem.Name = "관망해석ToolStripMenuItem";
            this.관망해석ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.관망해석ToolStripMenuItem.Text = "관망해석";
            // 
            // iNP파일관리ToolStripMenuItem
            // 
            this.iNP파일관리ToolStripMenuItem.Name = "iNP파일관리ToolStripMenuItem";
            this.iNP파일관리ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.iNP파일관리ToolStripMenuItem.Text = "INP 파일관리";
            this.iNP파일관리ToolStripMenuItem.Click += new System.EventHandler(this.iNP파일관리ToolStripMenuItem_Click);
            // 
            // 실시간관망해석ToolStripMenuItem
            // 
            this.실시간관망해석ToolStripMenuItem.Name = "실시간관망해석ToolStripMenuItem";
            this.실시간관망해석ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.실시간관망해석ToolStripMenuItem.Text = "실시간관망해석";
            this.실시간관망해석ToolStripMenuItem.Click += new System.EventHandler(this.실시간관망해석ToolStripMenuItem_Click);
            // 
            // MenuBlockFlowInfo
            // 
            this.MenuBlockFlowInfo.Name = "MenuBlockFlowInfo";
            this.MenuBlockFlowInfo.Size = new System.Drawing.Size(158, 22);
            this.MenuBlockFlowInfo.Text = "블록유량 현황";
            this.MenuBlockFlowInfo.Click += new System.EventHandler(this.MenuBlockFlowInfo_Click);
            // 
            // 수량분석ToolStripMenuItem
            // 
            this.수량분석ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.누수감시ToolStripMenuItem,
            this.유수율분석ToolStripMenuItem,
            this.유수율제고사업추진관리ToolStripMenuItem,
            this.블록별일반현황정보조회ToolStripMenuItem,
            this.블록별수량관리기본설정ToolStripMenuItem,
            this.스마트미터관리ToolStripMenuItem});
            this.수량분석ToolStripMenuItem.Name = "수량분석ToolStripMenuItem";
            this.수량분석ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.수량분석ToolStripMenuItem.Text = "수량관리";
            // 
            // 누수감시ToolStripMenuItem
            // 
            this.누수감시ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.자동누수감시결과ToolStripMenuItem,
            this.누수량산정결과조회ToolStripMenuItem,
            this.집중관리우선순위조회ToolStripMenuItem,
            this.공급량야간최소유량분석조회ToolStripMenuItem,
            this.야간최소유량산정방식별조회ToolStripMenuItem});
            this.누수감시ToolStripMenuItem.Name = "누수감시ToolStripMenuItem";
            this.누수감시ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.누수감시ToolStripMenuItem.Text = "누수감시";
            // 
            // 자동누수감시결과ToolStripMenuItem
            // 
            this.자동누수감시결과ToolStripMenuItem.Name = "자동누수감시결과ToolStripMenuItem";
            this.자동누수감시결과ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.자동누수감시결과ToolStripMenuItem.Text = "자동 누수감시 결과";
            this.자동누수감시결과ToolStripMenuItem.Click += new System.EventHandler(this.누수감시결과ToolStripMenuItem_Click);
            // 
            // 누수량산정결과조회ToolStripMenuItem
            // 
            this.누수량산정결과조회ToolStripMenuItem.Name = "누수량산정결과조회ToolStripMenuItem";
            this.누수량산정결과조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.누수량산정결과조회ToolStripMenuItem.Text = "누수량 산정 결과 조회";
            this.누수량산정결과조회ToolStripMenuItem.Click += new System.EventHandler(this.누수량관리ToolStripMenuItem_Click);
            // 
            // 집중관리우선순위조회ToolStripMenuItem
            // 
            this.집중관리우선순위조회ToolStripMenuItem.Name = "집중관리우선순위조회ToolStripMenuItem";
            this.집중관리우선순위조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.집중관리우선순위조회ToolStripMenuItem.Text = "집중관리 블록 우선 순위 조회";
            this.집중관리우선순위조회ToolStripMenuItem.Click += new System.EventHandler(this.블록운영분석ToolStripMenuItem_Click);
            // 
            // 공급량야간최소유량분석조회ToolStripMenuItem
            // 
            this.공급량야간최소유량분석조회ToolStripMenuItem.Name = "공급량야간최소유량분석조회ToolStripMenuItem";
            this.공급량야간최소유량분석조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.공급량야간최소유량분석조회ToolStripMenuItem.Text = "공급량/ 야간최소유량 분석 조회";
            this.공급량야간최소유량분석조회ToolStripMenuItem.Click += new System.EventHandler(this.블록별공급량야간최소유량분석toolStripMenuItem_Click);
            // 
            // 야간최소유량산정방식별조회ToolStripMenuItem
            // 
            this.야간최소유량산정방식별조회ToolStripMenuItem.Name = "야간최소유량산정방식별조회ToolStripMenuItem";
            this.야간최소유량산정방식별조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.야간최소유량산정방식별조회ToolStripMenuItem.Text = "야간최소유량 산정 방식별 조회";
            this.야간최소유량산정방식별조회ToolStripMenuItem.Click += new System.EventHandler(this.야간최소유량필터링ToolStripMenuItem_Click);
            // 
            // 유수율분석ToolStripMenuItem
            // 
            this.유수율분석ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.블록별공급량수압트랜드조회ToolStripMenuItem,
            this.유수율현황조회ToolStripMenuItem,
            this.월유수율분석보고서ToolStripMenuItem,
            this.유수율상호분석ToolStripMenuItem,
            this.총괄수량수지분석ToolStripMenuItem,
            this.유수율ToolStripMenuItem});
            this.유수율분석ToolStripMenuItem.Name = "유수율분석ToolStripMenuItem";
            this.유수율분석ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.유수율분석ToolStripMenuItem.Text = "유수율 분석";
            // 
            // 블록별공급량수압트랜드조회ToolStripMenuItem
            // 
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Name = "블록별공급량수압트랜드조회ToolStripMenuItem";
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Text = "블록별 공급량/ 수압 트랜드 조회";
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Click += new System.EventHandler(this.유량패턴현황ToolStripMenuItem_Click);
            // 
            // 유수율현황조회ToolStripMenuItem
            // 
            this.유수율현황조회ToolStripMenuItem.Name = "유수율현황조회ToolStripMenuItem";
            this.유수율현황조회ToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.유수율현황조회ToolStripMenuItem.Text = "유수율 현황 조회";
            this.유수율현황조회ToolStripMenuItem.Click += new System.EventHandler(this.유수율현황ToolStripMenuItem_Click);
            // 
            // 월유수율분석보고서ToolStripMenuItem
            // 
            this.월유수율분석보고서ToolStripMenuItem.Name = "월유수율분석보고서ToolStripMenuItem";
            this.월유수율분석보고서ToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.월유수율분석보고서ToolStripMenuItem.Text = "월유수율 분석 보고서";
            this.월유수율분석보고서ToolStripMenuItem.Click += new System.EventHandler(this.월유수율분석보고서ToolStripMenuItem_Click);
            // 
            // 유수율상호분석ToolStripMenuItem
            // 
            this.유수율상호분석ToolStripMenuItem.Name = "유수율상호분석ToolStripMenuItem";
            this.유수율상호분석ToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.유수율상호분석ToolStripMenuItem.Text = "유수율 상호 분석";
            this.유수율상호분석ToolStripMenuItem.Click += new System.EventHandler(this.유수율상호분석ToolStripMenuItem_Click);
            // 
            // 총괄수량수지분석ToolStripMenuItem
            // 
            this.총괄수량수지분석ToolStripMenuItem.Name = "총괄수량수지분석ToolStripMenuItem";
            this.총괄수량수지분석ToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.총괄수량수지분석ToolStripMenuItem.Text = "총괄수량수지 분석";
            this.총괄수량수지분석ToolStripMenuItem.Click += new System.EventHandler(this.총괄수량수지분석ToolStripMenuItem_Click);
            // 
            // 유수율ToolStripMenuItem
            // 
            this.유수율ToolStripMenuItem.Name = "유수율ToolStripMenuItem";
            this.유수율ToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.유수율ToolStripMenuItem.Text = "월별 유수율관리";
            this.유수율ToolStripMenuItem.Click += new System.EventHandler(this.유수율ToolStripMenuItem_Click);
            // 
            // 유수율제고사업추진관리ToolStripMenuItem
            // 
            this.유수율제고사업추진관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.계량기교체관리ToolStripMenuItem,
            this.누수지점관리ToolStripMenuItem,
            this.관개대체관리ToolStripMenuItem,
            this.단수작업관리ToolStripMenuItem,
            this.사업추진효과분석ToolStripMenuItem});
            this.유수율제고사업추진관리ToolStripMenuItem.Name = "유수율제고사업추진관리ToolStripMenuItem";
            this.유수율제고사업추진관리ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.유수율제고사업추진관리ToolStripMenuItem.Text = "유수율제고 사업추진관리";
            // 
            // 계량기교체관리ToolStripMenuItem
            // 
            this.계량기교체관리ToolStripMenuItem.Name = "계량기교체관리ToolStripMenuItem";
            this.계량기교체관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.계량기교체관리ToolStripMenuItem.Text = "계량기 교체 관리";
            this.계량기교체관리ToolStripMenuItem.Click += new System.EventHandler(this.계량기교체관리ToolStripMenuItem_Click);
            // 
            // 누수지점관리ToolStripMenuItem
            // 
            this.누수지점관리ToolStripMenuItem.Name = "누수지점관리ToolStripMenuItem";
            this.누수지점관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.누수지점관리ToolStripMenuItem.Text = "누수지점 관리";
            this.누수지점관리ToolStripMenuItem.Click += new System.EventHandler(this.누수지점관리ToolStripMenuItem_Click);
            // 
            // 관개대체관리ToolStripMenuItem
            // 
            this.관개대체관리ToolStripMenuItem.Name = "관개대체관리ToolStripMenuItem";
            this.관개대체관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.관개대체관리ToolStripMenuItem.Text = "관개대체 관리";
            this.관개대체관리ToolStripMenuItem.Click += new System.EventHandler(this.관개대체관리ToolStripMenuItem_Click);
            // 
            // 단수작업관리ToolStripMenuItem
            // 
            this.단수작업관리ToolStripMenuItem.Name = "단수작업관리ToolStripMenuItem";
            this.단수작업관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.단수작업관리ToolStripMenuItem.Text = "단수작업 관리";
            this.단수작업관리ToolStripMenuItem.Click += new System.EventHandler(this.기타단수작업ToolStripMenuItem_Click);
            // 
            // 사업추진효과분석ToolStripMenuItem
            // 
            this.사업추진효과분석ToolStripMenuItem.Name = "사업추진효과분석ToolStripMenuItem";
            this.사업추진효과분석ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.사업추진효과분석ToolStripMenuItem.Text = "사업추진 효과 분석";
            this.사업추진효과분석ToolStripMenuItem.Click += new System.EventHandler(this.사업추진효과분석ToolStripMenuItem_Click);
            // 
            // 블록별일반현황정보조회ToolStripMenuItem
            // 
            this.블록별일반현황정보조회ToolStripMenuItem.Name = "블록별일반현황정보조회ToolStripMenuItem";
            this.블록별일반현황정보조회ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.블록별일반현황정보조회ToolStripMenuItem.Text = "블록별 일반현황 정보 조회";
            this.블록별일반현황정보조회ToolStripMenuItem.Click += new System.EventHandler(this.일반현황관리ToolStripMenuItem_Click);
            // 
            // 블록별수량관리기본설정ToolStripMenuItem
            // 
            this.블록별수량관리기본설정ToolStripMenuItem.Name = "블록별수량관리기본설정ToolStripMenuItem";
            this.블록별수량관리기본설정ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.블록별수량관리기본설정ToolStripMenuItem.Text = "블록별 수량관리 기본 설정";
            this.블록별수량관리기본설정ToolStripMenuItem.Click += new System.EventHandler(this.블록기본설정ToolStripMenuItem_Click);
            // 
            // 스마트미터관리ToolStripMenuItem
            // 
            this.스마트미터관리ToolStripMenuItem.Name = "스마트미터관리ToolStripMenuItem";
            this.스마트미터관리ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.스마트미터관리ToolStripMenuItem.Text = "스마트미터관리";
            this.스마트미터관리ToolStripMenuItem.Click += new System.EventHandler(this.스마트미터관리ToolStripMenuItem_Click);
            // 
            // 감압밸브ToolStripMenuItem
            // 
            this.감압밸브ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.감압밸브감시ToolStripMenuItem});
            this.감압밸브ToolStripMenuItem.Name = "감압밸브ToolStripMenuItem";
            this.감압밸브ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.감압밸브ToolStripMenuItem.Text = "수압감시";
            // 
            // 감압밸브감시ToolStripMenuItem
            // 
            this.감압밸브감시ToolStripMenuItem.Name = "감압밸브감시ToolStripMenuItem";
            this.감압밸브감시ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.감압밸브감시ToolStripMenuItem.Text = "감압밸브 감시";
            this.감압밸브감시ToolStripMenuItem.Click += new System.EventHandler(this.감압밸브감시ToolStripMenuItem_Click);
            // 
            // 수질ToolStripMenuItem
            // 
            this.수질ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.수질모의ToolStripMenuItem,
            this.실시간감시관리ToolStripMenuItem,
            this.실시간감시ToolStripMenuItem,
            this.수질민원관리ToolStripMenuItem,
            this.잔류염소예측ToolStripMenuItem,
            this.관세척구간관리ToolStripMenuItem,
            this.수질검사자료관리ToolStripMenuItem});
            this.수질ToolStripMenuItem.Name = "수질ToolStripMenuItem";
            this.수질ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.수질ToolStripMenuItem.Text = "수질관리";
            // 
            // 수질모의ToolStripMenuItem
            // 
            this.수질모의ToolStripMenuItem.Name = "수질모의ToolStripMenuItem";
            this.수질모의ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.수질모의ToolStripMenuItem.Text = "수질모의";
            this.수질모의ToolStripMenuItem.Click += new System.EventHandler(this.수질모의ToolStripMenuItem_Click);
            // 
            // 실시간감시관리ToolStripMenuItem
            // 
            this.실시간감시관리ToolStripMenuItem.Name = "실시간감시관리ToolStripMenuItem";
            this.실시간감시관리ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.실시간감시관리ToolStripMenuItem.Text = "수질 감시 지점 선정";
            this.실시간감시관리ToolStripMenuItem.Click += new System.EventHandler(this.실시간감시관리ToolStripMenuItem_Click);
            // 
            // 실시간감시ToolStripMenuItem
            // 
            this.실시간감시ToolStripMenuItem.Name = "실시간감시ToolStripMenuItem";
            this.실시간감시ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.실시간감시ToolStripMenuItem.Text = "실시간 수질 감시";
            this.실시간감시ToolStripMenuItem.Click += new System.EventHandler(this.실시간감시ToolStripMenuItem_Click);
            // 
            // 수질민원관리ToolStripMenuItem
            // 
            this.수질민원관리ToolStripMenuItem.Name = "수질민원관리ToolStripMenuItem";
            this.수질민원관리ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.수질민원관리ToolStripMenuItem.Text = "수질 민원 관리";
            this.수질민원관리ToolStripMenuItem.Click += new System.EventHandler(this.수질민원관리ToolStripMenuItem_Click);
            // 
            // 잔류염소예측ToolStripMenuItem
            // 
            this.잔류염소예측ToolStripMenuItem.Name = "잔류염소예측ToolStripMenuItem";
            this.잔류염소예측ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.잔류염소예측ToolStripMenuItem.Text = "잔류염소 예측 및 지점 관리";
            this.잔류염소예측ToolStripMenuItem.Click += new System.EventHandler(this.잔류염소예측ToolStripMenuItem_Click);
            // 
            // 관세척구간관리ToolStripMenuItem
            // 
            this.관세척구간관리ToolStripMenuItem.Name = "관세척구간관리ToolStripMenuItem";
            this.관세척구간관리ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.관세척구간관리ToolStripMenuItem.Text = "관세척 구간 예측 및 지점 관리";
            this.관세척구간관리ToolStripMenuItem.Click += new System.EventHandler(this.관세척구간관리ToolStripMenuItem_Click);
            // 
            // 수질검사자료관리ToolStripMenuItem
            // 
            this.수질검사자료관리ToolStripMenuItem.Name = "수질검사자료관리ToolStripMenuItem";
            this.수질검사자료관리ToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.수질검사자료관리ToolStripMenuItem.Text = "수질 검사 자료 관리";
            this.수질검사자료관리ToolStripMenuItem.Click += new System.EventHandler(this.수질검사자료관리ToolStripMenuItem_Click);
            // 
            // ToolStripMenuEnergy
            // 
            this.ToolStripMenuEnergy.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.일별펌프성능모의분석ToolStripMenuItem,
            this.실시간펌프성능모의분석ToolStripMenuItem,
            this.펌프장전력량원단위분석ToolStripMenuItem,
            this.펌프특성곡선분석ToolStripMenuItem,
            this.ToolStripMenuEnergyState,
            this.ToolStripMenuPumpSchedule,
            this.toolStripMenuItem1,
            this.tsOptimunConstruct,
            this.tsOptimunSpec,
            this.toolStripMenuItem2,
            this.성능진단자료관리ToolStripMenuItem,
            this.사업장관리ToolStripMenuItem,
            this.펌프설정ToolStripMenuItem,
            this.tsEnergyBaseInfoSet});
            this.ToolStripMenuEnergy.Name = "ToolStripMenuEnergy";
            this.ToolStripMenuEnergy.Size = new System.Drawing.Size(79, 20);
            this.ToolStripMenuEnergy.Text = "에너지관리";
            // 
            // 일별펌프성능모의분석ToolStripMenuItem
            // 
            this.일별펌프성능모의분석ToolStripMenuItem.Name = "일별펌프성능모의분석ToolStripMenuItem";
            this.일별펌프성능모의분석ToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.일별펌프성능모의분석ToolStripMenuItem.Text = "일별펌프성능모의분석";
            this.일별펌프성능모의분석ToolStripMenuItem.Click += new System.EventHandler(this.일별펌프성능모의분석ToolStripMenuItem_Click);
            // 
            // 실시간펌프성능모의분석ToolStripMenuItem
            // 
            this.실시간펌프성능모의분석ToolStripMenuItem.Name = "실시간펌프성능모의분석ToolStripMenuItem";
            this.실시간펌프성능모의분석ToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.실시간펌프성능모의분석ToolStripMenuItem.Text = "실시간펌프성능모의분석";
            this.실시간펌프성능모의분석ToolStripMenuItem.Click += new System.EventHandler(this.실시간펌프성능모의분석ToolStripMenuItem_Click);
            // 
            // 펌프장전력량원단위분석ToolStripMenuItem
            // 
            this.펌프장전력량원단위분석ToolStripMenuItem.Name = "펌프장전력량원단위분석ToolStripMenuItem";
            this.펌프장전력량원단위분석ToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.펌프장전력량원단위분석ToolStripMenuItem.Text = "펌프장전력량/원단위분석";
            this.펌프장전력량원단위분석ToolStripMenuItem.Click += new System.EventHandler(this.펌프장전력량원단위분석ToolStripMenuItem_Click);
            // 
            // 펌프특성곡선분석ToolStripMenuItem
            // 
            this.펌프특성곡선분석ToolStripMenuItem.Name = "펌프특성곡선분석ToolStripMenuItem";
            this.펌프특성곡선분석ToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.펌프특성곡선분석ToolStripMenuItem.Text = "펌프특성곡선분석";
            this.펌프특성곡선분석ToolStripMenuItem.Click += new System.EventHandler(this.펌프특성곡선분석ToolStripMenuItem_Click);
            // 
            // ToolStripMenuEnergyState
            // 
            this.ToolStripMenuEnergyState.Name = "ToolStripMenuEnergyState";
            this.ToolStripMenuEnergyState.Size = new System.Drawing.Size(211, 22);
            this.ToolStripMenuEnergyState.Text = "펌프 에너지 현황";
            this.ToolStripMenuEnergyState.Click += new System.EventHandler(this.ToolStripMenuEnergyState_Click);
            // 
            // ToolStripMenuPumpSchedule
            // 
            this.ToolStripMenuPumpSchedule.Name = "ToolStripMenuPumpSchedule";
            this.ToolStripMenuPumpSchedule.Size = new System.Drawing.Size(211, 22);
            this.ToolStripMenuPumpSchedule.Text = "에너지 모의";
            this.ToolStripMenuPumpSchedule.Click += new System.EventHandler(this.ToolStripMenuPumpSchedule_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(208, 6);
            // 
            // tsOptimunConstruct
            // 
            this.tsOptimunConstruct.Name = "tsOptimunConstruct";
            this.tsOptimunConstruct.Size = new System.Drawing.Size(211, 22);
            this.tsOptimunConstruct.Text = "최적조합분석";
            this.tsOptimunConstruct.Click += new System.EventHandler(this.tsOptimunConstruct_Click);
            // 
            // tsOptimunSpec
            // 
            this.tsOptimunSpec.Name = "tsOptimunSpec";
            this.tsOptimunSpec.Size = new System.Drawing.Size(211, 22);
            this.tsOptimunSpec.Text = "최적규격분석";
            this.tsOptimunSpec.Click += new System.EventHandler(this.tsOptimunSpec_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(208, 6);
            // 
            // 성능진단자료관리ToolStripMenuItem
            // 
            this.성능진단자료관리ToolStripMenuItem.Name = "성능진단자료관리ToolStripMenuItem";
            this.성능진단자료관리ToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.성능진단자료관리ToolStripMenuItem.Text = "성능진단자료관리";
            this.성능진단자료관리ToolStripMenuItem.Click += new System.EventHandler(this.성능진단자료관리ToolStripMenuItem_Click);
            // 
            // 사업장관리ToolStripMenuItem
            // 
            this.사업장관리ToolStripMenuItem.Name = "사업장관리ToolStripMenuItem";
            this.사업장관리ToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.사업장관리ToolStripMenuItem.Text = "사업장관리";
            this.사업장관리ToolStripMenuItem.Click += new System.EventHandler(this.사업장관리ToolStripMenuItem_Click);
            // 
            // 펌프설정ToolStripMenuItem
            // 
            this.펌프설정ToolStripMenuItem.Name = "펌프설정ToolStripMenuItem";
            this.펌프설정ToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.펌프설정ToolStripMenuItem.Text = "펌프설정";
            this.펌프설정ToolStripMenuItem.Click += new System.EventHandler(this.펌프설정ToolStripMenuItem_Click);
            // 
            // tsEnergyBaseInfoSet
            // 
            this.tsEnergyBaseInfoSet.Name = "tsEnergyBaseInfoSet";
            this.tsEnergyBaseInfoSet.Size = new System.Drawing.Size(211, 22);
            this.tsEnergyBaseInfoSet.Text = "펌프/감시지점 관리";
            this.tsEnergyBaseInfoSet.Click += new System.EventHandler(this.tsEnergyBaseInfoSet_Click);
            // 
            // ToolStripMenuCrisis
            // 
            this.ToolStripMenuCrisis.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuCrisisWaterBreak,
            this.ToolStripMenuWaterTrouble});
            this.ToolStripMenuCrisis.Name = "ToolStripMenuCrisis";
            this.ToolStripMenuCrisis.Size = new System.Drawing.Size(67, 20);
            this.ToolStripMenuCrisis.Text = "위기관리";
            // 
            // MenuCrisisWaterBreak
            // 
            this.MenuCrisisWaterBreak.Name = "MenuCrisisWaterBreak";
            this.MenuCrisisWaterBreak.Size = new System.Drawing.Size(122, 22);
            this.MenuCrisisWaterBreak.Text = "단수사고";
            this.MenuCrisisWaterBreak.Click += new System.EventHandler(this.MenuCrisisWaterBreak_Click);
            // 
            // ToolStripMenuWaterTrouble
            // 
            this.ToolStripMenuWaterTrouble.Name = "ToolStripMenuWaterTrouble";
            this.ToolStripMenuWaterTrouble.Size = new System.Drawing.Size(122, 22);
            this.ToolStripMenuWaterTrouble.Text = "수질사고";
            this.ToolStripMenuWaterTrouble.Click += new System.EventHandler(this.ToolStripMenuWaterTrouble_Click);
            // 
            // 수요예측ToolStripMenuItem
            // 
            this.수요예측ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.배수지운영시뮬레이션ToolStripMenuItem,
            this.수요예측결과조회ToolStripMenuItem,
            this.ToolStripMenuManage,
            this.ToolStripMenuSupplyArea});
            this.수요예측ToolStripMenuItem.Name = "수요예측ToolStripMenuItem";
            this.수요예측ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.수요예측ToolStripMenuItem.Text = "수요예측";
            // 
            // 배수지운영시뮬레이션ToolStripMenuItem
            // 
            this.배수지운영시뮬레이션ToolStripMenuItem.Name = "배수지운영시뮬레이션ToolStripMenuItem";
            this.배수지운영시뮬레이션ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.배수지운영시뮬레이션ToolStripMenuItem.Text = "배수지운영시뮬레이션";
            this.배수지운영시뮬레이션ToolStripMenuItem.Click += new System.EventHandler(this.배수지운영시뮬레이션ToolStripMenuItem_Click);
            // 
            // 수요예측결과조회ToolStripMenuItem
            // 
            this.수요예측결과조회ToolStripMenuItem.Name = "수요예측결과조회ToolStripMenuItem";
            this.수요예측결과조회ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.수요예측결과조회ToolStripMenuItem.Text = "수요예측결과조회";
            this.수요예측결과조회ToolStripMenuItem.Click += new System.EventHandler(this.수요예측결과조회ToolStripMenuItem_Click);
            // 
            // ToolStripMenuManage
            // 
            this.ToolStripMenuManage.Name = "ToolStripMenuManage";
            this.ToolStripMenuManage.Size = new System.Drawing.Size(194, 22);
            this.ToolStripMenuManage.Text = "수요예측관리";
            this.ToolStripMenuManage.Click += new System.EventHandler(this.ToolStripMenuManage_Click);
            // 
            // ToolStripMenuSupplyArea
            // 
            this.ToolStripMenuSupplyArea.Name = "ToolStripMenuSupplyArea";
            this.ToolStripMenuSupplyArea.Size = new System.Drawing.Size(194, 22);
            this.ToolStripMenuSupplyArea.Text = "배수지관리";
            this.ToolStripMenuSupplyArea.Click += new System.EventHandler(this.ToolStripMenuSupplyArea_Click);
            // 
            // 트렌드ToolStripMenuItem
            // 
            this.트렌드ToolStripMenuItem.Name = "트렌드ToolStripMenuItem";
            this.트렌드ToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.트렌드ToolStripMenuItem.Text = "트렌드분석";
            this.트렌드ToolStripMenuItem.Click += new System.EventHandler(this.트렌드ToolStripMenuItem_Click);
            // 
            // 리포트ToolStripMenuItem
            // 
            this.리포트ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.관망운영일보ToolStripMenuItem,
            this.시단위공급량ToolStripMenuItem,
            this.시단위공급량2ToolStripMenuItem,
            this.일공급량ToolStripMenuItem,
            this.일야간최소유량ToolStripMenuItem,
            this.특이사항기록ToolStripMenuItem,
            this.시단위공급량분기ToolStripMenuItem,
            this.용수공급일보ToolStripMenuItem,
            this.공급량수정ToolStripMenuItem,
            this.ToolStripMenuRealTimeData});
            this.리포트ToolStripMenuItem.Name = "리포트ToolStripMenuItem";
            this.리포트ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.리포트ToolStripMenuItem.Text = "운영일보";
            // 
            // 관망운영일보ToolStripMenuItem
            // 
            this.관망운영일보ToolStripMenuItem.Name = "관망운영일보ToolStripMenuItem";
            this.관망운영일보ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.관망운영일보ToolStripMenuItem.Text = "관망운영일보";
            this.관망운영일보ToolStripMenuItem.Click += new System.EventHandler(this.관망운영일보ToolStripMenuItem_Click);
            // 
            // 시단위공급량ToolStripMenuItem
            // 
            this.시단위공급량ToolStripMenuItem.Name = "시단위공급량ToolStripMenuItem";
            this.시단위공급량ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.시단위공급량ToolStripMenuItem.Text = "시간별공급량";
            this.시단위공급량ToolStripMenuItem.Click += new System.EventHandler(this.시단위공급량ToolStripMenuItem_Click);
            // 
            // 시단위공급량2ToolStripMenuItem
            // 
            this.시단위공급량2ToolStripMenuItem.Name = "시단위공급량2ToolStripMenuItem";
            this.시단위공급량2ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.시단위공급량2ToolStripMenuItem.Text = "시단위공급량2";
            this.시단위공급량2ToolStripMenuItem.Click += new System.EventHandler(this.시단위공급량2ToolStripMenuItem_Click);
            // 
            // 일공급량ToolStripMenuItem
            // 
            this.일공급량ToolStripMenuItem.Name = "일공급량ToolStripMenuItem";
            this.일공급량ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.일공급량ToolStripMenuItem.Text = "일별공급량";
            this.일공급량ToolStripMenuItem.Click += new System.EventHandler(this.일공급량ToolStripMenuItem_Click);
            // 
            // 일야간최소유량ToolStripMenuItem
            // 
            this.일야간최소유량ToolStripMenuItem.Name = "일야간최소유량ToolStripMenuItem";
            this.일야간최소유량ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.일야간최소유량ToolStripMenuItem.Text = "야간최소유량";
            this.일야간최소유량ToolStripMenuItem.Click += new System.EventHandler(this.일야간최소유량ToolStripMenuItem_Click);
            // 
            // 특이사항기록ToolStripMenuItem
            // 
            this.특이사항기록ToolStripMenuItem.Name = "특이사항기록ToolStripMenuItem";
            this.특이사항기록ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.특이사항기록ToolStripMenuItem.Text = "특이사항기록";
            this.특이사항기록ToolStripMenuItem.Click += new System.EventHandler(this.특이사항기록ToolStripMenuItem_Click);
            // 
            // 시단위공급량분기ToolStripMenuItem
            // 
            this.시단위공급량분기ToolStripMenuItem.Name = "시단위공급량분기ToolStripMenuItem";
            this.시단위공급량분기ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.시단위공급량분기ToolStripMenuItem.Text = "시간별공급량(분기)";
            this.시단위공급량분기ToolStripMenuItem.Click += new System.EventHandler(this.시단위공급량분기ToolStripMenuItem_Click);
            // 
            // 용수공급일보ToolStripMenuItem
            // 
            this.용수공급일보ToolStripMenuItem.Name = "용수공급일보ToolStripMenuItem";
            this.용수공급일보ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.용수공급일보ToolStripMenuItem.Text = "시간별공급량(배수지)";
            this.용수공급일보ToolStripMenuItem.Click += new System.EventHandler(this.용수공급일보ToolStripMenuItem_Click);
            // 
            // 공급량수정ToolStripMenuItem
            // 
            this.공급량수정ToolStripMenuItem.Name = "공급량수정ToolStripMenuItem";
            this.공급량수정ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.공급량수정ToolStripMenuItem.Text = "공급량 수정";
            this.공급량수정ToolStripMenuItem.Click += new System.EventHandler(this.공급량수정ToolStripMenuItem_Click);
            // 
            // ToolStripMenuRealTimeData
            // 
            this.ToolStripMenuRealTimeData.Name = "ToolStripMenuRealTimeData";
            this.ToolStripMenuRealTimeData.Size = new System.Drawing.Size(198, 22);
            this.ToolStripMenuRealTimeData.Text = "실시간수집데이터 조회";
            this.ToolStripMenuRealTimeData.Click += new System.EventHandler(this.ToolStripMenuRealTimeData_Click);
            // 
            // 실시간경고ToolStripMenuItem
            // 
            this.실시간경고ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuContents,
            this.실시간경보확인ToolStripMenuItem});
            this.실시간경고ToolStripMenuItem.Name = "실시간경고ToolStripMenuItem";
            this.실시간경고ToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.실시간경고ToolStripMenuItem.Text = "실시간경보";
            // 
            // MenuContents
            // 
            this.MenuContents.CheckOnClick = true;
            this.MenuContents.Name = "MenuContents";
            this.MenuContents.Size = new System.Drawing.Size(162, 22);
            this.MenuContents.Text = "실시간경보 표시";
            this.MenuContents.Click += new System.EventHandler(this.MenuContents_Click);
            // 
            // 실시간경보확인ToolStripMenuItem
            // 
            this.실시간경보확인ToolStripMenuItem.Name = "실시간경보확인ToolStripMenuItem";
            this.실시간경보확인ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.실시간경보확인ToolStripMenuItem.Text = "실시간경보 이력";
            this.실시간경보확인ToolStripMenuItem.Click += new System.EventHandler(this.실시간경보확인ToolStripMenuItem_Click);
            // 
            // 기본설정ToolStripMenuItem
            // 
            this.기본설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuTagLinkage,
            this.DBConnToolStripMenuItem,
            this.UserManageToolStripMenuItem,
            this.PasswordManageToolStripMenuItem,
            this.ENV_TCPToolStripMenuItem,
            this.VersionManagerToolStripMenuItem,
            this.waterNET코드관리ToolStripMenuItem,
            this.waterNET로컬버전초기화ToolStripMenuItem,
            this.수용가핸드폰번호등록ToolStripMenuItem,
            this.tsMenuInfosImport,
            this.waterNET수질모델생성ToolStripMenuItem,
            this.MapExtentToolStripMenuItem});
            this.기본설정ToolStripMenuItem.Name = "기본설정ToolStripMenuItem";
            this.기본설정ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.기본설정ToolStripMenuItem.Text = "기본설정";
            // 
            // ToolStripMenuTagLinkage
            // 
            this.ToolStripMenuTagLinkage.Name = "ToolStripMenuTagLinkage";
            this.ToolStripMenuTagLinkage.Size = new System.Drawing.Size(285, 22);
            this.ToolStripMenuTagLinkage.Text = "IWater Tag Mapping";
            this.ToolStripMenuTagLinkage.Click += new System.EventHandler(this.ToolStripMenuTagLinkage_Click);
            // 
            // DBConnToolStripMenuItem
            // 
            this.DBConnToolStripMenuItem.Name = "DBConnToolStripMenuItem";
            this.DBConnToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.DBConnToolStripMenuItem.Text = "water-NET 데이터베이스 연결정보 관리";
            this.DBConnToolStripMenuItem.Click += new System.EventHandler(this.DBConnToolStripMenuItem_Click);
            // 
            // UserManageToolStripMenuItem
            // 
            this.UserManageToolStripMenuItem.Name = "UserManageToolStripMenuItem";
            this.UserManageToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.UserManageToolStripMenuItem.Text = "water-NET 사용자 관리";
            this.UserManageToolStripMenuItem.Click += new System.EventHandler(this.UserManageToolStripMenuItem_Click);
            // 
            // PasswordManageToolStripMenuItem
            // 
            this.PasswordManageToolStripMenuItem.Name = "PasswordManageToolStripMenuItem";
            this.PasswordManageToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.PasswordManageToolStripMenuItem.Text = "water-NET 사용자 비밀번호 관리";
            this.PasswordManageToolStripMenuItem.Click += new System.EventHandler(this.PasswordManageToolStripMenuItem_Click);
            // 
            // ENV_TCPToolStripMenuItem
            // 
            this.ENV_TCPToolStripMenuItem.Name = "ENV_TCPToolStripMenuItem";
            this.ENV_TCPToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.ENV_TCPToolStripMenuItem.Text = "water-NET FTP 환경정보 관리";
            this.ENV_TCPToolStripMenuItem.Click += new System.EventHandler(this.ENV_TCPToolStripMenuItem_Click);
            // 
            // VersionManagerToolStripMenuItem
            // 
            this.VersionManagerToolStripMenuItem.Name = "VersionManagerToolStripMenuItem";
            this.VersionManagerToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.VersionManagerToolStripMenuItem.Text = "water-NET 버전 관리";
            this.VersionManagerToolStripMenuItem.Click += new System.EventHandler(this.VersionManagerToolStripMenuItem_Click);
            // 
            // waterNET코드관리ToolStripMenuItem
            // 
            this.waterNET코드관리ToolStripMenuItem.Name = "waterNET코드관리ToolStripMenuItem";
            this.waterNET코드관리ToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.waterNET코드관리ToolStripMenuItem.Text = "water-NET 코드 관리";
            this.waterNET코드관리ToolStripMenuItem.Click += new System.EventHandler(this.waterNET코드관리ToolStripMenuItem_Click);
            // 
            // waterNET로컬버전초기화ToolStripMenuItem
            // 
            this.waterNET로컬버전초기화ToolStripMenuItem.Name = "waterNET로컬버전초기화ToolStripMenuItem";
            this.waterNET로컬버전초기화ToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.waterNET로컬버전초기화ToolStripMenuItem.Text = "water-NET 로컬 버전 초기화";
            this.waterNET로컬버전초기화ToolStripMenuItem.Click += new System.EventHandler(this.waterNET로컬버전초기화ToolStripMenuItem_Click);
            // 
            // 수용가핸드폰번호등록ToolStripMenuItem
            // 
            this.수용가핸드폰번호등록ToolStripMenuItem.Name = "수용가핸드폰번호등록ToolStripMenuItem";
            this.수용가핸드폰번호등록ToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.수용가핸드폰번호등록ToolStripMenuItem.Text = "수용가 핸드폰 번호 등록";
            this.수용가핸드폰번호등록ToolStripMenuItem.Click += new System.EventHandler(this.수용가핸드폰번호등록ToolStripMenuItem_Click);
            // 
            // tsMenuInfosImport
            // 
            this.tsMenuInfosImport.Name = "tsMenuInfosImport";
            this.tsMenuInfosImport.Size = new System.Drawing.Size(285, 22);
            this.tsMenuInfosImport.Text = "INFOS 데이터 수동등록";
            this.tsMenuInfosImport.Click += new System.EventHandler(this.tsMenuInfosImport_Click);
            // 
            // waterNET수질모델생성ToolStripMenuItem
            // 
            this.waterNET수질모델생성ToolStripMenuItem.Name = "waterNET수질모델생성ToolStripMenuItem";
            this.waterNET수질모델생성ToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.waterNET수질모델생성ToolStripMenuItem.Text = "water-NET 수질모델생성";
            this.waterNET수질모델생성ToolStripMenuItem.Click += new System.EventHandler(this.waterNET수질모델생성ToolStripMenuItem_Click);
            // 
            // MapExtentToolStripMenuItem
            // 
            this.MapExtentToolStripMenuItem.Name = "MapExtentToolStripMenuItem";
            this.MapExtentToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.MapExtentToolStripMenuItem.Text = "지도범위 설정";
            this.MapExtentToolStripMenuItem.Click += new System.EventHandler(this.MapExtentToolStripMenuItem_Click);
            // 
            // 진단관리ToolStripMenuItem
            // 
            this.진단관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuDrPipe,
            this.ToolStripMenuMapMaker});
            this.진단관리ToolStripMenuItem.Name = "진단관리ToolStripMenuItem";
            this.진단관리ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.진단관리ToolStripMenuItem.Text = "진단관리";
            // 
            // ToolStripMenuDrPipe
            // 
            this.ToolStripMenuDrPipe.Name = "ToolStripMenuDrPipe";
            this.ToolStripMenuDrPipe.Size = new System.Drawing.Size(135, 22);
            this.ToolStripMenuDrPipe.Text = "Dr. Pipe";
            this.ToolStripMenuDrPipe.Click += new System.EventHandler(this.ToolStripMenuDrPipe_Click);
            // 
            // ToolStripMenuMapMaker
            // 
            this.ToolStripMenuMapMaker.Name = "ToolStripMenuMapMaker";
            this.ToolStripMenuMapMaker.Size = new System.Drawing.Size(135, 22);
            this.ToolStripMenuMapMaker.Text = "Map Maker";
            this.ToolStripMenuMapMaker.Click += new System.EventHandler(this.ToolStripMenuMapMaker_Click);
            // 
            // 태그맵핑ToolStripMenuItem
            // 
            this.태그맵핑ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.태그맵핑관리ToolStripMenuItem});
            this.태그맵핑ToolStripMenuItem.Name = "태그맵핑ToolStripMenuItem";
            this.태그맵핑ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.태그맵핑ToolStripMenuItem.Text = "태그맵핑";
            this.태그맵핑ToolStripMenuItem.Visible = false;
            // 
            // 태그맵핑관리ToolStripMenuItem
            // 
            this.태그맵핑관리ToolStripMenuItem.Name = "태그맵핑관리ToolStripMenuItem";
            this.태그맵핑관리ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.태그맵핑관리ToolStripMenuItem.Text = "태그맵핑관리";
            this.태그맵핑관리ToolStripMenuItem.Click += new System.EventHandler(this.태그맵핑관리ToolStripMenuItem_Click);
            // 
            // statusBarXY
            // 
            this.statusBarXY.AutoSize = false;
            this.statusBarXY.AutoToolTip = true;
            this.statusBarXY.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.statusBarXY.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.statusBarXY.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.statusBarXY.Name = "statusBarXY";
            this.statusBarXY.Size = new System.Drawing.Size(200, 17);
            this.statusBarXY.ToolTipText = "지도좌표를 표시합니다.";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Enabled = false;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarXY,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 671);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1006, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusBar1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnAllClose,
            this.tsbtnClose,
            this.toolStripSeparator1,
            this.toolStripButton01,
            this.toolStripButton2,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton1,
            this.toolStripButton3,
            this.tsbtnTransfer,
            this.toolStripButton6,
            this.tsbtnEnergyPump,
            this.toolStripButton7,
            this.toolStripButtonReport,
            this.toolStripSeparator2,
            this.ToolStripMenuSimulation,
            this.tsbtnDrPipe,
            this.tsbtnMapMaker});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1006, 54);
            this.toolStrip1.TabIndex = 10;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbtnAllClose
            // 
            this.tsbtnAllClose.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnAllClose.Image")));
            this.tsbtnAllClose.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tsbtnAllClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnAllClose.Name = "tsbtnAllClose";
            this.tsbtnAllClose.Size = new System.Drawing.Size(59, 51);
            this.tsbtnAllClose.Text = "전체화면";
            this.tsbtnAllClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnAllClose.ToolTipText = "전체화면 지우기";
            this.tsbtnAllClose.Visible = false;
            this.tsbtnAllClose.Click += new System.EventHandler(this.tsbtnAllClose_Click);
            // 
            // tsbtnClose
            // 
            this.tsbtnClose.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnClose.Image")));
            this.tsbtnClose.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tsbtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnClose.Name = "tsbtnClose";
            this.tsbtnClose.Size = new System.Drawing.Size(59, 51);
            this.tsbtnClose.Text = "현재화면";
            this.tsbtnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnClose.ToolTipText = "현재화면 지우기";
            this.tsbtnClose.Visible = false;
            this.tsbtnClose.Click += new System.EventHandler(this.tsbtnClose_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 54);
            this.toolStripSeparator1.Visible = false;
            // 
            // toolStripButton01
            // 
            this.toolStripButton01.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton01.Image")));
            this.toolStripButton01.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolStripButton01.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton01.Name = "toolStripButton01";
            this.toolStripButton01.Size = new System.Drawing.Size(59, 51);
            this.toolStripButton01.Text = "관망해석";
            this.toolStripButton01.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton01.Click += new System.EventHandler(this.toolStripButton01_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(59, 51);
            this.toolStripButton2.Text = "누수감시";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(71, 51);
            this.toolStripButton4.Text = "유수율분석";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(59, 51);
            this.toolStripButton5.Text = "수압감시";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(59, 51);
            this.toolStripButton1.Text = "수질감시";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(59, 51);
            this.toolStripButton3.Text = "수질민원";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // tsbtnTransfer
            // 
            this.tsbtnTransfer.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnTransfer.Image")));
            this.tsbtnTransfer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnTransfer.Name = "tsbtnTransfer";
            this.tsbtnTransfer.Size = new System.Drawing.Size(59, 51);
            this.tsbtnTransfer.Text = "단수사고";
            this.tsbtnTransfer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnTransfer.Click += new System.EventHandler(this.tsbtnTransfer_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(71, 51);
            this.toolStripButton6.Text = "배수지운영";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // tsbtnEnergyPump
            // 
            this.tsbtnEnergyPump.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnEnergyPump.Image")));
            this.tsbtnEnergyPump.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnEnergyPump.Name = "tsbtnEnergyPump";
            this.tsbtnEnergyPump.Size = new System.Drawing.Size(47, 51);
            this.tsbtnEnergyPump.Text = "에너지";
            this.tsbtnEnergyPump.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnEnergyPump.Visible = false;
            this.tsbtnEnergyPump.Click += new System.EventHandler(this.tsbtnEnergyPump_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.CheckOnClick = true;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(59, 51);
            this.toolStripButton7.Text = "경보표시";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButtonReport
            // 
            this.toolStripButtonReport.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonReport.Image")));
            this.toolStripButtonReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReport.Name = "toolStripButtonReport";
            this.toolStripButtonReport.Size = new System.Drawing.Size(59, 51);
            this.toolStripButtonReport.Text = "운영일보";
            this.toolStripButtonReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonReport.Click += new System.EventHandler(this.toolStripButtonReport_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 54);
            // 
            // ToolStripMenuSimulation
            // 
            this.ToolStripMenuSimulation.Image = global::WaterNet.Water_Net.Properties.Resources.Simulation;
            this.ToolStripMenuSimulation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripMenuSimulation.Name = "ToolStripMenuSimulation";
            this.ToolStripMenuSimulation.Size = new System.Drawing.Size(71, 51);
            this.ToolStripMenuSimulation.Text = "시뮬레이션";
            this.ToolStripMenuSimulation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ToolStripMenuSimulation.Visible = false;
            this.ToolStripMenuSimulation.Click += new System.EventHandler(this.ToolStripMenuSimulation_Click);
            // 
            // tsbtnDrPipe
            // 
            this.tsbtnDrPipe.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnDrPipe.Image")));
            this.tsbtnDrPipe.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnDrPipe.Name = "tsbtnDrPipe";
            this.tsbtnDrPipe.Size = new System.Drawing.Size(50, 51);
            this.tsbtnDrPipe.Text = "Dr.Pipe";
            this.tsbtnDrPipe.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnDrPipe.Visible = false;
            this.tsbtnDrPipe.Click += new System.EventHandler(this.tsbtnDrPipe_Click);
            // 
            // tsbtnMapMaker
            // 
            this.tsbtnMapMaker.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnMapMaker.Image")));
            this.tsbtnMapMaker.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnMapMaker.Name = "tsbtnMapMaker";
            this.tsbtnMapMaker.Size = new System.Drawing.Size(68, 51);
            this.tsbtnMapMaker.Text = "MapMaker";
            this.tsbtnMapMaker.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnMapMaker.Visible = false;
            this.tsbtnMapMaker.Click += new System.EventHandler(this.tsbtnMapMaker_Click);
            // 
            // spcContents
            // 
            this.spcContents.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            this.spcContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcContents.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.spcContents.Location = new System.Drawing.Point(0, 78);
            this.spcContents.Name = "spcContents";
            this.spcContents.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcContents.Panel1
            // 
            this.spcContents.Panel1.Controls.Add(this.uTabContents);
            // 
            // spcContents.Panel2
            // 
            this.spcContents.Panel2.Controls.Add(this.griWarningList);
            this.spcContents.Panel2MinSize = 60;
            this.spcContents.Size = new System.Drawing.Size(1006, 593);
            this.spcContents.SplitterDistance = 532;
            this.spcContents.SplitterWidth = 1;
            this.spcContents.TabIndex = 11;
            // 
            // uTabContents
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTabContents.ActiveTabAppearance = appearance15;
            appearance13.BackColor = System.Drawing.Color.Silver;
            this.uTabContents.Appearance = appearance13;
            appearance16.BackColor = System.Drawing.Color.LightGray;
            this.uTabContents.ClientAreaAppearance = appearance16;
            this.uTabContents.CloseButtonLocation = Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.HeaderArea;
            this.uTabContents.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabContents.Location = new System.Drawing.Point(0, 0);
            this.uTabContents.Name = "uTabContents";
            this.uTabContents.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabContents.ShowButtonSeparators = true;
            this.uTabContents.Size = new System.Drawing.Size(1006, 532);
            this.uTabContents.TabCloseButtonAlignment = Infragistics.Win.UltraWinTabs.TabCloseButtonAlignment.AfterContent;
            this.uTabContents.TabCloseButtonVisibility = Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Always;
            appearance14.BackColor = System.Drawing.Color.LightGray;
            this.uTabContents.TabHeaderAreaAppearance = appearance14;
            this.uTabContents.TabIndex = 0;
            this.uTabContents.TabLayoutStyle = Infragistics.Win.UltraWinTabs.TabLayoutStyle.SingleRowFixed;
            this.uTabContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTabContents_MouseDown);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1002, 509);
            // 
            // griWarningList
            // 
            this.griWarningList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griWarningList.Location = new System.Drawing.Point(0, 0);
            this.griWarningList.Name = "griWarningList";
            this.griWarningList.Size = new System.Drawing.Size(1006, 60);
            this.griWarningList.TabIndex = 124;
            this.griWarningList.Text = "ultraGrid1";
            this.griWarningList.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.griWarningList_InitializeRow);
            this.griWarningList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griWarningList_DoubleClickRow);
            // 
            // contextMenu1
            // 
            this.contextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuthisClose,
            this.ToolStripMenuAllClose,
            this.ToolStripMenuNthisClose});
            this.contextMenu1.Name = "contextMenu1";
            this.contextMenu1.Size = new System.Drawing.Size(191, 70);
            // 
            // ToolStripMenuthisClose
            // 
            this.ToolStripMenuthisClose.Name = "ToolStripMenuthisClose";
            this.ToolStripMenuthisClose.Size = new System.Drawing.Size(190, 22);
            this.ToolStripMenuthisClose.Text = "현재화면 닫기";
            this.ToolStripMenuthisClose.Click += new System.EventHandler(this.ToolStripMenuthisClose_Click);
            // 
            // ToolStripMenuAllClose
            // 
            this.ToolStripMenuAllClose.Name = "ToolStripMenuAllClose";
            this.ToolStripMenuAllClose.Size = new System.Drawing.Size(190, 22);
            this.ToolStripMenuAllClose.Text = "전체화면 닫기";
            this.ToolStripMenuAllClose.Click += new System.EventHandler(this.ToolStripMenuAllClose_Click);
            // 
            // ToolStripMenuNthisClose
            // 
            this.ToolStripMenuNthisClose.Name = "ToolStripMenuNthisClose";
            this.ToolStripMenuNthisClose.Size = new System.Drawing.Size(190, 22);
            this.ToolStripMenuNthisClose.Text = "전체닫기(현재화면외)";
            this.ToolStripMenuNthisClose.Click += new System.EventHandler(this.ToolStripMenuNthisClose_Click);
            // 
            // frmLMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(1006, 693);
            this.Controls.Add(this.spcContents);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.MaximizeBox = false;
            this.Name = "frmLMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "water-NET";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.frmLMain_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcContents)).EndInit();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabContents)).EndInit();
            this.uTabContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griWarningList)).EndInit();
            this.contextMenu1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 관망해석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수량분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuEnergy;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuCrisis;
        private System.Windows.Forms.ToolStripMenuItem 진단관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 기본설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel statusBarXY;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabContents;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton tsbtnAllClose;
        private System.Windows.Forms.ToolStripButton tsbtnClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton01;
        private System.Windows.Forms.ToolStripMenuItem MenuCrisisWaterBreak;
        private System.Windows.Forms.ToolStripMenuItem 수질ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수질검사자료관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 실시간감시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 실시간감시관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 잔류염소예측ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 관세척구간관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNP파일관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuPumpSchedule;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuTagLinkage;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuEnergyState;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuDrPipe;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuMapMaker;
        private System.Windows.Forms.ToolStripMenuItem 실시간관망해석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수요예측ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuManage;
        private System.Windows.Forms.ToolStripMenuItem 수질민원관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton tsbtnTransfer;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton tsbtnEnergyPump;
        private System.Windows.Forms.ToolStripButton tsbtnDrPipe;
        private System.Windows.Forms.ToolStripButton tsbtnMapMaker;
        private System.Windows.Forms.ToolStripMenuItem DBConnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UserManageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PasswordManageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuWaterTrouble;
        private System.Windows.Forms.ToolStripMenuItem VersionManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ENV_TCPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 감압밸브ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 감압밸브감시ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenu1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuthisClose;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuAllClose;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButtonReport;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton ToolStripMenuSimulation;
        public System.Windows.Forms.SplitContainer spcContents;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuNthisClose;
        private System.Windows.Forms.ToolStripMenuItem 리포트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 관망운영일보ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 시단위공급량ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 일공급량ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 일야간최소유량ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 특이사항기록ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 누수감시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자동누수감시결과ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 공급량야간최소유량분석조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 누수량산정결과조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 야간최소유량산정방식별조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 블록별공급량수압트랜드조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율현황조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 월유수율분석보고서ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율상호분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 총괄수량수지분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율제고사업추진관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 계량기교체관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 누수지점관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 관개대체관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 단수작업관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 사업추진효과분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 블록별일반현황정보조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 집중관리우선순위조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 시단위공급량분기ToolStripMenuItem;
        private Infragistics.Win.UltraWinGrid.UltraGrid griWarningList;
        private System.Windows.Forms.ToolStripMenuItem 실시간경고ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 실시간경보확인ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuBlockFlowInfo;
        private System.Windows.Forms.ToolStripMenuItem MenuContents;
        private System.Windows.Forms.ToolStripMenuItem 블록별수량관리기본설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 용수공급일보ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수용가핸드폰번호등록ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem waterNET코드관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem waterNET로컬버전초기화ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수요예측결과조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 배수지운영시뮬레이션ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsMenuInfosImport;
        private System.Windows.Forms.ToolStripMenuItem 사업장관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 일별펌프성능모의분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 실시간펌프성능모의분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 성능진단자료관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 펌프설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 펌프장전력량원단위분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 펌프특성곡선분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsOptimunConstruct;
        private System.Windows.Forms.ToolStripMenuItem tsOptimunSpec;
        private System.Windows.Forms.ToolStripMenuItem tsEnergyBaseInfoSet;
        private System.Windows.Forms.ToolStripMenuItem 공급량수정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem waterNET수질모델생성ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MapExtentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuSupplyArea;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuRealTimeData;
        private System.Windows.Forms.ToolStripMenuItem 유수율ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 시단위공급량2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수질모의ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 스마트미터관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 트렌드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 태그맵핑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 태그맵핑관리ToolStripMenuItem;
    }
}

