﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace WaterNet.Water_Net.form
{
    public partial class frmFTPDownload : EMFrame.form.BaseForm
    {
        private delegate void SetProgressBarCallBack(int v);
        private delegate void SetProgressBarPerformCallBack();
        private delegate void SetMessageCallBack(string msg);
        private delegate void ExitCallBack();
        private delegate void SetProcessStatus(int v);

        private bool m_SUCCESS = false;
        private bool m_ReplaceApp = false;
        //private int m_RevisionDataNo = 0;
        //private int m_RevisionSystemNo = 0;
        private int m_RevisionNo = 0;

        private string m_DataType = "SYSTEM";

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        #region Socket Client 및 File 관련 정의
        //FTP Information
        private struct STRC_TCP_ENV_COMMON
        {
            public string FTP_IP;
            public string FTP_PORT;
            public string FTP_UID;
            public string FTP_PWD;
            public string FTP_PATH;
            public string FTP_SGCCD;
        }

        private STRC_TCP_ENV_COMMON m_STRC_TCP_ENV_COMMON;
        //private FTPClient.FTPConnection m_FTPCLIENT = null;

        private EnterpriseDT.Net.Ftp.FTPConnection _FTPCLIENT = null;

        #endregion

        //private System.Threading.Thread t1 = null;

        public frmFTPDownload()
        {
            InitializeComponent();
        }
        /// <summary>
        /// System Or Data
        /// </summary>
        public string DATATYPE
        {
            set { this.m_DataType = value; }
        }
        public bool SUCCESS
        {
            get { return m_SUCCESS; }
        }

        public bool REPLACED
        {
            get { return m_ReplaceApp; }
        }

        private void frmFTPDownload_Load(object sender, EventArgs e)
        {
            this.LbChecking.EnabledChanged += new EventHandler(LabelChecking_EnabledChanged);
            this.LbComplete.EnabledChanged += new EventHandler(LabelChecking_EnabledChanged);
            this.LbConnecting.EnabledChanged += new EventHandler(LabelChecking_EnabledChanged);
            this.LbDownloading.EnabledChanged += new EventHandler(LabelChecking_EnabledChanged);
        }

        private void frmFTPDownload_Shown(object sender, EventArgs e)
        {
            try
            {
                this.SetStatus(0);
                this.GetTCPInfor();
                if (string.IsNullOrEmpty(m_STRC_TCP_ENV_COMMON.FTP_IP))
                {
                    MessageBox.Show("FTP 서버의 정보 취득에 실패했습니다. 관리자에게 문의하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return;
                }

                Application.DoEvents();
                this.FTP_Connect();
                if (!this._FTPCLIENT.IsConnected)
                {
                    MessageBox.Show("FTP 서버에 연결 실패! FTP 환경정보를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return;
                }

                //if (!FTP_CurrentDirectory(EMFrame.statics.AppStatic.USER_SGCCD))
                //{
                //    MessageBox.Show("FTP 서버에 " + EMFrame.statics.AppStatic.USER_SGCCD + "폴더가 없습니다..", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    this.Close();
                //    return;
                //}

                this.getRevision_number();

                System.Threading.Thread t1 = new System.Threading.Thread(new System.Threading.ThreadStart(RevisionDownload));
                t1.Start();
            }
            catch (Exception ex)
            {   
                Console.WriteLine(ex.ToString());
            }

        }
        private string IniReadValue(string Section, string Key)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "Revision.ini";

            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, strPath + strFile);
            return temp.ToString();
        }

        private void IniWriteValue(string Section, string Key, string Value)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "Revision.ini";

            WritePrivateProfileString(Section, Key, Value, strPath + strFile);
        }

        private void getRevision_number()
        {
            if (this.m_DataType.Equals("SYSTEM"))
            {
                string SystemNo = IniReadValue("REVISION", "SYSTEM");
                this.m_RevisionNo = (SystemNo.Length == 0) ? 0 : Convert.ToInt32(SystemNo);
            }
            else if (this.m_DataType.Equals("DATA"))
            {
                string DataNo = IniReadValue("REVISION", EMFrame.statics.AppStatic.USER_SGCCD);
                this.m_RevisionNo = (DataNo.Length == 0) ? 0 : Convert.ToInt32(DataNo);
            }
            

            //this.m_RevisionDataNo = (DataNo.Length == 0) ? 0 : Convert.ToInt32(DataNo);
            //this.m_RevisionSystemNo = (SystemNo.Length == 0) ? 0 : Convert.ToInt32(SystemNo);

            EMFrame.utils.LoggerUtils.logger.Info(string.Format("Type : {0}\tRevision Number : {1}", this.m_DataType, this.m_RevisionNo));
        }


        private void setRevision_number()
        {
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                StringBuilder oStringBuilder = new StringBuilder();
                if (this.m_DataType.Equals("SYSTEM"))
                {
                    oStringBuilder.AppendLine("    SELECT NVL(MAX(A.REVISION_NO),:REVISION_NO) REVISION_NO, 'SYSTEM' AS GBN");
                    oStringBuilder.AppendLine("       FROM CM_REVISION A                                                     ");
                    oStringBuilder.AppendLine("          , CM_VERSION B                                                      ");
                    oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                 ");
                    oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                     ");
                    oStringBuilder.AppendLine("        AND A.ZONE_GBN = :ZONE_GBN                                            ");
                    oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                         ");
                    oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                              ");
                    oStringBuilder.AppendLine("        AND A.REVISION_NO > TO_NUMBER(:REVISION_NO)                    ");
                }
                else if (this.m_DataType.Equals("DATA"))
                {
                    oStringBuilder.AppendLine("    SELECT NVL(MAX(A.REVISION_NO),:REVISION_NO) REVISION_NO, 'DATA' AS GBN");
                    oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
                    oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
                    oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
                    oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
                    oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
                    oStringBuilder.AppendLine("       AND A.ZONE_GBN = :ZONE_GBN                                             ");
                    oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
                    oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
                    oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T','N')                                       ");
                    oStringBuilder.AppendLine("       AND A.REVISION_NO > TO_NUMBER(:REVISION_NO)                            ");
                }

                //oStringBuilder.AppendLine("    SELECT NVL(MAX(A.REVISION_NO),:REVISION_DATA_NO) REVISION_NO, 'DATA' AS GBN");
                //oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
                //oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
                //oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
                //oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
                //oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
                //oStringBuilder.AppendLine("       AND A.ZONE_GBN = :ZONE_GBN                                             ");
                //oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
                //oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
                //oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T')                                           ");
                //oStringBuilder.AppendLine("       AND A.REVISION_NO > TO_NUMBER(:REVISION_DATA_NO)                       ");
                //oStringBuilder.AppendLine("     UNION ALL                                                                ");
                //oStringBuilder.AppendLine("    SELECT NVL(MAX(A.REVISION_NO),:REVISION_SYSTEM_NO) REVISION_NO, 'SYSTEM' AS GBN");
                //oStringBuilder.AppendLine("       FROM CM_REVISION A                                                     ");
                //oStringBuilder.AppendLine("          , CM_VERSION B                                                      ");
                //oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                 ");
                //oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                     ");
                //oStringBuilder.AppendLine("        AND A.ZONE_GBN = :ZONE_GBN                                            ");
                //oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                         ");
                //oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                              ");
                //oStringBuilder.AppendLine("        AND A.REVISION_NO > TO_NUMBER(:REVISION_SYSTEM_NO)                    ");

                System.Collections.Hashtable param = new System.Collections.Hashtable();
                param.Add("MGDPCD", EMFrame.statics.AppStatic.USER_SGCCD);
                param.Add("REVISION_NO", this.m_RevisionNo);
                //param.Add("REVISION_SYSTEM_NO", this.m_RevisionSystemNo);
                param.Add("ZONE_GBN", EMFrame.statics.AppStatic.ZONE_GBN);

                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);

                foreach (DataRow row in table.Rows)
                {
                    if (Convert.ToString(row["GBN"]).Equals("DATA"))
                    {
                        IniWriteValue("REVISION", EMFrame.statics.AppStatic.USER_SGCCD, Convert.ToString(row["REVISION_NO"]));
                    }
                    else
                    {
                        IniWriteValue("REVISION", "SYSTEM", Convert.ToString(row["REVISION_NO"]));
                    }
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        void LabelChecking_EnabledChanged(object sender, EventArgs e)
        {
            ((Label)sender).ForeColor = ((Label)sender).Enabled ? Color.Red : Color.Black;
            Application.DoEvents();
        }

        private void frmFTPDownload_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (this._FTPCLIENT != null && this._FTPCLIENT.IsConnected) _FTPCLIENT.Close();

                System.Threading.Thread.Sleep(100);
            }
            catch (Exception ex)
            {   
                MessageBox.Show(ex.ToString());
            }

        }

        #region Delegate Method
        private void Exit()
        {
            ExitCallBack dele = new ExitCallBack(this.Close);
            this.Invoke(dele);
        }

        private void SetProgressBar(int v)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarCallBack dele = new SetProgressBarCallBack(SetProgressBar);
                this.Invoke(dele, new object[] { v });
            }
            else this.progressBar1.Value = v;
        }

        private void SetProgressBarMaximum(int v)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarCallBack dele = new SetProgressBarCallBack(SetProgressBarMaximum);
                this.Invoke(dele, new object[] { v });
            }
            else this.progressBar1.Maximum = v;
        }

        private void SetProgressBarMinimum(int v)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarCallBack dele = new SetProgressBarCallBack(SetProgressBarMinimum);
                this.Invoke(dele, new object[] { v });
            }
            else this.progressBar1.Minimum = v;
        }

        private void SetProgressBarStep(int v)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarCallBack dele = new SetProgressBarCallBack(SetProgressBarStep);
                this.Invoke(dele, new object[] { v });
            }
            else this.progressBar1.Step = v;
        }

        private void SetProgressBarPerform()
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarPerformCallBack dele = new SetProgressBarPerformCallBack(SetProgressBarPerform);
                this.Invoke(dele, new object[] { });
            }
            else this.progressBar1.PerformStep();
        }

        private void SetMessage(string msg)
        {
            if (this.LbFileName.InvokeRequired)
            {
                SetMessageCallBack dele = new SetMessageCallBack(SetMessage);
                this.Invoke(dele, new object[] { msg });
            }
            else this.LbFileName.Text = "파일 명: " + msg;
        }

        // 진행 상황 체크
        private void SetStatus(int v)
        {
            switch (v)
            {
                case 0:
                    if (this.LbConnecting.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbConnecting.Enabled = true;

                    if (this.LbChecking.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbChecking.Enabled = false;

                    if (this.LbDownloading.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbDownloading.Enabled = false;

                    if (this.LbComplete.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbComplete.Enabled = false;
                    break;
                case 1:
                    if (this.LbConnecting.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbConnecting.Enabled = false;

                    if (this.LbChecking.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbChecking.Enabled = true;

                    if (this.LbDownloading.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbDownloading.Enabled = false;

                    if (this.LbComplete.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbComplete.Enabled = false;
                    break;
                case 2:
                    if (this.LbConnecting.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbConnecting.Enabled = false;

                    if (this.LbChecking.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbChecking.Enabled = false;

                    if (this.LbDownloading.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbDownloading.Enabled = true;

                    if (this.LbComplete.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbComplete.Enabled = false;
                    break;
                case 3:
                    if (this.LbConnecting.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbConnecting.Enabled = false;

                    if (this.LbChecking.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbChecking.Enabled = false;

                    if (this.LbDownloading.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbDownloading.Enabled = false;

                    if (this.LbComplete.InvokeRequired)
                    {
                        SetProcessStatus dele = new SetProcessStatus(SetStatus);
                        this.Invoke(dele, new object[] { v });
                    }
                    else this.LbComplete.Enabled = true;
                    break;
            }
            Application.DoEvents();

        }

        #endregion Delegate Method

        private void ReplaceFile(string filename)
        {
            string file = System.IO.Path.GetFileName(filename);
            string newName = filename.Replace(file, file + ".update");

            if (System.IO.File.Exists(newName))
            {
                System.IO.File.Delete(newName);
            }
            if (System.IO.File.Exists(filename))
            {
                System.IO.File.Move(filename, newName);
                if (file.Equals("ServerManager.exe") || file.Equals("WaterNet.Water_Net.exe"))
                {
                    m_ReplaceApp = true;
                }
            }
        }

        private bool IsAccessable(string filename)
        {
            bool result = true;
            System.IO.FileStream fs = null;
            try
            {
                if (System.IO.File.Exists(filename))
                {
                    fs = new System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
                }
            }
            catch (System.IO.IOException)
            {
                //에러가 발생한 이유는 이미 다른 프로세서에서 점유중이거나.
                //혹은 파일이 존재하지 않기 때문이다.
                result = false;
            }
            finally
            {
                if (fs != null)
                {
                    //만약에 파일이 정상적으로 열렸다면 점유중이 아니다.
                    //다시 파일을 닫아줘야 한다.
                    fs.Close();
                }
            }
 
            return result;
        }

        public void RevisionDownload()
        {
            //1.클라인언트 (SGCCD) 폴더 확인
            string folder = EMFrame.statics.AppStatic.DATA_DIR + "\\" + EMFrame.statics.AppStatic.USER_SGCCD;
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(folder);
            if (!di.Exists) di.Create();
            System.IO.DirectoryInfo INPfolder = new System.IO.DirectoryInfo(di.FullName + "\\INPgraphic");
            if (!INPfolder.Exists) INPfolder.Create();
            System.IO.DirectoryInfo PIPEfolder = new System.IO.DirectoryInfo(di.FullName + "\\Pipegraphic");
            if (!PIPEfolder.Exists) PIPEfolder.Create();
            System.IO.DirectoryInfo TOPOfoler = new System.IO.DirectoryInfo(di.FullName + "\\Topographic");
            if (!TOPOfoler.Exists) TOPOfoler.Create();
            System.IO.DirectoryInfo NETfoler = new System.IO.DirectoryInfo(di.FullName + "\\Netgraphic");
            if (!NETfoler.Exists) NETfoler.Create();
            System.IO.DirectoryInfo APPfoler = new System.IO.DirectoryInfo(Application.StartupPath);

            try
            {
                this.SetStatus(1);
                System.Data.DataTable data = this.GetRevisionFile(this.m_RevisionNo);
                this.SetProgressBarMaximum(data.Rows.Count);
                this.SetProgressBarStep(1);

                System.IO.DirectoryInfo Cdi = null;

                this.SetStatus(2);
                foreach (DataRow row in data.Rows)
                {
                    string file_name = Convert.ToString(row["FILE_NAME"]);
                    string file_rname = Convert.ToString(row["FILE_RNAME"]);
                    DateTime file_date = Convert.ToDateTime(row["FILE_DATE"]);

                    this.SetProgressBarPerform();
                    this.SetMessage(file_name);
                    Application.DoEvents();

                    switch (Convert.ToString(row["FILE_TYPE"]))
                    {
                        case "S":
                            Cdi = APPfoler;
                            FTP_CurrentDirectory("System");
                            break;
                        case "P":
                            Cdi = PIPEfolder;
                            FTP_CurrentDirectory(EMFrame.statics.AppStatic.USER_SGCCD);
                            break;
                        case "T":
                            Cdi = TOPOfoler;
                            FTP_CurrentDirectory(EMFrame.statics.AppStatic.USER_SGCCD);
                            break;
                        case "N":
                            Cdi = NETfoler;
                            FTP_CurrentDirectory(EMFrame.statics.AppStatic.USER_SGCCD);
                            break;
                    }

                    ///FTP서버에 파일이 있는지 확인
                    ///Local에 파일 확인, 파일일자 >= 리비전파일일자 비교

                    if (!_FTPCLIENT.Exists(file_rname)) continue;

                    //if (System.IO.File.Exists(Cdi.FullName + "\\" + file_name))
                    //{
                    //    System.IO.FileInfo file = new System.IO.FileInfo(Cdi + "\\" + file_name);
                    //    if (file.LastWriteTime >= file_date) continue; //리비전 파일보다 날짜가 크면 Skip
                    //}
                    //------------------------------------------------------------------
                    bool result = this.FTP_GetData(Cdi, file_name, file_rname);
                    if (result)
                    {
                        System.IO.FileInfo file = new System.IO.FileInfo(Cdi + "\\" + file_name);
                        file.LastWriteTime = file_date;
                    }
                }

                m_SUCCESS = true;

                this.SetStatus(3);

                this.setRevision_number();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                m_SUCCESS = false;
            }
            finally
            {
                System.Threading.Thread.Sleep(100);
                this.Exit();
            }
        }

        /// <summary>
        /// FTP Server에서 특정 Directory로 이동한다.
        /// Directory가 없으면, 생성한다.
        /// </summary>
        /// <param name="strPath">이동할 경로</param>
        private void FTP_ChangeDirectory(string strPath)
        {
            if (_FTPCLIENT == null || !_FTPCLIENT.IsConnected) return;

            try
            {
                _FTPCLIENT.ServerDirectory = "/";
                if (!_FTPCLIENT.DirectoryExists(strPath))
                {
                    _FTPCLIENT.CreateDirectory(strPath);
                }

                _FTPCLIENT.ServerDirectory = strPath;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="di"></param>
        /// <param name="file_name">로컬파일</param>
        /// <param name="file_rname">서버파일</param>
        /// <returns></returns>
        private bool FTP_GetData(System.IO.DirectoryInfo di, string file_name, string file_rname)
        {
            bool result = false;
            System.IO.FileStream oFileStream = null;
            try
            {
                //Console.Write(di.FullName + "\\" + file_name + "=>");
                //Console.WriteLine(file_rname);
                if (!IsAccessable(di.FullName + "\\" + file_name))  //현재 사용중인 파일
                {
                    this.ReplaceFile(di.FullName + "\\" + file_name);
                }

                oFileStream = new System.IO.FileStream(di.FullName + "\\" + file_name, System.IO.FileMode.Create);
                _FTPCLIENT.DownloadStream(oFileStream, file_rname);
                //m_FTPCLIENT.GetStream(file_rname, oFileStream, FTPClient.FTPFileTransferType.Binary);
                Application.DoEvents();
                result = true;
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                oFileStream.Close();
            }

            return result;
        }

        /// <summary>
        /// FTP 정보를 취득
        /// </summary>
        private void GetTCPInfor()
        {
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT   IP_ADDR, FTP_PORT, FTP_UID, FTP_PWD, FTP_PATH");
                oStringBuilder.AppendLine("FROM     CM_ENV_TCP");
                oStringBuilder.AppendLine("WHERE    ROWNUM = 1");

                DataTable datatable = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new IDataParameter[] { });

                this.m_STRC_TCP_ENV_COMMON = new STRC_TCP_ENV_COMMON();

                foreach (DataRow oDRow in datatable.Rows)
                {
                    this.m_STRC_TCP_ENV_COMMON.FTP_IP = oDRow["IP_ADDR"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_PORT = oDRow["FTP_PORT"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_UID = oDRow["FTP_UID"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_PWD = EMFrame.utils.ConvertUtils.DecryptKey(oDRow["FTP_PWD"].ToString().Trim());
                    this.m_STRC_TCP_ENV_COMMON.FTP_PATH = oDRow["FTP_PATH"].ToString().Trim();
                }

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        /// <summary>
        /// FTP Server에 Connect
        /// </summary>     
        private void FTP_Connect()
        {
            int intPort = Int32.Parse(m_STRC_TCP_ENV_COMMON.FTP_PORT);
            try
            {
                if (_FTPCLIENT == null) _FTPCLIENT = new EnterpriseDT.Net.Ftp.FTPConnection();
                if (_FTPCLIENT.IsConnected) _FTPCLIENT.Close();

                if (!_FTPCLIENT.IsConnected)
                {
                    _FTPCLIENT.ServerAddress = m_STRC_TCP_ENV_COMMON.FTP_IP;
                    _FTPCLIENT.ServerPort = intPort;
                    _FTPCLIENT.UserName = m_STRC_TCP_ENV_COMMON.FTP_UID;
                    _FTPCLIENT.Password = m_STRC_TCP_ENV_COMMON.FTP_PWD;

                    _FTPCLIENT.Connect();

                    _FTPCLIENT.ServerDirectory = "/";
                }

                //if (m_FTPCLIENT == null) m_FTPCLIENT = new FTPClient.FTPConnection();
                //if (!m_FTPCLIENT.Connected)
                //{
                //    m_FTPCLIENT.Open(m_STRC_TCP_ENV_COMMON.FTP_IP, intPort, m_STRC_TCP_ENV_COMMON.FTP_UID, m_STRC_TCP_ENV_COMMON.FTP_PWD, FTPClient.FTPMode.Passive);
                //}
            }
            catch (Exception)
            { throw; }
        }

        private bool FTP_CurrentDirectory(string strPath)
        {
            bool result = false;

            _FTPCLIENT.ServerDirectory = "/";
            if (_FTPCLIENT.DirectoryExists(_FTPCLIENT.ServerDirectory + strPath))
            {
                _FTPCLIENT.ServerDirectory = strPath;
                result = true;
            }
            //string[] files = _FTPCLIENT.GetFiles(strPath);

            //if (files.Length != 0)
            //{
            //    _FTPCLIENT.ServerDirectory = strPath;
            //    result = true;
            //}

            //System.Collections.ArrayList array = m_FTPCLIENT.Dir(strPath);
            //if (array.Count != 0)
            //{
            //    m_FTPCLIENT.SetCurrentDirectory(strPath);
            //    result = true;
            //}

            return result;
        }

        private bool FTP_FileExist(string[] FTP_fils, string FTP_file)
        {
            bool result = false;
            foreach (string file in FTP_fils)
            {
                if (file.Equals(FTP_file))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        private System.Data.DataTable GetRevisionFile(int RevisionNo)
        {
            EMFrame.dm.EMapper mapper = null;
            System.Data.DataTable result = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                System.Collections.Hashtable param = new System.Collections.Hashtable();

                param.Add("MGDPCD", EMFrame.statics.AppStatic.USER_SGCCD);
                param.Add("REVISION_NO", RevisionNo);
                param.Add("ZONE_GBN", EMFrame.statics.AppStatic.ZONE_GBN);
                if (this.m_DataType.Equals("SYSTEM"))
                {
                    result = this.GetVersionHistorySystem(ref mapper, param);
                }
                else if (this.m_DataType.Equals("DATA"))
                {
                    result = this.GetVersionHistoryData(ref mapper, param);
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        private System.Data.DataTable GetVersionHistorySystem(ref EMFrame.dm.EMapper mapper, System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("WITH VER AS                                                                   ");
            oStringBuilder.AppendLine("(                                                                             ");
            oStringBuilder.AppendLine("     SELECT :MGDPCD MGDPCD                                                    ");
            oStringBuilder.AppendLine("            ,C.FILE_NAME, C.FILE_TYPE                                         ");
            oStringBuilder.AppendLine("            ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE  ");
            oStringBuilder.AppendLine("       FROM CM_REVISION A                                                     ");
            oStringBuilder.AppendLine("          , CM_VERSION B                                                      ");
            oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                 ");
            oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                     ");
            oStringBuilder.AppendLine("        AND A.ZONE_GBN = :ZONE_GBN                                            ");
            oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                         ");
            oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                              ");
            oStringBuilder.AppendLine("        AND A.REVISION_NO > TO_NUMBER(:REVISION_NO)                    ");
            oStringBuilder.AppendLine(")                                                                             ");
            oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                             ");
            oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                             ");
            oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                            ");
            oStringBuilder.AppendLine("  FROM VER                                                                    ");
            oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                       ");
            oStringBuilder.AppendLine("           FROM VER                                                           ");
            oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                  ");
            oStringBuilder.AppendLine("        ) B                                                                   ");
            oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                           ");
            oStringBuilder.AppendLine("    AND VER.REVISION_NO = B.REVISION_NO                                       ");
            oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC                             ");

            System.Data.DataTable result = null;
            try
            {
                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }

            return result;

        }

        private System.Data.DataTable GetVersionHistoryData(ref EMFrame.dm.EMapper mapper, System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("WITH VER AS                                                                   ");
            oStringBuilder.AppendLine("(                                                                             ");
            oStringBuilder.AppendLine("    SELECT A.MGDPCD                                                           ");
            oStringBuilder.AppendLine("          ,C.FILE_NAME, C.FILE_TYPE                                           ");
            oStringBuilder.AppendLine("          ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE    ");
            oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
            oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
            oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
            oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
            oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
            oStringBuilder.AppendLine("       AND A.ZONE_GBN = :ZONE_GBN                                             ");
            oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
            oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
            oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T','N')                                       ");
            oStringBuilder.AppendLine("       AND A.REVISION_NO > TO_NUMBER(:REVISION_NO)                            ");
            oStringBuilder.AppendLine(")                                                                             ");
            oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                             ");
            oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                             ");
            oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                            ");
            oStringBuilder.AppendLine("  FROM VER                                                                    ");
            oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                       ");
            oStringBuilder.AppendLine("           FROM VER                                                           ");
            oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                  ");
            oStringBuilder.AppendLine("        ) B                                                                   ");
            oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                           ");
            oStringBuilder.AppendLine("    AND VER.REVISION_NO = B.REVISION_NO                                       ");
            oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC                             ");

            System.Data.DataTable result = null;
            try
            {
                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }

            return result;
        }
        ///// <summary>
        ///// 현재 리비전 이하의 모든 파일을 표시한다. 
        ///// </summary>
        ///// <param name="param"></param>
        ///// <returns></returns>
        //private System.Data.DataTable GetVersionHistoryAllData(ref EMFrame.dm.EMapper mapper, System.Collections.Hashtable param)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("WITH VER AS                                                                   ");
        //    oStringBuilder.AppendLine("(                                                                             ");
        //    oStringBuilder.AppendLine("    SELECT A.MGDPCD                                                           ");
        //    oStringBuilder.AppendLine("          ,C.FILE_NAME, C.FILE_TYPE                                           ");
        //    oStringBuilder.AppendLine("          ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE    ");
        //    oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
        //    oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
        //    oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
        //    oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
        //    oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
        //    oStringBuilder.AppendLine("       AND A.ZONE_GBN = :ZONE_GBN                                             ");
        //    oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
        //    oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
        //    oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T')                                           ");
        //    oStringBuilder.AppendLine("       AND A.REVISION_NO > TO_NUMBER(:REVISION_DATA_NO)                       ");
        //    //oStringBuilder.AppendLine("     UNION ALL                                                                ");
        //    //oStringBuilder.AppendLine("     SELECT :MGDPCD MGDPCD                                                    ");
        //    //oStringBuilder.AppendLine("            ,C.FILE_NAME, C.FILE_TYPE                                         ");
        //    //oStringBuilder.AppendLine("            ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE  ");
        //    //oStringBuilder.AppendLine("       FROM CM_REVISION A                                                     ");
        //    //oStringBuilder.AppendLine("          , CM_VERSION B                                                      ");
        //    //oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                 ");
        //    //oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                     ");
        //    //oStringBuilder.AppendLine("        AND A.ZONE_GBN = :ZONE_GBN                                            ");
        //    //oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                         ");
        //    //oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                              ");
        //    //oStringBuilder.AppendLine("        AND A.REVISION_NO > TO_NUMBER(:REVISION_SYSTEM_NO)                    ");
        //    oStringBuilder.AppendLine(")                                                                             ");
        //    oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                             ");
        //    oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                             ");
        //    oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                            ");
        //    oStringBuilder.AppendLine("  FROM VER                                                                    ");
        //    oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                       ");
        //    oStringBuilder.AppendLine("           FROM VER                                                           ");
        //    oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                  ");
        //    oStringBuilder.AppendLine("        ) B                                                                   ");
        //    oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                           ");
        //    oStringBuilder.AppendLine("    AND VER.REVISION_NO = B.REVISION_NO                                       ");
        //    oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC                             ");

        //    System.Data.DataTable result = null;
        //    try
        //    {
        //        result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
        //    }
        //    catch (Exception oException)
        //    {
        //        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
        //    }

        //    return result;
        //}




    }
}
