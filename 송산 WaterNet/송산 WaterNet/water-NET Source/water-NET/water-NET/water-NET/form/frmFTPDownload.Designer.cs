﻿namespace WaterNet.Water_Net.form
{
    partial class frmFTPDownload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.TitlePanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.StatusPanel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LbComplete = new System.Windows.Forms.Label();
            this.LbDownloading = new System.Windows.Forms.Label();
            this.LbChecking = new System.Windows.Forms.Label();
            this.LbConnecting = new System.Windows.Forms.Label();
            this.Status1 = new System.Windows.Forms.Panel();
            this.LbFileName = new System.Windows.Forms.Label();
            this.LbStatus = new System.Windows.Forms.Label();
            this.TitlePanel.SuspendLayout();
            this.StatusPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 156);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(485, 23);
            this.progressBar1.TabIndex = 0;
            // 
            // TitlePanel
            // 
            this.TitlePanel.BackColor = System.Drawing.Color.SlateGray;
            this.TitlePanel.Controls.Add(this.label2);
            this.TitlePanel.Location = new System.Drawing.Point(12, 12);
            this.TitlePanel.Name = "TitlePanel";
            this.TitlePanel.Size = new System.Drawing.Size(485, 24);
            this.TitlePanel.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(485, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "최신 데이터 업데이트";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StatusPanel
            // 
            this.StatusPanel.BackColor = System.Drawing.Color.White;
            this.StatusPanel.Controls.Add(this.panel3);
            this.StatusPanel.Controls.Add(this.panel2);
            this.StatusPanel.Controls.Add(this.panel1);
            this.StatusPanel.Controls.Add(this.LbComplete);
            this.StatusPanel.Controls.Add(this.LbDownloading);
            this.StatusPanel.Controls.Add(this.LbChecking);
            this.StatusPanel.Controls.Add(this.LbConnecting);
            this.StatusPanel.Controls.Add(this.Status1);
            this.StatusPanel.Location = new System.Drawing.Point(12, 42);
            this.StatusPanel.Name = "StatusPanel";
            this.StatusPanel.Size = new System.Drawing.Size(485, 58);
            this.StatusPanel.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(422, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(23, 24);
            this.panel3.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(295, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(23, 24);
            this.panel2.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(168, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(23, 24);
            this.panel1.TabIndex = 2;
            // 
            // LbComplete
            // 
            this.LbComplete.AutoSize = true;
            this.LbComplete.Enabled = false;
            this.LbComplete.Location = new System.Drawing.Point(386, 35);
            this.LbComplete.Name = "LbComplete";
            this.LbComplete.Size = new System.Drawing.Size(93, 12);
            this.LbComplete.TabIndex = 1;
            this.LbComplete.Text = "업그레이드 완료";
            // 
            // LbDownloading
            // 
            this.LbDownloading.AutoSize = true;
            this.LbDownloading.Enabled = false;
            this.LbDownloading.Location = new System.Drawing.Point(278, 35);
            this.LbDownloading.Name = "LbDownloading";
            this.LbDownloading.Size = new System.Drawing.Size(69, 12);
            this.LbDownloading.TabIndex = 1;
            this.LbDownloading.Text = "다운 로딩...";
            // 
            // LbChecking
            // 
            this.LbChecking.AutoSize = true;
            this.LbChecking.Enabled = false;
            this.LbChecking.Location = new System.Drawing.Point(142, 35);
            this.LbChecking.Name = "LbChecking";
            this.LbChecking.Size = new System.Drawing.Size(85, 12);
            this.LbChecking.TabIndex = 1;
            this.LbChecking.Text = "버젼 체크 중...";
            // 
            // LbConnecting
            // 
            this.LbConnecting.AutoSize = true;
            this.LbConnecting.Location = new System.Drawing.Point(16, 35);
            this.LbConnecting.Name = "LbConnecting";
            this.LbConnecting.Size = new System.Drawing.Size(85, 12);
            this.LbConnecting.TabIndex = 1;
            this.LbConnecting.Text = "서버 연결 중...";
            // 
            // Status1
            // 
            this.Status1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Status1.Location = new System.Drawing.Point(41, 7);
            this.Status1.Name = "Status1";
            this.Status1.Size = new System.Drawing.Size(23, 24);
            this.Status1.TabIndex = 0;
            // 
            // LbFileName
            // 
            this.LbFileName.AutoSize = true;
            this.LbFileName.Location = new System.Drawing.Point(13, 113);
            this.LbFileName.Name = "LbFileName";
            this.LbFileName.Size = new System.Drawing.Size(53, 12);
            this.LbFileName.TabIndex = 6;
            this.LbFileName.Text = "파일 명: ";
            // 
            // LbStatus
            // 
            this.LbStatus.AutoSize = true;
            this.LbStatus.Location = new System.Drawing.Point(13, 138);
            this.LbStatus.Name = "LbStatus";
            this.LbStatus.Size = new System.Drawing.Size(65, 12);
            this.LbStatus.TabIndex = 7;
            this.LbStatus.Text = "진행 내역: ";
            // 
            // frmFTPDownload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 192);
            this.Controls.Add(this.LbStatus);
            this.Controls.Add(this.LbFileName);
            this.Controls.Add(this.StatusPanel);
            this.Controls.Add(this.TitlePanel);
            this.Controls.Add(this.progressBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFTPDownload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmFTPDownload";
            this.Load += new System.EventHandler(this.frmFTPDownload_Load);
            this.Shown += new System.EventHandler(this.frmFTPDownload_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmFTPDownload_FormClosing);
            this.TitlePanel.ResumeLayout(false);
            this.StatusPanel.ResumeLayout(false);
            this.StatusPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel TitlePanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel StatusPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LbComplete;
        private System.Windows.Forms.Label LbDownloading;
        private System.Windows.Forms.Label LbChecking;
        private System.Windows.Forms.Label LbConnecting;
        private System.Windows.Forms.Panel Status1;
        private System.Windows.Forms.Label LbFileName;
        private System.Windows.Forms.Label LbStatus;
    }
}