﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.Water_Net.form
{
    public partial class frmDuplicateUser : Form
    {
        private string _username = string.Empty;
        private string _userid = string.Empty;
        public frmDuplicateUser(string userid)
        {
            InitializeComponent();
            this._userid = userid;
        }

        public string USERID
        {
            get { return this._userid; }
        }

        public string USERNAME
        {
            get { return this._username; }
        }
        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "USER_NAME", "사용자", 80, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "USER_ID", "사번", 60, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "ROHQNM", "지역본부", 80, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "MGDPNM", "서비스센터", 80, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "USER_DIVISION", "부서", 150, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WaterNetCore.FormManager.SetGridStyle(this.ugUser);
            //WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugUser);
        }

        private void frmDuplicateUser_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();

            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                //Console.WriteLine(strEncUPWD);
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT user_name, user_id, rohqnm, mgdpnm, user_division FROM V_USER                   ");
                oStringBuilder.AppendLine(" WHERE USER_ID = '" + this._userid + "'");
                oStringBuilder.AppendLine("   AND USE_YN = '1'");

                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });

                if (table.Rows.Count == 0)
                {
                    this._userid = string.Empty;
                    this.Close();
                }
                else if (table.Rows.Count == 1)
                {
                    this._username = Convert.ToString(table.Rows[0]["USER_NAME"]);
                    this.Close();
                }
                this.ugUser.DataSource = table;

            }
            catch (Exception oException)
            {

                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            this._userid = Convert.ToString(this.ugUser.ActiveRow.Cells["USER_ID"].Value);
            this.Close();
        }
    }
}
