﻿namespace WaterNet.Water_Net.FormPopup
{
    partial class frmDatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.lblDataSource = new System.Windows.Forms.Label();
            this.txtDataSource = new System.Windows.Forms.TextBox();
            this.txtDataBase = new System.Windows.Forms.TextBox();
            this.lblUserID = new System.Windows.Forms.Label();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtPw = new System.Windows.Forms.TextBox();
            this.lblPw = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Image = global::WaterNet.Water_Net.Properties.Resources.Save;
            this.btnSave.Location = new System.Drawing.Point(121, 220);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 26);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "저장";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnTest
            // 
            this.btnTest.Image = global::WaterNet.Water_Net.Properties.Resources.Refresh;
            this.btnTest.Location = new System.Drawing.Point(45, 220);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(70, 26);
            this.btnTest.TabIndex = 5;
            this.btnTest.Text = "테스트";
            this.btnTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // lblDataSource
            // 
            this.lblDataSource.AutoSize = true;
            this.lblDataSource.BackColor = System.Drawing.Color.Transparent;
            this.lblDataSource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataSource.Location = new System.Drawing.Point(21, 121);
            this.lblDataSource.Name = "lblDataSource";
            this.lblDataSource.Size = new System.Drawing.Size(90, 14);
            this.lblDataSource.TabIndex = 501;
            this.lblDataSource.Text = "Data Source :";
            this.lblDataSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDataSource
            // 
            this.txtDataSource.Location = new System.Drawing.Point(113, 118);
            this.txtDataSource.MaxLength = 16;
            this.txtDataSource.Name = "txtDataSource";
            this.txtDataSource.Size = new System.Drawing.Size(198, 22);
            this.txtDataSource.TabIndex = 1;
            this.txtDataSource.Text = "Database SID";
            this.txtDataSource.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtDataSource_MouseDown);
            this.txtDataSource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDataSource_KeyPreass);
            // 
            // txtDataBase
            // 
            this.txtDataBase.Location = new System.Drawing.Point(113, 94);
            this.txtDataBase.MaxLength = 16;
            this.txtDataBase.Name = "txtDataBase";
            this.txtDataBase.Size = new System.Drawing.Size(198, 22);
            this.txtDataBase.TabIndex = 0;
            this.txtDataBase.Text = "Database IP Address";
            this.txtDataBase.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtDataBase_MouseDown);
            this.txtDataBase.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDataBase_KeyPreass);
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.BackColor = System.Drawing.Color.Transparent;
            this.lblUserID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserID.Location = new System.Drawing.Point(52, 173);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(59, 14);
            this.lblUserID.TabIndex = 502;
            this.lblUserID.Text = "User ID :";
            this.lblUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.BackColor = System.Drawing.Color.Transparent;
            this.lblDatabase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabase.Location = new System.Drawing.Point(6, 97);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(105, 14);
            this.lblDatabase.TabIndex = 504;
            this.lblDatabase.Text = "Database Host :";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(113, 170);
            this.txtUserID.MaxLength = 16;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(198, 22);
            this.txtUserID.TabIndex = 3;
            this.txtUserID.Text = "Database ID";
            this.txtUserID.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtUserID_MouseDown);
            this.txtUserID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserID_KeyPreass);
            // 
            // txtPw
            // 
            this.txtPw.Location = new System.Drawing.Point(113, 194);
            this.txtPw.MaxLength = 16;
            this.txtPw.Name = "txtPw";
            this.txtPw.PasswordChar = '*';
            this.txtPw.Size = new System.Drawing.Size(198, 22);
            this.txtPw.TabIndex = 4;
            this.txtPw.Text = "Database Password";
            this.txtPw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtPw_MouseDown);
            this.txtPw.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPw_KeyPreass);
            // 
            // lblPw
            // 
            this.lblPw.AutoSize = true;
            this.lblPw.BackColor = System.Drawing.Color.Transparent;
            this.lblPw.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPw.Location = new System.Drawing.Point(37, 197);
            this.lblPw.Name = "lblPw";
            this.lblPw.Size = new System.Drawing.Size(74, 14);
            this.lblPw.TabIndex = 503;
            this.lblPw.Text = "Password :";
            this.lblPw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnExit
            // 
            this.btnExit.Image = global::WaterNet.Water_Net.Properties.Resources.Close2;
            this.btnExit.Location = new System.Drawing.Point(197, 220);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(70, 26);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "닫기";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = global::WaterNet.Water_Net.Properties.Resources.LogOn;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(320, 89);
            this.pictureBox1.TabIndex = 518;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(63, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 14);
            this.label1.TabIndex = 520;
            this.label1.Text = "PORT :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(113, 144);
            this.txtPort.MaxLength = 16;
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(198, 22);
            this.txtPort.TabIndex = 2;
            this.txtPort.Text = "Database Port";
            // 
            // frmDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 251);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.lblDataSource);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtDataSource);
            this.Controls.Add(this.txtDataBase);
            this.Controls.Add(this.lblUserID);
            this.Controls.Add(this.lblPw);
            this.Controls.Add(this.lblDatabase);
            this.Controls.Add(this.txtPw);
            this.Controls.Add(this.txtUserID);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDatabase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "water-NET 데이터베이스 연결 정보";
            this.Load += new System.EventHandler(this.frmDatabase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label lblDataSource;
        private System.Windows.Forms.TextBox txtDataSource;
        private System.Windows.Forms.TextBox txtDataBase;
        private System.Windows.Forms.Label lblUserID;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.TextBox txtPw;
        private System.Windows.Forms.Label lblPw;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPort;

    }
}