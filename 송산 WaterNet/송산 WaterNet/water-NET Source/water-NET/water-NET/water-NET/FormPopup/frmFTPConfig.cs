﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;


namespace WaterNet.Water_Net.FormPopup
{
    public partial class frmFTPConfig : Form
    {
        public frmFTPConfig()
        {
            InitializeComponent();
        }

        private void frmFTPConfig_Load(object sender, EventArgs e)
        {
            this.GetEnvTcpData();
        }

        #region Button Events

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsDataValid(0) != true) return;
            if (this.IsDataValid(1) != true) return;
            if (this.IsDataValid(2) != true) return;
            if (this.IsDataValid(3) != true) return;

            this.DeleteEnvTcpInfor();

            this.SaveEnvTcpInfor();

            MessageBox.Show("FTP 환경정보를 저장했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion


        #region Control Events

        private void txtIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자 && Back Space && \r (Return) && .
            if ((Char.IsDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13) && (e.KeyChar != 46))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtPort.Focus();
            }
        }

        private void txtPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자 && Back Space && \r (Return)
            if ((Char.IsDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtUID.Focus();
            }
        }

        private void txtUID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtUPWD.Focus();
            }
        }

        private void txtUPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtPath.Focus();
            }
        }

        private void txtPath_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.btnSave.Focus();
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// ENV_TCP Data를 Select해서 그리드에 Set
        /// </summary>
        private void GetEnvTcpData()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   IP_ADDR, FTP_PORT, FTP_UID, FTP_PWD, FTP_PATH ");
            oStringBuilder.AppendLine("FROM     CM_ENV_TCP");
            oStringBuilder.AppendLine("WHERE    ROWNUM = 1");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_ENV_TCP");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.txtIP.Text = oDRow["IP_ADDR"].ToString().Trim();
                    this.txtPort.Text = oDRow["FTP_PORT"].ToString().Trim();
                    this.txtUID.Text = oDRow["FTP_UID"].ToString().Trim();
                    this.txtUPWD.Text = EMFrame.utils.ConvertUtils.DecryptKey(oDRow["FTP_PWD"].ToString().Trim());
                    this.txtPath.Text = oDRow["FTP_PATH"].ToString().Trim();
                }
            }

            pDS.Dispose();
            oDBManager.Close();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// ENV_TCP Data의 적합성 검증
        /// </summary>
        /// <param name="iType">0 : IP, 1 : Port, 2 : User ID</param>
        /// 3 : User Password</param>
        /// <returns></returns>
        private bool IsDataValid(int iType)
        {
            bool blRtn = true;

            switch (iType)
            {
                case 0:
                    if (this.txtIP.Text.Trim() == "")
                    {
                        blRtn = false;
                        this.txtIP.Text = "";
                        this.txtIP.Focus();
                        MessageBox.Show("주소를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 1:
                    if (this.txtPort.Text.Trim() == "")
                    {
                        blRtn = false;
                        this.txtPort.Text = "";
                        this.txtPort.Focus();
                        MessageBox.Show("포트를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:
                    if (this.txtUID.Text.Trim().Length < 8 || this.txtUID.Text.Trim().Length > 12)
                    {
                        blRtn = false;
                        this.txtUID.Text = "";
                        this.txtUID.Focus();
                        MessageBox.Show("사용자 ID를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:
                    if ((this.txtUPWD.Text.Trim().Length < 8) || (this.txtUPWD.Text.Trim().Length > 12))
                    {
                        blRtn = false;
                        this.txtUPWD.Text = "";
                        this.txtUPWD.Focus();
                        MessageBox.Show("비밀번호을 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
            return blRtn;
        }

        /// <summary>
        /// ENV_TCP를 Delete한다.
        /// </summary>
        private void DeleteEnvTcpInfor()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   CM_ENV_TCP");

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        /// <summary>
        /// ENV_TCP를 Save한다.
        /// </summary>
        private void SaveEnvTcpInfor()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();
            string strEncUPWD = string.Empty;
            string strURight = string.Empty;
            string strParam = string.Empty;

            DataSet pDS = new DataSet();

            strEncUPWD = EMFrame.utils.ConvertUtils.EncryptKey(this.txtUPWD.Text.Trim());

            strParam = "('" + this.txtIP.Text.Trim() + "', '" + this.txtPort.Text.Trim() + "', '" + this.txtUID.Text.Trim() + "', '" + strEncUPWD + "', '" + this.txtPath.Text.Trim() + "', '" + AppStatic.USER_ID + "', TO_CHAR(SYSDATE, 'YYYYMMDD'))";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    CM_ENV_TCP");
            oStringBuilder.AppendLine("(IP_ADDR, FTP_PORT, FTP_UID, FTP_PWD, FTP_PATH, REG_UID, REG_DATE)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        #endregion

    }
}
