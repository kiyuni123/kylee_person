﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

namespace WaterNet.Water_Net.FormPopup
{
    public partial class frmCodeManager : Form
    {
        public frmCodeManager()
        {
            InitializeComponent();

            this.InitializeGridSetting();

            this.SetCombo_CodeGroup(this.cboCDMAS2);
        }
        

        private void frmCodeManager_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.FormClose();
        }

        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (uTabCode.ActiveTab.Index)
                {
                    case 0: //코드그룹
                        this.GetCodeGroup();
                        break;
                    case 1: //코드
                        this.GetCode();
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValidationSave() == true)
            {
                switch (uTabCode.ActiveTab.Index)
                {
                    case 0: //코드그룹
                        this.SaveCodeGroup();
                        this.SetCombo_CodeGroup(this.cboCDMAS2);
                        break;
                    case 1: //코드
                        this.SaveCode();
                        break;
                }
                this.GetCodeGroup();
                this.GetCode();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string strPCODE = string.Empty;
            string strCODE = string.Empty;

            if (this.IsValidationDelete() == true)
            {
                switch (uTabCode.ActiveTab.Index)
                {
                    case 0: //코드그룹
                        if (this.uGrid1.Rows.Count <= 0) return;
                        strPCODE = this.uGrid1.ActiveRow.Cells[0].Text;
                        this.DeleteCodeGroup(strPCODE);
                        this.DeleteCode(strPCODE, strCODE);
                        this.SetCombo_CodeGroup(this.cboCDMAS2);
                        break;
                    case 1: //코드
                        if (this.uGrid2.Rows.Count <= 0) return;
                        strPCODE = this.SplitToCode(this.uGrid2.ActiveRow.Cells[0].Text);
                        strCODE = this.uGrid2.ActiveRow.Cells[1].Text;
                        this.DeleteCode(strPCODE, strCODE);
                        break;
                }

                this.GetCodeGroup();
                this.GetCode();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.FormClose();
        }

        #endregion


        #region Control Events

        private void uGrid1_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (this.uGrid1.Rows.Count <= 0) return;
            this.txtPCODE.Text = this.uGrid1.ActiveRow.Cells[0].Text;
            this.txtPCODE_NAME.Text = this.uGrid1.ActiveRow.Cells[1].Text;
            this.txtREMARK.Text = this.uGrid1.ActiveRow.Cells[2].Text;
        }

        private void uGrid2_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (this.uGrid2.Rows.Count <= 0) return;
            this.txtPCODE2.Text = this.SplitToCode(this.uGrid2.ActiveRow.Cells[0].Text);
            this.txtCODE.Text = this.uGrid2.ActiveRow.Cells[1].Text;
            this.txtCODE_NAME.Text = this.uGrid2.ActiveRow.Cells[2].Text;
            this.txtREMARK2.Text = this.uGrid2.ActiveRow.Cells[3].Text;
        }

        #endregion


        #region User Function

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeGridSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region CODE MASTER

            oUltraGridColumn = this.uGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PCODE";
            oUltraGridColumn.Header.Caption = "그룹 코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PCODE_NAME";
            oUltraGridColumn.Header.Caption = "그룹 명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 200;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGrid1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "비고";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 400;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGrid1);

            #endregion

            #region CODE

            oUltraGridColumn = this.uGrid2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PCODE";
            oUltraGridColumn.Header.Caption = "그룹";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGrid2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CODE";
            oUltraGridColumn.Header.Caption = "코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 200;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGrid2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CODE_NAME";
            oUltraGridColumn.Header.Caption = "코드";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 200;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGrid2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "비고";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 400;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGrid2);

            #endregion
        }

        /// <summary>
        /// CM_CODE_MASTER를 조회한다.
        /// </summary>
        private void GetCodeGroup()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);

            oStringBuilder.AppendLine("SELECT   PCODE, PCODE_NAME, REMARK");
            oStringBuilder.AppendLine("FROM     CM_CODE_MASTER");
            oStringBuilder.AppendLine("ORDER BY PCODE");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PCODE");

            this.uGrid1.DataSource = pDS.Tables["PCODE"].DefaultView;

            FormManager.SetGridStyle_PerformAutoResize(this.uGrid1);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// CM_CODE를 조회한다.
        /// </summary>
        private void GetCode()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            string strPCODE = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            if (this.cboCDMAS2.Text.Trim() != "전체")
            {
                strPCODE = this.SplitToCode(this.cboCDMAS2.Text.Trim());
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);

            oStringBuilder.AppendLine("SELECT   B.PCODE_NAME || ' (' || B.PCODE || ')' AS PCODE, A.CODE, A.CODE_NAME, A.REMARK");
            oStringBuilder.AppendLine("FROM     CM_CODE A, CM_CODE_MASTER B");
            oStringBuilder.AppendLine("WHERE    B.PCODE = A.PCODE");

            if (strPCODE != string.Empty)
            {
                oStringBuilder.AppendLine("         AND A.PCODE = '" + strPCODE+ "'");
            }
            oStringBuilder.AppendLine("ORDER BY B.PCODE, A.CODE");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_CODE");

            this.uGrid2.DataSource = pDS.Tables["CM_CODE"].DefaultView;

            FormManager.SetGridStyle_PerformAutoResize(this.uGrid2);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 등록 시 필수 정보 체크
        /// </summary>
        /// <returns>true : 정상, false : 비정상</returns>
        private bool IsValidationSave()
        {
            bool blRtn = true;

            switch (uTabCode.ActiveTab.Index)
            {
                case 0: //코드그룹
                    if (this.txtPCODE.Text.Trim() == "") blRtn = false;
                    if (this.txtPCODE_NAME.Text.Trim() == "") blRtn = false;
                    break;
                case 1: //코드
                    if (this.txtPCODE2.Text.Trim() == "") blRtn = false;
                    if (this.txtCODE.Text.Trim() == "") blRtn = false;
                    if (this.txtCODE_NAME.Text.Trim() == "") blRtn = false;
                    break;
            }

            if (blRtn == false)
            {
                MessageBox.Show("등록할 코드 그룹 또는 코드의 정보를 확인하세요.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return blRtn;
        }

        /// <summary>
        /// 삭제 시 필수 정보 체크
        /// </summary>
        /// <returns>true : 정상, false : 비정상</returns>
        private bool IsValidationDelete()
        {
            bool blRtn = true;

            switch (uTabCode.ActiveTab.Index)
            {
                case 0: //코드그룹
                    if (this.txtPCODE.Text.Trim() == "") blRtn = false;
                    break;
                case 1: //코드
                    if (this.txtPCODE2.Text.Trim() == "") blRtn = false;
                    if (this.txtCODE.Text.Trim() == "") blRtn = false;
                    break;
            }

            if (blRtn == false)
            {
                MessageBox.Show("삭제할 코드 그룹 또는 코드의 정보를 확인하세요.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return blRtn;
        }

        /// <summary>
        /// CM_CODE_MASTER에 PCODE를 저장한다.
        /// </summary>
        private void SaveCodeGroup()
        {
            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            string strPCODE = string.Empty;
            string strPCODE_NAME = string.Empty;
            string strREMARK = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            strPCODE = this.txtPCODE.Text;
            strPCODE_NAME = this.txtPCODE_NAME.Text;
            strREMARK = this.txtREMARK.Text;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO  CM_CODE_MASTER");
            oStringBuilder.AppendLine("(PCODE, PCODE_NAME, REMARK)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("('" + strPCODE + "', '" + strPCODE_NAME + "', '" + strREMARK + "')");

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            oDBManager.Close();
        }

        /// <summary>
        /// CM_CODE에 CODE를 저장한다.
        /// </summary>
        private void SaveCode()
        {
            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            string strPCODE = string.Empty;
            string strCODE = string.Empty;
            string strCODE_NAME = string.Empty;
            string strREMARK = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            strPCODE = this.txtPCODE2.Text;
            strCODE = this.txtCODE.Text;
            strCODE_NAME = this.txtCODE_NAME.Text;
            strREMARK = this.txtREMARK2.Text;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO  CM_CODE");
            oStringBuilder.AppendLine("(PCODE, CODE, CODE_NAME, REMARK)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("('" + strPCODE + "', '" + strCODE + "', '" + strCODE_NAME + "', '" + strREMARK + "')");

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
            oDBManager.Close();
        }

        /// <summary>
        /// CM_CODE_MASTER에서 코드 그룹을 삭제
        /// </summary>
        /// <param name="strPCODE"></param>
        private void DeleteCodeGroup(string strPCODE)
        {
            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE");
            oStringBuilder.AppendLine("FROM     CM_CODE_MASTER");
            oStringBuilder.AppendLine("WHERE    PCODE = '" + strPCODE + "'");

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        /// <summary>
        /// CM_CODE에서 코드를 삭제
        /// </summary>
        /// <param name="strPCODE"></param>
        /// <param name="strCODE"></param>
        private void DeleteCode(string strPCODE, string strCODE)
        {
            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE");
            oStringBuilder.AppendLine("FROM     CM_CODE");

            if (strCODE != string.Empty)
            {
                oStringBuilder.AppendLine("WHERE    PCODE = '" + strPCODE + "' AND CODE = '" + strCODE + "'");
            }
            else
            {
                oStringBuilder.AppendLine("WHERE    PCODE = '" + strPCODE + "'");
            }

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        /// <summary>
        /// 코드명 (코드) 타입의 스트링에서 코드를 만환한다.
        /// </summary>
        /// <param name="strOrgData"></param>
        /// <returns></returns>
        private string SplitToCode(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[1].Substring(0, strArr[1].Length - 1);
            }
            else
            {
                if (strArr[0].Trim() == "전체")
                {
                    strCode = " ";
                }
                else
                {
                    strCode = strArr[0].Trim();
                }
            }

            return strCode;
        }

        /// <summary>
        /// Code Master를 Combo에 Set
        /// </summary>
        /// <param name="oCombo"></param>
        private void SetCombo_CodeGroup(ComboBox oCombo)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   PCODE_NAME || ' (' || PCODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_CODE_MASTER");
            oStringBuilder.AppendLine("ORDER BY PCODE");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_CODE_MASTER");

            oCombo.Items.Clear();
            oCombo.Items.Add("전체");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }

            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            
            pDS.Dispose();

            oDBManager.Close();
        }

        /// <summary>
        /// 현재 폼을 닫는다.
        /// </summary>
        private void FormClose()
        {
            this.Dispose();
            this.Close();
        }

        #endregion

    }
}
