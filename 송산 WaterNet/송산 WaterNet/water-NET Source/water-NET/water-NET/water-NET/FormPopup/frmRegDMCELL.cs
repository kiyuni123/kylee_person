﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;
using WaterNet.WaterAOCore;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

#endregion

namespace WaterNet.Water_Net.FormPopup
{
    public partial class frmRegDMCELL : Form
    {
        // 확장명 XLS (Excel 97~2003 용)
        private const string ConnectStrFrm_Excel97_2003 =
            "Provider=Microsoft.Jet.OLEDB.4.0;" +
            "Data Source=\"{0}\";" +
            "Mode=ReadWrite|Share Deny None;" +
            "Extended Properties='Excel 8.0; HDR={1}; IMEX={2}';" +
            "Persist Security Info=False";

        // 확장명 XLSX (Excel 2007 이상용)
        private const string ConnectStrFrm_Excel =
            "Provider=Microsoft.ACE.OLEDB.12.0;" +
            "Data Source=\"{0}\";" +
            "Mode=ReadWrite|Share Deny None;" +
            "Extended Properties='Excel 12.0; HDR={1}; IMEX={2}';" +
            "Persist Security Info=False";
        
        public frmRegDMCELL()
        {
            InitializeComponent();
            
            this.InitializeGird();

            Load += new EventHandler(frmRegDMCELL_Load);    //동진 수정_2012.6.08
        }

        //=========================================================
        //
        //                    동진 수정_2012.6.08
        //			권한박탈(조회만 가능)
        //=========================================================

        private void frmRegDMCELL_Load(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["수용가핸드폰번호등록ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnUpdate.Enabled = false;
            }
        }

        //===========================================================================

        /// <summary>
        /// Grid를 초기화 한다. Column 설정 등...
        /// </summary>
        private void InitializeGird()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = this.uGrid.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGrid.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMCELL";
            oUltraGridColumn.Header.Caption = "핸드폰 번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGrid);

            #region - Grid Caption Visible

            this.uGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            #endregion

        }

        #region Button Events
        
        private void btnOpen_Click(object sender, EventArgs e)
        {
            int iFileType = -1;

            OpenFileDialog oOFD = new OpenFileDialog();

            oOFD.Filter = "엑셀 파일|*.xls;*.xlsx";
            oOFD.Multiselect = false;
            oOFD.FilterIndex = 1;
            oOFD.RestoreDirectory = true;

            if (oOFD.ShowDialog() == DialogResult.OK)
            {
                iFileType = this.ExcelFileType(oOFD.FileName);
                if (iFileType == 0 || iFileType == 1)
                {
                    DataSet oDS = null;

                    oDS = this.OpenExcel(oOFD.FileName, true);

                    DataTable oDT = oDS.Tables[0];

                    ColumnsCollection oColumns = this.uGrid.DisplayLayout.Bands[0].Columns;

                    foreach (UltraGridColumn oColumn in oColumns)
                    {
                        if (oColumn.Index < oDT.Columns.Count)
                        {
                            oDT.Columns[oColumn.Index].ColumnName = oColumn.Key;
                        }
                    }
                    this.uGrid.DataSource = oDT;
                }
            }

            if (this.uGrid.Rows.Count > 0)
            {
                this.btnUpdate.Enabled = true;
            }
            else
            {
                this.btnUpdate.Enabled = false;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.uGrid.Rows.Count <= 0) return;

                this.UpdateCellNo();

                MessageBox.Show("수용가 핸드폰 번호를 정상적으로 등록했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Dispose();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }

        #endregion

        #region Excel Function

        /// <summary>
        ///    Excel 파일의 형태를 반환한다.
        ///    -2 : Error  
        ///    -1 : 엑셀파일아님
        ///     0 : 97-2003 엑셀 파일 (xls)
        ///     1 : 2007 이상 파일 (xlsx)
        /// </summary>
        /// <param name="XlsFile">
        ///    Excel File 명 전체 경로입니다.
        /// </param>
        private int ExcelFileType(string XlsFile)
        {
            byte[,] ExcelHeader = {
                { 0xD0, 0xCF, 0x11, 0xE0, 0xA1 }, // XLS  File Header
                { 0x50, 0x4B, 0x03, 0x04, 0x14 }  // XLSX File Header
            };

            // result -2=error, -1=not excel , 0=xls , 1=xlsx
            int result = -1;

            FileInfo FI = new FileInfo(XlsFile);
            FileStream FS = FI.Open(FileMode.Open);

            try
            {
                byte[] FH = new byte[5];

                FS.Read(FH, 0, 5);

                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (FH[j] != ExcelHeader[i, j]) break;
                        else if (j == 4) result = i;
                    }
                    if (result >= 0) break;
                }
            }
            catch
            {
                result = (-2);
                //throw e;
            }
            finally
            {
                FS.Close();
            }
            return result;
        }

        /// <summary>
        ///    Excel 파일을 DataSet 으로 변환하여 반환한다.
        /// </summary>
        /// <param name="FileName">
        ///    Excel File 명 PullPath
        /// </param>
        /// <param name="UseHeader">
        ///    첫번째 줄을 Field 명으로 사용할 것이지 여부
        /// </param>
        private DataSet OpenExcel(string FileName, bool UseHeader)
        {
            DataSet DS = null;

            string[] HDROpt = { "NO", "YES" };
            string HDR = "";
            string ConnStr = "";

            if (UseHeader)
                HDR = HDROpt[1];
            else
                HDR = HDROpt[0];

            int ExcelType = ExcelFileType(FileName);

            switch (ExcelType)
            {
                case (-2): throw new Exception(FileName + "의 형식검사중 오류가 발생하였습니다.");
                case (-1): throw new Exception(FileName + "은 엑셀 파일형식이 아닙니다.");
                case (0):
                    ConnStr = string.Format(ConnectStrFrm_Excel97_2003, FileName, HDR, "1");
                    break;
                case (1):
                    ConnStr = string.Format(ConnectStrFrm_Excel, FileName, HDR, "1");
                    break;
            }

            OleDbConnection OleDBConn = null;
            OleDbDataAdapter OleDBAdap = null;
            DataTable Schema;

            try
            {
                OleDBConn = new OleDbConnection(ConnStr);
                OleDBConn.Open();

                Schema = OleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                DS = new DataSet();

                foreach (DataRow DR in Schema.Rows)
                {
                    OleDBAdap = new OleDbDataAdapter(DR["TABLE_NAME"].ToString(), OleDBConn);

                    OleDBAdap.SelectCommand.CommandType = CommandType.TableDirect;
                    OleDBAdap.AcceptChangesDuringFill = false;

                    string TableName = DR["TABLE_NAME"].ToString().Replace("$", String.Empty).Replace("'", String.Empty);

                    if (DR["TABLE_NAME"].ToString().Contains("$")) OleDBAdap.Fill(DS, TableName);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (OleDBConn != null) OleDBConn.Close();
            }
            return DS;
        }

        #endregion

        #region SQL Funtion

        /// <summary>
        /// 수용가 별로 Cell No를 Update한다.
        /// </summary>
        private void UpdateCellNo()
        {
            string strDMNO = string.Empty;
            string strDMCELL = string.Empty;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager != null)
            {
                StringBuilder oStringBuilder = new StringBuilder();
                
                for (int i = 0; i < this.uGrid.Rows.Count; i++)
                {
                    strDMNO = this.uGrid.Rows[i].Cells[0].Text.Trim();
                    strDMCELL = this.uGrid.Rows[i].Cells[1].Text.Trim();

                    if (strDMNO != "" && strDMCELL != "")
                    {
                        oStringBuilder.Remove(0, oStringBuilder.Length);
                        oStringBuilder.AppendLine("UPDATE   WI_DMINFO");
                        oStringBuilder.AppendLine("SET      DMCELL = '" + strDMCELL + "'");
                        oStringBuilder.AppendLine("WHERE    DMNO = '" + strDMNO + "'");

                        oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
                    }
                }
            }

            oDBManager.Close();
        }

        #endregion

    }
}
