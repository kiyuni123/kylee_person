﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WaterNetCore;
using EMFrame.log;


namespace WaterNet.Water_Net.FormPopup
{
    public partial class frmUserManager : Form
    {
        public frmUserManager()
        {
            InitializeComponent();
            
            this.InitializeGridSetting();
        }

        private void frmUserManager_Load(object sender, EventArgs e)
        {
            this.GetUserData();
        }


        #region Button Events

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.IsDataValid(0) != true) return;
                if (this.IsDataValid(1) != true) return;
                if (this.IsDataValid(2) != true) return;
                if (this.IsDataValid(3) != true) return;
                if (this.IsDataValid(4) != true) return;

                this.DeleteUser();

                this.SaveUser();

                MessageBox.Show("입력한 사용자를 저장했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.GetUserData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }
        
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.ugData.Rows.Count <= 0) return;

            DialogResult oDRtn = MessageBox.Show("선택한 사용자(" + this.ugData.ActiveRow.Cells[0].Text + ")를 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oDRtn == DialogResult.Yes)
            {
                this.DeleteUser();

                MessageBox.Show("선택한 사용자를 삭제했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            this.GetUserData();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion


        #region Control Events

        private void txtUID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtPWD.Focus();
            }
        }

        private void txtPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtNCPWD.Focus();
            }
        }

        private void txtNCPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtUName.Focus();
            }
        }

        private void txtUName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtUDivision.Focus();
            }
        }

        private void txtUDivision_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.rdoOperator.Focus();
            }
        }

        private void ugData_Click(object sender, EventArgs e)
        {
            if (this.ugData.Rows.Count <= 0) return;

            this.txtUID.Text = this.ugData.ActiveRow.Cells[0].Text;
            this.txtPWD.Text = "";
            this.txtNCPWD.Text = "";
            this.txtUName.Text = "";
            this.txtUDivision.Text = "";
        }

        #endregion

        
        #region User Function

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USER_ID";
            oUltraGridColumn.Header.Caption = "사용자 ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USER_NAME";
            oUltraGridColumn.Header.Caption = "성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USER_DIVISION";
            oUltraGridColumn.Header.Caption = "부서";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "URIGHT";
            oUltraGridColumn.Header.Caption = "권한";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.ugData);

            this.ugData.DisplayLayout.CaptionVisible = DefaultableBoolean.True;
        }

        /// <summary>
        /// User Data를 Select해서 그리드에 Set
        /// </summary>
        private void GetUserData()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   USER_ID, USER_NAME, USER_DIVISION, DECODE(USER_RIGHT, '1', '관리자', '운영자') AS URIGHT ");
            oStringBuilder.AppendLine("FROM     CM_USER");
            oStringBuilder.AppendLine("WHERE    USE_YN = 'Y'");
            oStringBuilder.AppendLine("ORDER BY USER_RIGHT, USER_NAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_USER");

            this.ugData.DataSource = pDS.Tables["CM_USER"].DefaultView;

            if (this.ugData.Rows.Count > 0)
            {
                this.ugData.Rows[0].Activated = true;
                this.txtUID.Text = this.ugData.ActiveRow.Cells[0].Text;
                this.txtPWD.Text = "";
                this.txtNCPWD.Text = "";
                this.txtUName.Text = "";
                this.txtUDivision.Text = "";
            }

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.ugData);

            oDBManager.Close();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// User Data의 적합성 검증
        /// </summary>
        /// <param name="iType">0 : User ID, 1 : New Password, 2 : New Confirm Password</param>
        /// 3 : User Name, 4 : User Division</param>
        /// <returns></returns>
        private bool IsDataValid(int iType)
        {
            bool blRtn = true;

            switch (iType)
            {
                case 0:
                    if ((this.txtUID.Text.Trim().Length < 8) || (this.txtUID.Text.Trim().Length > 12))
                    {
                        blRtn = false;
                        this.txtUID.Text = "";
                        this.txtUID.Focus();
                        MessageBox.Show("사용자 ID를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    if (this.IsUserIDExists() == true)
                    {
                        blRtn = false;
                        this.txtUID.Text = "";
                        this.txtUID.Focus();
                        MessageBox.Show("이미 사용중인 사용자 ID입니다. 다른 사용자 ID를 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 1:
                    if ((this.txtPWD.Text.Trim() == this.txtUID.Text.Trim()) || (this.txtPWD.Text.Trim().Length < 8) || (this.txtPWD.Text.Trim().Length > 12))
                    {
                        blRtn = false;
                        this.txtPWD.Text = "";
                        this.txtPWD.Focus();
                        MessageBox.Show("등록할 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:
                    if ((this.txtNCPWD.Text.Trim() == this.txtUID.Text.Trim()) || (this.txtNCPWD.Text.Trim() != this.txtPWD.Text.Trim()) || (this.txtNCPWD.Text.Trim().Length < 8) || (this.txtNCPWD.Text.Trim().Length > 12))
                    {
                        blRtn = false;
                        this.txtNCPWD.Text = "";
                        this.txtNCPWD.Focus();
                        MessageBox.Show("확인 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:
                    if ((this.txtUName.Text.Trim().Length <= 0) || (this.txtUName.Text.Trim().Length > 32))
                    {
                        blRtn = false;
                        this.txtUName.Text = "";
                        this.txtUName.Focus();
                        MessageBox.Show("사용자 성명을 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 4:
                    if ((this.txtUDivision.Text.Trim().Length <= 0) || (this.txtUDivision.Text.Trim().Length > 32))
                    {
                        blRtn = false;
                        this.txtUDivision.Text = "";
                        this.txtUDivision.Focus();
                        MessageBox.Show("사용자 부서를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
            return blRtn;
        }

        /// <summary>
        /// 입력한 User ID의 사용여부
        /// </summary>
        /// <returns></returns>
        private bool IsUserIDExists()
        {
            bool blRtn = false;

            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager != null)
            {
                StringBuilder oStringBuilder = new StringBuilder();

                DataSet pDS = new DataSet();

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   USER_ID");
                oStringBuilder.AppendLine("FROM     CM_USER");
                oStringBuilder.AppendLine("WHERE    USER_ID = '" + this.txtUID.Text.Trim() + "'");

                pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_USER");

                if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
                {
                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        blRtn = true;
                    }
                }
                else
                {
                    blRtn = false;
                }
            }
            else
            {
                blRtn = false;
            }

            oDBManager.Close();

            return blRtn;
        }

        /// <summary>
        /// 사용자를 Delete한다.
        /// </summary>
        private void DeleteUser()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   CM_USER");
            oStringBuilder.AppendLine("WHERE    USER_ID = '" + this.txtUID.Text.Trim() + "'");
 
            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        /// <summary>
        /// 사용자를 Save한다.
        /// </summary>
        private void SaveUser()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();
            string strEncUPWD = string.Empty;
            string strURight = string.Empty;
            string strParam = string.Empty;
            
            DataSet pDS = new DataSet();

            strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(this.txtUID.Text.Trim() + this.txtPWD.Text.Trim());

            if (this.rdoManager.Checked == true)
            {
                strURight = "1";
            }
            else
            {
                strURight = "2";
            }

            strParam = "('" + this.txtUID.Text.Trim() + "', '" + this.txtUName.Text.Trim() + "', '" + strEncUPWD + "', '" + this.txtUDivision.Text.Trim() + "', '" + strURight + "', '" + EMFrame.statics.AppStatic.USER_ID + "', TO_CHAR(SYSDATE, 'YYYYMMDD'), 'Y')";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    CM_USER");
            oStringBuilder.AppendLine("(USER_ID, USER_NAME, USER_PWD, USER_DIVISION, USER_RIGHT, REG_UID, REG_DATE, USE_YN)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        #endregion

    }
}
