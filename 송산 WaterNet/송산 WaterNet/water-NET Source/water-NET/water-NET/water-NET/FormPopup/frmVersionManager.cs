﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WaterNetCore;

namespace WaterNet.Water_Net.FormPopup
{
    public partial class frmVersionManager : Form
    {
        #region Socket Client 및 File 관련 정의

        //FTP Information
        private struct STRC_TCP_ENV_COMMON
        {
            public string FTP_IP;
            public string FTP_PORT;
            public string FTP_UID;
            public string FTP_PWD;
            public string FTP_PATH;

        }
        private STRC_TCP_ENV_COMMON m_STRC_TCP_ENV_COMMON;

        private Socket m_FTP;                 // FTP용 Socket

        const int TRANSFER_BUFFER_SIZE = 1024;
        const int READ_BUFFER_SIZE = 512;
        const int TRANSFER_ASCII = 0;
        const int TRANSFER_BINARY = 1;

        const int FILE_TYPE_LEN = 2;

        private bool m_IsConnected = false;
        private bool m_IsFirstConnect = true;

        #endregion

        public frmVersionManager()
        {
            InitializeComponent();

            this.InitializeGridSetting();
        }

        private void frmVersionManager_Load(object sender, EventArgs e)
        {
            this.GetTCPInfor();

            if (m_STRC_TCP_ENV_COMMON.FTP_IP == null)
            {
                MessageBox.Show("FTP 서버의 정보 취득에 실패했습니다. Water-NET 버전관리를 닫습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }

            this.SetCombo_VersionFileType(this.cboFileType);

            this.GetVersionData();
        }

        private void frmVersionManager_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_IsConnected == true)
            {
                this.FTP_Disconnect();
            }
        }


        #region Button Events

        private void btnOpen_Click(object sender, EventArgs e)
        {
            this.OpenVersionFile();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            //if (m_IsConnected == true)
            //{
            //    this.FTP_Disconnect();
            //}
            this.FTP_Connect();

            if (m_IsConnected == true)
            {
                MessageBox.Show("FTP 서버에 연결 성공!", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.btnUpload.Enabled = true;
            }
            else
            {
                MessageBox.Show("FTP 서버에 연결 실패! FTP 환경정보를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (this.lstFile.Items.Count == 0)
            {
                this.btnUpload.Enabled = false;
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            this.FTP_Upload();

            this.FTP_Disconnect();

            this.Cursor = System.Windows.Forms.Cursors.Default;

            MessageBox.Show("파일 업로드 완료", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.btnUpload.Enabled = false;

            this.lstFile.Items.Clear();

            this.GetVersionData();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion


        #region Control Events


        #endregion


        #region User Function

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FILE_NAME";
            oUltraGridColumn.Header.Caption = "파일명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FILE_RNAME";
            oUltraGridColumn.Header.Caption = "임시파일명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FILE_TYPE";
            oUltraGridColumn.Header.Caption = "파일 타입";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.ugData);

            this.ugData.DisplayLayout.CaptionVisible = DefaultableBoolean.True;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 버전 파일 타입
        /// 대상 Table : CM_CODE
        /// </summary>
        /// <param name="oCombo"></param>
        private void SetCombo_VersionFileType(ComboBox oCombo)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = null;

            oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();
            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            oStringBuilder.AppendLine("SELECT   CODE_NAME || ' (' || CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_CODE");
            oStringBuilder.AppendLine("WHERE    PCODE = '8001'");
            oStringBuilder.AppendLine("ORDER BY CODE_NAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_CODE");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                    i++;
                }
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = 0;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
            oDBManager.Close();
        }

        /// <summary>
        /// 인자에서 (로 Split하여 (뒤 글자들을 반환
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) XXXX (YYY) 중 YYY을 반환</param>
        /// <returns>string Code</returns>
        private string SplitToCode(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[1].Substring(0, strArr[1].Length - 1);
            }
            else
            {
                strCode = strArr[0].Trim();
            }

            return strCode;
        }

        /// <summary>
        /// FTP 정보를 취득
        /// </summary>
        private void GetTCPInfor()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   IP_ADDR, FTP_PORT, FTP_UID, FTP_PWD, FTP_PATH");
            oStringBuilder.AppendLine("FROM     CM_ENV_TCP");
            oStringBuilder.AppendLine("WHERE    ROWNUM = 1");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_ENV_TCP");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                this.m_STRC_TCP_ENV_COMMON = new STRC_TCP_ENV_COMMON();

                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.m_STRC_TCP_ENV_COMMON.FTP_IP = oDRow["IP_ADDR"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_PORT = oDRow["FTP_PORT"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_UID = oDRow["FTP_UID"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_PWD = EMFrame.utils.ConvertUtils.DecryptKey(oDRow["FTP_PWD"].ToString().Trim());
                    this.m_STRC_TCP_ENV_COMMON.FTP_PATH = oDRow["FTP_PATH"].ToString().Trim();
                }
            }
            
            pDS.Dispose();

            oDBManager.Close();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// Version Data를 Select해서 그리드에 Set
        /// </summary>
        private void GetVersionData()
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   FILE_NAME, FILE_RNAME, FILE_TYPE ");
            oStringBuilder.AppendLine("FROM     CM_VERSION");
            oStringBuilder.AppendLine("ORDER BY FILE_TYPE, FILE_NAME");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_VERSION");

            this.ugData.DataSource = pDS.Tables["CM_VERSION"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.ugData);

            oDBManager.Close();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// OpenFileDialog를 이용해서 업로드할 파일을 ListView에 등록한다.
        /// </summary>
        private void OpenVersionFile()
        {
            string[] arrFullName;
            string strFileType = string.Empty;
            string strFileName = string.Empty;
            string[] arrSplit;
            string strInitPath = string.Empty;

            OpenFileDialog oOFD = new OpenFileDialog();

            strFileType = this.SplitToCode(this.cboFileType.Text);

            oOFD.Filter = "모든 파일 (*.*)|*.*";
            oOFD.Multiselect = true;
            oOFD.FilterIndex = 1;
            oOFD.RestoreDirectory = true;

            switch (strFileType)
            {
                case "C":
                case "D":
                case "S":
                    strInitPath = Application.StartupPath;
                    break;
                case "P":
                    strInitPath = @"C:\Water-NET\WaterNet-Data\Pipegraphic";
                    break;
                case "T":
                    strInitPath = @"C:\Water-NET\WaterNet-Data\Topographic";
                    break;
            }

            oOFD.InitialDirectory = strInitPath;

            this.lstFile.Items.Clear();

            if (oOFD.ShowDialog() == DialogResult.OK)
            {
                arrFullName = oOFD.FileNames;

                for (int i = 0; i < arrFullName.Length; i++)
                {
                    arrSplit = arrFullName[i].Split('\\');

                    strFileName = arrSplit[arrSplit.Length - 1].ToString();

                    ListViewItem oLItem = new ListViewItem(strFileName);

                    strFileName = arrSplit[arrSplit.Length - 1].ToString();
                    oLItem.SubItems.Add(strFileType);
                    oLItem.SubItems.Add(this.GetFileSize(arrFullName[i]));
                    oLItem.SubItems.Add(arrFullName[i]);
                    oLItem.SubItems.Add("준비");
                    this.lstFile.Items.Add(oLItem);
                }
            }
        }

        /// <summary>
        /// File의 Size를 구해 리턴한다.
        /// </summary>
        /// <param name="strSrcFilePath">Size를 구할 File의 Full Path</param>
        /// <returns>File Size</returns>
        private string GetFileSize(string strFullName)
        {
            try
            {
                string strSize = string.Empty;

                string strPath = string.Empty;
                string strFileName = string.Empty;

                string[] arrSplit = strFullName.Split('\\');
                strFileName = arrSplit[arrSplit.Length-1].ToString();
                strPath = strFullName.Substring(0, strFullName.Length - strFileName.Length - 1);
                DirectoryInfo diRootDir = new DirectoryInfo(strPath);
                FileInfo[] arrFileInfor = diRootDir.GetFiles(strFileName);

                foreach (FileInfo arrFileInfo in arrFileInfor)
                {
                    strSize = arrFileInfo.Length.ToString();
                }
                return strSize;
            }
            catch
            {
                return "0";
            }
        }

        /// <summary>
        /// File을 Upload한다.
        /// </summary>
        private void FTP_Upload()
        {
            Byte[] btReadBytes;
            int intRcvSize = 0;
            int intFileSize = 0;
            int intReadBytes = 0;
            int intTotalByte = 0;
            string strCommand = string.Empty;

            this.progressBar.Maximum = lstFile.Items.Count;
            this.progressBar.Value = 0;
            this.progressBar.Refresh();

            //Upload할 FTP Type을 Binary로 설정한다.
            FTP_SetTransferType();

            FTP_ChangeDirectory(m_STRC_TCP_ENV_COMMON.FTP_PATH);

            //Upload할 FTP Server의 Path 생성 
            FTP_CreateDirectory(m_STRC_TCP_ENV_COMMON.FTP_PATH);

            for (int i = 0; i < this.lstFile.Items.Count; i++)
            {
                this.progressBar.Value = 1 + i;
                this.progressBar.Refresh();

                this.lstFile.Items[i].Selected = true;
                this.lstFile.Refresh();

                string strNewFileName = this.GenerationWQSerialNumber("VI");

                //원본파일을 임시파일로 Rename하고 Temp 경로로 Copy해서 임시파일로 업로드
                this.CreateDirectory(Application.StartupPath + @"\Upload\");                
                this.CopyFile(this.lstFile.Items[i].SubItems[3].Text.ToString(), Application.StartupPath + @"\Upload\" + strNewFileName, true);

                //Upload할 기존 서버에 있는 File을 Delete한다.
                FTP_DeleteFile(m_STRC_TCP_ENV_COMMON.FTP_PATH + strNewFileName);

                //Upload할 File 정보를 정리한다.
                FileStream oFileStream = new FileStream(this.lstFile.Items[i].SubItems[3].Text.ToString(), FileMode.Open);

                //Upload용 PASV Socket Object를 생성한다.
                Socket oPasvSoc = OepnAndGetPASVSocket();

                //Store file on remote system over-writing the file if it already exists. 
                strCommand = "stor " + strNewFileName;
                strCommand += "\r\n";
                FTP_SendCommand(strCommand);

                string strRcvCommand = FTP_GetResponseCommand();

                FileInfo oFi = new FileInfo(strNewFileName);
                do
                {
                    intFileSize = Convert.ToInt32(this.lstFile.Items[i].SubItems[2].Text.ToString());
                    if (intTotalByte == intFileSize)
                    {
                        break;
                    }
                    if ((intTotalByte < intFileSize) && (intFileSize < TRANSFER_BUFFER_SIZE))
                    {
                        btReadBytes = new Byte[intFileSize];
                    }
                    else
                    {
                        if (intFileSize < (intTotalByte + TRANSFER_BUFFER_SIZE))
                        {
                            btReadBytes = new Byte[(intFileSize - intTotalByte)];
                        }
                        else
                        {
                            btReadBytes = new Byte[TRANSFER_BUFFER_SIZE];
                        }
                    }
                    intReadBytes = oFileStream.Read(btReadBytes, 0, btReadBytes.Length);
                    if (intReadBytes == 0)
                    {
                        break;
                    }
                    intRcvSize = oPasvSoc.Send(btReadBytes);
                    strRcvCommand = Encoding.ASCII.GetString(btReadBytes, 0, intRcvSize);
                    intTotalByte += intReadBytes;
                }
                while (true);

                oFileStream.Close();    //FileStream Close
                oPasvSoc.Close();       //PASV Socket Close

                if (intTotalByte == intFileSize)
                {
                    string[] arrPath = this.lstFile.Items[i].SubItems[3].Text.ToString().Split('\\');
                    string strFilePath = this.lstFile.Items[i].SubItems[3].Text.Substring(0, this.lstFile.Items[i].SubItems[3].Text.Length - arrPath[arrPath.Length - 1].Length);
                    this.SaveVersionInfor(this.lstFile.Items[i].SubItems[0].Text.ToString(), strNewFileName, this.lstFile.Items[i].SubItems[1].Text.ToString(), intFileSize.ToString(), strFilePath);
                    this.lstFile.Items[i].SubItems[4].Text = "성공";
                    this.lstFile.Items[i].BackColor = Color.White;
                }
                else
                {
                    this.lstFile.Items[i].SubItems[4].Text = "실패";
                    this.lstFile.Items[i].BackColor = Color.Red;
                }

                intRcvSize = 0;
                intFileSize = 0;
                intReadBytes = 0;
                intTotalByte = 0;
            }
        }

        /// <summary>
        /// 업로드 성공한 파일 정보 Save
        /// </summary>
        private void SaveVersionInfor(string strFileName, string strNewFileName, string strFileType, string strFileSize, string strFilePath)
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();
            string strParam = string.Empty;

            DataSet pDS = new DataSet();

            this.DeleteVersionInfor(strFileName);

            strParam = "('" + strFileName + "', '" + strNewFileName  + "', '" + strFileType + "', '" + strFileSize + "', '" + strFilePath + "', '" + AppStatic.USER_ID + "', TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS'))";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    CM_VERSION");
            oStringBuilder.AppendLine("(FILE_NAME, FILE_RNAME, FILE_TYPE, FILE_SIZE, FILE_PATH, REG_UID, REG_DATE)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        /// <summary>
        /// File Name으로 Version 정보 Delete
        /// </summary>
        /// <param name="strFileName"></param>
        private void DeleteVersionInfor(string strFileName)
        {
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            oDBManager.Open();

            if (oDBManager == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   CM_VERSION");
            oStringBuilder.AppendLine("WHERE    FILE_NAME = '" + strFileName + "'");

            oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

            oDBManager.Close();
        }

        /// <summary>
        /// 수질용 일련번호 생성 함수
        /// 기본 길이 15Byte = Key 2Byte + YYYYMMDD 8Byte + 초 2Byte + 밀리초 3Byte
        /// 전체 길이 16Byte = 기본길이 14Byte + 랜덤숫자 1Byte
        /// </summary>
        /// <param name="strKey">일련번호 앞에 들어갈 대표문자 2Byte (영문)</param>
        /// <returns></returns>
        private String GenerationWQSerialNumber(string strKey)
        {
            Random pRandom = new Random();

            string strRtn = string.Empty;
            string strWQSN = string.Empty;

            strWQSN = this.StringToDateTime(DateTime.Now)
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0');
            strWQSN += (pRandom.Next(1, 9).ToString()).PadLeft(1, '0');

            strRtn = strKey + strWQSN;

            return strRtn;
        }

        /// <summary>
        /// DateTime을 받아 YYYYMMDD Format의 String로 변환해 반환한다.
        /// </summary>
        /// <param name="oDateTime">DateTime Object</param>
        /// <returns>string YYYYMMDD</returns>
        private string StringToDateTime(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;
        }

        /// <summary>
        /// Directory를 생성한다.
        /// </summary>
        /// <param name="strDirPath">생성할 Directory의 Full Path</param>
        private void CreateDirectory(string strDirPath)
        {
            Directory.CreateDirectory(strDirPath);
        }

        /// <summary>
        /// File를 Copy 한다.
        /// </summary>
        /// <param name="strSrcFilePath">Copy할 Source File Path</param>
        /// <param name="strDstFilePath">Copy할 Destination File Path</param>
        private void CopyFile(string strSrcFilePath, string strDstFilePath, bool OverWrite)
        {
            File.Copy(strSrcFilePath, strDstFilePath, OverWrite);
        }

        #endregion


        #region Socket FTP Function

        /// <summary>
        /// FTP Server에 Connect
        /// </summary>     
        private void FTP_Connect()
        {
            string strConnResult = string.Empty;

            IPAddress IPAddr;
            int intPort = Int32.Parse(m_STRC_TCP_ENV_COMMON.FTP_PORT);

            IPHostEntry IPHost = Dns.GetHostByName(m_STRC_TCP_ENV_COMMON.FTP_IP);
            IPAddr = IPHost.AddressList[0];//ip of the host name
            IPEndPoint IPEPoint = new IPEndPoint(IPAddr, intPort);

            m_FTP = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_FTP.Connect(IPEPoint);
            if (m_FTP.Connected == true)
            {
                string strCommand = string.Empty;

                //FTP User ID Send
                strCommand = "USER " + m_STRC_TCP_ENV_COMMON.FTP_UID + "\r\n";
                FTP_SendCommand(strCommand);
                strConnResult = FTP_GetResponseCommand();

                //FTP User Password Send
                strCommand = "PASS " + m_STRC_TCP_ENV_COMMON.FTP_PWD + "\r\n";
                FTP_SendCommand(strCommand);

                do
                {
                    strConnResult = FTP_GetResponseCommand();
                    if (strConnResult == "")
                    {
                        strConnResult = "530 로그인 실패";
                        break;
                    }
                } while (strConnResult.Substring(0, 3) != "230");

                //230 : Login successful.
                //530 : Login Incorrect.
                if (strConnResult.Substring(0, 3) != "230")
                {
                    MessageBox.Show(strConnResult);
                    FTP_Disconnect();
                    m_IsConnected = false;
                }
                else
                {
                    m_IsConnected = true;
                }

                m_IsFirstConnect = true;
            }
            else
            {
                m_IsConnected = false;
            }
        }

        /// <summary>
        /// FTP Server와 연결을 끊는다.
        /// </summary>
        private void FTP_Disconnect()
        {
            string strCommand = string.Empty;

            strCommand = "quit \r\n";
            FTP_SendCommand(strCommand);
            FTP_GetResponseCommand();
            m_FTP.Close();

            m_IsConnected = false;
        }

        /// <summary>
        /// FTP Server로 Command를 전송한다.
        /// </summary>
        /// <param name="strCommand">Send할 Command</param>
        private void FTP_SendCommand(string strCommand)
        {
            m_FTP.Send(Encoding.ASCII.GetBytes(strCommand), strCommand.Length, 0);
        }

        /// <summary>
        /// FTP Server의 Response를 Command를 받아 리턴한다.
        /// </summary>
        private string FTP_GetResponseCommand()
        {
            try
            {
                Byte[] btReadBytes;
                int intRcvSize = 0;
                string strRcvMessage = string.Empty;
                bool blLastLine;

                m_FTP.Poll(500, SelectMode.SelectRead);

                // Go back for more if necessary
                do
                {
                    btReadBytes = new Byte[512];
                    intRcvSize = m_FTP.Receive(btReadBytes);
                    strRcvMessage += Encoding.ASCII.GetString(btReadBytes, 0, intRcvSize);
                }
                while (intRcvSize == btReadBytes.Length && !strRcvMessage.EndsWith("\r\n"));

                if (strRcvMessage == "")
                {
                    return string.Empty;
                }

                int intCode = Convert.ToInt32(strRcvMessage.Substring(0, 3));
                if (strRcvMessage[3] == '-' & strRcvMessage.IndexOf("\r\n" + intCode + ' ') == -1)
                {
                    // Go back for more if necessary
                    do
                    {
                        btReadBytes = new Byte[512];
                        intRcvSize = m_FTP.Receive(btReadBytes);
                        strRcvMessage += Encoding.ASCII.GetString(btReadBytes, 0, intRcvSize);

                        blLastLine = strRcvMessage.IndexOf("\r\n" + intCode + ' ') > 0;
                    }
                    while (!blLastLine | !strRcvMessage.EndsWith("\r\n"));
                }

                return strRcvMessage;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// FTP Server의 Transfer Type을 변경한다.
        /// </summary>
        private void FTP_SetTransferType()
        {
            string strCommand = string.Empty;

            //set type as BINARY
            strCommand = "TYPE I\r\n";

            FTP_SendCommand(strCommand);
            FTP_GetResponseCommand();
        }

        /// <summary>
        /// FTP Server에서 특정 Directory로 이동한다.
        /// </summary>
        /// <param name="strPath">이동할 경로</param>
        private void FTP_ChangeDirectory(string strPath)
        {
            string strCommand = string.Empty;

            strCommand = "CWD " + strPath + "\r\n";
            FTP_SendCommand(strCommand);
            FTP_GetResponseCommand();
        }

        /// <summary>
        /// FTP Server에서 특정 경로의 File을 Delete한다.
        /// </summary>
        /// <param name="strPath">이동할 경로</param>
        private void FTP_DeleteFile(string strPath)
        {
            string strCommand = string.Empty;

            strCommand = "DELE " + strPath + "\r\n";
            FTP_SendCommand(strCommand);
            FTP_GetResponseCommand();
        }

        /// <summary>
        /// FTP Server에 접속한 Client의 현재 위치 가져온다.
        /// </summary>
        /// <returns>FTP Path</returns>
        private string FTP_GetCurrentPath()
        {
            try
            {
                string strCommand = string.Empty;
                string strRes = string.Empty;
                string strFTPPath = string.Empty;

                strCommand = "PWD\r\n";
                FTP_SendCommand(strCommand);
                strRes = FTP_GetResponseCommand();

                string[] arrDir = strRes.Split('"');
                strFTPPath = arrDir[1].ToString();

                return strFTPPath;
            }
            catch
            {
                return string.Empty;
            }
        }
                
        /// <summary>
        /// FTP Server에서 특정 Directory를 만든다.
        /// </summary>
        /// <param name="strPath"></param>
        private void FTP_CreateDirectory(string strPath)
        {
            string strCommand = string.Empty;

            strCommand = "MKD " + strPath + "\r\n";
            FTP_SendCommand(strCommand);
            FTP_GetResponseCommand();
        }

        /// <summary>
        /// to get a new socket that listens on the port determined by the server
        /// </summary>
        /// <returns>Socket Object</returns>
        private Socket OepnAndGetPASVSocket()
        {
            Socket oPasvSock = null;

            string strCommand = string.Empty;
            string strResCommand = string.Empty;
            string strFullAddress = string.Empty;

            //********************************************************************
            //필독 : PASV 모드로 전환시.. 그 응답 Command가 늦게 오는 경우가 있음.
            //       따라서 아래와 같이 227을 꼭 받고 Pasv모드가 되야만 넘어가야 한다.
            //********************************************************************

            //PASV:Specifies that the server data transfer process is to listen for a connection request from the client data transfer process. 
            strCommand = "PASV" + "\r\n";

            FTP_SendCommand(strCommand);

            if (m_IsFirstConnect == true)
            {
                strResCommand = FTP_GetResponseCommand();
                m_IsFirstConnect = false;
            }

            // Get server IP and port address
            bool IsPasvOK = false;


            if (strResCommand.Length > 0 && strResCommand.Substring(0, 3) == "227")
            {
                IsPasvOK = true;
            }
            else
            {
                do
                {
                    strResCommand = FTP_GetResponseCommand();
                    if (strResCommand.Length > 0 && strResCommand.Substring(0, 3) == "227")
                    {
                        IsPasvOK = true;
                        break;
                    }
                } while (IsPasvOK == false);
            }
            strFullAddress = strResCommand;

            strFullAddress = strFullAddress.Remove(0, strFullAddress.IndexOf('(') + 1);//takes only what is 
            strFullAddress = strFullAddress.Substring(0, strFullAddress.IndexOf(')'));//between paranthesis
            string[] arrAddrParts = strFullAddress.Split(',');
            string strPasvAddress = arrAddrParts[0] + "." + arrAddrParts[1] + "." + arrAddrParts[2] + "." + arrAddrParts[3];
            int intPasvPort = Convert.ToInt32(arrAddrParts[4]) * 256 + Convert.ToInt32(arrAddrParts[5]);

            // Open the Data socket
            oPasvSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            oPasvSock.Connect(new IPEndPoint(IPAddress.Parse(strPasvAddress), intPasvPort));

            return oPasvSock;
        }

        #endregion

    }
}
