﻿namespace WaterNet.Water_Net.FormPopup
{
    partial class frmCodeManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtREMARK = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPCODE_NAME = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPCODE = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnQuery1 = new System.Windows.Forms.Button();
            this.btnSave1 = new System.Windows.Forms.Button();
            this.btnDelete1 = new System.Windows.Forms.Button();
            this.btnClose1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS11 = new System.Windows.Forms.PictureBox();
            this.picFrBottomS11 = new System.Windows.Forms.PictureBox();
            this.picFrTopS11 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCODE = new System.Windows.Forms.TextBox();
            this.txtREMARK2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCODE_NAME = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPCODE2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnQuery2 = new System.Windows.Forms.Button();
            this.btnSave2 = new System.Windows.Forms.Button();
            this.btnDelete2 = new System.Windows.Forms.Button();
            this.cboCDMAS2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose2 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTabCode = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabCode)).BeginInit();
            this.uTabCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.panel3);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox9);
            this.ultraTabPageControl1.Controls.Add(this.uGrid1);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox2);
            this.ultraTabPageControl1.Controls.Add(this.picFrLeftS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrBottomS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrTopS11);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(670, 414);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtREMARK);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.txtPCODE_NAME);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtPCODE);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 355);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(664, 55);
            this.panel3.TabIndex = 123;
            // 
            // txtREMARK
            // 
            this.txtREMARK.Location = new System.Drawing.Point(84, 29);
            this.txtREMARK.MaxLength = 200;
            this.txtREMARK.Name = "txtREMARK";
            this.txtREMARK.Size = new System.Drawing.Size(573, 21);
            this.txtREMARK.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(37, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 70;
            this.label4.Text = "비고 :";
            // 
            // txtPCODE_NAME
            // 
            this.txtPCODE_NAME.Location = new System.Drawing.Point(235, 4);
            this.txtPCODE_NAME.MaxLength = 60;
            this.txtPCODE_NAME.Name = "txtPCODE_NAME";
            this.txtPCODE_NAME.Size = new System.Drawing.Size(422, 21);
            this.txtPCODE_NAME.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(170, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 68;
            this.label3.Text = "그룹 명 :";
            // 
            // txtPCODE
            // 
            this.txtPCODE.Location = new System.Drawing.Point(84, 4);
            this.txtPCODE.MaxLength = 6;
            this.txtPCODE.Name = "txtPCODE";
            this.txtPCODE.Size = new System.Drawing.Size(80, 21);
            this.txtPCODE.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(6, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 12);
            this.label2.TabIndex = 67;
            this.label2.Text = "그룹 코드 :";
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Gold;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(4, 351);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(664, 4);
            this.pictureBox9.TabIndex = 122;
            this.pictureBox9.TabStop = false;
            // 
            // uGrid1
            // 
            this.uGrid1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGrid1.Location = new System.Drawing.Point(4, 36);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(664, 315);
            this.uGrid1.TabIndex = 115;
            this.uGrid1.Text = "코드 그룹";
            this.uGrid1.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid1_ClickCell);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Gold;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox17.Location = new System.Drawing.Point(4, 32);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(664, 4);
            this.pictureBox17.TabIndex = 114;
            this.pictureBox17.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnQuery1);
            this.panel2.Controls.Add(this.btnSave1);
            this.panel2.Controls.Add(this.btnDelete1);
            this.panel2.Controls.Add(this.btnClose1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(664, 28);
            this.panel2.TabIndex = 113;
            // 
            // btnQuery1
            // 
            this.btnQuery1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnQuery1.Image = global::WaterNet.Water_Net.Properties.Resources.Query;
            this.btnQuery1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery1.Location = new System.Drawing.Point(382, 0);
            this.btnQuery1.Name = "btnQuery1";
            this.btnQuery1.Size = new System.Drawing.Size(70, 26);
            this.btnQuery1.TabIndex = 5;
            this.btnQuery1.Text = "조회";
            this.btnQuery1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery1.UseVisualStyleBackColor = true;
            this.btnQuery1.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnSave1
            // 
            this.btnSave1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave1.Image = global::WaterNet.Water_Net.Properties.Resources.Save;
            this.btnSave1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave1.Location = new System.Drawing.Point(452, 0);
            this.btnSave1.Name = "btnSave1";
            this.btnSave1.Size = new System.Drawing.Size(70, 26);
            this.btnSave1.TabIndex = 69;
            this.btnSave1.Text = "등록";
            this.btnSave1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave1.UseVisualStyleBackColor = true;
            this.btnSave1.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete1
            // 
            this.btnDelete1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDelete1.Image = global::WaterNet.Water_Net.Properties.Resources.Delete;
            this.btnDelete1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete1.Location = new System.Drawing.Point(522, 0);
            this.btnDelete1.Name = "btnDelete1";
            this.btnDelete1.Size = new System.Drawing.Size(70, 26);
            this.btnDelete1.TabIndex = 70;
            this.btnDelete1.Text = "삭제";
            this.btnDelete1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete1.UseVisualStyleBackColor = true;
            this.btnDelete1.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose1
            // 
            this.btnClose1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose1.Image = global::WaterNet.Water_Net.Properties.Resources.Close2;
            this.btnClose1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose1.Location = new System.Drawing.Point(592, 0);
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.Size = new System.Drawing.Size(70, 26);
            this.btnClose1.TabIndex = 3;
            this.btnClose1.Text = "닫기";
            this.btnClose1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose1.UseVisualStyleBackColor = true;
            this.btnClose1.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Location = new System.Drawing.Point(668, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(2, 406);
            this.pictureBox2.TabIndex = 112;
            this.pictureBox2.TabStop = false;
            // 
            // picFrLeftS11
            // 
            this.picFrLeftS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS11.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS11.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftS11.Name = "picFrLeftS11";
            this.picFrLeftS11.Size = new System.Drawing.Size(4, 406);
            this.picFrLeftS11.TabIndex = 111;
            this.picFrLeftS11.TabStop = false;
            // 
            // picFrBottomS11
            // 
            this.picFrBottomS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS11.Location = new System.Drawing.Point(0, 410);
            this.picFrBottomS11.Name = "picFrBottomS11";
            this.picFrBottomS11.Size = new System.Drawing.Size(670, 4);
            this.picFrBottomS11.TabIndex = 110;
            this.picFrBottomS11.TabStop = false;
            // 
            // picFrTopS11
            // 
            this.picFrTopS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS11.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS11.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS11.Name = "picFrTopS11";
            this.picFrTopS11.Size = new System.Drawing.Size(670, 4);
            this.picFrTopS11.TabIndex = 109;
            this.picFrTopS11.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.panel4);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox8);
            this.ultraTabPageControl2.Controls.Add(this.uGrid2);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox3);
            this.ultraTabPageControl2.Controls.Add(this.panel1);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox4);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox5);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox7);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(670, 414);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.txtCODE);
            this.panel4.Controls.Add(this.txtREMARK2);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.txtCODE_NAME);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.txtPCODE2);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(4, 355);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(664, 55);
            this.panel4.TabIndex = 124;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(303, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 12);
            this.label8.TabIndex = 72;
            this.label8.Text = "코드 명 :";
            // 
            // txtCODE
            // 
            this.txtCODE.Location = new System.Drawing.Point(217, 4);
            this.txtCODE.MaxLength = 6;
            this.txtCODE.Name = "txtCODE";
            this.txtCODE.Size = new System.Drawing.Size(80, 21);
            this.txtCODE.TabIndex = 1;
            // 
            // txtREMARK2
            // 
            this.txtREMARK2.Location = new System.Drawing.Point(84, 29);
            this.txtREMARK2.MaxLength = 200;
            this.txtREMARK2.Name = "txtREMARK2";
            this.txtREMARK2.Size = new System.Drawing.Size(573, 21);
            this.txtREMARK2.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(37, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 70;
            this.label5.Text = "비고 :";
            // 
            // txtCODE_NAME
            // 
            this.txtCODE_NAME.Location = new System.Drawing.Point(368, 4);
            this.txtCODE_NAME.MaxLength = 60;
            this.txtCODE_NAME.Name = "txtCODE_NAME";
            this.txtCODE_NAME.Size = new System.Drawing.Size(289, 21);
            this.txtCODE_NAME.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(170, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 68;
            this.label6.Text = "코드 :";
            // 
            // txtPCODE2
            // 
            this.txtPCODE2.Location = new System.Drawing.Point(84, 4);
            this.txtPCODE2.MaxLength = 6;
            this.txtPCODE2.Name = "txtPCODE2";
            this.txtPCODE2.Size = new System.Drawing.Size(80, 21);
            this.txtPCODE2.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(6, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 12);
            this.label7.TabIndex = 67;
            this.label7.Text = "그룹 코드 :";
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Gold;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox8.Location = new System.Drawing.Point(4, 351);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(664, 4);
            this.pictureBox8.TabIndex = 123;
            this.pictureBox8.TabStop = false;
            // 
            // uGrid2
            // 
            this.uGrid2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGrid2.Location = new System.Drawing.Point(4, 36);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(664, 315);
            this.uGrid2.TabIndex = 122;
            this.uGrid2.Text = "코드";
            this.uGrid2.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid2_ClickCell);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(4, 32);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(664, 4);
            this.pictureBox3.TabIndex = 121;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnQuery2);
            this.panel1.Controls.Add(this.btnSave2);
            this.panel1.Controls.Add(this.btnDelete2);
            this.panel1.Controls.Add(this.cboCDMAS2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnClose2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(664, 28);
            this.panel1.TabIndex = 120;
            // 
            // btnQuery2
            // 
            this.btnQuery2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnQuery2.Image = global::WaterNet.Water_Net.Properties.Resources.Query;
            this.btnQuery2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery2.Location = new System.Drawing.Point(382, 0);
            this.btnQuery2.Name = "btnQuery2";
            this.btnQuery2.Size = new System.Drawing.Size(70, 26);
            this.btnQuery2.TabIndex = 5;
            this.btnQuery2.Text = "조회";
            this.btnQuery2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery2.UseVisualStyleBackColor = true;
            this.btnQuery2.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnSave2
            // 
            this.btnSave2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave2.Image = global::WaterNet.Water_Net.Properties.Resources.Save;
            this.btnSave2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave2.Location = new System.Drawing.Point(452, 0);
            this.btnSave2.Name = "btnSave2";
            this.btnSave2.Size = new System.Drawing.Size(70, 26);
            this.btnSave2.TabIndex = 71;
            this.btnSave2.Text = "등록";
            this.btnSave2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave2.UseVisualStyleBackColor = true;
            this.btnSave2.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete2
            // 
            this.btnDelete2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDelete2.Image = global::WaterNet.Water_Net.Properties.Resources.Delete;
            this.btnDelete2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete2.Location = new System.Drawing.Point(522, 0);
            this.btnDelete2.Name = "btnDelete2";
            this.btnDelete2.Size = new System.Drawing.Size(70, 26);
            this.btnDelete2.TabIndex = 70;
            this.btnDelete2.Text = "삭제";
            this.btnDelete2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete2.UseVisualStyleBackColor = true;
            this.btnDelete2.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // cboCDMAS2
            // 
            this.cboCDMAS2.FormattingEnabled = true;
            this.cboCDMAS2.Location = new System.Drawing.Point(97, 3);
            this.cboCDMAS2.Name = "cboCDMAS2";
            this.cboCDMAS2.Size = new System.Drawing.Size(220, 20);
            this.cboCDMAS2.TabIndex = 68;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 12);
            this.label1.TabIndex = 67;
            this.label1.Text = "코드 마스터 :";
            // 
            // btnClose2
            // 
            this.btnClose2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose2.Image = global::WaterNet.Water_Net.Properties.Resources.Close2;
            this.btnClose2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose2.Location = new System.Drawing.Point(592, 0);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.Size = new System.Drawing.Size(70, 26);
            this.btnClose2.TabIndex = 3;
            this.btnClose2.Text = "닫기";
            this.btnClose2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose2.UseVisualStyleBackColor = true;
            this.btnClose2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(668, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(2, 406);
            this.pictureBox4.TabIndex = 119;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox5.Location = new System.Drawing.Point(0, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(4, 406);
            this.pictureBox5.TabIndex = 118;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox6.Location = new System.Drawing.Point(0, 410);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(670, 4);
            this.pictureBox6.TabIndex = 117;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox7.Location = new System.Drawing.Point(0, 0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(670, 4);
            this.pictureBox7.TabIndex = 116;
            this.pictureBox7.TabStop = false;
            // 
            // uTab
            // 
            this.uTab.Location = new System.Drawing.Point(0, 0);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(200, 100);
            this.uTab.TabIndex = 0;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(196, 77);
            // 
            // uTabCode
            // 
            this.uTabCode.Controls.Add(this.ultraTabSharedControlsPage2);
            this.uTabCode.Controls.Add(this.ultraTabPageControl1);
            this.uTabCode.Controls.Add(this.ultraTabPageControl2);
            this.uTabCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabCode.Location = new System.Drawing.Point(4, 4);
            this.uTabCode.Name = "uTabCode";
            this.uTabCode.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.uTabCode.Size = new System.Drawing.Size(674, 440);
            this.uTabCode.TabIndex = 115;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "코드 그룹 관리";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "코드 관리";
            this.uTabCode.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(670, 414);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(678, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(2, 440);
            this.pictureBox1.TabIndex = 114;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 440);
            this.picFrLeft.TabIndex = 113;
            this.picFrLeft.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 444);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(680, 4);
            this.picFrBottom.TabIndex = 112;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(680, 4);
            this.picFrTop.TabIndex = 111;
            this.picFrTop.TabStop = false;
            // 
            // frmCodeManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 448);
            this.Controls.Add(this.uTabCode);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCodeManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "water-NET 코드 관리";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmCodeManager_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabCode)).EndInit();
            this.uTabCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabCode;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery1;
        private System.Windows.Forms.Button btnSave1;
        private System.Windows.Forms.Button btnClose1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox picFrLeftS11;
        private System.Windows.Forms.PictureBox picFrBottomS11;
        private System.Windows.Forms.PictureBox picFrTopS11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnQuery2;
        private System.Windows.Forms.Button btnDelete2;
        private System.Windows.Forms.ComboBox cboCDMAS2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button btnDelete1;
        private System.Windows.Forms.Button btnSave2;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPCODE;
        private System.Windows.Forms.TextBox txtPCODE_NAME;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtREMARK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCODE;
        private System.Windows.Forms.TextBox txtREMARK2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCODE_NAME;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPCODE2;
        private System.Windows.Forms.Label label7;
    }
}