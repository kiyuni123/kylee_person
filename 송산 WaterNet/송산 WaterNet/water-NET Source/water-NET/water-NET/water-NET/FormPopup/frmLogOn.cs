﻿using System;
using System.Data;
using System.Net;
using System.Text;
using System.Windows.Forms;
using WaterNet.WaterNetCore;


namespace WaterNet.Water_Net.FormPopup
{
    /// <summary>
    /// Water-NET 로그온
    /// </summary>
    public partial class frmLogOn : Form
    {
        #region 프로퍼티

        private bool _LOGON;

        /// <summary>
        /// 로그온 성공여부 (true 성공)
        /// </summary>
        public bool IsLogOn
        {
            get
            {
                return _LOGON;
            }
            set
            {
                _LOGON = value;
            }
        }

        #endregion

        OracleDBManager m_oDBManager = new OracleDBManager();

        public frmLogOn()
        {
            InitializeComponent();

            m_oDBManager.ConnectionString = FunctionManager.GetConnectionString();

            m_oDBManager.Open();
        }

        private void frmLogOn_Load(object sender, EventArgs e)
        {
            this.SetCombo_Municipality(this.cboSGCCD);
        }

        private void frmLogOn_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_oDBManager.Close();
        }

        #region Button Events

        private void btnLogon_Click(object sender, EventArgs e)
        {
            _LOGON = this.LogOnProcess();

            if (_LOGON == true)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("Water-NET 사용자 ID 및 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _LOGON = false;
            this.Close();
        }

        #endregion


        #region Control Events

        private void txtUID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtPWD.Focus();
            }
        }

        private void txtPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.btnLogon.Focus();
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 로그온 성공여부
        /// </summary>
        /// <returns></returns>
        private bool LogOnProcess()
        {
            bool blRtn = false;
            string strEncUID = string.Empty;
            string strEncUPWD = string.Empty;

            //프로그램 로그온
            if (m_oDBManager != null)
            {
                StringBuilder oStringBuilder = new StringBuilder();

                DataSet pDS = new DataSet();

                //User ID는 그대로 Password는 UserID+Password로 인크립트
                strEncUID = this.txtUID.Text.Trim();
                strEncUPWD = EMFrame.utils.ConvertUtils.EncryptKey(this.txtUID.Text.Trim() + this.txtPWD.Text.Trim());

                //Console.WriteLine(strEncUPWD);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   USER_NAME, USER_DIVISION, USER_RIGHT");
                oStringBuilder.AppendLine("FROM     CM_USER");
                oStringBuilder.AppendLine("WHERE    USER_ID = '" + strEncUID + "' AND USER_PWD = '" + strEncUPWD + "' AND USE_YN = 'Y' AND ROWNUM = 1");

                pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_USER");

                if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
                {
                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        AppStatic.USER_ID = this.txtUID.Text.Trim();
                        AppStatic.USER_NAME = oDRow["USER_NAME"].ToString();
                        AppStatic.USER_DIVISION = oDRow["USER_DIVISION"].ToString();
                        AppStatic.USER_RIGHT = oDRow["USER_RIGHT"].ToString();
                        //Dns.GetHostName();
                        IPHostEntry MyIPHostEntry = Dns.GetHostEntry(Dns.GetHostName());
                        foreach (IPAddress MyIpAddress in MyIPHostEntry.AddressList)
                        {
                            if (MyIpAddress.IsIPv6LinkLocal != true) //IPv4의 IP인 경우 즉, IPv6가 아닌 경우
                            {
                                AppStatic.USER_IP = MyIpAddress.ToString();
                                break;
                            }
                        }

                        AppStatic.USER_SGCCD = this.SplitToCode(this.cboSGCCD.Text);
                        AppStatic.USER_SGCNM = this.SplitToCodeName(this.cboSGCCD.Text);

                        blRtn = true;
                    }
                }
                else
                {
                    blRtn = false;
                }
            }
            else
            {
                blRtn =  false;
            }

            return blRtn;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 지자체
        /// 대상 Table : OCSGCCD
        /// </summary>
        /// <param name="oCombo"></param>
        private void SetCombo_Municipality(ComboBox oCombo)
        {
            int i = 0;
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            //WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            //oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            //oDBManager.Open();

            //프로그램 로그온
            if (m_oDBManager != null)
            {
                oStringBuilder.AppendLine("SELECT   SGCNM || ' (' || SGCCD || ')' AS RTNDATA");
                oStringBuilder.AppendLine("FROM     OCSGCCD");
                oStringBuilder.AppendLine("ORDER BY SGCCD");

                pDS = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "OCSGCCD");

                oCombo.Items.Clear();

                if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
                {
                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());

                        i++;
                    }
                }

                if (oCombo.Items.Count > 0)
                {
                    oCombo.SelectedIndex = 0;
                }
                oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
                pDS.Dispose();
            }
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 001을 반환</param>
        /// <returns>string Code</returns>
        private string SplitToCode(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[1].Substring(0, strArr[1].Length - 1);
            }
            else
            {
                if (strArr[0].Trim() == "전체")
                {
                    strCode = " ";
                }
                else
                {
                    strCode = strArr[0].Trim();
                }
            }

            return strCode;
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드명 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 산정정수장을 반환</param>
        /// <returns>string Code Name</returns>
        private string SplitToCodeName(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[0].Substring(0, strArr[0].Length - 1);
            }
            else
            {
                strCode = strArr[0].Trim();
            }

            return strCode;
        }

        #endregion
    }
}
