﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WH_Common.utils;
using WaterNet.WH_Common.work;
using WaterNet.WaterNetCore;
using WaterNet.Water_Net.work;
using EMFrame.log;

namespace WaterNet.Water_Net.FormPopup
{
    public partial class frmWarning : Form,WaterNet.WaterNetCore.IForminterface
    {
        public frmLMain parentForm = null;
        private WarningWork work = null;
        private CommonWork cWork = null;

        private DataSet warningDataSet = null;

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        public frmWarning()
        {
            InitializeComponent();
            InitializeSetting();
        }

        //메인폼에서 호출
        public void Open()
        {
        }

        //메인폼에서 호출
        private void frmWQMain_FormClosing(object sender, FormClosingEventArgs e)
        {
        }


        private void InitializeSetting()
        {
            work = WarningWork.GetInstance();
            cWork = CommonWork.GetInstance();

            UltraGridColumn warningColumn;

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "SELECTED";
            warningColumn.SortIndicator = SortIndicator.Disabled;
            warningColumn.Header.Caption = "";
            warningColumn.CellActivation = Activation.AllowEdit;
            warningColumn.CellClickAction = CellClickAction.Edit;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 20;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WARN_NO";
            warningColumn.Header.Caption = "WARN_NO";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "CREATE_DATE";
            warningColumn.Header.Caption = "경보일자";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "FIXED_DATE";
            warningColumn.Header.Caption = "확인(처리)일자";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WORK_GBN";
            warningColumn.Header.Caption = "작업구분코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "LOC_CODE";
            warningColumn.Header.Caption = "지역코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WAR_CODE";
            warningColumn.Header.Caption = "경보코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "LOC_NAME";
            warningColumn.Header.Caption = "지역";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 100;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WORK_GBN_NAME";
            warningColumn.Header.Caption = "작업구분";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 100;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WAR_CODE_NAME";
            warningColumn.Header.Caption = "경보내용";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Left;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "CHECK_YN";
            warningColumn.Header.Caption = "확인여부";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 80;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "FIXED_YN";
            warningColumn.Header.Caption = "처리여부";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 80;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "SERIAL_NO";
            warningColumn.Header.Caption = "SERIAL_NO";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "REMARK";
            warningColumn.Header.Caption = "비고";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Left;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 400;
            warningColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griWarningList);

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns["SELECTED"];
            warningColumn.CellActivation = Activation.AllowEdit;    
            warningColumn.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            warningColumn.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.None;
        }

        //폼로딩 시 호출
        private void frmWarning_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.12
            //                      권한박탈(조회만 가능)       
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간경보확인ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butCheckAll.Enabled = false;
            }

            //========================================================================

            //datStartDate.Value = Utils.GetCalcTime("dd", -3)["yyyymmdd"];
            datStartDate.Value = Utils.GetTime()["yyyymmdd"];
            datEndDate.Value = Utils.GetTime()["yyyymmdd"];

            //경고 업무구분 combobox 설정
            object[,] comWorkGbnData = new object[6, 2];    

            comWorkGbnData[0, 0] = "";
            comWorkGbnData[0, 1] = "선택";

            comWorkGbnData[1, 0] = "0001";
            comWorkGbnData[1, 1] = "수리경보";

            comWorkGbnData[2, 0] = "0002";
            comWorkGbnData[2, 1] = "수량경보";

            comWorkGbnData[3, 0] = "0003";
            comWorkGbnData[3, 1] = "수질경보";

            comWorkGbnData[4, 0] = "0004";
            comWorkGbnData[4, 1] = "감압밸브경보";

            comWorkGbnData[5, 0] = "0005";
            comWorkGbnData[5, 1] = "실시간수집경보";

            FormManager.SetComboBoxEX(comWorkGbn, comWorkGbnData, false);

            //경고코드 설정
            comWarCode.ValueMember = "CODE";
            comWarCode.DisplayMember = "CODE_NAME";

            //블록정보 할당(검색조건)
            comLftridn.ValueMember = "LOC_CODE";
            comLftridn.DisplayMember = "LOC_NAME";
            comMftridn.ValueMember = "LOC_CODE";
            comMftridn.DisplayMember = "LOC_NAME";
            comSftridn.ValueMember = "LOC_CODE";
            comSftridn.DisplayMember = "LOC_NAME";

            DataSet blockDataSet = cWork.GetLblockDataList(new Hashtable());

            if (blockDataSet != null)
            {
                comLftridn.DataSource = blockDataSet.Tables["CM_LOCATION"];
            }
        }

        //업무구분 변경 시 경고코드 조회
        private void comWorkGbn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();

            conditions.Add("PCODE", comWorkGbn.SelectedValue.ToString());

            DataSet warningDataSet = cWork.GetCodeList(conditions);

            if(warningDataSet != null)
            {
                comWarCode.DataSource = warningDataSet.Tables["CM_CODE"];
            }
        }

        //대블록 변경 시 중블록리스트 조회
        private void comLftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comLftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comMftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //중블록 변경 시 소블록리스트 조회
        private void comMftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comMftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comSftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //경고리스트 조회
        private void butSelectWarningList_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable conditions = new Hashtable();

                conditions.Add("startDate", datStartDate.DateTime.Year + Convert.ToString(datStartDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datStartDate.DateTime.Day).PadLeft(2, '0'));
                conditions.Add("endDate", datEndDate.DateTime.Year + Convert.ToString(datEndDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datEndDate.DateTime.Day).PadLeft(2, '0'));

                if (!"".Equals(comWorkGbn.SelectedValue.ToString()))
                {
                    conditions.Add("WORK_GBN", comWorkGbn.SelectedValue.ToString());
                }

                if (!"".Equals(comWarCode.SelectedValue.ToString()))
                {
                    conditions.Add("WAR_CODE", comWarCode.SelectedValue.ToString());
                }

                string locCode = "";

                if (!"".Equals(comSftridn.SelectedValue.ToString()))
                {
                    locCode = comSftridn.SelectedValue.ToString();
                }
                else if (!"".Equals(comMftridn.SelectedValue.ToString()))
                {
                    locCode = comMftridn.SelectedValue.ToString();
                }
                else if (!"".Equals(comLftridn.SelectedValue.ToString()))
                {
                    locCode = comLftridn.SelectedValue.ToString();
                }

                if (!"".Equals(locCode))
                {
                    conditions.Add("LOC_CODE", locCode);
                }

                if (cheCheckYn.Checked)
                {
                    conditions.Add("CHECK_YN", "N");
                }

                if (checFixedYn.Checked)
                {
                    conditions.Add("FIXED_YN", "Y");
                }

                warningDataSet = work.SelectWarningHistoryList(conditions);

                if (warningDataSet != null)
                {
                    griWarningList.DataSource = warningDataSet.Tables["WA_WARNING"];

                    for (int i = 0; i < griWarningList.Rows.Count; i++)
                    {
                        if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
                        {
                            griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
                        }
                        else
                        {
                            griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
                            griWarningList.Rows[i].Cells["SELECTED"].Activation = Activation.Disabled;
                        }
                    }
                }

                if (griWarningList.Rows.Count != 0)
                {
                    griWarningList.Rows[0].Selected = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //경고리스트 행을 더블클릭 했을 경우
        private void griWarningList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (griWarningList.Selected.Rows.Count != 0)
            {
                Hashtable parameters = new Hashtable();

                parameters.Add("warnNo", griWarningList.Selected.Rows[0].GetCellValue("WARN_NO").ToString());
                parameters.Add("workGbn", griWarningList.Selected.Rows[0].GetCellValue("WORK_GBN").ToString());
                parameters.Add("checkYn", griWarningList.Selected.Rows[0].GetCellValue("CHECK_YN").ToString());
                parameters.Add("warCode", griWarningList.Selected.Rows[0].GetCellValue("WAR_CODE").ToString());
                parameters.Add("serialNo", griWarningList.Selected.Rows[0].GetCellValue("SERIAL_NO").ToString());
                parameters.Add("increaseDate", griWarningList.Selected.Rows[0].GetCellValue("CREATE_DATE").ToString());

                parentForm.ChangeScreenByWarning(parameters);
            }
        }

        //엑셀출력
        private void butExcelExport_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (griWarningList.Selected.Rows.Count != 0)
                {
                    FormManager.ExportToExcelFromUltraGrid(griWarningList, "경보내역");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //cell header의 checkbox를 click한 경우
        private void griWarningList_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {

            for (int i = 0; i < griWarningList.Rows.Count; i++)
            {
                if (e.Column.GetHeaderCheckedState(e.Rows) == CheckState.Checked)
                {
                   if(griWarningList.Rows[i].Cells["SELECTED"].Activation != Activation.Disabled)
                   {
                       griWarningList.Rows[i].Cells["SELECTED"].Value = true;
                   }
                }
                else
                {
                    griWarningList.Rows[i].Cells["SELECTED"].Value = false;
                }
            }
        }

        private void butCheckAll_Click(object sender, EventArgs e)
        {
            if(griWarningList.Rows.Count != 0 )
            {
                DialogResult result = MessageBox.Show("체크된 경보내역들을 일괄 확인처리 하시겠습니까?", "경보내역 확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    Hashtable conditions = new Hashtable();

                    conditions.Add("startDate", datStartDate.DateTime.Year + Convert.ToString(datStartDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datStartDate.DateTime.Day).PadLeft(2, '0'));
                    conditions.Add("endDate", datEndDate.DateTime.Year + Convert.ToString(datEndDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datEndDate.DateTime.Day).PadLeft(2, '0'));

                    if (!"".Equals(comWorkGbn.SelectedValue.ToString()))
                    {
                        conditions.Add("WORK_GBN", comWorkGbn.SelectedValue.ToString());
                    }

                    if (!"".Equals(comWarCode.SelectedValue.ToString()))
                    {
                        conditions.Add("WAR_CODE", comWarCode.SelectedValue.ToString());
                    }

                    string locCode = "";

                    if (!"".Equals(comSftridn.SelectedValue.ToString()))
                    {
                        locCode = comSftridn.SelectedValue.ToString();
                    }
                    else if (!"".Equals(comMftridn.SelectedValue.ToString()))
                    {
                        locCode = comMftridn.SelectedValue.ToString();
                    }
                    else if (!"".Equals(comLftridn.SelectedValue.ToString()))
                    {
                        locCode = comLftridn.SelectedValue.ToString();
                    }

                    if (!"".Equals(locCode))
                    {
                        conditions.Add("LOC_CODE", locCode);
                    }

                    if (cheCheckYn.Checked)
                    {
                        conditions.Add("CHECK_YN", "N");
                    }

                    if (checFixedYn.Checked)
                    {
                        conditions.Add("FIXED_YN", "Y");
                    }

                    conditions.Add("dataSource",griWarningList.DataSource);

                    warningDataSet = work.UpdateCheckYnAll(conditions);

                    if (warningDataSet != null)
                    {
                        griWarningList.DataSource = warningDataSet.Tables["WA_WARNING"];

                        for (int i = 0; i < griWarningList.Rows.Count; i++)
                        {
                            if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
                            {
                                griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
                            }
                            else
                            {
                                griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
                                griWarningList.Rows[i].Cells["SELECTED"].Activation = Activation.Disabled;
                            }
                        }
                    }

                    if (griWarningList.Rows.Count != 0)
                    {
                        griWarningList.Rows[0].Selected = true;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                else
                {
                    return;
                }
            }
        }
    }
}
