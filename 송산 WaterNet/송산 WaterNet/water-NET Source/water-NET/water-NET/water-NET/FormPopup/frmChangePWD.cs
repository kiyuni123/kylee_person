﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using System.Text.RegularExpressions;


namespace WaterNet.Water_Net.FormPopup
{
    public partial class frmChangePWD : Form
    {
        public frmChangePWD()
        {
            InitializeComponent();
        }

        private void frmChangePWD_Load(object sender, EventArgs e)
        {
            this.txtUID.Text = EMFrame.statics.AppStatic.USER_ID;
        }


        #region Button Events

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsPasswordValid(0) != true) return;
            if (this.IsPasswordValid(1) != true) return;
            if (this.IsPasswordValid(2) != true) return;
            if (this.IsPasswordValid(3) != true) return;

            this.ChangePassword();

            MessageBox.Show("비밀번호를 변경했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion


        #region Control Events

        private void txtPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            //if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            //{
            //    e.Handled = true;
            //}

            if (e.KeyChar == 13)
            {
                this.txtNPWD.Focus();
            }
        }

        private void txtNPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            ////문자 or 숫자 && Back Space && \r (Return)
            //if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            //{
            //    e.Handled = true;
            //}

            if (e.KeyChar == 13)
            {
                this.txtNCPWD.Focus();
            }
        }

        private void txtNCPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            //if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            //{
            //    e.Handled = true;
            //}

            if (e.KeyChar == 13)
            {
                this.btnSave.Focus();
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 비밀번호의 적합성 검증
        /// </summary>
        /// <param name="iType">0 : Current Password, 1 : New Password, 2 : New Confirm Password</param>
        /// <returns></returns>
        private bool IsPasswordValid(int iType)
        {
            bool blRtn = true;

            switch (iType)
            {
                case 0:
                    if (this.LogOnProcess() != true)
                    {
                        blRtn = false;
                        this.txtPWD.Text = "";
                        this.txtPWD.Focus();
                        MessageBox.Show("비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 1:
                    if ((this.txtNPWD.Text.Trim() == this.txtPWD.Text.Trim()) || (this.txtNPWD.Text.Trim() == this.txtUID.Text.Trim())) // || (this.txtNPWD.Text.Trim().Length < 8) || (this.txtNPWD.Text.Trim().Length > 12))
                    {
                        blRtn = false;
                        this.txtNPWD.Text = "";
                        this.txtNPWD.Focus();
                        MessageBox.Show("새 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:
                    if ((this.txtNCPWD.Text.Trim() == this.txtPWD.Text.Trim()) || (this.txtNCPWD.Text.Trim() != this.txtNPWD.Text.Trim())) // || (this.txtNCPWD.Text.Trim().Length < 8) || (this.txtNCPWD.Text.Trim().Length > 12))
                    {
                        blRtn = false;
                        this.txtNCPWD.Text = "";
                        this.txtNCPWD.Focus();
                        MessageBox.Show("확인 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:
                    if (Regex.IsMatch(this.txtNPWD.Text, @"^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{9,16}$") == false)
                    {
                        blRtn = false;
                        this.txtNPWD.Text = "";
                        this.txtNCPWD.Text = "";
                        this.txtNPWD.Focus();
                        MessageBox.Show("비밀번호는 문자, 숫자, 특수문자를 포함한 9자리 이상 16자리 이하 여야 합니다..", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
            return blRtn;
        }

        /// <summary>
        /// 로그온 성공여부(현재비밀번호 체크)
        /// </summary>
        /// <returns></returns>
        private bool LogOnProcess()
        {
            bool blRtn = false;
            string strEncUID = string.Empty;
            string strEncUPWD = string.Empty;

            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                if (mapper == null)
                {
                    MessageBox.Show("통합 데이터베이스에 접속할 수 없습니다.");
                    return blRtn;
                }

                StringBuilder oStringBuilder = new StringBuilder();

                DataSet pDS = new DataSet();

                //User ID는 그대로 Password는 UserID+Password로 인크립트
                strEncUID = this.txtUID.Text.Trim();
                strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(EMFrame.statics.AppStatic.USER_NAME.Trim() + this.txtPWD.Text.Trim());

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT   USER_NAME, USER_DIVISION, USER_RIGHT");
                oStringBuilder.AppendLine("FROM     CM_USER");
                oStringBuilder.AppendLine("WHERE    USER_ID = '" + strEncUID + "' AND USER_PWD = '" + strEncUPWD + "' AND USE_YN = '1' AND ROWNUM = 1");

                pDS = mapper.ExecuteScriptDataSet(oStringBuilder.ToString(), new IDataParameter[] { }, "CM_USER");

                if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
                {
                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        blRtn = true;
                    }
                }
                else
                {
                    blRtn = false;
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return blRtn;
        }

        /// <summary>
        /// 사용자의 새 비밀번호를 저장한다.
        /// </summary>
        private void ChangePassword()
        {
            string strEncUPWD = string.Empty;

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                if (mapper == null)
                {
                    MessageBox.Show("통합 데이터베이스에 접속할 수 없습니다.");
                    return;
                }

                StringBuilder oStringBuilder = new StringBuilder();

                strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(EMFrame.statics.AppStatic.USER_NAME.Trim() + this.txtNPWD.Text.Trim());

                oStringBuilder.AppendLine("UPDATE   CM_USER");
                oStringBuilder.AppendLine("SET      USER_PWD = '" + strEncUPWD + "'");
                oStringBuilder.AppendLine("         ,PWDT = '" + DateTime.Today.ToString("yyyyMMdd") + "'");
                oStringBuilder.AppendLine("WHERE    USER_ID = '" + this.txtUID.Text.Trim() + "'");

                mapper.ExecuteScript(oStringBuilder.ToString(), new IDataParameter[] { });

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

        }

        #endregion

    }
}
