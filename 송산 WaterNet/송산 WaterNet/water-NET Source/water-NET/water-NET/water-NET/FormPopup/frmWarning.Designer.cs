﻿namespace WaterNet.Water_Net.FormPopup
{
    partial class frmWarning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butExcelExport = new System.Windows.Forms.Button();
            this.butSelectWarningList = new System.Windows.Forms.Button();
            this.checFixedYn = new System.Windows.Forms.CheckBox();
            this.cheCheckYn = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comSftridn = new System.Windows.Forms.ComboBox();
            this.comMftridn = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comLftridn = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comWarCode = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comWorkGbn = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.datEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.datStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.griWarningList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butCheckAll = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griWarningList)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1244, 9);
            this.pictureBox2.TabIndex = 115;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 810);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1244, 9);
            this.pictureBox1.TabIndex = 116;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(0, 9);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 801);
            this.pictureBox7.TabIndex = 118;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(1234, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 801);
            this.pictureBox3.TabIndex = 119;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butExcelExport);
            this.groupBox1.Controls.Add(this.butSelectWarningList);
            this.groupBox1.Controls.Add(this.checFixedYn);
            this.groupBox1.Controls.Add(this.cheCheckYn);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comSftridn);
            this.groupBox1.Controls.Add(this.comMftridn);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comLftridn);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comWarCode);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comWorkGbn);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.datEndDate);
            this.groupBox1.Controls.Add(this.datStartDate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1224, 83);
            this.groupBox1.TabIndex = 120;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butExcelExport
            // 
            this.butExcelExport.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butExcelExport.Font = new System.Drawing.Font("굴림", 9F);
            this.butExcelExport.Location = new System.Drawing.Point(1133, 51);
            this.butExcelExport.Name = "butExcelExport";
            this.butExcelExport.Size = new System.Drawing.Size(85, 22);
            this.butExcelExport.TabIndex = 122;
            this.butExcelExport.Text = "Excel 출력";
            this.butExcelExport.UseVisualStyleBackColor = true;
            this.butExcelExport.Click += new System.EventHandler(this.butExcelExport_Click);
            // 
            // butSelectWarningList
            // 
            this.butSelectWarningList.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSelectWarningList.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectWarningList.Location = new System.Drawing.Point(1084, 51);
            this.butSelectWarningList.Name = "butSelectWarningList";
            this.butSelectWarningList.Size = new System.Drawing.Size(43, 22);
            this.butSelectWarningList.TabIndex = 121;
            this.butSelectWarningList.Text = "조회";
            this.butSelectWarningList.UseVisualStyleBackColor = true;
            this.butSelectWarningList.Click += new System.EventHandler(this.butSelectWarningList_Click);
            // 
            // checFixedYn
            // 
            this.checFixedYn.AutoSize = true;
            this.checFixedYn.Location = new System.Drawing.Point(840, 22);
            this.checFixedYn.Name = "checFixedYn";
            this.checFixedYn.Size = new System.Drawing.Size(72, 16);
            this.checFixedYn.TabIndex = 46;
            this.checFixedYn.Text = "처리완료";
            this.checFixedYn.UseVisualStyleBackColor = true;
            // 
            // cheCheckYn
            // 
            this.cheCheckYn.AutoSize = true;
            this.cheCheckYn.Location = new System.Drawing.Point(774, 23);
            this.cheCheckYn.Name = "cheCheckYn";
            this.cheCheckYn.Size = new System.Drawing.Size(60, 16);
            this.cheCheckYn.TabIndex = 45;
            this.cheCheckYn.Text = "미확인";
            this.cheCheckYn.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(516, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 44;
            this.label7.Text = "소블록";
            // 
            // comSftridn
            // 
            this.comSftridn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comSftridn.Font = new System.Drawing.Font("굴림", 9F);
            this.comSftridn.FormattingEnabled = true;
            this.comSftridn.Location = new System.Drawing.Point(563, 53);
            this.comSftridn.Name = "comSftridn";
            this.comSftridn.Size = new System.Drawing.Size(100, 20);
            this.comSftridn.TabIndex = 43;
            // 
            // comMftridn
            // 
            this.comMftridn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comMftridn.Font = new System.Drawing.Font("굴림", 9F);
            this.comMftridn.FormattingEnabled = true;
            this.comMftridn.Location = new System.Drawing.Point(346, 53);
            this.comMftridn.Name = "comMftridn";
            this.comMftridn.Size = new System.Drawing.Size(100, 20);
            this.comMftridn.TabIndex = 42;
            this.comMftridn.SelectedIndexChanged += new System.EventHandler(this.comMftridn_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(299, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 41;
            this.label6.Text = "중블록";
            // 
            // comLftridn
            // 
            this.comLftridn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comLftridn.Font = new System.Drawing.Font("굴림", 9F);
            this.comLftridn.FormattingEnabled = true;
            this.comLftridn.Location = new System.Drawing.Point(91, 51);
            this.comLftridn.Name = "comLftridn";
            this.comLftridn.Size = new System.Drawing.Size(100, 20);
            this.comLftridn.TabIndex = 40;
            this.comLftridn.SelectedIndexChanged += new System.EventHandler(this.comLftridn_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 39;
            this.label5.Text = "대블록";
            // 
            // comWarCode
            // 
            this.comWarCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comWarCode.Font = new System.Drawing.Font("굴림", 9F);
            this.comWarCode.FormattingEnabled = true;
            this.comWarCode.Location = new System.Drawing.Point(620, 18);
            this.comWarCode.Name = "comWarCode";
            this.comWarCode.Size = new System.Drawing.Size(120, 20);
            this.comWarCode.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(561, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 37;
            this.label4.Text = "경보구분";
            // 
            // comWorkGbn
            // 
            this.comWorkGbn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comWorkGbn.Font = new System.Drawing.Font("굴림", 9F);
            this.comWorkGbn.FormattingEnabled = true;
            this.comWorkGbn.Location = new System.Drawing.Point(403, 18);
            this.comWorkGbn.Name = "comWorkGbn";
            this.comWorkGbn.Size = new System.Drawing.Size(120, 20);
            this.comWorkGbn.TabIndex = 36;
            this.comWorkGbn.SelectedIndexChanged += new System.EventHandler(this.comWorkGbn_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(344, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 35;
            this.label3.Text = "업무구분";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 34;
            this.label2.Text = "-";
            // 
            // datEndDate
            // 
            this.datEndDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datEndDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datEndDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datEndDate.Location = new System.Drawing.Point(208, 19);
            this.datEndDate.Name = "datEndDate";
            this.datEndDate.Size = new System.Drawing.Size(94, 19);
            this.datEndDate.TabIndex = 33;
            this.datEndDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // datStartDate
            // 
            this.datStartDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datStartDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datStartDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datStartDate.Location = new System.Drawing.Point(91, 19);
            this.datStartDate.Name = "datStartDate";
            this.datStartDate.Size = new System.Drawing.Size(94, 19);
            this.datStartDate.TabIndex = 32;
            this.datStartDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "검색기간";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 92);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1224, 9);
            this.pictureBox4.TabIndex = 121;
            this.pictureBox4.TabStop = false;
            // 
            // griWarningList
            // 
            this.griWarningList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griWarningList.Location = new System.Drawing.Point(10, 138);
            this.griWarningList.Name = "griWarningList";
            this.griWarningList.Size = new System.Drawing.Size(1224, 672);
            this.griWarningList.TabIndex = 122;
            this.griWarningList.Text = "ultraGrid1";
            this.griWarningList.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.griWarningList_AfterHeaderCheckStateChanged);
            this.griWarningList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griWarningList_DoubleClickRow);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butCheckAll);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1224, 37);
            this.panel1.TabIndex = 124;
            // 
            // butCheckAll
            // 
            this.butCheckAll.Font = new System.Drawing.Font("굴림", 9F);
            this.butCheckAll.Location = new System.Drawing.Point(6, 6);
            this.butCheckAll.Name = "butCheckAll";
            this.butCheckAll.Size = new System.Drawing.Size(67, 22);
            this.butCheckAll.TabIndex = 125;
            this.butCheckAll.Text = "일괄확인";
            this.butCheckAll.UseVisualStyleBackColor = true;
            this.butCheckAll.Click += new System.EventHandler(this.butCheckAll_Click);
            // 
            // frmWarning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1244, 819);
            this.Controls.Add(this.griWarningList);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "frmWarning";
            this.Text = "실시간 경보";
            this.Load += new System.EventHandler(this.frmWarning_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griWarningList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datStartDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comWarCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comWorkGbn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comSftridn;
        private System.Windows.Forms.ComboBox comMftridn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comLftridn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checFixedYn;
        private System.Windows.Forms.CheckBox cheCheckYn;
        private System.Windows.Forms.Button butSelectWarningList;
        private System.Windows.Forms.PictureBox pictureBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid griWarningList;
        private System.Windows.Forms.Button butExcelExport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butCheckAll;
    }
}