﻿namespace WaterNet.Water_Net.FormPopup
{
    partial class frmUserManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtNCPWD = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPWD = new System.Windows.Forms.TextBox();
            this.txtUID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUDivision = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rdoManager = new System.Windows.Forms.RadioButton();
            this.rdoOperator = new System.Windows.Forms.RadioButton();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ugData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugData)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 280);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 12);
            this.label4.TabIndex = 25;
            this.label4.Text = "확인 :";
            // 
            // txtNCPWD
            // 
            this.txtNCPWD.Location = new System.Drawing.Point(88, 275);
            this.txtNCPWD.MaxLength = 12;
            this.txtNCPWD.Name = "txtNCPWD";
            this.txtNCPWD.PasswordChar = '*';
            this.txtNCPWD.Size = new System.Drawing.Size(220, 21);
            this.txtNCPWD.TabIndex = 3;
            this.txtNCPWD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNCPWD_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 12);
            this.label2.TabIndex = 23;
            this.label2.Text = "사용자 ID :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 12);
            this.label1.TabIndex = 22;
            this.label1.Text = "비밀번호 :";
            // 
            // txtPWD
            // 
            this.txtPWD.BackColor = System.Drawing.Color.Ivory;
            this.txtPWD.Location = new System.Drawing.Point(88, 251);
            this.txtPWD.MaxLength = 12;
            this.txtPWD.Name = "txtPWD";
            this.txtPWD.PasswordChar = '*';
            this.txtPWD.Size = new System.Drawing.Size(220, 21);
            this.txtPWD.TabIndex = 1;
            this.txtPWD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPWD_KeyPress);
            // 
            // txtUID
            // 
            this.txtUID.Location = new System.Drawing.Point(88, 227);
            this.txtUID.MaxLength = 12;
            this.txtUID.Name = "txtUID";
            this.txtUID.Size = new System.Drawing.Size(220, 21);
            this.txtUID.TabIndex = 0;
            this.txtUID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUID_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 304);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 27;
            this.label5.Text = "사용자 성명 :";
            // 
            // txtUName
            // 
            this.txtUName.Location = new System.Drawing.Point(88, 299);
            this.txtUName.MaxLength = 32;
            this.txtUName.Name = "txtUName";
            this.txtUName.Size = new System.Drawing.Size(220, 21);
            this.txtUName.TabIndex = 4;
            this.txtUName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUName_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 328);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 29;
            this.label6.Text = "사용자 부서 :";
            // 
            // txtUDivision
            // 
            this.txtUDivision.Location = new System.Drawing.Point(88, 323);
            this.txtUDivision.MaxLength = 32;
            this.txtUDivision.Name = "txtUDivision";
            this.txtUDivision.Size = new System.Drawing.Size(220, 21);
            this.txtUDivision.TabIndex = 5;
            this.txtUDivision.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUDivision_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 352);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 31;
            this.label7.Text = "사용자 권한 :";
            // 
            // rdoManager
            // 
            this.rdoManager.AutoSize = true;
            this.rdoManager.Location = new System.Drawing.Point(88, 349);
            this.rdoManager.Name = "rdoManager";
            this.rdoManager.Size = new System.Drawing.Size(59, 16);
            this.rdoManager.TabIndex = 6;
            this.rdoManager.Text = "관리자";
            this.rdoManager.UseVisualStyleBackColor = true;
            // 
            // rdoOperator
            // 
            this.rdoOperator.AutoSize = true;
            this.rdoOperator.Checked = true;
            this.rdoOperator.Location = new System.Drawing.Point(205, 349);
            this.rdoOperator.Name = "rdoOperator";
            this.rdoOperator.Size = new System.Drawing.Size(59, 16);
            this.rdoOperator.TabIndex = 7;
            this.rdoOperator.TabStop = true;
            this.rdoOperator.Text = "운영자";
            this.rdoOperator.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::WaterNet.Water_Net.Properties.Resources.Delete;
            this.btnDelete.Location = new System.Drawing.Point(125, 370);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 26);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "삭제";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::WaterNet.Water_Net.Properties.Resources.Save;
            this.btnSave.Location = new System.Drawing.Point(45, 370);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 26);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "저장";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.Image = global::WaterNet.Water_Net.Properties.Resources.Close2;
            this.btnExit.Location = new System.Drawing.Point(205, 370);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(70, 26);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "닫기";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = global::WaterNet.Water_Net.Properties.Resources.LogOn;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(320, 89);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // ugData
            // 
            this.ugData.Dock = System.Windows.Forms.DockStyle.Top;
            this.ugData.Location = new System.Drawing.Point(0, 89);
            this.ugData.Name = "ugData";
            this.ugData.Size = new System.Drawing.Size(320, 132);
            this.ugData.TabIndex = 35;
            this.ugData.Text = "사용자";
            this.ugData.Click += new System.EventHandler(this.ugData_Click);
            // 
            // frmUserManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 401);
            this.Controls.Add(this.ugData);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.rdoOperator);
            this.Controls.Add(this.rdoManager);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtUDivision);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtUName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNCPWD);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPWD);
            this.Controls.Add(this.txtUID);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "water-NET 사용자 관리";
            this.Load += new System.EventHandler(this.frmUserManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNCPWD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPWD;
        private System.Windows.Forms.TextBox txtUID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUDivision;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rdoManager;
        private System.Windows.Forms.RadioButton rdoOperator;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDelete;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugData;
    }
}