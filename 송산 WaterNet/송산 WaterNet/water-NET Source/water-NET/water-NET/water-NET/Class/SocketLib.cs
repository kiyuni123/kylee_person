﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace WaterNet.Water_Net.Class
{
    class SocketLib
    {
        #region Delegates

        public delegate void ConnectionDelegate(Socket soc);
        public delegate void ErrorDelegate(string ErroMessage, Socket soc, int ErroCode);

        #endregion


        #region Events

        public event ConnectionDelegate OnConnect;
        public event ConnectionDelegate OnDisconnect;
        public event ConnectionDelegate OnRead;
        public event ConnectionDelegate OnWrite;
        public event ErrorDelegate OnError;
        public event ConnectionDelegate OnSendFile;

        #endregion


        #region Variables

        private AsyncCallback WorkerCallBack;
        private Socket MainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); 
        private IPEndPoint ServerEndPoint;  
        private byte[] DataBuffer = new byte[4096];  
        private int mPort = 0;
        private byte[] mBytesReceived;
        private string mDataReceived = "";
        private string mDataSent = ""; 
        private string mRemoteAddress = "";
        private string mRemoteHost = "";

        #endregion


        #region Propetiers

        /// <summary>
        /// Port to connect to server
        /// </summary>
        public int Port
        {
            get
            {
                return (mPort);
            }
        }

        /// <summary>
        /// Bytes received by the Socket
        /// </summary>
        public byte[] ReceivedBytes
        {
            get
            {
                byte[] temp = null;
                if (mBytesReceived != null)
                {
                    temp = mBytesReceived;
                    mBytesReceived = null;
                }
                return (temp);
            }
        }

        /// <summary>
        /// Message received by the Socket
        /// </summary>
        public string ReceivedData
        {
            get
            {
                string temp = mDataReceived;
                mDataReceived = "";
                return (temp);
            }
        }

        /// <summary>
        /// Message send by the Socket
        /// </summary>
        public string WriteData
        {
            get
            {
                string temp = mDataSent;
                mDataSent = "";
                return (temp);
            }
        }

        /// <summary>
        /// IP Server
        /// </summary>
        public string RemoteAddress
        {
            get
            {
                if (MainSocket.Connected)
                {
                    return (mRemoteAddress);
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Host Server
        /// </summary>
        public string RemoteHost
        {
            get
            {
                if (MainSocket.Connected)
                {
                    return (mRemoteHost);
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Return true if the ClientSocket is connected to the Server
        /// </summary>
        public bool Connected
        {
            get
            {
                MainSocket.Poll(1000, SelectMode.SelectWrite);
                return (MainSocket.Connected);
            }
        }

        #endregion


        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="port">Port to connection
        /// </param>
        public SocketLib(string strIPAddr, int intPort)
        {
            try
            {
                mPort = intPort;
                IPAddress IPAddress = IPAddress.Parse(strIPAddr);
                mRemoteAddress = IPAddress.ToString();
                IPHostEntry IPHostEntry = Dns.GetHostEntry(mRemoteAddress);
                mRemoteHost = IPHostEntry.HostName;
                ServerEndPoint = new IPEndPoint(IPAddress, intPort);
            }
            catch (Exception ex)
            {
                if (OnError != null)
                {
                    OnError(ex.Message, null, 0);
                }
            }
        }

        #endregion


        #region Functions and Events

        /// <summary>
        /// Establishes connection with the IP and Port Server
        /// </summary>
        public bool Connect()
        {
            try
            {
                //Connect to Server
                MainSocket.BeginConnect(ServerEndPoint, new AsyncCallback(ConfirmConnect), null);
                return true;
            }
            catch (ArgumentException ex)
            {
                if (OnError != null)
                {
                    OnError(ex.Message, null, 0);
                }
                return false;
            }
            catch (InvalidOperationException ex)
            {
                if (OnError != null)
                {
                    OnError(ex.Message, null, 0);
                }
                return false;
            }
            catch (SocketException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, MainSocket, se.ErrorCode);
                }
                return false;
            }
        }

        private void ConfirmConnect(IAsyncResult asyn)
        {
            try
            {
                MainSocket.EndConnect(asyn);
                WaitForData(MainSocket);
                if (OnConnect != null)
                {
                    OnConnect(MainSocket);
                }
            }
            catch (ObjectDisposedException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
            }
            catch (SocketException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
            }
        }

        private void WaitForData(Socket soc)
        {
            try
            {
                if (WorkerCallBack == null)
                {
                    WorkerCallBack = new AsyncCallback(OnDataReceived);
                }
                soc.BeginReceive(DataBuffer, 0, DataBuffer.Length, SocketFlags.None, WorkerCallBack, null);
            }
            catch (SocketException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, soc, se.ErrorCode);
                }
            }
        }

        private void OnDataReceived(IAsyncResult asyn)
        {
            try
            {
                int iRx = MainSocket.EndReceive(asyn);
                if (iRx < 1)
                {   
                    //MainSocket.Close();
                    //if (!MainSocket.Connected)
                    //    if (OnDisconnect != null)
                    //        OnDisconnect(MainSocket);
                }
                else
                {
                    mBytesReceived = DataBuffer;
                    char[] chars = new char[iRx + 1];
                    Decoder dec = Encoding.UTF8.GetDecoder();
                    dec.GetChars(DataBuffer, 0, iRx, chars, 0);
                    mDataReceived = new String(chars);
                    if (OnRead != null)
                    {
                        OnRead(MainSocket);
                    }
                    WaitForData(MainSocket);
                }
            }
            catch (ArgumentException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
            }
            catch (InvalidOperationException se)
            {
                MainSocket.Close();
                if (!MainSocket.Connected)
                {
                    if (OnDisconnect != null)
                    {
                        OnDisconnect(MainSocket);
                    }
                    else
                    {
                        if (OnError != null)
                        {
                            OnError(se.Message, null, 0);
                        }
                    }
                }
            }
            catch (SocketException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, MainSocket, se.ErrorCode);
                }
                if (!MainSocket.Connected)
                {
                    if (OnDisconnect != null)
                    {
                        OnDisconnect(MainSocket);
                    }
                }
            }
        }

        /// <summary>
        /// Send a text message
        /// </summary>
        /// <param name="mens">Message</param>
        public bool SendData(string mens)
        {
            try
            {
                byte[] byData = System.Text.Encoding.UTF8.GetBytes(mens);
                int NumBytes = MainSocket.Send(byData);
                if (NumBytes == byData.Length)
                {
                    if (OnWrite != null)
                    {
                        mDataSent = mens;
                        OnWrite(MainSocket);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (ArgumentException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
                return false;
            }
            catch (ObjectDisposedException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
                return false;
            }
            catch (SocketException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, MainSocket, se.ErrorCode);
                }
                return false;
            }
        }

        /// <summary>
        /// Send file
        /// </summary>
        /// <param name="FileName">Path File</param>
        public bool SendFile(string FileName)
        {
            try
            {
                MainSocket.BeginSendFile(FileName, new AsyncCallback(FileSendCallback), MainSocket);
                return true;
            }
            catch (FileNotFoundException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
                return false;
            }
            catch (ObjectDisposedException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
                return false;
            }
            catch (SocketException se)
            {
                if (OnError != null)
                { 
                    OnError(se.Message, MainSocket, se.ErrorCode);
                }
                return false;
            }
        }

        /// <summary>
        /// Send file
        /// </summary>
        /// <param name="FileName">Path File</param>
        /// <param name="PreString">Message sent before the file</param>
        /// <param name="PosString">Message sent after the File</param>
        public bool SendFile(string FileName, string PreString, string PosString)
        {
            try
            {
                byte[] preBuf = Encoding.UTF8.GetBytes(PreString);
                byte[] postBuf = Encoding.UTF8.GetBytes(PosString);
                MainSocket.BeginSendFile(FileName, preBuf, postBuf, 0, new AsyncCallback(FileSendCallback), MainSocket);
                return true;
            }
            catch (ArgumentException se)
            {
                if (OnError != null)
                { 
                    OnError(se.Message, null, 0);
                }
                return false;
            }
            catch (ObjectDisposedException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, null, 0);
                }
                return false;
            }
            catch (SocketException se)
            {
                if (OnError != null)
                {
                    OnError(se.Message, MainSocket, se.ErrorCode);
                }
                return false;
            }
        }

        private void FileSendCallback(IAsyncResult ar)
        {
            Socket workerSocket = (Socket)ar.AsyncState;
            workerSocket.EndSendFile(ar);
            if (OnSendFile != null)
            {
                OnSendFile(workerSocket);
            }
        }

        /// <summary>
        /// Close connection to the server
        /// </summary>
        public bool Disconnect()
        {
            MainSocket.Close();
            if (!MainSocket.Connected)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
