﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

namespace WaterNet.Water_Net.dao
{
    class WarningDao
    {
        private static WarningDao dao = null;

        private WarningDao()
        {
        }

        public static WarningDao GetInstance()
        {
            if (dao == null)
            {
                dao = new WarningDao();
            }

            return dao;
        }

        //경고리스트 조회 (메인화면)
        public DataSet SelectWarningList(OracleDBManager dbManager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      CREATE_DATE			                                                            ");
            queryString.AppendLine("            ,WORK_GBN			                                                            ");
            queryString.AppendLine("            ,LOC_CODE			                                                            ");
            queryString.AppendLine("            ,WAR_CODE			                                                            ");
            queryString.AppendLine("            ,CHECK_YN			                                                            ");
            queryString.AppendLine("            ,SERIAL_NO			                                                            ");
            queryString.AppendLine("            ,FIXED_DATE			                                                            ");
            queryString.AppendLine("            ,FIXED_YN			                                                            ");
            queryString.AppendLine("            ,REMARK			                                                                ");
            queryString.AppendLine("            ,PCODE_NAME     as WORK_GBN_NAME			                                    ");
            queryString.AppendLine("            ,CODE_NAME		as WAR_CODE_NAME	                                            ");
            queryString.AppendLine("            ,LOC_NAME			                                                            ");
            queryString.AppendLine("            ,WARN_NO			                                                            ");
            queryString.AppendLine("from        (            			                                                        ");
            queryString.AppendLine("                select      a.CREATE_DATE			                                        ");
            queryString.AppendLine("                            ,a.WORK_GBN			                                            ");
            queryString.AppendLine("                            ,a.LOC_CODE			                                            ");
            queryString.AppendLine("                            ,a.WAR_CODE			                                            ");
            queryString.AppendLine("                            ,a.CHECK_YN			                                            ");
            queryString.AppendLine("                            ,a.SERIAL_NO			                                        ");
            queryString.AppendLine("                            ,a.FIXED_DATE			                                        ");
            queryString.AppendLine("                            ,a.FIXED_YN			                                            ");
            queryString.AppendLine("                            ,a.REMARK			                                            ");
            queryString.AppendLine("                            ,a.WARN_NO			                                            ");
            queryString.AppendLine("                            ,b.PCODE_NAME			                                        ");
            queryString.AppendLine("                            ,c.CODE_NAME			                                        ");
            queryString.AppendLine("                            ,d.LOC_NAME			                                            ");
            queryString.AppendLine("                from        WA_WARNING      a			                                    ");
            queryString.AppendLine("                            ,CM_CODE_MASTER b			                                    ");
            queryString.AppendLine("                            ,CM_CODE        c			                                    ");
            queryString.AppendLine("                            ,CM_LOCATION    d			                                    ");
            queryString.AppendLine("                where       b.PCODE                     = a.WORK_GBN			            ");
            queryString.AppendLine("                and         c.PCODE                     = a.WORK_GBN			            ");
            queryString.AppendLine("                and         c.CODE                      = a.WAR_CODE			            ");
            queryString.AppendLine("                and         a.LOC_CODE                  = d.LOC_CODE(+)				        ");
            queryString.AppendLine("                and         to_date(a.CREATE_DATE,'yyyy-mm-dd hh24:mi:ss') > sysdate - 4    ");
            queryString.AppendLine("             )											                                    ");
            queryString.AppendLine("order by     to_date(CREATE_DATE,'yyyy-mm-dd hh24:mi:ss') desc					            ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WA_WARNING");
        }

        //확인여부 update
        public void UpdateCheckYn(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WA_WARNING set                                              ");
            queryString.AppendLine("        CHECK_YN       = 'Y'                                        ");
            queryString.AppendLine("        ,FIXED_DATE     = to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')  ");
            queryString.AppendLine("where   WARN_NO         = '" + conditions["WARN_NO"] + "'           ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //경고리스트 조회 (경고리스트)
        public DataSet SelectWarningHistoryList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();


            queryString.AppendLine("select      a.CREATE_DATE			                                        ");
            queryString.AppendLine("            ,a.WORK_GBN			                                            ");
            queryString.AppendLine("            ,a.LOC_CODE			                                            ");
            queryString.AppendLine("            ,a.WAR_CODE			                                            ");
            queryString.AppendLine("            ,a.CHECK_YN			                                            ");
            queryString.AppendLine("            ,a.SERIAL_NO			                                        ");
            queryString.AppendLine("            ,a.FIXED_DATE			                                        ");
            queryString.AppendLine("            ,a.FIXED_YN			                                            ");
            queryString.AppendLine("            ,a.REMARK			                                            ");
            queryString.AppendLine("            ,a.WARN_NO			                                            ");
            queryString.AppendLine("            ,b.PCODE_NAME	as WORK_GBN_NAME		                        ");
            queryString.AppendLine("            ,c.CODE_NAME	as WAR_CODE_NAME		                        ");
            queryString.AppendLine("            ,d.LOC_NAME			                                            ");
            queryString.AppendLine("            ,'False'        as SELECTED                                     ");
            queryString.AppendLine("from        WA_WARNING      a			                                    ");
            queryString.AppendLine("            ,CM_CODE_MASTER b			                                    ");
            queryString.AppendLine("            ,CM_CODE        c			                                    ");
            queryString.AppendLine("            ,CM_LOCATION    d			                                    ");
            queryString.AppendLine("where       1 = 1                                                           ");

            if (conditions["WORK_GBN"] != null)
            {
                queryString.AppendLine("and        a.WORK_GBN               = '" + conditions["WORK_GBN"] + "'  ");
            }

            if (conditions["WAR_CODE"] != null)
            {
                queryString.AppendLine("and         a.WAR_CODE              = '" + conditions["WAR_CODE"] + "'  ");
            }

            if (conditions["LOC_CODE"] != null)
            {
                queryString.AppendLine("and         a.LOC_CODE              = '" + conditions["LOC_CODE"] + "'  ");
            }

            if (conditions["CHECK_YN"] != null)
            {
                queryString.AppendLine("and         a.CHECK_YN              = '" + conditions["CHECK_YN"] + "'  ");
            }

            if (conditions["FIXED_YN"] != null)
            {
                queryString.AppendLine("and         a.FIXED_YN              = '" + conditions["FIXED_YN"] + "'  ");
            }
            
            queryString.AppendLine("and         b.PCODE                     = a.WORK_GBN			                    ");
            queryString.AppendLine("and         c.PCODE                     = a.WORK_GBN			                    ");
            queryString.AppendLine("and         c.CODE                      = a.WAR_CODE			                    ");
            queryString.AppendLine("and         a.LOC_CODE                  = d.LOC_CODE(+)				                ");
            queryString.AppendLine("and         to_date(a.CREATE_DATE,'yyyy-mm-dd hh24:mi:ss')                          ");
            queryString.AppendLine("between     to_date('" + conditions["startDate"] + "000000','yyyymmddhh24miss')     ");
            queryString.AppendLine("and         to_date('" + conditions["endDate"] + "235959','yyyymmddhh24miss')       ");
            queryString.AppendLine("order by    to_date(a.CREATE_DATE,'yyyy-mm-dd hh24:mi:ss')          desc            ");
           
            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WA_WARNING");
        }


    }
}
