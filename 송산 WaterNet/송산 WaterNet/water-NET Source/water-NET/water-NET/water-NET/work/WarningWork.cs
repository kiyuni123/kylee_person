﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WaterNet.WaterNetCore;
using WaterNet.Water_Net.dao;

namespace WaterNet.Water_Net.work
{
    public class WarningWork
    {
        private static WarningWork work = null;
        private WarningDao dao = null;

        private WarningWork()
        {
            dao = WarningDao.GetInstance();
        }

        public static WarningWork GetInstance()
        {
            if (work == null)
            {
                work = new WarningWork();
            }

            return work;
        }

        //경고리스트 조회
        public DataSet SelectWarningList()
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = dao.SelectWarningList(dbManager);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //경고확인
        public DataSet UpdateWarningList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.UpdateCheckYn(dbManager, conditions);
                result = dao.SelectWarningList(dbManager);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //경고리스트 조회
        public DataSet SelectWarningHistoryList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = dao.SelectWarningHistoryList(dbManager, conditions);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //경고리스트 일괄처리
        public DataSet UpdateCheckYnAll(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();


                if (conditions["dataSource"] != null)
                {
                    DataTable tmpTable = (DataTable)conditions["dataSource"];

                    foreach (DataRow row in tmpTable.Rows)
                    {
                        if ("True".Equals(row["SELECTED"].ToString()))
                        {
                            Hashtable checkConditions = new Hashtable();

                            checkConditions.Add("WARN_NO", row["WARN_NO"].ToString());

                            dao.UpdateCheckYn(dbManager,checkConditions);
                        }
                    }
                }

                result = dao.SelectWarningHistoryList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
            }


            return result;
        }
    }
}
