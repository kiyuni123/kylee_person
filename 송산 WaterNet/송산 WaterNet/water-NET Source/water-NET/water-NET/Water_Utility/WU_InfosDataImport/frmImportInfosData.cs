﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

namespace WaterNet.WU_InfosDataImport
{
    public partial class frmImportInfosData : Form
    {
        private DataSet m_ExcelDataSet = null;
        private WaterNetCore.OracleDBManager oDBManager = new WaterNetCore.OracleDBManager();
        private DataSet m_CodeDataSet = new DataSet();

        private string m_Excelfile = string.Empty;

        public frmImportInfosData()
        {
            InitializeComponent();

            InitializeSetting();
        }

        private void frmImportInfosData_Load(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["tsMenuInfosImport"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }
        }

        private void InitializeSetting()
        {
            WaterNetCore.FormManager.SetGridStyle(this.GridExcelData);

            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            oDBManager.Open();

            StringBuilder query = new StringBuilder();
            query.AppendLine("SELECT * FROM CM_LOCATION WHERE FTR_CODE IN ('BZ001','BZ002','BZ003')");
            DataTable table1 = oDBManager.ExecuteScriptDataTable(query.ToString(), null, "LOCATION");
            this.m_CodeDataSet.Tables.Add(table1);

            query.Remove(0, query.Length);
            query.AppendLine("SELECT * FROM CM_CODE");
            DataTable table2 = oDBManager.ExecuteScriptDataTable(query.ToString(), null, "CODE");
            this.m_CodeDataSet.Tables.Add(table2);
        }

        #region 엑셀
        private string getConnectionString(string filename, bool showHeader)
        {
            StringBuilder connectionString = new StringBuilder();

            connectionString.AppendFormat(@"Provider=Microsoft.Jet.OLEDB.4.0;");
            connectionString.AppendFormat(@"Data Source={0};", filename);
            connectionString.AppendFormat(@"Extended Properties='Excel 8.0;");
            //if (showHeader) connectionString.AppendFormat(@"HDR=No;");
            //else connectionString.AppendFormat(@"HDR=Yes;");
            connectionString.AppendFormat(@"HDR=No;");
            connectionString.AppendFormat(@"IMEX=1;'");     // 데이터타입을 Text, Majority Type으로 사용하기 위함

            return connectionString.ToString();
        }

        private string getConnectionString2(string filename, bool showHeader)
        {
            StringBuilder connectionString = new StringBuilder();

            connectionString.AppendFormat(@"Provider=Microsoft Office 12.0 Access Database Engine OLE DB Provider;");
            connectionString.AppendFormat(@"Data Source={0};", filename);
            connectionString.AppendFormat(@"Extended Properties='Excel 12.0;");
            //if (showHeader) connectionString.AppendFormat(@"HDR=No;");
            //else connectionString.AppendFormat(@"HDR=Yes;");
            connectionString.AppendFormat(@"HDR=No;");
            connectionString.AppendFormat(@"IMEX=1;'");     // 데이터타입을 Text, Majority Type으로 사용하기 위함

            return connectionString.ToString();
        }

        private DataSet Import(string FileName)
        {
            System.Data.OleDb.OleDbConnection oleDbConnection = null;
            System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter = null;
            DataSet ds = null;

            string ext = WaterNetCore.FunctionManager.GetFileExtension(FileName);

            try
            {
                oleDbConnection = new System.Data.OleDb.OleDbConnection();
                if (ext.Equals(".xls"))
                {
                    oleDbConnection.ConnectionString = getConnectionString(FileName, true);
                }
                else if (ext.Equals(".xlsx"))
                {
                    oleDbConnection.ConnectionString = getConnectionString2(FileName, true);
                }
                else return null;
                oleDbConnection.Open();

                ds = new DataSet();
                if (oleDbConnection.GetSchema("Tables").Rows.Count == 0) return null;
                string sheetname = oleDbConnection.GetSchema("Tables").Rows[0]["TABLE_NAME"].ToString();

                string query = String.Format("SELECT * FROM [{0}]", oleDbConnection.GetSchema("Tables").Rows[0]["TABLE_NAME"].ToString());
                oleDbDataAdapter = new System.Data.OleDb.OleDbDataAdapter(query, oleDbConnection);
                oleDbDataAdapter.Fill(ds, oleDbConnection.GetSchema("Tables").Rows[0]["TABLE_NAME"].ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oleDbDataAdapter != null)
                {
                    oleDbDataAdapter.Dispose();
                    oleDbDataAdapter = null;
                }
                if (oleDbConnection != null)
                {
                    if (oleDbConnection.State == ConnectionState.Open) oleDbConnection.Close();
                    oleDbConnection.Dispose();
                    oleDbConnection = null;
                }
            }

            return ds;
        }
        #endregion 엑셀

        #region 코드 찾기
        /// <summary>
        /// 대블록 코드 찾기
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private string GetLargeBlockCode(string value)
        {
            string result = string.Empty;
            try
            {
                //if (Convert.IsDBNull(row["대블럭"])) return result;

                DataRow[] rows = this.m_CodeDataSet.Tables["LOCATION"].Select("LOC_NAME = '" + value + "' AND FTR_CODE = 'BZ001'");
                if (rows.Count() > 0)
                {
                    result = Convert.ToString(rows[0]["FTR_IDN"]);
                }
            }
            catch (Exception)
            {
                ;
            }

            return result;
        }

        /// <summary>
        /// 중블록 코드 찾기
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private string GetMiddleBlockCode(string value)
        {
            string result = string.Empty;

            try
            {
                DataRow[] rows = this.m_CodeDataSet.Tables["LOCATION"].Select("LOC_NAME = '" + value + "' AND FTR_CODE = 'BZ002'");
                if (rows.Count() > 0)
                {
                    result = Convert.ToString(rows[0]["FTR_IDN"]);
                }
            }
            catch (Exception)
            {
                ;
            }

            return result;
        }

        /// <summary>
        /// 소블록 코드 찾기
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private string GetSmallBlockCode(string value)
        {
            string result = string.Empty;
            try
            {
                DataRow[] rows = this.m_CodeDataSet.Tables["LOCATION"].Select("LOC_NAME = '" + value + "' AND FTR_CODE = 'BZ003'");
                if (rows.Count() > 0)
                {
                    result = Convert.ToString(rows[0]["FTR_IDN"]);
                }
            }
            catch (Exception)
            {
                ;
            }

            return result;
        }

        /// <summary>
        /// CM_CDOE의 코드값 찾기
        /// </summary>
        /// <param name="pcode"></param>
        /// <param name="codename"></param>
        /// <returns></returns>
        private string GetCodeValue(string pcode, string codename)
        {
            string result = string.Empty;

            DataRow[] rows = this.m_CodeDataSet.Tables["CODE"].Select("PCODE = '" + pcode + "' AND CODE_NAME = '" + codename + "'");
            if (rows.Count() > 0)
            {
                result = Convert.ToString(rows[0]["CODE"]);
            }
            return result;
        }
        #endregion 코드 찾기

        /// <summary>
        /// 엑셀파일 불러오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExcel_Click(object sender, EventArgs e)
        {
            OpenFileDialog oOFD = new OpenFileDialog();
            oOFD.Filter = "엑셀 파일|*.xls;*.xlsx";
            oOFD.Multiselect = true;
            oOFD.FilterIndex = 1;
            oOFD.RestoreDirectory = true;
            if (oOFD.ShowDialog() != DialogResult.OK) return;

            m_ExcelDataSet = Import(oOFD.FileName);
            if (m_ExcelDataSet == null) return;
            if (m_ExcelDataSet.Tables.Count == 0) return;

            this.cboSheet.Items.Clear();
            foreach (DataTable table in m_ExcelDataSet.Tables)
            {
                this.cboSheet.Items.Add(table.TableName);    
            }

            this.cboSheet.SelectedIndex = 0;
        }

        /// <summary>
        /// 계량기교체-Columns = 22
        /// </summary>
        /// <param name="table"></param>
        void setChangeMeta1(DataTable table)
        {
            table.Columns["F1"].ColumnName = "수용가번호";
            table.Columns["F2"].ColumnName = "수용가명";
            table.Columns["F3"].ColumnName = "수전번호";
            table.Columns["F4"].ColumnName = "수용가주소";
            table.Columns["F5"].ColumnName = "전화번호";
            table.Columns["F6"].ColumnName = "휴대폰번호";
            table.Columns["F7"].ColumnName = "교체구분";
            table.Columns["F8"].ColumnName = "교체일자";
            table.Columns["F9"].ColumnName = "제작번호(교체전)";
            table.Columns["F10"].ColumnName = "구경(교체전)";
            table.Columns["F11"].ColumnName = "철거지침(교체전)";
            table.Columns["F12"].ColumnName = "제작번호(교체후)";
            table.Columns["F13"].ColumnName = "구경(교체후)";
            table.Columns["F14"].ColumnName = "설치지침(교체후)";
            table.Columns["F15"].ColumnName = "소블록";
            table.Columns["F16"].ColumnName = "배수지";
            table.Columns["F17"].ColumnName = "봉인번호";
            table.Columns["F18"].ColumnName = "검침적용년월";
            table.Columns["F19"].ColumnName = "변경사유";
            table.Columns["F20"].ColumnName = "검침원";
            table.Columns["F21"].ColumnName = "가구구분";


            ///주석Rows 삭제
            for (int i = 2; i > 0; i--)
                table.Rows.RemoveAt(i - 1);
            ///마지막 Row 삭제 - 합계Row
            table.Rows.RemoveAt(table.Rows.Count - 1);

            ///엑셀에서 병합된 데이터가 있는경우 null data인경우 데이터생성해준다.
            DataRow[] rows1 = table.Select("수용가번호 is null");
            foreach (DataRow row in rows1)
            {
                int i = table.Rows.IndexOf(row);
                if (!(i > 1)) continue;

                row["수용가번호"] = table.Rows[i - 1]["수용가번호"];
                row["수용가명"] = table.Rows[i - 1]["수용가명"];
                row["수전번호"] = table.Rows[i - 1]["수전번호"];
                row["수용가주소"] = table.Rows[i - 1]["수용가주소"];
                row["전화번호"] = table.Rows[i - 1]["전화번호"];
                row["휴대폰번호"] = table.Rows[i - 1]["휴대폰번호"];
                row["가구구분"] = table.Rows[i - 1]["가구구분"];
            }

            foreach (DataRow row in table.Rows)
            {
                //교체일자->DateTime으로 변환
                row["교체일자"] = Convertion.toDate(row["교체일자"]);

                //구경(문자)->숫자로 변환
                row["구경(교체전)"] = Convert.ToString(row["구경(교체전)"]).Replace("mm", string.Empty);
                row["구경(교체후)"] = Convert.ToString(row["구경(교체후)"]).Replace("mm", string.Empty);

                ////수용가번호 "-"문자 삭제
                //s = Convert.ToString(row["수용가번호"]).Replace("-", string.Empty);
                //row["수용가번호"] = s;

                ////철거지침, 설치지침 ","문자 삭제
                //s = Convert.ToString(row["철거지침(교체전)"]).Replace(",", string.Empty);
                //row["철거지침(교체전)"] = s;

                //s = Convert.ToString(row["설치지침(교체후)"]).Replace(",", string.Empty);
                //row["설치지침(교체후)"] = s;
            }

            table.AcceptChanges();

        }

        /// <summary>
        /// 계량기교체-Columns = 24(동두천)
        /// </summary>
        /// <param name="table"></param>
        void setChangeMeta2(DataTable table)
        {
            table.Columns["F1"].ColumnName = "수용가번호";
            table.Columns["F2"].ColumnName = "수용가명";
            table.Columns["F3"].ColumnName = "수전번호";
            table.Columns["F4"].ColumnName = "수용가주소";
            table.Columns["F5"].ColumnName = "전화번호";
            table.Columns["F6"].ColumnName = "휴대폰번호";
            table.Columns["F7"].ColumnName = "교체구분";
            table.Columns["F8"].ColumnName = "교체일자";
            table.Columns["F9"].ColumnName = "제작번호(교체전)";
            table.Columns["F10"].ColumnName = "구경(교체전)";
            table.Columns["F11"].ColumnName = "철거지침(교체전)";
            table.Columns["F12"].ColumnName = "제작번호(교체후)";
            table.Columns["F13"].ColumnName = "구경(교체후)";
            table.Columns["F14"].ColumnName = "설치지침(교체후)";
            table.Columns["F15"].ColumnName = "대블록";
            table.Columns["F16"].ColumnName = "중블록";
            table.Columns["F17"].ColumnName = "소블록";
            table.Columns["F18"].ColumnName = "배수지";
            table.Columns["F19"].ColumnName = "봉인번호";
            table.Columns["F20"].ColumnName = "검침적용년월";
            table.Columns["F21"].ColumnName = "변경사유";
            table.Columns["F22"].ColumnName = "검침원";
            table.Columns["F23"].ColumnName = "가구구분";

            ///주석Rows 삭제
            for (int i = 2; i > 0; i--)
                table.Rows.RemoveAt(i - 1);
            ///마지막 Row 삭제 - 합계Row
            table.Rows.RemoveAt(table.Rows.Count - 1);

            ///엑셀에서 병합된 데이터가 있는경우 null data인경우 데이터생성해준다.
            DataRow[] rows1 = table.Select("수용가번호 is null");
            foreach (DataRow row in rows1)
            {
                int i = table.Rows.IndexOf(row);
                if (!(i > 1)) continue;

                row["수용가번호"] = table.Rows[i - 1]["수용가번호"];
                row["수용가명"] = table.Rows[i - 1]["수용가명"];
                row["수전번호"] = table.Rows[i - 1]["수전번호"];
                row["수용가주소"] = table.Rows[i - 1]["수용가주소"];
                row["전화번호"] = table.Rows[i - 1]["전화번호"];
                row["휴대폰번호"] = table.Rows[i - 1]["휴대폰번호"];
                row["가구구분"] = table.Rows[i - 1]["가구구분"];
            }

            foreach (DataRow row in table.Rows)
            {
                //교체일자->DateTime으로 변환
                row["교체일자"] = Convertion.toDate(row["교체일자"]);

                //구경(문자)->숫자로 변환
                row["구경(교체전)"] = Convert.ToString(row["구경(교체전)"]).Replace("mm", string.Empty);
                row["구경(교체후)"] = Convert.ToString(row["구경(교체후)"]).Replace("mm", string.Empty);

                ////수용가번호 "-"문자 삭제
                //s = Convert.ToString(row["수용가번호"]).Replace("-", string.Empty);
                //row["수용가번호"] = s;

                ////철거지침, 설치지침 ","문자 삭제
                //s = Convert.ToString(row["철거지침(교체전)"]).Replace(",", string.Empty);
                //row["철거지침(교체전)"] = s;

                //s = Convert.ToString(row["설치지침(교체후)"]).Replace(",", string.Empty);
                //row["설치지침(교체후)"] = s;
            }

            table.AcceptChanges();
        }

        /// <summary>
        /// 계량기교체-Columns = 26(단양)
        /// </summary>
        /// <param name="table"></param>
        void setChangeMeta3(DataTable table)
        {
            table.Columns["F2"].ColumnName = "수용가번호";
            table.Columns["F3"].ColumnName = "수용가명";
            table.Columns["F4"].ColumnName = "상호명";
            table.Columns["F5"].ColumnName = "수전번호";
            table.Columns["F6"].ColumnName = "교체구분";
            table.Columns["F7"].ColumnName = "교체일자";
            table.Columns["F8"].ColumnName = "검침적용년월";
            table.Columns["F9"].ColumnName = "제작번호(교체전)";
            table.Columns["F10"].ColumnName = "구경(교체전)";
            table.Columns["F11"].ColumnName = "철거지침(교체전)";
            table.Columns["F12"].ColumnName = "제작번호(교체후)";
            table.Columns["F13"].ColumnName = "구경(교체후)";
            table.Columns["F14"].ColumnName = "설치지침(교체후)";
            table.Columns["F15"].ColumnName = "계량기봉인번호";
            table.Columns["F16"].ColumnName = "수용가주소";
            table.Columns["F17"].ColumnName = "소블록";
            table.Columns["F18"].ColumnName = "계량기대금";
            table.Columns["F19"].ColumnName = "제작회사(교체전)";
            table.Columns["F20"].ColumnName = "제작회사(교체후)";
            table.Columns["F21"].ColumnName = "변경사유";
            table.Columns["F22"].ColumnName = "검침년월(교체전)";
            table.Columns["F23"].ColumnName = "검침일자(교체전)";
            table.Columns["F24"].ColumnName = "전화번호";
            table.Columns["F25"].ColumnName = "휴대폰번호";

            //0번 컬럼은 순번이므로 삭제
            table.Columns.Remove("F1");

            ///주석Rows 삭제
            for (int i = 2; i > 0; i--)
                table.Rows.RemoveAt(i - 1);
            ///마지막 Row 삭제 - 합계Row
            table.Rows.RemoveAt(table.Rows.Count - 1);

            ///엑셀에서 병합된 데이터가 있는경우 null data인경우 데이터생성해준다.
            DataRow[] rows1 = table.Select("수용가번호 is null");
            foreach (DataRow row in rows1)
            {
                int i = table.Rows.IndexOf(row);
                if (!(i > 1)) continue;

                row["수용가번호"] = table.Rows[i - 1]["수용가번호"];
                row["수용가명"] = table.Rows[i - 1]["수용가명"];
                row["수전번호"] = table.Rows[i - 1]["수전번호"];
                row["수용가주소"] = table.Rows[i - 1]["수용가주소"];
                row["전화번호"] = table.Rows[i - 1]["전화번호"];
                row["휴대폰번호"] = table.Rows[i - 1]["휴대폰번호"];
            }

            foreach (DataRow row in table.Rows)
            {
                //교체일자->DateTime으로 변환
                DateTime d;
                if (DateTime.TryParse(DateTime.FromOADate(Convert.ToDouble(row["교체일자"])).ToString(), out d))
                    row["교체일자"] = d.ToShortDateString();
                else
                    row["교체일자"] = "1900-01-01";

                //구경(문자)->숫자로 변환
                string s = Convert.ToString(row["구경(교체전)"]).Replace("mm", string.Empty);
                row["구경(교체전)"] = s;
                s = Convert.ToString(row["구경(교체후)"]).Replace("mm", string.Empty);
                row["구경(교체후)"] = s;

                //수용가번호 "-"문자 삭제
                s = Convert.ToString(row["수용가번호"]).Replace("-", string.Empty);
                row["수용가번호"] = s;

                //철거지침, 설치지침 ","문자 삭제
                s = Convert.ToString(row["철거지침(교체전)"]).Replace(",", string.Empty);
                row["철거지침(교체전)"] = s;

                s = Convert.ToString(row["설치지침(교체후)"]).Replace(",", string.Empty);
                row["설치지침(교체후)"] = s;
            }

            table.AcceptChanges();
        }

        /// <summary>
        /// 민원정보-Columns = 27
        /// </summary>
        /// <param name="table"></param>
        void setMinwon1(DataTable table)
        {
            table.Columns["F2"].ColumnName = "민원번호";
            table.Columns["F3"].ColumnName = "신청일시";
            table.Columns["F4"].ColumnName = "대분류";
            table.Columns["F5"].ColumnName = "중분류";
            table.Columns["F6"].ColumnName = "신청인";
            table.Columns["F7"].ColumnName = "수용가명";
            table.Columns["F8"].ColumnName = "수용가번호";
            table.Columns["F9"].ColumnName = "상태";
            table.Columns["F10"].ColumnName = "접수자";
            table.Columns["F11"].ColumnName = "접수일시";
            table.Columns["F12"].ColumnName = "처리자";
            table.Columns["F13"].ColumnName = "처리일시";
            table.Columns["F14"].ColumnName = "전화번호";
            table.Columns["F15"].ColumnName = "핸드폰번호";
            table.Columns["F16"].ColumnName = "수용가주소";
            table.Columns["F17"].ColumnName = "신청인주소";
            table.Columns["F18"].ColumnName = "민원내용";
            table.Columns["F19"].ColumnName = "처리결과";
            table.Columns["F20"].ColumnName = "VOC";
            table.Columns["F21"].ColumnName = "소블록";
            table.Columns["F22"].ColumnName = "수전번호";
            table.Columns["F23"].ColumnName = "대행업체";
            table.Columns["F24"].ColumnName = "결재상태";
            table.Columns["F25"].ColumnName = "검침원";
            table.Columns["F26"].ColumnName = "등록자";
            table.Columns["F27"].ColumnName = "등록일자";


            //주석Rows 삭제
            for (int i = 1; i > 0; i--)
                table.Rows.RemoveAt(i - 1);

            table.AcceptChanges();
        }

        /// <summary>
        /// 사용량-Columns = 21
        /// </summary>
        /// <param name="table"></param>
        void setUsedVolumn1(DataTable table)
        {
            table.Columns["F1"].ColumnName = "수용가번호";
            table.Columns["F2"].ColumnName = "수용가명";
            table.Columns["F3"].ColumnName = "수전번호";
            table.Columns["F4"].ColumnName = "수용가주소";
            table.Columns["F5"].ColumnName = "소블록";
            table.Columns["F6"].ColumnName = "급수상태";
            table.Columns["F7"].ColumnName = "검침구역";
            table.Columns["F8"].ColumnName = "상수도대표업종";
            table.Columns["F9"].ColumnName = "하수도대표업종";
            table.Columns["F10"].ColumnName = "가구수";
            table.Columns["F11"].ColumnName = "구경";
            table.Columns["F12"].ColumnName = "지침(전월)";
            table.Columns["F13"].ColumnName = "검침량(전월)";
            table.Columns["F14"].ColumnName = "사용량(전월)";
            table.Columns["F15"].ColumnName = "검침일(전월)";
            table.Columns["F16"].ColumnName = "지침(당월)";
            table.Columns["F17"].ColumnName = "검침량(당월)";
            table.Columns["F18"].ColumnName = "사용량(당월)";
            table.Columns["F19"].ColumnName = "검침일(당월)";
            table.Columns["F20"].ColumnName = "검침원";
            table.Columns["F21"].ColumnName = "검침순서";

            //주석Rows 삭제
            for (int i = 2; i > 0; i--)
                table.Rows.RemoveAt(i - 1);

            ///마지막 Row 삭제 - 합계Row
            table.Rows.RemoveAt(table.Rows.Count - 1);

            foreach (DataRow row in table.Rows)
            {
                row["검침일(전월)"] = Convertion.toDate(row["검침일(전월)"]);
                row["검침일(당월)"] = Convertion.toDate(row["검침일(당월)"]);
                //교체일자->DateTime으로 변환
                //DateTime d;
                //if (DateTime.TryParse(DateTime.FromOADate(Convert.ToDouble(row["검침일(전월)"])).ToString(), out d))
                //    row["검침일(전월)"] = d.ToShortDateString();
                //else
                //    row["검침일(전월)"] = "1900-01-01";

                //if (DateTime.TryParse(DateTime.FromOADate(Convert.ToDouble(row["검침일(당월)"])).ToString(), out d))
                //    row["검침일(당월)"] = d.ToShortDateString();
                //else
                //    row["검침일(당월)"] = "1900-01-01";
                //구경(문자)->숫자로 변환
                row["구경"] = Convert.ToString(row["구경"]).Replace("mm", string.Empty);

                ////수용가번호 "-"문자 삭제
                //s = Convert.ToString(row["수용가번호"]).Replace("-", string.Empty);
                //row["수용가번호"] = s;

                ////철거지침, 설치지침 ","문자 삭제
                //s = Convert.ToString(row["철거지침(교체전)"]).Replace(",", string.Empty);
                //row["철거지침(교체전)"] = s;

                //s = Convert.ToString(row["설치지침(교체후)"]).Replace(",", string.Empty);
                //row["설치지침(교체후)"] = s;
            }

            table.AcceptChanges();
        }

        /// <summary>
        /// 사용량-Columns = 67(단양)
        /// </summary>
        /// <param name="table"></param>
        void setUsedVolumn2(DataTable table)
        {
            table.Columns["F3"].ColumnName = "수용가번호";
            table.Columns["F4"].ColumnName = "수용가명";
            table.Columns["F5"].ColumnName = "수전번호";
            table.Columns["F6"].ColumnName = "수용가주소";
            table.Columns["F7"].ColumnName = "소블록";
            table.Columns["F8"].ColumnName = "급수상태";
            table.Columns["F9"].ColumnName = "검침구역";
            table.Columns["F10"].ColumnName = "상수도대표업종";
            table.Columns["F11"].ColumnName = "하수도대표업종";
            table.Columns["F12"].ColumnName = "가구수";
            table.Columns["F13"].ColumnName = "구경";
            table.Columns["F14"].ColumnName = "지침(전월)";
            table.Columns["F15"].ColumnName = "검침량(전월)";
            table.Columns["F16"].ColumnName = "사용량(전월)";
            table.Columns["F17"].ColumnName = "검침일(전월)";
            table.Columns["F18"].ColumnName = "지침(당월)";
            table.Columns["F19"].ColumnName = "검침량(당월)";
            table.Columns["F20"].ColumnName = "사용량(당월)";
            table.Columns["F21"].ColumnName = "검침일(당월)";
            table.Columns["F22"].ColumnName = "검침원";
            table.Columns["F23"].ColumnName = "검침순서";

            //0번 컬럼은 공백, 1번 컬럼은 순번이므로 삭제
            table.Columns.Remove("F1");
            table.Columns.Remove("F2");

            //주석Rows 삭제
            for (int i = 3; i > 0; i--)
                table.Rows.RemoveAt(i - 1);

            ///마지막 Row 삭제 - 합계Row
            table.Rows.RemoveAt(table.Rows.Count - 1);
        }

        /// <summary>
        /// 수용가-Columns = 30, 필드명(Rowindex) = 1
        /// </summary>
        /// <param name="table"></param>
        void setCustomer1(DataTable table)
        {
            table.Columns["F2"].ColumnName = "수용가번호";
            table.Columns["F3"].ColumnName = "수용가명";
            table.Columns["F4"].ColumnName = "상호명";
            table.Columns["F5"].ColumnName = "실사용자명";
            table.Columns["F6"].ColumnName = "수전번호";
            table.Columns["F7"].ColumnName = "주소";
            table.Columns["F8"].ColumnName = "새주소";
            table.Columns["F9"].ColumnName = "업종";
            table.Columns["F10"].ColumnName = "구경";
            table.Columns["F11"].ColumnName = "가구수";
            table.Columns["F12"].ColumnName = "급수상태";
            table.Columns["F13"].ColumnName = "검침구분";
            table.Columns["F14"].ColumnName = "자원구성형태";
            table.Columns["F15"].ColumnName = "최초개시일자";
            table.Columns["F16"].ColumnName = "계기번호";
            table.Columns["F17"].ColumnName = "고지방법";
            table.Columns["F18"].ColumnName = "전화번호";
            table.Columns["F19"].ColumnName = "핸드폰번호";
            table.Columns["F20"].ColumnName = "실사용자연락처";
            table.Columns["F21"].ColumnName = "대블록";
            table.Columns["F22"].ColumnName = "중블록";
            table.Columns["F23"].ColumnName = "소블록";
            table.Columns["F24"].ColumnName = "배수지";
            table.Columns["F25"].ColumnName = "수용가번호(구)";
            table.Columns["F26"].ColumnName = "관리번호(구)";
            table.Columns["F27"].ColumnName = "검침원";
            table.Columns["F28"].ColumnName = "검침순서";
            table.Columns["F29"].ColumnName = "설치지침";
            table.Columns["F30"].ColumnName = "등록자";
            table.Columns["F31"].ColumnName = "등록일자";

            //주석Rows 삭제
            for (int i = 1; i > 0; i--)
                table.Rows.RemoveAt(i - 1);

            table.AcceptChanges();
        }

        /// <summary>
        /// 엑셀 쉬트 선택 : 콤보박스 Hide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSheet.Items.Count == 0) return;
            this.GridExcelData.ResetLayouts();

            DataTable table = m_ExcelDataSet.Tables[cboSheet.SelectedIndex] as DataTable;
            panel2.Visible = false;

            m_Excelfile = string.Empty;


            try
            {
                switch (table.Columns.Count)
                {
                    case 31: //수용가기본정보
                        m_Excelfile = "수용가정보";
                        this.setCustomer1(table);

                        table.Columns.Remove("F1"); //0번 컬럼은 순번이므로 삭제
                        break;                
                   
                    case 23:   //계량기 교체정보-동두천
                        m_Excelfile = "계량기교체정보";
                        this.setChangeMeta2(table);

                        //table.Columns.Remove("F1");  //0번 컬럼은 공백이므로 삭제
                        break;
                    case 27: //민원정보
                        m_Excelfile = "민원정보";
                        this.setMinwon1(table);

                        table.Columns.Remove("F1");  //0번 컬럼은 순번이므로 삭제
                        break;
                    case 21: //검침(사용량)정보, 계량기교체정보
                        Console.WriteLine(string.Format("{0}", table.Rows[0]["F12"]));
                        if (Convert.ToString(table.Rows[0]["F12"]).Equals("전월검침정보"))
                        {
                            m_Excelfile = "검침정보";
                            this.setUsedVolumn1(table);
                            panel2.Visible = true;                           
                        }
                        else if (Convert.ToString(table.Rows[0]["F8"]).Equals("교체일자"))
                        {
                            m_Excelfile = "계량기교체정보";
                            this.setChangeMeta1(table);
                        }
                        else
                        {
                            MessageBox.Show("선택한 엑셀파일은 규정된 양식과 다르므로, 등록할 수 없습니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        break;
                    default:
                        MessageBox.Show("선택한 엑셀파일은 규정된 양식과 다르므로, 등록할 수 없습니다.","안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                }

                GridExcelData.DataSource = m_ExcelDataSet.Tables[cboSheet.SelectedIndex] as DataTable;
                ResetCboMonth();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            } 

        }

        private void ResetCboMonth()
        {
            if (panel2.Visible)
            {
                DataRow row = ((DataTable)this.GridExcelData.DataSource).Rows[0];
                DateTime dt = ValidateDateTime(row["검침일(당월)"]);

                #region 컨트롤 초기화
                //년 콤보박스 초기화
                object[,] aoSource = {
                    {Convert.ToString(dt.AddYears(-2).Year),   Convert.ToString(dt.AddYears(-2).Year) + "년"},
                    {Convert.ToString(dt.AddYears(-1).Year),   Convert.ToString(dt.AddYears(-1).Year) + "년"},
                    {Convert.ToString(dt.Year),   Convert.ToString(dt.Year) + "년"}
                };
                WaterNetCore.FormManager.SetComboBoxEX(this.cboYear, aoSource, false);

                //월 콤보박스 초기화
                object[,] aoSource2 = {
                    {"01",   "1월"},
                    {"02",   "2월"},
                    {"03",   "3월"},
                    {"04",   "4월"},
                    {"05",   "5월"},
                    {"06",   "6월"},
                    {"07",   "7월"},
                    {"08",   "8월"},
                    {"09",   "9월"},
                    {"10",   "10월"},
                    {"11",   "11월"},
                    {"12",   "12월"},
                };
                WaterNetCore.FormManager.SetComboBoxEX(this.cboMonth, aoSource2, false);
                #endregion

                cboYear.SelectedValue = Convert.ToString(dt.Year);
                cboMonth.SelectedValue = dt.Month.ToString().PadLeft(2, '0');
            }
        }

        /// <summary>
        /// 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(m_Excelfile)) return;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                switch (m_Excelfile)
                {
                    case "수용가정보":  //수용가정보, 상하수도자원정보
                        this.UpdateDM_INFO(oDBManager, (DataTable)this.GridExcelData.DataSource); //수용가정보
                        this.UpdateDMWSRSRC(oDBManager, (DataTable)this.GridExcelData.DataSource);  //상하수도자원정보
                        break;
                    case "민원정보":    //민원정보
                        this.UpdateCA_INFO(oDBManager, (DataTable)this.GridExcelData.DataSource);
                        break;
                    case "검침정보":    //수용가정보, 상하수도자원정보, 요금조정
                        this.UpdateSTWCHRG(oDBManager, (DataTable)this.GridExcelData.DataSource);
                        break;
                    case "계량기교체정보":   //수도계량기교체정보
                        this.UpdateMTRCHGINFO(oDBManager, (DataTable)this.GridExcelData.DataSource);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("{0}\n{1}", "데이터 등록중 에러가 발생했습니다.", ex.ToString()));
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        #region 데이터베이스 Update 및 Insert 질의문
        /// <summary>
        /// 민원기본정보 등록 및 수정
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="table"></param>
        private void UpdateCA_INFO(WaterNet.WaterNetCore.OracleDBManager manager, DataTable table)
        {
            StringBuilder query = new StringBuilder();
            //query.AppendLine("merge into wi_cainfo cainfo                  ");
            //query.AppendLine("using (select :CANO cano from dual) a        ");
            //query.AppendLine("  on (cainfo.cano = a.cano)                  ");
            //query.AppendLine("when matched then                            ");
            //query.AppendLine("     update set SGCCD        = :SGCCD        ");
            //query.AppendLine("				,CAAPPLMETH    = :CAAPPLMETH   ");
            //query.AppendLine("				,CAAPPLCLASS   = :CAAPPLCLASS  ");
            //query.AppendLine("				,FRMYN         = :FRMYN        ");
            //query.AppendLine("				,VSTCAYN       = :VSTCAYN      ");
            //query.AppendLine("				,APPRVYN       = :APPRVYN      ");
            //query.AppendLine("				,CALRGCD       = :CALRGCD      ");
            //query.AppendLine("				,CAMIDCD       = :CAMIDCD      ");
            //query.AppendLine("				,CAAPPLDT      = :CAAPPLDT     ");
            //query.AppendLine("				,DMNO          = :DMNO         ");
            //query.AppendLine("				,CANM          = :CANM         ");
            //query.AppendLine("				,CAADDR        = :CAADDR       ");
            //query.AppendLine("				,PRCSDT        = :PRCSDT       ");
            //query.AppendLine("				,USERNM        = :USERNM       ");
            //query.AppendLine("				,DEPT          = :DEPT         ");
            //query.AppendLine("				,CACONT        = :CACONT       ");
            //query.AppendLine("				,CAPRCSRSLT    = :CAPRCSRSLT   ");
            //query.AppendLine("				,REGDT         = :REGDT        ");
            //query.AppendLine("when not matched then                        ");
            //query.AppendLine("            insert (CANO                   ");
            //query.AppendLine("                   ,SGCCD                  ");
            //query.AppendLine("                   ,CAAPPLMETH             ");
            //query.AppendLine("                   ,CAAPPLCLASS            ");
            //query.AppendLine("                   ,FRMYN                  ");
            //query.AppendLine("                   ,VSTCAYN                ");
            //query.AppendLine("                   ,APPRVYN                ");
            //query.AppendLine("                   ,CALRGCD                ");
            //query.AppendLine("                   ,CAMIDCD                ");
            //query.AppendLine("                   ,CAAPPLDT               ");
            //query.AppendLine("                   ,DMNO                   ");
            //query.AppendLine("                   ,CANM                   ");
            //query.AppendLine("                   ,CAADDR                 ");
            //query.AppendLine("                   ,PRCSDT                 ");
            //query.AppendLine("                   ,USERNM                 ");
            //query.AppendLine("                   ,DEPT                   ");
            //query.AppendLine("                   ,CACONT                 ");
            //query.AppendLine("                   ,CAPRCSRSLT             ");
            //query.AppendLine("                   ,REGDT  )               ");
            //query.AppendLine("            values (a.cano                 ");
            //query.AppendLine("                   ,:SGCCD2                ");
            //query.AppendLine("                   ,:CAAPPLMETH2           ");
            //query.AppendLine("                   ,:CAAPPLCLASS2          ");
            //query.AppendLine("                   ,:FRMYN2                ");
            //query.AppendLine("                   ,:VSTCAYN2              ");
            //query.AppendLine("                   ,:APPRVYN2              ");
            //query.AppendLine("                   ,:CALRGCD2              ");
            //query.AppendLine("                   ,:CAMIDCD2              ");
            //query.AppendLine("                   ,:CAAPPLDT2             ");
            //query.AppendLine("                   ,:DMNO2                 ");
            //query.AppendLine("                   ,:CANM2                 ");
            //query.AppendLine("                   ,:CAADDR2               ");
            //query.AppendLine("                   ,:PRCSDT2               ");
            //query.AppendLine("                   ,:USERNM2               ");
            //query.AppendLine("                   ,:DEPT2                 ");
            //query.AppendLine("                   ,:CACONT2               ");
            //query.AppendLine("                   ,:CAPRCSRSLT2           ");
            //query.AppendLine("                   ,:REGDT2  )             ");

            query.AppendLine("merge into wi_cainfo a");
            query.AppendLine("using (");
            query.AppendLine("  select   :CANO as cano");
            query.AppendLine("          ,:SGCCD as sgccd");
            query.AppendLine("          ,:CALRGCD as calrgcd");
            query.AppendLine("          ,:CAMIDCD as camidcd");
            query.AppendLine("          ,:CANM as canm");
            query.AppendLine("          ,:DMNO as dmno");
            query.AppendLine("          ,:USERNM as usernm");
            query.AppendLine("          ,:CAAPPLDT as caappldt");
            query.AppendLine("          ,:PRCSDT as prcsdt");
            query.AppendLine("          ,:CAADDR as caaddr");
            query.AppendLine("          ,:CACONT as cacont");
            query.AppendLine("          ,:CAPRCSRSLT as caprcsrslt");
            query.AppendLine("          ,sysdate as regdt");
            query.AppendLine("  from dual");
            query.AppendLine("      ) b");
            query.AppendLine("  on(a.cano = b.cano)");
            query.AppendLine("when matched then");
            query.AppendLine("      update set");
            query.AppendLine("                  a.sgccd = b.sgccd");
            query.AppendLine("                  ,a.calrgcd = b.calrgcd");
            query.AppendLine("                  ,a.camidcd = b.camidcd");
            query.AppendLine("                  ,a.caappldt = decode(b.caappldt, null, null, to_date(b.caappldt,'yyyy-mm-dd hh24:mi'))");
            //query.AppendLine("                  ,a.caappldt = b.caappldt");
            query.AppendLine("                  ,a.dmno = b.dmno");
            query.AppendLine("                  ,a.canm = b.canm");
            query.AppendLine("                  ,a.caaddr = b.caaddr");
            query.AppendLine("                  ,a.prcsdt = decode(b.prcsdt, null, null, to_date(b.prcsdt,'yyyy-mm-dd hh24:mi'))");
            //query.AppendLine("                  ,a.prcsdt = b.prcsdt");
            query.AppendLine("                  ,a.usernm = b.usernm");
            query.AppendLine("                  ,a.cacont = b.cacont");
            query.AppendLine("                  ,a.caprcsrslt = b.caprcsrslt");
            query.AppendLine("                  ,a.regdt = b.regdt");
            query.AppendLine("when not matched then ");
            //query.AppendLine("     insert (a.cano, a.calrgcd, a.camidcd, a.caappldt, a.dmno, a.canm, a.caaddr, a.prcsdt, a.usernm, a.cacont, a.caprcsrslt, a.regdt) ");
            //query.AppendLine("     values (b.cano, b.calrgcd, b.camidcd, decode(b.caappldt, null, null, to_date(b.caappldt,'yyyy-mm-dd hh24:mi')), b.dmno, b.canm, b.caaddr, decode(b.prcsdt, null, null, to_date(b.prcsdt,'yyyy-mm-dd hh24:mi')), b.usernm, b.cacont, b.caprcsrslt, b.regdt) ");
            
            query.AppendLine("            insert (CANO                   ");
            query.AppendLine("                   ,SGCCD                  ");
            query.AppendLine("                   ,CAAPPLMETH             ");
            query.AppendLine("                   ,CAAPPLCLASS            ");
            query.AppendLine("                   ,FRMYN                  ");
            query.AppendLine("                   ,VSTCAYN                ");
            query.AppendLine("                   ,APPRVYN                ");
            query.AppendLine("                   ,CALRGCD                ");
            query.AppendLine("                   ,CAMIDCD                ");
            query.AppendLine("                   ,CAAPPLDT               ");
            query.AppendLine("                   ,DMNO                   ");
            query.AppendLine("                   ,CANM                   ");
            query.AppendLine("                   ,CAADDR                 ");
            query.AppendLine("                   ,PRCSDT                 ");
            query.AppendLine("                   ,USERNM                 ");
            query.AppendLine("                   ,DEPT                   ");
            query.AppendLine("                   ,CACONT                 ");
            query.AppendLine("                   ,CAPRCSRSLT             ");
            query.AppendLine("                   ,REGDT  )               ");
            query.AppendLine("            values (B.CANO                   ");
            query.AppendLine("                   ,B.SGCCD                  ");
            query.AppendLine("                   ,NULL             ");
            query.AppendLine("                   ,NULL            ");
            query.AppendLine("                   ,NULL                  ");
            query.AppendLine("                   ,NULL                ");
            query.AppendLine("                   ,NULL                ");
            query.AppendLine("                   ,B.CALRGCD                ");
            query.AppendLine("                   ,B.CAMIDCD                ");
            query.AppendLine("                   ,decode(b.caappldt, null, null, to_date(b.caappldt,'yyyy-mm-dd hh24:mi'))               ");
            query.AppendLine("                   ,B.DMNO                   ");
            query.AppendLine("                   ,B.CANM                   ");
            query.AppendLine("                   ,B.CAADDR                 ");
            query.AppendLine("                   ,decode(b.prcsdt, null, null, to_date(b.prcsdt,'yyyy-mm-dd hh24:mi'))                 ");
            query.AppendLine("                   ,B.USERNM                 ");
            query.AppendLine("                   ,NULL                   ");
            query.AppendLine("                   ,B.CACONT                 ");
            query.AppendLine("                   ,B.CAPRCSRSLT             ");
            query.AppendLine("                   ,B.REGDT  )               ");


            //query.AppendLine("     insert (a.cano, a.sgccd, a.calrgcd, a.camidcd, a.caappldt, a.dmno, a.canm, a.caaddr, a.prcsdt, a.usernm, a.cacont, a.caprcsrslt, a.regdt) ");
            //query.AppendLine("     values (b.cano, decode(b.dmno, null, null, substr(b.dmno, 1, 5), b.calrgcd, b.camidcd, b.caappldt, b.dmno, b.canm, b.caaddr, b.prcsdt, b.usernm, b.cacont, b.caprcsrslt, b.regdt) ");
            //query.AppendLine("     insert (a.cano, a.camidcd, a.caappldt, a.calrgcd, a.canm, a.dmno, a.usernm, a.prcsdt, a.caaddr, a.cacont, a.caprcsrslt, a.regdt) ");
            //query.AppendLine("     values (b.cano, b.camidcd, b.caappldt, b.calrgcd ,b.canm, b.dmno, b.usernm, b.prcsdt, b.caaddr, b.cacont, b.caprcsrslt, b.regdt) ");

            //System.Collections.Hashtable parameter = new System.Collections.Hashtable();

            try
            {
                oDBManager.BeginTransaction();

                OracleCommand commandOracle = new OracleCommand();
                commandOracle.CommandText = query.ToString();
                commandOracle.Connection = oDBManager.Connection;
                commandOracle.ArrayBindCount = table.Rows.Count;
                commandOracle.BindByName = true;

                IList<object> list_parameter = new List<object>();
                OracleParameter parameter = null;

                foreach (DataColumn column in table.Columns)
                {
                    list_parameter.Clear();

                    Console.WriteLine(column.ColumnName);

                    if (column.ColumnName == "민원번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }
                        
                        parameter = new OracleParameter("CANO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);

                        list_parameter.Clear();
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;
                            
                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Substring(0, 5);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("SGCCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }

                    if (column.ColumnName == "대분류")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                               value = GetCodeValue("9003", Convert.ToString( Convert.ToString(row[column.ColumnName])));
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("CALRGCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "중분류")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = GetCodeValue("9004", Convert.ToString(Convert.ToString(row[column.ColumnName])));
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("CAMIDCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "신청일시")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            DateTime time;
                            if (DateTime.TryParse(row[column.ColumnName].ToString(), out time))
                            {
                                value = row[column.ColumnName].ToString();
                            }
                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("CAAPPLDT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "수용가번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "신청인")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("CANM", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "신청인주소")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("CAADDR", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "처리일시")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;
                            DateTime time;
                            if (DateTime.TryParse(row[column.ColumnName].ToString(), out time))
                            {
                                value = row[column.ColumnName].ToString();
                            }
                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("PRCSDT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "처리자")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("USERNM", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "민원내용")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("CACONT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "처리결과")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("CAPRCSRSLT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                }
                Console.WriteLine("시작 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                commandOracle.ExecuteNonQuery();
                oDBManager.CommitTransaction();

                Console.WriteLine("종료 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                if (chkLog.Checked)
                    rtbLog.AppendText("\n민원정보를 저장 했습니다.");
                WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");

                //DateTime dt = DateTime.Now;
                //object data = null;

                //foreach (DataRow row in table.Rows)
                //{
                //    IDataParameter[] parameters =  {
                //         new OracleParameter("CANO"         ,OracleDbType.Varchar2)
                //        ,new OracleParameter("SGCCD"        ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAAPPLMETH"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAAPPLCLASS"  ,OracleDbType.Varchar2)
                //        ,new OracleParameter("FRMYN"        ,OracleDbType.Varchar2)
                //        ,new OracleParameter("VSTCAYN"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("APPRVYN"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CALRGCD"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAMIDCD"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAAPPLDT"     ,OracleDbType.Date)
                //        ,new OracleParameter("DMNO"         ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CANM"         ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAADDR"       ,OracleDbType.Varchar2)
                //        ,new OracleParameter("PRCSDT"       ,OracleDbType.Date)
                //        ,new OracleParameter("USERNM"       ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DEPT"         ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CACONT"       ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAPRCSRSLT"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("REGDT"        ,OracleDbType.Date)

                //        ,new OracleParameter("SGCCD2"        ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAAPPLMETH2"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAAPPLCLASS2"  ,OracleDbType.Varchar2)
                //        ,new OracleParameter("FRMYN2"        ,OracleDbType.Varchar2)
                //        ,new OracleParameter("VSTCAYN2"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("APPRVYN2"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CALRGCD2"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAMIDCD2"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAAPPLDT2"     ,OracleDbType.Date)
                //        ,new OracleParameter("DMNO2"         ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CANM2"         ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAADDR2"       ,OracleDbType.Varchar2)
                //        ,new OracleParameter("PRCSDT2"       ,OracleDbType.Date)
                //        ,new OracleParameter("USERNM2"       ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DEPT2"         ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CACONT2"       ,OracleDbType.Varchar2)
                //        ,new OracleParameter("CAPRCSRSLT2"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("REGDT2"        ,OracleDbType.Date)
                //    };

                    

                //    parameter.Clear();
                //    data = IsData(row["민원번호"]);
                //    parameter.Add("CANO" , data == null ? Convert.DBNull :  Convert.ToString(data).Replace("-", ""));
                //    data = IsData(row["수용가번호"]);
                //    parameter.Add("SGCCD", data == null ? Convert.DBNull : Convert.ToString(data).Substring(0, 5));
                //    parameter.Add("CAAPPLMETH"   ,Convert.DBNull);
                //    parameter.Add("CAAPPLCLASS"  ,Convert.DBNull);
                //    parameter.Add("FRMYN", Convert.DBNull);
                //    parameter.Add("VSTCAYN", Convert.DBNull);
                //    parameter.Add("APPRVYN", Convert.DBNull);
                //    data = IsData(row["대분류"]);
                //    parameter.Add("CALRGCD", data == null ? Convert.DBNull : GetCodeValue("9003", Convert.ToString(data)));
                //    data = IsData(row["중분류"]);
                //    parameter.Add("CAMIDCD", data == null ? Convert.DBNull : GetCodeValue("9004", Convert.ToString(data)));
                //    dt = ValidateDateTime(row["신청일시"]);
                //    parameter.Add("CAAPPLDT", dt == DateTime.MinValue ? Convert.DBNull : dt);
                //    data = IsData(row["수용가번호"]);
                //    parameter.Add("DMNO", data == null ? Convert.DBNull : Convert.ToString(data).Replace("-", ""));
                //    data = IsData(row["신청인"]);
                //    parameter.Add("CANM", data == null ? Convert.DBNull : data);
                //    data = IsData(row["신청인주소"]);
                //    parameter.Add("CAADDR", data == null ? Convert.DBNull : data);
                //    dt = ValidateDateTime(row["처리일시"]);
                //    parameter.Add("PRCSDT", dt == DateTime.MinValue ? Convert.DBNull : dt);
                //    data = IsData(row["처리자"]);
                //    parameter.Add("USERNM", data == null ? Convert.DBNull : data);
                //    parameter.Add("DEPT"         ,Convert.DBNull);
                //    data = IsData(row["민원내용"]);
                //    parameter.Add("CACONT", data == null ? Convert.DBNull : data);
                //    data = IsData(row["처리결과"]);
                //    parameter.Add("CAPRCSRSLT", data == null ? Convert.DBNull : data);
                //    dt = ValidateDateTime(row["등록일자"]);
                //    parameter.Add("REGDT", dt == DateTime.MinValue ? DateTime.Now : dt);

                    

                //    parameters[0].Value = parameter["CANO"];
                //    parameters[1].Value = parameter["SGCCD"];
                //    parameters[2].Value = parameter["CAAPPLMETH"];
                //    parameters[3].Value = parameter["CAAPPLCLASS"];
                //    parameters[4].Value = parameter["FRMYN"];
                //    parameters[5].Value = parameter["VSTCAYN"];
                //    parameters[6].Value = parameter["APPRVYN"];
                //    parameters[7].Value = parameter["CALRGCD"];
                //    parameters[8].Value = parameter["CAMIDCD"];
                //    parameters[9].Value = parameter["CAAPPLDT"];
                //    parameters[10].Value = parameter["DMNO"];
                //    parameters[11].Value = parameter["CANM"];
                //    parameters[12].Value = parameter["CAADDR"];
                //    parameters[13].Value = parameter["PRCSDT"];
                //    parameters[14].Value = parameter["USERNM"];
                //    parameters[15].Value = parameter["DEPT"];
                //    parameters[16].Value = parameter["CACONT"];
                //    parameters[17].Value = parameter["CAPRCSRSLT"];
                //    parameters[18].Value = parameter["REGDT"];

                //    parameters[19].Value = parameter["SGCCD"];
                //    parameters[20].Value = parameter["CAAPPLMETH"];
                //    parameters[21].Value = parameter["CAAPPLCLASS"];
                //    parameters[22].Value = parameter["FRMYN"];
                //    parameters[23].Value = parameter["VSTCAYN"];
                //    parameters[24].Value = parameter["APPRVYN"];
                //    parameters[25].Value = parameter["CALRGCD"];
                //    parameters[26].Value = parameter["CAMIDCD"];
                //    parameters[27].Value = parameter["CAAPPLDT"];
                //    parameters[28].Value = parameter["DMNO"];
                //    parameters[29].Value = parameter["CANM"];
                //    parameters[30].Value = parameter["CAADDR"];
                //    parameters[31].Value = parameter["PRCSDT"];
                //    parameters[32].Value = parameter["USERNM"];
                //    parameters[33].Value = parameter["DEPT"];
                //    parameters[34].Value = parameter["CACONT"];
                //    parameters[35].Value = parameter["CAPRCSRSLT"];
                //    parameters[36].Value = parameter["REGDT"];

                //    if (chkLog.Checked)
                //        rtbLog.AppendText("\n 민원정보 => " + Convert.ToString(parameter["CANO"]) + "를 저장합니다.");
                //    //Application.DoEvents();
                //    oDBManager.ExecuteScript(query.ToString(), parameters);
                //};

                //oDBManager.CommitTransaction();

                //WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");
            }
            catch (Exception e)
            {
                //rtbLog.AppendText("\n Error : 민원정보 => " + Convert.ToString(parameter["CANO"]));
                oDBManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                //throw e;
            }
            finally
            {
                oDBManager.Transaction.Dispose();
                oDBManager.Transaction = null;
            }
        }


        /// <summary>
        /// 수용가 정보 등록 및 수정
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="table"></param>
        private bool UpdateDM_INFO(WaterNet.WaterNetCore.OracleDBManager manager, DataTable table)
        {
            bool result = false;

            StringBuilder query = new StringBuilder();
            //query.AppendLine("merge into wi_dminfo dminfo                                                ");
            //query.AppendLine("using (select :DMNO dmno from dual) a                                      ");
            //query.AppendLine("  on (dminfo.dmno = a.dmno)                                                ");
            //query.AppendLine("when matched then                                                          ");
            //query.AppendLine("     update set SGCCD        = :SGCCD        ");
            //query.AppendLine("					,UMDCD      = :UMDCD     ");
            //query.AppendLine("					,HYDRNTNO   = :HYDRNTNO  ");
            //query.AppendLine("					,LFTRIDN    = :LFTRIDN   ");
            //query.AppendLine("					,MFTRIDN    = :MFTRIDN   ");
            //query.AppendLine("					,SFTRIDN    = :SFTRIDN   ");
            //query.AppendLine("					,DMCLASS    = :DMCLASS   ");
            //query.AppendLine("					,DMNM       = :DMNM      ");
            //query.AppendLine("					,TRADENM    = :TRADENM   ");
            //query.AppendLine("					,DMADDR     = :DMADDR    ");
            //query.AppendLine("					,REGDT      = :REGDT     ");
            //query.AppendLine("					,DMCELL     = :DMCELL    ");
            //query.AppendLine("when not matched then                      ");
            //query.AppendLine("     insert (DMNO       ");
            //query.AppendLine("            ,SGCCD      ");
            //query.AppendLine("            ,UMDCD       ");
            //query.AppendLine("            ,HYDRNTNO    ");
            //query.AppendLine("            ,LFTRIDN     ");
            //query.AppendLine("            ,MFTRIDN     ");
            //query.AppendLine("            ,SFTRIDN     ");
            //query.AppendLine("            ,DMCLASS     ");
            //query.AppendLine("            ,DMNM        ");
            //query.AppendLine("            ,TRADENM     ");
            //query.AppendLine("            ,DMADDR      ");
            //query.AppendLine("            ,REGDT       ");
            //query.AppendLine("            ,DMCELL)     ");
            //query.AppendLine("     values (a.dmno       ");
            //query.AppendLine("            ,:SGCCD2     ");
            //query.AppendLine("            ,:UMDCD2      ");
            //query.AppendLine("            ,:HYDRNTNO2   ");
            //query.AppendLine("            ,:LFTRIDN2    ");
            //query.AppendLine("            ,:MFTRIDN2    ");
            //query.AppendLine("            ,:SFTRIDN2    ");
            //query.AppendLine("            ,:DMCLASS2    ");
            //query.AppendLine("            ,:DMNM2       ");
            //query.AppendLine("            ,:TRADENM2    ");
            //query.AppendLine("            ,:DMADDR2     ");
            //query.AppendLine("            ,:REGDT2      ");
            //query.AppendLine("            ,:DMCELL2)    ");

            query.AppendLine("merge into wi_dminfo a");
            query.AppendLine("using (");
            query.AppendLine("      select :DMNO dmno");
            query.AppendLine("              ,:SGCCD sgccd");
            query.AppendLine("              ,:UMDCD umdcd");
            query.AppendLine("              ,:HYDRNTNO hydrntno");
            query.AppendLine("              ,:LFTRIDN lftridn");
            query.AppendLine("              ,:MFTRIDN mftridn");
            query.AppendLine("              ,:SFTRIDN sftridn");
            //query.AppendLine("              ,:DMCLASS dmclass");
            query.AppendLine("              ,:DMNM dmnm");
            query.AppendLine("              ,:TRADENM tradenm");
            query.AppendLine("              ,:DMADDR dmaddr");
            query.AppendLine("              ,sysdate regdt");
            query.AppendLine("              ,:DMCELL dmcell");
            query.AppendLine("      from dual");
            query.AppendLine("      ) b");
            query.AppendLine("      on (a.dmno = b.dmno)");
            query.AppendLine("when matched then");
            query.AppendLine("      update set a.sgccd = b.sgccd");
            query.AppendLine("                  ,a.umdcd = b.umdcd");
            query.AppendLine("                  ,a.hydrntno = b.hydrntno");
            query.AppendLine("                  ,a.lftridn = b.lftridn");
            query.AppendLine("                  ,a.mftridn = b.mftridn");
            query.AppendLine("                  ,a.sftridn = b.sftridn");
            //query.AppendLine("                  ,a.dmclass = b.dmclass");
            query.AppendLine("                  ,a.dmnm = b.dmnm");
            query.AppendLine("                  ,a.tradenm = b.tradenm");
            query.AppendLine("                  ,a.dmaddr = b.dmaddr");
            query.AppendLine("                  ,a.regdt = b.regdt");
            query.AppendLine("                  ,a.dmcell = b.dmcell");
            query.AppendLine("when not matched then");
            query.AppendLine("      insert (a.dmno, a.sgccd, a.umdcd, a.hydrntno, a.lftridn, a.mftridn, a.sftridn, a.dmnm, a.tradenm, a.dmaddr, a.regdt, a.dmcell )");
            query.AppendLine("      values (b.dmno, b.sgccd, b.umdcd, b.hydrntno, b.lftridn, b.mftridn, b.sftridn, b.dmnm, b.tradenm, b.dmaddr, b.regdt, b.dmcell )");
            //query.AppendLine("      insert (a.dmno, a.sgccd, a.umdcd, a.hydrntno, a.lftridn, a.mftridn, a.sftridn, a.dmnm, a.tradenm, a.dmaddr, a.regdt, a.dmcell )");
            //query.AppendLine("      values (b.dmno, b.sgccd, b.umdcd, b.hydrntno, b.lftridn, b.mftridn, b.sftridn, b.dmnm, b.tradenm, b.dmaddr, b.regdt, b.dmcell )");



            //System.Collections.Hashtable parameter = new System.Collections.Hashtable();
            try
            {
                oDBManager.BeginTransaction();
                OracleCommand commandOracle = new OracleCommand();
                commandOracle.CommandText = query.ToString();
                commandOracle.Connection = oDBManager.Connection;
                commandOracle.ArrayBindCount = table.Rows.Count;
                commandOracle.BindByName = true;

                Console.WriteLine(commandOracle.Connection.ToString());

                IList<object> list_parameter = new List<object>();
                OracleParameter parameter = null;

                foreach (DataColumn column in table.Columns)
                {
                    list_parameter.Clear();

                    if (column.ColumnName == "수용가번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);

                        list_parameter.Clear();
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Substring(0, 5);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("SGCCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);

                        list_parameter.Clear();
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Substring(6, 2);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("UMDCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "수전번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("HYDRNTNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "대블록")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = this.GetLargeBlockCode(Convert.ToString(row[column.ColumnName]));
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("LFTRIDN", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "중블록")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = this.GetMiddleBlockCode(Convert.ToString(row[column.ColumnName])) ;
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("MFTRIDN", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "소블록")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = this.GetSmallBlockCode(Convert.ToString(row[column.ColumnName]));
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("SFTRIDN", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "수용가명")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMNM", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "상호명")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("TRADENM", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "주소")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMADDR", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    //if (column.ColumnName == "등록일자")
                    //{
                    //    foreach (DataRow row in table.Rows)
                    //    {
                    //        object value = null;

                    //        if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                    //        {
                    //            value = DBNull.Value;
                    //        }
                    //        else
                    //        {
                    //            value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                    //        }

                    //        list_parameter.Add(value);
                    //    }

                    //    parameter = new OracleParameter("REGDT", OracleDbType.Varchar2);
                    //    parameter.Direction = ParameterDirection.Input;
                    //    parameter.Value = list_parameter.ToArray();
                    //    commandOracle.Parameters.Add(parameter);
                    //}
                    if (column.ColumnName == "핸드폰번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMCELL", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                }
                Console.WriteLine("시작 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                commandOracle.ExecuteNonQuery();
                oDBManager.CommitTransaction();

                Console.WriteLine("종료 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                if (chkLog.Checked)
                    rtbLog.AppendText("\n수용가정보를 저장 했습니다.");
                WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");


                //DateTime dt = DateTime.MinValue;
                //object data = null;

                //foreach (DataRow row in table.Rows)
                //{
                //    if (Convert.IsDBNull(row["수용가번호"])) continue;

                //    IDataParameter[] parameters =  {
                //         new OracleParameter("DMNO"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("SGCCD"     ,OracleDbType.Varchar2)
                //        ,new OracleParameter("UMDCD"     ,OracleDbType.Varchar2)
                //        ,new OracleParameter("HYDRNTNO"  ,OracleDbType.Varchar2)
                //        ,new OracleParameter("LFTRIDN"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("MFTRIDN"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("SFTRIDN"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DMCLASS"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DMNM"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("TRADENM"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DMADDR"    ,OracleDbType.Varchar2)
                //        ,new OracleParameter("REGDT"     ,OracleDbType.Date)
                //        ,new OracleParameter("DMCELL"    ,OracleDbType.Varchar2)

                //        ,new OracleParameter("SGCCD2"     ,OracleDbType.Varchar2)
                //        ,new OracleParameter("UMDCD2"     ,OracleDbType.Varchar2)
                //        ,new OracleParameter("HYDRNTNO2"  ,OracleDbType.Varchar2)
                //        ,new OracleParameter("LFTRIDN2"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("MFTRIDN2"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("SFTRIDN2"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DMCLASS2"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DMNM2"      ,OracleDbType.Varchar2)
                //        ,new OracleParameter("TRADENM2"   ,OracleDbType.Varchar2)
                //        ,new OracleParameter("DMADDR2"    ,OracleDbType.Varchar2)
                //        ,new OracleParameter("REGDT2"     ,OracleDbType.Date)
                //        ,new OracleParameter("DMCELL2"    ,OracleDbType.Varchar2)
                //    };

                //    parameter.Clear();
                //    data = IsData(row["수용가번호"]);
                //    parameter.Add("DMNO", data == null ? Convert.DBNull : Convert.ToString(data).Replace("-", ""));
                //    parameter.Add("SGCCD", data == null ? Convert.DBNull : Convert.ToString(data).Substring(0, 5));
                //    parameter.Add("UMDCD", data == null ? Convert.DBNull : Convert.ToString(data).Substring(6, 2));
                //    data = IsData(row["수전번호"]);
                //    parameter.Add("HYDRNTNO", data == null ? Convert.DBNull : data);
                //    data = IsData(row["대블럭"]);
                //    parameter.Add("LFTRIDN", data == null ? Convert.DBNull : this.GetLargeBlockCode(row));
                //    data = IsData(row["중블럭"]);
                //    parameter.Add("MFTRIDN", data == null ? Convert.DBNull : this.GetMiddleBlockCode(row));
                //    data = IsData(row["소블럭"]);
                //    parameter.Add("SFTRIDN", data == null ? Convert.DBNull : this.GetSmallBlockCode(row));
                //    parameter.Add("DMCLASS", Convert.DBNull);
                //    data = IsData(row["수용가명"]);
                //    parameter.Add("DMNM", data == null ? Convert.DBNull : data);
                //    data = IsData(row["상호명"]);
                //    parameter.Add("TRADENM", data == null ? Convert.DBNull : data);
                //    data = IsData(row["주소"]);
                //    parameter.Add("DMADDR", data == null ? Convert.DBNull : data);
                //    dt = ValidateDateTime(row["등록일자"]);
                //    parameter.Add("REGDT", dt == DateTime.MinValue ? DateTime.Now : dt);
                //    data = IsData(row["핸드폰번호"]);
                //    parameter.Add("DMCELL", data == null ? Convert.DBNull : data);


                //    parameters[0].Value = parameter["DMNO"];
                //    parameters[1].Value = parameter["SGCCD"];
                //    parameters[2].Value = parameter["UMDCD"];
                //    parameters[3].Value = parameter["HYDRNTNO"];
                //    parameters[4].Value = parameter["LFTRIDN"];
                //    parameters[5].Value = parameter["MFTRIDN"];
                //    parameters[6].Value = parameter["SFTRIDN"];
                //    parameters[7].Value = parameter["DMCLASS"];
                //    parameters[8].Value = parameter["DMNM"];
                //    parameters[9].Value = parameter["TRADENM"];
                //    parameters[10].Value = parameter["DMADDR"];
                //    parameters[11].Value = parameter["REGDT"];
                //    parameters[12].Value = parameter["DMCELL"];

                //    parameters[13].Value = parameter["SGCCD"];
                //    parameters[14].Value = parameter["UMDCD"];
                //    parameters[15].Value = parameter["HYDRNTNO"];
                //    parameters[16].Value = parameter["LFTRIDN"];
                //    parameters[17].Value = parameter["MFTRIDN"];
                //    parameters[18].Value = parameter["SFTRIDN"];
                //    parameters[19].Value = parameter["DMCLASS"];
                //    parameters[20].Value = parameter["DMNM"];
                //    parameters[21].Value = parameter["TRADENM"];
                //    parameters[22].Value = parameter["DMADDR"];
                //    parameters[23].Value = parameter["REGDT"];
                //    parameters[24].Value = parameter["DMCELL"];

                //    if (chkLog.Checked)
                //        rtbLog.AppendText("\n 수용가정보 => " + Convert.ToString(parameter["DMNO"]) + "를 저장합니다.");
                //    //Application.DoEvents();

                //    oDBManager.ExecuteScript(query.ToString(), parameters);
                //};

                //oDBManager.CommitTransaction();

                //result = true;
                //WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");
            }
            catch (Exception e)
            {
                result = false;
                oDBManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                //rtbLog.AppendText("\n Error : 수용가정보 => " + Convert.ToString(parameter["DMNO"]));
                throw e;
            }
            finally
            {
                oDBManager.Transaction.Dispose();
                oDBManager.Transaction = null;
            }
            return result;
        }

        /// <summary>
        /// 상하수도 자원정보 등록 및 수정
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="table"></param>
        private void UpdateDMWSRSRC(WaterNet.WaterNetCore.OracleDBManager manager, DataTable table)
        {
            StringBuilder query = new StringBuilder();
            //query.AppendLine("merge into wi_dmwsrsrc dmwsrsrc                                            ");
            //query.AppendLine("using (select :DMNO dmno from dual) a                                      ");
            //query.AppendLine("  on (dmwsrsrc.dmno = a.dmno)                                              ");
            //query.AppendLine("when matched then                                                          ");
            //query.AppendLine("     update set HYDRNTSTAT = :HYDRNTSTAT                         ");
            //query.AppendLine("                ,STATAPPDT = :STATAPPDT                           ");
            //query.AppendLine("                ,STRTDT = :STRTDT                                 ");
            //query.AppendLine("                ,WSPIPESZCD = :WSPIPESZCD                         ");
            //query.AppendLine("                ,WSREPBIZKNDCD = :WSREPBIZKNDCD                   ");
            //query.AppendLine("                ,WSNOHSHD = :WSNOHSHD                             ");
            //query.AppendLine("                ,REGDT = :REGDT                                   ");
            //query.AppendLine("when not matched then                                                                 ");
            //query.AppendLine("     insert (DMNO,HYDRNTSTAT,STATAPPDT,STRTDT,WSPIPESZCD,WSREPBIZKNDCD,WSNOHSHD,REGDT )");
            //query.AppendLine("     values (a.dmno, :HYDRNTSTAT2, :STATAPPDT2, :STRTDT2, :WSPIPESZCD2, :WSREPBIZKNDCD2, :WSNOHSHD2, :REGDT2 )");

            query.AppendLine("merge into wi_dmwsrsrc a                                            ");
            query.AppendLine("using (select :DMNO dmno                              ");
            query.AppendLine("                ,:HYDRNTSTAT  hydrntstat                         ");
            query.AppendLine("                ,:STATAPPDT  statappdt                           ");
            query.AppendLine("                ,:STRTDT  strtdt                                 ");
            query.AppendLine("                ,:WSPIPESZCD  wspipeszcd                         ");
            query.AppendLine("                ,:WSREPBIZKNDCD  wsrepbizkndcd                   ");
            query.AppendLine("                ,:WSNOHSHD  wsnohshd                             ");
            query.AppendLine("                ,sysdate  regdt                                 ");
            query.AppendLine("      from dual) b                                      ");
            query.AppendLine("  on (a.dmno = b.dmno)                                              ");
            query.AppendLine("when matched then                                                          ");
            query.AppendLine("     update set a.hydrntstat = b.hydrntstat                         ");
            query.AppendLine("                ,a.statappdt = b.statappdt                           ");
            query.AppendLine("                ,a.strtdt = b.strtdt                                 ");
            query.AppendLine("                ,a.wspipeszcd = b.wspipeszcd                         ");
            query.AppendLine("                ,a.wsrepbizkndcd = b.wsrepbizkndcd                   ");
            query.AppendLine("                ,a.wsnohshd = b.wsnohshd                             ");
            query.AppendLine("                ,a.regdt = b.regdt                                   ");
            query.AppendLine("when not matched then                                                                 ");
            query.AppendLine("     insert (a.dmno, a.hydrntstat, a.statappdt, a.strtdt, a.wspipeszcd, a.wsrepbizkndcd, a.wsnohshd, a.regdt )");
            query.AppendLine("     values (b.dmno, b.hydrntstat, b.statappdt, b.strtdt, b.wspipeszcd, b.wsrepbizkndcd, b.wsnohshd, b.regdt )");
            //query.AppendLine("     insert (a.dmno, a.hydrntstat, a.strtdt, a.wspipeszcd, a.wsrepbizkndcd, a.wsnohshd, a.regdt )");
            //query.AppendLine("     values (b.dmno, b.hydrntstat, b.strtdt, b.wspipeszcd, b.wsrepbizkndcd, b.wsnohshd, b.regdt )");

            //System.Collections.Hashtable parameter = new System.Collections.Hashtable();
            try
            {
                oDBManager.BeginTransaction();

                OracleCommand commandOracle = new OracleCommand();
                commandOracle.CommandText = query.ToString();
                commandOracle.Connection = oDBManager.Connection;
                commandOracle.ArrayBindCount = table.Rows.Count;
                commandOracle.BindByName = true;

                Console.WriteLine(commandOracle.Connection.ToString());

                IList<object> list_parameter = new List<object>();
                OracleParameter parameter = null;

                foreach (DataColumn column in table.Columns)    
                {
                    list_parameter.Clear();

                    if (column.ColumnName == "수용가번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "급수상태")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("HYDRNTSTAT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "최초개시일자")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }
                        parameter = new OracleParameter("STRTDT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    
                    if (column.ColumnName == "구경")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]);
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("WSPIPESZCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "업종")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = GetCodeValue("2017", Convert.ToString(row[column.ColumnName]));
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("WSREPBIZKNDCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "등록일자")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("STATAPPDT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "가구수")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                int parse = 0;
                                int.TryParse(Convert.ToString(row[column.ColumnName]).Replace(",",""), out parse);
                                value = parse;
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("WSNOHSHD", OracleDbType.Int32);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }

                }

                Console.WriteLine("시작 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                commandOracle.ExecuteNonQuery();
                oDBManager.CommitTransaction();

                Console.WriteLine("종료 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                if (chkLog.Checked)
                    rtbLog.AppendText("\n상하수도자원정보 를 저장 했습니다.");
                WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");


                //DateTime dt = DateTime.MinValue;
                //object data = null;

                //foreach (DataRow row in table.Rows)
                //{
                //    IDataParameter[] parameters =  {
                //          new OracleParameter("DMNO", OracleDbType.Varchar2)
                //         ,new OracleParameter("HYDRNTSTAT", OracleDbType.Varchar2)
                //         ,new OracleParameter("STATAPPDT", OracleDbType.Varchar2)
                //         ,new OracleParameter("STRTDT", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSPIPESZCD", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSREPBIZKNDCD", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSNOHSHD", OracleDbType.Int32)
                //         ,new OracleParameter("REGDT", OracleDbType.Date)

                //         ,new OracleParameter("HYDRNTSTAT2", OracleDbType.Varchar2)
                //         ,new OracleParameter("STATAPPDT2", OracleDbType.Varchar2)
                //         ,new OracleParameter("STRTDT2", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSPIPESZCD2", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSREPBIZKNDCD2", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSNOHSHD2", OracleDbType.Int32)
                //         ,new OracleParameter("REGDT2", OracleDbType.Date)
                //    };

                //    parameter.Clear();
                //    data = IsData(row["수용가번호"]);
                //    parameter.Add("DMNO", data == null ? Convert.DBNull : Convert.ToString(data).Replace("-", ""));
                //    data = IsData(row["급수상태"]);
                //    parameter.Add("HYDRNTSTAT", data == null ? Convert.DBNull : data);
                //    dt = ValidateDateTime(row["최초 개시일자"]);
                //    parameter.Add("STATAPPDT", dt == DateTime.MinValue ? Convert.DBNull : dt.ToString("yyyyMMdd"));
                //    parameter.Add("STRTDT", dt == DateTime.MinValue ? Convert.DBNull : dt.ToString("yyyyMMdd"));
                //    data = IsData(row["구경"]);
                //    parameter.Add("WSPIPESZCD", data == null ? Convert.DBNull : data);
                //    data = IsData(row["업종"]);
                //    parameter.Add("WSREPBIZKNDCD", data == null ? Convert.DBNull : GetCodeValue("2017", Convert.ToString(data)));
                //    data = IsData(row["가구 수"]);
                //    parameter.Add("WSNOHSHD", data == null ? Convert.DBNull : data);
                //    dt = ValidateDateTime(row["등록일자"]);
                //    parameter.Add("REGDT", dt == DateTime.MinValue ? DateTime.Now : dt);

                //    parameters[0].Value = parameter["DMNO"];
                //    parameters[1].Value = parameter["HYDRNTSTAT"];
                //    parameters[2].Value = parameter["STATAPPDT"];
                //    parameters[3].Value = parameter["STRTDT"];
                //    parameters[4].Value = parameter["WSPIPESZCD"];
                //    parameters[5].Value = parameter["WSREPBIZKNDCD"];
                //    parameters[6].Value = parameter["WSNOHSHD"];
                //    parameters[7].Value = parameter["REGDT"];

                //    parameters[8].Value = parameter["HYDRNTSTAT"];
                //    parameters[9].Value = parameter["STATAPPDT"];
                //    parameters[10].Value = parameter["STRTDT"];
                //    parameters[11].Value = parameter["WSPIPESZCD"];
                //    parameters[12].Value = parameter["WSREPBIZKNDCD"];
                //    parameters[13].Value = parameter["WSNOHSHD"];
                //    parameters[14].Value = parameter["REGDT"];

                //if (chkLog.Checked)
                //    rtbLog.AppendText("\n상하수도자원정보 => " + Convert.ToString(parameter["DMNO"]) + "를 저장합니다.");
                //Application.DoEvents();

                //    oDBManager.ExecuteScript(query.ToString(), parameters);
                //};

                //oDBManager.CommitTransaction();

                //WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");
            }
            catch (Exception e)
            {
                //rtbLog.AppendText("\n Error : 상하수도자원정보 => " + Convert.ToString(parameter["DMNO"]));
                oDBManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                oDBManager.Transaction.Dispose();
                oDBManager.Transaction = null;
            }
        }

        /// <summary>
        /// 계량기교체내역 정보 등록 및 수정
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="table"></param>
        private void UpdateMTRCHGINFO(WaterNet.WaterNetCore.OracleDBManager manager, DataTable table)
        {
            StringBuilder query = new StringBuilder();
            //query.AppendLine("merge into wi_mtrchginfo mtrchginfo");
            ////query.AppendLine("using (select :DMNO dmno, :MTRCHGSERNO mtrchgserno from dual) a                    ");
            //query.AppendLine("using (select :DMNO dmno, :MDDT mddt from dual) a");
            //query.AppendLine("  on (mtrchginfo.dmno = a.dmno and mtrchginfo.mddt = a.mddt)");
            //query.AppendLine("when matched then                                                                     ");
            //query.AppendLine("      update set MTRCHGSERNO    = :MTRCHGSERNO");
            //query.AppendLine("                ,TRANSCLASS     = :TRANSCLASS");
            //query.AppendLine("                ,MRYM           = :MRYM");
            //query.AppendLine("                ,OMFRNO         = :OMFRNO");
            //query.AppendLine("                ,OWSPIPESZCD    = :OWSPIPESZCD");
            //query.AppendLine("                ,WSRMVNDL       = :WSRMVNDL");
            //query.AppendLine("                ,NMFRNO         = :NMFRNO");
            //query.AppendLine("                ,NWSPIPESZCD    = :NWSPIPESZCD");
            //query.AppendLine("                ,WSATTNDL       = :WSATTNDL");
            //query.AppendLine("                ,TRANSRS        = :TRANSRS");
            //query.AppendLine("                ,REGDT          = :REGDT");
            //query.AppendLine("when not matched then");
            //query.AppendLine("     insert (DMNO,MTRCHGSERNO,TRANSCLASS,MRYM,MDDT,OMFRNO,OWSPIPESZCD");
            //query.AppendLine("            ,WSRMVNDL,NMFRNO,NWSPIPESZCD,WSATTNDL,TRANSRS,REGDT)");
            //query.AppendLine("     values (a.dmno, :MTRCHGSERNO2,:TRANSCLASS2, :MRYM2, a.mddt, :OMFRNO2, :OWSPIPESZCD2");
            //query.AppendLine("            , :WSRMVNDL2, :NMFRNO2, :NWSPIPESZCD2, :WSATTNDL2, :TRANSRS2, :REGDT2)");

            query.AppendLine("merge into wi_mtrchginfo a");
            query.AppendLine("using (");
            query.AppendLine("         select    :DMNO dmno");
            //query.AppendLine("                  ,:MTRCHGSERNO mtrchgserno");
            query.AppendLine("                  ,:TRANSCLASS transclass");
            query.AppendLine("                  ,:MRYM mrym");
            query.AppendLine("                  ,:MDDT mddt");
            query.AppendLine("                  ,:OMFRNO omfrno");
            query.AppendLine("                  ,:OWSPIPESZCD owspipeszcd");
            query.AppendLine("                  ,:WSRMVNDL wsrmvndl");
            query.AppendLine("                  ,:NMFRNO nmfrno");
            query.AppendLine("                  ,:NWSPIPESZCD nwspipeszcd");
            query.AppendLine("                  ,:WSATTNDL wsattndl");
            query.AppendLine("                  ,:TRANSRS transrs");
            query.AppendLine("                  ,sysdate regdt ");
            query.AppendLine("          from dual");
            query.AppendLine("      ) b");
            query.AppendLine("    on (a.dmno = b.dmno and a.mddt = b.mddt) ");
            query.AppendLine(" when matched then");
            query.AppendLine("    update set"); // a.mtrchgserno = b.mtrchgserno");
            query.AppendLine("                a.transclass = b.transclass");
            query.AppendLine("               ,a.mrym = b.mrym");
            query.AppendLine("               ,a.omfrno = b.omfrno");
            query.AppendLine("               ,a.owspipeszcd = b.owspipeszcd");
            query.AppendLine("               ,a.wsrmvndl = b.wsrmvndl");
            query.AppendLine("               ,a.nmfrno = b.nmfrno");
            query.AppendLine("               ,a.nwspipeszcd = b.nwspipeszcd");
            query.AppendLine("               ,a.wsattndl = b.wsattndl");
            query.AppendLine("               ,a.transrs = b.transrs");
            query.AppendLine("               ,a.regdt = b.regdt");
            query.AppendLine("when not matched then ");
            //query.AppendLine("     insert (a.dmno, a.mtrchgserno, a.transclass, a.mrym, a.mddt, a.omfrno, a.owspipeszcd ,a.wsrmvndl ,a.nmfrno ,a.nwspipeszcd ,a.wsattndl, a.transrs, a.regdt ) ");
            //query.AppendLine("     values (b.dmno, b.mtrchgserno, b.transclass, b.mrym, b.mddt, b.omfrno, b.owspipeszcd ,b.wsrmvndl ,b.nmfrno ,b.nwspipeszcd ,b.wsattndl, b.transrs, b.regdt ) ");
            query.AppendLine("     insert(a.dmno, a.transclass, a.mrym, a.mddt, a.omfrno, a.owspipeszcd ,a.wsrmvndl ,a.nmfrno ,a.nwspipeszcd ,a.wsattndl, a.transrs, a.regdt ) ");
            query.AppendLine("     values(b.dmno, b.transclass, b.mrym, b.mddt, b.omfrno, b.owspipeszcd ,b.wsrmvndl ,b.nmfrno ,b.nwspipeszcd ,b.wsattndl, b.transrs, b.regdt ) ");

            //System.Collections.Hashtable parameter = new System.Collections.Hashtable();
            try
            {
                oDBManager.BeginTransaction();

                OracleCommand commandOracle = new OracleCommand();
                commandOracle.CommandText = query.ToString();
                commandOracle.Connection = oDBManager.Connection;
                commandOracle.ArrayBindCount = table.Rows.Count;
                commandOracle.BindByName = true;

                Console.WriteLine(commandOracle.Connection.ToString());

                IList<object> list_parameter = new List<object>();
                OracleParameter parameter = null;

                foreach (DataColumn column in table.Columns)
                {
                    list_parameter.Clear();

                    if (column.ColumnName == "수용가번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    //else if (column.ColumnName == "계량기교체순번")
                    //{
                    //    foreach (DataRow row in table.Rows)
                    //    {
                    //        object value = null;

                    //        if (row[column.ColumnName] == null)
                    //        {
                    //            value = DBNull.Value;
                    //        }
                    //        else
                    //        {
                    //            value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                    //        }

                    //        list_parameter.Add(value);
                    //    }

                    //    parameter = new OracleParameter("MTRCHGSERNO", OracleDbType.Varchar2);
                    //    parameter.Direction = ParameterDirection.Input;
                    //    parameter.Value = list_parameter.ToArray();
                    //    commandOracle.Parameters.Add(parameter);
                    //}
                    if (column.ColumnName == "교체일자")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            } 

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("MDDT", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "교체구분")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace(",", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("TRANSCLASS", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "검침적용년월")       //
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("MRYM", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "제작번호(교체전)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("OMFRNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "구경(교체전)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Trim().Replace(",", "").Replace("mm","");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("OWSPIPESZCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "철거지침(교체전)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace(",", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("WSRMVNDL", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "제작번호(교체후)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("NMFRNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "구경(교체후)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Trim().Replace(",", "").Replace("mm", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("NWSPIPESZCD", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "설치지침(교체후)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace(",", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("WSATTNDL", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                    if (column.ColumnName == "변경사유")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null || string.IsNullOrEmpty(Convert.ToString(row[column.ColumnName])))
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace(",", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("TRANSRS", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                }
                    Console.WriteLine("시작 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    commandOracle.ExecuteNonQuery();
                    oDBManager.CommitTransaction();

                    Console.WriteLine("종료 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    if (chkLog.Checked)
                        rtbLog.AppendText("\n계량기교체내역를 저장 했습니다.");
                    WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");
                
                //DateTime dt = DateTime.MinValue;
                //object data = null;

                //foreach (DataRow row in table.Rows)
                //{
                //    IDataParameter[] parameters =  {
                //          new OracleParameter("DMNO", OracleDbType.Varchar2)
                //         ,new OracleParameter("MDDT", OracleDbType.Varchar2)
                //         ,new OracleParameter("MTRCHGSERNO", OracleDbType.Varchar2)
                //         ,new OracleParameter("TRANSCLASS", OracleDbType.Varchar2)
                //         ,new OracleParameter("MRYM", OracleDbType.Varchar2)
                //         ,new OracleParameter("OMFRNO", OracleDbType.Varchar2)
                //         ,new OracleParameter("OWSPIPESZCD", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSRMVNDL", OracleDbType.Int32)
                //         ,new OracleParameter("NMFRNO", OracleDbType.Varchar2)
                //         ,new OracleParameter("NWSPIPESZCD", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSATTNDL", OracleDbType.Int32)
                //         ,new OracleParameter("TRANSRS", OracleDbType.Varchar2)
                //         ,new OracleParameter("REGDT", OracleDbType.Date)

                //         ,new OracleParameter("MTRCHGSERNO2", OracleDbType.Varchar2)
                //         ,new OracleParameter("TRANSCLASS2", OracleDbType.Varchar2)
                //         ,new OracleParameter("MRYM2", OracleDbType.Varchar2)
                //         ,new OracleParameter("OMFRNO2", OracleDbType.Varchar2)
                //         ,new OracleParameter("OWSPIPESZCD2", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSRMVNDL2", OracleDbType.Int32)
                //         ,new OracleParameter("NMFRNO2", OracleDbType.Varchar2)
                //         ,new OracleParameter("NWSPIPESZCD2", OracleDbType.Varchar2)
                //         ,new OracleParameter("WSATTNDL2", OracleDbType.Int32)
                //         ,new OracleParameter("TRANSRS2", OracleDbType.Varchar2)
                //         ,new OracleParameter("REGDT2", OracleDbType.Date)
                //    };

                //    parameter.Clear();
                //    data = IsData(row["수용가번호"]);
                //    parameter.Add("DMNO", data == null ? Convert.DBNull : Convert.ToString(data).Replace("-", ""));
                //    dt = ValidateDateTime(row["교체일자"]);
                //    parameter.Add("MDDT", dt == DateTime.MinValue ? Convert.DBNull : dt.ToString("yyyyMMdd"));
                //    parameter.Add("MTRCHGSERNO", Convert.DBNull);
                //    data = IsData(row["교체 구분"]);
                //    parameter.Add("TRANSCLASS", data == null ? Convert.DBNull : data);
                //    dt = ValidateDateTime(row["검침 적용년월"]);
                //    parameter.Add("MRYM", dt == DateTime.MinValue ? Convert.DBNull : dt.ToString("yyyyMM"));
                //    data = IsData(row["제작번호(교체전)"]);
                //    parameter.Add("OMFRNO", data == null ? Convert.DBNull : data);
                //    data = IsData(row["구경(교체전)"]);
                //    parameter.Add("OWSPIPESZCD", data == null ? Convert.DBNull : Convert.ToString(data).Replace("mm","").Trim());;
                //    data = IsData(row["철거지침(교체전)"]);
                //    parameter.Add("WSRMVNDL", data == null ? Convert.DBNull : Convert.ToString(data).Replace(",", "").Trim()); ;
                //    data = IsData(row["제작번호(교체후)"]);
                //    parameter.Add("NMFRNO", data == null ? Convert.DBNull : data);
                //    data = IsData(row["구경(교체후)"]);
                //    parameter.Add("NWSPIPESZCD", data == null ? Convert.DBNull : Convert.ToString(data).Replace("mm", "").Trim()); ;
                //    data = IsData(row["철거지침(교체후)"]);
                //    parameter.Add("WSATTNDL", data == null ? Convert.DBNull : Convert.ToString(data).Replace("mm", "").Trim()); ;
                //    data = IsData(row["변경사유"]);
                //    parameter.Add("TRANSRS", data == null ? Convert.DBNull : data);
                //    parameter.Add("REGDT", DateTime.Now);

                //    parameters[0].Value = parameter["DMNO"];
                //    parameters[1].Value = parameter["MDDT"];
                //    parameters[2].Value = parameter["MTRCHGSERNO"];
                //    parameters[3].Value = parameter["TRANSCLASS"];
                //    parameters[4].Value = parameter["MRYM"];
                //    parameters[5].Value = parameter["OMFRNO"];
                //    parameters[6].Value = parameter["OWSPIPESZCD"];
                //    parameters[7].Value = parameter["WSRMVNDL"];
                //    parameters[8].Value = parameter["NMFRNO"];
                //    parameters[9].Value = parameter["NWSPIPESZCD"];
                //    parameters[10].Value = Convert.ToInt32(parameter["WSATTNDL"]);
                //    parameters[11].Value = parameter["TRANSRS"];
                //    parameters[12].Value = parameter["REGDT"];

                //    parameters[13].Value = parameter["MTRCHGSERNO"];
                //    parameters[14].Value = parameter["TRANSCLASS"];
                //    parameters[15].Value = parameter["MRYM"];
                //    parameters[16].Value = parameter["OMFRNO"];
                //    parameters[17].Value = parameter["OWSPIPESZCD"];
                //    parameters[18].Value = parameter["WSRMVNDL"];
                //    parameters[19].Value = parameter["NMFRNO"];
                //    parameters[20].Value = parameter["NWSPIPESZCD"];
                //    parameters[21].Value = Convert.ToInt32(parameter["WSATTNDL"]);
                //    parameters[22].Value = parameter["TRANSRS"];
                //    parameters[23].Value = parameter["REGDT"];

                //    if (chkLog.Checked)
                //        rtbLog.AppendText("\n계량기교체내역 => " + Convert.ToString(parameter["DMNO"]) + "를 저장합니다.");
                //    //Application.DoEvents();

                //    oDBManager.ExecuteScript(query.ToString(), parameters);
                //};

                //oDBManager.CommitTransaction();

                //WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                //rtbLog.AppendText("\n Error : 계량기교체내역 => " + Convert.ToString(parameter["DMNO"]));
                oDBManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                oDBManager.Transaction.Dispose();
                oDBManager.Transaction = null;
            }
        }

        /// <summary>
        /// 요금조정 정보 등록 및 수정
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="table"></param>
        private void UpdateSTWCHRG(WaterNet.WaterNetCore.OracleDBManager manager, DataTable table)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wi_stwchrg a ");
            query.AppendLine("using ( ");
            query.Append("      select '").Append(Convert.ToString(cboYear.SelectedValue) + Convert.ToString(cboMonth.SelectedValue)).AppendLine("' stym");
            query.AppendLine("            ,:DMNO dmno ");
            query.AppendLine("            ,:WSUSEVOL wsusevol ");
            query.AppendLine("            ,:WSSTVOL wsstvol ");
            query.AppendLine("            ,sysdate regdt ");
            query.AppendLine("        from dual ");
            query.AppendLine("      ) b ");
            query.AppendLine("  on (a.stym = b.stym and a.dmno = b.dmno) ");
            query.AppendLine("when matched then ");
            query.AppendLine("     update set a.wsusevol = b.wsusevol ");
            query.AppendLine("               ,a.wsstvol  = b.wsstvol ");
            query.AppendLine("               ,a.regdt    = b.regdt ");
            query.AppendLine("when not matched then ");
            query.AppendLine("     insert (a.stym, a.dmno, a.wsusevol, a.wsstvol, a.regdt ) ");
            query.AppendLine("     values (b.stym, b.dmno, b.wsusevol, b.wsstvol, b.regdt ) ");

            //Console.WriteLine( (Convert.ToString(cboYear.SelectedValue) + Convert.ToString(cboMonth.SelectedValue)).ToString() );

            try
            {
                oDBManager.BeginTransaction();

                OracleCommand commandOracle = new OracleCommand();
                commandOracle.CommandText = query.ToString();
                commandOracle.Connection = oDBManager.Connection;
                commandOracle.ArrayBindCount = table.Rows.Count;
                commandOracle.BindByName = true;

                Console.WriteLine(commandOracle.Connection.ToString());

                IList<object> list_parameter = new List<object>();
                OracleParameter parameter = null;

                foreach (DataColumn column in table.Columns)
                {
                    list_parameter.Clear();

                    if (column.ColumnName == "수용가번호")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace("-", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("DMNO", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }

                    if (column.ColumnName == "검침량(당월)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace(",", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("WSUSEVOL", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }

                    if (column.ColumnName == "사용량(당월)")
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            object value = null;

                            if (row[column.ColumnName] == null)
                            {
                                value = DBNull.Value;
                            }
                            else
                            {
                                value = Convert.ToString(row[column.ColumnName]).Replace(",", "");
                            }

                            list_parameter.Add(value);
                        }

                        parameter = new OracleParameter("WSSTVOL", OracleDbType.Varchar2);
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = list_parameter.ToArray();
                        commandOracle.Parameters.Add(parameter);
                    }
                }

                Console.WriteLine("시작 : "+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                commandOracle.ExecuteNonQuery();
                oDBManager.CommitTransaction();

                Console.WriteLine("종료 : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                if (chkLog.Checked)
                    rtbLog.AppendText("\n요금 조정 정보를 저장 했습니다.");
                WaterNetCore.MessageManager.ShowInformationMessage("저장이 완료되었습니다.", "안내");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                oDBManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                oDBManager.Transaction.Dispose();
                oDBManager.Transaction = null;
            }
        }
        #endregion 데이터베이스 Update 및 Insert 질의문

        private object IsData(object data)
        {
            object o = null;
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(data)))
                {
                    o = data;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return o;
        }

        private DateTime ValidateDateTime(object data)
        {
            DateTime result = DateTime.MinValue;
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(data)))
                {
                    result = Convert.ToDateTime(data);    
                }
            }
            catch (System.FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine((string)data);
                switch (((string)data).Replace("-", "").Length)
                {
                    case 4:
                        data += "-01-01";
                        break;
                    case 6:
                        data += "-01";
                        break;
                }

                result = Convert.ToDateTime(data);
            }

            return result;
        }

        private void rtbLog_TextChanged(object sender, EventArgs e)
        {
            rtbLog.ScrollToCaret();
        }
    }

    public static class Convertion
    {
        public static string toDate(object data)
        {
            DateTime result = DateTime.MinValue;
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(data)))
                {
                    result = Convert.ToDateTime(data);
                }
            }
            catch (System.FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine((string)data);
                switch (((string)data).Replace("-", "").Length)
                {
                    case 4:
                        data += "-01-01";
                        break;
                    case 6:
                        data += "-01";
                        break;
                }

                result = Convert.ToDateTime(data);
            }

            return result.ToShortDateString();
        }
    }
}
