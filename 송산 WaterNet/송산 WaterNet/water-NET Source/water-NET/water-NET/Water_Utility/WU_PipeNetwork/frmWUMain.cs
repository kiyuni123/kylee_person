﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;

using WaterNet.WaterAOCore;

namespace WaterNet.WU_PipeNetwork
{
    public partial class frmWUMain : Form
    {
        ArrayList m_ArrayNetworkLog = new ArrayList();  //관로 네트워크 : 상수관로-급수관로-수도계량기
        ArrayList m_ArrayPipeLog = new ArrayList();     //관로 소속 블록 : 관로-소블록,중블록,대블록

        string m_DeleteQuery = string.Empty;
        string m_InsertQuery = string.Empty;

        public frmWUMain()
        {
            InitializeComponent();
            InitializeSetting();
            this.Load +=new EventHandler(frmWUMain_Load);
        }

        //접속여부, 분석실행여부
        private bool isActive = false; private bool isRuned = false;
        private Dictionary<string, LAYERS> Layers = new Dictionary<string, LAYERS>();
 

        void initUI()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);

            doActive();

        }

        void initEventHandler()
        {
            this.cboHEADQUARTER.SelectedValueChanged += delegate
            {
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "' AND ZONE_GBN = '" + EMFrame.statics.AppStatic.ZONE_GBN + "'");
                WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), false);
            };
            this.btnConnect.Click += delegate
            {
                this.isActive = !this.isActive; this.isRuned = false;
                if (this.isActive)
                {
                    EMFrame.statics.AppStatic.USER_SGCCD = this.cboManageDept.SelectedValue.ToString();
                    SetDataPaht(EMFrame.statics.AppStatic.USER_SGCCD);
                    SetSi_Layers(EMFrame.statics.AppStatic.USER_SGCCD);    
                }                
                
                doActive();

                Console.WriteLine(this.DATA_Directory);
            };

            DelegateShowJobRateInstance = new DelegateShowJobRate(this.UpdatertLog);
        }

        void doActive()
        {
            this.btnExecute.Enabled = this.isActive; this.btnSave.Enabled = this.isActive && this.isRuned;
            this.cboHEADQUARTER.Enabled = this.cboManageDept.Enabled = !this.isActive;
            this.btnConnect.Text = this.isActive ? "접속종료" : "접속";
        }

        void SetDataPaht(string sgccd)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                if (mapper == null)
                {
                    MessageBox.Show(Convert.ToString(this.cboManageDept.SelectedValue) + " 데이터베이스 경로가 설정되지 않았습니다.");
                    return;
                }


                oStringBuilder.AppendLine("SELECT DISTINCT b.rohqcd, b.rohqnm, a.mgdpcd, a.mgdpnm, a.data_path, a.zone_gbn ");
                oStringBuilder.AppendLine("       ,NVL(a.MIN_X,0.0) MIN_X, NVL(a.MAX_X,0.0) MAX_X, NVL(a.MIN_Y,0.0) MIN_Y, NVL(a.MAX_Y,0.0) MAX_Y ");
                oStringBuilder.AppendLine("       FROM MANAGEMENT_DEPARTMENT a, REGIONAL_HEADQUARTER b ");
                oStringBuilder.AppendLine("WHERE a.rohqcd = b.rohqcd and a.MGDPCD = '" + sgccd + "'");
                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new IDataParameter[] { });

                EMFrame.statics.AppStatic.DATA_DIR = @Convert.ToString(table.Rows[0]["DATA_PATH"]);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        void SetSi_Layers(string sgccd)
        {
            this.Layers.Clear();

            StringBuilder oStringBuilder = new StringBuilder();
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper(sgccd);
                if (mapper == null)
                {
                    MessageBox.Show(Convert.ToString(this.cboManageDept.SelectedValue) + " 데이터베이스 경로가 설정되지 않았습니다.");
                    return;
                }

                oStringBuilder.AppendLine("SELECT * FROM SI_LAYER WHERE F_ALIAS IN ('상수관로','급수관로','수도계량기','소블록','중블록','대블록')");
                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new IDataParameter[] { });
                foreach (var r in table.Rows.OfType<DataRow>())
                {
                    var layer = ArcManager.GetShapeLayer(this.DATA_Directory, r.Field<string>("F_NAME"));
                    this.Layers.Add(r.Field<string>("F_ALIAS"), new LAYERS() { f_alias = r.Field<string>("F_ALIAS"), f_name = r.Field<string>("F_NAME"), layer = layer as IFeatureLayer ?? null });
                }

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        #region 초기화설정
        /// <summary>
        /// 삭제, 추가 쿼리문 설정
        /// </summary>
        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("DELETE FROM SI_PIPE_NETWORK");
            m_DeleteQuery = oStringBuilder.ToString();

            ////////////////////////////////////////////////////////////
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO SI_PIPE_NETWORK (");
            oStringBuilder.AppendLine("   S_NAME       ,");
            oStringBuilder.AppendLine("   S_FTR_IDN    ,");
            oStringBuilder.AppendLine("   S_OID        ,");
            oStringBuilder.AppendLine("   L_NAME       ,");
            oStringBuilder.AppendLine("   L_FTR_IDN   ,");
            oStringBuilder.AppendLine("   L_OID        )");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine("   :S_NAME       ,");
            oStringBuilder.AppendLine("   :S_FTR_IDN    ,");
            oStringBuilder.AppendLine("   :S_OID        ,");
            oStringBuilder.AppendLine("   :L_NAME       ,");
            oStringBuilder.AppendLine("   :L_FTR_IDN   ,");
            oStringBuilder.AppendLine("   :L_OID        )");
            m_InsertQuery = oStringBuilder.ToString();
        }
        #endregion

        #region Private Property ------------------------------------------------------------------------
        private string DATA_Directory
        {
            get { return System.IO.Path.Combine(System.IO.Path.Combine(EMFrame.statics.AppStatic.DATA_DIR, EMFrame.statics.AppStatic.USER_SGCCD), "Pipegraphic"); }
        }

    
        #endregion Private Property ------------------------------------------------------------------------
        void UpdatertLog(string text)
        {
            this.rtLog.AppendText(text + "\r\n");
            this.rtLog.ScrollToCaret();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            m_ArrayNetworkLog.Clear();
            m_ArrayPipeLog.Clear();
            
            if (this.Layers["상수관로"].layer == null) return;
            if (this.Layers["급수관로"].layer == null) return;
            if (this.Layers["수도계량기"].layer == null) return;

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            try
            {
                this.statusStrip1.Items["toolStripStatusLabel1"].Text = "상수관망 레이어에 대하여 인덱싱 중입니다.";

                this.UpdatertLog(string.Format("-----------------------{0}------------------------", "FeatureIndexing"));
                this.UpdatertLog(string.Format("{0} - {1}", "상수관로", DateTime.Now.ToString()));

                IFeatureLayer pipeLayer = this.Layers["상수관로"].layer;
                IFeatureIndex2 PipeFeatureIndex = new FeatureIndexClass();
                IGeoDataset PipeGeoDataset = pipeLayer.FeatureClass as IGeoDataset;
                PipeFeatureIndex.FeatureClass = pipeLayer.FeatureClass;
                PipeFeatureIndex.set_OutputSpatialReference(pipeLayer.FeatureClass.ShapeFieldName, PipeGeoDataset.SpatialReference);
                PipeFeatureIndex.Index(null, PipeGeoDataset.Extent);

                this.UpdatertLog(string.Format("{0} - {1}", "급수관로", DateTime.Now.ToString()));

                IFeatureLayer splyLayer = this.Layers["급수관로"].layer;
                IFeatureIndex2 SplyFeatureIndex = new FeatureIndexClass();
                IGeoDataset SplyGeoDataset = splyLayer.FeatureClass as IGeoDataset;
                SplyFeatureIndex.FeatureClass = splyLayer.FeatureClass;
                SplyFeatureIndex.set_OutputSpatialReference(splyLayer.FeatureClass.ShapeFieldName, SplyGeoDataset.SpatialReference);
                SplyFeatureIndex.Index(null, SplyGeoDataset.Extent);

                this.UpdatertLog(string.Format("{0} - {1}", "수도계량기", DateTime.Now.ToString()));

                IFeatureLayer metaLayer = this.Layers["수도계량기"].layer;
                IFeatureIndex2 MetaFeatureIndex = new FeatureIndexClass();
                IGeoDataset MetaGeoDataset = metaLayer.FeatureClass as IGeoDataset;
                MetaFeatureIndex.FeatureClass = metaLayer.FeatureClass;
                MetaFeatureIndex.set_OutputSpatialReference(metaLayer.FeatureClass.ShapeFieldName, MetaGeoDataset.SpatialReference);
                MetaFeatureIndex.Index(null, MetaGeoDataset.Extent);


                this.statusStrip1.Items["toolStripStatusLabel1"].Text = "상수관망 레이어에 대하여 검색중입니다.";

                this.UpdatertLog(string.Format("-----------------------{0}------------------------", "Feature Querying"));
                this.UpdatertLog(string.Format("{0} - {1}", "상수관로", DateTime.Now.ToString()));

                Console.WriteLine("//----------------------------Pipe Query");
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor pFeatureCursor = pipeLayer.Search(null, true);
                    comReleaser.ManageLifetime(pFeatureCursor);

                    IFeature pFeature = pFeatureCursor.NextFeature();
                    while (pFeature != null)
                    {
                        m_ArrayNetworkLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                         + "," + pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString());

                        ///-------------상수관로(시작점)이 위치한 블록-------------------------------------
                        IFeature theFeature = GetBlockIsContain(pFeature);
                        if (theFeature != null)
                            m_ArrayPipeLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                               + "," + pFeature.get_Value(pFeature.Fields.FindField("SAA_CDE")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("MOP_CDE")).ToString()
                                               + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_LEN")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_DIP")).ToString()
                                               + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString()
                                               + "," + "");

                        //Console.WriteLine(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                        //                   + "," + pFeature.get_Value(pFeature.Fields.FindField("SAA_CDE")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("MOP_CDE")).ToString()
                        //                   + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_LEN")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_DIP")).ToString()
                        //                   + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString());

                        ///-------------상수관로(시작점)이 위치한 블록-------------------------------------


                        pFeature = pFeatureCursor.NextFeature();
                    }
                }

                this.UpdatertLog(string.Format("{0} - {1}", "급수관로", DateTime.Now.ToString()));
                Console.WriteLine("//----------------------------Sply Query");
                TreeView SplytreeView = new TreeView();
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor pFeatureCursor = splyLayer.Search(null, true);
                    comReleaser.ManageLifetime(pFeatureCursor);

                    IFeature pFeature = pFeatureCursor.NextFeature();
                    while (pFeature != null)
                    {
                        TreeNode pNode = new TreeNode();
                        pNode.Tag = pFeature.Class.AliasName;
                        pNode.Text = pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString();
                        pNode.Name = pFeature.OID.ToString();
                        SplytreeView.Nodes.Add(pNode);

                        IFeature theFeature = GetBlockIsContain(pFeature);
                        if (theFeature != null)
                            m_ArrayPipeLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                               + "," + pFeature.get_Value(pFeature.Fields.FindField("SAA_CDE")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("MOP_CDE")).ToString()
                                               + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_LEN")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_DIP")).ToString()
                                               + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString()
                                               + "," + "");

                        //Console.WriteLine(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                        //                   + "," + pFeature.get_Value(pFeature.Fields.FindField("SAA_CDE")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("MOP_CDE")).ToString()
                        //                   + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_LEN")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_DIP")).ToString()
                        //                   + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString());

                        pFeature = pFeatureCursor.NextFeature();
                    }
                }

                this.UpdatertLog(string.Format("{0} - {1}", "수도계량기", DateTime.Now.ToString()));
                Console.WriteLine("//----------------------------Meta Query");
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor pFeatureCursor = metaLayer.Search(null, true);
                    comReleaser.ManageLifetime(pFeatureCursor);

                    IFeature pFeature = pFeatureCursor.NextFeature();
                    while (pFeature != null)
                    {
                        IFeature theFeature = GetBlockIsContain(pFeature);
                        if (theFeature != null)
                            m_ArrayPipeLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                               + "," + "" + "," + ""
                                               + "," + "" + "," + pFeature.get_Value(pFeature.Fields.FindField("MET_DIP")).ToString()
                                               + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString()
                                               + "," + pFeature.get_Value(pFeature.Fields.FindField("DMNO")).ToString());

                        //Console.WriteLine(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                        //                   + "," + pFeature.get_Value(pFeature.Fields.FindField("SAA_CDE")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("MOP_CDE")).ToString()
                        //                   + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_LEN")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_DIP")).ToString()
                        //                   + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString());

                        pFeature = pFeatureCursor.NextFeature();
                    }
                }

                //this.statusStrip1.Items["toolStripStatusLabel1"].Text = "네트워크 분석을 위한 기초자료를 생성중입니다.";

                //this.UpdatertLog(string.Format("-----------------------{0}------------------------", "자료구조 생성 및 분할"));
                //this.UpdatertLog(string.Format("Process : {0}, Node Count : {1}, Range : {2}", Environment.ProcessorCount, SplytreeView.Nodes.Count, SplytreeView.Nodes.Count / Environment.ProcessorCount));
                
                //int ProcessCount = Environment.ProcessorCount;
                //int size = SplytreeView.Nodes.Count;
                //int range = size / ProcessCount;
                //int start = 0;
                //int end = 0;

                //this.UpdatertLog(string.Format("{0} - {1}", "자료구조 생성", DateTime.Now.ToString()));
                //Console.WriteLine("//----------------------------threed");
                //ArrayList startlist = new ArrayList();
                //ArrayList endlist = new ArrayList();
                //for (int i = 0; i < size; i += range + 1)
                //{
                //    start = i;
                //    end = (i + range) > size ? size : (i + range);

                //    startlist.Add(start);
                //    endlist.Add(end);
                //}

                //this.UpdatertLog(string.Format("{0} - {1}", "자료구조 분할", DateTime.Now.ToString()));
                //Console.WriteLine("//----------------------------tree 분할");
                //ArrayList treeCount = new ArrayList();
                //for (int i = 0; i < startlist.Count; i++)
                //{
                //    TreeView tree = new TreeView();
                //    foreach (TreeNode node in SplytreeView.Nodes)
                //    {
                //        if (node.Index > Convert.ToInt32(endlist[i])) continue;
                //        if (node.Index >= Convert.ToInt32(startlist[i]) && node.Index <= Convert.ToInt32(endlist[i]))
                //        {
                //            TreeNode nodeClone = new TreeNode();
                //            nodeClone = node.Clone() as TreeNode;
                //            tree.Nodes.Add(nodeClone);
                //        }
                //    }

                //    treeCount.Add(tree);
                //}

                this.statusStrip1.Items["toolStripStatusLabel1"].Text = "네트워크 분석을 실행중입니다.";
                this.UpdatertLog(string.Format("-----------------------{0}-{1}-----------------------", "네트워크 분석", DateTime.Now.ToString()));
                //this.rtLog.Invoke(DelegateShowJobRateInstance, string.Format("{0} / {1} 번째 네트워크 분석", 1, treeCount.Count + 1));
                ///병렬 실행
                //Parallel.For(0, treeCount.Count, delegate(int i)
                //{
                //    Console.WriteLine("//----------------------------Parallel 실행" + i.ToString());
                //    //this.rtLog.Invoke(DelegateShowJobRateInstance, string.Format("{0} / {1} 번째 네트워크 분석", i + 1, treeCount.Count+1));
                //    //System.Threading.Thread.Sleep(1000);

                //    TreeView tree = treeCount[i] as TreeView;
                //    NetworkOperator oNetWorkOperator = new NetworkOperator(i, pipeLayer, splyLayer, metaLayer);
                //    oNetWorkOperator.treeview = tree;
                //    oNetWorkOperator.log = m_ArrayNetworkLog;
                //    oNetWorkOperator.MetaFeatureIndex = MetaFeatureIndex;
                //    oNetWorkOperator.PipeFeatureIndex = PipeFeatureIndex;
                //    oNetWorkOperator.SplyFeatureIndex = SplyFeatureIndex;
                //    oNetWorkOperator.Execute();
                //});

                NetworkOperator oNetWorkOperator = new NetworkOperator(pipeLayer, splyLayer, metaLayer);
                oNetWorkOperator.treeview = SplytreeView;
                oNetWorkOperator.log = m_ArrayNetworkLog;
                oNetWorkOperator.MetaFeatureIndex = MetaFeatureIndex;
                oNetWorkOperator.PipeFeatureIndex = PipeFeatureIndex;
                oNetWorkOperator.SplyFeatureIndex = SplyFeatureIndex;
                oNetWorkOperator.Execute();

                this.statusStrip1.Items["toolStripStatusLabel1"].Text = "상수관망 레이어에 대하여 네트워크 분석이 종료되었습니다.";

                this.UpdatertLog(string.Format("-----------------------{0}--{1}----------------------", "네트워크 분석종료", DateTime.Now.ToString()));
                this.UpdatertLog(string.Format("-----------------------{0}------------------------", "분석결과를 저장하십시오"));

                this.isRuned = true;
                doActive();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }


        }

        public delegate void DelegateShowJobRate(string s);
        public DelegateShowJobRate DelegateShowJobRateInstance = null;

        /// <summary>
        /// 상수관로, 급수관로의 시작점이 있는 블록을 가져온다.
        /// 소블록, 중블록, 대블록의 순서로 검색한다.
        /// </summary>
        /// <param name="pFeature"></param>
        private IFeature GetBlockIsContain(IFeature pFeature)
        {
            IPoint pPoint = getStartPoint(pFeature);

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pPoint as IGeometry;
            pSpatialFilter.GeometryField = "Shape";
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
            pSpatialFilter.WhereClause = string.Empty;

            IFeature theFeature = null;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor pCursor = this.Layers["소블록"].layer.FeatureClass.Search(pSpatialFilter, true);
                comReleaser.ManageLifetime(pCursor);

                theFeature = pCursor.NextFeature();
                if (theFeature == null)
                {
                    pCursor = this.Layers["중블록"].layer.FeatureClass.Search(pSpatialFilter, true);
                    theFeature = pCursor.NextFeature();
                    if (theFeature == null)
                    {
                        pCursor = this.Layers["대블록"].layer.FeatureClass.Search(pSpatialFilter, true);
                        theFeature = pCursor.NextFeature();
                    }
                }
            }


            return theFeature;
        }

        /// <summary>
        /// 시작점
        /// </summary>
        /// <param name="pFeature"></param>
        /// <returns></returns>
        private IPoint getStartPoint(IFeature pFeature)
        {
            IPoint pPoint = null;
            if (pFeature.Shape.GeometryType == esriGeometryType.esriGeometryPolyline)
            {
                IPolyline pPolyline = pFeature.Shape as IPolyline;
                pPoint = new PointClass();
                pPoint = pPolyline.FromPoint;
            }
            else if (pFeature.Shape.GeometryType == esriGeometryType.esriGeometryPoint)
            {
                pPoint = new PointClass();
                pPoint = pFeature.ShapeCopy as IPoint;
            }
            

            return pPoint;
        }

        /// <summary>
        /// 상수관로, 급수관로의 블록정보를 데이터베이스에 저장한다.
        /// </summary>
        private void SavePipeBlockQuery()
        {
            if (this.m_ArrayPipeLog.Count == 0)
            {
                //MessageManager.ShowInformationMessage("상수관망 네트워크 분석을 실행하십시오.");
                return;
            }


            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            try
            {
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                oDBManager.BeginTransaction();
                ///기존 데이터 삭제
                
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("DELETE FROM SI_PIPE_BLOCK");
                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                ////////////////////////////////////////////////////////////
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT INTO SI_PIPE_BLOCK (");
                oStringBuilder.AppendLine("   S_NAME       ,");
                oStringBuilder.AppendLine("   S_FTR_IDN    ,");
                oStringBuilder.AppendLine("   S_OID        ,");
                oStringBuilder.AppendLine("   SAA_CDE      ,");
                oStringBuilder.AppendLine("   MOP_CDE      ,");
                oStringBuilder.AppendLine("   PIP_LEN      ,");
                oStringBuilder.AppendLine("   PIP_DIP      ,");
                oStringBuilder.AppendLine("   B_NAME       ,");
                oStringBuilder.AppendLine("   B_FTR_IDN    ,");
                oStringBuilder.AppendLine("   B_OID        ,");
                oStringBuilder.AppendLine("   DMNO         )");
                oStringBuilder.AppendLine("VALUES (");
                oStringBuilder.AppendLine("   :S_NAME       ,");
                oStringBuilder.AppendLine("   :S_FTR_IDN    ,");
                oStringBuilder.AppendLine("   :S_OID        ,");
                oStringBuilder.AppendLine("   :SAA_CDE      ,");
                oStringBuilder.AppendLine("   :MOP_CDE      ,");
                oStringBuilder.AppendLine("   :PIP_LEN      ,");
                oStringBuilder.AppendLine("   :PIP_DIP      ,");
                oStringBuilder.AppendLine("   :B_NAME       ,");
                oStringBuilder.AppendLine("   :B_FTR_IDN    ,");
                oStringBuilder.AppendLine("   :B_OID        ,");
                oStringBuilder.AppendLine("   :DMNO        )");

                OracleCommand commandOracle = new OracleCommand();
                commandOracle.CommandText = oStringBuilder.ToString();
                commandOracle.Connection = oDBManager.Connection;
                commandOracle.BindByName = true;

                List<IList<object>> list_parameter = new List<IList<object>>() { new List<object>(), new List<object>(), new List<object>(), new List<object>(), new List<object>(), new List<object>()
                                                    , new List<object>(), new List<object>(), new List<object>(), new List<object>(), new List<object>()};

                foreach (string item in this.m_ArrayPipeLog)
                {
                    string[] splitedstring = item.Split(',');
                    if (splitedstring.Length < 11) continue;

                    list_parameter[0].Add(splitedstring[0]);
                    list_parameter[1].Add(splitedstring[1]);
                    list_parameter[2].Add(Convert.ToInt32(splitedstring[2]));
                    list_parameter[3].Add(splitedstring[3]);
                    list_parameter[4].Add(splitedstring[4]);
                    list_parameter[5].Add((splitedstring[5].Length > 0) ? Convert.ToDouble(splitedstring[5]) : 0);
                    list_parameter[6].Add((splitedstring[6].Length > 0) ? Convert.ToDouble(splitedstring[6]) : 0);
                    list_parameter[7].Add(splitedstring[7]);
                    list_parameter[8].Add(splitedstring[8]);
                    list_parameter[9].Add(Convert.ToInt32(splitedstring[9]));
                    list_parameter[10].Add(splitedstring[10]);


                }
              
                commandOracle.ArrayBindCount = list_parameter[0].Count;

                OracleParameter parameter_0 = new OracleParameter("S_NAME", OracleDbType.Varchar2);
                parameter_0.Direction = ParameterDirection.Input;
                parameter_0.Value = list_parameter[0].ToArray();
                commandOracle.Parameters.Add(parameter_0);

                OracleParameter parameter_1 = new OracleParameter("S_FTR_IDN", OracleDbType.Varchar2);
                parameter_1.Direction = ParameterDirection.Input;
                parameter_1.Value = list_parameter[1].ToArray();
                commandOracle.Parameters.Add(parameter_1);

                OracleParameter parameter_2 = new OracleParameter("S_OID", OracleDbType.Int32);
                parameter_2.Direction = ParameterDirection.Input;
                parameter_2.Value = list_parameter[2].ToArray();
                commandOracle.Parameters.Add(parameter_2);

                OracleParameter parameter_3 = new OracleParameter("SAA_CDE", OracleDbType.Varchar2);
                parameter_3.Direction = ParameterDirection.Input;
                parameter_3.Value = list_parameter[3].ToArray();
                commandOracle.Parameters.Add(parameter_3);

                OracleParameter parameter_4 = new OracleParameter("MOP_CDE", OracleDbType.Varchar2);
                parameter_4.Direction = ParameterDirection.Input;
                parameter_4.Value = list_parameter[4].ToArray();
                commandOracle.Parameters.Add(parameter_4);

                OracleParameter parameter_5 = new OracleParameter("PIP_LEN", OracleDbType.Double);
                parameter_5.Direction = ParameterDirection.Input;
                parameter_5.Value = list_parameter[5].ToArray();
                commandOracle.Parameters.Add(parameter_5);

                OracleParameter parameter_6 = new OracleParameter("PIP_DIP", OracleDbType.Double);
                parameter_6.Direction = ParameterDirection.Input;
                parameter_6.Value = list_parameter[6].ToArray();
                commandOracle.Parameters.Add(parameter_6);

                OracleParameter parameter_7 = new OracleParameter("B_NAME", OracleDbType.Varchar2);
                parameter_7.Direction = ParameterDirection.Input;
                parameter_7.Value = list_parameter[7].ToArray();
                commandOracle.Parameters.Add(parameter_7);

                OracleParameter parameter_8 = new OracleParameter("B_FTR_IDN", OracleDbType.Varchar2);
                parameter_8.Direction = ParameterDirection.Input;
                parameter_8.Value = list_parameter[8].ToArray();
                commandOracle.Parameters.Add(parameter_8);

                OracleParameter parameter_9 = new OracleParameter("B_OID", OracleDbType.Int32);
                parameter_9.Direction = ParameterDirection.Input;
                parameter_9.Value = list_parameter[9].ToArray();
                commandOracle.Parameters.Add(parameter_9);

                OracleParameter parameter_10 = new OracleParameter("DMNO", OracleDbType.Varchar2);
                parameter_10.Direction = ParameterDirection.Input;
                parameter_10.Value = list_parameter[10].ToArray();
                commandOracle.Parameters.Add(parameter_10);

                //foreach (string item in this.m_ArrayPipeLog)
                //{
                //    string[] splitedstring = item.Split(',');
                //    if (splitedstring.Length < 11) continue;

                //    IDataParameter[] oParams = {
                //        new OracleParameter(":S_NAME",      OracleDbType.Varchar2,50),
                //        new OracleParameter(":S_FTR_IDN",   OracleDbType.Varchar2,10),
                //        new OracleParameter(":S_OID",       OracleDbType.Int32,   4),
                //        new OracleParameter(":SAA_CDE",     OracleDbType.Varchar2,50),
                //        new OracleParameter(":MOP_CDE",     OracleDbType.Varchar2,10),
                //        new OracleParameter(":PIP_LEN",     OracleDbType.Double,  8),
                //        new OracleParameter(":PIP_DIP",     OracleDbType.Double,  8),
                //        new OracleParameter(":B_NAME",      OracleDbType.Varchar2,50),
                //        new OracleParameter(":B_FTR_IDN",   OracleDbType.Varchar2,10),        
                //        new OracleParameter(":B_OID",       OracleDbType.Int32,   4),
                //        new OracleParameter(":DMNO",        OracleDbType.Varchar2,20)
                //    };

                //    oParams[0].Value = splitedstring[0];
                //    oParams[1].Value = splitedstring[1];
                //    oParams[2].Value = Convert.ToInt32(splitedstring[2]);
                //    oParams[3].Value = splitedstring[3];
                //    oParams[4].Value = splitedstring[4];
                //    oParams[5].Value = (splitedstring[5].Length > 0) ? Convert.ToDouble(splitedstring[5]) : 0;
                //    oParams[6].Value = (splitedstring[6].Length > 0) ? Convert.ToDouble(splitedstring[6]) : 0;
                //    oParams[7].Value = splitedstring[7];
                //    oParams[8].Value = splitedstring[8];
                //    oParams[9].Value = Convert.ToInt32(splitedstring[9]);
                //    oParams[10].Value = splitedstring[10];

                //    oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                //}
                commandOracle.ExecuteNonQuery();
                oDBManager.CommitTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
            }
            catch (Exception ex)
            {
                oDBManager.RollbackTransaction();
                MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                throw ex;
            }
            finally
            {
                if (oDBManager != null)  oDBManager.Close();
            }
        }

        /// <summary>
        /// 상수관망 네트워크 구조를 데이터베이스에 저장한다.
        /// </summary>
        private void SavePipeNetworkQuery()
        {
            if (this.m_ArrayNetworkLog.Count == 0)
            {
                //MessageManager.ShowInformationMessage("상수관망 네트워크 분석을 실행하십시오.");
                return;
            }

            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            try
            {
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                oDBManager.BeginTransaction();
                ///기존 데이터 삭제
                oDBManager.ExecuteScript(m_DeleteQuery, null);

                OracleCommand commandOracle = new OracleCommand();
                commandOracle.CommandText = m_InsertQuery.ToString();
                commandOracle.Connection = oDBManager.Connection;
                
                commandOracle.BindByName = true;

                List<IList<object>> list_parameter = new List<IList<object>>() { new List<object>(), new List<object>(), new List<object>(), new List<object>(), new List<object>(), new List<object>() };
                
                foreach (string item in this.m_ArrayNetworkLog)
                {
                    string[] splitedstring = item.Split(',');
                    if (splitedstring.Length < 6) continue;

                    list_parameter[0].Add(splitedstring[0]);
                    list_parameter[1].Add(splitedstring[1]);
                    list_parameter[2].Add(Convert.ToInt32(splitedstring[2]));
                    list_parameter[3].Add(splitedstring[3]);
                    list_parameter[4].Add(splitedstring[4]);
                    list_parameter[5].Add(Convert.ToInt32(splitedstring[5]));
                }

                commandOracle.ArrayBindCount = list_parameter[0].Count;

                OracleParameter parameter_0 = new OracleParameter("S_NAME", OracleDbType.Varchar2);
                parameter_0.Direction = ParameterDirection.Input;
                parameter_0.Value = list_parameter[0].ToArray();
                commandOracle.Parameters.Add(parameter_0);

                OracleParameter parameter_1 = new OracleParameter("S_FTR_IDN", OracleDbType.Varchar2);
                parameter_1.Direction = ParameterDirection.Input;
                parameter_1.Value = list_parameter[1].ToArray();
                commandOracle.Parameters.Add(parameter_1);

                OracleParameter parameter_2 = new OracleParameter("S_OID", OracleDbType.Varchar2);
                parameter_2.Direction = ParameterDirection.Input;
                parameter_2.Value = list_parameter[2].ToArray();
                commandOracle.Parameters.Add(parameter_2);

                OracleParameter parameter_3 = new OracleParameter("L_NAME", OracleDbType.Varchar2);
                parameter_3.Direction = ParameterDirection.Input;
                parameter_3.Value = list_parameter[3].ToArray();
                commandOracle.Parameters.Add(parameter_3);

                OracleParameter parameter_4 = new OracleParameter("L_FTR_IDN", OracleDbType.Varchar2);
                parameter_4.Direction = ParameterDirection.Input;
                parameter_4.Value = list_parameter[4].ToArray();
                commandOracle.Parameters.Add(parameter_4);

                OracleParameter parameter_5 = new OracleParameter("L_OID", OracleDbType.Varchar2);
                parameter_5.Direction = ParameterDirection.Input;
                parameter_5.Value = list_parameter[5].ToArray();
                commandOracle.Parameters.Add(parameter_5);
                //foreach (string item in this.m_ArrayNetworkLog)
                //{
                //    string[] splitedstring = item.Split(',');
                //    if (splitedstring.Length < 6) continue;

                //    IDataParameter[] oParams = {
                //        new OracleParameter(":S_NAME",      OracleDbType.Varchar2,50),
                //        new OracleParameter(":S_FTR_IDN",   OracleDbType.Varchar2,10),
                //        new OracleParameter(":S_OID",       OracleDbType.Int32,   4),
                //        new OracleParameter(":L_NAME",      OracleDbType.Varchar2,50),
                //        new OracleParameter(":L_FTR_IDN",   OracleDbType.Varchar2,10),        
                //        new OracleParameter(":L_OID",       OracleDbType.Int32,  4)
                //    };

                //    oParams[0].Value = splitedstring[0];
                //    oParams[1].Value = splitedstring[1];
                //    oParams[2].Value = Convert.ToInt32(splitedstring[2]);
                //    oParams[3].Value = splitedstring[3];
                //    oParams[4].Value = splitedstring[4];
                //    oParams[5].Value = Convert.ToInt32(splitedstring[5]);

                //    oDBManager.ExecuteScript(m_InsertQuery, oParams);

                //}

                commandOracle.ExecuteNonQuery();
                oDBManager.CommitTransaction();
                //MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
            }
            catch (Exception ex)
            {
                oDBManager.RollbackTransaction();
                //MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
            }
            finally
            {
                if (oDBManager != null)   oDBManager.Close();
            }
        }
        /// <summary>
        /// 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            try
            {
                SavePipeNetworkQuery();
                SavePipeBlockQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
            
        }

        private void frmWUMain_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

    
        private void frmWUMain_Load(object sender, EventArgs e)
        {
            this.initEventHandler();
            this.initUI();

            return;

            this.Visible = false;

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT");
            oStringBuilder.AppendLine("F_DATABASE FROM SI_WORKSPACE");
            oStringBuilder.AppendLine("WHERE UPPER(F_TYPE) = 'SHAPE' AND F_NAME = '관망도'");
            
            WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            try
            {
                oDBManager.ConnectionString = FunctionManager.GetConnectionString();
                oDBManager.Open();

                object o = oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null);

                if (Convert.IsDBNull(o))
                {
                    MessageManager.ShowErrorMessage("상수관망 레이어의 경로가 없습니다. \n\r 관리자에게 문의하십시오");
                    Application.Exit();
                }

                //LocalVariableManager.m_Topographic = WaterAOCore.ArcManager.getShapeWorkspace(Convert.ToString(o));

                btnExecute_Click(this, new EventArgs());

                btnSave_Click(this, new EventArgs());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oDBManager != null)   oDBManager.Close();

                this.Close();
                Application.Exit();        
            }
            
        }

    }


    public class LAYERS
    {
        public string f_name = string.Empty;
        public string f_alias = string.Empty;
        public IFeatureLayer layer = null;
    }
}
