﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WU_PipeNetwork
{
    public interface IFormMessage
    {
        void SetStatusMessage(string message);

        void SetProgressBarStep(int step);

        void SetProgressBarMax(int max);

        void SetProgressBarValue(int value);
    }
}
