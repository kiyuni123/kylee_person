﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Oracle.DataAccess.Client;
using WaterNet.WaterNetCore;

namespace WaterNet.WU_PipeNetwork
{
    public partial class frmDatabase : Form
    {
        public static FunctionManager.ReadXml m_doc = null;

        public frmDatabase()
        {
            InitializeComponent();
        }
        
        private void frmDatabase_Load(object sender, EventArgs e)
        {
            if (File.Exists(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new FunctionManager.ReadXml(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH);
                }

                this.txtDataSource.Text = EMFrame.utils.ConvertUtils.DecryptKey(m_doc.getProp("/Root/waternet/servicename"));
                this.txtDataBase.Text = EMFrame.utils.ConvertUtils.DecryptKey(m_doc.getProp("/Root/waternet/ip"));
                this.txtUserID.Text = EMFrame.utils.ConvertUtils.DecryptKey(m_doc.getProp("/Root/waternet/id"));
                this.txtPw.Text = EMFrame.utils.ConvertUtils.DecryptKey(m_doc.getProp("/Root/waternet/password"));
                this.txtPort.Text = EMFrame.utils.ConvertUtils.DecryptKey(m_doc.getProp("/Root/waternet/port"));
            }

            this.btnSave.Enabled = false;
        }


        #region Button Events

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (this.ConnectionTest() == true)
            {
                MessageBox.Show("Water-NET 데이터베이스 연결 성공. 현재 설정을 저장하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Water-NET 데이터베이스 연결 실패. 데이터베이스 정보를 다시 입력후 테스트 하십시오", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.SaveDatabaseConfigXML() == true)
            {
                MessageBox.Show("Water-NET 데이터베이스 연결정보 파일을 저장했습니다.\r\nWater-NET 프로그램을 다시 시작하십시오. 종료합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
            else
            {
                MessageBox.Show("Water-NET 데이터베이스 연결정보 파일을 저장에 실패했습니다. 다시 시도하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }            
        }


        #endregion


        #region Control Events

        private void txtDataBase_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.txtDataBase.Text == "Database IP Address")
            {
                this.txtDataBase.Text = "";
            }
        }

        private void txtDataSource_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.txtDataSource.Text == "Database SID")
            {
                this.txtDataSource.Text = "";
            }
        }
        
        private void txtUserID_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.txtUserID.Text == "Database ID")
            {
                this.txtUserID.Text = "";
            }
        }
        
        private void txtPw_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.txtPw.Text == "Database Password")
            {
                this.txtPw.Text = "";
            }
        }

        private void txtDataBase_KeyPreass(object sender, KeyPressEventArgs e)
        {
            if (this.txtDataBase.Text == "Database IP Address")
            {
                this.txtDataBase.Text = "";
            }
            if (e.KeyChar == 13)
            {
                this.txtDataSource.Focus();
            }
        }

        private void txtDataSource_KeyPreass(object sender, KeyPressEventArgs e)
        {
            if (this.txtDataSource.Text == "Database SID")
            {
                this.txtDataSource.Text = "";
            }
            if (e.KeyChar == 13)
            {
                this.txtUserID.Focus();
            }
        }

        private void txtUserID_KeyPreass(object sender, KeyPressEventArgs e)
        {
            if (this.txtUserID.Text == "Database ID")
            {
                this.txtUserID.Text = "";
            }
            if (e.KeyChar == 13)
            {
                this.txtPw.Focus();
            }
        }

        private void txtPw_KeyPreass(object sender, KeyPressEventArgs e)
        {
            if (this.txtPw.Text == "Database Password")
            {
                this.txtPw.Text = "";
            }
            if (e.KeyChar == 13)
            {
                this.btnTest.Focus();
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 데이터베이스 연결 테스트
        /// </summary>
        /// <returns></returns>
        private bool ConnectionTest()
        {
            try
            {
                string strConnString = string.Empty;
                
                //strConnString = "Data Source=(DESCRIPTION="
                //              + "(ADDRESS=(PROTOCOL=TCP)(HOST=" + txtDataBase.Text + ")(PORT=" + txtPort.Text + "))"
                //              + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + txtDataSource.Text + ")));"
                //              + "User Id=" + txtUserID.Text + ";Password=" + txtPw.Text + ";";

                strConnString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + txtDataBase.Text + ")(PORT=" + txtPort.Text + ")))"
                    + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + txtDataSource.Text + ")));"
                    + "User Id=" + txtUserID.Text + ";Password=" + txtPw.Text + ";Enlist=false";

                OracleConnection oDBConn = new OracleConnection(strConnString);

                oDBConn.Open();
                btnSave.Enabled = true;

                return true;
            }
            catch (Exception ex)
            {
                btnSave.Enabled = false;
                txtDataBase.Focus();

                MessageBox.Show(ex.ToString());

                return false;
            }
        }

        /// <summary>
        /// 데이터베이스 연결 정보를 XML로 Save
        /// </summary>
        private bool SaveDatabaseConfigXML()
        {
            try
            {
                string[] strDbValue = new string[5];

                string strConfigPath = Application.StartupPath + @"\Config\";
                string strConfigFile = "DB_Config.xml";

                if (this.ConnectionTest() != true)
                {
                    btnSave.Enabled = false;
                    txtDataBase.Focus();

                    MessageBox.Show("Water-NET 데이터베이스 연결 실패. 데이터베이스 정보를 다시 입력후 데스트 하십시오", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return false;
                }

                if (FunctionManager.IsDirectoryExists(strConfigPath) != true)
                {
                    FunctionManager.CreateDirectory(strConfigPath);
                }

                if (File.Exists(strConfigPath + strConfigFile) == true)
                {
                    File.Delete(strConfigPath + strConfigFile);
                }

                strDbValue[0] = txtDataBase.Text.Trim();    //IP
                strDbValue[1] = txtDataSource.Text.Trim();  //SID
                strDbValue[2] = txtUserID.Text.Trim();      //ID
                strDbValue[3] = txtPw.Text.Trim();          //Password
                strDbValue[4] = txtPort.Text.Trim();          //Port

                if (this.CreateDatabaseConfigFile(strDbValue, strConfigPath + strConfigFile) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }                
            }
            catch
            {
                btnSave.Enabled = false;
                txtDataBase.Focus();

                return false;
            }
        }

        /// <summary>
        /// Database Connection Information 정보를 XML파일로 생성한다.
        /// </summary>
        /// <param name="strValue">서브로 만들어질 목록</param>
        /// <param name="XMLfile">파일경로</param>
        public bool CreateDatabaseConfigFile(string[] strValue, string XMLfile)
        {
            try
            {
                XmlTextWriter textWriter = new XmlTextWriter(XMLfile, System.Text.Encoding.Default);
                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("Root");
                textWriter.WriteEndElement();
                textWriter.Close();

                XmlDocument doc = new XmlDocument();
                doc.Load(XMLfile);

                XmlNode insertNode;
                XmlDocumentFragment docInsert = doc.CreateDocumentFragment();

                docInsert.InnerXml = "<waternet>"
                                   + "<servicename>" + EMFrame.utils.ConvertUtils.EncryptKey(strValue[1].Trim()) + "</servicename>"
                                   + "<ip>" + EMFrame.utils.ConvertUtils.EncryptKey(strValue[0].Trim()) + "</ip>"
                                   + "<id>" + EMFrame.utils.ConvertUtils.EncryptKey(strValue[2].Trim()) + "</id>"
                                   + "<password>" + EMFrame.utils.ConvertUtils.EncryptKey(strValue[3].Trim()) + "</password>"
                                   + "<port>" + EMFrame.utils.ConvertUtils.EncryptKey(strValue[4].Trim()) + "</port>"
                                   + "</waternet>";

                insertNode = doc.DocumentElement;
                insertNode.InsertAfter(docInsert, insertNode.LastChild);
                doc.Save(XMLfile);

                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
}
