﻿using System;
using System.Data;
using System.Net;
using System.Text;
using System.Windows.Forms;
using EMFrame.log;


namespace WaterNet.WU_PipeNetwork
{
    /// <summary>
    /// Water-NET 로그온
    /// 
    /// </summary>
    public partial class frmLogOn : Form
    {
        #region 프로퍼티

        private bool _LOGON;
        private string username;
        /// <summary>
        /// 로그온 성공여부 (true 성공)
        /// </summary>
        public bool IsLogOn
        {
            get
            {
                return _LOGON;
            }
            set
            {
                _LOGON = value;
            }
        }

        #endregion

        public frmLogOn()
        {
            InitializeComponent();
        
        }

        /// <summary>
        /// 화면초기화
        /// </summary>
        private void InitializeControlSetting()
        {
            this.splitContainer1.Panel2Collapsed = true;
            this.Size = new System.Drawing.Size(340, 153);

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);

        }

        private void frmLogOn_Load(object sender, EventArgs e)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "ID.ini";

            if (!System.IO.File.Exists(strPath + strFile)) this.txtUID.Text = string.Empty;
            else
            {
                System.IO.StreamReader sr = null;
                try
                {
                    sr = System.IO.File.OpenText(strPath + strFile);
                    if (sr.Peek() >= 0)
                    {
                        this.txtUID.Text = sr.ReadLine();
                    }
                }
                catch (Exception)
                {
                    this.txtUID.Text = string.Empty;
                }
                finally
                {
                    sr.Close();
                }
            }

            this.InitializeControlSetting();
            this.cboHEADQUARTER.SelectedIndexChanged += new EventHandler(cboHEADQUARTER_SelectedIndexChanged);

            if (this.cboHEADQUARTER.Items.Count > 0)
            {
                cboHEADQUARTER_SelectedIndexChanged(this, new EventArgs());
            }
        }

        private void cboHEADQUARTER_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "' AND ZONE_GBN = '" + EMFrame.statics.AppStatic.ZONE_GBN + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), false);
        }

        #region Button Events
        /// <summary>
        /// 1차 로그인 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogon_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                frmDuplicateUser oform = new frmDuplicateUser(this.txtUID.Text.Trim());
                oform.TopMost = true;
                oform.StartPosition = FormStartPosition.CenterScreen;
                oform.ShowDialog(this);
                Console.WriteLine(oform.USERID);
                this.username = oform.USERNAME;
                //로그정보에서 중복된 사용자 체크
                _LOGON = this.LogOnProcess(oform.USERID);
                if (!_LOGON) return;
                
                ///사용자의 지역본부 및 센터를 기본으로 표시한다.
                this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;

                this.SecondLogOnProcess();

                this.Size = new System.Drawing.Size(340, 290);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 타부서 조회 권한을 체크하여 본부 및 지역센터를 선택할 수있도록 한다.
        /// </summary>
        private void SecondLogOnProcess()
        {
            this.splitContainer1.Panel2Collapsed = false;
            if (EMFrame.statics.AppStatic.USER_RIGHT.Equals("0"))  //일반사용자
            {
                this.splitContainer1.Panel2Collapsed = true;
                this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                this.cboHEADQUARTER_SelectedIndexChanged(this.cboHEADQUARTER, new EventArgs());
                this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;

                btnConnect_Click(this, new EventArgs());
            }
            else
            {
                switch (EMFrame.statics.AppStatic.EXT_YN)  //타부서조회 권한
                {
                    case "0":  //사용안함
                        this.splitContainer1.Panel2Collapsed = true;
                        this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                        this.cboHEADQUARTER_SelectedIndexChanged(this.cboHEADQUARTER, new EventArgs());
                        this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                        btnConnect_Click(this, new EventArgs());
                        break;
                    case "1":  //지역본부
                        //로그인 성공이면, 서비스센터를 선택하는 부분 표시한다.
                        this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                        this.cboHEADQUARTER.Enabled = false;
                        this.cboHEADQUARTER_SelectedIndexChanged(this.cboHEADQUARTER, new EventArgs());
                        break;
                    case "2":  //본사 관리자
                        //로그인 성공이면, 서비스센터를 선택하는 부분 표시한다.
                        this.cboHEADQUARTER.Enabled = true;
                        this.cboHEADQUARTER_SelectedIndexChanged(this.cboHEADQUARTER, new EventArgs());
                        break;
                }
            }
        }

        /// <summary>
        /// 2차 로그인 실제접속 버튼 클릭
        /// 로그정보는 ClientListener.cs의 네트워크 모듈에서 처리됨.
        /// 데이터(상수관망 및 지형도) 경로를 전역변수에 저장함.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (this.cboHEADQUARTER.SelectedItem == null) return;
            if (this.cboManageDept.SelectedItem == null)
            {
                MessageBox.Show("접속할 서비스센터를 선택하지 않았습니다.\n\r다시 선택하십시오.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();
            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {

                mapper = new EMFrame.dm.EMapper("SVR");
                if (mapper == null)
                {
                    MessageBox.Show(Convert.ToString(this.cboManageDept.SelectedValue) + " 데이터베이스 경로가 설정되지 않았습니다.");
                    return;
                }


                oStringBuilder.AppendLine("SELECT DISTINCT b.rohqcd, b.rohqnm, a.mgdpcd, a.mgdpnm, a.data_path, a.zone_gbn ");
                oStringBuilder.AppendLine("       ,NVL(a.MIN_X,0.0) MIN_X, NVL(a.MAX_X,0.0) MAX_X, NVL(a.MIN_Y,0.0) MIN_Y, NVL(a.MAX_Y,0.0) MAX_Y ");
                oStringBuilder.AppendLine("       FROM MANAGEMENT_DEPARTMENT a, REGIONAL_HEADQUARTER b ");
                oStringBuilder.AppendLine("WHERE a.rohqcd = b.rohqcd and a.MGDPCD = '" + this.cboManageDept.SelectedValue + "'");
                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new IDataParameter[] { });

                if (table == null || table.Rows.Count == 0)
                {
                    EMFrame.statics.AppStatic.DATA_DIR = Application.StartupPath + @"\WaterNET-Data";
                    EMFrame.statics.AppStatic.USER_SGCNM = Convert.ToString(this.cboManageDept.SelectedText);
                    EMFrame.statics.AppStatic.USER_SGCCD = Convert.ToString(this.cboManageDept.SelectedValue);
                    //MessageBox.Show(Convert.ToString(this.cboManageDept.SelectedValue) + " 의 데이터 경로가 설정되지 않았습니다.");
                    //return;
                }
                else
                {
                    EMFrame.statics.AppStatic.DATA_DIR = @Convert.ToString(table.Rows[0]["DATA_PATH"]);
                    EMFrame.statics.AppStatic.USER_SGCCD = Convert.ToString(table.Rows[0]["MGDPCD"]);
                    EMFrame.statics.AppStatic.USER_SGCNM = Convert.ToString(table.Rows[0]["MGDPNM"]);
                    EMFrame.statics.AppStatic.USER_HEADQUARTECD = Convert.ToString(table.Rows[0]["ROHQCD"]);
                    EMFrame.statics.AppStatic.USER_HEADQUARTENM = Convert.ToString(table.Rows[0]["ROHQCD"]);
                    //EMFrame.statics.AppStatic.ZONE_GBN = Convert.ToString(table.Rows[0]["ZONE_GBN"]);

                    EMFrame.statics.AppStatic.MIN_X = Convert.ToDouble(table.Rows[0]["MIN_X"]);
                    EMFrame.statics.AppStatic.MAX_X = Convert.ToDouble(table.Rows[0]["MAX_X"]);
                    EMFrame.statics.AppStatic.MIN_Y = Convert.ToDouble(table.Rows[0]["MIN_Y"]);
                    EMFrame.statics.AppStatic.MAX_Y = Convert.ToDouble(table.Rows[0]["MAX_Y"]);
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            this.Close();
        }

        private bool GetAvailableDirName(string Directory)
        {
            return (System.IO.Directory.Exists(Directory));
        }

        /// <summary>
        /// 데이터 폴더 생성
        /// </summary>
        /// <returns></returns>
        private bool CreateDataDirectory(string parent, string folder)
        {
            bool result = false;

            if (System.IO.Directory.Exists(folder))
            {
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo("aa");
                

            }
            else
            {
                System.IO.Directory.CreateDirectory(parent + "\\" + folder);
            
            
            }

            return result;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _LOGON = false;
            this.Close();
        }



        #endregion


        #region Control Events

        private void txtUID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtPWD.Focus();
            }
        }

        private void txtPWD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogon_Click(sender, e);
            }
        }

        private void txtPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            ////문자 or 숫자 && Back Space && \r (Return)
            //if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            //{
            //    e.Handled = true;
            //}

            if (e.KeyChar == 13)
            {
                this.btnLogon.Focus();
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 로그온 성공여부
        /// </summary>
        /// <returns></returns>
        private bool LogOnProcess(string strUserID)
        {
            bool blRtn = false;
            string strEncUID = string.Empty;
            string strEncUPWD = string.Empty;

            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                //User ID는 그대로 Password는 UserID+Password로 인크립트
                strEncUID = strUserID; // this.txtUID.Text.Trim();
                strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(this.username.Trim() + this.txtPWD.Text.Trim());

                EMFrame.utils.LoggerUtils.logger.Info("로그인  " + (this.txtUID.Text.Trim() + this.txtPWD.Text.Trim() + strEncUPWD));

                //Console.WriteLine(strEncUPWD);
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT * FROM V_USER                   ");
                oStringBuilder.AppendLine(" WHERE USER_ID = '" + strEncUID + "'"   );
                oStringBuilder.AppendLine("   AND USER_PWD = '" + strEncUPWD + "'");
                oStringBuilder.AppendLine("   AND USE_YN = '1'");

                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });

                if (table.Rows.Count == 0)
                {
                    MessageBox.Show("Water-NET 사용자 ID 및 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return blRtn;
                }

                if (Convert.IsDBNull(table.Rows[0]["L_USER_GROUP_ID"]))
                {
                    MessageBox.Show("사용자(" + strEncUID + ")는 시스템의 메뉴권한이 설정되지 않았습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return blRtn;
                }

                EMFrame.statics.AppStatic.USER_ID = strUserID; //this.txtUID.Text.Trim();
                EMFrame.statics.AppStatic.USER_NAME = table.Rows[0]["USER_NAME"].ToString();
                EMFrame.statics.AppStatic.USER_DIVISION = table.Rows[0]["USER_DIVISION"].ToString();
                EMFrame.statics.AppStatic.USER_RIGHT = table.Rows[0]["USER_RIGHT"].ToString();
                EMFrame.statics.AppStatic.USER_GROUP = table.Rows[0]["L_USER_GROUP_ID"].ToString();
                EMFrame.statics.AppStatic.EXT_YN = table.Rows[0]["EXT_YN"].ToString();
                EMFrame.statics.AppStatic.USER_SGCCD = Convert.ToString(table.Rows[0]["MGDPCD"]);
                EMFrame.statics.AppStatic.USER_SGCNM = Convert.ToString(table.Rows[0]["MGDPNM"]);
                EMFrame.statics.AppStatic.USER_HEADQUARTECD = Convert.ToString(table.Rows[0]["ROHQCD"]);
                EMFrame.statics.AppStatic.USER_HEADQUARTENM = Convert.ToString(table.Rows[0]["ROHQCD"]);
                //EMFrame.statics.AppStatic.ZONE_GBN = Convert.ToString(table.Rows[0]["ZONE_GBN"]);

                EMFrame.statics.AppStatic.LOGIN_DT = DateTime.Now.ToString("yyyyMMddHHmmss");
                //Dns.GetHostName();
                IPHostEntry MyIPHostEntry = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress MyIpAddress in MyIPHostEntry.AddressList)
                {
                    if (MyIpAddress.IsIPv6LinkLocal != true) //IPv4의 IP인 경우 즉, IPv6가 아닌 경우
                    {
                        EMFrame.statics.AppStatic.USER_IP = MyIpAddress.ToString();
                        break;
                    }
                }

                blRtn = true;

                EMFrame.utils.LoggerUtils.logger.Info("로그인");

                //비밀번호 변경 여부
                if (!"".Equals(table.Rows[0]["PWDT"].ToString()))
                {
                    DateTime pwdt = DateTime.ParseExact(table.Rows[0]["PWDT"].ToString(), "yyyyMMdd", null).AddMonths(3);
                    if (pwdt < DateTime.Now)
                    {
                        MessageBox.Show("비밀번호 사용기간이 3개월이 지났습니다.\n변경하시기 바랍니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FormPopup.frmChangePWD oForm = new FormPopup.frmChangePWD();
                        oForm.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("비밀번호 사용기간이 3개월이 지났습니다.\n변경하시기 바랍니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FormPopup.frmChangePWD oForm = new FormPopup.frmChangePWD();
                    oForm.ShowDialog();
                }
            }
            catch (Exception oException)
            {   
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return blRtn;
        }

        /// <summary>
        /// 중복접속 사용자를 체크한다.
        /// 나중에 네트워크로 접속하는 방안으로 변경필요.
        /// </summary>
        private void DuplicateIsLogin()
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", this.txtUID.Text.Trim());

            System.Data.DataTable datatable = EMFrame.utils.DatabaseUtils.GetInstance().IsBeforeLogin(param);

            if (datatable.Rows.Count > 0)
            {
                MessageBox.Show(this.txtUID.Text.Trim() + " 는 이미 로그인되어 있습니다." + "\n\r" +
                                "기존 로그인 정보를 삭제합니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);

                EMFrame.utils.DatabaseUtils.GetInstance().DisableBeforeLogin(param);
            }
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 001을 반환</param>
        /// <returns>string Code</returns>
        private string SplitToCode(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[1].Substring(0, strArr[1].Length - 1);
            }
            else
            {
                if (strArr[0].Trim() == "전체")
                {
                    strCode = " ";
                }
                else
                {
                    strCode = strArr[0].Trim();
                }
            }

            return strCode;
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드명 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 산정정수장을 반환</param>
        /// <returns>string Code Name</returns>
        private string SplitToCodeName(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[0].Substring(0, strArr[0].Length);
            }
            else
            {
                strCode = strArr[0].Trim();
            }

            return strCode;
        }

        #endregion

        /// <summary>
        /// 사용자ID를 임시파일에 등록한다.
        /// 로그인시 임시파일을 읽어 초기표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtUID.Text.Trim()))
            {
                MessageBox.Show(" 사용자ID가 입력되지 않았습니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
          
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "ID.ini";

            System.IO.FileStream oFileStream = null;
            try
            {
                oFileStream = new System.IO.FileStream(strPath + strFile, System.IO.FileMode.Create);

                oFileStream.Write(UTF8Encoding.UTF8.GetBytes(this.txtUID.Text.Trim()), 0, UTF8Encoding.UTF8.GetByteCount(this.txtUID.Text.Trim()));
                oFileStream.Flush();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                oFileStream.Close();
                oFileStream.Dispose();
            }
        }




    }
}
