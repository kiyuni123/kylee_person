﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;

using WaterNet.WaterAOCore;

namespace WaterNet.WU_PipeNetwork
{
    /// <summary>
    /// Project ID : WN_WC_A00
    /// Project Explain : 상수관망의 네트워크를 구성을 위한 클래스
    /// Project Developer : 전병록
    /// Project Create Date : 2010.10.13
    /// Class Explain : 상수관로-급수관로-수도계량기 구성
    /// </summary>
    public class NetworkOperator
    {
        private TreeView m_tree;

        private IFeatureLayer m_pPipeLayer; //상수관로
        private IFeatureLayer m_pSplyLayer; //급수관로
        private IFeatureLayer m_pMetaLayer; //수도계량기

        private ISpatialFilter m_MetaQueryfilter;
        private ISpatialFilter m_SplyQueryfilter;

        private IFeatureIndex2 m_MetaFeatureIndex;
        private IIndexQuery2 m_MetaIndexQuery;

        private IFeatureIndex2 m_PipeFeatureIndex;
        private IIndexQuery2 m_PipeIndexQuery;

        private IFeatureIndex2 m_SplyFeatureIndex;
        private IIndexQuery2 m_SplyIndexQuery;

        //private int m_RunID;

        //private StringBuilder m_log;
        private ArrayList m_log;

        #region 생성자 -----------------------------------------------------------------------------------
        /// <summary>
        /// 기본 생성자
        /// </summary>
        /// <param name="ultraGrid"></param>
        public NetworkOperator(IFeatureLayer pipe, IFeatureLayer sply, IFeatureLayer meta)
        {
            this.m_pPipeLayer = pipe; this.m_pSplyLayer = sply; this.m_pMetaLayer = meta;
            InitializeSetting();
        }

        /// <summary>
        /// 환경 초기화 - 공통적으로 사용하는 쿼리필더 생성
        /// </summary>
        private void InitializeSetting()
        {
            m_MetaQueryfilter = new SpatialFilterClass();
            m_MetaQueryfilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelTouches;
            m_MetaQueryfilter.GeometryField = MetaLayer.FeatureClass.ShapeFieldName;
            m_MetaQueryfilter.WhereClause = string.Empty;

            m_SplyQueryfilter = new SpatialFilterClass();
            m_SplyQueryfilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelTouches;
            m_SplyQueryfilter.GeometryField = SplyLayer.FeatureClass.ShapeFieldName;
            m_SplyQueryfilter.WhereClause = string.Empty;

        }
        #endregion 생성자 --------------------------------------------------------------------------------

        #region Public Property ---------------------------------------------------------------------------
        /// <summary>
        /// Focus Map
        /// </summary>
        public TreeView treeview
        {
            set
            {
                m_tree = value;
            }
        }

        public ArrayList log
        {
            set
            {
                m_log = value;
            }
        }

        public IFeatureIndex2 PipeFeatureIndex
        {
            set
            {
                m_PipeFeatureIndex = value;
                m_PipeIndexQuery = (IIndexQuery2)m_PipeFeatureIndex;
            }
        }

        public IFeatureIndex2 MetaFeatureIndex
        {
            set
            {
                m_MetaFeatureIndex = value;
                m_MetaIndexQuery = (IIndexQuery2)m_MetaFeatureIndex;
            }
        }

        public IFeatureIndex2 SplyFeatureIndex
        {
            set
            {
                m_SplyFeatureIndex = value;
                m_SplyIndexQuery = (IIndexQuery2)m_SplyFeatureIndex;
            }
        }

        #endregion Public Property ------------------------------------------------------------------------

        #region Private Property ------------------------------------------------------------------------
        /// <summary>
        /// 상수관로 레이어()
        /// </summary>
        private IFeatureLayer PipeLayer
        {
            get
            {  
                return m_pPipeLayer;
            }
        }

        /// <summary>
        /// 급수관로 레이어
        /// </summary>
        private IFeatureLayer SplyLayer
        {
            get
            {
                return m_pSplyLayer;
            }
        }

        /// <summary>
        /// 수도계량기 레이어
        /// </summary>
        private IFeatureLayer MetaLayer
        {
            get
            {  
                return m_pMetaLayer;
            }
        }
        #endregion Private Property ------------------------------------------------------------------------

        #region Public Method ----------------------------------------------------------------------------
        public void Execute()
        {
            foreach (TreeNode node in m_tree.Nodes)
            {
                //Console.WriteLine("ThreedID : " + m_RunID.ToString() + " : " + node.Index.ToString() + " : " + node.Text);
                ///상수관로 객체
                IFeature splyFeature = SplyLayer.FeatureClass.GetFeature(Convert.ToInt32(node.Name));
                if (splyFeature == null) continue;
                string strOwner = splyFeature.Class.AliasName + "," + splyFeature.get_Value(splyFeature.Fields.FindField("FTR_IDN")).ToString() + "," + splyFeature.OID.ToString();

                IPoint pPoint = getStartPoint(splyFeature);
                if (pPoint == null) continue;
                object iDs;

                IGeometry pGeom = GetBuffer((IGeometry)pPoint, 0.1);

                m_SplyIndexQuery.IntersectedFeatures(pGeom, out iDs);
                if (iDs != null)
                {
                    int[] iD = iDs as int[];
                    if (iD.Length != 0)
                    {
                        foreach (int ObjectID in iD)
                        {
                            if (splyFeature.OID == ObjectID) continue;
                            IFeature pFeature = SplyLayer.FeatureClass.GetFeature(ObjectID);
                            if (pFeature != null)
                            {
                                m_log.Add(strOwner + "," + pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString());
                            }

                        }
                    }
                }

                m_PipeIndexQuery.IntersectedFeatures(pGeom, out iDs);
                if (iDs != null)
                {
                    int[] iD = iDs as int[];
                    if (iD.Length != 0)
                    {
                        foreach (int ObjectID in iD)
                        {
                            //if (splyFeature.OID == ObjectID) continue;
                            IFeature pFeature = PipeLayer.FeatureClass.GetFeature(ObjectID);
                            if (pFeature != null)
                            {
                                m_log.Add(strOwner + "," + pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString());
                            }

                        }
                    }
                }

                m_MetaIndexQuery.IntersectedFeatures(pGeom, out iDs);
                if (iDs != null)
                {
                    int[] iD = iDs as int[];
                    if (iD.Length != 0)
                    {
                        foreach (int ObjectID in iD)
                        {
                            //if (splyFeature.OID == ObjectID) continue;
                            IFeature pFeature = MetaLayer.FeatureClass.GetFeature(ObjectID);
                            if (pFeature != null)
                            {
                                m_log.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString() + "," + strOwner);
                            }

                        }
                    }
                }

                pPoint = getEndPoint(splyFeature);
                if (pPoint == null) continue;

                pGeom = GetBuffer((IGeometry)pPoint, 0.1);
                m_SplyIndexQuery.IntersectedFeatures(pGeom, out iDs);
                if (iDs != null)
                {
                    int[] iD = iDs as int[];
                    if (iD.Length != 0)
                    {
                        foreach (int ObjectID in iD)
                        {
                            if (splyFeature.OID == ObjectID) continue;
                            IFeature pFeature = SplyLayer.FeatureClass.GetFeature(ObjectID);
                            if (pFeature != null)
                            {
                                m_log.Add(strOwner + "," + pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString());
                            }

                        }
                    }
                }

                m_PipeIndexQuery.IntersectedFeatures(pGeom, out iDs);
                if (iDs != null)
                {
                    int[] iD = iDs as int[];
                    if (iD.Length != 0)
                    {
                        foreach (int ObjectID in iD)
                        {
                            //if (splyFeature.OID == ObjectID) continue;
                            IFeature pFeature = PipeLayer.FeatureClass.GetFeature(ObjectID);
                            if (pFeature != null)
                            {
                                m_log.Add(strOwner + "," + pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString());
                            }

                        }
                    }
                }
                
                m_MetaIndexQuery.IntersectedFeatures(pGeom, out iDs);
                if (iDs != null)
                {
                    int[] iD = iDs as int[];
                    if (iD.Length != 0)
                    {
                        foreach (int ObjectID in iD)
                        {
                            //if (splyFeature.OID == ObjectID) continue;
                            IFeature pFeature = MetaLayer.FeatureClass.GetFeature(ObjectID);
                            if (pFeature != null)
                            {
                                m_log.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString() + "," + strOwner);
                            }

                        }
                    }
                }
                /////상수관에 연결된 급수관로 객체
                //SplyList = GetLinkedSply(pipeFeature);

                //ExecuteSply2Level(node, SplyList);
            }

        }
        #endregion Public Method ----------------------------------------------------------------------------

        #region Private Method ----------------------------------------------------------------------------
        /// <summary>
        /// 시작점
        /// </summary>
        /// <param name="pFeature"></param>
        /// <returns></returns>
        private IPoint getStartPoint(IFeature pFeature)
        {
            IPoint pPoint = null;
            if (pFeature.Shape.GeometryType == esriGeometryType.esriGeometryPolyline)
            {
                IPolyline pPolyline = pFeature.Shape as IPolyline;
                pPoint = new PointClass();
                pPoint = pPolyline.FromPoint;
            }

            return pPoint;
        }

        /// <summary>
        /// 종료점
        /// </summary>
        /// <param name="pFeature"></param>
        /// <returns></returns>
        private IPoint getEndPoint(IFeature pFeature)
        {
            IPoint pPoint = null;
            if (pFeature.Shape.GeometryType == esriGeometryType.esriGeometryPolyline)
            {
                IPolyline pPolyline = pFeature.Shape as IPolyline;
                pPoint = new PointClass();
                pPoint = pPolyline.ToPoint;
            }

            return pPoint;
        }

        /// <summary>
        /// Point 객체 비교 = 0이면 동일한 위치
        /// </summary>
        /// <param name="pJunctionPoint"></param>
        /// <param name="pCrossPoint"></param>
        /// <returns></returns>
        private int CompareLocation(IPoint pPoint, IPoint pCrossPoint)
        {
            return pPoint.Compare(pCrossPoint);
        }

        private IGeometry GetBuffer(IGeometry pGeometry, double distance)
        {
            ITopologicalOperator pTopoOp = pGeometry as ITopologicalOperator;
            pTopoOp.Simplify();

            return pTopoOp.Buffer(distance);
        }
        #endregion Private Method ----------------------------------------------------------------------------
    }
}
