﻿namespace WaterNet.WU_TagLinkage
{
    partial class frmWUMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWUMain));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label_Desc = new System.Windows.Forms.Label();
            this.label_FN = new System.Windows.Forms.Label();
            this.label_FR = new System.Windows.Forms.Label();
            this.label_TagName = new System.Windows.Forms.Label();
            this.comboBox_FN = new System.Windows.Forms.ComboBox();
            this.comboBox_BR = new System.Windows.Forms.ComboBox();
            this.textBox_Desc = new System.Windows.Forms.TextBox();
            this.textBox_TagName = new System.Windows.Forms.TextBox();
            this.button_Select = new System.Windows.Forms.Button();
            this.button_Sel = new System.Windows.Forms.Button();
            this.label_Layer = new System.Windows.Forms.Label();
            this.comboBox_Layer = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ultraGrid_TagList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraGrid_TagName = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagList)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagName)).BeginInit();
            this.SuspendLayout();
            // 
            // spcContents
            // 
            this.spcContents.Location = new System.Drawing.Point(24, 169);
            this.spcContents.Size = new System.Drawing.Size(1103, 414);
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            this.axToolbar.Size = new System.Drawing.Size(840, 28);
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // _frmMapUnpinnedTabAreaRight
            // 
            this._frmMapUnpinnedTabAreaRight.Location = new System.Drawing.Point(1127, 0);
            // 
            // _frmMapUnpinnedTabAreaTop
            // 
            this._frmMapUnpinnedTabAreaTop.Size = new System.Drawing.Size(1103, 0);
            // 
            // _frmMapUnpinnedTabAreaBottom
            // 
            this._frmMapUnpinnedTabAreaBottom.Size = new System.Drawing.Size(1103, 0);
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.TabIndex = 12;
            // 
            // tabTOC
            // 
            this.tabTOC.Size = new System.Drawing.Size(251, 233);
            // 
            // spcTOC
            // 
            this.spcTOC.Size = new System.Drawing.Size(251, 233);
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            // 
            // tabPageTOC
            // 
            this.tabPageTOC.Size = new System.Drawing.Size(243, 207);
            // 
            // pnlLayerFunc
            // 
            this.pnlLayerFunc.Location = new System.Drawing.Point(0, 186);
            // 
            // spcIndexMap
            // 
            this.spcIndexMap.Size = new System.Drawing.Size(257, 414);
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            this.axTOC.Size = new System.Drawing.Size(243, 186);
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(840, 355);
            this.axMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axMap_OnMouseDown);
            this.axMap.OnMouseUp += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseUpEventHandler(this.axMap_OnMouseUp);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(24, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label_Desc);
            this.splitContainer1.Panel1.Controls.Add(this.label_FN);
            this.splitContainer1.Panel1.Controls.Add(this.label_FR);
            this.splitContainer1.Panel1.Controls.Add(this.label_TagName);
            this.splitContainer1.Panel1.Controls.Add(this.comboBox_FN);
            this.splitContainer1.Panel1.Controls.Add(this.comboBox_BR);
            this.splitContainer1.Panel1.Controls.Add(this.textBox_Desc);
            this.splitContainer1.Panel1.Controls.Add(this.textBox_TagName);
            this.splitContainer1.Panel1.Controls.Add(this.button_Select);
            this.splitContainer1.Panel1.Controls.Add(this.button_Sel);
            this.splitContainer1.Panel1.Controls.Add(this.label_Layer);
            this.splitContainer1.Panel1.Controls.Add(this.comboBox_Layer);
            this.splitContainer1.Panel1.Controls.Add(this.btnSave);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(1103, 169);
            this.splitContainer1.SplitterDistance = 33;
            this.splitContainer1.TabIndex = 11;
            // 
            // label_Desc
            // 
            this.label_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Desc.AutoSize = true;
            this.label_Desc.Location = new System.Drawing.Point(752, 13);
            this.label_Desc.Name = "label_Desc";
            this.label_Desc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_Desc.Size = new System.Drawing.Size(53, 12);
            this.label_Desc.TabIndex = 14;
            this.label_Desc.Text = "태그설명";
            // 
            // label_FN
            // 
            this.label_FN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_FN.AutoSize = true;
            this.label_FN.Location = new System.Drawing.Point(611, 13);
            this.label_FN.Name = "label_FN";
            this.label_FN.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_FN.Size = new System.Drawing.Size(57, 12);
            this.label_FN.TabIndex = 13;
            this.label_FN.Text = " 기능기호";
            // 
            // label_FR
            // 
            this.label_FR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_FR.AutoSize = true;
            this.label_FR.Location = new System.Drawing.Point(475, 13);
            this.label_FR.Name = "label_FR";
            this.label_FR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_FR.Size = new System.Drawing.Size(53, 12);
            this.label_FR.TabIndex = 12;
            this.label_FR.Text = "변량기호";
            // 
            // label_TagName
            // 
            this.label_TagName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_TagName.AutoSize = true;
            this.label_TagName.Location = new System.Drawing.Point(299, 13);
            this.label_TagName.Name = "label_TagName";
            this.label_TagName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_TagName.Size = new System.Drawing.Size(41, 12);
            this.label_TagName.TabIndex = 11;
            this.label_TagName.Text = "태그명";
            // 
            // comboBox_FN
            // 
            this.comboBox_FN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_FN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_FN.FormattingEnabled = true;
            this.comboBox_FN.Location = new System.Drawing.Point(672, 8);
            this.comboBox_FN.Name = "comboBox_FN";
            this.comboBox_FN.Size = new System.Drawing.Size(71, 20);
            this.comboBox_FN.TabIndex = 10;
            // 
            // comboBox_BR
            // 
            this.comboBox_BR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_BR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BR.FormattingEnabled = true;
            this.comboBox_BR.Location = new System.Drawing.Point(534, 8);
            this.comboBox_BR.Name = "comboBox_BR";
            this.comboBox_BR.Size = new System.Drawing.Size(71, 20);
            this.comboBox_BR.TabIndex = 9;
            // 
            // textBox_Desc
            // 
            this.textBox_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Desc.Location = new System.Drawing.Point(812, 8);
            this.textBox_Desc.Name = "textBox_Desc";
            this.textBox_Desc.Size = new System.Drawing.Size(120, 21);
            this.textBox_Desc.TabIndex = 8;
            // 
            // textBox_TagName
            // 
            this.textBox_TagName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_TagName.Location = new System.Drawing.Point(347, 8);
            this.textBox_TagName.Name = "textBox_TagName";
            this.textBox_TagName.Size = new System.Drawing.Size(123, 21);
            this.textBox_TagName.TabIndex = 7;
            // 
            // button_Select
            // 
            this.button_Select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Select.Location = new System.Drawing.Point(938, 5);
            this.button_Select.Name = "button_Select";
            this.button_Select.Size = new System.Drawing.Size(76, 25);
            this.button_Select.TabIndex = 6;
            this.button_Select.Text = "조회";
            this.button_Select.UseVisualStyleBackColor = true;
            this.button_Select.Click += new System.EventHandler(this.button_Select_Click);
            // 
            // button_Sel
            // 
            this.button_Sel.Location = new System.Drawing.Point(207, 7);
            this.button_Sel.Name = "button_Sel";
            this.button_Sel.Size = new System.Drawing.Size(50, 25);
            this.button_Sel.TabIndex = 5;
            this.button_Sel.Text = "선택";
            this.button_Sel.UseVisualStyleBackColor = true;
            this.button_Sel.Click += new System.EventHandler(this.button_Sel_Click);
            // 
            // label_Layer
            // 
            this.label_Layer.Location = new System.Drawing.Point(12, 13);
            this.label_Layer.Name = "label_Layer";
            this.label_Layer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_Layer.Size = new System.Drawing.Size(64, 14);
            this.label_Layer.TabIndex = 3;
            this.label_Layer.Text = "레이어";
            // 
            // comboBox_Layer
            // 
            this.comboBox_Layer.FormattingEnabled = true;
            this.comboBox_Layer.Location = new System.Drawing.Point(80, 9);
            this.comboBox_Layer.Name = "comboBox_Layer";
            this.comboBox_Layer.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Layer.TabIndex = 2;
            this.comboBox_Layer.SelectedIndexChanged += new System.EventHandler(this.comboBox_Layer_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(1018, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 25);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ultraGrid_TagList);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(39, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1064, 132);
            this.panel4.TabIndex = 2;
            // 
            // ultraGrid_TagList
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_TagList.DisplayLayout.Appearance = appearance13;
            this.ultraGrid_TagList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_TagList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ultraGrid_TagList.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_TagList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_TagList.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_TagList.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ultraGrid_TagList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_TagList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagList.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_TagList.DisplayLayout.Override.CellAppearance = appearance20;
            this.ultraGrid_TagList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_TagList.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagList.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ultraGrid_TagList.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ultraGrid_TagList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_TagList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_TagList.DisplayLayout.Override.RowAppearance = appearance23;
            this.ultraGrid_TagList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_TagList.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ultraGrid_TagList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_TagList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_TagList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_TagList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_TagList.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_TagList.Name = "ultraGrid_TagList";
            this.ultraGrid_TagList.Size = new System.Drawing.Size(1064, 132);
            this.ultraGrid_TagList.TabIndex = 0;
            this.ultraGrid_TagList.Text = "ultraGrid1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnRight);
            this.panel3.Controls.Add(this.btnLeft);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(39, 132);
            this.panel3.TabIndex = 1;
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(4, 63);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(30, 25);
            this.btnRight.TabIndex = 1;
            this.btnRight.Text = "->";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(4, 34);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(30, 25);
            this.btnLeft.TabIndex = 0;
            this.btnLeft.Text = "<-";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraGrid_TagName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(440, 132);
            this.panel1.TabIndex = 0;
            // 
            // ultraGrid_TagName
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_TagName.DisplayLayout.Appearance = appearance4;
            this.ultraGrid_TagName.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_TagName.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagName.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagName.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.ultraGrid_TagName.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagName.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.ultraGrid_TagName.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_TagName.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_TagName.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_TagName.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.ultraGrid_TagName.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_TagName.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagName.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_TagName.DisplayLayout.Override.CellAppearance = appearance5;
            this.ultraGrid_TagName.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_TagName.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagName.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.ultraGrid_TagName.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.ultraGrid_TagName.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_TagName.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_TagName.DisplayLayout.Override.RowAppearance = appearance10;
            this.ultraGrid_TagName.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_TagName.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.ultraGrid_TagName.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_TagName.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_TagName.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_TagName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_TagName.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_TagName.Name = "ultraGrid_TagName";
            this.ultraGrid_TagName.Size = new System.Drawing.Size(440, 132);
            this.ultraGrid_TagName.TabIndex = 1;
            this.ultraGrid_TagName.Text = "ultraGrid2";
            // 
            // frmWUMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.ClientSize = new System.Drawing.Size(1127, 583);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmWUMain";
            this.Load += new System.EventHandler(this.frmWUMain_Load);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagList)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label_Layer;
        private System.Windows.Forms.ComboBox comboBox_Layer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_TagList;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_TagName;
        private System.Windows.Forms.Button button_Sel;
        private System.Windows.Forms.TextBox textBox_Desc;
        private System.Windows.Forms.TextBox textBox_TagName;
        private System.Windows.Forms.Button button_Select;
        private System.Windows.Forms.ComboBox comboBox_BR;
        private System.Windows.Forms.ComboBox comboBox_FN;
        private System.Windows.Forms.Label label_TagName;
        private System.Windows.Forms.Label label_FR;
        private System.Windows.Forms.Label label_FN;
        private System.Windows.Forms.Label label_Desc;
    }
}
