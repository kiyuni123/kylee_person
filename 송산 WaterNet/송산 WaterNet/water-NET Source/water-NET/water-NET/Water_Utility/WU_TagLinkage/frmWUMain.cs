﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Oracle.DataAccess.Client;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
#endregion

using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WU_TagLinkage
{
    public partial class frmWUMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        private ILayer m_Layer = null;     //현재 작업레이어
        private IFeature m_Feature = null; //현재 작업 객체

        public frmWUMain()
        {
            InitializeComponent();

            InitializeSetting();
        }

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....

            #region 그리드 설정

            UltraGridColumn oUltraGridColumn;
            oUltraGridColumn = ultraGrid_TagName.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "TAGNAME";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            oUltraGridColumn = ultraGrid_TagName.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "IO_GBN";
            oUltraGridColumn.Header.Caption = "유입/유출";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            object[,] aoSource = {
                {"INF",   "유입"},
                {"OUT",   "유출"}
            };
            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(aoSource);
            oUltraGridColumn.ValueList = objValue;

            oUltraGridColumn = ultraGrid_TagName.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DESCRIPTION";
            oUltraGridColumn.Header.Caption = "TAG설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            WaterNetCore.FormManager.SetGridStyle(ultraGrid_TagName);
            WaterNetCore.FormManager.ColumeAllowEdit(ultraGrid_TagName, 1, 2);
            //------------------------------------------------------------------

            oUltraGridColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "TAGNAME";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            oUltraGridColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "IO_GBN";
            oUltraGridColumn.Header.Caption = "유입/유출";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            object[,] aoSource2 = {
                {"INF",   "유입"},
                {"OUT",   "유출"}
            };
            Infragistics.Win.ValueList objValue2 = new Infragistics.Win.ValueList();
            objValue = FormManager.SetValueList(aoSource2);
            oUltraGridColumn.ValueList = objValue2;

            oUltraGridColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DESCRIPTION";
            oUltraGridColumn.Header.Caption = "TAG설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            oUltraGridColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "USE_YN";
            oUltraGridColumn.Header.Caption = "사용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            oUltraGridColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_IDN";
            oUltraGridColumn.Header.Caption = "FTR_IDN";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;

            WaterNetCore.FormManager.SetGridStyle(ultraGrid_TagList);

            #endregion 그리드 설정

            #region 컨트롤 초기화
            //콤보박스 데이터 초기화 
        
            object[,] aoSource3 = {
                {"정수장",   "정수장"},
                {"유량계",   "유량계"},
                {"가압장",   "가압장"},
                {"배수지",   "배수지"},
                {"수압계",   "수압계"}                
            };
            FormManager.SetComboBoxEX(comboBox_Layer, aoSource3, false);
            #endregion

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT BR_CODE CD, BR_CODE CDNM");
            oStringBuilder.AppendLine("  FROM IF_IHTAGS ");
            oStringBuilder.AppendLine(" WHERE BR_CODE IS NOT NULL");
            oStringBuilder.AppendLine(" ORDER BY BR_CODE ASC ");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_BR, oStringBuilder.ToString(), true);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT DISTINCT FN_CODE CD, FN_CODE CDNM");
            oStringBuilder.AppendLine("  FROM IF_IHTAGS ");
            oStringBuilder.AppendLine(" WHERE FN_CODE IS NOT NULL");
            oStringBuilder.AppendLine(" ORDER BY FN_CODE ASC ");
            WaterNetCore.FormManager.SetComboBoxEX(comboBox_FN, oStringBuilder.ToString(), true);

            getTagList(string.Empty);

        }

        /// <summary>
        /// TAG LIST 질의
        /// </summary>
        private void getTagList(string sWhere)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                oStringBuilder.AppendLine("SELECT TAGNAME, TAG_GBN, IO_GBN, DESCRIPTION, USE_YN, FTR_IDN ");
                oStringBuilder.AppendLine("FROM IF_IHTAGS");
                if (!string.IsNullOrEmpty(sWhere))
                {
                    oStringBuilder.AppendLine(sWhere);
                }
                oStringBuilder.AppendLine("ORDER BY TAGNAME ASC");

                //Clipboard.SetText(oStringBuilder.ToString());
                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "TAGLIST");

                ultraGrid_TagList.DataSource = oDataTable;


            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        #region IForminterface 멤버
        public string FormID
        {
            get { return "IWater(Tag) 맵핑"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();

            //추가적인 구현은 여기에....
            SetGeneralizeLayer();

        }

        /// <summary>
        /// SetGeneralizeLayer - 지도화면에 레이어를 숨기거나, 렌더링을 설정한다.
        /// </summary>
        private void SetGeneralizeLayer()
        {
            string[] strLayers = {"재염소처리지점","중요시설","감시지점","누수지점","민원지점","관세척구간",
                                  "수압계","소방시설","스탠드파이프","표고점","수원지","밸브실","철도","도로경계선",
                                  "등고선","담장","법정읍면동","건물","대블록"};

            ILayer pLayer = null;
            foreach (string item in strLayers)
            {
                pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, item);
                if (pLayer == null) continue;
                pLayer.Visible = false;
            }
        }

        private void comboBox_Layer_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// 비즈니스 로직에서는 추가저장이지만
        /// 실제는 DB에 업데이트한다
        /// </summary>
        private void setUpdateQuery()
        {
            //작업레이어 및 선택객체가 없을경우 return
            if (m_Layer == null | m_Feature == null) return;

            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                oStringBuilder.AppendLine("UPDATE IF_IHTAGS SET                     ");
                oStringBuilder.AppendLine("       TAG_GBN = :TAG_GBN,               ");
                oStringBuilder.AppendLine("       IO_GBN  = :IO_GBN,                ");
                oStringBuilder.AppendLine("       DESCRIPTION = :DESCRIPTION,       ");
                oStringBuilder.AppendLine("       USE_YN  = :USE_YN,                ");
                oStringBuilder.AppendLine("       FTR_IDN = :FTR_IDN                ");
                oStringBuilder.AppendLine("WHERW TAGNAME = :TAGNAME                 ");

                oDBManager.BeginTransaction();

                foreach (UltraGridRow oRow in ultraGrid_TagName.Rows)
                {
                    if (oRow.Tag.Equals('I') | oRow.Tag.Equals('U'))    //추가 or 수정됨
                    {
                        IDataParameter[] oParams = {
                            new OracleParameter(":TAG_GBN",OracleDbType.Varchar2,48),
                            new OracleParameter(":IO_GBN",OracleDbType.Varchar2,48),
                            new OracleParameter(":DESCRIPTION",OracleDbType.Varchar2,200),
                            new OracleParameter(":USE_YN",OracleDbType.Varchar2,48),
                            new OracleParameter(":FTR_IDN",OracleDbType.Varchar2,48),
                            new OracleParameter(":TAGNAME",OracleDbType.Varchar2,48)
                        };


                        oParams[0].Value = m_Layer.Name;                 //TAG_GBN,   
                        oParams[1].Value = oRow.Cells["IO_GBN"].Value;  //IO_GBN,    
                        oParams[2].Value = oRow.Cells["DESCRIPTION"].Value;  //DESCRIPTION
                        oParams[3].Value = "Y";                          //USE_YN,    
                        oParams[4].Value = ArcManager.GetValue(m_Feature, "FTR_IDN");  //FTR_IDN    
                        oParams[5].Value = oRow.Cells["TAGNAME"].Value;  //TAGNAME  

                        oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                    }
                }
                oDBManager.CommitTransaction();

            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }

        }

        /// <summary>
        /// 비즈니스 로직에서는 삭제이지만
        /// 실제는 DB에 업데이트한다
        /// </summary>
        private void setDeleteQuery()
        {
            //작업레이어 및 선택객체가 없을경우 return
            if (m_Layer == null | m_Feature == null) return;

            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                oStringBuilder.AppendLine("UPDATE IF_IHTAGS SET                     ");
                oStringBuilder.AppendLine("       TAG_GBN = :TAG_GBN,               ");
                oStringBuilder.AppendLine("       FTR_IDN = :FTR_IDN                ");
                oStringBuilder.AppendLine("WHERE TAGNAME = :TAGNAME                 ");

                oDBManager.BeginTransaction();

                UltraGridRow oRow = ultraGrid_TagName.ActiveRow;

                IDataParameter[] oParams = {
                    new OracleParameter(":TAG_GBN",OracleDbType.Varchar2,48),
                    new OracleParameter(":FTR_IDN",OracleDbType.Varchar2,48),
                    new OracleParameter(":TAGNAME",OracleDbType.Varchar2,48)
                };


                oParams[0].Value = "";                  //TAG_GBN,   
                oParams[1].Value = "";                  //IO_GBN,    
                oParams[2].Value = oRow.Cells["TAGNAME"].Value;  //TAGNAME  

                oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
            
                oDBManager.CommitTransaction();

                oRow.Delete(false);
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        private void getTagName(string tag_gbn, string ftr_idn)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            OracleDBManager oDBManager = new OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();

                oStringBuilder.AppendLine("SELECT TAGNAME, DESCRIPTION, IO_GBN");
                oStringBuilder.AppendLine("  FROM IF_IHTAGS");
                oStringBuilder.AppendLine(" WHERE TAG_GBN = '" + tag_gbn +"'");
                oStringBuilder.AppendLine("   AND FTR_IDN = '" + ftr_idn +"'");

                //Clipboard.SetText(oStringBuilder.ToString());
                DataTable oDataTable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "TAGNAME");

                ultraGrid_TagName.DataSource = oDataTable;

            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 그리드 치환버튼 설정
        /// </summary>
        private void SetButtonStatus()
        {
            if (m_Layer != null && m_Feature != null)
            {
                btnLeft.Enabled = true;
                btnRight.Enabled = true;
            }
            else
            {
                btnLeft.Enabled = false;
                btnRight.Enabled = false;
            }

        }

        private IGeometry DrawRectangle(ESRI.ArcGIS.Carto.IActiveView activeView)
        {
            if (activeView == null) return null;

            ESRI.ArcGIS.Display.IScreenDisplay screenDisplay = activeView.ScreenDisplay;

            // Constant.
            screenDisplay.StartDrawing(screenDisplay.hDC, (System.Int16)ESRI.ArcGIS.Display.esriScreenCache.esriNoScreenCache); // Explicit cast.
            ESRI.ArcGIS.Display.IRgbColor rgbColor = new ESRI.ArcGIS.Display.RgbColorClass();
            rgbColor.Red = 255;

            ESRI.ArcGIS.Display.IColor color = rgbColor; // Implicit cast.
            ESRI.ArcGIS.Display.ISimpleFillSymbol simpleFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbolClass();
            simpleFillSymbol.Color = color;

            ESRI.ArcGIS.Display.ISymbol symbol = simpleFillSymbol as ESRI.ArcGIS.Display.ISymbol; // Dynamic cast.
            ESRI.ArcGIS.Display.IRubberBand rubberBand = new ESRI.ArcGIS.Display.RubberEnvelopeClass();
            ESRI.ArcGIS.Geometry.IGeometry geometry = rubberBand.TrackNew(screenDisplay, null);
            screenDisplay.SetSymbol(symbol);
            screenDisplay.DrawRectangle(geometry as ESRI.ArcGIS.Geometry.IEnvelope);
            // Dynamic cast.
            screenDisplay.FinishDrawing();

            return geometry;
        }

        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;

            IActiveView activeView = axMap.ActiveView;
            IScreenDisplay screenDisplay = activeView.ScreenDisplay;
            ISimpleFillSymbol fillSymbol = new SimpleFillSymbolClass();
            IRgbColor rgbColor = new RgbColorClass();
            rgbColor.Red = 255;
            fillSymbol.Color = rgbColor;
            fillSymbol.Color.Transparency = 35;
            IRubberBand rubberEnv = new RubberEnvelopeClass();
            IEnvelope pSearchEnv = (IEnvelope)rubberEnv.TrackNew(screenDisplay, null);

            if (pSearchEnv == null | pSearchEnv.IsEmpty) return;
            //screenDisplay.StartDrawing(screenDisplay.hDC, (short)esriScreenCache.esriNoScreenCache);
            //screenDisplay.SetSymbol((ISymbol)fillSymbol);
            //screenDisplay.DrawRectangle(pSearchEnv);
            //screenDisplay.FinishDrawing();


            IGeometry pGeom = pSearchEnv as IGeometry;
            pGeom.SpatialReference = ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");
            
            try
            {
                ISpatialFilter pSpatialFilter = new SpatialFilterClass();
                pSpatialFilter.Geometry = pGeom;
                pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;

                m_Layer = ArcManager.GetMapLayer((IMapControl3)this.axMap.Object, comboBox_Layer.SelectedValue.ToString());
                if (m_Layer == null) return;

                IFeatureSelection pFeatureSel = FeatureSelect(m_Layer, pGeom, esriSpatialRelEnum.esriSpatialRelContains, string.Empty);
                pFeatureSel.SelectionChanged();

                if (pFeatureSel.SelectionSet.Count == 0)
                {
                    MessageManager.ShowInformationMessage("선택되어진 객체가 없습니다. 다시 선택하십시오.");
                    return;
                }

                if (pFeatureSel.SelectionSet.Count > 1)
                {
                    MessageManager.ShowInformationMessage(pFeatureSel.SelectionSet.Count.ToString() + " 개의 객체가 선택되었습니다. 다시 선택하십시오.");
                    return;
                }

                //IElement pElem = new RectangleElementClass();
                //pElem.Geometry = pSearchEnv;
                //axMap.ActiveView.GraphicsContainer.AddElement(pElem, 0);

                ICursor pCursor = null;
                pFeatureSel.SelectionSet.Search(null, false, out pCursor);
                if (pCursor != null)
                {
                    IFeature pFeature = pCursor.NextRow() as IFeature;
                    if (pFeature != null)
                    {
                        m_Feature = pFeature; //멤버변수에 저장
                        getTagName(m_Layer.Name, Convert.ToString(ArcManager.GetValue(pFeature, "FTR_IDN")));
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw ex;
            }
            finally
            {
                ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeoSelection | esriViewDrawPhase.esriViewGraphics);
            }


            SetButtonStatus();  //그리드 치환버튼 Enable설정
            
        }



        private void axMap_OnMouseUp(object sender, IMapControlEvents2_OnMouseUpEvent e)
        {
            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeoSelection | esriViewDrawPhase.esriViewGraphics);
            
            
        }

        #region "객체선택하기 - FeatureSelect(pLayer, pFeature)"

        private void FeatureSelect(ILayer pLayer, IFeature pFeature)
        {
            IFeatureSelection pFeatureSelection = default(IFeatureSelection);
            IColor pColor = ArcManager.GetColor(255, 192, 203); //분홍색
            if (pColor == null)
            {
                MessageBox.Show("not Assigned(pColor)");
                return;
            }

            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
            ESRI.ArcGIS.Display.ISymbol pSymbol = null;

            switch (pFeature.Shape.GeometryType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbol();
                    //pMarkerSymbol = ArcManager.MakeCharacterMarkerSymbol(20, 17, 0.0, pColor);
                    //pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
                    //pMarkerSymbol.Color = pColor;
                    //pMarkerSymbol.Size = 20;
                    pSymbol = ArcManager.MakeCharacterMarkerSymbol(20, 17, 0.0, pColor);
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                    pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbol();
                    pLineSymbol.Width = 4;
                    pLineSymbol.Color = pColor;
                    pSymbol = pLineSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbol();
                    pFillSymbol.Outline = null;
                    pFillSymbol.Color = pColor;
                    pSymbol = pFillSymbol as ISymbol;
                    break;
            }

            pFeatureSelection = (IFeatureSelection)pLayer;
            pFeatureSelection.Clear();

            pFeatureSelection.SetSelectionSymbol = true;
            pFeatureSelection.SelectionSymbol = pSymbol;
            //pFeatureSelection.BufferDistance = 0.1
            pFeatureSelection.Add(pFeature);
        }

        private IFeatureSelection FeatureSelect(ILayer pLayer, IGeometry pGeometry, esriSpatialRelEnum eSpatialRel, string sWhereClause)
        {

            IFeatureSelection pFeatureSelection = default(IFeatureSelection);

            IColor pColor = ArcManager.GetColor(128, 0, 0); //초록색
            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
            ESRI.ArcGIS.Display.ISymbol pSymbol = null;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)pLayer;

            switch (pFeatureLayer.FeatureClass.ShapeType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbol();
                    pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
                    pMarkerSymbol.Color = pColor;
                    pSymbol = pMarkerSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                    pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbol();
                    pLineSymbol.Width = 4;
                    pLineSymbol.Color = pColor;
                    pSymbol = pLineSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbol();
                    pFillSymbol.Outline = null;
                    pFillSymbol.Color = pColor;
                    pSymbol = pFillSymbol as ISymbol;
                    break;
            }

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pGeometry;
            pSpatialFilter.GeometryField = pFeatureLayer.FeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = eSpatialRel;
            pSpatialFilter.WhereClause = sWhereClause;

            pFeatureSelection = (IFeatureSelection)pLayer;
            pFeatureSelection.Clear();

            pFeatureSelection.SetSelectionSymbol = true;
            pFeatureSelection.SelectionSymbol = pSymbol;
            pFeatureSelection.SelectFeatures((IQueryFilter)pSpatialFilter, esriSelectionResultEnum.esriSelectionResultNew, false);

            return pFeatureSelection;
        }

        private void FeatureSelect(ILayer pLayer, string sWhereClause)
        {
	        IQueryFilter pQueryFilter = new QueryFilter();
	        IFeatureSelection pFeatureSelection = default(IFeatureSelection);
            
	        IColor pColor = ArcManager.GetColor(128, 0, 0); //초록색
	        ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
	        ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
	        ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
	        ESRI.ArcGIS.Display.ISymbol pSymbol = null;

	        IFeatureLayer pFeatureLayer = (IFeatureLayer)pLayer;

	        switch (pFeatureLayer.FeatureClass.ShapeType) {
		        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
			        pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbol();
			        pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
			        pMarkerSymbol.Color = pColor;
			        pSymbol = pMarkerSymbol as ISymbol;
			        break;
		        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
			        pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbol();
			        pLineSymbol.Width = 4;
			        pLineSymbol.Color = pColor;
			        pSymbol = pLineSymbol as ISymbol;
			        break;
		        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
			        pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbol();
			        pFillSymbol.Outline = null;
			        pFillSymbol.Color = pColor;
			        pSymbol = pFillSymbol as ISymbol;
			        break;
	        }

	        pQueryFilter.WhereClause = sWhereClause;

	        pFeatureSelection = (IFeatureSelection)pLayer;
	        pFeatureSelection.Clear();

	        pFeatureSelection.SetSelectionSymbol = true;
	        pFeatureSelection.SelectionSymbol = pSymbol;
	        pFeatureSelection.SelectFeatures(pQueryFilter, esriSelectionResultEnum.esriSelectionResultNew, false);

        }

        #endregion

        #region "선택된 객체 렌더링 - GetFeatureSelectionColor()"
        private IColor GetFeatureSelectionColor()
        {
            IColor pColor = ArcManager.GetColor(255, 192, 203); //분홍색

            return pColor;
        }
        #endregion

        /// <summary>
        /// 지도 객체 선택 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Sel_Click(object sender, EventArgs e)
        {
            toolActionCommand.Checked = true;
            base.toolActionCommand_Click(this, new EventArgs());
        }

        /// <summary>
        /// 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                setUpdateQuery();

                getTagList(getQueryWheres());
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색조건의 where조건절 생성하기
        /// </summary>
        /// <returns></returns>
        private string getQueryWheres()
        {
            string sWhere = string.Empty;
            if (!string.IsNullOrEmpty(textBox_TagName.Text))
            {
                if (sWhere.Length == 0)
                {
                    sWhere = " WHERE TAGNAME LIKE '" + textBox_TagName.Text.ToUpper() + "%'";
                }
                else
                {
                    sWhere += " AND TAGNAME LIKE '" + textBox_TagName.Text.ToUpper() + "%'";
                }
            }

            if (!string.IsNullOrEmpty(textBox_Desc.Text))
            {
                if (sWhere.Length == 0)
                {
                    sWhere = " WHERE DESCRIPTION LIKE '" + textBox_Desc.Text.ToUpper() + "%'";
                }
                else
                {
                    sWhere += " AND DESCRIPTION LIKE '" + textBox_Desc.Text.ToUpper() + "%'";
                }
            }


            if (comboBox_BR.SelectedIndex > 0)
            {
                if (sWhere.Length == 0)
                {
                    sWhere = " WHERE BR_CODE = '" + comboBox_BR.SelectedValue.ToString() + "'";
                }
                else
                {
                    sWhere += " AND BR_CODE = '" + comboBox_BR.SelectedValue.ToString() + "'";
                }
            }


            if (comboBox_FN.SelectedIndex > 0)
            {
                if (sWhere.Length == 0)
                {
                    sWhere = " WHERE FN_CODE = '" + comboBox_FN.SelectedValue.ToString() + "'";
                }
                else
                {
                    sWhere += " AND FN_CODE = '" + comboBox_FN.SelectedValue.ToString() + "'";
                }
            }

            return sWhere;
        }

        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Select_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                getTagList(getQueryWheres());
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// TagName 삭제버튼 클릭
        /// 삭제시 DB에 직접 Update할까
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRight_Click(object sender, EventArgs e)
        {
            //작업레이어 및 선택객체가 없을경우 return
            if (m_Layer == null | m_Feature == null) return;
            //삭제 -> DB에 Update
            setDeleteQuery();
        }

        /// <summary>
        /// TagName 추가버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLeft_Click(object sender, EventArgs e)
        {
            //작업레이어 및 선택객체가 없을경우 return
            if (m_Layer == null | m_Feature == null) return;
            UltraGridRow ListRow = ultraGrid_TagList.ActiveRow;
            if (ListRow == null) return;

            /////동일한 tagname이 있으면 skip
            foreach (UltraGridRow item in ultraGrid_TagName.Rows)
            {
                if (Convert.ToString(item.Cells["TAGNAME"].Value).Equals(Convert.ToString(ListRow.Cells["TAGNAME"].Value))) return;
            }

            UltraGridRow NameRow = ultraGrid_TagName.DisplayLayout.Bands[0].AddNew();
            NameRow.Cells["TAGNAME"].Value = ListRow.Cells["TAGNAME"].Value;
            NameRow.Cells["DESCRIPTION"].Value = ListRow.Cells["DESCRIPTION"].Value;
            NameRow.Cells["IO_GBN"].Value = ListRow.Cells["IO_GBN"].Value;

        }

        /// <summary>
        /// 화면 로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWUMain_Load(object sender, EventArgs e)
        {
            //====================================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //====================================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["ToolStripMenuTagLinkage"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }

            //===========================================================================

            ultraGrid_TagName.AfterRowInsert += new RowEventHandler(WaterNet.WaterNetCore.FormManager.AfterRowInsert);
            ultraGrid_TagName.AfterCellUpdate += new CellEventHandler(WaterNet.WaterNetCore.FormManager.AfterCellUpdate);
        }
    }
}
