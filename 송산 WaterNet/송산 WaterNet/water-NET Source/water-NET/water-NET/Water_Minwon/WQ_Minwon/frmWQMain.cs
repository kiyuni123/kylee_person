﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using EMFrame.log;

namespace WaterNet.WQ_Minwon
{
    /// <summary>
    /// Project ID : WN_WQ_A06
    /// Project Explain : 수질민원관리, 기본적으로 WaterAOCore.frmMap을 사용
    /// Project Developer : 오두석
    /// Project Create Date : 2010.10.15
    /// Form Explain : 수질민원관리 Main Form
    /// </summary>
    public partial class frmWQMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        ArrayList m_ArrayContents = new ArrayList(); //민원지점 간략 폼배열

        ILayer m_Layer = null; //민원지점 레이어

        string m_SFTR_IDN = string.Empty;   //소블록 FTR_IDN

        public frmWQMain()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버 (메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.)

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
            //axToolbar.AddItem("esriControls.ControlsAddDataCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();

            #region - WQ_AppStatic에 필요한 Object Set
            //Database Setting
            WQ_AppStatic.ORACLE_MANAGER = new OracleDBManager();
            WQ_AppStatic.ORACLE_MANAGER.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            WQ_AppStatic.ORACLE_MANAGER.Open();
            if (WQ_AppStatic.ORACLE_MANAGER == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다. 확인 후 다시 시작하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            //IMAP Set
            WQ_AppStatic.IMAP = (IMapControl3)axMap.Object;
            WQ_AppStatic.ITOC = (ITOCControl2)axTOC.Object;

            WQ_AppStatic.IS_SHOW_FORM_BRIEF_DETAIL = false;
            WQ_AppStatic.IS_SHOW_FORM_COLOR_DATA = false;
            WQ_AppStatic.IS_SHOW_FORM_DATA_MANAGE = false;
            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE = false;
            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE2 = false;
            WQ_AppStatic.IS_SHOW_FORM_REPORT = false;

            #endregion

            m_Layer = WaterAOCore.ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_Pipegraphic, "민원지점");
            axMap.AddLayer(m_Layer);
            WaterAOCore.ArcManager.SetDefaultRenderer(axMap.ActiveView.FocusMap, (IFeatureLayer)m_Layer);
            m_Layer.Visible = true;

            //Control Popup Form Load
            FormPopup.frmColor oForm = new FormPopup.frmColor();
            oForm.StartPosition = FormStartPosition.Manual;
            oForm.TopLevel = false;
            oForm.Parent = this;
            oForm.Tag = "Color";
            oForm.SetBounds(this.Width - (this.axMap.Width + 18), this.axToolbar.Height + 6, 355, 97);
            oForm.BringToFront();
            oForm.Visible = true;
        }

        private void frmWQMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_ArrayContents.Clear();
        }


        #region Button Events

        private void btnMWPointManage_Click(object sender, EventArgs e)
        {
            if (WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE == false)
            {
                FormPopup.frmWQMWPointManage oForm = new FormPopup.frmWQMWPointManage();

                oForm.TopLevel = false;
                oForm.Parent = this;            
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 96, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }   
        }

        private void btnMWDataManage_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (WQ_AppStatic.IS_SHOW_FORM_DATA_MANAGE == false)
                {
                    FormPopup.frmWQMWResultManage oForm = new FormPopup.frmWQMWResultManage();

                    oForm.TopLevel = false;
                    oForm.Parent = this;
                    oForm.StartPosition = FormStartPosition.Manual;
                    oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 96, oForm.Width, oForm.Height);
                    oForm.BringToFront();
                    oForm.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
           
        }

        private void btnMWReport_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (WQ_AppStatic.IS_SHOW_FORM_REPORT == false)
                {
                    WaterNet.WQ_Minwon.FormPopup.frmWQPMWReport oForm = new WaterNet.WQ_Minwon.FormPopup.frmWQPMWReport();
                    oForm.TopLevel = false;
                    oForm.Parent = this;
                    oForm.StartPosition = FormStartPosition.Manual;
                    oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 96, oForm.Width, oForm.Height);
                    oForm.BringToFront();
                    oForm.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        #endregion


        #region Control Events

        /// <summary>
        /// Map에서 오른쪽 마우스 버튼 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            ///*****************************************************************************
            ///GIS MAP에서 Mouse 다운이 일어난 위치
            ///----------------------------------------------------------------------------S
            ///GIS 상 좌표
            WQ_AppStatic.IMAP_X = e.mapX;
            WQ_AppStatic.IMAP_Y = e.mapY;
            ///Windows 상 좌표
            WQ_AppStatic.WIN_X = e.x;
            WQ_AppStatic.WIN_Y = e.y;

            WQ_AppStatic.CURRENT_IGEOMETRY = null;

            ///----------------------------------------------------------------------------E

            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;

            #region 위치선택 - 지도에 사고지점을 선택하고, 별표 표시

            if (WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE2 == true)
            {
                ///기존 객체 및 환경 초기화
                ArcManager.ClearSelection(axMap.ActiveView.FocusMap);
                IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
                pGraphicsContainer.DeleteAllElements();

                IActiveView activeView = axMap.ActiveView;
                IScreenDisplay screenDisplay = activeView.ScreenDisplay;
                IColor pColor = ArcManager.GetColor(255, 0, 0);

                ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
                IRubberBand pRubberBand = new RubberPointClass();
                IPoint pSearchPoint = (IPoint)pRubberBand.TrackNew(screenDisplay, pSymbol);

                if (pSearchPoint == null | pSearchPoint.IsEmpty) return;

                IElement pElement = new MarkerElementClass();
                pElement.Geometry = pSearchPoint as IGeometry;
                IMarkerElement pMarkerElement = pElement as IMarkerElement;
                pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;

                pGraphicsContainer.AddElement(pElement, 0);
                ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
                Application.DoEvents();
                ArcManager.FlashShape(axMap.ActiveView, pSearchPoint as IPoint, 3);
            }

            #endregion 지도에 사고지점을 선택하고, 별표 표시 끝

            IPoint pPoint = new PointClass();
            pPoint.X = WQ_AppStatic.IMAP_X;
            pPoint.Y = WQ_AppStatic.IMAP_Y;
            pPoint.Z = 0;

            IGeometry pGeom = (IGeometry)pPoint;

            WQ_AppStatic.CURRENT_IGEOMETRY = pGeom;

            ///*****************************************************************************
            ///ArcManager.GetSpatialCursor()로 Map에 마우스 클릭한 Map 상 좌표에서 반경 10m 
            ///지점의 특정 값을 가져와 FeatureCursor로 만들고 FeatureCursor로 Feature를
            ///만든 다음, ArcManager.GetValue()로 Layer 상의 특정 Field값을 Object로 받는다.
            ///----------------------------------------------------------------------------S
            double dblContains = 0;

            if (ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)100000)
            {
                dblContains = 300;
            }
            else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)100000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)50000)
            {
                dblContains = 200;
            }
            else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)50000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)15000)
            {
                dblContains = 100;
            }
            else
            {
                dblContains = 10;
            }

            IFeatureCursor pIFCursor = ArcManager.GetSpatialCursor(m_Layer, WQ_AppStatic.GetCurrentIGeometryFromXY(), dblContains, esriSpatialRelEnum.esriSpatialRelContains, string.Empty);

            IFeature pFeature = pIFCursor.NextFeature();

            if (pFeature != null)
            {
                object oCANO = WaterAOCore.ArcManager.GetValue(pFeature, "CANO");
                
                if (oCANO != null)
                {
                    foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
                    this.MakeControlsOnlySelectedPoint(oCANO.ToString());
                    IEnvelope pEnvelope = axMap.Extent;
                    this.ExtentUpdatedEvent(pEnvelope);
                }
                else
                {
                    foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
                }
            }
            else
            {
                foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
            }

            ///----------------------------------------------------------------------------E
            switch (e.button)
            {
                case 1:    //왼쪽버튼
                    break;

                case 2:    //오른쪽버튼
                    ILayer pLayerS = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");
                    IFeatureCursor pIFCursorS = ArcManager.GetSpatialCursor(pLayerS, WQ_AppStatic.GetCurrentIGeometryFromXY(), (double)1, esriSpatialRelEnum.esriSpatialRelIntersects, string.Empty);

                    IFeature pFeatureS = pIFCursorS.NextFeature();

                    if (pFeatureS != null)
                    {
                        object oSFTR_IDN = WaterAOCore.ArcManager.GetValue(pFeatureS, "FTR_IDN");

                        if (oSFTR_IDN != null)
                        {
                            this.m_SFTR_IDN = oSFTR_IDN.ToString();
                        }
                    }

                    ContextMenuStripPopup.Show(axMap, WQ_AppStatic.WIN_X, WQ_AppStatic.WIN_Y);
                    break;
            }
        }

        private void axMap_OnExtentUpdated(object sender, IMapControlEvents2_OnExtentUpdatedEvent e)
        {
            //Map의 축척이 15000 이하일 경우에만 간략 폼을 뷰한다.
            //if (ArcManager.GetMapScale((IMapControl3)axMap.Object) < (double)15000)
            //{
            //    this.MakeControls();
            //    this.ExtentUpdatedEvent(e.newEnvelope as IEnvelope);
            //}
            //else
            //{
            //    foreach (var item in m_ArrayContents) ((Form)item).Visible = false;
            //}
        }

        private void 민원조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WaterNet.WQ_Minwon.FormPopup.frmWQPMWReport oForm = new WaterNet.WQ_Minwon.FormPopup.frmWQPMWReport();
            oForm.SFTR_IDN = this.m_SFTR_IDN;
            oForm.TopLevel = false;
            oForm.Parent = this;
            oForm.StartPosition = FormStartPosition.Manual;
            oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 96, oForm.Width, oForm.Height);
            oForm.BringToFront();
            oForm.Show();
        }

        #endregion


        #region User Function

        /// <summary>
        /// Array에 저장된 수질민원 간략화면을 가져온다
        /// </summary>
        /// <param name="nOID">유량계 OID</param>
        /// <returns></returns>
        private Form GetControl(string nOID)
        {
            foreach (var item in m_ArrayContents)
            {
                if (((Form)item).Tag.ToString() == nOID)
                {
                    return (Form)item;
                }
            }
            return null;
        }

        /// <summary>
        /// 현재 Extent에 Intersect되는 감시지점의 수질민원 간략화면 표시한다.
        /// </summary>
        /// <param name="pEnvelope"></param>
        private void ExtentUpdatedEvent(IEnvelope pEnvelope)
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;

            if (!pFeatureLayer.Visible) return;
            if (pFeatureLayer.MaximumScale > axMap.MapScale && pFeatureLayer.MaximumScale != 0) return;
            if (pFeatureLayer.MinimumScale < axMap.MapScale && pFeatureLayer.MinimumScale != 0) return;

            //먼저 콤포넌트를 보이지 않게 한다.
            foreach (var item in m_ArrayContents) ((Form)item).Visible = false;

            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pEnvelope;
            pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;
            pSpatialFilter.WhereClause = string.Empty;

            IFeatureCursor pFCursor = pFeatureClass.Search(pSpatialFilter, true);
            IFeature pFeature = pFCursor.NextFeature();
            try
            {
                while (pFeature != null)
                {
                    IGeometry pGeom = pFeature.Shape;
                    if (pGeom is IPoint)
                    {
                        IPoint pPoint = pGeom as IPoint;
                        int x; int y;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.Units = esriUnits.esriMeters;
                        axMap.ActiveView.ScreenDisplay.DisplayTransformation.FromMapPoint(pPoint, out x, out y);

                        Form oControl = GetControl(Convert.ToString(pFeature.OID));
                        if (oControl != null)
                        {
                            oControl.SetBounds(x + 11, y + 25, 275, 150);
                            oControl.BringToFront();
                            oControl.Visible = true;
                            Application.DoEvents();
                        }
                    }
                    pFeature = pFCursor.NextFeature();
                }
            }
            catch (Exception oExec)
            {
                //Console.WriteLine(oExec.Message);
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pFCursor);
                
            }
        }

        /// <summary>
        /// MakeControl - 수질민원 데이터 화면 생성하여 Array에 저장
        /// </summary>
        private void MakeControls()
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ICursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, string.Empty);
            IRow pRow = pCursor.NextRow();
            try
            {
                //이미 등록된 Control(Form)을 클로즈 한다.
                foreach (var item in m_ArrayContents) ((Form)item).Close();
                m_ArrayContents.Clear();
                while (pRow != null)
                {
                    object o = WaterAOCore.ArcManager.GetValue(pRow, "FID");
                    object oCANO = WaterAOCore.ArcManager.GetValue(pRow, "CANO");
                    if (o != null)
                    {
                        FormPopup.frmMWBrief oControl = new FormPopup.frmMWBrief();
                        oControl.TopLevel = false;
                        oControl.Parent = this;
                        oControl.Tag = Convert.ToString(o);
                        oControl.CANO = oCANO.ToString();
                        m_ArrayContents.Add(oControl);
                    }
                    pRow = pCursor.NextRow();
                }
            }
            catch (Exception)
            {
                //Console.WriteLine("MakeControl");
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
            }
        }

        /// <summary>
        /// MakeControl - 수질민원 데이터 화면 생성하여 Array에 저장
        /// </summary>
        private void MakeControlsOnlySelectedPoint(string strCANO)
        {
            if (m_Layer == null) return;
            if (!(m_Layer is IFeatureLayer)) return;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)m_Layer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "CANO='" + strCANO + "'") as IFeatureCursor;
            IFeature pFeature = pCursor.NextFeature();
            try
            {
                //이미 등록된 Control(Form)을 클로즈 한다.
                foreach (var item in m_ArrayContents) ((Form)item).Close();
                m_ArrayContents.Clear();

                if (pFeature != null)
                {
                    FormPopup.frmMWBrief oControl = new FormPopup.frmMWBrief();
                    oControl.TopLevel = false;
                    oControl.Parent = this;
                    oControl.Tag = WaterAOCore.ArcManager.GetValue(pFeature, "FID").ToString();
                    oControl.CANO = strCANO;
                    m_ArrayContents.Add(oControl);
                }
            }
            catch (Exception)
            {
                //Console.WriteLine("MakeControl");
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
            }
        }

        #endregion
    }
}
