﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_Minwon.FormPopup
{
    public partial class frmMWBrief : Form
    {
        #region 프로퍼티

        private string _CANO;

        /// <summary>
        /// 민원관리번호
        /// </summary>
        public string CANO
        {
            get
            {
                return _CANO;
            }
            set
            {
                _CANO = value;
            }
        }
        #endregion

        public frmMWBrief()
        {
            InitializeComponent();
        }
        
        private void frmMWBrief_Load(object sender, EventArgs e)
        {
            this.GetBriefData();
        }

        #region Button Events

        private void btnDetail_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (WQ_AppStatic.IS_SHOW_FORM_BRIEF_DETAIL == false)
                {
                    FormPopup.frmMWBriefDetail oForm = new FormPopup.frmMWBriefDetail();
                    oForm.CANO = _CANO;
                    oForm.TopLevel = false;
                    oForm.Parent = this.Parent;
                    oForm.StartPosition = FormStartPosition.Manual;
                    //oForm.SetBounds(this.Left, this.Top + this.Height + 2, 798, 279);
                    oForm.BringToFront();
                    oForm.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        #endregion

        #region User Function

        /// <summary>
        /// MAP의 민원 Marker를 클릭시 취득한 민원관리번호로 간략정보를 표시한다.
        /// </summary>
        private void GetBriefData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.CANO, TO_CHAR(A.CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, B.CODE_NAME AS CAMIDCD");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE B ON B.PCODE = '9004' AND B.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("WHERE    A.CALRGCD = '2000' AND A.CANO = '" + _CANO + "'");
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT DESC");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                DataRow oDRow = pDS.Tables[0].Rows[0];

                this.lblCANO.Text = oDRow["CANO"].ToString();
                this.lblMWDT.Text = oDRow["CAAPPLDT"].ToString();
                this.txtDMNO.Text = "" + oDRow["DMNO"].ToString();
                this.txtMWNM.Text = "" + oDRow["CANM"].ToString();
                this.txtMWGBN.Text = "" + oDRow["CAMIDCD"].ToString();
            }
        }

        #endregion
    }
}
