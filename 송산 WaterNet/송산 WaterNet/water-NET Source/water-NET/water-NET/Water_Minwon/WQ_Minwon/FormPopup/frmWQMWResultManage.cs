﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;
using WaterNet.WaterAOCore;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_Minwon.FormPopup
{
    /// <summary>
    /// 민원 지점등록이 된 민원들에 대해 정보 및 처리 결과를 등록한다.
    /// </summary>
    public partial class frmWQMWResultManage : Form
    {
        //=======================================================================
        //
        //                    동진 수정_2012.6.07
        //                      권한박탈(조회만 가능)       
        //=======================================================================

        private void frmWQMWResultManage_Load(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["수질민원관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }
        }

        //==========================================================================================
        public frmWQMWResultManage()
        {
            InitializeComponent();

            this.InitializeGird();

            this.InitializeControl();

            WQ_AppStatic.IS_SHOW_FORM_DATA_MANAGE = true;
        }

        private void frmWQMWResultManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.ClosePopup();
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetRegisteredWQMinwon();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.SaveMinwonProcessResultData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void uGridMWReg_Click(object sender, EventArgs e)
        {
            if (this.uGridMWReg.Rows.Count <= 0) return;

            this.GetWQMinwonDetailProcessResultData();
        }

        private void uGridMWReg_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridMWReg.Rows.Count <= 0) return;

            this.ViewMap_SelectedMinwon();
        }

        #endregion


        #region User Function

        #region - Initialize Function

        /// <summary>
        /// Grid를 초기화 한다. Column 설정 등...
        /// </summary>
        private void InitializeGird()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region - 그리드 설정

            #region - 민원접수현황

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANO";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAAPPLDT";
            oUltraGridColumn.Header.Caption = "접수일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANM";
            oUltraGridColumn.Header.Caption = "민원인 성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACELL";
            oUltraGridColumn.Header.Caption = "연락처";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CALRGCD";
            oUltraGridColumn.Header.Caption = "민원구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAMIDCD";
            oUltraGridColumn.Header.Caption = "민원분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACONT";
            oUltraGridColumn.Header.Caption = "민원내용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SFTRIDN";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridMWReg);

            #endregion

            #region - Grid Caption Visible

            this.uGridMWReg.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            #endregion

            #endregion
        }

        /// <summary>
        /// 본 기능에 사용되는 Control을 초기화 한다. Data Setting 등..
        /// </summary>
        private void InitializeControl()
        {
            //민원분류
            WQ_Function.SetCombo_MinwonType(this.cboMWType);
            //접수일자
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateS, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtMWDateS, -90);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateE, 0, 0);
        }


        #endregion

        #region - Base Function

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_DATA_MANAGE = false;
            this.Dispose();
            this.Close();
        }

        #endregion

        #region - SQL Function

        /// <summary>
        /// 지점이 등록된 민원 Data를 Select 해서 Grid에 Set
        /// </summary>
        private void GetRegisteredWQMinwon()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strCAMIDCD = string.Empty;
            string strWHERE_PARAM = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strSDate = WQ_Function.StringToDateTime(this.udtMWDateS.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtMWDateE.DateTime);

            strCAMIDCD = WQ_Function.SplitToCode(this.cboMWType.Text).Trim();
                        
            if (strCAMIDCD == "")
            {
                strWHERE_PARAM = "";
            }
            else
            {
                strWHERE_PARAM = "AND A.CAMIDCD = '" + strCAMIDCD + "'";
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.CANO, TO_CHAR(CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, '' AS CACELL, A.CAADDR, D.CODE_NAME AS CALRGCD, E.CODE_NAME || ' (' || E.CODE || ')' AS CAMIDCD, replace(a.cacont,'개인정보 기입 금지' || chr(10),'') cacont, F.SFTRIDN");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE D ON D.PCODE = '9003' AND D.CODE = A.CALRGCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE E ON E.PCODE = '9004' AND E.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN WI_DMINFO F ON F.DMNO = A.DMNO");
            oStringBuilder.AppendLine("WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("         AND A.DMNO IS NOT NULL");
            oStringBuilder.AppendLine("         AND A.CALRGCD = '2000' AND SUBSTR(A.CAMIDCD, 0, 2) = '20' AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("         AND TO_CHAR(A.CAAPPLDT, 'RRRRMMDD') BETWEEN '" + strSDate + "' AND '" + strEDate + "' ");
            oStringBuilder.AppendLine("         AND A.CANO IN (SELECT X.CANO FROM WQ_MINWON X WHERE SUBSTR(X.MW_DATE, 0, 8) = TO_CHAR(A.CAAPPLDT, 'RRRRMMDD')) " + strWHERE_PARAM);
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT, A.CANO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridMWReg.DataSource = pDS.Tables["WI_CAINFO"].DefaultView;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGridMWReg);

            if (this.uGridMWReg.Rows.Count > 0) this.uGridMWReg.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region -- 처리결과

        /// <summary>
        /// 지점이 등록된 민원 Data의 처리결과를 Select해서 Control에 Set
        /// </summary>
        private void GetWQMinwonDetailProcessResultData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strCANO = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strCANO = uGridMWReg.ActiveRow.Cells[0].Text;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT       TO_CHAR(PRCSDT, 'RRRR-MM-DD') AS DT, DECODE(USERNM, NULL, ' ', USERNM) AS USERNM, DECODE(DEPT, NULL, ' ', DEPT) AS DEPT, DECODE(CAPRCSRSLT, NULL, ' ', CAPRCSRSLT) AS CAPRCSRSLT");
            oStringBuilder.AppendLine("FROM         WI_CAINFO");
            oStringBuilder.AppendLine("WHERE        CALRGCD = '2000' AND CANO = '" + strCANO + "'");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PROCRESULT");

            this.rtxtMWResult.Text = "";

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.udtPROCDT.Value = oDRow["DT"].ToString().Trim();
                    this.txtUName.Text = oDRow["USERNM"].ToString().Trim();
                    this.txtDEPT.Text = oDRow["DEPT"].ToString().Trim();

                    this.rtxtMWResult.Text = oDRow["CAPRCSRSLT"].ToString().Trim();
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 수질민원기본 Data의 처리결과 Data를 저장한다.
        /// </summary>
        private void SaveMinwonProcessResultData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strCANO = string.Empty; //수질민원일련번호
            string strPROC_DATE = string.Empty; //수질민원처리일자
            string strRESULT = string.Empty;
            bool IsValid = true;

            if (this.uGridMWReg.Rows.Count <= 0) return;

            strCANO = this.uGridMWReg.ActiveRow.Cells[0].Text;

            if (this.txtUName.Text.Trim() == "") IsValid = false;
            //if (this.txtDEPT.Text.Trim() == "") IsValid = false;
            if (this.rtxtMWResult.Text.Trim() == "") IsValid = false;

            if (IsValid == true)
            {
                //수질민원처리결과 - 일련번호, 접수일
                strPROC_DATE = WQ_Function.StringToDateTime(this.udtPROCDT.DateTime);
                strRESULT = this.rtxtMWResult.Text.Trim();
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("UPDATE   WI_CAINFO");
                oStringBuilder.AppendLine("SET      PRCSDT = '" + strPROC_DATE + "', USERNM = '" + this.txtUName.Text.Trim() + "', DEPT = '" + this.txtDEPT.Text.Trim() + "', CAPRCSRSLT = '" + strRESULT + "'");
                oStringBuilder.AppendLine("WHERE    CALRGCD = '2000' AND CANO = '" + strCANO + "'");

                WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);

                MessageBox.Show("선택하신 민원의 처리결과를 정상적으로 저장했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("선택하신 민원의 처리결과 항목을 모두 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #endregion

        #region - MAP Function

        /// <summary>
        /// Grid에서 선택된 관리번호로 MAP의 위치 이동 및 축척 확대
        /// </summary>
        private void ViewMap_SelectedMinwon()
        {
            string strKEY_VALUE = string.Empty;
            string strKEY_NAME = string.Empty;
            string strLAYER_NAME = string.Empty;
            double dblMAP_SCALE = 0;

            strLAYER_NAME = "민원지점";
            strKEY_NAME = "CANO";
            strKEY_VALUE = this.uGridMWReg.ActiveRow.Cells[0].Text;
            dblMAP_SCALE = 5000;

            if (WQ_AppStatic.IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strLAYER_NAME);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strKEY_NAME + "='" + strKEY_VALUE + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    IGeometry oIGeometry = pFeature.Shape;

                    ArcManager.FlashShape(WQ_AppStatic.IMAP.ActiveView, oIGeometry, 10);

                    if (ArcManager.GetMapScale(WQ_AppStatic.IMAP) > dblMAP_SCALE)
                    {
                        ArcManager.SetMapScale(WQ_AppStatic.IMAP, dblMAP_SCALE);
                    }

                    ArcManager.MoveCenterAt(WQ_AppStatic.IMAP, pFeature);

                    ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                    Application.DoEvents();
                }
                this.BringToFront();
            }
        }

        #endregion


    }
}
