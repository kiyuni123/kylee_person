﻿namespace WaterNet.WQ_Minwon.FormPopup
{
    partial class frmWQPMWReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.udtPROCDT = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.rtxtMWContent = new System.Windows.Forms.RichTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtDEPT = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtUName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.rtxtMWResult = new System.Windows.Forms.RichTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.uGridBase = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.udtMWDateE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.udtMWDateS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cboMWType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExcel1 = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.btnQuery1 = new System.Windows.Forms.Button();
            this.btnClose1 = new System.Windows.Forms.Button();
            this.picFrRightS11 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS11 = new System.Windows.Forms.PictureBox();
            this.picFrBottomS11 = new System.Windows.Forms.PictureBox();
            this.picFrTopS11 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.cFXData = new ChartFX.WinForms.Chart();
            this.uGridDetailPS = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkChart = new System.Windows.Forms.CheckBox();
            this.chkGrid = new System.Windows.Forms.CheckBox();
            this.btnQuery2 = new System.Windows.Forms.Button();
            this.btnExcel2 = new System.Windows.Forms.Button();
            this.udtMWDetailE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label6 = new System.Windows.Forms.Label();
            this.udtMWDetailS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnClose2 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.uGridChartImage = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.cFXData2 = new ChartFX.WinForms.Chart();
            this.uGridDetailPS2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cboMBlock2 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cboLBlock2 = new System.Windows.Forms.ComboBox();
            this.chkChart2 = new System.Windows.Forms.CheckBox();
            this.chkGrid2 = new System.Windows.Forms.CheckBox();
            this.btnQuery3 = new System.Windows.Forms.Button();
            this.btnExcel3 = new System.Windows.Forms.Button();
            this.udtMWDetailE2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label8 = new System.Windows.Forms.Label();
            this.udtMWDetailS2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnClose3 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.uGridChartImage2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridDetailPS3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cboMBlock3 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cboLBlock3 = new System.Windows.Forms.ComboBox();
            this.btnQuery4 = new System.Windows.Forms.Button();
            this.btnExcel4 = new System.Windows.Forms.Button();
            this.udtMWDetailE3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label24 = new System.Windows.Forms.Label();
            this.udtMWDetailS3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnClose4 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uTabMW = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.picFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.ultraGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtPROCDT)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChartImage)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXData2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailPS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChartImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailPS3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailE3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailS3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMW)).BeginInit();
            this.uTabMW.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.panel6);
            this.ultraTabPageControl1.Controls.Add(this.picFrRightS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrLeftS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrBottomS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrTopS11);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(745, 483);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pictureBox2);
            this.panel6.Controls.Add(this.panel2);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Controls.Add(this.pictureBox1);
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 9);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(737, 470);
            this.panel6.TabIndex = 123;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 234);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(737, 4);
            this.pictureBox2.TabIndex = 241;
            this.pictureBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.udtPROCDT);
            this.panel2.Controls.Add(this.rtxtMWContent);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.txtDEPT);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.txtUName);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.rtxtMWResult);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 234);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(737, 236);
            this.panel2.TabIndex = 240;
            // 
            // udtPROCDT
            // 
            this.udtPROCDT.DateTime = new System.DateTime(2014, 7, 17, 0, 0, 0, 0);
            this.udtPROCDT.Location = new System.Drawing.Point(153, 67);
            this.udtPROCDT.Name = "udtPROCDT";
            this.udtPROCDT.ReadOnly = true;
            this.udtPROCDT.Size = new System.Drawing.Size(100, 21);
            this.udtPROCDT.TabIndex = 244;
            this.udtPROCDT.Value = new System.DateTime(2014, 7, 17, 0, 0, 0, 0);
            // 
            // rtxtMWContent
            // 
            this.rtxtMWContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtxtMWContent.Location = new System.Drawing.Point(153, 9);
            this.rtxtMWContent.MaxLength = 500;
            this.rtxtMWContent.Name = "rtxtMWContent";
            this.rtxtMWContent.ReadOnly = true;
            this.rtxtMWContent.Size = new System.Drawing.Size(540, 52);
            this.rtxtMWContent.TabIndex = 243;
            this.rtxtMWContent.Tag = "CTS";
            this.rtxtMWContent.Text = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(86, 13);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(61, 12);
            this.label27.TabIndex = 242;
            this.label27.Text = "민원내용 :";
            // 
            // txtDEPT
            // 
            this.txtDEPT.Location = new System.Drawing.Point(484, 67);
            this.txtDEPT.Name = "txtDEPT";
            this.txtDEPT.ReadOnly = true;
            this.txtDEPT.Size = new System.Drawing.Size(209, 21);
            this.txtDEPT.TabIndex = 241;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(417, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 12);
            this.label19.TabIndex = 240;
            this.label19.Text = "처리부서 :";
            // 
            // txtUName
            // 
            this.txtUName.Location = new System.Drawing.Point(309, 67);
            this.txtUName.Name = "txtUName";
            this.txtUName.ReadOnly = true;
            this.txtUName.Size = new System.Drawing.Size(97, 21);
            this.txtUName.TabIndex = 239;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(254, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 12);
            this.label18.TabIndex = 238;
            this.label18.Text = "담당자 :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(85, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 12);
            this.label17.TabIndex = 237;
            this.label17.Text = "처리일자 :";
            // 
            // rtxtMWResult
            // 
            this.rtxtMWResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtxtMWResult.Location = new System.Drawing.Point(152, 94);
            this.rtxtMWResult.MaxLength = 1300;
            this.rtxtMWResult.Name = "rtxtMWResult";
            this.rtxtMWResult.ReadOnly = true;
            this.rtxtMWResult.Size = new System.Drawing.Size(541, 128);
            this.rtxtMWResult.TabIndex = 236;
            this.rtxtMWResult.Tag = "CTS";
            this.rtxtMWResult.Text = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(85, 97);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 12);
            this.label22.TabIndex = 235;
            this.label22.Text = "처리결과 :";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.uGridBase);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 69);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(737, 165);
            this.panel3.TabIndex = 238;
            // 
            // uGridBase
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridBase.DisplayLayout.Appearance = appearance37;
            this.uGridBase.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridBase.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridBase.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridBase.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.uGridBase.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridBase.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.uGridBase.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridBase.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridBase.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridBase.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.uGridBase.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridBase.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.uGridBase.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridBase.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGridBase.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridBase.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridBase.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.uGridBase.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.uGridBase.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridBase.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.uGridBase.DisplayLayout.Override.RowAppearance = appearance47;
            this.uGridBase.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridBase.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.uGridBase.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridBase.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridBase.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridBase.Location = new System.Drawing.Point(0, 0);
            this.uGridBase.Name = "uGridBase";
            this.uGridBase.Size = new System.Drawing.Size(737, 165);
            this.uGridBase.TabIndex = 113;
            this.uGridBase.Text = "민원정보";
            this.uGridBase.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridBase_DoubleClickRow);
            this.uGridBase.Click += new System.EventHandler(this.uGridBase_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(737, 4);
            this.pictureBox1.TabIndex = 237;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cboSBlock);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cboMBlock);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cboLBlock);
            this.groupBox1.Controls.Add(this.udtMWDateE);
            this.groupBox1.Controls.Add(this.udtMWDateS);
            this.groupBox1.Controls.Add(this.cboMWType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnExcel1);
            this.groupBox1.Controls.Add(this.label47);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.btnQuery1);
            this.groupBox1.Controls.Add(this.btnClose1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(737, 65);
            this.groupBox1.TabIndex = 236;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F);
            this.label11.Location = new System.Drawing.Point(448, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 12);
            this.label11.TabIndex = 185;
            this.label11.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(503, 14);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 20);
            this.cboSBlock.TabIndex = 184;
            this.cboSBlock.SelectedIndexChanged += new System.EventHandler(this.cboSBlock_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F);
            this.label12.Location = new System.Drawing.Point(233, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 12);
            this.label12.TabIndex = 183;
            this.label12.Text = "중블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(288, 14);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 20);
            this.cboMBlock.TabIndex = 182;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F);
            this.label13.Location = new System.Drawing.Point(18, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 12);
            this.label13.TabIndex = 181;
            this.label13.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(72, 14);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 20);
            this.cboLBlock.TabIndex = 180;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // udtMWDateE
            // 
            this.udtMWDateE.DateTime = new System.DateTime(2014, 7, 17, 0, 0, 0, 0);
            this.udtMWDateE.Location = new System.Drawing.Point(402, 39);
            this.udtMWDateE.Name = "udtMWDateE";
            this.udtMWDateE.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateE.TabIndex = 179;
            this.udtMWDateE.Value = new System.DateTime(2014, 7, 17, 0, 0, 0, 0);
            // 
            // udtMWDateS
            // 
            this.udtMWDateS.Location = new System.Drawing.Point(288, 39);
            this.udtMWDateS.Name = "udtMWDateS";
            this.udtMWDateS.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateS.TabIndex = 178;
            // 
            // cboMWType
            // 
            this.cboMWType.FormattingEnabled = true;
            this.cboMWType.Location = new System.Drawing.Point(72, 40);
            this.cboMWType.Name = "cboMWType";
            this.cboMWType.Size = new System.Drawing.Size(140, 20);
            this.cboMWType.TabIndex = 177;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.Location = new System.Drawing.Point(6, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 12);
            this.label2.TabIndex = 176;
            this.label2.Text = "민원분류 :";
            // 
            // btnExcel1
            // 
            this.btnExcel1.Location = new System.Drawing.Point(563, 40);
            this.btnExcel1.Name = "btnExcel1";
            this.btnExcel1.Size = new System.Drawing.Size(40, 22);
            this.btnExcel1.TabIndex = 175;
            this.btnExcel1.Text = "엑셀";
            this.btnExcel1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel1.UseVisualStyleBackColor = true;
            this.btnExcel1.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9F);
            this.label47.Location = new System.Drawing.Point(389, 45);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(14, 12);
            this.label47.TabIndex = 168;
            this.label47.Text = "~";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9F);
            this.label44.Location = new System.Drawing.Point(221, 43);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 12);
            this.label44.TabIndex = 166;
            this.label44.Text = "접수일자 :";
            // 
            // btnQuery1
            // 
            this.btnQuery1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery1.Location = new System.Drawing.Point(517, 40);
            this.btnQuery1.Name = "btnQuery1";
            this.btnQuery1.Size = new System.Drawing.Size(40, 22);
            this.btnQuery1.TabIndex = 164;
            this.btnQuery1.Text = "조회";
            this.btnQuery1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery1.UseVisualStyleBackColor = true;
            this.btnQuery1.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnClose1
            // 
            this.btnClose1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose1.Location = new System.Drawing.Point(609, 40);
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.Size = new System.Drawing.Size(40, 22);
            this.btnClose1.TabIndex = 163;
            this.btnClose1.Text = "닫기";
            this.btnClose1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose1.UseVisualStyleBackColor = true;
            this.btnClose1.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // picFrRightS11
            // 
            this.picFrRightS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightS11.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightS11.Location = new System.Drawing.Point(741, 9);
            this.picFrRightS11.Name = "picFrRightS11";
            this.picFrRightS11.Size = new System.Drawing.Size(4, 470);
            this.picFrRightS11.TabIndex = 109;
            this.picFrRightS11.TabStop = false;
            // 
            // picFrLeftS11
            // 
            this.picFrLeftS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS11.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS11.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftS11.Name = "picFrLeftS11";
            this.picFrLeftS11.Size = new System.Drawing.Size(4, 470);
            this.picFrLeftS11.TabIndex = 108;
            this.picFrLeftS11.TabStop = false;
            // 
            // picFrBottomS11
            // 
            this.picFrBottomS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS11.Location = new System.Drawing.Point(0, 479);
            this.picFrBottomS11.Name = "picFrBottomS11";
            this.picFrBottomS11.Size = new System.Drawing.Size(745, 4);
            this.picFrBottomS11.TabIndex = 107;
            this.picFrBottomS11.TabStop = false;
            // 
            // picFrTopS11
            // 
            this.picFrTopS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS11.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS11.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS11.Name = "picFrTopS11";
            this.picFrTopS11.Size = new System.Drawing.Size(745, 9);
            this.picFrTopS11.TabIndex = 106;
            this.picFrTopS11.TabStop = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.cFXData);
            this.ultraTabPageControl3.Controls.Add(this.uGridDetailPS);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox16);
            this.ultraTabPageControl3.Controls.Add(this.groupBox2);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox7);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox8);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox9);
            this.ultraTabPageControl3.Controls.Add(this.uGridChartImage);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(750, 483);
            // 
            // cFXData
            // 
            this.cFXData.AllSeries.Gallery = ChartFX.WinForms.Gallery.Bar;
            this.cFXData.AxisX.Staggered = true;
            this.cFXData.AxisY.Title.Text = "민원등록건수";
            this.cFXData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cFXData.Location = new System.Drawing.Point(4, 216);
            this.cFXData.Name = "cFXData";
            this.cFXData.Personalize.AutoLoad = true;
            this.cFXData.RandomData.Series = 1;
            this.cFXData.Size = new System.Drawing.Size(742, 263);
            this.cFXData.TabIndex = 122;
            // 
            // uGridDetailPS
            // 
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetailPS.DisplayLayout.Appearance = appearance73;
            this.uGridDetailPS.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetailPS.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance74.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS.DisplayLayout.GroupByBox.Appearance = appearance74;
            appearance75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailPS.DisplayLayout.GroupByBox.BandLabelAppearance = appearance75;
            this.uGridDetailPS.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance76.BackColor2 = System.Drawing.SystemColors.Control;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailPS.DisplayLayout.GroupByBox.PromptAppearance = appearance76;
            this.uGridDetailPS.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetailPS.DisplayLayout.MaxRowScrollRegions = 1;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetailPS.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            appearance78.BackColor = System.Drawing.SystemColors.Highlight;
            appearance78.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetailPS.DisplayLayout.Override.ActiveRowAppearance = appearance78;
            this.uGridDetailPS.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetailPS.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS.DisplayLayout.Override.CardAreaAppearance = appearance79;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            appearance80.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetailPS.DisplayLayout.Override.CellAppearance = appearance80;
            this.uGridDetailPS.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetailPS.DisplayLayout.Override.CellPadding = 0;
            appearance81.BackColor = System.Drawing.SystemColors.Control;
            appearance81.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance81.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance81.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS.DisplayLayout.Override.GroupByRowAppearance = appearance81;
            appearance82.TextHAlignAsString = "Left";
            this.uGridDetailPS.DisplayLayout.Override.HeaderAppearance = appearance82;
            this.uGridDetailPS.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetailPS.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetailPS.DisplayLayout.Override.RowAppearance = appearance83;
            this.uGridDetailPS.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetailPS.DisplayLayout.Override.TemplateAddRowAppearance = appearance84;
            this.uGridDetailPS.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetailPS.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetailPS.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetailPS.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridDetailPS.Location = new System.Drawing.Point(4, 51);
            this.uGridDetailPS.Name = "uGridDetailPS";
            this.uGridDetailPS.Size = new System.Drawing.Size(742, 165);
            this.uGridDetailPS.TabIndex = 121;
            this.uGridDetailPS.Text = "ultraGrid1";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox16.Location = new System.Drawing.Point(4, 47);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(742, 4);
            this.pictureBox16.TabIndex = 120;
            this.pictureBox16.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkChart);
            this.groupBox2.Controls.Add(this.chkGrid);
            this.groupBox2.Controls.Add(this.btnQuery2);
            this.groupBox2.Controls.Add(this.btnExcel2);
            this.groupBox2.Controls.Add(this.udtMWDetailE);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.udtMWDetailS);
            this.groupBox2.Controls.Add(this.btnClose2);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(4, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(742, 38);
            this.groupBox2.TabIndex = 119;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "검색조건";
            // 
            // chkChart
            // 
            this.chkChart.AutoSize = true;
            this.chkChart.Font = new System.Drawing.Font("굴림", 9F);
            this.chkChart.Location = new System.Drawing.Point(468, 15);
            this.chkChart.Name = "chkChart";
            this.chkChart.Size = new System.Drawing.Size(48, 16);
            this.chkChart.TabIndex = 90;
            this.chkChart.Text = "챠트";
            this.chkChart.UseVisualStyleBackColor = true;
            this.chkChart.CheckedChanged += new System.EventHandler(this.chkChart_CheckedChanged);
            // 
            // chkGrid
            // 
            this.chkGrid.AutoSize = true;
            this.chkGrid.Font = new System.Drawing.Font("굴림", 9F);
            this.chkGrid.Location = new System.Drawing.Point(400, 15);
            this.chkGrid.Name = "chkGrid";
            this.chkGrid.Size = new System.Drawing.Size(60, 16);
            this.chkGrid.TabIndex = 89;
            this.chkGrid.Text = "그리드";
            this.chkGrid.UseVisualStyleBackColor = true;
            this.chkGrid.CheckedChanged += new System.EventHandler(this.chkGrid_CheckedChanged);
            // 
            // btnQuery2
            // 
            this.btnQuery2.Location = new System.Drawing.Point(599, 12);
            this.btnQuery2.Name = "btnQuery2";
            this.btnQuery2.Size = new System.Drawing.Size(40, 22);
            this.btnQuery2.TabIndex = 82;
            this.btnQuery2.Text = "조회";
            this.btnQuery2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery2.UseVisualStyleBackColor = true;
            this.btnQuery2.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnExcel2
            // 
            this.btnExcel2.Location = new System.Drawing.Point(645, 12);
            this.btnExcel2.Name = "btnExcel2";
            this.btnExcel2.Size = new System.Drawing.Size(40, 22);
            this.btnExcel2.TabIndex = 87;
            this.btnExcel2.Text = "엑셀";
            this.btnExcel2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel2.UseVisualStyleBackColor = true;
            this.btnExcel2.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // udtMWDetailE
            // 
            this.udtMWDetailE.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.udtMWDetailE.Location = new System.Drawing.Point(186, 12);
            this.udtMWDetailE.Name = "udtMWDetailE";
            this.udtMWDetailE.Size = new System.Drawing.Size(100, 21);
            this.udtMWDetailE.TabIndex = 85;
            this.udtMWDetailE.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F);
            this.label6.Location = new System.Drawing.Point(6, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 12);
            this.label6.TabIndex = 84;
            this.label6.Text = "접수일자 :";
            // 
            // udtMWDetailS
            // 
            this.udtMWDetailS.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.udtMWDetailS.FormatString = "";
            this.udtMWDetailS.Location = new System.Drawing.Point(73, 12);
            this.udtMWDetailS.MaskInput = "{LOC}yyyy/mm";
            this.udtMWDetailS.Name = "udtMWDetailS";
            this.udtMWDetailS.Size = new System.Drawing.Size(100, 21);
            this.udtMWDetailS.TabIndex = 83;
            this.udtMWDetailS.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // btnClose2
            // 
            this.btnClose2.Location = new System.Drawing.Point(691, 12);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.Size = new System.Drawing.Size(40, 22);
            this.btnClose2.TabIndex = 81;
            this.btnClose2.Text = "닫기";
            this.btnClose2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose2.UseVisualStyleBackColor = true;
            this.btnClose2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F);
            this.label10.Location = new System.Drawing.Point(285, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 12);
            this.label10.TabIndex = 88;
            this.label10.Text = "(월 단위 조회)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(173, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 12);
            this.label1.TabIndex = 86;
            this.label1.Text = "~";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox6.Location = new System.Drawing.Point(746, 9);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(4, 470);
            this.pictureBox6.TabIndex = 113;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(0, 9);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(4, 470);
            this.pictureBox7.TabIndex = 112;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox8.Location = new System.Drawing.Point(0, 479);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(750, 4);
            this.pictureBox8.TabIndex = 111;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(0, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(750, 9);
            this.pictureBox9.TabIndex = 110;
            this.pictureBox9.TabStop = false;
            // 
            // uGridChartImage
            // 
            this.uGridChartImage.Location = new System.Drawing.Point(7, 207);
            this.uGridChartImage.Name = "uGridChartImage";
            this.uGridChartImage.Size = new System.Drawing.Size(507, 105);
            this.uGridChartImage.TabIndex = 118;
            this.uGridChartImage.Text = "ultraGrid1";
            this.uGridChartImage.Visible = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.cFXData2);
            this.ultraTabPageControl2.Controls.Add(this.uGridDetailPS2);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox18);
            this.ultraTabPageControl2.Controls.Add(this.groupBox3);
            this.ultraTabPageControl2.Controls.Add(this.uGridChartImage2);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox3);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox4);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox5);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox10);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(748, 483);
            // 
            // cFXData2
            // 
            this.cFXData2.AllSeries.Gallery = ChartFX.WinForms.Gallery.Bar;
            this.cFXData2.AxisX.Staggered = true;
            this.cFXData2.AxisY.Title.Text = "민원등록건수";
            this.cFXData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cFXData2.Location = new System.Drawing.Point(4, 220);
            this.cFXData2.Name = "cFXData2";
            this.cFXData2.Personalize.AutoLoad = true;
            this.cFXData2.RandomData.Series = 1;
            this.cFXData2.Size = new System.Drawing.Size(740, 259);
            this.cFXData2.TabIndex = 127;
            // 
            // uGridDetailPS2
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetailPS2.DisplayLayout.Appearance = appearance13;
            this.uGridDetailPS2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetailPS2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS2.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailPS2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridDetailPS2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailPS2.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridDetailPS2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetailPS2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetailPS2.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetailPS2.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGridDetailPS2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetailPS2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS2.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetailPS2.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridDetailPS2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetailPS2.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS2.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGridDetailPS2.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGridDetailPS2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetailPS2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetailPS2.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridDetailPS2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetailPS2.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridDetailPS2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetailPS2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetailPS2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetailPS2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridDetailPS2.Location = new System.Drawing.Point(4, 78);
            this.uGridDetailPS2.Name = "uGridDetailPS2";
            this.uGridDetailPS2.Size = new System.Drawing.Size(740, 142);
            this.uGridDetailPS2.TabIndex = 126;
            this.uGridDetailPS2.Text = "ultraGrid1";
            // 
            // pictureBox18
            // 
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox18.Location = new System.Drawing.Point(4, 74);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(740, 4);
            this.pictureBox18.TabIndex = 125;
            this.pictureBox18.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.cboMBlock2);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.cboLBlock2);
            this.groupBox3.Controls.Add(this.chkChart2);
            this.groupBox3.Controls.Add(this.chkGrid2);
            this.groupBox3.Controls.Add(this.btnQuery3);
            this.groupBox3.Controls.Add(this.btnExcel3);
            this.groupBox3.Controls.Add(this.udtMWDetailE2);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.udtMWDetailS2);
            this.groupBox3.Controls.Add(this.btnClose3);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(4, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(740, 65);
            this.groupBox3.TabIndex = 124;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "검색조건";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F);
            this.label15.Location = new System.Drawing.Point(233, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 12);
            this.label15.TabIndex = 172;
            this.label15.Text = "중블록 :";
            // 
            // cboMBlock2
            // 
            this.cboMBlock2.FormattingEnabled = true;
            this.cboMBlock2.Location = new System.Drawing.Point(288, 14);
            this.cboMBlock2.Name = "cboMBlock2";
            this.cboMBlock2.Size = new System.Drawing.Size(140, 20);
            this.cboMBlock2.TabIndex = 171;
            this.cboMBlock2.SelectedIndexChanged += new System.EventHandler(this.cboMBlock2_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F);
            this.label16.Location = new System.Drawing.Point(18, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 12);
            this.label16.TabIndex = 170;
            this.label16.Text = "대블록 :";
            // 
            // cboLBlock2
            // 
            this.cboLBlock2.FormattingEnabled = true;
            this.cboLBlock2.Location = new System.Drawing.Point(72, 14);
            this.cboLBlock2.Name = "cboLBlock2";
            this.cboLBlock2.Size = new System.Drawing.Size(140, 20);
            this.cboLBlock2.TabIndex = 169;
            this.cboLBlock2.SelectedIndexChanged += new System.EventHandler(this.cboLBlock2_SelectedIndexChanged);
            // 
            // chkChart2
            // 
            this.chkChart2.AutoSize = true;
            this.chkChart2.Font = new System.Drawing.Font("굴림", 9F);
            this.chkChart2.Location = new System.Drawing.Point(542, 42);
            this.chkChart2.Name = "chkChart2";
            this.chkChart2.Size = new System.Drawing.Size(48, 16);
            this.chkChart2.TabIndex = 168;
            this.chkChart2.Text = "챠트";
            this.chkChart2.UseVisualStyleBackColor = true;
            // 
            // chkGrid2
            // 
            this.chkGrid2.AutoSize = true;
            this.chkGrid2.Font = new System.Drawing.Font("굴림", 9F);
            this.chkGrid2.Location = new System.Drawing.Point(472, 43);
            this.chkGrid2.Name = "chkGrid2";
            this.chkGrid2.Size = new System.Drawing.Size(60, 16);
            this.chkGrid2.TabIndex = 167;
            this.chkGrid2.Text = "그리드";
            this.chkGrid2.UseVisualStyleBackColor = true;
            this.chkGrid2.CheckedChanged += new System.EventHandler(this.chkGrid_CheckedChanged);
            // 
            // btnQuery3
            // 
            this.btnQuery3.Location = new System.Drawing.Point(599, 38);
            this.btnQuery3.Name = "btnQuery3";
            this.btnQuery3.Size = new System.Drawing.Size(40, 22);
            this.btnQuery3.TabIndex = 160;
            this.btnQuery3.Text = "조회";
            this.btnQuery3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery3.UseVisualStyleBackColor = true;
            this.btnQuery3.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnExcel3
            // 
            this.btnExcel3.Location = new System.Drawing.Point(645, 38);
            this.btnExcel3.Name = "btnExcel3";
            this.btnExcel3.Size = new System.Drawing.Size(40, 22);
            this.btnExcel3.TabIndex = 165;
            this.btnExcel3.Text = "엑셀";
            this.btnExcel3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel3.UseVisualStyleBackColor = true;
            this.btnExcel3.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // udtMWDetailE2
            // 
            this.udtMWDetailE2.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.udtMWDetailE2.Location = new System.Drawing.Point(185, 40);
            this.udtMWDetailE2.Name = "udtMWDetailE2";
            this.udtMWDetailE2.Size = new System.Drawing.Size(100, 21);
            this.udtMWDetailE2.TabIndex = 163;
            this.udtMWDetailE2.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F);
            this.label8.Location = new System.Drawing.Point(6, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 12);
            this.label8.TabIndex = 162;
            this.label8.Text = "접수일자 :";
            // 
            // udtMWDetailS2
            // 
            this.udtMWDetailS2.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.udtMWDetailS2.FormatString = "";
            this.udtMWDetailS2.Location = new System.Drawing.Point(72, 40);
            this.udtMWDetailS2.MaskInput = "{LOC}yyyy/mm";
            this.udtMWDetailS2.Name = "udtMWDetailS2";
            this.udtMWDetailS2.Size = new System.Drawing.Size(100, 21);
            this.udtMWDetailS2.TabIndex = 161;
            this.udtMWDetailS2.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // btnClose3
            // 
            this.btnClose3.Location = new System.Drawing.Point(691, 38);
            this.btnClose3.Name = "btnClose3";
            this.btnClose3.Size = new System.Drawing.Size(40, 22);
            this.btnClose3.TabIndex = 159;
            this.btnClose3.Text = "닫기";
            this.btnClose3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose3.UseVisualStyleBackColor = true;
            this.btnClose3.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F);
            this.label9.Location = new System.Drawing.Point(285, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 12);
            this.label9.TabIndex = 166;
            this.label9.Text = "(월 단위 조회)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F);
            this.label7.Location = new System.Drawing.Point(172, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 12);
            this.label7.TabIndex = 164;
            this.label7.Text = "~";
            // 
            // uGridChartImage2
            // 
            this.uGridChartImage2.Location = new System.Drawing.Point(7, 228);
            this.uGridChartImage2.Name = "uGridChartImage2";
            this.uGridChartImage2.Size = new System.Drawing.Size(507, 105);
            this.uGridChartImage2.TabIndex = 123;
            this.uGridChartImage2.Text = "ultraGrid1";
            this.uGridChartImage2.Visible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(744, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(4, 470);
            this.pictureBox3.TabIndex = 117;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox4.Location = new System.Drawing.Point(0, 9);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(4, 470);
            this.pictureBox4.TabIndex = 116;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(0, 479);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(748, 4);
            this.pictureBox5.TabIndex = 115;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox10.Location = new System.Drawing.Point(0, 0);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(748, 9);
            this.pictureBox10.TabIndex = 114;
            this.pictureBox10.TabStop = false;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uGridDetailPS3);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox23);
            this.ultraTabPageControl4.Controls.Add(this.groupBox4);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox19);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox20);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox21);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox22);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(747, 483);
            // 
            // uGridDetailPS3
            // 
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetailPS3.DisplayLayout.Appearance = appearance61;
            this.uGridDetailPS3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetailPS3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS3.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailPS3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.uGridDetailPS3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailPS3.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.uGridDetailPS3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetailPS3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetailPS3.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetailPS3.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.uGridDetailPS3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetailPS3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS3.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetailPS3.DisplayLayout.Override.CellAppearance = appearance68;
            this.uGridDetailPS3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetailPS3.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailPS3.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.uGridDetailPS3.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.uGridDetailPS3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetailPS3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetailPS3.DisplayLayout.Override.RowAppearance = appearance71;
            this.uGridDetailPS3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetailPS3.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.uGridDetailPS3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetailPS3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetailPS3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetailPS3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDetailPS3.Location = new System.Drawing.Point(4, 78);
            this.uGridDetailPS3.Name = "uGridDetailPS3";
            this.uGridDetailPS3.Size = new System.Drawing.Size(739, 401);
            this.uGridDetailPS3.TabIndex = 127;
            this.uGridDetailPS3.Text = "ultraGrid1";
            // 
            // pictureBox23
            // 
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox23.Location = new System.Drawing.Point(4, 74);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(739, 4);
            this.pictureBox23.TabIndex = 126;
            this.pictureBox23.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.cboMBlock3);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.cboLBlock3);
            this.groupBox4.Controls.Add(this.btnQuery4);
            this.groupBox4.Controls.Add(this.btnExcel4);
            this.groupBox4.Controls.Add(this.udtMWDetailE3);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.udtMWDetailS3);
            this.groupBox4.Controls.Add(this.btnClose4);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(4, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(739, 65);
            this.groupBox4.TabIndex = 125;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "검색조건";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F);
            this.label14.Location = new System.Drawing.Point(233, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 12);
            this.label14.TabIndex = 170;
            this.label14.Text = "중블록 :";
            // 
            // cboMBlock3
            // 
            this.cboMBlock3.FormattingEnabled = true;
            this.cboMBlock3.Location = new System.Drawing.Point(288, 14);
            this.cboMBlock3.Name = "cboMBlock3";
            this.cboMBlock3.Size = new System.Drawing.Size(140, 20);
            this.cboMBlock3.TabIndex = 169;
            this.cboMBlock3.SelectedIndexChanged += new System.EventHandler(this.cboMBlock3_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F);
            this.label23.Location = new System.Drawing.Point(18, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 12);
            this.label23.TabIndex = 168;
            this.label23.Text = "대블록 :";
            // 
            // cboLBlock3
            // 
            this.cboLBlock3.FormattingEnabled = true;
            this.cboLBlock3.Location = new System.Drawing.Point(72, 14);
            this.cboLBlock3.Name = "cboLBlock3";
            this.cboLBlock3.Size = new System.Drawing.Size(140, 20);
            this.cboLBlock3.TabIndex = 167;
            this.cboLBlock3.SelectedIndexChanged += new System.EventHandler(this.cboLBlock3_SelectedIndexChanged);
            // 
            // btnQuery4
            // 
            this.btnQuery4.Location = new System.Drawing.Point(599, 38);
            this.btnQuery4.Name = "btnQuery4";
            this.btnQuery4.Size = new System.Drawing.Size(40, 22);
            this.btnQuery4.TabIndex = 160;
            this.btnQuery4.Text = "조회";
            this.btnQuery4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery4.UseVisualStyleBackColor = true;
            this.btnQuery4.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnExcel4
            // 
            this.btnExcel4.Location = new System.Drawing.Point(645, 38);
            this.btnExcel4.Name = "btnExcel4";
            this.btnExcel4.Size = new System.Drawing.Size(40, 22);
            this.btnExcel4.TabIndex = 165;
            this.btnExcel4.Text = "엑셀";
            this.btnExcel4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExcel4.UseVisualStyleBackColor = true;
            this.btnExcel4.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // udtMWDetailE3
            // 
            this.udtMWDetailE3.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.udtMWDetailE3.Location = new System.Drawing.Point(185, 40);
            this.udtMWDetailE3.Name = "udtMWDetailE3";
            this.udtMWDetailE3.Size = new System.Drawing.Size(100, 21);
            this.udtMWDetailE3.TabIndex = 163;
            this.udtMWDetailE3.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("굴림", 9F);
            this.label24.Location = new System.Drawing.Point(6, 43);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 12);
            this.label24.TabIndex = 162;
            this.label24.Text = "접수일자 :";
            // 
            // udtMWDetailS3
            // 
            this.udtMWDetailS3.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.udtMWDetailS3.FormatString = "";
            this.udtMWDetailS3.Location = new System.Drawing.Point(72, 40);
            this.udtMWDetailS3.MaskInput = "{LOC}yyyy/mm";
            this.udtMWDetailS3.Name = "udtMWDetailS3";
            this.udtMWDetailS3.Size = new System.Drawing.Size(100, 21);
            this.udtMWDetailS3.TabIndex = 161;
            this.udtMWDetailS3.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // btnClose4
            // 
            this.btnClose4.Location = new System.Drawing.Point(691, 38);
            this.btnClose4.Name = "btnClose4";
            this.btnClose4.Size = new System.Drawing.Size(40, 22);
            this.btnClose4.TabIndex = 159;
            this.btnClose4.Text = "닫기";
            this.btnClose4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose4.UseVisualStyleBackColor = true;
            this.btnClose4.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("굴림", 9F);
            this.label25.Location = new System.Drawing.Point(285, 43);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 12);
            this.label25.TabIndex = 166;
            this.label25.Text = "(월 단위 조회)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("굴림", 9F);
            this.label26.Location = new System.Drawing.Point(172, 46);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 12);
            this.label26.TabIndex = 164;
            this.label26.Text = "~";
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox19.Location = new System.Drawing.Point(743, 9);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(4, 470);
            this.pictureBox19.TabIndex = 121;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox20.Location = new System.Drawing.Point(0, 9);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(4, 470);
            this.pictureBox20.TabIndex = 120;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox21.Location = new System.Drawing.Point(0, 479);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(747, 4);
            this.pictureBox21.TabIndex = 119;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox22.Location = new System.Drawing.Point(0, 0);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(747, 9);
            this.pictureBox22.TabIndex = 118;
            this.pictureBox22.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.uTabMW);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(9, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(751, 509);
            this.panel1.TabIndex = 108;
            // 
            // uTabMW
            // 
            this.uTabMW.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabMW.Controls.Add(this.ultraTabPageControl1);
            this.uTabMW.Controls.Add(this.ultraTabPageControl3);
            this.uTabMW.Controls.Add(this.ultraTabPageControl2);
            this.uTabMW.Controls.Add(this.ultraTabPageControl4);
            this.uTabMW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabMW.Location = new System.Drawing.Point(0, 0);
            this.uTabMW.Name = "uTabMW";
            this.uTabMW.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabMW.Size = new System.Drawing.Size(751, 509);
            this.uTabMW.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "수질 민원 조회";
            ultraTab2.TabPage = this.ultraTabPageControl3;
            ultraTab2.Text = "수질 민원 현황(기간)";
            ultraTab3.TabPage = this.ultraTabPageControl2;
            ultraTab3.Text = "수질 민원 현황(블록)";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "블록별 수질 민원 건수";
            this.uTabMW.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.uTabMW.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.uTabMW_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(747, 483);
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 9);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(9, 509);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // picFrRight
            // 
            this.picFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRight.Location = new System.Drawing.Point(760, 9);
            this.picFrRight.Name = "picFrRight";
            this.picFrRight.Size = new System.Drawing.Size(9, 509);
            this.picFrRight.TabIndex = 107;
            this.picFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 518);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(769, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(769, 9);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // chart1
            // 
            this.chart1.AllSeries.Gallery = ChartFX.WinForms.Gallery.Bar;
            this.chart1.AxisY.Title.Text = "민원등록건수";
            this.chart1.Location = new System.Drawing.Point(4, 201);
            this.chart1.Name = "chart1";
            this.chart1.Personalize.AutoLoad = true;
            this.chart1.RandomData.Series = 1;
            this.chart1.Size = new System.Drawing.Size(705, 90);
            this.chart1.TabIndex = 117;
            // 
            // ultraGrid1
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance25;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance32;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance35;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGrid1.Location = new System.Drawing.Point(4, 36);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(868, 165);
            this.ultraGrid1.TabIndex = 115;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Gold;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox11.Location = new System.Drawing.Point(4, 32);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(868, 4);
            this.pictureBox11.TabIndex = 116;
            this.pictureBox11.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.checkBox1);
            this.panel4.Controls.Add(this.checkBox2);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.ultraDateTimeEditor1);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.ultraDateTimeEditor2);
            this.panel4.Controls.Add(this.button3);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(868, 28);
            this.panel4.TabIndex = 114;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.checkBox1.Location = new System.Drawing.Point(549, 5);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(50, 16);
            this.checkBox1.TabIndex = 80;
            this.checkBox1.Text = "챠트";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.checkBox2.Location = new System.Drawing.Point(480, 5);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(63, 16);
            this.checkBox2.TabIndex = 79;
            this.checkBox2.Text = "그리드";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Query;
            this.button1.Location = new System.Drawing.Point(656, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 26);
            this.button1.TabIndex = 5;
            this.button1.Text = "조회";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Excel;
            this.button2.Location = new System.Drawing.Point(726, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 26);
            this.button2.TabIndex = 75;
            this.button2.Text = "엑셀";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(218, 2);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 73;
            this.ultraDateTimeEditor1.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(204, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 12);
            this.label3.TabIndex = 74;
            this.label3.Text = "~";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(6, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 12);
            this.label4.TabIndex = 72;
            this.label4.Text = "상세등록기간 :";
            // 
            // ultraDateTimeEditor2
            // 
            this.ultraDateTimeEditor2.DateTime = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            this.ultraDateTimeEditor2.FormatString = "";
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(105, 2);
            this.ultraDateTimeEditor2.MaskInput = "{LOC}yyyy/mm";
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor2.TabIndex = 71;
            this.ultraDateTimeEditor2.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Right;
            this.button3.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Close2;
            this.button3.Location = new System.Drawing.Point(796, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(70, 26);
            this.button3.TabIndex = 3;
            this.button3.Text = "닫기";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(317, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 12);
            this.label5.TabIndex = 76;
            this.label5.Text = "(월 단위 조회)";
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox12.Location = new System.Drawing.Point(872, 4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(4, 346);
            this.pictureBox12.TabIndex = 113;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox13.Location = new System.Drawing.Point(0, 4);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(4, 346);
            this.pictureBox13.TabIndex = 112;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox14.Location = new System.Drawing.Point(0, 350);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(876, 4);
            this.pictureBox14.TabIndex = 111;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox15.Location = new System.Drawing.Point(0, 0);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(876, 4);
            this.pictureBox15.TabIndex = 110;
            this.pictureBox15.TabStop = false;
            // 
            // ultraGrid2
            // 
            this.ultraGrid2.Location = new System.Drawing.Point(7, 207);
            this.ultraGrid2.Name = "ultraGrid2";
            this.ultraGrid2.Size = new System.Drawing.Size(507, 105);
            this.ultraGrid2.TabIndex = 118;
            this.ultraGrid2.Text = "ultraGrid1";
            // 
            // frmWQPMWReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 522);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.picFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.MinimizeBox = false;
            this.Name = "frmWQPMWReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "수질 민원 종류별 정보 조회";
            this.Load += new System.EventHandler(this.frmWQPMWReport_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQPMWReport_FormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtPROCDT)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChartImage)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cFXData2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailPS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChartImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailPS3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailE3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDetailS3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabMW)).EndInit();
            this.uTabMW.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox picFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMW;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.PictureBox picFrRightS11;
        private System.Windows.Forms.PictureBox picFrLeftS11;
        private System.Windows.Forms.PictureBox picFrBottomS11;
        private System.Windows.Forms.PictureBox picFrTopS11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChartImage;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChartImage2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox10;
        private ChartFX.WinForms.Chart chart1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid2;
        private System.Windows.Forms.Panel panel6;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private ChartFX.WinForms.Chart cFXData;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetailPS;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkChart;
        private System.Windows.Forms.CheckBox chkGrid;
        private System.Windows.Forms.Button btnQuery2;
        private System.Windows.Forms.Button btnExcel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDetailE;
        private System.Windows.Forms.Label label6;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDetailS;
        private System.Windows.Forms.Button btnClose2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkGrid2;
        private System.Windows.Forms.Button btnQuery3;
        private System.Windows.Forms.Button btnExcel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDetailE2;
        private System.Windows.Forms.Label label8;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDetailS2;
        private System.Windows.Forms.Button btnClose3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private ChartFX.WinForms.Chart cFXData2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetailPS2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetailPS3;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnQuery4;
        private System.Windows.Forms.Button btnExcel4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDetailE3;
        private System.Windows.Forms.Label label24;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDetailS3;
        private System.Windows.Forms.Button btnClose4;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox chkChart2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox rtxtMWContent;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtDEPT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtUName;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridBase;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboMWType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExcel1;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnQuery1;
        private System.Windows.Forms.Button btnClose1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateE;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateS;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtPROCDT;
        private System.Windows.Forms.RichTextBox rtxtMWResult;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboMBlock2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboLBlock2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboMBlock3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cboLBlock3;

    }
}