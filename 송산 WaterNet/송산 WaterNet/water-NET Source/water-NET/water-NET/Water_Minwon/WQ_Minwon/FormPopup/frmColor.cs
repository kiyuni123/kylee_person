﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;
using WaterNet.WaterAOCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_Minwon.FormPopup
{
    public partial class frmColor : Form
    {
        public frmColor()
        {
            InitializeComponent();
        }

        private void frmColor_Load(object sender, EventArgs e)
        {
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateS, 0, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtMWDateS, -3);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateE, 0, 0);

            this.SetBlockColorMWSerious();
            this.SetMWMarkerInMWDate();
        }

        private void btnQuery1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.SetBlockColorMWSerious();
                this.SetMWMarkerInMWDate();

                if (WQ_AppStatic.IS_SHOW_FORM_COLOR_DATA == false)
                {
                    FormPopup.frmColorData oForm = new FormPopup.frmColorData();
                    oForm.START_DATE = this.udtMWDateS.DateTime;
                    oForm.END_DATE = this.udtMWDateE.DateTime;
                    oForm.TopLevel = false;
                    oForm.Parent = this.Parent;
                    oForm.StartPosition = FormStartPosition.CenterScreen;
                    oForm.BringToFront();
                    oForm.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        #region User Function

        /// <summary>
        /// 모든 소블록에 접수일자안에 블록별 민원이 발생한 Count를 구해서
        /// 그 정도에 따라 블록 색을 다르게 표시한다.
        /// </summary>
        /// <returns></returns>
        private void SetBlockColorMWSerious()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            string strSBLOCK = string.Empty;
            string strSDate = WQ_Function.StringToDateTime(this.udtMWDateS.DateTime);
            string strEDate = WQ_Function.StringToDateTime(this.udtMWDateE.DateTime);

            ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "소블록");
            //소블록 색을 셋팅하기 전에 소블록 레이어의 기본 랜더러로 원복한다.
            ArcManager.SetDefaultRenderer(WQ_AppStatic.IMAP.ActiveView.FocusMap, (IFeatureLayer)pLayer);

            //ILayer pLayer = WaterAOCore.ArcManager.GetMapLayer(this.axMap.ActiveView.FocusMap, "소블록");
            WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);


            DataSet pDS = new DataSet();

            //SYSDATE로 부터 90일 전까지의 민원 데이터
            //민원대분류 CALRGCD = '2000' 인 민원데이터가 수질민원 2는 누수
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    C.LOC_NAME AS SBLOCK, COUNT(A.CANO) AS MWVOL");
            oStringBuilder.AppendLine("FROM      WI_CAINFO A, WI_DMINFO B, CM_LOCATION C");
            oStringBuilder.AppendLine("WHERE     A.CALRGCD = '2000' ");
            oStringBuilder.AppendLine("          AND A.DMNO IS NOT NULL");
            oStringBuilder.AppendLine("          AND A.CALRGCD = '2000' AND SUBSTR(A.CAMIDCD, 0, 2) = '20' AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("          AND B.DMNO = A.DMNO AND C.FTR_CODE = 'BZ003' AND C.FTR_IDN = B.SFTRIDN");            
            oStringBuilder.AppendLine("          AND TO_DATE(TO_CHAR(A.CAAPPLDT, 'RRRRMMDD'), 'RRRR-MM-DD') BETWEEN TO_DATE('" + strSDate + "', 'RRRR-MM-DD') AND TO_DATE('" + strEDate + "', 'RRRR-MM-DD') ");
            oStringBuilder.AppendLine("GROUP BY  C.LOC_NAME");
            oStringBuilder.AppendLine("ORDER BY  COUNT(A.CANO)");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    if (Convert.ToInt32(oDRow["MWVOL"].ToString()) > 0)
                    {
                        strSBLOCK = oDRow["SBLOCK"].ToString().Trim();  //블록명

                        IColor pColor = null;

                        //블록별로 민원 발생 횟수별 색
                        if (Convert.ToInt32(oDRow["MWVOL"].ToString()) <= Convert.ToInt32(this.txtLV1.Text.Trim()))
                        {
                            pColor = WaterAOCore.ArcManager.GetColor(255, 250, 70); //노랑
                        }
                        else if (Convert.ToInt32(oDRow["MWVOL"].ToString()) <= Convert.ToInt32(this.txtLV2.Text.Trim()))
                        {
                            pColor = WaterAOCore.ArcManager.GetColor(255, 128, 0);  //주황
                        }
                        else if (Convert.ToInt32(oDRow["MWVOL"].ToString()) > Convert.ToInt32(this.txtLV3.Text.Trim()))
                        {
                            pColor = WaterAOCore.ArcManager.GetColor(255, 50, 50);  //빨강
                        }
 
                        IColor pOutColor = WaterAOCore.ArcManager.GetColor(100, 100, 200);
                        ISymbol pLineSymbol = null;

                        if (((IGeoFeatureLayer)pLayer).Renderer is IUniqueValueRenderer)
                        {
                            IUniqueValueRenderer pUniqueRenderer = ((IGeoFeatureLayer)pLayer).Renderer as IUniqueValueRenderer;

                            pLineSymbol = WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 3, pOutColor);
                            ISymbol oSymbol = ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, (ILineSymbol)pLineSymbol, pColor);

                            pUniqueRenderer.set_Symbol(strSBLOCK, oSymbol);

                            ILayerEffects layereffect = pLayer as ILayerEffects;
                            layereffect.Transparency = (short)60;
                        }
                    }
                }

                ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                Application.DoEvents();
            }

            pDS.Dispose();
        }

        /// <summary>
        /// 민원일자 기간에 속하는 민원의 Marker만 표시
        /// </summary>
        private void SetMWMarkerInMWDate()
        {
            string strCANOS = string.Empty;
            string strSDate = WQ_Function.StringToDateTime(this.udtMWDateS.DateTime);
            string strEDate = WQ_Function.StringToDateTime(this.udtMWDateE.DateTime);

            StringBuilder oStringBuilder = new StringBuilder();
            
            DataSet pDS = new DataSet();

            ILayer pMinwonLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "민원지점");

            ArcManager.DeleteAllFeatures(pMinwonLayer);
            
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.CANO, A.MAP_X, A.MAP_Y, B.DMNO, B.CAMIDCD");
            oStringBuilder.AppendLine("FROM      WQ_MINWON A, WI_CAINFO B");
            oStringBuilder.AppendLine("WHERE     TO_DATE(A.MW_DATE, 'RRRR-MM-DD') BETWEEN TO_DATE('" + strSDate + "', 'RRRR-MM-DD') AND TO_DATE('" + strEDate + "', 'RRRR-MM-DD') ");
            oStringBuilder.AppendLine("          AND B.CANO = A.CANO");
            oStringBuilder.AppendLine("ORDER BY  A.CANO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_MINWON");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {

                IWorkspaceEdit pWorkspaceEdit = ArcManager.getWorkspaceEdit(pMinwonLayer as IFeatureLayer);
                if (pWorkspaceEdit == null) return;

                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureBuffer pFeatureBuffer = ((IFeatureLayer)pMinwonLayer).FeatureClass.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = ((IFeatureLayer)pMinwonLayer).FeatureClass.Insert(true);
                    IFeature pFeaturebuffer = pFeatureBuffer as IFeature;

                    comReleaser.ManageLifetime(pFeatureCursor);

                    IPoint pPoint = new PointClass();

                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        //레이어의 지점 좌표
                        pPoint.PutCoords(Convert.ToDouble(oDRow["MAP_X"].ToString()), Convert.ToDouble(oDRow["MAP_Y"].ToString()));

                        //레이어의 지점 데이터
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("CANO"), (object)oDRow["CANO"].ToString());
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("MW_GBN"), (object)"수질민원");
                        pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("CLS_CD"), (object)oDRow["CAMIDCD"].ToString());

                        //레이어의 지점 좌표로 IGeometry
                        pFeaturebuffer.Shape = pPoint as IGeometry;

                        pFeatureCursor.InsertFeature(pFeatureBuffer);
                    }
                    pFeatureCursor.Flush();
                }

                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
            }

            ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();

            pDS.Dispose();
        }

        #endregion

    }
}
