﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;
using WaterNet.WaterAOCore;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WQ_Minwon.FormPopup
{
    /// <summary>
    /// 민원 지점을 등록, 삭제 한다.
    /// </summary>
    public partial class frmWQMWPointManage : Form
    {
        public frmWQMWPointManage()
        {
            InitializeComponent();

            this.InitializeGird();

            this.InitializeControl();
           
            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE = true;

            Load += new EventHandler(frmWQMWPointManage_Load);  //동진 수정_2012.6.08
        }
        //=======================================================================
        //
        //                    동진 수정_2012.6.07
        //                      권한박탈(조회만 가능)       
        //=======================================================================

        private void frmWQMWPointManage_Load(object sender, EventArgs e)
        {

            object o = EMFrame.statics.AppStatic.USER_MENU["수질민원관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }
        }

        //==========================================================================================

        private void frmWQMWPointManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (this.uTab.ActiveTab.Index)
                {
                    case 0: //지점등록조회
                        this.GetNotRegisteredWQMinwon();
                        break;
                    case 1: //지점삭제조회
                        this.GetRegisteredWQMinwon();
                        break;
                }

                this.chkSelectAllR.Checked = false;
                this.chkSelectAllD.Checked = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.SaveNotRegisteredWQMinwon();

            ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();

            this.GetNotRegisteredWQMinwon();

            this.chkSelectAllR.Checked = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.RemoveRegisteredWQMinwon();

            this.GetRegisteredWQMinwon();

            this.chkSelectAllD.Checked = false;
        }

        #endregion


        #region Control Events

        private void uGrid_Click(object sender, EventArgs e)
        {
            this.CheckActivedRowCell();
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            this.ViewMap_SelectedMinwon();
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            bool IsCheck = false;

            switch (this.uTab.ActiveTab.Index)
            {
                case 0: //지점등록조회
                    if (this.uGridMWNotReg.Rows.Count <= 0) return;
                    IsCheck = this.chkSelectAllR.Checked;

                    this.CheckAllRowCell(this.uGridMWNotReg, IsCheck);
                    break;
                case 1: //지점삭제조회
                    if (this.uGridMWReg.Rows.Count <= 0) return;
                    IsCheck = this.chkSelectAllD.Checked;

                    this.CheckAllRowCell(this.uGridMWReg, IsCheck);
                    break;
            }
        }

        #endregion


        #region User Function

        #region - Initialize Function
        /// <summary>
        /// Grid를 초기화 한다. Column 설정 등...
        /// </summary>
        private void InitializeGird()
        {
            UltraGridColumn oUltraGridColumn;

            #region - 미등록된 수질민원

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "V";
            oUltraGridColumn.Header.Caption = "v";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 12;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANO";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAAPPLDT";
            oUltraGridColumn.Header.Caption = "접수일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANM";
            oUltraGridColumn.Header.Caption = "민원인 성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACELL";
            oUltraGridColumn.Header.Caption = "연락처";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CALRGCD";
            oUltraGridColumn.Header.Caption = "민원구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAMIDCD";
            oUltraGridColumn.Header.Caption = "민원분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACONT";
            oUltraGridColumn.Header.Caption = "민원내용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SFTRIDN";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridMWNotReg);

            #endregion

            #region - 등록된 수질민원

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "V";
            oUltraGridColumn.Header.Caption = "v";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 12;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANO";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAAPPLDT";
            oUltraGridColumn.Header.Caption = "접수일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANM";
            oUltraGridColumn.Header.Caption = "민원인 성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACELL";
            oUltraGridColumn.Header.Caption = "연락처";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CALRGCD";
            oUltraGridColumn.Header.Caption = "민원구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAMIDCD";
            oUltraGridColumn.Header.Caption = "민원분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACONT";
            oUltraGridColumn.Header.Caption = "민원내용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SFTRIDN";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridMWReg);

            #endregion

            #region - Grid Caption Visible

            this.uGridMWNotReg.DisplayLayout.CaptionVisible = DefaultableBoolean.True;
            this.uGridMWReg.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            #endregion
        }

        /// <summary>
        /// 본 기능에 사용되는 Control을 초기화 한다. Data Setting 등..
        /// </summary>
        private void InitializeControl()
        {
            //민원분류
            WQ_Function.SetCombo_MinwonType(this.cboMWTypeR);
            WQ_Function.SetCombo_MinwonType(this.cboMWTypeD);
            //접수일자
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateRS, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtMWDateRS, -90);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateRE, 0, 0);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateDS, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.udtMWDateDS, -90);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateDE, 0, 0);

            //Tab 초기화
            this.uTab.TabIndex = 0;

            //Check 초기화
            this.chkSelectAllR.Checked = false;
            this.chkSelectAllD.Checked = false;
        }

        #endregion

        #region - Base Function

        /// <summary>
        /// 민원등록
        /// </summary>
        private void SaveNotRegisteredWQMinwon()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            if (this.uGridMWNotReg.Rows.Count <= 0) return;

            string strLAYER_NAME = string.Empty;
            string strKEY_VALUE = string.Empty;
            string strDMNO = string.Empty;
            string strCLS_CD = string.Empty;
            string strMW_DATE = string.Empty;
            string strFTRIDN = string.Empty;
            string strChecked = string.Empty;
            string strNotAddMW = string.Empty;

            bool IsNoneDMNOOnLayer = false; //true 이면 수도계량기 레이어에 등록이 안된 DMNO가 있는 민원이 존재함.

            ILayer pLayer = WaterAOCore.ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "수도계량기");

            for (int i = 0; i < this.uGridMWNotReg.Rows.Count; i++)
            {
                strChecked = this.uGridMWNotReg.Rows[i].Cells[0].Text.ToUpper();

                if (strChecked == "TRUE")
                {
                    strKEY_VALUE = this.uGridMWNotReg.Rows[i].Cells[1].Text;
                    strDMNO = this.uGridMWNotReg.Rows[i].Cells[3].Text;
                    strCLS_CD = this.uGridMWNotReg.Rows[i].Cells[8].Text;
                    strCLS_CD = WQ_Function.SplitToCode(strCLS_CD);
                    strMW_DATE = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGridMWNotReg.Rows[i].Cells[2].Text));
                    strFTRIDN = this.uGridMWNotReg.Rows[i].Cells[10].Text;

                    IFeature pFeature = WaterAOCore.ArcManager.GetFeature(pLayer, "DMNO='" + strDMNO + "'");

                    if (pFeature != null)
                    {
                        IGeometry oIGeometry = pFeature.Shape;
                        this.DeleteMinwonDataOnDB(strKEY_VALUE);
                        this.SaveMinwonDataOnDB(strKEY_VALUE, strMW_DATE, strFTRIDN, oIGeometry);
                        this.SaveMinwonPointToLayer(oIGeometry, strKEY_VALUE, strCLS_CD);
                    }
                    else
                    {
                        strNotAddMW += "'" + strDMNO + "', ";
                        IsNoneDMNOOnLayer = true;
                    }
                }
            }

            if (IsNoneDMNOOnLayer == true)
            {
                strNotAddMW = strNotAddMW.Substring(0, strNotAddMW.Length - 2);
                MessageBox.Show("수용가번호가 GIS에 존재하지 않는 수질민원은 곧 표시되는 창에서 등록하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

                if (WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE2 != true)
                {
                    FormPopup.frmWQMWPointManage2 oForm = new FormPopup.frmWQMWPointManage2();
                    oForm.DMNOS = strNotAddMW;
                    oForm.START_DATE = WQ_Function.StringToDateTime(this.udtMWDateRS.DateTime);
                    oForm.END_DATE = WQ_Function.StringToDateTime(this.udtMWDateRE.DateTime);
                    oForm.TopLevel = false;
                    oForm.Parent = this.Parent;
                    oForm.StartPosition = FormStartPosition.CenterScreen;
                    oForm.BringToFront();
                    oForm.Show();
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            MessageBox.Show("선택하신 민원을 등록했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 민원삭제
        /// </summary>
        private void RemoveRegisteredWQMinwon()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            if (this.uGridMWReg.Rows.Count <= 0) return;
            
            string strLAYER_NAME = string.Empty;
            string strKEY_NAME = string.Empty;
            string strKEY_VALUE = string.Empty;
            string strChecked = string.Empty;

            for (int i = 0; i < this.uGridMWReg.Rows.Count; i++)
            {
                strChecked = this.uGridMWReg.Rows[i].Cells[0].Text.ToUpper();

                if (strChecked == "TRUE")
                {
                    strKEY_VALUE = this.uGridMWReg.Rows[i].Cells[1].Text;
                    strKEY_NAME = "CANO";
                    strLAYER_NAME = "민원지점";

                    this.DeleteMinwonDataOnDB(strKEY_VALUE);
                    this.RemovePointOnLayer(strLAYER_NAME, strKEY_NAME, strKEY_VALUE);
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;

            MessageBox.Show("선택하신 민원을 삭제했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE = false;
            this.Dispose();
            this.Close();
        }

        #endregion

        #region - Grid Function

        /// <summary>
        /// 그리드에서 선택된 Row의 0번 Cell을 Check or UnCheck
        /// </summary>
        private void CheckActivedRowCell()
        {
            UltraGrid oGrid = new UltraGrid();

            switch (this.uTab.ActiveTab.Index)
            {
                case 0: //지점등록조회 (수용가)
                    if (this.uGridMWNotReg.Rows.Count <= 0) return;

                    oGrid = this.uGridMWNotReg;
                    break;
                case 1: //지점삭제조회 (관리번호)
                    if (this.uGridMWReg.Rows.Count <= 0) return;

                    oGrid = this.uGridMWReg;
                    break;
            }
            if (oGrid.ActiveRow.Cells[0].Value.ToString().ToUpper() == false.ToString().ToUpper())
            {
                oGrid.ActiveRow.Cells[0].Value = true;
            }
            else
            {
                oGrid.ActiveRow.Cells[0].Value = false;
            }

            Application.DoEvents();
        }

        /// <summary>
        /// 그리드에서 전체 Row의 0번 Cell을 Check or UnCheck
        /// </summary>
        /// <param name="oGrid"></param>
        /// <param name="IsCheck">true : 전체 Check, false = 전체 UnCheck</param>
        private void CheckAllRowCell(UltraGrid oGrid, bool IsCheck)
        {
            for (int i = 0; i < oGrid.Rows.Count; i++)
            {
                oGrid.Rows[i].Cells[0].Value = IsCheck;
            }

            Application.DoEvents();
        }

        #endregion

        #region - SQL Function

        /// <summary>
        /// 지점이 미등록된 민원 Data를 Select 해서 Grid에 Set
        /// </summary>
        private void GetNotRegisteredWQMinwon()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strCAMIDCD = string.Empty;
            string strWHERE_PARAM = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strSDate = WQ_Function.StringToDateTime(this.udtMWDateRS.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtMWDateRE.DateTime);

            strCAMIDCD = WQ_Function.SplitToCode(this.cboMWTypeR.Text).Trim();

            if (strCAMIDCD == "")
            {
                strWHERE_PARAM = "";
            }
            else
            {
                strWHERE_PARAM = "AND A.CAMIDCD = '" + strCAMIDCD + "'";
            }
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   'false' AS V, A.CANO, TO_CHAR(CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, '' AS CACELL, A.CAADDR, D.CODE_NAME AS CALRGCD, E.CODE_NAME || ' (' || E.CODE || ')' AS CAMIDCD, replace(a.cacont,'개인정보 기입 금지' || chr(10),'') cacont, F.SFTRIDN");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE D ON D.PCODE = '9003' AND D.CODE = A.CALRGCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE E ON E.PCODE = '9004' AND E.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN WI_DMINFO F ON F.DMNO = A.DMNO");
            oStringBuilder.AppendLine("WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("         AND A.DMNO IS NOT NULL");
            oStringBuilder.AppendLine("         AND A.CALRGCD = '2000' AND SUBSTR(A.CAMIDCD, 0, 2) = '20' AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("         AND TO_CHAR(A.CAAPPLDT, 'RRRRMMDD') BETWEEN '" + strSDate + "' AND '" + strEDate + "' ");
            oStringBuilder.AppendLine("         AND A.CANO NOT IN (SELECT X.CANO FROM WQ_MINWON X WHERE SUBSTR(X.MW_DATE, 0, 8) = TO_CHAR(A.CAAPPLDT, 'RRRRMMDD')) " + strWHERE_PARAM);
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT DESC, A.CANO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridMWNotReg.DataSource = pDS.Tables["WI_CAINFO"].DefaultView;

            //AutoResizeColumes
           // FormManager.SetGridStyle_PerformAutoResize(this.uGridMWNotReg);

            if (this.uGridMWNotReg.Rows.Count > 0) this.uGridMWNotReg.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 지점이 등록된 민원 Data를 Select 해서 Grid에 Set
        /// </summary>
        private void GetRegisteredWQMinwon()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strCAMIDCD = string.Empty;
            string strWHERE_PARAM = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strSDate = WQ_Function.StringToDateTime(this.udtMWDateDS.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtMWDateDE.DateTime);

            strCAMIDCD = WQ_Function.SplitToCode(this.cboMWTypeR.Text).Trim();

            if (strCAMIDCD == "")
            {
                strWHERE_PARAM = "";
            }
            else
            {
                strWHERE_PARAM = "AND A.CAMIDCD = '" + strCAMIDCD + "'";
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   'false' AS V, A.CANO, TO_CHAR(CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, '' AS CACELL, A.CAADDR, D.CODE_NAME AS CALRGCD, E.CODE_NAME || ' (' || E.CODE || ')' AS CAMIDCD, replace(a.cacont,'개인정보 기입 금지' || chr(10),'') cacont, F.SFTRIDN");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE D ON D.PCODE = '9003' AND D.CODE = A.CALRGCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE E ON E.PCODE = '9004' AND E.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN WI_DMINFO F ON F.DMNO = A.DMNO");
            oStringBuilder.AppendLine("WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND A.CALRGCD = '2000' AND TO_CHAR(A.CAAPPLDT, 'RRRRMMDD') BETWEEN '" + strSDate + "' AND '" + strEDate + "' ");
            oStringBuilder.AppendLine("         AND A.CANO IN (SELECT X.CANO FROM WQ_MINWON X WHERE SUBSTR(X.MW_DATE, 0, 8) = TO_CHAR(A.CAAPPLDT, 'RRRRMMDD')) " + strWHERE_PARAM);
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT DESC, A.CANO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridMWReg.DataSource = pDS.Tables["WI_CAINFO"].DefaultView;

            //AutoResizeColumes
            //FormManager.SetGridStyle_PerformAutoResize(this.uGridMWReg);

            if (this.uGridMWReg.Rows.Count > 0) this.uGridMWReg.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// DB에서 민원 지점이 기등록된 Data를 Delete한다.
        /// </summary>
        private void DeleteMinwonDataOnDB(string strKEY_VALUE)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE");
            oStringBuilder.AppendLine("FROM     WQ_MINWON");
            oStringBuilder.AppendLine("WHERE    CANO = '" + strKEY_VALUE + "'");

            WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// DB에 민원 지점 정보를 등록한다.
        /// </summary>
        /// <param name="strCANO"></param>
        /// <param name="strMW_DATE"></param>
        /// <param name="strFTRIDN"></param>
        private void SaveMinwonDataOnDB(string strCANO, string strMW_DATE, string strFTRIDN, IGeometry oIGeometry)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            IPoint pPoint = new PointClass();
            pPoint = (IPoint)oIGeometry;
   
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO  WQ_MINWON");
            oStringBuilder.AppendLine("(CANO, MW_DATE, REGMNGR, REGMNGRIP, REGDT, SFTR_IDN, AUTO_YN, MAP_X, MAP_Y)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("('" + strCANO + "', '" + strMW_DATE + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', TO_CHAR(SYSDATE, 'RRRRMMDDHH24MISS'), '" + strFTRIDN + "', 'Y', '" + pPoint.X.ToString() + "', '" + pPoint.Y.ToString() + "')");

            WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        #endregion

        #region - MAP Function
   
        /// <summary>
        /// Grid에서 선택된 수용가 또는 관리번호로 MAP의 위치 이동 및 축척 확대
        /// </summary>
        private void ViewMap_SelectedMinwon()
        {
            string strKEY_VALUE = string.Empty;
            string strKEY_NAME = string.Empty;
            string strLAYER_NAME = string.Empty;
            double dblMAP_SCALE = 2000;

            UltraGrid oGrid = new UltraGrid();

            switch (this.uTab.ActiveTab.Index)
            {
                case 0: //지점등록조회 (수용가)
                    if (this.uGridMWNotReg.Rows.Count <= 0) return;

                    oGrid = this.uGridMWNotReg;
                    strLAYER_NAME = "수도계량기";
                    strKEY_NAME = "DMNO";
                    strKEY_VALUE = oGrid.ActiveRow.Cells[2].Text;                    
                    break;
                case 1: //지점삭제조회 (관리번호)
                    if (this.uGridMWReg.Rows.Count <= 0) return;

                    oGrid = this.uGridMWReg;
                    strLAYER_NAME = "민원지점";
                    strKEY_NAME = "CANO";
                    strKEY_VALUE = oGrid.ActiveRow.Cells[1].Text;
                    break;
            }

            if (WQ_AppStatic.IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strLAYER_NAME);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strKEY_NAME + "='" + strKEY_VALUE + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    IGeometry oIGeometry = pFeature.Shape;

                    ArcManager.FlashShape(WQ_AppStatic.IMAP.ActiveView, oIGeometry, 10);

                    if (ArcManager.GetMapScale(WQ_AppStatic.IMAP) > dblMAP_SCALE)
                    {
                        ArcManager.SetMapScale(WQ_AppStatic.IMAP, dblMAP_SCALE);
                    }

                    ArcManager.MoveCenterAt(WQ_AppStatic.IMAP, pFeature);

                    ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                    Application.DoEvents();
                }
                this.BringToFront();
            }
        }

        /// <summary>
        /// 민원 Layer에 Point를 저장한다.
        /// </summary>
        private void SaveMinwonPointToLayer(IGeometry oIGeometry, string strKEY_VALUE, string strCLS_CD)
        {
            string strLAYER_NAME = string.Empty;

            if (WQ_AppStatic.IMAP != null && oIGeometry != null)
            {
                strLAYER_NAME = "민원지점";

                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strLAYER_NAME);

                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeature pFeature = pFeatureClass.CreateFeature();

                //Shpae에 지점 등록
                pFeature.Shape = oIGeometry;

                //등록된 지점에 밸류 등록
                ArcManager.SetValue(pFeature, "MW_GBN", (object)"수질민원");
                ArcManager.SetValue(pFeature, "CANO", (object)strKEY_VALUE);
                ArcManager.SetValue(pFeature, "CLS_CD", (object)strCLS_CD);

                //Shape 저장
                pFeature.Store();
            }
        }

        /// <summary>
        /// Layer에서 Point를 Remove한다.
        /// </summary>
        private void RemovePointOnLayer(string strLAYER_NAME, string strKEY_NAME, string strKEY_VALUE)
        {
            if (WQ_AppStatic.IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strLAYER_NAME);
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strKEY_NAME + "='" + strKEY_VALUE + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (pFeature != null)
                {
                    pFeature.Delete();
                }

                this.BringToFront();
            }

            ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();
        }

        #endregion

        #endregion
    }
}
