﻿namespace WaterNet.WQ_Minwon.FormPopup
{
    partial class frmColorData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.picFrRightS11 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS11 = new System.Windows.Forms.PictureBox();
            this.picFrTopS11 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.udtMWDateE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.udtMWDateS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnQuery1 = new System.Windows.Forms.Button();
            this.btnClose1 = new System.Windows.Forms.Button();
            this.picFrBottomS11 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.uGridData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridData)).BeginInit();
            this.SuspendLayout();
            // 
            // picFrRightS11
            // 
            this.picFrRightS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightS11.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightS11.Location = new System.Drawing.Point(650, 4);
            this.picFrRightS11.Name = "picFrRightS11";
            this.picFrRightS11.Size = new System.Drawing.Size(4, 463);
            this.picFrRightS11.TabIndex = 114;
            this.picFrRightS11.TabStop = false;
            // 
            // picFrLeftS11
            // 
            this.picFrLeftS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS11.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS11.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftS11.Name = "picFrLeftS11";
            this.picFrLeftS11.Size = new System.Drawing.Size(4, 463);
            this.picFrLeftS11.TabIndex = 113;
            this.picFrLeftS11.TabStop = false;
            // 
            // picFrTopS11
            // 
            this.picFrTopS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS11.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS11.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS11.Name = "picFrTopS11";
            this.picFrTopS11.Size = new System.Drawing.Size(654, 4);
            this.picFrTopS11.TabIndex = 112;
            this.picFrTopS11.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.cboSBlock);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.cboMBlock);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.cboLBlock);
            this.panel2.Controls.Add(this.udtMWDateE);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.udtMWDateS);
            this.panel2.Controls.Add(this.btnQuery1);
            this.panel2.Controls.Add(this.btnClose1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(646, 54);
            this.panel2.TabIndex = 115;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(433, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 12);
            this.label11.TabIndex = 154;
            this.label11.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(493, 4);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 20);
            this.cboSBlock.TabIndex = 153;
            this.cboSBlock.SelectedIndexChanged += new System.EventHandler(this.cboSBlock_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(227, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 12);
            this.label12.TabIndex = 152;
            this.label12.Text = "중블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(287, 4);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 20);
            this.cboMBlock.TabIndex = 151;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(21, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 12);
            this.label13.TabIndex = 150;
            this.label13.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(81, 4);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 20);
            this.cboLBlock.TabIndex = 149;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // udtMWDateE
            // 
            this.udtMWDateE.Location = new System.Drawing.Point(195, 27);
            this.udtMWDateE.Name = "udtMWDateE";
            this.udtMWDateE.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateE.TabIndex = 65;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.Location = new System.Drawing.Point(181, 33);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(15, 12);
            this.label47.TabIndex = 66;
            this.label47.Text = "~";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.Location = new System.Drawing.Point(8, 32);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 12);
            this.label44.TabIndex = 64;
            this.label44.Text = "접수일자 :";
            // 
            // udtMWDateS
            // 
            this.udtMWDateS.Location = new System.Drawing.Point(81, 27);
            this.udtMWDateS.Name = "udtMWDateS";
            this.udtMWDateS.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateS.TabIndex = 63;
            // 
            // btnQuery1
            // 
            this.btnQuery1.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Query;
            this.btnQuery1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery1.Location = new System.Drawing.Point(494, 26);
            this.btnQuery1.Name = "btnQuery1";
            this.btnQuery1.Size = new System.Drawing.Size(70, 26);
            this.btnQuery1.TabIndex = 5;
            this.btnQuery1.Text = "조회";
            this.btnQuery1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery1.UseVisualStyleBackColor = true;
            this.btnQuery1.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnClose1
            // 
            this.btnClose1.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Close2;
            this.btnClose1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose1.Location = new System.Drawing.Point(563, 26);
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.Size = new System.Drawing.Size(70, 26);
            this.btnClose1.TabIndex = 3;
            this.btnClose1.Text = "닫기";
            this.btnClose1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose1.UseVisualStyleBackColor = true;
            this.btnClose1.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // picFrBottomS11
            // 
            this.picFrBottomS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS11.Location = new System.Drawing.Point(4, 463);
            this.picFrBottomS11.Name = "picFrBottomS11";
            this.picFrBottomS11.Size = new System.Drawing.Size(646, 4);
            this.picFrBottomS11.TabIndex = 116;
            this.picFrBottomS11.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(4, 58);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(646, 4);
            this.pictureBox2.TabIndex = 123;
            this.pictureBox2.TabStop = false;
            // 
            // uGridData
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridData.DisplayLayout.Appearance = appearance37;
            this.uGridData.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridData.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridData.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridData.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.uGridData.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridData.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.uGridData.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridData.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridData.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridData.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.uGridData.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridData.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.uGridData.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridData.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGridData.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridData.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridData.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.uGridData.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.uGridData.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridData.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.uGridData.DisplayLayout.Override.RowAppearance = appearance47;
            this.uGridData.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridData.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.uGridData.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridData.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridData.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridData.Location = new System.Drawing.Point(4, 62);
            this.uGridData.Name = "uGridData";
            this.uGridData.Size = new System.Drawing.Size(646, 401);
            this.uGridData.TabIndex = 124;
            this.uGridData.Text = "민원정보";
            this.uGridData.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridData_DoubleClickRow);
            // 
            // frmColorData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 467);
            this.Controls.Add(this.uGridData);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.picFrBottomS11);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.picFrRightS11);
            this.Controls.Add(this.picFrLeftS11);
            this.Controls.Add(this.picFrTopS11);
            this.MinimizeBox = false;
            this.Name = "frmColorData";
            this.Text = "기간별 수질 민원 접수 내역";
            this.Load += new System.EventHandler(this.frmColorData_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmColorData_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrRightS11;
        private System.Windows.Forms.PictureBox picFrLeftS11;
        private System.Windows.Forms.PictureBox picFrTopS11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboLBlock;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateE;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateS;
        private System.Windows.Forms.Button btnQuery1;
        private System.Windows.Forms.Button btnClose1;
        private System.Windows.Forms.PictureBox picFrBottomS11;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridData;

    }
}