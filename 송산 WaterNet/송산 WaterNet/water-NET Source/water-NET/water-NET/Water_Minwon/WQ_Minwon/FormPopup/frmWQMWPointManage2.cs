﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;
using WaterNet.WaterAOCore;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WQ_Minwon.FormPopup
{
    public partial class frmWQMWPointManage2 : Form
    {
        #region 프로퍼티

        private string _DMNOS;
        private string _SDATE;
        private string _EDATE;
        /// <summary>
        /// 레이어에 없는 수용가들 '수용가번호', '수용가번호', '수용가번호' 형식으로 받아 사용
        /// </summary>
        public string DMNOS
        {
            get
            {
                return _DMNOS;
            }
            set
            {
                _DMNOS = value;
            }
        }

        /// <summary>
        /// 민원조회 시작일
        /// </summary>
        public string START_DATE
        {
            get
            {
                return _SDATE;
            }
            set
            {
                _SDATE = value;
            }
        }

        /// <summary>
        /// 민원조회 종료일
        /// </summary>
        public string END_DATE
        {
            get
            {
                return _EDATE;
            }
            set
            {
                _EDATE = value;
            }
        }

        #endregion

        public frmWQMWPointManage2()
        {
            InitializeComponent();

            this.InitializeGird();
        }

        private void frmWQMWPointManage2_Load(object sender, EventArgs e)
        {

            //=======================================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=======================================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["수질민원관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
            }

            //==========================================================================================

            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE2 = true;

            this.GetMinwonDataToNotRegisteredInLayer();
        }

        private void frmWQMWPointManage2_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE2 = false;
        }


        #region Button Events

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.uGridMWNotReg.Rows.Count <= 0) return;

            if (WQ_AppStatic.CURRENT_IGEOMETRY == null)
            {
                MessageBox.Show("Map에서 수질민원지점을 선택하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.SaveNotRegisteredWQMinwon();

            ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();            

            MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.RemoveRegisteredDMNOFromDMNOS();

            this.GetMinwonDataToNotRegisteredInLayer();
        }

        private void btnCloseR_Click(object sender, EventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_POINT_MANAGE2 = false;
            this.Close();
        }

        #endregion


        #region User Function

        #region - Initialize Function
        /// <summary>
        /// Grid를 초기화 한다. Column 설정 등...
        /// </summary>
        private void InitializeGird()
        {
            UltraGridColumn oUltraGridColumn;

            #region - 미등록된 수질민원

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANO";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAAPPLDT";
            oUltraGridColumn.Header.Caption = "접수일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANM";
            oUltraGridColumn.Header.Caption = "민원인 성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACELL";
            oUltraGridColumn.Header.Caption = "연락처";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CALRGCD";
            oUltraGridColumn.Header.Caption = "민원구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAMIDCD";
            oUltraGridColumn.Header.Caption = "민원분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACONT";
            oUltraGridColumn.Header.Caption = "민원내용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWNotReg.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SFTRIDN";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridMWNotReg);

            #endregion

            #region - Grid Caption Visible

            this.uGridMWNotReg.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            #endregion
        }

        #endregion

        #region - SQL Function

        /// <summary>
        /// 수도계량기에 미등록된 수용가번호를 갖은 수질민원을 가져온다.
        /// </summary>
        private void GetMinwonDataToNotRegisteredInLayer()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            //string strWHERE_PARAM = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.CANO, TO_CHAR(CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, '' AS CACELL, A.CAADDR, D.CODE_NAME AS CALRGCD, E.CODE_NAME || ' (' || E.CODE || ')' AS CAMIDCD, replace(a.cacont,'개인정보 기입 금지' || chr(10),'') cacont, F.SFTRIDN");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE D ON D.PCODE = '9003' AND D.CODE = A.CALRGCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE E ON E.PCODE = '9004' AND E.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN WI_DMINFO F ON F.DMNO = A.DMNO");
            oStringBuilder.AppendLine("WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("         AND A.DMNO IN (" + _DMNOS + ")");
            oStringBuilder.AppendLine("         AND A.CALRGCD = '2000' AND SUBSTR(A.CAMIDCD, 0, 2) = '20' AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("         AND TO_CHAR(A.CAAPPLDT, 'RRRRMMDD') BETWEEN '" + _SDATE + "' AND '" + _EDATE + "' ");
            oStringBuilder.AppendLine("         AND A.CANO NOT IN (SELECT X.CANO FROM WQ_MINWON X WHERE SUBSTR(X.MW_DATE, 0, 8) = TO_CHAR(A.CAAPPLDT, 'RRRRMMDD'))");
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT DESC, A.CANO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridMWNotReg.DataSource = pDS.Tables["WI_CAINFO"].DefaultView;

            //AutoResizeColumes
            // FormManager.SetGridStyle_PerformAutoResize(this.uGridMWNotReg);

            if (this.uGridMWNotReg.Rows.Count > 0) this.uGridMWNotReg.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 민원등록
        /// </summary>
        private void SaveNotRegisteredWQMinwon()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            if (this.uGridMWNotReg.Rows.Count <= 0) return;

            string strLAYER_NAME = string.Empty;
            string strKEY_VALUE = string.Empty;
            string strDMNO = string.Empty;
            string strCLS_CD = string.Empty;
            string strMW_DATE = string.Empty;
            string strFTRIDN = string.Empty;

            strKEY_VALUE = this.uGridMWNotReg.ActiveRow.Cells[0].Text;
            strDMNO = this.uGridMWNotReg.ActiveRow.Cells[2].Text;
            strCLS_CD = this.uGridMWNotReg.ActiveRow.Cells[7].Text;
            strCLS_CD = WQ_Function.SplitToCode(strCLS_CD);
            strMW_DATE = WQ_Function.StringToDateTime(Convert.ToDateTime(this.uGridMWNotReg.ActiveRow.Cells[1].Text));
            strFTRIDN = this.uGridMWNotReg.ActiveRow.Cells[9].Text;

            this.DeleteMinwonDataOnDB(strKEY_VALUE);
            this.SaveMinwonDataOnDB(strKEY_VALUE, strMW_DATE, strFTRIDN, WQ_AppStatic.CURRENT_IGEOMETRY);
            this.SaveMinwonPointToLayer(WQ_AppStatic.CURRENT_IGEOMETRY, strKEY_VALUE, strCLS_CD);

            this.Cursor = System.Windows.Forms.Cursors.Default;

            MessageBox.Show("선택하신 민원을 등록했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// DB에서 민원 지점이 기등록된 Data를 Delete한다.
        /// </summary>
        private void DeleteMinwonDataOnDB(string strKEY_VALUE)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE");
            oStringBuilder.AppendLine("FROM     WQ_MINWON");
            oStringBuilder.AppendLine("WHERE    CANO = '" + strKEY_VALUE + "'");

            WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// DB에 민원 지점 정보를 등록한다.
        /// </summary>
        /// <param name="strCANO"></param>
        /// <param name="strMW_DATE"></param>
        /// <param name="strFTRIDN"></param>
        private void SaveMinwonDataOnDB(string strCANO, string strMW_DATE, string strFTRIDN, IGeometry oIGeometry)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            IPoint pPoint = new PointClass();
            pPoint = (IPoint)oIGeometry;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO  WQ_MINWON");
            oStringBuilder.AppendLine("(CANO, MW_DATE, REGMNGR, REGMNGRIP, REGDT, SFTR_IDN, AUTO_YN, MAP_X, MAP_Y)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("('" + strCANO + "', '" + strMW_DATE + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + EMFrame.statics.AppStatic.USER_IP + "', TO_CHAR(SYSDATE, 'RRRRMMDDHH24MISS'), '" + strFTRIDN + "', 'Y', '" + pPoint.X.ToString() + "', '" + pPoint.Y.ToString() + "')");

            WQ_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        #endregion

        #region - Map Function

        /// <summary>
        /// 민원 Layer에 Point를 저장한다.
        /// </summary>
        private void SaveMinwonPointToLayer(IGeometry oIGeometry, string strKEY_VALUE, string strCLS_CD)
        {
            string strLAYER_NAME = string.Empty;

            if (WQ_AppStatic.IMAP != null && oIGeometry != null)
            {
                strLAYER_NAME = "민원지점";

                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, strLAYER_NAME);

                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeature pFeature = pFeatureClass.CreateFeature();

                //Shpae에 지점 등록
                pFeature.Shape = oIGeometry;

                //등록된 지점에 밸류 등록
                ArcManager.SetValue(pFeature, "MW_GBN", (object)"수질민원");
                ArcManager.SetValue(pFeature, "CANO", (object)strKEY_VALUE);
                ArcManager.SetValue(pFeature, "CLS_CD", (object)strCLS_CD);

                //Shape 저장
                pFeature.Store();
            }
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// _DMNOS에서 등록한 수질민원의 DMNO를 삭제
        /// </summary>
        private void RemoveRegisteredDMNOFromDMNOS()
        {
            string strDMNO = string.Empty;
            
            string[] strArr = _DMNOS.Split(',');

            int iStartIndex = 1;
            int iEndIndex = 2;
            
            if (_DMNOS.Length > 0 && strArr.Length > 1)
            {
                for (int i = 0; i < strArr.Length; i++)
                {
                    if (i != 0)
                    {
                        iStartIndex = 2;
                        iEndIndex = 3;
                    }

                    if (this.uGridMWNotReg.ActiveRow.Cells[2].Text != strArr[i].Substring(iStartIndex, strArr[i].Length - iEndIndex))
                    {
                        strDMNO += strArr[i].Trim() + ", ";
                    }
                }

                _DMNOS = strDMNO.Substring(0, strDMNO.Length - 2);
            }            
        }

        #endregion

        #endregion

    }
}
