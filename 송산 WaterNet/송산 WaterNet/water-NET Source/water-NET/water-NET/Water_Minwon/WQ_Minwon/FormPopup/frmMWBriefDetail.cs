﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;
using WaterNet.WaterAOCore;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.WQ_Minwon.FormPopup
{
    /// <summary>
    /// 간략표시 폼에서 상세를 누른 경우 해당 민원에 대해 상세 정보를 표시하는 폼
    /// </summary>
    public partial class frmMWBriefDetail : Form
    {
        #region 프로퍼티

        private string _CANO;

        /// <summary>
        /// 민원관리번호
        /// </summary>
        public string CANO
        {
            get
            {
                return _CANO;
            }
            set
            {
                _CANO = value;
            }
        }
        #endregion

        public frmMWBriefDetail()
        {
            InitializeComponent();

            this.InitializeGird();
        }

        private void frmMWBriefDetail_Load(object sender, EventArgs e)
        {
            this.GetRegisteredWQMinwon();
            this.GetWQMinwonDetailProcessResultData();

            WQ_AppStatic.IS_SHOW_FORM_BRIEF_DETAIL = true;
        }

        private void frmMWBriefDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            WQ_AppStatic.IS_SHOW_FORM_BRIEF_DETAIL = false;
        }


        #region User Function

        #region - Initialize Function

        /// <summary>
        /// Grid를 초기화 한다. Column 설정 등...
        /// </summary>
        private void InitializeGird()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANO";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAAPPLDT";
            oUltraGridColumn.Header.Caption = "접수일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANM";
            oUltraGridColumn.Header.Caption = "민원인 성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACELL";
            oUltraGridColumn.Header.Caption = "연락처";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CALRGCD";
            oUltraGridColumn.Header.Caption = "민원구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAMIDCD";
            oUltraGridColumn.Header.Caption = "민원분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridMWReg1.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACONT";
            oUltraGridColumn.Header.Caption = "민원내용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기


            WaterNetCore.FormManager.SetGridStyle(this.uGridMWReg1);

            #region - Grid Caption Visible

            this.uGridMWReg1.DisplayLayout.CaptionVisible = DefaultableBoolean.True;

            #endregion
        }

        #endregion

        #region - SQL Function

        /// <summary>
        /// MAP의 민원 Marker를 클릭시 취득한 민원관리번호로 상세정보를 표시한다.
        /// </summary>
        private void GetRegisteredWQMinwon()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSBLOCK = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strCAMIDCD = string.Empty;
            string strWHERE_PARAM = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   A.CANO, TO_CHAR(CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, '' AS CACELL, A.CAADDR, D.CODE_NAME AS CALRGCD,");
            oStringBuilder.AppendLine("E.CODE_NAME || ' (' || E.CODE || ')' AS CAMIDCD, replace(a.cacont,'개인정보 기입 금지' || chr(10),'') cacont");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE D ON D.PCODE = '9003' AND D.CODE = A.CALRGCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE E ON E.PCODE = '9004' AND E.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "' AND A.CANO = '" + _CANO + "'");
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT, A.CANO");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridMWReg1.DataSource = pDS.Tables["WI_CAINFO"].DefaultView;

            //AutoResizeColumes
           // FormManager.SetGridStyle_PerformAutoResize(this.uGridMWReg1);

            if (this.uGridMWReg1.Rows.Count > 0) this.uGridMWReg1.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 지점이 등록된 민원 Data의 처리결과를 Select해서 Control에 Set
        /// </summary>
        private void GetWQMinwonDetailProcessResultData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT       TO_CHAR(PRCSDT, 'RRRR-MM-DD') AS DT, USERNM, DEPT, CAPRCSRSLT");
            oStringBuilder.AppendLine("FROM         WI_CAINFO");
            oStringBuilder.AppendLine("WHERE        CALRGCD = '2000' AND CANO = '" + _CANO + "'");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PROCRESULT");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                this.rtxtMWResult.Text = "";

                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.udtPROCDT.Value = oDRow["DT"].ToString().Trim();
                    this.txtUName.Text = oDRow["USERNM"].ToString().Trim();
                    this.txtDEPT.Text = oDRow["DEPT"].ToString().Trim();

                    this.rtxtMWResult.Text = oDRow["CAPRCSRSLT"].ToString().Trim();
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #endregion
    }
}
