﻿namespace WaterNet.WQ_Minwon.FormPopup
{
    partial class frmMWBrief
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblCANO = new System.Windows.Forms.Label();
            this.lblMWDT = new System.Windows.Forms.Label();
            this.lblDMNO = new System.Windows.Forms.Label();
            this.lblMWNM = new System.Windows.Forms.Label();
            this.lblMWGBN = new System.Windows.Forms.Label();
            this.btnDetail = new System.Windows.Forms.Button();
            this.tmrGetData = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtMWGBN = new System.Windows.Forms.TextBox();
            this.txtMWNM = new System.Windows.Forms.TextBox();
            this.txtDMNO = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCANO
            // 
            this.lblCANO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblCANO.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCANO.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCANO.Location = new System.Drawing.Point(0, 0);
            this.lblCANO.Name = "lblCANO";
            this.lblCANO.Size = new System.Drawing.Size(261, 22);
            this.lblCANO.TabIndex = 0;
            this.lblCANO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMWDT
            // 
            this.lblMWDT.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMWDT.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMWDT.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMWDT.Location = new System.Drawing.Point(0, 22);
            this.lblMWDT.Name = "lblMWDT";
            this.lblMWDT.Size = new System.Drawing.Size(261, 22);
            this.lblMWDT.TabIndex = 1;
            this.lblMWDT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDMNO
            // 
            this.lblDMNO.AutoSize = true;
            this.lblDMNO.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDMNO.Location = new System.Drawing.Point(16, 51);
            this.lblDMNO.Name = "lblDMNO";
            this.lblDMNO.Size = new System.Drawing.Size(54, 12);
            this.lblDMNO.TabIndex = 2;
            this.lblDMNO.Text = "수용가 :";
            this.lblDMNO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMWNM
            // 
            this.lblMWNM.AutoSize = true;
            this.lblMWNM.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMWNM.Location = new System.Drawing.Point(16, 73);
            this.lblMWNM.Name = "lblMWNM";
            this.lblMWNM.Size = new System.Drawing.Size(54, 12);
            this.lblMWNM.TabIndex = 4;
            this.lblMWNM.Text = "신청자 :";
            this.lblMWNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMWGBN
            // 
            this.lblMWGBN.AutoSize = true;
            this.lblMWGBN.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMWGBN.Location = new System.Drawing.Point(3, 97);
            this.lblMWGBN.Name = "lblMWGBN";
            this.lblMWGBN.Size = new System.Drawing.Size(67, 12);
            this.lblMWGBN.TabIndex = 6;
            this.lblMWGBN.Text = "민원분류 :";
            this.lblMWGBN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDetail
            // 
            this.btnDetail.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Infor;
            this.btnDetail.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetail.Location = new System.Drawing.Point(185, 91);
            this.btnDetail.Name = "btnDetail";
            this.btnDetail.Size = new System.Drawing.Size(72, 25);
            this.btnDetail.TabIndex = 7;
            this.btnDetail.Text = "상세";
            this.btnDetail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDetail.UseVisualStyleBackColor = true;
            this.btnDetail.Click += new System.EventHandler(this.btnDetail_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtMWGBN);
            this.panel1.Controls.Add(this.txtMWNM);
            this.panel1.Controls.Add(this.txtDMNO);
            this.panel1.Controls.Add(this.btnDetail);
            this.panel1.Controls.Add(this.lblMWGBN);
            this.panel1.Controls.Add(this.lblMWNM);
            this.panel1.Controls.Add(this.lblDMNO);
            this.panel1.Controls.Add(this.lblMWDT);
            this.panel1.Controls.Add(this.lblCANO);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(263, 120);
            this.panel1.TabIndex = 8;
            // 
            // txtMWGBN
            // 
            this.txtMWGBN.Location = new System.Drawing.Point(74, 93);
            this.txtMWGBN.Name = "txtMWGBN";
            this.txtMWGBN.ReadOnly = true;
            this.txtMWGBN.Size = new System.Drawing.Size(108, 21);
            this.txtMWGBN.TabIndex = 125;
            // 
            // txtMWNM
            // 
            this.txtMWNM.Location = new System.Drawing.Point(74, 70);
            this.txtMWNM.Name = "txtMWNM";
            this.txtMWNM.ReadOnly = true;
            this.txtMWNM.Size = new System.Drawing.Size(182, 21);
            this.txtMWNM.TabIndex = 124;
            // 
            // txtDMNO
            // 
            this.txtDMNO.Location = new System.Drawing.Point(74, 47);
            this.txtDMNO.Name = "txtDMNO";
            this.txtDMNO.ReadOnly = true;
            this.txtDMNO.Size = new System.Drawing.Size(182, 21);
            this.txtDMNO.TabIndex = 123;
            // 
            // frmMWBrief
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(269, 126);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMWBrief";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "수질 민원 지점 정보";
            this.Load += new System.EventHandler(this.frmMWBrief_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCANO;
        private System.Windows.Forms.Label lblMWDT;
        private System.Windows.Forms.Label lblDMNO;
        private System.Windows.Forms.Label lblMWNM;
        private System.Windows.Forms.Label lblMWGBN;
        private System.Windows.Forms.Button btnDetail;
        private System.Windows.Forms.Timer tmrGetData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtMWGBN;
        private System.Windows.Forms.TextBox txtMWNM;
        private System.Windows.Forms.TextBox txtDMNO;
    }
}