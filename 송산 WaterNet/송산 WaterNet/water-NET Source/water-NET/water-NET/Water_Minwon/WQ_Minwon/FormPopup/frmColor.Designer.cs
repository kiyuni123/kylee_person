﻿namespace WaterNet.WQ_Minwon.FormPopup
{
    partial class frmColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlLvl1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlLvl3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlLvl2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtLV1 = new System.Windows.Forms.TextBox();
            this.txtLV2 = new System.Windows.Forms.TextBox();
            this.txtLV3 = new System.Windows.Forms.TextBox();
            this.udtMWDateE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.udtMWDateS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnQuery1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLvl1
            // 
            this.pnlLvl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(250)))), ((int)(((byte)(70)))));
            this.pnlLvl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLvl1.Location = new System.Drawing.Point(7, 7);
            this.pnlLvl1.Name = "pnlLvl1";
            this.pnlLvl1.Size = new System.Drawing.Size(8, 18);
            this.pnlLvl1.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 12);
            this.label6.TabIndex = 20;
            this.label6.Text = "민원       건 이하";
            // 
            // pnlLvl3
            // 
            this.pnlLvl3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.pnlLvl3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLvl3.Location = new System.Drawing.Point(7, 49);
            this.pnlLvl3.Name = "pnlLvl3";
            this.pnlLvl3.Size = new System.Drawing.Size(8, 18);
            this.pnlLvl3.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 12);
            this.label8.TabIndex = 24;
            this.label8.Text = "민원       건 이상";
            // 
            // pnlLvl2
            // 
            this.pnlLvl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pnlLvl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLvl2.Location = new System.Drawing.Point(7, 28);
            this.pnlLvl2.Name = "pnlLvl2";
            this.pnlLvl2.Size = new System.Drawing.Size(8, 18);
            this.pnlLvl2.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 12);
            this.label9.TabIndex = 22;
            this.label9.Text = "민원       건 이하";
            // 
            // txtLV1
            // 
            this.txtLV1.Location = new System.Drawing.Point(49, 5);
            this.txtLV1.Name = "txtLV1";
            this.txtLV1.Size = new System.Drawing.Size(24, 21);
            this.txtLV1.TabIndex = 26;
            this.txtLV1.Text = "5";
            this.txtLV1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLV2
            // 
            this.txtLV2.Location = new System.Drawing.Point(49, 26);
            this.txtLV2.Name = "txtLV2";
            this.txtLV2.Size = new System.Drawing.Size(24, 21);
            this.txtLV2.TabIndex = 27;
            this.txtLV2.Text = "10";
            this.txtLV2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLV3
            // 
            this.txtLV3.Location = new System.Drawing.Point(49, 47);
            this.txtLV3.Name = "txtLV3";
            this.txtLV3.Size = new System.Drawing.Size(24, 21);
            this.txtLV3.TabIndex = 28;
            this.txtLV3.Text = "11";
            this.txtLV3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // udtMWDateE
            // 
            this.udtMWDateE.Location = new System.Drawing.Point(244, 25);
            this.udtMWDateE.Name = "udtMWDateE";
            this.udtMWDateE.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateE.TabIndex = 69;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.Location = new System.Drawing.Point(229, 29);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(15, 12);
            this.label47.TabIndex = 70;
            this.label47.Text = "~";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.Location = new System.Drawing.Point(126, 9);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 12);
            this.label44.TabIndex = 68;
            this.label44.Text = "접수일자 :";
            // 
            // udtMWDateS
            // 
            this.udtMWDateS.Location = new System.Drawing.Point(128, 25);
            this.udtMWDateS.Name = "udtMWDateS";
            this.udtMWDateS.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateS.TabIndex = 67;
            // 
            // btnQuery1
            // 
            this.btnQuery1.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Query;
            this.btnQuery1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery1.Location = new System.Drawing.Point(274, 46);
            this.btnQuery1.Name = "btnQuery1";
            this.btnQuery1.Size = new System.Drawing.Size(70, 26);
            this.btnQuery1.TabIndex = 71;
            this.btnQuery1.Text = "조회";
            this.btnQuery1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery1.UseVisualStyleBackColor = true;
            this.btnQuery1.Click += new System.EventHandler(this.btnQuery1_Click);
            // 
            // frmColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Ivory;
            this.ClientSize = new System.Drawing.Size(349, 73);
            this.ControlBox = false;
            this.Controls.Add(this.btnQuery1);
            this.Controls.Add(this.udtMWDateE);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.udtMWDateS);
            this.Controls.Add(this.txtLV3);
            this.Controls.Add(this.txtLV2);
            this.Controls.Add(this.txtLV1);
            this.Controls.Add(this.pnlLvl1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pnlLvl3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pnlLvl2);
            this.Controls.Add(this.label9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmColor";
            this.Text = "수질 민원의 GIS 표시 기준";
            this.Load += new System.EventHandler(this.frmColor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlLvl1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlLvl3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlLvl2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtLV1;
        private System.Windows.Forms.TextBox txtLV2;
        private System.Windows.Forms.TextBox txtLV3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateE;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateS;
        private System.Windows.Forms.Button btnQuery1;
    }
}