﻿namespace WaterNet.WQ_Minwon.FormPopup
{
    partial class frmWQMWPointManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWQMWPointManage));
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridMWNotReg = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnQueryR = new System.Windows.Forms.Button();
            this.chkSelectAllR = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.cboMWTypeR = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.udtMWDateRE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.udtMWDateRS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnCloseR = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS11 = new System.Windows.Forms.PictureBox();
            this.picFrBottomS11 = new System.Windows.Forms.PictureBox();
            this.picFrTopS11 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridMWReg = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnQueryD = new System.Windows.Forms.Button();
            this.chkSelectAllD = new System.Windows.Forms.CheckBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cboMWTypeD = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.udtMWDateDE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.udtMWDateDS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnCloseD = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.picFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMWNotReg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateRS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMWReg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateDE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridMWNotReg);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox1);
            this.ultraTabPageControl1.Controls.Add(this.picFrLeftS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrBottomS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrTopS11);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(847, 390);
            // 
            // uGridMWNotReg
            // 
            this.uGridMWNotReg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridMWNotReg.Location = new System.Drawing.Point(4, 36);
            this.uGridMWNotReg.Name = "uGridMWNotReg";
            this.uGridMWNotReg.Size = new System.Drawing.Size(841, 350);
            this.uGridMWNotReg.TabIndex = 115;
            this.uGridMWNotReg.Text = "미등록된 수질민원";
            this.uGridMWNotReg.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGridMWNotReg.Click += new System.EventHandler(this.uGrid_Click);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Gold;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox17.Location = new System.Drawing.Point(4, 32);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(841, 4);
            this.pictureBox17.TabIndex = 114;
            this.pictureBox17.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnQueryR);
            this.panel2.Controls.Add(this.chkSelectAllR);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.cboMWTypeR);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.udtMWDateRE);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.udtMWDateRS);
            this.panel2.Controls.Add(this.btnCloseR);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(841, 28);
            this.panel2.TabIndex = 113;
            // 
            // btnQueryR
            // 
            this.btnQueryR.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQueryR.Image = ((System.Drawing.Image)(resources.GetObject("btnQueryR.Image")));
            this.btnQueryR.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQueryR.Location = new System.Drawing.Point(569, 0);
            this.btnQueryR.Name = "btnQueryR";
            this.btnQueryR.Size = new System.Drawing.Size(90, 26);
            this.btnQueryR.TabIndex = 5;
            this.btnQueryR.Text = "민원 조회";
            this.btnQueryR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQueryR.UseVisualStyleBackColor = true;
            this.btnQueryR.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // chkSelectAllR
            // 
            this.chkSelectAllR.AutoSize = true;
            this.chkSelectAllR.Location = new System.Drawing.Point(496, 6);
            this.chkSelectAllR.Name = "chkSelectAllR";
            this.chkSelectAllR.Size = new System.Drawing.Size(72, 16);
            this.chkSelectAllR.TabIndex = 70;
            this.chkSelectAllR.Text = "전체선택";
            this.chkSelectAllR.UseVisualStyleBackColor = true;
            this.chkSelectAllR.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(659, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 26);
            this.btnSave.TabIndex = 69;
            this.btnSave.Text = "지점 등록";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cboMWTypeR
            // 
            this.cboMWTypeR.FormattingEnabled = true;
            this.cboMWTypeR.Location = new System.Drawing.Point(79, 3);
            this.cboMWTypeR.Name = "cboMWTypeR";
            this.cboMWTypeR.Size = new System.Drawing.Size(118, 20);
            this.cboMWTypeR.TabIndex = 68;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(6, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 12);
            this.label11.TabIndex = 67;
            this.label11.Text = "민원분류 :";
            // 
            // udtMWDateRE
            // 
            this.udtMWDateRE.Location = new System.Drawing.Point(390, 3);
            this.udtMWDateRE.Name = "udtMWDateRE";
            this.udtMWDateRE.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateRE.TabIndex = 65;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.Location = new System.Drawing.Point(376, 9);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(15, 12);
            this.label47.TabIndex = 66;
            this.label47.Text = "~";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.Location = new System.Drawing.Point(203, 8);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 12);
            this.label44.TabIndex = 64;
            this.label44.Text = "접수일자 :";
            // 
            // udtMWDateRS
            // 
            this.udtMWDateRS.Location = new System.Drawing.Point(276, 3);
            this.udtMWDateRS.Name = "udtMWDateRS";
            this.udtMWDateRS.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateRS.TabIndex = 63;
            // 
            // btnCloseR
            // 
            this.btnCloseR.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnCloseR.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseR.Image")));
            this.btnCloseR.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCloseR.Location = new System.Drawing.Point(749, 0);
            this.btnCloseR.Name = "btnCloseR";
            this.btnCloseR.Size = new System.Drawing.Size(90, 26);
            this.btnCloseR.TabIndex = 3;
            this.btnCloseR.Text = "닫기";
            this.btnCloseR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCloseR.UseVisualStyleBackColor = true;
            this.btnCloseR.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(845, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(2, 382);
            this.pictureBox1.TabIndex = 112;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftS11
            // 
            this.picFrLeftS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS11.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS11.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftS11.Name = "picFrLeftS11";
            this.picFrLeftS11.Size = new System.Drawing.Size(4, 382);
            this.picFrLeftS11.TabIndex = 111;
            this.picFrLeftS11.TabStop = false;
            // 
            // picFrBottomS11
            // 
            this.picFrBottomS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS11.Location = new System.Drawing.Point(0, 386);
            this.picFrBottomS11.Name = "picFrBottomS11";
            this.picFrBottomS11.Size = new System.Drawing.Size(847, 4);
            this.picFrBottomS11.TabIndex = 110;
            this.picFrBottomS11.TabStop = false;
            // 
            // picFrTopS11
            // 
            this.picFrTopS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS11.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS11.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS11.Name = "picFrTopS11";
            this.picFrTopS11.Size = new System.Drawing.Size(847, 4);
            this.picFrTopS11.TabIndex = 109;
            this.picFrTopS11.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridMWReg);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox2);
            this.ultraTabPageControl2.Controls.Add(this.panel1);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox3);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox4);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox5);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(847, 390);
            // 
            // uGridMWReg
            // 
            this.uGridMWReg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridMWReg.Location = new System.Drawing.Point(4, 36);
            this.uGridMWReg.Name = "uGridMWReg";
            this.uGridMWReg.Size = new System.Drawing.Size(841, 350);
            this.uGridMWReg.TabIndex = 122;
            this.uGridMWReg.Text = "등록된 수질민원";
            this.uGridMWReg.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGridMWReg.Click += new System.EventHandler(this.uGrid_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(4, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(841, 4);
            this.pictureBox2.TabIndex = 121;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnQueryD);
            this.panel1.Controls.Add(this.chkSelectAllD);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.cboMWTypeD);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.udtMWDateDE);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.udtMWDateDS);
            this.panel1.Controls.Add(this.btnCloseD);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(841, 28);
            this.panel1.TabIndex = 120;
            // 
            // btnQueryD
            // 
            this.btnQueryD.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQueryD.Image = ((System.Drawing.Image)(resources.GetObject("btnQueryD.Image")));
            this.btnQueryD.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQueryD.Location = new System.Drawing.Point(569, 0);
            this.btnQueryD.Name = "btnQueryD";
            this.btnQueryD.Size = new System.Drawing.Size(90, 26);
            this.btnQueryD.TabIndex = 5;
            this.btnQueryD.Text = "지점 조회";
            this.btnQueryD.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQueryD.UseVisualStyleBackColor = true;
            this.btnQueryD.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // chkSelectAllD
            // 
            this.chkSelectAllD.AutoSize = true;
            this.chkSelectAllD.Location = new System.Drawing.Point(496, 6);
            this.chkSelectAllD.Name = "chkSelectAllD";
            this.chkSelectAllD.Size = new System.Drawing.Size(72, 16);
            this.chkSelectAllD.TabIndex = 71;
            this.chkSelectAllD.Text = "전체선택";
            this.chkSelectAllD.UseVisualStyleBackColor = true;
            this.chkSelectAllD.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(659, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 26);
            this.btnDelete.TabIndex = 70;
            this.btnDelete.Text = "지점 삭제";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // cboMWTypeD
            // 
            this.cboMWTypeD.FormattingEnabled = true;
            this.cboMWTypeD.Location = new System.Drawing.Point(79, 3);
            this.cboMWTypeD.Name = "cboMWTypeD";
            this.cboMWTypeD.Size = new System.Drawing.Size(118, 20);
            this.cboMWTypeD.TabIndex = 68;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 12);
            this.label1.TabIndex = 67;
            this.label1.Text = "민원분류 :";
            // 
            // udtMWDateDE
            // 
            this.udtMWDateDE.Location = new System.Drawing.Point(390, 3);
            this.udtMWDateDE.Name = "udtMWDateDE";
            this.udtMWDateDE.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateDE.TabIndex = 65;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(376, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 12);
            this.label2.TabIndex = 66;
            this.label2.Text = "~";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(203, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 12);
            this.label3.TabIndex = 64;
            this.label3.Text = "접수일자 :";
            // 
            // udtMWDateDS
            // 
            this.udtMWDateDS.Location = new System.Drawing.Point(276, 3);
            this.udtMWDateDS.Name = "udtMWDateDS";
            this.udtMWDateDS.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateDS.TabIndex = 63;
            // 
            // btnCloseD
            // 
            this.btnCloseD.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnCloseD.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseD.Image")));
            this.btnCloseD.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCloseD.Location = new System.Drawing.Point(749, 0);
            this.btnCloseD.Name = "btnCloseD";
            this.btnCloseD.Size = new System.Drawing.Size(90, 26);
            this.btnCloseD.TabIndex = 3;
            this.btnCloseD.Text = "닫기";
            this.btnCloseD.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCloseD.UseVisualStyleBackColor = true;
            this.btnCloseD.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(845, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(2, 382);
            this.pictureBox3.TabIndex = 119;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox4.Location = new System.Drawing.Point(0, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(4, 382);
            this.pictureBox4.TabIndex = 118;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(0, 386);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(847, 4);
            this.pictureBox5.TabIndex = 117;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(0, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(847, 4);
            this.pictureBox6.TabIndex = 116;
            this.pictureBox6.TabStop = false;
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 416);
            this.picFrLeft.TabIndex = 110;
            this.picFrLeft.TabStop = false;
            // 
            // picFrRight
            // 
            this.picFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRight.Location = new System.Drawing.Point(855, 4);
            this.picFrRight.Name = "picFrRight";
            this.picFrRight.Size = new System.Drawing.Size(2, 416);
            this.picFrRight.TabIndex = 111;
            this.picFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 420);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(857, 4);
            this.picFrBottom.TabIndex = 109;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(857, 4);
            this.picFrTop.TabIndex = 108;
            this.picFrTop.TabStop = false;
            // 
            // uTab
            // 
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTab.Location = new System.Drawing.Point(4, 4);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(851, 416);
            this.uTab.TabIndex = 112;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "지점등록";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "지점삭제";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(847, 390);
            // 
            // frmWQMWPointManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 424);
            this.Controls.Add(this.uTab);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.picFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.MinimizeBox = false;
            this.Name = "frmWQMWPointManage";
            this.Text = "수질 민원 지점 등록 및 삭제";
            this.Load += new System.EventHandler(this.frmWQMWPointManage_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWQMWPointManage_FormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMWNotReg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateRS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMWReg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateDE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox picFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftS11;
        private System.Windows.Forms.PictureBox picFrBottomS11;
        private System.Windows.Forms.PictureBox picFrTopS11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cboMWTypeR;
        private System.Windows.Forms.Label label11;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateRE;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateRS;
        private System.Windows.Forms.Button btnQueryR;
        private System.Windows.Forms.Button btnCloseR;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Button btnSave;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMWNotReg;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMWReg;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cboMWTypeD;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateDE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateDS;
        private System.Windows.Forms.Button btnQueryD;
        private System.Windows.Forms.Button btnCloseD;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.CheckBox chkSelectAllR;
        private System.Windows.Forms.CheckBox chkSelectAllD;
    }
}