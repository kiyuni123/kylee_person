﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;
using WaterNet.WaterAOCore;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using ChartFX.WinForms;
using ChartFX.WinForms.DataProviders;
using EMFrame.log;

namespace WaterNet.WQ_Minwon.FormPopup
{
    /// <summary>
    /// Project ID : WN_WQ_A06
    /// Project Explain : 수질민원관리
    /// Project Developer : 오두석
    /// Project Create Date : 2010.10.15
    /// Form Explain : 수질민원정보조회 Popup Form
    /// </summary>
    public partial class frmWQPMWReport : Form
    {
        #region 프로퍼티

        private string _SFTR_IDN;

        /// <summary>
        /// 소블록 FTR_IDN
        /// </summary>
        public string SFTR_IDN
        {
            get
            {
                return _SFTR_IDN;
            }
            set
            {
                _SFTR_IDN = value;
            }
        }
        #endregion


        private string m_MW_NO = string.Empty;
        
        public frmWQPMWReport()
        {
            InitializeComponent();

            this.InitializeSetting();
        }

        private void frmWQPMWReport_Load(object sender, EventArgs e)
        {
            this.chkChart.Checked = true;
            this.chkGrid.Checked = true;
            this.chkChart2.Checked = true;
            this.chkGrid2.Checked= true;
            this.udtMWDateS.DateTime = DateTime.Now;
            this.udtMWDateE.DateTime = DateTime.Now;
            this.udtMWDetailE.DateTime = DateTime.Now;
            this.udtMWDetailE2.DateTime = DateTime.Now;
            this.udtMWDetailE3.DateTime = DateTime.Now;
            this.udtMWDetailS.DateTime = DateTime.Now;
            this.udtMWDetailS2.DateTime = DateTime.Now;
            this.udtMWDetailS3.DateTime = DateTime.Now;

            WQ_Function.SetCombo_MinwonType(this.cboMWType);
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock2, EMFrame.statics.AppStatic.USER_SGCCD);
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock3, EMFrame.statics.AppStatic.USER_SGCCD);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateS, 0, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtMWDateS, -3);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDateE, 0, 0);            
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDetailS, 2, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtMWDetailS, -3);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDetailE, 2, 0);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDetailS2, 2, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtMWDetailS2, -3);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDetailE2, 2, 0);

            WQ_Function.SetUDateTime_MaskInput(this.udtMWDetailS3, 2, 0);
            WQ_Function.SetUDateTime_ModifyMonth(this.udtMWDetailS3, -3);
            WQ_Function.SetUDateTime_MaskInput(this.udtMWDetailE3, 2, 0);

            //frmWQMain에서 소블록 FTR_IDN을 프로퍼티로 넘겨 폼을 로드한경우 해당 중,소블록으로 셋
            if (_SFTR_IDN != string.Empty)
            {
                string strSBLOCK = WQ_Function.GetBlockNameCodeByFTR_IDN(_SFTR_IDN);
                string strMFTR_IDN = WQ_Function.GetMediumBlockBySmallBlockCode(WQ_Function.SplitToCode(strSBLOCK));
                strMFTR_IDN = WQ_Function.GetMediumBlockFTR_IDN(strMFTR_IDN);
                string strMBLOCK = WQ_Function.GetBlockNameCodeByFTR_IDN(strMFTR_IDN);

                WQ_Function.SetCombo_SelectFindData(this.cboMBlock, strMBLOCK);
                WQ_Function.SetCombo_SelectFindData(this.cboSBlock, strSBLOCK);
            }

            this.GetMinwonDetailData();
            if (this.uGridBase.Rows.Count > 0)
            {
                this.GetMinwonDetailProcessResultData(this.uGridBase.ActiveRow.Cells[0].Text);
            }
            this.GetMinwonDetailPresentStateData();
            this.GetMinwonDetailPresentStateDataByMediumBlock();
            this.GetMinwonDetailVolumeByMediumBlock();

            WQ_AppStatic.IS_SHOW_FORM_REPORT = true;
        }

        private void frmWQPMWReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.ClosePopup();
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (this.uTabMW.SelectedTab.Index)
                {
                    case 0:
                        this.GetMinwonDetailData();
                        if (this.uGridBase.Rows.Count > 0)
                        {
                            this.GetMinwonDetailProcessResultData(this.uGridBase.ActiveRow.Cells[0].Text);
                        }
                        break;
                    case 1:
                        this.GetMinwonDetailPresentStateData();
                        break;
                    case 2:
                        this.GetMinwonDetailPresentStateDataByMediumBlock();
                        break;
                    case 3:
                        this.GetMinwonDetailVolumeByMediumBlock();
                        break;
                }            
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (this.uTabMW.SelectedTab.Index)
                {
                    case 0:
                        if (this.uGridBase.Rows.Count <= 0) return;
                        FormManager.ExportToExcelFromUltraGrid(this.uGridBase, this.uTabMW.SelectedTab.Text);
                        break;
                    case 1:
                        if (this.uGridDetailPS.Rows.Count <= 0) return;

                        if (FunctionManager.IsDirectoryExists(WQ_AppStatic.TEMP_IMG_PATH) != true) FunctionManager.CreateDirectory(WQ_AppStatic.TEMP_IMG_PATH);

                        this.cFXData.Export(FileFormat.Bitmap, WQ_AppStatic.TEMP_IMG_PATH + "Chart.png");

                        //Temp Grid에 Chart FX에서 Export한 Image File을 Add 한다.
                        //-----------------------------------------------------------------------------------------------------------
                        System.IO.FileStream pFS = new System.IO.FileStream(WQ_AppStatic.TEMP_IMG_PATH + "Chart.png", FileMode.OpenOrCreate, FileAccess.Read);
                        byte[] pByte = new byte[pFS.Length];

                        pFS.Read(pByte, 0, System.Convert.ToInt32(pFS.Length));
                        pFS.Close();

                        DataSet pDS = new DataSet();
                        DataTable pDT = new DataTable("TBLCHART");
                        DataColumn pDC = new DataColumn("CHART_IMG", typeof(byte[]));
                        DataRow pRow;

                        pDT.Columns.Add(pDC);
                        pDS.Tables.Add(pDT);

                        pRow = pDS.Tables["TBLCHART"].NewRow();
                        pRow[0] = pByte;

                        pDS.Tables["TBLCHART"].Rows.Add(pRow);

                        uGridChartImage.DataSource = pDS.Tables["TBLCHART"];
                        FormManager.SetGridStyle_PerformAutoResize(this.uGridChartImage);
                        //-----------------------------------------------------------------------------------------------------------

                        FormManager.ExportToExcelFromUltraGrid(this.uGridDetailPS, this.uGridChartImage, this.uTabMW.SelectedTab.Text, 3);
                        break;
                    case 2:
                        if (this.uGridDetailPS2.Rows.Count <= 0) return;

                        if (FunctionManager.IsDirectoryExists(WQ_AppStatic.TEMP_IMG_PATH) != true) FunctionManager.CreateDirectory(WQ_AppStatic.TEMP_IMG_PATH);

                        this.cFXData2.Export(FileFormat.Bitmap, WQ_AppStatic.TEMP_IMG_PATH + "Chart.png");

                        //Temp Grid에 Chart FX에서 Export한 Image File을 Add 한다.
                        //-----------------------------------------------------------------------------------------------------------
                        System.IO.FileStream pFS2 = new System.IO.FileStream(WQ_AppStatic.TEMP_IMG_PATH + "Chart.png", FileMode.OpenOrCreate, FileAccess.Read);
                        byte[] pByte2 = new byte[pFS2.Length];

                        pFS2.Read(pByte2, 0, System.Convert.ToInt32(pFS2.Length));
                        pFS2.Close();

                        DataSet pDS2 = new DataSet();
                        DataTable pDT2 = new DataTable("TBLCHART");
                        DataColumn pDC2 = new DataColumn("CHART_IMG", typeof(byte[]));
                        DataRow pRow2;

                        pDT2.Columns.Add(pDC2);
                        pDS2.Tables.Add(pDT2);

                        pRow2 = pDS2.Tables["TBLCHART"].NewRow();
                        pRow2[0] = pByte2;

                        pDS2.Tables["TBLCHART"].Rows.Add(pRow2);

                        uGridChartImage2.DataSource = pDS2.Tables["TBLCHART"];
                        FormManager.SetGridStyle_PerformAutoResize(this.uGridChartImage2);
                        //-----------------------------------------------------------------------------------------------------------

                        FormManager.ExportToExcelFromUltraGrid(this.uGridDetailPS2, this.uGridChartImage2, this.uTabMW.SelectedTab.Text, 3);
                        break;
                    case 3:
                        if (this.uGridDetailPS3.Rows.Count <= 0) return;
                        FormManager.ExportToExcelFromUltraGrid(this.uGridDetailPS3, this.uTabMW.SelectedTab.Text);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ClosePopup();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        #endregion


        #region Control Events

        private void uGridBase_Click(object sender, EventArgs e)
        {
            if (this.uGridBase.Rows.Count <= 0) return;

            this.rtxtMWContent.Text = "";
            this.m_MW_NO = this.uGridBase.ActiveRow.Cells[1].Text;
            this.rtxtMWContent.Text = this.uGridBase.ActiveRow.Cells[9].Text;
            this.GetMinwonDetailProcessResultData(m_MW_NO);
        }

        private void uGridBase_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridBase.Rows.Count <= 0) return;

            this.m_MW_NO = this.uGridBase.ActiveRow.Cells[1].Text;

            double dblMAP_SCALE = 5000;

            if (WQ_AppStatic.IMAP != null)
            {
                ILayer pLayer = ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "민원지점");
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, "CANO='" + this.m_MW_NO + "'") as IFeatureCursor;

                IFeature pFeature = pCursor.NextFeature();

                if (WQ_AppStatic.IMAP != null)
                {
                    if (pFeature != null)
                    {
                        IGeometry oIGeometry = pFeature.Shape;

                        ArcManager.FlashShape(WQ_AppStatic.IMAP.ActiveView, oIGeometry, 10);

                        if (ArcManager.GetMapScale(WQ_AppStatic.IMAP) > dblMAP_SCALE)
                        {
                            ArcManager.SetMapScale(WQ_AppStatic.IMAP, dblMAP_SCALE);
                        }

                        ArcManager.MoveCenterAt(WQ_AppStatic.IMAP, pFeature);

                        ArcManager.PartialRefresh(WQ_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
                        Application.DoEvents();
                    }
                    this.BringToFront();
                }
            }
        }

        private void uTabMW_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            switch (e.Tab.Index)
            {
                case 0:
                    this.GetMinwonDetailData();
                    break;
                case 1:
                    this.GetMinwonDetailPresentStateData();
                    break;
                case 2:
                    this.GetMinwonDetailPresentStateDataByMediumBlock();
                    break;
                case 3:
                    this.GetMinwonDetailVolumeByMediumBlock();
                    break;
            }
        }

        #region - Combo

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);
            //this.Viewmap_MinwonBlock(strMFTRIDN, 0);
        }

        private void cboSBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strSFTRIDN = WQ_Function.SplitToCode(this.cboSBlock.Text);
            //this.Viewmap_MinwonBlock(strSFTRIDN, 1);
        }
        

        private void cboLBlock2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock2.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock2, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN);
        }

        private void cboLBlock3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock3.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock3, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN);
        }

        private void cboMBlock2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock2.Text);
            //this.Viewmap_MinwonBlock(strMFTRIDN, 0);
        }

        private void cboMBlock3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock3.Text);
            //this.Viewmap_MinwonBlock(strMFTRIDN, 0);
        }

        #endregion

        #region - Check

        private void chkGrid_CheckedChanged(object sender, EventArgs e)
        {
            this.SetViewControl();
        }

        private void chkChart_CheckedChanged(object sender, EventArgs e)
        {
            this.SetViewControl();
        }

        #endregion

        #endregion


        #region User Function

        #region - Initialize Function

        /// <summary>
        /// 초기 실행시 환경설정
        /// </summary>
        private void InitializeSetting()
        {
            //UltraGrid 초기설정
            UltraGridColumn oUltraGridColumn;

            #region - 그리드 설정

            #region - 민원상세기본정보조회

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANO";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            
            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAAPPLDT";
            oUltraGridColumn.Header.Caption = "접수일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CANM";
            oUltraGridColumn.Header.Caption = "민원인 성명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACELL";
            oUltraGridColumn.Header.Caption = "연락처";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAADDR";
            oUltraGridColumn.Header.Caption = "주소";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CALRGCD";
            oUltraGridColumn.Header.Caption = "민원구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CAMIDCD";
            oUltraGridColumn.Header.Caption = "민원분류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridBase.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CACONT";
            oUltraGridColumn.Header.Caption = "민원내용";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridBase);

            #endregion

            #region - 민원상세현황정보(기간)

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DT";
            oUltraGridColumn.Header.Caption = "상세등록월";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2010";
            oUltraGridColumn.Header.Caption = "흑수발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2015";
            oUltraGridColumn.Header.Caption = "적수발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2030";
            oUltraGridColumn.Header.Caption = "이물질발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2040";
            oUltraGridColumn.Header.Caption = "흙탕물발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2050";
            oUltraGridColumn.Header.Caption = "악취발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2099";
            oUltraGridColumn.Header.Caption = "수질기타";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridDetailPS);

            #endregion

            #region - 민원상세현황정보(중블록)

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DT";
            oUltraGridColumn.Header.Caption = "상세등록월";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2010";
            oUltraGridColumn.Header.Caption = "흑수발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2015";
            oUltraGridColumn.Header.Caption = "적수발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2030";
            oUltraGridColumn.Header.Caption = "이물질발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2040";
            oUltraGridColumn.Header.Caption = "흙탕물발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2050";
            oUltraGridColumn.Header.Caption = "악취발생";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_2099";
            oUltraGridColumn.Header.Caption = "수질기타";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridDetailPS2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VOL_SUM";
            oUltraGridColumn.Header.Caption = "합계";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridDetailPS2);

            #endregion

            #region - 차트이미지

            oUltraGridColumn = this.uGridChartImage.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHART_IMG";
            oUltraGridColumn.Header.Caption = "차트";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Image;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = this.uGridChartImage2.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHART_IMG";
            oUltraGridColumn.Header.Caption = "차트";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Image;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기
            //WaterNetCore.FormManager.SetGridStyle(this.uGridChartImage);

            #endregion

            #region - 블록별민원건수(중블록)

            WaterNetCore.FormManager.SetGridStyle(this.uGridDetailPS3);

            #endregion

            #endregion
        }

        #endregion

        #region - SQL Fuction
        /// <summary>
        /// 수질민원데이터 중 Layer에 지점이 등록된 민원에 대해 Detail Data를 Grid에 Set
        /// </summary>
        private void GetMinwonDetailData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strCAMIDCD = string.Empty;
            string strWHERE_PARAM = string.Empty;
            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strSDate = WQ_Function.StringToDateTime(this.udtMWDateS.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.udtMWDateE.DateTime);

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);
            strBLOCK_CD_S = WQ_Function.GetSmallBlockFTR_IDN(strBLOCK_CD_S);
            

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockFTRIDNInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "         AND F.SFTRIDN IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = "         AND F.SFTRIDN = '" + strBLOCK_CD_S + "'";
                }
            }

            strCAMIDCD = WQ_Function.SplitToCode(this.cboMWType.Text).Trim();

            if (strCAMIDCD != "")
            {
                strWHERE_PARAM = "AND A.CAMIDCD = '" + strCAMIDCD + "'";
            }

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   G.LOC_NAME || ' (' || G.LOC_CODE || ')' AS BLOCK, A.CANO, TO_CHAR(CAAPPLDT, 'RRRR-MM-DD HH24:MI:SS') AS CAAPPLDT, A.DMNO, A.CANM, '' AS CACELL, A.CAADDR, D.CODE_NAME AS CALRGCD, E.CODE_NAME || ' (' || E.CODE || ')' AS CAMIDCD, replace(a.cacont,'개인정보 기입 금지' || chr(10),'') cacont");
            oStringBuilder.AppendLine("FROM     WI_CAINFO A");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE D ON D.PCODE = '9003' AND D.CODE = A.CALRGCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_CODE E ON E.PCODE = '9004' AND E.CODE = A.CAMIDCD");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN WI_DMINFO F ON F.DMNO = A.DMNO");
            oStringBuilder.AppendLine("         LEFT OUTER JOIN CM_LOCATION G ON G.FTR_IDN = F.SFTRIDN");
            oStringBuilder.AppendLine("WHERE    A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("         AND A.DMNO IS NOT NULL");
            oStringBuilder.AppendLine("         AND A.CALRGCD = '2000' AND SUBSTR(A.CAMIDCD, 0, 2) = '20' AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("         AND TO_CHAR(A.CAAPPLDT, 'RRRRMMDD') BETWEEN '" + strSDate + "' AND '" + strEDate + "' ");
            oStringBuilder.AppendLine("         " + strBLOCK_SQL);
            oStringBuilder.AppendLine("         AND A.CANO IN (SELECT X.CANO FROM WQ_MINWON X WHERE SUBSTR(X.MW_DATE, 0, 8) = TO_CHAR(A.CAAPPLDT, 'RRRRMMDD') AND X.SFTR_IDN = F.SFTRIDN) " + strWHERE_PARAM);
            oStringBuilder.AppendLine("ORDER BY CAAPPLDT DESC, A.CANO");
 
            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridBase.DataSource = pDS.Tables["WI_CAINFO"].DefaultView;

            //AutoResizeColumes
            //FormManager.SetGridStyle_PerformAutoResize(this.uGridBase);

            if (this.uGridBase.Rows.Count > 0) this.uGridBase.Rows[0].Activated = true;

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// I-Water의 수질민원기본 Data를 조회한 민원에 대해 상세하게 입력한 Data중 처리결과 조회
        /// </summary>
        private void GetMinwonDetailProcessResultData(string strCANO)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);

            oStringBuilder.AppendLine("SELECT       TO_CHAR(PRCSDT, 'RRRR-MM-DD') AS DT, USERNM, DEPT, CAPRCSRSLT");
            oStringBuilder.AppendLine("FROM         WI_CAINFO");
            oStringBuilder.AppendLine("WHERE        CALRGCD = '2000' AND CANO = '" + strCANO + "'");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PROCRESULT");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                this.rtxtMWResult.Text = "";

                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    this.udtPROCDT.Value = oDRow["DT"].ToString().Trim();
                    this.txtUName.Text = oDRow["USERNM"].ToString().Trim();
                    this.txtDEPT.Text = oDRow["DEPT"].ToString().Trim();

                    this.rtxtMWResult.Text = oDRow["CAPRCSRSLT"].ToString().Trim();
                }
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// I-Water의 수질민원기본 Data를 조회한 민원에 대해 상세하게 입력한 Data의 현황을 조회하여 Grid에 Set한다.
        /// </summary>
        private void GetMinwonDetailPresentStateData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strDateS = string.Empty;
            string strDateE = string.Empty;

            DataSet pDS = new DataSet();

            strDateS = WQ_Function.StringToDateTime(this.udtMWDetailS.DateTime).Substring(0, 6);
            strDateE = WQ_Function.StringToDateTime(this.udtMWDetailE.DateTime).Substring(0, 6);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT MON.DT                                       ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2010,0) AS VOL_2010             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2015,0) AS VOL_2015             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2030,0) AS VOL_2030             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2040,0) AS VOL_2040             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2050,0) AS VOL_2050             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2099,0) AS VOL_2099             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_SUM,0) AS VOL_SUM               ");
            oStringBuilder.AppendLine("FROM                                                ");
            oStringBuilder.AppendLine("(                                                   ");
            oStringBuilder.AppendLine("SELECT       TO_CHAR(A.CAAPPLDT, 'RRRR-MM') AS DT,  ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2010', 1)) AS VOL_2010, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2015', 1)) AS VOL_2015, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2030', 1)) AS VOL_2030, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2040', 1)) AS VOL_2040, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2050', 1)) AS VOL_2050, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2050', 1)) AS VOL_2099, ");
            oStringBuilder.AppendLine("             (COUNT(DECODE(A.CAMIDCD, '2010', 1)) + COUNT(DECODE(A.CAMIDCD, '2030', 1)) + COUNT(DECODE(A.CAMIDCD, '2040', 1)) + COUNT(DECODE(A.CAMIDCD, '2050', 1)) + COUNT(DECODE(A.CAMIDCD, '2099', 1))) AS VOL_SUM ");
            oStringBuilder.AppendLine("FROM         WI_CAINFO A");
            oStringBuilder.AppendLine("WHERE        A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("             AND A.DMNO IS NOT NULL");
            oStringBuilder.AppendLine("             AND A.CALRGCD = '2000'");
            oStringBuilder.AppendLine("             AND SUBSTR(A.CAMIDCD, 0, 2) = '20' ");
            oStringBuilder.AppendLine("             AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("             AND TO_CHAR(A.CAAPPLDT, 'RRRRMM') BETWEEN '" + strDateS + "' AND '" + strDateE + "'");
            oStringBuilder.AppendLine("GROUP BY     TO_CHAR(A.CAAPPLDT, 'RRRR-MM')");
            oStringBuilder.AppendLine(")CA                                        ");
            oStringBuilder.AppendLine(",(                                          ");
            oStringBuilder.AppendLine("SELECT to_char(ADD_MONTHS(to_date('" + strDateS + "', 'YYYY-MM') , level -1 ),'YYYY-MM') AS DT                           ");
            oStringBuilder.AppendLine("FROM   DUAL                                                                                                              ");
            oStringBuilder.AppendLine("CONNECT BY LEVEL <= MONTHS_BETWEEN(to_date('" + strDateE + "', 'YYYYMM') , to_date('" + strDateS + "', 'YYYYMM')) + 1    ");
            oStringBuilder.AppendLine(")MON                                                                                                                     ");
            oStringBuilder.AppendLine("WHERE CA.DT(+) = MON.DT                                          ");
            oStringBuilder.AppendLine("ORDER BY     DT");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            //this.uGridDetailPS.DataSource = pDS.Tables["WQ_MINWON_MAPPING"].DefaultView;
            this.uGridDetailPS.DataSource = pDS.Tables["WI_CAINFO"];

            //AutoResizeColumes
            //FormManager.SetGridStyle_PerformAutoResize(this.uGridDetailPS);

            if (this.uGridDetailPS.Rows.Count > 0) this.uGridDetailPS.Rows[0].Selected = true;

            //ChartFX에 DataSet 연결
            this.cFXData.Data.Clear();
            this.cFXData.Series.Clear();
            this.cFXData.DataSourceSettings.Fields.Clear();
            if (this.uGridDetailPS.Rows.Count > 0) this.SetChartData();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// I-Water의 수질민원기본 Data를 조회한 민원에 대해 상세하게 입력한 Data의 현황을 조회하여 Grid에 Set한다.
        /// </summary>
        private void GetMinwonDetailPresentStateDataByMediumBlock()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strDateS = string.Empty;
            string strDateE = string.Empty;
            string strMFTRIDN = string.Empty;

            DataSet pDS = new DataSet();

            strDateS = WQ_Function.StringToDateTime(this.udtMWDetailS2.DateTime).Substring(0, 6);
            strDateE = WQ_Function.StringToDateTime(this.udtMWDetailE2.DateTime).Substring(0, 6);
            strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock2.Text);
            strMFTRIDN = WQ_Function.GetMediumBlockFTR_IDN(strMFTRIDN);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT MON.DT                                       ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2010,0) AS VOL_2010             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2015,0) AS VOL_2015             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2030,0) AS VOL_2030             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2040,0) AS VOL_2040             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2050,0) AS VOL_2050             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_2099,0) AS VOL_2099             ");
            oStringBuilder.AppendLine("       , NVL(CA.VOL_SUM,0) AS VOL_SUM               ");
            oStringBuilder.AppendLine("FROM                                                ");
            oStringBuilder.AppendLine("(                                                   ");
            oStringBuilder.AppendLine("SELECT       TO_CHAR(A.CAAPPLDT, 'RRRR-MM') AS DT,  ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2010', 1)) AS VOL_2010, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2015', 1)) AS VOL_2015, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2030', 1)) AS VOL_2030, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2040', 1)) AS VOL_2040, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2050', 1)) AS VOL_2050, ");
            oStringBuilder.AppendLine("             COUNT(DECODE(A.CAMIDCD, '2099', 1)) AS VOL_2099, ");
            oStringBuilder.AppendLine("             (COUNT(DECODE(A.CAMIDCD, '2010', 1)) + COUNT(DECODE(A.CAMIDCD, '2030', 1)) + COUNT(DECODE(A.CAMIDCD, '2040', 1)) + COUNT(DECODE(A.CAMIDCD, '2050', 1)) + COUNT(DECODE(A.CAMIDCD, '2099', 1))) AS VOL_SUM ");
            oStringBuilder.AppendLine("FROM         WI_CAINFO A, WI_DMINFO B");
            oStringBuilder.AppendLine("WHERE        A.SGCCD = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("             AND A.DMNO IS NOT NULL");
            oStringBuilder.AppendLine("             AND A.CALRGCD = '2000'");
            oStringBuilder.AppendLine("             AND SUBSTR(A.CAMIDCD, 0, 2) = '20' ");
            oStringBuilder.AppendLine("             AND A.CAMIDCD NOT IN ('2020', '2060')");
            oStringBuilder.AppendLine("             AND TO_CHAR(A.CAAPPLDT, 'RRRRMM') BETWEEN '" + strDateS + "' AND '" + strDateE + "'");
            oStringBuilder.AppendLine("             AND B.DMNO = A.DMNO");
            oStringBuilder.AppendLine("             AND B.MFTRIDN = '" + strMFTRIDN + "'");
            oStringBuilder.AppendLine("GROUP BY     TO_CHAR(A.CAAPPLDT, 'RRRR-MM')");
            oStringBuilder.AppendLine(")CA                                        ");
            oStringBuilder.AppendLine(",(                                          ");
            oStringBuilder.AppendLine("SELECT to_char(ADD_MONTHS(to_date('" + strDateS + "', 'YYYY-MM') , level -1 ),'YYYY-MM') AS DT                           ");
            oStringBuilder.AppendLine("FROM   DUAL                                                                                                              ");
            oStringBuilder.AppendLine("CONNECT BY LEVEL <= MONTHS_BETWEEN(to_date('" + strDateE + "', 'YYYYMM') , to_date('" + strDateS + "', 'YYYYMM')) + 1    ");
            oStringBuilder.AppendLine(")MON                                                                                                                     ");
            oStringBuilder.AppendLine("WHERE CA.DT(+) = MON.DT                                          ");
            oStringBuilder.AppendLine("ORDER BY     DT");            

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WI_CAINFO");

            this.uGridDetailPS2.DataSource = pDS.Tables["WI_CAINFO"];

            if (this.uGridDetailPS2.Rows.Count > 0) this.uGridDetailPS2.Rows[0].Selected = true;

            //ChartFX에 DataSet 연결
            this.cFXData2.Data.Clear();
            this.cFXData2.Series.Clear();
            this.cFXData2.DataSourceSettings.Fields.Clear();
            if (this.uGridDetailPS2.Rows.Count > 0) this.SetChartData();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        //중블록에 속한 소블록들의 민원 건수
        private void GetMinwonDetailVolumeByMediumBlock()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            string strDateS = string.Empty;
            string strDateE = string.Empty;
            string strMFTRIDN = string.Empty;
            string strLOC_NAME = string.Empty;
            string strFTR_IDN = string.Empty;

            DataSet pDS = new DataSet();

            strDateS = WQ_Function.StringToDateTime(this.udtMWDetailS3.DateTime).Substring(0, 6);
            strDateE = WQ_Function.StringToDateTime(this.udtMWDetailE3.DateTime).Substring(0, 6);
            strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock3.Text);

            int i = 0;

            oStringBuilder.Remove(0, oStringBuilder.Length);

            oStringBuilder.AppendLine("SELECT   LOC_NAME, FTR_IDN");
            oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("WHERE    PLOC_CODE = '" + strMFTRIDN + "' AND FTR_CODE = 'BZ003'");
            oStringBuilder.AppendLine("ORDER BY LOC_NAME");

            pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                oStringBuilder.Remove(0, oStringBuilder.Length);
                
                oStringBuilder.AppendLine("SELECT       TO_CHAR(TO_DATE(SUBSTR(A.MW_DATE, 0, 6), 'RRRR-MM'), 'RRRR-MM') AS 접수연월, ");
                i = 0;
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    //KHB 2014-10-14 (블록명이 특수문자가 올 수 있기 때문에 "를 추가한다)
                    strLOC_NAME = "\"" + oDRow["LOC_NAME"].ToString().Trim() + "\"";
                    strFTR_IDN = oDRow["FTR_IDN"].ToString().Trim();

                    if (i < pDS.Tables[0].Rows.Count - 1)
                    {
                        oStringBuilder.AppendLine("            (SELECT COUNT(CANO) FROM WQ_MINWON WHERE SFTR_IDN = '" + strFTR_IDN + "' AND SUBSTR(MW_DATE, 0, 6)  = SUBSTR(A.MW_DATE, 0, 6)) AS " + strLOC_NAME + ", ");
                    }
                    else
                    {
                        oStringBuilder.AppendLine("            (SELECT COUNT(CANO) FROM WQ_MINWON WHERE SFTR_IDN = '" + strFTR_IDN + "' AND SUBSTR(MW_DATE, 0, 6)  = SUBSTR(A.MW_DATE, 0, 6)) AS " + strLOC_NAME + " ");
                    }
                    i++;
                }

                oStringBuilder.AppendLine("FROM         WQ_MINWON A");
                oStringBuilder.AppendLine("WHERE        SUBSTR(A.MW_DATE, 0, 6) BETWEEN '" + strDateS + "' AND '" + strDateE + "'");
                oStringBuilder.AppendLine("GROUP BY     SUBSTR(A.MW_DATE, 0, 6)");
                oStringBuilder.AppendLine("ORDER BY     접수연월");

                pDS = WQ_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WQ_MINWON");

                this.uGridDetailPS3.DataSource = pDS.Tables["WQ_MINWON"];

                if (this.uGridDetailPS3.Rows.Count > 0) this.uGridDetailPS3.Rows[0].Selected = true;

                FormManager.SetGridStyle_PerformAutoResize(this.uGridDetailPS3);
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region - Chart Function
        /// <summary>
        /// Chart FX를 Setting 해서 Data를 연결한다.
        /// </summary>
        private void SetChartData()
        {
            double dblMax = 0;
            int i = 0;

            switch (this.uTabMW.SelectedTab.Index)
            {
                case 1:
                    //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
                    this.cFXData.Data.Clear();
                    this.cFXData.Series.Clear();
                    this.cFXData.DataSourceSettings.Fields.Clear();

                    DataTable pDT = ((DataTable)this.uGridDetailPS.DataSource);

                    this.cFXData.Data.Series = 6;
                    this.cFXData.Data.Points = pDT.Rows.Count;

                    foreach (DataRow pRow in pDT.Rows)
                    {
                        int iIndex = pDT.Rows.IndexOf(pRow);

                        this.cFXData.AxisX.Labels[iIndex] = Convert.ToDateTime(pRow["DT"]).ToShortDateString();

                        //Series 값 중 가장 큰 값을 Max로 Set
                        for (i = 1; i <= 5; i++)
                        {
                            if (dblMax < Convert.ToDouble(pRow[i].ToString()))
                            {
                                dblMax = Convert.ToDouble(pRow[i].ToString());
                            }
                        }

                        this.cFXData.Data[0, iIndex] = Convert.ToDouble(pRow["VOL_2010"]);
                        this.cFXData.Data[1, iIndex] = Convert.ToDouble(pRow["VOL_2015"]);
                        this.cFXData.Data[2, iIndex] = Convert.ToDouble(pRow["VOL_2030"]);
                        this.cFXData.Data[3, iIndex] = Convert.ToDouble(pRow["VOL_2040"]);
                        this.cFXData.Data[4, iIndex] = Convert.ToDouble(pRow["VOL_2050"]);
                        this.cFXData.Data[5, iIndex] = Convert.ToDouble(pRow["VOL_2099"]);
                    }
                    this.cFXData.AxisY.Max = dblMax;
                    this.cFXData.AxisY.Min = 0;

                    this.cFXData.Series[0].Gallery = Gallery.Bar;
                    this.cFXData.Series[1].Gallery = Gallery.Bar;
                    this.cFXData.Series[2].Gallery = Gallery.Bar;
                    this.cFXData.Series[3].Gallery = Gallery.Bar;
                    this.cFXData.Series[4].Gallery = Gallery.Bar;
                    this.cFXData.Series[5].Gallery = Gallery.Bar;
                    this.cFXData.Series[0].Text = "흑수발생";
                    this.cFXData.Series[1].Text = "적수발생";
                    this.cFXData.Series[2].Text = "이물질발생";
                    this.cFXData.Series[3].Text = "흙탕물발생";
                    this.cFXData.Series[4].Text = "악취발생";
                    this.cFXData.Series[5].Text = "수질기타";
                    break;
                case 2:
                    //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
                    this.cFXData2.Data.Clear();
                    this.cFXData2.Series.Clear();
                    this.cFXData2.DataSourceSettings.Fields.Clear();

                    DataTable pDT2 = ((DataTable)this.uGridDetailPS2.DataSource);

                    this.cFXData2.Data.Series = 5;
                    this.cFXData2.Data.Points = pDT2.Rows.Count;

                    foreach (DataRow pRow2 in pDT2.Rows)
                    {
                        int iIndex2 = pDT2.Rows.IndexOf(pRow2);

                        this.cFXData2.AxisX.Labels[iIndex2] = Convert.ToDateTime(pRow2["DT"]).ToShortDateString();

                        //Series 값 중 가장 큰 값을 Max로 Set
                        for (i = 1; i <= 5; i++)
                        {
                            if (dblMax < Convert.ToDouble(pRow2[i].ToString()))
                            {
                                dblMax = Convert.ToDouble(pRow2[i].ToString());
                            }
                        }

                        this.cFXData2.Data[0, iIndex2] = Convert.ToDouble(pRow2["VOL_2010"]);
                        this.cFXData2.Data[1, iIndex2] = Convert.ToDouble(pRow2["VOL_2015"]);
                        this.cFXData2.Data[2, iIndex2] = Convert.ToDouble(pRow2["VOL_2030"]);
                        this.cFXData2.Data[3, iIndex2] = Convert.ToDouble(pRow2["VOL_2040"]);
                        this.cFXData2.Data[4, iIndex2] = Convert.ToDouble(pRow2["VOL_2050"]);
                        this.cFXData2.Data[5, iIndex2] = Convert.ToDouble(pRow2["VOL_2099"]);
                    }
                    this.cFXData2.AxisY.Max = dblMax;
                    this.cFXData2.AxisY.Min = 0;

                    this.cFXData2.Series[0].Gallery = Gallery.Bar;
                    this.cFXData2.Series[1].Gallery = Gallery.Bar;
                    this.cFXData2.Series[2].Gallery = Gallery.Bar;
                    this.cFXData2.Series[3].Gallery = Gallery.Bar;
                    this.cFXData2.Series[4].Gallery = Gallery.Bar;
                    this.cFXData2.Series[5].Gallery = Gallery.Bar;
                    this.cFXData2.Series[0].Text = "흑수발생";
                    this.cFXData2.Series[1].Text = "적수발생";
                    this.cFXData2.Series[2].Text = "이물질발생";
                    this.cFXData2.Series[3].Text = "흙탕물발생";
                    this.cFXData2.Series[4].Text = "악취발생";
                    this.cFXData2.Series[5].Text = "수질기타";
                    break;
            }
        }

        #endregion

        #region - Map Function

        /// <summary>
        /// 맵 이동
        /// </summary>
        /// <param name="strLOC_CODE">중, 소블록 LOC_CODE</param>
        /// <param name="iType">0 : 중블록, 1 : 소블록</param>
        private void Viewmap_MinwonBlock(string strLOC_CODE, int iType)
        {
            string strFTR_IDN = string.Empty;

            IFeatureLayer pFeatureLayer = default(IFeatureLayer);

            if (iType == 0)
            {
                pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "중블록");
                strFTR_IDN = WQ_Function.GetMediumBlockFTR_IDN(strLOC_CODE);
            }
            else
            {
                pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(WQ_AppStatic.IMAP, "소블록");
                strFTR_IDN = WQ_Function.GetSmallBlockFTR_IDN(strLOC_CODE);
            }

            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + strFTR_IDN + "'");
            if (pFeature == null) return;

            WQ_AppStatic.IMAP.Extent = pFeature.Shape.Envelope;
            WQ_AppStatic.IMAP.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);
        }

        #endregion

        /// <summary>
        /// 그리드, Chart의 보이고 안보이고 셋
        /// </summary>
        private void SetViewControl()
        {
            switch (this.uTabMW.SelectedTab.Index)
            {
                case 1:
                    if (this.chkGrid.Checked == true && this.chkChart.Checked == true)
                    {
                        this.uGridDetailPS.Visible = true;
                        this.uGridDetailPS.Dock = DockStyle.Top;
                        this.cFXData.Visible = true;
                        this.cFXData.Dock = DockStyle.Top;
                    }
                    else if (this.chkGrid.Checked == true && this.chkChart.Checked == false)
                    {
                        this.uGridDetailPS.Visible = true;
                        this.uGridDetailPS.Dock = DockStyle.Fill;
                        this.cFXData.Visible = false;
                        this.cFXData.Dock = DockStyle.Top;
                    }
                    else if (this.chkGrid.Checked == false && this.chkChart.Checked == true)
                    {
                        this.uGridDetailPS.Visible = false;
                        this.uGridDetailPS.Dock = DockStyle.Top;
                        this.cFXData.Visible = true;
                        this.cFXData.Dock = DockStyle.Fill;
                    }
                    else if (this.chkGrid.Checked == false && this.chkChart.Checked == false)
                    {
                        this.chkGrid.Checked = true;
                        this.uGridDetailPS.Visible = true;
                        this.uGridDetailPS.Dock = DockStyle.Fill;
                        this.cFXData.Visible = false;
                        this.cFXData.Dock = DockStyle.Top;
                    }
                    break;
                case 2:
                    if (this.chkGrid2.Checked == true && this.chkChart2.Checked == true)
                    {
                        this.uGridDetailPS2.Visible = true;
                        this.uGridDetailPS2.Dock = DockStyle.Top;
                        this.cFXData2.Visible = true;
                        this.cFXData2.Dock = DockStyle.Top;
                    }
                    else if (this.chkGrid2.Checked == true && this.chkChart2.Checked == false)
                    {
                        this.uGridDetailPS2.Visible = true;
                        this.uGridDetailPS2.Dock = DockStyle.Fill;
                        this.cFXData2.Visible = false;
                        this.cFXData2.Dock = DockStyle.Top;
                    }
                    else if (this.chkGrid2.Checked == false && this.chkChart2.Checked == true)
                    {
                        this.uGridDetailPS2.Visible = false;
                        this.uGridDetailPS2.Dock = DockStyle.Top;
                        this.cFXData2.Visible = true;
                        this.cFXData2.Dock = DockStyle.Fill;
                    }
                    else if (this.chkGrid2.Checked == false && this.chkChart2.Checked == false)
                    {
                        this.chkGrid2.Checked = true;
                        this.uGridDetailPS2.Visible = true;
                        this.uGridDetailPS2.Dock = DockStyle.Fill;
                        this.cFXData2.Visible = false;
                        this.cFXData2.Dock = DockStyle.Top;
                    }
                    break;
            }
        }

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WQ_AppStatic.IS_SHOW_FORM_REPORT = false;
            this.Dispose();
            this.Close();
        }

        #endregion

    }
}
