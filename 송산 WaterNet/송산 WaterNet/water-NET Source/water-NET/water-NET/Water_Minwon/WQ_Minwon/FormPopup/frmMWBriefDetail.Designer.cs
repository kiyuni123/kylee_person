﻿namespace WaterNet.WQ_Minwon.FormPopup
{
    partial class frmMWBriefDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picFrRightS11 = new System.Windows.Forms.PictureBox();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.txtDEPT = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtUName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.udtPROCDT = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label17 = new System.Windows.Forms.Label();
            this.rtxtMWResult = new System.Windows.Forms.RichTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.uGridMWReg1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtPROCDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMWReg1)).BeginInit();
            this.SuspendLayout();
            // 
            // picFrRightS11
            // 
            this.picFrRightS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightS11.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightS11.Location = new System.Drawing.Point(788, 4);
            this.picFrRightS11.Name = "picFrRightS11";
            this.picFrRightS11.Size = new System.Drawing.Size(4, 243);
            this.picFrRightS11.TabIndex = 113;
            this.picFrRightS11.TabStop = false;
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 243);
            this.picFrLeft.TabIndex = 112;
            this.picFrLeft.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 247);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(792, 4);
            this.picFrBottom.TabIndex = 111;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(792, 4);
            this.picFrTop.TabIndex = 110;
            this.picFrTop.TabStop = false;
            // 
            // txtDEPT
            // 
            this.txtDEPT.Location = new System.Drawing.Point(489, 100);
            this.txtDEPT.Name = "txtDEPT";
            this.txtDEPT.ReadOnly = true;
            this.txtDEPT.Size = new System.Drawing.Size(208, 21);
            this.txtDEPT.TabIndex = 154;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(421, 104);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 12);
            this.label19.TabIndex = 153;
            this.label19.Text = "처리부서 :";
            // 
            // txtUName
            // 
            this.txtUName.Location = new System.Drawing.Point(318, 100);
            this.txtUName.Name = "txtUName";
            this.txtUName.ReadOnly = true;
            this.txtUName.Size = new System.Drawing.Size(97, 21);
            this.txtUName.TabIndex = 152;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(263, 104);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 12);
            this.label18.TabIndex = 151;
            this.label18.Text = "담당자 :";
            // 
            // udtPROCDT
            // 
            this.udtPROCDT.Location = new System.Drawing.Point(157, 100);
            this.udtPROCDT.Name = "udtPROCDT";
            this.udtPROCDT.ReadOnly = true;
            this.udtPROCDT.Size = new System.Drawing.Size(100, 21);
            this.udtPROCDT.TabIndex = 150;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(90, 104);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 12);
            this.label17.TabIndex = 149;
            this.label17.Text = "처리일자 :";
            // 
            // rtxtMWResult
            // 
            this.rtxtMWResult.Location = new System.Drawing.Point(157, 123);
            this.rtxtMWResult.MaxLength = 1300;
            this.rtxtMWResult.Name = "rtxtMWResult";
            this.rtxtMWResult.ReadOnly = true;
            this.rtxtMWResult.Size = new System.Drawing.Size(540, 122);
            this.rtxtMWResult.TabIndex = 148;
            this.rtxtMWResult.Tag = "CTS";
            this.rtxtMWResult.Text = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(90, 129);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 12);
            this.label22.TabIndex = 147;
            this.label22.Text = "처리결과 :";
            // 
            // uGridMWReg1
            // 
            this.uGridMWReg1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridMWReg1.Location = new System.Drawing.Point(4, 4);
            this.uGridMWReg1.Name = "uGridMWReg1";
            this.uGridMWReg1.Size = new System.Drawing.Size(784, 94);
            this.uGridMWReg1.TabIndex = 204;
            this.uGridMWReg1.Text = "선택된 수질민원";
            // 
            // frmMWBriefDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 251);
            this.Controls.Add(this.uGridMWReg1);
            this.Controls.Add(this.txtDEPT);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtUName);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.udtPROCDT);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.rtxtMWResult);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.picFrRightS11);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMWBriefDetail";
            this.Text = "수질 민원 지점 상세 정보";
            this.Load += new System.EventHandler(this.frmMWBriefDetail_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMWBriefDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtPROCDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMWReg1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrRightS11;
        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.TextBox txtDEPT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtUName;
        private System.Windows.Forms.Label label18;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtPROCDT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RichTextBox rtxtMWResult;
        private System.Windows.Forms.Label label22;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMWReg1;
    }
}