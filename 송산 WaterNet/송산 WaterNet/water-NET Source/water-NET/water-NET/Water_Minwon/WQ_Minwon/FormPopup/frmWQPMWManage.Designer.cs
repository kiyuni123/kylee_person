﻿namespace WaterNet.WQ_Minwon.FormPopup
{
    partial class frmWQPMWManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridBase = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboMWType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.udtMWDateE = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.udtMWDateS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label14 = new System.Windows.Forms.Label();
            this.cbMunicipality = new System.Windows.Forms.ComboBox();
            this.btnQuery1 = new System.Windows.Forms.Button();
            this.btnClose1 = new System.Windows.Forms.Button();
            this.picFrRightS11 = new System.Windows.Forms.PictureBox();
            this.picFrLeftS11 = new System.Windows.Forms.PictureBox();
            this.picFrBottomS11 = new System.Windows.Forms.PictureBox();
            this.picFrTopS11 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel15 = new System.Windows.Forms.Panel();
            this.rdoETC = new System.Windows.Forms.RadioButton();
            this.rdoPTC = new System.Windows.Forms.RadioButton();
            this.rdoTSM = new System.Windows.Forms.RadioButton();
            this.rdoCLR = new System.Windows.Forms.RadioButton();
            this.txtSFTR_IDN = new System.Windows.Forms.TextBox();
            this.txtETC99 = new System.Windows.Forms.TextBox();
            this.chkETC99 = new System.Windows.Forms.CheckBox();
            this.chkETC02 = new System.Windows.Forms.CheckBox();
            this.chkETC01 = new System.Windows.Forms.CheckBox();
            this.txtPTC99 = new System.Windows.Forms.TextBox();
            this.chkPTC99 = new System.Windows.Forms.CheckBox();
            this.chkPTC06 = new System.Windows.Forms.CheckBox();
            this.chkPTC05 = new System.Windows.Forms.CheckBox();
            this.chkPTC04 = new System.Windows.Forms.CheckBox();
            this.chkPTC03 = new System.Windows.Forms.CheckBox();
            this.chkPTC02 = new System.Windows.Forms.CheckBox();
            this.chkPTC01 = new System.Windows.Forms.CheckBox();
            this.txtTSM99 = new System.Windows.Forms.TextBox();
            this.chkTSM99 = new System.Windows.Forms.CheckBox();
            this.chkTSM06 = new System.Windows.Forms.CheckBox();
            this.chkTSM05 = new System.Windows.Forms.CheckBox();
            this.chkTSM04 = new System.Windows.Forms.CheckBox();
            this.chkTSM03 = new System.Windows.Forms.CheckBox();
            this.chkTSM02 = new System.Windows.Forms.CheckBox();
            this.chkTSM01 = new System.Windows.Forms.CheckBox();
            this.txtCLR99 = new System.Windows.Forms.TextBox();
            this.chkCLR99 = new System.Windows.Forms.CheckBox();
            this.chkCLR06 = new System.Windows.Forms.CheckBox();
            this.chkCLR05 = new System.Windows.Forms.CheckBox();
            this.chkCLR04 = new System.Windows.Forms.CheckBox();
            this.chkCLR03 = new System.Windows.Forms.CheckBox();
            this.chkCLR02 = new System.Windows.Forms.CheckBox();
            this.chkCLR01 = new System.Windows.Forms.CheckBox();
            this.rtxtMWContent = new System.Windows.Forms.RichTextBox();
            this.cboMWOccurTime = new System.Windows.Forms.ComboBox();
            this.cboMWPeriod = new System.Windows.Forms.ComboBox();
            this.cboMWSerious = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.udtMWDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtCANO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnClose2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rtxtMWResult2 = new System.Windows.Forms.RichTextBox();
            this.rtxtMWResult1 = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDEPT = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtUName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.udtPROCDT = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label17 = new System.Windows.Forms.Label();
            this.rtxtMWResult3 = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSave2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.udtMWDate2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtCANO2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnClose3 = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uTabMW = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.picFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtPROCDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMW)).BeginInit();
            this.uTabMW.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridBase);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Controls.Add(this.picFrRightS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrLeftS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrBottomS11);
            this.ultraTabPageControl1.Controls.Add(this.picFrTopS11);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(800, 354);
            // 
            // uGridBase
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridBase.DisplayLayout.Appearance = appearance1;
            this.uGridBase.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridBase.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridBase.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridBase.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridBase.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridBase.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridBase.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridBase.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridBase.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridBase.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridBase.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridBase.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridBase.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridBase.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridBase.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridBase.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridBase.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridBase.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridBase.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridBase.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridBase.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridBase.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridBase.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridBase.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridBase.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridBase.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridBase.Location = new System.Drawing.Point(4, 36);
            this.uGridBase.Name = "uGridBase";
            this.uGridBase.Size = new System.Drawing.Size(792, 314);
            this.uGridBase.TabIndex = 112;
            this.uGridBase.Text = "ultraGrid1";
            this.uGridBase.Click += new System.EventHandler(this.uGridBase_Click);
            this.uGridBase.DoubleClick += new System.EventHandler(this.uGridBase_DoubleClick);
            this.uGridBase.AfterRowActivate += new System.EventHandler(this.uGridBase_AfterRowActivate);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Gold;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox17.Location = new System.Drawing.Point(4, 32);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(792, 4);
            this.pictureBox17.TabIndex = 113;
            this.pictureBox17.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cboMWType);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.udtMWDateE);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.udtMWDateS);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.cbMunicipality);
            this.panel2.Controls.Add(this.btnQuery1);
            this.panel2.Controls.Add(this.btnClose1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(792, 28);
            this.panel2.TabIndex = 110;
            // 
            // cboMWType
            // 
            this.cboMWType.FormattingEnabled = true;
            this.cboMWType.Location = new System.Drawing.Point(556, 3);
            this.cboMWType.Name = "cboMWType";
            this.cboMWType.Size = new System.Drawing.Size(88, 20);
            this.cboMWType.TabIndex = 68;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(483, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 12);
            this.label11.TabIndex = 67;
            this.label11.Text = "민원분류 :";
            // 
            // udtMWDateE
            // 
            this.udtMWDateE.Location = new System.Drawing.Point(377, 2);
            this.udtMWDateE.Name = "udtMWDateE";
            this.udtMWDateE.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateE.TabIndex = 65;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.Location = new System.Drawing.Point(363, 8);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(15, 12);
            this.label47.TabIndex = 66;
            this.label47.Text = "~";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.Location = new System.Drawing.Point(190, 7);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 12);
            this.label44.TabIndex = 64;
            this.label44.Text = "접수일자 :";
            // 
            // udtMWDateS
            // 
            this.udtMWDateS.Location = new System.Drawing.Point(263, 2);
            this.udtMWDateS.Name = "udtMWDateS";
            this.udtMWDateS.Size = new System.Drawing.Size(100, 21);
            this.udtMWDateS.TabIndex = 63;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(6, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 12);
            this.label14.TabIndex = 60;
            this.label14.Text = "지자체 :";
            // 
            // cbMunicipality
            // 
            this.cbMunicipality.FormattingEnabled = true;
            this.cbMunicipality.Location = new System.Drawing.Point(66, 3);
            this.cbMunicipality.Name = "cbMunicipality";
            this.cbMunicipality.Size = new System.Drawing.Size(118, 20);
            this.cbMunicipality.TabIndex = 59;
            // 
            // btnQuery1
            // 
            this.btnQuery1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnQuery1.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Query;
            this.btnQuery1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery1.Location = new System.Drawing.Point(650, 0);
            this.btnQuery1.Name = "btnQuery1";
            this.btnQuery1.Size = new System.Drawing.Size(70, 26);
            this.btnQuery1.TabIndex = 5;
            this.btnQuery1.Text = "조회";
            this.btnQuery1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery1.UseVisualStyleBackColor = true;
            this.btnQuery1.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnClose1
            // 
            this.btnClose1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose1.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Close2;
            this.btnClose1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose1.Location = new System.Drawing.Point(720, 0);
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.Size = new System.Drawing.Size(70, 26);
            this.btnClose1.TabIndex = 3;
            this.btnClose1.Text = "닫기";
            this.btnClose1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose1.UseVisualStyleBackColor = true;
            this.btnClose1.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // picFrRightS11
            // 
            this.picFrRightS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRightS11.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRightS11.Location = new System.Drawing.Point(796, 4);
            this.picFrRightS11.Name = "picFrRightS11";
            this.picFrRightS11.Size = new System.Drawing.Size(4, 346);
            this.picFrRightS11.TabIndex = 109;
            this.picFrRightS11.TabStop = false;
            // 
            // picFrLeftS11
            // 
            this.picFrLeftS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftS11.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftS11.Location = new System.Drawing.Point(0, 4);
            this.picFrLeftS11.Name = "picFrLeftS11";
            this.picFrLeftS11.Size = new System.Drawing.Size(4, 346);
            this.picFrLeftS11.TabIndex = 108;
            this.picFrLeftS11.TabStop = false;
            // 
            // picFrBottomS11
            // 
            this.picFrBottomS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottomS11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottomS11.Location = new System.Drawing.Point(0, 350);
            this.picFrBottomS11.Name = "picFrBottomS11";
            this.picFrBottomS11.Size = new System.Drawing.Size(800, 4);
            this.picFrBottomS11.TabIndex = 107;
            this.picFrBottomS11.TabStop = false;
            // 
            // picFrTopS11
            // 
            this.picFrTopS11.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTopS11.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTopS11.Location = new System.Drawing.Point(0, 0);
            this.picFrTopS11.Name = "picFrTopS11";
            this.picFrTopS11.Size = new System.Drawing.Size(800, 4);
            this.picFrTopS11.TabIndex = 106;
            this.picFrTopS11.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.panel15);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox10);
            this.ultraTabPageControl2.Controls.Add(this.panel9);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox1);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox2);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox3);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox4);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(800, 354);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.Control;
            this.panel15.Controls.Add(this.rdoETC);
            this.panel15.Controls.Add(this.rdoPTC);
            this.panel15.Controls.Add(this.rdoTSM);
            this.panel15.Controls.Add(this.rdoCLR);
            this.panel15.Controls.Add(this.txtSFTR_IDN);
            this.panel15.Controls.Add(this.txtETC99);
            this.panel15.Controls.Add(this.chkETC99);
            this.panel15.Controls.Add(this.chkETC02);
            this.panel15.Controls.Add(this.chkETC01);
            this.panel15.Controls.Add(this.txtPTC99);
            this.panel15.Controls.Add(this.chkPTC99);
            this.panel15.Controls.Add(this.chkPTC06);
            this.panel15.Controls.Add(this.chkPTC05);
            this.panel15.Controls.Add(this.chkPTC04);
            this.panel15.Controls.Add(this.chkPTC03);
            this.panel15.Controls.Add(this.chkPTC02);
            this.panel15.Controls.Add(this.chkPTC01);
            this.panel15.Controls.Add(this.txtTSM99);
            this.panel15.Controls.Add(this.chkTSM99);
            this.panel15.Controls.Add(this.chkTSM06);
            this.panel15.Controls.Add(this.chkTSM05);
            this.panel15.Controls.Add(this.chkTSM04);
            this.panel15.Controls.Add(this.chkTSM03);
            this.panel15.Controls.Add(this.chkTSM02);
            this.panel15.Controls.Add(this.chkTSM01);
            this.panel15.Controls.Add(this.txtCLR99);
            this.panel15.Controls.Add(this.chkCLR99);
            this.panel15.Controls.Add(this.chkCLR06);
            this.panel15.Controls.Add(this.chkCLR05);
            this.panel15.Controls.Add(this.chkCLR04);
            this.panel15.Controls.Add(this.chkCLR03);
            this.panel15.Controls.Add(this.chkCLR02);
            this.panel15.Controls.Add(this.chkCLR01);
            this.panel15.Controls.Add(this.rtxtMWContent);
            this.panel15.Controls.Add(this.cboMWOccurTime);
            this.panel15.Controls.Add(this.cboMWPeriod);
            this.panel15.Controls.Add(this.cboMWSerious);
            this.panel15.Controls.Add(this.label8);
            this.panel15.Controls.Add(this.label7);
            this.panel15.Controls.Add(this.label4);
            this.panel15.Controls.Add(this.label3);
            this.panel15.Controls.Add(this.pictureBox15);
            this.panel15.Controls.Add(this.pictureBox14);
            this.panel15.Controls.Add(this.pictureBox13);
            this.panel15.Controls.Add(this.pictureBox12);
            this.panel15.Controls.Add(this.pictureBox11);
            this.panel15.Controls.Add(this.pictureBox5);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(4, 36);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(792, 314);
            this.panel15.TabIndex = 121;
            // 
            // rdoETC
            // 
            this.rdoETC.AutoSize = true;
            this.rdoETC.Location = new System.Drawing.Point(11, 102);
            this.rdoETC.Name = "rdoETC";
            this.rdoETC.Size = new System.Drawing.Size(47, 16);
            this.rdoETC.TabIndex = 112;
            this.rdoETC.TabStop = true;
            this.rdoETC.Tag = "ETC";
            this.rdoETC.Text = "기타";
            this.rdoETC.UseVisualStyleBackColor = true;
            // 
            // rdoPTC
            // 
            this.rdoPTC.AutoSize = true;
            this.rdoPTC.Location = new System.Drawing.Point(11, 69);
            this.rdoPTC.Name = "rdoPTC";
            this.rdoPTC.Size = new System.Drawing.Size(47, 16);
            this.rdoPTC.TabIndex = 111;
            this.rdoPTC.TabStop = true;
            this.rdoPTC.Tag = "PTC";
            this.rdoPTC.Text = "입자";
            this.rdoPTC.UseVisualStyleBackColor = true;
            // 
            // rdoTSM
            // 
            this.rdoTSM.AutoSize = true;
            this.rdoTSM.Location = new System.Drawing.Point(11, 37);
            this.rdoTSM.Name = "rdoTSM";
            this.rdoTSM.Size = new System.Drawing.Size(65, 16);
            this.rdoTSM.TabIndex = 110;
            this.rdoTSM.TabStop = true;
            this.rdoTSM.Tag = "TSM";
            this.rdoTSM.Text = "맛/냄새";
            this.rdoTSM.UseVisualStyleBackColor = true;
            // 
            // rdoCLR
            // 
            this.rdoCLR.AutoSize = true;
            this.rdoCLR.Location = new System.Drawing.Point(11, 6);
            this.rdoCLR.Name = "rdoCLR";
            this.rdoCLR.Size = new System.Drawing.Size(35, 16);
            this.rdoCLR.TabIndex = 109;
            this.rdoCLR.TabStop = true;
            this.rdoCLR.Tag = "CLR";
            this.rdoCLR.Text = "색";
            this.rdoCLR.UseVisualStyleBackColor = true;
            // 
            // txtSFTR_IDN
            // 
            this.txtSFTR_IDN.BackColor = System.Drawing.SystemColors.Control;
            this.txtSFTR_IDN.Location = new System.Drawing.Point(358, 163);
            this.txtSFTR_IDN.Name = "txtSFTR_IDN";
            this.txtSFTR_IDN.ReadOnly = true;
            this.txtSFTR_IDN.Size = new System.Drawing.Size(99, 21);
            this.txtSFTR_IDN.TabIndex = 49;
            this.txtSFTR_IDN.Visible = false;
            // 
            // txtETC99
            // 
            this.txtETC99.Location = new System.Drawing.Point(592, 99);
            this.txtETC99.Name = "txtETC99";
            this.txtETC99.Size = new System.Drawing.Size(194, 21);
            this.txtETC99.TabIndex = 48;
            // 
            // chkETC99
            // 
            this.chkETC99.AutoSize = true;
            this.chkETC99.Location = new System.Drawing.Point(536, 101);
            this.chkETC99.Name = "chkETC99";
            this.chkETC99.Size = new System.Drawing.Size(56, 16);
            this.chkETC99.TabIndex = 47;
            this.chkETC99.Text = "기타 :";
            this.chkETC99.UseVisualStyleBackColor = true;
            // 
            // chkETC02
            // 
            this.chkETC02.AutoSize = true;
            this.chkETC02.Location = new System.Drawing.Point(197, 102);
            this.chkETC02.Name = "chkETC02";
            this.chkETC02.Size = new System.Drawing.Size(88, 16);
            this.chkETC02.TabIndex = 46;
            this.chkETC02.Text = "미생물 검출";
            this.chkETC02.UseVisualStyleBackColor = true;
            // 
            // chkETC01
            // 
            this.chkETC01.AutoSize = true;
            this.chkETC01.Location = new System.Drawing.Point(95, 102);
            this.chkETC01.Name = "chkETC01";
            this.chkETC01.Size = new System.Drawing.Size(96, 16);
            this.chkETC01.TabIndex = 45;
            this.chkETC01.Text = "염소농도미달";
            this.chkETC01.UseVisualStyleBackColor = true;
            // 
            // txtPTC99
            // 
            this.txtPTC99.Location = new System.Drawing.Point(592, 67);
            this.txtPTC99.Name = "txtPTC99";
            this.txtPTC99.Size = new System.Drawing.Size(194, 21);
            this.txtPTC99.TabIndex = 43;
            // 
            // chkPTC99
            // 
            this.chkPTC99.AutoSize = true;
            this.chkPTC99.Location = new System.Drawing.Point(536, 69);
            this.chkPTC99.Name = "chkPTC99";
            this.chkPTC99.Size = new System.Drawing.Size(56, 16);
            this.chkPTC99.TabIndex = 42;
            this.chkPTC99.Text = "기타 :";
            this.chkPTC99.UseVisualStyleBackColor = true;
            // 
            // chkPTC06
            // 
            this.chkPTC06.AutoSize = true;
            this.chkPTC06.Location = new System.Drawing.Point(413, 69);
            this.chkPTC06.Name = "chkPTC06";
            this.chkPTC06.Size = new System.Drawing.Size(118, 16);
            this.chkPTC06.TabIndex = 41;
            this.chkPTC06.Text = "흰색알갱이(곡류)";
            this.chkPTC06.UseVisualStyleBackColor = true;
            // 
            // chkPTC05
            // 
            this.chkPTC05.AutoSize = true;
            this.chkPTC05.Location = new System.Drawing.Point(311, 69);
            this.chkPTC05.Name = "chkPTC05";
            this.chkPTC05.Size = new System.Drawing.Size(96, 16);
            this.chkPTC05.TabIndex = 40;
            this.chkPTC05.Text = "흰색합성수지";
            this.chkPTC05.UseVisualStyleBackColor = true;
            // 
            // chkPTC04
            // 
            this.chkPTC04.AutoSize = true;
            this.chkPTC04.Location = new System.Drawing.Point(257, 69);
            this.chkPTC04.Name = "chkPTC04";
            this.chkPTC04.Size = new System.Drawing.Size(48, 16);
            this.chkPTC04.TabIndex = 39;
            this.chkPTC04.Text = "조각";
            this.chkPTC04.UseVisualStyleBackColor = true;
            // 
            // chkPTC03
            // 
            this.chkPTC03.AutoSize = true;
            this.chkPTC03.Location = new System.Drawing.Point(191, 69);
            this.chkPTC03.Name = "chkPTC03";
            this.chkPTC03.Size = new System.Drawing.Size(60, 16);
            this.chkPTC03.TabIndex = 38;
            this.chkPTC03.Text = "금속류";
            this.chkPTC03.UseVisualStyleBackColor = true;
            // 
            // chkPTC02
            // 
            this.chkPTC02.AutoSize = true;
            this.chkPTC02.Location = new System.Drawing.Point(137, 69);
            this.chkPTC02.Name = "chkPTC02";
            this.chkPTC02.Size = new System.Drawing.Size(48, 16);
            this.chkPTC02.TabIndex = 37;
            this.chkPTC02.Text = "분진";
            this.chkPTC02.UseVisualStyleBackColor = true;
            // 
            // chkPTC01
            // 
            this.chkPTC01.AutoSize = true;
            this.chkPTC01.Location = new System.Drawing.Point(95, 69);
            this.chkPTC01.Name = "chkPTC01";
            this.chkPTC01.Size = new System.Drawing.Size(36, 16);
            this.chkPTC01.TabIndex = 36;
            this.chkPTC01.Text = "녹";
            this.chkPTC01.UseVisualStyleBackColor = true;
            // 
            // txtTSM99
            // 
            this.txtTSM99.Location = new System.Drawing.Point(592, 35);
            this.txtTSM99.Name = "txtTSM99";
            this.txtTSM99.Size = new System.Drawing.Size(194, 21);
            this.txtTSM99.TabIndex = 34;
            // 
            // chkTSM99
            // 
            this.chkTSM99.AutoSize = true;
            this.chkTSM99.Location = new System.Drawing.Point(536, 37);
            this.chkTSM99.Name = "chkTSM99";
            this.chkTSM99.Size = new System.Drawing.Size(56, 16);
            this.chkTSM99.TabIndex = 33;
            this.chkTSM99.Text = "기타 :";
            this.chkTSM99.UseVisualStyleBackColor = true;
            // 
            // chkTSM06
            // 
            this.chkTSM06.AutoSize = true;
            this.chkTSM06.Location = new System.Drawing.Point(425, 37);
            this.chkTSM06.Name = "chkTSM06";
            this.chkTSM06.Size = new System.Drawing.Size(58, 16);
            this.chkTSM06.TabIndex = 32;
            this.chkTSM06.Text = "늪(흙)";
            this.chkTSM06.UseVisualStyleBackColor = true;
            // 
            // chkTSM05
            // 
            this.chkTSM05.AutoSize = true;
            this.chkTSM05.Location = new System.Drawing.Point(359, 37);
            this.chkTSM05.Name = "chkTSM05";
            this.chkTSM05.Size = new System.Drawing.Size(60, 16);
            this.chkTSM05.TabIndex = 31;
            this.chkTSM05.Text = "곰팡이";
            this.chkTSM05.UseVisualStyleBackColor = true;
            // 
            // chkTSM04
            // 
            this.chkTSM04.AutoSize = true;
            this.chkTSM04.Location = new System.Drawing.Point(293, 37);
            this.chkTSM04.Name = "chkTSM04";
            this.chkTSM04.Size = new System.Drawing.Size(60, 16);
            this.chkTSM04.TabIndex = 30;
            this.chkTSM04.Text = "황화물";
            this.chkTSM04.UseVisualStyleBackColor = true;
            // 
            // chkTSM03
            // 
            this.chkTSM03.AutoSize = true;
            this.chkTSM03.Location = new System.Drawing.Point(227, 37);
            this.chkTSM03.Name = "chkTSM03";
            this.chkTSM03.Size = new System.Drawing.Size(60, 16);
            this.chkTSM03.TabIndex = 29;
            this.chkTSM03.Text = "금속류";
            this.chkTSM03.UseVisualStyleBackColor = true;
            // 
            // chkTSM02
            // 
            this.chkTSM02.AutoSize = true;
            this.chkTSM02.Location = new System.Drawing.Point(161, 37);
            this.chkTSM02.Name = "chkTSM02";
            this.chkTSM02.Size = new System.Drawing.Size(60, 16);
            this.chkTSM02.TabIndex = 28;
            this.chkTSM02.Text = "약품류";
            this.chkTSM02.UseVisualStyleBackColor = true;
            // 
            // chkTSM01
            // 
            this.chkTSM01.AutoSize = true;
            this.chkTSM01.Location = new System.Drawing.Point(95, 37);
            this.chkTSM01.Name = "chkTSM01";
            this.chkTSM01.Size = new System.Drawing.Size(60, 16);
            this.chkTSM01.TabIndex = 27;
            this.chkTSM01.Text = "소독제";
            this.chkTSM01.UseVisualStyleBackColor = true;
            // 
            // txtCLR99
            // 
            this.txtCLR99.Location = new System.Drawing.Point(592, 4);
            this.txtCLR99.Name = "txtCLR99";
            this.txtCLR99.Size = new System.Drawing.Size(194, 21);
            this.txtCLR99.TabIndex = 25;
            // 
            // chkCLR99
            // 
            this.chkCLR99.AutoSize = true;
            this.chkCLR99.Location = new System.Drawing.Point(536, 6);
            this.chkCLR99.Name = "chkCLR99";
            this.chkCLR99.Size = new System.Drawing.Size(56, 16);
            this.chkCLR99.TabIndex = 24;
            this.chkCLR99.Text = "기타 :";
            this.chkCLR99.UseVisualStyleBackColor = true;
            // 
            // chkCLR06
            // 
            this.chkCLR06.AutoSize = true;
            this.chkCLR06.Location = new System.Drawing.Point(431, 6);
            this.chkCLR06.Name = "chkCLR06";
            this.chkCLR06.Size = new System.Drawing.Size(48, 16);
            this.chkCLR06.TabIndex = 23;
            this.chkCLR06.Text = "회색";
            this.chkCLR06.UseVisualStyleBackColor = true;
            // 
            // chkCLR05
            // 
            this.chkCLR05.AutoSize = true;
            this.chkCLR05.Location = new System.Drawing.Point(377, 6);
            this.chkCLR05.Name = "chkCLR05";
            this.chkCLR05.Size = new System.Drawing.Size(48, 16);
            this.chkCLR05.TabIndex = 22;
            this.chkCLR05.Text = "흰색";
            this.chkCLR05.UseVisualStyleBackColor = true;
            // 
            // chkCLR04
            // 
            this.chkCLR04.AutoSize = true;
            this.chkCLR04.Location = new System.Drawing.Point(269, 6);
            this.chkCLR04.Name = "chkCLR04";
            this.chkCLR04.Size = new System.Drawing.Size(102, 16);
            this.chkCLR04.TabIndex = 21;
            this.chkCLR04.Text = "노란/오렌지색";
            this.chkCLR04.UseVisualStyleBackColor = true;
            // 
            // chkCLR03
            // 
            this.chkCLR03.AutoSize = true;
            this.chkCLR03.Location = new System.Drawing.Point(215, 6);
            this.chkCLR03.Name = "chkCLR03";
            this.chkCLR03.Size = new System.Drawing.Size(48, 16);
            this.chkCLR03.TabIndex = 20;
            this.chkCLR03.Text = "갈색";
            this.chkCLR03.UseVisualStyleBackColor = true;
            // 
            // chkCLR02
            // 
            this.chkCLR02.AutoSize = true;
            this.chkCLR02.Location = new System.Drawing.Point(149, 6);
            this.chkCLR02.Name = "chkCLR02";
            this.chkCLR02.Size = new System.Drawing.Size(60, 16);
            this.chkCLR02.TabIndex = 19;
            this.chkCLR02.Text = "검정색";
            this.chkCLR02.UseVisualStyleBackColor = true;
            // 
            // chkCLR01
            // 
            this.chkCLR01.AutoSize = true;
            this.chkCLR01.Location = new System.Drawing.Point(95, 6);
            this.chkCLR01.Name = "chkCLR01";
            this.chkCLR01.Size = new System.Drawing.Size(48, 16);
            this.chkCLR01.TabIndex = 18;
            this.chkCLR01.Text = "청색";
            this.chkCLR01.UseVisualStyleBackColor = true;
            // 
            // rtxtMWContent
            // 
            this.rtxtMWContent.Location = new System.Drawing.Point(5, 186);
            this.rtxtMWContent.MaxLength = 500;
            this.rtxtMWContent.Name = "rtxtMWContent";
            this.rtxtMWContent.Size = new System.Drawing.Size(780, 122);
            this.rtxtMWContent.TabIndex = 16;
            this.rtxtMWContent.Tag = "CTS";
            this.rtxtMWContent.Text = "";
            // 
            // cboMWOccurTime
            // 
            this.cboMWOccurTime.FormattingEnabled = true;
            this.cboMWOccurTime.Location = new System.Drawing.Point(463, 132);
            this.cboMWOccurTime.Name = "cboMWOccurTime";
            this.cboMWOccurTime.Size = new System.Drawing.Size(140, 20);
            this.cboMWOccurTime.TabIndex = 15;
            this.cboMWOccurTime.Tag = "OTM";
            // 
            // cboMWPeriod
            // 
            this.cboMWPeriod.FormattingEnabled = true;
            this.cboMWPeriod.Location = new System.Drawing.Point(250, 132);
            this.cboMWPeriod.Name = "cboMWPeriod";
            this.cboMWPeriod.Size = new System.Drawing.Size(140, 20);
            this.cboMWPeriod.TabIndex = 14;
            this.cboMWPeriod.Tag = "PRD";
            // 
            // cboMWSerious
            // 
            this.cboMWSerious.FormattingEnabled = true;
            this.cboMWSerious.Location = new System.Drawing.Point(61, 132);
            this.cboMWSerious.Name = "cboMWSerious";
            this.cboMWSerious.Size = new System.Drawing.Size(140, 20);
            this.cboMWSerious.TabIndex = 13;
            this.cboMWSerious.Tag = "CTC";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "민원내용";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(396, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "발생시기 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "주기 :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "심각도 :";
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.RoyalBlue;
            this.pictureBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox15.Location = new System.Drawing.Point(5, 156);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(782, 4);
            this.pictureBox15.TabIndex = 4;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox14.Location = new System.Drawing.Point(5, 124);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(782, 4);
            this.pictureBox14.TabIndex = 3;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox13.Location = new System.Drawing.Point(5, 92);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(782, 4);
            this.pictureBox13.TabIndex = 2;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox12.Location = new System.Drawing.Point(5, 60);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(782, 4);
            this.pictureBox12.TabIndex = 1;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox11.Location = new System.Drawing.Point(5, 28);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(782, 4);
            this.pictureBox11.TabIndex = 0;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Location = new System.Drawing.Point(79, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(4, 126);
            this.pictureBox5.TabIndex = 17;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Gold;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox10.Location = new System.Drawing.Point(4, 32);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(792, 4);
            this.pictureBox10.TabIndex = 114;
            this.pictureBox10.TabStop = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnSave);
            this.panel9.Controls.Add(this.btnDelete);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.udtMWDate);
            this.panel9.Controls.Add(this.txtCANO);
            this.panel9.Controls.Add(this.label2);
            this.panel9.Controls.Add(this.label5);
            this.panel9.Controls.Add(this.btnClose2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(4, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(792, 28);
            this.panel9.TabIndex = 111;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Save;
            this.btnSave.Location = new System.Drawing.Point(580, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 26);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "저장";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDelete.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Delete;
            this.btnDelete.Location = new System.Drawing.Point(650, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 26);
            this.btnDelete.TabIndex = 67;
            this.btnDelete.Text = "삭제";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(320, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 12);
            this.label9.TabIndex = 66;
            this.label9.Text = "접수일자 :";
            // 
            // udtMWDate
            // 
            this.udtMWDate.Location = new System.Drawing.Point(393, 2);
            this.udtMWDate.Name = "udtMWDate";
            this.udtMWDate.ReadOnly = true;
            this.udtMWDate.Size = new System.Drawing.Size(100, 21);
            this.udtMWDate.TabIndex = 65;
            // 
            // txtCANO
            // 
            this.txtCANO.BackColor = System.Drawing.SystemColors.Control;
            this.txtCANO.Location = new System.Drawing.Point(217, 2);
            this.txtCANO.Name = "txtCANO";
            this.txtCANO.ReadOnly = true;
            this.txtCANO.Size = new System.Drawing.Size(99, 21);
            this.txtCANO.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(146, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "민원번호 : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(3, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "민원 상세정보 입력";
            // 
            // btnClose2
            // 
            this.btnClose2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose2.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Close2;
            this.btnClose2.Location = new System.Drawing.Point(720, 0);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.Size = new System.Drawing.Size(70, 26);
            this.btnClose2.TabIndex = 3;
            this.btnClose2.Text = "닫기";
            this.btnClose2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose2.UseVisualStyleBackColor = true;
            this.btnClose2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(796, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(4, 346);
            this.pictureBox1.TabIndex = 109;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(4, 346);
            this.pictureBox2.TabIndex = 108;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox3.Location = new System.Drawing.Point(0, 350);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(800, 4);
            this.pictureBox3.TabIndex = 107;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(800, 4);
            this.pictureBox4.TabIndex = 106;
            this.pictureBox4.TabStop = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.panel4);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox16);
            this.ultraTabPageControl3.Controls.Add(this.panel3);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox7);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox8);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox9);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(800, 354);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.rtxtMWResult2);
            this.panel4.Controls.Add(this.rtxtMWResult1);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.txtDEPT);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.txtUName);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.udtPROCDT);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.rtxtMWResult3);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(4, 36);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(792, 314);
            this.panel4.TabIndex = 117;
            // 
            // rtxtMWResult2
            // 
            this.rtxtMWResult2.Location = new System.Drawing.Point(74, 81);
            this.rtxtMWResult2.MaxLength = 300;
            this.rtxtMWResult2.Name = "rtxtMWResult2";
            this.rtxtMWResult2.Size = new System.Drawing.Size(712, 50);
            this.rtxtMWResult2.TabIndex = 72;
            this.rtxtMWResult2.Tag = "CTS";
            this.rtxtMWResult2.Text = "";
            // 
            // rtxtMWResult1
            // 
            this.rtxtMWResult1.Location = new System.Drawing.Point(74, 28);
            this.rtxtMWResult1.MaxLength = 300;
            this.rtxtMWResult1.Name = "rtxtMWResult1";
            this.rtxtMWResult1.Size = new System.Drawing.Size(712, 50);
            this.rtxtMWResult1.TabIndex = 71;
            this.rtxtMWResult1.Tag = "CTS";
            this.rtxtMWResult1.Text = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 12);
            this.label21.TabIndex = 70;
            this.label21.Text = "조사의견 :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 32);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 12);
            this.label20.TabIndex = 69;
            this.label20.Text = "원인분석 :";
            // 
            // txtDEPT
            // 
            this.txtDEPT.Location = new System.Drawing.Point(402, 4);
            this.txtDEPT.Name = "txtDEPT";
            this.txtDEPT.Size = new System.Drawing.Size(190, 21);
            this.txtDEPT.TabIndex = 68;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(338, 8);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 12);
            this.label19.TabIndex = 67;
            this.label19.Text = "처리부서 :";
            // 
            // txtUName
            // 
            this.txtUName.Location = new System.Drawing.Point(235, 4);
            this.txtUName.Name = "txtUName";
            this.txtUName.Size = new System.Drawing.Size(97, 21);
            this.txtUName.TabIndex = 66;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(180, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 12);
            this.label18.TabIndex = 65;
            this.label18.Text = "담당자 :";
            // 
            // udtPROCDT
            // 
            this.udtPROCDT.Location = new System.Drawing.Point(74, 4);
            this.udtPROCDT.Name = "udtPROCDT";
            this.udtPROCDT.Size = new System.Drawing.Size(100, 21);
            this.udtPROCDT.TabIndex = 64;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 12);
            this.label17.TabIndex = 19;
            this.label17.Text = "처리일자 :";
            // 
            // rtxtMWResult3
            // 
            this.rtxtMWResult3.Location = new System.Drawing.Point(74, 135);
            this.rtxtMWResult3.MaxLength = 1300;
            this.rtxtMWResult3.Name = "rtxtMWResult3";
            this.rtxtMWResult3.Size = new System.Drawing.Size(712, 173);
            this.rtxtMWResult3.TabIndex = 18;
            this.rtxtMWResult3.Tag = "CTS";
            this.rtxtMWResult3.Text = "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 138);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 12);
            this.label16.TabIndex = 17;
            this.label16.Text = "처리결과 :";
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.Gold;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox16.Location = new System.Drawing.Point(4, 32);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(792, 4);
            this.pictureBox16.TabIndex = 116;
            this.pictureBox16.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnSave2);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.udtMWDate2);
            this.panel3.Controls.Add(this.txtCANO2);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.btnClose3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(792, 28);
            this.panel3.TabIndex = 115;
            // 
            // btnSave2
            // 
            this.btnSave2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave2.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Save;
            this.btnSave2.Location = new System.Drawing.Point(650, 0);
            this.btnSave2.Name = "btnSave2";
            this.btnSave2.Size = new System.Drawing.Size(70, 26);
            this.btnSave2.TabIndex = 5;
            this.btnSave2.Text = "저장";
            this.btnSave2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave2.UseVisualStyleBackColor = true;
            this.btnSave2.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(320, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 12);
            this.label12.TabIndex = 66;
            this.label12.Text = "접수일자 :";
            // 
            // udtMWDate2
            // 
            this.udtMWDate2.Location = new System.Drawing.Point(393, 2);
            this.udtMWDate2.Name = "udtMWDate2";
            this.udtMWDate2.ReadOnly = true;
            this.udtMWDate2.Size = new System.Drawing.Size(100, 21);
            this.udtMWDate2.TabIndex = 65;
            // 
            // txtCANO2
            // 
            this.txtCANO2.BackColor = System.Drawing.SystemColors.Control;
            this.txtCANO2.Location = new System.Drawing.Point(217, 2);
            this.txtCANO2.Name = "txtCANO2";
            this.txtCANO2.ReadOnly = true;
            this.txtCANO2.Size = new System.Drawing.Size(99, 21);
            this.txtCANO2.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(146, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 12);
            this.label13.TabIndex = 9;
            this.label13.Text = "민원번호 : ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(3, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 12);
            this.label15.TabIndex = 7;
            this.label15.Text = "민원 처리결과 입력";
            // 
            // btnClose3
            // 
            this.btnClose3.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose3.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Close2;
            this.btnClose3.Location = new System.Drawing.Point(720, 0);
            this.btnClose3.Name = "btnClose3";
            this.btnClose3.Size = new System.Drawing.Size(70, 26);
            this.btnClose3.TabIndex = 3;
            this.btnClose3.Text = "닫기";
            this.btnClose3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose3.UseVisualStyleBackColor = true;
            this.btnClose3.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox6.Location = new System.Drawing.Point(796, 4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(4, 346);
            this.pictureBox6.TabIndex = 113;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(0, 4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(4, 346);
            this.pictureBox7.TabIndex = 112;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox8.Location = new System.Drawing.Point(0, 350);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(800, 4);
            this.pictureBox8.TabIndex = 111;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(0, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(800, 4);
            this.pictureBox9.TabIndex = 110;
            this.pictureBox9.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.uTabMW);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 380);
            this.panel1.TabIndex = 108;
            // 
            // uTabMW
            // 
            this.uTabMW.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabMW.Controls.Add(this.ultraTabPageControl1);
            this.uTabMW.Controls.Add(this.ultraTabPageControl2);
            this.uTabMW.Controls.Add(this.ultraTabPageControl3);
            this.uTabMW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabMW.Location = new System.Drawing.Point(0, 0);
            this.uTabMW.Name = "uTabMW";
            this.uTabMW.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabMW.Size = new System.Drawing.Size(804, 380);
            this.uTabMW.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "민원기본정보조회";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "민원상세정보관리";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "민원처리결과관리";
            this.uTabMW.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3});
            this.uTabMW.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.uTabMW_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(800, 354);
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 380);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // picFrRight
            // 
            this.picFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.picFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.picFrRight.Location = new System.Drawing.Point(808, 4);
            this.picFrRight.Name = "picFrRight";
            this.picFrRight.Size = new System.Drawing.Size(2, 380);
            this.picFrRight.TabIndex = 107;
            this.picFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 384);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(810, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(810, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Save;
            this.button1.Location = new System.Drawing.Point(580, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 26);
            this.button1.TabIndex = 5;
            this.button1.Text = "저장";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Delete;
            this.button2.Location = new System.Drawing.Point(650, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 26);
            this.button2.TabIndex = 67;
            this.button2.Text = "삭제";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(320, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 12);
            this.label1.TabIndex = 66;
            this.label1.Text = "접수일자 :";
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(393, 2);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.ReadOnly = true;
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 65;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.Location = new System.Drawing.Point(217, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(99, 21);
            this.textBox1.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(146, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "민원번호 : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(3, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 12);
            this.label10.TabIndex = 7;
            this.label10.Text = "민원 상세정보 입력";
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Right;
            this.button3.Image = global::WaterNet.WQ_Minwon.Properties.Resources.Close2;
            this.button3.Location = new System.Drawing.Point(720, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(70, 26);
            this.button3.TabIndex = 3;
            this.button3.Text = "닫기";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // frmWQPMWManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 388);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.picFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWQPMWManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "수질민원상세정보관리(운영자 지점 선정 방식)";
            this.Load += new System.EventHandler(this.frmWQPMWManage_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDateS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRightS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottomS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTopS11)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtPROCDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udtMWDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabMW)).EndInit();
            this.uTabMW.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox picFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMW;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.PictureBox picFrRightS11;
        private System.Windows.Forms.PictureBox picFrLeftS11;
        private System.Windows.Forms.PictureBox picFrBottomS11;
        private System.Windows.Forms.PictureBox picFrTopS11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery1;
        private System.Windows.Forms.Button btnClose1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridBase;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose2;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbMunicipality;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateE;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDateS;
        private System.Windows.Forms.TextBox txtCANO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboMWSerious;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboMWOccurTime;
        private System.Windows.Forms.ComboBox cboMWPeriod;
        private System.Windows.Forms.RichTextBox rtxtMWContent;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.CheckBox chkCLR99;
        private System.Windows.Forms.CheckBox chkCLR06;
        private System.Windows.Forms.CheckBox chkCLR05;
        private System.Windows.Forms.CheckBox chkCLR04;
        private System.Windows.Forms.CheckBox chkCLR03;
        private System.Windows.Forms.CheckBox chkCLR02;
        private System.Windows.Forms.CheckBox chkCLR01;
        private System.Windows.Forms.TextBox txtCLR99;
        private System.Windows.Forms.TextBox txtTSM99;
        private System.Windows.Forms.CheckBox chkTSM99;
        private System.Windows.Forms.CheckBox chkTSM06;
        private System.Windows.Forms.CheckBox chkTSM05;
        private System.Windows.Forms.CheckBox chkTSM04;
        private System.Windows.Forms.CheckBox chkTSM03;
        private System.Windows.Forms.CheckBox chkTSM02;
        private System.Windows.Forms.CheckBox chkTSM01;
        private System.Windows.Forms.TextBox txtPTC99;
        private System.Windows.Forms.CheckBox chkPTC99;
        private System.Windows.Forms.CheckBox chkPTC06;
        private System.Windows.Forms.CheckBox chkPTC05;
        private System.Windows.Forms.CheckBox chkPTC04;
        private System.Windows.Forms.CheckBox chkPTC03;
        private System.Windows.Forms.CheckBox chkPTC02;
        private System.Windows.Forms.CheckBox chkPTC01;
        private System.Windows.Forms.CheckBox chkETC02;
        private System.Windows.Forms.CheckBox chkETC01;
        private System.Windows.Forms.TextBox txtETC99;
        private System.Windows.Forms.CheckBox chkETC99;
        private System.Windows.Forms.Label label9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDate;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox txtSFTR_IDN;
        private System.Windows.Forms.RadioButton rdoCLR;
        private System.Windows.Forms.RadioButton rdoETC;
        private System.Windows.Forms.RadioButton rdoPTC;
        private System.Windows.Forms.RadioButton rdoTSM;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboMWType;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSave2;
        private System.Windows.Forms.Label label12;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtMWDate2;
        private System.Windows.Forms.TextBox txtCANO2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnClose3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RichTextBox rtxtMWResult3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtUName;
        private System.Windows.Forms.Label label18;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udtPROCDT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDEPT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RichTextBox rtxtMWResult2;
        private System.Windows.Forms.RichTextBox rtxtMWResult1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;

    }
}