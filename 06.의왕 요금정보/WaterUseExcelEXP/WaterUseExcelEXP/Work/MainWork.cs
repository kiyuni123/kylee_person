﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterUseExcelEXP.Dao;

namespace WaterUseExcelEXP.Work
{
    class MainWork
    {
        MainDao dao = new MainDao();

        /// <summary>
        /// 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_Main(Hashtable conditions)
        {
            return dao.Select_Main(conditions);
        }
    }
}
