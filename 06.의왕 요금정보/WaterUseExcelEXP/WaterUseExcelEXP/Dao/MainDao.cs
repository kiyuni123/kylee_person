﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterUseExcelEXP.Dao
{
    class MainDao
    {
        /// <summary>
        /// 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_Main(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_Main", conditions);
        }
    }
}
