﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Threading;
using WaterUseExcelEXP.Work;

namespace WaterUseExcelEXP
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWork work = new MainWork();
        public static NotifyIcon Notify;

        Thread threadAuto;              //자동쓰레드
        System.Timers.Timer timer;      //자동타이머
        Hashtable htAutoconditions = new Hashtable();
        DataTable dtAutotemp = new DataTable();
        bool bchk = false;

        Thread threadManual;            //수동쓰레드
        bool bManualstat = false;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            //Unloaded += MainWindow_Unloaded;
            Closing += MainWindow_Closing;
        }

        /// <summary>
        /// 로드 이벤트 OK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            NotifyInit();

            //시간 Text Binding
            txtPath.Content = Properties.Settings.Default.Path;
            txtTime.Content = Properties.Settings.Default.HH + "시 " + Properties.Settings.Default.mm + "분";
            spinAddMonth.EditValue = Properties.Settings.Default.AddMonth;

            //이벤트 선언
            btnTest.Click += BtnTest_Click;
            btnPath.Click += BtnPath_Click;
            spinAddMonth.EditValueChanged += SpinAddMonth_EditValueChanged;

            //AUTO쓰레드 선언
            threadAuto = new Thread(new ThreadStart(ThreadAutofx)) { IsBackground = true };
            threadAuto.Name = "EXPthreadAuto";
            threadAuto.Start();
        }

        private void SpinAddMonth_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.AddMonth = Convert.ToInt32(spinAddMonth.EditValue);
                Properties.Settings.Default.Save();
                Messages.ShowInfoMsgBox("분석월이 변경되었습니다.");
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 언로드 이벤트 (사용안됨)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                threadAuto.Abort();
                threadManual.Abort();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        
        #region 주요기능
        /// <summary>
        /// 데이터 조회
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private bool expcsv(string fileName, DateTime dt)
        {
            try
            {
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           this.Cursor = System.Windows.Input.Cursors.Wait;
                           ListBoxlog("CSV파일 생성 시작");
                           ListBoxlog("요금관리 시스템 사용량 조회 시작");
                       })));


                htAutoconditions.Clear();
                htAutoconditions.Add("PAY_YM", dt.AddMonths(Properties.Settings.Default.AddMonth).ToString("yyyyMM"));

                dtAutotemp = null;
                dtAutotemp = work.Select_Main(htAutoconditions);

                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           ListBoxlog("요금관리 시스템 사용량 조회 완료");
                           ListBoxlog("CSV 파일 생성 시작");
                       })));

                //파일 저장을 위해 스트림 생성.
                FileStream fs = new FileStream(Properties.Settings.Default.Path + fileName, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);

                //컬럼 이름들을 ","로 나누고 저장.
                string line = string.Join("$", dtAutotemp.Columns.Cast<object>());
                sw.WriteLine(line);

                //row들을 ","로 나누고 저장.
                foreach (DataRow item in dtAutotemp.Rows)
                {
                    line = string.Join("$", item.ItemArray.Cast<object>());
                    sw.WriteLine(line);
                }

                sw.Close();
                fs.Close();

                sw.Dispose();
                fs.Dispose();

                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           ListBoxlog("CSV 파일 생성 완료");
                           this.Cursor = null;
                       })));

                return true;
            }
            catch (Exception ex)
            {
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           this.Cursor = null;
                           ListBoxlog("CSV 파일 생성 실패");
                       })));

                return false;
            }
        } 
        #endregion

        #region Notify 관련
        /// <summary>
        /// Notify 생성
        /// </summary>
        private void NotifyInit()
        {
            //트레이아이콘 생성
            Notify = new System.Windows.Forms.NotifyIcon();
            Notify.Icon = Properties.Resources.TrayIcon;
            Notify.Text = "요금연계시스템";
            Notify.Visible = true;

            Notify.DoubleClick += Notify_DoubleClick;
            Notify.MouseDown += Notify_MouseDown;
        }

        /// <summary>
        /// Notify 더블클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Notify_DoubleClick(object sender, EventArgs e)
        {
            this.Topmost = true;
            this.Topmost = false;

            this.Show();
            this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// Notify 우클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Notify_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                System.Windows.Controls.ContextMenu m_menu = new System.Windows.Controls.ContextMenu();
                System.Windows.Controls.MenuItem menu = new System.Windows.Controls.MenuItem();
                menu.Header = "닫기";
                menu.Click += Menu_Click;
                m_menu.Items.Add(menu);

                m_menu.IsOpen = true;
            }
        }

        /// <summary>
        /// Notify 메뉴 닫기 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.Show();
            this.WindowState = WindowState.Normal;
            this.Topmost = true;

            if (Messages.ShowYesNoMsgBox("프로그램을 정말 종료 하시겠습니까?") == MessageBoxResult.Yes)
            {
                Notify.Dispose();
                System.Environment.Exit(0);
            }
        }
        #endregion

        #region UI관련
        /// <summary>
        /// 닫기버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// 경로설정 ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                dialog.ShowDialog();

                if(Messages.ShowYesNoMsgBox("경로를 변경하시겠습니까?") == MessageBoxResult.Yes)
                {
                    Properties.Settings.Default.Path = dialog.SelectedPath + "\\";
                    Properties.Settings.Default.Save();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }

            txtPath.Content = Properties.Settings.Default.Path;
        }

        /// <summary>
        /// 수동(내려받기) 버튼 클릭 이벤트 ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTest_Click(object sender, RoutedEventArgs e)
        {
            if (!bManualstat)
            {
                bManualstat = true;

                threadManual = null;
                threadManual = new Thread(new ThreadStart(ThreadManualfx)) { IsBackground = true }; ;
                threadManual.Name = "EXPthreadManual";

                threadManual.Start();
            }
            else
            {
                Messages.ShowInfoMsgBox("수동이 실행중입니다.");
            }
        }

        /// <summary>
        /// 메인 Log기록 (listbox, Logfile)
        /// </summary>
        /// <param name="strText"></param>
        public void ListBoxlog(string strText)
        {
            Paragraph paragraph = new Paragraph();
            paragraph.Inlines.Add(new Run(string.Format(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss : ") + strText)));
            richbox_Log.Document.Blocks.Add(paragraph);

            //마지막행 포커스
            richbox_Log.Focus();
            richbox_Log.ScrollToEnd();

            File.AppendAllText("log/UseWaterrichBoxlog" + DateTime.Now.ToShortDateString() + ".log", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss : " + strText + "\n"));

            if (richbox_Log.Document.Blocks.Count > 100)
            {
                richbox_Log.Document.Blocks.Remove(richbox_Log.Document.Blocks.FirstBlock);
            }
        }
        #endregion

        #region Thread 작업
        /// <summary>
        /// Auto Thread fx
        /// </summary>
        private void ThreadAutofx()
        {
            //자동 스케줄 작업시간 체크 및 저장을 위한 타이머 생성 및 선언 (1초마다 확인)
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        /// <summary>
        /// Auto Thread Timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            DateTime nowtime = DateTime.Now;

            if (nowtime.Day.ToString().Equals(DateTime.DaysInMonth(nowtime.Year, nowtime.Month).ToString())
                && nowtime.Hour.ToString().PadLeft(2, '0').Equals(Properties.Settings.Default.HH)
                && nowtime.Minute.ToString().PadLeft(2, '0').Equals(Properties.Settings.Default.mm)
                && nowtime.Second.ToString().PadLeft(2, '0').Equals("00"))
            {
                bchk = false;

                if (!bchk)
                {
                    try
                    {
                        bchk = expcsv("WATERUSE" + nowtime.ToString("yyyyMM") + ".csv", nowtime);
                    }
                    catch (Exception ex)
                    {
                        Messages.ErrLog(ex);
                    }
                    finally
                    {
                        int intruncnt = 0;

                        //실패시 9번 재반복 (초기 실행 1번 + 9번 반복 = 10번)
                        while (!bchk && intruncnt < 9)
                        {
                            intruncnt = intruncnt + 1;

                            this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                new Action(delegate ()
                                {
                                    ListBoxlog((intruncnt + 1).ToString() + "번째 시도...");
                                }));

                            try
                            {
                                bchk = expcsv("WATERUSE" + nowtime.ToString("yyyyMM") + ".csv", nowtime);
                            }
                            catch (Exception)
                            {
                            }
                        }

                        bchk = false;
                    }
                }
            }
        }

        /// <summary>
        /// Manual Thread fx
        /// </summary>
        private void ThreadManualfx()
        {
            try
            {
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           if(dateTest.Text == "")
                           {
                               Messages.ShowInfoMsgBox("분석월을 선택해 주세요.");
                               return;
                           }
                       })));

                int intYYYY = 0;
                int intMM = 0;

                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           intYYYY = dateTest.DateTime.Year;
                           intMM = dateTest.DateTime.Month;
                       })));

                expcsv("수동WATERUSE" + new DateTime(intYYYY, intMM, 1).ToString("yyyyMMddHHmmss") + ".csv", new DateTime(intYYYY, intMM, 1));
                bManualstat = false;
            }
            catch (Exception ex)
            {
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           ListBoxlog("수동 내려받기 실패하였습니다.");
                       })));
                bManualstat = false;
                Messages.ErrLog(ex);
            }
        }
        #endregion

    }
}
