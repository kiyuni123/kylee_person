﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WaterUseExcelEXP
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.ToString());
        }

        Mutex _mutex = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            string mutexName = "물사용량 다운로드";
            bool isCreatedNew = false;

            try
            {
                _mutex = new Mutex(true, mutexName, out isCreatedNew);
                if (isCreatedNew)
                {
                    base.OnStartup(e);
                }
                else
                {
                    Messages.ShowInfoMsgBox("프로그램이 실행중입니다.");
                    Application.Current.Shutdown();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex, "프로그램을 실행 할 수 없습니다.");
                Application.Current.Shutdown();
            }
        }
    }
}
