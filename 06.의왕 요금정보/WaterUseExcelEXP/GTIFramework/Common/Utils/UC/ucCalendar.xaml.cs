﻿using DevExpress.Xpf.Editors;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTIFramework.Common.Utils.UC
{
    /// <summary>
    /// Interaction logic for ucCalendar.xaml
    /// </summary>
    public partial class ucCalendar : UserControl
    {
        public ucCalendar()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            dtselect.DateTime = DateTime.Now;
        }

        private void MakeCal()
        {
            string[] strweekTbl = new string[] { "일", "월", "화", "수", "목", "금", "토" };
            int[] intmonthTbl = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

            int intnowYear = dtselect.DateTime.Year;
            int intnowMonth = dtselect.DateTime.Month;
            int intnowDay = dtselect.DateTime.Day;
            int intnowWeek = (int)dtselect.DateTime.DayOfWeek;
            int intBeMonth = dtselect.DateTime.AddMonths(-1).Month;
            int intAfMonth = dtselect.DateTime.AddMonths(1).Month;


            DateTime dtFirstdate = new DateTime(intnowYear, intnowMonth, 1);
            int intfirstWeek = (int)dtFirstdate.DayOfWeek;

            string[] strdayTbl = new string[42];

            try
            {
                if (intnowYear % 4 == 0 && intnowYear % 100 != 0 || intnowYear % 400 == 0)
                {
                    intmonthTbl[1] = 29;
                }

                for (int i = 0; i < intmonthTbl[intnowMonth - 1]; i++)
                {
                    strdayTbl[intfirstWeek + i] = (i + 1).ToString();

                    //if (intfirstWeek == 0)
                    //{
                    //    strdayTbl[intfirstWeek + i] = (i + 1).ToString();
                    //}
                    //else
                    //{
                    //    DateTime MonthIDX = new DateTime(2000, intnowMonth - 1, 1);
                    //    int intMonthIDX = MonthIDX.Month;

                    //    strdayTbl[i] = (intmonthTbl[intMonthIDX - 1] -0).ToString() ;
                    //}
                }

                for (int i = 0; i < intmonthTbl[intBeMonth - 1]; i++)
                {
                    strdayTbl[intfirstWeek + i] = (i + 1).ToString();


                }

                //for (int i = strdayTbl.Length-1 ; i >= 0; i--)
                //{
                //    if(strdayTbl[i]==null)
                //    {
                //        if(i>21) strdayTbl[i] = (strdayTbl.Length - i).ToString();
                //        else strdayTbl[i] = (strdayTbl.Length - i).ToString();
                //    }
                //}

                for (int i = 0; i < strweekTbl.Length; i++)
                {
                    Border border = new Border();
                    border.BorderBrush = Brushes.Gray;
                    TextBlock txt = new TextBlock();
                    txt.Foreground = Brushes.White;
                    txt.FontSize = 12;
                    txt.Text = strweekTbl[i];
                    border.Child = txt;

                    gdCal.Children.Add(border);
                    Grid.SetRow(border, 0);
                    Grid.SetColumn(border, 0);
                }

                for (int i = 0; i < 6; i++)
                {
                    RowDefinition row = new RowDefinition();
                    gdCal.RowDefinitions.Add(row);

                    for (int j = 0; j < 7; j++)
                    {
                        Border border = new Border();
                        border.Background = Brushes.Azure;

                        StackPanel stack = new StackPanel();
                        stack.Orientation = Orientation.Vertical;

                        TextBlock txt = new TextBlock();
                        txt.Foreground = Brushes.Black;
                        txt.FontSize = 12;
                        txt.Text = strdayTbl[j + (i * 7)];

                        //TextBlock txtInput = new TextBlock();
                        //txtInput.Foreground = Brushes.Black;
                        //txtInput.FontSize = 12;
                        //txtInput.Text = "Test " + i.ToString() + " " + j.ToString();

                        stack.Children.Add(txt);
                        //stack.Children.Add(txtInput);

                        border.Child = stack;

                        gdCal.Children.Add(border);
                        Grid.SetRow(border, i);
                        Grid.SetColumn(border, j);
                    }
                }
            }
            catch (Exception ex)
            {

                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void dtselect_EditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            gdCal.Children.Clear();
            gdCal.RowDefinitions.Clear();
            MakeCal();
        }

        private void before_Click(object sender, RoutedEventArgs e)
        {
            dtselect.DateTime = dtselect.DateTime.AddMonths(-1);
        }

        private void after_Click(object sender, RoutedEventArgs e)
        {
            dtselect.DateTime = dtselect.DateTime.AddMonths(1);
        }
    }
}
