﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterUseExcelIMP.Dao
{
    class MainDao
    {
        /// <summary>
        /// 사용량 입력
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Insert_WaterUse(ArrayList arrconditions)
        {
            DBManager.QueryForOracleBulkInsert("Insert_WaterUse", arrconditions);
        }

        /// <summary>
        /// 입력자료 확인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_WaterUseCHK(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_WaterUseCHK", conditions);
        }
    }
}
