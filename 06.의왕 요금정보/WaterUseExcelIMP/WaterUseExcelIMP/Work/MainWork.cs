﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterUseExcelIMP.Dao;

namespace WaterUseExcelIMP.Work
{
    class MainWork
    {
        MainDao dao = new MainDao();

        /// <summary>
        /// 사용량 입력
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Insert_WaterUse(ArrayList arrconditions)
        {   
            dao.Insert_WaterUse(arrconditions);
        }

        /// <summary>
        /// 입력자료 확인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_WaterUseCHK(Hashtable conditions)
        {
            return dao.Select_WaterUseCHK(conditions);
        }
    }
}
