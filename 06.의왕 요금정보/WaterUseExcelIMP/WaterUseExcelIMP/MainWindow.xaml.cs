﻿
using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WaterUseExcelIMP.Work;

namespace WaterUseExcelIMP
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWork work = new MainWork();
        public static NotifyIcon Notify;

        Thread threadAuto;              //자동쓰레드
        System.Timers.Timer timer;      //자동타이머
        Hashtable htAutoconditions = new Hashtable();
        DataTable dtAutotemp = new DataTable();
        DateTime nowtime;
        bool bchk = false;

        Thread threadManual;            //수동쓰레드
        bool bManualstat = false;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            //Unloaded += MainWindow_Unloaded;
            Closing += MainWindow_Closing;
        }

        /// <summary>
        /// 로드 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            NotifyInit();

            //시간 Text Binding
            txtPath.Content = Properties.Settings.Default.Path;
            txtTime.Content = Properties.Settings.Default.HH + "시 " + Properties.Settings.Default.mm + "분";
            spinAddMonth.EditValue = Properties.Settings.Default.AddMonth;

            //이벤트 선언
            btnTest.Click += BtnTest_Click;
            btnPath.Click += BtnPath_Click;
            spinAddMonth.EditValueChanged += SpinAddMonth_EditValueChanged;

            //AUTO쓰레드 선언
            threadAuto = new Thread(new ThreadStart(ThreadAutofx));
            threadAuto.Name = "IMPthreadAuto";
            threadAuto.Start();
        }

        private void SpinAddMonth_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.AddMonth = Convert.ToInt32(spinAddMonth.EditValue);
                Properties.Settings.Default.Save();
                Messages.ShowInfoMsgBox("분석월이 변경되었습니다.");
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 언로드 이벤트 (사용안됨)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                threadAuto.Abort();
                threadManual.Abort();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        #region 주요기능
        /// <summary>
        /// 데이터 IMP
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private bool csvimp(string filename)
        {
            try
            {
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            ListBoxlog("csv파일 업로드 시작");
                        })));

                dtAutotemp = null;
                dtAutotemp = new DataTable();

                StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("euc-kr"));

                string[] headers = sr.ReadLine().Split('$');

                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            ListBoxlog("csv 파일 Load 시작");
                        })));


                foreach (string header in headers)
                {
                    dtAutotemp.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split('$');
                    DataRow dr = dtAutotemp.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dtAutotemp.Rows.Add(dr);
                }

                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            ListBoxlog("csv 파일 Load 완료");
                            ListBoxlog("데이터 검증 시작");
                        })));

                Hashtable chkconditions = new Hashtable();
                string strym = "";

                foreach (DataRow r in dtAutotemp.DefaultView.ToTable(true, "YM").Rows)
                {
                    strym = strym + r["YM"].ToString() + ",";
                }

                chkconditions.Add("YM", strym.Substring(0, strym.Length - 1));

                if (work.Select_WaterUseCHK(chkconditions).Rows.Count == 0)
                {
                    List<string> YM = dtAutotemp.AsEnumerable().Select(x => x["YM"].ToString()).ToList();
                    List<string> D = dtAutotemp.AsEnumerable().Select(x => x["D"].ToString()).ToList();
                    List<string> CUSTOMER_NO = dtAutotemp.AsEnumerable().Select(x => x["CUSTOMER_NO"].ToString()).ToList();
                    List<string> CUSTOMER_NAME = dtAutotemp.AsEnumerable().Select(x => x["CUSTOMER_NAME"].ToString()).ToList();
                    List<string> ADDRESS = dtAutotemp.AsEnumerable().Select(x => x["ADDRESS"].ToString()).ToList();
                    List<string> USE_WATER = dtAutotemp.AsEnumerable().Select(x => x["USE_WATER"].ToString()).ToList();
                    List<string> INDUSTRY_CLASS_NAME = dtAutotemp.AsEnumerable().Select(x => x["INDUSTRY_CLASS_NAME"].ToString()).ToList();
                    List<string> DIAMETER = dtAutotemp.AsEnumerable().Select(x => x["DIAMETER"].ToString()).ToList();
                    List<string> AREA_CODE = dtAutotemp.AsEnumerable().Select(x => x["AREA_CODE"].ToString()).ToList();

                    this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            ListBoxlog("데이터 검증 완료");
                        })));


                    ArrayList tmpArr = new ArrayList();

                    tmpArr.Add(YM);
                    tmpArr.Add(D);
                    tmpArr.Add(CUSTOMER_NO);
                    tmpArr.Add(CUSTOMER_NAME);
                    tmpArr.Add(ADDRESS);
                    tmpArr.Add(USE_WATER);
                    tmpArr.Add(INDUSTRY_CLASS_NAME);
                    tmpArr.Add(DIAMETER);
                    tmpArr.Add(AREA_CODE);

                    work.Insert_WaterUse(tmpArr);

                    this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            ListBoxlog("csv파일 업로드 완료");
                        })));

                    sr.Dispose();
                }
                else
                {
                    this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            ListBoxlog(strym.Substring(0, strym.Length - 1) + " 자료가 이미 존재합니다.");
                        })));

                    sr.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                       new Action((delegate ()
                       {
                           this.Cursor = null;
                           ListBoxlog("csv파일 업로드 실패");
                       })));
                Messages.ErrLog(ex);

                return false;
            }
        }
        #endregion

        #region Notify 관련
        /// <summary>
        /// Notify 생성
        /// </summary>
        private void NotifyInit()
        {
            //트레이아이콘 생성
            Notify = new System.Windows.Forms.NotifyIcon();
            Notify.Icon = Properties.Resources.TrayIcon;
            Notify.Text = "요금연계시스템";
            Notify.Visible = true;

            Notify.DoubleClick += Notify_DoubleClick;
            Notify.MouseDown += Notify_MouseDown;
        }

        /// <summary>
        /// Notify 더블클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Notify_DoubleClick(object sender, EventArgs e)
        {
            this.Topmost = true;
            this.Topmost = false;

            this.Show();
            this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// Notify 우클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Notify_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                System.Windows.Controls.ContextMenu m_menu = new System.Windows.Controls.ContextMenu();
                System.Windows.Controls.MenuItem menu = new System.Windows.Controls.MenuItem();
                menu.Header = "닫기";
                menu.Click += Menu_Click;
                m_menu.Items.Add(menu);

                m_menu.IsOpen = true;
            };
        }

        /// <summary>
        /// Notify 메뉴 닫기 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.Show();
            this.WindowState = WindowState.Normal;
            this.Topmost = true;

            if (Messages.ShowYesNoMsgBox("프로그램을 정말 종료 하시겠습니까?") == MessageBoxResult.Yes)
            {
                Notify.Dispose();
                System.Environment.Exit(0);
            }
        }
        #endregion

        #region UI 관련
        /// <summary>
        /// 닫기버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// 경로설정 ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                dialog.ShowDialog();

                if (Messages.ShowYesNoMsgBox("경로를 변경하시겠습니까?") == MessageBoxResult.Yes)
                {
                    Properties.Settings.Default.Path = dialog.SelectedPath + "\\";
                    Properties.Settings.Default.Save();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }

            txtPath.Content = Properties.Settings.Default.Path;
        }

        /// <summary>
        /// 수동(올리기) 버튼 클릭 이벤트 ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTest_Click(object sender, RoutedEventArgs e)
        {
            if (!bManualstat)
            {
                bManualstat = true;

                threadManual = null;
                threadManual = new Thread(new ThreadStart(ThreadManualfx));
                threadManual.Name = "EXPthreadManual";

                threadManual.Start();
            }
            else
            {
                Messages.ShowInfoMsgBox("수동이 실행중입니다.");
            }
        }

        /// <summary>
        /// 메인 Log기록 (listbox, Logfile)
        /// </summary>
        /// <param name="strText"></param>
        public void ListBoxlog(string strText)
        {
            Paragraph paragraph = new Paragraph();
            paragraph.Inlines.Add(new Run(string.Format(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss : ") + strText)));
            richbox_Log.Document.Blocks.Add(paragraph);

            //마지막행 포커스
            richbox_Log.Focus();
            richbox_Log.ScrollToEnd();

            File.AppendAllText("log/UseWaterrichBoxlog" + DateTime.Now.ToShortDateString() + ".log", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss : " + strText + "\n"), Encoding.UTF8);

            if (richbox_Log.Document.Blocks.Count > 100)
            {
                richbox_Log.Document.Blocks.Remove(richbox_Log.Document.Blocks.FirstBlock);
            }
        }
        #endregion

        #region Thread 작업
        /// <summary>
        /// Auto Thread fx
        /// </summary>
        private void ThreadAutofx()
        {
            //자동 스케줄 작업시간 체크 및 저장을 위한 타이머 생성 및 선언 (1초마다 확인)
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        /// <summary>
        /// Auto Thread Timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            nowtime = DateTime.Now;

            //작업 성공여부
            bchk = false;

            if (!bchk)
            {
                //말일, 설정시간, 설정분 00초
                if (nowtime.Day.ToString().Equals(DateTime.DaysInMonth(nowtime.Year, nowtime.Month).ToString())
                && nowtime.Hour.ToString().PadLeft(2, '0').Equals(Properties.Settings.Default.HH)
                && nowtime.Minute.ToString().PadLeft(2, '0').Equals(Properties.Settings.Default.mm)
                && nowtime.Second.ToString().PadLeft(2, '0').Equals("00"))
                {
                    try
                    {
                        FileInfo fi = new FileInfo(Properties.Settings.Default.Path + "\\WATERUSE" + DateTime.Now.AddMonths(Properties.Settings.Default.AddMonth).ToString("yyyyMM") + ".csv");
                        DirectoryInfo di = new DirectoryInfo(Properties.Settings.Default.Path);

                        if (di.Exists)
                        {
                            if (fi.Exists)
                            {
                                bchk = csvimp(Properties.Settings.Default.Path + "\\WATERUSE" + DateTime.Now.AddMonths(Properties.Settings.Default.AddMonth).ToString("yyyyMM") + ".csv");
                            }
                            else
                            {
                                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                    new Action((delegate ()
                                    {
                                        ListBoxlog("사용량 파일 WATERUSE" + DateTime.Now.AddMonths(Properties.Settings.Default.AddMonth).ToString("yyyyMM") + ".csv 파일이 없습니다.");
                                    })));
                                bchk = true;
                            }
                        }
                        else
                        {
                            this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                new Action((delegate ()
                                {
                                    ListBoxlog("설정 경로가 없습니다.");
                                })));
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        Messages.ErrLog(ex);
                    }
                    finally
                    {
                        int intruncnt = 0;

                        //실패시 9번 재반복 (초기 실행 1번 + 9번 반복 = 10번)
                        while (!bchk && intruncnt < 9)
                        {
                            intruncnt = intruncnt + 1;
                            this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                new Action((delegate ()
                                {
                                    ListBoxlog((intruncnt + 1).ToString() + "번째 시도...");
                                })));

                            try
                            {
                                bchk = csvimp(Properties.Settings.Default.Path + "\\WATERUSE" + DateTime.Now.AddMonths(Properties.Settings.Default.AddMonth).ToString("yyyyMM") + ".csv");
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Manual Thread fx
        /// </summary>
        private void ThreadManualfx()
        {
            bool brun = true;

            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "csv files (*.csv)|*.csv";
            openFileDialog.InitialDirectory = "";

            try
            {
                Nullable<bool> bopen = openFileDialog.ShowDialog();

                if (bopen == true)
                {
                    brun = csvimp(openFileDialog.FileName);
                    bManualstat = false;
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                bManualstat = false;
            }
            finally
            {
                int intruncnt = 0;

                while (!brun && intruncnt < 9)
                {
                    intruncnt = intruncnt + 1;
                    this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                new Action((delegate ()
                                {
                                    ListBoxlog((intruncnt + 1).ToString() + "번째 시도...");
                                })));
                    brun = csvimp(openFileDialog.FileName);
                }

                bManualstat = false;
            }
        }
        #endregion
    }
}
